.class public Lcom/facebook/search/results/rows/sections/videos/SearchResultsLiveVideoPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/7Lk;",
        ":",
        "LX/CxP;",
        ":",
        "LX/CxV;",
        ":",
        "LX/Cxj;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/CzL",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;>;",
        "Ljava/lang/Void;",
        "TE;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "LX/3J0;",
            ">;"
        }
    .end annotation
.end field

.field private static h:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/video/channelfeed/ChannelFeedGenericLauncherPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/video/channelfeed/ChannelFeedGenericLauncherPartDefinition",
            "<TE;",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/D4u;

.field private final e:Lcom/facebook/video/channelfeed/PublisherInfoPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/video/channelfeed/PublisherInfoPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CvY;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 609766
    new-instance v0, LX/3bF;

    invoke-direct {v0}, LX/3bF;-><init>()V

    sput-object v0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsLiveVideoPartDefinition;->a:LX/1Cz;

    .line 609767
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    sput-object v0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsLiveVideoPartDefinition;->b:Ljava/util/concurrent/atomic/AtomicReference;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/video/channelfeed/ChannelFeedGenericLauncherPartDefinition;LX/D4u;Lcom/facebook/video/channelfeed/PublisherInfoPartDefinition;LX/0Ot;Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerPartDefinition;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/video/channelfeed/ChannelFeedGenericLauncherPartDefinition;",
            "LX/D4u;",
            "Lcom/facebook/video/channelfeed/PublisherInfoPartDefinition;",
            "LX/0Ot",
            "<",
            "LX/CvY;",
            ">;",
            "Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerPartDefinition;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 609768
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 609769
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsLiveVideoPartDefinition;->c:Lcom/facebook/video/channelfeed/ChannelFeedGenericLauncherPartDefinition;

    .line 609770
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsLiveVideoPartDefinition;->d:LX/D4u;

    .line 609771
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsLiveVideoPartDefinition;->e:Lcom/facebook/video/channelfeed/PublisherInfoPartDefinition;

    .line 609772
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsLiveVideoPartDefinition;->f:LX/0Ot;

    .line 609773
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsLiveVideoPartDefinition;->g:Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerPartDefinition;

    .line 609774
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/videos/SearchResultsLiveVideoPartDefinition;
    .locals 9

    .prologue
    .line 609775
    const-class v1, Lcom/facebook/search/results/rows/sections/videos/SearchResultsLiveVideoPartDefinition;

    monitor-enter v1

    .line 609776
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsLiveVideoPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 609777
    sput-object v2, Lcom/facebook/search/results/rows/sections/videos/SearchResultsLiveVideoPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 609778
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 609779
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 609780
    new-instance v3, Lcom/facebook/search/results/rows/sections/videos/SearchResultsLiveVideoPartDefinition;

    invoke-static {v0}, Lcom/facebook/video/channelfeed/ChannelFeedGenericLauncherPartDefinition;->a(LX/0QB;)Lcom/facebook/video/channelfeed/ChannelFeedGenericLauncherPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/video/channelfeed/ChannelFeedGenericLauncherPartDefinition;

    invoke-static {v0}, LX/D4u;->a(LX/0QB;)LX/D4u;

    move-result-object v5

    check-cast v5, LX/D4u;

    invoke-static {v0}, Lcom/facebook/video/channelfeed/PublisherInfoPartDefinition;->a(LX/0QB;)Lcom/facebook/video/channelfeed/PublisherInfoPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/video/channelfeed/PublisherInfoPartDefinition;

    const/16 v7, 0x32d4

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerPartDefinition;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/search/results/rows/sections/videos/SearchResultsLiveVideoPartDefinition;-><init>(Lcom/facebook/video/channelfeed/ChannelFeedGenericLauncherPartDefinition;LX/D4u;Lcom/facebook/video/channelfeed/PublisherInfoPartDefinition;LX/0Ot;Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerPartDefinition;)V

    .line 609781
    move-object v0, v3

    .line 609782
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 609783
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsLiveVideoPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 609784
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 609785
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 609786
    sget-object v0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsLiveVideoPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 609787
    check-cast p2, LX/CzL;

    check-cast p3, LX/1Pn;

    .line 609788
    const v0, 0x7f0d2ca1

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsLiveVideoPartDefinition;->g:Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerPartDefinition;

    invoke-interface {p1, v0, v1, p2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 609789
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 609790
    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 609791
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v1

    .line 609792
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 609793
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 609794
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 609795
    const v1, 0x7f0d0952

    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsLiveVideoPartDefinition;->e:Lcom/facebook/video/channelfeed/PublisherInfoPartDefinition;

    .line 609796
    new-instance v3, LX/D6O;

    invoke-direct {v3, v0}, LX/D6O;-><init>(Lcom/facebook/graphql/model/GraphQLStory;)V

    move-object v3, v3

    .line 609797
    invoke-virtual {p2}, LX/CzL;->j()Z

    move-result v4

    .line 609798
    iput-boolean v4, v3, LX/D6O;->b:Z

    .line 609799
    move-object v3, v3

    .line 609800
    invoke-static {v0}, LX/16y;->b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    .line 609801
    iput-object v0, v3, LX/D6O;->c:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 609802
    move-object v0, v3

    .line 609803
    new-instance v3, LX/D6P;

    iget-object v4, v0, LX/D6O;->a:Lcom/facebook/graphql/model/GraphQLStory;

    iget-boolean v5, v0, LX/D6O;->b:Z

    iget-object v6, v0, LX/D6O;->c:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-direct {v3, v4, v5, v6}, LX/D6P;-><init>(Lcom/facebook/graphql/model/GraphQLStory;ZLcom/facebook/graphql/model/GraphQLTextWithEntities;)V

    move-object v0, v3

    .line 609804
    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 609805
    const/4 v1, 0x0

    .line 609806
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 609807
    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 609808
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v2

    .line 609809
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 609810
    invoke-static {v0}, LX/17E;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    .line 609811
    if-nez v2, :cond_1

    move-object v0, v1

    .line 609812
    :goto_0
    move-object v0, v0

    .line 609813
    if-eqz v0, :cond_0

    .line 609814
    const v1, 0x7f0d0952

    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsLiveVideoPartDefinition;->c:Lcom/facebook/video/channelfeed/ChannelFeedGenericLauncherPartDefinition;

    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 609815
    :cond_0
    const/4 v0, 0x0

    return-object v0

    .line 609816
    :cond_1
    new-instance v0, LX/3Qv;

    invoke-direct {v0}, LX/3Qv;-><init>()V

    invoke-static {v2, v1}, LX/D4u;->a(Lcom/facebook/graphql/model/GraphQLActor;Ljava/lang/String;)LX/D4s;

    move-result-object v1

    .line 609817
    iput-object v1, v0, LX/3Qv;->k:LX/D4s;

    .line 609818
    move-object v0, v0

    .line 609819
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->ar()Lcom/facebook/graphql/model/GraphQLSinglePublisherVideoChannelsConnection;

    move-result-object v1

    .line 609820
    if-nez v1, :cond_3

    .line 609821
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v1

    .line 609822
    if-eqz v1, :cond_2

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 609823
    :goto_1
    move-object v1, v1

    .line 609824
    invoke-virtual {v0, v1}, LX/3Qv;->b(Ljava/util/List;)LX/3Qv;

    move-result-object v0

    const-string v1, "SEARCH"

    .line 609825
    iput-object v1, v0, LX/3Qv;->d:Ljava/lang/String;

    .line 609826
    move-object v0, v0

    .line 609827
    sget-object v1, LX/04g;->BY_USER:LX/04g;

    .line 609828
    iput-object v1, v0, LX/3Qv;->h:LX/04g;

    .line 609829
    move-object v0, v0

    .line 609830
    sget-object v1, LX/04D;->SEARCH_RESULTS:LX/04D;

    .line 609831
    iput-object v1, v0, LX/3Qv;->g:LX/04D;

    .line 609832
    move-object v1, v0

    .line 609833
    move-object v0, p3

    check-cast v0, LX/CxV;

    invoke-interface {v0}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->A()LX/8ef;

    move-result-object v0

    invoke-interface {v0}, LX/8ef;->c()Ljava/lang/String;

    move-result-object v0

    .line 609834
    iput-object v0, v1, LX/3Qv;->m:Ljava/lang/String;

    .line 609835
    move-object v0, v1

    .line 609836
    invoke-virtual {v0}, LX/3Qv;->a()LX/3Qw;

    move-result-object v1

    .line 609837
    new-instance v0, LX/D4q;

    sget-object v3, Lcom/facebook/search/results/rows/sections/videos/SearchResultsLiveVideoPartDefinition;->b:Ljava/util/concurrent/atomic/AtomicReference;

    new-instance v4, LX/EPg;

    invoke-direct {v4, p0, p3, p2, v2}, LX/EPg;-><init>(Lcom/facebook/search/results/rows/sections/videos/SearchResultsLiveVideoPartDefinition;LX/1Pn;LX/CzL;Lcom/facebook/graphql/model/GraphQLActor;)V

    invoke-direct {v0, v1, v3, v4}, LX/D4q;-><init>(LX/3Qw;Ljava/util/concurrent/atomic/AtomicReference;LX/D6L;)V

    goto :goto_0

    .line 609838
    :cond_2
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 609839
    goto :goto_1

    .line 609840
    :cond_3
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 609841
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLSinglePublisherVideoChannelsConnection;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    const/4 v1, 0x0

    move v3, v1

    :goto_2
    if-ge v3, v6, :cond_6

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLSinglePublisherVideoChannelsEdge;

    .line 609842
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLSinglePublisherVideoChannelsEdge;->a()Lcom/facebook/graphql/model/GraphQLVideoChannel;

    move-result-object v1

    .line 609843
    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLVideoChannel;->k()Ljava/lang/String;

    move-result-object v1

    .line 609844
    :goto_3
    if-eqz v1, :cond_4

    .line 609845
    invoke-virtual {v4, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 609846
    :cond_4
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_2

    .line 609847
    :cond_5
    const/4 v1, 0x0

    goto :goto_3

    .line 609848
    :cond_6
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 609849
    const/4 v0, 0x1

    return v0
.end method
