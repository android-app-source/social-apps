.class public Lcom/facebook/search/results/rows/sections/derp/SmallPhotoShareAttachmentWithMarginPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        "V:",
        "Landroid/view/View;",
        ":",
        "LX/35o;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "TE;TV;>;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;


# instance fields
.field private final b:Lcom/facebook/feedplugins/attachments/linkshare/SmallPhotoShareAttachmentPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 610967
    new-instance v0, LX/3bP;

    invoke-direct {v0}, LX/3bP;-><init>()V

    sput-object v0, Lcom/facebook/search/results/rows/sections/derp/SmallPhotoShareAttachmentWithMarginPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedplugins/attachments/linkshare/SmallPhotoShareAttachmentPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 610964
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 610965
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/derp/SmallPhotoShareAttachmentWithMarginPartDefinition;->b:Lcom/facebook/feedplugins/attachments/linkshare/SmallPhotoShareAttachmentPartDefinition;

    .line 610966
    return-void
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 610959
    sget-object v0, Lcom/facebook/search/results/rows/sections/derp/SmallPhotoShareAttachmentWithMarginPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 610961
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 610962
    const v0, 0x7f0d2cb3

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/derp/SmallPhotoShareAttachmentWithMarginPartDefinition;->b:Lcom/facebook/feedplugins/attachments/linkshare/SmallPhotoShareAttachmentPartDefinition;

    invoke-interface {p1, v0, v1, p2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 610963
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 610960
    const/4 v0, 0x1

    return v0
.end method
