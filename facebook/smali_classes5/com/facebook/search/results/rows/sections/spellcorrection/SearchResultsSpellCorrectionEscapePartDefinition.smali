.class public Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellCorrectionEscapePartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/CxG;",
        ":",
        "LX/CxV;",
        ":",
        "LX/1Pn;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsSpellCorrectionUnit;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;",
            ">;"
        }
    .end annotation
.end field

.field private static h:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final d:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

.field private final e:Lcom/facebook/content/SecureContextHelper;

.field private final f:LX/1nD;

.field private final g:LX/CvY;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 608022
    const v0, 0x7f0309b0

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellCorrectionEscapePartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;Lcom/facebook/content/SecureContextHelper;LX/1nD;LX/CvY;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 608074
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 608075
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellCorrectionEscapePartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 608076
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellCorrectionEscapePartDefinition;->c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 608077
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellCorrectionEscapePartDefinition;->d:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    .line 608078
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellCorrectionEscapePartDefinition;->e:Lcom/facebook/content/SecureContextHelper;

    .line 608079
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellCorrectionEscapePartDefinition;->f:LX/1nD;

    .line 608080
    iput-object p6, p0, Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellCorrectionEscapePartDefinition;->g:LX/CvY;

    .line 608081
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellCorrectionEscapePartDefinition;
    .locals 10

    .prologue
    .line 608063
    const-class v1, Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellCorrectionEscapePartDefinition;

    monitor-enter v1

    .line 608064
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellCorrectionEscapePartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 608065
    sput-object v2, Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellCorrectionEscapePartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 608066
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 608067
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 608068
    new-instance v3, Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellCorrectionEscapePartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v7

    check-cast v7, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/1nD;->a(LX/0QB;)LX/1nD;

    move-result-object v8

    check-cast v8, LX/1nD;

    invoke-static {v0}, LX/CvY;->a(LX/0QB;)LX/CvY;

    move-result-object v9

    check-cast v9, LX/CvY;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellCorrectionEscapePartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;Lcom/facebook/content/SecureContextHelper;LX/1nD;LX/CvY;)V

    .line 608069
    move-object v0, v3

    .line 608070
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 608071
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellCorrectionEscapePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 608072
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 608073
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;)Z
    .locals 1

    .prologue
    .line 608062
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;->DID_YOU_MEAN:Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;

    if-eq p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellCorrectionEscapePartDefinition;Landroid/content/Context;LX/1Ps;Lcom/facebook/search/results/model/unit/SearchResultsSpellCorrectionUnit;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "TE;",
            "Lcom/facebook/search/results/model/unit/SearchResultsSpellCorrectionUnit;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    .line 608042
    iget-object v0, p3, Lcom/facebook/search/results/model/unit/SearchResultsSpellCorrectionUnit;->a:Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellCorrectionEscapePartDefinition;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 608043
    iget-object v1, p3, Lcom/facebook/search/results/model/unit/SearchResultsSpellCorrectionUnit;->d:Ljava/lang/String;

    .line 608044
    iget-object v2, p3, Lcom/facebook/search/results/model/unit/SearchResultsSpellCorrectionUnit;->e:Ljava/lang/String;

    .line 608045
    :goto_0
    iget-object v0, p3, Lcom/facebook/search/results/model/unit/SearchResultsSpellCorrectionUnit;->f:LX/CwB;

    if-eqz v0, :cond_1

    iget-object v0, p3, Lcom/facebook/search/results/model/unit/SearchResultsSpellCorrectionUnit;->f:LX/CwB;

    invoke-interface {v0}, LX/CwB;->k()LX/103;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 608046
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellCorrectionEscapePartDefinition;->f:LX/1nD;

    iget-object v2, p3, Lcom/facebook/search/results/model/unit/SearchResultsSpellCorrectionUnit;->f:LX/CwB;

    invoke-interface {v2}, LX/CwB;->k()LX/103;

    move-result-object v2

    iget-object v3, p3, Lcom/facebook/search/results/model/unit/SearchResultsSpellCorrectionUnit;->f:LX/CwB;

    invoke-interface {v3}, LX/CwB;->i()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p3, Lcom/facebook/search/results/model/unit/SearchResultsSpellCorrectionUnit;->f:LX/CwB;

    invoke-interface {v4}, LX/CwB;->h()LX/0P1;

    move-result-object v4

    invoke-static {v2, v1, v3, v4}, LX/7BG;->a(LX/103;Ljava/lang/String;Ljava/lang/String;LX/0P1;)Ljava/lang/String;

    move-result-object v2

    move-object v3, p2

    check-cast v3, LX/CxV;

    invoke-interface {v3}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v3

    .line 608047
    iget-object v4, v3, Lcom/facebook/search/results/model/SearchResultsMutableContext;->t:Ljava/lang/String;

    move-object v3, v4

    .line 608048
    iget-object v4, p3, Lcom/facebook/search/results/model/unit/SearchResultsSpellCorrectionUnit;->f:LX/CwB;

    invoke-interface {v4}, LX/CwB;->h()LX/0P1;

    move-result-object v4

    sget-object v5, LX/8ci;->p:LX/8ci;

    move-object v6, p2

    check-cast v6, LX/CxV;

    invoke-interface {v6}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v6

    .line 608049
    iget-object v8, v6, Lcom/facebook/search/results/model/SearchResultsMutableContext;->e:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    move-object v6, v8

    .line 608050
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    iget-object v8, p3, Lcom/facebook/search/results/model/unit/SearchResultsSpellCorrectionUnit;->f:LX/CwB;

    invoke-interface {v8}, LX/CwB;->k()LX/103;

    move-result-object v8

    iget-object v9, p3, Lcom/facebook/search/results/model/unit/SearchResultsSpellCorrectionUnit;->f:LX/CwB;

    invoke-interface {v9}, LX/CwB;->i()Ljava/lang/String;

    move-result-object v9

    iget-object v10, p3, Lcom/facebook/search/results/model/unit/SearchResultsSpellCorrectionUnit;->f:LX/CwB;

    invoke-interface {v10}, LX/CwB;->j()Ljava/lang/String;

    move-result-object v10

    invoke-virtual/range {v0 .. v10}, LX/1nD;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0P1;LX/8ci;Lcom/facebook/search/logging/api/SearchTypeaheadSession;Ljava/lang/Boolean;LX/103;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 608051
    :goto_1
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellCorrectionEscapePartDefinition;->e:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 608052
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellCorrectionEscapePartDefinition;->g:LX/CvY;

    move-object v1, p2

    check-cast v1, LX/CxV;

    invoke-interface {v1}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v1

    check-cast p2, LX/CxG;

    invoke-interface {p2, p3}, LX/CxG;->a(Ljava/lang/Object;)I

    move-result v2

    iget-object v3, p3, Lcom/facebook/search/results/model/unit/SearchResultsSpellCorrectionUnit;->a:Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;

    iget-object v4, p3, Lcom/facebook/search/results/model/unit/SearchResultsSpellCorrectionUnit;->d:Ljava/lang/String;

    iget-object v5, p3, Lcom/facebook/search/results/model/unit/SearchResultsSpellCorrectionUnit;->e:Ljava/lang/String;

    .line 608053
    invoke-static {v1, v2, v3, v4, v5}, LX/CvY;->c(Lcom/facebook/search/results/model/SearchResultsMutableContext;ILcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    invoke-static {v0, v1, v6}, LX/CvY;->a(LX/CvY;Lcom/facebook/search/results/model/SearchResultsMutableContext;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 608054
    return-void

    .line 608055
    :cond_0
    iget-object v1, p3, Lcom/facebook/search/results/model/unit/SearchResultsSpellCorrectionUnit;->b:Ljava/lang/String;

    .line 608056
    iget-object v2, p3, Lcom/facebook/search/results/model/unit/SearchResultsSpellCorrectionUnit;->c:Ljava/lang/String;

    goto :goto_0

    .line 608057
    :cond_1
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellCorrectionEscapePartDefinition;->f:LX/1nD;

    move-object v3, p2

    check-cast v3, LX/CxV;

    invoke-interface {v3}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v3

    .line 608058
    iget-object v4, v3, Lcom/facebook/search/results/model/SearchResultsMutableContext;->t:Ljava/lang/String;

    move-object v3, v4

    .line 608059
    sget-object v4, LX/8ci;->p:LX/8ci;

    move-object v5, p2

    check-cast v5, LX/CxV;

    invoke-interface {v5}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v5

    .line 608060
    iget-object v6, v5, Lcom/facebook/search/results/model/SearchResultsMutableContext;->e:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    move-object v5, v6

    .line 608061
    sget-object v6, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual/range {v0 .. v7}, LX/1nD;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/8ci;Lcom/facebook/search/logging/api/SearchTypeaheadSession;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Ljava/lang/Boolean;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_1
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;",
            ">;"
        }
    .end annotation

    .prologue
    .line 608041
    sget-object v0, Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellCorrectionEscapePartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 608024
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Ps;

    .line 608025
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 608026
    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsSpellCorrectionUnit;

    .line 608027
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellCorrectionEscapePartDefinition;->c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    new-instance v2, LX/EPU;

    invoke-direct {v2, p0, p3, v0}, LX/EPU;-><init>(Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellCorrectionEscapePartDefinition;LX/1Ps;Lcom/facebook/search/results/model/unit/SearchResultsSpellCorrectionUnit;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 608028
    iget-object v1, v0, Lcom/facebook/search/results/model/unit/SearchResultsSpellCorrectionUnit;->a:Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;->DID_YOU_MEAN:Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;

    if-ne v1, v2, :cond_0

    const/high16 v1, 0x40c00000    # 6.0f

    .line 608029
    :goto_0
    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellCorrectionEscapePartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v3, LX/1X6;

    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v4

    .line 608030
    iput v1, v4, LX/1UY;->b:F

    .line 608031
    move-object v1, v4

    .line 608032
    invoke-virtual {v1}, LX/1UY;->i()LX/1Ua;

    move-result-object v1

    invoke-direct {v3, p2, v1}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v2, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 608033
    iget-object v1, v0, Lcom/facebook/search/results/model/unit/SearchResultsSpellCorrectionUnit;->a:Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;

    invoke-static {v1}, Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellCorrectionEscapePartDefinition;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;)Z

    move-result v2

    .line 608034
    if-eqz v2, :cond_1

    const v1, 0x7f0822ad

    .line 608035
    :goto_1
    if-eqz v2, :cond_2

    iget-object v0, v0, Lcom/facebook/search/results/model/unit/SearchResultsSpellCorrectionUnit;->b:Ljava/lang/String;

    .line 608036
    :goto_2
    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellCorrectionEscapePartDefinition;->d:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    check-cast p3, LX/1Pn;

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-virtual {v3, v1, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-interface {p1, v2, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 608037
    const/4 v0, 0x0

    return-object v0

    .line 608038
    :cond_0
    const/high16 v1, 0x40000000    # 2.0f

    goto :goto_0

    .line 608039
    :cond_1
    const v1, 0x7f0822ae

    goto :goto_1

    .line 608040
    :cond_2
    iget-object v0, v0, Lcom/facebook/search/results/model/unit/SearchResultsSpellCorrectionUnit;->d:Ljava/lang/String;

    goto :goto_2
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 608023
    const/4 v0, 0x1

    return v0
.end method
