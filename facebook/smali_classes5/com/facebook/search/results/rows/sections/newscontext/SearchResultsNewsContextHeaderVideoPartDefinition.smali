.class public Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextHeaderVideoPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/CzL",
        "<",
        "Ljava/lang/Object;",
        ">;",
        "Ljava/lang/Void;",
        "LX/Cxk;",
        "Lcom/facebook/feedplugins/video/RichVideoAttachmentView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;


# instance fields
.field private final b:Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerPartDefinition;

.field private final c:Lcom/facebook/multirow/parts/ViewDimensionsPartDefinition;

.field private final d:LX/19B;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 608696
    new-instance v0, LX/3az;

    invoke-direct {v0}, LX/3az;-><init>()V

    sput-object v0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextHeaderVideoPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerPartDefinition;Lcom/facebook/multirow/parts/ViewDimensionsPartDefinition;LX/19B;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 608697
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 608698
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextHeaderVideoPartDefinition;->b:Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerPartDefinition;

    .line 608699
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextHeaderVideoPartDefinition;->c:Lcom/facebook/multirow/parts/ViewDimensionsPartDefinition;

    .line 608700
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextHeaderVideoPartDefinition;->d:LX/19B;

    .line 608701
    return-void
.end method

.method private static b(LX/CzL;)LX/CzL;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CzL",
            "<",
            "Ljava/lang/Object;",
            ">;)",
            "LX/CzL",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 608702
    iget-object v0, p0, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v0, v0

    .line 608703
    invoke-static {v0}, LX/8eM;->h(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)LX/0Px;

    move-result-object v1

    .line 608704
    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;->fl_()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;->fl_()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->be()Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel$TopHeadlineObjectModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;->fl_()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->be()Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel$TopHeadlineObjectModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel$TopHeadlineObjectModel;->b()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-nez v0, :cond_1

    .line 608705
    :cond_0
    const/4 v0, 0x0

    .line 608706
    :goto_0
    return-object v0

    .line 608707
    :cond_1
    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;->fl_()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->be()Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel$TopHeadlineObjectModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel$TopHeadlineObjectModel;->b()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 608708
    iget-object v0, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 608709
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 608710
    invoke-virtual {v1, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-virtual {p0, v0, v2}, LX/CzL;->a(Ljava/lang/Object;I)LX/CzL;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/feedplugins/video/RichVideoAttachmentView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 608711
    sget-object v0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextHeaderVideoPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 608712
    check-cast p2, LX/CzL;

    .line 608713
    invoke-static {p2}, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextHeaderVideoPartDefinition;->b(LX/CzL;)LX/CzL;

    move-result-object v1

    .line 608714
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextHeaderVideoPartDefinition;->b:Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerPartDefinition;

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 608715
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextHeaderVideoPartDefinition;->d:LX/19B;

    invoke-virtual {v0}, LX/19B;->a()Landroid/graphics/Point;

    move-result-object v2

    .line 608716
    iget-object v0, v1, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 608717
    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 608718
    iget-object v3, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v3

    .line 608719
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->bB()I

    move-result v0

    int-to-float v3, v0

    .line 608720
    iget-object v0, v1, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 608721
    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 608722
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 608723
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->S()I

    move-result v0

    int-to-float v0, v0

    div-float v1, v3, v0

    .line 608724
    iget v0, v2, Landroid/graphics/Point;->y:I

    iget v3, v2, Landroid/graphics/Point;->x:I

    if-ge v0, v3, :cond_0

    iget v0, v2, Landroid/graphics/Point;->y:I

    .line 608725
    :goto_0
    int-to-float v2, v0

    div-float v1, v2, v1

    float-to-int v1, v1

    .line 608726
    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextHeaderVideoPartDefinition;->c:Lcom/facebook/multirow/parts/ViewDimensionsPartDefinition;

    new-instance v3, LX/24k;

    invoke-direct {v3, v0, v1}, LX/24k;-><init>(II)V

    invoke-interface {p1, v2, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 608727
    const/4 v0, 0x0

    return-object v0

    .line 608728
    :cond_0
    iget v0, v2, Landroid/graphics/Point;->x:I

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 608729
    check-cast p1, LX/CzL;

    .line 608730
    invoke-static {p1}, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextHeaderVideoPartDefinition;->b(LX/CzL;)LX/CzL;

    move-result-object v0

    .line 608731
    if-eqz v0, :cond_0

    .line 608732
    const/4 v0, 0x1

    move v0, v0

    .line 608733
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
