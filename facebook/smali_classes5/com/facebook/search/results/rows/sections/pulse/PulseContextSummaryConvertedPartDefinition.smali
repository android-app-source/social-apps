.class public Lcom/facebook/search/results/rows/sections/pulse/PulseContextSummaryConvertedPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/A3T;",
        "Ljava/lang/CharSequence;",
        "LX/1Ps;",
        "Lcom/facebook/widget/text/BetterTextView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/text/BetterTextView;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:LX/1Ua;

.field private static d:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 610761
    new-instance v0, LX/3bO;

    invoke-direct {v0}, LX/3bO;-><init>()V

    sput-object v0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextSummaryConvertedPartDefinition;->a:LX/1Cz;

    .line 610762
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v0

    const/high16 v1, -0x3f400000    # -6.0f

    .line 610763
    iput v1, v0, LX/1UY;->c:F

    .line 610764
    move-object v0, v0

    .line 610765
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextSummaryConvertedPartDefinition;->b:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 610757
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 610758
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextSummaryConvertedPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 610759
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/pulse/PulseContextSummaryConvertedPartDefinition;
    .locals 4

    .prologue
    .line 610746
    const-class v1, Lcom/facebook/search/results/rows/sections/pulse/PulseContextSummaryConvertedPartDefinition;

    monitor-enter v1

    .line 610747
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextSummaryConvertedPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 610748
    sput-object v2, Lcom/facebook/search/results/rows/sections/pulse/PulseContextSummaryConvertedPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 610749
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 610750
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 610751
    new-instance p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextSummaryConvertedPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/search/results/rows/sections/pulse/PulseContextSummaryConvertedPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V

    .line 610752
    move-object v0, p0

    .line 610753
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 610754
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextSummaryConvertedPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 610755
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 610756
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/text/BetterTextView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 610760
    sget-object v0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextSummaryConvertedPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 610738
    check-cast p2, LX/A3T;

    .line 610739
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextSummaryConvertedPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    sget-object v2, Lcom/facebook/search/results/rows/sections/pulse/PulseContextSummaryConvertedPartDefinition;->b:LX/1Ua;

    invoke-direct {v1, v2}, LX/1X6;-><init>(LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 610740
    invoke-interface {p2}, LX/A3T;->cr()LX/A4H;

    move-result-object v0

    invoke-interface {v0}, LX/A4H;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x3c46b17

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 610741
    check-cast p2, Ljava/lang/CharSequence;

    check-cast p4, Lcom/facebook/widget/text/BetterTextView;

    .line 610742
    invoke-virtual {p4, p2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 610743
    const/16 v1, 0x1f

    const v2, -0x604ead39

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 610744
    check-cast p1, LX/A3T;

    .line 610745
    invoke-interface {p1}, LX/A3T;->cr()LX/A4H;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
