.class public Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextTitlePartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/CxV;",
        ":",
        "LX/CxP;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/CzL",
        "<",
        "LX/8dK;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/widget/CustomLinearLayout;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/CustomLinearLayout;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:LX/1Ua;


# instance fields
.field private final c:Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextAttributionPartDefinition;

.field private final d:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final e:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final f:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field public final g:Lcom/facebook/content/SecureContextHelper;

.field public final h:LX/CvY;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 608863
    const v0, 0x7f030671

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextTitlePartDefinition;->a:LX/1Cz;

    .line 608864
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v0

    const/high16 v1, 0x40000000    # 2.0f

    .line 608865
    iput v1, v0, LX/1UY;->b:F

    .line 608866
    move-object v0, v0

    .line 608867
    const/high16 v1, -0x3fc00000    # -3.0f

    .line 608868
    iput v1, v0, LX/1UY;->c:F

    .line 608869
    move-object v0, v0

    .line 608870
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextTitlePartDefinition;->b:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextAttributionPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/content/SecureContextHelper;LX/CvY;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 608871
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 608872
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextTitlePartDefinition;->e:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 608873
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextTitlePartDefinition;->d:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 608874
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextTitlePartDefinition;->c:Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextAttributionPartDefinition;

    .line 608875
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextTitlePartDefinition;->f:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 608876
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextTitlePartDefinition;->g:Lcom/facebook/content/SecureContextHelper;

    .line 608877
    iput-object p6, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextTitlePartDefinition;->h:LX/CvY;

    .line 608878
    return-void
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/CustomLinearLayout;",
            ">;"
        }
    .end annotation

    .prologue
    .line 608879
    sget-object v0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextTitlePartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 608880
    check-cast p2, LX/CzL;

    check-cast p3, LX/1Ps;

    const/4 v8, 0x0

    .line 608881
    iget-object v0, p2, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v0, v0

    .line 608882
    invoke-static {v0}, LX/8eM;->h(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;->e()Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel$EdgesModel$MetadataModel;

    move-result-object v0

    .line 608883
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel$EdgesModel$MetadataModel;->a()Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel$EdgesModel$MetadataModel;->a()Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;

    move-result-object v0

    .line 608884
    :goto_0
    sget-object v1, LX/1X9;->MIDDLE:LX/1X9;

    .line 608885
    const v2, 0x7f0d11c2

    iget-object v3, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextTitlePartDefinition;->d:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;->e()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p1, v2, v3, v4}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 608886
    const v2, 0x7f0d11c3

    iget-object v3, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextTitlePartDefinition;->d:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;->d()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p1, v2, v3, v4}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 608887
    const v2, 0x7f0d11c4

    iget-object v3, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextTitlePartDefinition;->c:Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextAttributionPartDefinition;

    new-instance v4, LX/ENS;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;->fp_()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;->c()J

    move-result-wide v6

    invoke-direct {v4, v5, v6, v7}, LX/ENS;-><init>(Ljava/lang/String;J)V

    invoke-interface {p1, v2, v3, v4}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 608888
    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextTitlePartDefinition;->e:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v3, LX/1X6;

    sget-object v4, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextTitlePartDefinition;->b:LX/1Ua;

    invoke-direct {v3, v8, v4, v1}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    invoke-interface {p1, v2, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 608889
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;->a()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 608890
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 608891
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextTitlePartDefinition;->f:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    new-instance v2, LX/ENj;

    invoke-direct {v2, p0, v0, p3, p2}, LX/ENj;-><init>(Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextTitlePartDefinition;Landroid/net/Uri;LX/1Ps;LX/CzL;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 608892
    :cond_0
    return-object v8

    .line 608893
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel$EdgesModel$MetadataModel;->b()Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 608894
    check-cast p1, LX/CzL;

    const/4 v1, 0x0

    .line 608895
    iget-object v0, p1, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v0, v0

    .line 608896
    invoke-static {v0}, LX/8eM;->h(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)LX/0Px;

    move-result-object v2

    .line 608897
    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8dD;

    invoke-interface {v0}, LX/8dD;->e()Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel$EdgesModel$MetadataModel;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8dD;

    invoke-interface {v0}, LX/8dD;->e()Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel$EdgesModel$MetadataModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel$EdgesModel$MetadataModel;->a()Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8dD;

    invoke-interface {v0}, LX/8dD;->e()Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel$EdgesModel$MetadataModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel$EdgesModel$MetadataModel;->b()Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method
