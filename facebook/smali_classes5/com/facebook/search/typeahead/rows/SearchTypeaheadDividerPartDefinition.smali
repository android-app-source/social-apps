.class public Lcom/facebook/search/typeahead/rows/SearchTypeaheadDividerPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/search/model/DividerTypeaheadUnit;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static b:LX/0Xm;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 611488
    new-instance v0, LX/3bc;

    invoke-direct {v0}, LX/3bc;-><init>()V

    sput-object v0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadDividerPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 611489
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 611490
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/typeahead/rows/SearchTypeaheadDividerPartDefinition;
    .locals 3

    .prologue
    .line 611491
    const-class v1, Lcom/facebook/search/typeahead/rows/SearchTypeaheadDividerPartDefinition;

    monitor-enter v1

    .line 611492
    :try_start_0
    sget-object v0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadDividerPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 611493
    sput-object v2, Lcom/facebook/search/typeahead/rows/SearchTypeaheadDividerPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 611494
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 611495
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 611496
    new-instance v0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadDividerPartDefinition;

    invoke-direct {v0}, Lcom/facebook/search/typeahead/rows/SearchTypeaheadDividerPartDefinition;-><init>()V

    .line 611497
    move-object v0, v0

    .line 611498
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 611499
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadDividerPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 611500
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 611501
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 611502
    sget-object v0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadDividerPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 611503
    const/4 v0, 0x1

    return v0
.end method
