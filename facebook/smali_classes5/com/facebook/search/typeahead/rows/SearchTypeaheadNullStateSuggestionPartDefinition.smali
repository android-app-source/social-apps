.class public Lcom/facebook/search/typeahead/rows/SearchTypeaheadNullStateSuggestionPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;",
        "LX/Fiv;",
        "LX/FhE;",
        "Lcom/facebook/fbui/widget/contentview/ContentView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentView;",
            ">;"
        }
    .end annotation
.end field

.field private static e:LX/0Xm;


# instance fields
.field private final b:Landroid/content/res/Resources;

.field private final c:LX/0Uh;

.field private final d:Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 611587
    const v0, 0x7f0311a4

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadNullStateSuggestionPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;LX/0Uh;Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 611588
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 611589
    iput-object p1, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadNullStateSuggestionPartDefinition;->b:Landroid/content/res/Resources;

    .line 611590
    iput-object p2, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadNullStateSuggestionPartDefinition;->c:LX/0Uh;

    .line 611591
    iput-object p3, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadNullStateSuggestionPartDefinition;->d:Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;

    .line 611592
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/typeahead/rows/SearchTypeaheadNullStateSuggestionPartDefinition;
    .locals 6

    .prologue
    .line 611593
    const-class v1, Lcom/facebook/search/typeahead/rows/SearchTypeaheadNullStateSuggestionPartDefinition;

    monitor-enter v1

    .line 611594
    :try_start_0
    sget-object v0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadNullStateSuggestionPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 611595
    sput-object v2, Lcom/facebook/search/typeahead/rows/SearchTypeaheadNullStateSuggestionPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 611596
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 611597
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 611598
    new-instance p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadNullStateSuggestionPartDefinition;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-static {v0}, Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;->a(LX/0QB;)Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;

    move-result-object v5

    check-cast v5, Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/search/typeahead/rows/SearchTypeaheadNullStateSuggestionPartDefinition;-><init>(Landroid/content/res/Resources;LX/0Uh;Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;)V

    .line 611599
    move-object v0, p0

    .line 611600
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 611601
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadNullStateSuggestionPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 611602
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 611603
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 611604
    sget-object v0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadNullStateSuggestionPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 611605
    check-cast p2, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;

    check-cast p3, LX/FhE;

    .line 611606
    iget-object v0, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadNullStateSuggestionPartDefinition;->d:Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;

    invoke-virtual {p3}, LX/1Qj;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 611607
    iget-object v2, p2, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->h:LX/CwF;

    move-object v2, v2

    .line 611608
    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;->a(Landroid/content/Context;LX/CwF;Z)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 611609
    new-instance v1, LX/Fiv;

    iget-object v2, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadNullStateSuggestionPartDefinition;->b:Landroid/content/res/Resources;

    const v3, 0x7f0f0113

    .line 611610
    iget v4, p2, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->i:I

    move v4, v4

    .line 611611
    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v2

    .line 611612
    iget v3, p2, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->i:I

    move v3, v3

    .line 611613
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, LX/Fiv;-><init>(Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V

    return-object v1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 5

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x5106c3ef

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 611614
    check-cast p1, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;

    check-cast p2, LX/Fiv;

    check-cast p4, Lcom/facebook/fbui/widget/contentview/ContentView;

    const/4 p3, 0x0

    const/4 p0, 0x1

    const/4 v4, 0x0

    .line 611615
    sget-object v1, LX/6VF;->SMALL:LX/6VF;

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setThumbnailSize(LX/6VF;)V

    .line 611616
    iget-object v1, p1, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->k:LX/0Px;

    move-object v1, v1

    .line 611617
    invoke-static {v1}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 611618
    iget-object v1, p1, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->k:LX/0Px;

    move-object v1, v1

    .line 611619
    invoke-virtual {v1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED_VIDEOS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    if-ne v1, v2, :cond_0

    .line 611620
    invoke-virtual {p4, v4}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setShowThumbnail(Z)V

    .line 611621
    :goto_0
    iget-object v1, p1, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->b:Ljava/lang/String;

    move-object v1, v1

    .line 611622
    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 611623
    const v1, 0x7f0e0872

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleTextAppearance(I)V

    .line 611624
    invoke-virtual {p4}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->getLocaleGravity()I

    move-result v1

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleGravity(I)V

    .line 611625
    iget v1, p1, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->i:I

    move v1, v1

    .line 611626
    if-lez v1, :cond_3

    .line 611627
    invoke-virtual {p4}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->getLocaleGravity()I

    move-result v1

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleGravity(I)V

    .line 611628
    const v1, 0x7f0e087d

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleTextAppearance(I)V

    .line 611629
    iget-object v1, p2, LX/Fiv;->a:Ljava/lang/String;

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 611630
    invoke-virtual {p4, v4}, Lcom/facebook/fbui/widget/contentview/ContentView;->setMaxLinesFromThumbnailSize(Z)V

    .line 611631
    invoke-virtual {p4, p0, p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->e(II)V

    .line 611632
    :goto_1
    const/16 v1, 0x10

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailGravity(I)V

    .line 611633
    const/16 v1, 0x1f

    const v2, -0x2d507cb9

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 611634
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->z()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 611635
    invoke-virtual {p4, p3}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailView(Landroid/view/View;)V

    .line 611636
    iget-object v1, p2, LX/Fiv;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 611637
    :cond_1
    sget-object v1, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    .line 611638
    iget-object v2, p1, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->e:Landroid/net/Uri;

    move-object v2, v2

    .line 611639
    invoke-virtual {v1, v2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 611640
    iget-object v1, p1, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->e:Landroid/net/Uri;

    move-object v1, v1

    .line 611641
    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Landroid/net/Uri;)V

    goto :goto_0

    .line 611642
    :cond_2
    invoke-virtual {p4, p3}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Ljava/lang/String;)V

    goto :goto_0

    .line 611643
    :cond_3
    invoke-virtual {p4}, Lcom/facebook/fbui/widget/contentview/ContentView;->d()V

    .line 611644
    invoke-virtual {p4, p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setMaxLinesFromThumbnailSize(Z)V

    .line 611645
    invoke-virtual {p4, p3}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 611646
    const/4 v0, 0x1

    return v0
.end method
