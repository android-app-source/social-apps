.class public Lcom/facebook/search/typeahead/rows/SearchTypeaheadPlaceTipsPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/search/model/PlaceTipsTypeaheadUnit;",
        "Ljava/lang/String;",
        "LX/1PW;",
        "Lcom/facebook/fbui/widget/layout/ImageBlockLayout;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static c:LX/0Xm;


# instance fields
.field public final b:LX/FiQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 611681
    new-instance v0, LX/3bg;

    invoke-direct {v0}, LX/3bg;-><init>()V

    sput-object v0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadPlaceTipsPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/FiQ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 611678
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 611679
    iput-object p1, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadPlaceTipsPartDefinition;->b:LX/FiQ;

    .line 611680
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/typeahead/rows/SearchTypeaheadPlaceTipsPartDefinition;
    .locals 6

    .prologue
    .line 611647
    const-class v1, Lcom/facebook/search/typeahead/rows/SearchTypeaheadPlaceTipsPartDefinition;

    monitor-enter v1

    .line 611648
    :try_start_0
    sget-object v0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadPlaceTipsPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 611649
    sput-object v2, Lcom/facebook/search/typeahead/rows/SearchTypeaheadPlaceTipsPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 611650
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 611651
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 611652
    new-instance v4, Lcom/facebook/search/typeahead/rows/SearchTypeaheadPlaceTipsPartDefinition;

    .line 611653
    new-instance p0, LX/FiQ;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {v0}, LX/2cw;->a(LX/0QB;)LX/2cw;

    move-result-object v5

    check-cast v5, LX/2cw;

    invoke-direct {p0, v3, v5}, LX/FiQ;-><init>(Landroid/content/res/Resources;LX/2cw;)V

    .line 611654
    move-object v3, p0

    .line 611655
    check-cast v3, LX/FiQ;

    invoke-direct {v4, v3}, Lcom/facebook/search/typeahead/rows/SearchTypeaheadPlaceTipsPartDefinition;-><init>(LX/FiQ;)V

    .line 611656
    move-object v0, v4

    .line 611657
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 611658
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadPlaceTipsPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 611659
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 611660
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 611677
    sget-object v0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadPlaceTipsPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 611675
    const-string v0, "%s"

    move-object v0, v0

    .line 611676
    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 8

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x563db7cb

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 611662
    check-cast p1, Lcom/facebook/search/model/PlaceTipsTypeaheadUnit;

    check-cast p2, Ljava/lang/String;

    check-cast p4, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    .line 611663
    iget-object v1, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadPlaceTipsPartDefinition;->b:LX/FiQ;

    const/4 p3, 0x1

    const/4 p0, 0x0

    .line 611664
    const v2, 0x7f0d2578

    invoke-static {p4, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 611665
    const v4, 0x7f0d2579

    invoke-static {p4, v4}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 611666
    iget-object v5, p1, Lcom/facebook/search/model/PlaceTipsTypeaheadUnit;->a:Lcom/facebook/placetips/bootstrap/PresenceDescription;

    move-object v5, v5

    .line 611667
    new-array v6, p3, [Ljava/lang/Object;

    invoke-virtual {v5}, Lcom/facebook/placetips/bootstrap/PresenceDescription;->h()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, p0

    invoke-static {p2, v6}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 611668
    const-string v2, ""

    .line 611669
    invoke-virtual {v5}, Lcom/facebook/placetips/bootstrap/PresenceDescription;->j()LX/0Px;

    move-result-object v6

    .line 611670
    if-eqz v6, :cond_0

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    if-lez v7, :cond_0

    .line 611671
    iget-object v2, v1, LX/FiQ;->a:Landroid/content/res/Resources;

    const v7, 0x7f0822e6

    new-array p3, p3, [Ljava/lang/Object;

    invoke-virtual {v6, p0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    aput-object v6, p3, p0

    invoke-virtual {v2, v7, p3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 611672
    :cond_0
    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 611673
    iget-object v2, v1, LX/FiQ;->b:LX/2cw;

    sget-object v4, LX/3FC;->SEARCH_NULL_STATE_VPV:LX/3FC;

    invoke-virtual {v5}, Lcom/facebook/placetips/bootstrap/PresenceDescription;->l()LX/2cx;

    move-result-object v6

    invoke-virtual {v5}, Lcom/facebook/placetips/bootstrap/PresenceDescription;->i()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5}, Lcom/facebook/placetips/bootstrap/PresenceDescription;->d()Z

    move-result v5

    invoke-virtual {v2, v4, v6, v7, v5}, LX/2cw;->a(LX/3FC;LX/2cx;Ljava/lang/String;Z)V

    .line 611674
    const/16 v1, 0x1f

    const v2, -0x5714dd91

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 611661
    const/4 v0, 0x1

    return v0
.end method
