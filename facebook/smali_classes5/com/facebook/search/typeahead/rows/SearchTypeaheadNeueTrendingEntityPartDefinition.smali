.class public Lcom/facebook/search/typeahead/rows/SearchTypeaheadNeueTrendingEntityPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/search/model/TrendingTypeaheadUnit;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Lcom/facebook/fbui/widget/contentview/ContentView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static d:LX/0Xm;


# instance fields
.field public final b:Landroid/content/Context;

.field public final c:LX/0wM;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 611487
    const v0, 0x7f0307f1

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadNeueTrendingEntityPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0wM;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 611483
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 611484
    iput-object p1, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadNeueTrendingEntityPartDefinition;->b:Landroid/content/Context;

    .line 611485
    iput-object p2, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadNeueTrendingEntityPartDefinition;->c:LX/0wM;

    .line 611486
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/typeahead/rows/SearchTypeaheadNeueTrendingEntityPartDefinition;
    .locals 5

    .prologue
    .line 611451
    const-class v1, Lcom/facebook/search/typeahead/rows/SearchTypeaheadNeueTrendingEntityPartDefinition;

    monitor-enter v1

    .line 611452
    :try_start_0
    sget-object v0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadNeueTrendingEntityPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 611453
    sput-object v2, Lcom/facebook/search/typeahead/rows/SearchTypeaheadNeueTrendingEntityPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 611454
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 611455
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 611456
    new-instance p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadNeueTrendingEntityPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v4

    check-cast v4, LX/0wM;

    invoke-direct {p0, v3, v4}, Lcom/facebook/search/typeahead/rows/SearchTypeaheadNeueTrendingEntityPartDefinition;-><init>(Landroid/content/Context;LX/0wM;)V

    .line 611457
    move-object v0, p0

    .line 611458
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 611459
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadNeueTrendingEntityPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 611460
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 611461
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 611482
    sget-object v0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadNeueTrendingEntityPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 5

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x78dc3a00

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 611463
    check-cast p1, Lcom/facebook/search/model/TrendingTypeaheadUnit;

    check-cast p4, Lcom/facebook/fbui/widget/contentview/ContentView;

    const/4 v2, 0x1

    .line 611464
    const/4 v1, 0x0

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setMaxLinesFromThumbnailSize(Z)V

    .line 611465
    invoke-virtual {p4, v2, v2}, Lcom/facebook/fbui/widget/contentview/ContentView;->e(II)V

    .line 611466
    const v1, 0x7f0e086d

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleTextAppearance(I)V

    .line 611467
    iget-object v1, p1, Lcom/facebook/search/model/TrendingTypeaheadUnit;->b:Ljava/lang/String;

    move-object v1, v1

    .line 611468
    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 611469
    sget-object v1, LX/6VF;->SMALL:LX/6VF;

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setThumbnailSize(LX/6VF;)V

    .line 611470
    iget-object v1, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadNeueTrendingEntityPartDefinition;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b17b1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailPadding(I)V

    .line 611471
    const/16 v1, 0x11

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailGravity(I)V

    .line 611472
    invoke-virtual {p1}, Lcom/facebook/search/model/TrendingTypeaheadUnit;->u()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 611473
    const v1, 0x7f020c22

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailResource(I)V

    .line 611474
    :goto_0
    const v1, 0x7f0e0877

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleTextAppearance(I)V

    .line 611475
    iget-object v1, p1, Lcom/facebook/search/model/TrendingTypeaheadUnit;->g:Ljava/lang/String;

    move-object v1, v1

    .line 611476
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 611477
    iget-object v1, p1, Lcom/facebook/search/model/TrendingTypeaheadUnit;->g:Ljava/lang/String;

    move-object v1, v1

    .line 611478
    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 611479
    :cond_0
    invoke-virtual {p4}, Lcom/facebook/fbui/widget/contentview/ContentView;->getPaddingLeft()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadNeueTrendingEntityPartDefinition;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0b17b2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {p4}, Lcom/facebook/fbui/widget/contentview/ContentView;->getPaddingRight()I

    move-result v4

    iget-object p2, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadNeueTrendingEntityPartDefinition;->b:Landroid/content/Context;

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const p3, 0x7f0b17b2

    invoke-virtual {p2, p3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p2

    invoke-virtual {p4, v1, v2, v4, p2}, Lcom/facebook/fbui/widget/contentview/ContentView;->setPadding(IIII)V

    .line 611480
    const/16 v1, 0x1f

    const v2, 0x54113f2c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 611481
    :cond_1
    iget-object v1, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadNeueTrendingEntityPartDefinition;->c:LX/0wM;

    const v2, 0x7f021101

    iget-object v4, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadNeueTrendingEntityPartDefinition;->b:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const p2, 0x7f0a06fd

    invoke-virtual {v4, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v1, v2, v4}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 611462
    const/4 v0, 0x1

    return v0
.end method
