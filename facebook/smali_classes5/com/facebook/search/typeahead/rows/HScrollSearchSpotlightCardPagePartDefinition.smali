.class public Lcom/facebook/search/typeahead/rows/HScrollSearchSpotlightCardPagePartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/search/model/SearchSpotlightCardUnit;",
        "Landroid/graphics/drawable/Drawable;",
        "LX/1Pn;",
        "Lcom/facebook/search/typeahead/ui/HorizontalSearchSpotlightCardView;",
        ">;"
    }
.end annotation


# static fields
.field public static a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/search/typeahead/ui/HorizontalSearchSpotlightCardView;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private final b:LX/0Uh;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 611877
    new-instance v0, LX/3bh;

    invoke-direct {v0}, LX/3bh;-><init>()V

    sput-object v0, Lcom/facebook/search/typeahead/rows/HScrollSearchSpotlightCardPagePartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/0Uh;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 611878
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 611879
    iput-object p1, p0, Lcom/facebook/search/typeahead/rows/HScrollSearchSpotlightCardPagePartDefinition;->b:LX/0Uh;

    .line 611880
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/typeahead/rows/HScrollSearchSpotlightCardPagePartDefinition;
    .locals 4

    .prologue
    .line 611881
    const-class v1, Lcom/facebook/search/typeahead/rows/HScrollSearchSpotlightCardPagePartDefinition;

    monitor-enter v1

    .line 611882
    :try_start_0
    sget-object v0, Lcom/facebook/search/typeahead/rows/HScrollSearchSpotlightCardPagePartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 611883
    sput-object v2, Lcom/facebook/search/typeahead/rows/HScrollSearchSpotlightCardPagePartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 611884
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 611885
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 611886
    new-instance p0, Lcom/facebook/search/typeahead/rows/HScrollSearchSpotlightCardPagePartDefinition;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-direct {p0, v3}, Lcom/facebook/search/typeahead/rows/HScrollSearchSpotlightCardPagePartDefinition;-><init>(LX/0Uh;)V

    .line 611887
    move-object v0, p0

    .line 611888
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 611889
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/typeahead/rows/HScrollSearchSpotlightCardPagePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 611890
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 611891
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/search/typeahead/ui/HorizontalSearchSpotlightCardView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 611892
    sget-object v0, Lcom/facebook/search/typeahead/rows/HScrollSearchSpotlightCardPagePartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 611893
    check-cast p2, Lcom/facebook/search/model/SearchSpotlightCardUnit;

    .line 611894
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    .line 611895
    iget v1, p2, Lcom/facebook/search/model/SearchSpotlightCardUnit;->b:I

    move v1, v1

    .line 611896
    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x5e6b6b23

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 611897
    check-cast p1, Lcom/facebook/search/model/SearchSpotlightCardUnit;

    check-cast p2, Landroid/graphics/drawable/Drawable;

    check-cast p4, Lcom/facebook/search/typeahead/ui/HorizontalSearchSpotlightCardView;

    .line 611898
    invoke-virtual {p4, p2}, Lcom/facebook/search/typeahead/ui/HorizontalSearchSpotlightCardView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 611899
    iget-object v1, p1, Lcom/facebook/search/model/SearchSpotlightCardUnit;->a:Ljava/lang/String;

    move-object v1, v1

    .line 611900
    invoke-virtual {p4, v1}, Lcom/facebook/search/typeahead/ui/HorizontalSearchSpotlightCardView;->setText(Ljava/lang/CharSequence;)V

    .line 611901
    const/16 v1, 0x1f

    const v2, 0xb73774f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 611902
    iget-object v0, p0, Lcom/facebook/search/typeahead/rows/HScrollSearchSpotlightCardPagePartDefinition;->b:LX/0Uh;

    sget v1, LX/2SU;->E:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method
