.class public Lcom/facebook/search/typeahead/rows/UnsupportedSearchTypeaheadPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/search/model/TypeaheadUnit;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Lcom/facebook/widget/text/BetterTextView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static b:LX/0Xm;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 611313
    new-instance v0, LX/3bb;

    invoke-direct {v0}, LX/3bb;-><init>()V

    sput-object v0, Lcom/facebook/search/typeahead/rows/UnsupportedSearchTypeaheadPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 611294
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 611295
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/typeahead/rows/UnsupportedSearchTypeaheadPartDefinition;
    .locals 3

    .prologue
    .line 611302
    const-class v1, Lcom/facebook/search/typeahead/rows/UnsupportedSearchTypeaheadPartDefinition;

    monitor-enter v1

    .line 611303
    :try_start_0
    sget-object v0, Lcom/facebook/search/typeahead/rows/UnsupportedSearchTypeaheadPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 611304
    sput-object v2, Lcom/facebook/search/typeahead/rows/UnsupportedSearchTypeaheadPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 611305
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 611306
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 611307
    new-instance v0, Lcom/facebook/search/typeahead/rows/UnsupportedSearchTypeaheadPartDefinition;

    invoke-direct {v0}, Lcom/facebook/search/typeahead/rows/UnsupportedSearchTypeaheadPartDefinition;-><init>()V

    .line 611308
    move-object v0, v0

    .line 611309
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 611310
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/typeahead/rows/UnsupportedSearchTypeaheadPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 611311
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 611312
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 611301
    sget-object v0, Lcom/facebook/search/typeahead/rows/UnsupportedSearchTypeaheadPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x2d8575e9

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 611297
    check-cast p1, Lcom/facebook/search/model/TypeaheadUnit;

    check-cast p4, Lcom/facebook/widget/text/BetterTextView;

    .line 611298
    const/16 v1, 0x11

    invoke-virtual {p4, v1}, Lcom/facebook/widget/text/BetterTextView;->setGravity(I)V

    .line 611299
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p4, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 611300
    const/16 v1, 0x1f

    const v2, -0x2e629f4e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 611296
    const/4 v0, 0x1

    return v0
.end method
