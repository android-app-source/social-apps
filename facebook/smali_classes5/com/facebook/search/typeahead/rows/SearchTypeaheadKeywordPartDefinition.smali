.class public Lcom/facebook/search/typeahead/rows/SearchTypeaheadKeywordPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/search/model/KeywordTypeaheadUnit;",
        "Ljava/lang/Object;",
        "LX/FhE;",
        "Lcom/facebook/fbui/widget/contentview/ContentView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentView;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 611729
    const v0, 0x7f0307f1

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadKeywordPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 611730
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 611731
    iput-object p1, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadKeywordPartDefinition;->b:Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;

    .line 611732
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/typeahead/rows/SearchTypeaheadKeywordPartDefinition;
    .locals 4

    .prologue
    .line 611733
    const-class v1, Lcom/facebook/search/typeahead/rows/SearchTypeaheadKeywordPartDefinition;

    monitor-enter v1

    .line 611734
    :try_start_0
    sget-object v0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadKeywordPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 611735
    sput-object v2, Lcom/facebook/search/typeahead/rows/SearchTypeaheadKeywordPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 611736
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 611737
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 611738
    new-instance p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadKeywordPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;->a(LX/0QB;)Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;

    invoke-direct {p0, v3}, Lcom/facebook/search/typeahead/rows/SearchTypeaheadKeywordPartDefinition;-><init>(Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;)V

    .line 611739
    move-object v0, p0

    .line 611740
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 611741
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadKeywordPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 611742
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 611743
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Lcom/facebook/search/model/KeywordTypeaheadUnit;Lcom/facebook/fbui/widget/contentview/ContentView;)V
    .locals 13

    .prologue
    .line 611744
    iget-object v0, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadKeywordPartDefinition;->b:Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 611745
    invoke-virtual {p2}, Lcom/facebook/fbui/widget/contentview/ContentView;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 611746
    invoke-virtual {p1}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p1}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->jC_()LX/0Px;

    move-result-object v2

    invoke-static {v2}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p1}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->jC_()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED_VIDEOS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    if-ne v2, v3, :cond_2

    .line 611747
    :cond_0
    invoke-virtual {p2, v5}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setShowThumbnail(Z)V

    .line 611748
    :goto_0
    const/4 v3, 0x0

    .line 611749
    const/4 v8, 0x1

    const/4 v10, 0x0

    .line 611750
    invoke-virtual {p1}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->jB_()LX/CwF;

    move-result-object v2

    sget-object v4, LX/CwF;->escape:LX/CwF;

    invoke-virtual {v2, v4}, LX/CwF;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 611751
    new-instance v2, Landroid/text/SpannableString;

    iget-object v4, v0, Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;->e:Landroid/content/res/Resources;

    const v7, 0x7f0822ee

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v0}, Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;->a(Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-virtual {v4, v7, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v4

    invoke-direct {v2, v4}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 611752
    :goto_1
    move-object v2, v2

    .line 611753
    iget-boolean v4, p1, Lcom/facebook/search/model/KeywordTypeaheadUnit;->C:Z

    move v4, v4

    .line 611754
    if-eqz v4, :cond_1

    iget-object v4, v0, Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;->j:LX/0Uh;

    sget v7, LX/2SU;->v:I

    invoke-virtual {v4, v7, v3}, LX/0Uh;->a(IZ)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v3, 0x1

    .line 611755
    :cond_1
    if-nez v3, :cond_5

    .line 611756
    :goto_2
    move-object v2, v2

    .line 611757
    invoke-virtual {p2, v2}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 611758
    invoke-virtual {p1}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->jB_()LX/CwF;

    move-result-object v2

    sget-object v3, LX/CwF;->escape:LX/CwF;

    invoke-virtual {v2, v3}, LX/CwF;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 611759
    invoke-virtual {p1}, Lcom/facebook/search/model/TypeaheadUnit;->D()Z

    move-result v2

    if-eqz v2, :cond_9

    const v2, 0x7f0e0875

    .line 611760
    :goto_3
    move v2, v2

    .line 611761
    invoke-virtual {p2, v2}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleTextAppearance(I)V

    .line 611762
    invoke-virtual {p2}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->getLocaleGravity()I

    move-result v2

    invoke-virtual {p2, v2}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleGravity(I)V

    .line 611763
    const p0, 0xffffff

    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 611764
    iget-object v2, p1, Lcom/facebook/search/model/KeywordTypeaheadUnit;->t:Ljava/lang/String;

    move-object v4, v2

    .line 611765
    iget-object v2, p1, Lcom/facebook/search/model/KeywordTypeaheadUnit;->s:Ljava/lang/String;

    move-object v3, v2

    .line 611766
    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_d

    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 611767
    const/4 v2, 0x0

    .line 611768
    :goto_4
    move-object v2, v2

    .line 611769
    if-eqz v2, :cond_4

    .line 611770
    invoke-virtual {p1}, Lcom/facebook/search/model/TypeaheadUnit;->D()Z

    move-result v1

    if-eqz v1, :cond_3

    const v1, 0x7f0e0878

    :goto_5
    invoke-virtual {p2, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleTextAppearance(I)V

    .line 611771
    invoke-virtual {p2}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->getLocaleGravity()I

    move-result v1

    invoke-virtual {p2, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleGravity(I)V

    .line 611772
    invoke-virtual {p2, v2}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 611773
    invoke-virtual {p2, v5}, Lcom/facebook/fbui/widget/contentview/ContentView;->setMaxLinesFromThumbnailSize(Z)V

    .line 611774
    invoke-virtual {p2, v6, v6}, Lcom/facebook/fbui/widget/contentview/ContentView;->e(II)V

    .line 611775
    :goto_6
    return-void

    .line 611776
    :cond_2
    invoke-virtual {p2}, Lcom/facebook/fbui/widget/contentview/ContentView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->jB_()LX/CwF;

    move-result-object v3

    invoke-virtual {p1}, Lcom/facebook/search/model/TypeaheadUnit;->D()Z

    move-result v4

    invoke-virtual {v0, v2, v3, v4}, Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;->a(Landroid/content/Context;LX/CwF;Z)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-static {v0, p2, v2}, Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;->a(Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;Lcom/facebook/fbui/widget/contentview/ContentView;Landroid/graphics/drawable/Drawable;)V

    .line 611777
    invoke-virtual {p2, v6}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setShowThumbnail(Z)V

    goto/16 :goto_0

    .line 611778
    :cond_3
    const v1, 0x7f0e0876

    goto :goto_5

    .line 611779
    :cond_4
    const/4 v1, 0x0

    invoke-virtual {p2, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 611780
    invoke-virtual {p2, v5}, Lcom/facebook/fbui/widget/contentview/ContentView;->setMaxLinesFromThumbnailSize(Z)V

    .line 611781
    invoke-virtual {p2, v6, v5}, Lcom/facebook/fbui/widget/contentview/ContentView;->e(II)V

    goto :goto_6

    .line 611782
    :cond_5
    new-instance v3, Landroid/text/SpannableStringBuilder;

    invoke-direct {v3, v2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 611783
    iget-object v2, v0, Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;->i:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/8i7;

    invoke-virtual {v2, v3}, LX/8i7;->c(Landroid/text/SpannableStringBuilder;)Landroid/text/SpannableStringBuilder;

    move-object v2, v3

    .line 611784
    goto/16 :goto_2

    .line 611785
    :cond_6
    invoke-virtual {p1}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->jB_()LX/CwF;

    move-result-object v2

    sget-object v4, LX/CwF;->escape_pps_style:LX/CwF;

    invoke-virtual {v2, v4}, LX/CwF;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 611786
    new-instance v2, Landroid/text/SpannableString;

    iget-object v4, v0, Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;->e:Landroid/content/res/Resources;

    const v7, 0x7f0822ef

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v0}, Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;->a(Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-virtual {v4, v7, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v4

    invoke-direct {v2, v4}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 611787
    :cond_7
    invoke-virtual {p1}, Lcom/facebook/search/model/TypeaheadUnit;->D()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 611788
    invoke-virtual {p1}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->a()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    .line 611789
    :cond_8
    iget-object v2, v0, Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;->g:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/8iE;

    invoke-virtual {p1}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0}, Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;->a(Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;)Ljava/lang/String;

    move-result-object v7

    iget-object v8, v0, Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;->k:LX/0Px;

    iget-object v9, v0, Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;->l:LX/0Px;

    invoke-virtual {v2, v4, v7, v8, v9}, LX/8iE;->a(Ljava/lang/String;Ljava/lang/String;LX/0Px;LX/0Px;)Ljava/lang/CharSequence;

    move-result-object v2

    goto/16 :goto_1

    .line 611790
    :cond_9
    const v2, 0x7f0e0873

    goto/16 :goto_3

    .line 611791
    :cond_a
    invoke-virtual {p1}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->jB_()LX/CwF;

    move-result-object v2

    sget-object v3, LX/CwF;->escape_pps_style:LX/CwF;

    invoke-virtual {v2, v3}, LX/CwF;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 611792
    const v2, 0x7f0e0874

    goto/16 :goto_3

    .line 611793
    :cond_b
    invoke-virtual {p1}, Lcom/facebook/search/model/TypeaheadUnit;->D()Z

    move-result v2

    if-eqz v2, :cond_c

    const v2, 0x7f0e0870

    goto/16 :goto_3

    :cond_c
    const v2, 0x7f0e086d

    goto/16 :goto_3

    .line 611794
    :cond_d
    invoke-virtual {p1}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->jB_()LX/CwF;

    move-result-object v2

    .line 611795
    sget-object v7, LX/FiM;->g:Ljava/util/Map;

    invoke-interface {v7, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    .line 611796
    if-eqz v7, :cond_14

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    :goto_7
    move v7, v7

    .line 611797
    new-instance v8, Landroid/text/style/TextAppearanceSpan;

    invoke-direct {v8, v1, v7}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    move-object v2, v8

    .line 611798
    if-nez v2, :cond_e

    move-object v2, v3

    .line 611799
    goto/16 :goto_4

    .line 611800
    :cond_e
    invoke-virtual {v2}, Landroid/text/style/TextAppearanceSpan;->getTextColor()Landroid/content/res/ColorStateList;

    move-result-object v2

    .line 611801
    if-nez v2, :cond_f

    move-object v2, v3

    .line 611802
    goto/16 :goto_4

    .line 611803
    :cond_f
    invoke-virtual {p1}, Lcom/facebook/search/model/TypeaheadUnit;->D()Z

    move-result v7

    if-eqz v7, :cond_11

    const/16 v7, 0x99

    invoke-virtual {v2, v7}, Landroid/content/res/ColorStateList;->withAlpha(I)Landroid/content/res/ColorStateList;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v2

    .line 611804
    :goto_8
    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_10

    if-nez v2, :cond_12

    :cond_10
    move-object v2, v3

    .line 611805
    goto/16 :goto_4

    .line 611806
    :cond_11
    invoke-virtual {v2}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v2

    goto :goto_8

    .line 611807
    :cond_12
    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_13

    .line 611808
    iget-object v3, v0, Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;->e:Landroid/content/res/Resources;

    const v7, 0x7f082a7a

    new-array v8, v12, [Ljava/lang/Object;

    and-int/2addr v2, p0

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v8, v10

    aput-object v4, v8, v11

    invoke-virtual {v3, v7, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    goto/16 :goto_4

    .line 611809
    :cond_13
    iget-object v7, v0, Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;->e:Landroid/content/res/Resources;

    const v8, 0x7f082a79

    const/4 v9, 0x4

    new-array v9, v9, [Ljava/lang/Object;

    and-int/2addr v2, p0

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v9, v10

    aput-object v4, v9, v11

    const-string v2, "\u00b7"

    aput-object v2, v9, v12

    const/4 v2, 0x3

    aput-object v3, v9, v2

    invoke-virtual {v7, v8, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    goto/16 :goto_4

    :cond_14
    sget v7, LX/FiM;->h:I

    goto :goto_7
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 611810
    sget-object v0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadKeywordPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x44362a15

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 611811
    check-cast p1, Lcom/facebook/search/model/KeywordTypeaheadUnit;

    check-cast p4, Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-direct {p0, p1, p4}, Lcom/facebook/search/typeahead/rows/SearchTypeaheadKeywordPartDefinition;->a(Lcom/facebook/search/model/KeywordTypeaheadUnit;Lcom/facebook/fbui/widget/contentview/ContentView;)V

    const/16 v1, 0x1f

    const v2, 0x178630b6

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 611812
    const/4 v0, 0x1

    return v0
.end method
