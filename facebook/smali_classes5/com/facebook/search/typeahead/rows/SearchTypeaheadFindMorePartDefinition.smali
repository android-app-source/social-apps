.class public Lcom/facebook/search/typeahead/rows/SearchTypeaheadFindMorePartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/search/model/SeeMoreTypeaheadUnit;",
        "LX/Fiq;",
        "LX/1PW;",
        "Lcom/facebook/fbui/widget/contentview/ContentView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static e:LX/0Xm;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:LX/FgS;

.field public final d:Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 611728
    const v0, 0x7f0307f1

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadFindMorePartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/FgS;Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 611723
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 611724
    iput-object p1, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadFindMorePartDefinition;->b:Landroid/content/Context;

    .line 611725
    iput-object p2, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadFindMorePartDefinition;->c:LX/FgS;

    .line 611726
    iput-object p3, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadFindMorePartDefinition;->d:Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;

    .line 611727
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/typeahead/rows/SearchTypeaheadFindMorePartDefinition;
    .locals 6

    .prologue
    .line 611712
    const-class v1, Lcom/facebook/search/typeahead/rows/SearchTypeaheadFindMorePartDefinition;

    monitor-enter v1

    .line 611713
    :try_start_0
    sget-object v0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadFindMorePartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 611714
    sput-object v2, Lcom/facebook/search/typeahead/rows/SearchTypeaheadFindMorePartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 611715
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 611716
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 611717
    new-instance p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadFindMorePartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/FgS;->a(LX/0QB;)LX/FgS;

    move-result-object v4

    check-cast v4, LX/FgS;

    invoke-static {v0}, Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;->a(LX/0QB;)Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;

    move-result-object v5

    check-cast v5, Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/search/typeahead/rows/SearchTypeaheadFindMorePartDefinition;-><init>(Landroid/content/Context;LX/FgS;Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;)V

    .line 611718
    move-object v0, p0

    .line 611719
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 611720
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadFindMorePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 611721
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 611722
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 611686
    sget-object v0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadFindMorePartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 611695
    const/4 v7, 0x0

    .line 611696
    iget-object v0, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadFindMorePartDefinition;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0217dd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 611697
    iget-object v0, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadFindMorePartDefinition;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a00d1

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 611698
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    new-instance v3, Landroid/graphics/PorterDuffColorFilter;

    sget-object v4, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v3, v2, v4}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v3}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 611699
    new-instance v0, Landroid/text/SpannableString;

    const-string v3, ""

    invoke-direct {v0, v3}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 611700
    iget-object v3, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadFindMorePartDefinition;->c:LX/FgS;

    .line 611701
    iget-object v4, v3, LX/FgS;->a:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    move-object v3, v4

    .line 611702
    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadFindMorePartDefinition;->c:LX/FgS;

    .line 611703
    iget-object v4, v3, LX/FgS;->a:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    move-object v3, v4

    .line 611704
    iget-object v4, v3, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    move-object v3, v4

    .line 611705
    if-eqz v3, :cond_0

    .line 611706
    new-instance v0, Landroid/text/SpannableString;

    iget-object v3, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadFindMorePartDefinition;->b:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0822ef

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadFindMorePartDefinition;->c:LX/FgS;

    .line 611707
    iget-object p0, v6, LX/FgS;->a:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    move-object v6, p0

    .line 611708
    iget-object p0, v6, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    move-object v6, p0

    .line 611709
    invoke-virtual {v6}, Lcom/facebook/ui/search/SearchEditText;->getText()Landroid/text/Editable;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 611710
    new-instance v3, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v3, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-interface {v0}, Landroid/text/Spannable;->length()I

    move-result v2

    const/16 v4, 0x11

    invoke-interface {v0, v3, v7, v2, v4}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 611711
    :cond_0
    new-instance v2, LX/Fiq;

    invoke-direct {v2, v0, v1}, LX/Fiq;-><init>(Landroid/text/Spanned;Landroid/graphics/drawable/Drawable;)V

    return-object v2
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x6278ee2e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 611688
    check-cast p2, LX/Fiq;

    check-cast p4, Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 611689
    iget-object v1, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadFindMorePartDefinition;->d:Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;

    iget-object v2, p2, LX/Fiq;->a:Landroid/text/Spanned;

    iget-object p1, p2, LX/Fiq;->b:Landroid/graphics/drawable/Drawable;

    .line 611690
    invoke-static {v1, p4, p1}, Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;->a(Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;Lcom/facebook/fbui/widget/contentview/ContentView;Landroid/graphics/drawable/Drawable;)V

    .line 611691
    invoke-virtual {p4, v2}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 611692
    const p0, 0x7f0e086d

    invoke-virtual {p4, p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleTextAppearance(I)V

    .line 611693
    invoke-virtual {p4}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->getLocaleGravity()I

    move-result p0

    invoke-virtual {p4, p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleGravity(I)V

    .line 611694
    const/16 v1, 0x1f

    const v2, 0x1ebceb9b    # 2.0002718E-20f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 611687
    const/4 v0, 0x1

    return v0
.end method
