.class public Lcom/facebook/search/typeahead/rows/SearchTypeaheadHeaderPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pn;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/search/model/HeaderRowTypeaheadUnit;",
        "Landroid/view/View$OnClickListener;",
        "TE;",
        "Lcom/facebook/fbui/widget/contentview/ContentView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final b:LX/1Ua;

.field private static e:LX/0Xm;


# instance fields
.field private final c:LX/FjC;

.field private final d:Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/high16 v1, -0x40000000    # -2.0f

    .line 611568
    new-instance v0, LX/3be;

    invoke-direct {v0}, LX/3be;-><init>()V

    sput-object v0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadHeaderPartDefinition;->a:LX/1Cz;

    .line 611569
    invoke-static {}, LX/1UY;->c()LX/1UY;

    move-result-object v0

    .line 611570
    iput v1, v0, LX/1UY;->b:F

    .line 611571
    move-object v0, v0

    .line 611572
    iput v1, v0, LX/1UY;->c:F

    .line 611573
    move-object v0, v0

    .line 611574
    const/high16 v1, 0x41400000    # 12.0f

    .line 611575
    iput v1, v0, LX/1UY;->d:F

    .line 611576
    move-object v0, v0

    .line 611577
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadHeaderPartDefinition;->b:LX/1Ua;

    return-void
.end method

.method public constructor <init>(LX/FjC;Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 611564
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 611565
    iput-object p1, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadHeaderPartDefinition;->c:LX/FjC;

    .line 611566
    iput-object p2, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadHeaderPartDefinition;->d:Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;

    .line 611567
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/typeahead/rows/SearchTypeaheadHeaderPartDefinition;
    .locals 5

    .prologue
    .line 611525
    const-class v1, Lcom/facebook/search/typeahead/rows/SearchTypeaheadHeaderPartDefinition;

    monitor-enter v1

    .line 611526
    :try_start_0
    sget-object v0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadHeaderPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 611527
    sput-object v2, Lcom/facebook/search/typeahead/rows/SearchTypeaheadHeaderPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 611528
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 611529
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 611530
    new-instance p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadHeaderPartDefinition;

    invoke-static {v0}, LX/FjC;->b(LX/0QB;)LX/FjC;

    move-result-object v3

    check-cast v3, LX/FjC;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/search/typeahead/rows/SearchTypeaheadHeaderPartDefinition;-><init>(LX/FjC;Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;)V

    .line 611531
    move-object v0, p0

    .line 611532
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 611533
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadHeaderPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 611534
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 611535
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 611563
    sget-object v0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadHeaderPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 611560
    check-cast p2, Lcom/facebook/search/model/HeaderRowTypeaheadUnit;

    .line 611561
    iget-object v0, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadHeaderPartDefinition;->d:Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;

    new-instance v1, LX/1X6;

    invoke-static {p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    sget-object v3, Lcom/facebook/search/typeahead/rows/SearchTypeaheadHeaderPartDefinition;->b:LX/1Ua;

    sget-object v4, LX/1X9;->MIDDLE:LX/1X9;

    invoke-direct {v1, v2, v3, v4}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 611562
    iget-object v0, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadHeaderPartDefinition;->c:LX/FjC;

    invoke-virtual {p2}, Lcom/facebook/search/model/TypeaheadUnit;->l()LX/Cwb;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/FjC;->a(LX/Cwb;)Landroid/view/View$OnClickListener;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x49acb2df

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 611543
    check-cast p1, Lcom/facebook/search/model/HeaderRowTypeaheadUnit;

    check-cast p2, Landroid/view/View$OnClickListener;

    check-cast p4, Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 611544
    sget-object v1, LX/6VF;->SMALL:LX/6VF;

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setThumbnailSize(LX/6VF;)V

    .line 611545
    iget-object v1, p1, Lcom/facebook/search/model/HeaderRowTypeaheadUnit;->a:LX/Cwc;

    move-object v1, v1

    .line 611546
    iget-object v2, v1, LX/Cwc;->c:Ljava/lang/String;

    move-object v1, v2

    .line 611547
    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 611548
    iget-object v1, p1, Lcom/facebook/search/model/HeaderRowTypeaheadUnit;->a:LX/Cwc;

    move-object v1, v1

    .line 611549
    iget-boolean v2, v1, LX/Cwc;->f:Z

    move v1, v2

    .line 611550
    if-eqz v1, :cond_0

    .line 611551
    iget-object v1, p4, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    move-object v1, v1

    .line 611552
    check-cast v1, Lcom/facebook/widget/text/BetterTextView;

    .line 611553
    invoke-virtual {v1, p2}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 611554
    iget-object v2, p1, Lcom/facebook/search/model/HeaderRowTypeaheadUnit;->a:LX/Cwc;

    move-object v2, v2

    .line 611555
    iget-object p1, v2, LX/Cwc;->d:Ljava/lang/String;

    move-object v2, p1

    .line 611556
    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 611557
    const/4 v1, 0x1

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setShowAuxView(Z)V

    .line 611558
    :goto_0
    const/16 v1, 0x1f

    const v2, -0x6968899e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 611559
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setShowAuxView(Z)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 611542
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 611536
    check-cast p4, Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 611537
    iget-object v0, p4, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    move-object v0, v0

    .line 611538
    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    .line 611539
    if-eqz v0, :cond_0

    .line 611540
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 611541
    :cond_0
    return-void
.end method
