.class public Lcom/facebook/search/typeahead/rows/SearchTypeaheadShortcutPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/search/model/ShortcutTypeaheadUnit;",
        "LX/Fiw;",
        "LX/1PW;",
        "Lcom/facebook/fbui/widget/contentview/ContentView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentView;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private final b:Landroid/content/Context;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 611813
    const v0, 0x7f031335

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadShortcutPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 611845
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 611846
    iput-object p1, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadShortcutPartDefinition;->b:Landroid/content/Context;

    .line 611847
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/typeahead/rows/SearchTypeaheadShortcutPartDefinition;
    .locals 4

    .prologue
    .line 611834
    const-class v1, Lcom/facebook/search/typeahead/rows/SearchTypeaheadShortcutPartDefinition;

    monitor-enter v1

    .line 611835
    :try_start_0
    sget-object v0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadShortcutPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 611836
    sput-object v2, Lcom/facebook/search/typeahead/rows/SearchTypeaheadShortcutPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 611837
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 611838
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 611839
    new-instance p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadShortcutPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {p0, v3}, Lcom/facebook/search/typeahead/rows/SearchTypeaheadShortcutPartDefinition;-><init>(Landroid/content/Context;)V

    .line 611840
    move-object v0, p0

    .line 611841
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 611842
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadShortcutPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 611843
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 611844
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 611833
    sget-object v0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadShortcutPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 611830
    iget-object v0, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadShortcutPartDefinition;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b17ae

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 611831
    iget-object v1, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadShortcutPartDefinition;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b17b3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 611832
    new-instance v2, LX/Fiw;

    invoke-direct {v2, v0, v1}, LX/Fiw;-><init>(II)V

    return-object v2
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x6aaea547

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 611815
    check-cast p1, Lcom/facebook/search/model/ShortcutTypeaheadUnit;

    check-cast p2, LX/Fiw;

    check-cast p4, Lcom/facebook/fbui/widget/contentview/ContentView;

    const/4 v2, 0x1

    .line 611816
    sget-object v1, LX/6VF;->SMALL:LX/6VF;

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setThumbnailSize(LX/6VF;)V

    .line 611817
    iget-object v1, p1, Lcom/facebook/search/model/ShortcutTypeaheadUnit;->d:Landroid/net/Uri;

    move-object v1, v1

    .line 611818
    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Landroid/net/Uri;)V

    .line 611819
    iget v1, p2, LX/Fiw;->a:I

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailPadding(I)V

    .line 611820
    const/4 v1, 0x0

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setMaxLinesFromThumbnailSize(Z)V

    .line 611821
    invoke-virtual {p4, v2, v2}, Lcom/facebook/fbui/widget/contentview/ContentView;->e(II)V

    .line 611822
    const v1, 0x7f0e086d

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleTextAppearance(I)V

    .line 611823
    iget-object v1, p1, Lcom/facebook/search/model/ShortcutTypeaheadUnit;->b:Ljava/lang/String;

    move-object v1, v1

    .line 611824
    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 611825
    iget-object v1, p1, Lcom/facebook/search/model/ShortcutTypeaheadUnit;->f:Ljava/lang/String;

    move-object v1, v1

    .line 611826
    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 611827
    const v1, 0x7f0e0877

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleTextAppearance(I)V

    .line 611828
    invoke-virtual {p4}, Lcom/facebook/fbui/widget/contentview/ContentView;->getPaddingLeft()I

    move-result v1

    iget v2, p2, LX/Fiw;->b:I

    invoke-virtual {p4}, Lcom/facebook/fbui/widget/contentview/ContentView;->getPaddingRight()I

    move-result p0

    iget p3, p2, LX/Fiw;->b:I

    invoke-virtual {p4, v1, v2, p0, p3}, Lcom/facebook/fbui/widget/contentview/ContentView;->setPadding(IIII)V

    .line 611829
    const/16 v1, 0x1f

    const v2, 0x19d7c0c6

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 611814
    const/4 v0, 0x1

    return v0
.end method
