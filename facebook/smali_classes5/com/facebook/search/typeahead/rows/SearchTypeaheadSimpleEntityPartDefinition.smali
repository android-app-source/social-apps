.class public Lcom/facebook/search/typeahead/rows/SearchTypeaheadSimpleEntityPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/search/model/EntityTypeaheadUnit;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Lcom/facebook/widget/CustomFrameLayout;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/CustomFrameLayout;",
            ">;"
        }
    .end annotation
.end field

.field private static d:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;

.field private final c:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 611425
    const v0, 0x7f031329

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadSimpleEntityPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 611426
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 611427
    iput-object p1, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadSimpleEntityPartDefinition;->b:Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;

    .line 611428
    iput-object p2, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadSimpleEntityPartDefinition;->c:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    .line 611429
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/typeahead/rows/SearchTypeaheadSimpleEntityPartDefinition;
    .locals 5

    .prologue
    .line 611430
    const-class v1, Lcom/facebook/search/typeahead/rows/SearchTypeaheadSimpleEntityPartDefinition;

    monitor-enter v1

    .line 611431
    :try_start_0
    sget-object v0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadSimpleEntityPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 611432
    sput-object v2, Lcom/facebook/search/typeahead/rows/SearchTypeaheadSimpleEntityPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 611433
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 611434
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 611435
    new-instance p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadSimpleEntityPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/search/typeahead/rows/SearchTypeaheadSimpleEntityPartDefinition;-><init>(Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;)V

    .line 611436
    move-object v0, p0

    .line 611437
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 611438
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadSimpleEntityPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 611439
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 611440
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/CustomFrameLayout;",
            ">;"
        }
    .end annotation

    .prologue
    .line 611441
    sget-object v0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadSimpleEntityPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 611442
    check-cast p2, Lcom/facebook/search/model/EntityTypeaheadUnit;

    .line 611443
    const v0, 0x7f0d2c72

    iget-object v1, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadSimpleEntityPartDefinition;->c:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    .line 611444
    iget-object v2, p2, Lcom/facebook/search/model/EntityTypeaheadUnit;->b:Ljava/lang/String;

    move-object v2, v2

    .line 611445
    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 611446
    const v0, 0x7f0d2c72

    iget-object v1, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadSimpleEntityPartDefinition;->b:Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;

    .line 611447
    iget-object v2, p2, Lcom/facebook/search/model/EntityTypeaheadUnit;->e:Landroid/net/Uri;

    move-object v2, v2

    .line 611448
    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 611449
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 611450
    const/4 v0, 0x1

    return v0
.end method
