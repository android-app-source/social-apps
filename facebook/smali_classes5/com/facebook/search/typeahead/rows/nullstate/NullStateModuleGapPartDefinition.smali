.class public Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleGapPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/search/model/NullStateModuleCollectionUnit;",
        "Ljava/lang/Void;",
        "LX/FhE;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static b:LX/0Xm;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 612055
    new-instance v0, LX/3bm;

    invoke-direct {v0}, LX/3bm;-><init>()V

    sput-object v0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleGapPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 612056
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 612057
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleGapPartDefinition;
    .locals 3

    .prologue
    .line 612058
    const-class v1, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleGapPartDefinition;

    monitor-enter v1

    .line 612059
    :try_start_0
    sget-object v0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleGapPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 612060
    sput-object v2, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleGapPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 612061
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 612062
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 612063
    new-instance v0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleGapPartDefinition;

    invoke-direct {v0}, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleGapPartDefinition;-><init>()V

    .line 612064
    move-object v0, v0

    .line 612065
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 612066
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleGapPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 612067
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 612068
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 612069
    sget-object v0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleGapPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 612070
    const/4 v0, 0x1

    return v0
.end method
