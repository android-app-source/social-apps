.class public Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleSeeMorePartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/search/model/NullStateModuleCollectionUnit;",
        "Ljava/lang/Void;",
        "LX/FhE;",
        "Lcom/facebook/widget/text/BetterTextView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final b:LX/1Ua;

.field private static g:LX/0Xm;


# instance fields
.field private final c:Landroid/content/Context;

.field private final d:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final f:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 612045
    new-instance v0, LX/3bl;

    invoke-direct {v0}, LX/3bl;-><init>()V

    sput-object v0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleSeeMorePartDefinition;->a:LX/1Cz;

    .line 612046
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v0

    const/high16 v1, 0x40000000    # 2.0f

    .line 612047
    iput v1, v0, LX/1UY;->c:F

    .line 612048
    move-object v0, v0

    .line 612049
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleSeeMorePartDefinition;->b:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 612039
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 612040
    iput-object p1, p0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleSeeMorePartDefinition;->c:Landroid/content/Context;

    .line 612041
    iput-object p2, p0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleSeeMorePartDefinition;->d:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 612042
    iput-object p3, p0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleSeeMorePartDefinition;->e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 612043
    iput-object p4, p0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleSeeMorePartDefinition;->f:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 612044
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleSeeMorePartDefinition;
    .locals 7

    .prologue
    .line 612028
    const-class v1, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleSeeMorePartDefinition;

    monitor-enter v1

    .line 612029
    :try_start_0
    sget-object v0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleSeeMorePartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 612030
    sput-object v2, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleSeeMorePartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 612031
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 612032
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 612033
    new-instance p0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleSeeMorePartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleSeeMorePartDefinition;-><init>(Landroid/content/Context;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V

    .line 612034
    move-object v0, p0

    .line 612035
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 612036
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleSeeMorePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 612037
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 612038
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/text/BetterTextView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 612016
    sget-object v0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleSeeMorePartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 612023
    check-cast p2, Lcom/facebook/search/model/NullStateModuleCollectionUnit;

    check-cast p3, LX/FhE;

    .line 612024
    iget-object v0, p0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleSeeMorePartDefinition;->d:Lcom/facebook/multirow/parts/TextPartDefinition;

    iget-object v1, p0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleSeeMorePartDefinition;->c:Landroid/content/Context;

    const v2, 0x7f082322

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 612025
    iget-object v0, p0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleSeeMorePartDefinition;->e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    new-instance v1, LX/Fj5;

    invoke-direct {v1, p0, p2, p3}, LX/Fj5;-><init>(Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleSeeMorePartDefinition;Lcom/facebook/search/model/NullStateModuleCollectionUnit;LX/FhE;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 612026
    iget-object v0, p0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleSeeMorePartDefinition;->f:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    sget-object v2, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleSeeMorePartDefinition;->b:LX/1Ua;

    invoke-direct {v1, v2}, LX/1X6;-><init>(LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 612027
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 612017
    check-cast p1, Lcom/facebook/search/model/NullStateModuleCollectionUnit;

    .line 612018
    invoke-virtual {p1}, Lcom/facebook/search/model/TypeaheadCollectionUnit;->o()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    .line 612019
    iget v1, p1, Lcom/facebook/search/model/NullStateModuleCollectionUnit;->f:I

    move v1, v1

    .line 612020
    if-le v0, v1, :cond_0

    .line 612021
    iget-boolean v0, p1, Lcom/facebook/search/model/NullStateModuleCollectionUnit;->g:Z

    move v0, v0

    .line 612022
    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
