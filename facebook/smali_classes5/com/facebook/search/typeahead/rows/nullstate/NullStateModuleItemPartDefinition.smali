.class public Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleItemPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/search/model/NullStateModuleSuggestionUnit;",
        "LX/Fj4;",
        "LX/FhE;",
        "Lcom/facebook/fbui/widget/contentview/ContentView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentView;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "LX/3bj;",
            "LX/3bk;",
            ">;"
        }
    .end annotation
.end field

.field private static i:LX/0Xm;


# instance fields
.field private final c:LX/0ad;

.field private final d:LX/0wM;

.field private final e:Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleItemClickPartDefinition;

.field private final f:Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;

.field public final g:LX/8i7;

.field private final h:LX/0Uh;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const v6, -0xc5a768

    .line 611996
    const v0, 0x7f031282

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleItemPartDefinition;->a:LX/1Cz;

    .line 611997
    new-instance v0, LX/0P2;

    invoke-direct {v0}, LX/0P2;-><init>()V

    sget-object v1, LX/3bj;->ns_pulse:LX/3bj;

    new-instance v2, LX/3bk;

    const v3, 0x7f0210d9

    const v4, -0x55d279

    invoke-direct {v2, v3, v4}, LX/3bk;-><init>(II)V

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/3bj;->ns_trending:LX/3bj;

    new-instance v2, LX/3bk;

    const v3, 0x7f021101

    const v4, -0xca6e01

    invoke-direct {v2, v3, v4}, LX/3bk;-><init>(II)V

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/3bj;->ns_local:LX/3bj;

    new-instance v2, LX/3bk;

    const v3, 0x7f0210da

    invoke-direct {v2, v3, v6}, LX/3bk;-><init>(II)V

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/3bj;->ns_interest:LX/3bj;

    new-instance v2, LX/3bk;

    const v3, 0x7f0217d3

    const v4, -0x943145

    invoke-direct {v2, v3, v4}, LX/3bk;-><init>(II)V

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/3bj;->ns_social:LX/3bj;

    new-instance v2, LX/3bk;

    const v3, 0x7f0210d9

    invoke-direct {v2, v3, v6}, LX/3bk;-><init>(II)V

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/3bj;->ns_suggested:LX/3bj;

    new-instance v2, LX/3bk;

    const v3, 0x7f0210d9

    invoke-direct {v2, v3, v6}, LX/3bk;-><init>(II)V

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/3bj;->ns_top:LX/3bj;

    new-instance v2, LX/3bk;

    const v3, 0x7f0210d9

    invoke-direct {v2, v3, v6}, LX/3bk;-><init>(II)V

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/3bj;->unset:LX/3bj;

    new-instance v2, LX/3bk;

    const v3, 0x7f0210d9

    invoke-direct {v2, v3, v6}, LX/3bk;-><init>(II)V

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleItemPartDefinition;->b:LX/0P1;

    return-void
.end method

.method public constructor <init>(LX/0ad;LX/0wM;Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleItemClickPartDefinition;Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;LX/8i7;LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 611988
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 611989
    iput-object p1, p0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleItemPartDefinition;->c:LX/0ad;

    .line 611990
    iput-object p6, p0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleItemPartDefinition;->h:LX/0Uh;

    .line 611991
    iput-object p2, p0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleItemPartDefinition;->d:LX/0wM;

    .line 611992
    iput-object p3, p0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleItemPartDefinition;->e:Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleItemClickPartDefinition;

    .line 611993
    iput-object p4, p0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleItemPartDefinition;->f:Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;

    .line 611994
    iput-object p5, p0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleItemPartDefinition;->g:LX/8i7;

    .line 611995
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleItemPartDefinition;
    .locals 10

    .prologue
    .line 611931
    const-class v1, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleItemPartDefinition;

    monitor-enter v1

    .line 611932
    :try_start_0
    sget-object v0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleItemPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 611933
    sput-object v2, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleItemPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 611934
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 611935
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 611936
    new-instance v3, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleItemPartDefinition;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v5

    check-cast v5, LX/0wM;

    invoke-static {v0}, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleItemClickPartDefinition;->a(LX/0QB;)Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleItemClickPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleItemClickPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;->a(LX/0QB;)Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;

    move-result-object v7

    check-cast v7, Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;

    invoke-static {v0}, LX/8i7;->a(LX/0QB;)LX/8i7;

    move-result-object v8

    check-cast v8, LX/8i7;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v9

    check-cast v9, LX/0Uh;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleItemPartDefinition;-><init>(LX/0ad;LX/0wM;Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleItemClickPartDefinition;Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;LX/8i7;LX/0Uh;)V

    .line 611937
    move-object v0, v3

    .line 611938
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 611939
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleItemPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 611940
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 611941
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 611998
    sget-object v0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleItemPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 611949
    check-cast p2, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;

    check-cast p3, LX/FhE;

    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 611950
    iget-object v1, p0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleItemPartDefinition;->e:Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleItemClickPartDefinition;

    invoke-interface {p1, v1, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 611951
    iget-object v1, p0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleItemPartDefinition;->c:LX/0ad;

    sget-short v3, LX/100;->aR:S

    invoke-interface {v1, v3, v2}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 611952
    iget-object v1, p2, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;->a:LX/3bj;

    move-object v1, v1

    .line 611953
    sget-object v3, LX/3bj;->ns_trending:LX/3bj;

    invoke-virtual {v1, v3}, LX/3bj;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v0

    .line 611954
    :goto_0
    if-eqz v1, :cond_1

    .line 611955
    iget-boolean v1, p2, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;->q:Z

    move v1, v1

    .line 611956
    if-nez v1, :cond_1

    .line 611957
    iget-boolean v1, p2, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;->p:Z

    move v1, v1

    .line 611958
    if-eqz v1, :cond_1

    move v1, v0

    .line 611959
    :goto_1
    invoke-virtual {p2}, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;->z()V

    .line 611960
    sget-object v0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleItemPartDefinition;->b:LX/0P1;

    .line 611961
    iget-object v3, p2, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;->a:LX/3bj;

    move-object v3, v3

    .line 611962
    invoke-virtual {v0, v3}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleItemPartDefinition;->b:LX/0P1;

    .line 611963
    iget-object v3, p2, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;->a:LX/3bj;

    move-object v3, v3

    .line 611964
    invoke-virtual {v0, v3}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3bk;

    .line 611965
    :goto_2
    iget-boolean v3, p2, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;->n:Z

    move v3, v3

    .line 611966
    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleItemPartDefinition;->h:LX/0Uh;

    sget v4, LX/2SU;->v:I

    invoke-virtual {v3, v4, v2}, LX/0Uh;->a(IZ)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 611967
    new-instance v3, Landroid/text/SpannableStringBuilder;

    .line 611968
    iget-object v4, p2, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;->b:Ljava/lang/String;

    move-object v4, v4

    .line 611969
    invoke-direct {v3, v4}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 611970
    iget-object v4, p0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleItemPartDefinition;->g:LX/8i7;

    invoke-virtual {v4, v3}, LX/8i7;->c(Landroid/text/SpannableStringBuilder;)Landroid/text/SpannableStringBuilder;

    move-result-object v3

    move-object v3, v3

    .line 611971
    :goto_3
    new-instance v4, LX/Fj4;

    iget-object v5, p0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleItemPartDefinition;->f:Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;

    invoke-virtual {p3}, LX/1Qj;->getContext()Landroid/content/Context;

    move-result-object v6

    iget-object v7, p0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleItemPartDefinition;->d:LX/0wM;

    iget v8, v0, LX/3bk;->a:I

    iget v9, v0, LX/3bk;->b:I

    invoke-virtual {v7, v8, v9}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-virtual {v5, v6, v7, v2}, Lcom/facebook/search/suggestions/viewbinder/KeywordSuggestionViewBinder;->a(Landroid/content/Context;Landroid/graphics/drawable/Drawable;Z)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iget v0, v0, LX/3bk;->b:I

    const/16 p0, 0x21

    const/4 v9, 0x0

    .line 611972
    invoke-virtual {p2}, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;->o()Ljava/lang/String;

    move-result-object v5

    .line 611973
    iget-object v6, p2, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;->c:Ljava/lang/String;

    move-object v6, v6

    .line 611974
    invoke-static {v6}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 611975
    :goto_4
    move-object v0, v5

    .line 611976
    invoke-direct {v4, v1, v2, v3, v0}, LX/Fj4;-><init>(ZLandroid/graphics/drawable/Drawable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    return-object v4

    :cond_0
    move v1, v2

    .line 611977
    goto :goto_0

    :cond_1
    move v1, v2

    .line 611978
    goto :goto_1

    .line 611979
    :cond_2
    new-instance v0, LX/3bk;

    const v3, 0x7f0210d9

    const v4, -0xc5a768

    invoke-direct {v0, v3, v4}, LX/3bk;-><init>(II)V

    goto :goto_2

    .line 611980
    :cond_3
    iget-object v3, p2, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;->b:Ljava/lang/String;

    move-object v3, v3

    .line 611981
    goto :goto_3

    .line 611982
    :cond_4
    new-instance v6, Landroid/text/SpannableStringBuilder;

    invoke-direct {v6, v5}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 611983
    iget-object v5, p2, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;->c:Ljava/lang/String;

    move-object v5, v5

    .line 611984
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    .line 611985
    new-instance v7, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v7, v0}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v6, v7, v9, v5, p0}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 611986
    new-instance v7, Landroid/text/style/StyleSpan;

    const/4 v8, 0x1

    invoke-direct {v7, v8}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v6, v7, v9, v5, p0}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    move-object v5, v6

    .line 611987
    goto :goto_4
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x69482741

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 611943
    check-cast p2, LX/Fj4;

    check-cast p4, Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 611944
    iget-object v1, p2, LX/Fj4;->c:Ljava/lang/CharSequence;

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 611945
    iget-object v1, p2, LX/Fj4;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 611946
    iget-object v1, p2, LX/Fj4;->d:Ljava/lang/CharSequence;

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 611947
    iget-boolean v1, p2, LX/Fj4;->a:Z

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setShowAuxView(Z)V

    .line 611948
    const/16 v1, 0x1f

    const v2, 0x3166e8de

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 611942
    const/4 v0, 0x1

    return v0
.end method
