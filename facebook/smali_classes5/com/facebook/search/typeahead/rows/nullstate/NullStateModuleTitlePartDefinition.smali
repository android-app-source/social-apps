.class public Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleTitlePartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/search/model/NullStateModuleCollectionUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/FhE;",
        "Lcom/facebook/fbui/widget/contentview/ContentView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentView;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:LX/1Ua;

.field private static f:LX/0Xm;


# instance fields
.field public final c:Landroid/content/Context;

.field private final d:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

.field private final e:Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/high16 v1, -0x40000000    # -2.0f

    .line 612073
    const v0, 0x7f030c3c

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleTitlePartDefinition;->a:LX/1Cz;

    .line 612074
    invoke-static {}, LX/1UY;->c()LX/1UY;

    move-result-object v0

    .line 612075
    iput v1, v0, LX/1UY;->b:F

    .line 612076
    move-object v0, v0

    .line 612077
    iput v1, v0, LX/1UY;->c:F

    .line 612078
    move-object v0, v0

    .line 612079
    const/high16 v1, 0x41400000    # 12.0f

    .line 612080
    iput v1, v0, LX/1UY;->d:F

    .line 612081
    move-object v0, v0

    .line 612082
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleTitlePartDefinition;->b:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 612083
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 612084
    iput-object p1, p0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleTitlePartDefinition;->c:Landroid/content/Context;

    .line 612085
    iput-object p2, p0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleTitlePartDefinition;->d:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    .line 612086
    iput-object p3, p0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleTitlePartDefinition;->e:Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;

    .line 612087
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleTitlePartDefinition;
    .locals 6

    .prologue
    .line 612088
    const-class v1, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleTitlePartDefinition;

    monitor-enter v1

    .line 612089
    :try_start_0
    sget-object v0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleTitlePartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 612090
    sput-object v2, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleTitlePartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 612091
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 612092
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 612093
    new-instance p0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleTitlePartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleTitlePartDefinition;-><init>(Landroid/content/Context;Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;)V

    .line 612094
    move-object v0, p0

    .line 612095
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 612096
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleTitlePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 612097
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 612098
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 612099
    sget-object v0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleTitlePartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 612100
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 612101
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 612102
    check-cast v0, Lcom/facebook/search/model/NullStateModuleCollectionUnit;

    .line 612103
    iget-object v1, p0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleTitlePartDefinition;->d:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    .line 612104
    iget-object v2, v0, Lcom/facebook/search/model/NullStateModuleCollectionUnit;->c:Ljava/lang/String;

    move-object v2, v2

    .line 612105
    iget-object v3, v0, Lcom/facebook/search/model/NullStateModuleCollectionUnit;->d:Ljava/lang/String;

    move-object v0, v3

    .line 612106
    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 612107
    const/4 v3, 0x0

    .line 612108
    :cond_0
    :goto_0
    move-object v0, v3

    .line 612109
    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 612110
    iget-object v0, p0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleTitlePartDefinition;->e:Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;

    new-instance v1, LX/1X6;

    sget-object v2, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleTitlePartDefinition;->b:LX/1Ua;

    sget-object v3, LX/1X9;->MIDDLE:LX/1X9;

    invoke-direct {v1, p2, v2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 612111
    const/4 v0, 0x0

    return-object v0

    .line 612112
    :cond_1
    new-instance v3, LX/3DI;

    const-string v4, " "

    invoke-direct {v3, v4}, LX/3DI;-><init>(Ljava/lang/CharSequence;)V

    .line 612113
    invoke-virtual {v3, v2}, LX/3DI;->a(Ljava/lang/CharSequence;)LX/3DI;

    .line 612114
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 612115
    new-instance v4, Landroid/text/style/TextAppearanceSpan;

    iget-object v5, p0, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleTitlePartDefinition;->c:Landroid/content/Context;

    const p3, 0x7f0e086c

    invoke-direct {v4, v5, p3}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    const/16 v5, 0x11

    invoke-virtual {v3, v0, v4, v5}, LX/3DI;->a(Ljava/lang/CharSequence;Ljava/lang/Object;I)LX/3DI;

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x63248d30

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 612116
    check-cast p4, Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 612117
    sget-object v1, LX/6VF;->SMALL:LX/6VF;

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setThumbnailSize(LX/6VF;)V

    .line 612118
    const/16 v1, 0x10

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setGravity(I)V

    .line 612119
    const v1, 0x7f0e0867

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleTextAppearance(I)V

    .line 612120
    const/4 v1, 0x0

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailView(Landroid/view/View;)V

    .line 612121
    const/16 v1, 0x1f

    const v2, -0x68b762c4

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 612122
    const/4 v0, 0x1

    return v0
.end method
