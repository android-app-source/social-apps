.class public Lcom/facebook/search/typeahead/rows/SearchTypeaheadEntityPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/search/model/EntityTypeaheadUnit;",
        "LX/Fio;",
        "LX/1PW;",
        "Lcom/facebook/fbui/widget/contentview/ContentView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentView;",
            ">;"
        }
    .end annotation
.end field

.field private static f:LX/0Xm;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:LX/8i7;

.field private final d:LX/0ad;

.field private final e:LX/0Uh;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 611418
    const v0, 0x7f031335

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEntityPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/8i7;LX/0ad;LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 611419
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 611420
    iput-object p1, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEntityPartDefinition;->b:Landroid/content/Context;

    .line 611421
    iput-object p2, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEntityPartDefinition;->c:LX/8i7;

    .line 611422
    iput-object p3, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEntityPartDefinition;->d:LX/0ad;

    .line 611423
    iput-object p4, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEntityPartDefinition;->e:LX/0Uh;

    .line 611424
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/typeahead/rows/SearchTypeaheadEntityPartDefinition;
    .locals 7

    .prologue
    .line 611389
    const-class v1, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEntityPartDefinition;

    monitor-enter v1

    .line 611390
    :try_start_0
    sget-object v0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEntityPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 611391
    sput-object v2, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEntityPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 611392
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 611393
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 611394
    new-instance p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEntityPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/8i7;->a(LX/0QB;)LX/8i7;

    move-result-object v4

    check-cast v4, LX/8i7;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v6

    check-cast v6, LX/0Uh;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEntityPartDefinition;-><init>(Landroid/content/Context;LX/8i7;LX/0ad;LX/0Uh;)V

    .line 611395
    move-object v0, p0

    .line 611396
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 611397
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEntityPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 611398
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 611399
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/8i7;Lcom/facebook/search/model/EntityTypeaheadUnit;LX/0Uh;)Ljava/lang/CharSequence;
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 611347
    iget-object v0, p1, Lcom/facebook/search/model/EntityTypeaheadUnit;->b:Ljava/lang/String;

    move-object v4, v0

    .line 611348
    iget-object v0, p1, Lcom/facebook/search/model/EntityTypeaheadUnit;->i:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    move-object v3, v0

    .line 611349
    const/4 v0, 0x1

    const/4 v5, 0x0

    .line 611350
    iget-object v6, p1, Lcom/facebook/search/model/EntityTypeaheadUnit;->d:Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-object v6, v6

    .line 611351
    if-eqz v6, :cond_9

    .line 611352
    iget-object v6, p1, Lcom/facebook/search/model/EntityTypeaheadUnit;->d:Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-object v6, v6

    .line 611353
    invoke-virtual {v6}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v6

    const v7, 0x25d6af

    if-ne v6, v7, :cond_9

    move v6, v0

    .line 611354
    :goto_0
    if-eqz v6, :cond_b

    .line 611355
    iget-object v6, p1, Lcom/facebook/search/model/EntityTypeaheadUnit;->i:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    move-object v6, v6

    .line 611356
    sget-object v7, Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;->NOT_VERIFIED:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    if-eq v6, v7, :cond_a

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    if-eq v6, v7, :cond_a

    .line 611357
    :goto_1
    move v6, v0

    .line 611358
    iget-boolean v0, p1, Lcom/facebook/search/model/EntityTypeaheadUnit;->t:Z

    move v0, v0

    .line 611359
    if-eqz v0, :cond_0

    sget v0, LX/2SU;->v:I

    invoke-virtual {p2, v0, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 611360
    :goto_2
    sget-boolean v5, LX/007;->j:Z

    move v5, v5

    .line 611361
    if-eqz v5, :cond_1

    .line 611362
    iget-boolean v5, p1, Lcom/facebook/search/model/EntityTypeaheadUnit;->w:Z

    move v5, v5

    .line 611363
    if-eqz v5, :cond_1

    move v5, v1

    .line 611364
    :goto_3
    if-nez v6, :cond_2

    if-nez v0, :cond_2

    .line 611365
    iget-boolean v1, p1, Lcom/facebook/search/model/EntityTypeaheadUnit;->q:Z

    move v1, v1

    .line 611366
    if-nez v1, :cond_2

    if-nez v5, :cond_2

    move-object v0, v4

    .line 611367
    :goto_4
    return-object v0

    :cond_0
    move v0, v2

    .line 611368
    goto :goto_2

    :cond_1
    move v5, v2

    .line 611369
    goto :goto_3

    .line 611370
    :cond_2
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2, v4}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 611371
    if-eqz v6, :cond_4

    .line 611372
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;->NOT_VERIFIED:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    if-eq v3, v1, :cond_3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    if-ne v3, v1, :cond_8

    :cond_3
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;->BLUE_VERIFIED:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    .line 611373
    :goto_5
    invoke-static {p0, v1}, LX/8i7;->a(LX/8i7;Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 611374
    invoke-static {v2, v3}, LX/8i7;->a(Landroid/text/SpannableStringBuilder;Landroid/graphics/drawable/Drawable;)V

    .line 611375
    :cond_4
    if-eqz v0, :cond_5

    .line 611376
    invoke-virtual {p0, v2}, LX/8i7;->c(Landroid/text/SpannableStringBuilder;)Landroid/text/SpannableStringBuilder;

    .line 611377
    :cond_5
    iget-boolean v0, p1, Lcom/facebook/search/model/EntityTypeaheadUnit;->q:Z

    move v0, v0

    .line 611378
    if-eqz v0, :cond_6

    .line 611379
    :cond_6
    if-eqz v5, :cond_7

    .line 611380
    const/4 p0, 0x0

    move-object v0, p0

    .line 611381
    invoke-static {v2, v0}, LX/47q;->a(Landroid/text/SpannableStringBuilder;Landroid/graphics/drawable/Drawable;)Landroid/text/Spannable;

    .line 611382
    :cond_7
    move-object v0, v2

    .line 611383
    goto :goto_4

    :cond_8
    move-object v1, v3

    .line 611384
    goto :goto_5

    :cond_9
    move v6, v5

    .line 611385
    goto :goto_0

    :cond_a
    move v0, v5

    .line 611386
    goto :goto_1

    .line 611387
    :cond_b
    iget-boolean v0, p1, Lcom/facebook/search/model/EntityTypeaheadUnit;->h:Z

    move v0, v0

    .line 611388
    goto :goto_1
.end method

.method public static c(Lcom/facebook/search/model/EntityTypeaheadUnit;)Ljava/lang/String;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 611400
    iget-object v0, p0, Lcom/facebook/search/model/EntityTypeaheadUnit;->g:Ljava/lang/String;

    move-object v0, v0

    .line 611401
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 611402
    iget-object v1, p0, Lcom/facebook/search/model/EntityTypeaheadUnit;->d:Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-object v1, v1

    .line 611403
    if-eqz v1, :cond_0

    .line 611404
    iget-object v1, p0, Lcom/facebook/search/model/EntityTypeaheadUnit;->f:Ljava/lang/String;

    move-object v1, v1

    .line 611405
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 611406
    :cond_0
    :goto_0
    move v0, v0

    .line 611407
    if-eqz v0, :cond_1

    .line 611408
    iget-object v0, p0, Lcom/facebook/search/model/EntityTypeaheadUnit;->g:Ljava/lang/String;

    move-object v0, v0

    .line 611409
    :goto_1
    return-object v0

    .line 611410
    :cond_1
    iget-object v0, p0, Lcom/facebook/search/model/EntityTypeaheadUnit;->f:Ljava/lang/String;

    move-object v0, v0

    .line 611411
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 611412
    iget-object v0, p0, Lcom/facebook/search/model/EntityTypeaheadUnit;->f:Ljava/lang/String;

    move-object v0, v0

    .line 611413
    goto :goto_1

    .line 611414
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 611415
    :cond_3
    iget-object v1, p0, Lcom/facebook/search/model/EntityTypeaheadUnit;->d:Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-object v1, v1

    .line 611416
    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    .line 611417
    const v2, 0x285feb

    if-eq v1, v2, :cond_4

    const v2, 0x25d6af

    if-eq v1, v2, :cond_4

    const v2, 0x499e8e7

    if-ne v1, v2, :cond_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 611316
    sget-object v0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEntityPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 611317
    check-cast p2, Lcom/facebook/search/model/EntityTypeaheadUnit;

    .line 611318
    iget-object v0, p2, Lcom/facebook/search/model/EntityTypeaheadUnit;->e:Landroid/net/Uri;

    move-object v0, v0

    .line 611319
    if-eqz v0, :cond_0

    .line 611320
    iget-object v0, p2, Lcom/facebook/search/model/EntityTypeaheadUnit;->e:Landroid/net/Uri;

    move-object v0, v0

    .line 611321
    sget-object v1, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 611322
    :goto_0
    if-eqz v0, :cond_1

    .line 611323
    iget-object v0, p2, Lcom/facebook/search/model/EntityTypeaheadUnit;->e:Landroid/net/Uri;

    move-object v0, v0

    .line 611324
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    .line 611325
    :goto_1
    const v0, 0x7f0b17b1

    .line 611326
    iget-object v2, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEntityPartDefinition;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 611327
    iget-object v0, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEntityPartDefinition;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0b17b2

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 611328
    new-instance v0, LX/Fio;

    const/4 v4, -0x1

    iget-object v5, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEntityPartDefinition;->c:LX/8i7;

    iget-object v6, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEntityPartDefinition;->e:LX/0Uh;

    invoke-static {v5, p2, v6}, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEntityPartDefinition;->a(LX/8i7;Lcom/facebook/search/model/EntityTypeaheadUnit;LX/0Uh;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-static {p2}, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEntityPartDefinition;->c(Lcom/facebook/search/model/EntityTypeaheadUnit;)Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, LX/Fio;-><init>(Ljava/lang/String;IIILjava/lang/CharSequence;Ljava/lang/CharSequence;)V

    return-object v0

    .line 611329
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 611330
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x578fa683

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 611331
    check-cast p2, LX/Fio;

    check-cast p4, Lcom/facebook/fbui/widget/contentview/ContentView;

    const/4 v2, 0x1

    .line 611332
    iget-object v1, p2, LX/Fio;->a:Ljava/lang/String;

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Ljava/lang/String;)V

    .line 611333
    sget-object v1, LX/6VF;->SMALL:LX/6VF;

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setThumbnailSize(LX/6VF;)V

    .line 611334
    iget v1, p2, LX/Fio;->b:I

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailPadding(I)V

    .line 611335
    const/16 v1, 0x10

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailGravity(I)V

    .line 611336
    const/4 v1, 0x0

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setMaxLinesFromThumbnailSize(Z)V

    .line 611337
    invoke-virtual {p4, v2, v2}, Lcom/facebook/fbui/widget/contentview/ContentView;->e(II)V

    .line 611338
    iget-object v1, p2, LX/Fio;->e:Ljava/lang/CharSequence;

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 611339
    const v1, 0x7f0e086d

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleTextAppearance(I)V

    .line 611340
    iget-object v1, p2, LX/Fio;->f:Ljava/lang/CharSequence;

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 611341
    const v1, 0x7f0e0877

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleTextAppearance(I)V

    .line 611342
    iget v1, p2, LX/Fio;->d:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 611343
    iget v1, p2, LX/Fio;->d:I

    iget v2, p2, LX/Fio;->c:I

    iget p0, p2, LX/Fio;->d:I

    iget p1, p2, LX/Fio;->c:I

    invoke-virtual {p4, v1, v2, p0, p1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setPadding(IIII)V

    .line 611344
    :goto_0
    const/16 v1, 0x1f

    const v2, 0x209e53db

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 611345
    :cond_0
    invoke-virtual {p4}, Lcom/facebook/fbui/widget/contentview/ContentView;->getPaddingLeft()I

    move-result v1

    iget v2, p2, LX/Fio;->c:I

    invoke-virtual {p4}, Lcom/facebook/fbui/widget/contentview/ContentView;->getPaddingRight()I

    move-result p0

    iget p1, p2, LX/Fio;->c:I

    invoke-virtual {p4, v1, v2, p0, p1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setPadding(IIII)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 611346
    const/4 v0, 0x1

    return v0
.end method
