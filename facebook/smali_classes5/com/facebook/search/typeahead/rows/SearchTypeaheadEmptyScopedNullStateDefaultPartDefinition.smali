.class public Lcom/facebook/search/typeahead/rows/SearchTypeaheadEmptyScopedNullStateDefaultPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/search/model/EmptyScopedNullStateTypeaheadUnit;",
        "Ljava/lang/String;",
        "LX/1PW;",
        "Lcom/facebook/fbui/widget/text/ImageWithTextView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static c:LX/0Xm;


# instance fields
.field private final b:Landroid/content/Context;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 611875
    const v0, 0x7f0307eb

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEmptyScopedNullStateDefaultPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 611872
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 611873
    iput-object p1, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEmptyScopedNullStateDefaultPartDefinition;->b:Landroid/content/Context;

    .line 611874
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/typeahead/rows/SearchTypeaheadEmptyScopedNullStateDefaultPartDefinition;
    .locals 4

    .prologue
    .line 611861
    const-class v1, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEmptyScopedNullStateDefaultPartDefinition;

    monitor-enter v1

    .line 611862
    :try_start_0
    sget-object v0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEmptyScopedNullStateDefaultPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 611863
    sput-object v2, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEmptyScopedNullStateDefaultPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 611864
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 611865
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 611866
    new-instance p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEmptyScopedNullStateDefaultPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {p0, v3}, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEmptyScopedNullStateDefaultPartDefinition;-><init>(Landroid/content/Context;)V

    .line 611867
    move-object v0, p0

    .line 611868
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 611869
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEmptyScopedNullStateDefaultPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 611870
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 611871
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 611876
    sget-object v0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEmptyScopedNullStateDefaultPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 611852
    check-cast p2, Lcom/facebook/search/model/EmptyScopedNullStateTypeaheadUnit;

    .line 611853
    const-string v0, ""

    .line 611854
    sget-object v1, LX/Fik;->a:[I

    .line 611855
    iget-object v2, p2, Lcom/facebook/search/model/EmptyScopedNullStateTypeaheadUnit;->a:LX/103;

    move-object v2, v2

    .line 611856
    invoke-virtual {v2}, LX/103;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 611857
    :goto_0
    return-object v0

    .line 611858
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEmptyScopedNullStateDefaultPartDefinition;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0820b9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 611859
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEmptyScopedNullStateDefaultPartDefinition;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0820ba

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 611860
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEmptyScopedNullStateDefaultPartDefinition;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0820bb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x5d7a6f96

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 611849
    check-cast p2, Ljava/lang/String;

    check-cast p4, Lcom/facebook/fbui/widget/text/ImageWithTextView;

    .line 611850
    invoke-virtual {p4, p2}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setText(Ljava/lang/CharSequence;)V

    .line 611851
    const/16 v1, 0x1f

    const v2, 0x48d1001e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 611848
    const/4 v0, 0x1

    return v0
.end method
