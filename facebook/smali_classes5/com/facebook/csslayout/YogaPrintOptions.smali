.class public final enum Lcom/facebook/csslayout/YogaPrintOptions;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/csslayout/YogaPrintOptions;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/csslayout/YogaPrintOptions;

.field public static final enum CHILDREN:Lcom/facebook/csslayout/YogaPrintOptions;

.field public static final enum LAYOUT:Lcom/facebook/csslayout/YogaPrintOptions;

.field public static final enum STYLE:Lcom/facebook/csslayout/YogaPrintOptions;


# instance fields
.field private mIntValue:I


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 674040
    new-instance v0, Lcom/facebook/csslayout/YogaPrintOptions;

    const-string v1, "LAYOUT"

    invoke-direct {v0, v1, v5, v3}, Lcom/facebook/csslayout/YogaPrintOptions;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/facebook/csslayout/YogaPrintOptions;->LAYOUT:Lcom/facebook/csslayout/YogaPrintOptions;

    .line 674041
    new-instance v0, Lcom/facebook/csslayout/YogaPrintOptions;

    const-string v1, "STYLE"

    invoke-direct {v0, v1, v3, v4}, Lcom/facebook/csslayout/YogaPrintOptions;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/facebook/csslayout/YogaPrintOptions;->STYLE:Lcom/facebook/csslayout/YogaPrintOptions;

    .line 674042
    new-instance v0, Lcom/facebook/csslayout/YogaPrintOptions;

    const-string v1, "CHILDREN"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v4, v2}, Lcom/facebook/csslayout/YogaPrintOptions;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/facebook/csslayout/YogaPrintOptions;->CHILDREN:Lcom/facebook/csslayout/YogaPrintOptions;

    .line 674043
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/facebook/csslayout/YogaPrintOptions;

    sget-object v1, Lcom/facebook/csslayout/YogaPrintOptions;->LAYOUT:Lcom/facebook/csslayout/YogaPrintOptions;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/csslayout/YogaPrintOptions;->STYLE:Lcom/facebook/csslayout/YogaPrintOptions;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/csslayout/YogaPrintOptions;->CHILDREN:Lcom/facebook/csslayout/YogaPrintOptions;

    aput-object v1, v0, v4

    sput-object v0, Lcom/facebook/csslayout/YogaPrintOptions;->$VALUES:[Lcom/facebook/csslayout/YogaPrintOptions;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 674030
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 674031
    iput p3, p0, Lcom/facebook/csslayout/YogaPrintOptions;->mIntValue:I

    .line 674032
    return-void
.end method

.method public static fromInt(I)Lcom/facebook/csslayout/YogaPrintOptions;
    .locals 3

    .prologue
    .line 674034
    packed-switch p0, :pswitch_data_0

    .line 674035
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unkown enum value: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 674036
    :pswitch_1
    sget-object v0, Lcom/facebook/csslayout/YogaPrintOptions;->LAYOUT:Lcom/facebook/csslayout/YogaPrintOptions;

    .line 674037
    :goto_0
    return-object v0

    .line 674038
    :pswitch_2
    sget-object v0, Lcom/facebook/csslayout/YogaPrintOptions;->STYLE:Lcom/facebook/csslayout/YogaPrintOptions;

    goto :goto_0

    .line 674039
    :pswitch_3
    sget-object v0, Lcom/facebook/csslayout/YogaPrintOptions;->CHILDREN:Lcom/facebook/csslayout/YogaPrintOptions;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/csslayout/YogaPrintOptions;
    .locals 1

    .prologue
    .line 674044
    const-class v0, Lcom/facebook/csslayout/YogaPrintOptions;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/csslayout/YogaPrintOptions;

    return-object v0
.end method

.method public static values()[Lcom/facebook/csslayout/YogaPrintOptions;
    .locals 1

    .prologue
    .line 674033
    sget-object v0, Lcom/facebook/csslayout/YogaPrintOptions;->$VALUES:[Lcom/facebook/csslayout/YogaPrintOptions;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/csslayout/YogaPrintOptions;

    return-object v0
.end method


# virtual methods
.method public final intValue()I
    .locals 1

    .prologue
    .line 674029
    iget v0, p0, Lcom/facebook/csslayout/YogaPrintOptions;->mIntValue:I

    return v0
.end method
