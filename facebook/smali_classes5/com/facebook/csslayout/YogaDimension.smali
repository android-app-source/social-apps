.class public final enum Lcom/facebook/csslayout/YogaDimension;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/csslayout/YogaDimension;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/csslayout/YogaDimension;

.field public static final enum HEIGHT:Lcom/facebook/csslayout/YogaDimension;

.field public static final enum WIDTH:Lcom/facebook/csslayout/YogaDimension;


# instance fields
.field private mIntValue:I


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 673996
    new-instance v0, Lcom/facebook/csslayout/YogaDimension;

    const-string v1, "WIDTH"

    invoke-direct {v0, v1, v2, v2}, Lcom/facebook/csslayout/YogaDimension;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/facebook/csslayout/YogaDimension;->WIDTH:Lcom/facebook/csslayout/YogaDimension;

    .line 673997
    new-instance v0, Lcom/facebook/csslayout/YogaDimension;

    const-string v1, "HEIGHT"

    invoke-direct {v0, v1, v3, v3}, Lcom/facebook/csslayout/YogaDimension;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/facebook/csslayout/YogaDimension;->HEIGHT:Lcom/facebook/csslayout/YogaDimension;

    .line 673998
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/facebook/csslayout/YogaDimension;

    sget-object v1, Lcom/facebook/csslayout/YogaDimension;->WIDTH:Lcom/facebook/csslayout/YogaDimension;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/csslayout/YogaDimension;->HEIGHT:Lcom/facebook/csslayout/YogaDimension;

    aput-object v1, v0, v3

    sput-object v0, Lcom/facebook/csslayout/YogaDimension;->$VALUES:[Lcom/facebook/csslayout/YogaDimension;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 673999
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 674000
    iput p3, p0, Lcom/facebook/csslayout/YogaDimension;->mIntValue:I

    .line 674001
    return-void
.end method

.method public static fromInt(I)Lcom/facebook/csslayout/YogaDimension;
    .locals 3

    .prologue
    .line 674002
    packed-switch p0, :pswitch_data_0

    .line 674003
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unkown enum value: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 674004
    :pswitch_0
    sget-object v0, Lcom/facebook/csslayout/YogaDimension;->WIDTH:Lcom/facebook/csslayout/YogaDimension;

    .line 674005
    :goto_0
    return-object v0

    :pswitch_1
    sget-object v0, Lcom/facebook/csslayout/YogaDimension;->HEIGHT:Lcom/facebook/csslayout/YogaDimension;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/csslayout/YogaDimension;
    .locals 1

    .prologue
    .line 674006
    const-class v0, Lcom/facebook/csslayout/YogaDimension;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/csslayout/YogaDimension;

    return-object v0
.end method

.method public static values()[Lcom/facebook/csslayout/YogaDimension;
    .locals 1

    .prologue
    .line 674007
    sget-object v0, Lcom/facebook/csslayout/YogaDimension;->$VALUES:[Lcom/facebook/csslayout/YogaDimension;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/csslayout/YogaDimension;

    return-object v0
.end method


# virtual methods
.method public final intValue()I
    .locals 1

    .prologue
    .line 674008
    iget v0, p0, Lcom/facebook/csslayout/YogaDimension;->mIntValue:I

    return v0
.end method
