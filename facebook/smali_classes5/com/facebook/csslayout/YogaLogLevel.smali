.class public final enum Lcom/facebook/csslayout/YogaLogLevel;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/csslayout/YogaLogLevel;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/csslayout/YogaLogLevel;

.field public static final enum DEBUG:Lcom/facebook/csslayout/YogaLogLevel;

.field public static final enum ERROR:Lcom/facebook/csslayout/YogaLogLevel;

.field public static final enum INFO:Lcom/facebook/csslayout/YogaLogLevel;

.field public static final enum VERBOSE:Lcom/facebook/csslayout/YogaLogLevel;

.field public static final enum WARN:Lcom/facebook/csslayout/YogaLogLevel;


# instance fields
.field private mIntValue:I


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 674009
    new-instance v0, Lcom/facebook/csslayout/YogaLogLevel;

    const-string v1, "ERROR"

    invoke-direct {v0, v1, v2, v2}, Lcom/facebook/csslayout/YogaLogLevel;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/facebook/csslayout/YogaLogLevel;->ERROR:Lcom/facebook/csslayout/YogaLogLevel;

    .line 674010
    new-instance v0, Lcom/facebook/csslayout/YogaLogLevel;

    const-string v1, "WARN"

    invoke-direct {v0, v1, v3, v3}, Lcom/facebook/csslayout/YogaLogLevel;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/facebook/csslayout/YogaLogLevel;->WARN:Lcom/facebook/csslayout/YogaLogLevel;

    .line 674011
    new-instance v0, Lcom/facebook/csslayout/YogaLogLevel;

    const-string v1, "INFO"

    invoke-direct {v0, v1, v4, v4}, Lcom/facebook/csslayout/YogaLogLevel;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/facebook/csslayout/YogaLogLevel;->INFO:Lcom/facebook/csslayout/YogaLogLevel;

    .line 674012
    new-instance v0, Lcom/facebook/csslayout/YogaLogLevel;

    const-string v1, "DEBUG"

    invoke-direct {v0, v1, v5, v5}, Lcom/facebook/csslayout/YogaLogLevel;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/facebook/csslayout/YogaLogLevel;->DEBUG:Lcom/facebook/csslayout/YogaLogLevel;

    .line 674013
    new-instance v0, Lcom/facebook/csslayout/YogaLogLevel;

    const-string v1, "VERBOSE"

    invoke-direct {v0, v1, v6, v6}, Lcom/facebook/csslayout/YogaLogLevel;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/facebook/csslayout/YogaLogLevel;->VERBOSE:Lcom/facebook/csslayout/YogaLogLevel;

    .line 674014
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/facebook/csslayout/YogaLogLevel;

    sget-object v1, Lcom/facebook/csslayout/YogaLogLevel;->ERROR:Lcom/facebook/csslayout/YogaLogLevel;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/csslayout/YogaLogLevel;->WARN:Lcom/facebook/csslayout/YogaLogLevel;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/csslayout/YogaLogLevel;->INFO:Lcom/facebook/csslayout/YogaLogLevel;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/csslayout/YogaLogLevel;->DEBUG:Lcom/facebook/csslayout/YogaLogLevel;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/csslayout/YogaLogLevel;->VERBOSE:Lcom/facebook/csslayout/YogaLogLevel;

    aput-object v1, v0, v6

    sput-object v0, Lcom/facebook/csslayout/YogaLogLevel;->$VALUES:[Lcom/facebook/csslayout/YogaLogLevel;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 674026
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 674027
    iput p3, p0, Lcom/facebook/csslayout/YogaLogLevel;->mIntValue:I

    .line 674028
    return-void
.end method

.method public static fromInt(I)Lcom/facebook/csslayout/YogaLogLevel;
    .locals 3

    .prologue
    .line 674018
    packed-switch p0, :pswitch_data_0

    .line 674019
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unkown enum value: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 674020
    :pswitch_0
    sget-object v0, Lcom/facebook/csslayout/YogaLogLevel;->ERROR:Lcom/facebook/csslayout/YogaLogLevel;

    .line 674021
    :goto_0
    return-object v0

    .line 674022
    :pswitch_1
    sget-object v0, Lcom/facebook/csslayout/YogaLogLevel;->WARN:Lcom/facebook/csslayout/YogaLogLevel;

    goto :goto_0

    .line 674023
    :pswitch_2
    sget-object v0, Lcom/facebook/csslayout/YogaLogLevel;->INFO:Lcom/facebook/csslayout/YogaLogLevel;

    goto :goto_0

    .line 674024
    :pswitch_3
    sget-object v0, Lcom/facebook/csslayout/YogaLogLevel;->DEBUG:Lcom/facebook/csslayout/YogaLogLevel;

    goto :goto_0

    .line 674025
    :pswitch_4
    sget-object v0, Lcom/facebook/csslayout/YogaLogLevel;->VERBOSE:Lcom/facebook/csslayout/YogaLogLevel;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/csslayout/YogaLogLevel;
    .locals 1

    .prologue
    .line 674017
    const-class v0, Lcom/facebook/csslayout/YogaLogLevel;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/csslayout/YogaLogLevel;

    return-object v0
.end method

.method public static values()[Lcom/facebook/csslayout/YogaLogLevel;
    .locals 1

    .prologue
    .line 674016
    sget-object v0, Lcom/facebook/csslayout/YogaLogLevel;->$VALUES:[Lcom/facebook/csslayout/YogaLogLevel;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/csslayout/YogaLogLevel;

    return-object v0
.end method


# virtual methods
.method public final intValue()I
    .locals 1

    .prologue
    .line 674015
    iget v0, p0, Lcom/facebook/csslayout/YogaLogLevel;->mIntValue:I

    return v0
.end method
