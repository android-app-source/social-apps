.class public final Lcom/facebook/analytics/reporters/AppStateReporter$ReportAppStateRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/2EZ;


# direct methods
.method public constructor <init>(LX/2EZ;)V
    .locals 0

    .prologue
    .line 570938
    iput-object p1, p0, Lcom/facebook/analytics/reporters/AppStateReporter$ReportAppStateRunnable;->a:LX/2EZ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 570939
    iget-object v0, p0, Lcom/facebook/analytics/reporters/AppStateReporter$ReportAppStateRunnable;->a:LX/2EZ;

    iget-object v0, v0, LX/2EZ;->c:LX/0XZ;

    const-string v1, "fbandroid_cold_start"

    invoke-virtual {v0, v1}, LX/0XZ;->a(Ljava/lang/String;)LX/03R;

    move-result-object v2

    .line 570940
    iget-object v0, p0, Lcom/facebook/analytics/reporters/AppStateReporter$ReportAppStateRunnable;->a:LX/2EZ;

    iget-object v0, v0, LX/2EZ;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 570941
    const/4 v1, 0x0

    .line 570942
    if-nez v0, :cond_0

    .line 570943
    const-string v0, "User not logged in"

    .line 570944
    :goto_0
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 570945
    :goto_1
    if-eqz v0, :cond_2

    .line 570946
    iget-object v0, p0, Lcom/facebook/analytics/reporters/AppStateReporter$ReportAppStateRunnable;->a:LX/2EZ;

    iget-object v0, v0, LX/2EZ;->d:Ljava/util/concurrent/ScheduledExecutorService;

    const-wide/16 v2, 0x1

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, p0, v2, v3, v1}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 570947
    :goto_2
    return-void

    .line 570948
    :cond_0
    sget-object v0, LX/03R;->UNSET:LX/03R;

    if-ne v2, v0, :cond_3

    .line 570949
    const-string v0, "Sampling config not available"

    goto :goto_0

    .line 570950
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 570951
    :cond_2
    iget-object v0, p0, Lcom/facebook/analytics/reporters/AppStateReporter$ReportAppStateRunnable;->a:LX/2EZ;

    invoke-static {v0}, LX/2EZ;->c(LX/2EZ;)V

    goto :goto_2

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method
