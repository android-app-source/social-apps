.class public Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/03V;

.field private final c:LX/0So;

.field private final d:Ljava/util/concurrent/ScheduledExecutorService;

.field private final e:LX/0ad;

.field private final f:LX/2EF;

.field private final g:LX/03R;

.field private h:J

.field private i:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "LX/2EG;",
            ">;>;"
        }
    .end annotation
.end field

.field private j:LX/2EG;

.field private k:I

.field private l:I

.field private m:I

.field private n:I

.field private o:I

.field private p:I

.field private q:J

.field public r:Z

.field private final s:I

.field private final t:I

.field private final u:I

.field private final v:I

.field private final w:I

.field private final x:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 570937
    const-class v0, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;

    sput-object v0, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/03V;LX/0So;Ljava/util/concurrent/ScheduledExecutorService;LX/0ad;LX/03R;LX/2EF;)V
    .locals 12
    .param p3    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .param p5    # LX/03R;
        .annotation runtime Lcom/facebook/auth/annotations/IsMeUserAnEmployee;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 570919
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 570920
    const/16 v2, 0x3c

    iput v2, p0, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;->s:I

    .line 570921
    const/16 v2, 0x50

    iput v2, p0, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;->t:I

    .line 570922
    const/16 v2, 0x28

    iput v2, p0, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;->u:I

    .line 570923
    const/16 v2, 0x1e

    iput v2, p0, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;->v:I

    .line 570924
    const/16 v2, 0xf

    iput v2, p0, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;->w:I

    .line 570925
    const/16 v2, 0x64

    iput v2, p0, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;->x:I

    .line 570926
    iput-object p1, p0, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;->b:LX/03V;

    .line 570927
    iput-object p3, p0, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;->d:Ljava/util/concurrent/ScheduledExecutorService;

    .line 570928
    move-object/from16 v0, p4

    iput-object v0, p0, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;->e:LX/0ad;

    .line 570929
    move-object/from16 v0, p5

    iput-object v0, p0, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;->g:LX/03R;

    .line 570930
    move-object/from16 v0, p6

    iput-object v0, p0, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;->f:LX/2EF;

    .line 570931
    iput-object p2, p0, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;->c:LX/0So;

    .line 570932
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;->i:Ljava/util/Map;

    .line 570933
    iget-object v2, p0, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;->c:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;->h:J

    .line 570934
    new-instance v3, LX/2EG;

    const-wide/16 v4, 0x0

    const-wide/16 v6, 0x0

    const-wide/16 v8, 0x0

    const-wide/16 v10, 0x0

    invoke-direct/range {v3 .. v11}, LX/2EG;-><init>(DDDD)V

    iput-object v3, p0, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;->j:LX/2EG;

    .line 570935
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;->i:Ljava/util/Map;

    .line 570936
    return-void
.end method

.method private a(IIJ)Landroid/util/Pair;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIJ)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Boolean;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 570796
    iget-boolean v0, p0, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;->r:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;->k:I

    .line 570797
    :goto_0
    iget-boolean v1, p0, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;->r:Z

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;->m:I

    .line 570798
    :goto_1
    iget-boolean v2, p0, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;->r:Z

    if-eqz v2, :cond_2

    iget v2, p0, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;->o:I

    .line 570799
    :goto_2
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Runtime;->availableProcessors()I

    move-result v3

    invoke-static {v3, v7}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 570800
    mul-int/2addr v3, v0

    .line 570801
    int-to-long v4, v2

    cmp-long v2, p3, v4

    if-gez v2, :cond_3

    .line 570802
    new-instance v0, Landroid/util/Pair;

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {v0, v1, v8}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 570803
    :goto_3
    return-object v0

    .line 570804
    :cond_0
    iget v0, p0, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;->l:I

    goto :goto_0

    .line 570805
    :cond_1
    iget v1, p0, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;->n:I

    goto :goto_1

    .line 570806
    :cond_2
    iget v2, p0, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;->p:I

    goto :goto_2

    .line 570807
    :cond_3
    if-le p1, v3, :cond_4

    .line 570808
    const-string v2, "ProcessCpu Exceeded -> "

    .line 570809
    :goto_4
    const-string v4, "%s Time: %d, Thresholds(P/EP/T): (%d/%d/%d) Proc: %d "

    const/4 v5, 0x6

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v2, v5, v6

    long-to-int v2, p3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v7

    const/4 v2, 0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v2

    const/4 v0, 0x3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v0

    const/4 v0, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v5, v0

    const/4 v0, 0x5

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v5, v0

    invoke-static {v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 570810
    new-instance v0, Landroid/util/Pair;

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-direct {v0, v2, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_3

    .line 570811
    :cond_4
    if-le p2, v1, :cond_5

    .line 570812
    const-string v2, "ThreadCpu Exceeded -> "

    goto :goto_4

    .line 570813
    :cond_5
    new-instance v0, Landroid/util/Pair;

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {v0, v1, v8}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_3
.end method

.method private a(J)Ljava/util/List;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 570891
    invoke-static {}, Lcom/facebook/analytics/cpuusage/CpuTimeGetter;->c()Ljava/util/Map;

    move-result-object v3

    .line 570892
    if-eqz v3, :cond_1

    .line 570893
    new-instance v6, Ljava/util/ArrayList;

    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v0

    invoke-direct {v6, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 570894
    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 570895
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 570896
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/util/Pair;

    iget-object v2, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    .line 570897
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, LX/2EG;

    invoke-virtual {v0}, LX/2EG;->c()D

    move-result-wide v8

    .line 570898
    const-wide/16 v4, 0x0

    .line 570899
    iget-object v0, p0, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;->i:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 570900
    iget-object v0, p0, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;->i:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, LX/2EG;

    invoke-virtual {v0}, LX/2EG;->c()D

    move-result-wide v0

    .line 570901
    :goto_1
    sub-double v0, v8, v0

    long-to-double v4, p1

    div-double/2addr v0, v4

    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    mul-double/2addr v0, v4

    .line 570902
    new-instance v4, Landroid/util/Pair;

    double-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {v4, v2, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v6, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 570903
    :cond_0
    iput-object v3, p0, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;->i:Ljava/util/Map;

    .line 570904
    const/4 v0, 0x2

    const/4 v7, 0x0

    .line 570905
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v1

    if-ge v1, v0, :cond_3

    .line 570906
    :goto_2
    move-object v0, v6

    .line 570907
    :goto_3
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_3

    :cond_2
    move-wide v0, v4

    goto :goto_1

    :cond_3
    move v4, v7

    .line 570908
    :goto_4
    if-ge v4, v0, :cond_6

    .line 570909
    invoke-interface {v6, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/Pair;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 570910
    add-int/lit8 v1, v4, 0x1

    move v3, v4

    move v5, v2

    move v2, v1

    :goto_5
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v1

    if-ge v2, v1, :cond_5

    .line 570911
    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/Pair;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ge v5, v1, :cond_4

    .line 570912
    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/Pair;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    move v3, v2

    .line 570913
    :cond_4
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_5

    .line 570914
    :cond_5
    invoke-interface {v6, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/Pair;

    .line 570915
    invoke-interface {v6, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v6, v4, v2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 570916
    invoke-interface {v6, v3, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 570917
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 570918
    :cond_6
    invoke-interface {v6, v7, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v6

    goto :goto_2
.end method

.method private a(Ljava/util/List;Ljava/lang/String;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 570868
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 570869
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 570870
    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 570871
    const-string v1, "ebook"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "facebook."

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 570872
    :cond_1
    const-string v0, "main"

    move-object v2, v0

    .line 570873
    :goto_0
    invoke-static {}, Ljava/lang/Thread;->getAllStackTraces()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 570874
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 570875
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->getState()Ljava/lang/Thread$State;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread$State;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v7, "RUNNABLE"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 570876
    new-instance v7, Ljava/lang/StackTraceElement;

    const-string v8, "CpuSpin ---> "

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->getState()Ljava/lang/Thread$State;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread$State;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v10, 0x1

    invoke-direct {v7, v8, v9, v1, v10}, Ljava/lang/StackTraceElement;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 570877
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/StackTraceElement;

    array-length v7, v0

    move v1, v3

    :goto_1
    if-ge v1, v7, :cond_2

    aget-object v8, v0, v1

    .line 570878
    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 570879
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 570880
    :cond_3
    iget-object v1, p0, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;->b:LX/03V;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "CpuSpin-"

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 570881
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/StackTraceElement;

    invoke-interface {v4, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/StackTraceElement;

    .line 570882
    new-instance v3, LX/Ee2;

    invoke-direct {v3, p2}, LX/Ee2;-><init>(Ljava/lang/String;)V

    .line 570883
    invoke-virtual {v3, v2}, Ljava/lang/Exception;->setStackTrace([Ljava/lang/StackTraceElement;)V

    .line 570884
    move-object v2, v3

    .line 570885
    const/16 v3, 0x64

    invoke-virtual {v1, v0, p2, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;I)V

    .line 570886
    iget-object v0, p0, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;->f:LX/2EF;

    iget-wide v2, p0, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;->q:J

    const v5, 0x6e0030

    .line 570887
    iget-object v1, v0, LX/2EF;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v1, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 570888
    iget-object v1, v0, LX/2EF;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v4, "CpuSpin"

    invoke-interface {v1, v5, v4, p2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(ILjava/lang/String;Ljava/lang/String;)V

    .line 570889
    iget-object v1, v0, LX/2EF;->e:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v4, Lcom/facebook/analytics/cpuspindetector/CpuSpinUtils$1;

    invoke-direct {v4, v0}, Lcom/facebook/analytics/cpuspindetector/CpuSpinUtils$1;-><init>(LX/2EF;)V

    sget-object v5, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v1, v4, v2, v3, v5}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 570890
    return-void

    :cond_4
    move-object v2, v0

    goto/16 :goto_0
.end method

.method public static b(LX/0QB;)Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;
    .locals 13

    .prologue
    .line 570863
    new-instance v0, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v1

    check-cast v1, LX/03V;

    invoke-static {p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v2

    check-cast v2, LX/0So;

    invoke-static {p0}, LX/0um;->a(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-static {p0}, LX/0XD;->b(LX/0QB;)LX/03R;

    move-result-object v5

    check-cast v5, LX/03R;

    .line 570864
    new-instance v7, LX/2EF;

    const-class v8, Landroid/content/Context;

    const-class v9, Lcom/facebook/inject/ForAppContext;

    invoke-interface {p0, v8, v9}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/Context;

    invoke-static {p0}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v9

    check-cast v9, Landroid/os/Handler;

    invoke-static {p0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v10

    check-cast v10, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {p0}, LX/0um;->a(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v11

    check-cast v11, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v12

    check-cast v12, LX/0Uh;

    invoke-direct/range {v7 .. v12}, LX/2EF;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/facebook/quicklog/QuickPerformanceLogger;Ljava/util/concurrent/ScheduledExecutorService;LX/0Uh;)V

    .line 570865
    move-object v6, v7

    .line 570866
    check-cast v6, LX/2EF;

    invoke-direct/range {v0 .. v6}, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;-><init>(LX/03V;LX/0So;Ljava/util/concurrent/ScheduledExecutorService;LX/0ad;LX/03R;LX/2EF;)V

    .line 570867
    return-object v0
.end method

.method private static b()Z
    .locals 2

    .prologue
    .line 570861
    const-string v0, "java.vm.version"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 570862
    if-eqz v0, :cond_0

    const-string v1, "1."

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "0."

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static native getDalvikInfo()Ljava/lang/String;
.end method

.method private static native initDalvikInfoLogger()V
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    const/16 v3, 0x3c

    .line 570848
    iget-object v0, p0, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;->e:LX/0ad;

    sget v1, LX/2EK;->d:I

    invoke-interface {v0, v1, v3}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;->o:I

    .line 570849
    iget-object v0, p0, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;->e:LX/0ad;

    sget v1, LX/2EK;->c:I

    const/16 v2, 0x384

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;->p:I

    .line 570850
    iget-object v0, p0, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;->e:LX/0ad;

    sget v1, LX/2EK;->e:I

    invoke-interface {v0, v1, v3}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;->k:I

    .line 570851
    iget-object v0, p0, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;->e:LX/0ad;

    sget v1, LX/2EK;->f:I

    const/16 v2, 0x28

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;->l:I

    .line 570852
    iget-object v0, p0, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;->e:LX/0ad;

    sget v1, LX/2EK;->g:I

    const/16 v2, 0x50

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;->m:I

    .line 570853
    iget-object v0, p0, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;->e:LX/0ad;

    sget v1, LX/2EK;->h:I

    const/16 v2, 0x1e

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;->n:I

    .line 570854
    iget-object v0, p0, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;->e:LX/0ad;

    sget v1, LX/2EK;->b:I

    const/16 v2, 0xf

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;->q:J

    .line 570855
    invoke-static {}, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 570856
    :try_start_0
    const-string v0, "cpuspindetector"

    invoke-static {v0}, LX/01L;->a(Ljava/lang/String;)V

    .line 570857
    invoke-static {}, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;->initDalvikInfoLogger()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 570858
    :cond_0
    :goto_0
    return-void

    .line 570859
    :catch_0
    move-exception v0

    .line 570860
    sget-object v1, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;->a:Ljava/lang/Class;

    const-string v2, "Error initializing dalvik info logger"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final run()V
    .locals 12

    .prologue
    const/4 v2, 0x0

    .line 570814
    iget-boolean v0, p0, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;->r:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 570815
    iget-object v0, p0, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;->c:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v4

    .line 570816
    iget-wide v0, p0, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;->h:J

    sub-long v0, v4, v0

    const-wide/16 v6, 0x3e8

    div-long v6, v0, v6

    .line 570817
    const-wide/16 v0, 0x0

    cmp-long v0, v6, v0

    if-gtz v0, :cond_0

    .line 570818
    sget-object v0, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;->a:Ljava/lang/Class;

    const-string v1, "Time Elapsed <=0. Current Time: %s, Previous Time: %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v2

    const/4 v2, 0x1

    iget-wide v4, p0, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;->h:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v2

    invoke-static {v0, v1, v3}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 570819
    :goto_0
    return-void

    .line 570820
    :cond_0
    invoke-static {}, Lcom/facebook/analytics/cpuusage/CpuTimeGetter;->b()LX/2EG;

    move-result-object v3

    .line 570821
    invoke-direct {p0, v6, v7}, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;->a(J)Ljava/util/List;

    move-result-object v8

    .line 570822
    if-eqz v3, :cond_1

    if-eqz v8, :cond_1

    invoke-interface {v8}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 570823
    :cond_1
    sget-object v0, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;->a:Ljava/lang/Class;

    const-string v1, "CPU Usage Data was null. Will wait for next time"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_0

    .line 570824
    :cond_2
    invoke-virtual {v3}, LX/2EG;->c()D

    move-result-wide v0

    iget-object v9, p0, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;->j:LX/2EG;

    invoke-virtual {v9}, LX/2EG;->c()D

    move-result-wide v10

    sub-double/2addr v0, v10

    .line 570825
    long-to-double v10, v6

    div-double/2addr v0, v10

    const-wide/high16 v10, 0x4059000000000000L    # 100.0

    mul-double/2addr v0, v10

    .line 570826
    double-to-int v1, v0

    invoke-interface {v8, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v1, v0, v6, v7}, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;->a(IIJ)Landroid/util/Pair;

    move-result-object v1

    .line 570827
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 570828
    new-instance v6, Ljava/lang/StringBuilder;

    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-direct {v6, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move v1, v2

    .line 570829
    :goto_1
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 570830
    const-string v7, ", Thread%d: %s, Thread%d CPU: %d"

    add-int/lit8 v0, v1, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v8, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v10, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    add-int/lit8 v0, v1, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-interface {v8, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-static {v7, v9, v10, v11, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 570831
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 570832
    :cond_3
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 570833
    sget-object v0, LX/03R;->YES:LX/03R;

    iget-object v6, p0, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;->g:LX/03R;

    invoke-virtual {v0, v6}, LX/03R;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 570834
    invoke-interface {v8, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    const-string v2, "Liger-EventBase"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 570835
    invoke-static {}, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 570836
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", DalvikInfo: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;->getDalvikInfo()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 570837
    :goto_2
    invoke-direct {p0, v8, v0}, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;->a(Ljava/util/List;Ljava/lang/String;)V

    .line 570838
    :cond_4
    iput-wide v4, p0, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;->h:J

    .line 570839
    iput-object v3, p0, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;->j:LX/2EG;

    goto/16 :goto_0

    .line 570840
    :cond_5
    iget-object v0, p0, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;->f:LX/2EF;

    .line 570841
    iget-object v2, v0, LX/2EF;->a:LX/0Uh;

    const/16 v6, 0x47a

    const/4 v7, 0x0

    invoke-virtual {v2, v6, v7}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 570842
    iget-object v2, v0, LX/2EF;->c:Landroid/os/Handler;

    new-instance v6, Lcom/facebook/analytics/cpuspindetector/CpuSpinUtils$3;

    invoke-direct {v6, v0, v1}, Lcom/facebook/analytics/cpuspindetector/CpuSpinUtils$3;-><init>(LX/2EF;Ljava/lang/String;)V

    const v7, -0x16ffa66

    invoke-static {v2, v6, v7}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 570843
    :cond_6
    move-object v0, v1

    goto :goto_2

    .line 570844
    :cond_7
    iget-object v0, p0, Lcom/facebook/analytics/cpuspindetector/CpuSpinCheckerWorker;->f:LX/2EF;

    .line 570845
    iget-object v2, v0, LX/2EF;->a:LX/0Uh;

    const/16 v6, 0x478

    const/4 v7, 0x0

    invoke-virtual {v2, v6, v7}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 570846
    iget-object v2, v0, LX/2EF;->c:Landroid/os/Handler;

    new-instance v6, Lcom/facebook/analytics/cpuspindetector/CpuSpinUtils$2;

    invoke-direct {v6, v0, v1}, Lcom/facebook/analytics/cpuspindetector/CpuSpinUtils$2;-><init>(LX/2EF;Ljava/lang/String;)V

    const v7, -0x71fa6c90

    invoke-static {v2, v6, v7}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 570847
    :cond_8
    move-object v0, v1

    goto :goto_2
.end method
