.class public Lcom/facebook/analytics/cpuusage/CpuTimeGetter;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 571069
    const-class v0, Lcom/facebook/analytics/cpuusage/CpuTimeGetter;

    sput-object v0, Lcom/facebook/analytics/cpuusage/CpuTimeGetter;->a:Ljava/lang/Class;

    .line 571070
    const-string v0, "analytics4a"

    invoke-static {v0}, LX/01L;->a(Ljava/lang/String;)V

    .line 571071
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 571068
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a([Ljava/lang/String;IJ)D
    .locals 6

    .prologue
    const-wide/16 v0, 0x0

    .line 571059
    array-length v2, p0

    if-lt p1, v2, :cond_0

    .line 571060
    :goto_0
    return-wide v0

    .line 571061
    :cond_0
    aget-object v2, p0, p1

    .line 571062
    :try_start_0
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 571063
    long-to-double v0, v0

    long-to-double v2, p2

    div-double/2addr v0, v2

    .line 571064
    goto :goto_0

    .line 571065
    :catch_0
    move-exception v3

    .line 571066
    const-string v4, "Error parsing %d /proc/[pid]/stat field as long: %s"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v4, v5, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 571067
    invoke-static {v3, v2}, Lcom/facebook/analytics/cpuusage/CpuTimeGetter;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static a([Ljava/lang/String;)LX/2EG;
    .locals 10

    .prologue
    .line 571050
    if-nez p0, :cond_0

    .line 571051
    const/4 v1, 0x0

    .line 571052
    :goto_0
    return-object v1

    .line 571053
    :cond_0
    invoke-static {}, Lcom/facebook/analytics/cpuusage/CpuTimeGetter;->getClockTicksPerSecond()J

    move-result-wide v0

    .line 571054
    const/16 v2, 0xd

    invoke-static {p0, v2, v0, v1}, Lcom/facebook/analytics/cpuusage/CpuTimeGetter;->a([Ljava/lang/String;IJ)D

    move-result-wide v2

    .line 571055
    const/16 v4, 0xe

    invoke-static {p0, v4, v0, v1}, Lcom/facebook/analytics/cpuusage/CpuTimeGetter;->a([Ljava/lang/String;IJ)D

    move-result-wide v4

    .line 571056
    const/16 v6, 0xf

    invoke-static {p0, v6, v0, v1}, Lcom/facebook/analytics/cpuusage/CpuTimeGetter;->a([Ljava/lang/String;IJ)D

    move-result-wide v6

    .line 571057
    const/16 v8, 0x10

    invoke-static {p0, v8, v0, v1}, Lcom/facebook/analytics/cpuusage/CpuTimeGetter;->a([Ljava/lang/String;IJ)D

    move-result-wide v8

    .line 571058
    new-instance v1, LX/2EG;

    invoke-direct/range {v1 .. v9}, LX/2EG;-><init>(DDDD)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/Throwable;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 571048
    sget-object v0, Lcom/facebook/analytics/cpuusage/CpuTimeGetter;->a:Ljava/lang/Class;

    invoke-static {v0, p1, p0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 571049
    return-void
.end method

.method public static a()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 571047
    const-string v0, "/proc/self/stat"

    invoke-static {v0}, Lcom/facebook/analytics/cpuusage/CpuTimeGetter;->a(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/String;)[Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 571020
    invoke-static {}, Landroid/os/StrictMode;->allowThreadDiskReads()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v3

    .line 571021
    :try_start_0
    new-instance v2, Ljava/io/RandomAccessFile;

    const-string v1, "r"

    invoke-direct {v2, p0, v1}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 571022
    :try_start_1
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->readLine()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 571023
    :try_start_2
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 571024
    :goto_0
    invoke-static {v3}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    .line 571025
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 571026
    :cond_0
    :goto_1
    return-object v0

    .line 571027
    :catch_0
    move-exception v1

    .line 571028
    const-string v2, "Error closing procfs file: %s"

    invoke-static {v2, p0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 571029
    invoke-static {v1, v2}, Lcom/facebook/analytics/cpuusage/CpuTimeGetter;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0

    .line 571030
    :catch_1
    move-exception v1

    move-object v2, v0

    .line 571031
    :goto_2
    :try_start_3
    const-string v4, "Error reading cpu time from procfs file: %s"

    invoke-static {v4, p0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 571032
    invoke-static {v1, v4}, Lcom/facebook/analytics/cpuusage/CpuTimeGetter;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 571033
    if-eqz v2, :cond_0

    .line 571034
    :try_start_4
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 571035
    :goto_3
    invoke-static {v3}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    goto :goto_1

    .line 571036
    :catch_2
    move-exception v1

    .line 571037
    const-string v2, "Error closing procfs file: %s"

    invoke-static {v2, p0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 571038
    invoke-static {v1, v2}, Lcom/facebook/analytics/cpuusage/CpuTimeGetter;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_3

    .line 571039
    :catchall_0
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    :goto_4
    if-eqz v2, :cond_1

    .line 571040
    :try_start_5
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 571041
    :goto_5
    invoke-static {v3}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    :cond_1
    throw v0

    .line 571042
    :catch_3
    move-exception v1

    .line 571043
    const-string v2, "Error closing procfs file: %s"

    invoke-static {v2, p0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 571044
    invoke-static {v1, v2}, Lcom/facebook/analytics/cpuusage/CpuTimeGetter;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_5

    .line 571045
    :catchall_1
    move-exception v0

    goto :goto_4

    .line 571046
    :catch_4
    move-exception v1

    goto :goto_2
.end method

.method public static b()LX/2EG;
    .locals 1

    .prologue
    .line 570974
    invoke-static {}, Lcom/facebook/analytics/cpuusage/CpuTimeGetter;->a()[Ljava/lang/String;

    move-result-object v0

    .line 570975
    invoke-static {v0}, Lcom/facebook/analytics/cpuusage/CpuTimeGetter;->a([Ljava/lang/String;)LX/2EG;

    move-result-object v0

    return-object v0
.end method

.method private static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 571011
    const-string v0, "/proc/self/task/%s/comm"

    invoke-static {v0, p0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 571012
    :try_start_0
    new-instance v3, Ljava/io/RandomAccessFile;

    const-string v2, "r"

    invoke-direct {v3, v0, v2}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 571013
    :try_start_1
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->readLine()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 571014
    :try_start_2
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 571015
    :goto_0
    return-object v0

    .line 571016
    :catch_0
    move-exception v2

    :try_start_3
    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 571017
    :catchall_0
    move-exception v0

    :goto_1
    if-eqz v2, :cond_0

    :try_start_4
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    :goto_2
    :try_start_5
    throw v0

    .line 571018
    :catch_1
    move-object v0, v1

    goto :goto_0

    .line 571019
    :catch_2
    move-exception v3

    invoke-static {v2, v3}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_0
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_2

    :catchall_1
    move-exception v0

    move-object v2, v1

    goto :goto_1
.end method

.method public static c()Ljava/util/Map;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "LX/2EG;",
            ">;>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 570998
    :try_start_0
    invoke-static {}, Lcom/facebook/analytics/cpuusage/CpuTimeGetter;->d()Ljava/util/Map;

    move-result-object v0

    .line 570999
    if-nez v0, :cond_0

    move-object v0, v1

    .line 571000
    :goto_0
    return-object v0

    .line 571001
    :cond_0
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 571002
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 571003
    const-string v4, "/proc/%d/task/%s/stat"

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 571004
    invoke-static {v4}, Lcom/facebook/analytics/cpuusage/CpuTimeGetter;->a(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/analytics/cpuusage/CpuTimeGetter;->a([Ljava/lang/String;)LX/2EG;

    move-result-object v4

    .line 571005
    if-eqz v4, :cond_1

    .line 571006
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    new-instance v6, Landroid/util/Pair;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-direct {v6, v0, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v2, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 571007
    :catch_0
    move-exception v0

    .line 571008
    sget-object v2, Lcom/facebook/analytics/cpuusage/CpuTimeGetter;->a:Ljava/lang/Class;

    const-string v3, "Error getting thread level CPU Usage data"

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    .line 571009
    goto :goto_0

    :cond_2
    move-object v0, v2

    .line 571010
    goto :goto_0
.end method

.method private static d()Ljava/util/Map;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 570977
    invoke-static {}, Landroid/os/StrictMode;->allowThreadDiskReads()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v4

    .line 570978
    const-string v5, "/proc/self/task"

    .line 570979
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 570980
    :try_start_0
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 570981
    :try_start_1
    invoke-virtual {v2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v6

    .line 570982
    if-eqz v6, :cond_1

    .line 570983
    array-length v7, v6

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v7, :cond_1

    aget-object v8, v6, v3

    .line 570984
    invoke-virtual {v8}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/facebook/analytics/cpuusage/CpuTimeGetter;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 570985
    if-eqz v9, :cond_0

    .line 570986
    invoke-virtual {v8}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v0, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 570987
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 570988
    :cond_1
    invoke-static {v4}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    :goto_1
    return-object v0

    .line 570989
    :catch_0
    move-exception v0

    move-object v2, v1

    .line 570990
    :goto_2
    :try_start_2
    const-string v3, "Error getting threads from task directory: %s"

    invoke-static {v3, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 570991
    invoke-static {v0, v3}, Lcom/facebook/analytics/cpuusage/CpuTimeGetter;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 570992
    if-eqz v2, :cond_2

    .line 570993
    invoke-static {v4}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    :cond_2
    move-object v0, v1

    goto :goto_1

    .line 570994
    :catchall_0
    move-exception v0

    move-object v2, v1

    :goto_3
    if-eqz v2, :cond_3

    .line 570995
    invoke-static {v4}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    :cond_3
    throw v0

    .line 570996
    :catchall_1
    move-exception v0

    goto :goto_3

    .line 570997
    :catch_1
    move-exception v0

    goto :goto_2
.end method

.method public static native getClockTicksPerSecond()J
.end method

.method public static init()V
    .locals 0
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 570976
    return-void
.end method
