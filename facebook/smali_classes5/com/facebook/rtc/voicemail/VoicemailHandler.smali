.class public Lcom/facebook/rtc/voicemail/VoicemailHandler;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/rtc/voicemail/VoicemailHandler;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static volatile t:Lcom/facebook/rtc/voicemail/VoicemailHandler;


# instance fields
.field public final c:LX/2S5;

.field private final d:Landroid/content/Context;

.field public final e:LX/2S7;

.field public final f:LX/2SC;

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0ad;

.field public final i:LX/2SD;

.field public final j:LX/0Sh;

.field private final k:LX/00H;

.field public l:Z

.field public m:Z

.field private n:Landroid/media/MediaPlayer;

.field public o:Ljava/lang/String;

.field public p:Ljava/io/File;

.field public q:Ljava/lang/String;

.field public r:J

.field public s:LX/ECC;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 573284
    const-class v0, Lcom/facebook/rtc/voicemail/VoicemailHandler;

    sput-object v0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->a:Ljava/lang/Class;

    .line 573285
    const-class v0, Lcom/facebook/rtc/voicemail/VoicemailHandler;

    const-string v1, "voip_voicemail_audio"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/2S5;Landroid/content/Context;LX/2S7;LX/2SC;LX/0Ot;LX/0ad;LX/2SD;LX/0Sh;LX/00H;)V
    .locals 1
    .param p5    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2S5;",
            "Landroid/content/Context;",
            "LX/2S7;",
            "LX/2SC;",
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;",
            "LX/0ad;",
            "LX/2SD;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/00H;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 573272
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 573273
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->l:Z

    .line 573274
    iput-object p1, p0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->c:LX/2S5;

    .line 573275
    iput-object p2, p0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->d:Landroid/content/Context;

    .line 573276
    iput-object p3, p0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->e:LX/2S7;

    .line 573277
    iput-object p4, p0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->f:LX/2SC;

    .line 573278
    iput-object p5, p0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->g:LX/0Ot;

    .line 573279
    iput-object p6, p0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->h:LX/0ad;

    .line 573280
    iput-object p7, p0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->i:LX/2SD;

    .line 573281
    iput-object p8, p0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->j:LX/0Sh;

    .line 573282
    iput-object p9, p0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->k:LX/00H;

    .line 573283
    return-void
.end method

.method public static a(Lcom/facebook/rtc/voicemail/VoicemailHandler;I)Landroid/net/Uri;
    .locals 3

    .prologue
    .line 573237
    iget-object v0, p0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 573238
    new-instance v1, Landroid/net/Uri$Builder;

    invoke-direct {v1}, Landroid/net/Uri$Builder;-><init>()V

    const-string v2, "android.resource"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getResourcePackageName(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getResourceTypeName(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getResourceEntryName(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/rtc/voicemail/VoicemailHandler;
    .locals 14

    .prologue
    .line 573256
    sget-object v0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->t:Lcom/facebook/rtc/voicemail/VoicemailHandler;

    if-nez v0, :cond_1

    .line 573257
    const-class v1, Lcom/facebook/rtc/voicemail/VoicemailHandler;

    monitor-enter v1

    .line 573258
    :try_start_0
    sget-object v0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->t:Lcom/facebook/rtc/voicemail/VoicemailHandler;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 573259
    if-eqz v2, :cond_0

    .line 573260
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 573261
    new-instance v3, Lcom/facebook/rtc/voicemail/VoicemailHandler;

    invoke-static {v0}, LX/2S5;->a(LX/0QB;)LX/2S5;

    move-result-object v4

    check-cast v4, LX/2S5;

    const-class v5, Landroid/content/Context;

    invoke-interface {v0, v5}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    invoke-static {v0}, LX/2S7;->a(LX/0QB;)LX/2S7;

    move-result-object v6

    check-cast v6, LX/2S7;

    invoke-static {v0}, LX/2SC;->b(LX/0QB;)LX/2SC;

    move-result-object v7

    check-cast v7, LX/2SC;

    const/16 v8, 0x140d

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v9

    check-cast v9, LX/0ad;

    .line 573262
    new-instance v13, LX/2SD;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v10

    check-cast v10, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v11

    check-cast v11, LX/0SG;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v12

    check-cast v12, LX/0W3;

    invoke-direct {v13, v10, v11, v12}, LX/2SD;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;LX/0W3;)V

    .line 573263
    move-object v10, v13

    .line 573264
    check-cast v10, LX/2SD;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v11

    check-cast v11, LX/0Sh;

    const-class v12, LX/00H;

    invoke-interface {v0, v12}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, LX/00H;

    invoke-direct/range {v3 .. v12}, Lcom/facebook/rtc/voicemail/VoicemailHandler;-><init>(LX/2S5;Landroid/content/Context;LX/2S7;LX/2SC;LX/0Ot;LX/0ad;LX/2SD;LX/0Sh;LX/00H;)V

    .line 573265
    move-object v0, v3

    .line 573266
    sput-object v0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->t:Lcom/facebook/rtc/voicemail/VoicemailHandler;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 573267
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 573268
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 573269
    :cond_1
    sget-object v0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->t:Lcom/facebook/rtc/voicemail/VoicemailHandler;

    return-object v0

    .line 573270
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 573271
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/rtc/voicemail/VoicemailHandler;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;FLandroid/media/MediaPlayer$OnCompletionListener;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 573242
    new-instance v1, Landroid/media/MediaPlayer;

    invoke-direct {v1}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v1, p0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->n:Landroid/media/MediaPlayer;

    .line 573243
    iget-object v1, p0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->n:Landroid/media/MediaPlayer;

    invoke-virtual {v1, v0}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    .line 573244
    iget-object v1, p0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->n:Landroid/media/MediaPlayer;

    new-instance v2, LX/EIm;

    invoke-direct {v2, p0, p2}, LX/EIm;-><init>(Lcom/facebook/rtc/voicemail/VoicemailHandler;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 573245
    iget-object v1, p0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->n:Landroid/media/MediaPlayer;

    invoke-virtual {v1, p4, p4}, Landroid/media/MediaPlayer;->setVolume(FF)V

    .line 573246
    :try_start_0
    iget-object v1, p0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->n:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->d:Landroid/content/Context;

    invoke-virtual {v1, v2, p1}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    .line 573247
    iget-object v1, p0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->n:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->prepare()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 573248
    if-eqz p5, :cond_0

    .line 573249
    iget-object v0, p0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->n:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p5}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 573250
    :cond_0
    iget-object v0, p0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->n:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 573251
    const/4 v0, 0x1

    :cond_1
    :goto_0
    return v0

    .line 573252
    :catch_0
    invoke-static {p0, p3}, Lcom/facebook/rtc/voicemail/VoicemailHandler;->a$redex0(Lcom/facebook/rtc/voicemail/VoicemailHandler;Ljava/lang/String;)V

    .line 573253
    iget-object v1, p0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->p:Ljava/io/File;

    if-eqz v1, :cond_1

    .line 573254
    iget-object v1, p0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->p:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    .line 573255
    iget-object v1, p0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->p:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/rtc/voicemail/VoicemailHandler;Ljava/lang/String;)V
    .locals 8

    .prologue
    const-wide/16 v2, 0x0

    .line 573239
    iget-object v1, p0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->e:LX/2S7;

    const-string v6, "voicemail_error"

    move-wide v4, v2

    move-object v7, p1

    invoke-virtual/range {v1 .. v7}, LX/2S7;->logCallAction(JJLjava/lang/String;Ljava/lang/String;)V

    .line 573240
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->m:Z

    .line 573241
    return-void
.end method

.method private g()V
    .locals 1

    .prologue
    .line 573286
    iget-object v0, p0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->n:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_1

    .line 573287
    iget-object v0, p0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->n:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 573288
    iget-object v0, p0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->n:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 573289
    :cond_0
    iget-object v0, p0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->n:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    .line 573290
    iget-object v0, p0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->n:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 573291
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->n:Landroid/media/MediaPlayer;

    .line 573292
    :cond_1
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 573219
    iget-object v0, p0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->k:LX/00H;

    .line 573220
    iget-object v1, v0, LX/00H;->j:LX/01T;

    move-object v0, v1

    .line 573221
    sget-object v1, LX/01T;->MESSENGER:LX/01T;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    .line 573222
    :goto_0
    if-nez v0, :cond_1

    .line 573223
    :goto_1
    return-void

    .line 573224
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 573225
    :cond_1
    iget-object v0, p0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/rtc/voicemail/VoicemailHandler$1;

    invoke-direct {v1, p0}, Lcom/facebook/rtc/voicemail/VoicemailHandler$1;-><init>(Lcom/facebook/rtc/voicemail/VoicemailHandler;)V

    const v2, -0x67106016

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_1
.end method

.method public final c()V
    .locals 14

    .prologue
    const-wide/16 v10, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 573185
    iget-object v0, p0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->c:LX/2S5;

    .line 573186
    iget-boolean v1, v0, LX/2S5;->k:Z

    move v0, v1

    .line 573187
    if-eqz v0, :cond_1

    .line 573188
    iget-object v0, p0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->c:LX/2S5;

    invoke-virtual {v0}, LX/2S5;->b()V

    .line 573189
    iget-object v0, p0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->c:LX/2S5;

    .line 573190
    iget-boolean v1, v0, LX/2S5;->l:Z

    if-eqz v1, :cond_2

    sget-object v1, LX/Dcu;->TIME_LIMIT_REACHED_SUCCESS:LX/Dcu;

    :goto_0
    invoke-static {v0, v1}, LX/2S5;->a(LX/2S5;LX/Dcu;)Landroid/net/Uri;

    move-result-object v1

    move-object v1, v1

    .line 573191
    iget-object v0, p0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->s:LX/ECC;

    if-eqz v0, :cond_0

    .line 573192
    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->c:LX/2S5;

    .line 573193
    iget-wide v12, v0, LX/2S5;->j:J

    move-wide v2, v12

    .line 573194
    const-wide/16 v4, 0x7d0

    cmp-long v0, v2, v4

    if-lez v0, :cond_0

    .line 573195
    iget-object v0, p0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->c:LX/2S5;

    .line 573196
    iget-wide v12, v0, LX/2S5;->j:J

    move-wide v2, v12

    .line 573197
    iget-object v0, p0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->s:LX/ECC;

    iget-wide v4, p0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->r:J

    iget-object v6, p0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->q:Ljava/lang/String;

    invoke-interface/range {v0 .. v6}, LX/ECC;->a(Landroid/net/Uri;JJLjava/lang/String;)V

    .line 573198
    iget-object v0, p0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->e:LX/2S7;

    const-string v1, "voicemail_recorded"

    invoke-virtual {v0, v1, v7}, LX/2S7;->b(Ljava/lang/String;Z)Z

    .line 573199
    iget-object v0, p0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->e:LX/2S7;

    const-string v1, "voicemail_duration"

    iget-object v2, p0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->c:LX/2S5;

    .line 573200
    iget-wide v12, v2, LX/2S5;->j:J

    move-wide v2, v12

    .line 573201
    invoke-virtual {v0, v1, v2, v3}, LX/2S7;->b(Ljava/lang/String;J)Z

    .line 573202
    :cond_0
    iget-object v1, p0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->e:LX/2S7;

    const-string v6, "voicemail"

    iget-object v0, p0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->c:LX/2S5;

    .line 573203
    iget-wide v12, v0, LX/2S5;->j:J

    move-wide v2, v12

    .line 573204
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    move-wide v2, v10

    move-wide v4, v10

    invoke-virtual/range {v1 .. v7}, LX/2S7;->logCallAction(JJLjava/lang/String;Ljava/lang/String;)V

    .line 573205
    iput-boolean v8, p0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->m:Z

    .line 573206
    :goto_1
    return-void

    .line 573207
    :cond_1
    iput-boolean v7, p0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->l:Z

    .line 573208
    iput-boolean v8, p0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->m:Z

    goto :goto_1

    :cond_2
    sget-object v1, LX/Dcu;->SUCCESS:LX/Dcu;

    goto :goto_0
.end method

.method public final d()V
    .locals 8

    .prologue
    const-wide/16 v2, 0x0

    .line 573209
    invoke-direct {p0}, Lcom/facebook/rtc/voicemail/VoicemailHandler;->g()V

    .line 573210
    iget-object v0, p0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->c:LX/2S5;

    .line 573211
    iget-boolean v1, v0, LX/2S5;->k:Z

    move v0, v1

    .line 573212
    if-eqz v0, :cond_0

    .line 573213
    iget-object v0, p0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->c:LX/2S5;

    .line 573214
    iget-boolean v1, v0, LX/2S5;->l:Z

    if-eqz v1, :cond_1

    sget-object v1, LX/Dcu;->TIME_LIMIT_REACHED_CANCELLED:LX/Dcu;

    :goto_0
    invoke-static {v0, v1}, LX/2S5;->a(LX/2S5;LX/Dcu;)Landroid/net/Uri;

    .line 573215
    iget-object v1, p0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->e:LX/2S7;

    const-string v6, "voicemail"

    const-string v7, "cancel"

    move-wide v4, v2

    invoke-virtual/range {v1 .. v7}, LX/2S7;->logCallAction(JJLjava/lang/String;Ljava/lang/String;)V

    .line 573216
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->m:Z

    .line 573217
    return-void

    .line 573218
    :cond_1
    sget-object v1, LX/Dcu;->CANCELLED:LX/Dcu;

    goto :goto_0
.end method

.method public final init()V
    .locals 0

    .prologue
    .line 573226
    invoke-virtual {p0}, Lcom/facebook/rtc/voicemail/VoicemailHandler;->a()V

    .line 573227
    return-void
.end method

.method public final onCompletion(Landroid/media/MediaPlayer;)V
    .locals 8

    .prologue
    const-wide/16 v2, 0x0

    .line 573228
    invoke-direct {p0}, Lcom/facebook/rtc/voicemail/VoicemailHandler;->g()V

    .line 573229
    iget-boolean v0, p0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->l:Z

    if-nez v0, :cond_1

    .line 573230
    iget-object v0, p0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->s:LX/ECC;

    if-eqz v0, :cond_0

    .line 573231
    iget-object v0, p0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->e:LX/2S7;

    const-string v1, "voicemail_prompt_finished"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LX/2S7;->b(Ljava/lang/String;Z)Z

    .line 573232
    iget-object v0, p0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->c:LX/2S5;

    invoke-virtual {v0}, LX/2S5;->a()V

    .line 573233
    iget-object v0, p0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->s:LX/ECC;

    invoke-interface {v0}, LX/ECC;->b()V

    .line 573234
    :goto_0
    return-void

    .line 573235
    :cond_0
    iget-object v1, p0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->e:LX/2S7;

    const-string v6, "voicemail"

    const-string v7, "null_listener"

    move-wide v4, v2

    invoke-virtual/range {v1 .. v7}, LX/2S7;->logCallAction(JJLjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 573236
    :cond_1
    iget-object v1, p0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->e:LX/2S7;

    const-string v6, "voicemail"

    const-string v7, "early_cancel"

    move-wide v4, v2

    invoke-virtual/range {v1 .. v7}, LX/2S7;->logCallAction(JJLjava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
