.class public final Lcom/facebook/rtc/logging/WebrtcLoggingHandler$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/2S7;


# direct methods
.method public constructor <init>(LX/2S7;)V
    .locals 0

    .prologue
    .line 573314
    iput-object p1, p0, Lcom/facebook/rtc/logging/WebrtcLoggingHandler$1;->a:LX/2S7;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 15

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 573315
    iget-object v0, p0, Lcom/facebook/rtc/logging/WebrtcLoggingHandler$1;->a:LX/2S7;

    iget-object v0, v0, LX/2S7;->t:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v6

    .line 573316
    if-nez v6, :cond_1

    .line 573317
    :cond_0
    return-void

    .line 573318
    :cond_1
    array-length v7, v6

    move v5, v1

    :goto_0
    if-ge v5, v7, :cond_0

    aget-object v8, v6, v5

    .line 573319
    const/4 v4, 0x0

    .line 573320
    const/4 v0, 0x2

    .line 573321
    :try_start_0
    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v10

    invoke-virtual {v8}, Ljava/io/File;->lastModified()J
    :try_end_0
    .catch Ljava/io/EOFException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v12

    sub-long/2addr v10, v12

    const-wide/32 v12, 0xa4cb800

    cmp-long v3, v10, v12

    if-gez v3, :cond_2

    .line 573322
    :try_start_1
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, v8}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/EOFException; {:try_start_1 .. :try_end_1} :catch_9
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 573323
    :try_start_2
    new-instance v4, Ljava/io/ObjectInputStream;

    invoke-direct {v4, v3}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V

    .line 573324
    invoke-virtual {v4}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 573325
    const-string v9, "crash"

    const-string v10, "1"

    invoke-virtual {v0, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 573326
    iget-object v9, p0, Lcom/facebook/rtc/logging/WebrtcLoggingHandler$1;->a:LX/2S7;

    invoke-static {v9, v0}, LX/2S7;->a$redex0(LX/2S7;Ljava/util/HashMap;)V
    :try_end_2
    .catch Ljava/io/EOFException; {:try_start_2 .. :try_end_2} :catch_a
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_7
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 573327
    :try_start_3
    invoke-virtual {v4}, Ljava/io/ObjectInputStream;->close()V
    :try_end_3
    .catch Ljava/io/EOFException; {:try_start_3 .. :try_end_3} :catch_b
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_8
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    move v0, v1

    .line 573328
    :cond_2
    invoke-virtual {v8}, Ljava/io/File;->delete()Z

    move-result v3

    .line 573329
    if-lez v0, :cond_3

    if-eqz v3, :cond_3

    .line 573330
    iget-object v3, p0, Lcom/facebook/rtc/logging/WebrtcLoggingHandler$1;->a:LX/2S7;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v8, "Deleted non-uploaded call summary due to: "

    invoke-direct {v4, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-ne v0, v2, :cond_4

    const-string v0, "exception"

    :goto_1
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/2S7;->a(Ljava/lang/String;)V

    .line 573331
    :cond_3
    :goto_2
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_0

    .line 573332
    :cond_4
    const-string v0, "old"

    goto :goto_1

    .line 573333
    :catch_0
    move-object v3, v4

    :goto_3
    :try_start_4
    iget-object v4, p0, Lcom/facebook/rtc/logging/WebrtcLoggingHandler$1;->a:LX/2S7;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Unable to upload crashed call summary: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " cannot be parsed"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, LX/2S7;->a(Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    .line 573334
    if-eqz v3, :cond_5

    .line 573335
    :try_start_5
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    .line 573336
    :cond_5
    :goto_4
    invoke-virtual {v8}, Ljava/io/File;->delete()Z

    move-result v3

    .line 573337
    if-lez v0, :cond_3

    if-eqz v3, :cond_3

    .line 573338
    iget-object v3, p0, Lcom/facebook/rtc/logging/WebrtcLoggingHandler$1;->a:LX/2S7;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v8, "Deleted non-uploaded call summary due to: "

    invoke-direct {v4, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-ne v0, v2, :cond_6

    const-string v0, "exception"

    :goto_5
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/2S7;->a(Ljava/lang/String;)V

    goto :goto_2

    .line 573339
    :catch_1
    move-exception v3

    .line 573340
    sget-object v4, LX/2S7;->b:Ljava/lang/Class;

    const-string v9, "Failed to close dangling Stream"

    invoke-static {v4, v9, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_4

    .line 573341
    :cond_6
    const-string v0, "old"

    goto :goto_5

    .line 573342
    :catch_2
    move-exception v3

    move-object v14, v3

    move v3, v0

    move-object v0, v14

    .line 573343
    :goto_6
    :try_start_6
    iget-object v9, p0, Lcom/facebook/rtc/logging/WebrtcLoggingHandler$1;->a:LX/2S7;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "Unable to upload crashed call summary: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, LX/23D;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, LX/2S7;->a(Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_5
    .catchall {:try_start_6 .. :try_end_6} :catchall_5

    .line 573344
    :goto_7
    if-eqz v4, :cond_7

    .line 573345
    :try_start_7
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    .line 573346
    :cond_7
    :goto_8
    invoke-virtual {v8}, Ljava/io/File;->delete()Z

    move-result v0

    .line 573347
    if-lez v3, :cond_3

    if-eqz v0, :cond_3

    .line 573348
    iget-object v4, p0, Lcom/facebook/rtc/logging/WebrtcLoggingHandler$1;->a:LX/2S7;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v0, "Deleted non-uploaded call summary due to: "

    invoke-direct {v8, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-ne v3, v2, :cond_8

    const-string v0, "exception"

    :goto_9
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/2S7;->a(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 573349
    :catch_3
    move-exception v0

    .line 573350
    sget-object v4, LX/2S7;->b:Ljava/lang/Class;

    const-string v9, "Failed to close dangling Stream"

    invoke-static {v4, v9, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_8

    .line 573351
    :cond_8
    const-string v0, "old"

    goto :goto_9

    .line 573352
    :catchall_0
    move-exception v1

    move-object v3, v4

    .line 573353
    :goto_a
    if-eqz v3, :cond_9

    .line 573354
    :try_start_8
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    .line 573355
    :cond_9
    :goto_b
    invoke-virtual {v8}, Ljava/io/File;->delete()Z

    move-result v3

    .line 573356
    if-lez v0, :cond_a

    if-eqz v3, :cond_a

    .line 573357
    iget-object v3, p0, Lcom/facebook/rtc/logging/WebrtcLoggingHandler$1;->a:LX/2S7;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Deleted non-uploaded call summary due to: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-ne v0, v2, :cond_b

    const-string v0, "exception"

    :goto_c
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/2S7;->a(Ljava/lang/String;)V

    .line 573358
    :cond_a
    throw v1

    .line 573359
    :catch_4
    move-exception v3

    .line 573360
    sget-object v4, LX/2S7;->b:Ljava/lang/Class;

    const-string v5, "Failed to close dangling Stream"

    invoke-static {v4, v5, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_b

    .line 573361
    :cond_b
    const-string v0, "old"

    goto :goto_c

    .line 573362
    :catchall_1
    move-exception v0

    move-object v1, v0

    move-object v3, v4

    move v0, v2

    goto :goto_a

    :catchall_2
    move-exception v0

    move-object v1, v0

    move v0, v2

    goto :goto_a

    :catchall_3
    move-exception v0

    move-object v14, v0

    move v0, v1

    move-object v1, v14

    goto :goto_a

    :catchall_4
    move-exception v1

    goto :goto_a

    :catchall_5
    move-exception v0

    move-object v1, v0

    move v0, v3

    move-object v3, v4

    goto :goto_a

    :catch_5
    goto :goto_7

    .line 573363
    :catch_6
    move-exception v0

    move v3, v2

    goto/16 :goto_6

    :catch_7
    move-exception v0

    move-object v4, v3

    move v3, v2

    goto/16 :goto_6

    :catch_8
    move-exception v0

    move-object v4, v3

    move v3, v1

    goto/16 :goto_6

    .line 573364
    :catch_9
    move v0, v2

    move-object v3, v4

    goto/16 :goto_3

    :catch_a
    move v0, v2

    goto/16 :goto_3

    :catch_b
    move v0, v1

    goto/16 :goto_3
.end method
