.class public abstract Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;
.super Lcom/facebook/iorg/common/zero/ui/IorgDialogFragment;
.source ""

# interfaces
.implements LX/0fh;


# instance fields
.field public l:LX/4g9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/12M;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:Ljava/lang/String;

.field public o:Ljava/lang/String;

.field public p:LX/0yY;

.field public q:LX/4g1;

.field public r:Ljava/lang/Object;

.field public s:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 799149
    invoke-direct {p0}, Lcom/facebook/iorg/common/zero/ui/IorgDialogFragment;-><init>()V

    .line 799150
    return-void
.end method

.method public static a(LX/0yY;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;LX/4g1;Lcom/facebook/iorg/common/zero/IorgDialogDisplayContext;)Landroid/os/Bundle;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Lcom/facebook/iorg/common/zero/IorgDialogDisplayContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 799135
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 799136
    if-eqz p5, :cond_0

    .line 799137
    const-string v1, "dialog_context"

    invoke-virtual {v0, v1, p5}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 799138
    :cond_0
    move-object v0, v0

    .line 799139
    const-string v1, "dialogName"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 799140
    const-string v1, "dialogState"

    invoke-virtual {v0, v1, p4}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 799141
    const-string v1, "dialogTitle"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 799142
    const-string v1, "dialogContent"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 799143
    if-eqz p3, :cond_1

    .line 799144
    instance-of v1, p3, Lcom/facebook/flatbuffers/Flattenable;

    if-eqz v1, :cond_2

    .line 799145
    const-string v1, "dialogExtraDataFlattenable"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 799146
    const-string v1, "dialogExtraData"

    invoke-static {v0, v1, p3}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 799147
    :cond_1
    :goto_0
    return-object v0

    .line 799148
    :cond_2
    const-string v1, "dialogExtraData"

    check-cast p3, Landroid/os/Parcelable;

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .param p0    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 799134
    instance-of v0, p0, Landroid/content/Intent;

    if-eqz v0, :cond_0

    check-cast p0, Landroid/content/Intent;

    const-string v0, "tracking_codes"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;

    invoke-static {p0}, LX/4g9;->a(LX/0QB;)LX/4g9;

    move-result-object v1

    check-cast v1, LX/4g9;

    invoke-static {p0}, LX/12M;->a(LX/0QB;)LX/12M;

    move-result-object p0

    check-cast p0, LX/12M;

    iput-object v1, p1, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->l:LX/4g9;

    iput-object p0, p1, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->m:LX/12M;

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 799115
    new-instance v4, LX/0P2;

    invoke-direct {v4}, LX/0P2;-><init>()V

    .line 799116
    const-string v0, "dialogName"

    iget-object v1, p0, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->p:LX/0yY;

    invoke-virtual {v1}, LX/0yY;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 799117
    const-string v0, "dialogState"

    iget-object v1, p0, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->q:LX/4g1;

    invoke-virtual {v1}, LX/4g1;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 799118
    if-eqz p2, :cond_0

    .line 799119
    const-string v0, "tracking_codes"

    invoke-virtual {v4, v0, p2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 799120
    :cond_0
    iget-object v0, p0, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->l:LX/4g9;

    invoke-virtual {p0}, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->m()LX/4g6;

    move-result-object v1

    const-string v2, "button"

    iget-object v3, p0, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->s:Ljava/lang/String;

    invoke-virtual {v4}, LX/0P2;->b()LX/0P1;

    move-result-object v5

    move-object v4, p1

    .line 799121
    new-instance p0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    iget-object p1, v1, LX/4g5;->n:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 799122
    iput-object v2, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->d:Ljava/lang/String;

    .line 799123
    move-object p0, p0

    .line 799124
    iput-object v3, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->f:Ljava/lang/String;

    .line 799125
    move-object p0, p0

    .line 799126
    iput-object v4, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->e:Ljava/lang/String;

    .line 799127
    move-object p0, p0

    .line 799128
    invoke-virtual {p0, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    .line 799129
    iget-object p1, v1, LX/4g5;->o:Ljava/lang/String;

    if-eqz p1, :cond_1

    .line 799130
    iget-object p1, v1, LX/4g5;->o:Ljava/lang/String;

    .line 799131
    iput-object p1, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 799132
    :cond_1
    iget-object p1, v0, LX/4g9;->a:LX/0Zb;

    invoke-interface {p1, p0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 799133
    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 799111
    invoke-super {p0, p1}, Lcom/facebook/iorg/common/zero/ui/IorgDialogFragment;->a(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    .line 799112
    new-instance v1, LX/4g8;

    invoke-direct {v1, p0}, LX/4g8;-><init>(Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 799113
    invoke-virtual {p0}, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->n()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->r:Ljava/lang/Object;

    invoke-static {v2}, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 799114
    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 799110
    invoke-virtual {p0}, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->m()LX/4g6;

    move-result-object v0

    iget-object v0, v0, LX/4g5;->o:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final j()V
    .locals 6

    .prologue
    .line 799106
    invoke-virtual {p0}, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->o()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->r:Ljava/lang/Object;

    invoke-static {v1}, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 799107
    iget-object v0, p0, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->m:LX/12M;

    new-instance v1, LX/4g4;

    iget-object v2, p0, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->p:LX/0yY;

    sget-object v3, LX/4g3;->CONFIRM:LX/4g3;

    iget-object v4, p0, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->r:Ljava/lang/Object;

    iget-object v5, p0, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->q:LX/4g1;

    invoke-direct {v1, v2, v3, v4, v5}, LX/4g4;-><init>(LX/0yY;LX/4g3;Ljava/lang/Object;LX/4g1;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 799108
    invoke-virtual {p0}, Lcom/facebook/iorg/common/zero/ui/IorgDialogFragment;->i()V

    .line 799109
    return-void
.end method

.method public final k()V
    .locals 6

    .prologue
    .line 799101
    invoke-virtual {p0}, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->p()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->r:Ljava/lang/Object;

    invoke-static {v1}, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 799102
    iget-object v0, p0, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->m:LX/12M;

    new-instance v1, LX/4g4;

    iget-object v2, p0, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->p:LX/0yY;

    sget-object v3, LX/4g3;->CANCEL:LX/4g3;

    iget-object v4, p0, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->r:Ljava/lang/Object;

    iget-object v5, p0, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->q:LX/4g1;

    invoke-direct {v1, v2, v3, v4, v5}, LX/4g4;-><init>(LX/0yY;LX/4g3;Ljava/lang/Object;LX/4g1;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 799103
    invoke-virtual {p0}, Lcom/facebook/iorg/common/zero/ui/IorgDialogFragment;->i()V

    .line 799104
    invoke-virtual {p0}, Lcom/facebook/iorg/common/zero/ui/IorgDialogFragment;->h()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, LX/2Na;->a(Landroid/app/Activity;)V

    .line 799105
    return-void
.end method

.method public m()LX/4g6;
    .locals 1

    .prologue
    .line 799100
    sget-object v0, LX/4g6;->k:LX/4g6;

    return-object v0
.end method

.method public abstract n()Ljava/lang/String;
.end method

.method public abstract o()Ljava/lang/String;
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 0

    .prologue
    .line 799098
    invoke-virtual {p0}, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->k()V

    .line 799099
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x9f3d611

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 799083
    invoke-super {p0, p1}, Lcom/facebook/iorg/common/zero/ui/IorgDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 799084
    const-class v0, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;

    invoke-static {v0, p0}, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 799085
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v0

    .line 799086
    if-eqz v2, :cond_0

    .line 799087
    const-string v0, "dialogName"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/0yY;

    iput-object v0, p0, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->p:LX/0yY;

    .line 799088
    const-string v0, "dialogState"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/4g1;

    iput-object v0, p0, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->q:LX/4g1;

    .line 799089
    const-string v0, "dialogTitle"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->n:Ljava/lang/String;

    .line 799090
    const-string v0, "dialogContent"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->o:Ljava/lang/String;

    .line 799091
    const-string v0, "dialogExtraDataFlattenable"

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 799092
    const-string v0, "dialogExtraData"

    invoke-static {v2, v0}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->r:Ljava/lang/Object;

    .line 799093
    :cond_0
    :goto_0
    if-eqz p1, :cond_2

    .line 799094
    const-string v0, "uuid"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->s:Ljava/lang/String;

    .line 799095
    :goto_1
    const v0, 0xd25df9e

    invoke-static {v0, v1}, LX/02F;->f(II)V

    return-void

    .line 799096
    :cond_1
    const-string v0, "dialogExtraData"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->r:Ljava/lang/Object;

    goto :goto_0

    .line 799097
    :cond_2
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->s:Ljava/lang/String;

    goto :goto_1
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 799080
    invoke-super {p0, p1}, Lcom/facebook/iorg/common/zero/ui/IorgDialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 799081
    const-string v0, "uuid"

    iget-object v1, p0, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->s:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 799082
    return-void
.end method

.method public abstract p()Ljava/lang/String;
.end method
