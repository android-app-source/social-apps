.class public Lcom/facebook/iorg/common/zero/ui/IorgDialogFragment;
.super Landroid/support/v4/app/DialogFragment;
.source ""

# interfaces
.implements LX/02k;


# instance fields
.field public final j:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public k:LX/4g7;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private l:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 799071
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    .line 799072
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/iorg/common/zero/ui/IorgDialogFragment;->l:Z

    .line 799073
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/iorg/common/zero/ui/IorgDialogFragment;->j:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/iorg/common/zero/ui/IorgDialogFragment;

    invoke-static {p0}, LX/4g7;->a(LX/0QB;)LX/4g7;

    move-result-object p0

    check-cast p0, LX/4g7;

    iput-object p0, p1, Lcom/facebook/iorg/common/zero/ui/IorgDialogFragment;->k:LX/4g7;

    return-void
.end method

.method private j()Lcom/facebook/iorg/common/zero/IorgDialogDisplayContext;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 799065
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 799066
    const-string v1, "dialog_context"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 799067
    const/4 v0, 0x0

    .line 799068
    :goto_0
    return-object v0

    .line 799069
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 799070
    const-string v1, "dialog_context"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/iorg/common/zero/IorgDialogDisplayContext;

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2

    .prologue
    .line 799061
    iget-object v0, p0, Lcom/facebook/iorg/common/zero/ui/IorgDialogFragment;->k:LX/4g7;

    invoke-direct {p0}, Lcom/facebook/iorg/common/zero/ui/IorgDialogFragment;->j()Lcom/facebook/iorg/common/zero/IorgDialogDisplayContext;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4g7;->a(Lcom/facebook/iorg/common/zero/IorgDialogDisplayContext;)V

    .line 799062
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->a(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    .line 799063
    invoke-static {v0}, LX/4md;->a(Landroid/app/Dialog;)V

    .line 799064
    return-object v0
.end method

.method public final h()Landroid/app/Activity;
    .locals 2

    .prologue
    .line 799060
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Landroid/app/Activity;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    return-object v0
.end method

.method public final i()V
    .locals 1

    .prologue
    .line 799055
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object v0, v0

    .line 799056
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/0gc;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 799057
    invoke-virtual {p0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 799058
    :goto_0
    return-void

    .line 799059
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/iorg/common/zero/ui/IorgDialogFragment;->l:Z

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x1a3a6643

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 799051
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 799052
    const-class v1, Lcom/facebook/iorg/common/zero/ui/IorgDialogFragment;

    invoke-static {v1, p0}, Lcom/facebook/iorg/common/zero/ui/IorgDialogFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 799053
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/iorg/common/zero/ui/IorgDialogFragment;->l:Z

    .line 799054
    const/16 v1, 0x2b

    const v2, 0xc35c6ff

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x2ea897b6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 799041
    invoke-super {p0}, Landroid/support/v4/app/DialogFragment;->onDestroyView()V

    .line 799042
    iget-object v1, p0, Lcom/facebook/iorg/common/zero/ui/IorgDialogFragment;->k:LX/4g7;

    invoke-direct {p0}, Lcom/facebook/iorg/common/zero/ui/IorgDialogFragment;->j()Lcom/facebook/iorg/common/zero/IorgDialogDisplayContext;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/4g7;->b(Lcom/facebook/iorg/common/zero/IorgDialogDisplayContext;)V

    .line 799043
    const/16 v1, 0x2b

    const v2, 0x40713873

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDismiss(Landroid/content/DialogInterface;)V
    .locals 2

    .prologue
    .line 799048
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 799049
    iget-object v0, p0, Lcom/facebook/iorg/common/zero/ui/IorgDialogFragment;->j:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_0

    .line 799050
    :cond_0
    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x575c9b58

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 799044
    invoke-super {p0}, Landroid/support/v4/app/DialogFragment;->onResume()V

    .line 799045
    iget-boolean v1, p0, Lcom/facebook/iorg/common/zero/ui/IorgDialogFragment;->l:Z

    if-eqz v1, :cond_0

    .line 799046
    invoke-virtual {p0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 799047
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x381f74d1

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
