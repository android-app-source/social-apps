.class public Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final mHybridData:Lcom/facebook/jni/HybridData;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 576390
    const-string v0, "mobileconfig-jni"

    invoke-static {v0}, LX/01L;->a(Ljava/lang/String;)V

    .line 576391
    return-void
.end method

.method private constructor <init>(Lcom/facebook/jni/HybridData;)V
    .locals 0

    .prologue
    .line 576387
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 576388
    iput-object p1, p0, Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;->mHybridData:Lcom/facebook/jni/HybridData;

    .line 576389
    return-void
.end method


# virtual methods
.method public native boolOverrideForParam(J)Z
.end method

.method public native doubleOverrideForParam(J)D
.end method

.method public native hasBoolOverrideForParam(J)Z
.end method

.method public native hasDoubleOverrideForParam(J)Z
.end method

.method public native hasIntOverrideForParam(J)Z
.end method

.method public native hasStringOverrideForParam(J)Z
.end method

.method public native intOverrideForParam(J)J
.end method

.method public native removeOverrideForParam(J)V
.end method

.method public native stringOverrideForParam(J)Ljava/lang/String;
.end method

.method public native updateOverrideForBool(JZ)V
.end method

.method public native updateOverrideForDouble(JD)V
.end method

.method public native updateOverrideForInt(JJ)V
.end method

.method public native updateOverrideForString(JLjava/lang/String;)V
.end method
