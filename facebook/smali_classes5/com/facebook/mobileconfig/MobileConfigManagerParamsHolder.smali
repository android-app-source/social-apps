.class public Lcom/facebook/mobileconfig/MobileConfigManagerParamsHolder;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final mHybridData:Lcom/facebook/jni/HybridData;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 576353
    const-string v0, "mobileconfig-jni"

    invoke-static {v0}, LX/01L;->a(Ljava/lang/String;)V

    .line 576354
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 576350
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 576351
    invoke-static {}, Lcom/facebook/mobileconfig/MobileConfigManagerParamsHolder;->initHybrid()Lcom/facebook/jni/HybridData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/mobileconfig/MobileConfigManagerParamsHolder;->mHybridData:Lcom/facebook/jni/HybridData;

    .line 576352
    return-void
.end method

.method private static native initHybrid()Lcom/facebook/jni/HybridData;
.end method


# virtual methods
.method public native setConsistencyLoggingEnabled(Z)V
.end method

.method public native setConsistencyLoggingEveryNSec(J)V
.end method

.method public native setFullRefreshOnUpgrade(Z)V
.end method

.method public native setOmnistoreUpdaterExpected(Z)V
.end method

.method public native setResponseCompressionEnabled(Z)V
.end method

.method public native setShadowAlternativeUpdater(Z)V
.end method

.method public native setUniverseType(I)V
.end method
