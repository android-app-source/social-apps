.class public final Lcom/facebook/mobileconfig/listener/MobileConfigChangeRegistry$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:[Ljava/lang/String;

.field public final synthetic b:LX/2Yr;


# direct methods
.method public constructor <init>(LX/2Yr;[Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 576548
    iput-object p1, p0, Lcom/facebook/mobileconfig/listener/MobileConfigChangeRegistry$1;->b:LX/2Yr;

    iput-object p2, p0, Lcom/facebook/mobileconfig/listener/MobileConfigChangeRegistry$1;->a:[Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 11

    .prologue
    .line 576549
    iget-object v0, p0, Lcom/facebook/mobileconfig/listener/MobileConfigChangeRegistry$1;->b:LX/2Yr;

    iget-object v1, p0, Lcom/facebook/mobileconfig/listener/MobileConfigChangeRegistry$1;->a:[Ljava/lang/String;

    .line 576550
    invoke-static {}, LX/18f;->c()LX/2Q8;

    move-result-object v3

    .line 576551
    iget-object v2, v0, LX/2Yr;->b:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0ug;

    .line 576552
    invoke-interface {v2}, LX/0ug;->a()I

    move-result v5

    .line 576553
    const/4 v6, -0x1

    if-ne v5, v6, :cond_5

    .line 576554
    const-string v6, "MOBILE_CONFIG_ANY"

    .line 576555
    :goto_1
    move-object v5, v6

    .line 576556
    invoke-virtual {v3, v5, v2}, LX/2Q8;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/2Q8;

    goto :goto_0

    .line 576557
    :cond_0
    invoke-virtual {v3}, LX/2Q8;->b()LX/18f;

    move-result-object v6

    .line 576558
    array-length v7, v1

    const/4 v2, 0x0

    move v5, v2

    :goto_2
    if-ge v5, v7, :cond_4

    aget-object v8, v1, v5

    .line 576559
    invoke-virtual {v6, v8}, LX/18f;->h(Ljava/lang/Object;)LX/0Py;

    move-result-object v2

    invoke-virtual {v2}, LX/0Py;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_3
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0ug;

    .line 576560
    :try_start_0
    invoke-interface {v2}, LX/0ug;->a()I

    move-result v3

    invoke-interface {v2, v3}, LX/0ug;->a(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    .line 576561
    :catch_0
    move-exception v3

    move-object v4, v3

    .line 576562
    iget-object v3, v0, LX/2Yr;->d:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2, v4}, LX/03V;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3

    .line 576563
    :cond_1
    const-string v2, "MOBILE_CONFIG_ANY"

    invoke-virtual {v6, v2}, LX/18f;->h(Ljava/lang/Object;)LX/0Py;

    move-result-object v2

    invoke-virtual {v2}, LX/0Py;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_4
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0ug;

    .line 576564
    :try_start_1
    iget-object v3, v0, LX/2Yr;->f:Ljava/util/Map;

    invoke-interface {v3, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    .line 576565
    if-nez v3, :cond_2

    .line 576566
    iget-object v3, v0, LX/2Yr;->d:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/03V;

    sget-object v4, LX/2Yr;->a:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string p0, "Invalid config name: "

    invoke-direct {v10, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v4, v10}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_4

    .line 576567
    :catch_1
    move-exception v3

    move-object v4, v3

    .line 576568
    iget-object v3, v0, LX/2Yr;->d:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/03V;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v10, ", config: "

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2, v4}, LX/03V;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_4

    .line 576569
    :cond_2
    :try_start_2
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-interface {v2, v3}, LX/0ug;->a(I)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_4

    .line 576570
    :cond_3
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto/16 :goto_2

    .line 576571
    :cond_4
    return-void

    :cond_5
    iget-object v6, v0, LX/2Yr;->e:Ljava/util/Map;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    goto/16 :goto_1
.end method
