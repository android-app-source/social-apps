.class public Lcom/facebook/mobileconfig/MobileConfigManagerHolderImpl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Wx;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private final mHybridData:Lcom/facebook/jni/HybridData;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 576355
    const-string v0, "mobileconfig-jni"

    invoke-static {v0}, LX/01L;->a(Ljava/lang/String;)V

    .line 576356
    return-void
.end method

.method public constructor <init>(Ljava/io/File;Lcom/facebook/common/jniexecutors/AndroidAsyncExecutorFactory;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/tigon/iface/TigonServiceHolder;ZLcom/facebook/xanalytics/XAnalyticsHolder;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/facebook/mobileconfig/MobileConfigParamsMapHolder;Lcom/facebook/mobileconfig/MobileConfigManagerParamsHolder;Ljava/util/Map;)V
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Lcom/facebook/common/jniexecutors/AndroidAsyncExecutorFactory;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/tigon/iface/TigonServiceHolder;",
            "Z",
            "Lcom/facebook/xanalytics/XAnalyticsHolder;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "Lcom/facebook/mobileconfig/MobileConfigParamsMapHolder;",
            "Lcom/facebook/mobileconfig/MobileConfigManagerParamsHolder;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 576357
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 576358
    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/facebook/mobileconfig/MobileConfigManagerHolderImpl;->a:Ljava/lang/String;

    .line 576359
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/mobileconfig/MobileConfigManagerHolderImpl;->b:Ljava/lang/String;

    .line 576360
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    move-object/from16 v5, p5

    move/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    move-object/from16 v14, p14

    invoke-static/range {v1 .. v14}, Lcom/facebook/mobileconfig/MobileConfigManagerHolderImpl;->initHybrid(Lcom/facebook/common/jniexecutors/AndroidAsyncExecutorFactory;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/tigon/iface/TigonServiceHolder;ZLcom/facebook/xanalytics/XAnalyticsHolder;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/facebook/mobileconfig/MobileConfigParamsMapHolder;Lcom/facebook/mobileconfig/MobileConfigManagerParamsHolder;Ljava/util/Map;)Lcom/facebook/jni/HybridData;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/mobileconfig/MobileConfigManagerHolderImpl;->mHybridData:Lcom/facebook/jni/HybridData;

    .line 576361
    return-void
.end method

.method private static native initHybrid(Lcom/facebook/common/jniexecutors/AndroidAsyncExecutorFactory;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/tigon/iface/TigonServiceHolder;ZLcom/facebook/xanalytics/XAnalyticsHolder;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/facebook/mobileconfig/MobileConfigParamsMapHolder;Lcom/facebook/mobileconfig/MobileConfigManagerParamsHolder;Ljava/util/Map;)Lcom/facebook/jni/HybridData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/jniexecutors/AndroidAsyncExecutorFactory;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/tigon/iface/TigonServiceHolder;",
            "Z",
            "Lcom/facebook/xanalytics/XAnalyticsHolder;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "Lcom/facebook/mobileconfig/MobileConfigParamsMapHolder;",
            "Lcom/facebook/mobileconfig/MobileConfigManagerParamsHolder;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/facebook/jni/HybridData;"
        }
    .end annotation
.end method

.method public static native parseParamsMap(Ljava/lang/String;)Z
.end method


# virtual methods
.method public final a()LX/0eT;
    .locals 1

    .prologue
    .line 576362
    invoke-virtual {p0}, Lcom/facebook/mobileconfig/MobileConfigManagerHolderImpl;->getLatestHandleHolder()Lcom/facebook/mobileconfig/MobileConfigMmapHandleHolder;

    move-result-object v0

    return-object v0
.end method

.method public final b()Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;
    .locals 1

    .prologue
    .line 576363
    invoke-virtual {p0}, Lcom/facebook/mobileconfig/MobileConfigManagerHolderImpl;->getNewOverridesTableHolder()Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;
    .locals 1

    .prologue
    .line 576364
    invoke-virtual {p0}, Lcom/facebook/mobileconfig/MobileConfigManagerHolderImpl;->getNewOverridesTableHolderIfExists()Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;

    move-result-object v0

    return-object v0
.end method

.method public native clearAlternativeUpdater()V
.end method

.method public native clearCurrentUserData()V
.end method

.method public native clearOverrides()V
.end method

.method public native deleteOldUserData(I)V
.end method

.method public native getFileOperationErrorString()Ljava/lang/String;
.end method

.method public native getFrameworkStatus()Ljava/lang/String;
.end method

.method public native getLatestHandleHolder()Lcom/facebook/mobileconfig/MobileConfigMmapHandleHolder;
.end method

.method public native getMobileConfigGKInfo()Lcom/facebook/mobileconfig/MobileConfigGKInfoListHolder;
.end method

.method public native getMobileConfigQEInfo()Lcom/facebook/mobileconfig/MobileConfigQEUniverseInfoListHolder;
.end method

.method public native getNewOverridesTableHolder()Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;
.end method

.method public native getNewOverridesTableHolderIfExists()Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;
.end method

.method public native getQEInfoFilename()Ljava/lang/String;
.end method

.method public native getQEJson()Ljava/lang/String;
.end method

.method public native getSchemaString()Ljava/lang/String;
.end method

.method public native isQEInfoAvailable()Z
.end method

.method public native isTigonServiceSet()Z
.end method

.method public native isValid()Z
.end method

.method public native logExposure(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public native logShadowResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public native refreshConfigInfos(I)Z
.end method

.method public native registerConfigChangeListener(Lcom/facebook/mobileconfig/MobileConfigCxxChangeListener;)Z
.end method

.method public native saveQEInfoToDisk(Ljava/lang/String;)V
.end method

.method public native setTigonService(Lcom/facebook/tigon/iface/TigonServiceHolder;Z)V
.end method

.method public native tryUpdateConfigs()Z
.end method

.method public native tryUpdateConfigsSynchronously(I)Z
.end method

.method public native updateConfigs()Z
.end method

.method public native updateConfigsSynchronously(I)Z
.end method
