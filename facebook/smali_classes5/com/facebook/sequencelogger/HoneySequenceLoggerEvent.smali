.class public Lcom/facebook/sequencelogger/HoneySequenceLoggerEvent;
.super Lcom/facebook/analytics/logger/HoneyClientEvent;
.source ""


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;JLX/0lF;LX/0lF;LX/0lF;LX/162;LX/0m9;LX/0m9;LX/0lF;JLjava/lang/Boolean;Z)V
    .locals 2
    .param p5    # LX/0lF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # LX/0lF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # LX/0lF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # LX/162;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p9    # LX/0m9;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p10    # LX/0m9;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p11    # LX/0lF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p14    # Ljava/lang/Boolean;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 638957
    if-nez p1, :cond_0

    const-string p1, "perf_sequence"

    :cond_0
    invoke-direct {p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 638958
    invoke-virtual {p0, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->f(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 638959
    const-string v0, "duration_ms"

    invoke-virtual {p0, v0, p3, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 638960
    if-eqz p5, :cond_1

    .line 638961
    const-string v0, "extra_start_map"

    invoke-virtual {p0, v0, p5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 638962
    :cond_1
    if-eqz p6, :cond_2

    .line 638963
    const-string v0, "extra_stop_map"

    invoke-virtual {p0, v0, p6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 638964
    :cond_2
    if-eqz p7, :cond_3

    .line 638965
    const-string v0, "extra_info_map"

    invoke-virtual {p0, v0, p7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 638966
    :cond_3
    if-eqz p11, :cond_4

    .line 638967
    const-string v0, "events"

    invoke-virtual {p0, v0, p11}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 638968
    :cond_4
    if-eqz p8, :cond_5

    .line 638969
    const-string v0, "errors"

    invoke-virtual {p0, v0, p8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 638970
    :cond_5
    if-eqz p9, :cond_6

    .line 638971
    const-string v0, "gks"

    invoke-virtual {p0, v0, p9}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 638972
    :cond_6
    if-eqz p10, :cond_7

    .line 638973
    const-string v0, "qes"

    invoke-virtual {p0, v0, p10}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 638974
    :cond_7
    if-eqz p14, :cond_8

    .line 638975
    const-string v0, "guess_was_backgrounded"

    invoke-virtual/range {p14 .. p14}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 638976
    :cond_8
    if-eqz p15, :cond_9

    .line 638977
    invoke-static {}, LX/00w;->a()LX/00w;

    move-result-object v0

    invoke-virtual {v0}, LX/00w;->c()Ljava/lang/Boolean;

    move-result-object v0

    .line 638978
    const-string v1, "guess_app_start_bg"

    invoke-direct {p0, v1, v0}, Lcom/facebook/sequencelogger/HoneySequenceLoggerEvent;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 638979
    :cond_9
    const-string v0, "time_since_boot_ms"

    invoke-virtual {p0, v0, p12, p13}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 638980
    sget-boolean v0, LX/008;->deoptTaint:Z

    if-eqz v0, :cond_a

    .line 638981
    const-string v0, "dex_unopt"

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-direct {p0, v0, v1}, Lcom/facebook/sequencelogger/HoneySequenceLoggerEvent;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 638982
    :cond_a
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/Boolean;)V
    .locals 0

    .prologue
    .line 638983
    if-eqz p2, :cond_0

    :goto_0
    invoke-virtual {p0, p1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 638984
    return-void

    .line 638985
    :cond_0
    const-string p2, "Unknown"

    goto :goto_0
.end method
