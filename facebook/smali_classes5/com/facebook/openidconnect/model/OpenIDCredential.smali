.class public Lcom/facebook/openidconnect/model/OpenIDCredential;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/openidconnect/model/OpenIDCredential;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/lang/String;

.field public b:LX/4gy;

.field public c:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 800330
    new-instance v0, LX/4gz;

    invoke-direct {v0}, LX/4gz;-><init>()V

    sput-object v0, Lcom/facebook/openidconnect/model/OpenIDCredential;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 800336
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 800337
    iput-object v0, p0, Lcom/facebook/openidconnect/model/OpenIDCredential;->a:Ljava/lang/String;

    .line 800338
    iput-object v0, p0, Lcom/facebook/openidconnect/model/OpenIDCredential;->b:LX/4gy;

    .line 800339
    iput-object v0, p0, Lcom/facebook/openidconnect/model/OpenIDCredential;->c:Ljava/lang/String;

    .line 800340
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 800331
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 800332
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/openidconnect/model/OpenIDCredential;->a:Ljava/lang/String;

    .line 800333
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/4gy;->valueOf(Ljava/lang/String;)LX/4gy;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/openidconnect/model/OpenIDCredential;->b:LX/4gy;

    .line 800334
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/openidconnect/model/OpenIDCredential;->c:Ljava/lang/String;

    .line 800335
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/4gy;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 800341
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 800342
    iput-object p1, p0, Lcom/facebook/openidconnect/model/OpenIDCredential;->a:Ljava/lang/String;

    .line 800343
    iput-object p2, p0, Lcom/facebook/openidconnect/model/OpenIDCredential;->b:LX/4gy;

    .line 800344
    iput-object p3, p0, Lcom/facebook/openidconnect/model/OpenIDCredential;->c:Ljava/lang/String;

    .line 800345
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 800325
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 800326
    iget-object v0, p0, Lcom/facebook/openidconnect/model/OpenIDCredential;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 800327
    iget-object v0, p0, Lcom/facebook/openidconnect/model/OpenIDCredential;->b:LX/4gy;

    invoke-virtual {v0}, LX/4gy;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 800328
    iget-object v0, p0, Lcom/facebook/openidconnect/model/OpenIDCredential;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 800329
    return-void
.end method
