.class public final Lcom/facebook/android/maps/internal/GrandCentralDispatch$DispatchThread;
.super Ljava/lang/Thread;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 622314
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 1

    .prologue
    .line 622315
    const/16 v0, 0xa

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    .line 622316
    :goto_0
    :try_start_0
    sget-object v0, LX/31l;->b:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->take()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 622317
    invoke-virtual {v0}, Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;->run()V

    .line 622318
    goto :goto_0

    .line 622319
    :catch_0
    goto :goto_0
.end method
