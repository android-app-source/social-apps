.class public final Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x6b2f1710
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel$NativeTemplateBundlesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 631686
    const-class v0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 631685
    const-class v0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 631683
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 631684
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 631680
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 631681
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 631682
    return-void
.end method

.method public static a(Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel;)Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel;
    .locals 11

    .prologue
    .line 631653
    if-nez p0, :cond_0

    .line 631654
    const/4 p0, 0x0

    .line 631655
    :goto_0
    return-object p0

    .line 631656
    :cond_0
    instance-of v0, p0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel;

    if-eqz v0, :cond_1

    .line 631657
    check-cast p0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel;

    goto :goto_0

    .line 631658
    :cond_1
    new-instance v2, LX/5ei;

    invoke-direct {v2}, LX/5ei;-><init>()V

    .line 631659
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 631660
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {p0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 631661
    invoke-virtual {p0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel$NativeTemplateBundlesModel;

    invoke-static {v0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel$NativeTemplateBundlesModel;->a(Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel$NativeTemplateBundlesModel;)Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel$NativeTemplateBundlesModel;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 631662
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 631663
    :cond_2
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v2, LX/5ei;->a:LX/0Px;

    .line 631664
    invoke-virtual {p0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, LX/5ei;->b:Ljava/lang/String;

    .line 631665
    const/4 v8, 0x1

    const/4 v10, 0x0

    const/4 v6, 0x0

    .line 631666
    new-instance v4, LX/186;

    const/16 v5, 0x80

    invoke-direct {v4, v5}, LX/186;-><init>(I)V

    .line 631667
    iget-object v5, v2, LX/5ei;->a:LX/0Px;

    invoke-static {v4, v5}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v5

    .line 631668
    iget-object v7, v2, LX/5ei;->b:Ljava/lang/String;

    invoke-virtual {v4, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 631669
    const/4 v9, 0x2

    invoke-virtual {v4, v9}, LX/186;->c(I)V

    .line 631670
    invoke-virtual {v4, v10, v5}, LX/186;->b(II)V

    .line 631671
    invoke-virtual {v4, v8, v7}, LX/186;->b(II)V

    .line 631672
    invoke-virtual {v4}, LX/186;->d()I

    move-result v5

    .line 631673
    invoke-virtual {v4, v5}, LX/186;->d(I)V

    .line 631674
    invoke-virtual {v4}, LX/186;->e()[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 631675
    invoke-virtual {v5, v10}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 631676
    new-instance v4, LX/15i;

    move-object v7, v6

    move-object v9, v6

    invoke-direct/range {v4 .. v9}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 631677
    new-instance v5, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel;

    invoke-direct {v5, v4}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel;-><init>(LX/15i;)V

    .line 631678
    move-object p0, v5

    .line 631679
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 631645
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 631646
    invoke-virtual {p0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 631647
    invoke-virtual {p0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 631648
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 631649
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 631650
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 631651
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 631652
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel$NativeTemplateBundlesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 631628
    iget-object v0, p0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel$NativeTemplateBundlesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel;->e:Ljava/util/List;

    .line 631629
    iget-object v0, p0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 631637
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 631638
    invoke-virtual {p0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 631639
    invoke-virtual {p0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 631640
    if-eqz v1, :cond_0

    .line 631641
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel;

    .line 631642
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel;->e:Ljava/util/List;

    .line 631643
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 631644
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 631634
    new-instance v0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel;

    invoke-direct {v0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel;-><init>()V

    .line 631635
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 631636
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 631632
    iget-object v0, p0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel;->f:Ljava/lang/String;

    .line 631633
    iget-object v0, p0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 631631
    const v0, 0x16043f61

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 631630
    const v0, -0x748c5bea

    return v0
.end method
