.class public Lcom/facebook/device_id/DefaultPhoneIdRequestReceiver;
.super LX/49c;
.source ""


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/27v;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 674595
    const-class v0, Lcom/facebook/device_id/DefaultPhoneIdRequestReceiver;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/device_id/DefaultPhoneIdRequestReceiver;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 674594
    invoke-direct {p0}, LX/49c;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/device_id/DefaultPhoneIdRequestReceiver;

    const/16 v1, 0x4c6

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/device_id/DefaultPhoneIdRequestReceiver;->a:LX/0Ot;

    return-void
.end method


# virtual methods
.method public final a()LX/282;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 674591
    iget-object v0, p0, Lcom/facebook/device_id/DefaultPhoneIdRequestReceiver;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/27v;

    invoke-virtual {v0}, LX/27v;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 674592
    iget-object v0, p0, Lcom/facebook/device_id/DefaultPhoneIdRequestReceiver;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/27v;

    invoke-virtual {v0}, LX/27v;->b()LX/282;

    move-result-object v0

    .line 674593
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 674587
    iget-object v0, p0, Lcom/facebook/device_id/DefaultPhoneIdRequestReceiver;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/27v;

    invoke-virtual {v0}, LX/27v;->a()Z

    move-result v0

    return v0
.end method

.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x26

    const v1, 0x5250004e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 674588
    invoke-static {p0, p1}, Lcom/facebook/device_id/DefaultPhoneIdRequestReceiver;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 674589
    invoke-super {p0, p1, p2}, LX/49c;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    .line 674590
    const/16 v1, 0x27

    const v2, 0x4da73308    # 3.50642432E8f

    invoke-static {p2, v3, v1, v2, v0}, LX/02F;->a(Landroid/content/Intent;IIII)V

    return-void
.end method
