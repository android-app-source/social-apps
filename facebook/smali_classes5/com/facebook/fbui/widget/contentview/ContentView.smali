.class public Lcom/facebook/fbui/widget/contentview/ContentView;
.super Lcom/facebook/fbui/widget/layout/ImageBlockLayout;
.source ""


# static fields
.field private static final j:[I


# instance fields
.field private k:LX/6VF;

.field private l:I

.field private m:LX/6VD;

.field private n:LX/6VD;

.field private o:LX/6VD;

.field private p:Z

.field private q:Z

.field private r:I

.field private s:I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 575706
    const/4 v0, 0x5

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x7f010201

    aput v2, v0, v1

    const/4 v1, 0x1

    const v2, 0x7f010202

    aput v2, v0, v1

    const/4 v1, 0x2

    const v2, 0x7f010203

    aput v2, v0, v1

    const/4 v1, 0x3

    const v2, 0x7f010204

    aput v2, v0, v1

    const/4 v1, 0x4

    const v2, 0x7f010205

    aput v2, v0, v1

    sput-object v0, Lcom/facebook/fbui/widget/contentview/ContentView;->j:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 575707
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 575708
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 575709
    const v0, 0x7f0101fb

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 575710
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 575711
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 575712
    iput-boolean v1, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->p:Z

    .line 575713
    new-instance v0, LX/6VD;

    invoke-direct {v0, p0}, LX/6VD;-><init>(Lcom/facebook/fbui/widget/contentview/ContentView;)V

    iput-object v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->m:LX/6VD;

    .line 575714
    new-instance v0, LX/6VD;

    invoke-direct {v0, p0}, LX/6VD;-><init>(Lcom/facebook/fbui/widget/contentview/ContentView;)V

    iput-object v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->n:LX/6VD;

    .line 575715
    new-instance v0, LX/6VD;

    invoke-direct {v0, p0}, LX/6VD;-><init>(Lcom/facebook/fbui/widget/contentview/ContentView;)V

    iput-object v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->o:LX/6VD;

    .line 575716
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 575717
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v3

    const v4, 0x7f0101fe

    invoke-virtual {v3, v4, v0, v1}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 575718
    iget-object v3, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->m:LX/6VD;

    iget-object v3, v3, LX/6VD;->b:LX/1nq;

    iget v4, v0, Landroid/util/TypedValue;->resourceId:I

    invoke-static {v3, p1, v4}, LX/5OZ;->a(LX/1nq;Landroid/content/Context;I)V

    .line 575719
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v3

    const v4, 0x7f0101ff

    invoke-virtual {v3, v4, v0, v1}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 575720
    iget-object v3, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->n:LX/6VD;

    iget-object v3, v3, LX/6VD;->b:LX/1nq;

    iget v4, v0, Landroid/util/TypedValue;->resourceId:I

    invoke-static {v3, p1, v4}, LX/5OZ;->a(LX/1nq;Landroid/content/Context;I)V

    .line 575721
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v3

    const v4, 0x7f010200

    invoke-virtual {v3, v4, v0, v1}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 575722
    iget-object v3, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->o:LX/6VD;

    iget-object v3, v3, LX/6VD;->b:LX/1nq;

    iget v0, v0, Landroid/util/TypedValue;->resourceId:I

    invoke-static {v3, p1, v0}, LX/5OZ;->a(LX/1nq;Landroid/content/Context;I)V

    .line 575723
    sget-object v0, LX/03r;->ContentView:[I

    invoke-virtual {p1, p2, v0, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v3

    .line 575724
    const/16 v0, 0x2

    invoke-virtual {v3, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    .line 575725
    if-lez v0, :cond_4

    .line 575726
    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(I)V

    .line 575727
    :goto_0
    const/16 v0, 0x3

    invoke-virtual {v3, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    .line 575728
    if-lez v0, :cond_5

    .line 575729
    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(I)V

    .line 575730
    :goto_1
    const/16 v0, 0x4

    invoke-virtual {v3, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    .line 575731
    if-lez v0, :cond_6

    .line 575732
    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setMetaText(I)V

    .line 575733
    :goto_2
    const/16 v0, 0x0

    invoke-virtual {v3, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    .line 575734
    if-lez v0, :cond_0

    .line 575735
    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleTextAppearance(I)V

    .line 575736
    :cond_0
    const/16 v0, 0x1

    invoke-virtual {v3, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    .line 575737
    if-lez v0, :cond_1

    .line 575738
    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleTextAppearance(I)V

    .line 575739
    :cond_1
    const/16 v0, 0x7

    invoke-virtual {v3, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    .line 575740
    if-lez v0, :cond_2

    .line 575741
    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setMetaTextAppearance(I)V

    .line 575742
    :cond_2
    const/16 v0, 0x5

    invoke-virtual {v3, v0, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v0

    .line 575743
    invoke-static {}, LX/6VF;->values()[LX/6VF;

    move-result-object v4

    aget-object v0, v4, v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setThumbnailSize(LX/6VF;)V

    .line 575744
    const/16 v0, 0x6

    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->p:Z

    .line 575745
    const/16 v0, 0x8

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v4

    .line 575746
    const/16 v0, 0x9

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v5

    .line 575747
    if-ne v4, v5, :cond_7

    move v0, v1

    :goto_3
    const-string v6, "titleMaxLines and subtitleMaxLines must both be specified if one is specified"

    invoke-static {v0, v6}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 575748
    if-eqz v4, :cond_3

    if-eqz v5, :cond_3

    move v2, v1

    :cond_3
    iput-boolean v2, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->q:Z

    .line 575749
    const/16 v0, 0x8

    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->r:I

    .line 575750
    const/16 v0, 0x9

    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->s:I

    .line 575751
    invoke-direct {p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->f()V

    .line 575752
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    .line 575753
    const/16 v0, 0x10

    iput v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->b:I

    .line 575754
    return-void

    .line 575755
    :cond_4
    const/16 v0, 0x2

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 575756
    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 575757
    :cond_5
    const/16 v0, 0x3

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 575758
    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 575759
    :cond_6
    const/16 v0, 0x4

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 575760
    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setMetaText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    :cond_7
    move v0, v2

    .line 575761
    goto :goto_3
.end method

.method private a(LX/6VD;Landroid/view/View;I)I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 575762
    iget-object v0, p1, LX/6VD;->a:Landroid/view/View;

    .line 575763
    if-eqz v0, :cond_0

    .line 575764
    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->indexOfChild(Landroid/view/View;)I

    move-result p3

    .line 575765
    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->removeView(Landroid/view/View;)V

    .line 575766
    :cond_0
    iput-object p2, p1, LX/6VD;->a:Landroid/view/View;

    .line 575767
    iput-object v1, p1, LX/6VD;->b:LX/1nq;

    .line 575768
    iput-object v1, p1, LX/6VD;->c:Landroid/text/Layout;

    .line 575769
    return p3
.end method

.method private a(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 575770
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    instance-of v0, v0, LX/6VC;

    if-eqz v0, :cond_0

    .line 575771
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/6VC;

    .line 575772
    sget-object v1, LX/6VA;->a:[I

    iget-object v0, v0, LX/6VC;->e:LX/6VB;

    invoke-virtual {v0}, LX/6VB;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 575773
    :cond_0
    :goto_0
    return-void

    .line 575774
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->m:LX/6VD;

    iput-object v2, v0, LX/6VD;->a:Landroid/view/View;

    .line 575775
    iget-object v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->m:LX/6VD;

    iput-object v2, v0, LX/6VD;->b:LX/1nq;

    .line 575776
    iget-object v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->m:LX/6VD;

    iput-object v2, v0, LX/6VD;->c:Landroid/text/Layout;

    goto :goto_0

    .line 575777
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->n:LX/6VD;

    iput-object v2, v0, LX/6VD;->a:Landroid/view/View;

    .line 575778
    iget-object v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->n:LX/6VD;

    iput-object v2, v0, LX/6VD;->b:LX/1nq;

    .line 575779
    iget-object v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->n:LX/6VD;

    iput-object v2, v0, LX/6VD;->c:Landroid/text/Layout;

    goto :goto_0

    .line 575780
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->o:LX/6VD;

    iput-object v2, v0, LX/6VD;->a:Landroid/view/View;

    .line 575781
    iget-object v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->o:LX/6VD;

    iput-object v2, v0, LX/6VD;->b:LX/1nq;

    .line 575782
    iget-object v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->o:LX/6VD;

    iput-object v2, v0, LX/6VD;->c:Landroid/text/Layout;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static synthetic a(Lcom/facebook/fbui/widget/contentview/ContentView;)Z
    .locals 1

    .prologue
    .line 575783
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->jx_()Z

    move-result v0

    return v0
.end method

.method private b(Landroid/util/AttributeSet;)LX/6VC;
    .locals 2

    .prologue
    .line 575784
    new-instance v0, LX/6VC;

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, LX/6VC;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method private static b(Landroid/view/ViewGroup$LayoutParams;)LX/6VC;
    .locals 1

    .prologue
    .line 575785
    instance-of v0, p0, LX/6VC;

    if-eqz v0, :cond_0

    .line 575786
    check-cast p0, LX/6VC;

    .line 575787
    :goto_0
    return-object p0

    :cond_0
    invoke-static {}, Lcom/facebook/fbui/widget/contentview/ContentView;->e()LX/6VC;

    move-result-object p0

    goto :goto_0
.end method

.method public static synthetic b(Lcom/facebook/fbui/widget/contentview/ContentView;)Z
    .locals 1

    .prologue
    .line 575788
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->jx_()Z

    move-result v0

    return v0
.end method

.method private static e()LX/6VC;
    .locals 2

    .prologue
    const/4 v1, -0x2

    .line 575789
    new-instance v0, LX/6VC;

    invoke-direct {v0, v1, v1}, LX/6VC;-><init>(II)V

    return-object v0
.end method

.method private f()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 575790
    iget v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->r:I

    if-ltz v0, :cond_2

    iget v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->s:I

    if-ltz v0, :cond_2

    move v0, v1

    :goto_0
    const-string v3, "titleMaxLines and subtitleMaxLines must be non-negative"

    invoke-static {v0, v3}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 575791
    iget-boolean v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->p:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->q:Z

    if-nez v0, :cond_1

    :cond_0
    move v2, v1

    :cond_1
    const-string v0, "maxLinesFromThumbnailSize must be false if titleMaxLines and subtitleMaxLines are specified"

    invoke-static {v2, v0}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 575792
    return-void

    :cond_2
    move v0, v2

    .line 575793
    goto :goto_0
.end method

.method private setSubtitleMaxLines(I)V
    .locals 1

    .prologue
    .line 575794
    iput p1, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->s:I

    .line 575795
    iget-object v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->n:LX/6VD;

    invoke-virtual {v0, p1}, LX/6VD;->c(I)V

    .line 575796
    return-void
.end method

.method private setTitleMaxLines(I)V
    .locals 1

    .prologue
    .line 575797
    iput p1, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->r:I

    .line 575798
    iget-object v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->m:LX/6VD;

    invoke-virtual {v0, p1}, LX/6VD;->c(I)V

    .line 575799
    return-void
.end method


# virtual methods
.method public final synthetic a(Landroid/util/AttributeSet;)LX/1au;
    .locals 1

    .prologue
    .line 575800
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/contentview/ContentView;->b(Landroid/util/AttributeSet;)LX/6VC;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Landroid/view/ViewGroup$LayoutParams;)LX/1au;
    .locals 1

    .prologue
    .line 575853
    invoke-static {p1}, Lcom/facebook/fbui/widget/contentview/ContentView;->b(Landroid/view/ViewGroup$LayoutParams;)LX/6VC;

    move-result-object v0

    return-object v0
.end method

.method public final a(II)V
    .locals 4

    .prologue
    .line 575801
    iget-object v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->m:LX/6VD;

    invoke-virtual {v0, p1, p2}, LX/6VD;->a(II)V

    .line 575802
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->m:LX/6VD;

    invoke-virtual {v1}, LX/6VD;->d()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 575803
    iget-object v1, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->m:LX/6VD;

    invoke-virtual {v1}, LX/6VD;->g()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->m:LX/6VD;

    invoke-virtual {v2}, LX/6VD;->e()I

    move-result v2

    add-int/2addr v1, v2

    add-int/lit8 v1, v1, 0x0

    .line 575804
    iget-object v2, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->n:LX/6VD;

    invoke-virtual {v2, p1, p2}, LX/6VD;->a(II)V

    .line 575805
    iget-object v2, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->n:LX/6VD;

    invoke-virtual {v2}, LX/6VD;->d()I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 575806
    iget-object v2, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->n:LX/6VD;

    invoke-virtual {v2}, LX/6VD;->g()I

    move-result v2

    iget-object v3, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->n:LX/6VD;

    invoke-virtual {v3}, LX/6VD;->e()I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    .line 575807
    iget-object v2, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->o:LX/6VD;

    invoke-virtual {v2, p1, p2}, LX/6VD;->a(II)V

    .line 575808
    iget-object v2, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->o:LX/6VD;

    invoke-virtual {v2}, LX/6VD;->d()I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 575809
    iget-object v2, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->o:LX/6VD;

    invoke-virtual {v2}, LX/6VD;->g()I

    move-result v2

    iget-object v3, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->o:LX/6VD;

    invoke-virtual {v3}, LX/6VD;->e()I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    .line 575810
    invoke-virtual {p0, v0, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->b(II)V

    .line 575811
    return-void
.end method

.method public final a(IIII)V
    .locals 3

    .prologue
    .line 575847
    iget-object v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->m:LX/6VD;

    iget-object v1, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->m:LX/6VD;

    invoke-virtual {v1}, LX/6VD;->f()I

    move-result v1

    add-int/2addr v1, p2

    invoke-virtual {v0, p1, v1, p3}, LX/6VD;->a(III)V

    .line 575848
    iget-object v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->m:LX/6VD;

    invoke-virtual {v0}, LX/6VD;->g()I

    move-result v0

    iget-object v1, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->m:LX/6VD;

    invoke-virtual {v1}, LX/6VD;->e()I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v0, p2

    .line 575849
    iget-object v1, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->n:LX/6VD;

    iget-object v2, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->n:LX/6VD;

    invoke-virtual {v2}, LX/6VD;->f()I

    move-result v2

    add-int/2addr v2, v0

    invoke-virtual {v1, p1, v2, p3}, LX/6VD;->a(III)V

    .line 575850
    iget-object v1, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->n:LX/6VD;

    invoke-virtual {v1}, LX/6VD;->g()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->n:LX/6VD;

    invoke-virtual {v2}, LX/6VD;->e()I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 575851
    iget-object v1, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->o:LX/6VD;

    iget-object v2, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->o:LX/6VD;

    invoke-virtual {v2}, LX/6VD;->f()I

    move-result v2

    add-int/2addr v0, v2

    invoke-virtual {v1, p1, v0, p3}, LX/6VD;->a(III)V

    .line 575852
    return-void
.end method

.method public final addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .locals 2

    .prologue
    .line 575837
    instance-of v0, p3, LX/6VC;

    if-eqz v0, :cond_0

    move-object v0, p3

    .line 575838
    check-cast v0, LX/6VC;

    .line 575839
    sget-object v1, LX/6VA;->a:[I

    iget-object v0, v0, LX/6VC;->e:LX/6VB;

    invoke-virtual {v0}, LX/6VB;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 575840
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 575841
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->f:Landroid/view/View;

    if-ne p1, v0, :cond_1

    .line 575842
    iget v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->l:I

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailSize(I)V

    .line 575843
    :cond_1
    return-void

    .line 575844
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->m:LX/6VD;

    invoke-direct {p0, v0, p1, p2}, Lcom/facebook/fbui/widget/contentview/ContentView;->a(LX/6VD;Landroid/view/View;I)I

    move-result p2

    goto :goto_0

    .line 575845
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->n:LX/6VD;

    invoke-direct {p0, v0, p1, p2}, Lcom/facebook/fbui/widget/contentview/ContentView;->a(LX/6VD;Landroid/view/View;I)I

    move-result p2

    goto :goto_0

    .line 575846
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->o:LX/6VD;

    invoke-direct {p0, v0, p1, p2}, Lcom/facebook/fbui/widget/contentview/ContentView;->a(LX/6VD;Landroid/view/View;I)I

    move-result p2

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final synthetic b()LX/1au;
    .locals 1

    .prologue
    .line 575836
    invoke-static {}, Lcom/facebook/fbui/widget/contentview/ContentView;->e()LX/6VC;

    move-result-object v0

    return-object v0
.end method

.method public final checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1

    .prologue
    .line 575835
    if-eqz p1, :cond_0

    instance-of v0, p1, LX/6VC;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 575830
    const/4 v0, 0x1

    iput v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->s:I

    iput v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->r:I

    .line 575831
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->q:Z

    .line 575832
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->requestLayout()V

    .line 575833
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->invalidate()V

    .line 575834
    return-void
.end method

.method public dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 575854
    invoke-super {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 575855
    iget-object v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->m:LX/6VD;

    invoke-virtual {v0, p1}, LX/6VD;->a(Landroid/graphics/Canvas;)V

    .line 575856
    iget-object v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->n:LX/6VD;

    invoke-virtual {v0, p1}, LX/6VD;->a(Landroid/graphics/Canvas;)V

    .line 575857
    iget-object v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->o:LX/6VD;

    invoke-virtual {v0, p1}, LX/6VD;->a(Landroid/graphics/Canvas;)V

    .line 575858
    return-void
.end method

.method public final e(II)V
    .locals 1

    .prologue
    .line 575822
    iget-boolean v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->q:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->r:I

    if-ne v0, p1, :cond_0

    iget v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->s:I

    if-eq v0, p2, :cond_1

    .line 575823
    :cond_0
    iput p1, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->r:I

    .line 575824
    iput p2, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->s:I

    .line 575825
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->q:Z

    .line 575826
    invoke-direct {p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->f()V

    .line 575827
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->requestLayout()V

    .line 575828
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->invalidate()V

    .line 575829
    :cond_1
    return-void
.end method

.method public final synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 575821
    invoke-static {}, Lcom/facebook/fbui/widget/contentview/ContentView;->e()LX/6VC;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 575820
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/contentview/ContentView;->b(Landroid/util/AttributeSet;)LX/6VC;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 575574
    invoke-static {p1}, Lcom/facebook/fbui/widget/contentview/ContentView;->b(Landroid/view/ViewGroup$LayoutParams;)LX/6VC;

    move-result-object v0

    return-object v0
.end method

.method public getFeatureTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 575819
    const-string v0, "thumbnail"

    return-object v0
.end method

.method public getMetaLayout()Landroid/text/Layout;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 575814
    sget-object v1, LX/6VA;->b:[I

    iget-object v2, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->o:LX/6VD;

    invoke-virtual {v2}, LX/6VD;->a()LX/6VE;

    move-result-object v2

    invoke-virtual {v2}, LX/6VE;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 575815
    :cond_0
    :goto_0
    return-object v0

    .line 575816
    :pswitch_0
    iget-object v1, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->o:LX/6VD;

    iget-object v1, v1, LX/6VD;->a:Landroid/view/View;

    instance-of v1, v1, Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 575817
    iget-object v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->o:LX/6VD;

    iget-object v0, v0, LX/6VD;->a:Landroid/view/View;

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v0

    goto :goto_0

    .line 575818
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->o:LX/6VD;

    iget-object v0, v0, LX/6VD;->c:Landroid/text/Layout;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getMetaText()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 575813
    iget-object v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->o:LX/6VD;

    invoke-virtual {v0}, LX/6VD;->b()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getMetaTextSize()F
    .locals 1

    .prologue
    .line 575705
    iget-object v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->o:LX/6VD;

    invoke-virtual {v0}, LX/6VD;->c()F

    move-result v0

    return v0
.end method

.method public getMetaView()Landroid/view/View;
    .locals 1

    .prologue
    .line 575812
    iget-object v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->o:LX/6VD;

    iget-object v0, v0, LX/6VD;->a:Landroid/view/View;

    return-object v0
.end method

.method public getSubitleLayout()Landroid/text/Layout;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 575578
    sget-object v1, LX/6VA;->b:[I

    iget-object v2, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->n:LX/6VD;

    invoke-virtual {v2}, LX/6VD;->a()LX/6VE;

    move-result-object v2

    invoke-virtual {v2}, LX/6VE;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 575579
    :cond_0
    :goto_0
    return-object v0

    .line 575580
    :pswitch_0
    iget-object v1, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->n:LX/6VD;

    iget-object v1, v1, LX/6VD;->a:Landroid/view/View;

    instance-of v1, v1, Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 575581
    iget-object v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->n:LX/6VD;

    iget-object v0, v0, LX/6VD;->a:Landroid/view/View;

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v0

    goto :goto_0

    .line 575582
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->n:LX/6VD;

    iget-object v0, v0, LX/6VD;->c:Landroid/text/Layout;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getSubtitleMaxLines()I
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 575575
    iget v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->s:I

    return v0
.end method

.method public getSubtitleText()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 575576
    iget-object v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->n:LX/6VD;

    invoke-virtual {v0}, LX/6VD;->b()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getSubtitleTextSize()F
    .locals 1

    .prologue
    .line 575577
    iget-object v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->n:LX/6VD;

    invoke-virtual {v0}, LX/6VD;->c()F

    move-result v0

    return v0
.end method

.method public getSubtitleView()Landroid/view/View;
    .locals 1

    .prologue
    .line 575583
    iget-object v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->n:LX/6VD;

    iget-object v0, v0, LX/6VD;->a:Landroid/view/View;

    return-object v0
.end method

.method public final getThumbnailDimensionSize()I
    .locals 4

    .prologue
    .line 575584
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 575585
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    sget-object v2, Lcom/facebook/fbui/widget/contentview/ContentView;->j:[I

    iget-object v3, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->k:LX/6VF;

    invoke-virtual {v3}, LX/6VF;->ordinal()I

    move-result v3

    aget v2, v2, v3

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 575586
    iget v1, v0, Landroid/util/TypedValue;->resourceId:I

    if-nez v1, :cond_0

    .line 575587
    const v0, 0x7f0b008b

    .line 575588
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0

    .line 575589
    :cond_0
    iget v0, v0, Landroid/util/TypedValue;->resourceId:I

    goto :goto_0
.end method

.method public getThumbnailSize()LX/6VF;
    .locals 1

    .prologue
    .line 575590
    iget-object v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->k:LX/6VF;

    return-object v0
.end method

.method public getTitleLayout()Landroid/text/Layout;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 575591
    sget-object v1, LX/6VA;->b:[I

    iget-object v2, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->m:LX/6VD;

    invoke-virtual {v2}, LX/6VD;->a()LX/6VE;

    move-result-object v2

    invoke-virtual {v2}, LX/6VE;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 575592
    :cond_0
    :goto_0
    return-object v0

    .line 575593
    :pswitch_0
    iget-object v1, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->m:LX/6VD;

    iget-object v1, v1, LX/6VD;->a:Landroid/view/View;

    instance-of v1, v1, Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 575594
    iget-object v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->m:LX/6VD;

    iget-object v0, v0, LX/6VD;->a:Landroid/view/View;

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v0

    goto :goto_0

    .line 575595
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->m:LX/6VD;

    iget-object v0, v0, LX/6VD;->c:Landroid/text/Layout;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getTitleMaxLines()I
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 575596
    iget v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->r:I

    return v0
.end method

.method public getTitleText()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 575597
    iget-object v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->m:LX/6VD;

    invoke-virtual {v0}, LX/6VD;->b()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getTitleTextSize()F
    .locals 1

    .prologue
    .line 575598
    iget-object v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->m:LX/6VD;

    invoke-virtual {v0}, LX/6VD;->c()F

    move-result v0

    return v0
.end method

.method public getTitleView()Landroid/view/View;
    .locals 1

    .prologue
    .line 575599
    iget-object v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->m:LX/6VD;

    iget-object v0, v0, LX/6VD;->a:Landroid/view/View;

    return-object v0
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 575600
    invoke-super {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 575601
    const-class v0, Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    .line 575602
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 575603
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 575604
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 575605
    :cond_0
    iget-object v1, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->m:LX/6VD;

    invoke-virtual {v1}, LX/6VD;->b()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v0, v1, v2}, LX/0wL;->a(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;Z)V

    .line 575606
    iget-object v1, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->n:LX/6VD;

    invoke-virtual {v1}, LX/6VD;->b()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v0, v1, v2}, LX/0wL;->a(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;Z)V

    .line 575607
    iget-object v1, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->o:LX/6VD;

    invoke-virtual {v1}, LX/6VD;->b()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v0, v1, v2}, LX/0wL;->a(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;Z)V

    .line 575608
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 575609
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 575610
    :cond_1
    return-void
.end method

.method public onMeasure(II)V
    .locals 9

    .prologue
    const/16 v5, 0x8

    const/4 v0, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 575611
    iget-boolean v3, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->p:Z

    if-nez v3, :cond_0

    iget-boolean v3, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->q:Z

    if-eqz v3, :cond_1

    .line 575612
    :cond_0
    iget-object v3, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->m:LX/6VD;

    invoke-virtual {v3}, LX/6VD;->h()Z

    move-result v6

    .line 575613
    iget-object v3, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->n:LX/6VD;

    invoke-virtual {v3}, LX/6VD;->h()Z

    move-result v4

    .line 575614
    iget-object v3, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->o:LX/6VD;

    invoke-virtual {v3}, LX/6VD;->h()Z

    move-result v3

    .line 575615
    iget-boolean v7, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->p:Z

    if-eqz v7, :cond_7

    .line 575616
    iget-object v7, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->k:LX/6VF;

    sget-object v8, LX/6VF;->LARGE:LX/6VF;

    if-ne v7, v8, :cond_3

    .line 575617
    if-eqz v3, :cond_2

    .line 575618
    invoke-direct {p0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleMaxLines(I)V

    .line 575619
    :goto_0
    invoke-direct {p0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleMaxLines(I)V

    move v0, v3

    move v3, v4

    .line 575620
    :goto_1
    iget-object v4, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->o:LX/6VD;

    invoke-virtual {v4, v1}, LX/6VD;->c(I)V

    .line 575621
    iget-object v4, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->m:LX/6VD;

    if-eqz v6, :cond_8

    move v1, v2

    :goto_2
    invoke-virtual {v4, v1}, LX/6VD;->b(I)V

    .line 575622
    iget-object v4, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->n:LX/6VD;

    if-eqz v3, :cond_9

    move v1, v2

    :goto_3
    invoke-virtual {v4, v1}, LX/6VD;->b(I)V

    .line 575623
    iget-object v1, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->o:LX/6VD;

    if-eqz v0, :cond_a

    :goto_4
    invoke-virtual {v1, v2}, LX/6VD;->b(I)V

    .line 575624
    :cond_1
    invoke-super {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->onMeasure(II)V

    .line 575625
    return-void

    .line 575626
    :cond_2
    invoke-direct {p0, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleMaxLines(I)V

    goto :goto_0

    .line 575627
    :cond_3
    iget-object v7, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->k:LX/6VF;

    sget-object v8, LX/6VF;->XLARGE:LX/6VF;

    if-ne v7, v8, :cond_5

    .line 575628
    invoke-direct {p0, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleMaxLines(I)V

    .line 575629
    if-eqz v3, :cond_4

    move v0, v1

    :cond_4
    invoke-direct {p0, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleMaxLines(I)V

    move v0, v3

    move v3, v4

    goto :goto_1

    .line 575630
    :cond_5
    invoke-direct {p0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleMaxLines(I)V

    .line 575631
    invoke-direct {p0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleMaxLines(I)V

    .line 575632
    iget-object v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->k:LX/6VF;

    sget-object v3, LX/6VF;->MEDIUM:LX/6VF;

    if-ne v0, v3, :cond_6

    move v0, v1

    :goto_5
    move v3, v0

    move v0, v2

    goto :goto_1

    :cond_6
    move v0, v2

    goto :goto_5

    .line 575633
    :cond_7
    iget v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->r:I

    invoke-direct {p0, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleMaxLines(I)V

    .line 575634
    iget v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->s:I

    invoke-direct {p0, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleMaxLines(I)V

    move v0, v3

    move v3, v4

    goto :goto_1

    :cond_8
    move v1, v5

    .line 575635
    goto :goto_2

    :cond_9
    move v1, v5

    .line 575636
    goto :goto_3

    :cond_a
    move v2, v5

    .line 575637
    goto :goto_4
.end method

.method public final removeView(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 575638
    if-nez p1, :cond_0

    .line 575639
    :goto_0
    return-void

    .line 575640
    :cond_0
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/contentview/ContentView;->a(Landroid/view/View;)V

    .line 575641
    invoke-super {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->removeView(Landroid/view/View;)V

    goto :goto_0
.end method

.method public final removeViewInLayout(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 575642
    if-nez p1, :cond_0

    .line 575643
    :goto_0
    return-void

    .line 575644
    :cond_0
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/contentview/ContentView;->a(Landroid/view/View;)V

    .line 575645
    invoke-super {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->removeViewInLayout(Landroid/view/View;)V

    goto :goto_0
.end method

.method public setMaxLinesFromThumbnailSize(Z)V
    .locals 1

    .prologue
    .line 575646
    iget-boolean v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->p:Z

    if-eq v0, p1, :cond_0

    .line 575647
    iput-boolean p1, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->p:Z

    .line 575648
    invoke-direct {p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->f()V

    .line 575649
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->requestLayout()V

    .line 575650
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->invalidate()V

    .line 575651
    :cond_0
    return-void
.end method

.method public setMetaText(I)V
    .locals 2

    .prologue
    .line 575652
    iget-object v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->o:LX/6VD;

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/6VD;->a(Ljava/lang/CharSequence;)V

    .line 575653
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->requestLayout()V

    .line 575654
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->invalidate()V

    .line 575655
    return-void
.end method

.method public setMetaText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 575656
    iget-object v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->o:LX/6VD;

    invoke-virtual {v0, p1}, LX/6VD;->a(Ljava/lang/CharSequence;)V

    .line 575657
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->requestLayout()V

    .line 575658
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->invalidate()V

    .line 575659
    return-void
.end method

.method public setMetaTextAppearance(I)V
    .locals 1

    .prologue
    .line 575660
    iget-object v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->o:LX/6VD;

    invoke-virtual {v0, p1}, LX/6VD;->d(I)V

    .line 575661
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->requestLayout()V

    .line 575662
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->invalidate()V

    .line 575663
    return-void
.end method

.method public setSubtitleGravity(I)V
    .locals 1

    .prologue
    .line 575664
    iget-object v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->n:LX/6VD;

    invoke-virtual {v0, p1}, LX/6VD;->a(I)V

    .line 575665
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->requestLayout()V

    .line 575666
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->invalidate()V

    .line 575667
    return-void
.end method

.method public setSubtitleText(I)V
    .locals 2

    .prologue
    .line 575668
    iget-object v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->n:LX/6VD;

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/6VD;->a(Ljava/lang/CharSequence;)V

    .line 575669
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->requestLayout()V

    .line 575670
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->invalidate()V

    .line 575671
    return-void
.end method

.method public setSubtitleText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 575672
    iget-object v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->n:LX/6VD;

    invoke-virtual {v0, p1}, LX/6VD;->a(Ljava/lang/CharSequence;)V

    .line 575673
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->requestLayout()V

    .line 575674
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->invalidate()V

    .line 575675
    return-void
.end method

.method public setSubtitleTextAppearance(I)V
    .locals 1

    .prologue
    .line 575676
    iget-object v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->n:LX/6VD;

    invoke-virtual {v0, p1}, LX/6VD;->d(I)V

    .line 575677
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->requestLayout()V

    .line 575678
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->invalidate()V

    .line 575679
    return-void
.end method

.method public setThumbnailDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 575680
    iget v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->l:I

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailSize(I)V

    .line 575681
    invoke-super {p0, p1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 575682
    return-void
.end method

.method public setThumbnailSize(LX/6VF;)V
    .locals 1

    .prologue
    .line 575683
    iget-object v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->k:LX/6VF;

    if-eq p1, v0, :cond_0

    .line 575684
    iput-object p1, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->k:LX/6VF;

    .line 575685
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->getThumbnailDimensionSize()I

    move-result v0

    iput v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->l:I

    .line 575686
    iget v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->l:I

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailSize(I)V

    .line 575687
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->requestLayout()V

    .line 575688
    :cond_0
    return-void
.end method

.method public setTitleGravity(I)V
    .locals 1

    .prologue
    .line 575689
    iget-object v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->m:LX/6VD;

    invoke-virtual {v0, p1}, LX/6VD;->a(I)V

    .line 575690
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->requestLayout()V

    .line 575691
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->invalidate()V

    .line 575692
    return-void
.end method

.method public setTitleText(I)V
    .locals 2

    .prologue
    .line 575693
    iget-object v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->m:LX/6VD;

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/6VD;->a(Ljava/lang/CharSequence;)V

    .line 575694
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->requestLayout()V

    .line 575695
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->invalidate()V

    .line 575696
    return-void
.end method

.method public setTitleText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 575697
    iget-object v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->m:LX/6VD;

    invoke-virtual {v0, p1}, LX/6VD;->a(Ljava/lang/CharSequence;)V

    .line 575698
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->requestLayout()V

    .line 575699
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->invalidate()V

    .line 575700
    return-void
.end method

.method public setTitleTextAppearance(I)V
    .locals 1

    .prologue
    .line 575701
    iget-object v0, p0, Lcom/facebook/fbui/widget/contentview/ContentView;->m:LX/6VD;

    invoke-virtual {v0, p1}, LX/6VD;->d(I)V

    .line 575702
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->requestLayout()V

    .line 575703
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->invalidate()V

    .line 575704
    return-void
.end method
