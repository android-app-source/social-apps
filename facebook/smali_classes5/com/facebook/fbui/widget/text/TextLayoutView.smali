.class public Lcom/facebook/fbui/widget/text/TextLayoutView;
.super Landroid/view/View;
.source ""


# instance fields
.field public a:Landroid/text/Layout;

.field private b:LX/5Of;

.field private c:I

.field private d:I

.field private e:LX/34L;

.field private f:Landroid/graphics/Paint;

.field private g:I

.field private h:I

.field private i:Landroid/graphics/Path;

.field private j:Z

.field private k:Z

.field private l:LX/2nR;

.field private m:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 575992
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/fbui/widget/text/TextLayoutView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 575993
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 575986
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 575987
    iput-boolean v1, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->j:Z

    .line 575988
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->k:Z

    .line 575989
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    float-to-int v0, v0

    iput v0, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->m:I

    .line 575990
    invoke-virtual {p0, v1}, Lcom/facebook/fbui/widget/text/TextLayoutView;->setFocusableInTouchMode(Z)V

    .line 575991
    return-void
.end method

.method private a(Landroid/view/MotionEvent;)Landroid/text/style/ClickableSpan;
    .locals 4

    .prologue
    .line 575971
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    .line 575972
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    .line 575973
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/TextLayoutView;->getPaddingLeft()I

    move-result v2

    sub-int/2addr v0, v2

    .line 575974
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/TextLayoutView;->getPaddingTop()I

    move-result v2

    sub-int/2addr v1, v2

    .line 575975
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/TextLayoutView;->getScrollX()I

    move-result v2

    add-int/2addr v0, v2

    .line 575976
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/TextLayoutView;->getScrollY()I

    move-result v2

    add-int/2addr v1, v2

    .line 575977
    iget-object v2, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->a:Landroid/text/Layout;

    invoke-virtual {v2, v1}, Landroid/text/Layout;->getLineForVertical(I)I

    move-result v1

    .line 575978
    iget-object v2, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->a:Landroid/text/Layout;

    invoke-virtual {v2, v1}, Landroid/text/Layout;->getLineLeft(I)F

    move-result v2

    int-to-float v3, v0

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_0

    int-to-float v2, v0

    iget-object v3, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->a:Landroid/text/Layout;

    invoke-virtual {v3, v1}, Landroid/text/Layout;->getLineRight(I)F

    move-result v3

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_0

    .line 575979
    iget-object v2, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->a:Landroid/text/Layout;

    int-to-float v0, v0

    invoke-virtual {v2, v1, v0}, Landroid/text/Layout;->getOffsetForHorizontal(IF)I

    move-result v1

    .line 575980
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->a:Landroid/text/Layout;

    invoke-virtual {v0}, Landroid/text/Layout;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 575981
    instance-of v2, v0, Landroid/text/Spanned;

    if-eqz v2, :cond_0

    .line 575982
    check-cast v0, Landroid/text/Spanned;

    const-class v2, Landroid/text/style/ClickableSpan;

    invoke-interface {v0, v1, v1, v2}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/ClickableSpan;

    .line 575983
    array-length v1, v0

    if-eqz v1, :cond_0

    .line 575984
    const/4 v1, 0x0

    aget-object v0, v0, v1

    .line 575985
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(II)V
    .locals 4

    .prologue
    .line 575959
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->a:Landroid/text/Layout;

    if-nez v0, :cond_1

    .line 575960
    :cond_0
    :goto_0
    return-void

    .line 575961
    :cond_1
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 575962
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/TextLayoutView;->getPaddingLeft()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/TextLayoutView;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    .line 575963
    iget-object v1, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->a:Landroid/text/Layout;

    invoke-virtual {v1}, Landroid/text/Layout;->getWidth()I

    move-result v1

    .line 575964
    iget v2, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->m:I

    add-int/2addr v2, v1

    if-gt v0, v2, :cond_2

    iget v2, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->m:I

    sub-int v2, v1, v2

    if-ge v0, v2, :cond_0

    .line 575965
    :cond_2
    iget v2, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->c:I

    if-ne v2, p1, :cond_3

    iget v2, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->d:I

    if-eq v2, p2, :cond_0

    .line 575966
    :cond_3
    iget-object v2, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->e:LX/34L;

    if-eqz v2, :cond_4

    .line 575967
    iget-object v2, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->e:LX/34L;

    iget-object v3, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->a:Landroid/text/Layout;

    invoke-virtual {v3}, Landroid/text/Layout;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v2, v3, v1, v0}, LX/34L;->a(Ljava/lang/CharSequence;II)V

    .line 575968
    :cond_4
    iget-object v1, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->b:LX/5Of;

    invoke-interface {v1, v0}, LX/5Of;->a(I)Landroid/text/Layout;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->a:Landroid/text/Layout;

    .line 575969
    iput p1, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->c:I

    .line 575970
    iput p2, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->d:I

    goto :goto_0
.end method

.method private b(II)V
    .locals 1

    .prologue
    .line 575953
    iget v0, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->g:I

    if-ne v0, p1, :cond_0

    iget v0, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->h:I

    if-ne v0, p2, :cond_0

    .line 575954
    :goto_0
    return-void

    .line 575955
    :cond_0
    iput p1, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->g:I

    .line 575956
    iput p2, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->h:I

    .line 575957
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->j:Z

    .line 575958
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/TextLayoutView;->invalidate()V

    goto :goto_0
.end method

.method private getSelectionPath()Landroid/graphics/Path;
    .locals 4

    .prologue
    .line 575944
    iget v0, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->g:I

    iget v1, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->h:I

    if-ne v0, v1, :cond_0

    .line 575945
    const/4 v0, 0x0

    .line 575946
    :goto_0
    return-object v0

    .line 575947
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->j:Z

    if-eqz v0, :cond_2

    .line 575948
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->i:Landroid/graphics/Path;

    if-nez v0, :cond_1

    .line 575949
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->i:Landroid/graphics/Path;

    .line 575950
    :cond_1
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->a:Landroid/text/Layout;

    iget v1, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->g:I

    iget v2, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->h:I

    iget-object v3, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->i:Landroid/graphics/Path;

    invoke-virtual {v0, v1, v2, v3}, Landroid/text/Layout;->getSelectionPath(IILandroid/graphics/Path;)V

    .line 575951
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->j:Z

    .line 575952
    :cond_2
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->i:Landroid/graphics/Path;

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/text/Layout;LX/5Of;LX/34L;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 575935
    iput-object p1, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->a:Landroid/text/Layout;

    .line 575936
    iput-object p2, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->b:LX/5Of;

    .line 575937
    iput v0, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->c:I

    .line 575938
    iput v0, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->d:I

    .line 575939
    iput-object p3, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->e:LX/34L;

    .line 575940
    if-eqz p1, :cond_0

    .line 575941
    invoke-virtual {p1}, Landroid/text/Layout;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/text/TextLayoutView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 575942
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/TextLayoutView;->requestLayout()V

    .line 575943
    return-void
.end method

.method public getHighlightColor()I
    .locals 1

    .prologue
    .line 575932
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->f:Landroid/graphics/Paint;

    if-nez v0, :cond_0

    .line 575933
    const/4 v0, 0x0

    .line 575934
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->f:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    goto :goto_0
.end method

.method public getLayout()Landroid/text/Layout;
    .locals 1

    .prologue
    .line 575931
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->a:Landroid/text/Layout;

    return-object v0
.end method

.method public getText()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 575930
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->a:Landroid/text/Layout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->a:Landroid/text/Layout;

    invoke-virtual {v0}, Landroid/text/Layout;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x7dfcbf06

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 575994
    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    .line 575995
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->k:Z

    .line 575996
    iget-object v1, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->l:LX/2nR;

    if-eqz v1, :cond_0

    .line 575997
    iget-object v1, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->l:LX/2nR;

    invoke-interface {v1, p0}, LX/2nR;->a(Landroid/view/View;)V

    .line 575998
    :cond_0
    const/16 v1, 0x2d

    const v2, 0x55dfef86

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x5372236d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 575859
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->k:Z

    .line 575860
    iget-object v1, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->l:LX/2nR;

    if-eqz v1, :cond_0

    .line 575861
    iget-object v1, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->l:LX/2nR;

    invoke-interface {v1, p0}, LX/2nR;->b(Landroid/view/View;)V

    .line 575862
    :cond_0
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 575863
    const/16 v1, 0x2d

    const v2, -0x4c775f37

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 575864
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 575865
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->a:Landroid/text/Layout;

    if-nez v0, :cond_0

    .line 575866
    :goto_0
    return-void

    .line 575867
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/TextLayoutView;->getPaddingLeft()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/TextLayoutView;->getPaddingTop()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 575868
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->f:Landroid/graphics/Paint;

    if-nez v0, :cond_1

    .line 575869
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->a:Landroid/text/Layout;

    invoke-virtual {v0, p1}, Landroid/text/Layout;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0

    .line 575870
    :cond_1
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->a:Landroid/text/Layout;

    invoke-direct {p0}, Lcom/facebook/fbui/widget/text/TextLayoutView;->getSelectionPath()Landroid/graphics/Path;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->f:Landroid/graphics/Paint;

    const/4 v3, 0x0

    invoke-virtual {v0, p1, v1, v2, v3}, Landroid/text/Layout;->draw(Landroid/graphics/Canvas;Landroid/graphics/Path;Landroid/graphics/Paint;I)V

    goto :goto_0
.end method

.method public final onFinishTemporaryDetach()V
    .locals 1

    .prologue
    .line 575871
    invoke-super {p0}, Landroid/view/View;->onFinishTemporaryDetach()V

    .line 575872
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->k:Z

    .line 575873
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->l:LX/2nR;

    if-eqz v0, :cond_0

    .line 575874
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->l:LX/2nR;

    invoke-interface {v0, p0}, LX/2nR;->a(Landroid/view/View;)V

    .line 575875
    :cond_0
    return-void
.end method

.method public onMeasure(II)V
    .locals 3

    .prologue
    .line 575876
    const-string v0, "TextLayoutView.onMeasure"

    const v1, -0x8efe817

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 575877
    :try_start_0
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->b:LX/5Of;

    if-eqz v0, :cond_0

    .line 575878
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/text/TextLayoutView;->a(II)V

    .line 575879
    :cond_0
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->a:Landroid/text/Layout;

    if-nez v0, :cond_1

    .line 575880
    invoke-super {p0, p1, p2}, Landroid/view/View;->onMeasure(II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 575881
    const v0, -0x1f4d38c4

    invoke-static {v0}, LX/02m;->a(I)V

    .line 575882
    :goto_0
    return-void

    .line 575883
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/TextLayoutView;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/TextLayoutView;->getPaddingRight()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->a:Landroid/text/Layout;

    invoke-virtual {v1}, Landroid/text/Layout;->getWidth()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/TextLayoutView;->getPaddingTop()I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/TextLayoutView;->getPaddingBottom()I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->a:Landroid/text/Layout;

    invoke-static {v2}, LX/1nt;->b(Landroid/text/Layout;)I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/fbui/widget/text/TextLayoutView;->setMeasuredDimension(II)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 575884
    const v0, -0x3e50e1e9

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_0

    :catchall_0
    move-exception v0

    const v1, 0x626e2493

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final onStartTemporaryDetach()V
    .locals 1

    .prologue
    .line 575885
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->k:Z

    .line 575886
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->l:LX/2nR;

    if-eqz v0, :cond_0

    .line 575887
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->l:LX/2nR;

    invoke-interface {v0, p0}, LX/2nR;->b(Landroid/view/View;)V

    .line 575888
    :cond_0
    invoke-super {p0}, Landroid/view/View;->onStartTemporaryDetach()V

    .line 575889
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x1

    const/4 v0, 0x0

    const v2, -0xd6e85a3

    invoke-static {v4, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 575890
    iget-object v3, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->a:Landroid/text/Layout;

    if-nez v3, :cond_0

    .line 575891
    const v1, 0x5553deb5

    invoke-static {v4, v4, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 575892
    :goto_0
    return v0

    .line 575893
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    .line 575894
    if-ne v3, v1, :cond_2

    .line 575895
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/text/TextLayoutView;->a(Landroid/view/MotionEvent;)Landroid/text/style/ClickableSpan;

    move-result-object v3

    .line 575896
    if-eqz v3, :cond_1

    .line 575897
    invoke-virtual {v3, p0}, Landroid/text/style/ClickableSpan;->onClick(Landroid/view/View;)V

    .line 575898
    :goto_1
    invoke-direct {p0, v0, v0}, Lcom/facebook/fbui/widget/text/TextLayoutView;->b(II)V

    .line 575899
    const v0, 0x7ad1fd62

    invoke-static {v0, v2}, LX/02F;->a(II)V

    move v0, v1

    goto :goto_0

    .line 575900
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/TextLayoutView;->performClick()Z

    goto :goto_1

    .line 575901
    :cond_2
    if-nez v3, :cond_4

    .line 575902
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/text/TextLayoutView;->a(Landroid/view/MotionEvent;)Landroid/text/style/ClickableSpan;

    move-result-object v3

    .line 575903
    if-eqz v3, :cond_3

    .line 575904
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->a:Landroid/text/Layout;

    invoke-virtual {v0}, Landroid/text/Layout;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Landroid/text/Spanned;

    .line 575905
    invoke-interface {v0, v3}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    move-result v4

    invoke-interface {v0, v3}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    move-result v0

    invoke-direct {p0, v4, v0}, Lcom/facebook/fbui/widget/text/TextLayoutView;->b(II)V

    .line 575906
    :goto_2
    const v0, -0x3f050a9a

    invoke-static {v0, v2}, LX/02F;->a(II)V

    move v0, v1

    goto :goto_0

    .line 575907
    :cond_3
    invoke-direct {p0, v0, v0}, Lcom/facebook/fbui/widget/text/TextLayoutView;->b(II)V

    goto :goto_2

    .line 575908
    :cond_4
    const/4 v1, 0x3

    if-ne v3, v1, :cond_5

    .line 575909
    invoke-direct {p0, v0, v0}, Lcom/facebook/fbui/widget/text/TextLayoutView;->b(II)V

    .line 575910
    :cond_5
    const v1, -0x62b3dec8

    invoke-static {v1, v2}, LX/02F;->a(II)V

    goto :goto_0
.end method

.method public setAttachDetachListener(LX/2nR;)V
    .locals 1

    .prologue
    .line 575911
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->l:LX/2nR;

    if-eqz v0, :cond_0

    .line 575912
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->l:LX/2nR;

    invoke-interface {v0, p0}, LX/2nR;->b(Landroid/view/View;)V

    .line 575913
    :cond_0
    iput-object p1, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->l:LX/2nR;

    .line 575914
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->l:LX/2nR;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->k:Z

    if-eqz v0, :cond_1

    .line 575915
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->l:LX/2nR;

    invoke-interface {v0, p0}, LX/2nR;->a(Landroid/view/View;)V

    .line 575916
    :cond_1
    return-void
.end method

.method public setHighlightColor(I)V
    .locals 2

    .prologue
    .line 575917
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->f:Landroid/graphics/Paint;

    if-nez v0, :cond_0

    .line 575918
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->f:Landroid/graphics/Paint;

    .line 575919
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->f:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 575920
    :cond_0
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->f:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 575921
    return-void
.end method

.method public setLayout(Landroid/text/Layout;)V
    .locals 0

    .prologue
    .line 575922
    iput-object p1, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->a:Landroid/text/Layout;

    .line 575923
    return-void
.end method

.method public setTextLayout(Landroid/text/Layout;)V
    .locals 1

    .prologue
    .line 575924
    iput-object p1, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->a:Landroid/text/Layout;

    .line 575925
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/fbui/widget/text/TextLayoutView;->b:LX/5Of;

    .line 575926
    if-eqz p1, :cond_0

    .line 575927
    invoke-virtual {p1}, Landroid/text/Layout;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/text/TextLayoutView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 575928
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/text/TextLayoutView;->requestLayout()V

    .line 575929
    return-void
.end method
