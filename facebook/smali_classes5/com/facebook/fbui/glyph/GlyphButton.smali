.class public Lcom/facebook/fbui/glyph/GlyphButton;
.super Landroid/widget/ImageButton;
.source ""


# instance fields
.field public a:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Landroid/content/res/ColorStateList;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 678034
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/fbui/glyph/GlyphButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 678035
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 678032
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/fbui/glyph/GlyphButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 678033
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 678011
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 678012
    const-class v0, Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-static {v0, p0}, Lcom/facebook/fbui/glyph/GlyphButton;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 678013
    sget-object v0, LX/03r;->GlyphColorizer:[I

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 678014
    const/16 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 678015
    const/16 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    .line 678016
    invoke-virtual {p0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setGlyphColor(Landroid/content/res/ColorStateList;)V

    .line 678017
    :cond_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 678018
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/fbui/glyph/GlyphButton;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v0

    check-cast v0, LX/0wM;

    iput-object v0, p0, Lcom/facebook/fbui/glyph/GlyphButton;->a:LX/0wM;

    return-void
.end method


# virtual methods
.method public final drawableStateChanged()V
    .locals 3

    .prologue
    .line 678026
    invoke-super {p0}, Landroid/widget/ImageButton;->drawableStateChanged()V

    .line 678027
    iget-object v0, p0, Lcom/facebook/fbui/glyph/GlyphButton;->b:Landroid/content/res/ColorStateList;

    if-eqz v0, :cond_0

    .line 678028
    iget-object v0, p0, Lcom/facebook/fbui/glyph/GlyphButton;->b:Landroid/content/res/ColorStateList;

    invoke-virtual {p0}, Lcom/facebook/fbui/glyph/GlyphButton;->getDrawableState()[I

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v0

    .line 678029
    iget-object v1, p0, Lcom/facebook/fbui/glyph/GlyphButton;->a:LX/0wM;

    invoke-virtual {v1, v0}, LX/0wM;->a(I)Landroid/graphics/ColorFilter;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/glyph/GlyphButton;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 678030
    :goto_0
    return-void

    .line 678031
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/glyph/GlyphButton;->setColorFilter(Landroid/graphics/ColorFilter;)V

    goto :goto_0
.end method

.method public getGlyphColor()Landroid/content/res/ColorStateList;
    .locals 1

    .prologue
    .line 678025
    iget-object v0, p0, Lcom/facebook/fbui/glyph/GlyphButton;->b:Landroid/content/res/ColorStateList;

    return-object v0
.end method

.method public setGlyphColor(I)V
    .locals 1

    .prologue
    .line 678023
    invoke-static {p1}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/glyph/GlyphButton;->setGlyphColor(Landroid/content/res/ColorStateList;)V

    .line 678024
    return-void
.end method

.method public setGlyphColor(Landroid/content/res/ColorStateList;)V
    .locals 0

    .prologue
    .line 678019
    iput-object p1, p0, Lcom/facebook/fbui/glyph/GlyphButton;->b:Landroid/content/res/ColorStateList;

    .line 678020
    invoke-virtual {p0}, Lcom/facebook/fbui/glyph/GlyphButton;->refreshDrawableState()V

    .line 678021
    invoke-virtual {p0}, Lcom/facebook/fbui/glyph/GlyphButton;->invalidate()V

    .line 678022
    return-void
.end method
