.class public Lcom/facebook/fbui/dialog/DialogButtonBar;
.super Landroid/widget/LinearLayout;
.source ""


# static fields
.field private static final b:[I


# instance fields
.field public a:LX/23P;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 677879
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x7f01022f

    aput v2, v0, v1

    sput-object v0, Lcom/facebook/fbui/dialog/DialogButtonBar;->b:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 677876
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 677877
    const-class v0, Lcom/facebook/fbui/dialog/DialogButtonBar;

    invoke-static {v0, p0}, Lcom/facebook/fbui/dialog/DialogButtonBar;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 677878
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 677870
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 677871
    const-class v0, Lcom/facebook/fbui/dialog/DialogButtonBar;

    invoke-static {v0, p0}, Lcom/facebook/fbui/dialog/DialogButtonBar;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 677872
    sget-object v0, Lcom/facebook/fbui/dialog/DialogButtonBar;->b:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 677873
    invoke-virtual {v0, v1, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/fbui/dialog/DialogButtonBar;->c:Z

    .line 677874
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 677875
    return-void
.end method

.method private static a(Landroid/widget/TextView;)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 677865
    invoke-virtual {p0}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v1

    invoke-virtual {v1}, Landroid/text/Layout;->getLineCount()I

    move-result v2

    move v1, v0

    .line 677866
    :goto_0
    if-ge v0, v2, :cond_0

    .line 677867
    int-to-float v1, v1

    invoke-virtual {p0}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/text/Layout;->getLineWidth(I)F

    move-result v3

    add-float/2addr v1, v3

    float-to-int v1, v1

    .line 677868
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 677869
    :cond_0
    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundPaddingLeft()I

    move-result v0

    add-int/2addr v0, v1

    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundPaddingRight()I

    move-result v1

    mul-int/2addr v1, v2

    add-int/2addr v0, v1

    return v0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/fbui/dialog/DialogButtonBar;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/fbui/dialog/DialogButtonBar;

    invoke-static {v0}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object v0

    check-cast v0, LX/23P;

    iput-object v0, p0, Lcom/facebook/fbui/dialog/DialogButtonBar;->a:LX/23P;

    return-void
.end method

.method private a()Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 677826
    invoke-virtual {p0}, Lcom/facebook/fbui/dialog/DialogButtonBar;->getChildCount()I

    move-result v3

    move v2, v1

    move v0, v1

    .line 677827
    :goto_0
    if-ge v2, v3, :cond_1

    .line 677828
    invoke-virtual {p0, v2}, Lcom/facebook/fbui/dialog/DialogButtonBar;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v4

    const/16 v5, 0x8

    if-eq v4, v5, :cond_0

    .line 677829
    add-int/lit8 v0, v0, 0x1

    .line 677830
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 677831
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/fbui/dialog/DialogButtonBar;->getMeasuredWidth()I

    move-result v2

    int-to-float v2, v2

    int-to-float v0, v0

    div-float/2addr v2, v0

    move v0, v1

    .line 677832
    :goto_1
    if-ge v0, v3, :cond_3

    .line 677833
    invoke-virtual {p0, v0}, Lcom/facebook/fbui/dialog/DialogButtonBar;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 677834
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    int-to-float v4, v4

    cmpl-float v4, v4, v2

    if-lez v4, :cond_2

    .line 677835
    const/4 v0, 0x1

    .line 677836
    :goto_2
    return v0

    .line 677837
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    move v0, v1

    .line 677838
    goto :goto_2
.end method

.method private b()Z
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 677852
    invoke-virtual {p0}, Lcom/facebook/fbui/dialog/DialogButtonBar;->getChildCount()I

    move-result v4

    .line 677853
    invoke-virtual {p0}, Lcom/facebook/fbui/dialog/DialogButtonBar;->getMeasuredWidth()I

    move-result v0

    int-to-float v5, v0

    move v3, v2

    move v1, v2

    .line 677854
    :goto_0
    if-ge v3, v4, :cond_3

    .line 677855
    invoke-virtual {p0, v3}, Lcom/facebook/fbui/dialog/DialogButtonBar;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 677856
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v6

    if-nez v6, :cond_1

    .line 677857
    instance-of v6, v0, Landroid/widget/TextView;

    if-eqz v6, :cond_0

    .line 677858
    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lcom/facebook/fbui/dialog/DialogButtonBar;->a(Landroid/widget/TextView;)I

    move-result v0

    add-int/2addr v0, v1

    .line 677859
    :goto_1
    int-to-float v1, v0

    cmpl-float v1, v1, v5

    if-lez v1, :cond_2

    .line 677860
    const/4 v0, 0x1

    .line 677861
    :goto_2
    return v0

    .line 677862
    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    add-int/2addr v0, v1

    goto :goto_1

    :cond_1
    move v0, v1

    .line 677863
    :cond_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_0

    :cond_3
    move v0, v2

    .line 677864
    goto :goto_2
.end method


# virtual methods
.method public final addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    .prologue
    .line 677848
    invoke-super {p0, p1, p2, p3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 677849
    instance-of v0, p1, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 677850
    check-cast p1, Landroid/widget/TextView;

    iget-object v0, p0, Lcom/facebook/fbui/dialog/DialogButtonBar;->a:LX/23P;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 677851
    :cond_0
    return-void
.end method

.method public final onMeasure(II)V
    .locals 1

    .prologue
    .line 677839
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/dialog/DialogButtonBar;->setOrientation(I)V

    .line 677840
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    .line 677841
    iget-boolean v0, p0, Lcom/facebook/fbui/dialog/DialogButtonBar;->c:Z

    if-eqz v0, :cond_1

    .line 677842
    invoke-direct {p0}, Lcom/facebook/fbui/dialog/DialogButtonBar;->b()Z

    move-result v0

    .line 677843
    :goto_0
    if-eqz v0, :cond_0

    .line 677844
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/dialog/DialogButtonBar;->setOrientation(I)V

    .line 677845
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    .line 677846
    :cond_0
    return-void

    .line 677847
    :cond_1
    invoke-direct {p0}, Lcom/facebook/fbui/dialog/DialogButtonBar;->a()Z

    move-result v0

    goto :goto_0
.end method
