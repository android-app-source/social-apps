.class public final Lcom/facebook/messaging/media/bandwidth/MediaBandwidthManager$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/2Mo;


# direct methods
.method public constructor <init>(LX/2Mo;)V
    .locals 0

    .prologue
    .line 572292
    iput-object p1, p0, Lcom/facebook/messaging/media/bandwidth/MediaBandwidthManager$1;->a:LX/2Mo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 12

    .prologue
    .line 572293
    iget-object v0, p0, Lcom/facebook/messaging/media/bandwidth/MediaBandwidthManager$1;->a:LX/2Mo;

    .line 572294
    new-instance v1, LX/0Yd;

    const-string v2, "com.facebook.orca.media.bandwidth.ACTION_PURGE_NETWORKS_IN_SHARED_PREFS"

    new-instance v3, LX/2VV;

    invoke-direct {v3, v0}, LX/2VV;-><init>(LX/2Mo;)V

    invoke-direct {v1, v2, v3}, LX/0Yd;-><init>(Ljava/lang/String;LX/0YZ;)V

    .line 572295
    iget-object v2, v0, LX/2Mo;->e:Landroid/content/Context;

    new-instance v3, Landroid/content/IntentFilter;

    const-string v4, "com.facebook.orca.media.bandwidth.ACTION_PURGE_NETWORKS_IN_SHARED_PREFS"

    invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 572296
    const/4 v6, 0x0

    .line 572297
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v7

    .line 572298
    const/4 v5, 0x7

    invoke-virtual {v7, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    rsub-int/lit8 v5, v5, 0x7

    add-int/lit8 v5, v5, 0x2

    .line 572299
    const/4 v8, 0x5

    invoke-virtual {v7, v8, v5}, Ljava/util/Calendar;->add(II)V

    .line 572300
    const/16 v5, 0xa

    const/4 v8, 0x1

    invoke-virtual {v7, v5, v8}, Ljava/util/Calendar;->set(II)V

    .line 572301
    const/16 v5, 0xc

    invoke-virtual {v7, v5, v6}, Ljava/util/Calendar;->set(II)V

    .line 572302
    const/16 v5, 0xd

    invoke-virtual {v7, v5, v6}, Ljava/util/Calendar;->set(II)V

    .line 572303
    const/16 v5, 0xe

    invoke-virtual {v7, v5, v6}, Ljava/util/Calendar;->set(II)V

    .line 572304
    new-instance v5, Landroid/content/Intent;

    const-string v8, "com.facebook.orca.media.bandwidth.ACTION_PURGE_NETWORKS_IN_SHARED_PREFS"

    invoke-direct {v5, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 572305
    iget-object v8, v0, LX/2Mo;->e:Landroid/content/Context;

    const/4 v9, 0x1

    const/high16 v10, 0x8000000

    invoke-static {v8, v9, v5, v10}, LX/0nt;->b(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v5

    move-object v11, v5

    .line 572306
    iget-object v5, v0, LX/2Mo;->i:LX/12x;

    invoke-virtual {v7}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v7

    const-wide/32 v9, 0x240c8400

    invoke-virtual/range {v5 .. v11}, LX/12x;->a(IJJLandroid/app/PendingIntent;)V

    .line 572307
    return-void
.end method
