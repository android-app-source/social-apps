.class public Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/0Up;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# static fields
.field private static final Q:Ljava/lang/Object;

.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "LX/2MK;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "LX/2MK;",
            ">;"
        }
    .end annotation
.end field

.field private static final d:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "LX/2MK;",
            ">;"
        }
    .end annotation
.end field

.field private static final e:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final A:LX/2Ms;

.field public final B:LX/2N3;

.field public final C:LX/2Nw;

.field public final D:LX/2Nx;

.field public final E:LX/2OB;

.field public final F:Landroid/content/Context;

.field public final G:LX/0QI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QI",
            "<",
            "LX/6ed;",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation
.end field

.field private final H:LX/0Yb;

.field public final I:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final J:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6eS;",
            ">;"
        }
    .end annotation
.end field

.field public final K:LX/0Uh;

.field private final L:LX/0W3;

.field public final M:LX/2OC;

.field public N:Z

.field public O:Z

.field public volatile P:Z

.field public final f:LX/0aG;

.field public final g:LX/2ML;

.field public final h:LX/2MO;

.field public final i:LX/2MR;

.field public final j:LX/2MT;

.field private final k:Lcom/facebook/ui/media/attachments/MediaResourceHelper;

.field private final l:LX/2Mb;

.field private final m:LX/2Mk;

.field public final n:LX/0Xl;

.field public final o:LX/03V;

.field private final p:LX/2Ml;

.field public final q:LX/2Mm;

.field public final r:LX/2Mn;

.field public final s:Ljava/util/concurrent/Executor;

.field public final t:LX/0TD;

.field public final u:Ljava/util/concurrent/ScheduledExecutorService;

.field public final v:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/6eZ;",
            ">;"
        }
    .end annotation
.end field

.field private final w:LX/FHp;

.field private final x:LX/2Mp;

.field private final y:LX/2Mq;

.field private final z:LX/0V8;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 571979
    const-class v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    sput-object v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->a:Ljava/lang/Class;

    .line 571980
    sget-object v0, LX/2MK;->PHOTO:LX/2MK;

    sget-object v1, LX/2MK;->VIDEO:LX/2MK;

    sget-object v2, LX/2MK;->AUDIO:LX/2MK;

    sget-object v3, LX/2MK;->OTHER:LX/2MK;

    sget-object v4, LX/2MK;->ENCRYPTED_PHOTO:LX/2MK;

    sget-object v5, LX/2MK;->ENT_PHOTO:LX/2MK;

    new-array v6, v7, [LX/2MK;

    invoke-static/range {v0 .. v6}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->b:LX/0Rf;

    .line 571981
    sget-object v0, LX/2MK;->PHOTO:LX/2MK;

    sget-object v1, LX/2MK;->VIDEO:LX/2MK;

    sget-object v2, LX/2MK;->AUDIO:LX/2MK;

    sget-object v3, LX/2MK;->OTHER:LX/2MK;

    sget-object v4, LX/2MK;->ENCRYPTED_PHOTO:LX/2MK;

    sget-object v5, LX/2MK;->ENT_PHOTO:LX/2MK;

    new-array v6, v7, [LX/2MK;

    invoke-static/range {v0 .. v6}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->c:LX/0Rf;

    .line 571982
    sget-object v0, LX/2MK;->PHOTO:LX/2MK;

    sget-object v1, LX/2MK;->VIDEO:LX/2MK;

    sget-object v2, LX/2MK;->ENCRYPTED_PHOTO:LX/2MK;

    sget-object v3, LX/2MK;->ENT_PHOTO:LX/2MK;

    invoke-static {v0, v1, v2, v3}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->d:LX/0Rf;

    .line 571983
    const-string v0, "image/gif"

    const-string v1, "image/png"

    const-string v2, "image/webp"

    invoke-static {v0, v1, v2}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->e:LX/0Rf;

    .line 571984
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->Q:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0aG;LX/2ML;LX/2MO;LX/2MR;LX/2MT;Lcom/facebook/ui/media/attachments/MediaResourceHelper;LX/2Mb;LX/2Mk;LX/0Xl;LX/03V;LX/2Ml;LX/2Mm;LX/2Mn;Ljava/util/concurrent/Executor;LX/0TD;Ljava/util/concurrent/ScheduledExecutorService;LX/0Or;LX/FHp;LX/2Mp;LX/0V8;LX/2Mq;LX/2Ms;LX/2N3;LX/0Ot;LX/0Uh;LX/0W3;LX/2Nw;LX/2Nx;LX/2OB;Landroid/content/Context;LX/2OC;)V
    .locals 6
    .param p9    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p14    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p15    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .param p16    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .param p30    # Landroid/content/Context;
        .annotation build Lcom/facebook/inject/ForAppContext;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0aG;",
            "LX/2ML;",
            "LX/2MO;",
            "LX/2MR;",
            "LX/2MT;",
            "Lcom/facebook/ui/media/attachments/MediaResourceHelper;",
            "LX/2Mb;",
            "LX/2Mk;",
            "LX/0Xl;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/2Ml;",
            "LX/2Mm;",
            "LX/2Mn;",
            "Ljava/util/concurrent/Executor;",
            "LX/0TD;",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            "LX/0Or",
            "<",
            "LX/6eZ;",
            ">;",
            "LX/FHp;",
            "LX/2Mp;",
            "LX/0V8;",
            "LX/2Mq;",
            "LX/2Ms;",
            "LX/2N3;",
            "LX/0Ot",
            "<",
            "LX/6eS;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            "LX/2Nw;",
            "LX/2Nx;",
            "LX/2OB;",
            "Landroid/content/Context;",
            "LX/2OC;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 571818
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 571819
    iput-object p1, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->f:LX/0aG;

    .line 571820
    iput-object p2, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->g:LX/2ML;

    .line 571821
    iput-object p3, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->h:LX/2MO;

    .line 571822
    iput-object p4, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->i:LX/2MR;

    .line 571823
    iput-object p5, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->j:LX/2MT;

    .line 571824
    iput-object p6, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->k:Lcom/facebook/ui/media/attachments/MediaResourceHelper;

    .line 571825
    iput-object p7, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->l:LX/2Mb;

    .line 571826
    iput-object p8, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->m:LX/2Mk;

    .line 571827
    iput-object p9, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->n:LX/0Xl;

    .line 571828
    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->o:LX/03V;

    .line 571829
    move-object/from16 v0, p11

    iput-object v0, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->p:LX/2Ml;

    .line 571830
    move-object/from16 v0, p12

    iput-object v0, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->q:LX/2Mm;

    .line 571831
    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->r:LX/2Mn;

    .line 571832
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->s:Ljava/util/concurrent/Executor;

    .line 571833
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->t:LX/0TD;

    .line 571834
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->u:Ljava/util/concurrent/ScheduledExecutorService;

    .line 571835
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->v:LX/0Or;

    .line 571836
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->w:LX/FHp;

    .line 571837
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->x:LX/2Mp;

    .line 571838
    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->z:LX/0V8;

    .line 571839
    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->y:LX/2Mq;

    .line 571840
    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->A:LX/2Ms;

    .line 571841
    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->B:LX/2N3;

    .line 571842
    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->J:LX/0Ot;

    .line 571843
    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->K:LX/0Uh;

    .line 571844
    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->L:LX/0W3;

    .line 571845
    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->C:LX/2Nw;

    .line 571846
    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->D:LX/2Nx;

    .line 571847
    move-object/from16 v0, p29

    iput-object v0, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->E:LX/2OB;

    .line 571848
    move-object/from16 v0, p30

    iput-object v0, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->F:Landroid/content/Context;

    .line 571849
    move-object/from16 v0, p31

    iput-object v0, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->M:LX/2OC;

    .line 571850
    invoke-static {}, LX/0QN;->newBuilder()LX/0QN;

    move-result-object v2

    const-wide/16 v4, 0x12c

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v4, v5, v3}, LX/0QN;->a(JLjava/util/concurrent/TimeUnit;)LX/0QN;

    move-result-object v2

    invoke-virtual {v2}, LX/0QN;->q()LX/0QI;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->G:LX/0QI;

    .line 571851
    invoke-static {}, LX/0PM;->e()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->I:Ljava/util/Set;

    .line 571852
    new-instance v2, LX/2OH;

    invoke-direct {v2, p0}, LX/2OH;-><init>(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;)V

    .line 571853
    iget-object v3, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->n:LX/0Xl;

    invoke-interface {v3}, LX/0Xl;->a()LX/0YX;

    move-result-object v3

    const-string v4, "com.facebook.orca.media.upload.MEDIA_TRANSCODE_PROGRESS"

    invoke-interface {v3, v4, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v3

    const-string v4, "com.facebook.orca.media.upload.MEDIA_TRANSCODE_COMPLETE"

    invoke-interface {v3, v4, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v3

    const-string v4, "com.facebook.orca.media.upload.MEDIA_UPLOAD_PROGRESS"

    invoke-interface {v3, v4, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v3

    const-string v4, "com.facebook.orca.media.upload.MEDIA_UPLOAD_COMPLETE"

    invoke-interface {v3, v4, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v3

    const-string v4, "com.facebook.orca.media.upload.MEDIA_GET_FBID_COMPLETE"

    invoke-interface {v3, v4, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v2

    invoke-interface {v2}, LX/0YX;->a()LX/0Yb;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->H:LX/0Yb;

    .line 571854
    iget-object v2, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->H:LX/0Yb;

    invoke-virtual {v2}, LX/0Yb;->b()V

    .line 571855
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->P:Z

    .line 571856
    return-void
.end method

.method public static a(LX/2MK;ZDD)D
    .locals 8
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    const-wide/16 v4, 0x0

    .line 571857
    sget-object v6, LX/2MK;->PHOTO:LX/2MK;

    invoke-virtual {p0, v6}, LX/2MK;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    sget-object v6, LX/2MK;->ENT_PHOTO:LX/2MK;

    invoke-virtual {p0, v6}, LX/2MK;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    :cond_0
    move-wide v0, v2

    move-wide v2, v4

    .line 571858
    :goto_0
    mul-double v4, p2, v2

    .line 571859
    mul-double v6, p4, v0

    .line 571860
    add-double/2addr v0, v2

    .line 571861
    add-double v2, v4, v6

    div-double v0, v2, v0

    .line 571862
    return-wide v0

    .line 571863
    :cond_1
    sget-object v6, LX/2MK;->VIDEO:LX/2MK;

    invoke-virtual {p0, v6}, LX/2MK;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 571864
    if-eqz p1, :cond_2

    move-wide v2, v0

    .line 571865
    goto :goto_0

    :cond_2
    move-wide v0, v2

    move-wide v2, v4

    .line 571866
    goto :goto_0

    .line 571867
    :cond_3
    sget-object v0, LX/2MK;->AUDIO:LX/2MK;

    invoke-virtual {p0, v0}, LX/2MK;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    move-wide v0, v2

    move-wide v2, v4

    .line 571868
    goto :goto_0

    .line 571869
    :cond_4
    sget-object v0, LX/2MK;->OTHER:LX/2MK;

    invoke-virtual {p0, v0}, LX/2MK;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    move-wide v0, v2

    move-wide v2, v4

    .line 571870
    goto :goto_0

    .line 571871
    :cond_5
    sget-object v0, LX/2MK;->ENCRYPTED_PHOTO:LX/2MK;

    invoke-virtual {p0, v0}, LX/2MK;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 571872
    const-wide/high16 v2, 0x3fd0000000000000L    # 0.25

    .line 571873
    const-wide/high16 v0, 0x3fe8000000000000L    # 0.75

    goto :goto_0

    .line 571874
    :cond_6
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Trying to get progress for an unsupported type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;
    .locals 7

    .prologue
    .line 571875
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 571876
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 571877
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 571878
    if-nez v1, :cond_0

    .line 571879
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 571880
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 571881
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 571882
    sget-object v1, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->Q:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 571883
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 571884
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 571885
    :cond_1
    if-nez v1, :cond_4

    .line 571886
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 571887
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 571888
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->b(LX/0QB;)Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 571889
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 571890
    if-nez v1, :cond_2

    .line 571891
    sget-object v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->Q:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 571892
    :goto_1
    if-eqz v0, :cond_3

    .line 571893
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 571894
    :goto_3
    check-cast v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 571895
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 571896
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 571897
    :catchall_1
    move-exception v0

    .line 571898
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 571899
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 571900
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 571901
    :cond_2
    :try_start_8
    sget-object v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->Q:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method public static a(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .param p1    # Lcom/facebook/ui/media/attachments/MediaResource;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/ui/media/attachments/MediaUploadResult;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 571902
    new-instance v0, LX/FH6;

    invoke-direct {v0, p0, p2, p1}, LX/FH6;-><init>(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/google/common/util/concurrent/ListenableFuture;Lcom/facebook/ui/media/attachments/MediaResource;)V

    iget-object v1, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->t:LX/0TD;

    invoke-static {p3, v0, v1}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 571903
    return-object v0
.end method

.method public static a(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;ZZLcom/facebook/messaging/media/photoquality/PhotoQuality;LX/FHx;ZZ)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            "ZZ",
            "Lcom/facebook/messaging/media/photoquality/PhotoQuality;",
            "LX/FHx;",
            "ZZ)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 571904
    if-nez p2, :cond_0

    .line 571905
    const/4 v0, 0x0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 571906
    :goto_0
    return-object v0

    .line 571907
    :cond_0
    if-eqz p3, :cond_2

    .line 571908
    invoke-static {p1}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->n(Lcom/facebook/ui/media/attachments/MediaResource;)Lcom/facebook/ui/media/attachments/MediaResource;

    move-result-object v2

    .line 571909
    sget-object v0, LX/FHx;->PHASE_TWO:LX/FHx;

    if-ne p5, v0, :cond_1

    iget-object v0, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->h:LX/2MO;

    invoke-virtual {v0, v2}, LX/2MO;->b(Lcom/facebook/ui/media/attachments/MediaResource;)Lcom/facebook/ui/media/attachments/MediaResource;

    move-result-object v0

    move-object v1, v0

    .line 571910
    :goto_1
    if-eqz v1, :cond_2

    .line 571911
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->J:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6eS;

    invoke-virtual {v0, v1}, LX/6eS;->a(Lcom/facebook/ui/media/attachments/MediaResource;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v8

    new-instance v0, LX/FGv;

    move-object v1, p0

    move-object v3, p5

    move-object v4, p1

    move-object v5, p4

    move v6, p6

    move/from16 v7, p7

    invoke-direct/range {v0 .. v7}, LX/FGv;-><init>(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;LX/FHx;Lcom/facebook/ui/media/attachments/MediaResource;Lcom/facebook/messaging/media/photoquality/PhotoQuality;ZZ)V

    iget-object v1, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->s:Ljava/util/concurrent/Executor;

    invoke-static {v8, v0, v1}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0

    .line 571912
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->h:LX/2MO;

    invoke-virtual {v0, v2}, LX/2MO;->a(Lcom/facebook/ui/media/attachments/MediaResource;)Lcom/facebook/ui/media/attachments/MediaResource;

    move-result-object v0

    move-object v1, v0

    goto :goto_1

    :cond_2
    move-object v0, p0

    move-object v1, p1

    move-object v2, p4

    move-object v3, p5

    move v4, p6

    move/from16 v5, p7

    .line 571913
    invoke-static/range {v0 .. v5}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->a$redex0(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;Lcom/facebook/messaging/media/photoquality/PhotoQuality;LX/FHx;ZZ)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Lcom/facebook/ui/media/attachments/MediaResource;LX/FHf;ZI)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            "LX/FHf;",
            "ZI)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/ui/media/attachments/MediaUploadResult;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x1

    .line 571914
    iget-object v0, p1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v1, LX/2MK;->VIDEO:LX/2MK;

    if-ne v0, v1, :cond_0

    move v0, v3

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 571915
    invoke-static {p0, p1, p4, p3}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->a(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;IZ)Z

    move-result v0

    .line 571916
    if-nez v0, :cond_1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p1

    move-object v4, p2

    move-object v6, v5

    .line 571917
    invoke-static/range {v0 .. v6}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->a$redex0(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;Lcom/facebook/ui/media/attachments/MediaResource;ZLX/FHf;Ljava/lang/Integer;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 571918
    :goto_1
    return-object v0

    .line 571919
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 571920
    :cond_1
    sget-object v0, LX/FGa;->STARTED:LX/FGa;

    invoke-static {p0, p1, v0}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->a$redex0(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;LX/FGa;)V

    .line 571921
    sget-object v0, LX/FGa;->TRANSCODING:LX/FGa;

    invoke-static {p0, p1, v0}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->a$redex0(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;LX/FGa;)V

    .line 571922
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 571923
    const-string v0, "mediaResource"

    invoke-virtual {v2, v0, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 571924
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->v:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6eZ;

    .line 571925
    const-string v1, "transcode"

    iget-boolean v0, v0, LX/6eZ;->a:Z

    invoke-virtual {v2, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 571926
    const-string v0, "isOutOfSpace"

    invoke-virtual {v2, v0, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 571927
    const-string v0, "estimatedBytes"

    invoke-virtual {v2, v0, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 571928
    const-string v1, "video_segment_transcode_upload"

    .line 571929
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->f:LX/0aG;

    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    sget-object v4, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->a:Ljava/lang/Class;

    const-string v5, "video_transcode_upload"

    invoke-static {v4, v5}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    const v5, -0x33756ef3    # -7.2648808E7f

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 571930
    iget-object v1, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->j:LX/2MT;

    invoke-virtual {v1, p1, v0}, LX/2MT;->a(Lcom/facebook/ui/media/attachments/MediaResource;LX/1ML;)V

    .line 571931
    new-instance v1, LX/FGx;

    invoke-direct {v1, p0}, LX/FGx;-><init>(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;)V

    iget-object v2, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->s:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_1
.end method

.method private a(Lcom/facebook/ui/media/attachments/MediaResource;LX/FHf;ZLcom/google/common/util/concurrent/ListenableFuture;Ljava/lang/Integer;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            "LX/FHf;",
            "Z",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;",
            "Ljava/lang/Integer;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/ui/media/attachments/MediaUploadResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 571932
    new-instance v0, LX/FGr;

    invoke-direct {v0, p0, p1}, LX/FGr;-><init>(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;)V

    iget-object v1, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->s:Ljava/util/concurrent/Executor;

    invoke-static {p4, v0, v1}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    move-object v6, v0

    .line 571933
    new-instance v0, LX/FH5;

    move-object v1, p0

    move-object v2, p1

    move v3, p3

    move-object v4, p2

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, LX/FH5;-><init>(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;ZLX/FHf;Ljava/lang/Integer;)V

    iget-object v1, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->s:Ljava/util/concurrent/Executor;

    invoke-static {v6, v0, v1}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 571934
    return-object v0
.end method

.method private a(Lcom/facebook/ui/media/attachments/MediaResource;Lcom/facebook/ui/media/attachments/MediaUploadResult;)V
    .locals 8

    .prologue
    .line 571935
    new-instance v6, Landroid/content/Intent;

    const-string v0, "EncryptedPhotoUploadStatusAction"

    invoke-direct {v6, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 571936
    const-string v7, "EncryptedPhotoUploadStatusKey"

    iget-object v0, p1, Lcom/facebook/ui/media/attachments/MediaResource;->p:Ljava/lang/String;

    iget-object v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    .line 571937
    iget-object v2, p2, Lcom/facebook/ui/media/attachments/MediaUploadResult;->a:Ljava/lang/String;

    move-object v2, v2

    .line 571938
    iget-object v3, p2, Lcom/facebook/ui/media/attachments/MediaUploadResult;->b:[B

    move-object v3, v3

    .line 571939
    iget-object v4, p2, Lcom/facebook/ui/media/attachments/MediaUploadResult;->c:Ljava/lang/String;

    move-object v4, v4

    .line 571940
    iget-object v5, p2, Lcom/facebook/ui/media/attachments/MediaUploadResult;->d:Ljava/lang/Long;

    move-object v5, v5

    .line 571941
    invoke-static/range {v0 .. v5}, Lcom/facebook/messaging/media/upload/EncryptedPhotoUploadResult;->a(Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;[BLjava/lang/String;Ljava/lang/Long;)Lcom/facebook/messaging/media/upload/EncryptedPhotoUploadResult;

    move-result-object v0

    invoke-virtual {v6, v7, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 571942
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->n:LX/0Xl;

    invoke-interface {v0, v6}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 571943
    return-void
.end method

.method private a(Lcom/google/common/util/concurrent/SettableFuture;Lcom/facebook/ui/media/attachments/MediaResource;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<",
            "LX/FGc;",
            ">;",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            ")V"
        }
    .end annotation

    .prologue
    .line 571944
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 571945
    const-string v3, "mediaResource"

    invoke-virtual {v5, v3, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 571946
    iget-object v3, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->f:LX/0aG;

    const-string v4, "media_get_fbid"

    sget-object v6, LX/1ME;->BY_EXCEPTION:LX/1ME;

    sget-object v7, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->a:Ljava/lang/Class;

    const-string v8, "media_dedupe"

    invoke-static {v7, v8}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v7

    const v8, 0x2de07f32

    invoke-static/range {v3 .. v8}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v3

    invoke-interface {v3}, LX/1MF;->start()LX/1ML;

    move-result-object v3

    .line 571947
    iget-object v4, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->j:LX/2MT;

    invoke-virtual {v4, p2, v3}, LX/2MT;->a(Lcom/facebook/ui/media/attachments/MediaResource;LX/1ML;)V

    .line 571948
    new-instance v4, LX/FGu;

    invoke-direct {v4, p0}, LX/FGu;-><init>(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;)V

    iget-object v5, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->s:Ljava/util/concurrent/Executor;

    invoke-static {v3, v4, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    move-object v0, v3

    .line 571949
    new-instance v1, LX/FGt;

    invoke-direct {v1, p0, p2, p1}, LX/FGt;-><init>(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;Lcom/google/common/util/concurrent/SettableFuture;)V

    iget-object v2, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->s:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 571950
    return-void
.end method

.method private a(I)Z
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v0, 0x0

    .line 572056
    if-lez p1, :cond_2

    iget-object v1, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->z:LX/0V8;

    sget-object v2, LX/0VA;->EXTERNAL:LX/0VA;

    invoke-virtual {v1, v2}, LX/0V8;->c(LX/0VA;)J

    move-result-wide v2

    cmp-long v1, v2, v4

    if-gtz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->z:LX/0V8;

    sget-object v2, LX/0VA;->INTERNAL:LX/0VA;

    invoke-virtual {v1, v2}, LX/0V8;->c(LX/0VA;)J

    move-result-wide v2

    cmp-long v1, v2, v4

    if-lez v1, :cond_2

    .line 572057
    :cond_0
    iget-object v1, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->z:LX/0V8;

    sget-object v2, LX/0VA;->EXTERNAL:LX/0VA;

    int-to-long v4, p1

    invoke-virtual {v1, v2, v4, v5}, LX/0V8;->a(LX/0VA;J)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->z:LX/0V8;

    sget-object v2, LX/0VA;->INTERNAL:LX/0VA;

    int-to-long v4, p1

    invoke-virtual {v1, v2, v4, v5}, LX/0V8;->a(LX/0VA;J)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    const/4 v0, 0x1

    .line 572058
    :cond_2
    return v0
.end method

.method public static a(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;IZ)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 571951
    iget-object v0, p1, Lcom/facebook/ui/media/attachments/MediaResource;->o:Landroid/net/Uri;

    if-nez v0, :cond_0

    iget-boolean v0, p1, Lcom/facebook/ui/media/attachments/MediaResource;->n:Z

    if-eqz v0, :cond_1

    :cond_0
    move v0, v1

    .line 571952
    :goto_0
    return v0

    .line 571953
    :cond_1
    if-nez p3, :cond_4

    iget-object v2, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->w:LX/FHp;

    iget-object v0, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->v:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6eZ;

    iget-boolean v0, v0, LX/6eZ;->a:Z

    invoke-virtual {v2, p1, p2, v0}, LX/FHp;->a(Lcom/facebook/ui/media/attachments/MediaResource;IZ)Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    .line 571954
    :goto_1
    iget-object v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    if-eqz v1, :cond_2

    iget-object v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getEncodedPath()Ljava/lang/String;

    .line 571955
    :cond_2
    if-nez v0, :cond_3

    .line 571956
    iget-object v1, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->h:LX/2MO;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, LX/2MO;->a(Lcom/facebook/ui/media/attachments/MediaResource;Lcom/facebook/ui/media/attachments/MediaResource;)V

    .line 571957
    :cond_3
    iget-object v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->e:LX/5zj;

    invoke-virtual {v1}, LX/5zj;->name()Ljava/lang/String;

    goto :goto_0

    .line 571958
    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private a(Lcom/facebook/ui/media/attachments/MediaResource;Lcom/facebook/messaging/media/photoquality/PhotoQuality;)Z
    .locals 4

    .prologue
    .line 572044
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->B:LX/2N3;

    invoke-virtual {v0, p1}, LX/2N3;->a(Lcom/facebook/ui/media/attachments/MediaResource;)Z

    move-result v1

    .line 572045
    const/4 v0, 0x0

    .line 572046
    if-eqz v1, :cond_0

    .line 572047
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->h:LX/2MO;

    invoke-virtual {v0, p1}, LX/2MO;->b(Lcom/facebook/ui/media/attachments/MediaResource;)Lcom/facebook/ui/media/attachments/MediaResource;

    move-result-object v0

    .line 572048
    :cond_0
    if-nez v0, :cond_1

    .line 572049
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->h:LX/2MO;

    invoke-virtual {v0, p1}, LX/2MO;->a(Lcom/facebook/ui/media/attachments/MediaResource;)Lcom/facebook/ui/media/attachments/MediaResource;

    move-result-object v0

    .line 572050
    :cond_1
    const/4 v1, 0x1

    .line 572051
    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v3, LX/2MK;->PHOTO:LX/2MK;

    if-ne v2, v3, :cond_3

    if-eqz v0, :cond_3

    .line 572052
    iget v2, p2, Lcom/facebook/messaging/media/photoquality/PhotoQuality;->b:I

    move v2, v2

    .line 572053
    iget v3, v0, Lcom/facebook/ui/media/attachments/MediaResource;->l:I

    if-le v2, v3, :cond_3

    iget v3, v0, Lcom/facebook/ui/media/attachments/MediaResource;->k:I

    if-le v2, v3, :cond_3

    iget v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->l:I

    iget v3, v0, Lcom/facebook/ui/media/attachments/MediaResource;->l:I

    if-gt v2, v3, :cond_2

    iget v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->k:I

    iget v0, v0, Lcom/facebook/ui/media/attachments/MediaResource;->k:I

    if-le v2, v0, :cond_3

    .line 572054
    :cond_2
    const/4 v0, 0x0

    .line 572055
    :goto_0
    return v0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;LX/FHf;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            "LX/FHf;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/FGc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 572041
    sget-object v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->b:LX/0Rf;

    iget-object v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    invoke-virtual {v0, v1}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 572042
    invoke-direct {p0, p1}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->f(Lcom/facebook/ui/media/attachments/MediaResource;)V

    .line 572043
    invoke-static {p0, p1, p2}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->b(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;LX/FHf;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public static a$redex0(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;LX/FHf;ZLcom/facebook/messaging/media/photoquality/PhotoQuality;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 23
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            "LX/FHf;",
            "Z",
            "Lcom/facebook/messaging/media/photoquality/PhotoQuality;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/FGc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 571987
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->q:LX/2Mm;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, LX/2Mm;->a(Lcom/facebook/ui/media/attachments/MediaResource;)V

    .line 571988
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->r:LX/2Mn;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, LX/2Mn;->a(Lcom/facebook/ui/media/attachments/MediaResource;)V

    .line 571989
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v21

    .line 571990
    sget-object v4, LX/FGa;->STARTED:LX/FGa;

    move-object/from16 v0, v21

    invoke-static {v4, v0}, LX/FGc;->a(LX/FGa;Lcom/google/common/util/concurrent/ListenableFuture;)LX/FGc;

    move-result-object v4

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v4}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->b(Lcom/facebook/ui/media/attachments/MediaResource;LX/FGc;)V

    .line 571991
    sget-object v4, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->c:LX/0Rf;

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    invoke-virtual {v4, v5}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v4

    .line 571992
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/facebook/ui/media/attachments/MediaResource;->e:LX/5zj;

    sget-object v6, LX/5zj;->FORWARD:LX/5zj;

    if-ne v5, v6, :cond_1

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->L:LX/0W3;

    sget-wide v6, LX/0X5;->ks:J

    invoke-interface {v5, v6, v7}, LX/0W4;->a(J)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 571993
    invoke-static/range {p1 .. p1}, LX/5zs;->fromOrNull(Lcom/facebook/ui/media/attachments/MediaResource;)LX/5zs;

    move-result-object v5

    .line 571994
    if-eqz v5, :cond_0

    invoke-virtual {v5}, LX/5zs;->isLikelyLocal()Z

    move-result v5

    if-nez v5, :cond_1

    .line 571995
    :cond_0
    const/4 v4, 0x0

    .line 571996
    :cond_1
    if-nez v4, :cond_2

    .line 571997
    const/4 v4, 0x0

    sget-object v5, LX/FH8;->NOT_REQUIRED:LX/FH8;

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, p1

    invoke-static {v0, v1, v2, v4, v5}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->a$redex0(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/google/common/util/concurrent/SettableFuture;Lcom/facebook/ui/media/attachments/MediaResource;Lcom/facebook/ui/media/attachments/MediaUploadResult;LX/FH8;)Z

    move-object/from16 v4, v21

    .line 571998
    :goto_0
    return-object v4

    .line 571999
    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->x:LX/2Mp;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, LX/2Mp;->a(Lcom/facebook/ui/media/attachments/MediaResource;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 572000
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->a(Lcom/google/common/util/concurrent/SettableFuture;Lcom/facebook/ui/media/attachments/MediaResource;)V

    .line 572001
    :cond_3
    sget-object v4, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->d:LX/0Rf;

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    invoke-virtual {v4, v5}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v6

    .line 572002
    const/4 v4, 0x0

    .line 572003
    sget-object v5, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->e:LX/0Rf;

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/facebook/ui/media/attachments/MediaResource;->r:Ljava/lang/String;

    invoke-virtual {v5, v7}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 572004
    const/4 v6, 0x0

    move v5, v4

    .line 572005
    :goto_1
    const/4 v4, 0x0

    .line 572006
    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v8, LX/2MK;->VIDEO:LX/2MK;

    if-ne v7, v8, :cond_8

    .line 572007
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->l:LX/2Mb;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, LX/2Mb;->a(Lcom/facebook/ui/media/attachments/MediaResource;)I

    move-result v4

    .line 572008
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->a(I)Z

    move-result v10

    move v7, v4

    .line 572009
    :goto_2
    invoke-direct/range {p0 .. p1}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->m(Lcom/facebook/ui/media/attachments/MediaResource;)Z

    move-result v22

    .line 572010
    invoke-direct/range {p0 .. p1}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->l(Lcom/facebook/ui/media/attachments/MediaResource;)Z

    move-result v4

    if-eqz v4, :cond_9

    if-nez v10, :cond_9

    const/4 v11, 0x1

    .line 572011
    :goto_3
    const/4 v4, 0x0

    .line 572012
    if-eqz v22, :cond_d

    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/facebook/ui/media/attachments/MediaResource;->q:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    if-eqz v8, :cond_d

    if-eqz v6, :cond_d

    .line 572013
    const/16 v8, 0x3c0

    .line 572014
    invoke-virtual/range {p4 .. p4}, Lcom/facebook/messaging/media/photoquality/PhotoQuality;->a()I

    move-result v9

    if-le v9, v8, :cond_d

    move-object/from16 v0, p1

    iget v9, v0, Lcom/facebook/ui/media/attachments/MediaResource;->k:I

    move-object/from16 v0, p1

    iget v12, v0, Lcom/facebook/ui/media/attachments/MediaResource;->l:I

    invoke-static {v9, v12}, Ljava/lang/Math;->max(II)I

    move-result v9

    if-le v9, v8, :cond_d

    .line 572015
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->A:LX/2Ms;

    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/facebook/ui/media/attachments/MediaResource;->q:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v8, v9}, LX/2Ms;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/media/photoquality/PhotoQuality;

    move-result-object v8

    .line 572016
    invoke-virtual {v8}, Lcom/facebook/messaging/media/photoquality/PhotoQuality;->a()I

    move-result v9

    invoke-virtual/range {p4 .. p4}, Lcom/facebook/messaging/media/photoquality/PhotoQuality;->a()I

    move-result v12

    if-ge v9, v12, :cond_d

    .line 572017
    :goto_4
    if-nez v6, :cond_4

    .line 572018
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->t:LX/0TD;

    new-instance v9, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl$5;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v9, v0, v1}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl$5;-><init>(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;)V

    const v12, 0x7201b89f

    invoke-static {v4, v9, v12}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 572019
    :cond_4
    if-eqz v11, :cond_a

    .line 572020
    invoke-static/range {p1 .. p1}, LX/FHy;->a(Lcom/facebook/ui/media/attachments/MediaResource;)Lcom/facebook/ui/media/attachments/MediaResource;

    move-result-object v5

    .line 572021
    sget-object v9, LX/FHx;->PHASE_ONE:LX/FHx;

    move-object/from16 v4, p0

    move/from16 v7, p3

    invoke-static/range {v4 .. v11}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->a(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;ZZLcom/facebook/messaging/media/photoquality/PhotoQuality;LX/FHx;ZZ)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v16

    .line 572022
    const/4 v15, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/ui/media/attachments/MediaResource;->c()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    move-object/from16 v12, p0

    move-object/from16 v13, p1

    move-object/from16 v14, p2

    invoke-direct/range {v12 .. v17}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->a(Lcom/facebook/ui/media/attachments/MediaResource;LX/FHf;ZLcom/google/common/util/concurrent/ListenableFuture;Ljava/lang/Integer;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v17

    .line 572023
    :goto_5
    new-instance v12, LX/FH4;

    move-object/from16 v13, p0

    move-object/from16 v14, p1

    move-object/from16 v15, v21

    move/from16 v16, v11

    move/from16 v18, p3

    move/from16 v19, v10

    move-object/from16 v20, p2

    invoke-direct/range {v12 .. v20}, LX/FH4;-><init>(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;Lcom/google/common/util/concurrent/SettableFuture;ZLcom/google/common/util/concurrent/ListenableFuture;ZZLX/FHf;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->s:Ljava/util/concurrent/Executor;

    move-object/from16 v0, v17

    invoke-static {v0, v12, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 572024
    if-eqz v22, :cond_c

    if-eqz p4, :cond_c

    const/4 v4, 0x1

    .line 572025
    :goto_6
    if-eqz v4, :cond_5

    .line 572026
    sget-object v9, LX/FHx;->PHASE_TWO:LX/FHx;

    const/4 v11, 0x0

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move/from16 v7, p3

    move-object/from16 v8, p4

    invoke-static/range {v4 .. v11}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->a(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;ZZLcom/facebook/messaging/media/photoquality/PhotoQuality;LX/FHx;ZZ)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    .line 572027
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v17

    invoke-static {v0, v1, v4, v2}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->a(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    :cond_5
    move-object/from16 v4, v21

    .line 572028
    goto/16 :goto_0

    .line 572029
    :cond_6
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v7, LX/2MK;->PHOTO:LX/2MK;

    if-ne v5, v7, :cond_7

    invoke-static/range {p1 .. p1}, LX/2Ms;->a(Lcom/facebook/ui/media/attachments/MediaResource;)Z

    move-result v5

    if-eqz v5, :cond_7

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->A:LX/2Ms;

    invoke-virtual {v5}, LX/2Ms;->b()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 572030
    const/4 v6, 0x0

    .line 572031
    const/4 v4, 0x1

    move v5, v4

    goto/16 :goto_1

    .line 572032
    :cond_7
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v7, LX/2MK;->PHOTO:LX/2MK;

    if-ne v5, v7, :cond_e

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->A:LX/2Ms;

    move-object/from16 v0, p1

    move-object/from16 v1, p4

    invoke-virtual {v5, v0, v1}, LX/2Ms;->a(Lcom/facebook/ui/media/attachments/MediaResource;Lcom/facebook/messaging/media/photoquality/PhotoQuality;)Z

    move-result v5

    if-eqz v5, :cond_e

    .line 572033
    const/4 v6, 0x0

    move v5, v4

    goto/16 :goto_1

    .line 572034
    :cond_8
    const/4 v10, 0x0

    move v7, v4

    goto/16 :goto_2

    .line 572035
    :cond_9
    const/4 v11, 0x0

    goto/16 :goto_3

    .line 572036
    :cond_a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->K:LX/0Uh;

    const/16 v9, 0x59f

    const/4 v12, 0x0

    invoke-virtual {v4, v9, v12}, LX/0Uh;->a(IZ)Z

    move-result v4

    if-eqz v4, :cond_b

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->P:Z

    if-eqz v4, :cond_b

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v9, LX/2MK;->VIDEO:LX/2MK;

    if-ne v4, v9, :cond_b

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->v:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/6eZ;

    iget-boolean v4, v4, LX/6eZ;->a:Z

    if-eqz v4, :cond_b

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/ui/media/attachments/MediaResource;->c()I

    move-result v4

    int-to-long v12, v4

    invoke-static {v12, v13}, LX/6bK;->a(J)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 572037
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v2, v10, v7}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->a(Lcom/facebook/ui/media/attachments/MediaResource;LX/FHf;ZI)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v17

    goto/16 :goto_5

    .line 572038
    :cond_b
    sget-object v17, LX/FHx;->PHASE_ONE:LX/FHx;

    const/16 v19, 0x0

    move-object/from16 v12, p0

    move-object/from16 v13, p1

    move v14, v6

    move/from16 v15, p3

    move-object/from16 v16, v8

    move/from16 v18, v10

    invoke-static/range {v12 .. v19}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->a(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;ZZLcom/facebook/messaging/media/photoquality/PhotoQuality;LX/FHx;ZZ)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v16

    .line 572039
    const/16 v17, 0x0

    move-object/from16 v12, p0

    move-object/from16 v13, p1

    move-object/from16 v14, p2

    move v15, v5

    invoke-direct/range {v12 .. v17}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->a(Lcom/facebook/ui/media/attachments/MediaResource;LX/FHf;ZLcom/google/common/util/concurrent/ListenableFuture;Ljava/lang/Integer;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v17

    goto/16 :goto_5

    .line 572040
    :cond_c
    const/4 v4, 0x0

    goto/16 :goto_6

    :cond_d
    move-object/from16 v8, p4

    move-object/from16 p4, v4

    goto/16 :goto_4

    :cond_e
    move v5, v4

    goto/16 :goto_1
.end method

.method public static a$redex0(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;Lcom/facebook/messaging/media/photoquality/PhotoQuality;LX/FHx;ZZ)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            "Lcom/facebook/messaging/media/photoquality/PhotoQuality;",
            "LX/FHx;",
            "ZZ)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 571985
    invoke-static {p1}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->n(Lcom/facebook/ui/media/attachments/MediaResource;)Lcom/facebook/ui/media/attachments/MediaResource;

    move-result-object v3

    .line 571986
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->J:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6eS;

    invoke-virtual {v0, v3}, LX/6eS;->a(Lcom/facebook/ui/media/attachments/MediaResource;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v8

    new-instance v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl$18;

    move-object v1, p0

    move-object v2, p1

    move-object v4, p2

    move-object v5, p3

    move v6, p4

    move v7, p5

    invoke-direct/range {v0 .. v7}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl$18;-><init>(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;Lcom/facebook/ui/media/attachments/MediaResource;Lcom/facebook/messaging/media/photoquality/PhotoQuality;LX/FHx;ZZ)V

    invoke-static {v8, v0}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public static a$redex0(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;Lcom/facebook/ui/media/attachments/MediaResource;ZLX/FHf;Ljava/lang/Integer;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            "Z",
            "LX/FHf;",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/ui/media/attachments/MediaUploadResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 571788
    sget-object v0, LX/FGa;->UPLOADING:LX/FGa;

    invoke-static {p0, p1, v0}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->a$redex0(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;LX/FGa;)V

    .line 571789
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->J:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6eS;

    invoke-virtual {v0, p2}, LX/6eS;->a(Lcom/facebook/ui/media/attachments/MediaResource;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v8

    new-instance v0, LX/FGy;

    move-object v1, p0

    move-object v2, p2

    move v3, p3

    move-object v4, p5

    move-object v5, p6

    move-object v6, p4

    move-object v7, p1

    invoke-direct/range {v0 .. v7}, LX/FGy;-><init>(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;ZLjava/lang/Integer;Ljava/lang/String;LX/FHf;Lcom/facebook/ui/media/attachments/MediaResource;)V

    iget-object v1, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->s:Ljava/util/concurrent/Executor;

    invoke-static {v8, v0, v1}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public static a$redex0(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 571970
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->A:LX/2Ms;

    iget-object v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->q:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0, v1}, LX/2Ms;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/media/photoquality/PhotoQuality;

    move-result-object v4

    .line 571971
    invoke-direct {p0, p1, v4}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->a(Lcom/facebook/ui/media/attachments/MediaResource;Lcom/facebook/messaging/media/photoquality/PhotoQuality;)Z

    move-result v3

    .line 571972
    const/4 v6, 0x0

    .line 571973
    iget-object v0, p1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v1, LX/2MK;->VIDEO:LX/2MK;

    if-ne v0, v1, :cond_0

    .line 571974
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->l:LX/2Mb;

    invoke-virtual {v0, p1}, LX/2Mb;->a(Lcom/facebook/ui/media/attachments/MediaResource;)I

    move-result v0

    .line 571975
    invoke-direct {p0, v0}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->a(I)Z

    move-result v6

    .line 571976
    :cond_0
    sget-object v5, LX/FHx;->PHASE_TWO:LX/FHx;

    move-object v0, p0

    move-object v1, p1

    move v7, v2

    invoke-static/range {v0 .. v7}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->a(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;ZZLcom/facebook/messaging/media/photoquality/PhotoQuality;LX/FHx;ZZ)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 571977
    new-instance v1, LX/FH7;

    invoke-direct {v1, p0, p1, p2}, LX/FH7;-><init>(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->t:LX/0TD;

    invoke-static {v0, v1, v2}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 571978
    return-object v0
.end method

.method public static a$redex0(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;Landroid/os/Bundle;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            "Ljava/lang/String;",
            "Landroid/os/Bundle;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/ui/media/attachments/MediaUploadResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 571967
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->f:LX/0aG;

    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    sget-object v1, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->a:Ljava/lang/Class;

    const-string v2, "media_upload"

    invoke-static {v1, v2}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    const v5, -0x73974b9d

    move-object v1, p2

    move-object v2, p3

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 571968
    iget-object v1, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->j:LX/2MT;

    invoke-virtual {v1, p1, v0}, LX/2MT;->a(Lcom/facebook/ui/media/attachments/MediaResource;LX/1ML;)V

    .line 571969
    new-instance v1, LX/FGz;

    invoke-direct {v1, p0}, LX/FGz;-><init>(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;)V

    iget-object v2, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->s:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public static a$redex0(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;LX/FGa;)V
    .locals 3

    .prologue
    .line 571959
    invoke-static {p1}, LX/6ed;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/6ed;

    move-result-object v0

    .line 571960
    iget-object v1, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->i:LX/2MR;

    invoke-virtual {v1, v0}, LX/2MR;->a(LX/6ed;)LX/FGc;

    move-result-object v1

    .line 571961
    if-nez v1, :cond_0

    .line 571962
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->o:LX/03V;

    const-string v1, "MediaUploadManagerImpl_MISSING_UPLOAD_STATUS"

    const-string v2, "Missing status for in progress media resource"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 571963
    :goto_0
    return-void

    .line 571964
    :cond_0
    iget-object v1, v1, LX/FGc;->d:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {p2, v1}, LX/FGc;->a(LX/FGa;Lcom/google/common/util/concurrent/ListenableFuture;)LX/FGc;

    move-result-object v1

    .line 571965
    iget-object v2, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->i:LX/2MR;

    invoke-virtual {v2, v0, v1}, LX/2MR;->a(LX/6ed;LX/FGc;)V

    .line 571966
    invoke-static {p0}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->d(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;)V

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/google/common/util/concurrent/SettableFuture;Lcom/facebook/ui/media/attachments/MediaResource;Lcom/facebook/ui/media/attachments/MediaUploadResult;LX/FH8;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<",
            "LX/FGc;",
            ">;",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            "Lcom/facebook/ui/media/attachments/MediaUploadResult;",
            "LX/FH8;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 571790
    iget-object v0, p2, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v1, LX/2MK;->ENCRYPTED_PHOTO:LX/2MK;

    if-ne v0, v1, :cond_0

    .line 571791
    invoke-direct {p0, p2, p3}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->a(Lcom/facebook/ui/media/attachments/MediaResource;Lcom/facebook/ui/media/attachments/MediaUploadResult;)V

    .line 571792
    :cond_0
    iget-object v0, p3, Lcom/facebook/ui/media/attachments/MediaUploadResult;->a:Ljava/lang/String;

    move-object v0, v0

    .line 571793
    invoke-static {p3}, LX/FGc;->a(Lcom/facebook/ui/media/attachments/MediaUploadResult;)LX/FGc;

    move-result-object v1

    .line 571794
    invoke-static {p2}, LX/6ed;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/6ed;

    move-result-object v2

    .line 571795
    iget-object v3, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->i:LX/2MR;

    invoke-virtual {v3, v2, v1}, LX/2MR;->b(LX/6ed;LX/FGc;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 571796
    invoke-static {p0}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->d(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;)V

    .line 571797
    const/4 v2, 0x1

    .line 571798
    :goto_0
    move v2, v2

    .line 571799
    if-nez v2, :cond_1

    .line 571800
    invoke-virtual {p0, p2}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->d(Lcom/facebook/ui/media/attachments/MediaResource;)LX/FGc;

    move-result-object v1

    .line 571801
    invoke-virtual {v1}, LX/FGc;->a()Ljava/lang/String;

    move-result-object v1

    .line 571802
    iget-object v2, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->g:LX/2ML;

    invoke-virtual {v2, p2}, LX/2ML;->a(Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/lang/String;

    .line 571803
    iget-object v2, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->p:LX/2Ml;

    invoke-virtual {p4}, LX/FH8;->toString()Ljava/lang/String;

    move-result-object v3

    .line 571804
    invoke-static {v2, p2}, LX/2Ml;->b(LX/2Ml;Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/util/Map;

    move-result-object p0

    .line 571805
    const-string p1, "media_fbid_first_returned"

    invoke-interface {p0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 571806
    const-string p1, "media_fbid_second_returned"

    invoke-interface {p0, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 571807
    const-string p1, "second_fbid_creation_path"

    invoke-interface {p0, p1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 571808
    const-string p1, "messenger_dup_media_fbid_returned"

    invoke-static {v2, p1, p0}, LX/2Ml;->a(LX/2Ml;Ljava/lang/String;Ljava/util/Map;)V

    .line 571809
    const/4 v0, 0x0

    .line 571810
    :goto_1
    return v0

    .line 571811
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->q:LX/2Mm;

    invoke-virtual {p4}, LX/FH8;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, p2, v2}, LX/2Mm;->a(Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;)V

    .line 571812
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->r:LX/2Mn;

    invoke-virtual {v0, p2}, LX/2Mn;->b(Lcom/facebook/ui/media/attachments/MediaResource;)V

    .line 571813
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->n:LX/0Xl;

    invoke-static {}, LX/FGn;->a()Landroid/content/Intent;

    move-result-object v2

    invoke-interface {v0, v2}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 571814
    const v0, -0x59c0e964

    invoke-static {p1, v1, v0}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 571815
    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static b(LX/0QB;)Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;
    .locals 35

    .prologue
    .line 571816
    new-instance v3, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    invoke-static/range {p0 .. p0}, LX/0aF;->getInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v4

    check-cast v4, LX/0aG;

    invoke-static/range {p0 .. p0}, LX/2ML;->a(LX/0QB;)LX/2ML;

    move-result-object v5

    check-cast v5, LX/2ML;

    invoke-static/range {p0 .. p0}, LX/2MO;->a(LX/0QB;)LX/2MO;

    move-result-object v6

    check-cast v6, LX/2MO;

    invoke-static/range {p0 .. p0}, LX/2MR;->a(LX/0QB;)LX/2MR;

    move-result-object v7

    check-cast v7, LX/2MR;

    invoke-static/range {p0 .. p0}, LX/2MT;->a(LX/0QB;)LX/2MT;

    move-result-object v8

    check-cast v8, LX/2MT;

    invoke-static/range {p0 .. p0}, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->a(LX/0QB;)Lcom/facebook/ui/media/attachments/MediaResourceHelper;

    move-result-object v9

    check-cast v9, Lcom/facebook/ui/media/attachments/MediaResourceHelper;

    invoke-static/range {p0 .. p0}, LX/2Mb;->a(LX/0QB;)LX/2Mb;

    move-result-object v10

    check-cast v10, LX/2Mb;

    invoke-static/range {p0 .. p0}, LX/2Mk;->a(LX/0QB;)LX/2Mk;

    move-result-object v11

    check-cast v11, LX/2Mk;

    invoke-static/range {p0 .. p0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v12

    check-cast v12, LX/0Xl;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v13

    check-cast v13, LX/03V;

    invoke-static/range {p0 .. p0}, LX/2Ml;->a(LX/0QB;)LX/2Ml;

    move-result-object v14

    check-cast v14, LX/2Ml;

    invoke-static/range {p0 .. p0}, LX/2Mm;->a(LX/0QB;)LX/2Mm;

    move-result-object v15

    check-cast v15, LX/2Mm;

    invoke-static/range {p0 .. p0}, LX/2Mn;->a(LX/0QB;)LX/2Mn;

    move-result-object v16

    check-cast v16, LX/2Mn;

    invoke-static/range {p0 .. p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v17

    check-cast v17, Ljava/util/concurrent/Executor;

    invoke-static/range {p0 .. p0}, LX/0Zo;->a(LX/0QB;)LX/0Tf;

    move-result-object v18

    check-cast v18, LX/0TD;

    invoke-static/range {p0 .. p0}, LX/0Zo;->a(LX/0QB;)LX/0Tf;

    move-result-object v19

    check-cast v19, Ljava/util/concurrent/ScheduledExecutorService;

    const/16 v20, 0x2805

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v20

    invoke-static/range {p0 .. p0}, LX/FHp;->a(LX/0QB;)LX/FHp;

    move-result-object v21

    check-cast v21, LX/FHp;

    invoke-static/range {p0 .. p0}, LX/2Mp;->a(LX/0QB;)LX/2Mp;

    move-result-object v22

    check-cast v22, LX/2Mp;

    invoke-static/range {p0 .. p0}, LX/0V7;->a(LX/0QB;)LX/0V8;

    move-result-object v23

    check-cast v23, LX/0V8;

    invoke-static/range {p0 .. p0}, LX/2Mq;->a(LX/0QB;)LX/2Mq;

    move-result-object v24

    check-cast v24, LX/2Mq;

    invoke-static/range {p0 .. p0}, LX/2Ms;->a(LX/0QB;)LX/2Ms;

    move-result-object v25

    check-cast v25, LX/2Ms;

    invoke-static/range {p0 .. p0}, LX/2N3;->a(LX/0QB;)LX/2N3;

    move-result-object v26

    check-cast v26, LX/2N3;

    const/16 v27, 0x27dc

    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v27

    invoke-static/range {p0 .. p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v28

    check-cast v28, LX/0Uh;

    invoke-static/range {p0 .. p0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v29

    check-cast v29, LX/0W3;

    invoke-static/range {p0 .. p0}, LX/2Nw;->a(LX/0QB;)LX/2Nw;

    move-result-object v30

    check-cast v30, LX/2Nw;

    invoke-static/range {p0 .. p0}, LX/2Nx;->a(LX/0QB;)LX/2Nx;

    move-result-object v31

    check-cast v31, LX/2Nx;

    invoke-static/range {p0 .. p0}, LX/2OB;->a(LX/0QB;)LX/2OB;

    move-result-object v32

    check-cast v32, LX/2OB;

    const-class v33, Landroid/content/Context;

    const-class v34, Lcom/facebook/inject/ForAppContext;

    move-object/from16 v0, p0

    move-object/from16 v1, v33

    move-object/from16 v2, v34

    invoke-interface {v0, v1, v2}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v33

    check-cast v33, Landroid/content/Context;

    invoke-static/range {p0 .. p0}, LX/2OC;->a(LX/0QB;)LX/2OC;

    move-result-object v34

    check-cast v34, LX/2OC;

    invoke-direct/range {v3 .. v34}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;-><init>(LX/0aG;LX/2ML;LX/2MO;LX/2MR;LX/2MT;Lcom/facebook/ui/media/attachments/MediaResourceHelper;LX/2Mb;LX/2Mk;LX/0Xl;LX/03V;LX/2Ml;LX/2Mm;LX/2Mn;Ljava/util/concurrent/Executor;LX/0TD;Ljava/util/concurrent/ScheduledExecutorService;LX/0Or;LX/FHp;LX/2Mp;LX/0V8;LX/2Mq;LX/2Ms;LX/2N3;LX/0Ot;LX/0Uh;LX/0W3;LX/2Nw;LX/2Nx;LX/2OB;Landroid/content/Context;LX/2OC;)V

    .line 571817
    return-object v3
.end method

.method public static b(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;LX/FHf;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            "LX/FHf;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/FGc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 571570
    sget-object v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->b:LX/0Rf;

    iget-object v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    invoke-virtual {v0, v1}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 571571
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->A:LX/2Ms;

    iget-object v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->q:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0, v1}, LX/2Ms;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/media/photoquality/PhotoQuality;

    move-result-object v5

    .line 571572
    invoke-direct {p0, p1, v5}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->a(Lcom/facebook/ui/media/attachments/MediaResource;Lcom/facebook/messaging/media/photoquality/PhotoQuality;)Z

    move-result v4

    .line 571573
    invoke-virtual {p0, p1}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->d(Lcom/facebook/ui/media/attachments/MediaResource;)LX/FGc;

    move-result-object v0

    .line 571574
    if-eqz v0, :cond_0

    if-eqz v4, :cond_0

    .line 571575
    iget-object v1, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->j:LX/2MT;

    invoke-virtual {v1, p1}, LX/2MT;->a(Lcom/facebook/ui/media/attachments/MediaResource;)V

    .line 571576
    iget-object v1, v0, LX/FGc;->b:LX/FGb;

    sget-object v2, LX/FGb;->SUCCEEDED:LX/FGb;

    if-ne v1, v2, :cond_2

    .line 571577
    iget-object v1, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->q:LX/2Mm;

    sget-object v2, LX/FH8;->SKIPPED_FROM_CACHE:LX/FH8;

    invoke-virtual {v2}, LX/FH8;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, LX/2Mm;->a(Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;)V

    .line 571578
    iget-object v1, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->r:LX/2Mn;

    invoke-virtual {v1, p1}, LX/2Mn;->b(Lcom/facebook/ui/media/attachments/MediaResource;)V

    .line 571579
    iget-object v1, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->g:LX/2ML;

    invoke-virtual {v1, p1}, LX/2ML;->a(Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/lang/String;

    invoke-virtual {v0}, LX/FGc;->a()Ljava/lang/String;

    .line 571580
    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 571581
    :goto_0
    move-object v0, v1

    .line 571582
    if-eqz v0, :cond_0

    .line 571583
    :goto_1
    return-object v0

    .line 571584
    :cond_0
    invoke-static {p1}, LX/5zt;->b(Lcom/facebook/ui/media/attachments/MediaResource;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->L:LX/0W3;

    sget-wide v2, LX/0X5;->kt:J

    invoke-interface {v0, v2, v3}, LX/0W4;->a(J)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 571585
    invoke-direct {p0, p1}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->g(Lcom/facebook/ui/media/attachments/MediaResource;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    new-instance v0, LX/FH1;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, LX/FH1;-><init>(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;LX/FHf;ZLcom/facebook/messaging/media/photoquality/PhotoQuality;)V

    iget-object v1, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->s:Ljava/util/concurrent/Executor;

    invoke-static {v6, v0, v1}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_1

    .line 571586
    :cond_1
    invoke-static {p0, p1, p2, v4, v5}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->a$redex0(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;LX/FHf;ZLcom/facebook/messaging/media/photoquality/PhotoQuality;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_1

    .line 571587
    :cond_2
    iget-object v1, v0, LX/FGc;->b:LX/FGb;

    sget-object v2, LX/FGb;->IN_PROGRESS:LX/FGb;

    if-ne v1, v2, :cond_3

    .line 571588
    iget-object v1, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->q:LX/2Mm;

    sget-object v2, LX/FH8;->SKIPPED_FROM_CACHE:LX/FH8;

    invoke-virtual {v2}, LX/FH8;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, LX/FGc;->e:LX/FGa;

    invoke-virtual {v3}, LX/FGa;->toString()Ljava/lang/String;

    move-result-object v3

    .line 571589
    invoke-static {p1}, LX/2Mm;->b(Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/util/Map;

    move-result-object v6

    .line 571590
    const-string v7, "result_path"

    invoke-interface {v6, v7, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 571591
    const-string v7, "progress_stage"

    invoke-interface {v6, v7, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 571592
    const-string v7, "messenger_media_upload_request_in_progress"

    invoke-static {v1, v7, v6}, LX/2Mm;->a(LX/2Mm;Ljava/lang/String;Ljava/util/Map;)V

    .line 571593
    iget-object v1, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->r:LX/2Mn;

    .line 571594
    invoke-static {p1}, LX/FHE;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/FHE;

    move-result-object v3

    .line 571595
    iget-object v2, v1, LX/2Mn;->d:LX/0QI;

    invoke-interface {v2, v3}, LX/0QI;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/FHF;

    .line 571596
    if-nez v2, :cond_4

    .line 571597
    :goto_2
    iget-object v1, v0, LX/FGc;->d:Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0

    .line 571598
    :cond_3
    const/4 v1, 0x0

    goto :goto_0

    .line 571599
    :cond_4
    iget-object v6, v2, LX/FHF;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 571600
    iget-object v2, v2, LX/FHF;->b:LX/0SW;

    .line 571601
    const-string v7, "preparation_finish"

    invoke-static {v6, v7, v2}, LX/2Mn;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/0SW;)V

    .line 571602
    const-string v2, "completion_status"

    sget-object v7, LX/FHH;->in_progress:LX/FHH;

    invoke-virtual {v7}, LX/FHH;->name()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v2, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 571603
    invoke-static {v1, v3, v6}, LX/2Mn;->a(LX/2Mn;LX/FHE;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    goto :goto_2
.end method

.method public static b(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 571660
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->M:LX/2OC;

    invoke-virtual {v0, p2}, LX/2OC;->b(Ljava/lang/String;)V

    .line 571661
    invoke-direct {p0, p1, p2}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->c(Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 571662
    new-instance v1, LX/FGo;

    invoke-direct {v1, p0}, LX/FGo;-><init>(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;)V

    .line 571663
    sget-object v2, LX/131;->INSTANCE:LX/131;

    move-object v2, v2

    .line 571664
    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 571665
    new-instance v1, LX/FGp;

    invoke-direct {v1, p0, p1, p2}, LX/FGp;-><init>(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->t:LX/0TD;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 571666
    return-object v0
.end method

.method private b(Lcom/facebook/ui/media/attachments/MediaResource;LX/FGc;)V
    .locals 2

    .prologue
    .line 571656
    invoke-static {p1}, LX/6ed;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/6ed;

    move-result-object v0

    .line 571657
    iget-object v1, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->i:LX/2MR;

    invoke-virtual {v1, v0, p2}, LX/2MR;->a(LX/6ed;LX/FGc;)V

    .line 571658
    invoke-static {p0}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->d(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;)V

    .line 571659
    return-void
.end method

.method private c(Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/ui/media/attachments/MediaUploadResult;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 571637
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->h:LX/2MO;

    invoke-virtual {v0, p1}, LX/2MO;->b(Lcom/facebook/ui/media/attachments/MediaResource;)Lcom/facebook/ui/media/attachments/MediaResource;

    move-result-object v0

    .line 571638
    if-nez v0, :cond_0

    .line 571639
    invoke-static {v2}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 571640
    :goto_0
    return-object v0

    .line 571641
    :cond_0
    iget-object v1, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->B:LX/2N3;

    invoke-virtual {v1, p1}, LX/2N3;->b(Lcom/facebook/ui/media/attachments/MediaResource;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 571642
    invoke-static {v2}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0

    .line 571643
    :cond_1
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 571644
    const-string v2, "mediaResource"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 571645
    const-string v0, "fullQualityImageUpload"

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 571646
    const-string v0, "originalFbid"

    invoke-virtual {v1, v0, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 571647
    const/4 v2, 0x0

    .line 571648
    const-string v0, "photo_upload_hires"

    .line 571649
    iget-object v3, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->K:LX/0Uh;

    const/16 p2, 0x17a

    invoke-virtual {v3, p2, v2}, LX/0Uh;->a(IZ)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 571650
    iget-boolean v0, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->N:Z

    if-eqz v0, :cond_4

    const-string v0, "photo_upload_hires_parallel"

    .line 571651
    :goto_1
    iget-boolean v3, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->N:Z

    if-nez v3, :cond_2

    const/4 v2, 0x1

    :cond_2
    iput-boolean v2, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->N:Z

    .line 571652
    :cond_3
    move-object v0, v0

    .line 571653
    invoke-static {p1}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->n(Lcom/facebook/ui/media/attachments/MediaResource;)Lcom/facebook/ui/media/attachments/MediaResource;

    move-result-object v2

    .line 571654
    invoke-static {p0, v2, v0, v1}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->a$redex0(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;Landroid/os/Bundle;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0

    .line 571655
    :cond_4
    const-string v0, "photo_upload_hires"

    goto :goto_1
.end method

.method public static d(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;)V
    .locals 3

    .prologue
    .line 571635
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->s:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl$24;

    invoke-direct {v1, p0}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl$24;-><init>(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;)V

    const v2, -0x327f8d9a

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 571636
    return-void
.end method

.method private f(Lcom/facebook/ui/media/attachments/MediaResource;)V
    .locals 6

    .prologue
    .line 571622
    invoke-static {p1}, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->a(Lcom/facebook/ui/media/attachments/MediaResource;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 571623
    :cond_0
    :goto_0
    return-void

    .line 571624
    :cond_1
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 571625
    iget-object v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->r:Ljava/lang/String;

    if-nez v1, :cond_2

    .line 571626
    const-string v1, "mime-type"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 571627
    :cond_2
    sget-object v1, LX/FH0;->a:[I

    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    invoke-virtual {v2}, LX/2MK;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 571628
    :cond_3
    :goto_1
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 571629
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Missing: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ","

    invoke-static {v2}, LX/0PO;->on(Ljava/lang/String;)LX/0PO;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/0PO;->join(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 571630
    iget-object v1, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->o:LX/03V;

    const-string v2, "MediaUploadManager_missing_metadata"

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 571631
    :pswitch_0
    iget v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->k:I

    if-eqz v1, :cond_4

    iget v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->l:I

    if-nez v1, :cond_3

    .line 571632
    :cond_4
    const-string v1, "size"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 571633
    :pswitch_1
    iget-wide v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->j:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-gtz v1, :cond_3

    .line 571634
    const-string v1, "duration"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private g(Lcom/facebook/ui/media/attachments/MediaResource;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            ">;"
        }
    .end annotation

    .prologue
    .line 571615
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->t:LX/0TD;

    new-instance v1, LX/FH2;

    invoke-direct {v1, p0, p1}, LX/FH2;-><init>(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 571616
    const-class v1, Ljava/lang/Exception;

    new-instance v2, LX/FH3;

    invoke-direct {v2, p0, p1}, LX/FH3;-><init>(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;)V

    .line 571617
    new-instance p0, Lcom/google/common/util/concurrent/Futures$CatchingFuture;

    invoke-direct {p0, v0, v1, v2}, Lcom/google/common/util/concurrent/Futures$CatchingFuture;-><init>(Lcom/google/common/util/concurrent/ListenableFuture;Ljava/lang/Class;LX/0QK;)V

    .line 571618
    sget-object p1, LX/131;->INSTANCE:LX/131;

    move-object p1, p1

    .line 571619
    invoke-interface {v0, p0, p1}, Lcom/google/common/util/concurrent/ListenableFuture;->addListener(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 571620
    move-object v0, p0

    .line 571621
    return-object v0
.end method

.method public static h(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;)V
    .locals 12

    .prologue
    .line 571609
    new-instance v0, Landroid/content/Intent;

    const-string v1, "EncryptedPhotoUploadStatusAction"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 571610
    const-string v1, "EncryptedPhotoUploadStatusKey"

    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->p:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    const/4 v8, 0x0

    .line 571611
    new-instance v4, Lcom/facebook/messaging/media/upload/EncryptedPhotoUploadResult;

    sget-object v7, LX/FGR;->Failure:LX/FGR;

    move-object v5, v2

    move-object v6, v3

    move-object v9, v8

    move-object v10, v8

    move-object v11, v8

    invoke-direct/range {v4 .. v11}, Lcom/facebook/messaging/media/upload/EncryptedPhotoUploadResult;-><init>(Ljava/lang/String;Landroid/net/Uri;LX/FGR;Ljava/lang/String;[BLjava/lang/String;Ljava/lang/Long;)V

    move-object v2, v4

    .line 571612
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 571613
    iget-object v1, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->n:LX/0Xl;

    invoke-interface {v1, v0}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 571614
    return-void
.end method

.method public static k(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;)Lcom/facebook/ui/media/attachments/MediaResource;
    .locals 2

    .prologue
    .line 571604
    sget-object v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->d:LX/0Rf;

    iget-object v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    invoke-virtual {v0, v1}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 571605
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->h:LX/2MO;

    invoke-virtual {v0, p1}, LX/2MO;->a(Lcom/facebook/ui/media/attachments/MediaResource;)Lcom/facebook/ui/media/attachments/MediaResource;

    move-result-object v0

    .line 571606
    if-nez v0, :cond_1

    .line 571607
    :cond_0
    :goto_0
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 571608
    return-object p1

    :cond_1
    move-object p1, v0

    goto :goto_0
.end method

.method private l(Lcom/facebook/ui/media/attachments/MediaResource;)Z
    .locals 2

    .prologue
    .line 571567
    iget-object v0, p1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v1, LX/2MK;->VIDEO:LX/2MK;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->B:LX/2N3;

    invoke-virtual {v0, p1}, LX/2N3;->a(Lcom/facebook/ui/media/attachments/MediaResource;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/ui/media/attachments/MediaResource;->c()I

    move-result v0

    .line 571568
    const/16 v1, 0x2710

    if-lt v0, v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 571569
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private m(Lcom/facebook/ui/media/attachments/MediaResource;)Z
    .locals 2

    .prologue
    .line 571667
    iget-object v0, p1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v1, LX/2MK;->PHOTO:LX/2MK;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->B:LX/2N3;

    invoke-virtual {v0, p1}, LX/2N3;->a(Lcom/facebook/ui/media/attachments/MediaResource;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static n(Lcom/facebook/ui/media/attachments/MediaResource;)Lcom/facebook/ui/media/attachments/MediaResource;
    .locals 1

    .prologue
    .line 571668
    iget-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->i:Lcom/facebook/ui/media/attachments/MediaResource;

    if-nez v0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    iget-object p0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->i:Lcom/facebook/ui/media/attachments/MediaResource;

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/messages/Message;)V
    .locals 5

    .prologue
    .line 571669
    invoke-static {p1}, LX/2Mk;->c(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 571670
    :cond_0
    return-void

    .line 571671
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->y:LX/2Mq;

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    const-string v2, "has_attachments"

    invoke-virtual {v0, v1, v2}, LX/2Mq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 571672
    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->t:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    .line 571673
    new-instance v4, LX/5zn;

    invoke-direct {v4}, LX/5zn;-><init>()V

    invoke-virtual {v4, v0}, LX/5zn;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/5zn;

    move-result-object v0

    iget-object v4, p1, Lcom/facebook/messaging/model/messages/Message;->F:Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    .line 571674
    iput-object v4, v0, LX/5zn;->y:Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    .line 571675
    move-object v0, v0

    .line 571676
    invoke-virtual {v0}, LX/5zn;->G()Lcom/facebook/ui/media/attachments/MediaResource;

    move-result-object v0

    .line 571677
    invoke-direct {p0, v0}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->f(Lcom/facebook/ui/media/attachments/MediaResource;)V

    .line 571678
    sget-object v4, LX/FHf;->UPLOAD:LX/FHf;

    invoke-static {p0, v0, v4}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->b(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;LX/FHf;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 571679
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public final b(Lcom/facebook/messaging/model/messages/Message;)LX/FHZ;
    .locals 6

    .prologue
    .line 571680
    invoke-static {p1}, LX/2Mk;->c(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 571681
    new-instance v0, LX/FHZ;

    sget-object v1, LX/FGc;->a:LX/FGc;

    sget-object v2, LX/FHY;->NO_MEDIA_ITEMS:LX/FHY;

    invoke-direct {v0, v1, v2}, LX/FHZ;-><init>(LX/FGc;LX/FHY;)V

    move-object v0, v0

    .line 571682
    :goto_0
    return-object v0

    .line 571683
    :cond_0
    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->t:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_4

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    .line 571684
    invoke-virtual {p0, v0}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->d(Lcom/facebook/ui/media/attachments/MediaResource;)LX/FGc;

    move-result-object v0

    .line 571685
    iget-object v4, v0, LX/FGc;->b:LX/FGb;

    sget-object v5, LX/FGb;->SUCCEEDED:LX/FGb;

    if-eq v4, v5, :cond_3

    .line 571686
    iget-object v1, v0, LX/FGc;->b:LX/FGb;

    sget-object v2, LX/FGb;->NOT_ACTIVE:LX/FGb;

    if-ne v1, v2, :cond_1

    .line 571687
    new-instance v1, LX/FHZ;

    sget-object v2, LX/FHY;->NOT_ALL_STARTED:LX/FHY;

    invoke-direct {v1, v0, v2}, LX/FHZ;-><init>(LX/FGc;LX/FHY;)V

    move-object v0, v1

    .line 571688
    goto :goto_0

    .line 571689
    :cond_1
    iget-object v1, v0, LX/FGc;->b:LX/FGb;

    sget-object v2, LX/FGb;->IN_PROGRESS:LX/FGb;

    if-ne v1, v2, :cond_2

    .line 571690
    new-instance v1, LX/FHZ;

    sget-object v2, LX/FHY;->IN_PROGRESS:LX/FHY;

    invoke-direct {v1, v0, v2}, LX/FHZ;-><init>(LX/FGc;LX/FHY;)V

    move-object v0, v1

    .line 571691
    goto :goto_0

    .line 571692
    :cond_2
    new-instance v1, LX/FHZ;

    sget-object v2, LX/FHY;->FAILED:LX/FHY;

    invoke-direct {v1, v0, v2}, LX/FHZ;-><init>(LX/FGc;LX/FHY;)V

    move-object v0, v1

    .line 571693
    goto :goto_0

    .line 571694
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 571695
    :cond_4
    new-instance v0, LX/FHZ;

    sget-object v1, LX/FGc;->a:LX/FGc;

    sget-object v2, LX/FHY;->SUCCEEDED:LX/FHY;

    invoke-direct {v0, v1, v2}, LX/FHZ;-><init>(LX/FGc;LX/FHY;)V

    move-object v0, v0

    .line 571696
    goto :goto_0
.end method

.method public final b(Lcom/facebook/ui/media/attachments/MediaResource;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/FGc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 571697
    sget-object v0, LX/FHf;->UPLOAD:LX/FHf;

    invoke-static {p0, p1, v0}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->a$redex0(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;LX/FHf;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final c(Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/model/messages/Message;
    .locals 11

    .prologue
    .line 571698
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->t:LX/0Px;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->t:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 571699
    :cond_0
    :goto_0
    return-object p1

    .line 571700
    :cond_1
    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->F:Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    .line 571701
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 571702
    iget-object v5, p1, Lcom/facebook/messaging/model/messages/Message;->t:LX/0Px;

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    const/4 v0, 0x0

    move v3, v0

    :goto_1
    if-ge v3, v6, :cond_5

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    .line 571703
    invoke-virtual {p0, v0}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->d(Lcom/facebook/ui/media/attachments/MediaResource;)LX/FGc;

    move-result-object v7

    .line 571704
    iget-object v1, v7, LX/FGc;->b:LX/FGb;

    sget-object v8, LX/FGb;->SUCCEEDED:LX/FGb;

    if-ne v1, v8, :cond_4

    .line 571705
    iget-object v1, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->h:LX/2MO;

    invoke-virtual {v1, v0}, LX/2MO;->a(Lcom/facebook/ui/media/attachments/MediaResource;)Lcom/facebook/ui/media/attachments/MediaResource;

    move-result-object v1

    .line 571706
    if-eqz v1, :cond_2

    move-object v0, v1

    .line 571707
    :cond_2
    invoke-static {}, Lcom/facebook/ui/media/attachments/MediaResource;->a()LX/5zn;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/5zn;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/5zn;

    move-result-object v0

    iget-object v1, v7, LX/FGc;->c:Lcom/facebook/ui/media/attachments/MediaUploadResult;

    .line 571708
    iput-object v1, v0, LX/5zn;->w:Lcom/facebook/ui/media/attachments/MediaUploadResult;

    .line 571709
    move-object v0, v0

    .line 571710
    invoke-virtual {v0}, LX/5zn;->G()Lcom/facebook/ui/media/attachments/MediaResource;

    move-result-object v1

    .line 571711
    if-nez v2, :cond_6

    iget-object v0, v1, Lcom/facebook/ui/media/attachments/MediaResource;->A:Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    if-eqz v0, :cond_6

    .line 571712
    iget-object v0, v1, Lcom/facebook/ui/media/attachments/MediaResource;->A:Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    :goto_2
    move-object v2, v0

    move-object v0, v1

    .line 571713
    :cond_3
    :goto_3
    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 571714
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 571715
    :cond_4
    iget-object v1, v7, LX/FGc;->b:LX/FGb;

    sget-object v8, LX/FGb;->FAILED:LX/FGb;

    if-eq v1, v8, :cond_3

    .line 571716
    iget-object v1, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->o:LX/03V;

    const-string v8, "MESSENGER_MEDIA_UPLOAD_NOT_FINISHED"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Media upload state is: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, v7, LX/FGc;->b:LX/FGb;

    invoke-virtual {v7}, LX/FGb;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v8, v7}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 571717
    :cond_5
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 571718
    invoke-static {}, Lcom/facebook/messaging/model/messages/Message;->newBuilder()LX/6f7;

    move-result-object v1

    invoke-virtual {v1, p1}, LX/6f7;->a(Lcom/facebook/messaging/model/messages/Message;)LX/6f7;

    move-result-object v1

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 571719
    if-nez v2, :cond_7

    .line 571720
    :goto_4
    move-object v2, v3

    .line 571721
    iput-object v2, v1, LX/6f7;->F:Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    .line 571722
    move-object v1, v1

    .line 571723
    iput-object v0, v1, LX/6f7;->r:Ljava/util/List;

    .line 571724
    move-object v0, v1

    .line 571725
    invoke-virtual {v0}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    move-result-object p1

    goto/16 :goto_0

    :cond_6
    move-object v0, v2

    goto :goto_2

    .line 571726
    :cond_7
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    if-eq v4, v6, :cond_8

    .line 571727
    sget-object v4, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->a:Ljava/lang/Class;

    const-string v5, "Messages with content attribution should have only one MediaResource: %s"

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v0, v6, v7

    invoke-static {v4, v5, v6}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_4

    .line 571728
    :cond_8
    invoke-static {}, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->newBuilder()LX/5dd;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/5dd;->a(Lcom/facebook/messaging/model/attribution/ContentAppAttribution;)LX/5dd;

    move-result-object v4

    invoke-virtual {v0, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-virtual {v3}, Lcom/facebook/ui/media/attachments/MediaResource;->b()Ljava/lang/String;

    move-result-object v3

    .line 571729
    iput-object v3, v4, LX/5dd;->a:Ljava/lang/String;

    .line 571730
    move-object v3, v4

    .line 571731
    invoke-virtual {v3}, LX/5dd;->k()Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    move-result-object v3

    goto :goto_4
.end method

.method public final clearUserData()V
    .locals 1

    .prologue
    .line 571732
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->H:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->c()V

    .line 571733
    return-void
.end method

.method public final d(Lcom/facebook/ui/media/attachments/MediaResource;)LX/FGc;
    .locals 2

    .prologue
    .line 571734
    if-nez p1, :cond_1

    .line 571735
    sget-object v0, LX/FGc;->a:LX/FGc;

    .line 571736
    :cond_0
    :goto_0
    return-object v0

    .line 571737
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/ui/media/attachments/MediaResource;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 571738
    iget-object v0, p1, Lcom/facebook/ui/media/attachments/MediaResource;->x:Lcom/facebook/ui/media/attachments/MediaUploadResult;

    invoke-static {v0}, LX/FGc;->a(Lcom/facebook/ui/media/attachments/MediaUploadResult;)LX/FGc;

    move-result-object v0

    goto :goto_0

    .line 571739
    :cond_2
    invoke-static {p1}, LX/6ed;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/6ed;

    move-result-object v0

    .line 571740
    iget-object v1, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->i:LX/2MR;

    invoke-virtual {v1, v0}, LX/2MR;->a(LX/6ed;)LX/FGc;

    move-result-object v0

    .line 571741
    if-nez v0, :cond_0

    sget-object v0, LX/FGc;->a:LX/FGc;

    goto :goto_0
.end method

.method public final e(Lcom/facebook/ui/media/attachments/MediaResource;)D
    .locals 4
    .param p1    # Lcom/facebook/ui/media/attachments/MediaResource;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 571742
    const-wide/16 v2, 0x0

    .line 571743
    if-eqz p1, :cond_0

    .line 571744
    invoke-static {p1}, LX/6ed;->b(Lcom/facebook/ui/media/attachments/MediaResource;)LX/6ed;

    move-result-object v0

    .line 571745
    iget-object v1, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->G:LX/0QI;

    invoke-interface {v1, v0}, LX/0QI;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    .line 571746
    if-eqz v0, :cond_0

    .line 571747
    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    .line 571748
    :goto_0
    return-wide v0

    :cond_0
    move-wide v0, v2

    goto :goto_0
.end method

.method public final declared-synchronized init()V
    .locals 6

    .prologue
    .line 571749
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->M:LX/2OC;

    .line 571750
    iget-object v1, v0, LX/2OC;->b:Lcom/facebook/compactdisk/DiskCache;

    if-nez v1, :cond_3

    .line 571751
    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    .line 571752
    :cond_0
    :goto_0
    move-object v0, v2

    .line 571753
    if-eqz v0, :cond_2

    .line 571754
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FHa;

    .line 571755
    iget-object v2, p0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->M:LX/2OC;

    .line 571756
    iget-object v3, v0, LX/FHa;->mFbid:Ljava/lang/String;

    move-object v3, v3

    .line 571757
    invoke-virtual {v2, v3}, LX/2OC;->c(Ljava/lang/String;)I

    move-result v2

    int-to-long v2, v2

    const-wide/16 v4, 0x32

    cmp-long v2, v2, v4

    if-gez v2, :cond_1

    .line 571758
    const/4 v5, 0x0

    .line 571759
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v3

    .line 571760
    iget-object v2, v0, LX/FHa;->mMediaResourceAsBytes:[B

    iget-object v4, v0, LX/FHa;->mMediaResourceAsBytes:[B

    array-length v4, v4

    invoke-virtual {v3, v2, v5, v4}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 571761
    invoke-virtual {v3, v5}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 571762
    sget-object v2, Lcom/facebook/ui/media/attachments/MediaResource;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v2, v3}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/ui/media/attachments/MediaResource;

    .line 571763
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 571764
    move-object v2, v2

    .line 571765
    iget-object v3, v0, LX/FHa;->mFbid:Ljava/lang/String;

    move-object v0, v3

    .line 571766
    invoke-static {p0, v2, v0}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->a$redex0(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 571767
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 571768
    :cond_2
    monitor-exit p0

    return-void

    .line 571769
    :cond_3
    :try_start_1
    const/4 v2, 0x0

    .line 571770
    iget-object v1, v0, LX/2OC;->b:Lcom/facebook/compactdisk/DiskCache;

    const-string v3, "phase_two_states_key"

    invoke-virtual {v1, v3}, Lcom/facebook/compactdisk/PersistentKeyValueStore;->fetch(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 571771
    const/4 v3, 0x0

    new-array v4, v3, [B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 571772
    :try_start_2
    invoke-static {v1}, LX/0Sa;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [B
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 571773
    :goto_2
    :try_start_3
    if-nez v1, :cond_4

    .line 571774
    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    goto :goto_0

    .line 571775
    :catch_0
    move-exception v1

    move-object v3, v1

    .line 571776
    iget-object v1, v0, LX/2OC;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/03V;

    const-string v5, "phase_two_upload_cache_fetch_failed"

    invoke-virtual {v1, v5, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v1, v4

    goto :goto_2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 571777
    :cond_4
    :try_start_4
    new-instance v3, Ljava/io/ByteArrayInputStream;

    invoke-direct {v3, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 571778
    new-instance v4, Ljava/io/ObjectInputStream;

    invoke-direct {v4, v3}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V

    .line 571779
    invoke-virtual {v4}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    move-object v1, v3
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 571780
    move-object v2, v1

    .line 571781
    :goto_3
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FHa;

    .line 571782
    iget-object v4, v0, LX/2OC;->d:Ljava/util/Map;

    .line 571783
    iget-object v5, v1, LX/FHa;->mFbid:Ljava/lang/String;

    move-object v5, v5

    .line 571784
    invoke-interface {v4, v5, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    .line 571785
    :catch_1
    move-exception v3

    .line 571786
    iget-object v1, v0, LX/2OC;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/03V;

    const-string v4, "two_phase_state_deserialization_failed"

    invoke-virtual {v1, v4, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 571787
    invoke-static {v0}, LX/2OC;->c(LX/2OC;)V

    goto :goto_3
.end method
