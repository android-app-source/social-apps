.class public Lcom/facebook/messaging/media/imageurirequest/ImageUriRequestManager;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field public a:LX/0Sh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final b:LX/11H;

.field private final c:LX/3em;


# direct methods
.method public constructor <init>(LX/11H;LX/3em;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 620924
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 620925
    iput-object p1, p0, Lcom/facebook/messaging/media/imageurirequest/ImageUriRequestManager;->b:LX/11H;

    .line 620926
    iput-object p2, p0, Lcom/facebook/messaging/media/imageurirequest/ImageUriRequestManager;->c:LX/3em;

    .line 620927
    return-void
.end method

.method public static b(LX/0QB;)Lcom/facebook/messaging/media/imageurirequest/ImageUriRequestManager;
    .locals 3

    .prologue
    .line 620928
    new-instance v2, Lcom/facebook/messaging/media/imageurirequest/ImageUriRequestManager;

    invoke-static {p0}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/0QB;)Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    move-result-object v0

    check-cast v0, LX/11H;

    invoke-static {p0}, LX/3em;->b(LX/0QB;)LX/3em;

    move-result-object v1

    check-cast v1, LX/3em;

    invoke-direct {v2, v0, v1}, Lcom/facebook/messaging/media/imageurirequest/ImageUriRequestManager;-><init>(LX/11H;LX/3em;)V

    .line 620929
    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v0

    check-cast v0, LX/0Sh;

    .line 620930
    iput-object v0, v2, Lcom/facebook/messaging/media/imageurirequest/ImageUriRequestManager;->a:LX/0Sh;

    .line 620931
    return-object v2
.end method


# virtual methods
.method public final a(Ljava/lang/String;ILcom/facebook/common/callercontext/CallerContext;)Landroid/net/Uri;
    .locals 7
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 620932
    iget-object v0, p0, Lcom/facebook/messaging/media/imageurirequest/ImageUriRequestManager;->a:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 620933
    if-nez p3, :cond_1

    move-object v0, v1

    .line 620934
    :goto_0
    :try_start_0
    iget-object v2, p0, Lcom/facebook/messaging/media/imageurirequest/ImageUriRequestManager;->b:LX/11H;

    iget-object v3, p0, Lcom/facebook/messaging/media/imageurirequest/ImageUriRequestManager;->c:LX/3em;

    new-instance v4, Lcom/facebook/messaging/media/imageurirequest/FetchImageParams;

    invoke-direct {v4, p1, p2}, Lcom/facebook/messaging/media/imageurirequest/FetchImageParams;-><init>(Ljava/lang/String;I)V

    const-class v5, Lcom/facebook/messaging/media/imageurirequest/ImageUriRequestManager;

    const-string v6, "download_image_info"

    invoke-static {v5, v6, v0}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    invoke-virtual {v2, v3, v4, v0}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/media/imageurirequest/graphql/DownloadImageFragmentModels$DownloadImageFragmentModel;

    .line 620935
    invoke-virtual {v0}, Lcom/facebook/messaging/media/imageurirequest/graphql/DownloadImageFragmentModels$DownloadImageFragmentModel;->a()Lcom/facebook/messaging/media/imageurirequest/graphql/DownloadImageFragmentModels$DownloadImageFragmentModel$AnimatedGifModel;

    move-result-object v2

    .line 620936
    if-eqz v2, :cond_2

    .line 620937
    invoke-virtual {v2}, Lcom/facebook/messaging/media/imageurirequest/graphql/DownloadImageFragmentModels$DownloadImageFragmentModel$AnimatedGifModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 620938
    :cond_0
    :goto_1
    return-object v1

    .line 620939
    :cond_1
    invoke-virtual {p3}, Lcom/facebook/common/callercontext/CallerContext;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 620940
    :cond_2
    :try_start_1
    invoke-virtual {v0}, Lcom/facebook/messaging/media/imageurirequest/graphql/DownloadImageFragmentModels$DownloadImageFragmentModel;->b()Lcom/facebook/messaging/media/imageurirequest/graphql/DownloadImageFragmentModels$DownloadImageFragmentModel$ImageModel;

    move-result-object v0

    .line 620941
    if-eqz v0, :cond_0

    .line 620942
    invoke-virtual {v0}, Lcom/facebook/messaging/media/imageurirequest/graphql/DownloadImageFragmentModels$DownloadImageFragmentModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v1

    goto :goto_1

    .line 620943
    :catch_0
    move-exception v0

    .line 620944
    const-class v1, Ljava/io/IOException;

    invoke-static {v0, v1}, LX/1Bz;->propagateIfPossible(Ljava/lang/Throwable;Ljava/lang/Class;)V

    .line 620945
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
