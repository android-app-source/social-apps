.class public Lcom/facebook/messaging/push/mqtt/OrcaMqttPushHandler;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/1fT;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile h:Lcom/facebook/messaging/push/mqtt/OrcaMqttPushHandler;


# instance fields
.field private final b:LX/0WJ;

.field public final c:LX/2Ow;

.field public final d:LX/0aG;

.field public final e:LX/2Pw;

.field public final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/B9n;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 576470
    const-class v0, Lcom/facebook/messaging/push/mqtt/OrcaMqttPushHandler;

    sput-object v0, Lcom/facebook/messaging/push/mqtt/OrcaMqttPushHandler;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0WJ;LX/2Pw;LX/2Ow;LX/0Or;LX/0aG;LX/0Or;)V
    .locals 0
    .param p6    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/annotations/IsMessengerSyncEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/auth/datastore/LoggedInUserAuthDataStore;",
            "LX/2Pw;",
            "LX/2Ow;",
            "LX/0Or",
            "<",
            "LX/B9n;",
            ">;",
            "LX/0aG;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 576427
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 576428
    iput-object p1, p0, Lcom/facebook/messaging/push/mqtt/OrcaMqttPushHandler;->b:LX/0WJ;

    .line 576429
    iput-object p2, p0, Lcom/facebook/messaging/push/mqtt/OrcaMqttPushHandler;->e:LX/2Pw;

    .line 576430
    iput-object p3, p0, Lcom/facebook/messaging/push/mqtt/OrcaMqttPushHandler;->c:LX/2Ow;

    .line 576431
    iput-object p4, p0, Lcom/facebook/messaging/push/mqtt/OrcaMqttPushHandler;->f:LX/0Or;

    .line 576432
    iput-object p5, p0, Lcom/facebook/messaging/push/mqtt/OrcaMqttPushHandler;->d:LX/0aG;

    .line 576433
    iput-object p6, p0, Lcom/facebook/messaging/push/mqtt/OrcaMqttPushHandler;->g:LX/0Or;

    .line 576434
    return-void
.end method

.method private static a([B)LX/6mS;
    .locals 5

    .prologue
    .line 576463
    new-instance v0, LX/1sp;

    invoke-direct {v0}, LX/1sp;-><init>()V

    .line 576464
    new-instance v1, LX/1sr;

    new-instance v2, Ljava/io/ByteArrayInputStream;

    const/4 v3, 0x0

    array-length v4, p0

    invoke-direct {v2, p0, v3, v4}, Ljava/io/ByteArrayInputStream;-><init>([BII)V

    invoke-direct {v1, v2}, LX/1sr;-><init>(Ljava/io/InputStream;)V

    invoke-interface {v0, v1}, LX/1sq;->a(LX/1ss;)LX/1su;

    move-result-object v0

    .line 576465
    :try_start_0
    invoke-static {v0}, LX/3ll;->b(LX/1su;)LX/3ll;

    .line 576466
    invoke-static {v0}, LX/6mS;->b(LX/1su;)LX/6mS;
    :try_end_0
    .catch LX/7H0; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 576467
    return-object v0

    .line 576468
    :catch_0
    move-exception v0

    .line 576469
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static a(LX/0QB;)Lcom/facebook/messaging/push/mqtt/OrcaMqttPushHandler;
    .locals 10

    .prologue
    .line 576450
    sget-object v0, Lcom/facebook/messaging/push/mqtt/OrcaMqttPushHandler;->h:Lcom/facebook/messaging/push/mqtt/OrcaMqttPushHandler;

    if-nez v0, :cond_1

    .line 576451
    const-class v1, Lcom/facebook/messaging/push/mqtt/OrcaMqttPushHandler;

    monitor-enter v1

    .line 576452
    :try_start_0
    sget-object v0, Lcom/facebook/messaging/push/mqtt/OrcaMqttPushHandler;->h:Lcom/facebook/messaging/push/mqtt/OrcaMqttPushHandler;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 576453
    if-eqz v2, :cond_0

    .line 576454
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 576455
    new-instance v3, Lcom/facebook/messaging/push/mqtt/OrcaMqttPushHandler;

    invoke-static {v0}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v4

    check-cast v4, LX/0WJ;

    invoke-static {v0}, LX/2Pw;->a(LX/0QB;)LX/2Pw;

    move-result-object v5

    check-cast v5, LX/2Pw;

    invoke-static {v0}, LX/2Ow;->a(LX/0QB;)LX/2Ow;

    move-result-object v6

    check-cast v6, LX/2Ow;

    const/16 v7, 0x2680

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v8

    check-cast v8, LX/0aG;

    const/16 v9, 0x14ea

    invoke-static {v0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-direct/range {v3 .. v9}, Lcom/facebook/messaging/push/mqtt/OrcaMqttPushHandler;-><init>(LX/0WJ;LX/2Pw;LX/2Ow;LX/0Or;LX/0aG;LX/0Or;)V

    .line 576456
    move-object v0, v3

    .line 576457
    sput-object v0, Lcom/facebook/messaging/push/mqtt/OrcaMqttPushHandler;->h:Lcom/facebook/messaging/push/mqtt/OrcaMqttPushHandler;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 576458
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 576459
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 576460
    :cond_1
    sget-object v0, Lcom/facebook/messaging/push/mqtt/OrcaMqttPushHandler;->h:Lcom/facebook/messaging/push/mqtt/OrcaMqttPushHandler;

    return-object v0

    .line 576461
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 576462
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final onMessage(Ljava/lang/String;[BJ)V
    .locals 9

    .prologue
    .line 576435
    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/push/mqtt/OrcaMqttPushHandler;->b:LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 576436
    :cond_0
    :goto_0
    return-void

    .line 576437
    :cond_1
    const-string v0, "/t_inbox"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 576438
    invoke-static {p2}, Lcom/facebook/messaging/push/mqtt/OrcaMqttPushHandler;->a([B)LX/6mS;

    move-result-object v0

    .line 576439
    iget-object v1, v0, LX/6mS;->unseen:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 576440
    iget-object v2, v0, LX/6mS;->unread:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 576441
    iget-object v3, p0, Lcom/facebook/messaging/push/mqtt/OrcaMqttPushHandler;->f:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/B9n;

    invoke-virtual {v3, v2, v1}, LX/B9n;->a(II)V

    .line 576442
    iget-object v3, p0, Lcom/facebook/messaging/push/mqtt/OrcaMqttPushHandler;->e:LX/2Pw;

    invoke-virtual {v3, v1}, LX/2Pw;->a(I)V

    .line 576443
    iget-object v3, p0, Lcom/facebook/messaging/push/mqtt/OrcaMqttPushHandler;->c:LX/2Ow;

    invoke-virtual {v3, v1}, LX/2Ow;->a(I)V

    .line 576444
    iget-object v3, p0, Lcom/facebook/messaging/push/mqtt/OrcaMqttPushHandler;->g:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 576445
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 576446
    const-string v3, "updateFolderCountsParams"

    new-instance v4, Lcom/facebook/messaging/service/model/UpdateFolderCountsParams;

    sget-object v6, LX/6ek;->INBOX:LX/6ek;

    invoke-direct {v4, v6, v2, v1}, Lcom/facebook/messaging/service/model/UpdateFolderCountsParams;-><init>(LX/6ek;II)V

    invoke-virtual {v5, v3, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 576447
    iget-object v3, p0, Lcom/facebook/messaging/push/mqtt/OrcaMqttPushHandler;->d:LX/0aG;

    const-string v4, "update_folder_counts"

    sget-object v6, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    const-class v7, Lcom/facebook/messaging/push/mqtt/OrcaMqttPushHandler;

    invoke-static {v7}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v7

    const v8, 0x5f2c0b79

    invoke-static/range {v3 .. v8}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v3

    const/4 v4, 0x1

    invoke-interface {v3, v4}, LX/1MF;->setFireAndForget(Z)LX/1MF;

    move-result-object v3

    invoke-interface {v3}, LX/1MF;->start()LX/1ML;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 576448
    :cond_2
    goto :goto_0

    .line 576449
    :catch_0
    goto :goto_0
.end method
