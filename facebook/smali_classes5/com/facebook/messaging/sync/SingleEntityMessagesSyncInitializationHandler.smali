.class public Lcom/facebook/messaging/sync/SingleEntityMessagesSyncInitializationHandler;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/2Sy;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile h:Lcom/facebook/messaging/sync/SingleEntityMessagesSyncInitializationHandler;


# instance fields
.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0aG;

.field private final d:LX/2Sz;

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation
.end field

.field private g:LX/2Sx;


# direct methods
.method public constructor <init>(LX/0aG;LX/0Or;LX/2Sz;LX/0Or;LX/2Sx;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/annotations/IsMessengerSyncEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0aG;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/2Sz;",
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;",
            "LX/2Sx;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 573674
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 573675
    iput-object p1, p0, Lcom/facebook/messaging/sync/SingleEntityMessagesSyncInitializationHandler;->c:LX/0aG;

    .line 573676
    iput-object p2, p0, Lcom/facebook/messaging/sync/SingleEntityMessagesSyncInitializationHandler;->b:LX/0Or;

    .line 573677
    iput-object p3, p0, Lcom/facebook/messaging/sync/SingleEntityMessagesSyncInitializationHandler;->d:LX/2Sz;

    .line 573678
    iput-object p5, p0, Lcom/facebook/messaging/sync/SingleEntityMessagesSyncInitializationHandler;->g:LX/2Sx;

    .line 573679
    iput-object p4, p0, Lcom/facebook/messaging/sync/SingleEntityMessagesSyncInitializationHandler;->e:LX/0Or;

    .line 573680
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/messaging/sync/SingleEntityMessagesSyncInitializationHandler;
    .locals 9

    .prologue
    .line 573661
    sget-object v0, Lcom/facebook/messaging/sync/SingleEntityMessagesSyncInitializationHandler;->h:Lcom/facebook/messaging/sync/SingleEntityMessagesSyncInitializationHandler;

    if-nez v0, :cond_1

    .line 573662
    const-class v1, Lcom/facebook/messaging/sync/SingleEntityMessagesSyncInitializationHandler;

    monitor-enter v1

    .line 573663
    :try_start_0
    sget-object v0, Lcom/facebook/messaging/sync/SingleEntityMessagesSyncInitializationHandler;->h:Lcom/facebook/messaging/sync/SingleEntityMessagesSyncInitializationHandler;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 573664
    if-eqz v2, :cond_0

    .line 573665
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 573666
    new-instance v3, Lcom/facebook/messaging/sync/SingleEntityMessagesSyncInitializationHandler;

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v4

    check-cast v4, LX/0aG;

    const/16 v5, 0x14ea

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {v0}, LX/2Sz;->a(LX/0QB;)LX/2Sz;

    move-result-object v6

    check-cast v6, LX/2Sz;

    const/16 v7, 0x19e

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static {v0}, LX/2Sx;->a(LX/0QB;)LX/2Sx;

    move-result-object v8

    check-cast v8, LX/2Sx;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/messaging/sync/SingleEntityMessagesSyncInitializationHandler;-><init>(LX/0aG;LX/0Or;LX/2Sz;LX/0Or;LX/2Sx;)V

    .line 573667
    move-object v0, v3

    .line 573668
    sput-object v0, Lcom/facebook/messaging/sync/SingleEntityMessagesSyncInitializationHandler;->h:Lcom/facebook/messaging/sync/SingleEntityMessagesSyncInitializationHandler;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 573669
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 573670
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 573671
    :cond_1
    sget-object v0, Lcom/facebook/messaging/sync/SingleEntityMessagesSyncInitializationHandler;->h:Lcom/facebook/messaging/sync/SingleEntityMessagesSyncInitializationHandler;

    return-object v0

    .line 573672
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 573673
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/2T6;)V
    .locals 5

    .prologue
    .line 573656
    sget-object v0, LX/2T6;->NORMAL:LX/2T6;

    if-eq p1, v0, :cond_0

    .line 573657
    sget-object v0, LX/7G7;->REFRESH_CONNECTION:LX/7G7;

    invoke-static {v0}, LX/2Sz;->a(LX/7G7;)Landroid/os/Bundle;

    move-result-object v0

    .line 573658
    :goto_0
    iget-object v1, p0, Lcom/facebook/messaging/sync/SingleEntityMessagesSyncInitializationHandler;->c:LX/0aG;

    const-string v2, "ensure_sync"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v3

    const v4, -0x56d14996

    invoke-static {v1, v2, v0, v3, v4}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/1MF;->setFireAndForget(Z)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    .line 573659
    return-void

    .line 573660
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 573655
    iget-object v0, p0, Lcom/facebook/messaging/sync/SingleEntityMessagesSyncInitializationHandler;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public final b()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/0Tn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 573653
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 573654
    return-object v0
.end method

.method public final b(LX/2T6;)V
    .locals 3

    .prologue
    .line 573647
    iget-object v1, p0, Lcom/facebook/messaging/sync/SingleEntityMessagesSyncInitializationHandler;->g:LX/2Sx;

    iget-object v0, p0, Lcom/facebook/messaging/sync/SingleEntityMessagesSyncInitializationHandler;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 573648
    iget-object v2, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v0, v2

    .line 573649
    sget-object v2, LX/7GT;->MESSAGES_QUEUE_TYPE:LX/7GT;

    move-object v2, v2

    .line 573650
    invoke-static {v0, v2}, LX/7G9;->a(Ljava/lang/String;LX/7GT;)LX/7G9;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/2Sx;->a(LX/7G9;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 573651
    :goto_0
    return-void

    .line 573652
    :cond_0
    invoke-virtual {p0, p1}, Lcom/facebook/messaging/sync/SingleEntityMessagesSyncInitializationHandler;->a(LX/2T6;)V

    goto :goto_0
.end method

.method public final c()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 573640
    const/16 v0, 0x180

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/16 v1, 0x55b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x1f9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 573646
    sget-object v0, Lcom/facebook/messaging/sync/SingleEntityMessagesSyncInitializationHandler;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final e()V
    .locals 5

    .prologue
    .line 573641
    iget-object v0, p0, Lcom/facebook/messaging/sync/SingleEntityMessagesSyncInitializationHandler;->f:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    .line 573642
    :goto_0
    return-void

    .line 573643
    :cond_0
    sget-object v0, LX/7G7;->REFRESH_CONNECTION:LX/7G7;

    invoke-static {v0}, LX/2Sz;->a(LX/7G7;)Landroid/os/Bundle;

    move-result-object v0

    .line 573644
    iget-object v1, p0, Lcom/facebook/messaging/sync/SingleEntityMessagesSyncInitializationHandler;->c:LX/0aG;

    const-string v2, "ensure_sync"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v3

    const v4, -0x3daaead7

    invoke-static {v1, v2, v0, v3, v4}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/1MF;->setFireAndForget(Z)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/sync/SingleEntityMessagesSyncInitializationHandler;->f:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 573645
    iget-object v0, p0, Lcom/facebook/messaging/sync/SingleEntityMessagesSyncInitializationHandler;->f:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v1, Lcom/facebook/messaging/sync/SingleEntityMessagesSyncInitializationHandler$1;

    invoke-direct {v1, p0}, Lcom/facebook/messaging/sync/SingleEntityMessagesSyncInitializationHandler$1;-><init>(Lcom/facebook/messaging/sync/SingleEntityMessagesSyncInitializationHandler;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/common/util/concurrent/ListenableFuture;->addListener(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method
