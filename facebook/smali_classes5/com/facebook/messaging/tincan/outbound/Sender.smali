.class public Lcom/facebook/messaging/tincan/outbound/Sender;
.super LX/2PI;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2PI",
        "<",
        "Lcom/facebook/messaging/tincan/outbound/SenderListener;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile j:Lcom/facebook/messaging/tincan/outbound/Sender;


# instance fields
.field private final c:LX/2PJ;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/messaging/tincan/crypto/CryptoSessionStorage;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0SF;

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2PM;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final i:LX/2PE;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 578915
    const-class v0, Lcom/facebook/messaging/tincan/outbound/Sender;

    sput-object v0, Lcom/facebook/messaging/tincan/outbound/Sender;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/2PJ;LX/0Or;LX/0Or;LX/0SF;LX/0Ot;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2PE;)V
    .locals 1
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2PJ;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/messaging/tincan/crypto/CryptoSessionStorage;",
            ">;",
            "LX/0SF;",
            "LX/0Ot",
            "<",
            "LX/2PM;",
            ">;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/2PE;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 578906
    const/16 v0, 0xb

    invoke-direct {p0, v0}, LX/2PI;-><init>(I)V

    .line 578907
    iput-object p1, p0, Lcom/facebook/messaging/tincan/outbound/Sender;->c:LX/2PJ;

    .line 578908
    iput-object p2, p0, Lcom/facebook/messaging/tincan/outbound/Sender;->d:LX/0Or;

    .line 578909
    iput-object p3, p0, Lcom/facebook/messaging/tincan/outbound/Sender;->e:LX/0Or;

    .line 578910
    iput-object p4, p0, Lcom/facebook/messaging/tincan/outbound/Sender;->f:LX/0SF;

    .line 578911
    iput-object p5, p0, Lcom/facebook/messaging/tincan/outbound/Sender;->g:LX/0Ot;

    .line 578912
    iput-object p6, p0, Lcom/facebook/messaging/tincan/outbound/Sender;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 578913
    iput-object p7, p0, Lcom/facebook/messaging/tincan/outbound/Sender;->i:LX/2PE;

    .line 578914
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/messaging/tincan/outbound/Sender;
    .locals 11

    .prologue
    .line 578893
    sget-object v0, Lcom/facebook/messaging/tincan/outbound/Sender;->j:Lcom/facebook/messaging/tincan/outbound/Sender;

    if-nez v0, :cond_1

    .line 578894
    const-class v1, Lcom/facebook/messaging/tincan/outbound/Sender;

    monitor-enter v1

    .line 578895
    :try_start_0
    sget-object v0, Lcom/facebook/messaging/tincan/outbound/Sender;->j:Lcom/facebook/messaging/tincan/outbound/Sender;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 578896
    if-eqz v2, :cond_0

    .line 578897
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 578898
    new-instance v3, Lcom/facebook/messaging/tincan/outbound/Sender;

    invoke-static {v0}, LX/2PJ;->a(LX/0QB;)LX/2PJ;

    move-result-object v4

    check-cast v4, LX/2PJ;

    const/16 v5, 0x15e8

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v6, 0x2a2d

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v7

    check-cast v7, LX/0SF;

    const/16 v8, 0xdc0

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v9

    check-cast v9, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/2PE;->a(LX/0QB;)LX/2PE;

    move-result-object v10

    check-cast v10, LX/2PE;

    invoke-direct/range {v3 .. v10}, Lcom/facebook/messaging/tincan/outbound/Sender;-><init>(LX/2PJ;LX/0Or;LX/0Or;LX/0SF;LX/0Ot;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2PE;)V

    .line 578899
    move-object v0, v3

    .line 578900
    sput-object v0, Lcom/facebook/messaging/tincan/outbound/Sender;->j:Lcom/facebook/messaging/tincan/outbound/Sender;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 578901
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 578902
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 578903
    :cond_1
    sget-object v0, Lcom/facebook/messaging/tincan/outbound/Sender;->j:Lcom/facebook/messaging/tincan/outbound/Sender;

    return-object v0

    .line 578904
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 578905
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private declared-synchronized a(LX/DpM;LX/DpM;II[B[B)V
    .locals 8
    .param p1    # LX/DpM;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/DpM;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 578886
    monitor-enter p0

    const/4 v1, 0x0

    .line 578887
    :try_start_0
    new-instance v0, LX/DpO;

    invoke-direct {v0}, LX/DpO;-><init>()V

    .line 578888
    invoke-static {v0, p5}, LX/DpO;->b(LX/DpO;[B)V

    .line 578889
    move-object v6, v0

    .line 578890
    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    move-object v7, p6

    invoke-static/range {v0 .. v7}, Lcom/facebook/messaging/tincan/outbound/Sender;->a(Lcom/facebook/messaging/tincan/outbound/Sender;LX/Dpe;LX/DpM;LX/DpM;IILX/DpO;[B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 578891
    monitor-exit p0

    return-void

    .line 578892
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized a(Lcom/facebook/messaging/tincan/outbound/Sender;LX/Dpe;LX/DpM;LX/DpM;IILX/DpO;[B)V
    .locals 9
    .param p0    # Lcom/facebook/messaging/tincan/outbound/Sender;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # LX/Dpe;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/DpM;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 578883
    monitor-enter p0

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    :try_start_0
    invoke-static/range {v0 .. v8}, Lcom/facebook/messaging/tincan/outbound/Sender;->a(Lcom/facebook/messaging/tincan/outbound/Sender;LX/Dpe;LX/DpM;LX/DpM;IILX/DpO;[BLjava/lang/Long;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 578884
    monitor-exit p0

    return-void

    .line 578885
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized a(Lcom/facebook/messaging/tincan/outbound/Sender;LX/Dpe;LX/DpM;LX/DpM;IILX/DpO;[BLjava/lang/Long;)V
    .locals 9
    .param p0    # Lcom/facebook/messaging/tincan/outbound/Sender;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # LX/Dpe;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/DpM;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 578875
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LX/2PI;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 578876
    iget-object v0, p0, Lcom/facebook/messaging/tincan/outbound/Sender;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2PM;

    sget-object v1, Lcom/facebook/messaging/tincan/outbound/Sender;->b:Ljava/lang/Class;

    const-string v2, "No stored procedure available to use for send"

    invoke-virtual {v0, v1, v2}, LX/2PM;->a(Ljava/lang/Class;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 578877
    :goto_0
    monitor-exit p0

    return-void

    .line 578878
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/facebook/messaging/tincan/outbound/Sender;->f:LX/0SF;

    invoke-virtual {v0}, LX/0SF;->a()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v2, v0

    move-object v0, p2

    move-object v1, p3

    move v4, p4

    move v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    .line 578879
    invoke-static/range {v0 .. v8}, LX/Dpm;->a(LX/DpM;LX/DpM;JIILX/DpO;[BLjava/lang/Long;)LX/DpN;

    move-result-object v0

    .line 578880
    invoke-static {v0}, LX/Dpn;->a(LX/1u2;)[B

    move-result-object v0

    .line 578881
    invoke-virtual {p0, v0}, LX/2PI;->a([B)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 578882
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized a(Lcom/facebook/messaging/tincan/outbound/Sender;LX/Dpe;LX/DpM;LX/DpM;ILX/DpO;[B)V
    .locals 8
    .param p0    # Lcom/facebook/messaging/tincan/outbound/Sender;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # LX/Dpe;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/DpM;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 578872
    monitor-enter p0

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    move-object v6, p6

    :try_start_0
    invoke-static/range {v0 .. v7}, Lcom/facebook/messaging/tincan/outbound/Sender;->a(Lcom/facebook/messaging/tincan/outbound/Sender;LX/Dpe;LX/DpM;LX/DpM;ILX/DpO;[BLjava/lang/Long;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 578873
    monitor-exit p0

    return-void

    .line 578874
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized a(Lcom/facebook/messaging/tincan/outbound/Sender;LX/Dpe;LX/DpM;LX/DpM;ILX/DpO;[BLjava/lang/Long;)V
    .locals 9
    .param p0    # Lcom/facebook/messaging/tincan/outbound/Sender;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # LX/Dpe;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/DpM;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 578762
    monitor-enter p0

    :try_start_0
    sget-object v0, LX/DpF;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v1, p0, Lcom/facebook/messaging/tincan/outbound/Sender;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/Dp9;->k:LX/0Tn;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v1

    add-int v5, v0, v1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v6, p5

    move-object v7, p6

    move-object/from16 v8, p7

    .line 578763
    invoke-static/range {v0 .. v8}, Lcom/facebook/messaging/tincan/outbound/Sender;->a(Lcom/facebook/messaging/tincan/outbound/Sender;LX/Dpe;LX/DpM;LX/DpM;IILX/DpO;[BLjava/lang/Long;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 578764
    monitor-exit p0

    return-void

    .line 578765
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static b([B)V
    .locals 4

    .prologue
    .line 578864
    array-length v0, p0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_1

    .line 578865
    :cond_0
    return-void

    .line 578866
    :cond_1
    array-length v0, p0

    add-int/lit8 v1, v0, -0x1

    .line 578867
    const/4 v0, 0x0

    :goto_0
    array-length v2, p0

    div-int/lit8 v2, v2, 0x2

    if-ge v0, v2, :cond_0

    .line 578868
    aget-byte v2, p0, v0

    .line 578869
    add-int v3, v0, v1

    aget-byte v3, p0, v3

    aput-byte v3, p0, v0

    .line 578870
    add-int v3, v0, v1

    aput-byte v2, p0, v3

    .line 578871
    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v1, v1, -0x2

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 578863
    return-void
.end method

.method public final declared-synchronized a(JLjava/lang/String;JLjava/lang/String;J[B)V
    .locals 7

    .prologue
    .line 578859
    monitor-enter p0

    :try_start_0
    new-instance v0, LX/DpV;

    invoke-static {p7, p8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-direct {v0, v1}, LX/DpV;-><init>(Ljava/lang/Long;)V

    .line 578860
    const/4 v1, 0x0

    invoke-static {p4, p5, p6}, LX/Dpm;->a(JLjava/lang/String;)LX/DpM;

    move-result-object v2

    invoke-static {p1, p2, p3}, LX/Dpm;->a(JLjava/lang/String;)LX/DpM;

    move-result-object v3

    const/16 v4, 0x32

    invoke-static {v0}, LX/DpO;->a(LX/DpV;)LX/DpO;

    move-result-object v5

    move-object v0, p0

    move-object/from16 v6, p9

    invoke-static/range {v0 .. v6}, Lcom/facebook/messaging/tincan/outbound/Sender;->a(Lcom/facebook/messaging/tincan/outbound/Sender;LX/Dpe;LX/DpM;LX/DpM;ILX/DpO;[B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 578861
    monitor-exit p0

    return-void

    .line 578862
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(JLjava/lang/String;JLjava/lang/String;LX/Dpl;[B)V
    .locals 8

    .prologue
    .line 578848
    monitor-enter p0

    :try_start_0
    invoke-static {p4, p5, p6}, LX/DoM;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 578849
    iget-object v0, p0, Lcom/facebook/messaging/tincan/outbound/Sender;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IuD;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LX/IuD;->a(Ljava/lang/String;Z)LX/DoK;

    move-result-object v1

    .line 578850
    if-nez v1, :cond_0

    .line 578851
    sget-object v0, Lcom/facebook/messaging/tincan/outbound/Sender;->b:Ljava/lang/Class;

    const-string v1, "No crypto session found for sending message content"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 578852
    :goto_0
    monitor-exit p0

    return-void

    .line 578853
    :cond_0
    :try_start_1
    invoke-static {p7}, LX/DpZ;->a(LX/Dpl;)LX/DpZ;

    move-result-object v0

    .line 578854
    const/4 v2, 0x7

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v2, v0, v3, v4}, LX/Dpm;->a(ILX/DpZ;[BLjava/lang/Integer;)LX/DpY;

    move-result-object v0

    .line 578855
    invoke-static {v0}, LX/Dpn;->a(LX/1u2;)[B

    move-result-object v2

    iget-object v0, p0, Lcom/facebook/messaging/tincan/outbound/Sender;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IuD;

    const/4 v3, 0x1

    invoke-static {v1, v2, v0, v3}, LX/DoJ;->a(LX/DoK;[BLX/IuD;Z)LX/DoI;

    move-result-object v4

    .line 578856
    new-instance v0, LX/Dpa;

    iget-object v1, v4, LX/DoI;->b:[B

    const/4 v2, 0x0

    const/4 v3, 0x0

    iget-boolean v4, v4, LX/DoI;->a:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, LX/Dpa;-><init>([B[B[BLjava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    .line 578857
    const/4 v1, 0x0

    invoke-static {p4, p5, p6}, LX/Dpm;->a(JLjava/lang/String;)LX/DpM;

    move-result-object v2

    invoke-static {p1, p2, p3}, LX/Dpm;->a(JLjava/lang/String;)LX/DpM;

    move-result-object v3

    const/4 v4, 0x7

    invoke-static {v0}, LX/DpO;->a(LX/Dpa;)LX/DpO;

    move-result-object v5

    iget-object v7, p7, LX/Dpl;->thread_fbid:Ljava/lang/Long;

    move-object v0, p0

    move-object/from16 v6, p8

    invoke-static/range {v0 .. v7}, Lcom/facebook/messaging/tincan/outbound/Sender;->a(Lcom/facebook/messaging/tincan/outbound/Sender;LX/Dpe;LX/DpM;LX/DpM;ILX/DpO;[BLjava/lang/Long;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 578858
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(JLjava/lang/String;JLjava/lang/String;[B[B[BZ)V
    .locals 8
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/annotation/TargetApi;
        value = 0x9
    .end annotation

    .prologue
    .line 578827
    monitor-enter p0

    :try_start_0
    invoke-static {p4, p5, p6}, LX/DoM;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 578828
    iget-object v0, p0, Lcom/facebook/messaging/tincan/outbound/Sender;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IuD;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/IuD;->a(Ljava/lang/String;Z)LX/DoK;

    move-result-object v1

    .line 578829
    if-nez v1, :cond_1

    .line 578830
    sget-object v0, Lcom/facebook/messaging/tincan/outbound/Sender;->b:Ljava/lang/Class;

    const-string v1, "No crypto session found for sending message content"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 578831
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 578832
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/facebook/messaging/tincan/outbound/Sender;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/Dp9;->c:LX/0Tn;

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 578833
    array-length v0, p7

    invoke-static {p7, v0}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object p7

    .line 578834
    invoke-static {p7}, Lcom/facebook/messaging/tincan/outbound/Sender;->b([B)V

    .line 578835
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/facebook/messaging/tincan/outbound/Sender;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IuD;

    const/4 v2, 0x0

    invoke-static {v1, p7, v0, v2}, LX/DoJ;->a(LX/DoK;[BLX/IuD;Z)LX/DoI;

    move-result-object v2

    .line 578836
    iget-object v0, p0, Lcom/facebook/messaging/tincan/outbound/Sender;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/Dp9;->e:LX/0Tn;

    const/4 v3, 0x0

    invoke-interface {v0, v1, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 578837
    iget-object v0, v2, LX/DoI;->b:[B

    invoke-static {v0}, Lcom/facebook/messaging/tincan/outbound/Sender;->b([B)V

    .line 578838
    :cond_3
    new-instance v0, LX/Dpa;

    iget-object v1, v2, LX/DoI;->b:[B

    const/4 v3, 0x0

    iget-boolean v2, v2, LX/DoI;->a:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-static/range {p10 .. p10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    move-object/from16 v2, p8

    invoke-direct/range {v0 .. v7}, LX/Dpa;-><init>([B[B[BLjava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    .line 578839
    invoke-static {v0}, LX/DpO;->a(LX/Dpa;)LX/DpO;

    move-result-object v5

    .line 578840
    const/4 v1, 0x0

    invoke-static {p4, p5, p6}, LX/Dpm;->a(JLjava/lang/String;)LX/DpM;

    move-result-object v2

    invoke-static {p1, p2, p3}, LX/Dpm;->a(JLjava/lang/String;)LX/DpM;

    move-result-object v3

    const/4 v4, 0x2

    move-object v0, p0

    move-object/from16 v6, p9

    invoke-static/range {v0 .. v6}, Lcom/facebook/messaging/tincan/outbound/Sender;->a(Lcom/facebook/messaging/tincan/outbound/Sender;LX/Dpe;LX/DpM;LX/DpM;ILX/DpO;[B)V

    .line 578841
    iget-object v0, p0, Lcom/facebook/messaging/tincan/outbound/Sender;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/Dp9;->i:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 578842
    const/4 v1, 0x0

    invoke-static {p4, p5, p6}, LX/Dpm;->a(JLjava/lang/String;)LX/DpM;

    move-result-object v2

    invoke-static {p1, p2, p3}, LX/Dpm;->a(JLjava/lang/String;)LX/DpM;

    move-result-object v3

    const/4 v4, 0x2

    move-object v0, p0

    move-object/from16 v6, p9

    invoke-static/range {v0 .. v6}, Lcom/facebook/messaging/tincan/outbound/Sender;->a(Lcom/facebook/messaging/tincan/outbound/Sender;LX/Dpe;LX/DpM;LX/DpM;ILX/DpO;[B)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 578843
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 578844
    :cond_4
    :try_start_2
    iget-object v0, p0, Lcom/facebook/messaging/tincan/outbound/Sender;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/Dp9;->d:LX/0Tn;

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 578845
    const/4 v0, 0x0

    new-array p7, v0, [B

    goto :goto_1

    .line 578846
    :cond_5
    iget-object v0, p0, Lcom/facebook/messaging/tincan/outbound/Sender;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/Dp9;->h:LX/0Tn;

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 578847
    invoke-static/range {p8 .. p8}, Lcom/facebook/messaging/tincan/outbound/Sender;->b([B)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_1
.end method

.method public final declared-synchronized a(LX/DpM;II[B[B)V
    .locals 7

    .prologue
    .line 578824
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/tincan/outbound/Sender;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iget-object v2, p0, Lcom/facebook/messaging/tincan/outbound/Sender;->c:LX/2PJ;

    invoke-virtual {v2}, LX/2PJ;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/Dpm;->a(JLjava/lang/String;)LX/DpM;

    move-result-object v2

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/facebook/messaging/tincan/outbound/Sender;->a(LX/DpM;LX/DpM;II[B[B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 578825
    monitor-exit p0

    return-void

    .line 578826
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(LX/Dph;)V
    .locals 12

    .prologue
    const/16 v2, 0xc8

    .line 578781
    if-nez p1, :cond_1

    .line 578782
    sget-object v0, Lcom/facebook/messaging/tincan/outbound/Sender;->b:Ljava/lang/Class;

    const-string v1, "Could not deserialise send result"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 578783
    :cond_0
    :goto_0
    return-void

    .line 578784
    :cond_1
    iget-object v0, p1, LX/Dph;->result:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, v2, :cond_3

    .line 578785
    iget-object v0, p1, LX/Dph;->result:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v1, 0x134

    if-ne v0, v1, :cond_2

    .line 578786
    iget-object v0, p0, LX/2PI;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;

    .line 578787
    iget-object v2, p1, LX/Dph;->nonce:[B

    .line 578788
    invoke-static {v2}, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->d([B)Ljava/lang/String;

    move-result-object v6

    .line 578789
    if-nez v6, :cond_6

    .line 578790
    :goto_2
    goto :goto_1

    .line 578791
    :cond_2
    sget-object v0, Lcom/facebook/messaging/tincan/outbound/Sender;->b:Ljava/lang/Class;

    const-string v1, "Error sending message %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p1, LX/Dph;->result:Ljava/lang/Integer;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 578792
    iget-object v0, p1, LX/Dph;->result:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 578793
    iget-object v0, p0, LX/2PI;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;

    .line 578794
    iget-object v2, p1, LX/Dph;->nonce:[B

    .line 578795
    invoke-static {v2}, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->d([B)Ljava/lang/String;

    move-result-object v3

    .line 578796
    if-nez v3, :cond_7

    .line 578797
    :goto_4
    goto :goto_3

    .line 578798
    :pswitch_0
    iget-object v0, p0, LX/2PI;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;

    .line 578799
    iget-object v2, p1, LX/Dph;->nonce:[B

    .line 578800
    invoke-static {v2}, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->d([B)Ljava/lang/String;

    move-result-object v3

    .line 578801
    if-nez v3, :cond_8

    .line 578802
    :goto_6
    goto :goto_5

    .line 578803
    :cond_3
    iget-object v0, p1, LX/Dph;->body:LX/Dpi;

    if-eqz v0, :cond_4

    iget-object v0, p1, LX/Dph;->body:LX/Dpi;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, LX/6kT;->a(I)Z

    move-result v0

    if-nez v0, :cond_5

    .line 578804
    :cond_4
    sget-object v0, Lcom/facebook/messaging/tincan/outbound/Sender;->b:Ljava/lang/Class;

    const-string v1, "Could not deserialise send result body"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 578805
    :cond_5
    iget-object v0, p1, LX/Dph;->result:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v2, :cond_0

    .line 578806
    iget-object v0, p1, LX/Dph;->body:LX/Dpi;

    invoke-virtual {v0}, LX/Dpi;->e()LX/Dpd;

    move-result-object v0

    iget-object v0, v0, LX/Dpd;->server_time_micros:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 578807
    iget-object v0, p1, LX/Dph;->body:LX/Dpi;

    invoke-virtual {v0}, LX/Dpi;->e()LX/Dpd;

    move-result-object v0

    iget-object v1, v0, LX/Dpd;->facebook_hmac:[B

    .line 578808
    iget-object v0, p0, LX/2PI;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_7
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;

    .line 578809
    iget-object v5, p1, LX/Dph;->nonce:[B

    .line 578810
    invoke-static {v5}, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->d([B)Ljava/lang/String;

    move-result-object v6

    .line 578811
    if-nez v6, :cond_9

    .line 578812
    :goto_8
    goto :goto_7

    .line 578813
    :cond_6
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 578814
    const-string v7, "message_id"

    invoke-virtual {v8, v7, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 578815
    iget-object v6, v0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->b:LX/0aG;

    const-string v7, "TincanSentMessageToNonPrimaryDevice"

    sget-object v9, LX/1ME;->BY_EXCEPTION:LX/1ME;

    const-class v10, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;

    invoke-static {v10}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v10

    const v11, 0x3cb69e18

    invoke-static/range {v6 .. v11}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v6

    invoke-interface {v6}, LX/1MF;->start()LX/1ML;

    goto/16 :goto_2

    .line 578816
    :cond_7
    iget-object v4, v0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->j:Landroid/content/res/Resources;

    const v5, 0x7f0806c0

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v3, v4}, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->a(Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 578817
    :cond_8
    iget-object v4, v0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->j:Landroid/content/res/Resources;

    const v5, 0x7f0806c0

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v3, v4}, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->a(Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;Ljava/lang/String;Ljava/lang/String;)V

    .line 578818
    iget-object v4, v0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->j:Landroid/content/res/Resources;

    const v5, 0x7f0806c3

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v3, v4}, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->b(Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6

    .line 578819
    :cond_9
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 578820
    const-string v7, "message_id"

    invoke-virtual {v8, v7, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 578821
    const-string v6, "timestamp_us"

    invoke-virtual {v8, v6, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 578822
    const-string v6, "facebook_hmac"

    invoke-virtual {v8, v6, v1}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 578823
    iget-object v6, v0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->b:LX/0aG;

    const-string v7, "TincanPostSendMessageUpdate"

    sget-object v9, LX/1ME;->BY_EXCEPTION:LX/1ME;

    const-class v10, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;

    invoke-static {v10}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v10

    const v11, -0x5a8c0ec1

    invoke-static/range {v6 .. v11}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v6

    invoke-interface {v6}, LX/1MF;->start()LX/1ML;

    goto :goto_8

    nop

    :pswitch_data_0
    .packed-switch 0x1aa
        :pswitch_0
    .end packed-switch
.end method

.method public final declared-synchronized a(Lcom/facebook/messaging/model/threadkey/ThreadKey;JLjava/lang/String;[B[B[BZ)V
    .locals 8

    .prologue
    .line 578770
    monitor-enter p0

    :try_start_0
    invoke-static {p2, p3, p4}, LX/DoM;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 578771
    new-instance v1, LX/Eay;

    invoke-virtual {p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/Eap;

    const/4 v4, 0x0

    invoke-direct {v3, v0, v4}, LX/Eap;-><init>(Ljava/lang/String;I)V

    invoke-direct {v1, v2, v3}, LX/Eay;-><init>(Ljava/lang/String;LX/Eap;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 578772
    :try_start_1
    new-instance v0, LX/Eaw;

    iget-object v2, p0, Lcom/facebook/messaging/tincan/outbound/Sender;->i:LX/2PE;

    invoke-direct {v0, v2, v1}, LX/Eaw;-><init>(LX/2PE;LX/Eay;)V

    .line 578773
    invoke-virtual {v0, p5}, LX/Eaw;->a([B)[B
    :try_end_1
    .catch LX/Eal; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    .line 578774
    :try_start_2
    new-instance v0, LX/Dpa;

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-static/range {p8 .. p8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    move-object v2, p6

    invoke-direct/range {v0 .. v7}, LX/Dpa;-><init>([B[B[BLjava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    .line 578775
    invoke-static {v0}, LX/DpO;->a(LX/Dpa;)LX/DpO;

    move-result-object v5

    .line 578776
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {p2, p3, p4}, LX/Dpm;->a(JLjava/lang/String;)LX/DpM;

    move-result-object v3

    const/4 v4, 0x2

    iget-wide v6, p1, Lcom/facebook/messaging/model/threadkey/ThreadKey;->b:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    move-object v0, p0

    move-object v6, p7

    invoke-static/range {v0 .. v7}, Lcom/facebook/messaging/tincan/outbound/Sender;->a(Lcom/facebook/messaging/tincan/outbound/Sender;LX/Dpe;LX/DpM;LX/DpM;ILX/DpO;[BLjava/lang/Long;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 578777
    :goto_0
    monitor-exit p0

    return-void

    .line 578778
    :catch_0
    move-exception v0

    .line 578779
    :try_start_3
    sget-object v1, Lcom/facebook/messaging/tincan/outbound/Sender;->b:Ljava/lang/Class;

    const-string v2, "No crypto session found for sending message content"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 578780
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(JLjava/lang/String;JLjava/lang/String;J[B)V
    .locals 7

    .prologue
    .line 578766
    monitor-enter p0

    :try_start_0
    new-instance v0, LX/DpV;

    invoke-static {p7, p8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-direct {v0, v1}, LX/DpV;-><init>(Ljava/lang/Long;)V

    .line 578767
    const/4 v1, 0x0

    invoke-static {p4, p5, p6}, LX/Dpm;->a(JLjava/lang/String;)LX/DpM;

    move-result-object v2

    invoke-static {p1, p2, p3}, LX/Dpm;->a(JLjava/lang/String;)LX/DpM;

    move-result-object v3

    const/16 v4, 0x33

    invoke-static {v0}, LX/DpO;->a(LX/DpV;)LX/DpO;

    move-result-object v5

    move-object v0, p0

    move-object/from16 v6, p9

    invoke-static/range {v0 .. v6}, Lcom/facebook/messaging/tincan/outbound/Sender;->a(Lcom/facebook/messaging/tincan/outbound/Sender;LX/Dpe;LX/DpM;LX/DpM;ILX/DpO;[B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 578768
    monitor-exit p0

    return-void

    .line 578769
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
