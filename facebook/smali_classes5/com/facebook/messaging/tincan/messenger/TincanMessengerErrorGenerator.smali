.class public Lcom/facebook/messaging/tincan/messenger/TincanMessengerErrorGenerator;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:LX/2Ox;

.field private final c:LX/2Ow;

.field public final d:LX/0aG;

.field public final e:LX/2P4;

.field public final f:LX/2P0;

.field public final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2Oe;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/2Ox;LX/2Ow;LX/0aG;LX/2P4;LX/2P0;LX/0Or;)V
    .locals 0
    .param p7    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/cache/TincanMessages;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "LX/2Ox;",
            "LX/2Ow;",
            "LX/0aG;",
            "LX/2P4;",
            "LX/2P0;",
            "LX/0Or",
            "<",
            "LX/2Oe;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 572567
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 572568
    iput-object p1, p0, Lcom/facebook/messaging/tincan/messenger/TincanMessengerErrorGenerator;->a:Landroid/content/res/Resources;

    .line 572569
    iput-object p2, p0, Lcom/facebook/messaging/tincan/messenger/TincanMessengerErrorGenerator;->b:LX/2Ox;

    .line 572570
    iput-object p3, p0, Lcom/facebook/messaging/tincan/messenger/TincanMessengerErrorGenerator;->c:LX/2Ow;

    .line 572571
    iput-object p4, p0, Lcom/facebook/messaging/tincan/messenger/TincanMessengerErrorGenerator;->d:LX/0aG;

    .line 572572
    iput-object p5, p0, Lcom/facebook/messaging/tincan/messenger/TincanMessengerErrorGenerator;->e:LX/2P4;

    .line 572573
    iput-object p6, p0, Lcom/facebook/messaging/tincan/messenger/TincanMessengerErrorGenerator;->f:LX/2P0;

    .line 572574
    iput-object p7, p0, Lcom/facebook/messaging/tincan/messenger/TincanMessengerErrorGenerator;->g:LX/0Or;

    .line 572575
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/messaging/tincan/messenger/TincanMessengerErrorGenerator;
    .locals 9

    .prologue
    .line 572616
    new-instance v1, Lcom/facebook/messaging/tincan/messenger/TincanMessengerErrorGenerator;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v2

    check-cast v2, Landroid/content/res/Resources;

    invoke-static {p0}, LX/2Ox;->a(LX/0QB;)LX/2Ox;

    move-result-object v3

    check-cast v3, LX/2Ox;

    invoke-static {p0}, LX/2Ow;->a(LX/0QB;)LX/2Ow;

    move-result-object v4

    check-cast v4, LX/2Ow;

    invoke-static {p0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v5

    check-cast v5, LX/0aG;

    invoke-static {p0}, LX/2P4;->a(LX/0QB;)LX/2P4;

    move-result-object v6

    check-cast v6, LX/2P4;

    invoke-static {p0}, LX/2P0;->a(LX/0QB;)LX/2P0;

    move-result-object v7

    check-cast v7, LX/2P0;

    const/16 v8, 0xce7

    invoke-static {p0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-direct/range {v1 .. v8}, Lcom/facebook/messaging/tincan/messenger/TincanMessengerErrorGenerator;-><init>(Landroid/content/res/Resources;LX/2Ox;LX/2Ow;LX/0aG;LX/2P4;LX/2P0;LX/0Or;)V

    .line 572617
    move-object v0, v1

    .line 572618
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/messages/Message;Ljava/lang/String;)Lcom/facebook/messaging/model/messages/Message;
    .locals 6

    .prologue
    .line 572597
    invoke-static {}, Lcom/facebook/messaging/model/send/SendError;->newBuilder()LX/6fO;

    move-result-object v0

    sget-object v1, LX/6fP;->TINCAN_RETRYABLE:LX/6fP;

    .line 572598
    iput-object v1, v0, LX/6fO;->a:LX/6fP;

    .line 572599
    move-object v0, v0

    .line 572600
    iput-object p2, v0, LX/6fO;->b:Ljava/lang/String;

    .line 572601
    move-object v0, v0

    .line 572602
    iget-wide v2, p1, Lcom/facebook/messaging/model/messages/Message;->d:J

    .line 572603
    iput-wide v2, v0, LX/6fO;->c:J

    .line 572604
    move-object v0, v0

    .line 572605
    invoke-virtual {v0}, LX/6fO;->g()Lcom/facebook/messaging/model/send/SendError;

    move-result-object v0

    .line 572606
    invoke-static {}, Lcom/facebook/messaging/model/messages/Message;->newBuilder()LX/6f7;

    move-result-object v1

    invoke-virtual {v1, p1}, LX/6f7;->a(Lcom/facebook/messaging/model/messages/Message;)LX/6f7;

    move-result-object v1

    sget-object v2, LX/2uW;->FAILED_SEND:LX/2uW;

    .line 572607
    iput-object v2, v1, LX/6f7;->l:LX/2uW;

    .line 572608
    move-object v1, v1

    .line 572609
    iput-object v0, v1, LX/6f7;->u:Lcom/facebook/messaging/model/send/SendError;

    .line 572610
    move-object v1, v1

    .line 572611
    iget-object v2, p0, Lcom/facebook/messaging/tincan/messenger/TincanMessengerErrorGenerator;->b:LX/2Ox;

    iget-object v3, p1, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    .line 572612
    iget-object v4, v1, LX/6f7;->l:LX/2uW;

    move-object v4, v4

    .line 572613
    invoke-virtual {v2, v3, v4}, LX/2Ox;->a(Ljava/lang/String;LX/2uW;)V

    .line 572614
    iget-object v2, p0, Lcom/facebook/messaging/tincan/messenger/TincanMessengerErrorGenerator;->b:LX/2Ox;

    iget-object v3, p1, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, LX/2Ox;->a(Ljava/lang/String;Lcom/facebook/messaging/model/send/SendError;)V

    .line 572615
    invoke-virtual {v1}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 572576
    iget-object v0, p0, Lcom/facebook/messaging/tincan/messenger/TincanMessengerErrorGenerator;->f:LX/2P0;

    invoke-virtual {v0, p1}, LX/2P0;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/messages/MessagesCollection;

    move-result-object v0

    .line 572577
    iget-object v1, v0, Lcom/facebook/messaging/model/messages/MessagesCollection;->b:LX/0Px;

    move-object v3, v1

    .line 572578
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_0
    if-ltz v2, :cond_1

    .line 572579
    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    .line 572580
    iget-object v1, v0, Lcom/facebook/messaging/model/messages/Message;->l:LX/2uW;

    sget-object v4, LX/2uW;->PENDING_SEND:LX/2uW;

    if-ne v1, v4, :cond_0

    const/4 v1, 0x1

    :goto_1
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 572581
    invoke-virtual {p0, v0, p2}, Lcom/facebook/messaging/tincan/messenger/TincanMessengerErrorGenerator;->a(Lcom/facebook/messaging/model/messages/Message;Ljava/lang/String;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v1

    .line 572582
    iget-object v0, p0, Lcom/facebook/messaging/tincan/messenger/TincanMessengerErrorGenerator;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Oe;

    .line 572583
    iget-object v4, v0, LX/2Oe;->a:LX/2OQ;

    invoke-virtual {v4, v1}, LX/2OQ;->a(Lcom/facebook/messaging/model/messages/Message;)V

    .line 572584
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_0

    .line 572585
    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    .line 572586
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/tincan/messenger/TincanMessengerErrorGenerator;->a:Landroid/content/res/Resources;

    const v1, 0x7f0806bf

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 572587
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 572588
    const-string v2, "thread_key"

    invoke-virtual {v4, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 572589
    const-string v2, "message"

    invoke-virtual {v4, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 572590
    iget-object v2, p0, Lcom/facebook/messaging/tincan/messenger/TincanMessengerErrorGenerator;->d:LX/0aG;

    const-string v3, "TincanAdminMessage"

    sget-object v5, LX/1ME;->BY_EXCEPTION:LX/1ME;

    const-class v6, Lcom/facebook/messaging/tincan/messenger/TincanIncomingDispatcher;

    invoke-static {v6}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v6

    const v7, -0x5d1e1ecf

    invoke-static/range {v2 .. v7}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v2

    invoke-interface {v2}, LX/1MF;->start()LX/1ML;

    .line 572591
    iget-object v0, p0, Lcom/facebook/messaging/tincan/messenger/TincanMessengerErrorGenerator;->e:LX/2P4;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, LX/2P4;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;I)Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v1

    .line 572592
    sget-object v0, Lcom/facebook/messaging/service/model/FetchThreadResult;->a:Lcom/facebook/messaging/service/model/FetchThreadResult;

    if-eq v1, v0, :cond_2

    .line 572593
    iget-object v0, p0, Lcom/facebook/messaging/tincan/messenger/TincanMessengerErrorGenerator;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Oe;

    invoke-virtual {v0, v1}, LX/2Oe;->b(Lcom/facebook/messaging/service/model/FetchThreadResult;)V

    .line 572594
    :cond_2
    iget-object v0, p0, Lcom/facebook/messaging/tincan/messenger/TincanMessengerErrorGenerator;->c:LX/2Ow;

    invoke-virtual {v0}, LX/2Ow;->a()V

    .line 572595
    iget-object v0, p0, Lcom/facebook/messaging/tincan/messenger/TincanMessengerErrorGenerator;->c:LX/2Ow;

    invoke-virtual {v0, p1}, LX/2Ow;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 572596
    return-void
.end method
