.class public Lcom/facebook/messaging/tincan/messenger/MessageExpirationHelper;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/0Up;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final i:Ljava/lang/String;

.field private static final j:Ljava/lang/Object;


# instance fields
.field public a:LX/0aG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/2Oe;
    .annotation runtime Lcom/facebook/messaging/cache/TincanMessages;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/2Ox;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Tf;
    .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/2Ow;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/2P0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/2P4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/26j;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 572446
    const-class v0, Lcom/facebook/messaging/tincan/messenger/MessageExpirationHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/tincan/messenger/MessageExpirationHelper;->i:Ljava/lang/String;

    .line 572447
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/facebook/messaging/tincan/messenger/MessageExpirationHelper;->j:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 572444
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 572445
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/messaging/tincan/messenger/MessageExpirationHelper;
    .locals 14

    .prologue
    .line 572413
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 572414
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 572415
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 572416
    if-nez v1, :cond_0

    .line 572417
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 572418
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 572419
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 572420
    sget-object v1, Lcom/facebook/messaging/tincan/messenger/MessageExpirationHelper;->j:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 572421
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 572422
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 572423
    :cond_1
    if-nez v1, :cond_4

    .line 572424
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 572425
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 572426
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 572427
    new-instance v1, Lcom/facebook/messaging/tincan/messenger/MessageExpirationHelper;

    invoke-direct {v1}, Lcom/facebook/messaging/tincan/messenger/MessageExpirationHelper;-><init>()V

    .line 572428
    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v7

    check-cast v7, LX/0aG;

    invoke-static {v0}, LX/2OK;->a(LX/0QB;)LX/2Oe;

    move-result-object v8

    check-cast v8, LX/2Oe;

    invoke-static {v0}, LX/2Ox;->a(LX/0QB;)LX/2Ox;

    move-result-object v9

    check-cast v9, LX/2Ox;

    invoke-static {v0}, LX/0Zo;->a(LX/0QB;)LX/0Tf;

    move-result-object v10

    check-cast v10, LX/0Tf;

    invoke-static {v0}, LX/2Ow;->a(LX/0QB;)LX/2Ow;

    move-result-object v11

    check-cast v11, LX/2Ow;

    invoke-static {v0}, LX/2P0;->a(LX/0QB;)LX/2P0;

    move-result-object v12

    check-cast v12, LX/2P0;

    invoke-static {v0}, LX/2P4;->a(LX/0QB;)LX/2P4;

    move-result-object v13

    check-cast v13, LX/2P4;

    invoke-static {v0}, LX/26j;->b(LX/0QB;)LX/26j;

    move-result-object p0

    check-cast p0, LX/26j;

    .line 572429
    iput-object v7, v1, Lcom/facebook/messaging/tincan/messenger/MessageExpirationHelper;->a:LX/0aG;

    iput-object v8, v1, Lcom/facebook/messaging/tincan/messenger/MessageExpirationHelper;->b:LX/2Oe;

    iput-object v9, v1, Lcom/facebook/messaging/tincan/messenger/MessageExpirationHelper;->c:LX/2Ox;

    iput-object v10, v1, Lcom/facebook/messaging/tincan/messenger/MessageExpirationHelper;->d:LX/0Tf;

    iput-object v11, v1, Lcom/facebook/messaging/tincan/messenger/MessageExpirationHelper;->e:LX/2Ow;

    iput-object v12, v1, Lcom/facebook/messaging/tincan/messenger/MessageExpirationHelper;->f:LX/2P0;

    iput-object v13, v1, Lcom/facebook/messaging/tincan/messenger/MessageExpirationHelper;->g:LX/2P4;

    iput-object p0, v1, Lcom/facebook/messaging/tincan/messenger/MessageExpirationHelper;->h:LX/26j;

    .line 572430
    move-object v1, v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 572431
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 572432
    if-nez v1, :cond_2

    .line 572433
    sget-object v0, Lcom/facebook/messaging/tincan/messenger/MessageExpirationHelper;->j:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/tincan/messenger/MessageExpirationHelper;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 572434
    :goto_1
    if-eqz v0, :cond_3

    .line 572435
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 572436
    :goto_3
    check-cast v0, Lcom/facebook/messaging/tincan/messenger/MessageExpirationHelper;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 572437
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 572438
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 572439
    :catchall_1
    move-exception v0

    .line 572440
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 572441
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 572442
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 572443
    :cond_2
    :try_start_8
    sget-object v0, Lcom/facebook/messaging/tincan/messenger/MessageExpirationHelper;->j:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/tincan/messenger/MessageExpirationHelper;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private declared-synchronized a()V
    .locals 5

    .prologue
    .line 572410
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/tincan/messenger/MessageExpirationHelper;->d:LX/0Tf;

    new-instance v1, Lcom/facebook/messaging/tincan/messenger/MessageExpirationHelper$3;

    invoke-direct {v1, p0}, Lcom/facebook/messaging/tincan/messenger/MessageExpirationHelper$3;-><init>(Lcom/facebook/messaging/tincan/messenger/MessageExpirationHelper;)V

    const-wide/32 v2, 0x1b7740

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v3, v4}, LX/0Tf;->a(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)LX/0YG;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 572411
    monitor-exit p0

    return-void

    .line 572412
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized a(Lcom/facebook/messaging/tincan/messenger/MessageExpirationHelper;Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/0Rf;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "LX/0Rf",
            "<",
            "Lcom/facebook/messaging/model/messages/Message;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 572399
    monitor-enter p0

    :try_start_0
    invoke-virtual {p2}, LX/0Rf;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 572400
    :goto_0
    monitor-exit p0

    return-void

    .line 572401
    :cond_0
    :try_start_1
    new-instance v1, LX/0cA;

    invoke-direct {v1}, LX/0cA;-><init>()V

    .line 572402
    invoke-virtual {p2}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    .line 572403
    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 572404
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 572405
    :cond_1
    :try_start_2
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 572406
    const-string v0, "deleteMessagesParams"

    new-instance v3, Lcom/facebook/messaging/service/model/DeleteMessagesParams;

    invoke-virtual {v1}, LX/0cA;->b()LX/0Rf;

    move-result-object v1

    sget-object v4, LX/6hi;->CLIENT_ONLY:LX/6hi;

    invoke-direct {v3, v1, v4, p1}, Lcom/facebook/messaging/service/model/DeleteMessagesParams;-><init>(LX/0Rf;LX/6hi;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 572407
    const-string v0, "KEEP_IN_DB_AS_HIDDEN"

    const/4 v1, 0x1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 572408
    iget-object v0, p0, Lcom/facebook/messaging/tincan/messenger/MessageExpirationHelper;->a:LX/0aG;

    const-string v1, "delete_messages"

    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    const-class v4, Lcom/facebook/messaging/tincan/messenger/MessageExpirationHelper;

    invoke-static {v4}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    const v5, -0x2e62a718

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/1MF;->setFireAndForget(Z)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    .line 572409
    invoke-direct {p0}, Lcom/facebook/messaging/tincan/messenger/MessageExpirationHelper;->a()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public static declared-synchronized a(Lcom/facebook/messaging/tincan/messenger/MessageExpirationHelper;Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/util/Set;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 572391
    monitor-enter p0

    :try_start_0
    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 572392
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 572393
    sget-object v4, LX/0SF;->a:LX/0SF;

    move-object v4, v4

    .line 572394
    invoke-virtual {v4}, LX/0SF;->a()J

    move-result-wide v4

    sub-long/2addr v2, v4

    .line 572395
    const-wide/16 v4, 0x0

    invoke-static {v4, v5, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    .line 572396
    iget-object v4, p0, Lcom/facebook/messaging/tincan/messenger/MessageExpirationHelper;->d:LX/0Tf;

    new-instance v5, Lcom/facebook/messaging/tincan/messenger/MessageExpirationHelper$1;

    invoke-direct {v5, p0, p1, v0}, Lcom/facebook/messaging/tincan/messenger/MessageExpirationHelper$1;-><init>(Lcom/facebook/messaging/tincan/messenger/MessageExpirationHelper;Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/Long;)V

    const-wide/16 v6, 0x3e8

    add-long/2addr v2, v6

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v4, v5, v2, v3, v0}, LX/0Tf;->a(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)LX/0YG;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 572397
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 572398
    :cond_0
    monitor-exit p0

    return-void
.end method

.method public static declared-synchronized b(Lcom/facebook/messaging/tincan/messenger/MessageExpirationHelper;)V
    .locals 4

    .prologue
    .line 572384
    monitor-enter p0

    .line 572385
    :try_start_0
    sget-object v0, LX/0SF;->a:LX/0SF;

    move-object v0, v0

    .line 572386
    invoke-virtual {v0}, LX/0SF;->a()J

    move-result-wide v0

    const-wide/32 v2, 0x1b7740

    sub-long/2addr v0, v2

    .line 572387
    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    .line 572388
    iget-object v2, p0, Lcom/facebook/messaging/tincan/messenger/MessageExpirationHelper;->c:LX/2Ox;

    invoke-virtual {v2, v0, v1}, LX/2Ox;->a(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 572389
    monitor-exit p0

    return-void

    .line 572390
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized b(Lcom/facebook/messaging/tincan/messenger/MessageExpirationHelper;Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/util/Set;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 572376
    monitor-enter p0

    :try_start_0
    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 572377
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 572378
    sget-object v4, LX/0SF;->a:LX/0SF;

    move-object v4, v4

    .line 572379
    invoke-virtual {v4}, LX/0SF;->a()J

    move-result-wide v4

    sub-long/2addr v2, v4

    .line 572380
    const-wide/16 v4, 0x0

    invoke-static {v4, v5, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    .line 572381
    iget-object v4, p0, Lcom/facebook/messaging/tincan/messenger/MessageExpirationHelper;->d:LX/0Tf;

    new-instance v5, Lcom/facebook/messaging/tincan/messenger/MessageExpirationHelper$2;

    invoke-direct {v5, p0, p1, v0}, Lcom/facebook/messaging/tincan/messenger/MessageExpirationHelper$2;-><init>(Lcom/facebook/messaging/tincan/messenger/MessageExpirationHelper;Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/Long;)V

    const-wide/16 v6, 0x3e8

    add-long/2addr v2, v6

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v4, v5, v2, v3, v0}, LX/0Tf;->a(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)LX/0YG;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 572382
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 572383
    :cond_0
    monitor-exit p0

    return-void
.end method

.method public static declared-synchronized b$redex0(Lcom/facebook/messaging/tincan/messenger/MessageExpirationHelper;Lcom/facebook/messaging/model/threadkey/ThreadKey;J)V
    .locals 4

    .prologue
    .line 572372
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/tincan/messenger/MessageExpirationHelper;->f:LX/2P0;

    iget-wide v2, p1, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, p2, p3, v1}, LX/2P0;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;JLjava/lang/String;)LX/0Rf;

    move-result-object v0

    .line 572373
    invoke-static {p0, p1, v0}, Lcom/facebook/messaging/tincan/messenger/MessageExpirationHelper;->a(Lcom/facebook/messaging/tincan/messenger/MessageExpirationHelper;Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/0Rf;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 572374
    monitor-exit p0

    return-void

    .line 572375
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized c(Lcom/facebook/messaging/tincan/messenger/MessageExpirationHelper;Lcom/facebook/messaging/model/threadkey/ThreadKey;J)V
    .locals 4

    .prologue
    .line 572368
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/tincan/messenger/MessageExpirationHelper;->f:LX/2P0;

    iget-wide v2, p1, Lcom/facebook/messaging/model/threadkey/ThreadKey;->e:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, p2, p3, v1}, LX/2P0;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;JLjava/lang/String;)LX/0Rf;

    move-result-object v0

    .line 572369
    invoke-static {p0, p1, v0}, Lcom/facebook/messaging/tincan/messenger/MessageExpirationHelper;->a(Lcom/facebook/messaging/tincan/messenger/MessageExpirationHelper;Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/0Rf;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 572370
    monitor-exit p0

    return-void

    .line 572371
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/facebook/messaging/model/threadkey/ThreadKey;J)V
    .locals 5

    .prologue
    .line 572332
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/tincan/messenger/MessageExpirationHelper;->f:LX/2P0;

    .line 572333
    invoke-static {}, LX/0uu;->a()LX/0uw;

    move-result-object v1

    .line 572334
    sget-object v2, LX/2P1;->b:LX/0U1;

    invoke-virtual {p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 572335
    sget-object v2, LX/2P1;->f:LX/0U1;

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0U1;->c(Ljava/lang/String;)LX/0ux;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 572336
    sget-object v2, LX/2P1;->l:LX/0U1;

    const-string v3, "0"

    invoke-virtual {v2, v3}, LX/0U1;->d(Ljava/lang/String;)LX/0ux;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 572337
    invoke-static {v0, v1}, LX/2P0;->a(LX/2P0;LX/0ux;)LX/0Rf;

    move-result-object v1

    move-object v0, v1

    .line 572338
    invoke-virtual {v0}, LX/0Rf;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_0

    .line 572339
    :goto_0
    monitor-exit p0

    return-void

    .line 572340
    :cond_0
    :try_start_1
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 572341
    sget-object v2, LX/0SF;->a:LX/0SF;

    move-object v2, v2

    .line 572342
    invoke-virtual {v2}, LX/0SF;->a()J

    .line 572343
    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    .line 572344
    iget-object v3, v0, Lcom/facebook/messaging/model/messages/Message;->K:Ljava/lang/Long;

    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 572345
    iget-object v3, v0, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v3, p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->equals(Ljava/lang/Object;)Z

    move-result v3

    invoke-static {v3}, LX/0PB;->checkArgument(Z)V

    .line 572346
    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->K:Ljava/lang/Long;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 572347
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 572348
    :cond_1
    :try_start_2
    invoke-static {p0, p1, v1}, Lcom/facebook/messaging/tincan/messenger/MessageExpirationHelper;->b(Lcom/facebook/messaging/tincan/messenger/MessageExpirationHelper;Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/util/Set;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public final init()V
    .locals 5

    .prologue
    .line 572349
    iget-object v0, p0, Lcom/facebook/messaging/tincan/messenger/MessageExpirationHelper;->h:LX/26j;

    invoke-virtual {v0}, LX/26j;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 572350
    :cond_0
    :goto_0
    return-void

    .line 572351
    :cond_1
    :try_start_0
    invoke-static {p0}, Lcom/facebook/messaging/tincan/messenger/MessageExpirationHelper;->b(Lcom/facebook/messaging/tincan/messenger/MessageExpirationHelper;)V

    .line 572352
    iget-object v0, p0, Lcom/facebook/messaging/tincan/messenger/MessageExpirationHelper;->g:LX/2P4;

    invoke-virtual {v0}, LX/2P4;->b()LX/0P1;

    move-result-object v0

    .line 572353
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 572354
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 572355
    new-instance v3, LX/IuA;

    invoke-direct {v3, p0, v0, v2, v1}, LX/IuA;-><init>(Lcom/facebook/messaging/tincan/messenger/MessageExpirationHelper;LX/0P1;Ljava/util/Map;Ljava/util/Map;)V

    .line 572356
    sget-object v0, LX/2P1;->l:LX/0U1;

    .line 572357
    iget-object v4, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v4

    .line 572358
    const-string v4, "0"

    invoke-static {v0, v4}, LX/0uu;->e(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v0

    .line 572359
    iget-object v4, p0, Lcom/facebook/messaging/tincan/messenger/MessageExpirationHelper;->f:LX/2P0;

    invoke-virtual {v4, v0, v3}, LX/2P0;->a(LX/0ux;LX/0QK;)V

    .line 572360
    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 572361
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-static {p0, v1, v0}, Lcom/facebook/messaging/tincan/messenger/MessageExpirationHelper;->a(Lcom/facebook/messaging/tincan/messenger/MessageExpirationHelper;Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/util/Set;)V

    goto :goto_1

    .line 572362
    :cond_2
    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 572363
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-static {p0, v1, v0}, Lcom/facebook/messaging/tincan/messenger/MessageExpirationHelper;->b(Lcom/facebook/messaging/tincan/messenger/MessageExpirationHelper;Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/util/Set;)V

    goto :goto_2

    .line 572364
    :cond_3
    iget-object v0, p0, Lcom/facebook/messaging/tincan/messenger/MessageExpirationHelper;->f:LX/2P0;

    invoke-virtual {v0}, LX/2P0;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 572365
    invoke-direct {p0}, Lcom/facebook/messaging/tincan/messenger/MessageExpirationHelper;->a()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 572366
    :catch_0
    move-exception v0

    .line 572367
    sget-object v1, Lcom/facebook/messaging/tincan/messenger/MessageExpirationHelper;->i:Ljava/lang/String;

    const-string v2, "Failed to reschedule expiration jobs for tincan ephemeral messages."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0
.end method
