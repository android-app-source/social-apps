.class public final Lcom/facebook/messaging/tincan/messenger/AttachmentUploadRetryColdStartTrigger$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/2OJ;


# direct methods
.method public constructor <init>(LX/2OJ;)V
    .locals 0

    .prologue
    .line 578928
    iput-object p1, p0, Lcom/facebook/messaging/tincan/messenger/AttachmentUploadRetryColdStartTrigger$2;->a:LX/2OJ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 578929
    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/tincan/messenger/AttachmentUploadRetryColdStartTrigger$2;->a:LX/2OJ;

    iget-object v0, v0, LX/2OJ;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3R7;

    .line 578930
    iget-object v1, v0, LX/3R7;->c:LX/2P0;

    invoke-virtual {v1}, LX/2P0;->b()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/ui/media/attachments/MediaResource;

    .line 578931
    invoke-static {v0, v1}, LX/3R7;->a(LX/3R7;Lcom/facebook/ui/media/attachments/MediaResource;)V

    goto :goto_0
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_1

    .line 578932
    :cond_0
    :goto_1
    return-void

    .line 578933
    :catch_0
    move-exception v0

    .line 578934
    :goto_2
    sget-object v1, LX/2OJ;->a:Ljava/lang/Class;

    const-string v2, "Upload retry failed most probably due to db access error"

    invoke-static {v1, v2, v0}, LX/01m;->c(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 578935
    :catch_1
    move-exception v0

    goto :goto_2
.end method
