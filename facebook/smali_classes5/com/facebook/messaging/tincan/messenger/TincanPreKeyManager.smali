.class public Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/2PD;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final b:LX/2PC;

.field private static final c:LX/2PC;

.field private static volatile t:Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;


# instance fields
.field private final d:LX/0TD;

.field private final e:LX/0aG;

.field private final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Dof;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/2P4;

.field public final h:LX/2PE;

.field private final i:LX/2Ox;

.field private final j:LX/0SF;

.field private final k:LX/2PG;

.field private final l:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/IuC;",
            ">;"
        }
    .end annotation
.end field

.field private final m:LX/2PH;

.field public final n:LX/2PK;

.field public final o:LX/2PL;

.field private final p:Lcom/facebook/messaging/tincan/messenger/TincanMessengerErrorGenerator;

.field private final q:Landroid/content/res/Resources;

.field private final r:LX/2PM;

.field public s:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 572543
    const-class v0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;

    sput-object v0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->a:Ljava/lang/Class;

    .line 572544
    new-instance v0, LX/2PC;

    const-string v1, "prekey_upload_state"

    invoke-direct {v0, v1}, LX/2PC;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->b:LX/2PC;

    .line 572545
    new-instance v0, LX/2PC;

    const-string v1, "last_prekey_upload_timestamp_ms"

    invoke-direct {v0, v1}, LX/2PC;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->c:LX/2PC;

    return-void
.end method

.method public constructor <init>(LX/0TD;LX/0aG;LX/0Or;LX/2P4;LX/2PE;LX/2Ox;LX/0SF;LX/2PG;LX/0Or;LX/2PH;LX/2PK;LX/2PL;Lcom/facebook/messaging/tincan/messenger/TincanMessengerErrorGenerator;Landroid/content/res/Resources;LX/2PM;)V
    .locals 4
    .param p1    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0TD;",
            "LX/0aG;",
            "LX/0Or",
            "<",
            "LX/Dof;",
            ">;",
            "LX/2P4;",
            "LX/2PE;",
            "LX/2Ox;",
            "LX/0SF;",
            "LX/2PG;",
            "LX/0Or",
            "<",
            "LX/IuC;",
            ">;",
            "LX/2PH;",
            "LX/2PK;",
            "LX/2PL;",
            "Lcom/facebook/messaging/tincan/messenger/TincanMessengerErrorGenerator;",
            "Landroid/content/res/Resources;",
            "LX/2PM;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 572523
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 572524
    iput-object p1, p0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->d:LX/0TD;

    .line 572525
    iput-object p2, p0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->e:LX/0aG;

    .line 572526
    iput-object p3, p0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->f:LX/0Or;

    .line 572527
    iput-object p4, p0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->g:LX/2P4;

    .line 572528
    iput-object p5, p0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->h:LX/2PE;

    .line 572529
    iput-object p6, p0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->i:LX/2Ox;

    .line 572530
    iput-object p7, p0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->j:LX/0SF;

    .line 572531
    iput-object p8, p0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->k:LX/2PG;

    .line 572532
    iput-object p9, p0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->l:LX/0Or;

    .line 572533
    iput-object p10, p0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->m:LX/2PH;

    .line 572534
    iput-object p11, p0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->n:LX/2PK;

    .line 572535
    move-object/from16 v0, p12

    iput-object v0, p0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->o:LX/2PL;

    .line 572536
    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->p:Lcom/facebook/messaging/tincan/messenger/TincanMessengerErrorGenerator;

    .line 572537
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->q:Landroid/content/res/Resources;

    .line 572538
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->r:LX/2PM;

    .line 572539
    iget-object v1, p0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->m:LX/2PH;

    invoke-virtual {v1, p0}, LX/2PI;->a(Ljava/lang/Object;)V

    .line 572540
    iget-object v1, p0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->n:LX/2PK;

    invoke-virtual {v1, p0}, LX/2PI;->a(Ljava/lang/Object;)V

    .line 572541
    iget-object v1, p0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->o:LX/2PL;

    new-instance v2, LX/2PN;

    invoke-direct {v2, p0}, LX/2PN;-><init>(Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;)V

    invoke-virtual {v1, v2}, LX/2PI;->a(Ljava/lang/Object;)V

    .line 572542
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;
    .locals 3

    .prologue
    .line 572513
    sget-object v0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->t:Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;

    if-nez v0, :cond_1

    .line 572514
    const-class v1, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;

    monitor-enter v1

    .line 572515
    :try_start_0
    sget-object v0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->t:Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 572516
    if-eqz v2, :cond_0

    .line 572517
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->b(LX/0QB;)Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->t:Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 572518
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 572519
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 572520
    :cond_1
    sget-object v0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->t:Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;

    return-object v0

    .line 572521
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 572522
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static declared-synchronized a(Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/Dop;)V
    .locals 1

    .prologue
    .line 572510
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->i:LX/2Ox;

    invoke-virtual {v0, p1, p2}, LX/2Ox;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/Dop;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 572511
    monitor-exit p0

    return-void

    .line 572512
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized a$redex0(Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;LX/IuM;)V
    .locals 3

    .prologue
    .line 572507
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dof;

    sget-object v1, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->b:LX/2PC;

    invoke-virtual {p1}, LX/IuM;->getValue()I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/Dod;->b(LX/2PC;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 572508
    monitor-exit p0

    return-void

    .line 572509
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized a$redex0(Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;Ljava/util/List;LX/Ebk;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/Ebg;",
            ">;",
            "LX/Ebk;",
            ")V"
        }
    .end annotation

    .prologue
    .line 572503
    monitor-enter p0

    :try_start_0
    sget-object v0, LX/IuM;->UPLOADING:LX/IuM;

    invoke-static {p0, v0}, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->a$redex0(Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;LX/IuM;)V

    .line 572504
    iget-object v0, p0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->m:LX/2PH;

    const/4 v1, 0x1

    new-array v1, v1, [B

    const/4 v2, 0x0

    const/4 v3, 0x0

    aput-byte v3, v1, v2

    invoke-virtual {v0, v1, p1, p2}, LX/2PH;->a([BLjava/util/List;LX/Ebk;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 572505
    monitor-exit p0

    return-void

    .line 572506
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static b(LX/0QB;)Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;
    .locals 17

    .prologue
    .line 572501
    new-instance v1, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;

    invoke-static/range {p0 .. p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v2

    check-cast v2, LX/0TD;

    invoke-static/range {p0 .. p0}, LX/0aF;->getInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v3

    check-cast v3, LX/0aG;

    const/16 v4, 0x2a14

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static/range {p0 .. p0}, LX/2P4;->a(LX/0QB;)LX/2P4;

    move-result-object v5

    check-cast v5, LX/2P4;

    invoke-static/range {p0 .. p0}, LX/2PE;->a(LX/0QB;)LX/2PE;

    move-result-object v6

    check-cast v6, LX/2PE;

    invoke-static/range {p0 .. p0}, LX/2Ox;->a(LX/0QB;)LX/2Ox;

    move-result-object v7

    check-cast v7, LX/2Ox;

    invoke-static/range {p0 .. p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v8

    check-cast v8, LX/0SF;

    invoke-static/range {p0 .. p0}, LX/2PG;->a(LX/0QB;)LX/2PG;

    move-result-object v9

    check-cast v9, LX/2PG;

    const/16 v10, 0x2a2c

    move-object/from16 v0, p0

    invoke-static {v0, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    invoke-static/range {p0 .. p0}, LX/2PH;->a(LX/0QB;)LX/2PH;

    move-result-object v11

    check-cast v11, LX/2PH;

    invoke-static/range {p0 .. p0}, LX/2PK;->a(LX/0QB;)LX/2PK;

    move-result-object v12

    check-cast v12, LX/2PK;

    invoke-static/range {p0 .. p0}, LX/2PL;->a(LX/0QB;)LX/2PL;

    move-result-object v13

    check-cast v13, LX/2PL;

    invoke-static/range {p0 .. p0}, Lcom/facebook/messaging/tincan/messenger/TincanMessengerErrorGenerator;->a(LX/0QB;)Lcom/facebook/messaging/tincan/messenger/TincanMessengerErrorGenerator;

    move-result-object v14

    check-cast v14, Lcom/facebook/messaging/tincan/messenger/TincanMessengerErrorGenerator;

    invoke-static/range {p0 .. p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v15

    check-cast v15, Landroid/content/res/Resources;

    invoke-static/range {p0 .. p0}, LX/2PM;->a(LX/0QB;)LX/2PM;

    move-result-object v16

    check-cast v16, LX/2PM;

    invoke-direct/range {v1 .. v16}, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;-><init>(LX/0TD;LX/0aG;LX/0Or;LX/2P4;LX/2PE;LX/2Ox;LX/0SF;LX/2PG;LX/0Or;LX/2PH;LX/2PK;LX/2PL;Lcom/facebook/messaging/tincan/messenger/TincanMessengerErrorGenerator;Landroid/content/res/Resources;LX/2PM;)V

    .line 572502
    return-object v1
.end method

.method private declared-synchronized e(Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/Dop;
    .locals 1

    .prologue
    .line 572500
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->g:LX/2P4;

    invoke-virtual {v0, p1}, LX/2P4;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/Dop;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 4

    .prologue
    .line 572448
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dof;

    sget-object v1, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->c:LX/2PC;

    iget-object v2, p0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->j:LX/0SF;

    invoke-virtual {v2}, LX/0SF;->a()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, LX/Dod;->b(LX/2PC;J)V

    .line 572449
    sget-object v0, LX/IuM;->COMPLETED:LX/IuM;

    invoke-static {p0, v0}, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->a$redex0(Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;LX/IuM;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 572450
    monitor-exit p0

    return-void

    .line 572451
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a([B)V
    .locals 4

    .prologue
    .line 572495
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p1}, Ljava/lang/String;-><init>([B)V

    invoke-static {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(Ljava/lang/String;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    .line 572496
    iget-object v1, p0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->p:Lcom/facebook/messaging/tincan/messenger/TincanMessengerErrorGenerator;

    iget-object v2, p0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->q:Landroid/content/res/Resources;

    const v3, 0x7f0806c7

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/messaging/tincan/messenger/TincanMessengerErrorGenerator;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)V

    .line 572497
    sget-object v1, LX/Dop;->FAILED:LX/Dop;

    invoke-static {p0, v0, v1}, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->a(Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/Dop;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 572498
    monitor-exit p0

    return-void

    .line 572499
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a([BLjava/lang/Long;LX/DpK;Z)V
    .locals 10

    .prologue
    .line 572546
    monitor-enter p0

    :try_start_0
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 572547
    const-string v0, "prekey_bundle"

    .line 572548
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 572549
    const-string v7, "codename"

    iget-object v8, p3, LX/DpK;->suggested_codename:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 572550
    const-string v7, "user_id_to"

    iget-object v8, p3, LX/DpK;->msg_to:LX/DpM;

    iget-object v8, v8, LX/DpM;->user_id:Ljava/lang/Long;

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-virtual {v6, v7, v8, v9}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 572551
    const-string v7, "device_id_to"

    iget-object v8, p3, LX/DpK;->msg_to:LX/DpM;

    iget-object v8, v8, LX/DpM;->instance_id:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 572552
    const-string v7, "prekey_id"

    iget-object v8, p3, LX/DpK;->pre_key_with_id:LX/DpU;

    iget-object v8, v8, LX/DpU;->id:Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-virtual {v6, v7, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 572553
    const-string v7, "prekey"

    iget-object v8, p3, LX/DpK;->pre_key_with_id:LX/DpU;

    iget-object v8, v8, LX/DpU;->public_key:[B

    invoke-virtual {v6, v7, v8}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 572554
    const-string v7, "signed_prekey_id"

    iget-object v8, p3, LX/DpK;->signed_pre_key_with_id:LX/Dpf;

    iget-object v8, v8, LX/Dpf;->public_key_with_id:LX/DpU;

    iget-object v8, v8, LX/DpU;->id:Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-virtual {v6, v7, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 572555
    const-string v7, "signed_prekey"

    iget-object v8, p3, LX/DpK;->signed_pre_key_with_id:LX/Dpf;

    iget-object v8, v8, LX/Dpf;->public_key_with_id:LX/DpU;

    iget-object v8, v8, LX/DpU;->public_key:[B

    invoke-virtual {v6, v7, v8}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 572556
    const-string v7, "signed_prekey_signature"

    iget-object v8, p3, LX/DpK;->signed_pre_key_with_id:LX/Dpf;

    iget-object v8, v8, LX/Dpf;->signature:[B

    invoke-virtual {v6, v7, v8}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 572557
    const-string v7, "identity_key"

    iget-object v8, p3, LX/DpK;->identity_key:[B

    invoke-virtual {v6, v7, v8}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 572558
    move-object v1, v6

    .line 572559
    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 572560
    const-string v0, "is_multidevice"

    invoke-virtual {v2, v0, p4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 572561
    iget-object v0, p0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->e:LX/0aG;

    const-string v1, "TincanProcessNewPreKey"

    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    const-class v4, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;

    invoke-static {v4}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    const v5, 0x302b0a67

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    .line 572562
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p1}, Ljava/lang/String;-><init>([B)V

    invoke-static {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(Ljava/lang/String;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    .line 572563
    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    sget-object v1, LX/5e9;->TINCAN:LX/5e9;

    if-ne v0, v1, :cond_0

    .line 572564
    invoke-virtual {p0, p1}, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->b([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 572565
    :cond_0
    monitor-exit p0

    return-void

    .line 572566
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z
    .locals 2

    .prologue
    .line 572494
    invoke-direct {p0, p1}, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->e(Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/Dop;

    move-result-object v0

    sget-object v1, LX/Dop;->LOOKING_UP:LX/Dop;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final declared-synchronized b()V
    .locals 1

    .prologue
    .line 572491
    monitor-enter p0

    :try_start_0
    sget-object v0, LX/IuM;->FAILED_UPLOAD:LX/IuM;

    invoke-static {p0, v0}, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->a$redex0(Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;LX/IuM;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 572492
    monitor-exit p0

    return-void

    .line 572493
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V
    .locals 5

    .prologue
    .line 572482
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->i(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 572483
    invoke-direct {p0, p1}, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->e(Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/Dop;

    move-result-object v0

    sget-object v1, LX/Dop;->LOOKING_UP:LX/Dop;

    if-ne v0, v1, :cond_0

    .line 572484
    iget-object v0, p0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->r:LX/2PM;

    const-string v1, "Lookup already in progress"

    invoke-virtual {v0, v1}, LX/2PM;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 572485
    :goto_0
    monitor-exit p0

    return-void

    .line 572486
    :cond_0
    :try_start_1
    invoke-virtual {p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->f()Z

    move-result v0

    if-nez v0, :cond_1

    .line 572487
    sget-object v0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->a:Ljava/lang/Class;

    const-string v1, "Unable to look up keys for thread type %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p1, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    invoke-virtual {v4}, LX/5e9;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 572488
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 572489
    :cond_1
    :try_start_2
    sget-object v0, LX/Dop;->LOOKING_UP:LX/Dop;

    invoke-static {p0, p1, v0}, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->a(Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/Dop;)V

    .line 572490
    iget-object v0, p0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->d:LX/0TD;

    new-instance v1, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager$3;

    invoke-direct {v1, p0, p1}, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager$3;-><init>(Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    const v2, -0x4105959a

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public final declared-synchronized b([B)V
    .locals 6

    .prologue
    .line 572477
    monitor-enter p0

    :try_start_0
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 572478
    const-string v0, "thread_key"

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, p1}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 572479
    iget-object v0, p0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->e:LX/0aG;

    const-string v1, "CompletePrekeyDelivery"

    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    const-class v4, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;

    invoke-static {v4}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    const v5, 0x329840dc

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 572480
    monitor-exit p0

    return-void

    .line 572481
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()V
    .locals 1

    .prologue
    .line 572474
    monitor-enter p0

    :try_start_0
    sget-object v0, LX/IuM;->FAILED_UPLOAD:LX/IuM;

    invoke-static {p0, v0}, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->a$redex0(Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;LX/IuM;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 572475
    monitor-exit p0

    return-void

    .line 572476
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final c(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z
    .locals 2

    .prologue
    .line 572472
    invoke-direct {p0, p1}, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->e(Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/Dop;

    move-result-object v0

    .line 572473
    sget-object v1, LX/Dop;->NOT_STARTED:LX/Dop;

    if-eq v0, v1, :cond_0

    sget-object v1, LX/Dop;->FAILED:LX/Dop;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final declared-synchronized d()Z
    .locals 3

    .prologue
    .line 572465
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->s:Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 572466
    const/4 v0, 0x0

    .line 572467
    :goto_0
    monitor-exit p0

    return v0

    .line 572468
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->d:LX/0TD;

    new-instance v1, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager$1;

    invoke-direct {v1, p0}, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager$1;-><init>(Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/lang/Runnable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->s:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 572469
    iget-object v0, p0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->s:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v1, LX/IuJ;

    invoke-direct {v1, p0}, LX/IuJ;-><init>(Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;)V

    iget-object v2, p0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->d:LX/0TD;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 572470
    const/4 v0, 0x1

    goto :goto_0

    .line 572471
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/Ebg;",
            ">;"
        }
    .end annotation

    .prologue
    .line 572460
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->k:LX/2PG;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, LX/2PG;->a(I)LX/IuI;

    move-result-object v2

    .line 572461
    iget-object v0, p0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->k:LX/2PG;

    iget v1, v2, LX/IuI;->a:I

    invoke-virtual {v0, v1}, LX/2PG;->b(I)V

    .line 572462
    iget-object v0, p0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->l:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IuC;

    iget-object v1, v2, LX/IuI;->b:Ljava/lang/Object;

    check-cast v1, Ljava/util/Collection;

    invoke-virtual {v0, v1}, LX/IuC;->a(Ljava/util/Collection;)V

    .line 572463
    iget-object v0, v2, LX/IuI;->b:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 572464
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized f()LX/Ebk;
    .locals 4

    .prologue
    .line 572452
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->k:LX/2PG;

    invoke-virtual {v0}, LX/2PG;->a()LX/IuI;
    :try_end_0
    .catch LX/Eag; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 572453
    :try_start_1
    iget-object v0, p0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->k:LX/2PG;

    iget v1, v2, LX/IuI;->a:I

    invoke-virtual {v0, v1}, LX/2PG;->c(I)V

    .line 572454
    iget-object v0, p0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->l:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IuC;

    iget-object v1, v2, LX/IuI;->b:Ljava/lang/Object;

    check-cast v1, LX/Ebk;

    invoke-virtual {v1}, LX/Ebk;->a()I

    move-result v3

    iget-object v1, v2, LX/IuI;->b:Ljava/lang/Object;

    check-cast v1, LX/Ebk;

    invoke-virtual {v0, v3, v1}, LX/IuC;->a(ILX/Ebk;)V

    .line 572455
    iget-object v0, v2, LX/IuI;->b:Ljava/lang/Object;

    check-cast v0, LX/Ebk;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v0

    .line 572456
    :catch_0
    move-exception v0

    .line 572457
    :try_start_2
    sget-object v1, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->a:Ljava/lang/Class;

    const-string v2, "Failed to generate signed pre-key"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 572458
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 572459
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
