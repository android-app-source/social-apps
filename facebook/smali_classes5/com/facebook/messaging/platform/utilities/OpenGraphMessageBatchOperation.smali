.class public Lcom/facebook/messaging/platform/utilities/OpenGraphMessageBatchOperation;
.super LX/2QZ;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field public final b:LX/18V;

.field public final c:LX/2Qw;

.field private final d:LX/2Qy;

.field public final e:LX/2Qz;

.field public final f:LX/2R0;

.field public final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2Qv;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/18V;LX/2Qw;LX/2Qy;LX/2Qz;LX/2R0;LX/0Or;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/http/protocol/ApiMethodRunner;",
            "LX/2Qw;",
            "LX/2Qy;",
            "LX/2Qz;",
            "LX/2R0;",
            "LX/0Or",
            "<",
            "LX/2Qv;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 573012
    const-string v0, "platform_open_graph_share_upload"

    invoke-direct {p0, v0}, LX/2QZ;-><init>(Ljava/lang/String;)V

    .line 573013
    iput-object p1, p0, Lcom/facebook/messaging/platform/utilities/OpenGraphMessageBatchOperation;->b:LX/18V;

    .line 573014
    iput-object p2, p0, Lcom/facebook/messaging/platform/utilities/OpenGraphMessageBatchOperation;->c:LX/2Qw;

    .line 573015
    iput-object p3, p0, Lcom/facebook/messaging/platform/utilities/OpenGraphMessageBatchOperation;->d:LX/2Qy;

    .line 573016
    iput-object p4, p0, Lcom/facebook/messaging/platform/utilities/OpenGraphMessageBatchOperation;->e:LX/2Qz;

    .line 573017
    iput-object p5, p0, Lcom/facebook/messaging/platform/utilities/OpenGraphMessageBatchOperation;->f:LX/2R0;

    .line 573018
    iput-object p6, p0, Lcom/facebook/messaging/platform/utilities/OpenGraphMessageBatchOperation;->g:LX/0Or;

    .line 573019
    return-void
.end method

.method public static a(Lcom/facebook/messaging/platform/utilities/OpenGraphMessageBatchOperation;LX/2VK;LX/8PF;Lcom/facebook/share/model/ComposerAppAttribution;Landroid/os/Bundle;)V
    .locals 15

    .prologue
    .line 573020
    new-instance v2, Lcom/facebook/platform/opengraph/server/PublishOpenGraphActionMethod$Params;

    invoke-virtual/range {p2 .. p2}, LX/8PF;->b()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    move-object/from16 v1, p4

    invoke-virtual {v0, v1}, LX/8PF;->a(Landroid/os/Bundle;)LX/0m9;

    move-result-object v4

    const/4 v5, 0x0

    const-string v6, "message"

    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {p3 .. p3}, Lcom/facebook/share/model/ComposerAppAttribution;->a()Ljava/lang/String;

    move-result-object v12

    invoke-virtual/range {p3 .. p3}, Lcom/facebook/share/model/ComposerAppAttribution;->b()Ljava/lang/String;

    move-result-object v13

    invoke-virtual/range {p3 .. p3}, Lcom/facebook/share/model/ComposerAppAttribution;->c()Ljava/lang/String;

    move-result-object v14

    invoke-direct/range {v2 .. v14}, Lcom/facebook/platform/opengraph/server/PublishOpenGraphActionMethod$Params;-><init>(Ljava/lang/String;LX/0m9;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 573021
    iget-object v3, p0, Lcom/facebook/messaging/platform/utilities/OpenGraphMessageBatchOperation;->d:LX/2Qy;

    invoke-static {v3, v2}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v2

    const-string v3, "og_action"

    invoke-virtual {v2, v3}, LX/2Vk;->a(Ljava/lang/String;)LX/2Vk;

    move-result-object v2

    invoke-virtual {v2}, LX/2Vk;->a()LX/2Vj;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, LX/2VK;->a(LX/2Vj;)V

    .line 573022
    return-void
.end method


# virtual methods
.method public final a(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 13

    .prologue
    .line 573023
    iget-object v0, p0, LX/2QZ;->a:Ljava/lang/String;

    .line 573024
    iget-object v1, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v1, v1

    .line 573025
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 573026
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 573027
    const-string v1, "platform_open_graph_share_upload_params"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/platform/utilities/OpenGraphMessageBatchOperation$Params;

    .line 573028
    iget-object v2, v0, Lcom/facebook/messaging/platform/utilities/OpenGraphMessageBatchOperation$Params;->a:Lcom/facebook/share/model/ShareItem;

    move-object v2, v2

    .line 573029
    iget-object v3, p0, Lcom/facebook/messaging/platform/utilities/OpenGraphMessageBatchOperation;->c:LX/2Qw;

    iget-object v4, v2, Lcom/facebook/share/model/ShareItem;->j:Lcom/facebook/share/model/OpenGraphShareItemData;

    iget-object v4, v4, Lcom/facebook/share/model/OpenGraphShareItemData;->a:Ljava/lang/String;

    iget-object v5, v2, Lcom/facebook/share/model/ShareItem;->j:Lcom/facebook/share/model/OpenGraphShareItemData;

    iget-object v5, v5, Lcom/facebook/share/model/OpenGraphShareItemData;->b:Ljava/lang/String;

    iget-object v6, v2, Lcom/facebook/share/model/ShareItem;->j:Lcom/facebook/share/model/OpenGraphShareItemData;

    iget-object v6, v6, Lcom/facebook/share/model/OpenGraphShareItemData;->c:Ljava/lang/String;

    .line 573030
    new-instance v7, LX/8PF;

    iget-object v8, v3, LX/2Qw;->a:LX/2Qx;

    iget-object v9, v3, LX/2Qw;->b:Landroid/content/Context;

    iget-object v10, v3, LX/2Qw;->c:LX/2N8;

    invoke-virtual {v10, v4}, LX/2N8;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v10

    check-cast v10, LX/0m9;

    move-object v11, v5

    move-object v12, v6

    invoke-direct/range {v7 .. v12}, LX/8PF;-><init>(LX/2Qx;Landroid/content/Context;LX/0m9;Ljava/lang/String;Ljava/lang/String;)V

    move-object v3, v7

    .line 573031
    invoke-virtual {v3}, LX/8PF;->h()V

    .line 573032
    iget-object v2, v2, Lcom/facebook/share/model/ShareItem;->i:Lcom/facebook/share/model/ComposerAppAttribution;

    .line 573033
    iget-object v4, v0, Lcom/facebook/messaging/platform/utilities/OpenGraphMessageBatchOperation$Params;->b:Landroid/os/Bundle;

    move-object v4, v4

    .line 573034
    iget-object v5, v0, Lcom/facebook/messaging/platform/utilities/OpenGraphMessageBatchOperation$Params;->c:Ljava/lang/String;

    move-object v5, v5

    .line 573035
    iget-object v6, p0, Lcom/facebook/messaging/platform/utilities/OpenGraphMessageBatchOperation;->b:LX/18V;

    invoke-virtual {v6}, LX/18V;->a()LX/2VK;

    move-result-object v6

    .line 573036
    invoke-static {p0, v6, v3, v2, v4}, Lcom/facebook/messaging/platform/utilities/OpenGraphMessageBatchOperation;->a(Lcom/facebook/messaging/platform/utilities/OpenGraphMessageBatchOperation;LX/2VK;LX/8PF;Lcom/facebook/share/model/ComposerAppAttribution;Landroid/os/Bundle;)V

    .line 573037
    new-instance v7, Lcom/facebook/platform/server/protocol/GetAppNameMethod$Params;

    invoke-virtual {v2}, Lcom/facebook/share/model/ComposerAppAttribution;->a()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v7, v4}, Lcom/facebook/platform/server/protocol/GetAppNameMethod$Params;-><init>(Ljava/lang/String;)V

    .line 573038
    iget-object v4, p0, Lcom/facebook/messaging/platform/utilities/OpenGraphMessageBatchOperation;->e:LX/2Qz;

    invoke-static {v4, v7}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v7

    const-string v4, "get_app_name"

    .line 573039
    iput-object v4, v7, LX/2Vk;->c:Ljava/lang/String;

    .line 573040
    move-object v7, v7

    .line 573041
    invoke-virtual {v7}, LX/2Vk;->a()LX/2Vj;

    move-result-object v7

    invoke-interface {v6, v7}, LX/2VK;->a(LX/2Vj;)V

    .line 573042
    new-instance v7, Lcom/facebook/platform/opengraph/server/GetRobotextPreviewMethod$Params;

    invoke-virtual {v3}, LX/8PF;->a()LX/0m9;

    move-result-object v8

    invoke-virtual {v8}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v8

    .line 573043
    iget-object v9, v3, LX/8PF;->d:Ljava/lang/String;

    move-object v9, v9

    .line 573044
    invoke-virtual {v2}, Lcom/facebook/share/model/ComposerAppAttribution;->a()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2}, Lcom/facebook/share/model/ComposerAppAttribution;->c()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v7, v8, v9, v10, v4}, Lcom/facebook/platform/opengraph/server/GetRobotextPreviewMethod$Params;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 573045
    iget-object v8, p0, Lcom/facebook/messaging/platform/utilities/OpenGraphMessageBatchOperation;->f:LX/2R0;

    invoke-static {v8, v7}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v7

    const-string v8, "get_robotext_preview"

    .line 573046
    iput-object v8, v7, LX/2Vk;->c:Ljava/lang/String;

    .line 573047
    move-object v7, v7

    .line 573048
    invoke-virtual {v7}, LX/2Vk;->a()LX/2Vj;

    move-result-object v7

    invoke-interface {v6, v7}, LX/2VK;->a(LX/2Vj;)V

    .line 573049
    if-eqz v5, :cond_0

    .line 573050
    const/4 v7, 0x0

    .line 573051
    const-string v8, "http"

    invoke-virtual {v5, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    move v8, v8

    .line 573052
    if-eqz v8, :cond_3

    .line 573053
    :goto_0
    new-instance v8, LX/BON;

    invoke-direct {v8}, LX/BON;-><init>()V

    .line 573054
    iput-object v7, v8, LX/BON;->a:Ljava/lang/String;

    .line 573055
    move-object v7, v8

    .line 573056
    iput-object v5, v7, LX/BON;->b:Ljava/lang/String;

    .line 573057
    move-object v7, v7

    .line 573058
    invoke-virtual {v7}, LX/BON;->a()Lcom/facebook/share/protocol/LinksPreviewParams;

    move-result-object v8

    .line 573059
    iget-object v7, p0, Lcom/facebook/messaging/platform/utilities/OpenGraphMessageBatchOperation;->g:LX/0Or;

    invoke-interface {v7}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/0e6;

    invoke-static {v7, v8}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v7

    const-string v8, "get_open_graph_url"

    .line 573060
    iput-object v8, v7, LX/2Vk;->c:Ljava/lang/String;

    .line 573061
    move-object v7, v7

    .line 573062
    invoke-virtual {v7}, LX/2Vk;->a()LX/2Vj;

    move-result-object v7

    invoke-interface {v6, v7}, LX/2VK;->a(LX/2Vj;)V

    .line 573063
    :cond_0
    move-object v4, v6

    .line 573064
    const-string v2, "openGraphShareUpload"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v3

    invoke-interface {v4, v2, v3}, LX/2VK;->a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 573065
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 573066
    const-string v2, "og_action"

    invoke-interface {v4, v2}, LX/2VK;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 573067
    const-string v3, "og_post_id"

    invoke-virtual {v5, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 573068
    const-string v2, "get_app_name"

    invoke-interface {v4, v2}, LX/2VK;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 573069
    const-string v3, "app_name"

    invoke-virtual {v5, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 573070
    const-string v2, "get_robotext_preview"

    invoke-interface {v4, v2}, LX/2VK;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/platform/opengraph/model/OpenGraphActionRobotext;

    .line 573071
    const-string v3, "robotext_preview_result"

    invoke-virtual {v5, v3, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 573072
    const/4 v3, 0x0

    .line 573073
    const-string v2, "get_open_graph_url"

    invoke-interface {v4, v2}, LX/2VK;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/share/model/LinksPreview;

    .line 573074
    if-eqz v2, :cond_2

    .line 573075
    new-instance v3, Lcom/facebook/platform/opengraph/model/OpenGraphObject;

    iget-object v4, v2, Lcom/facebook/share/model/LinksPreview;->name:Ljava/lang/String;

    iget-object v6, v2, Lcom/facebook/share/model/LinksPreview;->description:Ljava/lang/String;

    .line 573076
    invoke-virtual {v2}, Lcom/facebook/share/model/LinksPreview;->a()Lcom/facebook/share/model/LinksPreview$Media;

    move-result-object v7

    .line 573077
    if-eqz v7, :cond_4

    iget-object v7, v7, Lcom/facebook/share/model/LinksPreview$Media;->src:Ljava/lang/String;

    :goto_1
    move-object v2, v7

    .line 573078
    invoke-direct {v3, v4, v6, v2}, Lcom/facebook/platform/opengraph/model/OpenGraphObject;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v3

    .line 573079
    :goto_2
    if-eqz v2, :cond_1

    .line 573080
    const-string v3, "object_details"

    invoke-virtual {v5, v3, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 573081
    :cond_1
    invoke-static {v5}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v2

    move-object v0, v2

    .line 573082
    return-object v0

    :cond_2
    move-object v2, v3

    goto :goto_2

    :cond_3
    move-object v9, v7

    move-object v7, v5

    move-object v5, v9

    .line 573083
    goto/16 :goto_0

    :cond_4
    const/4 v7, 0x0

    goto :goto_1
.end method
