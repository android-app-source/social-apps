.class public Lcom/facebook/messaging/platform/utilities/LinkShareMessageBatchOperation;
.super LX/2QZ;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field public final b:LX/18V;

.field public final c:LX/0lC;

.field public final d:LX/2Qv;


# direct methods
.method public constructor <init>(LX/18V;LX/0lC;LX/2Qv;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 573007
    const-string v0, "platform_link_share_upload"

    invoke-direct {p0, v0}, LX/2QZ;-><init>(Ljava/lang/String;)V

    .line 573008
    iput-object p1, p0, Lcom/facebook/messaging/platform/utilities/LinkShareMessageBatchOperation;->b:LX/18V;

    .line 573009
    iput-object p2, p0, Lcom/facebook/messaging/platform/utilities/LinkShareMessageBatchOperation;->c:LX/0lC;

    .line 573010
    iput-object p3, p0, Lcom/facebook/messaging/platform/utilities/LinkShareMessageBatchOperation;->d:LX/2Qv;

    .line 573011
    return-void
.end method


# virtual methods
.method public final a(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 6

    .prologue
    .line 572960
    iget-object v0, p0, LX/2QZ;->a:Ljava/lang/String;

    .line 572961
    iget-object v1, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v1, v1

    .line 572962
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 572963
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 572964
    const-string v1, "platform_link_share_upload_params"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/platform/utilities/LinkShareMessageBatchOperation$Params;

    .line 572965
    iget-object v1, v0, Lcom/facebook/messaging/platform/utilities/LinkShareMessageBatchOperation$Params;->a:Lcom/facebook/share/model/ShareItem;

    move-object v1, v1

    .line 572966
    iget-object v2, p0, Lcom/facebook/messaging/platform/utilities/LinkShareMessageBatchOperation;->b:LX/18V;

    invoke-virtual {v2}, LX/18V;->a()LX/2VK;

    move-result-object v2

    .line 572967
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v3

    .line 572968
    const-string v4, "third_party_id"

    iget-object v5, v1, Lcom/facebook/share/model/ShareItem;->i:Lcom/facebook/share/model/ComposerAppAttribution;

    invoke-virtual {v5}, Lcom/facebook/share/model/ComposerAppAttribution;->a()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 572969
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v4

    .line 572970
    const-string v5, "version"

    const-string p1, "1"

    invoke-interface {v4, v5, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 572971
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v5

    .line 572972
    const-string p1, "type"

    const-string v0, "link"

    invoke-interface {v5, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 572973
    iget-object p1, v1, Lcom/facebook/share/model/ShareItem;->a:Ljava/lang/String;

    if-eqz p1, :cond_0

    .line 572974
    const-string p1, "name"

    iget-object v0, v1, Lcom/facebook/share/model/ShareItem;->a:Ljava/lang/String;

    invoke-interface {v5, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 572975
    :cond_0
    iget-object p1, v1, Lcom/facebook/share/model/ShareItem;->c:Ljava/lang/String;

    if-eqz p1, :cond_1

    .line 572976
    const-string p1, "description"

    iget-object v0, v1, Lcom/facebook/share/model/ShareItem;->c:Ljava/lang/String;

    invoke-interface {v5, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 572977
    :cond_1
    iget-object p1, v1, Lcom/facebook/share/model/ShareItem;->b:Ljava/lang/String;

    if-eqz p1, :cond_2

    .line 572978
    const-string p1, "caption"

    iget-object v0, v1, Lcom/facebook/share/model/ShareItem;->b:Ljava/lang/String;

    invoke-interface {v5, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 572979
    :cond_2
    iget-object p1, v1, Lcom/facebook/share/model/ShareItem;->d:Ljava/lang/String;

    if-eqz p1, :cond_3

    .line 572980
    const-string p1, "image"

    iget-object v0, v1, Lcom/facebook/share/model/ShareItem;->d:Ljava/lang/String;

    invoke-interface {v5, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 572981
    :cond_3
    new-instance p1, LX/4cv;

    const-string v0, "message_preview"

    invoke-direct {p1, v0, v3, v4, v5}, LX/4cv;-><init>(Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)V

    .line 572982
    move-object v3, p1

    .line 572983
    invoke-interface {v2, v3}, LX/2VK;->a(LX/4cv;)LX/2VK;

    .line 572984
    new-instance v3, LX/BON;

    invoke-direct {v3}, LX/BON;-><init>()V

    iget-object v4, v1, Lcom/facebook/share/model/ShareItem;->e:Ljava/lang/String;

    .line 572985
    iput-object v4, v3, LX/BON;->b:Ljava/lang/String;

    .line 572986
    move-object v3, v3

    .line 572987
    invoke-virtual {v3}, LX/BON;->a()Lcom/facebook/share/protocol/LinksPreviewParams;

    move-result-object v3

    .line 572988
    iget-object v4, p0, Lcom/facebook/messaging/platform/utilities/LinkShareMessageBatchOperation;->d:LX/2Qv;

    invoke-static {v4, v3}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v3

    const-string v4, "preview"

    .line 572989
    iput-object v4, v3, LX/2Vk;->c:Ljava/lang/String;

    .line 572990
    move-object v3, v3

    .line 572991
    invoke-virtual {v3}, LX/2Vk;->a()LX/2Vj;

    move-result-object v3

    invoke-interface {v2, v3}, LX/2VK;->a(LX/2Vj;)V

    .line 572992
    move-object v2, v2

    .line 572993
    const-string v1, "messagePreview"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v3

    invoke-interface {v2, v1, v3}, LX/2VK;->a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 572994
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 572995
    const-string v1, "preview"

    invoke-interface {v2, v1}, LX/2VK;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/share/model/LinksPreview;

    .line 572996
    invoke-interface {v2}, LX/2VK;->e()LX/4cw;

    move-result-object v2

    .line 572997
    if-eqz v2, :cond_4

    .line 572998
    iget-object v4, v2, LX/4cw;->a:LX/0lF;

    if-eqz v4, :cond_5

    const/4 v4, 0x1

    :goto_0
    move v4, v4

    .line 572999
    if-eqz v4, :cond_4

    .line 573000
    iget-object v1, v2, LX/4cw;->c:LX/0lF;

    move-object v1, v1

    .line 573001
    invoke-virtual {v1}, LX/0lF;->c()LX/15w;

    move-result-object v1

    .line 573002
    iget-object v2, p0, Lcom/facebook/messaging/platform/utilities/LinkShareMessageBatchOperation;->c:LX/0lC;

    invoke-virtual {v1, v2}, LX/15w;->a(LX/0lD;)V

    .line 573003
    const-class v2, Lcom/facebook/share/model/LinksPreview;

    invoke-virtual {v1, v2}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/share/model/LinksPreview;

    .line 573004
    :cond_4
    const-string v2, "links_preview_result"

    invoke-virtual {v3, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 573005
    invoke-static {v3}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v1

    move-object v0, v1

    .line 573006
    return-object v0

    :cond_5
    const/4 v4, 0x0

    goto :goto_0
.end method
