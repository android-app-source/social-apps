.class public Lcom/facebook/messaging/payment/sync/PaymentsSyncInitializationHandler;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/2Sw;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:Lcom/facebook/messaging/payment/sync/PaymentsSyncInitializationHandler;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0aG;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/2Sx;


# direct methods
.method public constructor <init>(LX/0Or;LX/0aG;LX/0Or;LX/2Sx;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/payments/p2p/config/IsP2pPaymentsSyncProtocolEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0aG;",
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;",
            "LX/2Sx;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 573634
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 573635
    iput-object p1, p0, Lcom/facebook/messaging/payment/sync/PaymentsSyncInitializationHandler;->a:LX/0Or;

    .line 573636
    iput-object p2, p0, Lcom/facebook/messaging/payment/sync/PaymentsSyncInitializationHandler;->b:LX/0aG;

    .line 573637
    iput-object p4, p0, Lcom/facebook/messaging/payment/sync/PaymentsSyncInitializationHandler;->d:LX/2Sx;

    .line 573638
    iput-object p3, p0, Lcom/facebook/messaging/payment/sync/PaymentsSyncInitializationHandler;->c:LX/0Or;

    .line 573639
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/messaging/payment/sync/PaymentsSyncInitializationHandler;
    .locals 7

    .prologue
    .line 573621
    sget-object v0, Lcom/facebook/messaging/payment/sync/PaymentsSyncInitializationHandler;->e:Lcom/facebook/messaging/payment/sync/PaymentsSyncInitializationHandler;

    if-nez v0, :cond_1

    .line 573622
    const-class v1, Lcom/facebook/messaging/payment/sync/PaymentsSyncInitializationHandler;

    monitor-enter v1

    .line 573623
    :try_start_0
    sget-object v0, Lcom/facebook/messaging/payment/sync/PaymentsSyncInitializationHandler;->e:Lcom/facebook/messaging/payment/sync/PaymentsSyncInitializationHandler;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 573624
    if-eqz v2, :cond_0

    .line 573625
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 573626
    new-instance v5, Lcom/facebook/messaging/payment/sync/PaymentsSyncInitializationHandler;

    const/16 v3, 0x1547

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v3

    check-cast v3, LX/0aG;

    const/16 v4, 0x19e

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/2Sx;->a(LX/0QB;)LX/2Sx;

    move-result-object v4

    check-cast v4, LX/2Sx;

    invoke-direct {v5, v6, v3, p0, v4}, Lcom/facebook/messaging/payment/sync/PaymentsSyncInitializationHandler;-><init>(LX/0Or;LX/0aG;LX/0Or;LX/2Sx;)V

    .line 573627
    move-object v0, v5

    .line 573628
    sput-object v0, Lcom/facebook/messaging/payment/sync/PaymentsSyncInitializationHandler;->e:Lcom/facebook/messaging/payment/sync/PaymentsSyncInitializationHandler;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 573629
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 573630
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 573631
    :cond_1
    sget-object v0, Lcom/facebook/messaging/payment/sync/PaymentsSyncInitializationHandler;->e:Lcom/facebook/messaging/payment/sync/PaymentsSyncInitializationHandler;

    return-object v0

    .line 573632
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 573633
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/2T6;)V
    .locals 5

    .prologue
    .line 573619
    iget-object v0, p0, Lcom/facebook/messaging/payment/sync/PaymentsSyncInitializationHandler;->b:LX/0aG;

    const-string v1, "ensure_payments_sync"

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v3

    const v4, -0xcb287a6

    invoke-static {v0, v1, v2, v3, v4}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/1MF;->setFireAndForget(Z)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    .line 573620
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 573618
    iget-object v0, p0, Lcom/facebook/messaging/payment/sync/PaymentsSyncInitializationHandler;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public final b()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/0Tn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 573616
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 573617
    return-object v0
.end method

.method public final b(LX/2T6;)V
    .locals 3

    .prologue
    .line 573607
    iget-object v1, p0, Lcom/facebook/messaging/payment/sync/PaymentsSyncInitializationHandler;->d:LX/2Sx;

    iget-object v0, p0, Lcom/facebook/messaging/payment/sync/PaymentsSyncInitializationHandler;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 573608
    iget-object v2, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v0, v2

    .line 573609
    sget-object v2, LX/7GT;->PAYMENTS_QUEUE_TYPE:LX/7GT;

    move-object v2, v2

    .line 573610
    invoke-static {v0, v2}, LX/7G9;->a(Ljava/lang/String;LX/7GT;)LX/7G9;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/2Sx;->a(LX/7G9;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 573611
    :goto_0
    return-void

    .line 573612
    :cond_0
    invoke-virtual {p0, p1}, Lcom/facebook/messaging/payment/sync/PaymentsSyncInitializationHandler;->a(LX/2T6;)V

    goto :goto_0
.end method

.method public final c()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 573615
    const/16 v0, 0x5a6

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 573614
    const/4 v0, 0x0

    return-object v0
.end method

.method public final e()V
    .locals 0

    .prologue
    .line 573613
    return-void
.end method
