.class public Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static volatile r:Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;


# instance fields
.field public final b:Landroid/content/Context;

.field public final c:Landroid/content/res/Resources;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2Og;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/3Rb;

.field private final f:LX/0aG;

.field private final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2Ou;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/FJv;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/2OS;

.field public final j:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/1FZ;

.field private l:LX/1HI;

.field public final m:LX/2Mq;

.field private final n:Ljava/util/Random;

.field private final o:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/3Ky;",
            ">;"
        }
    .end annotation
.end field

.field private final p:LX/3GL;

.field private final q:LX/3Rd;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 580748
    const-class v0, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;

    const-string v1, "notifications"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0Or;LX/3Rb;LX/1HI;LX/0aG;LX/0Or;LX/0Or;LX/2OS;LX/0Or;LX/2Mq;LX/1FZ;LX/0Or;LX/3GL;LX/3Rd;)V
    .locals 1
    .param p9    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/annotations/IsBadgeTrayNotificationsEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Or",
            "<",
            "LX/2Og;",
            ">;",
            "LX/3Rb;",
            "LX/1HI;",
            "LX/0aG;",
            "LX/0Or",
            "<",
            "LX/2Ou;",
            ">;",
            "LX/0Or",
            "<",
            "LX/FJv;",
            ">;",
            "LX/2OS;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/2Mq;",
            "LX/1FZ;",
            "LX/0Or",
            "<",
            "LX/3Ky;",
            ">;",
            "LX/3GL;",
            "LX/3Rd;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 580749
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 580750
    iput-object p1, p0, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->b:Landroid/content/Context;

    .line 580751
    iput-object p5, p0, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->f:LX/0aG;

    .line 580752
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->c:Landroid/content/res/Resources;

    .line 580753
    iput-object p2, p0, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->d:LX/0Or;

    .line 580754
    iput-object p3, p0, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->e:LX/3Rb;

    .line 580755
    iput-object p4, p0, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->l:LX/1HI;

    .line 580756
    iput-object p6, p0, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->g:LX/0Or;

    .line 580757
    iput-object p7, p0, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->h:LX/0Or;

    .line 580758
    iput-object p8, p0, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->i:LX/2OS;

    .line 580759
    iput-object p9, p0, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->j:LX/0Or;

    .line 580760
    iput-object p11, p0, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->k:LX/1FZ;

    .line 580761
    iput-object p10, p0, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->m:LX/2Mq;

    .line 580762
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->n:Ljava/util/Random;

    .line 580763
    iput-object p12, p0, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->o:LX/0Or;

    .line 580764
    iput-object p13, p0, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->p:LX/3GL;

    .line 580765
    iput-object p14, p0, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->q:LX/3Rd;

    .line 580766
    return-void
.end method

.method private a()I
    .locals 2

    .prologue
    .line 580767
    iget-object v0, p0, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->c:Landroid/content/res/Resources;

    const v1, 0x1050005

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method public static a(Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;LX/1bf;IIZLX/33B;LX/8Vc;)LX/1ca;
    .locals 6
    .param p4    # Z
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # LX/33B;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1bf;",
            "IIZ",
            "LX/33B;",
            "LX/8Vc;",
            ")",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 580768
    if-nez p1, :cond_0

    move-object v0, v2

    .line 580769
    :goto_0
    return-object v0

    .line 580770
    :cond_0
    invoke-static {p1}, LX/1bX;->a(LX/1bf;)LX/1bX;

    move-result-object v3

    .line 580771
    if-nez p5, :cond_2

    .line 580772
    invoke-static {p3, p2}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 580773
    if-eqz p6, :cond_5

    .line 580774
    invoke-interface {p6}, LX/8Vc;->b()LX/8ue;

    move-result-object v0

    sget-object v5, LX/8ue;->SMS:LX/8ue;

    if-ne v0, v5, :cond_4

    .line 580775
    const/4 v0, -0x1

    .line 580776
    :goto_1
    invoke-interface {p6}, LX/8Vc;->b()LX/8ue;

    move-result-object v5

    invoke-static {v5, p1}, LX/3Rd;->a(LX/8ue;LX/1bf;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 580777
    iget-object v1, p0, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->q:LX/3Rd;

    invoke-virtual {v1, p6}, LX/3Rd;->a(LX/8Vc;)I

    move-result v1

    .line 580778
    :cond_1
    :goto_2
    new-instance v5, LX/FJi;

    invoke-direct {v5, v4, v0, v1}, LX/FJi;-><init>(III)V

    .line 580779
    iput-object v5, v3, LX/1bX;->j:LX/33B;

    .line 580780
    :goto_3
    invoke-virtual {v3}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    .line 580781
    if-eqz p4, :cond_3

    .line 580782
    iget-object v1, p0, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->l:LX/1HI;

    sget-object v3, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v0, v3}, LX/1HI;->e(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-object v0, v2

    .line 580783
    goto :goto_0

    .line 580784
    :cond_2
    iput-object p5, v3, LX/1bX;->j:LX/33B;

    .line 580785
    goto :goto_3

    .line 580786
    :cond_3
    iget-object v1, p0, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->l:LX/1HI;

    sget-object v2, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v0, v2}, LX/1HI;->b(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v0

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1

    :cond_5
    move v0, v1

    goto :goto_2
.end method

.method public static a(Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;Lcom/facebook/messaging/model/messages/ParticipantInfo;ZLX/FJf;)LX/1ca;
    .locals 7
    .param p0    # Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Z
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/messages/ParticipantInfo;",
            "Z",
            "LX/FJf;",
            ")",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 580787
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/messaging/model/messages/ParticipantInfo;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 580788
    :cond_0
    :goto_0
    return-object v5

    .line 580789
    :cond_1
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    .line 580790
    invoke-direct {p0}, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->b()I

    move-result v3

    .line 580791
    invoke-direct {p0}, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->a()I

    move-result v2

    .line 580792
    iget-object v1, p0, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->e:LX/3Rb;

    invoke-static {v0}, LX/8t9;->a(Lcom/facebook/user/model/UserKey;)LX/8t9;

    move-result-object v0

    invoke-virtual {v1, v0, v2, v3}, LX/3Rb;->a(LX/8t9;II)LX/1bf;

    move-result-object v1

    move-object v0, p0

    move v4, p2

    move-object v6, v5

    .line 580793
    invoke-static/range {v0 .. v6}, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->a(Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;LX/1bf;IIZLX/33B;LX/8Vc;)LX/1ca;

    move-result-object v5

    .line 580794
    if-eqz p3, :cond_2

    if-eqz v5, :cond_2

    .line 580795
    invoke-static {p0, p3}, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->a(Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;LX/FJf;)LX/1ci;

    move-result-object v0

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v1

    invoke-interface {v5, v0, v1}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V

    goto :goto_0

    .line 580796
    :cond_2
    if-eqz p3, :cond_0

    .line 580797
    invoke-interface {p3}, LX/FJf;->a()V

    goto :goto_0
.end method

.method private static a(Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;LX/FJf;)LX/1ci;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/FJf;",
            ")",
            "LX/1ci",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 580798
    new-instance v0, LX/FJd;

    invoke-direct {v0, p0, p1}, LX/FJd;-><init>(Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;LX/FJf;)V

    return-object v0
.end method

.method public static a(I)Landroid/graphics/Rect;
    .locals 5

    .prologue
    const/high16 v4, 0x3f000000    # 0.5f

    .line 580799
    new-instance v0, Landroid/graphics/Rect;

    int-to-float v1, p0

    mul-float/2addr v1, v4

    float-to-int v1, v1

    sub-int v1, p0, v1

    div-int/lit8 v1, v1, 0x2

    const/4 v2, 0x0

    int-to-float v3, p0

    mul-float/2addr v3, v4

    float-to-int v3, v3

    add-int/2addr v3, p0

    div-int/lit8 v3, v3, 0x2

    invoke-direct {v0, v1, v2, v3, p0}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;
    .locals 3

    .prologue
    .line 580800
    sget-object v0, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->r:Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;

    if-nez v0, :cond_1

    .line 580801
    const-class v1, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;

    monitor-enter v1

    .line 580802
    :try_start_0
    sget-object v0, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->r:Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 580803
    if-eqz v2, :cond_0

    .line 580804
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->b(LX/0QB;)Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->r:Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 580805
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 580806
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 580807
    :cond_1
    sget-object v0, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->r:Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;

    return-object v0

    .line 580808
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 580809
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(IILX/FJf;Landroid/graphics/Bitmap;LX/8Vc;)V
    .locals 7
    .param p4    # Landroid/graphics/Bitmap;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    .line 580579
    const/4 v5, 0x0

    .line 580580
    if-eqz p4, :cond_0

    .line 580581
    new-instance v5, LX/FJb;

    invoke-direct {v5, p0, p4, p2}, LX/FJb;-><init>(Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;Landroid/graphics/Bitmap;I)V

    .line 580582
    :cond_0
    invoke-interface {p5}, LX/8Vc;->a()I

    move-result v0

    if-lez v0, :cond_1

    .line 580583
    invoke-interface {p5, v4, p2, p1}, LX/8Vc;->a(III)LX/1bf;

    move-result-object v1

    :goto_0
    move-object v0, p0

    move v2, p2

    move v3, p1

    move-object v6, p5

    .line 580584
    invoke-static/range {v0 .. v6}, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->a(Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;LX/1bf;IIZLX/33B;LX/8Vc;)LX/1ca;

    move-result-object v0

    .line 580585
    if-eqz v0, :cond_2

    .line 580586
    invoke-static {p0, p3}, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->a(Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;LX/FJf;)LX/1ci;

    move-result-object v1

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V

    .line 580587
    :goto_1
    return-void

    .line 580588
    :cond_1
    invoke-interface {p5, v4, p2, p1}, LX/8Vc;->b(III)LX/1bf;

    move-result-object v1

    goto :goto_0

    .line 580589
    :cond_2
    invoke-interface {p3}, LX/FJf;->a()V

    goto :goto_1
.end method

.method private a(LX/FJf;IILX/8Vc;)V
    .locals 9

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 580810
    invoke-interface {p4}, LX/8Vc;->a()I

    move-result v0

    new-array v8, v0, [LX/1ca;

    move v7, v4

    .line 580811
    :goto_0
    invoke-interface {p4}, LX/8Vc;->a()I

    move-result v0

    if-ge v7, v0, :cond_1

    .line 580812
    invoke-interface {p4, v7, p3, p2}, LX/8Vc;->a(III)LX/1bf;

    move-result-object v1

    move-object v0, p0

    move v2, p3

    move v3, p2

    move-object v6, v5

    .line 580813
    invoke-static/range {v0 .. v6}, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->a(Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;LX/1bf;IIZLX/33B;LX/8Vc;)LX/1ca;

    move-result-object v0

    .line 580814
    if-eqz v0, :cond_0

    .line 580815
    aput-object v0, v8, v7

    .line 580816
    :cond_0
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_0

    .line 580817
    :cond_1
    invoke-static {v8}, LX/4e2;->a([LX/1ca;)LX/4e2;

    move-result-object v0

    .line 580818
    new-instance v1, LX/FJc;

    invoke-direct {v1, p0, p1, p3}, LX/FJc;-><init>(Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;LX/FJf;I)V

    move-object v1, v1

    .line 580819
    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/1cZ;->a(LX/1cj;Ljava/util/concurrent/Executor;)V

    .line 580820
    return-void
.end method

.method public static a(Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;IILX/8Vc;)V
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 580821
    const/4 v0, 0x0

    move v7, v0

    :goto_0
    invoke-interface {p3}, LX/8Vc;->a()I

    move-result v0

    if-ge v7, v0, :cond_0

    .line 580822
    invoke-interface {p3, v7, p2, p1}, LX/8Vc;->a(III)LX/1bf;

    move-result-object v1

    .line 580823
    const/4 v4, 0x1

    move-object v0, p0

    move v2, p2

    move v3, p1

    move-object v6, v5

    invoke-static/range {v0 .. v6}, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->a(Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;LX/1bf;IIZLX/33B;LX/8Vc;)LX/1ca;

    .line 580824
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_0

    .line 580825
    :cond_0
    return-void
.end method

.method public static a$redex0(Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;Ljava/util/List;I)LX/1FJ;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;I)",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 580716
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 580717
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FJ;

    .line 580718
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v5

    instance-of v5, v5, LX/1lm;

    if-eqz v5, :cond_0

    .line 580719
    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1lm;

    invoke-virtual {v0}, LX/1lm;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 580720
    :cond_1
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v1, :cond_2

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    const/4 v4, 0x4

    if-ge v0, v4, :cond_2

    move v0, v1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 580721
    iget-object v0, p0, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->k:LX/1FZ;

    invoke-virtual {v0, p2, p2}, LX/1FZ;->a(II)LX/1FJ;

    move-result-object v0

    .line 580722
    if-nez v0, :cond_3

    .line 580723
    const/4 v0, 0x0

    .line 580724
    :goto_2
    return-object v0

    :cond_2
    move v0, v2

    .line 580725
    goto :goto_1

    .line 580726
    :cond_3
    new-instance v1, LX/1ll;

    sget-object v4, LX/1lk;->a:LX/1lk;

    invoke-direct {v1, v0, v4, v2}, LX/1ll;-><init>(LX/1FJ;LX/1lk;I)V

    .line 580727
    invoke-static {v1}, LX/1FJ;->a(Ljava/io/Closeable;)LX/1FJ;

    move-result-object v1

    .line 580728
    new-instance v2, Landroid/graphics/Canvas;

    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-direct {v2, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 580729
    iget-object v0, p0, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->b:Landroid/content/Context;

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-static {v0, v4}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v0

    .line 580730
    sub-int v4, p2, v0

    int-to-float v4, v4

    const/high16 v5, 0x3f000000    # 0.5f

    mul-float/2addr v4, v5

    float-to-int v4, v4

    .line 580731
    const/4 v5, 0x0

    .line 580732
    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6, v5, v5, v4, p2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 580733
    invoke-static {p2}, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->a(I)Landroid/graphics/Rect;

    move-result-object p0

    .line 580734
    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/Bitmap;

    const/4 p1, 0x0

    invoke-virtual {v2, v5, p0, v6, p1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 580735
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_4

    .line 580736
    new-instance v6, Landroid/graphics/Rect;

    add-int v5, v4, v0

    const/4 p0, 0x0

    invoke-direct {v6, v5, p0, p2, p2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 580737
    invoke-static {p2}, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->a(I)Landroid/graphics/Rect;

    move-result-object p0

    .line 580738
    const/4 v5, 0x1

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/Bitmap;

    const/4 p1, 0x0

    invoke-virtual {v2, v5, p0, v6, p1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 580739
    :goto_3
    move-object v0, v1

    .line 580740
    goto :goto_2

    .line 580741
    :cond_4
    const/4 p1, 0x0

    .line 580742
    sub-int v5, p2, v0

    div-int/lit8 v6, v5, 0x2

    .line 580743
    new-instance v7, Landroid/graphics/Rect;

    add-int v5, v4, v0

    const/4 p0, 0x0

    invoke-direct {v7, v5, p0, p2, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 580744
    const/4 v5, 0x1

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/Bitmap;

    invoke-virtual {v2, v5, p1, v7, p1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 580745
    new-instance v7, Landroid/graphics/Rect;

    add-int v5, v4, v0

    add-int/2addr v6, v0

    invoke-direct {v7, v5, v6, p2, p2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 580746
    const/4 v5, 0x2

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/Bitmap;

    invoke-virtual {v2, v5, p1, v7, p1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 580747
    goto :goto_3
.end method

.method private b()I
    .locals 2

    .prologue
    .line 580826
    iget-object v0, p0, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->c:Landroid/content/res/Resources;

    const v1, 0x1050006

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method private static b(LX/0QB;)Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;
    .locals 15

    .prologue
    .line 580714
    new-instance v0, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    const/16 v2, 0xce8

    invoke-static {p0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-static {p0}, LX/3Rb;->a(LX/0QB;)LX/3Rb;

    move-result-object v3

    check-cast v3, LX/3Rb;

    invoke-static {p0}, LX/1Fo;->a(LX/0QB;)LX/1HI;

    move-result-object v4

    check-cast v4, LX/1HI;

    invoke-static {p0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v5

    check-cast v5, LX/0aG;

    const/16 v6, 0xcee

    invoke-static {p0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 v7, 0x28e4

    invoke-static {p0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static {p0}, LX/2OS;->a(LX/0QB;)LX/2OS;

    move-result-object v8

    check-cast v8, LX/2OS;

    const/16 v9, 0x14dd

    invoke-static {p0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-static {p0}, LX/2Mq;->a(LX/0QB;)LX/2Mq;

    move-result-object v10

    check-cast v10, LX/2Mq;

    invoke-static {p0}, LX/1Et;->a(LX/0QB;)LX/1FZ;

    move-result-object v11

    check-cast v11, LX/1FZ;

    const/16 v12, 0xde1

    invoke-static {p0, v12}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v12

    invoke-static {p0}, LX/3GL;->b(LX/0QB;)LX/3GL;

    move-result-object v13

    check-cast v13, LX/3GL;

    invoke-static {p0}, LX/3Rd;->b(LX/0QB;)LX/3Rd;

    move-result-object v14

    check-cast v14, LX/3Rd;

    invoke-direct/range {v0 .. v14}, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;-><init>(Landroid/content/Context;LX/0Or;LX/3Rb;LX/1HI;LX/0aG;LX/0Or;LX/0Or;LX/2OS;LX/0Or;LX/2Mq;LX/1FZ;LX/0Or;LX/3GL;LX/3Rd;)V

    .line 580715
    return-object v0
.end method

.method private c(Lcom/facebook/messaging/model/messages/Message;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 580710
    invoke-virtual {p0, p1}, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->b(Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/model/messages/ParticipantInfo;

    move-result-object v1

    .line 580711
    if-eqz v1, :cond_0

    iget-object v0, v1, Lcom/facebook/messaging/model/messages/ParticipantInfo;->c:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 580712
    iget-object v0, p0, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Og;

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0, v2, v1}, LX/2Og;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/model/messages/ParticipantInfo;)Ljava/lang/String;

    move-result-object v0

    .line 580713
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->c:Landroid/content/res/Resources;

    invoke-static {v0}, LX/5MF;->b(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/messages/Message;)LX/1ca;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/messages/Message;",
            ")",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 580702
    iget-object v0, p0, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->i:LX/2OS;

    invoke-virtual {v0, p1}, LX/2OS;->e(Lcom/facebook/messaging/model/messages/Message;)LX/0Px;

    move-result-object v0

    .line 580703
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 580704
    :cond_0
    const/4 v0, 0x0

    .line 580705
    :goto_0
    return-object v0

    .line 580706
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/attachments/ImageAttachmentData;

    .line 580707
    iget-object v0, v0, Lcom/facebook/messaging/attachments/ImageAttachmentData;->a:Lcom/facebook/messaging/attachments/ImageAttachmentUris;

    .line 580708
    iget-object v0, v0, Lcom/facebook/messaging/attachments/ImageAttachmentUris;->a:Landroid/net/Uri;

    invoke-static {v0}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v0

    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    .line 580709
    iget-object v1, p0, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->l:LX/1HI;

    sget-object v2, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v0, v2}, LX/1HI;->b(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/messaging/model/messages/ParticipantInfo;)LX/1ca;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/messages/ParticipantInfo;",
            ")",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 580701
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-static {p0, p1, v0, v1}, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->a(Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;Lcom/facebook/messaging/model/messages/ParticipantInfo;ZLX/FJf;)LX/1ca;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;I)Lcom/facebook/messaging/model/messages/MessagesCollection;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/16 v0, 0x14

    .line 580677
    iget-object v1, p0, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->n:Ljava/util/Random;

    invoke-virtual {v1}, Ljava/util/Random;->nextInt()I

    move-result v1

    .line 580678
    iget-object v2, p0, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->m:LX/2Mq;

    const-string v3, "MessagingNotificationUtil.tryToGetThreadMessagesCollection"

    invoke-virtual {v2, v1, v3}, LX/2Mq;->a(ILjava/lang/String;)V

    .line 580679
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 580680
    new-instance v3, LX/6iM;

    invoke-direct {v3}, LX/6iM;-><init>()V

    invoke-static {p1}, Lcom/facebook/messaging/model/threads/ThreadCriteria;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadCriteria;

    move-result-object v4

    .line 580681
    iput-object v4, v3, LX/6iM;->a:Lcom/facebook/messaging/model/threads/ThreadCriteria;

    .line 580682
    move-object v3, v3

    .line 580683
    sget-object v4, LX/0rS;->DO_NOT_CHECK_SERVER:LX/0rS;

    .line 580684
    iput-object v4, v3, LX/6iM;->b:LX/0rS;

    .line 580685
    move-object v3, v3

    .line 580686
    if-le p2, v0, :cond_0

    .line 580687
    :goto_0
    iput p2, v3, LX/6iM;->g:I

    .line 580688
    move-object v0, v3

    .line 580689
    invoke-virtual {v0}, LX/6iM;->j()Lcom/facebook/messaging/service/model/FetchThreadParams;

    move-result-object v0

    .line 580690
    const-string v3, "fetchThreadParams"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 580691
    const-string v0, "logger_instance_key"

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 580692
    iget-object v0, p0, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->f:LX/0aG;

    const-string v3, "fetch_thread"

    const-class v4, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;

    invoke-static {v4}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    const v5, 0x67ba4466

    invoke-static {v0, v3, v2, v4, v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->startOnMainThread()LX/1ML;

    move-result-object v0

    .line 580693
    new-instance v2, LX/FJe;

    invoke-direct {v2, p0, v1}, LX/FJe;-><init>(Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;I)V

    invoke-static {v0, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 580694
    invoke-static {v0}, LX/1v3;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbservice/service/OperationResult;

    .line 580695
    if-nez v0, :cond_1

    .line 580696
    const/4 v0, 0x0

    .line 580697
    :goto_1
    return-object v0

    :cond_0
    move p2, v0

    .line 580698
    goto :goto_0

    .line 580699
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchThreadResult;

    .line 580700
    iget-object v0, v0, Lcom/facebook/messaging/service/model/FetchThreadResult;->e:Lcom/facebook/messaging/model/messages/MessagesCollection;

    goto :goto_1
.end method

.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 580655
    iget-object v0, p0, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->n:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextInt()I

    move-result v0

    .line 580656
    iget-object v1, p0, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->m:LX/2Mq;

    const-string v2, "MessagingNotificationUtil.tryToGetThreadSummary"

    invoke-virtual {v1, v0, v2}, LX/2Mq;->a(ILjava/lang/String;)V

    .line 580657
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 580658
    new-instance v2, LX/6iM;

    invoke-direct {v2}, LX/6iM;-><init>()V

    invoke-static {p1}, Lcom/facebook/messaging/model/threads/ThreadCriteria;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadCriteria;

    move-result-object v3

    .line 580659
    iput-object v3, v2, LX/6iM;->a:Lcom/facebook/messaging/model/threads/ThreadCriteria;

    .line 580660
    move-object v2, v2

    .line 580661
    sget-object v3, LX/0rS;->DO_NOT_CHECK_SERVER:LX/0rS;

    .line 580662
    iput-object v3, v2, LX/6iM;->b:LX/0rS;

    .line 580663
    move-object v2, v2

    .line 580664
    const/4 v3, 0x0

    .line 580665
    iput v3, v2, LX/6iM;->g:I

    .line 580666
    move-object v2, v2

    .line 580667
    invoke-virtual {v2}, LX/6iM;->j()Lcom/facebook/messaging/service/model/FetchThreadParams;

    move-result-object v2

    .line 580668
    const-string v3, "fetchThreadParams"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 580669
    const-string v2, "logger_instance_key"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 580670
    iget-object v0, p0, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->f:LX/0aG;

    const-string v2, "fetch_thread"

    const-class v3, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;

    invoke-static {v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v3

    const v4, -0x64549fd

    invoke-static {v0, v2, v1, v3, v4}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->startOnMainThread()LX/1ML;

    move-result-object v0

    .line 580671
    invoke-static {v0}, LX/1v3;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbservice/service/OperationResult;

    .line 580672
    if-nez v0, :cond_0

    .line 580673
    const/4 v0, 0x0

    .line 580674
    :goto_0
    return-object v0

    .line 580675
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchThreadResult;

    .line 580676
    iget-object v0, v0, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    goto :goto_0
.end method

.method public final a(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threads/ThreadSummary;)Ljava/lang/String;
    .locals 5
    .param p2    # Lcom/facebook/messaging/model/threads/ThreadSummary;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 580643
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->i(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 580644
    iget-object v0, p0, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->c:Landroid/content/res/Resources;

    const v1, 0x7f0806a8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 580645
    :cond_0
    :goto_0
    return-object v0

    .line 580646
    :cond_1
    invoke-direct {p0, p1}, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->c(Lcom/facebook/messaging/model/messages/Message;)Ljava/lang/String;

    move-result-object v3

    .line 580647
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v0

    if-eqz v0, :cond_4

    if-eqz p2, :cond_4

    iget-object v0, p2, Lcom/facebook/messaging/model/threads/ThreadSummary;->h:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-le v0, v1, :cond_4

    move v0, v1

    .line 580648
    :goto_1
    iget-object v4, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v4}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->c(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v4

    if-nez v4, :cond_2

    if-nez v0, :cond_2

    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    if-nez v0, :cond_3

    :cond_2
    move v2, v1

    .line 580649
    :cond_3
    if-eqz v2, :cond_5

    .line 580650
    invoke-virtual {p0, p2}, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)Ljava/lang/String;

    move-result-object v0

    .line 580651
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 580652
    invoke-direct {p0, p1}, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->c(Lcom/facebook/messaging/model/messages/Message;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_4
    move v0, v2

    .line 580653
    goto :goto_1

    :cond_5
    move-object v0, v3

    .line 580654
    goto :goto_0
.end method

.method public final a(Lcom/facebook/messaging/model/threads/ThreadSummary;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 580629
    const-string v1, ""

    .line 580630
    iget-object v0, p0, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->o:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Ky;

    invoke-virtual {v0, p1}, LX/3Ky;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)LX/FOO;

    move-result-object v0

    .line 580631
    if-nez v0, :cond_0

    .line 580632
    :goto_0
    return-object v1

    .line 580633
    :cond_0
    iget-boolean v2, v0, LX/FON;->a:Z

    move v2, v2

    .line 580634
    if-eqz v2, :cond_1

    .line 580635
    iget-object v1, v0, LX/FON;->b:Ljava/lang/String;

    move-object v0, v1

    .line 580636
    :goto_1
    move-object v1, v0

    .line 580637
    goto :goto_0

    .line 580638
    :cond_1
    iget-object v2, v0, LX/FON;->c:LX/0Px;

    move-object v2, v2

    .line 580639
    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 580640
    iget-object v1, p0, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->p:LX/3GL;

    .line 580641
    iget-object v2, v0, LX/FON;->c:LX/0Px;

    move-object v0, v2

    .line 580642
    invoke-virtual {v1, v0}, LX/3GL;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method

.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/FJf;Lcom/facebook/messaging/model/messages/ParticipantInfo;Landroid/graphics/Bitmap;)V
    .locals 6
    .param p3    # Lcom/facebook/messaging/model/messages/ParticipantInfo;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Landroid/graphics/Bitmap;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x1

    .line 580619
    invoke-static {p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->i(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 580620
    invoke-interface {p2}, LX/FJf;->a()V

    .line 580621
    :goto_0
    return-void

    .line 580622
    :cond_0
    invoke-virtual {p0, p1}, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v1

    .line 580623
    if-nez v1, :cond_1

    .line 580624
    invoke-static {p0, p3, v2, p2}, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->a(Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;Lcom/facebook/messaging/model/messages/ParticipantInfo;ZLX/FJf;)LX/1ca;

    goto :goto_0

    .line 580625
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->h:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FJv;

    invoke-virtual {v0, v1}, LX/FJv;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)LX/8Vc;

    move-result-object v5

    .line 580626
    invoke-interface {v5}, LX/8Vc;->a()I

    move-result v0

    if-gt v0, v2, :cond_2

    .line 580627
    invoke-direct {p0}, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->b()I

    move-result v1

    invoke-direct {p0}, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->a()I

    move-result v2

    move-object v0, p0

    move-object v3, p2

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->a(IILX/FJf;Landroid/graphics/Bitmap;LX/8Vc;)V

    goto :goto_0

    .line 580628
    :cond_2
    invoke-direct {p0}, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->b()I

    move-result v0

    invoke-direct {p0}, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->a()I

    move-result v1

    invoke-direct {p0, p2, v0, v1, v5}, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->a(LX/FJf;IILX/8Vc;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/messaging/notify/NewMessageNotification;LX/FJf;)V
    .locals 6

    .prologue
    .line 580607
    iget-object v0, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v1, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->b:Lcom/facebook/messaging/model/messages/Message;

    invoke-virtual {p0, v1}, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->b(Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/model/messages/ParticipantInfo;

    move-result-object v1

    .line 580608
    const/4 v2, 0x0

    .line 580609
    iget-object v4, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->k:LX/DiB;

    .line 580610
    iget-object v3, p0, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->j:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->b:Lcom/facebook/messaging/model/messages/Message;

    iget-object v3, v3, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v3, v3, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    sget-object v5, LX/5e9;->ONE_TO_ONE:LX/5e9;

    if-ne v3, v5, :cond_3

    sget-object v3, LX/DiB;->IS_MESSENGER_USER:LX/DiB;

    if-eq v4, v3, :cond_0

    sget-object v3, LX/DiB;->IS_NOT_MESSENGER_USER:LX/DiB;

    if-ne v4, v3, :cond_3

    :cond_0
    const/4 v3, 0x1

    :goto_0
    move v3, v3

    .line 580611
    if-eqz v3, :cond_1

    .line 580612
    iget-object v2, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->k:LX/DiB;

    .line 580613
    sget-object v3, LX/DiB;->IS_MESSENGER_USER:LX/DiB;

    if-ne v2, v3, :cond_2

    const v2, 0x7f021284

    .line 580614
    :goto_1
    iget-object v3, p0, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->c:Landroid/content/res/Resources;

    invoke-static {v3, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 580615
    :cond_1
    move-object v2, v2

    .line 580616
    invoke-virtual {p0, v0, p2, v1, v2}, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/FJf;Lcom/facebook/messaging/model/messages/ParticipantInfo;Landroid/graphics/Bitmap;)V

    .line 580617
    return-void

    .line 580618
    :cond_2
    const v2, 0x7f021240

    goto :goto_1

    :cond_3
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public final b(Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/model/messages/ParticipantInfo;
    .locals 3
    .param p1    # Lcom/facebook/messaging/model/messages/Message;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 580590
    iget-object v0, p0, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v0

    .line 580591
    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 580592
    if-nez v0, :cond_1

    .line 580593
    :cond_0
    :goto_0
    move-object v0, v1

    .line 580594
    return-object v0

    .line 580595
    :cond_1
    if-nez v1, :cond_2

    .line 580596
    const/4 v1, 0x0

    goto :goto_0

    .line 580597
    :cond_2
    iget-object v2, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->h:LX/0Px;

    iget-object p0, v1, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    invoke-static {v2, p0}, LX/2Ou;->a(Ljava/util/List;Lcom/facebook/user/model/UserKey;)Lcom/facebook/messaging/model/messages/ParticipantInfo;

    move-result-object v2

    .line 580598
    if-eqz v2, :cond_3

    move-object v1, v2

    .line 580599
    goto :goto_0

    .line 580600
    :cond_3
    iget-object v2, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->i:LX/0Px;

    iget-object p0, v1, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    invoke-static {v2, p0}, LX/2Ou;->a(Ljava/util/List;Lcom/facebook/user/model/UserKey;)Lcom/facebook/messaging/model/messages/ParticipantInfo;

    move-result-object v2

    .line 580601
    if-eqz v2, :cond_4

    move-object v1, v2

    .line 580602
    goto :goto_0

    .line 580603
    :cond_4
    iget-object v2, v1, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    invoke-virtual {v2}, Lcom/facebook/user/model/UserKey;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 580604
    iget-object v2, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->h:LX/0Px;

    iget-object p0, v1, Lcom/facebook/messaging/model/messages/ParticipantInfo;->f:Ljava/lang/String;

    invoke-static {v2, p0}, LX/2Ou;->a(Ljava/util/List;Ljava/lang/String;)Lcom/facebook/messaging/model/messages/ParticipantInfo;

    move-result-object v2

    .line 580605
    if-eqz v2, :cond_0

    move-object v1, v2

    .line 580606
    goto :goto_0
.end method
