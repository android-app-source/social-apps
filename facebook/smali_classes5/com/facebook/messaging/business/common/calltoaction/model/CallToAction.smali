.class public Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:LX/4ge;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final g:Z

.field public final h:Z

.field public final i:D

.field public final j:Z

.field public final k:Lcom/facebook/messaging/business/common/calltoaction/model/CTAUserConfirmation;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final l:Lcom/facebook/messaging/business/common/calltoaction/model/CTAPaymentInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 799922
    new-instance v0, LX/4gd;

    invoke-direct {v0}, LX/4gd;-><init>()V

    sput-object v0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/4gf;)V
    .locals 8

    .prologue
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    .line 799890
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 799891
    iget-object v2, p1, LX/4gf;->a:Ljava/lang/String;

    move-object v2, v2

    .line 799892
    iput-object v2, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->a:Ljava/lang/String;

    .line 799893
    iget-object v2, p1, LX/4gf;->b:Ljava/lang/String;

    move-object v2, v2

    .line 799894
    iput-object v2, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->b:Ljava/lang/String;

    .line 799895
    iget-object v2, p1, LX/4gf;->c:Landroid/net/Uri;

    move-object v2, v2

    .line 799896
    iput-object v2, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->c:Landroid/net/Uri;

    .line 799897
    iget-object v2, p1, LX/4gf;->d:Landroid/net/Uri;

    move-object v2, v2

    .line 799898
    iput-object v2, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->d:Landroid/net/Uri;

    .line 799899
    iget-object v2, p1, LX/4gf;->e:LX/4ge;

    move-object v2, v2

    .line 799900
    iput-object v2, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->e:LX/4ge;

    .line 799901
    iget-object v2, p1, LX/4gf;->f:Ljava/lang/String;

    move-object v2, v2

    .line 799902
    iput-object v2, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->f:Ljava/lang/String;

    .line 799903
    iget-boolean v2, p1, LX/4gf;->g:Z

    move v2, v2

    .line 799904
    iput-boolean v2, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->g:Z

    .line 799905
    iget-boolean v2, p1, LX/4gf;->h:Z

    move v2, v2

    .line 799906
    iput-boolean v2, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->h:Z

    .line 799907
    iget-object v2, p1, LX/4gf;->i:Lcom/facebook/messaging/business/common/calltoaction/model/CTAUserConfirmation;

    move-object v2, v2

    .line 799908
    iput-object v2, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->k:Lcom/facebook/messaging/business/common/calltoaction/model/CTAUserConfirmation;

    .line 799909
    iget-wide v6, p1, LX/4gf;->j:D

    move-wide v2, v6

    .line 799910
    const-wide/high16 v4, 0x3fd0000000000000L    # 0.25

    cmpg-double v2, v2, v4

    if-ltz v2, :cond_0

    .line 799911
    iget-wide v6, p1, LX/4gf;->j:D

    move-wide v2, v6

    .line 799912
    cmpl-double v2, v2, v0

    if-lez v2, :cond_1

    :cond_0
    :goto_0
    iput-wide v0, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->i:D

    .line 799913
    iget-object v0, p1, LX/4gf;->l:Lcom/facebook/messaging/business/common/calltoaction/model/CTAPaymentInfo;

    move-object v0, v0

    .line 799914
    iput-object v0, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->l:Lcom/facebook/messaging/business/common/calltoaction/model/CTAPaymentInfo;

    .line 799915
    iget-object v0, p1, LX/4gf;->m:Ljava/lang/String;

    move-object v0, v0

    .line 799916
    iput-object v0, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->m:Ljava/lang/String;

    .line 799917
    iget-boolean v0, p1, LX/4gf;->k:Z

    move v0, v0

    .line 799918
    iput-boolean v0, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->j:Z

    .line 799919
    return-void

    .line 799920
    :cond_1
    iget-wide v6, p1, LX/4gf;->j:D

    move-wide v0, v6

    .line 799921
    goto :goto_0
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 799870
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 799871
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->a:Ljava/lang/String;

    .line 799872
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->b:Ljava/lang/String;

    .line 799873
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->c:Landroid/net/Uri;

    .line 799874
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->d:Landroid/net/Uri;

    .line 799875
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 799876
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {v0}, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->a(Ljava/lang/String;)LX/4ge;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->e:LX/4ge;

    .line 799877
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->f:Ljava/lang/String;

    .line 799878
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->g:Z

    .line 799879
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->h:Z

    .line 799880
    const-class v0, Lcom/facebook/messaging/business/common/calltoaction/model/CTAUserConfirmation;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/common/calltoaction/model/CTAUserConfirmation;

    iput-object v0, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->k:Lcom/facebook/messaging/business/common/calltoaction/model/CTAUserConfirmation;

    .line 799881
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v4

    iput-wide v4, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->i:D

    .line 799882
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_3

    :goto_3
    iput-boolean v1, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->j:Z

    .line 799883
    const-class v0, Lcom/facebook/messaging/business/common/calltoaction/model/CTAPaymentInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/common/calltoaction/model/CTAPaymentInfo;

    iput-object v0, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->l:Lcom/facebook/messaging/business/common/calltoaction/model/CTAPaymentInfo;

    .line 799884
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->m:Ljava/lang/String;

    .line 799885
    return-void

    .line 799886
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    move v0, v2

    .line 799887
    goto :goto_1

    :cond_2
    move v0, v2

    .line 799888
    goto :goto_2

    :cond_3
    move v1, v2

    .line 799889
    goto :goto_3
.end method

.method public static a(Ljava/lang/String;)LX/4ge;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 799866
    if-nez p0, :cond_0

    .line 799867
    :goto_0
    return-object v0

    .line 799868
    :cond_0
    :try_start_0
    invoke-static {p0}, LX/4ge;->valueOf(Ljava/lang/String;)LX/4ge;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 799869
    :catch_0
    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 799865
    iget-object v0, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->b:Ljava/lang/String;

    invoke-static {v0}, LX/1fg;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 799864
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 799846
    iget-object v0, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 799847
    iget-object v0, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 799848
    iget-object v0, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->c:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 799849
    iget-object v0, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->d:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 799850
    iget-object v0, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->e:LX/4ge;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->e:LX/4ge;

    invoke-virtual {v0}, LX/4ge;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 799851
    iget-object v0, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 799852
    iget-boolean v0, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->g:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 799853
    iget-boolean v0, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->h:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 799854
    iget-object v0, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->k:Lcom/facebook/messaging/business/common/calltoaction/model/CTAUserConfirmation;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 799855
    iget-wide v4, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->i:D

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeDouble(D)V

    .line 799856
    iget-boolean v0, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->j:Z

    if-eqz v0, :cond_3

    :goto_3
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 799857
    iget-object v0, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->l:Lcom/facebook/messaging/business/common/calltoaction/model/CTAPaymentInfo;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 799858
    iget-object v0, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->m:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 799859
    return-void

    .line 799860
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    move v0, v2

    .line 799861
    goto :goto_1

    :cond_2
    move v0, v2

    .line 799862
    goto :goto_2

    :cond_3
    move v1, v2

    .line 799863
    goto :goto_3
.end method
