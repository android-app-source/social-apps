.class public Lcom/facebook/messaging/business/common/calltoaction/model/CTAUserConfirmation;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/business/common/calltoaction/model/CTAUserConfirmation;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 799818
    new-instance v0, LX/4gb;

    invoke-direct {v0}, LX/4gb;-><init>()V

    sput-object v0, Lcom/facebook/messaging/business/common/calltoaction/model/CTAUserConfirmation;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/4gc;)V
    .locals 1

    .prologue
    .line 799808
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 799809
    iget-object v0, p1, LX/4gc;->a:Ljava/lang/String;

    move-object v0, v0

    .line 799810
    iput-object v0, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CTAUserConfirmation;->a:Ljava/lang/String;

    .line 799811
    iget-object v0, p1, LX/4gc;->b:Ljava/lang/String;

    move-object v0, v0

    .line 799812
    iput-object v0, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CTAUserConfirmation;->b:Ljava/lang/String;

    .line 799813
    iget-object v0, p1, LX/4gc;->c:Ljava/lang/String;

    move-object v0, v0

    .line 799814
    iput-object v0, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CTAUserConfirmation;->c:Ljava/lang/String;

    .line 799815
    iget-object v0, p1, LX/4gc;->d:Ljava/lang/String;

    move-object v0, v0

    .line 799816
    iput-object v0, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CTAUserConfirmation;->d:Ljava/lang/String;

    .line 799817
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 799796
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 799797
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CTAUserConfirmation;->a:Ljava/lang/String;

    .line 799798
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CTAUserConfirmation;->b:Ljava/lang/String;

    .line 799799
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CTAUserConfirmation;->c:Ljava/lang/String;

    .line 799800
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CTAUserConfirmation;->d:Ljava/lang/String;

    .line 799801
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 799807
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 799802
    iget-object v0, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CTAUserConfirmation;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 799803
    iget-object v0, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CTAUserConfirmation;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 799804
    iget-object v0, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CTAUserConfirmation;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 799805
    iget-object v0, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CTAUserConfirmation;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 799806
    return-void
.end method
