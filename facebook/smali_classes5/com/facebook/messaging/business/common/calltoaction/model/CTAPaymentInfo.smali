.class public Lcom/facebook/messaging/business/common/calltoaction/model/CTAPaymentInfo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/business/common/calltoaction/model/CTAPaymentInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 799780
    new-instance v0, LX/4gZ;

    invoke-direct {v0}, LX/4gZ;-><init>()V

    sput-object v0, Lcom/facebook/messaging/business/common/calltoaction/model/CTAPaymentInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/4ga;)V
    .locals 1

    .prologue
    .line 799781
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 799782
    iget-object v0, p1, LX/4ga;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CTAPaymentInfo;->a:Ljava/lang/String;

    .line 799783
    iget-object v0, p1, LX/4ga;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CTAPaymentInfo;->b:Ljava/lang/String;

    .line 799784
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 799785
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 799786
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CTAPaymentInfo;->a:Ljava/lang/String;

    .line 799787
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CTAPaymentInfo;->b:Ljava/lang/String;

    .line 799788
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 799789
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 799790
    iget-object v0, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CTAPaymentInfo;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 799791
    iget-object v0, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CTAPaymentInfo;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 799792
    return-void
.end method
