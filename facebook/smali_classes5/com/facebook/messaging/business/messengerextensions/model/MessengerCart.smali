.class public Lcom/facebook/messaging/business/messengerextensions/model/MessengerCart;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/business/messengerextensions/model/MessengerCart;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:I

.field public final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 800065
    new-instance v0, LX/4gh;

    invoke-direct {v0}, LX/4gh;-><init>()V

    sput-object v0, Lcom/facebook/messaging/business/messengerextensions/model/MessengerCart;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/4gi;)V
    .locals 1

    .prologue
    .line 800066
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 800067
    iget v0, p1, LX/4gi;->a:I

    iput v0, p0, Lcom/facebook/messaging/business/messengerextensions/model/MessengerCart;->a:I

    .line 800068
    iget-object v0, p1, LX/4gi;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/business/messengerextensions/model/MessengerCart;->b:Ljava/lang/String;

    .line 800069
    iget-object v0, p1, LX/4gi;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/business/messengerextensions/model/MessengerCart;->c:Ljava/lang/String;

    .line 800070
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 800071
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 800072
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/business/messengerextensions/model/MessengerCart;->a:I

    .line 800073
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/messengerextensions/model/MessengerCart;->b:Ljava/lang/String;

    .line 800074
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/messengerextensions/model/MessengerCart;->c:Ljava/lang/String;

    .line 800075
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 800076
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 800077
    iget v0, p0, Lcom/facebook/messaging/business/messengerextensions/model/MessengerCart;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 800078
    iget-object v0, p0, Lcom/facebook/messaging/business/messengerextensions/model/MessengerCart;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 800079
    iget-object v0, p0, Lcom/facebook/messaging/business/messengerextensions/model/MessengerCart;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 800080
    return-void
.end method
