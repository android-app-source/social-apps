.class public Lcom/facebook/messaging/business/messengerextensions/model/MessengerExtensionProperties;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/business/messengerextensions/model/MessengerExtensionProperties;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Z

.field public final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Lcom/facebook/messaging/business/messengerextensions/model/MessengerCart;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 800087
    new-instance v0, LX/4gj;

    invoke-direct {v0}, LX/4gj;-><init>()V

    sput-object v0, Lcom/facebook/messaging/business/messengerextensions/model/MessengerExtensionProperties;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/4gk;)V
    .locals 1

    .prologue
    .line 800088
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 800089
    iget-boolean v0, p1, LX/4gk;->a:Z

    iput-boolean v0, p0, Lcom/facebook/messaging/business/messengerextensions/model/MessengerExtensionProperties;->a:Z

    .line 800090
    iget-object v0, p1, LX/4gk;->b:LX/0Px;

    iput-object v0, p0, Lcom/facebook/messaging/business/messengerextensions/model/MessengerExtensionProperties;->b:LX/0Px;

    .line 800091
    iget-object v0, p1, LX/4gk;->c:Lcom/facebook/messaging/business/messengerextensions/model/MessengerCart;

    iput-object v0, p0, Lcom/facebook/messaging/business/messengerextensions/model/MessengerExtensionProperties;->c:Lcom/facebook/messaging/business/messengerextensions/model/MessengerCart;

    .line 800092
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 800093
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 800094
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/messaging/business/messengerextensions/model/MessengerExtensionProperties;->a:Z

    .line 800095
    const-class v0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    .line 800096
    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_1
    iput-object v0, p0, Lcom/facebook/messaging/business/messengerextensions/model/MessengerExtensionProperties;->b:LX/0Px;

    .line 800097
    const-class v0, Lcom/facebook/messaging/business/messengerextensions/model/MessengerCart;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/messengerextensions/model/MessengerCart;

    iput-object v0, p0, Lcom/facebook/messaging/business/messengerextensions/model/MessengerExtensionProperties;->c:Lcom/facebook/messaging/business/messengerextensions/model/MessengerCart;

    .line 800098
    return-void

    .line 800099
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 800100
    :cond_1
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto :goto_1
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 800101
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 800102
    iget-boolean v0, p0, Lcom/facebook/messaging/business/messengerextensions/model/MessengerExtensionProperties;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 800103
    iget-object v0, p0, Lcom/facebook/messaging/business/messengerextensions/model/MessengerExtensionProperties;->b:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 800104
    iget-object v0, p0, Lcom/facebook/messaging/business/messengerextensions/model/MessengerExtensionProperties;->c:Lcom/facebook/messaging/business/messengerextensions/model/MessengerCart;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 800105
    return-void

    .line 800106
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
