.class public Lcom/facebook/messaging/inbox2/badge/MessagingInbox2UnitBadgeCalculator;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0aG;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Uh;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2My;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Jni;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/2Q1;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 572899
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 572900
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 572901
    iput-object v0, p0, Lcom/facebook/messaging/inbox2/badge/MessagingInbox2UnitBadgeCalculator;->a:LX/0Ot;

    .line 572902
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 572903
    iput-object v0, p0, Lcom/facebook/messaging/inbox2/badge/MessagingInbox2UnitBadgeCalculator;->b:LX/0Ot;

    .line 572904
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 572905
    iput-object v0, p0, Lcom/facebook/messaging/inbox2/badge/MessagingInbox2UnitBadgeCalculator;->d:LX/0Ot;

    .line 572906
    return-void
.end method

.method public static a(Lcom/facebook/messaging/inbox2/badge/MessagingInbox2UnitBadgeCalculator;LX/JlS;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/JlS;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 572867
    sget-object v0, LX/JlB;->a:[I

    iget-object v1, p1, LX/JlS;->a:Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 572868
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    :goto_0
    return-object v0

    .line 572869
    :pswitch_0
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 572870
    iget-object v2, p1, LX/JlS;->b:Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;

    invoke-virtual {v2}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->k()Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitConfigModel;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p1, LX/JlS;->b:Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;

    invoke-virtual {v2}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->k()Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitConfigModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitConfigModel;->l()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p1, LX/JlS;->b:Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;

    invoke-virtual {v2}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->k()Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitConfigModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel$MessengerInboxUnitConfigModel;->k()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/facebook/messaging/inbox2/badge/MessagingInbox2UnitBadgeCalculator;->f:LX/2Q1;

    .line 572871
    iget-object v3, v2, LX/2Q1;->a:LX/0Uh;

    const/16 p0, 0x126

    const/4 p1, 0x0

    invoke-virtual {v3, p0, p1}, LX/0Uh;->a(IZ)Z

    move-result v3

    move v2, v3

    .line 572872
    if-eqz v2, :cond_0

    move v2, v0

    .line 572873
    :goto_1
    if-eqz v2, :cond_1

    :goto_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    move-object v0, v0

    .line 572874
    goto :goto_0

    .line 572875
    :pswitch_1
    iget-object v2, p0, Lcom/facebook/messaging/inbox2/badge/MessagingInbox2UnitBadgeCalculator;->d:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2My;

    const/4 v4, 0x0

    .line 572876
    invoke-virtual {v2}, LX/2My;->a()Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, v2, LX/2My;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0ad;

    sget-short v5, LX/2N0;->a:S

    invoke-interface {v3, v5, v4}, LX/0ad;->a(SZ)Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x1

    :goto_3
    move v2, v3

    .line 572877
    if-nez v2, :cond_2

    .line 572878
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v2}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 572879
    :goto_4
    move-object v0, v2

    .line 572880
    goto :goto_0

    :cond_0
    move v2, v1

    .line 572881
    goto :goto_1

    :cond_1
    move v0, v1

    .line 572882
    goto :goto_2

    .line 572883
    :cond_2
    invoke-static {}, Lcom/facebook/messaging/service/model/FetchThreadListParams;->newBuilder()LX/6iI;

    move-result-object v5

    sget-object v6, LX/0rS;->DO_NOT_CHECK_SERVER:LX/0rS;

    .line 572884
    iput-object v6, v5, LX/6iI;->a:LX/0rS;

    .line 572885
    move-object v5, v5

    .line 572886
    sget-object v6, LX/6ek;->MONTAGE:LX/6ek;

    .line 572887
    iput-object v6, v5, LX/6iI;->b:LX/6ek;

    .line 572888
    move-object v5, v5

    .line 572889
    const/16 v6, 0x14

    .line 572890
    iput v6, v5, LX/6iI;->f:I

    .line 572891
    move-object v5, v5

    .line 572892
    invoke-virtual {v5}, LX/6iI;->i()Lcom/facebook/messaging/service/model/FetchThreadListParams;

    move-result-object v5

    .line 572893
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 572894
    const-string v6, "fetchThreadListParams"

    invoke-virtual {v7, v6, v5}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 572895
    iget-object v5, p0, Lcom/facebook/messaging/inbox2/badge/MessagingInbox2UnitBadgeCalculator;->a:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0aG;

    const-string v6, "fetch_thread_list"

    sget-object v8, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    const-class v9, Lcom/facebook/messaging/inbox2/badge/MessagingInbox2UnitBadgeCalculator;

    invoke-static {v9}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v9

    const v10, -0x7863e948

    invoke-static/range {v5 .. v10}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v5

    invoke-interface {v5}, LX/1MF;->start()LX/1ML;

    move-result-object v5

    move-object v2, v5

    .line 572896
    new-instance v3, LX/JlA;

    invoke-direct {v3, p0}, LX/JlA;-><init>(Lcom/facebook/messaging/inbox2/badge/MessagingInbox2UnitBadgeCalculator;)V

    .line 572897
    sget-object v4, LX/131;->INSTANCE:LX/131;

    move-object v4, v4

    .line 572898
    invoke-static {v2, v3, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    goto :goto_4

    :cond_3
    move v3, v4

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static b(LX/0QB;)Lcom/facebook/messaging/inbox2/badge/MessagingInbox2UnitBadgeCalculator;
    .locals 8

    .prologue
    .line 572858
    new-instance v0, Lcom/facebook/messaging/inbox2/badge/MessagingInbox2UnitBadgeCalculator;

    invoke-direct {v0}, Lcom/facebook/messaging/inbox2/badge/MessagingInbox2UnitBadgeCalculator;-><init>()V

    .line 572859
    const/16 v1, 0x542

    invoke-static {p0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0xf9a

    invoke-static {p0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    const/16 v4, 0xd6c

    invoke-static {p0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x282f

    invoke-static {p0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    .line 572860
    new-instance v7, LX/2Q1;

    invoke-direct {v7}, LX/2Q1;-><init>()V

    .line 572861
    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v6

    check-cast v6, LX/0Uh;

    .line 572862
    iput-object v6, v7, LX/2Q1;->a:LX/0Uh;

    .line 572863
    move-object v6, v7

    .line 572864
    check-cast v6, LX/2Q1;

    .line 572865
    iput-object v1, v0, Lcom/facebook/messaging/inbox2/badge/MessagingInbox2UnitBadgeCalculator;->a:LX/0Ot;

    iput-object v2, v0, Lcom/facebook/messaging/inbox2/badge/MessagingInbox2UnitBadgeCalculator;->b:LX/0Ot;

    iput-object v3, v0, Lcom/facebook/messaging/inbox2/badge/MessagingInbox2UnitBadgeCalculator;->c:LX/0Uh;

    iput-object v4, v0, Lcom/facebook/messaging/inbox2/badge/MessagingInbox2UnitBadgeCalculator;->d:LX/0Ot;

    iput-object v5, v0, Lcom/facebook/messaging/inbox2/badge/MessagingInbox2UnitBadgeCalculator;->e:LX/0Or;

    iput-object v6, v0, Lcom/facebook/messaging/inbox2/badge/MessagingInbox2UnitBadgeCalculator;->f:LX/2Q1;

    .line 572866
    return-object v0
.end method
