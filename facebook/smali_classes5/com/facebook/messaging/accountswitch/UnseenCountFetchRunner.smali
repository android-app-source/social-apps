.class public Lcom/facebook/messaging/accountswitch/UnseenCountFetchRunner;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile h:Lcom/facebook/messaging/accountswitch/UnseenCountFetchRunner;


# instance fields
.field private final a:LX/0aG;

.field private final b:Ljava/util/concurrent/ExecutorService;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Di5;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/2UW;

.field public volatile f:Z

.field public g:LX/1ML;


# direct methods
.method public constructor <init>(LX/0aG;Ljava/util/concurrent/ExecutorService;LX/0Or;LX/0Or;LX/2UW;)V
    .locals 1
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/annotations/IsUnseenCountFetchingForAccountSwitchingEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0aG;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "LX/Di5;",
            ">;",
            "LX/2UW;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 574541
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 574542
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/messaging/accountswitch/UnseenCountFetchRunner;->f:Z

    .line 574543
    iput-object p1, p0, Lcom/facebook/messaging/accountswitch/UnseenCountFetchRunner;->a:LX/0aG;

    .line 574544
    iput-object p2, p0, Lcom/facebook/messaging/accountswitch/UnseenCountFetchRunner;->b:Ljava/util/concurrent/ExecutorService;

    .line 574545
    iput-object p3, p0, Lcom/facebook/messaging/accountswitch/UnseenCountFetchRunner;->c:LX/0Or;

    .line 574546
    iput-object p4, p0, Lcom/facebook/messaging/accountswitch/UnseenCountFetchRunner;->d:LX/0Or;

    .line 574547
    iput-object p5, p0, Lcom/facebook/messaging/accountswitch/UnseenCountFetchRunner;->e:LX/2UW;

    .line 574548
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/messaging/accountswitch/UnseenCountFetchRunner;
    .locals 9

    .prologue
    .line 574549
    sget-object v0, Lcom/facebook/messaging/accountswitch/UnseenCountFetchRunner;->h:Lcom/facebook/messaging/accountswitch/UnseenCountFetchRunner;

    if-nez v0, :cond_1

    .line 574550
    const-class v1, Lcom/facebook/messaging/accountswitch/UnseenCountFetchRunner;

    monitor-enter v1

    .line 574551
    :try_start_0
    sget-object v0, Lcom/facebook/messaging/accountswitch/UnseenCountFetchRunner;->h:Lcom/facebook/messaging/accountswitch/UnseenCountFetchRunner;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 574552
    if-eqz v2, :cond_0

    .line 574553
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 574554
    new-instance v3, Lcom/facebook/messaging/accountswitch/UnseenCountFetchRunner;

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v4

    check-cast v4, LX/0aG;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/ExecutorService;

    const/16 v6, 0x14f3

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 v7, 0x2847

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static {v0}, LX/2UW;->a(LX/0QB;)LX/2UW;

    move-result-object v8

    check-cast v8, LX/2UW;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/messaging/accountswitch/UnseenCountFetchRunner;-><init>(LX/0aG;Ljava/util/concurrent/ExecutorService;LX/0Or;LX/0Or;LX/2UW;)V

    .line 574555
    move-object v0, v3

    .line 574556
    sput-object v0, Lcom/facebook/messaging/accountswitch/UnseenCountFetchRunner;->h:Lcom/facebook/messaging/accountswitch/UnseenCountFetchRunner;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 574557
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 574558
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 574559
    :cond_1
    sget-object v0, Lcom/facebook/messaging/accountswitch/UnseenCountFetchRunner;->h:Lcom/facebook/messaging/accountswitch/UnseenCountFetchRunner;

    return-object v0

    .line 574560
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 574561
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a$redex0(Lcom/facebook/messaging/accountswitch/UnseenCountFetchRunner;Lcom/facebook/fbservice/service/OperationResult;)V
    .locals 13
    .param p0    # Lcom/facebook/messaging/accountswitch/UnseenCountFetchRunner;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 574562
    iget-boolean v0, p0, Lcom/facebook/messaging/accountswitch/UnseenCountFetchRunner;->f:Z

    if-eqz v0, :cond_3

    .line 574563
    if-nez p1, :cond_1

    .line 574564
    :cond_0
    :goto_0
    return-void

    .line 574565
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableListNullOk()Ljava/util/ArrayList;

    move-result-object v3

    .line 574566
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 574567
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 574568
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/accountswitch/protocol/GetUnseenCountsNotificationResult;

    .line 574569
    iget-object v1, p0, Lcom/facebook/messaging/accountswitch/UnseenCountFetchRunner;->d:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Di5;

    iget-object v4, v0, Lcom/facebook/messaging/accountswitch/protocol/GetUnseenCountsNotificationResult;->a:Ljava/lang/String;

    iget-object v5, v0, Lcom/facebook/messaging/accountswitch/protocol/GetUnseenCountsNotificationResult;->b:Ljava/lang/String;

    iget v0, v0, Lcom/facebook/messaging/accountswitch/protocol/GetUnseenCountsNotificationResult;->c:I

    .line 574570
    new-instance v12, Landroid/content/Intent;

    sget-object v6, LX/2b2;->A:Ljava/lang/String;

    invoke-direct {v12, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 574571
    new-instance v6, Lcom/facebook/messaging/notify/MultipleAccountsNewMessagesNotification;

    const-string v8, ""

    const-string v10, ""

    move-object v7, v4

    move-object v9, v5

    move v11, v0

    invoke-direct/range {v6 .. v11}, Lcom/facebook/messaging/notify/MultipleAccountsNewMessagesNotification;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 574572
    const-string v7, "notification"

    invoke-virtual {v12, v7, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 574573
    iget-object v6, v1, LX/Di5;->c:LX/0Or;

    invoke-interface {v6}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/Bae;

    iget-object v7, v1, LX/Di5;->b:Landroid/content/Context;

    invoke-virtual {v6, v12, v7}, LX/2aA;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 574574
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 574575
    :cond_2
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/UnseenCountFetchRunner;->e:LX/2UW;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 574576
    if-nez v1, :cond_4

    .line 574577
    :goto_2
    goto :goto_0

    .line 574578
    :cond_3
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/UnseenCountFetchRunner;->e:LX/2UW;

    invoke-virtual {v0}, LX/2UW;->b()V

    goto :goto_0

    .line 574579
    :cond_4
    iget-object v2, v0, LX/2UW;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v3, LX/2Vv;->f:LX/0Tn;

    invoke-interface {v2, v3, v1}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    goto :goto_2
.end method


# virtual methods
.method public final declared-synchronized a()LX/1ML;
    .locals 6

    .prologue
    .line 574580
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/UnseenCountFetchRunner;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 574581
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 574582
    invoke-static {v0}, LX/1ML;->immediateFuture(Lcom/facebook/fbservice/service/OperationResult;)LX/1ML;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 574583
    :goto_0
    monitor-exit p0

    return-object v0

    .line 574584
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/UnseenCountFetchRunner;->g:LX/1ML;

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 574585
    if-eqz v0, :cond_1

    .line 574586
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/UnseenCountFetchRunner;->g:LX/1ML;

    goto :goto_0

    .line 574587
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/UnseenCountFetchRunner;->a:LX/0aG;

    const-string v1, "update_unseen_counts"

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    const-class v4, Lcom/facebook/messaging/accountswitch/UnseenCountFetchRunner;

    const-string v5, "UnseenCountFetchRunner"

    invoke-static {v4, v5}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    const v5, 0x1d2a9c07

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/accountswitch/UnseenCountFetchRunner;->g:LX/1ML;

    .line 574588
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/UnseenCountFetchRunner;->g:LX/1ML;

    new-instance v1, LX/JdK;

    invoke-direct {v1, p0}, LX/JdK;-><init>(Lcom/facebook/messaging/accountswitch/UnseenCountFetchRunner;)V

    iget-object v2, p0, Lcom/facebook/messaging/accountswitch/UnseenCountFetchRunner;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 574589
    iget-object v0, p0, Lcom/facebook/messaging/accountswitch/UnseenCountFetchRunner;->g:LX/1ML;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 574590
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method
