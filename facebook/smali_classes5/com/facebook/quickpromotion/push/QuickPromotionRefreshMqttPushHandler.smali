.class public Lcom/facebook/quickpromotion/push/QuickPromotionRefreshMqttPushHandler;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/1fT;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile h:Lcom/facebook/quickpromotion/push/QuickPromotionRefreshMqttPushHandler;


# instance fields
.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0iA;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/117;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2ae;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/11A;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 576493
    const-class v0, Lcom/facebook/quickpromotion/push/QuickPromotionRefreshMqttPushHandler;

    sput-object v0, Lcom/facebook/quickpromotion/push/QuickPromotionRefreshMqttPushHandler;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0iA;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/117;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2ae;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/11A;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 576494
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 576495
    iput-object p1, p0, Lcom/facebook/quickpromotion/push/QuickPromotionRefreshMqttPushHandler;->b:LX/0Ot;

    .line 576496
    iput-object p2, p0, Lcom/facebook/quickpromotion/push/QuickPromotionRefreshMqttPushHandler;->c:LX/0Ot;

    .line 576497
    iput-object p3, p0, Lcom/facebook/quickpromotion/push/QuickPromotionRefreshMqttPushHandler;->d:LX/0Ot;

    .line 576498
    iput-object p4, p0, Lcom/facebook/quickpromotion/push/QuickPromotionRefreshMqttPushHandler;->e:LX/0Ot;

    .line 576499
    iput-object p5, p0, Lcom/facebook/quickpromotion/push/QuickPromotionRefreshMqttPushHandler;->f:LX/0Ot;

    .line 576500
    iput-object p6, p0, Lcom/facebook/quickpromotion/push/QuickPromotionRefreshMqttPushHandler;->g:LX/0Ot;

    .line 576501
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/quickpromotion/push/QuickPromotionRefreshMqttPushHandler;
    .locals 10

    .prologue
    .line 576502
    sget-object v0, Lcom/facebook/quickpromotion/push/QuickPromotionRefreshMqttPushHandler;->h:Lcom/facebook/quickpromotion/push/QuickPromotionRefreshMqttPushHandler;

    if-nez v0, :cond_1

    .line 576503
    const-class v1, Lcom/facebook/quickpromotion/push/QuickPromotionRefreshMqttPushHandler;

    monitor-enter v1

    .line 576504
    :try_start_0
    sget-object v0, Lcom/facebook/quickpromotion/push/QuickPromotionRefreshMqttPushHandler;->h:Lcom/facebook/quickpromotion/push/QuickPromotionRefreshMqttPushHandler;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 576505
    if-eqz v2, :cond_0

    .line 576506
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 576507
    new-instance v3, Lcom/facebook/quickpromotion/push/QuickPromotionRefreshMqttPushHandler;

    const/16 v4, 0xbd2

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0xbd0

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0xbcc

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0xb83

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x259

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0xbcb

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-direct/range {v3 .. v9}, Lcom/facebook/quickpromotion/push/QuickPromotionRefreshMqttPushHandler;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 576508
    move-object v0, v3

    .line 576509
    sput-object v0, Lcom/facebook/quickpromotion/push/QuickPromotionRefreshMqttPushHandler;->h:Lcom/facebook/quickpromotion/push/QuickPromotionRefreshMqttPushHandler;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 576510
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 576511
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 576512
    :cond_1
    sget-object v0, Lcom/facebook/quickpromotion/push/QuickPromotionRefreshMqttPushHandler;->h:Lcom/facebook/quickpromotion/push/QuickPromotionRefreshMqttPushHandler;

    return-object v0

    .line 576513
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 576514
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final onMessage(Ljava/lang/String;[BJ)V
    .locals 4

    .prologue
    .line 576515
    :try_start_0
    const-string v0, "/quick_promotion_refresh"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 576516
    new-instance v2, Lcom/facebook/interstitial/api/FetchInterstitialsParams;

    iget-object v0, p0, Lcom/facebook/quickpromotion/push/QuickPromotionRefreshMqttPushHandler;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/117;

    invoke-virtual {v0}, LX/117;->a()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/facebook/interstitial/api/FetchInterstitialsParams;-><init>(LX/0Px;)V

    .line 576517
    iget-object v0, p0, Lcom/facebook/quickpromotion/push/QuickPromotionRefreshMqttPushHandler;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11A;

    .line 576518
    iget-object v1, v0, LX/11A;->a:LX/0Zb;

    const-string v3, "interstitials_push_fetch_start"

    const/4 p1, 0x0

    invoke-interface {v1, v3, p1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v1

    .line 576519
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 576520
    invoke-virtual {v1}, LX/0oG;->d()V

    .line 576521
    :cond_0
    iget-object v0, p0, Lcom/facebook/quickpromotion/push/QuickPromotionRefreshMqttPushHandler;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11H;

    iget-object v1, p0, Lcom/facebook/quickpromotion/push/QuickPromotionRefreshMqttPushHandler;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0e6;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 576522
    iget-object v1, p0, Lcom/facebook/quickpromotion/push/QuickPromotionRefreshMqttPushHandler;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0iA;

    invoke-virtual {v1, v0}, LX/0iA;->a(Ljava/util/List;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 576523
    :cond_1
    :goto_0
    return-void

    .line 576524
    :catch_0
    move-exception v0

    .line 576525
    sget-object v1, Lcom/facebook/quickpromotion/push/QuickPromotionRefreshMqttPushHandler;->a:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Failed to refresh QuickPromotions."

    invoke-static {v1, v2}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v1

    .line 576526
    iput-object v0, v1, LX/0VK;->c:Ljava/lang/Throwable;

    .line 576527
    move-object v0, v1

    .line 576528
    invoke-virtual {v0}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    .line 576529
    iget-object v0, p0, Lcom/facebook/quickpromotion/push/QuickPromotionRefreshMqttPushHandler;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    goto :goto_0
.end method
