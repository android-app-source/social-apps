.class public Lcom/facebook/quickpromotion/protocol/QuickPromotionDefinitionsFetchResultSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/quickpromotion/protocol/QuickPromotionDefinitionsFetchResult;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 635144
    const-class v0, Lcom/facebook/quickpromotion/protocol/QuickPromotionDefinitionsFetchResult;

    new-instance v1, Lcom/facebook/quickpromotion/protocol/QuickPromotionDefinitionsFetchResultSerializer;

    invoke-direct {v1}, Lcom/facebook/quickpromotion/protocol/QuickPromotionDefinitionsFetchResultSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 635145
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 635137
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/quickpromotion/protocol/QuickPromotionDefinitionsFetchResult;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 635138
    if-nez p0, :cond_0

    .line 635139
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 635140
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 635141
    invoke-static {p0, p1, p2}, Lcom/facebook/quickpromotion/protocol/QuickPromotionDefinitionsFetchResultSerializer;->b(Lcom/facebook/quickpromotion/protocol/QuickPromotionDefinitionsFetchResult;LX/0nX;LX/0my;)V

    .line 635142
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 635143
    return-void
.end method

.method private static b(Lcom/facebook/quickpromotion/protocol/QuickPromotionDefinitionsFetchResult;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 635135
    const-string v0, "promotions"

    iget-object v1, p0, Lcom/facebook/quickpromotion/protocol/QuickPromotionDefinitionsFetchResult;->mQuickPromotionDefinitions:Ljava/util/List;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 635136
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 635134
    check-cast p1, Lcom/facebook/quickpromotion/protocol/QuickPromotionDefinitionsFetchResult;

    invoke-static {p1, p2, p3}, Lcom/facebook/quickpromotion/protocol/QuickPromotionDefinitionsFetchResultSerializer;->a(Lcom/facebook/quickpromotion/protocol/QuickPromotionDefinitionsFetchResult;LX/0nX;LX/0my;)V

    return-void
.end method
