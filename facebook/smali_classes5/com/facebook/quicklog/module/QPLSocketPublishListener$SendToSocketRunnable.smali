.class public final Lcom/facebook/quicklog/module/QPLSocketPublishListener$SendToSocketRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/0YL;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/4l5;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0YL;)V
    .locals 2

    .prologue
    .line 803721
    iput-object p1, p0, Lcom/facebook/quicklog/module/QPLSocketPublishListener$SendToSocketRunnable;->a:LX/0YL;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 803722
    iget-object v1, p1, LX/0YL;->g:Ljava/util/List;

    monitor-enter v1

    .line 803723
    :try_start_0
    iget-object v0, p1, LX/0YL;->g:Ljava/util/List;

    iput-object v0, p0, Lcom/facebook/quicklog/module/QPLSocketPublishListener$SendToSocketRunnable;->b:Ljava/util/List;

    .line 803724
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 803725
    iput-object v0, p1, LX/0YL;->g:Ljava/util/List;

    .line 803726
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 803697
    iget-object v0, p0, Lcom/facebook/quicklog/module/QPLSocketPublishListener$SendToSocketRunnable;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 803698
    :goto_0
    return-void

    .line 803699
    :cond_0
    :try_start_0
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V

    .line 803700
    iget-object v0, p0, Lcom/facebook/quicklog/module/QPLSocketPublishListener$SendToSocketRunnable;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4l5;

    .line 803701
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    .line 803702
    const-string v4, "event"

    .line 803703
    iget-object v5, v0, LX/4l5;->a:Ljava/lang/String;

    move-object v5, v5

    .line 803704
    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 803705
    const-string v4, "action"

    .line 803706
    iget-object v5, v0, LX/4l5;->b:Ljava/lang/String;

    move-object v5, v5

    .line 803707
    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 803708
    const-string v4, "duration"

    .line 803709
    iget v5, v0, LX/4l5;->c:I

    move v0, v5

    .line 803710
    invoke-virtual {v3, v4, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 803711
    invoke-virtual {v1, v3}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    .line 803712
    :catch_0
    move-exception v0

    .line 803713
    const-class v1, Lcom/facebook/quicklog/module/QPLSocketPublishListener$SendToSocketRunnable;

    const-string v2, "Unable to construct JSON record."

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 803714
    :cond_1
    :try_start_1
    new-instance v2, Ljava/net/Socket;

    const-string v0, "localhost"

    iget-object v3, p0, Lcom/facebook/quicklog/module/QPLSocketPublishListener$SendToSocketRunnable;->a:LX/0YL;

    iget v3, v3, LX/0YL;->k:I

    invoke-direct {v2, v0, v3}, Ljava/net/Socket;-><init>(Ljava/lang/String;I)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 803715
    :try_start_2
    new-instance v0, Ljava/io/PrintWriter;

    invoke-virtual {v2}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v3

    const/4 v4, 0x1

    invoke-direct {v0, v3, v4}, Ljava/io/PrintWriter;-><init>(Ljava/io/OutputStream;Z)V

    .line 803716
    invoke-virtual {v1}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 803717
    :try_start_3
    invoke-virtual {v2}, Ljava/net/Socket;->close()V
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    .line 803718
    :catch_1
    move-exception v0

    .line 803719
    const-class v1, Lcom/facebook/quicklog/module/QPLSocketPublishListener$SendToSocketRunnable;

    const-string v2, "Unable to write record to socket."

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 803720
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Ljava/net/Socket;->close()V

    throw v0
.end method
