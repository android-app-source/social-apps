.class public final Lcom/facebook/push/mqtt/service/MqttPushServiceManager$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/29A;


# direct methods
.method public constructor <init>(LX/29A;)V
    .locals 0

    .prologue
    .line 571427
    iput-object p1, p0, Lcom/facebook/push/mqtt/service/MqttPushServiceManager$2;->a:LX/29A;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 571428
    const-string v0, "%s.doInit.run"

    const-class v1, LX/29A;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const v2, -0x1feb5b7d

    invoke-static {v0, v1, v2}, LX/02m;->a(Ljava/lang/String;Ljava/lang/Object;I)V

    .line 571429
    :try_start_0
    iget-object v0, p0, Lcom/facebook/push/mqtt/service/MqttPushServiceManager$2;->a:LX/29A;

    .line 571430
    iget-object v1, v0, LX/29A;->n:LX/00G;

    invoke-virtual {v1}, LX/00G;->e()Z

    move-result v1

    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 571431
    iget-object v1, v0, LX/29A;->c:Landroid/content/Context;

    invoke-static {v1}, LX/1mU;->a(Landroid/content/Context;)V

    .line 571432
    iget-object v1, v0, LX/29A;->j:LX/0Xl;

    invoke-interface {v1}, LX/0Xl;->a()LX/0YX;

    move-result-object v1

    const-string v2, "com.facebook.orca.login.AuthStateMachineMonitor.LOGIN_COMPLETE"

    new-instance v3, LX/30D;

    invoke-direct {v3, v0}, LX/30D;-><init>(LX/29A;)V

    invoke-interface {v1, v2, v3}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v1

    iget-object v2, v0, LX/29A;->s:Landroid/os/Handler;

    invoke-interface {v1, v2}, LX/0YX;->a(Landroid/os/Handler;)LX/0YX;

    move-result-object v1

    invoke-interface {v1}, LX/0YX;->a()LX/0Yb;

    move-result-object v1

    invoke-virtual {v1}, LX/0Yb;->b()V

    .line 571433
    const/4 v1, 0x1

    invoke-static {v0, v1}, LX/29A;->b(LX/29A;Z)V

    .line 571434
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 571435
    const-string v2, "com.facebook.rti.mqtt.intent.ACTION_WAKEUP"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 571436
    new-instance v2, LX/0Yd;

    const-string v3, "com.facebook.rti.mqtt.intent.ACTION_WAKEUP"

    new-instance v4, LX/30M;

    invoke-direct {v4, v0}, LX/30M;-><init>(LX/29A;)V

    invoke-direct {v2, v3, v4}, LX/0Yd;-><init>(Ljava/lang/String;LX/0YZ;)V

    iput-object v2, v0, LX/29A;->v:LX/0Yd;

    .line 571437
    iget-object v2, v0, LX/29A;->c:Landroid/content/Context;

    iget-object v3, v0, LX/29A;->v:LX/0Yd;

    const/4 v4, 0x0

    iget-object p0, v0, LX/29A;->s:Landroid/os/Handler;

    invoke-virtual {v2, v3, v1, v4, p0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 571438
    iget-object v1, v0, LX/29A;->j:LX/0Xl;

    invoke-interface {v1}, LX/0Xl;->a()LX/0YX;

    move-result-object v1

    const-string v2, "ACTION_MQTT_FORCE_REBIND"

    new-instance v3, LX/30O;

    invoke-direct {v3, v0}, LX/30O;-><init>(LX/29A;)V

    invoke-interface {v1, v2, v3}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v1

    const-string v2, "com.facebook.rti.mqtt.intent.ACTION_WAKEUP"

    new-instance v3, LX/30P;

    invoke-direct {v3, v0}, LX/30P;-><init>(LX/29A;)V

    invoke-interface {v1, v2, v3}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v1

    iget-object v2, v0, LX/29A;->s:Landroid/os/Handler;

    invoke-interface {v1, v2}, LX/0YX;->a(Landroid/os/Handler;)LX/0YX;

    move-result-object v1

    invoke-interface {v1}, LX/0YX;->a()LX/0Yb;

    move-result-object v1

    invoke-virtual {v1}, LX/0Yb;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 571439
    const v0, -0xffa49b9

    invoke-static {v0}, LX/02m;->a(I)V

    .line 571440
    return-void

    .line 571441
    :catchall_0
    move-exception v0

    const v1, 0x2000586e

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method
