.class public final Lcom/facebook/push/mqtt/service/ClientSubscriptionManager$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:LX/0Px;

.field public final synthetic c:LX/0Px;

.field public final synthetic d:LX/1fU;


# direct methods
.method public constructor <init>(LX/1fU;ZLX/0Px;LX/0Px;)V
    .locals 0

    .prologue
    .line 571412
    iput-object p1, p0, Lcom/facebook/push/mqtt/service/ClientSubscriptionManager$1;->d:LX/1fU;

    iput-boolean p2, p0, Lcom/facebook/push/mqtt/service/ClientSubscriptionManager$1;->a:Z

    iput-object p3, p0, Lcom/facebook/push/mqtt/service/ClientSubscriptionManager$1;->b:LX/0Px;

    iput-object p4, p0, Lcom/facebook/push/mqtt/service/ClientSubscriptionManager$1;->c:LX/0Px;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 571413
    iget-object v0, p0, Lcom/facebook/push/mqtt/service/ClientSubscriptionManager$1;->d:LX/1fU;

    iget-boolean v1, p0, Lcom/facebook/push/mqtt/service/ClientSubscriptionManager$1;->a:Z

    .line 571414
    iput-boolean v1, v0, LX/1fU;->h:Z

    .line 571415
    :try_start_0
    iget-object v0, p0, Lcom/facebook/push/mqtt/service/ClientSubscriptionManager$1;->d:LX/1fU;

    iget-object v0, v0, LX/1fU;->b:LX/1fY;

    iget-object v1, p0, Lcom/facebook/push/mqtt/service/ClientSubscriptionManager$1;->b:LX/0Px;

    iget-object v2, p0, Lcom/facebook/push/mqtt/service/ClientSubscriptionManager$1;->c:LX/0Px;

    invoke-virtual {v0, v1, v2}, LX/1fZ;->a(Ljava/util/List;Ljava/util/List;)V

    .line 571416
    iget-object v0, p0, Lcom/facebook/push/mqtt/service/ClientSubscriptionManager$1;->d:LX/1fU;

    iget-object v0, v0, LX/1fU;->g:LX/1tH;

    if-eqz v0, :cond_1

    .line 571417
    iget-object v0, p0, Lcom/facebook/push/mqtt/service/ClientSubscriptionManager$1;->b:LX/0Px;

    invoke-static {v0}, LX/1fU;->a(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v2

    .line 571418
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v3

    .line 571419
    iget-object v0, p0, Lcom/facebook/push/mqtt/service/ClientSubscriptionManager$1;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    iget-object v0, p0, Lcom/facebook/push/mqtt/service/ClientSubscriptionManager$1;->c:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1se;

    .line 571420
    iget-object v5, v0, LX/1se;->a:Ljava/lang/String;

    move-object v0, v5

    .line 571421
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 571422
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 571423
    :cond_0
    iget-object v0, p0, Lcom/facebook/push/mqtt/service/ClientSubscriptionManager$1;->d:LX/1fU;

    iget-object v0, v0, LX/1fU;->g:LX/1tH;

    iget-object v1, p0, Lcom/facebook/push/mqtt/service/ClientSubscriptionManager$1;->d:LX/1fU;

    iget-boolean v1, v1, LX/1fU;->h:Z

    invoke-interface {v0, v1, v2, v3}, LX/1tH;->a(ZLjava/util/List;Ljava/util/List;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 571424
    :cond_1
    :goto_1
    return-void

    .line 571425
    :catch_0
    move-exception v0

    .line 571426
    const-string v1, "ClientSubscriptionManager"

    const-string v2, "Ipc call failed"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method
