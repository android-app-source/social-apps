.class public final Lcom/facebook/push/mqtt/service/MqttPushServiceManager$10;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/29A;


# direct methods
.method public constructor <init>(LX/29A;)V
    .locals 0

    .prologue
    .line 576257
    iput-object p1, p0, Lcom/facebook/push/mqtt/service/MqttPushServiceManager$10;->a:LX/29A;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 576258
    iget-object v0, p0, Lcom/facebook/push/mqtt/service/MqttPushServiceManager$10;->a:LX/29A;

    .line 576259
    new-instance v1, LX/2Yw;

    iget-object v2, v0, LX/29A;->t:Landroid/os/Looper;

    invoke-direct {v1, v2}, LX/2Yw;-><init>(Landroid/os/Looper;)V

    .line 576260
    new-instance v2, Landroid/content/Intent;

    const-string v3, "Orca.START"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v3, "MESSENGER"

    new-instance p0, Landroid/os/Messenger;

    invoke-direct {p0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    invoke-virtual {v2, v3, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v2

    .line 576261
    iget-object v3, v0, LX/29A;->l:LX/2Bs;

    iget-object p0, v0, LX/29A;->c:Landroid/content/Context;

    invoke-virtual {v3, p0, v2}, LX/2Bs;->a(Landroid/content/Context;Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 576262
    new-instance v2, Lcom/facebook/push/mqtt/service/MqttPushServiceManager$11;

    invoke-direct {v2, v0}, Lcom/facebook/push/mqtt/service/MqttPushServiceManager$11;-><init>(LX/29A;)V

    .line 576263
    iget-object v3, v1, LX/2Yw;->a:Lcom/google/common/util/concurrent/SettableFuture;

    move-object v1, v3

    .line 576264
    iget-object v3, v0, LX/29A;->d:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v1, v2, v3}, Lcom/google/common/util/concurrent/ListenableFuture;->addListener(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 576265
    return-void
.end method
