.class public final Lcom/facebook/push/mqtt/service/MqttClientStateManager$3;
.super Lcom/facebook/common/executors/NamedRunnable;
.source ""


# instance fields
.field public final synthetic c:LX/2Hk;


# direct methods
.method public constructor <init>(LX/2Hk;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 571370
    iput-object p1, p0, Lcom/facebook/push/mqtt/service/MqttClientStateManager$3;->c:LX/2Hk;

    invoke-direct {p0, p2, p3}, Lcom/facebook/common/executors/NamedRunnable;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 14

    .prologue
    .line 571371
    iget-object v0, p0, Lcom/facebook/push/mqtt/service/MqttClientStateManager$3;->c:LX/2Hk;

    .line 571372
    iget-object v1, v0, LX/2Hk;->d:LX/0Uo;

    invoke-virtual {v1}, LX/0Uo;->o()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/2Hk;->h:LX/0pu;

    invoke-virtual {v1}, LX/0pu;->b()Z

    move-result v1

    if-nez v1, :cond_0

    .line 571373
    iget-object v2, v0, LX/2Hk;->d:LX/0Uo;

    invoke-virtual {v2}, LX/0Uo;->u()I

    move-result v2

    if-lez v2, :cond_1

    .line 571374
    const-string v2, "WrongAppStateActivity"

    .line 571375
    :goto_0
    iget-object v3, v0, LX/2Hk;->j:LX/03V;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Screen is off, but there are still active windows. Active floating windows="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, v0, LX/2Hk;->d:LX/0Uo;

    invoke-virtual {v5}, LX/0Uo;->t()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", active activities="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, LX/2Hk;->d:LX/0Uo;

    invoke-virtual {v5}, LX/0Uo;->u()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", Is AppBackgrounded: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, LX/2Hk;->d:LX/0Uo;

    invoke-virtual {v5}, LX/0Uo;->j()Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", Time since backgrounded: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, LX/2Hk;->d:LX/0Uo;

    invoke-virtual {v5}, LX/0Uo;->q()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", Time since foreground: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, LX/2Hk;->d:LX/0Uo;

    .line 571376
    iget-object v8, v5, LX/0Uo;->q:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/0So;

    invoke-interface {v8}, LX/0So;->now()J

    move-result-wide v8

    iget-wide v10, v5, LX/0Uo;->N:J

    sub-long v10, v8, v10

    iget-object v8, v5, LX/0Uo;->q:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/0So;

    invoke-interface {v8}, LX/0So;->now()J

    move-result-wide v8

    iget-wide v12, v5, LX/0Uo;->O:J

    sub-long/2addr v8, v12

    invoke-static {v10, v11, v8, v9}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v8

    move-wide v6, v8

    .line 571377
    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", Time since applaunch: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, LX/2Hk;->d:LX/0Uo;

    invoke-virtual {v5}, LX/0Uo;->c()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v2

    const/4 v4, 0x1

    .line 571378
    iput v4, v2, LX/0VK;->e:I

    .line 571379
    move-object v2, v2

    .line 571380
    invoke-virtual {v2}, LX/0VK;->g()LX/0VG;

    move-result-object v2

    invoke-virtual {v3, v2}, LX/03V;->a(LX/0VG;)V

    .line 571381
    :cond_0
    const/4 v1, 0x0

    iput-object v1, v0, LX/2Hk;->q:Ljava/util/concurrent/ScheduledFuture;

    .line 571382
    return-void

    .line 571383
    :cond_1
    iget-object v2, v0, LX/2Hk;->d:LX/0Uo;

    invoke-virtual {v2}, LX/0Uo;->t()I

    move-result v2

    if-lez v2, :cond_0

    .line 571384
    const-string v2, "WrongAppStateFloatingWindow"

    goto/16 :goto_0
.end method
