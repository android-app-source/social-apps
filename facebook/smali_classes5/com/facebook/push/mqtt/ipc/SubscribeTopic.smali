.class public Lcom/facebook/push/mqtt/ipc/SubscribeTopic;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/push/mqtt/ipc/SubscribeTopic;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 576298
    new-instance v0, LX/2Z4;

    invoke-direct {v0}, LX/2Z4;-><init>()V

    sput-object v0, Lcom/facebook/push/mqtt/ipc/SubscribeTopic;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 576294
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 576295
    iput-object p1, p0, Lcom/facebook/push/mqtt/ipc/SubscribeTopic;->a:Ljava/lang/String;

    .line 576296
    iput p2, p0, Lcom/facebook/push/mqtt/ipc/SubscribeTopic;->b:I

    .line 576297
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 576293
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 576299
    if-ne p0, p1, :cond_1

    .line 576300
    :cond_0
    :goto_0
    return v0

    .line 576301
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    .line 576302
    :cond_3
    check-cast p1, Lcom/facebook/push/mqtt/ipc/SubscribeTopic;

    .line 576303
    iget v2, p0, Lcom/facebook/push/mqtt/ipc/SubscribeTopic;->b:I

    iget v3, p1, Lcom/facebook/push/mqtt/ipc/SubscribeTopic;->b:I

    if-eq v2, v3, :cond_4

    move v0, v1

    goto :goto_0

    .line 576304
    :cond_4
    iget-object v2, p0, Lcom/facebook/push/mqtt/ipc/SubscribeTopic;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/push/mqtt/ipc/SubscribeTopic;->a:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 576305
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 576292
    iget-object v0, p0, Lcom/facebook/push/mqtt/ipc/SubscribeTopic;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    iget v1, p0, Lcom/facebook/push/mqtt/ipc/SubscribeTopic;->b:I

    add-int/2addr v0, v1

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 576291
    iget-object v0, p0, Lcom/facebook/push/mqtt/ipc/SubscribeTopic;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 576288
    iget-object v0, p0, Lcom/facebook/push/mqtt/ipc/SubscribeTopic;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 576289
    iget v0, p0, Lcom/facebook/push/mqtt/ipc/SubscribeTopic;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 576290
    return-void
.end method
