.class public Lcom/facebook/push/mqtt/ipc/StickySubscribeTopic;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/push/mqtt/ipc/StickySubscribeTopic;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/push/mqtt/ipc/SubscribeTopic;

.field private final b:Ljava/lang/String;

.field private final c:LX/768;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 576349
    new-instance v0, LX/2Z8;

    invoke-direct {v0}, LX/2Z8;-><init>()V

    sput-object v0, Lcom/facebook/push/mqtt/ipc/StickySubscribeTopic;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/push/mqtt/ipc/SubscribeTopic;Ljava/lang/String;LX/768;)V
    .locals 1
    .param p3    # LX/768;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 576344
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 576345
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/push/mqtt/ipc/SubscribeTopic;

    iput-object v0, p0, Lcom/facebook/push/mqtt/ipc/StickySubscribeTopic;->a:Lcom/facebook/push/mqtt/ipc/SubscribeTopic;

    .line 576346
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/push/mqtt/ipc/StickySubscribeTopic;->b:Ljava/lang/String;

    .line 576347
    iput-object p3, p0, Lcom/facebook/push/mqtt/ipc/StickySubscribeTopic;->c:LX/768;

    .line 576348
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 576343
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 576331
    if-ne p0, p1, :cond_1

    .line 576332
    :cond_0
    :goto_0
    return v0

    .line 576333
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 576334
    goto :goto_0

    .line 576335
    :cond_3
    check-cast p1, Lcom/facebook/push/mqtt/ipc/StickySubscribeTopic;

    .line 576336
    iget-object v2, p0, Lcom/facebook/push/mqtt/ipc/StickySubscribeTopic;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/push/mqtt/ipc/StickySubscribeTopic;->b:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/push/mqtt/ipc/StickySubscribeTopic;->c:LX/768;

    iget-object v3, p1, Lcom/facebook/push/mqtt/ipc/StickySubscribeTopic;->c:LX/768;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/push/mqtt/ipc/StickySubscribeTopic;->a:Lcom/facebook/push/mqtt/ipc/SubscribeTopic;

    iget-object v3, p1, Lcom/facebook/push/mqtt/ipc/StickySubscribeTopic;->a:Lcom/facebook/push/mqtt/ipc/SubscribeTopic;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 576342
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/push/mqtt/ipc/StickySubscribeTopic;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/push/mqtt/ipc/StickySubscribeTopic;->c:LX/768;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/push/mqtt/ipc/StickySubscribeTopic;->a:Lcom/facebook/push/mqtt/ipc/SubscribeTopic;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 576341
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "topic"

    iget-object v2, p0, Lcom/facebook/push/mqtt/ipc/StickySubscribeTopic;->a:Lcom/facebook/push/mqtt/ipc/SubscribeTopic;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "category"

    iget-object v2, p0, Lcom/facebook/push/mqtt/ipc/StickySubscribeTopic;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v1

    const-string v2, "listener"

    iget-object v0, p0, Lcom/facebook/push/mqtt/ipc/StickySubscribeTopic;->c:LX/768;

    if-eqz v0, :cond_0

    const-string v0, "non-null"

    :goto_0
    invoke-virtual {v1, v2, v0}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, "null"

    goto :goto_0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 576337
    iget-object v0, p0, Lcom/facebook/push/mqtt/ipc/StickySubscribeTopic;->a:Lcom/facebook/push/mqtt/ipc/SubscribeTopic;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 576338
    iget-object v0, p0, Lcom/facebook/push/mqtt/ipc/StickySubscribeTopic;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 576339
    iget-object v0, p0, Lcom/facebook/push/mqtt/ipc/StickySubscribeTopic;->c:LX/768;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStrongInterface(Landroid/os/IInterface;)V

    .line 576340
    return-void
.end method
