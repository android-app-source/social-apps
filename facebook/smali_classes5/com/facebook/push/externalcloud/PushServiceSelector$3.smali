.class public final Lcom/facebook/push/externalcloud/PushServiceSelector$3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:[Ljava/lang/Class;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/2Gc;


# direct methods
.method public constructor <init>(LX/2Gc;[Ljava/lang/Class;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 571351
    iput-object p1, p0, Lcom/facebook/push/externalcloud/PushServiceSelector$3;->c:LX/2Gc;

    iput-object p2, p0, Lcom/facebook/push/externalcloud/PushServiceSelector$3;->a:[Ljava/lang/Class;

    iput-object p3, p0, Lcom/facebook/push/externalcloud/PushServiceSelector$3;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    .line 571352
    iget-object v1, p0, Lcom/facebook/push/externalcloud/PushServiceSelector$3;->a:[Ljava/lang/Class;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 571353
    new-instance v4, Landroid/content/ComponentName;

    iget-object v5, p0, Lcom/facebook/push/externalcloud/PushServiceSelector$3;->c:LX/2Gc;

    iget-object v5, v5, LX/2Gc;->g:Landroid/content/Context;

    invoke-direct {v4, v5, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 571354
    iget-object v3, p0, Lcom/facebook/push/externalcloud/PushServiceSelector$3;->c:LX/2Gc;

    const/4 v5, 0x1

    .line 571355
    iget-object v6, v3, LX/2Gc;->f:Landroid/content/pm/PackageManager;

    invoke-virtual {v6, v4}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I

    move-result v6

    if-ne v5, v6, :cond_2

    :goto_1
    move v3, v5

    .line 571356
    if-nez v3, :cond_0

    .line 571357
    iget-object v3, p0, Lcom/facebook/push/externalcloud/PushServiceSelector$3;->c:LX/2Gc;

    iget-object v3, v3, LX/2Gc;->l:LX/0s6;

    iget-object v5, p0, Lcom/facebook/push/externalcloud/PushServiceSelector$3;->b:Ljava/lang/String;

    invoke-virtual {v3, v5}, LX/01H;->f(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 571358
    iget-object v3, p0, Lcom/facebook/push/externalcloud/PushServiceSelector$3;->c:LX/2Gc;

    const/4 v6, 0x1

    .line 571359
    iget-object v5, v3, LX/2Gc;->f:Landroid/content/pm/PackageManager;

    invoke-virtual {v5, v4, v6, v6}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 571360
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 571361
    :cond_1
    return-void

    :cond_2
    const/4 v5, 0x0

    goto :goto_1
.end method
