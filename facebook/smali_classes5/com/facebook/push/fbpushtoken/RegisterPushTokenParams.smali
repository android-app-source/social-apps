.class public Lcom/facebook/push/fbpushtoken/RegisterPushTokenParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/push/fbpushtoken/RegisterPushTokenParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:LX/2Ge;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Z

.field public final f:I

.field public final g:I

.field public final h:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 576155
    new-instance v0, LX/2XI;

    invoke-direct {v0}, LX/2XI;-><init>()V

    sput-object v0, Lcom/facebook/push/fbpushtoken/RegisterPushTokenParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/2Ge;Ljava/lang/String;Ljava/lang/String;ZIIJLjava/lang/String;)V
    .locals 1

    .prologue
    .line 576156
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 576157
    iput-object p1, p0, Lcom/facebook/push/fbpushtoken/RegisterPushTokenParams;->a:LX/2Ge;

    .line 576158
    iput-object p2, p0, Lcom/facebook/push/fbpushtoken/RegisterPushTokenParams;->b:Ljava/lang/String;

    .line 576159
    iput-object p3, p0, Lcom/facebook/push/fbpushtoken/RegisterPushTokenParams;->c:Ljava/lang/String;

    .line 576160
    iput-boolean p4, p0, Lcom/facebook/push/fbpushtoken/RegisterPushTokenParams;->e:Z

    .line 576161
    iput p5, p0, Lcom/facebook/push/fbpushtoken/RegisterPushTokenParams;->f:I

    .line 576162
    iput p6, p0, Lcom/facebook/push/fbpushtoken/RegisterPushTokenParams;->g:I

    .line 576163
    iput-wide p7, p0, Lcom/facebook/push/fbpushtoken/RegisterPushTokenParams;->h:J

    .line 576164
    iput-object p9, p0, Lcom/facebook/push/fbpushtoken/RegisterPushTokenParams;->d:Ljava/lang/String;

    .line 576165
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 576166
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 576167
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/2Ge;

    iput-object v0, p0, Lcom/facebook/push/fbpushtoken/RegisterPushTokenParams;->a:LX/2Ge;

    .line 576168
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/push/fbpushtoken/RegisterPushTokenParams;->b:Ljava/lang/String;

    .line 576169
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/push/fbpushtoken/RegisterPushTokenParams;->c:Ljava/lang/String;

    .line 576170
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/push/fbpushtoken/RegisterPushTokenParams;->e:Z

    .line 576171
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/push/fbpushtoken/RegisterPushTokenParams;->f:I

    .line 576172
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/push/fbpushtoken/RegisterPushTokenParams;->g:I

    .line 576173
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/push/fbpushtoken/RegisterPushTokenParams;->h:J

    .line 576174
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/push/fbpushtoken/RegisterPushTokenParams;->d:Ljava/lang/String;

    .line 576175
    return-void

    .line 576176
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 576177
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 576178
    iget-object v0, p0, Lcom/facebook/push/fbpushtoken/RegisterPushTokenParams;->a:LX/2Ge;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 576179
    iget-object v0, p0, Lcom/facebook/push/fbpushtoken/RegisterPushTokenParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 576180
    iget-object v0, p0, Lcom/facebook/push/fbpushtoken/RegisterPushTokenParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 576181
    iget-boolean v0, p0, Lcom/facebook/push/fbpushtoken/RegisterPushTokenParams;->e:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 576182
    iget v0, p0, Lcom/facebook/push/fbpushtoken/RegisterPushTokenParams;->f:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 576183
    iget v0, p0, Lcom/facebook/push/fbpushtoken/RegisterPushTokenParams;->g:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 576184
    iget-wide v0, p0, Lcom/facebook/push/fbpushtoken/RegisterPushTokenParams;->h:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 576185
    iget-object v0, p0, Lcom/facebook/push/fbpushtoken/RegisterPushTokenParams;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 576186
    return-void

    .line 576187
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
