.class public Lcom/facebook/push/fbpushtoken/RegisterPushTokenResult;
.super Lcom/facebook/fbservice/results/BaseResult;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/push/fbpushtoken/RegisterPushTokenResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Z

.field public b:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 576530
    new-instance v0, LX/2aR;

    invoke-direct {v0}, LX/2aR;-><init>()V

    sput-object v0, Lcom/facebook/push/fbpushtoken/RegisterPushTokenResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 576531
    invoke-direct {p0, p1}, Lcom/facebook/fbservice/results/BaseResult;-><init>(Landroid/os/Parcel;)V

    .line 576532
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/push/fbpushtoken/RegisterPushTokenResult;->a:Z

    .line 576533
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/facebook/push/fbpushtoken/RegisterPushTokenResult;->b:Z

    .line 576534
    return-void

    :cond_0
    move v0, v2

    .line 576535
    goto :goto_0

    :cond_1
    move v1, v2

    .line 576536
    goto :goto_1
.end method

.method public constructor <init>(ZZJ)V
    .locals 1

    .prologue
    .line 576537
    sget-object v0, LX/0ta;->FROM_SERVER:LX/0ta;

    invoke-direct {p0, v0, p3, p4}, Lcom/facebook/fbservice/results/BaseResult;-><init>(LX/0ta;J)V

    .line 576538
    iput-boolean p1, p0, Lcom/facebook/push/fbpushtoken/RegisterPushTokenResult;->a:Z

    .line 576539
    iput-boolean p2, p0, Lcom/facebook/push/fbpushtoken/RegisterPushTokenResult;->b:Z

    .line 576540
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 576541
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 576542
    invoke-super {p0, p1, p2}, Lcom/facebook/fbservice/results/BaseResult;->writeToParcel(Landroid/os/Parcel;I)V

    .line 576543
    iget-boolean v0, p0, Lcom/facebook/push/fbpushtoken/RegisterPushTokenResult;->a:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 576544
    iget-boolean v0, p0, Lcom/facebook/push/fbpushtoken/RegisterPushTokenResult;->b:Z

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 576545
    return-void

    :cond_0
    move v0, v2

    .line 576546
    goto :goto_0

    :cond_1
    move v1, v2

    .line 576547
    goto :goto_1
.end method
