.class public Lcom/facebook/push/registration/RegistrationHandler;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/1qM;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final f:Lcom/facebook/common/callercontext/CallerContext;

.field private static g:LX/0Xm;


# instance fields
.field public final a:LX/2XL;

.field public final b:LX/2Gt;

.field public final c:LX/2XM;

.field public final d:LX/2XN;

.field public final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 576190
    const-class v0, Lcom/facebook/push/registration/RegistrationHandler;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/push/registration/RegistrationHandler;->f:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/2XL;LX/2Gt;LX/2XM;LX/2XN;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2XL;",
            "LX/2Gt;",
            "LX/2XM;",
            "LX/2XN;",
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 576191
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 576192
    iput-object p1, p0, Lcom/facebook/push/registration/RegistrationHandler;->a:LX/2XL;

    .line 576193
    iput-object p2, p0, Lcom/facebook/push/registration/RegistrationHandler;->b:LX/2Gt;

    .line 576194
    iput-object p3, p0, Lcom/facebook/push/registration/RegistrationHandler;->c:LX/2XM;

    .line 576195
    iput-object p4, p0, Lcom/facebook/push/registration/RegistrationHandler;->d:LX/2XN;

    .line 576196
    iput-object p5, p0, Lcom/facebook/push/registration/RegistrationHandler;->e:LX/0Or;

    .line 576197
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/push/registration/RegistrationHandler;
    .locals 10

    .prologue
    .line 576198
    const-class v1, Lcom/facebook/push/registration/RegistrationHandler;

    monitor-enter v1

    .line 576199
    :try_start_0
    sget-object v0, Lcom/facebook/push/registration/RegistrationHandler;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 576200
    sput-object v2, Lcom/facebook/push/registration/RegistrationHandler;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 576201
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 576202
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 576203
    new-instance v3, Lcom/facebook/push/registration/RegistrationHandler;

    .line 576204
    new-instance v4, LX/2XL;

    invoke-direct {v4}, LX/2XL;-><init>()V

    .line 576205
    move-object v4, v4

    .line 576206
    move-object v4, v4

    .line 576207
    check-cast v4, LX/2XL;

    invoke-static {v0}, LX/2Gt;->a(LX/0QB;)LX/2Gt;

    move-result-object v5

    check-cast v5, LX/2Gt;

    .line 576208
    new-instance v6, LX/2XM;

    invoke-direct {v6}, LX/2XM;-><init>()V

    .line 576209
    move-object v6, v6

    .line 576210
    move-object v6, v6

    .line 576211
    check-cast v6, LX/2XM;

    .line 576212
    new-instance v9, LX/2XN;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v7

    check-cast v7, LX/0SG;

    const-class v8, LX/00H;

    invoke-interface {v0, v8}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/00H;

    invoke-direct {v9, v7, v8}, LX/2XN;-><init>(LX/0SG;LX/00H;)V

    .line 576213
    move-object v7, v9

    .line 576214
    check-cast v7, LX/2XN;

    const/16 v8, 0xb83

    invoke-static {v0, v8}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, Lcom/facebook/push/registration/RegistrationHandler;-><init>(LX/2XL;LX/2Gt;LX/2XM;LX/2XN;LX/0Or;)V

    .line 576215
    move-object v0, v3

    .line 576216
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 576217
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/push/registration/RegistrationHandler;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 576218
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 576219
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 576220
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 576221
    const-string v1, "register_push"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 576222
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 576223
    const-string v1, "registerPushTokenParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/push/fbpushtoken/RegisterPushTokenParams;

    .line 576224
    iget-object v1, p0, Lcom/facebook/push/registration/RegistrationHandler;->e:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/11H;

    .line 576225
    iget-object v2, p0, Lcom/facebook/push/registration/RegistrationHandler;->a:LX/2XL;

    invoke-virtual {v1, v2, v0}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/push/fbpushtoken/RegisterPushTokenResult;

    .line 576226
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    move-object v0, v0

    .line 576227
    :goto_0
    return-object v0

    .line 576228
    :cond_0
    const-string v1, "unregister_push"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 576229
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 576230
    const-string v1, "unregisterPushTokenParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/push/fbpushtoken/UnregisterPushTokenParams;

    .line 576231
    iget-object v1, p0, Lcom/facebook/push/registration/RegistrationHandler;->e:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/11H;

    .line 576232
    iget-object v2, p0, Lcom/facebook/push/registration/RegistrationHandler;->b:LX/2Gt;

    invoke-virtual {v1, v2, v0}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    .line 576233
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 576234
    move-object v0, v0

    .line 576235
    goto :goto_0

    .line 576236
    :cond_1
    const-string v1, "report_app_deletion"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 576237
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 576238
    const-string v1, "reportAppDeletionParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/push/fbpushtoken/ReportAppDeletionParams;

    .line 576239
    iget-object v1, p0, Lcom/facebook/push/registration/RegistrationHandler;->e:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/11H;

    .line 576240
    iget-object v2, p0, Lcom/facebook/push/registration/RegistrationHandler;->c:LX/2XM;

    invoke-virtual {v1, v2, v0}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    .line 576241
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 576242
    move-object v0, v0

    .line 576243
    goto :goto_0

    .line 576244
    :cond_2
    const-string v1, "register_push_no_user"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 576245
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 576246
    const-string v1, "registerPushTokenNoUserParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/push/fbpushtoken/RegisterPushTokenNoUserParams;

    .line 576247
    iget-object v1, p0, Lcom/facebook/push/registration/RegistrationHandler;->e:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/11H;

    .line 576248
    iget-object v2, p0, Lcom/facebook/push/registration/RegistrationHandler;->d:LX/2XN;

    sget-object v3, Lcom/facebook/push/registration/RegistrationHandler;->f:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2, v0, v3}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/push/fbpushtoken/RegisterPushTokenResult;

    .line 576249
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    move-object v0, v0

    .line 576250
    goto :goto_0

    .line 576251
    :cond_3
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "Unknown type"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0
.end method
