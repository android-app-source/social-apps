.class public Lcom/facebook/today/ui/components/partdefinition/VerticalListInnerCardUnitComponentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/2kk;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "LX/Hcj;",
        "TE;",
        "Landroid/widget/LinearLayout;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static e:LX/0Xm;


# instance fields
.field private final b:LX/DqU;

.field private final c:LX/1s9;

.field private final d:LX/1Qx;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 613378
    new-instance v0, LX/3cE;

    invoke-direct {v0}, LX/3cE;-><init>()V

    sput-object v0, Lcom/facebook/today/ui/components/partdefinition/VerticalListInnerCardUnitComponentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/DqU;LX/1s9;LX/1Qx;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 613373
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 613374
    iput-object p3, p0, Lcom/facebook/today/ui/components/partdefinition/VerticalListInnerCardUnitComponentPartDefinition;->d:LX/1Qx;

    .line 613375
    iput-object p2, p0, Lcom/facebook/today/ui/components/partdefinition/VerticalListInnerCardUnitComponentPartDefinition;->c:LX/1s9;

    .line 613376
    iput-object p1, p0, Lcom/facebook/today/ui/components/partdefinition/VerticalListInnerCardUnitComponentPartDefinition;->b:LX/DqU;

    .line 613377
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/today/ui/components/partdefinition/VerticalListInnerCardUnitComponentPartDefinition;
    .locals 6

    .prologue
    .line 613362
    const-class v1, Lcom/facebook/today/ui/components/partdefinition/VerticalListInnerCardUnitComponentPartDefinition;

    monitor-enter v1

    .line 613363
    :try_start_0
    sget-object v0, Lcom/facebook/today/ui/components/partdefinition/VerticalListInnerCardUnitComponentPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 613364
    sput-object v2, Lcom/facebook/today/ui/components/partdefinition/VerticalListInnerCardUnitComponentPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 613365
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 613366
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 613367
    new-instance p0, Lcom/facebook/today/ui/components/partdefinition/VerticalListInnerCardUnitComponentPartDefinition;

    invoke-static {v0}, LX/DqU;->a(LX/0QB;)LX/DqU;

    move-result-object v3

    check-cast v3, LX/DqU;

    invoke-static {v0}, LX/1s9;->a(LX/0QB;)LX/1s9;

    move-result-object v4

    check-cast v4, LX/1s9;

    invoke-static {v0}, LX/1Qu;->a(LX/0QB;)LX/1Qx;

    move-result-object v5

    check-cast v5, LX/1Qx;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/today/ui/components/partdefinition/VerticalListInnerCardUnitComponentPartDefinition;-><init>(LX/DqU;LX/1s9;LX/1Qx;)V

    .line 613368
    move-object v0, p0

    .line 613369
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 613370
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/today/ui/components/partdefinition/VerticalListInnerCardUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 613371
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 613372
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 613361
    sget-object v0, Lcom/facebook/today/ui/components/partdefinition/VerticalListInnerCardUnitComponentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 613352
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/1Pn;

    .line 613353
    iget-object v0, p0, Lcom/facebook/today/ui/components/partdefinition/VerticalListInnerCardUnitComponentPartDefinition;->c:LX/1s9;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VERTICAL_COMPONENTS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    invoke-virtual {v0, v1}, LX/1s9;->a(Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;)Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    move-result-object v0

    .line 613354
    check-cast v0, Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;

    sget-object v1, Lcom/facebook/today/ui/components/partdefinition/VerticalListInnerCardUnitComponentPartDefinition;->a:LX/1Cz;

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/today/ui/components/partdefinition/VerticalListInnerCardUnitComponentPartDefinition;->d:LX/1Qx;

    invoke-static {v0, v1, v2, v3}, LX/6Vo;->a(Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;LX/1Cz;Landroid/content/Context;LX/1Qx;)LX/1RC;

    move-result-object v2

    .line 613355
    invoke-static {p2}, LX/E1q;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/2nq;

    move-result-object v0

    .line 613356
    if-eqz v0, :cond_0

    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aF()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->SEEN_AND_READ:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    .line 613357
    :goto_0
    if-eqz v0, :cond_1

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02165e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    move-object v1, v0

    .line 613358
    :goto_1
    new-instance v3, LX/Hcj;

    invoke-interface {v2, p1, p2, p3}, LX/1Nt;->a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ra;

    invoke-direct {v3, v1, v0, v2}, LX/Hcj;-><init>(Landroid/graphics/drawable/Drawable;LX/1Ra;LX/1RC;)V

    return-object v3

    .line 613359
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 613360
    :cond_1
    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02165f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    move-object v1, v0

    goto :goto_1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x4bedf3be

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 613341
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p2, LX/Hcj;

    check-cast p3, LX/1Pn;

    check-cast p4, Landroid/widget/LinearLayout;

    .line 613342
    iget-object v1, p2, LX/Hcj;->a:Landroid/graphics/drawable/Drawable;

    invoke-static {p4, v1}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 613343
    iget-object v1, p2, LX/Hcj;->c:LX/1RC;

    iget-object v2, p2, LX/Hcj;->b:LX/1Ra;

    invoke-interface {v1, p1, v2, p3, p4}, LX/1Nt;->a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V

    .line 613344
    const/16 v1, 0x1f

    const v2, -0x6f4b7aba

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 613348
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 613349
    const-string v0, "SUCCESS"

    iget-object v1, p0, Lcom/facebook/today/ui/components/partdefinition/VerticalListInnerCardUnitComponentPartDefinition;->b:LX/DqU;

    invoke-virtual {v1, p1}, LX/DqU;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 613350
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 613351
    instance-of v0, v0, LX/9ud;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 613345
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p2, LX/Hcj;

    check-cast p3, LX/1Pn;

    check-cast p4, Landroid/widget/LinearLayout;

    .line 613346
    iget-object v0, p2, LX/Hcj;->c:LX/1RC;

    iget-object v1, p2, LX/Hcj;->b:LX/1Ra;

    invoke-interface {v0, p1, v1, p3, p4}, LX/1Nt;->b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V

    .line 613347
    return-void
.end method
