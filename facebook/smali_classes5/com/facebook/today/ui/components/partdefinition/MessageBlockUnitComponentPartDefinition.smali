.class public Lcom/facebook/today/ui/components/partdefinition/MessageBlockUnitComponentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kn;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/fbui/widget/contentview/ContentView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static e:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

.field private final c:Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

.field private final d:Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 613334
    new-instance v0, LX/3cD;

    invoke-direct {v0}, LX/3cD;-><init>()V

    sput-object v0, Lcom/facebook/today/ui/components/partdefinition/MessageBlockUnitComponentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 613329
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 613330
    iput-object p1, p0, Lcom/facebook/today/ui/components/partdefinition/MessageBlockUnitComponentPartDefinition;->b:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    .line 613331
    iput-object p2, p0, Lcom/facebook/today/ui/components/partdefinition/MessageBlockUnitComponentPartDefinition;->c:Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    .line 613332
    iput-object p3, p0, Lcom/facebook/today/ui/components/partdefinition/MessageBlockUnitComponentPartDefinition;->d:Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;

    .line 613333
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/today/ui/components/partdefinition/MessageBlockUnitComponentPartDefinition;
    .locals 6

    .prologue
    .line 613307
    const-class v1, Lcom/facebook/today/ui/components/partdefinition/MessageBlockUnitComponentPartDefinition;

    monitor-enter v1

    .line 613308
    :try_start_0
    sget-object v0, Lcom/facebook/today/ui/components/partdefinition/MessageBlockUnitComponentPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 613309
    sput-object v2, Lcom/facebook/today/ui/components/partdefinition/MessageBlockUnitComponentPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 613310
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 613311
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 613312
    new-instance p0, Lcom/facebook/today/ui/components/partdefinition/MessageBlockUnitComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/today/ui/components/partdefinition/MessageBlockUnitComponentPartDefinition;-><init>(Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;)V

    .line 613313
    move-object v0, p0

    .line 613314
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 613315
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/today/ui/components/partdefinition/MessageBlockUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 613316
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 613317
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 613328
    sget-object v0, Lcom/facebook/today/ui/components/partdefinition/MessageBlockUnitComponentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 613319
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    const/4 v1, 0x0

    .line 613320
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v2, v0

    .line 613321
    iget-object v3, p0, Lcom/facebook/today/ui/components/partdefinition/MessageBlockUnitComponentPartDefinition;->b:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    invoke-interface {v2}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;->a()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-interface {p1, v3, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 613322
    iget-object v3, p0, Lcom/facebook/today/ui/components/partdefinition/MessageBlockUnitComponentPartDefinition;->c:Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    invoke-interface {v2}, LX/9uc;->cT()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$SubMessageModel;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, LX/9uc;->cT()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$SubMessageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$SubMessageModel;->a()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-interface {p1, v3, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 613323
    iget-object v3, p0, Lcom/facebook/today/ui/components/partdefinition/MessageBlockUnitComponentPartDefinition;->d:Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;

    invoke-interface {v2}, LX/9uc;->hk_()LX/174;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, LX/9uc;->hk_()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-interface {p1, v3, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 613324
    return-object v1

    :cond_0
    move-object v0, v1

    .line 613325
    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 613326
    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 613327
    goto :goto_2
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 613318
    const/4 v0, 0x1

    return v0
.end method
