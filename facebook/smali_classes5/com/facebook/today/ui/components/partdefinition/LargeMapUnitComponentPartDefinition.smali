.class public Lcom/facebook/today/ui/components/partdefinition/LargeMapUnitComponentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kn;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "LX/HcZ;",
        "TE;",
        "Lcom/facebook/maps/FbStaticMapView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static c:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 613270
    new-instance v0, LX/3cC;

    invoke-direct {v0}, LX/3cC;-><init>()V

    sput-object v0, Lcom/facebook/today/ui/components/partdefinition/LargeMapUnitComponentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 613299
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 613300
    iput-object p1, p0, Lcom/facebook/today/ui/components/partdefinition/LargeMapUnitComponentPartDefinition;->b:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    .line 613301
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/today/ui/components/partdefinition/LargeMapUnitComponentPartDefinition;
    .locals 4

    .prologue
    .line 613288
    const-class v1, Lcom/facebook/today/ui/components/partdefinition/LargeMapUnitComponentPartDefinition;

    monitor-enter v1

    .line 613289
    :try_start_0
    sget-object v0, Lcom/facebook/today/ui/components/partdefinition/LargeMapUnitComponentPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 613290
    sput-object v2, Lcom/facebook/today/ui/components/partdefinition/LargeMapUnitComponentPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 613291
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 613292
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 613293
    new-instance p0, Lcom/facebook/today/ui/components/partdefinition/LargeMapUnitComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;->a(LX/0QB;)Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/today/ui/components/partdefinition/LargeMapUnitComponentPartDefinition;-><init>(Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;)V

    .line 613294
    move-object v0, p0

    .line 613295
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 613296
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/today/ui/components/partdefinition/LargeMapUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 613297
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 613298
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/maps/FbStaticMapView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 613287
    sget-object v0, Lcom/facebook/today/ui/components/partdefinition/LargeMapUnitComponentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 613279
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 613280
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 613281
    invoke-interface {v0}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 613282
    iget-object v1, p0, Lcom/facebook/today/ui/components/partdefinition/LargeMapUnitComponentPartDefinition;->b:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    new-instance v2, LX/E1o;

    invoke-interface {v0}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v3

    invoke-direct {v2, v3, p2}, LX/E1o;-><init>(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 613283
    :cond_0
    new-instance v1, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    const-string v2, "notifications"

    invoke-direct {v1, v2}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;-><init>(Ljava/lang/String;)V

    .line 613284
    invoke-interface {v0}, LX/9uc;->bt()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$LocationModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$LocationModel;->a()D

    move-result-wide v2

    invoke-interface {v0}, LX/9uc;->bt()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$LocationModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$LocationModel;->b()D

    move-result-wide v4

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->a(DD)Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    .line 613285
    invoke-interface {v0}, LX/9uc;->dv()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->a(I)Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    .line 613286
    new-instance v0, LX/HcZ;

    invoke-direct {v0, v1}, LX/HcZ;-><init>(Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;)V

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x70997539

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 613274
    check-cast p2, LX/HcZ;

    check-cast p4, Lcom/facebook/maps/FbStaticMapView;

    .line 613275
    const/16 v1, 0x8

    invoke-virtual {p4, v1}, LX/3BP;->setReportButtonVisibility(I)V

    .line 613276
    iget-object v1, p2, LX/HcZ;->a:Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    invoke-virtual {p4, v1}, LX/3BP;->setMapOptions(Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;)V

    .line 613277
    const/4 v1, 0x1

    invoke-virtual {p4, v1}, Lcom/facebook/maps/FbStaticMapView;->setEnabled(Z)V

    .line 613278
    const/16 v1, 0x1f

    const v2, 0x437a6bc0

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 613271
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 613272
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 613273
    invoke-interface {v0}, LX/9uc;->bt()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$LocationModel;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
