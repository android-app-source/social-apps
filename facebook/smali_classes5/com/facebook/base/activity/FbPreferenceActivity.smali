.class public Lcom/facebook/base/activity/FbPreferenceActivity;
.super Landroid/preference/PreferenceActivity;
.source ""

# interfaces
.implements LX/0ev;
.implements LX/0ez;
.implements LX/0Xn;


# instance fields
.field private final a:LX/0jb;

.field private b:LX/3p9;

.field public c:LX/0jf;

.field public d:LX/0jd;

.field public e:LX/0jn;

.field public f:LX/0Vt;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 660684
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    .line 660685
    new-instance v0, LX/0jb;

    invoke-direct {v0}, LX/0jb;-><init>()V

    iput-object v0, p0, Lcom/facebook/base/activity/FbPreferenceActivity;->a:LX/0jb;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/base/activity/FbPreferenceActivity;

    invoke-static {v2}, LX/0jf;->b(LX/0QB;)LX/0jf;

    move-result-object v0

    check-cast v0, LX/0jf;

    invoke-static {v2}, LX/0Vs;->a(LX/0QB;)LX/0Vs;

    move-result-object v1

    check-cast v1, LX/0Vt;

    invoke-static {v2}, LX/0jd;->b(LX/0QB;)LX/0jd;

    move-result-object v2

    check-cast v2, LX/0jd;

    iput-object v1, p0, Lcom/facebook/base/activity/FbPreferenceActivity;->f:LX/0Vt;

    iput-object v0, p0, Lcom/facebook/base/activity/FbPreferenceActivity;->c:LX/0jf;

    iput-object v2, p0, Lcom/facebook/base/activity/FbPreferenceActivity;->d:LX/0jd;

    return-void
.end method

.method public static synthetic a(Lcom/facebook/base/activity/FbPreferenceActivity;ILandroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 660622
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onMenuItemSelected(ILandroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(LX/0je;)V
    .locals 1

    .prologue
    .line 660623
    iget-object v0, p0, Lcom/facebook/base/activity/FbPreferenceActivity;->c:LX/0jf;

    invoke-virtual {v0, p1}, LX/0jf;->a(LX/0je;)V

    .line 660624
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 660625
    return-void
.end method

.method public final a(Ljava/lang/Throwable;)Z
    .locals 1

    .prologue
    .line 660626
    iget-object v0, p0, Lcom/facebook/base/activity/FbPreferenceActivity;->d:LX/0jd;

    invoke-virtual {v0, p1}, LX/0jd;->a(Ljava/lang/Throwable;)Z

    move-result v0

    return v0
.end method

.method public final addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    .prologue
    .line 660627
    iget-object v0, p0, Lcom/facebook/base/activity/FbPreferenceActivity;->d:LX/0jd;

    invoke-virtual {v0, p1, p2}, LX/0jd;->b(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 660628
    :goto_0
    return-void

    .line 660629
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/preference/PreferenceActivity;->addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public final attachBaseContext(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 660630
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->attachBaseContext(Landroid/content/Context;)V

    .line 660631
    invoke-static {p0, p0}, Lcom/facebook/base/activity/FbPreferenceActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 660632
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 660633
    return-void
.end method

.method public final c()LX/0gc;
    .locals 1

    .prologue
    .line 660634
    iget-object v0, p0, Lcom/facebook/base/activity/FbPreferenceActivity;->b:LX/3p9;

    invoke-virtual {v0}, LX/0k3;->p()LX/0gc;

    move-result-object v0

    return-object v0
.end method

.method public c(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 660635
    return-void
.end method

.method public finish()V
    .locals 1

    .prologue
    .line 660688
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->finish()V

    .line 660689
    iget-object v0, p0, Lcom/facebook/base/activity/FbPreferenceActivity;->d:LX/0jd;

    invoke-virtual {v0}, LX/0jd;->m()V

    .line 660690
    return-void
.end method

.method public final getMenuInflater()Landroid/view/MenuInflater;
    .locals 1

    .prologue
    .line 660636
    iget-object v0, p0, Lcom/facebook/base/activity/FbPreferenceActivity;->d:LX/0jd;

    invoke-virtual {v0}, LX/0jd;->c()Landroid/view/MenuInflater;

    move-result-object v0

    .line 660637
    if-eqz v0, :cond_0

    .line 660638
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    goto :goto_0
.end method

.method public final getProperty(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 660687
    iget-object v0, p0, Lcom/facebook/base/activity/FbPreferenceActivity;->a:LX/0jb;

    invoke-virtual {v0, p1}, LX/0jb;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getResources()Landroid/content/res/Resources;
    .locals 1

    .prologue
    .line 660686
    iget-object v0, p0, Lcom/facebook/base/activity/FbPreferenceActivity;->f:LX/0Vt;

    return-object v0
.end method

.method public final isValidFragment(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 660615
    const/4 v0, 0x1

    return v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 660681
    invoke-super {p0, p1, p2, p3}, Landroid/preference/PreferenceActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 660682
    iget-object v0, p0, Lcom/facebook/base/activity/FbPreferenceActivity;->d:LX/0jd;

    invoke-virtual {v0, p1, p2, p3}, LX/0jd;->a(IILandroid/content/Intent;)V

    .line 660683
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 660678
    iget-object v0, p0, Lcom/facebook/base/activity/FbPreferenceActivity;->d:LX/0jd;

    invoke-virtual {v0}, LX/0jd;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 660679
    :goto_0
    return-void

    .line 660680
    :cond_0
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 660675
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 660676
    iget-object v0, p0, Lcom/facebook/base/activity/FbPreferenceActivity;->d:LX/0jd;

    invoke-virtual {v0, p1}, LX/0jd;->a(Landroid/content/res/Configuration;)V

    .line 660677
    return-void
.end method

.method public final onContentChanged()V
    .locals 1

    .prologue
    .line 660672
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onContentChanged()V

    .line 660673
    iget-object v0, p0, Lcom/facebook/base/activity/FbPreferenceActivity;->d:LX/0jd;

    invoke-virtual {v0}, LX/0jd;->n()V

    .line 660674
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x22

    const v1, 0x2912e1d6

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 660642
    invoke-virtual {p0}, Lcom/facebook/base/activity/FbPreferenceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 660643
    const-string v2, ":android:show_fragment"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 660644
    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbPreferenceActivity;->setIntent(Landroid/content/Intent;)V

    .line 660645
    iget-object v0, p0, Lcom/facebook/base/activity/FbPreferenceActivity;->d:LX/0jd;

    .line 660646
    iget-object v2, p0, Lcom/facebook/base/activity/FbPreferenceActivity;->e:LX/0jn;

    if-nez v2, :cond_0

    .line 660647
    new-instance v2, LX/42k;

    invoke-direct {v2, p0}, LX/42k;-><init>(Lcom/facebook/base/activity/FbPreferenceActivity;)V

    iput-object v2, p0, Lcom/facebook/base/activity/FbPreferenceActivity;->e:LX/0jn;

    .line 660648
    :cond_0
    iget-object v2, p0, Lcom/facebook/base/activity/FbPreferenceActivity;->e:LX/0jn;

    move-object v2, v2

    .line 660649
    invoke-virtual {v0, p0, v2}, LX/0jd;->a(Landroid/app/Activity;LX/0jn;)V

    .line 660650
    new-instance v0, LX/3p9;

    invoke-direct {v0, p0}, LX/3p9;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/facebook/base/activity/FbPreferenceActivity;->b:LX/3p9;

    .line 660651
    invoke-virtual {p0, p1}, Lcom/facebook/base/activity/FbPreferenceActivity;->a(Landroid/os/Bundle;)V

    .line 660652
    invoke-virtual {p0}, Lcom/facebook/base/activity/FbPreferenceActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 660653
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 660654
    const/16 v0, 0x23

    const v2, 0x2cecd1ca

    invoke-static {v4, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 660655
    :goto_0
    return-void

    .line 660656
    :cond_1
    const/4 v0, 0x0

    .line 660657
    iget-object v2, p0, Lcom/facebook/base/activity/FbPreferenceActivity;->d:LX/0jd;

    invoke-virtual {v2, p1}, LX/0jd;->a(Landroid/os/Bundle;)Z

    .line 660658
    invoke-virtual {p0}, Lcom/facebook/base/activity/FbPreferenceActivity;->isFinishing()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 660659
    const/4 v0, 0x1

    .line 660660
    :cond_2
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 660661
    if-eqz v0, :cond_3

    .line 660662
    const v0, -0x719a964d

    invoke-static {v0, v1}, LX/02F;->c(II)V

    goto :goto_0

    .line 660663
    :cond_3
    iget-object v0, p0, Lcom/facebook/base/activity/FbPreferenceActivity;->b:LX/3p9;

    invoke-virtual {v0, p1}, LX/3p4;->a(Landroid/os/Bundle;)V

    .line 660664
    invoke-virtual {p0, p1}, Lcom/facebook/base/activity/FbPreferenceActivity;->b(Landroid/os/Bundle;)V

    .line 660665
    iget-object v0, p0, Lcom/facebook/base/activity/FbPreferenceActivity;->d:LX/0jd;

    invoke-virtual {v0, p1}, LX/0jd;->b(Landroid/os/Bundle;)Z

    .line 660666
    invoke-virtual {p0}, Lcom/facebook/base/activity/FbPreferenceActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 660667
    const v0, -0x3bbe154c

    invoke-static {v0, v1}, LX/02F;->c(II)V

    goto :goto_0

    .line 660668
    :cond_4
    invoke-virtual {p0, p1}, Lcom/facebook/base/activity/FbPreferenceActivity;->c(Landroid/os/Bundle;)V

    .line 660669
    iget-object v0, p0, Lcom/facebook/base/activity/FbPreferenceActivity;->d:LX/0jd;

    invoke-virtual {v0}, LX/0jd;->d()V

    .line 660670
    iget-object v0, p0, Lcom/facebook/base/activity/FbPreferenceActivity;->b:LX/3p9;

    invoke-virtual {v0}, LX/3p4;->a()V

    .line 660671
    const v0, 0xce00dd9

    invoke-static {v0, v1}, LX/02F;->c(II)V

    goto :goto_0
.end method

.method public final onCreateDialog(I)Landroid/app/Dialog;
    .locals 1

    .prologue
    .line 660639
    iget-object v0, p0, Lcom/facebook/base/activity/FbPreferenceActivity;->d:LX/0jd;

    invoke-virtual {v0, p1}, LX/0jd;->c(I)Landroid/app/Dialog;

    move-result-object v0

    .line 660640
    if-eqz v0, :cond_0

    .line 660641
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0
.end method

.method public final onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 660616
    iget-object v0, p0, Lcom/facebook/base/activity/FbPreferenceActivity;->d:LX/0jd;

    invoke-virtual {v0, p1}, LX/0jd;->a(Landroid/view/Menu;)V

    .line 660617
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public final onCreatePanelMenu(ILandroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 660618
    iget-object v0, p0, Lcom/facebook/base/activity/FbPreferenceActivity;->d:LX/0jd;

    invoke-virtual {v0, p1, p2}, LX/0jd;->a(ILandroid/view/Menu;)LX/0am;

    move-result-object v0

    .line 660619
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 660620
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 660621
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/preference/PreferenceActivity;->onCreatePanelMenu(ILandroid/view/Menu;)Z

    move-result v0

    goto :goto_0
.end method

.method public final onCreatePanelView(I)Landroid/view/View;
    .locals 1

    .prologue
    .line 660576
    iget-object v0, p0, Lcom/facebook/base/activity/FbPreferenceActivity;->d:LX/0jd;

    invoke-virtual {v0, p1}, LX/0jd;->a(I)LX/0am;

    move-result-object v0

    .line 660577
    if-eqz v0, :cond_0

    .line 660578
    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 660579
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreatePanelView(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x22

    const v2, 0x66e85927

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 660570
    :try_start_0
    iget-object v0, p0, Lcom/facebook/base/activity/FbPreferenceActivity;->c:LX/0jf;

    invoke-virtual {v0}, LX/0jf;->a()V

    .line 660571
    iget-object v0, p0, Lcom/facebook/base/activity/FbPreferenceActivity;->b:LX/3p9;

    invoke-virtual {v0}, LX/3p4;->f()V

    .line 660572
    iget-object v0, p0, Lcom/facebook/base/activity/FbPreferenceActivity;->d:LX/0jd;

    invoke-virtual {v0}, LX/0jd;->j()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 660573
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onDestroy()V

    .line 660574
    const v0, 0x2e26be04

    invoke-static {v0, v1}, LX/02F;->c(II)V

    return-void

    .line 660575
    :catchall_0
    move-exception v0

    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onDestroy()V

    const v2, 0xba6560a

    invoke-static {v2, v1}, LX/02F;->c(II)V

    throw v0
.end method

.method public final onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    .line 660566
    iget-object v0, p0, Lcom/facebook/base/activity/FbPreferenceActivity;->d:LX/0jd;

    invoke-virtual {v0, p1, p2}, LX/0jd;->a(ILandroid/view/KeyEvent;)LX/0am;

    move-result-object v0

    .line 660567
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 660568
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 660569
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/preference/PreferenceActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    .line 660562
    iget-object v0, p0, Lcom/facebook/base/activity/FbPreferenceActivity;->d:LX/0jd;

    invoke-virtual {v0, p1, p2}, LX/0jd;->b(ILandroid/view/KeyEvent;)LX/0am;

    move-result-object v0

    .line 660563
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 660564
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 660565
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/preference/PreferenceActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final onMenuItemSelected(ILandroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 660558
    iget-object v0, p0, Lcom/facebook/base/activity/FbPreferenceActivity;->d:LX/0jd;

    invoke-virtual {v0, p1, p2}, LX/0jd;->a(ILandroid/view/MenuItem;)LX/0am;

    move-result-object v0

    .line 660559
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 660560
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 660561
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/preference/PreferenceActivity;->onMenuItemSelected(ILandroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 660555
    iget-object v0, p0, Lcom/facebook/base/activity/FbPreferenceActivity;->d:LX/0jd;

    invoke-virtual {v0, p1}, LX/0jd;->a(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 660556
    const/4 v0, 0x1

    .line 660557
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x7a30266e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 660551
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onPause()V

    .line 660552
    iget-object v1, p0, Lcom/facebook/base/activity/FbPreferenceActivity;->b:LX/3p9;

    invoke-virtual {v1}, LX/3p4;->d()V

    .line 660553
    iget-object v1, p0, Lcom/facebook/base/activity/FbPreferenceActivity;->d:LX/0jd;

    invoke-virtual {v1}, LX/0jd;->g()V

    .line 660554
    const/16 v1, 0x23

    const v2, 0x5457fd39

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPostCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 660548
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onPostCreate(Landroid/os/Bundle;)V

    .line 660549
    iget-object v0, p0, Lcom/facebook/base/activity/FbPreferenceActivity;->d:LX/0jd;

    invoke-virtual {v0, p1}, LX/0jd;->d(Landroid/os/Bundle;)V

    .line 660550
    return-void
.end method

.method public final onPrepareDialog(ILandroid/app/Dialog;)V
    .locals 1

    .prologue
    .line 660545
    iget-object v0, p0, Lcom/facebook/base/activity/FbPreferenceActivity;->d:LX/0jd;

    invoke-virtual {v0, p1, p2}, LX/0jd;->a(ILandroid/app/Dialog;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 660546
    :goto_0
    return-void

    .line 660547
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/preference/PreferenceActivity;->onPrepareDialog(ILandroid/app/Dialog;)V

    goto :goto_0
.end method

.method public final onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 660543
    iget-object v0, p0, Lcom/facebook/base/activity/FbPreferenceActivity;->d:LX/0jd;

    invoke-virtual {v0, p1}, LX/0jd;->b(Landroid/view/Menu;)V

    .line 660544
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public final onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 660539
    iget-object v0, p0, Lcom/facebook/base/activity/FbPreferenceActivity;->d:LX/0jd;

    invoke-virtual {v0, p1, p2, p3}, LX/0jd;->a(ILandroid/view/View;Landroid/view/Menu;)LX/0am;

    move-result-object v0

    .line 660540
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 660541
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 660542
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/preference/PreferenceActivity;->onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z

    move-result v0

    goto :goto_0
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x30a4e27f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 660535
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onResume()V

    .line 660536
    iget-object v1, p0, Lcom/facebook/base/activity/FbPreferenceActivity;->b:LX/3p9;

    invoke-virtual {v1}, LX/3p4;->c()V

    .line 660537
    iget-object v1, p0, Lcom/facebook/base/activity/FbPreferenceActivity;->d:LX/0jd;

    invoke-virtual {v1}, LX/0jd;->h()V

    .line 660538
    const/16 v1, 0x23

    const v2, 0x77cfec6c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onRetainNonConfigurationInstance()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 660580
    const/4 v0, 0x0

    return-object v0
.end method

.method public final onSearchRequested()Z
    .locals 2

    .prologue
    .line 660581
    iget-object v0, p0, Lcom/facebook/base/activity/FbPreferenceActivity;->d:LX/0jd;

    invoke-virtual {v0}, LX/0jd;->l()LX/0am;

    move-result-object v0

    .line 660582
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 660583
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 660584
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onSearchRequested()Z

    move-result v0

    goto :goto_0
.end method

.method public onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x64c835d3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 660585
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onStart()V

    .line 660586
    iget-object v1, p0, Lcom/facebook/base/activity/FbPreferenceActivity;->b:LX/3p9;

    .line 660587
    iget-object v2, v1, LX/3p4;->b:LX/0jz;

    invoke-virtual {v2}, LX/0jz;->o()V

    .line 660588
    iget-object v1, p0, Lcom/facebook/base/activity/FbPreferenceActivity;->d:LX/0jd;

    invoke-virtual {v1}, LX/0jd;->e()V

    .line 660589
    const/16 v1, 0x23

    const v2, 0x151350df

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x4b01b274    # 8499828.0f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 660590
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onStop()V

    .line 660591
    iget-object v1, p0, Lcom/facebook/base/activity/FbPreferenceActivity;->b:LX/3p9;

    .line 660592
    iget-object v2, v1, LX/3p4;->b:LX/0jz;

    invoke-virtual {v2}, LX/0jz;->r()V

    .line 660593
    iget-object v1, p0, Lcom/facebook/base/activity/FbPreferenceActivity;->d:LX/0jd;

    invoke-virtual {v1}, LX/0jd;->f()V

    .line 660594
    const/16 v1, 0x23

    const v2, -0x2070b263

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onTitleChanged(Ljava/lang/CharSequence;I)V
    .locals 1

    .prologue
    .line 660595
    invoke-super {p0, p1, p2}, Landroid/preference/PreferenceActivity;->onTitleChanged(Ljava/lang/CharSequence;I)V

    .line 660596
    iget-object v0, p0, Lcom/facebook/base/activity/FbPreferenceActivity;->d:LX/0jd;

    invoke-virtual {v0, p1, p2}, LX/0jd;->a(Ljava/lang/CharSequence;I)V

    .line 660597
    return-void
.end method

.method public final onUserInteraction()V
    .locals 1

    .prologue
    .line 660598
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onUserInteraction()V

    .line 660599
    iget-object v0, p0, Lcom/facebook/base/activity/FbPreferenceActivity;->d:LX/0jd;

    invoke-virtual {v0}, LX/0jd;->k()V

    .line 660600
    return-void
.end method

.method public final onWindowFocusChanged(Z)V
    .locals 1

    .prologue
    .line 660601
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onWindowFocusChanged(Z)V

    .line 660602
    iget-object v0, p0, Lcom/facebook/base/activity/FbPreferenceActivity;->d:LX/0jd;

    invoke-virtual {v0, p1}, LX/0jd;->a(Z)V

    .line 660603
    return-void
.end method

.method public final setContentView(I)V
    .locals 1

    .prologue
    .line 660604
    iget-object v0, p0, Lcom/facebook/base/activity/FbPreferenceActivity;->d:LX/0jd;

    invoke-virtual {v0, p1}, LX/0jd;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 660605
    :goto_0
    return-void

    .line 660606
    :cond_0
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->setContentView(I)V

    goto :goto_0
.end method

.method public setContentView(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 660607
    iget-object v0, p0, Lcom/facebook/base/activity/FbPreferenceActivity;->d:LX/0jd;

    invoke-virtual {v0, p1}, LX/0jd;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 660608
    :goto_0
    return-void

    .line 660609
    :cond_0
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->setContentView(Landroid/view/View;)V

    goto :goto_0
.end method

.method public final setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    .prologue
    .line 660610
    iget-object v0, p0, Lcom/facebook/base/activity/FbPreferenceActivity;->d:LX/0jd;

    invoke-virtual {v0, p1, p2}, LX/0jd;->a(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 660611
    :goto_0
    return-void

    .line 660612
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/preference/PreferenceActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public final setProperty(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 660613
    iget-object v0, p0, Lcom/facebook/base/activity/FbPreferenceActivity;->a:LX/0jb;

    invoke-virtual {v0, p1, p2}, LX/0jb;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 660614
    return-void
.end method
