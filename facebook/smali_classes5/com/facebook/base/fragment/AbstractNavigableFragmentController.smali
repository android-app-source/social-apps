.class public abstract Lcom/facebook/base/fragment/AbstractNavigableFragmentController;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private b:Landroid/support/v4/app/Fragment;

.field private final c:LX/42n;

.field public d:LX/42q;

.field private e:LX/42s;

.field private f:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 668145
    const-class v0, Lcom/facebook/base/fragment/AbstractNavigableFragmentController;

    sput-object v0, Lcom/facebook/base/fragment/AbstractNavigableFragmentController;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 668034
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 668035
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/base/fragment/AbstractNavigableFragmentController;->f:I

    .line 668036
    new-instance v0, LX/42s;

    invoke-direct {v0}, LX/42s;-><init>()V

    iput-object v0, p0, Lcom/facebook/base/fragment/AbstractNavigableFragmentController;->e:LX/42s;

    .line 668037
    new-instance v0, LX/42o;

    invoke-direct {v0, p0}, LX/42o;-><init>(Lcom/facebook/base/fragment/AbstractNavigableFragmentController;)V

    iput-object v0, p0, Lcom/facebook/base/fragment/AbstractNavigableFragmentController;->c:LX/42n;

    .line 668038
    return-void
.end method

.method private declared-synchronized a(Ljava/lang/String;Landroid/os/Bundle;ZZIIII)V
    .locals 7

    .prologue
    .line 668112
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/facebook/base/fragment/AbstractNavigableFragmentController;->f:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/facebook/base/fragment/AbstractNavigableFragmentController;->f:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    .line 668113
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1, p2}, Landroid/support/v4/app/Fragment;->instantiate(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/base/fragment/AbstractNavigableFragmentController;->b:Landroid/support/v4/app/Fragment;

    .line 668114
    iget-object v0, p0, Lcom/facebook/base/fragment/AbstractNavigableFragmentController;->b:Landroid/support/v4/app/Fragment;

    check-cast v0, Lcom/facebook/base/fragment/NavigableFragment;

    iget-object v1, p0, Lcom/facebook/base/fragment/AbstractNavigableFragmentController;->c:LX/42n;

    invoke-interface {v0, v1}, Lcom/facebook/base/fragment/NavigableFragment;->a(LX/42n;)V

    .line 668115
    if-eqz p4, :cond_0

    .line 668116
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v3}, LX/0gc;->a(Ljava/lang/String;I)V

    .line 668117
    :cond_0
    iget-object v0, p0, Lcom/facebook/base/fragment/AbstractNavigableFragmentController;->e:LX/42s;

    const/4 v5, 0x0

    .line 668118
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v6

    .line 668119
    if-eqz p4, :cond_3

    iget-object v1, v0, LX/42s;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 668120
    iget-object v1, v0, LX/42s;->b:Ljava/util/ArrayList;

    iget-object v3, v0, LX/42s;->b:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/base/fragment/NavigableFragmentControllerBackStackHandler$ShadowBackstackEntry;

    move v4, v5

    .line 668121
    :goto_0
    iget-object v3, v0, LX/42s;->b:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ge v4, v3, :cond_2

    .line 668122
    iget-object v3, v0, LX/42s;->b:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/base/fragment/NavigableFragmentControllerBackStackHandler$ShadowBackstackEntry;

    .line 668123
    iget-boolean p1, v3, Lcom/facebook/base/fragment/NavigableFragmentControllerBackStackHandler$ShadowBackstackEntry;->b:Z

    if-nez p1, :cond_1

    .line 668124
    iget-object v3, v3, Lcom/facebook/base/fragment/NavigableFragmentControllerBackStackHandler$ShadowBackstackEntry;->a:Ljava/lang/String;

    invoke-virtual {v6, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 668125
    :cond_1
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_0

    .line 668126
    :cond_2
    iget-object v3, v0, LX/42s;->b:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 668127
    if-eqz p3, :cond_3

    .line 668128
    iget-object v3, v0, LX/42s;->b:Ljava/util/ArrayList;

    new-instance v4, Lcom/facebook/base/fragment/NavigableFragmentControllerBackStackHandler$ShadowBackstackEntry;

    iget-object v1, v1, Lcom/facebook/base/fragment/NavigableFragmentControllerBackStackHandler$ShadowBackstackEntry;->a:Ljava/lang/String;

    invoke-direct {v4, v1, v5}, Lcom/facebook/base/fragment/NavigableFragmentControllerBackStackHandler$ShadowBackstackEntry;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 668129
    :cond_3
    iget-object v1, v0, LX/42s;->b:Ljava/util/ArrayList;

    new-instance v3, Lcom/facebook/base/fragment/NavigableFragmentControllerBackStackHandler$ShadowBackstackEntry;

    invoke-direct {v3, v2, p3}, Lcom/facebook/base/fragment/NavigableFragmentControllerBackStackHandler$ShadowBackstackEntry;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 668130
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    move-object v3, v1

    .line 668131
    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 668132
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v4

    .line 668133
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v5, :cond_5

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 668134
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v6

    invoke-virtual {v6, v0}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 668135
    if-eqz v0, :cond_4

    .line 668136
    invoke-virtual {v4, v0}, LX/0hH;->a(Landroid/support/v4/app/Fragment;)LX/0hH;

    .line 668137
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 668138
    :cond_5
    invoke-virtual {v4}, LX/0hH;->b()I

    .line 668139
    :cond_6
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    invoke-virtual {v0, p5, p6, p7, p8}, LX/0hH;->a(IIII)LX/0hH;

    move-result-object v0

    const v1, 0x7f0d1a58

    iget-object v3, p0, Lcom/facebook/base/fragment/AbstractNavigableFragmentController;->b:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0, v1, v3, v2}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    .line 668140
    if-eqz p3, :cond_7

    .line 668141
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0hH;->a(Ljava/lang/String;)LX/0hH;

    .line 668142
    :cond_7
    invoke-virtual {v0}, LX/0hH;->b()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 668143
    monitor-exit p0

    return-void

    .line 668144
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static a$redex0(Lcom/facebook/base/fragment/AbstractNavigableFragmentController;Lcom/facebook/base/fragment/NavigableFragment;Landroid/content/Intent;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 668088
    if-eqz p1, :cond_0

    .line 668089
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    .line 668090
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->c()Z

    move-result v0

    if-nez v0, :cond_2

    .line 668091
    :cond_1
    :goto_0
    return-void

    .line 668092
    :cond_2
    const-string v0, "com.facebook.fragment.FRAGMENT_ACTION"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 668093
    invoke-virtual {p2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v1

    .line 668094
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Navigating to "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 668095
    if-eqz v1, :cond_1

    .line 668096
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 668097
    if-eqz v0, :cond_5

    .line 668098
    const-string v2, "com.facebook.fragment.PUSH_BACK_STACK"

    invoke-virtual {v0, v2, v8}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 668099
    const-string v2, "com.facebook.fragment.CLEAR_BACK_STACK"

    invoke-virtual {v0, v2, v8}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    .line 668100
    const-string v2, "com.facebook.fragment.ENTER_ANIM"

    invoke-virtual {v0, v2, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v5

    .line 668101
    const-string v2, "com.facebook.fragment.EXIT_ANIM"

    invoke-virtual {v0, v2, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v6

    .line 668102
    const-string v2, "com.facebook.fragment.POP_ENTER_ANIM"

    invoke-virtual {v0, v2, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v7

    .line 668103
    const-string v2, "com.facebook.fragment.POP_EXIT_ANIM"

    invoke-virtual {v0, v2, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v8

    .line 668104
    :goto_1
    const-string v0, "com.facebook.fragment.PUSH_BACK_STACK"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 668105
    const-string v0, "com.facebook.fragment.CLEAR_BACK_STACK"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 668106
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    move-object v0, p0

    invoke-direct/range {v0 .. v8}, Lcom/facebook/base/fragment/AbstractNavigableFragmentController;->a(Ljava/lang/String;Landroid/os/Bundle;ZZIIII)V

    goto :goto_0

    .line 668107
    :cond_3
    const-string v0, "com.facebook.fragment.BACK_ACTION"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 668108
    invoke-static {p0}, Lcom/facebook/base/fragment/AbstractNavigableFragmentController;->e(Lcom/facebook/base/fragment/AbstractNavigableFragmentController;)V

    .line 668109
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->d()V

    .line 668110
    goto :goto_0

    .line 668111
    :cond_4
    iget-object v0, p0, Lcom/facebook/base/fragment/AbstractNavigableFragmentController;->d:LX/42q;

    invoke-interface {v0, p1, p2}, LX/42q;->a(Lcom/facebook/base/fragment/NavigableFragment;Landroid/content/Intent;)V

    goto :goto_0

    :cond_5
    move v7, v8

    move v6, v8

    move v5, v8

    move v4, v8

    move v3, v8

    goto :goto_1
.end method

.method public static e(Lcom/facebook/base/fragment/AbstractNavigableFragmentController;)V
    .locals 5

    .prologue
    .line 668069
    iget-object v0, p0, Lcom/facebook/base/fragment/AbstractNavigableFragmentController;->e:LX/42s;

    .line 668070
    iget-object v1, v0, LX/42s;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v2, v1, -0x1

    .line 668071
    const/4 v1, 0x0

    move v3, v2

    move-object v2, v1

    .line 668072
    :goto_0
    if-ltz v3, :cond_1

    iget-object v1, v0, LX/42s;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/base/fragment/NavigableFragmentControllerBackStackHandler$ShadowBackstackEntry;

    iget-boolean v1, v1, Lcom/facebook/base/fragment/NavigableFragmentControllerBackStackHandler$ShadowBackstackEntry;->b:Z

    if-nez v1, :cond_1

    .line 668073
    iget-object v1, v0, LX/42s;->b:Ljava/util/ArrayList;

    add-int/lit8 v4, v3, -0x1

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/base/fragment/NavigableFragmentControllerBackStackHandler$ShadowBackstackEntry;

    .line 668074
    if-nez v2, :cond_0

    .line 668075
    iget-object v2, v1, Lcom/facebook/base/fragment/NavigableFragmentControllerBackStackHandler$ShadowBackstackEntry;->a:Ljava/lang/String;

    .line 668076
    move v3, v4

    goto :goto_0

    .line 668077
    :cond_0
    move v3, v4

    .line 668078
    goto :goto_0

    .line 668079
    :cond_1
    if-ltz v3, :cond_2

    .line 668080
    iget-object v1, v0, LX/42s;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/base/fragment/NavigableFragmentControllerBackStackHandler$ShadowBackstackEntry;

    iget-boolean v1, v1, Lcom/facebook/base/fragment/NavigableFragmentControllerBackStackHandler$ShadowBackstackEntry;->b:Z

    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 668081
    :cond_2
    move-object v0, v2

    .line 668082
    if-eqz v0, :cond_3

    .line 668083
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 668084
    if-eqz v0, :cond_3

    .line 668085
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/0hH;->a(Landroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v1

    invoke-virtual {v1}, LX/0hH;->b()I

    .line 668086
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 668087
    :cond_3
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 668067
    const/4 v0, 0x0

    invoke-static {p0, v0, p1}, Lcom/facebook/base/fragment/AbstractNavigableFragmentController;->a$redex0(Lcom/facebook/base/fragment/AbstractNavigableFragmentController;Lcom/facebook/base/fragment/NavigableFragment;Landroid/content/Intent;)V

    .line 668068
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 668060
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 668061
    if-eqz p1, :cond_0

    .line 668062
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    const v1, 0x7f0d1a58

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/base/fragment/AbstractNavigableFragmentController;->b:Landroid/support/v4/app/Fragment;

    .line 668063
    const-string v0, "tag_counter"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/facebook/base/fragment/AbstractNavigableFragmentController;->f:I

    .line 668064
    const-string v0, "shadow_backstack"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 668065
    new-instance v1, LX/42s;

    invoke-direct {v1, v0}, LX/42s;-><init>(Ljava/util/ArrayList;)V

    iput-object v1, p0, Lcom/facebook/base/fragment/AbstractNavigableFragmentController;->e:LX/42s;

    .line 668066
    :cond_0
    return-void
.end method

.method public final b()Z
    .locals 3

    .prologue
    .line 668053
    iget-object v0, p0, Lcom/facebook/base/fragment/AbstractNavigableFragmentController;->e:LX/42s;

    const/4 p0, 0x0

    .line 668054
    move v2, p0

    :goto_0
    iget-object v1, v0, LX/42s;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v2, v1, :cond_1

    .line 668055
    iget-object v1, v0, LX/42s;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/base/fragment/NavigableFragmentControllerBackStackHandler$ShadowBackstackEntry;

    iget-boolean v1, v1, Lcom/facebook/base/fragment/NavigableFragmentControllerBackStackHandler$ShadowBackstackEntry;->b:Z

    if-eqz v1, :cond_0

    .line 668056
    :goto_1
    move v0, p0

    .line 668057
    return v0

    .line 668058
    :cond_0
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 668059
    :cond_1
    const/4 p0, 0x1

    goto :goto_1
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 668051
    invoke-static {p0}, Lcom/facebook/base/fragment/AbstractNavigableFragmentController;->e(Lcom/facebook/base/fragment/AbstractNavigableFragmentController;)V

    .line 668052
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->e()Z

    move-result v0

    return v0
.end method

.method public final ds_()Z
    .locals 1

    .prologue
    .line 668050
    iget-object v0, p0, Lcom/facebook/base/fragment/AbstractNavigableFragmentController;->b:Landroid/support/v4/app/Fragment;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onAttachFragment(Landroid/support/v4/app/Fragment;)V
    .locals 1

    .prologue
    .line 668046
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onAttachFragment(Landroid/support/v4/app/Fragment;)V

    .line 668047
    instance-of v0, p1, Lcom/facebook/base/fragment/NavigableFragment;

    if-eqz v0, :cond_0

    .line 668048
    check-cast p1, Lcom/facebook/base/fragment/NavigableFragment;

    iget-object v0, p0, Lcom/facebook/base/fragment/AbstractNavigableFragmentController;->c:LX/42n;

    invoke-interface {p1, v0}, Lcom/facebook/base/fragment/NavigableFragment;->a(LX/42n;)V

    .line 668049
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x1cb2321d

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 668045
    const v1, 0x7f030a59

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x3d7c6be8

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 668039
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 668040
    const-string v0, "tag_counter"

    iget v1, p0, Lcom/facebook/base/fragment/AbstractNavigableFragmentController;->f:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 668041
    const-string v0, "shadow_backstack"

    iget-object v1, p0, Lcom/facebook/base/fragment/AbstractNavigableFragmentController;->e:LX/42s;

    .line 668042
    iget-object p0, v1, LX/42s;->b:Ljava/util/ArrayList;

    invoke-static {p0}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object p0

    move-object v1, p0

    .line 668043
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 668044
    return-void
.end method
