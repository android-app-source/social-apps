.class public Lcom/facebook/base/fragment/AbstractNavigableFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements Lcom/facebook/base/fragment/NavigableFragment;


# static fields
.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/42n;

.field public c:Landroid/content/Intent;

.field private d:Landroid/content/Intent;

.field private e:Ljava/lang/String;

.field private f:LX/03V;

.field public g:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 668030
    const-class v0, Lcom/facebook/base/fragment/AbstractNavigableFragment;

    sput-object v0, Lcom/facebook/base/fragment/AbstractNavigableFragment;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 668028
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 668029
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/base/fragment/AbstractNavigableFragment;->g:Z

    return-void
.end method

.method private b(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 668012
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/base/fragment/AbstractNavigableFragment;->d:Landroid/content/Intent;

    .line 668013
    iget-boolean v0, p0, Lcom/facebook/base/fragment/AbstractNavigableFragment;->g:Z

    move v0, v0

    .line 668014
    if-eqz v0, :cond_1

    .line 668015
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": Fragment already finished"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 668016
    iget-object v1, p0, Lcom/facebook/base/fragment/AbstractNavigableFragment;->c:Landroid/content/Intent;

    if-eqz v1, :cond_0

    .line 668017
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " with saved intent: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/base/fragment/AbstractNavigableFragment;->c:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 668018
    :cond_0
    sget-object v1, Lcom/facebook/base/fragment/AbstractNavigableFragment;->b:Ljava/lang/Class;

    invoke-static {v1, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 668019
    iget-object v1, p0, Lcom/facebook/base/fragment/AbstractNavigableFragment;->f:LX/03V;

    const-string v2, "FRAGMENT_NAVIGATION"

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 668020
    :goto_0
    return-void

    .line 668021
    :cond_1
    iget-object v0, p0, Lcom/facebook/base/fragment/AbstractNavigableFragment;->a:LX/42n;

    if-nez v0, :cond_2

    .line 668022
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": No navigation listener set; saving intent.  Created at:\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/base/fragment/AbstractNavigableFragment;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 668023
    sget-object v1, Lcom/facebook/base/fragment/AbstractNavigableFragment;->b:Ljava/lang/Class;

    new-instance v2, Ljava/lang/Throwable;

    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v1, v0, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 668024
    iget-object v1, p0, Lcom/facebook/base/fragment/AbstractNavigableFragment;->f:LX/03V;

    const-string v2, "FRAGMENT_NAVIGATION"

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 668025
    iput-object p1, p0, Lcom/facebook/base/fragment/AbstractNavigableFragment;->c:Landroid/content/Intent;

    .line 668026
    :goto_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/base/fragment/AbstractNavigableFragment;->g:Z

    goto :goto_0

    .line 668027
    :cond_2
    iget-object v0, p0, Lcom/facebook/base/fragment/AbstractNavigableFragment;->a:LX/42n;

    invoke-interface {v0, p0, p1}, LX/42n;->a(Lcom/facebook/base/fragment/NavigableFragment;Landroid/content/Intent;)V

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/42n;)V
    .locals 3

    .prologue
    .line 668005
    iput-object p1, p0, Lcom/facebook/base/fragment/AbstractNavigableFragment;->a:LX/42n;

    .line 668006
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/facebook/base/fragment/AbstractNavigableFragment;->c:Landroid/content/Intent;

    if-eqz v0, :cond_0

    .line 668007
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": Saved intent found: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/base/fragment/AbstractNavigableFragment;->c:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 668008
    sget-object v1, Lcom/facebook/base/fragment/AbstractNavigableFragment;->b:Ljava/lang/Class;

    new-instance v2, Ljava/lang/Throwable;

    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v1, v0, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 668009
    iget-object v1, p0, Lcom/facebook/base/fragment/AbstractNavigableFragment;->f:LX/03V;

    const-string v2, "FRAGMENT_NAVIGATION"

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 668010
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/facebook/base/fragment/AbstractNavigableFragment$1;

    invoke-direct {v1, p0, p1}, Lcom/facebook/base/fragment/AbstractNavigableFragment$1;-><init>(Lcom/facebook/base/fragment/AbstractNavigableFragment;LX/42n;)V

    const v2, -0x3dbe4470

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 668011
    :cond_0
    return-void
.end method

.method public final a(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 668000
    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->mResumed:Z

    move v0, v0

    .line 668001
    if-eqz v0, :cond_0

    .line 668002
    invoke-direct {p0, p1}, Lcom/facebook/base/fragment/AbstractNavigableFragment;->b(Landroid/content/Intent;)V

    .line 668003
    :goto_0
    return-void

    .line 668004
    :cond_0
    iput-object p1, p0, Lcom/facebook/base/fragment/AbstractNavigableFragment;->d:Landroid/content/Intent;

    goto :goto_0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 667996
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 667997
    new-instance v0, Ljava/lang/Throwable;

    invoke-direct {v0}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v0}, LX/23D;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/base/fragment/AbstractNavigableFragment;->e:Ljava/lang/String;

    .line 667998
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->kX_()LX/0QA;

    move-result-object v0

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    iput-object v0, p0, Lcom/facebook/base/fragment/AbstractNavigableFragment;->f:LX/03V;

    .line 667999
    return-void
.end method

.method public c_()V
    .locals 0

    .prologue
    .line 667984
    return-void
.end method

.method public onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x679b6842

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 667993
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 667994
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/base/fragment/AbstractNavigableFragment;->g:Z

    .line 667995
    const/16 v1, 0x2b

    const v2, -0x53e716d2

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x19eea75b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 667985
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 667986
    iget-object v1, p0, Lcom/facebook/base/fragment/AbstractNavigableFragment;->d:Landroid/content/Intent;

    if-eqz v1, :cond_0

    .line 667987
    iget-object v1, p0, Lcom/facebook/base/fragment/AbstractNavigableFragment;->d:Landroid/content/Intent;

    invoke-direct {p0, v1}, Lcom/facebook/base/fragment/AbstractNavigableFragment;->b(Landroid/content/Intent;)V

    .line 667988
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/base/fragment/AbstractNavigableFragment;->d:Landroid/content/Intent;

    .line 667989
    :cond_0
    iget-boolean v1, p0, Lcom/facebook/base/fragment/AbstractNavigableFragment;->g:Z

    move v1, v1

    .line 667990
    if-nez v1, :cond_1

    .line 667991
    invoke-virtual {p0}, Lcom/facebook/base/fragment/AbstractNavigableFragment;->c_()V

    .line 667992
    :cond_1
    const/16 v1, 0x2b

    const v2, 0x6190ee1d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
