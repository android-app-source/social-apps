.class public Lcom/facebook/cipher/jni/CipherHybrid;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final mHybridData:Lcom/facebook/jni/HybridData;
    .annotation build Lcom/facebook/crypto/proguard/annotations/DoNotStrip;
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/facebook/jni/HybridData;)V
    .locals 0

    .prologue
    .line 669220
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 669221
    iput-object p1, p0, Lcom/facebook/cipher/jni/CipherHybrid;->mHybridData:Lcom/facebook/jni/HybridData;

    .line 669222
    return-void
.end method

.method private static native initHybrid(Lcom/facebook/crypto/keychain/KeyChain;)Lcom/facebook/jni/HybridData;
.end method


# virtual methods
.method public native createDecrypt([BII)Lcom/facebook/cipher/jni/DecryptHybrid;
.end method

.method public native createEncrypt([BII)Lcom/facebook/cipher/jni/EncryptHybrid;
.end method
