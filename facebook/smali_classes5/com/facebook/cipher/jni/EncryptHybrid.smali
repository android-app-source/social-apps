.class public Lcom/facebook/cipher/jni/EncryptHybrid;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final mHybridData:Lcom/facebook/jni/HybridData;
    .annotation build Lcom/facebook/crypto/proguard/annotations/DoNotStrip;
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/facebook/jni/HybridData;)V
    .locals 0
    .annotation build Lcom/facebook/crypto/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 669226
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 669227
    iput-object p1, p0, Lcom/facebook/cipher/jni/EncryptHybrid;->mHybridData:Lcom/facebook/jni/HybridData;

    .line 669228
    return-void
.end method

.method private static native initHybrid([B[B[B)Lcom/facebook/jni/HybridData;
.end method


# virtual methods
.method public native end()[B
.end method

.method public native start()[B
.end method

.method public native write([BI[BII)V
.end method
