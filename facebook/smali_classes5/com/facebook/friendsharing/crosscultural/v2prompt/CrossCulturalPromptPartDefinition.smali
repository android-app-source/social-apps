.class public Lcom/facebook/friendsharing/crosscultural/v2prompt/CrossCulturalPromptPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/1Ri;",
        "Ljava/lang/Void;",
        "LX/1Qi;",
        "LX/AkC;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static h:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field public final c:LX/AqW;

.field private final d:Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;

.field private final e:LX/24B;

.field public final f:LX/BMP;

.field public final g:LX/B5l;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 586950
    new-instance v0, LX/3Ul;

    invoke-direct {v0}, LX/3Ul;-><init>()V

    sput-object v0, Lcom/facebook/friendsharing/crosscultural/v2prompt/CrossCulturalPromptPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/AqW;Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;LX/24B;LX/BMP;LX/B5l;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 586951
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 586952
    iput-object p1, p0, Lcom/facebook/friendsharing/crosscultural/v2prompt/CrossCulturalPromptPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 586953
    iput-object p2, p0, Lcom/facebook/friendsharing/crosscultural/v2prompt/CrossCulturalPromptPartDefinition;->c:LX/AqW;

    .line 586954
    iput-object p3, p0, Lcom/facebook/friendsharing/crosscultural/v2prompt/CrossCulturalPromptPartDefinition;->d:Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;

    .line 586955
    iput-object p4, p0, Lcom/facebook/friendsharing/crosscultural/v2prompt/CrossCulturalPromptPartDefinition;->e:LX/24B;

    .line 586956
    iput-object p5, p0, Lcom/facebook/friendsharing/crosscultural/v2prompt/CrossCulturalPromptPartDefinition;->f:LX/BMP;

    .line 586957
    iput-object p6, p0, Lcom/facebook/friendsharing/crosscultural/v2prompt/CrossCulturalPromptPartDefinition;->g:LX/B5l;

    .line 586958
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/friendsharing/crosscultural/v2prompt/CrossCulturalPromptPartDefinition;
    .locals 10

    .prologue
    .line 586959
    const-class v1, Lcom/facebook/friendsharing/crosscultural/v2prompt/CrossCulturalPromptPartDefinition;

    monitor-enter v1

    .line 586960
    :try_start_0
    sget-object v0, Lcom/facebook/friendsharing/crosscultural/v2prompt/CrossCulturalPromptPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 586961
    sput-object v2, Lcom/facebook/friendsharing/crosscultural/v2prompt/CrossCulturalPromptPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 586962
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 586963
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 586964
    new-instance v3, Lcom/facebook/friendsharing/crosscultural/v2prompt/CrossCulturalPromptPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/AqW;->b(LX/0QB;)LX/AqW;

    move-result-object v5

    check-cast v5, LX/AqW;

    invoke-static {v0}, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;

    invoke-static {v0}, LX/24B;->b(LX/0QB;)LX/24B;

    move-result-object v7

    check-cast v7, LX/24B;

    invoke-static {v0}, LX/BMP;->b(LX/0QB;)LX/BMP;

    move-result-object v8

    check-cast v8, LX/BMP;

    invoke-static {v0}, LX/B5l;->b(LX/0QB;)LX/B5l;

    move-result-object v9

    check-cast v9, LX/B5l;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/friendsharing/crosscultural/v2prompt/CrossCulturalPromptPartDefinition;-><init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/AqW;Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;LX/24B;LX/BMP;LX/B5l;)V

    .line 586965
    move-object v0, v3

    .line 586966
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 586967
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/friendsharing/crosscultural/v2prompt/CrossCulturalPromptPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 586968
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 586969
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 586970
    sget-object v0, Lcom/facebook/friendsharing/crosscultural/v2prompt/CrossCulturalPromptPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 586971
    check-cast p2, LX/1Ri;

    .line 586972
    iget-object v6, p0, Lcom/facebook/friendsharing/crosscultural/v2prompt/CrossCulturalPromptPartDefinition;->d:Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;

    new-instance v0, LX/24H;

    iget-object v1, p2, LX/1Ri;->c:LX/AkL;

    iget-object v2, p2, LX/1Ri;->c:LX/AkL;

    invoke-interface {v2}, LX/AkL;->g()LX/AkM;

    move-result-object v2

    iget-object v3, p2, LX/1Ri;->a:LX/1RN;

    iget-object v4, p2, LX/1Ri;->b:LX/0jW;

    iget-object v5, p0, Lcom/facebook/friendsharing/crosscultural/v2prompt/CrossCulturalPromptPartDefinition;->e:LX/24B;

    iget-object v7, p2, LX/1Ri;->a:LX/1RN;

    invoke-virtual {v5, v7}, LX/24B;->b(LX/1RN;)Z

    move-result v5

    invoke-direct/range {v0 .. v5}, LX/24H;-><init>(LX/AkL;LX/AkM;LX/1RN;LX/0jW;Z)V

    invoke-interface {p1, v6, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 586973
    iget-object v0, p0, Lcom/facebook/friendsharing/crosscultural/v2prompt/CrossCulturalPromptPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    iget-object v1, p2, LX/1Ri;->a:LX/1RN;

    .line 586974
    new-instance v2, LX/AqV;

    invoke-direct {v2, p0, v1}, LX/AqV;-><init>(Lcom/facebook/friendsharing/crosscultural/v2prompt/CrossCulturalPromptPartDefinition;LX/1RN;)V

    move-object v1, v2

    .line 586975
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 586976
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x4af9cfc9    # 8185828.5f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 586977
    check-cast p1, LX/1Ri;

    check-cast p4, LX/AkC;

    .line 586978
    iget-object v1, p0, Lcom/facebook/friendsharing/crosscultural/v2prompt/CrossCulturalPromptPartDefinition;->c:LX/AqW;

    invoke-virtual {v1}, LX/AqW;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 586979
    invoke-virtual {p4}, LX/AkC;->getV2AttachmentView()Landroid/view/View;

    move-result-object v1

    check-cast v1, LX/AqX;

    .line 586980
    iget-object v2, p0, Lcom/facebook/friendsharing/crosscultural/v2prompt/CrossCulturalPromptPartDefinition;->c:LX/AqW;

    iget-object p2, p1, LX/1Ri;->a:LX/1RN;

    invoke-virtual {v2, p2}, LX/AqW;->b(LX/1RN;)LX/0Px;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/AqX;->a(LX/0Px;)V

    .line 586981
    :cond_0
    invoke-virtual {p4}, LX/AkC;->getAttachmentInsertPoint()Landroid/widget/FrameLayout;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 586982
    const/16 v1, 0x1f

    const v2, -0x6cfaab36

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 586983
    check-cast p1, LX/1Ri;

    .line 586984
    iget-object v1, p1, LX/1Ri;->a:LX/1RN;

    invoke-static {v1}, LX/AqW;->a(LX/1RN;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 586985
    check-cast p4, LX/AkC;

    .line 586986
    invoke-virtual {p4}, LX/AkC;->getAttachmentInsertPoint()Landroid/widget/FrameLayout;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 586987
    return-void
.end method
