.class public Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/v3/SuggestedCoverPhotosPromptLargePartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/1Ri;",
        "LX/0Px",
        "<",
        "Ljava/lang/String;",
        ">;TE;",
        "LX/Aza;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static e:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;

.field private final c:LX/24B;

.field private final d:LX/AzX;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 587265
    new-instance v0, LX/3Ut;

    invoke-direct {v0}, LX/3Ut;-><init>()V

    sput-object v0, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/v3/SuggestedCoverPhotosPromptLargePartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;LX/24B;LX/AzX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 587297
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 587298
    iput-object p1, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/v3/SuggestedCoverPhotosPromptLargePartDefinition;->b:Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;

    .line 587299
    iput-object p2, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/v3/SuggestedCoverPhotosPromptLargePartDefinition;->c:LX/24B;

    .line 587300
    iput-object p3, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/v3/SuggestedCoverPhotosPromptLargePartDefinition;->d:LX/AzX;

    .line 587301
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/v3/SuggestedCoverPhotosPromptLargePartDefinition;
    .locals 6

    .prologue
    .line 587286
    const-class v1, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/v3/SuggestedCoverPhotosPromptLargePartDefinition;

    monitor-enter v1

    .line 587287
    :try_start_0
    sget-object v0, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/v3/SuggestedCoverPhotosPromptLargePartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 587288
    sput-object v2, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/v3/SuggestedCoverPhotosPromptLargePartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 587289
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 587290
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 587291
    new-instance p0, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/v3/SuggestedCoverPhotosPromptLargePartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;

    invoke-static {v0}, LX/24B;->b(LX/0QB;)LX/24B;

    move-result-object v4

    check-cast v4, LX/24B;

    const-class v5, LX/AzX;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/AzX;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/v3/SuggestedCoverPhotosPromptLargePartDefinition;-><init>(Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;LX/24B;LX/AzX;)V

    .line 587292
    move-object v0, p0

    .line 587293
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 587294
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/v3/SuggestedCoverPhotosPromptLargePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 587295
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 587296
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/Aza;",
            ">;"
        }
    .end annotation

    .prologue
    .line 587285
    sget-object v0, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/v3/SuggestedCoverPhotosPromptLargePartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 587302
    check-cast p2, LX/1Ri;

    check-cast p3, LX/1Pn;

    .line 587303
    iget-object v0, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/v3/SuggestedCoverPhotosPromptLargePartDefinition;->c:LX/24B;

    iget-object v2, p2, LX/1Ri;->a:LX/1RN;

    iget-object v3, p2, LX/1Ri;->b:LX/0jW;

    const-class v4, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/v3/SuggestedCoverPhotosPromptLargePartDefinition;

    iget-object v1, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/v3/SuggestedCoverPhotosPromptLargePartDefinition;->d:LX/AzX;

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v1, v5}, LX/AzX;->a(Landroid/content/Context;)LX/AzW;

    move-result-object v5

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/24B;->a(LX/1aD;LX/1RN;LX/0jW;Ljava/lang/Class;LX/24J;)V

    .line 587304
    iget-object v0, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/v3/SuggestedCoverPhotosPromptLargePartDefinition;->b:Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;

    invoke-static {p2}, LX/Ak0;->a(LX/1Ri;)LX/Ak0;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 587305
    iget-object v0, p2, LX/1Ri;->a:LX/1RN;

    iget-object v0, v0, LX/1RN;->a:LX/1kK;

    check-cast v0, LX/1kW;

    .line 587306
    iget-object v1, v0, LX/1kW;->a:Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-object v0, v1

    .line 587307
    invoke-virtual {v0}, Lcom/facebook/productionprompts/model/ProductionPrompt;->A()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x4a35255a    # 2967894.5f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 587280
    check-cast p1, LX/1Ri;

    check-cast p2, LX/0Px;

    check-cast p3, LX/1Pn;

    check-cast p4, LX/Aza;

    .line 587281
    iget-object v1, p4, LX/Aza;->l:Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptView;

    move-object v1, v1

    .line 587282
    iget-object v2, p1, LX/1Ri;->a:LX/1RN;

    invoke-virtual {v1, v2}, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptView;->a(LX/1RN;)V

    .line 587283
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;->a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V

    .line 587284
    const/16 v1, 0x1f

    const v2, -0x605a05f2

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 587271
    check-cast p1, LX/1Ri;

    const/4 v1, 0x0

    .line 587272
    if-eqz p1, :cond_0

    iget-object v0, p1, LX/1Ri;->a:LX/1RN;

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/1Ri;->a:LX/1RN;

    iget-object v0, v0, LX/1RN;->a:LX/1kK;

    instance-of v0, v0, LX/1kW;

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/1Ri;->a:LX/1RN;

    iget-object v0, v0, LX/1RN;->a:LX/1kK;

    check-cast v0, LX/1kW;

    .line 587273
    iget-object p0, v0, LX/1kW;->a:Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-object v0, p0

    .line 587274
    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 587275
    :goto_0
    return v0

    .line 587276
    :cond_1
    iget-object v0, p1, LX/1Ri;->a:LX/1RN;

    iget-object v0, v0, LX/1RN;->a:LX/1kK;

    check-cast v0, LX/1kW;

    .line 587277
    iget-object p0, v0, LX/1kW;->a:Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-object v0, p0

    .line 587278
    invoke-virtual {v0}, Lcom/facebook/productionprompts/model/ProductionPrompt;->A()LX/0Px;

    move-result-object v0

    .line 587279
    if-eqz v0, :cond_2

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 587266
    check-cast p1, LX/1Ri;

    check-cast p2, LX/0Px;

    check-cast p3, LX/1Pn;

    check-cast p4, LX/Aza;

    .line 587267
    iget-object v0, p4, LX/Aza;->l:Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptView;

    move-object v0, v0

    .line 587268
    invoke-virtual {v0}, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptView;->k()V

    .line 587269
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;->b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V

    .line 587270
    return-void
.end method
