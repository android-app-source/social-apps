.class public Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/1Ri;",
        "LX/AzR;",
        "LX/1Qi;",
        "LX/AkC;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static g:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;

.field private final c:LX/24B;

.field private final d:LX/AzX;

.field private final e:LX/AzS;

.field public f:LX/AzR;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 586945
    new-instance v0, LX/3Uk;

    invoke-direct {v0}, LX/3Uk;-><init>()V

    sput-object v0, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;LX/24B;LX/AzX;LX/AzS;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 586939
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 586940
    iput-object p1, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptPartDefinition;->b:Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;

    .line 586941
    iput-object p2, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptPartDefinition;->c:LX/24B;

    .line 586942
    iput-object p3, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptPartDefinition;->d:LX/AzX;

    .line 586943
    iput-object p4, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptPartDefinition;->e:LX/AzS;

    .line 586944
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptPartDefinition;
    .locals 7

    .prologue
    .line 586928
    const-class v1, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptPartDefinition;

    monitor-enter v1

    .line 586929
    :try_start_0
    sget-object v0, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 586930
    sput-object v2, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 586931
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 586932
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 586933
    new-instance p0, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;

    invoke-static {v0}, LX/24B;->b(LX/0QB;)LX/24B;

    move-result-object v4

    check-cast v4, LX/24B;

    const-class v5, LX/AzX;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/AzX;

    const-class v6, LX/AzS;

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/AzS;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptPartDefinition;-><init>(Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;LX/24B;LX/AzX;LX/AzS;)V

    .line 586934
    move-object v0, p0

    .line 586935
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 586936
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 586937
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 586938
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 586927
    sget-object v0, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 586922
    check-cast p2, LX/1Ri;

    check-cast p3, LX/1Qi;

    .line 586923
    iget-object v6, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptPartDefinition;->b:Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;

    new-instance v0, LX/24H;

    iget-object v1, p2, LX/1Ri;->c:LX/AkL;

    iget-object v2, p2, LX/1Ri;->c:LX/AkL;

    invoke-interface {v2}, LX/AkL;->g()LX/AkM;

    move-result-object v2

    iget-object v3, p2, LX/1Ri;->a:LX/1RN;

    iget-object v4, p2, LX/1Ri;->b:LX/0jW;

    iget-object v5, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptPartDefinition;->c:LX/24B;

    iget-object v7, p2, LX/1Ri;->a:LX/1RN;

    invoke-virtual {v5, v7}, LX/24B;->b(LX/1RN;)Z

    move-result v5

    invoke-direct/range {v0 .. v5}, LX/24H;-><init>(LX/AkL;LX/AkM;LX/1RN;LX/0jW;Z)V

    invoke-interface {p1, v6, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 586924
    iget-object v0, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptPartDefinition;->c:LX/24B;

    iget-object v2, p2, LX/1Ri;->a:LX/1RN;

    iget-object v3, p2, LX/1Ri;->b:LX/0jW;

    const-class v4, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptPartDefinition;

    iget-object v1, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptPartDefinition;->d:LX/AzX;

    invoke-virtual {p3}, LX/1Qj;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v1, v5}, LX/AzX;->a(Landroid/content/Context;)LX/AzW;

    move-result-object v5

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/24B;->a(LX/1aD;LX/1RN;LX/0jW;Ljava/lang/Class;LX/24J;)V

    .line 586925
    iget-object v0, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptPartDefinition;->e:LX/AzS;

    iget-object v1, p2, LX/1Ri;->a:LX/1RN;

    invoke-virtual {v0, v1}, LX/AzS;->a(LX/1RN;)LX/AzR;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptPartDefinition;->f:LX/AzR;

    .line 586926
    iget-object v0, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptPartDefinition;->f:LX/AzR;

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x169a445c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 586916
    check-cast p1, LX/1Ri;

    check-cast p4, LX/AkC;

    .line 586917
    invoke-virtual {p4}, LX/AkC;->getV2AttachmentView()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptView;

    .line 586918
    iget-object v2, p1, LX/1Ri;->a:LX/1RN;

    invoke-virtual {v1, v2}, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptView;->a(LX/1RN;)V

    .line 586919
    invoke-virtual {p4}, LX/AkC;->getFlyoutView()Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptPartDefinition;->f:LX/AzR;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 586920
    invoke-virtual {p4}, LX/AkC;->getAttachmentInsertPoint()Landroid/widget/FrameLayout;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 586921
    const/16 v1, 0x1f

    const v2, 0x4715fac3

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 586904
    check-cast p1, LX/1Ri;

    const/4 v1, 0x0

    .line 586905
    iget-object v0, p1, LX/1Ri;->a:LX/1RN;

    invoke-static {v0}, LX/1RN;->a(LX/1RN;)LX/1kK;

    move-result-object v0

    .line 586906
    instance-of v2, v0, LX/1kW;

    if-nez v2, :cond_0

    move v0, v1

    .line 586907
    :goto_0
    return v0

    .line 586908
    :cond_0
    check-cast v0, LX/1kW;

    .line 586909
    iget-object v2, v0, LX/1kW;->a:Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-object v0, v2

    .line 586910
    invoke-virtual {v0}, Lcom/facebook/productionprompts/model/ProductionPrompt;->A()LX/0Px;

    move-result-object v0

    .line 586911
    if-eqz v0, :cond_1

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 586912
    check-cast p4, LX/AkC;

    .line 586913
    invoke-virtual {p4}, LX/AkC;->getFlyoutView()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 586914
    invoke-virtual {p4}, LX/AkC;->getAttachmentInsertPoint()Landroid/widget/FrameLayout;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 586915
    return-void
.end method
