.class public Lcom/facebook/friendsharing/meme/prompt/v3/MemePromptSmallPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/1Ri;",
        "LX/AxH;",
        "TE;",
        "LX/BMR;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static g:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/productionprompts/common/v3/V3PromptsDraweeHelper;

.field private final c:Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;

.field private final d:Lcom/facebook/feed/inlinecomposer/multirow/common/PromptActionHandlePartDefinition;

.field private final e:LX/B5l;

.field private final f:LX/0ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 587179
    new-instance v0, LX/3Ur;

    invoke-direct {v0}, LX/3Ur;-><init>()V

    sput-object v0, Lcom/facebook/friendsharing/meme/prompt/v3/MemePromptSmallPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/B5l;Lcom/facebook/feed/inlinecomposer/multirow/common/PromptActionHandlePartDefinition;Lcom/facebook/productionprompts/common/v3/V3PromptsDraweeHelper;Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 587180
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 587181
    iput-object p1, p0, Lcom/facebook/friendsharing/meme/prompt/v3/MemePromptSmallPartDefinition;->e:LX/B5l;

    .line 587182
    iput-object p2, p0, Lcom/facebook/friendsharing/meme/prompt/v3/MemePromptSmallPartDefinition;->d:Lcom/facebook/feed/inlinecomposer/multirow/common/PromptActionHandlePartDefinition;

    .line 587183
    iput-object p3, p0, Lcom/facebook/friendsharing/meme/prompt/v3/MemePromptSmallPartDefinition;->b:Lcom/facebook/productionprompts/common/v3/V3PromptsDraweeHelper;

    .line 587184
    iput-object p4, p0, Lcom/facebook/friendsharing/meme/prompt/v3/MemePromptSmallPartDefinition;->c:Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;

    .line 587185
    iput-object p5, p0, Lcom/facebook/friendsharing/meme/prompt/v3/MemePromptSmallPartDefinition;->f:LX/0ad;

    .line 587186
    return-void
.end method

.method private a(Landroid/net/Uri;Landroid/content/Context;)LX/Alv;
    .locals 2

    .prologue
    .line 587187
    iget-object v0, p0, Lcom/facebook/friendsharing/meme/prompt/v3/MemePromptSmallPartDefinition;->b:Lcom/facebook/productionprompts/common/v3/V3PromptsDraweeHelper;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/productionprompts/common/v3/V3PromptsDraweeHelper;->a(Landroid/net/Uri;Landroid/content/Context;)LX/1aX;

    move-result-object v0

    .line 587188
    new-instance v1, LX/AxG;

    invoke-direct {v1, v0}, LX/AxG;-><init>(LX/1aX;)V

    return-object v1
.end method

.method public static a(LX/0QB;)Lcom/facebook/friendsharing/meme/prompt/v3/MemePromptSmallPartDefinition;
    .locals 9

    .prologue
    .line 587189
    const-class v1, Lcom/facebook/friendsharing/meme/prompt/v3/MemePromptSmallPartDefinition;

    monitor-enter v1

    .line 587190
    :try_start_0
    sget-object v0, Lcom/facebook/friendsharing/meme/prompt/v3/MemePromptSmallPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 587191
    sput-object v2, Lcom/facebook/friendsharing/meme/prompt/v3/MemePromptSmallPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 587192
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 587193
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 587194
    new-instance v3, Lcom/facebook/friendsharing/meme/prompt/v3/MemePromptSmallPartDefinition;

    invoke-static {v0}, LX/B5l;->b(LX/0QB;)LX/B5l;

    move-result-object v4

    check-cast v4, LX/B5l;

    invoke-static {v0}, Lcom/facebook/feed/inlinecomposer/multirow/common/PromptActionHandlePartDefinition;->a(LX/0QB;)Lcom/facebook/feed/inlinecomposer/multirow/common/PromptActionHandlePartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/inlinecomposer/multirow/common/PromptActionHandlePartDefinition;

    invoke-static {v0}, Lcom/facebook/productionprompts/common/v3/V3PromptsDraweeHelper;->b(LX/0QB;)Lcom/facebook/productionprompts/common/v3/V3PromptsDraweeHelper;

    move-result-object v6

    check-cast v6, Lcom/facebook/productionprompts/common/v3/V3PromptsDraweeHelper;

    invoke-static {v0}, Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/friendsharing/meme/prompt/v3/MemePromptSmallPartDefinition;-><init>(LX/B5l;Lcom/facebook/feed/inlinecomposer/multirow/common/PromptActionHandlePartDefinition;Lcom/facebook/productionprompts/common/v3/V3PromptsDraweeHelper;Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;LX/0ad;)V

    .line 587195
    move-object v0, v3

    .line 587196
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 587197
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/friendsharing/meme/prompt/v3/MemePromptSmallPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 587198
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 587199
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/BMR;",
            ">;"
        }
    .end annotation

    .prologue
    .line 587200
    sget-object v0, Lcom/facebook/friendsharing/meme/prompt/v3/MemePromptSmallPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 587201
    check-cast p2, LX/1Ri;

    check-cast p3, LX/1Pn;

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 587202
    iget-object v0, p0, Lcom/facebook/friendsharing/meme/prompt/v3/MemePromptSmallPartDefinition;->c:Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;

    new-instance v1, LX/Ak0;

    invoke-direct {v1, p2, v4}, LX/Ak0;-><init>(LX/1Ri;Z)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 587203
    iget-object v0, p0, Lcom/facebook/friendsharing/meme/prompt/v3/MemePromptSmallPartDefinition;->d:Lcom/facebook/feed/inlinecomposer/multirow/common/PromptActionHandlePartDefinition;

    iget-object v1, p0, Lcom/facebook/friendsharing/meme/prompt/v3/MemePromptSmallPartDefinition;->e:LX/B5l;

    iget-object v2, p2, LX/1Ri;->a:LX/1RN;

    invoke-virtual {v1, v2}, LX/B5l;->a(LX/1RN;)LX/B5n;

    move-result-object v1

    invoke-virtual {v1}, LX/B5n;->a()LX/B5p;

    move-result-object v1

    invoke-static {v1, p2}, LX/Ak3;->a(LX/B5p;LX/1Ri;)LX/Ak3;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 587204
    iget-object v0, p2, LX/1Ri;->a:LX/1RN;

    iget-object v0, v0, LX/1RN;->a:LX/1kK;

    check-cast v0, LX/1kW;

    .line 587205
    iget-object v1, v0, LX/1kW;->a:Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-object v0, v1

    .line 587206
    invoke-virtual {v0}, Lcom/facebook/productionprompts/model/ProductionPrompt;->z()Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel;->c()LX/0Px;

    move-result-object v1

    .line 587207
    invoke-virtual {v1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lcom/facebook/friendsharing/meme/prompt/v3/MemePromptSmallPartDefinition;->a(Landroid/net/Uri;Landroid/content/Context;)LX/Alv;

    move-result-object v2

    .line 587208
    const/4 v0, 0x0

    .line 587209
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v3

    if-le v3, v5, :cond_0

    .line 587210
    invoke-virtual {v1, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/facebook/friendsharing/meme/prompt/v3/MemePromptSmallPartDefinition;->a(Landroid/net/Uri;Landroid/content/Context;)LX/Alv;

    move-result-object v0

    .line 587211
    :cond_0
    new-instance v1, LX/AxH;

    invoke-direct {v1, v2, v0}, LX/AxH;-><init>(LX/Alv;LX/Alv;)V

    return-object v1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x5bdb846e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 587212
    check-cast p2, LX/AxH;

    check-cast p4, LX/BMR;

    .line 587213
    iget-object v1, p2, LX/AxH;->a:LX/Alv;

    iget-object v2, p2, LX/AxH;->b:LX/Alv;

    invoke-virtual {p4, v1, v2}, LX/BMR;->a(LX/Alv;LX/Alv;)V

    .line 587214
    const/16 v1, 0x1f

    const v2, -0x2b6976e7

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 587215
    check-cast p1, LX/1Ri;

    const/4 v1, 0x0

    .line 587216
    if-eqz p1, :cond_0

    iget-object v0, p1, LX/1Ri;->a:LX/1RN;

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/1Ri;->a:LX/1RN;

    iget-object v0, v0, LX/1RN;->a:LX/1kK;

    instance-of v0, v0, LX/1kW;

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/1Ri;->a:LX/1RN;

    iget-object v0, v0, LX/1RN;->a:LX/1kK;

    check-cast v0, LX/1kW;

    .line 587217
    iget-object v2, v0, LX/1kW;->a:Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-object v0, v2

    .line 587218
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/friendsharing/meme/prompt/v3/MemePromptSmallPartDefinition;->f:LX/0ad;

    sget-short v2, LX/Awu;->a:S

    invoke-interface {v0, v2, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 587219
    :goto_0
    return v0

    .line 587220
    :cond_1
    iget-object v0, p1, LX/1Ri;->a:LX/1RN;

    iget-object v0, v0, LX/1RN;->a:LX/1kK;

    check-cast v0, LX/1kW;

    .line 587221
    iget-object v2, v0, LX/1kW;->a:Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-object v0, v2

    .line 587222
    invoke-virtual {v0}, Lcom/facebook/productionprompts/model/ProductionPrompt;->z()Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-static {p1}, LX/BMT;->b(LX/1Ri;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method
