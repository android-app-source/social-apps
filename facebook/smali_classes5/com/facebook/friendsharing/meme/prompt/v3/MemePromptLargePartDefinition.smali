.class public Lcom/facebook/friendsharing/meme/prompt/v3/MemePromptLargePartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/1Ri;",
        "LX/0Px",
        "<",
        "Ljava/lang/String;",
        ">;TE;",
        "LX/AxI;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static f:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;

.field private final c:LX/24B;

.field private final d:LX/Ax1;

.field private final e:LX/0ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 587354
    new-instance v0, LX/3Uu;

    invoke-direct {v0}, LX/3Uu;-><init>()V

    sput-object v0, Lcom/facebook/friendsharing/meme/prompt/v3/MemePromptLargePartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;LX/24B;LX/Ax1;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 587348
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 587349
    iput-object p1, p0, Lcom/facebook/friendsharing/meme/prompt/v3/MemePromptLargePartDefinition;->b:Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;

    .line 587350
    iput-object p2, p0, Lcom/facebook/friendsharing/meme/prompt/v3/MemePromptLargePartDefinition;->c:LX/24B;

    .line 587351
    iput-object p3, p0, Lcom/facebook/friendsharing/meme/prompt/v3/MemePromptLargePartDefinition;->d:LX/Ax1;

    .line 587352
    iput-object p4, p0, Lcom/facebook/friendsharing/meme/prompt/v3/MemePromptLargePartDefinition;->e:LX/0ad;

    .line 587353
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/friendsharing/meme/prompt/v3/MemePromptLargePartDefinition;
    .locals 7

    .prologue
    .line 587337
    const-class v1, Lcom/facebook/friendsharing/meme/prompt/v3/MemePromptLargePartDefinition;

    monitor-enter v1

    .line 587338
    :try_start_0
    sget-object v0, Lcom/facebook/friendsharing/meme/prompt/v3/MemePromptLargePartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 587339
    sput-object v2, Lcom/facebook/friendsharing/meme/prompt/v3/MemePromptLargePartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 587340
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 587341
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 587342
    new-instance p0, Lcom/facebook/friendsharing/meme/prompt/v3/MemePromptLargePartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;

    invoke-static {v0}, LX/24B;->b(LX/0QB;)LX/24B;

    move-result-object v4

    check-cast v4, LX/24B;

    const-class v5, LX/Ax1;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/Ax1;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v6

    check-cast v6, LX/0ad;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/friendsharing/meme/prompt/v3/MemePromptLargePartDefinition;-><init>(Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;LX/24B;LX/Ax1;LX/0ad;)V

    .line 587343
    move-object v0, p0

    .line 587344
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 587345
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/friendsharing/meme/prompt/v3/MemePromptLargePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 587346
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 587347
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/AxI;",
            ">;"
        }
    .end annotation

    .prologue
    .line 587310
    sget-object v0, Lcom/facebook/friendsharing/meme/prompt/v3/MemePromptLargePartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 587331
    check-cast p2, LX/1Ri;

    check-cast p3, LX/1Pn;

    .line 587332
    iget-object v0, p0, Lcom/facebook/friendsharing/meme/prompt/v3/MemePromptLargePartDefinition;->c:LX/24B;

    iget-object v2, p2, LX/1Ri;->a:LX/1RN;

    iget-object v3, p2, LX/1Ri;->b:LX/0jW;

    const-class v4, Lcom/facebook/friendsharing/meme/prompt/v3/MemePromptLargePartDefinition;

    iget-object v1, p0, Lcom/facebook/friendsharing/meme/prompt/v3/MemePromptLargePartDefinition;->d:LX/Ax1;

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v1, v5}, LX/Ax1;->a(Landroid/content/Context;)LX/Ax0;

    move-result-object v5

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/24B;->a(LX/1aD;LX/1RN;LX/0jW;Ljava/lang/Class;LX/24J;)V

    .line 587333
    iget-object v0, p0, Lcom/facebook/friendsharing/meme/prompt/v3/MemePromptLargePartDefinition;->b:Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;

    invoke-static {p2}, LX/Ak0;->a(LX/1Ri;)LX/Ak0;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 587334
    iget-object v0, p2, LX/1Ri;->a:LX/1RN;

    iget-object v0, v0, LX/1RN;->a:LX/1kK;

    check-cast v0, LX/1kW;

    .line 587335
    iget-object v1, v0, LX/1kW;->a:Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-object v0, v1

    .line 587336
    invoke-virtual {v0}, Lcom/facebook/productionprompts/model/ProductionPrompt;->z()Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel;->c()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x3635bb1b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 587326
    check-cast p1, LX/1Ri;

    check-cast p2, LX/0Px;

    check-cast p3, LX/1Pn;

    check-cast p4, LX/AxI;

    .line 587327
    iget-object v1, p4, LX/AxI;->l:Lcom/facebook/friendsharing/meme/prompt/MemePromptView;

    move-object v1, v1

    .line 587328
    iget-object v2, p1, LX/1Ri;->a:LX/1RN;

    invoke-virtual {v1, v2}, Lcom/facebook/friendsharing/meme/prompt/MemePromptView;->a(LX/1RN;)V

    .line 587329
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;->a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V

    .line 587330
    const/16 v1, 0x1f

    const v2, -0x63a214eb

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 587316
    check-cast p1, LX/1Ri;

    const/4 v1, 0x0

    .line 587317
    if-eqz p1, :cond_0

    iget-object v0, p1, LX/1Ri;->a:LX/1RN;

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/1Ri;->a:LX/1RN;

    iget-object v0, v0, LX/1RN;->a:LX/1kK;

    instance-of v0, v0, LX/1kW;

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/1Ri;->a:LX/1RN;

    iget-object v0, v0, LX/1RN;->a:LX/1kK;

    check-cast v0, LX/1kW;

    .line 587318
    iget-object v2, v0, LX/1kW;->a:Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-object v0, v2

    .line 587319
    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 587320
    :goto_0
    return v0

    .line 587321
    :cond_1
    iget-object v0, p0, Lcom/facebook/friendsharing/meme/prompt/v3/MemePromptLargePartDefinition;->e:LX/0ad;

    sget-short v2, LX/Awu;->a:S

    invoke-interface {v0, v2, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 587322
    goto :goto_0

    .line 587323
    :cond_2
    iget-object v0, p1, LX/1Ri;->a:LX/1RN;

    iget-object v0, v0, LX/1RN;->a:LX/1kK;

    check-cast v0, LX/1kW;

    .line 587324
    iget-object v2, v0, LX/1kW;->a:Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-object v0, v2

    .line 587325
    invoke-virtual {v0}, Lcom/facebook/productionprompts/model/ProductionPrompt;->z()Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-static {p1}, LX/BMT;->a(LX/1Ri;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 587311
    check-cast p1, LX/1Ri;

    check-cast p2, LX/0Px;

    check-cast p3, LX/1Pn;

    check-cast p4, LX/AxI;

    .line 587312
    iget-object v0, p4, LX/AxI;->l:Lcom/facebook/friendsharing/meme/prompt/MemePromptView;

    move-object v0, v0

    .line 587313
    invoke-virtual {v0}, Lcom/facebook/friendsharing/meme/prompt/MemePromptView;->k()V

    .line 587314
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;->b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V

    .line 587315
    return-void
.end method
