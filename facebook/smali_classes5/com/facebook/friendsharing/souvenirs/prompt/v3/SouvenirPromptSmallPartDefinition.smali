.class public Lcom/facebook/friendsharing/souvenirs/prompt/v3/SouvenirPromptSmallPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Qa;",
        ":",
        "LX/1Pn;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/1Ri;",
        "LX/Ayc;",
        "TE;",
        "LX/BMS;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field public static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static g:LX/0Xm;


# instance fields
.field private final c:LX/AyS;

.field public final d:LX/AzD;

.field public final e:LX/Axw;

.field private final f:Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 587141
    new-instance v0, LX/3Uq;

    invoke-direct {v0}, LX/3Uq;-><init>()V

    sput-object v0, Lcom/facebook/friendsharing/souvenirs/prompt/v3/SouvenirPromptSmallPartDefinition;->a:LX/1Cz;

    .line 587142
    const-class v0, Lcom/facebook/friendsharing/souvenirs/prompt/v3/SouvenirPromptSmallPartDefinition;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/friendsharing/souvenirs/prompt/v3/SouvenirPromptSmallPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/AyS;LX/AzD;LX/Axw;Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 587143
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 587144
    iput-object p1, p0, Lcom/facebook/friendsharing/souvenirs/prompt/v3/SouvenirPromptSmallPartDefinition;->c:LX/AyS;

    .line 587145
    iput-object p2, p0, Lcom/facebook/friendsharing/souvenirs/prompt/v3/SouvenirPromptSmallPartDefinition;->d:LX/AzD;

    .line 587146
    iput-object p3, p0, Lcom/facebook/friendsharing/souvenirs/prompt/v3/SouvenirPromptSmallPartDefinition;->e:LX/Axw;

    .line 587147
    iput-object p4, p0, Lcom/facebook/friendsharing/souvenirs/prompt/v3/SouvenirPromptSmallPartDefinition;->f:Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;

    .line 587148
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/friendsharing/souvenirs/prompt/v3/SouvenirPromptSmallPartDefinition;
    .locals 7

    .prologue
    .line 587149
    const-class v1, Lcom/facebook/friendsharing/souvenirs/prompt/v3/SouvenirPromptSmallPartDefinition;

    monitor-enter v1

    .line 587150
    :try_start_0
    sget-object v0, Lcom/facebook/friendsharing/souvenirs/prompt/v3/SouvenirPromptSmallPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 587151
    sput-object v2, Lcom/facebook/friendsharing/souvenirs/prompt/v3/SouvenirPromptSmallPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 587152
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 587153
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 587154
    new-instance p0, Lcom/facebook/friendsharing/souvenirs/prompt/v3/SouvenirPromptSmallPartDefinition;

    invoke-static {v0}, LX/AyS;->b(LX/0QB;)LX/AyS;

    move-result-object v3

    check-cast v3, LX/AyS;

    invoke-static {v0}, LX/AzD;->b(LX/0QB;)LX/AzD;

    move-result-object v4

    check-cast v4, LX/AzD;

    invoke-static {v0}, LX/Axw;->a(LX/0QB;)LX/Axw;

    move-result-object v5

    check-cast v5, LX/Axw;

    invoke-static {v0}, Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/friendsharing/souvenirs/prompt/v3/SouvenirPromptSmallPartDefinition;-><init>(LX/AyS;LX/AzD;LX/Axw;Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;)V

    .line 587155
    move-object v0, p0

    .line 587156
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 587157
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/friendsharing/souvenirs/prompt/v3/SouvenirPromptSmallPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 587158
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 587159
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/BMS;",
            ">;"
        }
    .end annotation

    .prologue
    .line 587160
    sget-object v0, Lcom/facebook/friendsharing/souvenirs/prompt/v3/SouvenirPromptSmallPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 587161
    check-cast p2, LX/1Ri;

    check-cast p3, LX/1Ps;

    .line 587162
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/prompt/v3/SouvenirPromptSmallPartDefinition;->f:Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;

    invoke-static {p2}, LX/Ak0;->a(LX/1Ri;)LX/Ak0;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 587163
    iget-object v0, p2, LX/1Ri;->a:LX/1RN;

    iget-object v0, v0, LX/1RN;->a:LX/1kK;

    check-cast v0, LX/Ayb;

    .line 587164
    new-instance v1, LX/Ayc;

    invoke-virtual {v0}, LX/Ayb;->e()Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/friendsharing/souvenirs/prompt/v3/SouvenirPromptSmallPartDefinition;->c:LX/AyS;

    invoke-virtual {v3, v0}, LX/AyS;->a(LX/Ayb;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/friendsharing/souvenirs/prompt/v3/SouvenirPromptSmallPartDefinition;->c:LX/AyS;

    move-object v0, p3

    check-cast v0, LX/1Pn;

    invoke-interface {v0}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    check-cast p3, LX/1Pn;

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f082766

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, LX/AyS;->a(Landroid/content/res/Resources;Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    invoke-direct {v1, v2, v3, v0}, LX/Ayc;-><init>(Landroid/net/Uri;Ljava/lang/String;Landroid/text/SpannableStringBuilder;)V

    return-object v1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x6c3d4f03

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 587165
    check-cast p1, LX/1Ri;

    check-cast p2, LX/Ayc;

    check-cast p4, LX/BMS;

    .line 587166
    iget-object v1, p2, LX/Ayc;->a:Landroid/net/Uri;

    sget-object v2, Lcom/facebook/friendsharing/souvenirs/prompt/v3/SouvenirPromptSmallPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p4, v1, v2}, LX/BMS;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 587167
    iget-object v1, p1, LX/1Ri;->a:LX/1RN;

    invoke-static {v1}, LX/1RN;->a(LX/1RN;)LX/1kK;

    move-result-object v2

    .line 587168
    instance-of v1, v2, LX/Ayb;

    const-string p3, "Didn\'t get a souvenir prompt object"

    invoke-static {v1, p3}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    move-object v1, v2

    .line 587169
    check-cast v1, LX/Ayb;

    .line 587170
    iget-boolean p3, p2, LX/Ayc;->d:Z

    if-nez p3, :cond_0

    .line 587171
    iget-object p3, p0, Lcom/facebook/friendsharing/souvenirs/prompt/v3/SouvenirPromptSmallPartDefinition;->d:LX/AzD;

    invoke-virtual {p3, v1}, LX/AzD;->b(LX/Ayb;)V

    .line 587172
    iget-object v1, p0, Lcom/facebook/friendsharing/souvenirs/prompt/v3/SouvenirPromptSmallPartDefinition;->e:LX/Axw;

    invoke-interface {v2}, LX/1kK;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/Axw;->a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 587173
    const/4 v1, 0x1

    iput-boolean v1, p2, LX/Ayc;->d:Z

    .line 587174
    :cond_0
    const/16 v1, 0x1f

    const v2, -0x7edccf7c    # -2.997318E-38f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 587175
    check-cast p1, LX/1Ri;

    .line 587176
    if-eqz p1, :cond_0

    iget-object v0, p1, LX/1Ri;->a:LX/1RN;

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/1Ri;->a:LX/1RN;

    iget-object v0, v0, LX/1RN;->a:LX/1kK;

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/1Ri;->a:LX/1RN;

    iget-object v0, v0, LX/1RN;->a:LX/1kK;

    instance-of v0, v0, LX/Ayb;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
