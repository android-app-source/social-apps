.class public Lcom/facebook/friendsharing/souvenirs/prompt/SouvenirPromptsPromptPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/1Ri;",
        "LX/AyV;",
        "LX/1Qi;",
        "LX/AkC;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static i:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;

.field private final c:LX/AyS;

.field public final d:LX/AzD;

.field public final e:LX/Axw;

.field public final f:LX/BMP;

.field public final g:LX/B5l;

.field private final h:LX/24B;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 586852
    new-instance v0, LX/3Ui;

    invoke-direct {v0}, LX/3Ui;-><init>()V

    sput-object v0, Lcom/facebook/friendsharing/souvenirs/prompt/SouvenirPromptsPromptPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;LX/AyS;LX/AzD;LX/Axw;LX/BMP;LX/B5l;LX/24B;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 586843
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 586844
    iput-object p1, p0, Lcom/facebook/friendsharing/souvenirs/prompt/SouvenirPromptsPromptPartDefinition;->b:Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;

    .line 586845
    iput-object p2, p0, Lcom/facebook/friendsharing/souvenirs/prompt/SouvenirPromptsPromptPartDefinition;->c:LX/AyS;

    .line 586846
    iput-object p3, p0, Lcom/facebook/friendsharing/souvenirs/prompt/SouvenirPromptsPromptPartDefinition;->d:LX/AzD;

    .line 586847
    iput-object p4, p0, Lcom/facebook/friendsharing/souvenirs/prompt/SouvenirPromptsPromptPartDefinition;->e:LX/Axw;

    .line 586848
    iput-object p5, p0, Lcom/facebook/friendsharing/souvenirs/prompt/SouvenirPromptsPromptPartDefinition;->f:LX/BMP;

    .line 586849
    iput-object p6, p0, Lcom/facebook/friendsharing/souvenirs/prompt/SouvenirPromptsPromptPartDefinition;->g:LX/B5l;

    .line 586850
    iput-object p7, p0, Lcom/facebook/friendsharing/souvenirs/prompt/SouvenirPromptsPromptPartDefinition;->h:LX/24B;

    .line 586851
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/friendsharing/souvenirs/prompt/SouvenirPromptsPromptPartDefinition;
    .locals 11

    .prologue
    .line 586832
    const-class v1, Lcom/facebook/friendsharing/souvenirs/prompt/SouvenirPromptsPromptPartDefinition;

    monitor-enter v1

    .line 586833
    :try_start_0
    sget-object v0, Lcom/facebook/friendsharing/souvenirs/prompt/SouvenirPromptsPromptPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 586834
    sput-object v2, Lcom/facebook/friendsharing/souvenirs/prompt/SouvenirPromptsPromptPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 586835
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 586836
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 586837
    new-instance v3, Lcom/facebook/friendsharing/souvenirs/prompt/SouvenirPromptsPromptPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;

    invoke-static {v0}, LX/AyS;->b(LX/0QB;)LX/AyS;

    move-result-object v5

    check-cast v5, LX/AyS;

    invoke-static {v0}, LX/AzD;->b(LX/0QB;)LX/AzD;

    move-result-object v6

    check-cast v6, LX/AzD;

    invoke-static {v0}, LX/Axw;->a(LX/0QB;)LX/Axw;

    move-result-object v7

    check-cast v7, LX/Axw;

    invoke-static {v0}, LX/BMP;->b(LX/0QB;)LX/BMP;

    move-result-object v8

    check-cast v8, LX/BMP;

    invoke-static {v0}, LX/B5l;->b(LX/0QB;)LX/B5l;

    move-result-object v9

    check-cast v9, LX/B5l;

    invoke-static {v0}, LX/24B;->b(LX/0QB;)LX/24B;

    move-result-object v10

    check-cast v10, LX/24B;

    invoke-direct/range {v3 .. v10}, Lcom/facebook/friendsharing/souvenirs/prompt/SouvenirPromptsPromptPartDefinition;-><init>(Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;LX/AyS;LX/AzD;LX/Axw;LX/BMP;LX/B5l;LX/24B;)V

    .line 586838
    move-object v0, v3

    .line 586839
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 586840
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/friendsharing/souvenirs/prompt/SouvenirPromptsPromptPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 586841
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 586842
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 586831
    sget-object v0, Lcom/facebook/friendsharing/souvenirs/prompt/SouvenirPromptsPromptPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 586807
    check-cast p2, LX/1Ri;

    check-cast p3, LX/1Qi;

    .line 586808
    iget-object v6, p0, Lcom/facebook/friendsharing/souvenirs/prompt/SouvenirPromptsPromptPartDefinition;->b:Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutPartDefinition;

    new-instance v0, LX/24H;

    iget-object v1, p2, LX/1Ri;->c:LX/AkL;

    iget-object v2, p2, LX/1Ri;->c:LX/AkL;

    invoke-interface {v2}, LX/AkL;->g()LX/AkM;

    move-result-object v2

    iget-object v3, p2, LX/1Ri;->a:LX/1RN;

    iget-object v4, p2, LX/1Ri;->b:LX/0jW;

    iget-object v5, p0, Lcom/facebook/friendsharing/souvenirs/prompt/SouvenirPromptsPromptPartDefinition;->h:LX/24B;

    iget-object v7, p2, LX/1Ri;->a:LX/1RN;

    invoke-virtual {v5, v7}, LX/24B;->b(LX/1RN;)Z

    move-result v5

    invoke-direct/range {v0 .. v5}, LX/24H;-><init>(LX/AkL;LX/AkM;LX/1RN;LX/0jW;Z)V

    invoke-interface {p1, v6, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 586809
    iget-object v0, p2, LX/1Ri;->a:LX/1RN;

    iget-object v0, v0, LX/1RN;->a:LX/1kK;

    check-cast v0, LX/Ayb;

    .line 586810
    new-instance v1, LX/AyV;

    invoke-virtual {v0}, LX/Ayb;->e()Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/friendsharing/souvenirs/prompt/SouvenirPromptsPromptPartDefinition;->c:LX/AyS;

    invoke-virtual {v3, v0}, LX/AyS;->a(LX/Ayb;)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/facebook/friendsharing/souvenirs/prompt/SouvenirPromptsPromptPartDefinition;->c:LX/AyS;

    invoke-virtual {p3}, LX/1Qj;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {p3}, LX/1Qj;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f082766

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/AyS;->a(Landroid/content/res/Resources;Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v3

    new-instance v4, LX/AyU;

    invoke-direct {v4, p0, p2}, LX/AyU;-><init>(Lcom/facebook/friendsharing/souvenirs/prompt/SouvenirPromptsPromptPartDefinition;LX/1Ri;)V

    invoke-direct {v1, v2, v0, v3, v4}, LX/AyV;-><init>(Landroid/net/Uri;Ljava/lang/String;Landroid/text/SpannableStringBuilder;Landroid/view/View$OnClickListener;)V

    return-object v1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 5

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x19912af4    # 1.5009997E-23f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 586818
    check-cast p1, LX/1Ri;

    check-cast p2, LX/AyV;

    check-cast p4, LX/AkC;

    .line 586819
    invoke-virtual {p4}, LX/AkC;->getV2AttachmentView()Landroid/view/View;

    .line 586820
    invoke-virtual {p4}, LX/AkC;->getV2AttachmentView()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/friendsharing/souvenirs/prompt/SouvenirPromptAttachment;

    iget-object v2, p2, LX/AyV;->a:Landroid/net/Uri;

    iget-object v4, p2, LX/AyV;->b:Ljava/lang/String;

    iget-object p3, p2, LX/AyV;->c:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v1, v2, v4, p3}, Lcom/facebook/friendsharing/souvenirs/prompt/SouvenirPromptAttachment;->a(Landroid/net/Uri;Ljava/lang/String;Landroid/text/SpannableStringBuilder;)V

    .line 586821
    invoke-virtual {p4}, LX/AkC;->getFlyoutView()Landroid/view/View;

    move-result-object v1

    iget-object v2, p2, LX/AyV;->d:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 586822
    invoke-virtual {p4}, LX/AkC;->getAttachmentInsertPoint()Landroid/widget/FrameLayout;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 586823
    iget-object v1, p1, LX/1Ri;->a:LX/1RN;

    invoke-static {v1}, LX/1RN;->a(LX/1RN;)LX/1kK;

    move-result-object v2

    .line 586824
    instance-of v1, v2, LX/Ayb;

    const-string v4, "Didn\'t get a souvenir prompt object"

    invoke-static {v1, v4}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    move-object v1, v2

    .line 586825
    check-cast v1, LX/Ayb;

    .line 586826
    iget-boolean v4, p2, LX/AyV;->e:Z

    if-nez v4, :cond_0

    .line 586827
    iget-object v4, p0, Lcom/facebook/friendsharing/souvenirs/prompt/SouvenirPromptsPromptPartDefinition;->d:LX/AzD;

    invoke-virtual {v4, v1}, LX/AzD;->b(LX/Ayb;)V

    .line 586828
    iget-object v1, p0, Lcom/facebook/friendsharing/souvenirs/prompt/SouvenirPromptsPromptPartDefinition;->e:LX/Axw;

    invoke-interface {v2}, LX/1kK;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/Axw;->a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 586829
    const/4 v1, 0x1

    iput-boolean v1, p2, LX/AyV;->e:Z

    .line 586830
    :cond_0
    const/16 v1, 0x1f

    const v2, 0x81ccead

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 586815
    check-cast p1, LX/1Ri;

    .line 586816
    iget-object v0, p1, LX/1Ri;->a:LX/1RN;

    invoke-static {v0}, LX/1RN;->a(LX/1RN;)LX/1kK;

    move-result-object v0

    .line 586817
    instance-of v0, v0, LX/Ayb;

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 586811
    check-cast p4, LX/AkC;

    .line 586812
    invoke-virtual {p4}, LX/AkC;->getFlyoutView()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 586813
    invoke-virtual {p4}, LX/AkC;->getAttachmentInsertPoint()Landroid/widget/FrameLayout;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 586814
    return-void
.end method
