.class public final Lcom/facebook/presence/PresenceAccuracyExpHandler$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/2a7;


# direct methods
.method public constructor <init>(LX/2a7;)V
    .locals 0

    .prologue
    .line 576471
    iput-object p1, p0, Lcom/facebook/presence/PresenceAccuracyExpHandler$1;->a:LX/2a7;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 11

    .prologue
    .line 576472
    iget-object v0, p0, Lcom/facebook/presence/PresenceAccuracyExpHandler$1;->a:LX/2a7;

    iget-object v0, v0, LX/2a7;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2CH;

    .line 576473
    iget-object v1, v0, LX/2CH;->y:Ljava/util/concurrent/ConcurrentMap;

    move-object v6, v1

    .line 576474
    new-instance v7, Lorg/json/JSONArray;

    invoke-direct {v7}, Lorg/json/JSONArray;-><init>()V

    .line 576475
    invoke-interface {v6}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 576476
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v6, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, LX/3hU;

    .line 576477
    if-eqz v4, :cond_0

    .line 576478
    :try_start_0
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserKey;

    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v0

    iget-boolean v1, v4, LX/3hU;->d:Z

    iget-wide v2, v4, LX/3hU;->e:J

    iget-wide v4, v4, LX/3hU;->g:J

    .line 576479
    new-instance v9, Lorg/json/JSONObject;

    invoke-direct {v9}, Lorg/json/JSONObject;-><init>()V

    .line 576480
    const-string v10, "u"

    invoke-virtual {v9, v10, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 576481
    const-string v10, "p"

    invoke-virtual {v9, v10, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 576482
    const-string v10, "l"

    invoke-virtual {v9, v10, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 576483
    const-string v10, "vc"

    invoke-virtual {v9, v10, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 576484
    move-object v9, v9

    .line 576485
    move-object v0, v9

    .line 576486
    invoke-virtual {v7, v0}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 576487
    :catch_0
    :cond_1
    :goto_1
    return-void

    .line 576488
    :cond_2
    invoke-virtual {v7}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/String;)[B

    move-result-object v1

    .line 576489
    sget-object v2, LX/2AL;->TP_FULL_LIST_RECEIVED:LX/2AL;

    iget-object v0, p0, Lcom/facebook/presence/PresenceAccuracyExpHandler$1;->a:LX/2a7;

    iget-object v0, v0, LX/2a7;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2CH;

    .line 576490
    iget-object v3, v0, LX/2CH;->K:LX/2AL;

    move-object v0, v3

    .line 576491
    invoke-virtual {v2, v0}, LX/2AL;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 576492
    iget-object v0, p0, Lcom/facebook/presence/PresenceAccuracyExpHandler$1;->a:LX/2a7;

    iget-object v0, v0, LX/2a7;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2gU;

    const-string v2, "/p_a_resp"

    sget-object v3, LX/2I2;->FIRE_AND_FORGET:LX/2I2;

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v1, v3, v4}, LX/2gU;->a(Ljava/lang/String;[BLX/2I2;LX/76H;)I

    goto :goto_1
.end method
