.class public final Lcom/facebook/pkg/event/PackageEventLogger$BackgroundRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final synthetic c:LX/2QO;


# direct methods
.method public constructor <init>(LX/2QO;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 572916
    iput-object p1, p0, Lcom/facebook/pkg/event/PackageEventLogger$BackgroundRunnable;->c:LX/2QO;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 572917
    iput-object p2, p0, Lcom/facebook/pkg/event/PackageEventLogger$BackgroundRunnable;->a:Ljava/lang/String;

    .line 572918
    iput-object p3, p0, Lcom/facebook/pkg/event/PackageEventLogger$BackgroundRunnable;->b:Ljava/lang/String;

    .line 572919
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 10

    .prologue
    .line 572920
    iget-object v0, p0, Lcom/facebook/pkg/event/PackageEventLogger$BackgroundRunnable;->c:LX/2QO;

    iget-object v0, v0, LX/2QO;->j:LX/0s6;

    iget-object v1, p0, Lcom/facebook/pkg/event/PackageEventLogger$BackgroundRunnable;->b:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/01H;->b(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 572921
    iget-object v1, p0, Lcom/facebook/pkg/event/PackageEventLogger$BackgroundRunnable;->a:Ljava/lang/String;

    const-string v2, "app_sibling_uninstalled"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 572922
    iget-object v1, p0, Lcom/facebook/pkg/event/PackageEventLogger$BackgroundRunnable;->c:LX/2QO;

    iget-object v1, v1, LX/2QO;->j:LX/0s6;

    iget-object v2, p0, Lcom/facebook/pkg/event/PackageEventLogger$BackgroundRunnable;->b:Ljava/lang/String;

    .line 572923
    :try_start_0
    iget-object v3, v1, LX/01H;->a:Landroid/content/pm/PackageManager;

    invoke-virtual {v3, v2}, Landroid/content/pm/PackageManager;->getInstallerPackageName(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 572924
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/facebook/pkg/event/PackageEventLogger$BackgroundRunnable;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/pkg/event/PackageEventLogger$BackgroundRunnable;->b:Ljava/lang/String;

    const/4 v3, 0x0

    const-wide/16 v8, 0x3e8

    .line 572925
    new-instance v5, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v5, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 572926
    const-string v4, "package_name"

    invoke-virtual {v5, v4, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 572927
    const-string v4, "installer_package_name"

    invoke-virtual {v5, v4, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 572928
    if-eqz v0, :cond_1

    .line 572929
    const-string v4, "build_number"

    iget v6, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-virtual {v5, v4, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 572930
    const-string v4, "install_unixtime"

    iget-wide v6, v0, Landroid/content/pm/PackageInfo;->firstInstallTime:J

    div-long/2addr v6, v8

    invoke-virtual {v5, v4, v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 572931
    const-string v4, "update_unixtime"

    iget-wide v6, v0, Landroid/content/pm/PackageInfo;->firstInstallTime:J

    div-long/2addr v6, v8

    invoke-virtual {v5, v4, v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 572932
    const-string v4, "version_number"

    iget-object v6, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-virtual {v5, v4, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 572933
    :cond_1
    iget-object v4, p0, Lcom/facebook/pkg/event/PackageEventLogger$BackgroundRunnable;->c:LX/2QO;

    iget-object v4, v4, LX/2QO;->k:LX/2F7;

    invoke-virtual {v4}, LX/2F7;->a()Ljava/util/Map;

    move-result-object v6

    .line 572934
    sget-object v4, LX/2QO;->g:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 572935
    invoke-interface {v6, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v5, v4, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_1

    .line 572936
    :cond_2
    move-object v0, v5

    .line 572937
    sget-boolean v1, LX/007;->i:Z

    move v1, v1

    .line 572938
    if-eqz v1, :cond_3

    .line 572939
    invoke-virtual {v0}, Lcom/facebook/analytics/HoneyAnalyticsEvent;->g()Ljava/lang/String;

    .line 572940
    :cond_3
    iget-object v1, p0, Lcom/facebook/pkg/event/PackageEventLogger$BackgroundRunnable;->c:LX/2QO;

    iget-object v1, v1, LX/2QO;->l:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 572941
    return-void

    :catch_0
    goto :goto_0
.end method
