.class public final Lcom/facebook/notifications/sync/NotificationsSyncHelper$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/auth/viewercontext/ViewerContext;

.field public final synthetic b:LX/3DO;

.field public final synthetic c:LX/2ub;

.field public final synthetic d:I

.field public final synthetic e:Ljava/lang/String;

.field public final synthetic f:Ljava/util/List;

.field public final synthetic g:LX/1rk;


# direct methods
.method public constructor <init>(LX/1rk;Lcom/facebook/auth/viewercontext/ViewerContext;LX/3DO;LX/2ub;ILjava/lang/String;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 581962
    iput-object p1, p0, Lcom/facebook/notifications/sync/NotificationsSyncHelper$1;->g:LX/1rk;

    iput-object p2, p0, Lcom/facebook/notifications/sync/NotificationsSyncHelper$1;->a:Lcom/facebook/auth/viewercontext/ViewerContext;

    iput-object p3, p0, Lcom/facebook/notifications/sync/NotificationsSyncHelper$1;->b:LX/3DO;

    iput-object p4, p0, Lcom/facebook/notifications/sync/NotificationsSyncHelper$1;->c:LX/2ub;

    iput p5, p0, Lcom/facebook/notifications/sync/NotificationsSyncHelper$1;->d:I

    iput-object p6, p0, Lcom/facebook/notifications/sync/NotificationsSyncHelper$1;->e:Ljava/lang/String;

    iput-object p7, p0, Lcom/facebook/notifications/sync/NotificationsSyncHelper$1;->f:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 13

    .prologue
    .line 581963
    iget-object v0, p0, Lcom/facebook/notifications/sync/NotificationsSyncHelper$1;->g:LX/1rk;

    iget-object v1, p0, Lcom/facebook/notifications/sync/NotificationsSyncHelper$1;->a:Lcom/facebook/auth/viewercontext/ViewerContext;

    iget-object v2, p0, Lcom/facebook/notifications/sync/NotificationsSyncHelper$1;->b:LX/3DO;

    iget-object v3, p0, Lcom/facebook/notifications/sync/NotificationsSyncHelper$1;->c:LX/2ub;

    iget v4, p0, Lcom/facebook/notifications/sync/NotificationsSyncHelper$1;->d:I

    iget-object v5, p0, Lcom/facebook/notifications/sync/NotificationsSyncHelper$1;->e:Ljava/lang/String;

    iget-object v6, p0, Lcom/facebook/notifications/sync/NotificationsSyncHelper$1;->f:Ljava/util/List;

    const/4 v12, 0x1

    .line 581964
    iget-object v7, v1, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v7, v7

    .line 581965
    invoke-static {v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v9

    .line 581966
    new-instance v7, LX/3DL;

    invoke-direct {v7}, LX/3DL;-><init>()V

    sget-object v8, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    .line 581967
    iput-object v8, v7, LX/3DL;->a:LX/0rS;

    .line 581968
    move-object v7, v7

    .line 581969
    iput-object v1, v7, LX/3DL;->f:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 581970
    move-object v8, v7

    .line 581971
    iget-object v7, v0, LX/1rk;->l:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/0dC;

    invoke-virtual {v7}, LX/0dC;->a()Ljava/lang/String;

    move-result-object v7

    .line 581972
    iput-object v7, v8, LX/3DL;->l:Ljava/lang/String;

    .line 581973
    move-object v7, v8

    .line 581974
    invoke-virtual {v3}, LX/2ub;->toString()Ljava/lang/String;

    move-result-object v8

    .line 581975
    iput-object v8, v7, LX/3DL;->g:Ljava/lang/String;

    .line 581976
    move-object v7, v7

    .line 581977
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    invoke-static {v8}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v8

    .line 581978
    iput-object v8, v7, LX/3DL;->p:Lcom/facebook/common/callercontext/CallerContext;

    .line 581979
    move-object v7, v7

    .line 581980
    iput v4, v7, LX/3DL;->c:I

    .line 581981
    move-object v7, v7

    .line 581982
    const/4 v8, 0x1

    move v8, v8

    .line 581983
    if-eqz v8, :cond_0

    .line 581984
    iput-boolean v12, v7, LX/3DL;->m:Z

    .line 581985
    iput-object v5, v7, LX/3DL;->n:Ljava/lang/String;

    .line 581986
    :cond_0
    iget-object v8, v0, LX/1rk;->j:LX/1rn;

    .line 581987
    iget-object v11, v8, LX/1rn;->e:LX/0Sh;

    invoke-virtual {v11}, LX/0Sh;->b()V

    .line 581988
    iget-object v11, v8, LX/1rn;->c:LX/0Ot;

    invoke-interface {v11}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    invoke-virtual {v11, v9, v10}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->d(J)Ljava/lang/String;

    move-result-object v11

    move-object v8, v11

    .line 581989
    invoke-static {v8}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 581990
    new-array v8, v12, [Ljava/lang/String;

    const/4 v9, 0x0

    const-string v10, "null"

    aput-object v10, v8, v9

    invoke-static {v8}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/3DL;->a(Ljava/util/List;)LX/3DL;

    .line 581991
    :cond_1
    :goto_0
    invoke-virtual {v7}, LX/3DL;->q()Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;

    move-result-object v7

    move-object v0, v7

    .line 581992
    return-object v0

    .line 581993
    :cond_2
    iput-object v8, v7, LX/3DL;->e:Ljava/lang/String;

    .line 581994
    invoke-static {v6}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 581995
    invoke-virtual {v7, v6}, LX/3DL;->a(Ljava/util/List;)LX/3DL;

    .line 581996
    iput-boolean v12, v7, LX/3DL;->o:Z

    .line 581997
    goto :goto_0

    .line 581998
    :cond_3
    sget-object v8, LX/3DO;->FULL:LX/3DO;

    if-ne v2, v8, :cond_1

    .line 581999
    iget-object v8, v0, LX/1rk;->j:LX/1rn;

    invoke-virtual {v8, v9, v10}, LX/1rn;->a(J)LX/0Px;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/3DL;->a(Ljava/util/List;)LX/3DL;

    goto :goto_0
.end method
