.class public Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;
.super Lcom/facebook/fbservice/results/BaseResult;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/3T7;

.field public final b:J

.field private c:Z

.field private d:Z

.field private final e:Lcom/facebook/notifications/model/NotificationStories;

.field private final f:Lcom/facebook/notifications/model/NewNotificationStories;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 583736
    new-instance v0, LX/3CX;

    invoke-direct {v0}, LX/3CX;-><init>()V

    sput-object v0, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 583720
    invoke-direct {p0, p1}, Lcom/facebook/fbservice/results/BaseResult;-><init>(Landroid/os/Parcel;)V

    .line 583721
    iput-boolean v2, p0, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;->c:Z

    .line 583722
    iput-boolean v2, p0, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;->d:Z

    .line 583723
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;->b:J

    .line 583724
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;->d:Z

    .line 583725
    iget-boolean v0, p0, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;->d:Z

    if-eqz v0, :cond_1

    .line 583726
    iput-object v3, p0, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;->e:Lcom/facebook/notifications/model/NotificationStories;

    .line 583727
    const-class v0, Lcom/facebook/notifications/model/NewNotificationStories;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/model/NewNotificationStories;

    iput-object v0, p0, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;->f:Lcom/facebook/notifications/model/NewNotificationStories;

    .line 583728
    new-instance v0, LX/3T7;

    iget-object v3, p0, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;->f:Lcom/facebook/notifications/model/NewNotificationStories;

    invoke-direct {v0, v3}, LX/3T7;-><init>(Lcom/facebook/notifications/model/NewNotificationStories;)V

    iput-object v0, p0, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;->a:LX/3T7;

    .line 583729
    :goto_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_2

    :goto_2
    iput-boolean v1, p0, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;->c:Z

    .line 583730
    return-void

    :cond_0
    move v0, v2

    .line 583731
    goto :goto_0

    .line 583732
    :cond_1
    const-class v0, Lcom/facebook/notifications/model/NotificationStories;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/model/NotificationStories;

    iput-object v0, p0, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;->e:Lcom/facebook/notifications/model/NotificationStories;

    .line 583733
    iput-object v3, p0, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;->f:Lcom/facebook/notifications/model/NewNotificationStories;

    .line 583734
    new-instance v0, LX/3T7;

    iget-object v3, p0, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;->e:Lcom/facebook/notifications/model/NotificationStories;

    invoke-direct {v0, v3}, LX/3T7;-><init>(Lcom/facebook/notifications/model/NotificationStories;)V

    iput-object v0, p0, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;->a:LX/3T7;

    goto :goto_1

    :cond_2
    move v1, v2

    .line 583735
    goto :goto_2
.end method

.method public constructor <init>(Lcom/facebook/notifications/model/NotificationStories;Lcom/facebook/notifications/model/NewNotificationStories;JLX/0ta;J)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 583711
    invoke-direct {p0, p5, p6, p7}, Lcom/facebook/fbservice/results/BaseResult;-><init>(LX/0ta;J)V

    .line 583712
    iput-boolean v0, p0, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;->c:Z

    .line 583713
    iput-boolean v0, p0, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;->d:Z

    .line 583714
    iput-object p1, p0, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;->e:Lcom/facebook/notifications/model/NotificationStories;

    .line 583715
    iput-object p2, p0, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;->f:Lcom/facebook/notifications/model/NewNotificationStories;

    .line 583716
    iget-object v0, p0, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;->e:Lcom/facebook/notifications/model/NotificationStories;

    if-eqz v0, :cond_0

    new-instance v0, LX/3T7;

    iget-object v1, p0, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;->e:Lcom/facebook/notifications/model/NotificationStories;

    invoke-direct {v0, v1}, LX/3T7;-><init>(Lcom/facebook/notifications/model/NotificationStories;)V

    :goto_0
    iput-object v0, p0, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;->a:LX/3T7;

    .line 583717
    iput-wide p3, p0, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;->b:J

    .line 583718
    return-void

    .line 583719
    :cond_0
    new-instance v0, LX/3T7;

    iget-object v1, p0, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;->f:Lcom/facebook/notifications/model/NewNotificationStories;

    invoke-direct {v0, v1}, LX/3T7;-><init>(Lcom/facebook/notifications/model/NewNotificationStories;)V

    goto :goto_0
.end method

.method public constructor <init>(Lcom/facebook/notifications/model/NotificationStories;Lcom/facebook/notifications/model/NewNotificationStories;JLX/0ta;JZ)V
    .locals 0

    .prologue
    .line 583737
    invoke-direct/range {p0 .. p7}, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;-><init>(Lcom/facebook/notifications/model/NotificationStories;Lcom/facebook/notifications/model/NewNotificationStories;JLX/0ta;J)V

    .line 583738
    iput-boolean p8, p0, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;->c:Z

    .line 583739
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 583710
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 583700
    invoke-super {p0, p1, p2}, Lcom/facebook/fbservice/results/BaseResult;->writeToParcel(Landroid/os/Parcel;I)V

    .line 583701
    iget-wide v4, p0, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;->b:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 583702
    iget-boolean v0, p0, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;->d:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 583703
    iget-boolean v0, p0, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;->d:Z

    if-eqz v0, :cond_1

    .line 583704
    iget-object v0, p0, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;->f:Lcom/facebook/notifications/model/NewNotificationStories;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 583705
    :goto_1
    iget-boolean v0, p0, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;->c:Z

    if-eqz v0, :cond_2

    :goto_2
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 583706
    return-void

    :cond_0
    move v0, v2

    .line 583707
    goto :goto_0

    .line 583708
    :cond_1
    iget-object v0, p0, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;->e:Lcom/facebook/notifications/model/NotificationStories;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    goto :goto_1

    :cond_2
    move v1, v2

    .line 583709
    goto :goto_2
.end method
