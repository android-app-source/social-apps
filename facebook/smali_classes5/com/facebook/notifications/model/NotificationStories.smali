.class public Lcom/facebook/notifications/model/NotificationStories;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/notifications/model/NotificationStoriesDeserializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/notifications/model/NotificationStories;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final deltaStories:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsDeltaConnectionFieldsModel;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "deltas"
    .end annotation
.end field

.field private final newStories:LX/0Px;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "edges"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;",
            ">;"
        }
    .end annotation
.end field

.field private final pageInfo:Lcom/facebook/graphql/model/GraphQLPageInfo;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "page_info"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 583646
    const-class v0, Lcom/facebook/notifications/model/NotificationStoriesDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 583645
    new-instance v0, LX/3T6;

    invoke-direct {v0}, LX/3T6;-><init>()V

    sput-object v0, Lcom/facebook/notifications/model/NotificationStories;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 583624
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 583625
    iput-object v0, p0, Lcom/facebook/notifications/model/NotificationStories;->newStories:LX/0Px;

    .line 583626
    iput-object v0, p0, Lcom/facebook/notifications/model/NotificationStories;->deltaStories:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsDeltaConnectionFieldsModel;

    .line 583627
    iput-object v0, p0, Lcom/facebook/notifications/model/NotificationStories;->pageInfo:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 583628
    return-void
.end method

.method public constructor <init>(LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/2nq;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 583643
    invoke-direct {p0, p1, v0, v0}, Lcom/facebook/notifications/model/NotificationStories;-><init>(LX/0Px;Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsDeltaConnectionFieldsModel;Lcom/facebook/graphql/model/GraphQLPageInfo;)V

    .line 583644
    return-void
.end method

.method private constructor <init>(LX/0Px;Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsDeltaConnectionFieldsModel;Lcom/facebook/graphql/model/GraphQLPageInfo;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/2nq;",
            ">;",
            "Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsDeltaConnectionFieldsModel;",
            "Lcom/facebook/graphql/model/GraphQLPageInfo;",
            ")V"
        }
    .end annotation

    .prologue
    .line 583632
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 583633
    if-eqz p1, :cond_1

    .line 583634
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 583635
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2nq;

    .line 583636
    invoke-static {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;->a(LX/2nq;)Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 583637
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 583638
    :cond_0
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/model/NotificationStories;->newStories:LX/0Px;

    .line 583639
    :goto_1
    iput-object p2, p0, Lcom/facebook/notifications/model/NotificationStories;->deltaStories:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsDeltaConnectionFieldsModel;

    .line 583640
    iput-object p3, p0, Lcom/facebook/notifications/model/NotificationStories;->pageInfo:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 583641
    return-void

    .line 583642
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/notifications/model/NotificationStories;->newStories:LX/0Px;

    goto :goto_1
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 583647
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 583648
    invoke-static {p1}, LX/4By;->b(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/model/NotificationStories;->newStories:LX/0Px;

    .line 583649
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsDeltaConnectionFieldsModel;

    iput-object v0, p0, Lcom/facebook/notifications/model/NotificationStories;->deltaStories:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsDeltaConnectionFieldsModel;

    .line 583650
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageInfo;

    iput-object v0, p0, Lcom/facebook/notifications/model/NotificationStories;->pageInfo:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 583651
    return-void
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 583631
    iget-object v0, p0, Lcom/facebook/notifications/model/NotificationStories;->newStories:LX/0Px;

    return-object v0
.end method

.method public final b()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsDeltaConnectionFieldsModel;
    .locals 1

    .prologue
    .line 583630
    iget-object v0, p0, Lcom/facebook/notifications/model/NotificationStories;->deltaStories:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsDeltaConnectionFieldsModel;

    return-object v0
.end method

.method public final c()Lcom/facebook/graphql/model/GraphQLPageInfo;
    .locals 1

    .prologue
    .line 583629
    iget-object v0, p0, Lcom/facebook/notifications/model/NotificationStories;->pageInfo:Lcom/facebook/graphql/model/GraphQLPageInfo;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 583623
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 583619
    iget-object v0, p0, Lcom/facebook/notifications/model/NotificationStories;->newStories:LX/0Px;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Ljava/util/List;)V

    .line 583620
    iget-object v0, p0, Lcom/facebook/notifications/model/NotificationStories;->deltaStories:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsDeltaConnectionFieldsModel;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 583621
    iget-object v0, p0, Lcom/facebook/notifications/model/NotificationStories;->pageInfo:Lcom/facebook/graphql/model/GraphQLPageInfo;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 583622
    return-void
.end method
