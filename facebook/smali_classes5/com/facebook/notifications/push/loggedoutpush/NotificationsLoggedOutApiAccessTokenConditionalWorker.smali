.class public Lcom/facebook/notifications/push/loggedoutpush/NotificationsLoggedOutApiAccessTokenConditionalWorker;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/2Jw;


# static fields
.field private static final e:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/03V;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/3SR;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/3T4;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/11H;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 582387
    const-class v0, Lcom/facebook/notifications/push/loggedoutpush/NotificationsLoggedOutApiAccessTokenConditionalWorker;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/notifications/push/loggedoutpush/NotificationsLoggedOutApiAccessTokenConditionalWorker;->e:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 582372
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 582373
    return-void
.end method


# virtual methods
.method public final a(LX/2Jy;)Z
    .locals 5

    .prologue
    .line 582374
    invoke-virtual {p1}, LX/2Jy;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 582375
    const/4 v0, 0x0

    .line 582376
    :goto_0
    return v0

    .line 582377
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/facebook/notifications/push/loggedoutpush/NotificationsLoggedOutApiAccessTokenConditionalWorker;->d:LX/11H;

    iget-object v1, p0, Lcom/facebook/notifications/push/loggedoutpush/NotificationsLoggedOutApiAccessTokenConditionalWorker;->b:LX/3SR;

    const/4 v2, 0x0

    sget-object v3, Lcom/facebook/notifications/push/loggedoutpush/NotificationsLoggedOutApiAccessTokenConditionalWorker;->e:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2, v3}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    .line 582378
    iget-object v0, p0, Lcom/facebook/notifications/push/loggedoutpush/NotificationsLoggedOutApiAccessTokenConditionalWorker;->c:LX/3T4;

    .line 582379
    iget-object v1, v0, LX/3T4;->b:LX/10M;

    invoke-virtual {v1}, LX/10M;->a()Ljava/util/List;

    move-result-object v1

    .line 582380
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    .line 582381
    iget-object v2, v0, LX/3T4;->a:Landroid/content/Context;

    const-string v4, "notification"

    invoke-virtual {v2, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/NotificationManager;

    .line 582382
    iget-object v1, v1, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mUserId:Ljava/lang/String;

    invoke-static {v1}, LX/3T4;->a(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 582383
    invoke-virtual {v2, v1}, Landroid/app/NotificationManager;->cancel(I)V

    goto :goto_1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 582384
    :cond_1
    :goto_2
    const/4 v0, 0x1

    goto :goto_0

    .line 582385
    :catch_0
    move-exception v0

    .line 582386
    iget-object v1, p0, Lcom/facebook/notifications/push/loggedoutpush/NotificationsLoggedOutApiAccessTokenConditionalWorker;->a:LX/03V;

    const-class v2, Lcom/facebook/notifications/push/loggedoutpush/NotificationsLoggedOutBadgePollConditionalWorker;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "error in logged out api access token fetch: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method
