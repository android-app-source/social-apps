.class public Lcom/facebook/notifications/push/loggedoutpush/NotificationsLoggedOutPushPollConditionalWorker;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/2Jw;


# static fields
.field private static final d:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/03V;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/3T4;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/11H;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 583546
    const-class v0, Lcom/facebook/notifications/push/loggedoutpush/NotificationsLoggedOutPushPollConditionalWorker;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/notifications/push/loggedoutpush/NotificationsLoggedOutPushPollConditionalWorker;->d:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 583547
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 583548
    return-void
.end method


# virtual methods
.method public final a(LX/2Jy;)Z
    .locals 5

    .prologue
    .line 583549
    invoke-virtual {p1}, LX/2Jy;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 583550
    const/4 v0, 0x0

    .line 583551
    :goto_0
    return v0

    .line 583552
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/facebook/notifications/push/loggedoutpush/NotificationsLoggedOutPushPollConditionalWorker;->c:LX/11H;

    iget-object v1, p0, Lcom/facebook/notifications/push/loggedoutpush/NotificationsLoggedOutPushPollConditionalWorker;->b:LX/3T4;

    const/4 v2, 0x0

    sget-object v3, Lcom/facebook/notifications/push/loggedoutpush/NotificationsLoggedOutPushPollConditionalWorker;->d:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2, v3}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 583553
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 583554
    :catch_0
    move-exception v0

    .line 583555
    iget-object v1, p0, Lcom/facebook/notifications/push/loggedoutpush/NotificationsLoggedOutPushPollConditionalWorker;->a:LX/03V;

    const-class v2, Lcom/facebook/notifications/push/loggedoutpush/NotificationsLoggedOutPushPollConditionalWorker;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "error in logged out push api fetch: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
