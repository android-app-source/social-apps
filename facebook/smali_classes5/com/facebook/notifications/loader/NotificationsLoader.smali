.class public Lcom/facebook/notifications/loader/NotificationsLoader;
.super LX/25A;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/25A",
        "<",
        "Ljava/util/List",
        "<",
        "LX/2nq;",
        ">;>;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# instance fields
.field public f:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

.field private final h:Lcom/facebook/auth/viewercontext/ViewerContext;

.field private final i:Landroid/content/ContentResolver;

.field private final j:Landroid/net/Uri;

.field private final k:I

.field private final l:LX/1qv;

.field private u:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/2nq;",
            ">;"
        }
    .end annotation
.end field

.field private v:LX/2s6;

.field private w:Z

.field public x:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;Landroid/content/ContentResolver;Landroid/net/Uri;Lcom/facebook/auth/viewercontext/ViewerContext;ILX/1qv;)V
    .locals 1

    .prologue
    .line 584182
    invoke-direct {p0, p1}, LX/25A;-><init>(Landroid/content/Context;)V

    .line 584183
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/notifications/loader/NotificationsLoader;->w:Z

    .line 584184
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/loader/NotificationsLoader;->f:LX/0am;

    .line 584185
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    iput-object v0, p0, Lcom/facebook/notifications/loader/NotificationsLoader;->g:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    .line 584186
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentResolver;

    iput-object v0, p0, Lcom/facebook/notifications/loader/NotificationsLoader;->i:Landroid/content/ContentResolver;

    .line 584187
    invoke-static {p4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/notifications/loader/NotificationsLoader;->j:Landroid/net/Uri;

    .line 584188
    iput-object p5, p0, Lcom/facebook/notifications/loader/NotificationsLoader;->h:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 584189
    new-instance v0, LX/2s6;

    invoke-direct {v0, p0}, LX/2s6;-><init>(LX/0k9;)V

    iput-object v0, p0, Lcom/facebook/notifications/loader/NotificationsLoader;->v:LX/2s6;

    .line 584190
    iput p6, p0, Lcom/facebook/notifications/loader/NotificationsLoader;->k:I

    .line 584191
    iput-object p7, p0, Lcom/facebook/notifications/loader/NotificationsLoader;->l:LX/1qv;

    .line 584192
    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/2nq;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 584177
    iput-object p1, p0, Lcom/facebook/notifications/loader/NotificationsLoader;->u:Ljava/util/List;

    .line 584178
    iget-boolean v0, p0, LX/0k9;->p:Z

    move v0, v0

    .line 584179
    if-eqz v0, :cond_0

    .line 584180
    invoke-super {p0, p1}, LX/25A;->b(Ljava/lang/Object;)V

    .line 584181
    :cond_0
    return-void
.end method


# virtual methods
.method public final synthetic b(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 584176
    check-cast p1, Ljava/util/List;

    invoke-direct {p0, p1}, Lcom/facebook/notifications/loader/NotificationsLoader;->a(Ljava/util/List;)V

    return-void
.end method

.method public final d()Ljava/lang/Object;
    .locals 7

    .prologue
    .line 584131
    const/4 v0, 0x0

    .line 584132
    invoke-static {}, Landroid/os/Process;->myTid()I

    move-result v1

    invoke-static {v1}, Landroid/os/Process;->getThreadPriority(I)I

    move-result v1

    .line 584133
    const/4 v2, 0x0

    :try_start_0
    invoke-static {v2}, Landroid/os/Process;->setThreadPriority(I)V

    .line 584134
    new-instance v2, LX/3DL;

    invoke-direct {v2}, LX/3DL;-><init>()V

    iget-object v3, p0, Lcom/facebook/notifications/loader/NotificationsLoader;->h:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 584135
    iput-object v3, v2, LX/3DL;->f:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 584136
    move-object v2, v2

    .line 584137
    sget-object v3, LX/0rS;->STALE_DATA_OKAY:LX/0rS;

    .line 584138
    iput-object v3, v2, LX/3DL;->a:LX/0rS;

    .line 584139
    move-object v2, v2

    .line 584140
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v3

    .line 584141
    iput-object v3, v2, LX/3DL;->p:Lcom/facebook/common/callercontext/CallerContext;

    .line 584142
    move-object v2, v2

    .line 584143
    iget v3, p0, Lcom/facebook/notifications/loader/NotificationsLoader;->x:I

    iget-object v4, p0, Lcom/facebook/notifications/loader/NotificationsLoader;->u:Ljava/util/List;

    if-nez v4, :cond_1

    :goto_0
    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 584144
    iget-object v3, p0, Lcom/facebook/notifications/loader/NotificationsLoader;->l:LX/1qv;

    .line 584145
    iget-object v4, v3, LX/1qv;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v5, LX/0hM;->f:LX/0Tn;

    const/4 v6, 0x0

    invoke-interface {v4, v5, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v4

    move v3, v4

    .line 584146
    add-int/2addr v0, v3

    .line 584147
    iget-boolean v3, p0, Lcom/facebook/notifications/loader/NotificationsLoader;->w:Z

    if-eqz v3, :cond_2

    .line 584148
    iget v0, p0, Lcom/facebook/notifications/loader/NotificationsLoader;->k:I

    .line 584149
    iput v0, v2, LX/3DL;->b:I

    .line 584150
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/notifications/loader/NotificationsLoader;->w:Z

    .line 584151
    :goto_1
    iget-object v0, p0, Lcom/facebook/notifications/loader/NotificationsLoader;->f:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 584152
    const/4 v0, 0x1

    .line 584153
    iput-boolean v0, v2, LX/3DL;->m:Z

    .line 584154
    iget-object v0, p0, Lcom/facebook/notifications/loader/NotificationsLoader;->f:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 584155
    iput-object v0, v2, LX/3DL;->n:Ljava/lang/String;

    .line 584156
    :cond_0
    invoke-virtual {v2}, LX/3DL;->q()Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;

    move-result-object v0

    .line 584157
    iget-object v2, p0, Lcom/facebook/notifications/loader/NotificationsLoader;->g:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    invoke-virtual {v2, v0}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a(Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;)Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;->a:LX/3T7;

    invoke-virtual {v0}, LX/3T7;->a()LX/0Px;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 584158
    invoke-static {v1}, Landroid/os/Process;->setThreadPriority(I)V

    return-object v0

    .line 584159
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/facebook/notifications/loader/NotificationsLoader;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0

    .line 584160
    :cond_2
    const/16 v3, 0xa

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 584161
    iput v0, v2, LX/3DL;->b:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 584162
    goto :goto_1

    .line 584163
    :catchall_0
    move-exception v0

    invoke-static {v1}, Landroid/os/Process;->setThreadPriority(I)V

    throw v0
.end method

.method public final g()V
    .locals 4

    .prologue
    .line 584170
    iget-object v0, p0, Lcom/facebook/notifications/loader/NotificationsLoader;->i:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/facebook/notifications/loader/NotificationsLoader;->j:Landroid/net/Uri;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/facebook/notifications/loader/NotificationsLoader;->v:LX/2s6;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 584171
    iget-object v0, p0, Lcom/facebook/notifications/loader/NotificationsLoader;->u:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 584172
    iget-object v0, p0, Lcom/facebook/notifications/loader/NotificationsLoader;->u:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/facebook/notifications/loader/NotificationsLoader;->a(Ljava/util/List;)V

    .line 584173
    :cond_0
    invoke-virtual {p0}, LX/0k9;->t()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/notifications/loader/NotificationsLoader;->u:Ljava/util/List;

    if-nez v0, :cond_2

    .line 584174
    :cond_1
    invoke-virtual {p0}, LX/0k9;->a()V

    .line 584175
    :cond_2
    return-void
.end method

.method public final h()V
    .locals 0

    .prologue
    .line 584168
    invoke-virtual {p0}, LX/25A;->b()Z

    .line 584169
    return-void
.end method

.method public final i()V
    .locals 2

    .prologue
    .line 584164
    invoke-virtual {p0}, Lcom/facebook/notifications/loader/NotificationsLoader;->h()V

    .line 584165
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/notifications/loader/NotificationsLoader;->u:Ljava/util/List;

    .line 584166
    iget-object v0, p0, Lcom/facebook/notifications/loader/NotificationsLoader;->i:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/facebook/notifications/loader/NotificationsLoader;->v:LX/2s6;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 584167
    return-void
.end method
