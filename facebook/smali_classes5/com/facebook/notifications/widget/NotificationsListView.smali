.class public Lcom/facebook/notifications/widget/NotificationsListView;
.super Lcom/facebook/widget/listview/BetterListView;
.source ""


# instance fields
.field public a:LX/03V;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 584250
    invoke-direct {p0, p1}, Lcom/facebook/widget/listview/BetterListView;-><init>(Landroid/content/Context;)V

    .line 584251
    const-class v0, Lcom/facebook/notifications/widget/NotificationsListView;

    invoke-static {v0, p0}, Lcom/facebook/notifications/widget/NotificationsListView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 584252
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 584253
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/listview/BetterListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 584254
    const-class v0, Lcom/facebook/notifications/widget/NotificationsListView;

    invoke-static {v0, p0}, Lcom/facebook/notifications/widget/NotificationsListView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 584255
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/notifications/widget/NotificationsListView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/notifications/widget/NotificationsListView;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    iput-object v0, p0, Lcom/facebook/notifications/widget/NotificationsListView;->a:LX/03V;

    return-void
.end method


# virtual methods
.method public final drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
    .locals 5

    .prologue
    .line 584256
    :try_start_0
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/widget/listview/BetterListView;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
    :try_end_0
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 584257
    return v0

    .line 584258
    :catch_0
    move-exception v1

    .line 584259
    invoke-virtual {p0}, Lcom/facebook/notifications/widget/NotificationsListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v2

    .line 584260
    instance-of v0, v2, LX/DsH;

    if-eqz v0, :cond_0

    .line 584261
    const v0, 0x7f0d0090

    invoke-virtual {p2, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 584262
    invoke-interface {v2, v0}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 584263
    instance-of v2, v0, LX/2nq;

    if-eqz v2, :cond_0

    .line 584264
    check-cast v0, LX/2nq;

    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 584265
    iget-object v2, p0, Lcom/facebook/notifications/widget/NotificationsListView;->a:LX/03V;

    const-string v3, "notification_out_of_bounds_category"

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 584266
    :cond_0
    throw v1
.end method
