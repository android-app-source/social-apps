.class public Lcom/facebook/notifications/multirow/partdefinition/NotificationsSectionHeaderPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/3Cg;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Lcom/facebook/widget/text/BetterTextView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static b:LX/0Xm;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 599941
    const v0, 0x7f030c39

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsSectionHeaderPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 599942
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 599943
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/notifications/multirow/partdefinition/NotificationsSectionHeaderPartDefinition;
    .locals 3

    .prologue
    .line 599944
    const-class v1, Lcom/facebook/notifications/multirow/partdefinition/NotificationsSectionHeaderPartDefinition;

    monitor-enter v1

    .line 599945
    :try_start_0
    sget-object v0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsSectionHeaderPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 599946
    sput-object v2, Lcom/facebook/notifications/multirow/partdefinition/NotificationsSectionHeaderPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 599947
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 599948
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 599949
    new-instance v0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsSectionHeaderPartDefinition;

    invoke-direct {v0}, Lcom/facebook/notifications/multirow/partdefinition/NotificationsSectionHeaderPartDefinition;-><init>()V

    .line 599950
    move-object v0, v0

    .line 599951
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 599952
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsSectionHeaderPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 599953
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 599954
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 599955
    sget-object v0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsSectionHeaderPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x62d45be8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 599956
    check-cast p1, LX/3Cg;

    check-cast p4, Lcom/facebook/widget/text/BetterTextView;

    .line 599957
    iget-object v1, p1, LX/3Cg;->a:Ljava/lang/String;

    move-object v1, v1

    .line 599958
    invoke-virtual {p4, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 599959
    const/16 v1, 0x1f

    const v2, 0x22885985

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 599960
    check-cast p1, LX/3Cg;

    .line 599961
    if-eqz p1, :cond_0

    .line 599962
    iget-object v0, p1, LX/3Cg;->a:Ljava/lang/String;

    move-object v0, v0

    .line 599963
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
