.class public Lcom/facebook/notifications/service/NotificationsServiceHandler;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1qM;


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BDq;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3Sx;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BDp;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BDl;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3cw;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BDj;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/B9n;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/BDq;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/3Sx;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/BDl;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/BDp;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/3cw;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/BDj;",
            ">;",
            "LX/0Or",
            "<",
            "LX/B9n;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 583237
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 583238
    iput-object p1, p0, Lcom/facebook/notifications/service/NotificationsServiceHandler;->a:LX/0Or;

    .line 583239
    iput-object p2, p0, Lcom/facebook/notifications/service/NotificationsServiceHandler;->b:LX/0Ot;

    .line 583240
    iput-object p3, p0, Lcom/facebook/notifications/service/NotificationsServiceHandler;->c:LX/0Ot;

    .line 583241
    iput-object p5, p0, Lcom/facebook/notifications/service/NotificationsServiceHandler;->d:LX/0Ot;

    .line 583242
    iput-object p4, p0, Lcom/facebook/notifications/service/NotificationsServiceHandler;->e:LX/0Ot;

    .line 583243
    iput-object p6, p0, Lcom/facebook/notifications/service/NotificationsServiceHandler;->f:LX/0Ot;

    .line 583244
    iput-object p7, p0, Lcom/facebook/notifications/service/NotificationsServiceHandler;->g:LX/0Ot;

    .line 583245
    iput-object p8, p0, Lcom/facebook/notifications/service/NotificationsServiceHandler;->h:LX/0Or;

    .line 583246
    return-void
.end method

.method private b(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 583231
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 583232
    const-string v1, "fetchGraphQLNotificationsParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;

    .line 583233
    iget-object v1, p0, Lcom/facebook/notifications/service/NotificationsServiceHandler;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/11H;

    iget-object v2, p0, Lcom/facebook/notifications/service/NotificationsServiceHandler;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0e6;

    .line 583234
    iget-object v3, v0, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->p:Lcom/facebook/common/callercontext/CallerContext;

    move-object v3, v3

    .line 583235
    invoke-virtual {v1, v2, v0, v3}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;

    .line 583236
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)Lcom/facebook/notifications/service/NotificationsServiceHandler;
    .locals 9

    .prologue
    .line 583177
    new-instance v0, Lcom/facebook/notifications/service/NotificationsServiceHandler;

    const/16 v1, 0xb83

    invoke-static {p0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    const/16 v2, 0x2af2

    invoke-static {p0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0xe5b

    invoke-static {p0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x2af0

    invoke-static {p0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x2af1

    invoke-static {p0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0xe5c

    invoke-static {p0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x2aef

    invoke-static {p0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x2680

    invoke-static {p0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-direct/range {v0 .. v8}, Lcom/facebook/notifications/service/NotificationsServiceHandler;-><init>(LX/0Or;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;)V

    .line 583178
    return-object v0
.end method

.method private c(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 583224
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 583225
    const-string v1, "graphNotifsUpdateSeenStatePrams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/server/NotificationsChangeSeenStateParams;

    .line 583226
    iget-object v1, p0, Lcom/facebook/notifications/service/NotificationsServiceHandler;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/11H;

    iget-object v2, p0, Lcom/facebook/notifications/service/NotificationsServiceHandler;->d:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0e6;

    .line 583227
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v3

    move-object v3, v3

    .line 583228
    invoke-virtual {v1, v2, v0, v3}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    .line 583229
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 583230
    return-object v0
.end method

.method private e(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 583218
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 583219
    const-string v1, "notificationsFetchSeenStatesParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/server/FetchNotificationSeenStatesParams;

    .line 583220
    iget-object v1, p0, Lcom/facebook/notifications/service/NotificationsServiceHandler;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/11H;

    iget-object v2, p0, Lcom/facebook/notifications/service/NotificationsServiceHandler;->f:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0e6;

    .line 583221
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v3

    move-object v3, v3

    .line 583222
    invoke-virtual {v1, v2, v0, v3}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/methods/FetchNotificationSeenStatesResult;

    .line 583223
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 583179
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 583180
    const-string v1, "notifChangeSetting"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 583181
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 583182
    const-string v1, "notificationsChangeSettingsParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/server/NotificationsChangeSettingsParams;

    .line 583183
    iget-object v1, p0, Lcom/facebook/notifications/service/NotificationsServiceHandler;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/11H;

    iget-object v2, p0, Lcom/facebook/notifications/service/NotificationsServiceHandler;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0e6;

    .line 583184
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v3

    move-object v3, v3

    .line 583185
    invoke-virtual {v1, v2, v0, v3}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    .line 583186
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 583187
    move-object v0, v0

    .line 583188
    :goto_0
    return-object v0

    .line 583189
    :cond_0
    const-string v1, "fetch_gql_notifications"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 583190
    invoke-direct {p0, p1}, Lcom/facebook/notifications/service/NotificationsServiceHandler;->b(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0

    .line 583191
    :cond_1
    const-string v1, "force_fetch_gql_notifications_from_server"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 583192
    invoke-direct {p0, p1}, Lcom/facebook/notifications/service/NotificationsServiceHandler;->b(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0

    .line 583193
    :cond_2
    const-string v1, "graphNotifUpdateSeenState"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 583194
    invoke-direct {p0, p1}, Lcom/facebook/notifications/service/NotificationsServiceHandler;->c(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0

    .line 583195
    :cond_3
    const-string v1, "graphNotifUpdateSeenStateOnlyOnServer"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 583196
    invoke-direct {p0, p1}, Lcom/facebook/notifications/service/NotificationsServiceHandler;->c(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0

    .line 583197
    :cond_4
    const-string v1, "getNotificationsSettings"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 583198
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 583199
    const-string v1, "notificationsGetSettingsParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/server/GetNotificationsSettingsParams;

    .line 583200
    iget-object v1, p0, Lcom/facebook/notifications/service/NotificationsServiceHandler;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/11H;

    iget-object v2, p0, Lcom/facebook/notifications/service/NotificationsServiceHandler;->e:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0e6;

    .line 583201
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v3

    move-object v3, v3

    .line 583202
    invoke-virtual {v1, v2, v0, v3}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/methods/GetNotificationsSettingsResult;

    .line 583203
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    move-object v0, v0

    .line 583204
    goto :goto_0

    .line 583205
    :cond_5
    const-string v1, "fecthNotificationSeenStates"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 583206
    invoke-direct {p0, p1}, Lcom/facebook/notifications/service/NotificationsServiceHandler;->e(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0

    .line 583207
    :cond_6
    const-string v1, "fetchNotificationSeenStatesServerOnly"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 583208
    invoke-direct {p0, p1}, Lcom/facebook/notifications/service/NotificationsServiceHandler;->e(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto/16 :goto_0

    .line 583209
    :cond_7
    const-string v1, "fetchNotificationURI"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 583210
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 583211
    const-string v1, "url"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 583212
    iget-object v0, p0, Lcom/facebook/notifications/service/NotificationsServiceHandler;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11H;

    iget-object v1, p0, Lcom/facebook/notifications/service/NotificationsServiceHandler;->g:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0e6;

    .line 583213
    iget-object v3, p1, LX/1qK;->mCallerContext:Lcom/facebook/common/callercontext/CallerContext;

    move-object v3, v3

    .line 583214
    invoke-virtual {v0, v1, v2, v3}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/methods/FetchNotificationURIResult;

    .line 583215
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    move-object v0, v0

    .line 583216
    goto/16 :goto_0

    .line 583217
    :cond_8
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "Unknown operation type"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0
.end method
