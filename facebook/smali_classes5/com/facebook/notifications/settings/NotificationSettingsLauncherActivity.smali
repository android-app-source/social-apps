.class public Lcom/facebook/notifications/settings/NotificationSettingsLauncherActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public p:LX/2PY;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/33a;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/17Y;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 572619
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/notifications/settings/NotificationSettingsLauncherActivity;LX/2PY;LX/33a;LX/17Y;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0

    .prologue
    .line 572620
    iput-object p1, p0, Lcom/facebook/notifications/settings/NotificationSettingsLauncherActivity;->p:LX/2PY;

    iput-object p2, p0, Lcom/facebook/notifications/settings/NotificationSettingsLauncherActivity;->q:LX/33a;

    iput-object p3, p0, Lcom/facebook/notifications/settings/NotificationSettingsLauncherActivity;->r:LX/17Y;

    iput-object p4, p0, Lcom/facebook/notifications/settings/NotificationSettingsLauncherActivity;->s:Lcom/facebook/content/SecureContextHelper;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, Lcom/facebook/notifications/settings/NotificationSettingsLauncherActivity;

    invoke-static {v3}, LX/2PY;->b(LX/0QB;)LX/2PY;

    move-result-object v0

    check-cast v0, LX/2PY;

    invoke-static {v3}, LX/33a;->b(LX/0QB;)LX/33a;

    move-result-object v1

    check-cast v1, LX/33a;

    invoke-static {v3}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v2

    check-cast v2, LX/17Y;

    invoke-static {v3}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0, v0, v1, v2, v3}, Lcom/facebook/notifications/settings/NotificationSettingsLauncherActivity;->a(Lcom/facebook/notifications/settings/NotificationSettingsLauncherActivity;LX/2PY;LX/33a;LX/17Y;Lcom/facebook/content/SecureContextHelper;)V

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 572621
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 572622
    invoke-static {p0, p0}, Lcom/facebook/notifications/settings/NotificationSettingsLauncherActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 572623
    invoke-static {p0}, LX/3qU;->a(Landroid/content/Context;)LX/3qU;

    move-result-object v0

    .line 572624
    iget-object v1, p0, Lcom/facebook/notifications/settings/NotificationSettingsLauncherActivity;->p:LX/2PY;

    .line 572625
    iget-object v2, v1, LX/2PY;->a:LX/0ad;

    sget-short v3, LX/15r;->x:S

    const/4 p1, 0x0

    invoke-interface {v2, v3, p1}, LX/0ad;->a(SZ)Z

    move-result v2

    move v1, v2

    .line 572626
    if-eqz v1, :cond_0

    .line 572627
    iget-object v1, p0, Lcom/facebook/notifications/settings/NotificationSettingsLauncherActivity;->r:LX/17Y;

    sget-object v2, LX/0ax;->cL:Ljava/lang/String;

    invoke-interface {v1, p0, v2}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/3qU;->a(Landroid/content/Intent;)LX/3qU;

    .line 572628
    :cond_0
    iget-object v1, p0, Lcom/facebook/notifications/settings/NotificationSettingsLauncherActivity;->r:LX/17Y;

    sget-object v2, LX/0ax;->de:Ljava/lang/String;

    invoke-interface {v1, p0, v2}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/3qU;->a(Landroid/content/Intent;)LX/3qU;

    .line 572629
    iget-object v1, p0, Lcom/facebook/notifications/settings/NotificationSettingsLauncherActivity;->s:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0}, Lcom/facebook/content/SecureContextHelper;->a(LX/3qU;)V

    .line 572630
    iget-object v0, p0, Lcom/facebook/notifications/settings/NotificationSettingsLauncherActivity;->q:LX/33a;

    .line 572631
    iget-object v1, v0, LX/33a;->a:LX/0Zb;

    const-string v2, "notification_settings_push_launched"

    invoke-static {v2}, LX/33a;->a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 572632
    invoke-virtual {p0}, Lcom/facebook/notifications/settings/NotificationSettingsLauncherActivity;->finish()V

    .line 572633
    return-void
.end method
