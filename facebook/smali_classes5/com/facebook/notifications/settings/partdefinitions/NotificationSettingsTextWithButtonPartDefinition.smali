.class public Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsTextWithButtonPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pp;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/DrP;",
        "LX/DrW;",
        "TE;",
        "Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static f:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;

.field public final c:LX/33W;

.field private final d:Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

.field private final e:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 600236
    const v0, 0x7f030c2a

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsTextWithButtonPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;LX/33W;Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 600237
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 600238
    iput-object p1, p0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsTextWithButtonPartDefinition;->b:Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;

    .line 600239
    iput-object p2, p0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsTextWithButtonPartDefinition;->c:LX/33W;

    .line 600240
    iput-object p3, p0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsTextWithButtonPartDefinition;->d:Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    .line 600241
    iput-object p4, p0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsTextWithButtonPartDefinition;->e:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    .line 600242
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsTextWithButtonPartDefinition;
    .locals 7

    .prologue
    .line 600243
    const-class v1, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsTextWithButtonPartDefinition;

    monitor-enter v1

    .line 600244
    :try_start_0
    sget-object v0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsTextWithButtonPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 600245
    sput-object v2, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsTextWithButtonPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 600246
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 600247
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 600248
    new-instance p0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsTextWithButtonPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;

    invoke-static {v0}, LX/33W;->a(LX/0QB;)LX/33W;

    move-result-object v4

    check-cast v4, LX/33W;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsTextWithButtonPartDefinition;-><init>(Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;LX/33W;Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;)V

    .line 600249
    move-object v0, p0

    .line 600250
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 600251
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsTextWithButtonPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 600252
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 600253
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 600254
    sget-object v0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsTextWithButtonPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 600255
    check-cast p2, LX/DrP;

    check-cast p3, LX/1Pn;

    const/4 v1, 0x0

    .line 600256
    iget-object v0, p2, LX/DrP;->a:Lcom/facebook/notifications/settings/data/NotifOptionNode;

    .line 600257
    iget-object v2, v0, Lcom/facebook/notifications/settings/data/NotifOptionNode;->a:LX/BCO;

    move-object v0, v2

    .line 600258
    invoke-interface {v0}, LX/BCO;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;

    move-result-object v3

    .line 600259
    invoke-virtual {v3}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;->k()LX/4aa;

    move-result-object v0

    invoke-interface {v0}, LX/4aa;->a()Ljava/lang/String;

    move-result-object v4

    .line 600260
    invoke-virtual {v3}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;->ic_()LX/174;

    move-result-object v0

    if-nez v0, :cond_2

    move-object v0, v1

    .line 600261
    :goto_0
    invoke-virtual {v3}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;->ib_()LX/174;

    move-result-object v2

    if-nez v2, :cond_3

    move-object v2, v1

    .line 600262
    :goto_1
    invoke-virtual {v3}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel$ImageSourceModel;

    move-result-object v5

    if-nez v5, :cond_4

    .line 600263
    :goto_2
    iget-object v3, p0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsTextWithButtonPartDefinition;->e:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    invoke-interface {p1, v3, v4}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 600264
    iget-object v3, p0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsTextWithButtonPartDefinition;->d:Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    invoke-interface {p1, v3, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 600265
    iget-object v3, p0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsTextWithButtonPartDefinition;->b:Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;

    .line 600266
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 600267
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 600268
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, " "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 600269
    :cond_0
    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 600270
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, " "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 600271
    :cond_1
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v0, v5

    .line 600272
    invoke-interface {p1, v3, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 600273
    new-instance v0, LX/DrW;

    new-instance v3, LX/DrV;

    invoke-direct {v3, p0, p2, p3}, LX/DrV;-><init>(Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsTextWithButtonPartDefinition;LX/DrP;LX/1Pn;)V

    invoke-direct {v0, v3, v2, v1}, LX/DrW;-><init>(Landroid/view/View$OnClickListener;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    .line 600274
    :cond_2
    invoke-virtual {v3}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;->ic_()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 600275
    :cond_3
    invoke-virtual {v3}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;->ib_()LX/174;

    move-result-object v2

    invoke-interface {v2}, LX/174;->a()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 600276
    :cond_4
    invoke-virtual {v3}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel$ImageSourceModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel$ImageSourceModel;->a()Ljava/lang/String;

    move-result-object v1

    goto :goto_2
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0xa0db230

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 600277
    check-cast p2, LX/DrW;

    check-cast p4, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    .line 600278
    iget-object v1, p2, LX/DrW;->c:Ljava/lang/String;

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 600279
    const/4 v1, 0x0

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setShowThumbnail(Z)V

    .line 600280
    :goto_0
    sget-object v1, LX/6VF;->SMALL:LX/6VF;

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setThumbnailSize(LX/6VF;)V

    .line 600281
    iget-object v1, p2, LX/DrW;->b:Ljava/lang/String;

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonText(Ljava/lang/CharSequence;)V

    .line 600282
    iget-object v1, p2, LX/DrW;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 600283
    const/16 v1, 0x1f

    const v2, 0x24f25908

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 600284
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setShowThumbnail(Z)V

    .line 600285
    iget-object v1, p2, LX/DrW;->c:Ljava/lang/String;

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 600286
    check-cast p1, LX/DrP;

    .line 600287
    iget-object v0, p1, LX/DrP;->a:Lcom/facebook/notifications/settings/data/NotifOptionNode;

    .line 600288
    iget-object p0, v0, Lcom/facebook/notifications/settings/data/NotifOptionNode;->a:LX/BCO;

    move-object v0, p0

    .line 600289
    invoke-interface {v0}, LX/BCO;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/DrP;->a:Lcom/facebook/notifications/settings/data/NotifOptionNode;

    .line 600290
    iget-object p0, v0, Lcom/facebook/notifications/settings/data/NotifOptionNode;->a:LX/BCO;

    move-object v0, p0

    .line 600291
    invoke-interface {v0}, LX/BCO;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;->k()LX/4aa;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/DrP;->a:Lcom/facebook/notifications/settings/data/NotifOptionNode;

    .line 600292
    iget-object p0, v0, Lcom/facebook/notifications/settings/data/NotifOptionNode;->a:LX/BCO;

    move-object v0, p0

    .line 600293
    invoke-interface {v0}, LX/BCO;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;->k()LX/4aa;

    move-result-object v0

    invoke-interface {v0}, LX/4aa;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, LX/DrP;->a:Lcom/facebook/notifications/settings/data/NotifOptionNode;

    .line 600294
    iget-object p0, v0, Lcom/facebook/notifications/settings/data/NotifOptionNode;->a:LX/BCO;

    move-object v0, p0

    .line 600295
    invoke-interface {v0}, LX/BCO;->b()LX/BCN;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/DrP;->a:Lcom/facebook/notifications/settings/data/NotifOptionNode;

    .line 600296
    iget-object p0, v0, Lcom/facebook/notifications/settings/data/NotifOptionNode;->a:LX/BCO;

    move-object v0, p0

    .line 600297
    invoke-interface {v0}, LX/BCO;->b()LX/BCN;

    move-result-object v0

    invoke-interface {v0}, LX/BCN;->b()Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 600298
    check-cast p4, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    .line 600299
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 600300
    return-void
.end method
