.class public Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsWashTextPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/notifications/settings/data/NotifOptionNode;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/widget/text/BetterTextView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static e:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field public final c:LX/33W;

.field private final d:Lcom/facebook/multirow/parts/TextPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 600335
    const v0, 0x7f030c2c

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsWashTextPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/33W;Lcom/facebook/multirow/parts/TextPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 600301
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 600302
    iput-object p1, p0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsWashTextPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 600303
    iput-object p2, p0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsWashTextPartDefinition;->c:LX/33W;

    .line 600304
    iput-object p3, p0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsWashTextPartDefinition;->d:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 600305
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsWashTextPartDefinition;
    .locals 6

    .prologue
    .line 600324
    const-class v1, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsWashTextPartDefinition;

    monitor-enter v1

    .line 600325
    :try_start_0
    sget-object v0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsWashTextPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 600326
    sput-object v2, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsWashTextPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 600327
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 600328
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 600329
    new-instance p0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsWashTextPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/33W;->a(LX/0QB;)LX/33W;

    move-result-object v4

    check-cast v4, LX/33W;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsWashTextPartDefinition;-><init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/33W;Lcom/facebook/multirow/parts/TextPartDefinition;)V

    .line 600330
    move-object v0, p0

    .line 600331
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 600332
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsWashTextPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 600333
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 600334
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 600323
    sget-object v0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsWashTextPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 600313
    check-cast p2, Lcom/facebook/notifications/settings/data/NotifOptionNode;

    check-cast p3, LX/1Pn;

    .line 600314
    iget-object v0, p0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsWashTextPartDefinition;->d:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 600315
    iget-object v1, p2, Lcom/facebook/notifications/settings/data/NotifOptionNode;->a:LX/BCO;

    move-object v1, v1

    .line 600316
    invoke-interface {v1}, LX/BCO;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;->k()LX/4aa;

    move-result-object v1

    invoke-interface {v1}, LX/4aa;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 600317
    iget-object v0, p2, Lcom/facebook/notifications/settings/data/NotifOptionNode;->a:LX/BCO;

    move-object v0, v0

    .line 600318
    invoke-interface {v0}, LX/BCO;->b()LX/BCN;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 600319
    iget-object v0, p2, Lcom/facebook/notifications/settings/data/NotifOptionNode;->a:LX/BCO;

    move-object v0, v0

    .line 600320
    invoke-interface {v0}, LX/BCO;->b()LX/BCN;

    move-result-object v0

    invoke-interface {v0}, LX/BCN;->b()Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 600321
    iget-object v0, p0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsWashTextPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    new-instance v1, LX/Dra;

    invoke-direct {v1, p0, p3, p2}, LX/Dra;-><init>(Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsWashTextPartDefinition;LX/1Pn;Lcom/facebook/notifications/settings/data/NotifOptionNode;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 600322
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 600306
    check-cast p1, Lcom/facebook/notifications/settings/data/NotifOptionNode;

    .line 600307
    iget-object v0, p1, Lcom/facebook/notifications/settings/data/NotifOptionNode;->a:LX/BCO;

    move-object v0, v0

    .line 600308
    invoke-interface {v0}, LX/BCO;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 600309
    iget-object v0, p1, Lcom/facebook/notifications/settings/data/NotifOptionNode;->a:LX/BCO;

    move-object v0, v0

    .line 600310
    invoke-interface {v0}, LX/BCO;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;->k()LX/4aa;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 600311
    iget-object v0, p1, Lcom/facebook/notifications/settings/data/NotifOptionNode;->a:LX/BCO;

    move-object v0, v0

    .line 600312
    invoke-interface {v0}, LX/BCO;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;->k()LX/4aa;

    move-result-object v0

    invoke-interface {v0}, LX/4aa;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
