.class public Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsHeaderPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/notifications/settings/data/NotifOptionSetNode;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Lcom/facebook/widget/text/BetterTextView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static c:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/multirow/parts/TextPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 600008
    const v0, 0x7f030c28

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsHeaderPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/TextPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 600005
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 600006
    iput-object p1, p0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsHeaderPartDefinition;->b:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 600007
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsHeaderPartDefinition;
    .locals 4

    .prologue
    .line 599994
    const-class v1, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsHeaderPartDefinition;

    monitor-enter v1

    .line 599995
    :try_start_0
    sget-object v0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsHeaderPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 599996
    sput-object v2, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsHeaderPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 599997
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 599998
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 599999
    new-instance p0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsHeaderPartDefinition;-><init>(Lcom/facebook/multirow/parts/TextPartDefinition;)V

    .line 600000
    move-object v0, p0

    .line 600001
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 600002
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsHeaderPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 600003
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 600004
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/notifications/settings/data/NotifOptionSetNode;)Z
    .locals 1

    .prologue
    .line 600009
    iget-object v0, p0, Lcom/facebook/notifications/settings/data/NotifOptionSetNode;->a:LX/BCR;

    move-object v0, v0

    .line 600010
    invoke-interface {v0}, LX/BCR;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 600011
    iget-object v0, p0, Lcom/facebook/notifications/settings/data/NotifOptionSetNode;->a:LX/BCR;

    move-object v0, v0

    .line 600012
    invoke-interface {v0}, LX/BCR;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;->id_()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel$TextModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 600013
    iget-object v0, p0, Lcom/facebook/notifications/settings/data/NotifOptionSetNode;->a:LX/BCR;

    move-object v0, v0

    .line 600014
    invoke-interface {v0}, LX/BCR;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;->id_()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel$TextModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel$TextModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 599993
    sget-object v0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsHeaderPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 599988
    check-cast p2, Lcom/facebook/notifications/settings/data/NotifOptionSetNode;

    .line 599989
    iget-object v0, p0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsHeaderPartDefinition;->b:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 599990
    iget-object v1, p2, Lcom/facebook/notifications/settings/data/NotifOptionSetNode;->a:LX/BCR;

    move-object v1, v1

    .line 599991
    invoke-interface {v1}, LX/BCR;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;->id_()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel$TextModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel$TextModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 599992
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 599987
    check-cast p1, Lcom/facebook/notifications/settings/data/NotifOptionSetNode;

    invoke-static {p1}, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsHeaderPartDefinition;->a(Lcom/facebook/notifications/settings/data/NotifOptionSetNode;)Z

    move-result v0

    return v0
.end method
