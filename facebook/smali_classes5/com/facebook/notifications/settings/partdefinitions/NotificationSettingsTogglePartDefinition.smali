.class public Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsTogglePartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/notifications/settings/data/NotifOptionSetNode;",
        "LX/DrZ;",
        "TE;",
        "Lcom/facebook/fig/listitem/FigListItem;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static d:LX/0Xm;


# instance fields
.field public final b:LX/33W;

.field public final c:Lcom/facebook/multirow/parts/FigListItemTitlePartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 600204
    const v0, 0x7f030c2b

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsTogglePartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/33W;Lcom/facebook/multirow/parts/FigListItemTitlePartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 600205
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 600206
    iput-object p1, p0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsTogglePartDefinition;->b:LX/33W;

    .line 600207
    iput-object p2, p0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsTogglePartDefinition;->c:Lcom/facebook/multirow/parts/FigListItemTitlePartDefinition;

    .line 600208
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsTogglePartDefinition;
    .locals 5

    .prologue
    .line 600155
    const-class v1, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsTogglePartDefinition;

    monitor-enter v1

    .line 600156
    :try_start_0
    sget-object v0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsTogglePartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 600157
    sput-object v2, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsTogglePartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 600158
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 600159
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 600160
    new-instance p0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsTogglePartDefinition;

    invoke-static {v0}, LX/33W;->a(LX/0QB;)LX/33W;

    move-result-object v3

    check-cast v3, LX/33W;

    invoke-static {v0}, Lcom/facebook/multirow/parts/FigListItemTitlePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/FigListItemTitlePartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/FigListItemTitlePartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsTogglePartDefinition;-><init>(LX/33W;Lcom/facebook/multirow/parts/FigListItemTitlePartDefinition;)V

    .line 600161
    move-object v0, p0

    .line 600162
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 600163
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsTogglePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 600164
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 600165
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 600203
    sget-object v0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsTogglePartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final bridge synthetic a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 600209
    check-cast p2, Lcom/facebook/notifications/settings/data/NotifOptionSetNode;

    check-cast p3, LX/1Pn;

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 600210
    check-cast p3, LX/1Pr;

    new-instance v0, LX/Drb;

    .line 600211
    iget-object v1, p2, Lcom/facebook/notifications/settings/data/NotifOptionSetNode;->a:LX/BCR;

    move-object v1, v1

    .line 600212
    invoke-interface {v1}, LX/BCR;->b()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/Drb;-><init>(Ljava/lang/String;)V

    invoke-interface {p3, v0, p2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Drc;

    .line 600213
    iget-object v1, p2, Lcom/facebook/notifications/settings/data/NotifOptionSetNode;->a:LX/BCR;

    move-object v1, v1

    .line 600214
    invoke-interface {v1}, LX/BCR;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;->b()Ljava/lang/String;

    move-result-object v7

    .line 600215
    iget-object v1, p2, Lcom/facebook/notifications/settings/data/NotifOptionSetNode;->a:LX/BCR;

    move-object v1, v1

    .line 600216
    invoke-interface {v1}, LX/BCR;->c()LX/BCP;

    move-result-object v1

    invoke-interface {v1}, LX/BCP;->a()LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    move v6, v5

    move-object v3, v2

    .line 600217
    :goto_0
    if-ge v6, v9, :cond_4

    invoke-virtual {v8, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/BCO;

    .line 600218
    invoke-interface {v1}, LX/BCO;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;

    move-result-object v10

    invoke-virtual {v10}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;->e()Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    move-result-object v10

    .line 600219
    sget-object v11, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->TOGGLE_ON:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    invoke-virtual {v11, v10}, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    move-object v3, v1

    .line 600220
    :cond_0
    :goto_1
    iget-boolean v11, v0, LX/Drc;->b:Z

    move v11, v11

    .line 600221
    if-nez v11, :cond_1

    invoke-interface {v1}, LX/BCO;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 600222
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->TOGGLE_ON:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    if-ne v1, v10, :cond_3

    move v1, v4

    .line 600223
    :goto_2
    iput-boolean v1, v0, LX/Drc;->a:Z

    .line 600224
    iput-boolean v4, v0, LX/Drc;->b:Z

    .line 600225
    :cond_1
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    goto :goto_0

    .line 600226
    :cond_2
    sget-object v11, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->TOGGLE_OFF:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    invoke-virtual {v11, v10}, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    move-object v2, v1

    .line 600227
    goto :goto_1

    :cond_3
    move v1, v5

    .line 600228
    goto :goto_2

    .line 600229
    :cond_4
    iget-boolean v1, v0, LX/Drc;->a:Z

    move v1, v1

    .line 600230
    if-eqz v1, :cond_5

    move-object v1, v3

    .line 600231
    :goto_3
    invoke-interface {v1}, LX/BCO;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;

    move-result-object v4

    .line 600232
    iget-object v5, p0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsTogglePartDefinition;->c:Lcom/facebook/multirow/parts/FigListItemTitlePartDefinition;

    new-instance v6, LX/8Cn;

    invoke-virtual {v4}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;->k()LX/4aa;

    move-result-object v4

    invoke-interface {v4}, LX/4aa;->a()Ljava/lang/String;

    move-result-object v4

    const/4 v7, 0x2

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-direct {v6, v4, v7}, LX/8Cn;-><init>(Ljava/lang/CharSequence;Ljava/lang/Integer;)V

    invoke-interface {p1, v5, v6}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 600233
    new-instance v1, LX/DrZ;

    invoke-direct {v1, v3, v2, v0}, LX/DrZ;-><init>(LX/BCO;LX/BCO;LX/Drc;)V

    move-object v0, v1

    .line 600234
    return-object v0

    :cond_5
    move-object v1, v2

    .line 600235
    goto :goto_3
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x4ebea079

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 600188
    check-cast p2, LX/DrZ;

    check-cast p3, LX/1Pn;

    check-cast p4, Lcom/facebook/fig/listitem/FigListItem;

    .line 600189
    iget-object v1, p2, LX/DrZ;->c:LX/Drc;

    .line 600190
    iget-boolean v2, v1, LX/Drc;->a:Z

    move v2, v2

    .line 600191
    if-eqz v2, :cond_0

    iget-object v1, p2, LX/DrZ;->a:LX/BCO;

    invoke-interface {v1}, LX/BCO;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel$ImageSourceModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel$ImageSourceModel;->a()Ljava/lang/String;

    move-result-object v1

    .line 600192
    :goto_0
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 600193
    const/4 v1, 0x0

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setShowThumbnail(Z)V

    .line 600194
    :goto_1
    const/4 v1, 0x4

    invoke-virtual {p4, v1}, Lcom/facebook/fig/listitem/FigListItem;->setThumbnailSizeType(I)V

    .line 600195
    const/16 v1, 0x11

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailGravity(I)V

    .line 600196
    invoke-virtual {p4, v2}, Lcom/facebook/fig/listitem/FigListItem;->setActionState(Z)V

    .line 600197
    new-instance v1, LX/DrY;

    invoke-direct {v1, p0, p4, p2, p3}, LX/DrY;-><init>(Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsTogglePartDefinition;Lcom/facebook/fig/listitem/FigListItem;LX/DrZ;LX/1Pn;)V

    move-object v1, v1

    .line 600198
    invoke-virtual {p4, v1}, Lcom/facebook/fig/listitem/FigListItem;->setActionOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 600199
    const/16 v1, 0x1f

    const v2, -0x614a145f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 600200
    :cond_0
    iget-object v1, p2, LX/DrZ;->b:LX/BCO;

    invoke-interface {v1}, LX/BCO;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel$ImageSourceModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel$ImageSourceModel;->a()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 600201
    :cond_1
    const/4 p1, 0x1

    invoke-virtual {p4, p1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setShowThumbnail(Z)V

    .line 600202
    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 12

    .prologue
    .line 600169
    check-cast p1, Lcom/facebook/notifications/settings/data/NotifOptionSetNode;

    const/4 v2, 0x1

    const/4 v5, 0x0

    .line 600170
    iget-object v0, p1, Lcom/facebook/notifications/settings/data/NotifOptionSetNode;->a:LX/BCR;

    move-object v0, v0

    .line 600171
    invoke-interface {v0}, LX/BCR;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;

    move-result-object v7

    .line 600172
    if-eqz v7, :cond_0

    invoke-virtual {v7}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 600173
    iget-object v0, p1, Lcom/facebook/notifications/settings/data/NotifOptionSetNode;->a:LX/BCR;

    move-object v0, v0

    .line 600174
    invoke-interface {v0}, LX/BCR;->c()LX/BCP;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 600175
    iget-object v0, p1, Lcom/facebook/notifications/settings/data/NotifOptionSetNode;->a:LX/BCR;

    move-object v0, v0

    .line 600176
    invoke-interface {v0}, LX/BCR;->c()LX/BCP;

    move-result-object v0

    invoke-interface {v0}, LX/BCP;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    :cond_0
    move v2, v5

    .line 600177
    :cond_1
    :goto_0
    return v2

    .line 600178
    :cond_2
    iget-object v0, p1, Lcom/facebook/notifications/settings/data/NotifOptionSetNode;->a:LX/BCR;

    move-object v0, v0

    .line 600179
    invoke-interface {v0}, LX/BCR;->c()LX/BCP;

    move-result-object v0

    invoke-interface {v0}, LX/BCP;->a()LX/0Px;

    move-result-object v8

    .line 600180
    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    move v6, v5

    move v4, v5

    move v3, v5

    move v1, v5

    :goto_1
    if-ge v6, v9, :cond_7

    invoke-virtual {v8, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BCO;

    .line 600181
    sget-object v10, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->TOGGLE_ON:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    invoke-interface {v0}, LX/BCO;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;

    move-result-object v11

    invoke-virtual {v11}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;->e()Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    move v1, v2

    .line 600182
    :cond_3
    sget-object v10, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->TOGGLE_OFF:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    invoke-interface {v0}, LX/BCO;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;

    move-result-object v11

    invoke-virtual {v11}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;->e()Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    move v3, v2

    .line 600183
    :cond_4
    invoke-interface {v0}, LX/BCO;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;

    move-result-object v10

    if-eqz v10, :cond_5

    invoke-interface {v0}, LX/BCO;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;

    move-result-object v10

    invoke-virtual {v10}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;->k()LX/4aa;

    move-result-object v10

    if-eqz v10, :cond_5

    invoke-interface {v0}, LX/BCO;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;

    move-result-object v10

    invoke-virtual {v10}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;->k()LX/4aa;

    move-result-object v10

    invoke-interface {v10}, LX/4aa;->a()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_5

    invoke-interface {v0}, LX/BCO;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;

    move-result-object v10

    invoke-virtual {v10}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel$ImageSourceModel;

    move-result-object v10

    if-eqz v10, :cond_5

    invoke-interface {v0}, LX/BCO;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;

    move-result-object v10

    invoke-virtual {v10}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel$ImageSourceModel;

    move-result-object v10

    invoke-virtual {v10}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel$ImageSourceModel;->a()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_6

    :cond_5
    move v2, v5

    .line 600184
    goto :goto_0

    .line 600185
    :cond_6
    invoke-interface {v0}, LX/BCO;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;->b()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    move v0, v2

    .line 600186
    :goto_2
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    move v4, v0

    goto :goto_1

    .line 600187
    :cond_7
    if-eqz v1, :cond_8

    if-eqz v3, :cond_8

    if-nez v4, :cond_1

    :cond_8
    move v2, v5

    goto/16 :goto_0

    :cond_9
    move v0, v4

    goto :goto_2
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 600166
    check-cast p4, Lcom/facebook/fig/listitem/FigListItem;

    .line 600167
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Lcom/facebook/fig/listitem/FigListItem;->setActionOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 600168
    return-void
.end method
