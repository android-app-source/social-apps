.class public Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsGapPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static b:LX/0Xm;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 599971
    const v0, 0x7f030c27

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsGapPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 599972
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 599973
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsGapPartDefinition;
    .locals 3

    .prologue
    .line 599974
    const-class v1, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsGapPartDefinition;

    monitor-enter v1

    .line 599975
    :try_start_0
    sget-object v0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsGapPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 599976
    sput-object v2, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsGapPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 599977
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 599978
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 599979
    new-instance v0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsGapPartDefinition;

    invoke-direct {v0}, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsGapPartDefinition;-><init>()V

    .line 599980
    move-object v0, v0

    .line 599981
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 599982
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsGapPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 599983
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 599984
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 599985
    sget-object v0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsGapPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 599986
    const/4 v0, 0x1

    return v0
.end method
