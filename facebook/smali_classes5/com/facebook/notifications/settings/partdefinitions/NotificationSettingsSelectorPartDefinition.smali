.class public Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsSelectorPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pn;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/DrQ;",
        "Ljava/lang/String;",
        "TE;",
        "Lcom/facebook/fbui/widget/contentview/CheckedContentView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/contentview/CheckedContentView;",
            ">;"
        }
    .end annotation
.end field

.field private static g:LX/0Xm;


# instance fields
.field public final b:LX/33W;

.field private final c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field public final d:LX/0wM;

.field private final e:Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

.field private final f:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 600085
    const v0, 0x7f030c29

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsSelectorPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/33W;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/0wM;Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 600086
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 600087
    iput-object p1, p0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsSelectorPartDefinition;->b:LX/33W;

    .line 600088
    iput-object p2, p0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsSelectorPartDefinition;->c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 600089
    iput-object p3, p0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsSelectorPartDefinition;->d:LX/0wM;

    .line 600090
    iput-object p4, p0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsSelectorPartDefinition;->e:Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    .line 600091
    iput-object p5, p0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsSelectorPartDefinition;->f:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    .line 600092
    return-void
.end method

.method private a(LX/Drj;LX/1Pr;Lcom/facebook/notifications/settings/data/NotifOptionNode;LX/Dq2;)Landroid/view/View$OnClickListener;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Drj;",
            "TE;",
            "Lcom/facebook/notifications/settings/data/NotifOptionNode;",
            "LX/Dq2;",
            ")",
            "Landroid/view/View$OnClickListener;"
        }
    .end annotation

    .prologue
    .line 600093
    new-instance v0, LX/DrS;

    move-object v1, p0

    move-object v2, p3

    move-object v3, p1

    move-object v4, p2

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/DrS;-><init>(Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsSelectorPartDefinition;Lcom/facebook/notifications/settings/data/NotifOptionNode;LX/Drj;LX/1Pr;LX/Dq2;)V

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsSelectorPartDefinition;
    .locals 9

    .prologue
    .line 600094
    const-class v1, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsSelectorPartDefinition;

    monitor-enter v1

    .line 600095
    :try_start_0
    sget-object v0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsSelectorPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 600096
    sput-object v2, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsSelectorPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 600097
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 600098
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 600099
    new-instance v3, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsSelectorPartDefinition;

    invoke-static {v0}, LX/33W;->a(LX/0QB;)LX/33W;

    move-result-object v4

    check-cast v4, LX/33W;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v6

    check-cast v6, LX/0wM;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsSelectorPartDefinition;-><init>(LX/33W;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/0wM;Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;)V

    .line 600100
    move-object v0, v3

    .line 600101
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 600102
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsSelectorPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 600103
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 600104
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/contentview/CheckedContentView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 600105
    sget-object v0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsSelectorPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 600106
    check-cast p2, LX/DrQ;

    check-cast p3, LX/1Pr;

    const/4 v1, 0x0

    .line 600107
    iget-object v0, p2, LX/DrQ;->a:Lcom/facebook/notifications/settings/data/NotifOptionNode;

    .line 600108
    iget-object v2, v0, Lcom/facebook/notifications/settings/data/NotifOptionNode;->a:LX/BCO;

    move-object v0, v2

    .line 600109
    invoke-interface {v0}, LX/BCO;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;

    move-result-object v2

    .line 600110
    invoke-virtual {v2}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;->k()LX/4aa;

    move-result-object v0

    invoke-interface {v0}, LX/4aa;->a()Ljava/lang/String;

    move-result-object v3

    .line 600111
    invoke-virtual {v2}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;->ic_()LX/174;

    move-result-object v0

    if-nez v0, :cond_0

    move-object v0, v1

    .line 600112
    :goto_0
    invoke-virtual {v2}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel$ImageSourceModel;

    move-result-object v4

    if-nez v4, :cond_1

    move-object v6, v1

    .line 600113
    :goto_1
    iget-object v1, p0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsSelectorPartDefinition;->f:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    invoke-interface {p1, v1, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 600114
    iget-object v1, p0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsSelectorPartDefinition;->e:Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 600115
    iget-boolean v0, p2, LX/DrQ;->b:Z

    if-eqz v0, :cond_2

    .line 600116
    iget-object v0, p2, LX/DrQ;->c:LX/Dre;

    move-object v2, v0

    .line 600117
    check-cast v2, LX/Drj;

    .line 600118
    iget-object v0, v2, LX/Drj;->a:Ljava/lang/String;

    move-object v3, v0

    .line 600119
    iget-object v7, p0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsSelectorPartDefinition;->c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    iget-object v8, p2, LX/DrQ;->a:Lcom/facebook/notifications/settings/data/NotifOptionNode;

    new-instance v0, LX/DrR;

    move-object v1, p0

    move-object v4, p3

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, LX/DrR;-><init>(Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsSelectorPartDefinition;LX/Drj;Ljava/lang/String;LX/1Pr;LX/DrQ;)V

    invoke-direct {p0, v2, p3, v8, v0}, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsSelectorPartDefinition;->a(LX/Drj;LX/1Pr;Lcom/facebook/notifications/settings/data/NotifOptionNode;LX/Dq2;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-interface {p1, v7, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 600120
    :goto_2
    return-object v6

    .line 600121
    :cond_0
    invoke-virtual {v2}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;->ic_()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 600122
    :cond_1
    invoke-virtual {v2}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel$ImageSourceModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel$ImageSourceModel;->a()Ljava/lang/String;

    move-result-object v1

    move-object v6, v1

    goto :goto_1

    .line 600123
    :cond_2
    iget-object v0, p2, LX/DrQ;->c:LX/Dre;

    move-object v0, v0

    .line 600124
    check-cast v0, LX/Drf;

    .line 600125
    iget-object v1, p0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsSelectorPartDefinition;->c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    iget-object v2, p2, LX/DrQ;->a:Lcom/facebook/notifications/settings/data/NotifOptionNode;

    .line 600126
    new-instance v3, LX/DrT;

    invoke-direct {v3, p0, v2, p3, v0}, LX/DrT;-><init>(Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsSelectorPartDefinition;Lcom/facebook/notifications/settings/data/NotifOptionNode;LX/1Pr;LX/Drf;)V

    move-object v0, v3

    .line 600127
    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    goto :goto_2
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x8926b4a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 600128
    check-cast p1, LX/DrQ;

    check-cast p2, Ljava/lang/String;

    check-cast p3, LX/1Pr;

    check-cast p4, Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    .line 600129
    invoke-static {p2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 600130
    const/4 v1, 0x0

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setShowThumbnail(Z)V

    .line 600131
    :goto_0
    sget-object v1, LX/6VF;->SMALL:LX/6VF;

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setThumbnailSize(LX/6VF;)V

    .line 600132
    iget-object v1, p1, LX/DrQ;->a:Lcom/facebook/notifications/settings/data/NotifOptionNode;

    .line 600133
    iget-object v2, v1, Lcom/facebook/notifications/settings/data/NotifOptionNode;->a:LX/BCO;

    move-object v1, v2

    .line 600134
    invoke-interface {v1}, LX/BCO;->c()Ljava/lang/String;

    move-result-object v1

    .line 600135
    iget-object v2, p1, LX/DrQ;->c:LX/Dre;

    move-object v2, v2

    .line 600136
    invoke-interface {v2, v1}, LX/Dre;->a(Ljava/lang/String;)Z

    move-result v2

    .line 600137
    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setTag(Ljava/lang/Object;)V

    .line 600138
    invoke-virtual {p4, v2}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setChecked(Z)V

    .line 600139
    check-cast p3, LX/1Pn;

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 600140
    if-eqz v2, :cond_1

    .line 600141
    iget-object p1, p0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsSelectorPartDefinition;->d:LX/0wM;

    const p2, 0x7f0207f2

    invoke-static {v1, p2}, LX/1ED;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object p2

    const p3, 0x7f0a00d9

    invoke-static {v1, p3}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result p3

    invoke-virtual {p1, p2, p3}, LX/0wM;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    .line 600142
    :goto_1
    move-object v1, p1

    .line 600143
    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setCheckMarkDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 600144
    const/16 v1, 0x1f

    const v2, -0x50b9fdc5

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 600145
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setShowThumbnail(Z)V

    .line 600146
    invoke-virtual {p4, p2}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsSelectorPartDefinition;->d:LX/0wM;

    const p2, 0x7f0207f3

    invoke-static {v1, p2}, LX/1ED;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object p2

    const p3, 0x7f0a00fb

    invoke-static {v1, p3}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result p3

    invoke-virtual {p1, p2, p3}, LX/0wM;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 600147
    check-cast p1, LX/DrQ;

    .line 600148
    iget-object v0, p1, LX/DrQ;->a:Lcom/facebook/notifications/settings/data/NotifOptionNode;

    .line 600149
    iget-object p0, v0, Lcom/facebook/notifications/settings/data/NotifOptionNode;->a:LX/BCO;

    move-object v0, p0

    .line 600150
    invoke-interface {v0}, LX/BCO;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/DrQ;->a:Lcom/facebook/notifications/settings/data/NotifOptionNode;

    .line 600151
    iget-object p0, v0, Lcom/facebook/notifications/settings/data/NotifOptionNode;->a:LX/BCO;

    move-object v0, p0

    .line 600152
    invoke-interface {v0}, LX/BCO;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;->k()LX/4aa;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/DrQ;->a:Lcom/facebook/notifications/settings/data/NotifOptionNode;

    .line 600153
    iget-object p0, v0, Lcom/facebook/notifications/settings/data/NotifOptionNode;->a:LX/BCO;

    move-object v0, p0

    .line 600154
    invoke-interface {v0}, LX/BCO;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;->k()LX/4aa;

    move-result-object v0

    invoke-interface {v0}, LX/4aa;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
