.class public Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/notifications/settings/data/NotifOptionNode;",
        "LX/DrO;",
        "TE;",
        "Lcom/facebook/fig/listitem/FigListItem;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static g:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final c:Lcom/facebook/multirow/parts/FigListItemContentDescriptionPartDefinition;

.field private final d:Lcom/facebook/multirow/parts/FigListItemMetaTextPartDefinition;

.field public final e:LX/33W;

.field private final f:Lcom/facebook/multirow/parts/FigListItemTitlePartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 600015
    new-instance v0, LX/3ZT;

    invoke-direct {v0}, LX/3ZT;-><init>()V

    sput-object v0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/FigListItemContentDescriptionPartDefinition;Lcom/facebook/multirow/parts/FigListItemMetaTextPartDefinition;LX/33W;Lcom/facebook/multirow/parts/FigListItemTitlePartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 600016
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 600017
    iput-object p1, p0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 600018
    iput-object p2, p0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsPartDefinition;->c:Lcom/facebook/multirow/parts/FigListItemContentDescriptionPartDefinition;

    .line 600019
    iput-object p3, p0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsPartDefinition;->d:Lcom/facebook/multirow/parts/FigListItemMetaTextPartDefinition;

    .line 600020
    iput-object p4, p0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsPartDefinition;->e:LX/33W;

    .line 600021
    iput-object p5, p0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsPartDefinition;->f:Lcom/facebook/multirow/parts/FigListItemTitlePartDefinition;

    .line 600022
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsPartDefinition;
    .locals 9

    .prologue
    .line 600023
    const-class v1, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsPartDefinition;

    monitor-enter v1

    .line 600024
    :try_start_0
    sget-object v0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 600025
    sput-object v2, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 600026
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 600027
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 600028
    new-instance v3, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/FigListItemContentDescriptionPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/FigListItemContentDescriptionPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/FigListItemContentDescriptionPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/FigListItemMetaTextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/FigListItemMetaTextPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/FigListItemMetaTextPartDefinition;

    invoke-static {v0}, LX/33W;->a(LX/0QB;)LX/33W;

    move-result-object v7

    check-cast v7, LX/33W;

    invoke-static {v0}, Lcom/facebook/multirow/parts/FigListItemTitlePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/FigListItemTitlePartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/multirow/parts/FigListItemTitlePartDefinition;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsPartDefinition;-><init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/FigListItemContentDescriptionPartDefinition;Lcom/facebook/multirow/parts/FigListItemMetaTextPartDefinition;LX/33W;Lcom/facebook/multirow/parts/FigListItemTitlePartDefinition;)V

    .line 600029
    move-object v0, v3

    .line 600030
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 600031
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 600032
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 600033
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 600034
    sget-object v0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 600035
    check-cast p2, Lcom/facebook/notifications/settings/data/NotifOptionNode;

    check-cast p3, LX/1Pn;

    const/4 v1, 0x0

    .line 600036
    iget-object v0, p2, Lcom/facebook/notifications/settings/data/NotifOptionNode;->a:LX/BCO;

    move-object v0, v0

    .line 600037
    invoke-interface {v0}, LX/BCO;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;

    move-result-object v3

    .line 600038
    invoke-virtual {v3}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;->k()LX/4aa;

    move-result-object v0

    invoke-interface {v0}, LX/4aa;->a()Ljava/lang/String;

    move-result-object v4

    .line 600039
    invoke-virtual {v3}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;->ic_()LX/174;

    move-result-object v0

    if-nez v0, :cond_2

    move-object v0, v1

    .line 600040
    :goto_0
    invoke-virtual {v3}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;->ib_()LX/174;

    move-result-object v2

    if-nez v2, :cond_3

    move-object v2, v1

    .line 600041
    :goto_1
    invoke-virtual {v3}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel$ImageSourceModel;

    move-result-object v5

    if-nez v5, :cond_5

    move-object v3, v1

    .line 600042
    :goto_2
    iget-object v5, p0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsPartDefinition;->f:Lcom/facebook/multirow/parts/FigListItemTitlePartDefinition;

    new-instance v6, LX/8Cn;

    invoke-direct {v6, v4, v1}, LX/8Cn;-><init>(Ljava/lang/CharSequence;Ljava/lang/Integer;)V

    invoke-interface {p1, v5, v6}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 600043
    iget-object v5, p0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsPartDefinition;->d:Lcom/facebook/multirow/parts/FigListItemMetaTextPartDefinition;

    new-instance v6, LX/8Cm;

    invoke-direct {v6, v0, v1}, LX/8Cm;-><init>(Ljava/lang/CharSequence;Ljava/lang/Integer;)V

    invoke-interface {p1, v5, v6}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 600044
    iget-object v1, p0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsPartDefinition;->c:Lcom/facebook/multirow/parts/FigListItemContentDescriptionPartDefinition;

    .line 600045
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 600046
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 600047
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, " "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 600048
    :cond_0
    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 600049
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, " "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 600050
    :cond_1
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v0, v5

    .line 600051
    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 600052
    iget-object v0, p0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    new-instance v1, LX/DrN;

    invoke-direct {v1, p0, p3, p2}, LX/DrN;-><init>(Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsPartDefinition;LX/1Pn;Lcom/facebook/notifications/settings/data/NotifOptionNode;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 600053
    new-instance v0, LX/DrO;

    invoke-direct {v0, v3, v2}, LX/DrO;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    .line 600054
    :cond_2
    invoke-virtual {v3}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;->ic_()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 600055
    :cond_3
    invoke-virtual {v3}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;->ib_()LX/174;

    move-result-object v2

    invoke-interface {v2}, LX/174;->a()Ljava/lang/String;

    move-result-object v2

    const/16 v6, 0x8

    .line 600056
    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 600057
    :cond_4
    :goto_3
    move-object v2, v2

    .line 600058
    goto/16 :goto_1

    .line 600059
    :cond_5
    invoke-virtual {v3}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel$ImageSourceModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel$ImageSourceModel;->a()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_2

    .line 600060
    :cond_6
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    if-le v5, v6, :cond_4

    .line 600061
    const/4 v5, 0x0

    invoke-virtual {v2, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 600062
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/16 v6, 0x2026

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_3
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x6d2cbcd1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 600063
    check-cast p2, LX/DrO;

    check-cast p4, Lcom/facebook/fig/listitem/FigListItem;

    .line 600064
    iget-object v1, p2, LX/DrO;->a:Ljava/lang/String;

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 600065
    const/4 v1, 0x0

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setShowThumbnail(Z)V

    .line 600066
    :goto_0
    const/4 v1, 0x4

    invoke-virtual {p4, v1}, Lcom/facebook/fig/listitem/FigListItem;->setThumbnailSizeType(I)V

    .line 600067
    const/16 v1, 0x11

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailGravity(I)V

    .line 600068
    iget-object v1, p2, LX/DrO;->b:Ljava/lang/String;

    invoke-virtual {p4, v1}, Lcom/facebook/fig/listitem/FigListItem;->setActionText(Ljava/lang/CharSequence;)V

    .line 600069
    const/16 v1, 0x1f

    const v2, 0x17236a6e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 600070
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setShowThumbnail(Z)V

    .line 600071
    iget-object v1, p2, LX/DrO;->a:Ljava/lang/String;

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 600072
    check-cast p1, Lcom/facebook/notifications/settings/data/NotifOptionNode;

    .line 600073
    iget-object v0, p1, Lcom/facebook/notifications/settings/data/NotifOptionNode;->a:LX/BCO;

    move-object v0, v0

    .line 600074
    invoke-interface {v0}, LX/BCO;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 600075
    iget-object v0, p1, Lcom/facebook/notifications/settings/data/NotifOptionNode;->a:LX/BCO;

    move-object v0, v0

    .line 600076
    invoke-interface {v0}, LX/BCO;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;->k()LX/4aa;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 600077
    iget-object v0, p1, Lcom/facebook/notifications/settings/data/NotifOptionNode;->a:LX/BCO;

    move-object v0, v0

    .line 600078
    invoke-interface {v0}, LX/BCO;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;->k()LX/4aa;

    move-result-object v0

    invoke-interface {v0}, LX/4aa;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 600079
    iget-object v0, p1, Lcom/facebook/notifications/settings/data/NotifOptionNode;->a:LX/BCO;

    move-object v0, v0

    .line 600080
    invoke-interface {v0}, LX/BCO;->b()LX/BCN;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 600081
    iget-object v0, p1, Lcom/facebook/notifications/settings/data/NotifOptionNode;->a:LX/BCO;

    move-object v0, v0

    .line 600082
    invoke-interface {v0}, LX/BCO;->b()LX/BCN;

    move-result-object v0

    invoke-interface {v0}, LX/BCN;->b()Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
