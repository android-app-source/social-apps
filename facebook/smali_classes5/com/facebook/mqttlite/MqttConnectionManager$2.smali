.class public final Lcom/facebook/mqttlite/MqttConnectionManager$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Landroid/util/Pair;

.field public final synthetic b:Ljava/lang/Boolean;

.field public final synthetic c:Ljava/lang/Integer;

.field public final synthetic d:LX/1tA;


# direct methods
.method public constructor <init>(LX/1tA;Landroid/util/Pair;Ljava/lang/Boolean;Ljava/lang/Integer;)V
    .locals 0

    .prologue
    .line 576306
    iput-object p1, p0, Lcom/facebook/mqttlite/MqttConnectionManager$2;->d:LX/1tA;

    iput-object p2, p0, Lcom/facebook/mqttlite/MqttConnectionManager$2;->a:Landroid/util/Pair;

    iput-object p3, p0, Lcom/facebook/mqttlite/MqttConnectionManager$2;->b:Ljava/lang/Boolean;

    iput-object p4, p0, Lcom/facebook/mqttlite/MqttConnectionManager$2;->c:Ljava/lang/Integer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 12

    .prologue
    const/4 v1, 0x0

    const/4 v8, 0x0

    .line 576307
    :try_start_0
    iget-object v0, p0, Lcom/facebook/mqttlite/MqttConnectionManager$2;->a:Landroid/util/Pair;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/mqttlite/MqttConnectionManager$2;->a:Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    move-object v2, v0

    .line 576308
    :goto_0
    iget-object v0, p0, Lcom/facebook/mqttlite/MqttConnectionManager$2;->a:Landroid/util/Pair;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/mqttlite/MqttConnectionManager$2;->a:Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    .line 576309
    :goto_1
    iget-object v3, p0, Lcom/facebook/mqttlite/MqttConnectionManager$2;->b:Ljava/lang/Boolean;

    iget-object v4, p0, Lcom/facebook/mqttlite/MqttConnectionManager$2;->c:Ljava/lang/Integer;

    const/4 v6, 0x0

    .line 576310
    new-instance v5, LX/1so;

    new-instance v7, LX/1sp;

    invoke-direct {v7}, LX/1sp;-><init>()V

    invoke-direct {v5, v7}, LX/1so;-><init>(LX/1sq;)V

    .line 576311
    invoke-static {v3, v4, v2, v0}, LX/1tU;->b(Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/util/List;Ljava/util/List;)LX/2Z9;

    move-result-object v7

    .line 576312
    new-instance v9, LX/2ZA;

    invoke-direct {v9, v6}, LX/2ZA;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch LX/0BK; {:try_start_0 .. :try_end_0} :catch_0

    .line 576313
    :try_start_1
    invoke-virtual {v5, v9}, LX/1so;->a(LX/1u2;)[B

    move-result-object v9

    .line 576314
    invoke-virtual {v5, v7}, LX/1so;->a(LX/1u2;)[B

    move-result-object v7

    .line 576315
    array-length v5, v9

    array-length v10, v7

    add-int/2addr v5, v10

    invoke-static {v9, v5}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v5

    .line 576316
    const/4 v10, 0x0

    array-length v9, v9

    array-length v11, v7

    invoke-static {v7, v10, v5, v9, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_1
    .catch LX/7H0; {:try_start_1 .. :try_end_1} :catch_1
    .catch LX/0BK; {:try_start_1 .. :try_end_1} :catch_0

    .line 576317
    :goto_2
    :try_start_2
    move-object v3, v5

    .line 576318
    if-nez v3, :cond_3

    .line 576319
    const-string v0, "MqttConnectionManager"

    const-string v1, "Failed to create /t_fs payload"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 576320
    :cond_0
    :goto_3
    return-void

    :cond_1
    move-object v2, v1

    .line 576321
    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 576322
    goto :goto_1

    .line 576323
    :cond_3
    const-string v4, "MqttConnectionManager"

    const-string v5, "send/publish/t_fs; sub=%s, unsub=%s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v2, v6, v7

    const/4 v2, 0x1

    aput-object v0, v6, v2

    invoke-static {v4, v5, v6}, LX/05D;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 576324
    iget-object v2, p0, Lcom/facebook/mqttlite/MqttConnectionManager$2;->d:LX/1tA;

    const-string v4, "/t_fs"

    sget-object v5, LX/0AL;->ACKNOWLEDGED_DELIVERY:LX/0AL;

    iget-object v0, p0, Lcom/facebook/mqttlite/MqttConnectionManager$2;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    new-instance v0, LX/2ZB;

    invoke-direct {v0, p0}, LX/2ZB;-><init>(Lcom/facebook/mqttlite/MqttConnectionManager$2;)V

    :goto_4
    invoke-virtual {v2, v4, v3, v5, v0}, LX/056;->a(Ljava/lang/String;[BLX/0AL;LX/0AM;)I
    :try_end_2
    .catch LX/0BK; {:try_start_2 .. :try_end_2} :catch_0

    move-result v0

    .line 576325
    if-gez v0, :cond_0

    .line 576326
    :goto_5
    iget-object v0, p0, Lcom/facebook/mqttlite/MqttConnectionManager$2;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 576327
    iget-object v0, p0, Lcom/facebook/mqttlite/MqttConnectionManager$2;->d:LX/1tA;

    iget-object v0, v0, LX/1tA;->t:LX/1tC;

    iget-object v1, p0, Lcom/facebook/mqttlite/MqttConnectionManager$2;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1, v8}, LX/1tC;->a(ZZ)V

    goto :goto_3

    :cond_4
    move-object v0, v1

    .line 576328
    goto :goto_4

    .line 576329
    :catch_0
    move-exception v0

    .line 576330
    const-string v1, "MqttConnectionManager"

    const-string v2, "exception/MqttException"

    new-array v3, v8, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/05D;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_5

    :catch_1
    move-object v5, v6

    goto :goto_2
.end method
