.class public final Lcom/facebook/mqttlite/MqttService$13;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/2gX;

.field public final synthetic b:Lcom/facebook/mqttlite/MqttService;


# direct methods
.method public constructor <init>(Lcom/facebook/mqttlite/MqttService;LX/2gX;)V
    .locals 0

    .prologue
    .line 576410
    iput-object p1, p0, Lcom/facebook/mqttlite/MqttService$13;->b:Lcom/facebook/mqttlite/MqttService;

    iput-object p2, p0, Lcom/facebook/mqttlite/MqttService$13;->a:LX/2gX;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 13

    .prologue
    .line 576411
    :try_start_0
    iget-object v0, p0, Lcom/facebook/mqttlite/MqttService$13;->a:LX/2gX;

    .line 576412
    iget-wide v11, v0, LX/2gX;->c:J

    move-wide v2, v11

    .line 576413
    iget-object v0, p0, Lcom/facebook/mqttlite/MqttService$13;->a:LX/2gX;

    .line 576414
    iget-object v1, v0, LX/2gX;->a:Ljava/lang/String;

    move-object v1, v1

    .line 576415
    iget-object v0, p0, Lcom/facebook/mqttlite/MqttService$13;->a:LX/2gX;

    .line 576416
    iget-object v4, v0, LX/2gX;->b:[B

    move-object v4, v4

    .line 576417
    iget-object v0, p0, Lcom/facebook/mqttlite/MqttService$13;->b:Lcom/facebook/mqttlite/MqttService;

    .line 576418
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/mqttlite/MqttService;->a$redex0(Lcom/facebook/mqttlite/MqttService;Ljava/lang/String;J)V

    .line 576419
    iget-object v0, p0, Lcom/facebook/mqttlite/MqttService$13;->b:Lcom/facebook/mqttlite/MqttService;

    iget-object v0, v0, Lcom/facebook/mqttlite/MqttService;->t:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1fT;

    .line 576420
    iget-object v6, p0, Lcom/facebook/mqttlite/MqttService$13;->b:Lcom/facebook/mqttlite/MqttService;

    iget-object v6, v6, LX/051;->f:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v6}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v6

    .line 576421
    invoke-interface {v0, v1, v4, v2, v3}, LX/1fT;->onMessage(Ljava/lang/String;[BJ)V

    .line 576422
    iget-object v8, p0, Lcom/facebook/mqttlite/MqttService$13;->b:Lcom/facebook/mqttlite/MqttService;

    iget-object v8, v8, LX/051;->f:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v8}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v8

    .line 576423
    cmp-long v10, v6, v8

    if-eqz v10, :cond_0

    .line 576424
    iget-object v10, p0, Lcom/facebook/mqttlite/MqttService$13;->b:Lcom/facebook/mqttlite/MqttService;

    iget-object v10, v10, Lcom/facebook/mqttlite/MqttService;->s:LX/1tJ;

    iget-object v10, v10, LX/1qk;->a:LX/1ql;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sub-long v6, v8, v6

    invoke-virtual {v10, v0, v6, v7}, LX/1ql;->a(Ljava/lang/String;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 576425
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/facebook/mqttlite/MqttService$13;->b:Lcom/facebook/mqttlite/MqttService;

    iget-object v1, v1, Lcom/facebook/mqttlite/MqttService;->s:LX/1tJ;

    iget-object v1, v1, LX/1qk;->a:LX/1ql;

    invoke-virtual {v1}, LX/1ql;->d()V

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/facebook/mqttlite/MqttService$13;->b:Lcom/facebook/mqttlite/MqttService;

    iget-object v0, v0, Lcom/facebook/mqttlite/MqttService;->s:LX/1tJ;

    iget-object v0, v0, LX/1qk;->a:LX/1ql;

    invoke-virtual {v0}, LX/1ql;->d()V

    .line 576426
    return-void
.end method
