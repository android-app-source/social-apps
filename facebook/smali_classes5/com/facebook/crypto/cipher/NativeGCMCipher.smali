.class public Lcom/facebook/crypto/cipher/NativeGCMCipher;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/crypto/proguard/annotations/DoNotStrip;
.end annotation


# instance fields
.field private a:LX/2X2;

.field private final b:LX/1Hq;

.field private mCtxPtr:J
    .annotation build Lcom/facebook/crypto/proguard/annotations/DoNotStrip;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1Hq;)V
    .locals 1

    .prologue
    .line 576103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 576104
    sget-object v0, LX/2X2;->UNINITIALIZED:LX/2X2;

    iput-object v0, p0, Lcom/facebook/crypto/cipher/NativeGCMCipher;->a:LX/2X2;

    .line 576105
    iput-object p1, p0, Lcom/facebook/crypto/cipher/NativeGCMCipher;->b:LX/1Hq;

    .line 576106
    return-void
.end method

.method private static varargs a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 576123
    const/4 v0, 0x0

    invoke-static {v0, p0, p1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static c()V
    .locals 4

    .prologue
    .line 576115
    new-instance v0, Lcom/facebook/crypto/cipher/NativeGCMCipher;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/facebook/crypto/cipher/NativeGCMCipher;-><init>(LX/1Hq;)V

    .line 576116
    sget-object v1, LX/1Hy;->KEY_256:LX/1Hy;

    .line 576117
    iget v2, v1, LX/1Hy;->keyLength:I

    new-array v2, v2, [B

    .line 576118
    iget v3, v1, LX/1Hy;->ivLength:I

    new-array v3, v3, [B

    .line 576119
    iget v1, v1, LX/1Hy;->tagLength:I

    new-array v1, v1, [B

    .line 576120
    invoke-direct {v0, v2, v3}, Lcom/facebook/crypto/cipher/NativeGCMCipher;->nativeDecryptInit([B[B)I

    .line 576121
    array-length v2, v1

    invoke-direct {v0, v1, v2}, Lcom/facebook/crypto/cipher/NativeGCMCipher;->nativeDecryptFinal([BI)I

    .line 576122
    return-void
.end method

.method private d()V
    .locals 2

    .prologue
    .line 576111
    iget-object v0, p0, Lcom/facebook/crypto/cipher/NativeGCMCipher;->a:LX/2X2;

    sget-object v1, LX/2X2;->DECRYPT_INITIALIZED:LX/2X2;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/crypto/cipher/NativeGCMCipher;->a:LX/2X2;

    sget-object v1, LX/2X2;->ENCRYPT_INITIALIZED:LX/2X2;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 576112
    :goto_0
    const-string v1, "Cipher has not been initialized"

    invoke-static {v0, v1}, LX/2X3;->a(ZLjava/lang/String;)V

    .line 576113
    return-void

    .line 576114
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 576107
    iget-object v0, p0, Lcom/facebook/crypto/cipher/NativeGCMCipher;->a:LX/2X2;

    sget-object v1, LX/2X2;->DECRYPT_FINALIZED:LX/2X2;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/crypto/cipher/NativeGCMCipher;->a:LX/2X2;

    sget-object v1, LX/2X2;->ENCRYPT_FINALIZED:LX/2X2;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 576108
    :goto_0
    const-string v1, "Cipher has not been finalized"

    invoke-static {v0, v1}, LX/2X3;->a(ZLjava/lang/String;)V

    .line 576109
    return-void

    .line 576110
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private native nativeDecryptFinal([BI)I
.end method

.method private native nativeDecryptInit([B[B)I
.end method

.method private native nativeDestroy()I
.end method

.method private native nativeEncryptFinal([BI)I
.end method

.method private native nativeEncryptInit([B[B)I
.end method

.method private static native nativeFailure()I
.end method

.method private native nativeGetCipherBlockSize()I
.end method

.method private native nativeUpdate([BII[BI)I
.end method

.method private native nativeUpdateAad([BI)I
.end method


# virtual methods
.method public final a([BII[BI)I
    .locals 6

    .prologue
    .line 576061
    invoke-direct {p0}, Lcom/facebook/crypto/cipher/NativeGCMCipher;->d()V

    .line 576062
    invoke-direct/range {p0 .. p5}, Lcom/facebook/crypto/cipher/NativeGCMCipher;->nativeUpdate([BII[BI)I

    move-result v0

    .line 576063
    if-gez v0, :cond_0

    .line 576064
    new-instance v1, LX/48e;

    const-string v2, "update: Offset = %d; DataLen = %d; Result = %d"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Lcom/facebook/crypto/cipher/NativeGCMCipher;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LX/48e;-><init>(Ljava/lang/String;)V

    throw v1

    .line 576065
    :cond_0
    return v0
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 576066
    invoke-direct {p0}, Lcom/facebook/crypto/cipher/NativeGCMCipher;->e()V

    .line 576067
    invoke-direct {p0}, Lcom/facebook/crypto/cipher/NativeGCMCipher;->nativeDestroy()I

    move-result v0

    invoke-static {}, Lcom/facebook/crypto/cipher/NativeGCMCipher;->nativeFailure()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 576068
    new-instance v0, LX/48e;

    const-string v1, "destroy"

    invoke-direct {v0, v1}, LX/48e;-><init>(Ljava/lang/String;)V

    throw v0

    .line 576069
    :cond_0
    sget-object v0, LX/2X2;->UNINITIALIZED:LX/2X2;

    iput-object v0, p0, Lcom/facebook/crypto/cipher/NativeGCMCipher;->a:LX/2X2;

    .line 576070
    return-void
.end method

.method public final a([BI)V
    .locals 5

    .prologue
    .line 576071
    invoke-direct {p0}, Lcom/facebook/crypto/cipher/NativeGCMCipher;->d()V

    .line 576072
    invoke-direct {p0, p1, p2}, Lcom/facebook/crypto/cipher/NativeGCMCipher;->nativeUpdateAad([BI)I

    move-result v0

    if-gez v0, :cond_0

    .line 576073
    new-instance v0, LX/48e;

    const-string v1, "updateAAd: DataLen = %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/facebook/crypto/cipher/NativeGCMCipher;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/48e;-><init>(Ljava/lang/String;)V

    throw v0

    .line 576074
    :cond_0
    return-void
.end method

.method public final a([B[B)V
    .locals 2

    .prologue
    .line 576075
    iget-object v0, p0, Lcom/facebook/crypto/cipher/NativeGCMCipher;->a:LX/2X2;

    sget-object v1, LX/2X2;->UNINITIALIZED:LX/2X2;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Cipher has already been initialized"

    invoke-static {v0, v1}, LX/2X3;->a(ZLjava/lang/String;)V

    .line 576076
    iget-object v0, p0, Lcom/facebook/crypto/cipher/NativeGCMCipher;->b:LX/1Hq;

    invoke-virtual {v0}, LX/1Hq;->a()V

    .line 576077
    invoke-direct {p0, p1, p2}, Lcom/facebook/crypto/cipher/NativeGCMCipher;->nativeEncryptInit([B[B)I

    move-result v0

    invoke-static {}, Lcom/facebook/crypto/cipher/NativeGCMCipher;->nativeFailure()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 576078
    new-instance v0, LX/48e;

    const-string v1, "encryptInit"

    invoke-direct {v0, v1}, LX/48e;-><init>(Ljava/lang/String;)V

    throw v0

    .line 576079
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 576080
    :cond_1
    sget-object v0, LX/2X2;->ENCRYPT_INITIALIZED:LX/2X2;

    iput-object v0, p0, Lcom/facebook/crypto/cipher/NativeGCMCipher;->a:LX/2X2;

    .line 576081
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 576082
    invoke-direct {p0}, Lcom/facebook/crypto/cipher/NativeGCMCipher;->d()V

    .line 576083
    invoke-direct {p0}, Lcom/facebook/crypto/cipher/NativeGCMCipher;->nativeGetCipherBlockSize()I

    move-result v0

    return v0
.end method

.method public final b([BI)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 576084
    iget-object v0, p0, Lcom/facebook/crypto/cipher/NativeGCMCipher;->a:LX/2X2;

    sget-object v3, LX/2X2;->ENCRYPT_INITIALIZED:LX/2X2;

    if-ne v0, v3, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Cipher has not been initialized"

    invoke-static {v0, v3}, LX/2X3;->a(ZLjava/lang/String;)V

    .line 576085
    sget-object v0, LX/2X2;->ENCRYPT_FINALIZED:LX/2X2;

    iput-object v0, p0, Lcom/facebook/crypto/cipher/NativeGCMCipher;->a:LX/2X2;

    .line 576086
    invoke-direct {p0, p1, p2}, Lcom/facebook/crypto/cipher/NativeGCMCipher;->nativeEncryptFinal([BI)I

    move-result v0

    invoke-static {}, Lcom/facebook/crypto/cipher/NativeGCMCipher;->nativeFailure()I

    move-result v3

    if-ne v0, v3, :cond_1

    .line 576087
    new-instance v0, LX/48e;

    const-string v3, "encryptFinal: %d"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {v3, v1}, Lcom/facebook/crypto/cipher/NativeGCMCipher;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/48e;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v2

    .line 576088
    goto :goto_0

    .line 576089
    :cond_1
    return-void
.end method

.method public final b([B[B)V
    .locals 2

    .prologue
    .line 576090
    iget-object v0, p0, Lcom/facebook/crypto/cipher/NativeGCMCipher;->a:LX/2X2;

    sget-object v1, LX/2X2;->UNINITIALIZED:LX/2X2;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Cipher has already been initialized"

    invoke-static {v0, v1}, LX/2X3;->a(ZLjava/lang/String;)V

    .line 576091
    iget-object v0, p0, Lcom/facebook/crypto/cipher/NativeGCMCipher;->b:LX/1Hq;

    invoke-virtual {v0}, LX/1Hq;->a()V

    .line 576092
    invoke-direct {p0, p1, p2}, Lcom/facebook/crypto/cipher/NativeGCMCipher;->nativeDecryptInit([B[B)I

    move-result v0

    invoke-static {}, Lcom/facebook/crypto/cipher/NativeGCMCipher;->nativeFailure()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 576093
    new-instance v0, LX/48e;

    const-string v1, "decryptInit"

    invoke-direct {v0, v1}, LX/48e;-><init>(Ljava/lang/String;)V

    throw v0

    .line 576094
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 576095
    :cond_1
    sget-object v0, LX/2X2;->DECRYPT_INITIALIZED:LX/2X2;

    iput-object v0, p0, Lcom/facebook/crypto/cipher/NativeGCMCipher;->a:LX/2X2;

    .line 576096
    return-void
.end method

.method public final c([BI)V
    .locals 2

    .prologue
    .line 576097
    iget-object v0, p0, Lcom/facebook/crypto/cipher/NativeGCMCipher;->a:LX/2X2;

    sget-object v1, LX/2X2;->DECRYPT_INITIALIZED:LX/2X2;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Cipher has not been initialized"

    invoke-static {v0, v1}, LX/2X3;->a(ZLjava/lang/String;)V

    .line 576098
    sget-object v0, LX/2X2;->DECRYPT_FINALIZED:LX/2X2;

    iput-object v0, p0, Lcom/facebook/crypto/cipher/NativeGCMCipher;->a:LX/2X2;

    .line 576099
    invoke-direct {p0, p1, p2}, Lcom/facebook/crypto/cipher/NativeGCMCipher;->nativeDecryptFinal([BI)I

    move-result v0

    invoke-static {}, Lcom/facebook/crypto/cipher/NativeGCMCipher;->nativeFailure()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 576100
    new-instance v0, LX/48e;

    const-string v1, "The message could not be decrypted successfully.It has either been tampered with or the wrong resource is being decrypted."

    invoke-direct {v0, v1}, LX/48e;-><init>(Ljava/lang/String;)V

    throw v0

    .line 576101
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 576102
    :cond_1
    return-void
.end method
