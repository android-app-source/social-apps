.class public Lcom/facebook/crypto/module/LoggedInUserCryptoHybrid;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final mHybridData:Lcom/facebook/jni/HybridData;
    .annotation build Lcom/facebook/crypto/proguard/annotations/DoNotStrip;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 673096
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 673097
    invoke-static {}, Lcom/facebook/crypto/module/LoggedInUserCryptoHybrid;->initHybrid()Lcom/facebook/jni/HybridData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/crypto/module/LoggedInUserCryptoHybrid;->mHybridData:Lcom/facebook/jni/HybridData;

    .line 673098
    return-void
.end method

.method private constructor <init>(Lcom/facebook/jni/HybridData;)V
    .locals 0
    .annotation build Lcom/facebook/crypto/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 673099
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 673100
    iput-object p1, p0, Lcom/facebook/crypto/module/LoggedInUserCryptoHybrid;->mHybridData:Lcom/facebook/jni/HybridData;

    .line 673101
    return-void
.end method

.method public static native get()Lcom/facebook/crypto/module/LoggedInUserCryptoHybrid;
.end method

.method private static native initHybrid()Lcom/facebook/jni/HybridData;
.end method


# virtual methods
.method public native createCipher()Lcom/facebook/cipher/jni/CipherHybrid;
.end method

.method public native setKeyChain(Lcom/facebook/crypto/keychain/KeyChain;)V
.end method

.method public native unsetKeyChain()V
.end method
