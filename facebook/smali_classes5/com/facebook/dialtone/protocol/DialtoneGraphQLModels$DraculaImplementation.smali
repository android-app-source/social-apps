.class public final Lcom/facebook/dialtone/protocol/DialtoneGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/dialtone/protocol/DialtoneGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 674794
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 674795
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 674854
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 674855
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 12

    .prologue
    const/4 v11, 0x3

    const/4 v10, 0x2

    const-wide/16 v4, 0x0

    const/4 v9, 0x1

    const/4 v1, 0x0

    .line 674814
    if-nez p1, :cond_0

    .line 674815
    :goto_0
    return v1

    .line 674816
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 674817
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 674818
    :sswitch_0
    invoke-virtual {p0, p1, v1, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 674819
    invoke-virtual {p0, p1, v9}, LX/15i;->p(II)I

    move-result v0

    .line 674820
    const v6, 0x46733b6

    invoke-static {p0, v0, v6, p3}, Lcom/facebook/dialtone/protocol/DialtoneGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v6

    .line 674821
    invoke-virtual {p0, p1, v10, v1}, LX/15i;->a(III)I

    move-result v7

    .line 674822
    invoke-virtual {p0, p1, v11, v1}, LX/15i;->a(III)I

    move-result v8

    .line 674823
    const/4 v0, 0x4

    invoke-virtual {p3, v0}, LX/186;->c(I)V

    move-object v0, p3

    .line 674824
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 674825
    invoke-virtual {p3, v9, v6}, LX/186;->b(II)V

    .line 674826
    invoke-virtual {p3, v10, v7, v1}, LX/186;->a(III)V

    .line 674827
    invoke-virtual {p3, v11, v8, v1}, LX/186;->a(III)V

    .line 674828
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    .line 674829
    :sswitch_1
    invoke-virtual {p0, p1, v1, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 674830
    invoke-virtual {p0, p1, v9}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 674831
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 674832
    invoke-virtual {p3, v10}, LX/186;->c(I)V

    move-object v0, p3

    .line 674833
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 674834
    invoke-virtual {p3, v9, v6}, LX/186;->b(II)V

    .line 674835
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    .line 674836
    :sswitch_2
    invoke-virtual {p0, p1, v1, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 674837
    invoke-virtual {p0, p1, v9}, LX/15i;->p(II)I

    move-result v0

    .line 674838
    const v6, 0x70d8a764    # 5.36409E29f

    invoke-static {p0, v0, v6, p3}, Lcom/facebook/dialtone/protocol/DialtoneGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v6

    .line 674839
    invoke-virtual {p0, p1, v10, v1}, LX/15i;->a(III)I

    move-result v7

    .line 674840
    invoke-virtual {p0, p1, v11, v1}, LX/15i;->a(III)I

    move-result v8

    .line 674841
    const/4 v0, 0x4

    invoke-virtual {p3, v0}, LX/186;->c(I)V

    move-object v0, p3

    .line 674842
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 674843
    invoke-virtual {p3, v9, v6}, LX/186;->b(II)V

    .line 674844
    invoke-virtual {p3, v10, v7, v1}, LX/186;->a(III)V

    .line 674845
    invoke-virtual {p3, v11, v8, v1}, LX/186;->a(III)V

    .line 674846
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    .line 674847
    :sswitch_3
    invoke-virtual {p0, p1, v1, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 674848
    invoke-virtual {p0, p1, v9}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 674849
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 674850
    invoke-virtual {p3, v10}, LX/186;->c(I)V

    move-object v0, p3

    .line 674851
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 674852
    invoke-virtual {p3, v9, v6}, LX/186;->b(II)V

    .line 674853
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x79de6625 -> :sswitch_0
        -0x4c791d59 -> :sswitch_2
        0x46733b6 -> :sswitch_1
        0x70d8a764 -> :sswitch_3
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/dialtone/protocol/DialtoneGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 674813
    new-instance v0, Lcom/facebook/dialtone/protocol/DialtoneGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/dialtone/protocol/DialtoneGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 674806
    sparse-switch p2, :sswitch_data_0

    .line 674807
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 674808
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 674809
    const v1, 0x46733b6

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/dialtone/protocol/DialtoneGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 674810
    :goto_0
    :sswitch_1
    return-void

    .line 674811
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 674812
    const v1, 0x70d8a764    # 5.36409E29f

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/dialtone/protocol/DialtoneGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x79de6625 -> :sswitch_0
        -0x4c791d59 -> :sswitch_2
        0x46733b6 -> :sswitch_1
        0x70d8a764 -> :sswitch_1
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/186;)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 674796
    if-nez p1, :cond_0

    move v0, v1

    .line 674797
    :goto_0
    return v0

    .line 674798
    :cond_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v2

    .line 674799
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 674800
    :goto_1
    if-ge v1, v2, :cond_2

    .line 674801
    invoke-virtual {p0, p1, v1}, LX/15i;->q(II)I

    move-result v3

    .line 674802
    invoke-static {p0, v3, p2, p3}, Lcom/facebook/dialtone/protocol/DialtoneGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v3

    aput v3, v0, v1

    .line 674803
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 674804
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 674805
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    .line 674787
    if-eqz p1, :cond_0

    .line 674788
    invoke-virtual {p0, p1}, LX/15i;->d(I)I

    move-result v1

    .line 674789
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 674790
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v2

    .line 674791
    invoke-static {p0, v2, p2, p3}, Lcom/facebook/dialtone/protocol/DialtoneGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 674792
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 674793
    :cond_0
    return-void
.end method

.method private static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 674781
    if-eqz p1, :cond_0

    .line 674782
    invoke-static {p0, p1, p2}, Lcom/facebook/dialtone/protocol/DialtoneGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/dialtone/protocol/DialtoneGraphQLModels$DraculaImplementation;

    move-result-object v1

    .line 674783
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/dialtone/protocol/DialtoneGraphQLModels$DraculaImplementation;

    .line 674784
    if-eq v0, v1, :cond_0

    .line 674785
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 674786
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 674780
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/dialtone/protocol/DialtoneGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 674856
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/dialtone/protocol/DialtoneGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 674857
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 674775
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 674776
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 674777
    :cond_0
    iput-object p1, p0, Lcom/facebook/dialtone/protocol/DialtoneGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 674778
    iput p2, p0, Lcom/facebook/dialtone/protocol/DialtoneGraphQLModels$DraculaImplementation;->b:I

    .line 674779
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 674774
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 674773
    new-instance v0, Lcom/facebook/dialtone/protocol/DialtoneGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/dialtone/protocol/DialtoneGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 674749
    iget v0, p0, LX/1vt;->c:I

    .line 674750
    move v0, v0

    .line 674751
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 674770
    iget v0, p0, LX/1vt;->c:I

    .line 674771
    move v0, v0

    .line 674772
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 674767
    iget v0, p0, LX/1vt;->b:I

    .line 674768
    move v0, v0

    .line 674769
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 674764
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 674765
    move-object v0, v0

    .line 674766
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 674755
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 674756
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 674757
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/dialtone/protocol/DialtoneGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 674758
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 674759
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 674760
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 674761
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 674762
    invoke-static {v3, v9, v2}, Lcom/facebook/dialtone/protocol/DialtoneGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/dialtone/protocol/DialtoneGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 674763
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 674752
    iget v0, p0, LX/1vt;->c:I

    .line 674753
    move v0, v0

    .line 674754
    return v0
.end method
