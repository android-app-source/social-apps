.class public Lcom/facebook/omnistore/IndexQuery;
.super Lcom/facebook/jni/Countable;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 0

    .prologue
    .line 572821
    invoke-static {}, LX/2Pp;->ensure()V

    .line 572822
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 572823
    invoke-direct {p0}, Lcom/facebook/jni/Countable;-><init>()V

    .line 572824
    return-void
.end method

.method public static native and(Ljava/util/List;)Lcom/facebook/omnistore/IndexQuery;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/omnistore/IndexQuery;",
            ">;)",
            "Lcom/facebook/omnistore/IndexQuery;"
        }
    .end annotation
.end method

.method public static native or(Ljava/util/List;)Lcom/facebook/omnistore/IndexQuery;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/omnistore/IndexQuery;",
            ">;)",
            "Lcom/facebook/omnistore/IndexQuery;"
        }
    .end annotation
.end method

.method public static native predicate(Ljava/lang/String;Lcom/facebook/omnistore/IndexQuery$QueryOperator;Ljava/lang/String;)Lcom/facebook/omnistore/IndexQuery;
.end method
