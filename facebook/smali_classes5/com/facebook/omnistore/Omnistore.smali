.class public Lcom/facebook/omnistore/Omnistore;
.super Lcom/facebook/jni/Countable;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# instance fields
.field private mIsRemoved:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 0

    .prologue
    .line 572741
    invoke-static {}, LX/2Pp;->ensure()V

    .line 572742
    return-void
.end method

.method private constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 572743
    invoke-direct {p0}, Lcom/facebook/jni/Countable;-><init>()V

    .line 572744
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/omnistore/Omnistore;->mIsRemoved:Z

    .line 572745
    return-void
.end method

.method private static checkDatabaseFile(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 572746
    if-nez p0, :cond_0

    .line 572747
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Null file path"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 572748
    :cond_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v0

    .line 572749
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 572750
    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v1

    if-nez v1, :cond_1

    .line 572751
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Omnistore database file %s exists but is not a regular file"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v5, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 572752
    :cond_1
    invoke-virtual {v0}, Ljava/io/File;->canWrite()Z

    move-result v1

    if-nez v1, :cond_5

    .line 572753
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Don\'t have write access to Omnistore database file %s"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v5, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 572754
    :cond_2
    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v1

    .line 572755
    if-nez v1, :cond_3

    .line 572756
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "The provided database file path (%s) does not seem to have a parent directory"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-static {v5, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 572757
    :cond_3
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_4

    .line 572758
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v2, "Parent directory of Omnistore database file (%s) does not exist"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v1, v3, v4

    invoke-static {v5, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 572759
    :cond_4
    invoke-virtual {v1}, Ljava/io/File;->canWrite()Z

    move-result v0

    if-nez v0, :cond_5

    .line 572760
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v2, "Don\'t have write access to Omnistore database file directory %s"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v1, v3, v4

    invoke-static {v5, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 572761
    :cond_5
    return-void
.end method

.method private native doApplyStoredProcedure(I[BLjava/lang/String;Ljava/lang/String;)V
.end method

.method private native doGetDebugInfo()Ljava/lang/String;
.end method

.method private native doGetSubscriptionCollectionNames()[Ljava/lang/String;
.end method

.method public static native doOpen(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/omnistore/MqttProtocolProvider;Lcom/facebook/omnistore/OmnistoreErrorReporter;Lcom/facebook/xanalytics/XAnalyticsNative;ZZZZZZZZZ)Lcom/facebook/omnistore/Omnistore;
.end method

.method private native doRemove()V
.end method

.method private native doStart()V
.end method

.method private native doSubscribeCollection(Lcom/facebook/omnistore/CollectionName;Ljava/lang/String;Ljava/lang/String;JZ)Lcom/facebook/omnistore/Collection;
.end method

.method private native doUnsubscribeCollection(Lcom/facebook/omnistore/CollectionName;)V
.end method

.method private native doWriteBugReport(Ljava/lang/String;)[Ljava/lang/String;
.end method

.method public static open(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/omnistore/MqttProtocolProvider;Lcom/facebook/omnistore/OmnistoreErrorReporter;Lcom/facebook/xanalytics/XAnalyticsNative;ZZZZZZZZZ)Lcom/facebook/omnistore/Omnistore;
    .locals 1

    .prologue
    .line 572762
    invoke-static {p0}, Lcom/facebook/omnistore/Omnistore;->checkDatabaseFile(Ljava/lang/String;)V

    .line 572763
    invoke-static/range {p0 .. p13}, Lcom/facebook/omnistore/Omnistore;->doOpen(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/omnistore/MqttProtocolProvider;Lcom/facebook/omnistore/OmnistoreErrorReporter;Lcom/facebook/xanalytics/XAnalyticsNative;ZZZZZZZZZ)Lcom/facebook/omnistore/Omnistore;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public native addDeltaReceivedCallback(Lcom/facebook/omnistore/Omnistore$DeltaReceivedCallback;)V
.end method

.method public native addSnapshotStateChangedCallback(Lcom/facebook/omnistore/Omnistore$SnapshotStateChangedCallback;)V
.end method

.method public native addStoredProcedureResultCallback(Lcom/facebook/omnistore/Omnistore$StoredProcedureResultCallback;)V
.end method

.method public native addStoredProcedureResultWithUniqueKeyCallback(Lcom/facebook/omnistore/Omnistore$StoredProcedureResultWithUniqueKeyCallback;)V
.end method

.method public declared-synchronized applyStoredProcedure(I[B)V
    .locals 2

    .prologue
    .line 572699
    monitor-enter p0

    const/4 v0, 0x0

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/facebook/omnistore/Omnistore;->applyStoredProcedure(I[BLjava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 572700
    monitor-exit p0

    return-void

    .line 572701
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized applyStoredProcedure(I[BLjava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 572702
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/facebook/omnistore/Omnistore;->mIsRemoved:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 572703
    :goto_0
    monitor-exit p0

    return-void

    .line 572704
    :cond_0
    :try_start_1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/facebook/omnistore/Omnistore;->doApplyStoredProcedure(I[BLjava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 572705
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public native createCollectionNameBuilder(Ljava/lang/String;)Lcom/facebook/omnistore/CollectionName$Builder;
.end method

.method public declared-synchronized getDebugInfo()Ljava/lang/String;
    .locals 1

    .prologue
    .line 572706
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/facebook/omnistore/Omnistore;->mIsRemoved:Z

    if-eqz v0, :cond_0

    .line 572707
    const-string v0, "Omnistore removed"
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 572708
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    invoke-direct {p0}, Lcom/facebook/omnistore/Omnistore;->doGetDebugInfo()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 572709
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getSubscriptionCollectionNames()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 572710
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/facebook/omnistore/Omnistore;->mIsRemoved:Z

    if-eqz v0, :cond_0

    .line 572711
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 572712
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    invoke-direct {p0}, Lcom/facebook/omnistore/Omnistore;->doGetSubscriptionCollectionNames()[Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 572713
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized remove()V
    .locals 1

    .prologue
    .line 572714
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/facebook/omnistore/Omnistore;->mIsRemoved:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 572715
    :goto_0
    monitor-exit p0

    return-void

    .line 572716
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/facebook/omnistore/Omnistore;->mIsRemoved:Z

    .line 572717
    invoke-direct {p0}, Lcom/facebook/omnistore/Omnistore;->doRemove()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 572718
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public native setCollectionIndexerFunction(Lcom/facebook/omnistore/Omnistore$CollectionIndexerFunction;)V
    .param p1    # Lcom/facebook/omnistore/Omnistore$CollectionIndexerFunction;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
.end method

.method public declared-synchronized start()V
    .locals 1

    .prologue
    .line 572719
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/facebook/omnistore/Omnistore;->mIsRemoved:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 572720
    :goto_0
    monitor-exit p0

    return-void

    .line 572721
    :cond_0
    :try_start_1
    invoke-direct {p0}, Lcom/facebook/omnistore/Omnistore;->doStart()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 572722
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public subscribeCollection(Lcom/facebook/omnistore/CollectionName;)Lcom/facebook/omnistore/Collection;
    .locals 1

    .prologue
    .line 572723
    new-instance v0, LX/2cX;

    invoke-direct {v0}, LX/2cX;-><init>()V

    invoke-virtual {v0}, LX/2cX;->build()LX/2cY;

    move-result-object v0

    .line 572724
    invoke-virtual {p0, p1, v0}, Lcom/facebook/omnistore/Omnistore;->subscribeCollection(Lcom/facebook/omnistore/CollectionName;LX/2cY;)Lcom/facebook/omnistore/Collection;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized subscribeCollection(Lcom/facebook/omnistore/CollectionName;LX/2cY;)Lcom/facebook/omnistore/Collection;
    .locals 9

    .prologue
    .line 572725
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/facebook/omnistore/Omnistore;->mIsRemoved:Z

    if-eqz v0, :cond_0

    .line 572726
    new-instance v0, Lcom/facebook/omnistore/OmnistoreException;

    const-string v1, "Called subscribeCollection after remove"

    invoke-direct {v0, v1}, Lcom/facebook/omnistore/OmnistoreException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 572727
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 572728
    :cond_0
    :try_start_1
    iget-object v0, p2, LX/2cY;->mBuilder:LX/2cX;

    iget-object v0, v0, LX/2cX;->mCollectionParams:Ljava/lang/String;

    move-object v2, v0

    .line 572729
    iget-object v0, p2, LX/2cY;->mBuilder:LX/2cX;

    iget-object v0, v0, LX/2cX;->mIdl:Ljava/lang/String;

    move-object v3, v0

    .line 572730
    iget-object v7, p2, LX/2cY;->mBuilder:LX/2cX;

    iget-wide v7, v7, LX/2cX;->mInitialGlobalVersionId:J

    move-wide v4, v7

    .line 572731
    iget-object v0, p2, LX/2cY;->mBuilder:LX/2cX;

    iget-boolean v0, v0, LX/2cX;->mRequiresSnapshot:Z

    move v6, v0

    .line 572732
    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/facebook/omnistore/Omnistore;->doSubscribeCollection(Lcom/facebook/omnistore/CollectionName;Ljava/lang/String;Ljava/lang/String;JZ)Lcom/facebook/omnistore/Collection;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0
.end method

.method public declared-synchronized unsubscribeCollection(Lcom/facebook/omnistore/CollectionName;)V
    .locals 1

    .prologue
    .line 572733
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/facebook/omnistore/Omnistore;->mIsRemoved:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 572734
    :goto_0
    monitor-exit p0

    return-void

    .line 572735
    :cond_0
    :try_start_1
    invoke-direct {p0, p1}, Lcom/facebook/omnistore/Omnistore;->doUnsubscribeCollection(Lcom/facebook/omnistore/CollectionName;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 572736
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized writeBugReport(Ljava/lang/String;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 572737
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/facebook/omnistore/Omnistore;->mIsRemoved:Z

    if-eqz v0, :cond_0

    .line 572738
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 572739
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    invoke-direct {p0, p1}, Lcom/facebook/omnistore/Omnistore;->doWriteBugReport(Ljava/lang/String;)[Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 572740
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
