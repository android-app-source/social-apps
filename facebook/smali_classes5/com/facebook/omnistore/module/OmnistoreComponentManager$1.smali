.class public final Lcom/facebook/omnistore/module/OmnistoreComponentManager$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic this$0:LX/2Pk;


# direct methods
.method public constructor <init>(LX/2Pk;)V
    .locals 0

    .prologue
    .line 572846
    iput-object p1, p0, Lcom/facebook/omnistore/module/OmnistoreComponentManager$1;->this$0:LX/2Pk;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 572847
    iget-object v0, p0, Lcom/facebook/omnistore/module/OmnistoreComponentManager$1;->this$0:LX/2Pk;

    iget-object v1, v0, LX/2Pk;->mOmnistoreMutex:Ljava/lang/Object;

    monitor-enter v1

    .line 572848
    :try_start_0
    iget-object v0, p0, Lcom/facebook/omnistore/module/OmnistoreComponentManager$1;->this$0:LX/2Pk;

    iget-object v0, v0, LX/2Pk;->mOmnistore:Lcom/facebook/omnistore/Omnistore;

    if-nez v0, :cond_0

    .line 572849
    monitor-exit v1

    .line 572850
    :goto_0
    return-void

    .line 572851
    :cond_0
    iget-object v0, p0, Lcom/facebook/omnistore/module/OmnistoreComponentManager$1;->this$0:LX/2Pk;

    iget-boolean v0, v0, LX/2Pk;->mIsOmnistoreStarted:Z

    if-nez v0, :cond_1

    .line 572852
    iget-object v0, p0, Lcom/facebook/omnistore/module/OmnistoreComponentManager$1;->this$0:LX/2Pk;

    iget-object v0, v0, LX/2Pk;->mInitTimeBugReportInfo:LX/2Pt;

    .line 572853
    const-string v2, "omnistore_start"

    invoke-static {v0, v2}, LX/2Pt;->logPoint(LX/2Pt;Ljava/lang/String;)V

    .line 572854
    iget-object v0, p0, Lcom/facebook/omnistore/module/OmnistoreComponentManager$1;->this$0:LX/2Pk;

    const/4 v2, 0x1

    .line 572855
    iput-boolean v2, v0, LX/2Pk;->mIsOmnistoreStarted:Z

    .line 572856
    iget-object v0, p0, Lcom/facebook/omnistore/module/OmnistoreComponentManager$1;->this$0:LX/2Pk;

    iget-object v0, v0, LX/2Pk;->mOmnistore:Lcom/facebook/omnistore/Omnistore;

    invoke-virtual {v0}, Lcom/facebook/omnistore/Omnistore;->start()V

    .line 572857
    :cond_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
