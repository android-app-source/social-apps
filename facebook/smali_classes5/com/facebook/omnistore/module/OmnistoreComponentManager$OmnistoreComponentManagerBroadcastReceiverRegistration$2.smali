.class public final Lcom/facebook/omnistore/module/OmnistoreComponentManager$OmnistoreComponentManagerBroadcastReceiverRegistration$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic this$0:LX/2cB;

.field public final synthetic val$componentManager:LX/2Pk;


# direct methods
.method public constructor <init>(LX/2cB;LX/2Pk;)V
    .locals 0

    .prologue
    .line 578748
    iput-object p1, p0, Lcom/facebook/omnistore/module/OmnistoreComponentManager$OmnistoreComponentManagerBroadcastReceiverRegistration$2;->this$0:LX/2cB;

    iput-object p2, p0, Lcom/facebook/omnistore/module/OmnistoreComponentManager$OmnistoreComponentManagerBroadcastReceiverRegistration$2;->val$componentManager:LX/2Pk;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 578745
    iget-object v0, p0, Lcom/facebook/omnistore/module/OmnistoreComponentManager$OmnistoreComponentManagerBroadcastReceiverRegistration$2;->val$componentManager:LX/2Pk;

    iget-object v0, v0, LX/2Pk;->mStartupOmnistoreComponents:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2cF;

    .line 578746
    iget-object v2, p0, Lcom/facebook/omnistore/module/OmnistoreComponentManager$OmnistoreComponentManagerBroadcastReceiverRegistration$2;->val$componentManager:LX/2Pk;

    invoke-virtual {v2, v0}, LX/2Pk;->maybeUpdateComponent(LX/2cF;)V

    goto :goto_0

    .line 578747
    :cond_0
    return-void
.end method
