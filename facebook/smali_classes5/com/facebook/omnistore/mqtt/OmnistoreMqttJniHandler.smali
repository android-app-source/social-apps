.class public Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;
.super Lcom/facebook/jni/Countable;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "OmnistoreMqttJniHandler"

.field private static volatile sInstance__com_facebook_omnistore_mqtt_OmnistoreMqttJniHandler__INJECTED_BY_TemplateInjector:Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;


# instance fields
.field private final mConnectionStarter:LX/2zl;

.field private final mDefaultExecutor:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;"
        }
    .end annotation
.end field

.field private final mExecutorService:LX/1fX;

.field public final mFbErrorReporter:LX/03V;

.field public volatile mIsOnConnectionEstablishedJobScheduled:Z

.field private final mMessagePublisher:LX/2Pn;

.field private mMqttProtocolProviderInstance:Lcom/facebook/omnistore/MqttProtocolProvider;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1fX;LX/2zl;LX/2Pn;LX/03V;LX/0Ot;)V
    .locals 1
    .param p1    # LX/1fX;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p5    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1fX;",
            "LX/2zl;",
            "LX/2Pn;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 572634
    invoke-direct {p0}, Lcom/facebook/jni/Countable;-><init>()V

    .line 572635
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;->mIsOnConnectionEstablishedJobScheduled:Z

    .line 572636
    iput-object p1, p0, Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;->mExecutorService:LX/1fX;

    .line 572637
    iput-object p2, p0, Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;->mConnectionStarter:LX/2zl;

    .line 572638
    iput-object p3, p0, Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;->mMessagePublisher:LX/2Pn;

    .line 572639
    iput-object p4, p0, Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;->mFbErrorReporter:LX/03V;

    .line 572640
    iput-object p5, p0, Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;->mDefaultExecutor:LX/0Ot;

    .line 572641
    return-void
.end method

.method public static synthetic access$000(Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;)V
    .locals 0

    .prologue
    .line 572684
    invoke-direct {p0}, Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;->onConnectionEstablished()V

    return-void
.end method

.method public static synthetic access$102(Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;Z)Z
    .locals 0

    .prologue
    .line 572683
    iput-boolean p1, p0, Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;->mIsOnConnectionEstablishedJobScheduled:Z

    return p1
.end method

.method public static synthetic access$200(Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;Lcom/facebook/omnistore/mqtt/PublishCallback;)V
    .locals 0

    .prologue
    .line 572682
    invoke-direct {p0, p1}, Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;->onPublishAcked(Lcom/facebook/omnistore/mqtt/PublishCallback;)V

    return-void
.end method

.method public static synthetic access$300(Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;)LX/03V;
    .locals 1

    .prologue
    .line 572681
    iget-object v0, p0, Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;->mFbErrorReporter:LX/03V;

    return-object v0
.end method

.method public static synthetic access$400(Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;Lcom/facebook/omnistore/mqtt/PublishCallback;)V
    .locals 0

    .prologue
    .line 572680
    invoke-direct {p0, p1}, Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;->onPublishFailed(Lcom/facebook/omnistore/mqtt/PublishCallback;)V

    return-void
.end method

.method private static createInstance__com_facebook_omnistore_mqtt_OmnistoreMqttJniHandler__INJECTED_BY_TemplateInjector(LX/0QB;)Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;
    .locals 6

    .prologue
    .line 572678
    new-instance v0, Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;

    invoke-static {p0}, LX/1fV;->b(LX/0QB;)LX/1fW;

    move-result-object v1

    check-cast v1, LX/1fX;

    invoke-static {p0}, LX/2zl;->getInstance__com_facebook_omnistore_mqtt_ConnectionStarter__INJECTED_BY_TemplateInjector(LX/0QB;)LX/2zl;

    move-result-object v2

    check-cast v2, LX/2zl;

    invoke-static {p0}, LX/2Pn;->createInstance__com_facebook_omnistore_mqtt_MessagePublisher__INJECTED_BY_TemplateInjector(LX/0QB;)LX/2Pn;

    move-result-object v3

    check-cast v3, LX/2Pn;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    const/16 v5, 0x140f

    invoke-static {p0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;-><init>(LX/1fX;LX/2zl;LX/2Pn;LX/03V;LX/0Ot;)V

    .line 572679
    return-object v0
.end method

.method private native doGetJavaMqtt()Lcom/facebook/omnistore/MqttProtocolProvider;
.end method

.method private native doHandleOmnistoreSyncMessage([B)V
.end method

.method private declared-synchronized ensureInitialized()V
    .locals 1

    .prologue
    .line 572673
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;->mMqttProtocolProviderInstance:Lcom/facebook/omnistore/MqttProtocolProvider;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 572674
    :goto_0
    monitor-exit p0

    return-void

    .line 572675
    :cond_0
    :try_start_1
    invoke-static {}, LX/2Pp;->ensure()V

    .line 572676
    invoke-direct {p0}, Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;->doGetJavaMqtt()Lcom/facebook/omnistore/MqttProtocolProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;->mMqttProtocolProviderInstance:Lcom/facebook/omnistore/MqttProtocolProvider;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 572677
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static getInstance__com_facebook_omnistore_mqtt_OmnistoreMqttJniHandler__INJECTED_BY_TemplateInjector(LX/0QB;)Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;
    .locals 3

    .prologue
    .line 572663
    sget-object v0, Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;->sInstance__com_facebook_omnistore_mqtt_OmnistoreMqttJniHandler__INJECTED_BY_TemplateInjector:Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;

    if-nez v0, :cond_1

    .line 572664
    const-class v1, Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;

    monitor-enter v1

    .line 572665
    :try_start_0
    sget-object v0, Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;->sInstance__com_facebook_omnistore_mqtt_OmnistoreMqttJniHandler__INJECTED_BY_TemplateInjector:Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 572666
    if-eqz v2, :cond_0

    .line 572667
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;->createInstance__com_facebook_omnistore_mqtt_OmnistoreMqttJniHandler__INJECTED_BY_TemplateInjector(LX/0QB;)Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;

    move-result-object v0

    sput-object v0, Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;->sInstance__com_facebook_omnistore_mqtt_OmnistoreMqttJniHandler__INJECTED_BY_TemplateInjector:Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 572668
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 572669
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 572670
    :cond_1
    sget-object v0, Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;->sInstance__com_facebook_omnistore_mqtt_OmnistoreMqttJniHandler__INJECTED_BY_TemplateInjector:Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;

    return-object v0

    .line 572671
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 572672
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private native onConnectionEstablished()V
.end method

.method private native onConnectionLost()V
.end method

.method private native onPublishAcked(Lcom/facebook/omnistore/mqtt/PublishCallback;)V
.end method

.method private native onPublishFailed(Lcom/facebook/omnistore/mqtt/PublishCallback;)V
.end method

.method private publishMessage(Ljava/lang/String;[BLcom/facebook/omnistore/mqtt/PublishCallback;)V
    .locals 3
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 572658
    iget-object v0, p0, Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;->mExecutorService:LX/1fX;

    iget-object v1, p0, Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;->mMessagePublisher:LX/2Pn;

    .line 572659
    new-instance v2, LX/3TQ;

    invoke-direct {v2, v1, p1, p2}, LX/3TQ;-><init>(LX/2Pn;Ljava/lang/String;[B)V

    move-object v1, v2

    .line 572660
    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 572661
    new-instance v1, LX/3TR;

    invoke-direct {v1, p0, p1, p3}, LX/3TR;-><init>(Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;Ljava/lang/String;Lcom/facebook/omnistore/mqtt/PublishCallback;)V

    iget-object v2, p0, Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;->mExecutorService:LX/1fX;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 572662
    return-void
.end method


# virtual methods
.method public connectionEstablished()V
    .locals 3

    .prologue
    .line 572654
    iget-boolean v0, p0, Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;->mIsOnConnectionEstablishedJobScheduled:Z

    if-eqz v0, :cond_0

    .line 572655
    :goto_0
    return-void

    .line 572656
    :cond_0
    iget-object v0, p0, Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;->mDefaultExecutor:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler$1;

    invoke-direct {v1, p0}, Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler$1;-><init>(Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;)V

    const v2, -0x25b87a7c

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 572657
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;->mIsOnConnectionEstablishedJobScheduled:Z

    goto :goto_0
.end method

.method public ensureConnection()V
    .locals 4
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 572647
    iget-object v0, p0, Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;->mConnectionStarter:LX/2zl;

    .line 572648
    iget-object v1, v0, LX/2zl;->mLocalBroadcastManager:LX/0Xl;

    invoke-interface {v1}, LX/0Xl;->a()LX/0YX;

    move-result-object v1

    const-string v2, "com.facebook.push.mqtt.ACTION_CHANNEL_STATE_CHANGED"

    new-instance v3, LX/3TH;

    invoke-direct {v3, v0, p0}, LX/3TH;-><init>(LX/2zl;Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;)V

    invoke-interface {v1, v2, v3}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v1

    invoke-interface {v1}, LX/0YX;->a()LX/0Yb;

    move-result-object v1

    invoke-virtual {v1}, LX/0Yb;->b()V

    .line 572649
    iget-boolean v1, v0, LX/2zl;->mIsEnabledInBackground:Z

    if-eqz v1, :cond_1

    .line 572650
    iget-object v1, v0, LX/2zl;->mChannelConnectivityTracker:LX/1MZ;

    invoke-virtual {v1}, LX/1MZ;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 572651
    invoke-virtual {p0}, Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;->connectionEstablished()V

    .line 572652
    :cond_0
    :goto_0
    return-void

    .line 572653
    :cond_1
    iget-object v1, v0, LX/2zl;->mUiThreadExecutor:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/Executor;

    new-instance v2, Lcom/facebook/omnistore/mqtt/ConnectionStarter$2;

    invoke-direct {v2, v0, p0}, Lcom/facebook/omnistore/mqtt/ConnectionStarter$2;-><init>(LX/2zl;Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;)V

    const v3, 0x6e06a0e0

    invoke-static {v1, v2, v3}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0
.end method

.method public getJavaMqtt()Lcom/facebook/omnistore/MqttProtocolProvider;
    .locals 1

    .prologue
    .line 572645
    invoke-direct {p0}, Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;->ensureInitialized()V

    .line 572646
    iget-object v0, p0, Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;->mMqttProtocolProviderInstance:Lcom/facebook/omnistore/MqttProtocolProvider;

    return-object v0
.end method

.method public handleOmnistoreSyncMessage([B)V
    .locals 0

    .prologue
    .line 572642
    invoke-direct {p0}, Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;->ensureInitialized()V

    .line 572643
    invoke-direct {p0, p1}, Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;->doHandleOmnistoreSyncMessage([B)V

    .line 572644
    return-void
.end method
