.class public final enum Lcom/facebook/omnistore/Omnistore$SnapshotState;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/omnistore/Omnistore$SnapshotState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/omnistore/Omnistore$SnapshotState;

.field public static final enum COMPLETE:Lcom/facebook/omnistore/Omnistore$SnapshotState;

.field public static final enum NONE:Lcom/facebook/omnistore/Omnistore$SnapshotState;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 572687
    new-instance v0, Lcom/facebook/omnistore/Omnistore$SnapshotState;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/omnistore/Omnistore$SnapshotState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/omnistore/Omnistore$SnapshotState;->NONE:Lcom/facebook/omnistore/Omnistore$SnapshotState;

    .line 572688
    new-instance v0, Lcom/facebook/omnistore/Omnistore$SnapshotState;

    const-string v1, "COMPLETE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/omnistore/Omnistore$SnapshotState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/omnistore/Omnistore$SnapshotState;->COMPLETE:Lcom/facebook/omnistore/Omnistore$SnapshotState;

    .line 572689
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/facebook/omnistore/Omnistore$SnapshotState;

    sget-object v1, Lcom/facebook/omnistore/Omnistore$SnapshotState;->NONE:Lcom/facebook/omnistore/Omnistore$SnapshotState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/omnistore/Omnistore$SnapshotState;->COMPLETE:Lcom/facebook/omnistore/Omnistore$SnapshotState;

    aput-object v1, v0, v3

    sput-object v0, Lcom/facebook/omnistore/Omnistore$SnapshotState;->$VALUES:[Lcom/facebook/omnistore/Omnistore$SnapshotState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 572690
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/omnistore/Omnistore$SnapshotState;
    .locals 1

    .prologue
    .line 572685
    const-class v0, Lcom/facebook/omnistore/Omnistore$SnapshotState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/omnistore/Omnistore$SnapshotState;

    return-object v0
.end method

.method public static values()[Lcom/facebook/omnistore/Omnistore$SnapshotState;
    .locals 1

    .prologue
    .line 572686
    sget-object v0, Lcom/facebook/omnistore/Omnistore$SnapshotState;->$VALUES:[Lcom/facebook/omnistore/Omnistore$SnapshotState;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/omnistore/Omnistore$SnapshotState;

    return-object v0
.end method
