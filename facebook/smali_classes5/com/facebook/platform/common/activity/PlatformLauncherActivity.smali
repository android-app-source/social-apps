.class public abstract Lcom/facebook/platform/common/activity/PlatformLauncherActivity;
.super Landroid/app/Activity;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static h:Ljava/lang/Integer;

.field private static i:J


# instance fields
.field public b:LX/0Uh;

.field public final c:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/base/activity/FbFragmentActivity;",
            ">;"
        }
    .end annotation
.end field

.field public final d:I

.field public e:Ljava/lang/String;

.field public f:LX/0QA;

.field public g:Z

.field public j:J

.field private k:LX/0So;

.field private l:LX/4hi;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 801632
    const-class v0, Lcom/facebook/platform/common/activity/PlatformLauncherActivity;

    sput-object v0, Lcom/facebook/platform/common/activity/PlatformLauncherActivity;->a:Ljava/lang/Class;

    .line 801633
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/facebook/platform/common/activity/PlatformLauncherActivity;->h:Ljava/lang/Integer;

    .line 801634
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/facebook/platform/common/activity/PlatformLauncherActivity;->i:J

    return-void
.end method

.method public constructor <init>(Ljava/lang/Class;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/base/activity/FbFragmentActivity;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 801635
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 801636
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/platform/common/activity/PlatformLauncherActivity;->j:J

    .line 801637
    iput-object p1, p0, Lcom/facebook/platform/common/activity/PlatformLauncherActivity;->c:Ljava/lang/Class;

    .line 801638
    iput p2, p0, Lcom/facebook/platform/common/activity/PlatformLauncherActivity;->d:I

    .line 801639
    return-void
.end method

.method private a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 801640
    if-nez p1, :cond_2

    .line 801641
    iget-object v0, p0, Lcom/facebook/platform/common/activity/PlatformLauncherActivity;->b:LX/0Uh;

    const/16 v1, 0x4e6

    const/4 p1, 0x0

    invoke-virtual {v0, v1, p1}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 801642
    invoke-virtual {p0}, Lcom/facebook/platform/common/activity/PlatformLauncherActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, LX/4uH;->a(Landroid/content/Context;Z)LX/4uH;

    move-result-object v0

    .line 801643
    invoke-interface {v0, p0}, LX/4u1;->a(Landroid/app/Activity;)Landroid/content/ComponentName;

    move-result-object v0

    .line 801644
    :goto_0
    move-object v0, v0

    .line 801645
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lcom/facebook/platform/common/activity/PlatformLauncherActivity;->e:Ljava/lang/String;

    .line 801646
    iget-object v0, p0, Lcom/facebook/platform/common/activity/PlatformLauncherActivity;->e:Ljava/lang/String;

    .line 801647
    sget-object v1, LX/007;->g:Ljava/lang/String;

    move-object v1, v1

    .line 801648
    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 801649
    invoke-virtual {p0}, Lcom/facebook/platform/common/activity/PlatformLauncherActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "calling_package_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/common/activity/PlatformLauncherActivity;->e:Ljava/lang/String;

    .line 801650
    :cond_0
    :goto_2
    return-void

    .line 801651
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 801652
    :cond_2
    const-string v0, "calling_package_key"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/common/activity/PlatformLauncherActivity;->e:Ljava/lang/String;

    goto :goto_2

    .line 801653
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/platform/common/activity/PlatformLauncherActivity;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 801654
    iget v0, p0, Lcom/facebook/platform/common/activity/PlatformLauncherActivity;->d:I

    if-ne p1, v0, :cond_0

    .line 801655
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/platform/common/activity/PlatformLauncherActivity;->g:Z

    .line 801656
    invoke-virtual {p0, p2, p3}, Lcom/facebook/platform/common/activity/PlatformLauncherActivity;->setResult(ILandroid/content/Intent;)V

    .line 801657
    invoke-virtual {p0}, Lcom/facebook/platform/common/activity/PlatformLauncherActivity;->finish()V

    .line 801658
    :cond_0
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x22

    const v1, -0x2e7ae489

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 801659
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 801660
    invoke-static {p0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/common/activity/PlatformLauncherActivity;->f:LX/0QA;

    .line 801661
    iget-object v0, p0, Lcom/facebook/platform/common/activity/PlatformLauncherActivity;->f:LX/0QA;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v0

    check-cast v0, LX/0So;

    iput-object v0, p0, Lcom/facebook/platform/common/activity/PlatformLauncherActivity;->k:LX/0So;

    .line 801662
    iget-object v0, p0, Lcom/facebook/platform/common/activity/PlatformLauncherActivity;->k:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/platform/common/activity/PlatformLauncherActivity;->j:J

    .line 801663
    iget-object v0, p0, Lcom/facebook/platform/common/activity/PlatformLauncherActivity;->f:LX/0QA;

    invoke-static {v0}, LX/4hi;->a(LX/0QB;)LX/4hi;

    move-result-object v0

    check-cast v0, LX/4hi;

    iput-object v0, p0, Lcom/facebook/platform/common/activity/PlatformLauncherActivity;->l:LX/4hi;

    .line 801664
    iget-object v0, p0, Lcom/facebook/platform/common/activity/PlatformLauncherActivity;->f:LX/0QA;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v0

    check-cast v0, LX/0Uh;

    iput-object v0, p0, Lcom/facebook/platform/common/activity/PlatformLauncherActivity;->b:LX/0Uh;

    .line 801665
    iget-object v0, p0, Lcom/facebook/platform/common/activity/PlatformLauncherActivity;->f:LX/0QA;

    invoke-static {v0}, LX/0Uq;->a(LX/0QB;)LX/0Uq;

    move-result-object v0

    check-cast v0, LX/0Uq;

    .line 801666
    invoke-virtual {v0}, LX/0Uq;->b()V

    .line 801667
    invoke-direct {p0, p1}, Lcom/facebook/platform/common/activity/PlatformLauncherActivity;->a(Landroid/os/Bundle;)V

    .line 801668
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/platform/common/activity/PlatformLauncherActivity;->g:Z

    .line 801669
    if-eqz p1, :cond_0

    .line 801670
    const-string v0, "child_act_launched"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/platform/common/activity/PlatformLauncherActivity;->g:Z

    .line 801671
    const-string v0, "platform_launch_time_ms"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/platform/common/activity/PlatformLauncherActivity;->j:J

    .line 801672
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/platform/common/activity/PlatformLauncherActivity;->g:Z

    if-nez v0, :cond_3

    .line 801673
    iget-object v0, p0, Lcom/facebook/platform/common/activity/PlatformLauncherActivity;->l:LX/4hi;

    invoke-virtual {v0}, LX/4hi;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 801674
    sget-object v0, Lcom/facebook/platform/common/activity/PlatformLauncherActivity;->a:Ljava/lang/Class;

    const-string v2, "Api requests exceed the rate limit"

    invoke-static {v0, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 801675
    invoke-virtual {p0}, Lcom/facebook/platform/common/activity/PlatformLauncherActivity;->finish()V

    .line 801676
    const/16 v0, 0x23

    const v2, -0x630fac3d

    invoke-static {v4, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 801677
    :goto_0
    return-void

    .line 801678
    :cond_1
    sget-object v5, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    .line 801679
    invoke-virtual {p0}, Lcom/facebook/platform/common/activity/PlatformLauncherActivity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v6

    if-eqz v6, :cond_2

    .line 801680
    invoke-virtual {p0}, Lcom/facebook/platform/common/activity/PlatformLauncherActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    .line 801681
    :cond_2
    new-instance v6, Landroid/content/Intent;

    iget-object v7, p0, Lcom/facebook/platform/common/activity/PlatformLauncherActivity;->c:Ljava/lang/Class;

    invoke-direct {v6, p0, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 801682
    invoke-virtual {v6, v5}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 801683
    const-string v5, "platform_launch_time_ms"

    iget-wide v7, p0, Lcom/facebook/platform/common/activity/PlatformLauncherActivity;->j:J

    invoke-virtual {v6, v5, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 801684
    const-string v5, "calling_package_key"

    iget-object v7, p0, Lcom/facebook/platform/common/activity/PlatformLauncherActivity;->e:Ljava/lang/String;

    invoke-virtual {v6, v5, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 801685
    iget-object v5, p0, Lcom/facebook/platform/common/activity/PlatformLauncherActivity;->f:LX/0QA;

    invoke-static {v5}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    .line 801686
    iget v7, p0, Lcom/facebook/platform/common/activity/PlatformLauncherActivity;->d:I

    invoke-interface {v5, v6, v7, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 801687
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/facebook/platform/common/activity/PlatformLauncherActivity;->g:Z

    .line 801688
    :cond_3
    const v0, 0x1da54e71

    invoke-static {v0, v1}, LX/02F;->c(II)V

    goto :goto_0
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 801689
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 801690
    const-string v0, "platform_launch_time_ms"

    iget-wide v2, p0, Lcom/facebook/platform/common/activity/PlatformLauncherActivity;->j:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 801691
    const-string v0, "calling_package_key"

    iget-object v1, p0, Lcom/facebook/platform/common/activity/PlatformLauncherActivity;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 801692
    const-string v0, "child_act_launched"

    iget-boolean v1, p0, Lcom/facebook/platform/common/activity/PlatformLauncherActivity;->g:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 801693
    return-void
.end method
