.class public Lcom/facebook/platform/common/activity/PlatformWrapperActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/0l6;


# static fields
.field private static final r:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public p:LX/0So;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/4hk;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private s:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 802072
    const-class v0, Lcom/facebook/platform/common/activity/PlatformWrapperActivity;

    sput-object v0, Lcom/facebook/platform/common/activity/PlatformWrapperActivity;->r:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 802071
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/platform/common/activity/PlatformWrapperActivity;LX/0So;LX/4hk;)V
    .locals 0

    .prologue
    .line 802070
    iput-object p1, p0, Lcom/facebook/platform/common/activity/PlatformWrapperActivity;->p:LX/0So;

    iput-object p2, p0, Lcom/facebook/platform/common/activity/PlatformWrapperActivity;->q:LX/4hk;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/platform/common/activity/PlatformWrapperActivity;

    invoke-static {v1}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v0

    check-cast v0, LX/0So;

    invoke-static {v1}, LX/4hk;->b(LX/0QB;)LX/4hk;

    move-result-object v1

    check-cast v1, LX/4hk;

    invoke-static {p0, v0, v1}, Lcom/facebook/platform/common/activity/PlatformWrapperActivity;->a(Lcom/facebook/platform/common/activity/PlatformWrapperActivity;LX/0So;LX/4hk;)V

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    .line 802066
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onActivityCreate "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 802067
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 802068
    iget-object v1, p0, Lcom/facebook/platform/common/activity/PlatformWrapperActivity;->q:LX/4hk;

    invoke-virtual {p0}, Lcom/facebook/platform/common/activity/PlatformWrapperActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const/4 v5, 0x1

    iget-wide v6, p0, Lcom/facebook/platform/common/activity/PlatformWrapperActivity;->s:J

    move-object v2, p1

    move-object v3, p0

    invoke-virtual/range {v1 .. v7}, LX/4hk;->a(Landroid/os/Bundle;Landroid/app/Activity;Landroid/content/Intent;ZJ)V

    .line 802069
    return-void
.end method

.method public final iy_()V
    .locals 2

    .prologue
    .line 802055
    invoke-static {p0, p0}, Lcom/facebook/platform/common/activity/PlatformWrapperActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 802056
    iget-object v0, p0, Lcom/facebook/platform/common/activity/PlatformWrapperActivity;->p:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/platform/common/activity/PlatformWrapperActivity;->s:J

    .line 802057
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 802064
    iget-object v0, p0, Lcom/facebook/platform/common/activity/PlatformWrapperActivity;->q:LX/4hk;

    invoke-virtual {v0, p1, p2, p3}, LX/4hk;->a(IILandroid/content/Intent;)V

    .line 802065
    return-void
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x33534cde

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 802061
    iget-object v1, p0, Lcom/facebook/platform/common/activity/PlatformWrapperActivity;->q:LX/4hk;

    invoke-virtual {v1}, LX/4hk;->a()V

    .line 802062
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onDestroy()V

    .line 802063
    const/16 v1, 0x23

    const v2, -0x4dc7bc74

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 802058
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 802059
    iget-object v0, p0, Lcom/facebook/platform/common/activity/PlatformWrapperActivity;->q:LX/4hk;

    invoke-virtual {v0, p1}, LX/4hk;->c(Landroid/os/Bundle;)V

    .line 802060
    return-void
.end method
