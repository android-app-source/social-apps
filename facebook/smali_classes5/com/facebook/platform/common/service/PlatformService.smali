.class public Lcom/facebook/platform/common/service/PlatformService;
.super LX/0te;
.source ""


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "LX/4hq;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/4hq;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/3N2;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 802234
    const-class v0, Lcom/facebook/platform/common/service/PlatformService;

    sput-object v0, Lcom/facebook/platform/common/service/PlatformService;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 802235
    invoke-direct {p0}, LX/0te;-><init>()V

    .line 802236
    return-void
.end method

.method private a(I)LX/4hq;
    .locals 4

    .prologue
    .line 802237
    iget-object v0, p0, Lcom/facebook/platform/common/service/PlatformService;->b:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 802238
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/common/service/PlatformService;->b:Ljava/util/Map;

    .line 802239
    iget-object v0, p0, Lcom/facebook/platform/common/service/PlatformService;->c:Ljava/util/Set;

    if-eqz v0, :cond_0

    .line 802240
    iget-object v0, p0, Lcom/facebook/platform/common/service/PlatformService;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4hq;

    .line 802241
    iget-object v2, p0, Lcom/facebook/platform/common/service/PlatformService;->b:Ljava/util/Map;

    invoke-interface {v0}, LX/4hq;->a()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 802242
    :cond_0
    iget-object v0, p0, Lcom/facebook/platform/common/service/PlatformService;->b:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4hq;

    return-object v0
.end method

.method public static synthetic a(Lcom/facebook/platform/common/service/PlatformService;I)LX/4hq;
    .locals 1

    .prologue
    .line 802243
    invoke-direct {p0, p1}, Lcom/facebook/platform/common/service/PlatformService;->a(I)LX/4hq;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/platform/common/service/PlatformService;

    new-instance v1, LX/0U8;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v2

    new-instance p1, LX/4hy;

    invoke-direct {p1, v0}, LX/4hy;-><init>(LX/0QB;)V

    invoke-direct {v1, v2, p1}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v1, v1

    invoke-static {v0}, LX/3N2;->b(LX/0QB;)LX/3N2;

    move-result-object v0

    check-cast v0, LX/3N2;

    invoke-direct {p0, v1, v0}, Lcom/facebook/platform/common/service/PlatformService;->a(Ljava/util/Set;LX/3N2;)V

    return-void
.end method

.method private a(Ljava/util/Set;LX/3N2;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/4hq;",
            ">;",
            "LX/3N2;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 802244
    iput-object p1, p0, Lcom/facebook/platform/common/service/PlatformService;->c:Ljava/util/Set;

    .line 802245
    iput-object p2, p0, Lcom/facebook/platform/common/service/PlatformService;->d:LX/3N2;

    .line 802246
    return-void
.end method


# virtual methods
.method public final onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2

    .prologue
    .line 802247
    new-instance v0, Landroid/os/Messenger;

    new-instance v1, LX/4hx;

    invoke-direct {v1, p0}, LX/4hx;-><init>(Lcom/facebook/platform/common/service/PlatformService;)V

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    .line 802248
    invoke-virtual {v0}, Landroid/os/Messenger;->getBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public final onFbCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, 0x79563815

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 802249
    invoke-super {p0}, LX/0te;->onFbCreate()V

    .line 802250
    invoke-static {p0}, LX/1mU;->a(Landroid/content/Context;)V

    .line 802251
    invoke-static {p0, p0}, Lcom/facebook/platform/common/service/PlatformService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 802252
    const/16 v1, 0x25

    const v2, 0x7afe4b05

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
