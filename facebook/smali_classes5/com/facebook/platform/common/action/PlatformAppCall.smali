.class public Lcom/facebook/platform/common/action/PlatformAppCall;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/platform/common/action/PlatformAppCall;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:I

.field public final c:Z

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field public final g:Ljava/lang/String;

.field public final h:Ljava/lang/String;

.field public final i:Ljava/lang/String;

.field public final j:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 801617
    new-instance v0, LX/4hZ;

    invoke-direct {v0}, LX/4hZ;-><init>()V

    sput-object v0, Lcom/facebook/platform/common/action/PlatformAppCall;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/4ha;)V
    .locals 1

    .prologue
    .line 801605
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 801606
    iget-object v0, p1, LX/4ha;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/platform/common/action/PlatformAppCall;->a:Ljava/lang/String;

    .line 801607
    iget v0, p1, LX/4ha;->b:I

    iput v0, p0, Lcom/facebook/platform/common/action/PlatformAppCall;->b:I

    .line 801608
    iget-boolean v0, p1, LX/4ha;->c:Z

    iput-boolean v0, p0, Lcom/facebook/platform/common/action/PlatformAppCall;->c:Z

    .line 801609
    iget-object v0, p1, LX/4ha;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/platform/common/action/PlatformAppCall;->d:Ljava/lang/String;

    .line 801610
    iget-object v0, p1, LX/4ha;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/platform/common/action/PlatformAppCall;->e:Ljava/lang/String;

    .line 801611
    iget-object v0, p1, LX/4ha;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/platform/common/action/PlatformAppCall;->f:Ljava/lang/String;

    .line 801612
    iget-object v0, p1, LX/4ha;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/platform/common/action/PlatformAppCall;->g:Ljava/lang/String;

    .line 801613
    iget-object v0, p1, LX/4ha;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/platform/common/action/PlatformAppCall;->h:Ljava/lang/String;

    .line 801614
    iget-object v0, p1, LX/4ha;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/platform/common/action/PlatformAppCall;->i:Ljava/lang/String;

    .line 801615
    iget-object v0, p1, LX/4ha;->j:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/platform/common/action/PlatformAppCall;->j:Ljava/lang/String;

    .line 801616
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 801592
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 801593
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/common/action/PlatformAppCall;->a:Ljava/lang/String;

    .line 801594
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/platform/common/action/PlatformAppCall;->b:I

    .line 801595
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/common/action/PlatformAppCall;->d:Ljava/lang/String;

    .line 801596
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/common/action/PlatformAppCall;->e:Ljava/lang/String;

    .line 801597
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/common/action/PlatformAppCall;->f:Ljava/lang/String;

    .line 801598
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/common/action/PlatformAppCall;->g:Ljava/lang/String;

    .line 801599
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/common/action/PlatformAppCall;->h:Ljava/lang/String;

    .line 801600
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/common/action/PlatformAppCall;->i:Ljava/lang/String;

    .line 801601
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/common/action/PlatformAppCall;->j:Ljava/lang/String;

    .line 801602
    invoke-virtual {p1}, Landroid/os/Parcel;->createBooleanArray()[Z

    move-result-object v0

    .line 801603
    const/4 v1, 0x0

    aget-boolean v0, v0, v1

    iput-boolean v0, p0, Lcom/facebook/platform/common/action/PlatformAppCall;->c:Z

    .line 801604
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 801580
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 801581
    iput-object p1, p0, Lcom/facebook/platform/common/action/PlatformAppCall;->a:Ljava/lang/String;

    .line 801582
    iput p2, p0, Lcom/facebook/platform/common/action/PlatformAppCall;->b:I

    .line 801583
    iput-boolean p3, p0, Lcom/facebook/platform/common/action/PlatformAppCall;->c:Z

    .line 801584
    iput-object p4, p0, Lcom/facebook/platform/common/action/PlatformAppCall;->d:Ljava/lang/String;

    .line 801585
    iput-object p5, p0, Lcom/facebook/platform/common/action/PlatformAppCall;->e:Ljava/lang/String;

    .line 801586
    iput-object p6, p0, Lcom/facebook/platform/common/action/PlatformAppCall;->f:Ljava/lang/String;

    .line 801587
    iput-object p7, p0, Lcom/facebook/platform/common/action/PlatformAppCall;->g:Ljava/lang/String;

    .line 801588
    iput-object p8, p0, Lcom/facebook/platform/common/action/PlatformAppCall;->h:Ljava/lang/String;

    .line 801589
    iput-object p9, p0, Lcom/facebook/platform/common/action/PlatformAppCall;->i:Ljava/lang/String;

    .line 801590
    iput-object p10, p0, Lcom/facebook/platform/common/action/PlatformAppCall;->j:Ljava/lang/String;

    .line 801591
    return-void
.end method

.method public static a(Landroid/content/Intent;)Landroid/os/Bundle;
    .locals 2

    .prologue
    .line 801563
    const-string v0, "com.facebook.platform.protocol.BRIDGE_ARGS"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 801564
    if-eqz v1, :cond_0

    .line 801565
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0, v1}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    .line 801566
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final d()Z
    .locals 2

    .prologue
    .line 801579
    iget-boolean v0, p0, Lcom/facebook/platform/common/action/PlatformAppCall;->c:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/facebook/platform/common/action/PlatformAppCall;->b:I

    const v1, 0x1335433

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 801578
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    .line 801567
    iget-object v0, p0, Lcom/facebook/platform/common/action/PlatformAppCall;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 801568
    iget v0, p0, Lcom/facebook/platform/common/action/PlatformAppCall;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 801569
    iget-object v0, p0, Lcom/facebook/platform/common/action/PlatformAppCall;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 801570
    iget-object v0, p0, Lcom/facebook/platform/common/action/PlatformAppCall;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 801571
    iget-object v0, p0, Lcom/facebook/platform/common/action/PlatformAppCall;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 801572
    iget-object v0, p0, Lcom/facebook/platform/common/action/PlatformAppCall;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 801573
    iget-object v0, p0, Lcom/facebook/platform/common/action/PlatformAppCall;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 801574
    iget-object v0, p0, Lcom/facebook/platform/common/action/PlatformAppCall;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 801575
    iget-object v0, p0, Lcom/facebook/platform/common/action/PlatformAppCall;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 801576
    const/4 v0, 0x1

    new-array v0, v0, [Z

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/facebook/platform/common/action/PlatformAppCall;->c:Z

    aput-boolean v2, v0, v1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBooleanArray([Z)V

    .line 801577
    return-void
.end method
