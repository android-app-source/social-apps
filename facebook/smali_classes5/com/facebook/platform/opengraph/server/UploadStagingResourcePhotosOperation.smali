.class public Lcom/facebook/platform/opengraph/server/UploadStagingResourcePhotosOperation;
.super LX/2QZ;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/ApiMethodRunner;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/2RH;


# direct methods
.method public constructor <init>(LX/0Or;LX/2RH;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/ApiMethodRunner;",
            ">;",
            "LX/2RH;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 573084
    const-string v0, "platform_upload_staging_resource_photos"

    invoke-direct {p0, v0}, LX/2QZ;-><init>(Ljava/lang/String;)V

    .line 573085
    iput-object p1, p0, Lcom/facebook/platform/opengraph/server/UploadStagingResourcePhotosOperation;->b:LX/0Or;

    .line 573086
    iput-object p2, p0, Lcom/facebook/platform/opengraph/server/UploadStagingResourcePhotosOperation;->c:LX/2RH;

    .line 573087
    return-void
.end method


# virtual methods
.method public final a(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 8

    .prologue
    .line 573088
    iget-object v0, p0, LX/2QZ;->a:Ljava/lang/String;

    .line 573089
    iget-object v1, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v1, v1

    .line 573090
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 573091
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 573092
    const-string v1, "platform_upload_staging_resource_photos_params"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/platform/opengraph/server/UploadStagingResourcePhotosOperation$Params;

    .line 573093
    iget-object v1, p0, Lcom/facebook/platform/opengraph/server/UploadStagingResourcePhotosOperation;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/18V;

    invoke-virtual {v1}, LX/18V;->a()LX/2VK;

    move-result-object v4

    .line 573094
    const/4 v1, 0x0

    .line 573095
    iget-object v2, v0, Lcom/facebook/platform/opengraph/server/UploadStagingResourcePhotosOperation$Params;->a:LX/0P1;

    .line 573096
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v5

    .line 573097
    invoke-virtual {v2}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v2

    invoke-virtual {v2}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v2, v1

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 573098
    const-string v7, "uploadStagingResourcePhoto%d"

    add-int/lit8 v3, v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v7, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 573099
    new-instance p1, Lcom/facebook/platform/server/protocol/UploadStagingResourcePhotoMethod$Params;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    invoke-direct {p1, v7, v2}, Lcom/facebook/platform/server/protocol/UploadStagingResourcePhotoMethod$Params;-><init>(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 573100
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v5, v1, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 573101
    iget-object v1, p0, Lcom/facebook/platform/opengraph/server/UploadStagingResourcePhotosOperation;->c:LX/2RH;

    invoke-static {v1, p1}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v1

    .line 573102
    iput-object v7, v1, LX/2Vk;->c:Ljava/lang/String;

    .line 573103
    move-object v1, v1

    .line 573104
    invoke-virtual {v1}, LX/2Vk;->a()LX/2Vj;

    move-result-object v1

    invoke-interface {v4, v1}, LX/2VK;->a(LX/2Vj;)V

    move v2, v3

    .line 573105
    goto :goto_0

    .line 573106
    :cond_0
    const-string v1, "uploadStagingResources"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v2

    invoke-interface {v4, v1, v2}, LX/2VK;->a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 573107
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 573108
    invoke-virtual {v5}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 573109
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v4, v1}, LX/2VK;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 573110
    :cond_1
    invoke-static {v3}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v1

    move-object v0, v1

    .line 573111
    return-object v0
.end method
