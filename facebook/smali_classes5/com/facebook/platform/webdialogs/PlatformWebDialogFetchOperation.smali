.class public Lcom/facebook/platform/webdialogs/PlatformWebDialogFetchOperation;
.super LX/2QZ;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements Lorg/apache/http/client/ResponseHandler;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2QZ;",
        "Lcom/facebook/common/callercontext/CallerContextable;",
        "Lorg/apache/http/client/ResponseHandler",
        "<",
        "Lorg/apache/http/HttpResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final b:LX/2QR;

.field private final c:LX/2Rs;

.field private d:Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;

.field private e:LX/1nY;


# direct methods
.method public constructor <init>(LX/2Rs;LX/2QR;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 573112
    const-string v0, "platform_webdialog_fetch"

    invoke-direct {p0, v0}, LX/2QZ;-><init>(Ljava/lang/String;)V

    .line 573113
    iput-object p1, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogFetchOperation;->c:LX/2Rs;

    .line 573114
    iput-object p2, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogFetchOperation;->b:LX/2QR;

    .line 573115
    return-void
.end method


# virtual methods
.method public final a(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 573116
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 573117
    const-string v1, "platform_webview_actionmanifest"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;

    iput-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogFetchOperation;->d:Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;

    .line 573118
    iget-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogFetchOperation;->d:Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;

    if-nez v0, :cond_0

    .line 573119
    sget-object v0, LX/1nY;->OTHER:LX/1nY;

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 573120
    :goto_0
    return-object v0

    .line 573121
    :cond_0
    iget-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogFetchOperation;->d:Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;

    invoke-virtual {v0}, Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;->c()Ljava/lang/String;

    move-result-object v0

    .line 573122
    iget-object v1, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogFetchOperation;->b:LX/2QR;

    invoke-virtual {v1, v0}, LX/2QR;->a(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    .line 573123
    if-eqz v1, :cond_1

    .line 573124
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 573125
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 573126
    goto :goto_0

    .line 573127
    :cond_1
    iget-object v1, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogFetchOperation;->c:LX/2Rs;

    const-class v2, Lcom/facebook/platform/webdialogs/PlatformWebDialogFetchOperation;

    invoke-static {v2}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v2

    invoke-virtual {v1, v0, v2, p0}, LX/2Rs;->a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;Lorg/apache/http/client/ResponseHandler;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 573128
    if-nez v0, :cond_2

    .line 573129
    sget-object v0, LX/1nY;->CONNECTION_FAILURE:LX/1nY;

    iput-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogFetchOperation;->e:LX/1nY;

    .line 573130
    :cond_2
    iget-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogFetchOperation;->e:LX/1nY;

    sget-object v1, LX/1nY;->NO_ERROR:LX/1nY;

    if-eq v0, v1, :cond_3

    .line 573131
    iget-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogFetchOperation;->e:LX/1nY;

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0

    .line 573132
    :cond_3
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 573133
    goto :goto_0
.end method

.method public final handleResponse(Lorg/apache/http/HttpResponse;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 573134
    invoke-static {p1}, LX/2Rs;->a(Lorg/apache/http/HttpResponse;)LX/1nY;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogFetchOperation;->e:LX/1nY;

    .line 573135
    iget-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogFetchOperation;->e:LX/1nY;

    sget-object v1, LX/1nY;->NO_ERROR:LX/1nY;

    if-ne v0, v1, :cond_0

    .line 573136
    iget-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogFetchOperation;->b:LX/2QR;

    iget-object v1, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogFetchOperation;->d:Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;

    invoke-virtual {v0, v1, p1}, LX/2QR;->a(Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;Lorg/apache/http/HttpResponse;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 573137
    sget-object v0, LX/1nY;->CONNECTION_FAILURE:LX/1nY;

    iput-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogFetchOperation;->e:LX/1nY;

    .line 573138
    :cond_0
    return-object p1
.end method
