.class public final Lcom/facebook/platform/webdialogs/PlatformWebDialogsManifest$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:LX/2QQ;


# direct methods
.method public constructor <init>(LX/2QQ;Z)V
    .locals 0

    .prologue
    .line 572942
    iput-object p1, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsManifest$1;->b:LX/2QQ;

    iput-boolean p2, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsManifest$1;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    .line 572943
    iget-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsManifest$1;->b:LX/2QQ;

    iget-object v0, v0, LX/2QQ;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 572944
    iget-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsManifest$1;->b:LX/2QQ;

    iget-object v1, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsManifest$1;->b:LX/2QQ;

    iget-object v1, v1, LX/2QQ;->s:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/2QS;->j:LX/0Tn;

    const-wide/16 v4, 0x0

    invoke-interface {v1, v2, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v2

    .line 572945
    iput-wide v2, v0, LX/2QQ;->d:J

    .line 572946
    :cond_0
    iget-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsManifest$1;->b:LX/2QQ;

    iget-object v0, v0, LX/2QQ;->p:LX/2QR;

    invoke-virtual {v0}, LX/2QR;->d()Z

    .line 572947
    iget-boolean v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsManifest$1;->a:Z

    if-eqz v0, :cond_1

    .line 572948
    iget-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsManifest$1;->b:LX/2QQ;

    const/4 v3, 0x0

    .line 572949
    iget-object v1, v0, LX/2QQ;->t:LX/0Sh;

    invoke-virtual {v1}, LX/0Sh;->b()V

    .line 572950
    iget-object v1, v0, LX/2QQ;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x1

    invoke-virtual {v1, v3, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v1

    if-nez v1, :cond_2

    .line 572951
    :goto_0
    return-void

    .line 572952
    :cond_1
    iget-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsManifest$1;->b:LX/2QQ;

    iget-object v0, v0, LX/2QQ;->n:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/facebook/platform/webdialogs/PlatformWebDialogsManifest$1$1;

    invoke-direct {v1, p0}, Lcom/facebook/platform/webdialogs/PlatformWebDialogsManifest$1$1;-><init>(Lcom/facebook/platform/webdialogs/PlatformWebDialogsManifest$1;)V

    const v2, 0x5143cfd7

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0

    .line 572953
    :cond_2
    iget-object v1, v0, LX/2QQ;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 572954
    iget-object v1, v0, LX/2QQ;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    goto :goto_0

    .line 572955
    :cond_3
    iget-object v1, v0, LX/2QQ;->m:LX/0aG;

    const-string v2, "platform_webdialogs_load_manifest"

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    const v4, 0x44abe82

    invoke-static {v1, v2, v3, v4}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v1

    invoke-interface {v1}, LX/1MF;->start()LX/1ML;

    move-result-object v1

    move-object v1, v1

    .line 572956
    new-instance v2, LX/2QT;

    invoke-direct {v2, v0}, LX/2QT;-><init>(LX/2QQ;)V

    iget-object v3, v0, LX/2QQ;->n:Ljava/util/concurrent/Executor;

    invoke-static {v1, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method
