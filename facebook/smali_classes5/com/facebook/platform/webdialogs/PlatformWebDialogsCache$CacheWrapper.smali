.class public final Lcom/facebook/platform/webdialogs/PlatformWebDialogsCache$CacheWrapper;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/platform/webdialogs/PlatformWebDialogsCache_CacheWrapperDeserializer;
.end annotation


# instance fields
.field private mCache:Ljava/util/Map;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "urlMapCache"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 573178
    const-class v0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsCache_CacheWrapperDeserializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 573179
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 573180
    return-void
.end method

.method public constructor <init>(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 573181
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 573182
    iput-object p1, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsCache$CacheWrapper;->mCache:Ljava/util/Map;

    .line 573183
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 573184
    iget-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsCache$CacheWrapper;->mCache:Ljava/util/Map;

    return-object v0
.end method
