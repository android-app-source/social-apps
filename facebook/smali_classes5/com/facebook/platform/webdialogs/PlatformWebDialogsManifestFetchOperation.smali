.class public Lcom/facebook/platform/webdialogs/PlatformWebDialogsManifestFetchOperation;
.super LX/2QZ;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements Lorg/apache/http/client/ResponseHandler;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2QZ;",
        "Lcom/facebook/common/callercontext/CallerContextable;",
        "Lorg/apache/http/client/ResponseHandler",
        "<",
        "Lorg/apache/http/HttpResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final b:LX/2Rs;

.field private c:Ljava/lang/String;

.field private d:LX/1nY;

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/2Rs;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 573151
    const-string v0, "platform_webdialogs_manifest_fetch"

    invoke-direct {p0, v0}, LX/2QZ;-><init>(Ljava/lang/String;)V

    .line 573152
    iput-object p1, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsManifestFetchOperation;->b:LX/2Rs;

    .line 573153
    return-void
.end method


# virtual methods
.method public final a(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 573143
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 573144
    const-string v1, "platform_webdialogs_manifest_fetch_URL"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsManifestFetchOperation;->c:Ljava/lang/String;

    .line 573145
    iget-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsManifestFetchOperation;->b:LX/2Rs;

    iget-object v1, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsManifestFetchOperation;->c:Ljava/lang/String;

    const-class v2, Lcom/facebook/platform/webdialogs/PlatformWebDialogsManifestFetchOperation;

    invoke-static {v2}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p0}, LX/2Rs;->a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;Lorg/apache/http/client/ResponseHandler;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 573146
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsManifestFetchOperation;->e:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 573147
    :cond_0
    sget-object v0, LX/1nY;->CONNECTION_FAILURE:LX/1nY;

    iput-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsManifestFetchOperation;->d:LX/1nY;

    .line 573148
    :cond_1
    iget-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsManifestFetchOperation;->d:LX/1nY;

    sget-object v1, LX/1nY;->NO_ERROR:LX/1nY;

    if-eq v0, v1, :cond_2

    .line 573149
    iget-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsManifestFetchOperation;->d:LX/1nY;

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 573150
    :goto_0
    return-object v0

    :cond_2
    iget-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsManifestFetchOperation;->e:Ljava/lang/String;

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0
.end method

.method public final handleResponse(Lorg/apache/http/HttpResponse;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 573139
    invoke-static {p1}, LX/2Rs;->a(Lorg/apache/http/HttpResponse;)LX/1nY;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsManifestFetchOperation;->d:LX/1nY;

    .line 573140
    iget-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsManifestFetchOperation;->d:LX/1nY;

    sget-object v1, LX/1nY;->NO_ERROR:LX/1nY;

    if-ne v0, v1, :cond_0

    .line 573141
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsManifestFetchOperation;->e:Ljava/lang/String;

    .line 573142
    :cond_0
    return-object p1
.end method
