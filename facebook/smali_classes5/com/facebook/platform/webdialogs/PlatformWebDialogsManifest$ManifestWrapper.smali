.class public final Lcom/facebook/platform/webdialogs/PlatformWebDialogsManifest$ManifestWrapper;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/platform/webdialogs/PlatformWebDialogsManifest_ManifestWrapperDeserializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/platform/webdialogs/PlatformWebDialogsManifest$ManifestWrapper;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mActionManifests:Ljava/util/List;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "actionManifests"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;",
            ">;"
        }
    .end annotation
.end field

.field private mLastManifestUrl:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "manifestUrl"
    .end annotation
.end field

.field private mLastRefreshTimestamp:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "timestamp"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 573177
    const-class v0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsManifest_ManifestWrapperDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 573154
    new-instance v0, LX/HY3;

    invoke-direct {v0}, LX/HY3;-><init>()V

    sput-object v0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsManifest$ManifestWrapper;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 573175
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 573176
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 573169
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 573170
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsManifest$ManifestWrapper;->mLastRefreshTimestamp:J

    .line 573171
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsManifest$ManifestWrapper;->mLastManifestUrl:Ljava/lang/String;

    .line 573172
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsManifest$ManifestWrapper;->mActionManifests:Ljava/util/List;

    .line 573173
    iget-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsManifest$ManifestWrapper;->mActionManifests:Ljava/util/List;

    const-class v1, LX/2QQ;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 573174
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Iterable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;",
            ">;"
        }
    .end annotation

    .prologue
    .line 573168
    iget-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsManifest$ManifestWrapper;->mActionManifests:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsManifest$ManifestWrapper;->mActionManifests:Ljava/util/List;

    invoke-static {v0}, LX/0Ph;->a(Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 573166
    iput-wide p1, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsManifest$ManifestWrapper;->mLastRefreshTimestamp:J

    .line 573167
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 573164
    iput-object p1, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsManifest$ManifestWrapper;->mLastManifestUrl:Ljava/lang/String;

    .line 573165
    return-void
.end method

.method public final a(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 573162
    iput-object p1, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsManifest$ManifestWrapper;->mActionManifests:Ljava/util/List;

    .line 573163
    return-void
.end method

.method public final b()J
    .locals 2

    .prologue
    .line 573161
    iget-wide v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsManifest$ManifestWrapper;->mLastRefreshTimestamp:J

    return-wide v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 573160
    iget-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsManifest$ManifestWrapper;->mLastManifestUrl:Ljava/lang/String;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 573159
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 573155
    iget-wide v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsManifest$ManifestWrapper;->mLastRefreshTimestamp:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 573156
    iget-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsManifest$ManifestWrapper;->mLastManifestUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 573157
    iget-object v0, p0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsManifest$ManifestWrapper;->mActionManifests:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 573158
    return-void
.end method
