.class public final Lcom/facebook/debug/fieldusage/flatbuffers/FieldAccessReporter;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final a:LX/0Zb;

.field private final b:LX/49M;

.field public final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/0Zb;LX/49M;Ljava/lang/String;)V
    .locals 0
    .param p2    # LX/49M;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 674365
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 674366
    iput-object p1, p0, Lcom/facebook/debug/fieldusage/flatbuffers/FieldAccessReporter;->a:LX/0Zb;

    .line 674367
    iput-object p2, p0, Lcom/facebook/debug/fieldusage/flatbuffers/FieldAccessReporter;->b:LX/49M;

    .line 674368
    iput-object p3, p0, Lcom/facebook/debug/fieldusage/flatbuffers/FieldAccessReporter;->c:Ljava/lang/String;

    .line 674369
    return-void
.end method

.method public static a(Lcom/facebook/debug/fieldusage/flatbuffers/FieldAccessReporter;)LX/0lF;
    .locals 5
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .prologue
    .line 674370
    iget-object v0, p0, Lcom/facebook/debug/fieldusage/flatbuffers/FieldAccessReporter;->b:LX/49M;

    invoke-virtual {v0}, LX/49M;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 674371
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 674372
    new-instance v2, LX/162;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v2, v0}, LX/162;-><init>(LX/0mC;)V

    .line 674373
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 674374
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/49L;

    .line 674375
    new-instance v3, LX/162;

    sget-object v4, LX/0mC;->a:LX/0mC;

    invoke-direct {v3, v4}, LX/162;-><init>(LX/0mC;)V

    .line 674376
    iget v4, v0, LX/49L;->a:I

    invoke-virtual {v3, v4}, LX/162;->c(I)LX/162;

    .line 674377
    iget v4, v0, LX/49L;->b:I

    invoke-virtual {v3, v4}, LX/162;->c(I)LX/162;

    .line 674378
    iget v0, v0, LX/49L;->c:I

    invoke-virtual {v3, v0}, LX/162;->c(I)LX/162;

    .line 674379
    invoke-virtual {v2, v3}, LX/162;->a(LX/0lF;)LX/162;

    goto :goto_0

    .line 674380
    :cond_0
    return-object v2
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 674381
    iget-object v0, p0, Lcom/facebook/debug/fieldusage/flatbuffers/FieldAccessReporter;->b:LX/49M;

    if-eqz v0, :cond_0

    .line 674382
    iget-object v0, p0, Lcom/facebook/debug/fieldusage/flatbuffers/FieldAccessReporter;->b:LX/49M;

    const/4 v1, 0x0

    .line 674383
    iput-boolean v1, v0, LX/49M;->c:Z

    .line 674384
    iget-object v0, p0, Lcom/facebook/debug/fieldusage/flatbuffers/FieldAccessReporter;->a:LX/0Zb;

    if-eqz v0, :cond_0

    .line 674385
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "graphql_fields_tracking"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 674386
    new-instance v1, LX/0m9;

    sget-object v2, LX/0mC;->a:LX/0mC;

    invoke-direct {v1, v2}, LX/0m9;-><init>(LX/0mC;)V

    .line 674387
    new-instance v2, LX/0m9;

    sget-object v3, LX/0mC;->a:LX/0mC;

    invoke-direct {v2, v3}, LX/0m9;-><init>(LX/0mC;)V

    .line 674388
    invoke-static {p0}, Lcom/facebook/debug/fieldusage/flatbuffers/FieldAccessReporter;->a(Lcom/facebook/debug/fieldusage/flatbuffers/FieldAccessReporter;)LX/0lF;

    move-result-object v3

    .line 674389
    const-string v4, "flatbuffer_access"

    invoke-virtual {v1, v4, v3}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 674390
    const-string v3, "flatbuffer_version"

    const/4 v4, 0x1

    invoke-virtual {v1, v3, v4}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 674391
    const-string v3, "flatbuffer_schema_id"

    const-string v4, "10155272039501729"

    invoke-virtual {v1, v3, v4}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 674392
    iget-object v3, p0, Lcom/facebook/debug/fieldusage/flatbuffers/FieldAccessReporter;->c:Ljava/lang/String;

    invoke-virtual {v2, v3, v1}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 674393
    const-string v1, "queries"

    invoke-static {p0}, Lcom/facebook/debug/fieldusage/flatbuffers/FieldAccessReporter;->a(Lcom/facebook/debug/fieldusage/flatbuffers/FieldAccessReporter;)LX/0lF;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 674394
    const-string v1, "source"

    const-string v2, "prod"

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 674395
    iget-object v1, p0, Lcom/facebook/debug/fieldusage/flatbuffers/FieldAccessReporter;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 674396
    :cond_0
    return-void
.end method
