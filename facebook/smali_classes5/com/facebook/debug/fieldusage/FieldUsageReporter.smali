.class public final Lcom/facebook/debug/fieldusage/FieldUsageReporter;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Ya;
.implements Ljava/lang/Runnable;


# instance fields
.field public final a:LX/49J;

.field public final b:Ljava/lang/Object;

.field public final c:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;LX/49J;Ljava/lang/Object;)V
    .locals 0
    .param p2    # LX/49J;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Ljava/lang/Object;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 674326
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 674327
    iput-object p1, p0, Lcom/facebook/debug/fieldusage/FieldUsageReporter;->c:LX/0Zb;

    .line 674328
    iput-object p2, p0, Lcom/facebook/debug/fieldusage/FieldUsageReporter;->a:LX/49J;

    .line 674329
    iput-object p3, p0, Lcom/facebook/debug/fieldusage/FieldUsageReporter;->b:Ljava/lang/Object;

    .line 674330
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 14

    .prologue
    .line 674331
    iget-object v0, p0, Lcom/facebook/debug/fieldusage/FieldUsageReporter;->a:LX/49J;

    if-eqz v0, :cond_5

    .line 674332
    iget-object v1, p0, Lcom/facebook/debug/fieldusage/FieldUsageReporter;->c:LX/0Zb;

    if-eqz v1, :cond_4

    invoke-static {}, LX/0sR;->a()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 674333
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "graphql_fields_tracking"

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 674334
    new-instance v2, LX/0m9;

    sget-object v3, LX/0mC;->a:LX/0mC;

    invoke-direct {v2, v3}, LX/0m9;-><init>(LX/0mC;)V

    .line 674335
    iget-object v3, p0, Lcom/facebook/debug/fieldusage/FieldUsageReporter;->a:LX/49J;

    .line 674336
    new-instance v7, LX/026;

    invoke-direct {v7}, LX/026;-><init>()V

    .line 674337
    iget-object v6, v3, LX/49J;->b:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/49K;

    .line 674338
    iget-boolean v9, v3, LX/49J;->h:Z

    invoke-virtual {v6, v7, v9}, LX/49K;->a(Ljava/util/Map;Z)V

    goto :goto_0

    .line 674339
    :cond_0
    new-instance v8, LX/0m9;

    sget-object v6, LX/0mC;->a:LX/0mC;

    invoke-direct {v8, v6}, LX/0m9;-><init>(LX/0mC;)V

    .line 674340
    invoke-interface {v7}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map$Entry;

    .line 674341
    new-instance v10, LX/0m9;

    sget-object v7, LX/0mC;->a:LX/0mC;

    invoke-direct {v10, v7}, LX/0m9;-><init>(LX/0mC;)V

    .line 674342
    const-string v11, "set_count"

    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/Map;

    const-string v12, "set_count"

    invoke-interface {v7, v12}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Long;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    invoke-virtual {v10, v11, v12, v13}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 674343
    const-string v11, "used_count"

    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/Map;

    const-string v12, "used_count"

    invoke-interface {v7, v12}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Long;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    invoke-virtual {v10, v11, v12, v13}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 674344
    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v8, v6, v10}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    goto :goto_1

    .line 674345
    :cond_1
    new-instance v6, LX/0m9;

    sget-object v7, LX/0mC;->a:LX/0mC;

    invoke-direct {v6, v7}, LX/0m9;-><init>(LX/0mC;)V

    .line 674346
    const-string v7, "tr_start_date"

    new-instance v9, Ljava/util/Date;

    invoke-direct {v9}, Ljava/util/Date;-><init>()V

    invoke-virtual {v9}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v7, v9}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 674347
    const-string v7, "age"

    iget-object v9, v3, LX/49J;->g:LX/0So;

    invoke-interface {v9}, LX/0So;->now()J

    move-result-wide v10

    iget-wide v12, v3, LX/49J;->f:J

    sub-long/2addr v10, v12

    const-wide/16 v12, 0x3e8

    div-long/2addr v10, v12

    invoke-virtual {v6, v7, v10, v11}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 674348
    const-string v7, "fields"

    invoke-virtual {v6, v7, v8}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 674349
    iget-object v7, v3, LX/49J;->e:Ljava/lang/String;

    if-eqz v7, :cond_2

    iget-object v7, v3, LX/49J;->e:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_2

    .line 674350
    const-string v7, "persist_id"

    iget-object v8, v3, LX/49J;->e:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 674351
    :cond_2
    move-object v3, v6

    .line 674352
    iget-object v4, p0, Lcom/facebook/debug/fieldusage/FieldUsageReporter;->a:LX/49J;

    .line 674353
    iget-boolean v5, v4, LX/49J;->h:Z

    move v4, v5

    .line 674354
    if-eqz v4, :cond_3

    .line 674355
    const-string v4, "flatbuffer_version"

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 674356
    const-string v4, "flatbuffer_schema_id"

    const-string v5, "10155272039501729"

    invoke-virtual {v3, v4, v5}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 674357
    :cond_3
    iget-object v4, p0, Lcom/facebook/debug/fieldusage/FieldUsageReporter;->a:LX/49J;

    .line 674358
    iget-object v5, v4, LX/49J;->d:Ljava/lang/String;

    move-object v4, v5

    .line 674359
    invoke-virtual {v2, v4, v3}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 674360
    const-string v3, "queries"

    invoke-virtual {v1, v3, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 674361
    const-string v2, "source"

    const-string v3, "prod"

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 674362
    iget-object v2, p0, Lcom/facebook/debug/fieldusage/FieldUsageReporter;->c:LX/0Zb;

    invoke-interface {v2, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 674363
    :cond_4
    iget-object v1, p0, Lcom/facebook/debug/fieldusage/FieldUsageReporter;->b:Ljava/lang/Object;

    invoke-static {v1}, LX/0sR;->a(Ljava/lang/Object;)V

    .line 674364
    :cond_5
    return-void
.end method
