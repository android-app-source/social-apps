.class public Lcom/facebook/debug/activitytracer/ActivityTracer;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile g:Lcom/facebook/debug/activitytracer/ActivityTracer;


# instance fields
.field private final b:LX/0Sh;

.field private final c:LX/0Sg;

.field private final d:LX/49F;

.field private final e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/49G;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Landroid/os/Handler;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 674225
    const-class v0, Lcom/facebook/debug/activitytracer/ActivityTracer;

    sput-object v0, Lcom/facebook/debug/activitytracer/ActivityTracer;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Sh;LX/0Sg;LX/49F;Ljava/util/Set;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "Lcom/facebook/common/appchoreographer/AppChoreographer;",
            "LX/49F;",
            "Ljava/util/Set",
            "<",
            "LX/49G;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 674218
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 674219
    iput-object p1, p0, Lcom/facebook/debug/activitytracer/ActivityTracer;->b:LX/0Sh;

    .line 674220
    iput-object p2, p0, Lcom/facebook/debug/activitytracer/ActivityTracer;->c:LX/0Sg;

    .line 674221
    iput-object p3, p0, Lcom/facebook/debug/activitytracer/ActivityTracer;->d:LX/49F;

    .line 674222
    iput-object p4, p0, Lcom/facebook/debug/activitytracer/ActivityTracer;->e:Ljava/util/Set;

    .line 674223
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/facebook/debug/activitytracer/ActivityTracer;->f:Landroid/os/Handler;

    .line 674224
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/debug/activitytracer/ActivityTracer;
    .locals 9

    .prologue
    .line 674203
    sget-object v0, Lcom/facebook/debug/activitytracer/ActivityTracer;->g:Lcom/facebook/debug/activitytracer/ActivityTracer;

    if-nez v0, :cond_1

    .line 674204
    const-class v1, Lcom/facebook/debug/activitytracer/ActivityTracer;

    monitor-enter v1

    .line 674205
    :try_start_0
    sget-object v0, Lcom/facebook/debug/activitytracer/ActivityTracer;->g:Lcom/facebook/debug/activitytracer/ActivityTracer;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 674206
    if-eqz v2, :cond_0

    .line 674207
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 674208
    new-instance v6, Lcom/facebook/debug/activitytracer/ActivityTracer;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v3

    check-cast v3, LX/0Sh;

    invoke-static {v0}, LX/0Sg;->a(LX/0QB;)LX/0Sg;

    move-result-object v4

    check-cast v4, LX/0Sg;

    const-class v5, LX/49F;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/49F;

    .line 674209
    new-instance v7, LX/0U8;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v8

    new-instance p0, LX/49H;

    invoke-direct {p0, v0}, LX/49H;-><init>(LX/0QB;)V

    invoke-direct {v7, v8, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v7, v7

    .line 674210
    invoke-direct {v6, v3, v4, v5, v7}, Lcom/facebook/debug/activitytracer/ActivityTracer;-><init>(LX/0Sh;LX/0Sg;LX/49F;Ljava/util/Set;)V

    .line 674211
    move-object v0, v6

    .line 674212
    sput-object v0, Lcom/facebook/debug/activitytracer/ActivityTracer;->g:Lcom/facebook/debug/activitytracer/ActivityTracer;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 674213
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 674214
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 674215
    :cond_1
    sget-object v0, Lcom/facebook/debug/activitytracer/ActivityTracer;->g:Lcom/facebook/debug/activitytracer/ActivityTracer;

    return-object v0

    .line 674216
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 674217
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
