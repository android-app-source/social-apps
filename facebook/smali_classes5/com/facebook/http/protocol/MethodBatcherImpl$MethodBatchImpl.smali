.class public final Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;
.super LX/2VJ;
.source ""


# instance fields
.field public final synthetic a:LX/18W;

.field private b:LX/18Z;


# direct methods
.method public constructor <init>(LX/18W;)V
    .locals 0

    .prologue
    .line 575229
    iput-object p1, p0, Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;->a:LX/18W;

    invoke-direct {p0}, LX/2VJ;-><init>()V

    .line 575230
    return-void
.end method

.method private static a(Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;LX/0s9;)Landroid/net/Uri;
    .locals 3

    .prologue
    .line 575207
    invoke-interface {p1}, LX/0s9;->b()Landroid/net/Uri$Builder;

    move-result-object v1

    .line 575208
    iget-object v0, p0, Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;->a:LX/18W;

    iget-object v0, v0, LX/18W;->h:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 575209
    const-string v0, "phprof_sample"

    const-string v2, "1"

    invoke-virtual {v1, v0, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 575210
    iget-object v0, p0, Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;->a:LX/18W;

    iget-object v0, v0, LX/18W;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 575211
    if-eqz v0, :cond_0

    .line 575212
    const-string v2, "phprof_user"

    invoke-virtual {v1, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 575213
    :cond_0
    iget-object v0, p0, Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;->a:LX/18W;

    iget-object v0, v0, LX/18W;->j:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 575214
    const-string v0, "wirehog_sample"

    const-string v2, "1"

    invoke-virtual {v1, v0, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 575215
    iget-object v0, p0, Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;->a:LX/18W;

    iget-object v0, v0, LX/18W;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 575216
    if-eqz v0, :cond_1

    .line 575217
    const-string v2, "wirehog_user"

    invoke-virtual {v1, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 575218
    :cond_1
    iget-object v0, p0, Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;->a:LX/18W;

    iget-object v0, v0, LX/18W;->k:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 575219
    const-string v0, "artillery_sample"

    const-string v2, "1"

    invoke-virtual {v1, v0, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 575220
    :cond_2
    const-string v0, "include_headers"

    const-string v2, "false"

    invoke-virtual {v1, v0, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 575221
    const-string v0, "decode_body_json"

    const-string v2, "false"

    invoke-virtual {v1, v0, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 575222
    const-string v0, "streamable_json_response"

    const-string v2, "true"

    invoke-virtual {v1, v0, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 575223
    iget-object v0, p0, Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;->a:LX/18W;

    iget-object v0, v0, LX/18W;->q:LX/11N;

    .line 575224
    const-string v2, "locale"

    iget-object p0, v0, LX/11N;->a:LX/0W9;

    invoke-virtual {p0}, LX/0W9;->c()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, v2, p0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 575225
    iget-object v2, v0, LX/11N;->b:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 575226
    if-eqz v2, :cond_3

    .line 575227
    const-string p0, "client_country_code"

    invoke-virtual {v1, p0, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 575228
    :cond_3
    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/14U;Ljava/util/List;)Lcom/facebook/http/interfaces/RequestPriority;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/14U;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/http/interfaces/RequestPriority;",
            ">;)",
            "Lcom/facebook/http/interfaces/RequestPriority;"
        }
    .end annotation

    .prologue
    .line 575195
    if-eqz p0, :cond_1

    .line 575196
    iget-object v0, p0, LX/14U;->e:Lcom/facebook/http/interfaces/RequestPriority;

    move-object v0, v0

    .line 575197
    if-eqz v0, :cond_1

    .line 575198
    iget-object v0, p0, LX/14U;->e:Lcom/facebook/http/interfaces/RequestPriority;

    move-object v1, v0

    .line 575199
    :cond_0
    :goto_0
    return-object v1

    .line 575200
    :cond_1
    const/4 v1, 0x0

    .line 575201
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/http/interfaces/RequestPriority;

    .line 575202
    if-eqz v0, :cond_2

    .line 575203
    invoke-virtual {v0, v1}, Lcom/facebook/http/interfaces/RequestPriority;->isHigherPriorityThan(Lcom/facebook/http/interfaces/RequestPriority;)Z

    move-result v3

    if-eqz v3, :cond_4

    :goto_2
    move-object v1, v0

    .line 575204
    goto :goto_1

    .line 575205
    :cond_3
    if-nez v1, :cond_0

    .line 575206
    invoke-static {}, LX/15A;->a()Lcom/facebook/http/interfaces/RequestPriority;

    move-result-object v1

    goto :goto_0

    :cond_4
    move-object v0, v1

    goto :goto_2
.end method

.method private static a(Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;Ljava/util/List;Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)Lorg/apache/http/HttpEntity;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/2Vr",
            "<**>;>;",
            "Ljava/lang/String;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")",
            "Lorg/apache/http/HttpEntity;"
        }
    .end annotation

    .prologue
    .line 575148
    iget-object v0, p0, Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;->a:LX/18W;

    iget-object v0, v0, LX/18W;->x:LX/0Zh;

    invoke-virtual {v0}, LX/0Zh;->b()LX/0n9;

    move-result-object v2

    .line 575149
    const-string v0, "batch"

    invoke-virtual {v2, v0}, LX/0n9;->c(Ljava/lang/String;)LX/0zL;

    move-result-object v1

    .line 575150
    invoke-static {}, LX/0nC;->a()LX/0nC;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0nA;->a(LX/0nD;)V

    .line 575151
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Vr;

    .line 575152
    iget-object v0, v0, LX/2Vr;->c:LX/0n9;

    invoke-virtual {v1, v0}, LX/0zL;->c(LX/0nA;)V

    goto :goto_0

    .line 575153
    :cond_0
    const-string v0, "fb_api_caller_class"

    .line 575154
    iget-object v1, p3, Lcom/facebook/common/callercontext/CallerContext;->b:Ljava/lang/String;

    move-object v1, v1

    .line 575155
    invoke-static {v2, v0, v1}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 575156
    const-string v0, "fb_api_req_friendly_name"

    .line 575157
    invoke-static {v2, v0, p2}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 575158
    invoke-virtual {p0}, LX/2VJ;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 575159
    invoke-virtual {p0}, LX/2VJ;->d()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 575160
    iget-object v0, p0, LX/2VJ;->f:LX/4cv;

    move-object v3, v0

    .line 575161
    const-string v0, "device_api"

    invoke-virtual {v2, v0}, LX/0n9;->b(Ljava/lang/String;)LX/0n9;

    move-result-object v4

    .line 575162
    invoke-static {}, LX/0nC;->a()LX/0nC;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0nA;->a(LX/0nD;)V

    .line 575163
    const-string v0, "method"

    iget-object v1, v3, LX/4cv;->a:Ljava/lang/String;

    .line 575164
    invoke-static {v4, v0, v1}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 575165
    const-string v0, "device_context"

    invoke-virtual {v4, v0}, LX/0n9;->b(Ljava/lang/String;)LX/0n9;

    move-result-object v5

    .line 575166
    iget-object v0, v3, LX/4cv;->b:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 575167
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 575168
    invoke-static {v5, v1, v0}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 575169
    goto :goto_1

    .line 575170
    :cond_1
    const-string v0, "app_context"

    invoke-virtual {v4, v0}, LX/0n9;->b(Ljava/lang/String;)LX/0n9;

    move-result-object v5

    .line 575171
    iget-object v0, v3, LX/4cv;->c:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 575172
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 575173
    invoke-static {v5, v1, v0}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 575174
    goto :goto_2

    .line 575175
    :cond_2
    const-string v0, "method_context"

    invoke-virtual {v4, v0}, LX/0n9;->b(Ljava/lang/String;)LX/0n9;

    move-result-object v4

    .line 575176
    iget-object v0, v3, LX/4cv;->d:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 575177
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 575178
    invoke-static {v4, v1, v0}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 575179
    goto :goto_3

    .line 575180
    :cond_3
    iget-object v0, p0, Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;->b:LX/18Z;

    invoke-interface {v0, v2}, LX/18Z;->a(LX/0n9;)V

    .line 575181
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Vr;

    .line 575182
    iget-object v0, v0, LX/2Vr;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 575183
    const/4 v0, 0x1

    .line 575184
    :goto_4
    move v0, v0

    .line 575185
    if-eqz v0, :cond_7

    .line 575186
    new-instance v1, LX/4cM;

    invoke-direct {v1}, LX/4cM;-><init>()V

    .line 575187
    invoke-virtual {v1, v2}, LX/4cM;->a(LX/0n9;)V

    .line 575188
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Vr;

    .line 575189
    iget-object v4, v0, LX/2Vr;->d:LX/0Px;

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v2, v0

    :goto_5
    if-ge v2, v5, :cond_5

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4cQ;

    .line 575190
    iget-object v6, v0, LX/4cQ;->a:Ljava/lang/String;

    move-object v6, v6

    .line 575191
    iget-object p0, v0, LX/4cQ;->c:LX/4cO;

    move-object v0, p0

    .line 575192
    invoke-virtual {v1, v6, v0}, LX/4cL;->a(Ljava/lang/String;LX/4cO;)V

    .line 575193
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_5

    :cond_6
    move-object v0, v1

    .line 575194
    :goto_6
    return-object v0

    :cond_7
    new-instance v0, LX/14d;

    invoke-direct {v0, v2}, LX/14d;-><init>(LX/0n9;)V

    goto :goto_6

    :cond_8
    const/4 v0, 0x0

    goto :goto_4
.end method

.method private a(LX/14U;Ljava/util/List;Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;LX/0s9;)Lorg/apache/http/client/methods/HttpPost;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/14U;",
            "Ljava/util/List",
            "<",
            "LX/2Vr",
            "<**>;>;",
            "Ljava/lang/String;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "LX/0s9;",
            ")",
            "Lorg/apache/http/client/methods/HttpPost;"
        }
    .end annotation

    .prologue
    .line 575105
    invoke-static {p0, p2, p3, p4}, Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;->a(Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;Ljava/util/List;Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)Lorg/apache/http/HttpEntity;

    move-result-object v0

    invoke-static {v0}, LX/14f;->a(Lorg/apache/http/HttpEntity;)Lorg/apache/http/HttpEntity;

    move-result-object v1

    .line 575106
    invoke-static {p0, p5}, Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;->a(Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;LX/0s9;)Landroid/net/Uri;

    move-result-object v0

    .line 575107
    new-instance v2, Lorg/apache/http/client/methods/HttpPost;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 575108
    const-string v0, "authLogin"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_b

    const-string v0, "handleGetSessionlessQEs"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_b

    const-string v0, "logged_out_set_nonce"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_b

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 575109
    if-eqz v0, :cond_9

    .line 575110
    iget-object v0, p0, Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;->a:LX/18W;

    iget-object v0, v0, LX/18W;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 575111
    if-eqz v0, :cond_0

    .line 575112
    iget-object v3, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->b:Ljava/lang/String;

    move-object v0, v3

    .line 575113
    :goto_1
    if-nez v0, :cond_1

    .line 575114
    new-instance v0, LX/4cl;

    const-string v1, "auth token is null, user logged out?"

    invoke-direct {v0, v1}, LX/4cl;-><init>(Ljava/lang/String;)V

    throw v0

    .line 575115
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 575116
    :cond_1
    const-string v3, "Authorization"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "OAuth "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Lorg/apache/http/client/methods/HttpPost;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 575117
    :cond_2
    :goto_2
    invoke-virtual {v2, v1}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 575118
    invoke-interface {p5}, LX/0s9;->h()Ljava/lang/String;

    move-result-object v0

    .line 575119
    if-eqz v0, :cond_3

    .line 575120
    const-string v1, "User-Agent"

    invoke-virtual {v2, v1, v0}, Lorg/apache/http/client/methods/HttpPost;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 575121
    :cond_3
    iget-object v0, p1, LX/14U;->f:LX/0Px;

    move-object v0, v0

    .line 575122
    if-eqz v0, :cond_4

    .line 575123
    iget-object v0, p1, LX/14U;->f:LX/0Px;

    move-object v3, v0

    .line 575124
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_3
    if-ge v1, v4, :cond_4

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/Header;

    .line 575125
    invoke-interface {v2, v0}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Lorg/apache/http/Header;)V

    .line 575126
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 575127
    :cond_4
    invoke-interface {p5}, LX/0s9;->i()Ljava/lang/String;

    move-result-object v0

    .line 575128
    if-eqz v0, :cond_5

    .line 575129
    const-string v1, "X-FB-Connection-Type"

    invoke-virtual {v2, v1, v0}, Lorg/apache/http/client/methods/HttpPost;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 575130
    :cond_5
    iget-object v0, p0, Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;->a:LX/18W;

    iget-object v0, v0, LX/18W;->t:LX/0rz;

    invoke-virtual {v0}, LX/0rz;->a()Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "fetch-feed-batch"

    invoke-virtual {p4}, Lcom/facebook/common/callercontext/CallerContext;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 575131
    const-string v0, "X-FB-Priming-Channel-ID"

    .line 575132
    sget-object v1, LX/00c;->a:LX/00c;

    move-object v1, v1

    .line 575133
    iget-object v3, v1, LX/00c;->c:Ljava/lang/String;

    move-object v1, v3

    .line 575134
    invoke-virtual {v2, v0, v1}, Lorg/apache/http/client/methods/HttpPost;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 575135
    :cond_6
    iget-object v0, p1, LX/14U;->g:Lcom/facebook/fbtrace/FbTraceNode;

    move-object v0, v0

    .line 575136
    sget-object v1, Lcom/facebook/fbtrace/FbTraceNode;->a:Lcom/facebook/fbtrace/FbTraceNode;

    if-eq v0, v1, :cond_7

    .line 575137
    const-string v0, "X-FBTrace-Sampled"

    const-string v1, "true"

    invoke-virtual {v2, v0, v1}, Lorg/apache/http/client/methods/HttpPost;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 575138
    const-string v0, "X-FBTrace-Meta"

    .line 575139
    iget-object v1, p1, LX/14U;->g:Lcom/facebook/fbtrace/FbTraceNode;

    move-object v1, v1

    .line 575140
    invoke-virtual {v1}, Lcom/facebook/fbtrace/FbTraceNode;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Lorg/apache/http/client/methods/HttpPost;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 575141
    :cond_7
    iget-object v0, p0, Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;->a:LX/18W;

    iget-object v0, v0, LX/18W;->m:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 575142
    const-string v0, "Date"

    iget-object v1, p0, Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;->a:LX/18W;

    iget-object v1, v1, LX/18W;->B:LX/11S;

    sget-object v3, LX/1lB;->RFC1123_STYLE:LX/1lB;

    iget-object v4, p0, Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;->a:LX/18W;

    iget-object v4, v4, LX/18W;->A:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    invoke-interface {v1, v3, v4, v5}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Lorg/apache/http/client/methods/HttpPost;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 575143
    :cond_8
    return-object v2

    .line 575144
    :cond_9
    const-string v0, "authLogin"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_a

    const-string v0, "handleGetSessionlessQEs"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_a

    const-string v0, "logged_out_set_nonce"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    :cond_a
    const/4 v0, 0x1

    :goto_4
    move v0, v0

    .line 575145
    if-eqz v0, :cond_2

    .line 575146
    const-string v0, "|"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;->a:LX/18W;

    iget-object v5, v5, LX/18W;->y:LX/00I;

    invoke-interface {v5}, LX/00I;->c()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;->a:LX/18W;

    iget-object v5, v5, LX/18W;->y:LX/00I;

    invoke-interface {v5}, LX/00I;->e()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v0, v3}, LX/0YN;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 575147
    const-string v3, "Authorization"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "OAuth "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Lorg/apache/http/client/methods/HttpPost;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_b
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_c
    const/4 v0, 0x0

    goto :goto_4
.end method

.method public static a$redex0(Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;LX/2Vr;LX/15w;Lorg/apache/http/HttpResponse;LX/14U;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;
    .locals 9
    .param p4    # LX/14U;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<P:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "LX/2Vr",
            "<TP;TR;>;",
            "LX/15w;",
            "Lorg/apache/http/HttpResponse;",
            "LX/14U;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")",
            "Ljava/lang/Object;"
        }
    .end annotation

    .prologue
    .line 575043
    iget-object v7, p1, LX/2Vr;->a:LX/2Vj;

    .line 575044
    iget-object v0, v7, LX/2Vj;->a:LX/0e6;

    move-object v6, v0

    .line 575045
    iget-object v1, p1, LX/2Vr;->b:LX/14N;

    .line 575046
    iget-object v0, p0, Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;->b:LX/18Z;

    iget-object v2, p0, Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;->a:LX/18W;

    iget-object v2, v2, LX/18W;->p:LX/11M;

    invoke-interface {v0, v7, p2, v2}, LX/18Z;->a(LX/2Vj;LX/15w;LX/11M;)LX/2WM;

    move-result-object v2

    .line 575047
    instance-of v0, v6, LX/0rp;

    if-eqz v0, :cond_0

    move-object v0, v6

    .line 575048
    check-cast v0, LX/0rp;

    .line 575049
    iget-object v3, v7, LX/2Vj;->b:Ljava/lang/Object;

    move-object v3, v3

    .line 575050
    invoke-interface {v0, v3}, LX/0rp;->c(Ljava/lang/Object;)V

    .line 575051
    :cond_0
    sget-object v0, LX/2WM;->a:LX/2WM;

    if-ne v2, v0, :cond_1

    .line 575052
    new-instance v0, LX/4cm;

    .line 575053
    iget-object v1, v7, LX/2Vj;->c:Ljava/lang/String;

    move-object v1, v1

    .line 575054
    invoke-direct {v0, v1}, LX/4cm;-><init>(Ljava/lang/String;)V

    throw v0

    .line 575055
    :cond_1
    iget-object v0, v2, LX/2WM;->e:LX/15w;

    move-object v3, v0

    .line 575056
    iget-object v0, p0, Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;->a:LX/18W;

    iget-object v0, v0, LX/18W;->o:LX/0lC;

    invoke-virtual {v3, v0}, LX/15w;->a(LX/0lD;)V

    .line 575057
    iget-object v0, p1, LX/2Vr;->a:LX/2Vj;

    .line 575058
    iget-object v4, v0, LX/2Vj;->c:Ljava/lang/String;

    move-object v0, v4

    .line 575059
    if-eqz v0, :cond_3

    const-string v4, "first-fetch"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p3}, LX/0rz;->a(Lorg/apache/http/HttpResponse;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v5, 0x1

    .line 575060
    :goto_0
    iget-object v0, p4, LX/14U;->d:Ljava/util/List;

    move-object v0, v0

    .line 575061
    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    .line 575062
    iget-object v0, v1, LX/14N;->k:LX/14S;

    move-object v0, v0

    .line 575063
    sget-object v4, LX/14S;->JSONPARSER:LX/14S;

    if-ne v0, v4, :cond_4

    .line 575064
    new-instance v0, LX/1pN;

    .line 575065
    iget v4, v2, LX/2WM;->b:I

    move v2, v4

    .line 575066
    iget-object v4, p0, Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;->a:LX/18W;

    iget-object v4, v4, LX/18W;->p:LX/11M;

    invoke-direct/range {v0 .. v5}, LX/1pN;-><init>(LX/14N;ILX/15w;LX/11M;Z)V

    .line 575067
    :goto_1
    const/4 v2, 0x0

    .line 575068
    instance-of v1, v6, LX/0ro;

    if-eqz v1, :cond_9

    move-object v3, v6

    .line 575069
    check-cast v3, LX/0ro;

    .line 575070
    const/4 v1, 0x0

    .line 575071
    :try_start_0
    iget-object v4, v7, LX/2Vj;->b:Ljava/lang/Object;

    move-object v4, v4

    .line 575072
    invoke-interface {v3, v4, v0}, LX/0e6;->a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    :try_end_0
    .catch LX/4cz; {:try_start_0 .. :try_end_0} :catch_0
    .catch LX/4d0; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 575073
    :goto_2
    if-eqz v1, :cond_2

    .line 575074
    iget-object v0, v7, LX/2Vj;->b:Ljava/lang/Object;

    move-object v0, v0

    .line 575075
    invoke-interface {v3, v0}, LX/0e6;->a(Ljava/lang/Object;)LX/14N;

    move-result-object v1

    .line 575076
    iget-object v0, p0, Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;->a:LX/18W;

    iget-object v0, v0, LX/18W;->r:Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    const/4 v4, 0x0

    .line 575077
    iget-object v2, v7, LX/2Vj;->b:Ljava/lang/Object;

    move-object v5, v2

    .line 575078
    move-object v2, p4

    move-object v6, p5

    invoke-virtual/range {v0 .. v6}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/14N;LX/14U;LX/0e6;LX/0rp;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)LX/1pW;

    move-result-object v0

    .line 575079
    iget-object v1, v0, LX/1pW;->b:Ljava/lang/Object;

    move-object v0, v1

    .line 575080
    :cond_2
    :goto_3
    return-object v0

    .line 575081
    :cond_3
    const/4 v5, 0x0

    goto :goto_0

    .line 575082
    :cond_4
    iget-object v0, v1, LX/14N;->k:LX/14S;

    move-object v0, v0

    .line 575083
    sget-object v4, LX/14S;->STREAM:LX/14S;

    if-eq v0, v4, :cond_5

    .line 575084
    iget-object v0, v1, LX/14N;->k:LX/14S;

    move-object v0, v0

    .line 575085
    sget-object v4, LX/14S;->FLATBUFFER:LX/14S;

    if-ne v0, v4, :cond_6

    .line 575086
    :cond_5
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not supportable"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 575087
    :cond_6
    iget-object v0, v1, LX/14N;->k:LX/14S;

    move-object v0, v0

    .line 575088
    sget-object v4, LX/14S;->JSON:LX/14S;

    if-ne v0, v4, :cond_7

    .line 575089
    new-instance v0, LX/1pN;

    .line 575090
    iget v4, v2, LX/2WM;->b:I

    move v2, v4

    .line 575091
    const-class v4, LX/0lF;

    invoke-virtual {v3, v4}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0lF;

    iget-object v4, p0, Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;->a:LX/18W;

    iget-object v4, v4, LX/18W;->p:LX/11M;

    invoke-direct/range {v0 .. v5}, LX/1pN;-><init>(LX/14N;ILX/0lF;LX/11M;Z)V

    goto :goto_1

    .line 575092
    :cond_7
    iget-object v0, v1, LX/14N;->k:LX/14S;

    move-object v0, v0

    .line 575093
    sget-object v4, LX/14S;->STRING:LX/14S;

    if-ne v0, v4, :cond_8

    .line 575094
    new-instance v0, LX/1pN;

    .line 575095
    iget v4, v2, LX/2WM;->b:I

    move v2, v4

    .line 575096
    iget-object v4, p0, Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;->a:LX/18W;

    iget-object v4, v4, LX/18W;->o:LX/0lC;

    const-class v8, LX/0lF;

    invoke-virtual {v3, v8}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v4, v3}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;->a:LX/18W;

    iget-object v4, v4, LX/18W;->p:LX/11M;

    invoke-direct/range {v0 .. v5}, LX/1pN;-><init>(LX/14N;ILjava/lang/String;LX/11M;Z)V

    goto :goto_1

    .line 575097
    :cond_8
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown api response type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 575098
    :catch_0
    move-exception v0

    .line 575099
    sget-object v1, LX/18W;->a:Ljava/lang/Class;

    const-string v4, "Invalid persisted graphql query id"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v1, v0, v4, v5}, LX/01m;->c(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 575100
    const/4 v0, 0x1

    move v1, v0

    move-object v0, v2

    .line 575101
    goto/16 :goto_2

    .line 575102
    :catch_1
    const/4 v0, 0x1

    move v1, v0

    move-object v0, v2

    goto/16 :goto_2

    .line 575103
    :cond_9
    iget-object v1, v7, LX/2Vj;->b:Ljava/lang/Object;

    move-object v1, v1

    .line 575104
    invoke-interface {v6, v1, v0}, LX/0e6;->a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_3
.end method

.method private static b(Ljava/util/List;)LX/14Q;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/2Vr",
            "<**>;>;)",
            "LX/14Q;"
        }
    .end annotation

    .prologue
    .line 575037
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Vr;

    .line 575038
    iget-object v0, v0, LX/2Vr;->b:LX/14N;

    .line 575039
    iget-object v2, v0, LX/14N;->w:LX/14Q;

    move-object v0, v2

    .line 575040
    sget-object v2, LX/14Q;->FALLBACK_REQUIRED:LX/14Q;

    if-ne v0, v2, :cond_0

    .line 575041
    sget-object v0, LX/14Q;->FALLBACK_REQUIRED:LX/14Q;

    .line 575042
    :goto_0
    return-object v0

    :cond_1
    sget-object v0, LX/14Q;->FALLBACK_NOT_REQUIRED:LX/14Q;

    goto :goto_0
.end method

.method private b(LX/2Vj;)LX/2Vr;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<P:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "LX/2Vj",
            "<TP;TR;>;)",
            "LX/2Vr",
            "<TP;TR;>;"
        }
    .end annotation

    .prologue
    .line 574913
    iget-object v0, p1, LX/2Vj;->a:LX/0e6;

    move-object v1, v0

    .line 574914
    const/4 v0, 0x0

    .line 574915
    instance-of v2, v1, LX/0ro;

    if-eqz v2, :cond_0

    move-object v0, v1

    .line 574916
    check-cast v0, LX/0ro;

    .line 574917
    iget-object v2, p1, LX/2Vj;->b:Ljava/lang/Object;

    move-object v2, v2

    .line 574918
    invoke-virtual {v0, v2}, LX/0ro;->d(Ljava/lang/Object;)LX/14N;

    move-result-object v0

    .line 574919
    :cond_0
    if-nez v0, :cond_10

    .line 574920
    iget-object v0, p1, LX/2Vj;->b:Ljava/lang/Object;

    move-object v0, v0

    .line 574921
    invoke-interface {v1, v0}, LX/0e6;->a(Ljava/lang/Object;)LX/14N;

    move-result-object v0

    move-object v2, v0

    .line 574922
    :goto_0
    instance-of v0, v1, LX/0rp;

    if-eqz v0, :cond_1

    .line 574923
    check-cast v1, LX/0rp;

    .line 574924
    iget-object v0, p1, LX/2Vj;->b:Ljava/lang/Object;

    move-object v0, v0

    .line 574925
    invoke-interface {v1, v0}, LX/0rp;->a_(Ljava/lang/Object;)V

    .line 574926
    :cond_1
    iget-object v0, p0, Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;->a:LX/18W;

    iget-object v0, v0, LX/18W;->x:LX/0Zh;

    invoke-virtual {v0}, LX/0Zh;->b()LX/0n9;

    move-result-object v3

    .line 574927
    invoke-static {}, LX/0nC;->a()LX/0nC;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0nA;->a(LX/0nD;)V

    .line 574928
    const-string v0, "method"

    .line 574929
    iget-object v1, v2, LX/14N;->b:Ljava/lang/String;

    move-object v1, v1

    .line 574930
    invoke-static {v3, v0, v1}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 574931
    iget-object v0, p0, Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;->a:LX/18W;

    iget-object v0, v0, LX/18W;->q:LX/11N;

    invoke-virtual {v0, v2}, LX/11N;->a(LX/14N;)LX/0n9;

    move-result-object v1

    .line 574932
    iget-object v0, p0, Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;->a:LX/18W;

    iget-object v0, v0, LX/18W;->h:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 574933
    const-string v0, "phprof_sample"

    const-string v4, "1"

    .line 574934
    invoke-static {v1, v0, v4}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 574935
    iget-object v0, p0, Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;->a:LX/18W;

    iget-object v0, v0, LX/18W;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 574936
    if-eqz v0, :cond_2

    .line 574937
    const-string v4, "phprof_user"

    .line 574938
    invoke-static {v1, v4, v0}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 574939
    :cond_2
    iget-object v0, p0, Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;->a:LX/18W;

    iget-object v0, v0, LX/18W;->i:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 574940
    const-string v0, "teak_sample"

    const-string v4, "1"

    .line 574941
    invoke-static {v1, v0, v4}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 574942
    iget-object v0, p0, Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;->a:LX/18W;

    iget-object v0, v0, LX/18W;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 574943
    if-eqz v0, :cond_3

    .line 574944
    const-string v4, "teak_user"

    .line 574945
    invoke-static {v1, v4, v0}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 574946
    :cond_3
    iget-object v0, p0, Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;->a:LX/18W;

    iget-object v0, v0, LX/18W;->j:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 574947
    const-string v0, "wirehog_sample"

    const-string v4, "1"

    .line 574948
    invoke-static {v1, v0, v4}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 574949
    iget-object v0, p0, Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;->a:LX/18W;

    iget-object v0, v0, LX/18W;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 574950
    if-eqz v0, :cond_4

    .line 574951
    const-string v4, "wirehog_user"

    .line 574952
    invoke-static {v1, v4, v0}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 574953
    :cond_4
    iget-object v0, p0, Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;->a:LX/18W;

    iget-object v0, v0, LX/18W;->k:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 574954
    const-string v0, "artillery_sample"

    const-string v4, "1"

    .line 574955
    invoke-static {v1, v0, v4}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 574956
    :cond_5
    const-string v0, "fb_api_req_friendly_name"

    .line 574957
    iget-object v4, v2, LX/14N;->a:Ljava/lang/String;

    move-object v4, v4

    .line 574958
    invoke-static {v1, v0, v4}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 574959
    move-object v1, v1

    .line 574960
    iget-object v0, v2, LX/14N;->c:Ljava/lang/String;

    move-object v0, v0

    .line 574961
    const-string v4, "GET"

    .line 574962
    iget-object v5, v2, LX/14N;->b:Ljava/lang/String;

    move-object v5, v5

    .line 574963
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 574964
    invoke-static {v1}, LX/14d;->a(LX/0n9;)Ljava/lang/String;

    move-result-object v4

    .line 574965
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, "?"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 574966
    invoke-virtual {v1}, LX/0nA;->a()V

    .line 574967
    :goto_1
    iget-object v1, p1, LX/2Vj;->c:Ljava/lang/String;

    move-object v1, v1

    .line 574968
    if-eqz v1, :cond_6

    .line 574969
    const-string v1, "name"

    .line 574970
    iget-object v4, p1, LX/2Vj;->c:Ljava/lang/String;

    move-object v4, v4

    .line 574971
    invoke-static {v3, v1, v4}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 574972
    :cond_6
    iget-object v1, p1, LX/2Vj;->d:Ljava/lang/String;

    move-object v1, v1

    .line 574973
    if-eqz v1, :cond_7

    .line 574974
    const-string v1, "depends_on"

    .line 574975
    iget-object v4, p1, LX/2Vj;->d:Ljava/lang/String;

    move-object v4, v4

    .line 574976
    invoke-static {v3, v1, v4}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 574977
    :cond_7
    iget-object v1, p1, LX/2Vj;->g:Ljava/lang/String;

    move-object v1, v1

    .line 574978
    if-eqz v1, :cond_f

    .line 574979
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 574980
    iget-object v1, p1, LX/2Vj;->g:Ljava/lang/String;

    move-object v1, v1

    .line 574981
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 574982
    :goto_2
    iget-object v0, p1, LX/2Vj;->e:Ljava/lang/String;

    move-object v0, v0

    .line 574983
    if-eqz v0, :cond_8

    .line 574984
    const-string v0, "continue_if_set"

    .line 574985
    iget-object v4, p1, LX/2Vj;->e:Ljava/lang/String;

    move-object v4, v4

    .line 574986
    invoke-static {v3, v0, v4}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 574987
    :cond_8
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 574988
    invoke-virtual {v2}, LX/14N;->m()Ljava/util/List;

    move-result-object v4

    if-eqz v4, :cond_e

    .line 574989
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 574990
    const-string v0, "attached_files"

    invoke-virtual {v3, v0}, LX/0n9;->b(Ljava/lang/String;)LX/0n9;

    move-result-object v5

    .line 574991
    invoke-virtual {v2}, LX/14N;->m()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4cQ;

    .line 574992
    iget-object v7, v0, LX/4cQ;->a:Ljava/lang/String;

    move-object v7, v7

    .line 574993
    invoke-virtual {v5, v7}, LX/0n9;->b(Ljava/lang/String;)LX/0n9;

    .line 574994
    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_3

    .line 574995
    :cond_9
    const-string v4, "POST"

    .line 574996
    iget-object v5, v2, LX/14N;->b:Ljava/lang/String;

    move-object v5, v5

    .line 574997
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 574998
    const-string v4, "body"

    invoke-virtual {v3, v4, v1}, LX/0n9;->a(Ljava/lang/String;LX/0nA;)V

    .line 574999
    invoke-static {}, LX/10F;->a()LX/10F;

    move-result-object v4

    invoke-virtual {v1, v4}, LX/0nA;->a(LX/0nD;)V

    .line 575000
    const-class v4, LX/0nC;

    const/4 v5, 0x1

    .line 575001
    iget-object v6, v1, LX/0nA;->a:LX/2ah;

    if-nez v6, :cond_a

    .line 575002
    new-instance v6, LX/2ah;

    const/4 v7, 0x1

    invoke-direct {v6, v7}, LX/2ah;-><init>(I)V

    iput-object v6, v1, LX/0nA;->a:LX/2ah;

    .line 575003
    :cond_a
    iget-object v6, v1, LX/0nA;->a:LX/2ah;

    .line 575004
    invoke-static {v6, v4}, LX/2ah;->a(LX/2ah;Ljava/lang/Object;)I

    move-result v7

    .line 575005
    if-ltz v7, :cond_11

    .line 575006
    iget-object v8, v6, LX/2ah;->b:[I

    aput v5, v8, v7

    .line 575007
    :goto_4
    goto/16 :goto_1

    .line 575008
    :cond_b
    const-string v4, "DELETE"

    .line 575009
    iget-object v5, v2, LX/14N;->b:Ljava/lang/String;

    move-object v5, v5

    .line 575010
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 575011
    invoke-static {v1}, LX/14d;->a(LX/0n9;)Ljava/lang/String;

    move-result-object v4

    .line 575012
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, "?"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 575013
    invoke-virtual {v1}, LX/0nA;->a()V

    goto/16 :goto_1

    .line 575014
    :cond_c
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Unsupported method: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 575015
    iget-object v3, v2, LX/14N;->b:Ljava/lang/String;

    move-object v2, v3

    .line 575016
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 575017
    :cond_d
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 575018
    :cond_e
    const-string v4, "omit_response_on_success"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    .line 575019
    invoke-static {v3, v4, v5}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 575020
    const-string v4, "relative_url"

    .line 575021
    invoke-static {v3, v4, v1}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 575022
    new-instance v1, LX/2Vr;

    invoke-direct {v1, p1, v2, v3, v0}, LX/2Vr;-><init>(LX/2Vj;LX/14N;LX/0n9;LX/0Px;)V

    return-object v1

    :cond_f
    move-object v1, v0

    goto/16 :goto_2

    :cond_10
    move-object v2, v0

    goto/16 :goto_0

    .line 575023
    :cond_11
    iget v7, v6, LX/2ah;->c:I

    add-int/lit8 v7, v7, 0x1

    const/4 v1, 0x0

    .line 575024
    iget-object v8, v6, LX/2ah;->a:[Ljava/lang/Object;

    array-length v8, v8

    .line 575025
    if-lt v8, v7, :cond_12

    .line 575026
    :goto_5
    iget-object v7, v6, LX/2ah;->a:[Ljava/lang/Object;

    iget v8, v6, LX/2ah;->c:I

    aput-object v4, v7, v8

    .line 575027
    iget-object v7, v6, LX/2ah;->b:[I

    iget v8, v6, LX/2ah;->c:I

    aput v5, v7, v8

    .line 575028
    iget v7, v6, LX/2ah;->c:I

    add-int/lit8 v7, v7, 0x1

    iput v7, v6, LX/2ah;->c:I

    goto :goto_4

    .line 575029
    :cond_12
    :goto_6
    if-ge v8, v7, :cond_13

    .line 575030
    mul-int/lit8 v8, v8, 0x2

    goto :goto_6

    .line 575031
    :cond_13
    new-array v9, v8, [Ljava/lang/Object;

    .line 575032
    iget-object v10, v6, LX/2ah;->a:[Ljava/lang/Object;

    iget p0, v6, LX/2ah;->c:I

    invoke-static {v10, v1, v9, v1, p0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 575033
    iput-object v9, v6, LX/2ah;->a:[Ljava/lang/Object;

    .line 575034
    new-array v8, v8, [I

    .line 575035
    iget-object v9, v6, LX/2ah;->b:[I

    iget v10, v6, LX/2ah;->c:I

    invoke-static {v9, v1, v8, v1, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 575036
    iput-object v8, v6, LX/2ah;->b:[I

    goto :goto_5
.end method

.method private static c(Ljava/util/List;)LX/14P;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/2Vr",
            "<**>;>;)",
            "LX/14P;"
        }
    .end annotation

    .prologue
    .line 574907
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :pswitch_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Vr;

    .line 574908
    iget-object v0, v0, LX/2Vr;->b:LX/14N;

    .line 574909
    iget-object v2, v0, LX/14N;->x:LX/14P;

    move-object v0, v2

    .line 574910
    sget-object v2, LX/2Vm;->b:[I

    invoke-virtual {v0}, LX/14P;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 574911
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown idempotency="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 574912
    :cond_0
    sget-object v0, LX/14P;->RETRY_SAFE:LX/14P;

    :pswitch_1
    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;LX/14U;)V
    .locals 20
    .param p3    # LX/14U;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 574864
    invoke-static/range {p1 .. p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 574865
    invoke-static/range {p2 .. p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 574866
    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->a(Lcom/facebook/common/callercontext/CallerContext;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v6

    .line 574867
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;->a:LX/18W;

    iget-object v2, v2, LX/18W;->s:LX/18X;

    invoke-virtual/range {p0 .. p0}, LX/2VJ;->b()LX/2VL;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/18X;->a(LX/2VL;)LX/18Z;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;->b:LX/18Z;

    .line 574868
    if-nez p3, :cond_7

    .line 574869
    new-instance v3, LX/14U;

    invoke-direct {v3}, LX/14U;-><init>()V

    .line 574870
    :goto_0
    sget-object v2, LX/18W;->C:LX/0Rf;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 574871
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;->a:LX/18W;

    iget-object v2, v2, LX/18W;->v:LX/11Q;

    invoke-virtual {v2, v3}, LX/11Q;->a(LX/14U;)V

    .line 574872
    :cond_0
    sget-object v2, LX/2Vm;->a:[I

    invoke-virtual {v3}, LX/14U;->b()LX/14V;

    move-result-object v4

    invoke-virtual {v4}, LX/14V;->ordinal()I

    move-result v4

    aget v2, v2, v4

    packed-switch v2, :pswitch_data_0

    .line 574873
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;->a:LX/18W;

    iget-object v2, v2, LX/18W;->b:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0s9;

    move-object v7, v2

    .line 574874
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/2VJ;->a()Ljava/util/List;

    move-result-object v10

    .line 574875
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 574876
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v13

    .line 574877
    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2Vj;

    .line 574878
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;->b(LX/2Vj;)LX/2Vr;

    move-result-object v2

    .line 574879
    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 574880
    if-eqz v2, :cond_1

    iget-object v8, v2, LX/2Vr;->b:LX/14N;

    if-eqz v8, :cond_1

    .line 574881
    iget-object v2, v2, LX/2Vr;->b:LX/14N;

    invoke-virtual {v2}, LX/14N;->g()LX/0zW;

    move-result-object v2

    .line 574882
    invoke-virtual {v2}, LX/0zW;->a()Lcom/facebook/http/interfaces/RequestPriority;

    move-result-object v2

    invoke-interface {v13, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 574883
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;->a:LX/18W;

    iget-object v2, v2, LX/18W;->c:LX/0s9;

    move-object v7, v2

    .line 574884
    goto :goto_1

    .line 574885
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;->a:LX/18W;

    iget-object v2, v2, LX/18W;->l:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_2

    .line 574886
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;->a:LX/18W;

    iget-object v2, v2, LX/18W;->d:LX/0s9;

    move-object v7, v2

    goto :goto_1

    .line 574887
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;->a:LX/18W;

    iget-object v2, v2, LX/18W;->b:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0s9;

    move-object v7, v2

    .line 574888
    goto :goto_1

    :cond_3
    move-object/from16 v2, p0

    move-object/from16 v5, p1

    .line 574889
    invoke-direct/range {v2 .. v7}, Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;->a(LX/14U;Ljava/util/List;Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;LX/0s9;)Lorg/apache/http/client/methods/HttpPost;

    move-result-object v19

    .line 574890
    :try_start_0
    new-instance v7, LX/2Vs;

    move-object/from16 v8, p0

    move-object v9, v3

    move-object v11, v4

    move-object v12, v6

    invoke-direct/range {v7 .. v12}, LX/2Vs;-><init>(Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;LX/14U;Ljava/util/List;Ljava/util/List;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 574891
    invoke-static {v3, v13}, Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;->a(LX/14U;Ljava/util/List;)Lcom/facebook/http/interfaces/RequestPriority;

    move-result-object v13

    .line 574892
    invoke-static {v4}, Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;->b(Ljava/util/List;)LX/14Q;

    move-result-object v14

    invoke-static {v4}, Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;->c(Ljava/util/List;)LX/14P;

    move-result-object v18

    move-object/from16 v11, p1

    move-object/from16 v12, v19

    move-object v15, v7

    move-object/from16 v16, v3

    move-object/from16 v17, v6

    invoke-static/range {v11 .. v18}, LX/14f;->a(Ljava/lang/String;Lorg/apache/http/client/methods/HttpUriRequest;Lcom/facebook/http/interfaces/RequestPriority;LX/14Q;Lorg/apache/http/client/ResponseHandler;LX/14U;Lcom/facebook/common/callercontext/CallerContext;LX/14P;)LX/15D;

    move-result-object v2

    .line 574893
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/http/protocol/MethodBatcherImpl$MethodBatchImpl;->a:LX/18W;

    iget-object v3, v3, LX/18W;->e:Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-virtual {v3, v2}, Lcom/facebook/http/common/FbHttpRequestProcessor;->a(LX/15D;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 574894
    invoke-static/range {v19 .. v19}, LX/14f;->a(Lorg/apache/http/client/methods/HttpUriRequest;)V

    .line 574895
    return-void

    .line 574896
    :catch_0
    move-exception v2

    .line 574897
    :try_start_1
    invoke-static {v2}, LX/2BF;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v5

    .line 574898
    const/4 v4, 0x0

    .line 574899
    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2Vj;

    .line 574900
    invoke-virtual {v2}, LX/2Vj;->a()LX/0e6;

    move-result-object v3

    .line 574901
    instance-of v7, v3, LX/0rp;

    if-eqz v7, :cond_6

    .line 574902
    check-cast v3, LX/0rp;

    invoke-virtual {v2}, LX/2Vj;->b()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v3, v2, v5}, LX/0rp;->a(Ljava/lang/Object;Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v2

    .line 574903
    if-nez v4, :cond_6

    if-eqz v2, :cond_6

    :goto_4
    move-object v4, v2

    .line 574904
    goto :goto_3

    .line 574905
    :cond_4
    if-nez v4, :cond_5

    move-object v4, v5

    :cond_5
    throw v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 574906
    :catchall_0
    move-exception v2

    invoke-static/range {v19 .. v19}, LX/14f;->a(Lorg/apache/http/client/methods/HttpUriRequest;)V

    throw v2

    :cond_6
    move-object v2, v4

    goto :goto_4

    :cond_7
    move-object/from16 v3, p3

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
