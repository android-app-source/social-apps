.class public Lcom/facebook/http/protocol/ApiErrorResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/io/Serializable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/http/protocol/ApiErrorResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mErrorCode:I

.field private final mErrorData:Ljava/lang/String;

.field private final mErrorDomain:LX/2Aa;

.field private final mErrorMessage:Ljava/lang/String;

.field public final mErrorSubCode:I

.field public final mErrorUserMessage:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final mErrorUserTitle:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final mIsTransient:Z

.field public final mJsonResponse:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 568709
    new-instance v0, LX/2CP;

    invoke-direct {v0}, LX/2CP;-><init>()V

    sput-object v0, Lcom/facebook/http/protocol/ApiErrorResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/2Aa;Ljava/lang/String;Z)V
    .locals 0
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 568698
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 568699
    iput p1, p0, Lcom/facebook/http/protocol/ApiErrorResult;->mErrorCode:I

    .line 568700
    iput p2, p0, Lcom/facebook/http/protocol/ApiErrorResult;->mErrorSubCode:I

    .line 568701
    iput-object p3, p0, Lcom/facebook/http/protocol/ApiErrorResult;->mErrorMessage:Ljava/lang/String;

    .line 568702
    iput-object p4, p0, Lcom/facebook/http/protocol/ApiErrorResult;->mErrorData:Ljava/lang/String;

    .line 568703
    iput-object p5, p0, Lcom/facebook/http/protocol/ApiErrorResult;->mErrorUserTitle:Ljava/lang/String;

    .line 568704
    iput-object p6, p0, Lcom/facebook/http/protocol/ApiErrorResult;->mErrorUserMessage:Ljava/lang/String;

    .line 568705
    iput-object p7, p0, Lcom/facebook/http/protocol/ApiErrorResult;->mErrorDomain:LX/2Aa;

    .line 568706
    iput-object p8, p0, Lcom/facebook/http/protocol/ApiErrorResult;->mJsonResponse:Ljava/lang/String;

    .line 568707
    iput-boolean p9, p0, Lcom/facebook/http/protocol/ApiErrorResult;->mIsTransient:Z

    .line 568708
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 568686
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 568687
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/http/protocol/ApiErrorResult;->mErrorCode:I

    .line 568688
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/http/protocol/ApiErrorResult;->mErrorSubCode:I

    .line 568689
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/http/protocol/ApiErrorResult;->mErrorMessage:Ljava/lang/String;

    .line 568690
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/http/protocol/ApiErrorResult;->mErrorData:Ljava/lang/String;

    .line 568691
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/http/protocol/ApiErrorResult;->mErrorUserTitle:Ljava/lang/String;

    .line 568692
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/http/protocol/ApiErrorResult;->mErrorUserMessage:Ljava/lang/String;

    .line 568693
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/http/protocol/ApiErrorResult;->mJsonResponse:Ljava/lang/String;

    .line 568694
    const-class v0, LX/2Aa;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/2Aa;

    iput-object v0, p0, Lcom/facebook/http/protocol/ApiErrorResult;->mErrorDomain:LX/2Aa;

    .line 568695
    invoke-virtual {p1}, Landroid/os/Parcel;->dataAvail()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/http/protocol/ApiErrorResult;->mIsTransient:Z

    .line 568696
    return-void

    .line 568697
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(ILjava/lang/String;)LX/2AV;
    .locals 1

    .prologue
    .line 568680
    new-instance v0, LX/2AV;

    invoke-direct {v0}, LX/2AV;-><init>()V

    .line 568681
    iput p0, v0, LX/2AV;->a:I

    .line 568682
    move-object v0, v0

    .line 568683
    iput-object p1, v0, LX/2AV;->c:Ljava/lang/String;

    .line 568684
    move-object v0, v0

    .line 568685
    return-object v0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 568679
    const-string v0, "^\\(\\#\\d+\\)\\s"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 568663
    iget v0, p0, Lcom/facebook/http/protocol/ApiErrorResult;->mErrorCode:I

    return v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 568678
    iget-object v0, p0, Lcom/facebook/http/protocol/ApiErrorResult;->mErrorMessage:Ljava/lang/String;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 568677
    iget-object v0, p0, Lcom/facebook/http/protocol/ApiErrorResult;->mErrorData:Ljava/lang/String;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 568676
    const/4 v0, 0x0

    return v0
.end method

.method public h()LX/2Aa;
    .locals 1

    .prologue
    .line 568675
    iget-object v0, p0, Lcom/facebook/http/protocol/ApiErrorResult;->mErrorDomain:LX/2Aa;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 568664
    iget v0, p0, Lcom/facebook/http/protocol/ApiErrorResult;->mErrorCode:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 568665
    iget v0, p0, Lcom/facebook/http/protocol/ApiErrorResult;->mErrorSubCode:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 568666
    iget-object v0, p0, Lcom/facebook/http/protocol/ApiErrorResult;->mErrorMessage:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 568667
    iget-object v0, p0, Lcom/facebook/http/protocol/ApiErrorResult;->mErrorData:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 568668
    iget-object v0, p0, Lcom/facebook/http/protocol/ApiErrorResult;->mErrorUserTitle:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 568669
    iget-object v0, p0, Lcom/facebook/http/protocol/ApiErrorResult;->mErrorUserMessage:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 568670
    iget-object v0, p0, Lcom/facebook/http/protocol/ApiErrorResult;->mJsonResponse:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 568671
    iget-object v0, p0, Lcom/facebook/http/protocol/ApiErrorResult;->mErrorDomain:LX/2Aa;

    invoke-virtual {v0}, LX/2Aa;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 568672
    iget-boolean v0, p0, Lcom/facebook/http/protocol/ApiErrorResult;->mIsTransient:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 568673
    return-void

    .line 568674
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
