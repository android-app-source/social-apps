.class public Lcom/facebook/http/protocol/BooleanApiResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/http/protocol/BooleanApiResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 795320
    new-instance v0, LX/4co;

    invoke-direct {v0}, LX/4co;-><init>()V

    sput-object v0, Lcom/facebook/http/protocol/BooleanApiResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 795321
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 795322
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/http/protocol/BooleanApiResult;->a:Z

    .line 795323
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 795324
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 795325
    instance-of v0, p1, Lcom/facebook/http/protocol/BooleanApiResult;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/http/protocol/BooleanApiResult;->a:Z

    check-cast p1, Lcom/facebook/http/protocol/BooleanApiResult;

    iget-boolean v1, p1, Lcom/facebook/http/protocol/BooleanApiResult;->a:Z

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 795326
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/facebook/http/protocol/BooleanApiResult;->a:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 795327
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "value"

    iget-boolean v2, p0, Lcom/facebook/http/protocol/BooleanApiResult;->a:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 795328
    iget-boolean v0, p0, Lcom/facebook/http/protocol/BooleanApiResult;->a:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 795329
    return-void
.end method
