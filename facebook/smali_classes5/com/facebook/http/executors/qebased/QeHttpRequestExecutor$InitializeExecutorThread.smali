.class public final Lcom/facebook/http/executors/qebased/QeHttpRequestExecutor$InitializeExecutorThread;
.super Ljava/lang/Thread;
.source ""


# instance fields
.field public final synthetic a:LX/4ca;


# direct methods
.method public constructor <init>(LX/4ca;)V
    .locals 0

    .prologue
    .line 795158
    iput-object p1, p0, Lcom/facebook/http/executors/qebased/QeHttpRequestExecutor$InitializeExecutorThread;->a:LX/4ca;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 795159
    iget-object v1, p0, Lcom/facebook/http/executors/qebased/QeHttpRequestExecutor$InitializeExecutorThread;->a:LX/4ca;

    monitor-enter v1

    .line 795160
    :try_start_0
    iget-object v0, p0, Lcom/facebook/http/executors/qebased/QeHttpRequestExecutor$InitializeExecutorThread;->a:LX/4ca;

    iget-object v0, v0, LX/4ca;->a:LX/14D;

    invoke-virtual {v0}, LX/14D;->a()LX/15e;

    move-result-object v0

    .line 795161
    sget-object v2, LX/15e;->LIGER:LX/15e;

    if-ne v0, v2, :cond_0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 795162
    :try_start_1
    iget-object v2, p0, Lcom/facebook/http/executors/qebased/QeHttpRequestExecutor$InitializeExecutorThread;->a:LX/4ca;

    iget-object v0, p0, Lcom/facebook/http/executors/qebased/QeHttpRequestExecutor$InitializeExecutorThread;->a:LX/4ca;

    iget-object v0, v0, LX/4ca;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1MQ;

    .line 795163
    iput-object v0, v2, LX/4ca;->e:LX/1MQ;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 795164
    :cond_0
    :goto_0
    :try_start_2
    iget-object v0, p0, Lcom/facebook/http/executors/qebased/QeHttpRequestExecutor$InitializeExecutorThread;->a:LX/4ca;

    iget-object v0, v0, LX/4ca;->e:LX/1MQ;

    if-nez v0, :cond_1

    .line 795165
    iget-object v2, p0, Lcom/facebook/http/executors/qebased/QeHttpRequestExecutor$InitializeExecutorThread;->a:LX/4ca;

    iget-object v0, p0, Lcom/facebook/http/executors/qebased/QeHttpRequestExecutor$InitializeExecutorThread;->a:LX/4ca;

    iget-object v0, v0, LX/4ca;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1MQ;

    .line 795166
    iput-object v0, v2, LX/4ca;->e:LX/1MQ;

    .line 795167
    :cond_1
    iget-object v0, p0, Lcom/facebook/http/executors/qebased/QeHttpRequestExecutor$InitializeExecutorThread;->a:LX/4ca;

    iget-object v0, v0, LX/4ca;->d:LX/03V;

    const-string v2, "HTTP_ENGINE"

    iget-object v3, p0, Lcom/facebook/http/executors/qebased/QeHttpRequestExecutor$InitializeExecutorThread;->a:LX/4ca;

    iget-object v3, v3, LX/4ca;->e:LX/1MQ;

    invoke-interface {v3}, LX/1MQ;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/03V;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 795168
    iget-object v0, p0, Lcom/facebook/http/executors/qebased/QeHttpRequestExecutor$InitializeExecutorThread;->a:LX/4ca;

    const v2, 0x1e8e0310

    invoke-static {v0, v2}, LX/02L;->c(Ljava/lang/Object;I)V

    .line 795169
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 795170
    :catch_0
    move-exception v0

    .line 795171
    iget-object v2, p0, Lcom/facebook/http/executors/qebased/QeHttpRequestExecutor$InitializeExecutorThread;->a:LX/4ca;

    iget-object v2, v2, LX/4ca;->d:LX/03V;

    const-string v3, "liger_init"

    invoke-virtual {v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
