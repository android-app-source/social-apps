.class public Lcom/facebook/http/onion/OnionRewriteRule;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/http/onion/OnionRewriteRuleDeserializer;
.end annotation


# instance fields
.field private a:Ljava/util/regex/Pattern;

.field public format:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "format"
    .end annotation
.end field

.field public matcher:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "matcher"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 795224
    const-class v0, Lcom/facebook/http/onion/OnionRewriteRuleDeserializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 795223
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private declared-synchronized a()Ljava/util/regex/Pattern;
    .locals 1

    .prologue
    .line 795225
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/http/onion/OnionRewriteRule;->a:Ljava/util/regex/Pattern;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/http/onion/OnionRewriteRule;->matcher:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 795226
    iget-object v0, p0, Lcom/facebook/http/onion/OnionRewriteRule;->matcher:Ljava/lang/String;

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/http/onion/OnionRewriteRule;->a:Ljava/util/regex/Pattern;

    .line 795227
    :cond_0
    iget-object v0, p0, Lcom/facebook/http/onion/OnionRewriteRule;->a:Ljava/util/regex/Pattern;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 795228
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 795210
    invoke-direct {p0}, Lcom/facebook/http/onion/OnionRewriteRule;->a()Ljava/util/regex/Pattern;

    move-result-object v0

    .line 795211
    if-nez v0, :cond_1

    .line 795212
    :cond_0
    :goto_0
    return-object p1

    .line 795213
    :cond_1
    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    .line 795214
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 795215
    iget-object v0, p0, Lcom/facebook/http/onion/OnionRewriteRule;->format:Ljava/lang/String;

    move-object v2, v0

    move v0, v1

    .line 795216
    :goto_1
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->groupCount()I

    move-result v3

    if-gt v0, v3, :cond_3

    .line 795217
    invoke-virtual {v4, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    .line 795218
    if-nez v3, :cond_2

    .line 795219
    const-string v3, ""

    .line 795220
    :cond_2
    sget-object v5, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v6, "$%d"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v1

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 795221
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    move-object p1, v2

    .line 795222
    goto :goto_0
.end method
