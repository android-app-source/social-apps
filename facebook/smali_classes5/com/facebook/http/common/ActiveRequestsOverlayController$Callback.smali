.class public final Lcom/facebook/http/common/ActiveRequestsOverlayController$Callback;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/2Ec;


# direct methods
.method public constructor <init>(LX/2Ec;)V
    .locals 0

    .prologue
    .line 570952
    iput-object p1, p0, Lcom/facebook/http/common/ActiveRequestsOverlayController$Callback;->a:LX/2Ec;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    .line 570953
    iget-object v0, p0, Lcom/facebook/http/common/ActiveRequestsOverlayController$Callback;->a:LX/2Ec;

    iget-object v0, v0, LX/2Ec;->d:Landroid/os/Handler;

    invoke-static {v0, p0}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 570954
    iget-object v0, p0, Lcom/facebook/http/common/ActiveRequestsOverlayController$Callback;->a:LX/2Ec;

    invoke-virtual {v0}, LX/2Ec;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 570955
    iget-object v0, p0, Lcom/facebook/http/common/ActiveRequestsOverlayController$Callback;->a:LX/2Ec;

    const/4 v2, 0x0

    .line 570956
    invoke-static {v0}, LX/2Ec;->f(LX/2Ec;)V

    .line 570957
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 570958
    iget-object v1, v0, LX/2Ec;->e:Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-virtual {v1}, Lcom/facebook/http/common/FbHttpRequestProcessor;->a()LX/2Ee;

    move-result-object v5

    .line 570959
    const-string v1, "Inflight: \n"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 570960
    iget-object v1, v5, LX/2Ee;->a:Ljava/util/ArrayList;

    move-object v6, v1

    .line 570961
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v7

    move v3, v2

    :goto_0
    if-ge v3, v7, :cond_0

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/15D;

    .line 570962
    invoke-virtual {v1, v4}, LX/15D;->a(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    .line 570963
    const-string v1, "\n"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 570964
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0

    .line 570965
    :cond_0
    const-string v1, "\nQueued: \n"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 570966
    iget-object v1, v5, LX/2Ee;->b:Ljava/util/ArrayList;

    move-object v3, v1

    .line 570967
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v5

    :goto_1
    if-ge v2, v5, :cond_1

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/15D;

    .line 570968
    invoke-virtual {v1, v4}, LX/15D;->a(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    .line 570969
    const-string v1, "\n"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 570970
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 570971
    :cond_1
    iget-object v1, v0, LX/2Ec;->i:Landroid/widget/TextView;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 570972
    iget-object v0, p0, Lcom/facebook/http/common/ActiveRequestsOverlayController$Callback;->a:LX/2Ec;

    invoke-virtual {v0}, LX/2Ec;->d()V

    .line 570973
    :cond_2
    return-void
.end method
