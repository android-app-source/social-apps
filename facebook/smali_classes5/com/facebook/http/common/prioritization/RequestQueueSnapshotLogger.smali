.class public Lcom/facebook/http/common/prioritization/RequestQueueSnapshotLogger;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:Lcom/facebook/http/common/prioritization/RequestQueueSnapshotLogger;


# instance fields
.field public final a:LX/0Zb;

.field public final b:LX/0oz;


# direct methods
.method public constructor <init>(LX/0Zb;LX/0oz;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 794707
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 794708
    iput-object p1, p0, Lcom/facebook/http/common/prioritization/RequestQueueSnapshotLogger;->a:LX/0Zb;

    .line 794709
    iput-object p2, p0, Lcom/facebook/http/common/prioritization/RequestQueueSnapshotLogger;->b:LX/0oz;

    .line 794710
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/http/common/prioritization/RequestQueueSnapshotLogger;
    .locals 5

    .prologue
    .line 794694
    sget-object v0, Lcom/facebook/http/common/prioritization/RequestQueueSnapshotLogger;->c:Lcom/facebook/http/common/prioritization/RequestQueueSnapshotLogger;

    if-nez v0, :cond_1

    .line 794695
    const-class v1, Lcom/facebook/http/common/prioritization/RequestQueueSnapshotLogger;

    monitor-enter v1

    .line 794696
    :try_start_0
    sget-object v0, Lcom/facebook/http/common/prioritization/RequestQueueSnapshotLogger;->c:Lcom/facebook/http/common/prioritization/RequestQueueSnapshotLogger;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 794697
    if-eqz v2, :cond_0

    .line 794698
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 794699
    new-instance p0, Lcom/facebook/http/common/prioritization/RequestQueueSnapshotLogger;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/0oz;->a(LX/0QB;)LX/0oz;

    move-result-object v4

    check-cast v4, LX/0oz;

    invoke-direct {p0, v3, v4}, Lcom/facebook/http/common/prioritization/RequestQueueSnapshotLogger;-><init>(LX/0Zb;LX/0oz;)V

    .line 794700
    move-object v0, p0

    .line 794701
    sput-object v0, Lcom/facebook/http/common/prioritization/RequestQueueSnapshotLogger;->c:Lcom/facebook/http/common/prioritization/RequestQueueSnapshotLogger;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 794702
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 794703
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 794704
    :cond_1
    sget-object v0, Lcom/facebook/http/common/prioritization/RequestQueueSnapshotLogger;->c:Lcom/facebook/http/common/prioritization/RequestQueueSnapshotLogger;

    return-object v0

    .line 794705
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 794706
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/0oG;LX/15D;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0oG;",
            "LX/15D",
            "<*>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 794677
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_friendlyname"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 794678
    iget-object v1, p1, LX/15D;->c:Ljava/lang/String;

    move-object v1, v1

    .line 794679
    invoke-virtual {p0, v0, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 794680
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_priority"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, LX/15D;->h()Lcom/facebook/http/interfaces/RequestPriority;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;

    .line 794681
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_calling_class"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Lcom/facebook/http/common/FbHttpUtils;->b(LX/15D;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 794682
    iget-object v0, p1, LX/15D;->d:Lcom/facebook/common/callercontext/CallerContext;

    move-object v0, v0

    .line 794683
    if-eqz v0, :cond_0

    .line 794684
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_feature_tag"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 794685
    iget-object v1, p1, LX/15D;->d:Lcom/facebook/common/callercontext/CallerContext;

    move-object v1, v1

    .line 794686
    invoke-virtual {v1}, Lcom/facebook/common/callercontext/CallerContext;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 794687
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_analytics_tag"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 794688
    iget-object v1, p1, LX/15D;->d:Lcom/facebook/common/callercontext/CallerContext;

    move-object v1, v1

    .line 794689
    invoke-virtual {v1}, Lcom/facebook/common/callercontext/CallerContext;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 794690
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_module_tag"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 794691
    iget-object v1, p1, LX/15D;->d:Lcom/facebook/common/callercontext/CallerContext;

    move-object v1, v1

    .line 794692
    invoke-virtual {v1}, Lcom/facebook/common/callercontext/CallerContext;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 794693
    :cond_0
    return-void
.end method

.method public static a(LX/0oG;Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0oG;",
            "Ljava/util/ArrayList",
            "<",
            "LX/15D",
            "<*>;>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 794671
    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 794672
    :goto_0
    return-void

    .line 794673
    :cond_0
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    .line 794674
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    .line 794675
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/15D;

    .line 794676
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_sample"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/facebook/http/common/prioritization/RequestQueueSnapshotLogger;->a(LX/0oG;LX/15D;Ljava/lang/String;)V

    goto :goto_0
.end method
