.class public final Lcom/facebook/http/common/PriorityRequestEngine$ExecuteRequestsRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/4bU;


# direct methods
.method public constructor <init>(LX/4bU;)V
    .locals 0

    .prologue
    .line 793732
    iput-object p1, p0, Lcom/facebook/http/common/PriorityRequestEngine$ExecuteRequestsRunnable;->a:LX/4bU;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 14

    .prologue
    .line 793733
    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/facebook/http/common/PriorityRequestEngine$ExecuteRequestsRunnable;->a:LX/4bU;

    iget-object v0, v0, LX/4bU;->c:LX/4bW;

    invoke-virtual {v0}, LX/4bW;->b()LX/4bV;

    move-result-object v0

    .line 793734
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 793735
    :try_start_1
    iget-object v3, p0, Lcom/facebook/http/common/PriorityRequestEngine$ExecuteRequestsRunnable;->a:LX/4bU;

    iget-object v3, v3, LX/4bU;->i:Lcom/facebook/http/common/FbHttpRequestProcessorLogger;

    iget-object v4, v0, LX/4bV;->c:LX/15D;

    .line 793736
    invoke-static {v3, v4}, Lcom/facebook/http/common/FbHttpRequestProcessorLogger;->b(Lcom/facebook/http/common/FbHttpRequestProcessorLogger;LX/15D;)LX/0oG;

    move-result-object v6

    .line 793737
    if-nez v6, :cond_0

    .line 793738
    :goto_1
    iget-object v3, p0, Lcom/facebook/http/common/PriorityRequestEngine$ExecuteRequestsRunnable;->a:LX/4bU;

    iget-object v3, v3, LX/4bU;->j:LX/4b8;

    iget-object v4, v0, LX/4bV;->c:LX/15D;

    .line 793739
    sget-object v5, LX/4b7;->START:LX/4b7;

    invoke-static {v3, v5, v4}, LX/4b8;->a(LX/4b8;LX/4b7;LX/15D;)V

    .line 793740
    iget-object v3, v0, LX/4bV;->a:Lcom/google/common/util/concurrent/SettableFuture;

    iget-object v4, p0, Lcom/facebook/http/common/PriorityRequestEngine$ExecuteRequestsRunnable;->a:LX/4bU;

    iget-object v4, v4, LX/4bU;->d:LX/1hl;

    iget-object v5, v0, LX/4bV;->c:LX/15D;

    invoke-virtual {v4, v5}, LX/1hl;->a(LX/15D;)Ljava/lang/Object;

    move-result-object v4

    const v5, -0x639af27f

    invoke-static {v3, v4, v5}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 793741
    :goto_2
    :try_start_2
    iget-object v1, p0, Lcom/facebook/http/common/PriorityRequestEngine$ExecuteRequestsRunnable;->a:LX/4bU;

    iget-object v1, v1, LX/4bU;->c:LX/4bW;

    invoke-virtual {v1, v0}, LX/4bW;->b(LX/4bV;)V

    goto :goto_0

    .line 793742
    :catch_0
    goto :goto_0

    .line 793743
    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/facebook/http/common/PriorityRequestEngine$ExecuteRequestsRunnable;->a:LX/4bU;

    iget-object v2, v2, LX/4bU;->c:LX/4bW;

    invoke-virtual {v2, v0}, LX/4bW;->b(LX/4bV;)V

    throw v1
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    .line 793744
    :catch_1
    :try_start_3
    move-exception v3

    .line 793745
    iget-object v4, v0, LX/4bV;->a:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v4, v3}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    goto :goto_2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 793746
    :cond_0
    const-string v7, "queue_time"

    invoke-static {}, Lcom/facebook/http/common/FbHttpRequestProcessorLogger;->a()J

    move-result-wide v8

    .line 793747
    iget-wide v12, v4, LX/15D;->n:J

    move-wide v10, v12

    .line 793748
    sub-long/2addr v8, v10

    invoke-virtual {v6, v7, v8, v9}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 793749
    const-string v7, "thread_priority"

    invoke-static {}, Landroid/os/Process;->myTid()I

    move-result v8

    invoke-static {v8}, Landroid/os/Process;->getThreadPriority(I)I

    move-result v8

    invoke-virtual {v6, v7, v8}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    goto :goto_1
.end method
