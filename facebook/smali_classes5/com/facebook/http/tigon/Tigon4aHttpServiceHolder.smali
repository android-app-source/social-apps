.class public Lcom/facebook/http/tigon/Tigon4aHttpServiceHolder;
.super Lcom/facebook/tigon/iface/TigonServiceHolder;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 573761
    const-string v0, "tigon4a"

    invoke-static {v0}, LX/01L;->a(Ljava/lang/String;)V

    .line 573762
    return-void
.end method

.method public constructor <init>(Lcom/facebook/http/tigon/Tigon4aHttpService;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 573763
    invoke-static {p1}, Lcom/facebook/http/tigon/Tigon4aHttpServiceHolder;->initHybrid(Lcom/facebook/http/tigon/Tigon4aHttpService;)Lcom/facebook/jni/HybridData;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/tigon/iface/TigonServiceHolder;-><init>(Lcom/facebook/jni/HybridData;)V

    .line 573764
    return-void
.end method

.method public static b(LX/0QB;)Lcom/facebook/http/tigon/Tigon4aHttpServiceHolder;
    .locals 2

    .prologue
    .line 573765
    new-instance v1, Lcom/facebook/http/tigon/Tigon4aHttpServiceHolder;

    invoke-static {p0}, Lcom/facebook/http/tigon/Tigon4aHttpService;->a(LX/0QB;)Lcom/facebook/http/tigon/Tigon4aHttpService;

    move-result-object v0

    check-cast v0, Lcom/facebook/http/tigon/Tigon4aHttpService;

    invoke-direct {v1, v0}, Lcom/facebook/http/tigon/Tigon4aHttpServiceHolder;-><init>(Lcom/facebook/http/tigon/Tigon4aHttpService;)V

    .line 573766
    return-object v1
.end method

.method private static native initHybrid(Lcom/facebook/http/tigon/Tigon4aHttpService;)Lcom/facebook/jni/HybridData;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end method
