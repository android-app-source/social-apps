.class public Lcom/facebook/http/tigon/Tigon4aRequestToken;
.super Lcom/facebook/tigon/javaservice/AbstractRequestToken;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field public b:LX/1j2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1j2",
            "<",
            "LX/2aS;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 573800
    const-class v0, Lcom/facebook/http/tigon/Tigon4aRequestToken;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/http/tigon/Tigon4aRequestToken;->a:Ljava/lang/String;

    .line 573801
    const-string v0, "tigon4a"

    invoke-static {v0}, LX/01L;->a(Ljava/lang/String;)V

    .line 573802
    return-void
.end method

.method private constructor <init>(Lcom/facebook/jni/HybridData;)V
    .locals 0

    .prologue
    .line 573798
    invoke-direct {p0, p1}, Lcom/facebook/tigon/javaservice/AbstractRequestToken;-><init>(Lcom/facebook/jni/HybridData;)V

    .line 573799
    return-void
.end method

.method private static a(Lcom/facebook/proxygen/HTTPRequestError$ProxygenError;)I
    .locals 3

    .prologue
    const/4 v0, 0x3

    .line 573794
    sget-object v1, LX/5Qn;->a:[I

    invoke-virtual {p0}, Lcom/facebook/proxygen/HTTPRequestError$ProxygenError;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 573795
    :goto_0
    :pswitch_0
    return v0

    .line 573796
    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    .line 573797
    :pswitch_2
    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private static c(Ljava/lang/Throwable;)I
    .locals 3

    .prologue
    const/4 v0, 0x2

    .line 573785
    instance-of v1, p0, Ljava/net/SocketTimeoutException;

    if-nez v1, :cond_0

    instance-of v1, p0, Lorg/apache/http/conn/ConnectTimeoutException;

    if-eqz v1, :cond_1

    .line 573786
    :cond_0
    :goto_0
    return v0

    .line 573787
    :cond_1
    instance-of v1, p0, Ljava/net/UnknownHostException;

    if-nez v1, :cond_0

    .line 573788
    instance-of v1, p0, Lorg/apache/http/conn/HttpHostConnectException;

    if-nez v1, :cond_0

    instance-of v1, p0, Ljava/net/ConnectException;

    if-nez v1, :cond_0

    .line 573789
    instance-of v1, p0, Lorg/apache/http/NoHttpResponseException;

    if-nez v1, :cond_0

    .line 573790
    instance-of v1, p0, LX/4bK;

    if-nez v1, :cond_0

    .line 573791
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-class v2, Ljavax/net/ssl/SSLException;

    if-eq v1, v2, :cond_0

    .line 573792
    instance-of v1, p0, LX/4bK;

    if-nez v1, :cond_0

    .line 573793
    const/4 v0, 0x3

    goto :goto_0
.end method

.method private native onError(ILjava/lang/String;ILjava/lang/String;)V
.end method

.method private native onWillRetry(ILjava/lang/String;ILjava/lang/String;)V
.end method


# virtual methods
.method public final a(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 573803
    sget-object v0, Lcom/facebook/http/tigon/Tigon4aRequestToken;->a:Ljava/lang/String;

    const-string v1, "Tigon4aRequestToken.onError"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, p1, v1, v2}, LX/01m;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 573804
    instance-of v0, p1, LX/5oc;

    if-eqz v0, :cond_0

    .line 573805
    check-cast p1, LX/5oc;

    .line 573806
    iget-object v0, p1, LX/5oc;->mError:Lcom/facebook/proxygen/HTTPRequestError;

    move-object v0, v0

    .line 573807
    iget-object v1, v0, Lcom/facebook/proxygen/HTTPRequestError;->mErrCode:Lcom/facebook/proxygen/HTTPRequestError$ProxygenError;

    move-object v1, v1

    .line 573808
    invoke-static {v1}, Lcom/facebook/http/tigon/Tigon4aRequestToken;->a(Lcom/facebook/proxygen/HTTPRequestError$ProxygenError;)I

    move-result v1

    .line 573809
    const-string v2, "LigerErrorDomain"

    .line 573810
    iget-object v3, v0, Lcom/facebook/proxygen/HTTPRequestError;->mErrCode:Lcom/facebook/proxygen/HTTPRequestError$ProxygenError;

    move-object v3, v3

    .line 573811
    invoke-virtual {v3}, Lcom/facebook/proxygen/HTTPRequestError$ProxygenError;->ordinal()I

    move-result v3

    .line 573812
    iget-object p1, v0, Lcom/facebook/proxygen/HTTPRequestError;->mErrMsg:Ljava/lang/String;

    move-object v0, p1

    .line 573813
    invoke-direct {p0, v1, v2, v3, v0}, Lcom/facebook/http/tigon/Tigon4aRequestToken;->onError(ILjava/lang/String;ILjava/lang/String;)V

    .line 573814
    :goto_0
    return-void

    .line 573815
    :cond_0
    invoke-static {p1}, Lcom/facebook/http/tigon/Tigon4aRequestToken;->c(Ljava/lang/Throwable;)I

    move-result v0

    .line 573816
    const-string v1, "Tigon4aErrorDomain"

    invoke-virtual {p1}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v1, v3, v2}, Lcom/facebook/http/tigon/Tigon4aRequestToken;->onError(ILjava/lang/String;ILjava/lang/String;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 573771
    sget-object v0, Lcom/facebook/http/tigon/Tigon4aRequestToken;->a:Ljava/lang/String;

    const-string v1, "Tigon4aRequestToken.onWillRetry"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, p1, v1, v2}, LX/01m;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 573772
    instance-of v0, p1, LX/5oc;

    if-eqz v0, :cond_0

    .line 573773
    check-cast p1, LX/5oc;

    .line 573774
    iget-object v0, p1, LX/5oc;->mError:Lcom/facebook/proxygen/HTTPRequestError;

    move-object v0, v0

    .line 573775
    iget-object v1, v0, Lcom/facebook/proxygen/HTTPRequestError;->mErrCode:Lcom/facebook/proxygen/HTTPRequestError$ProxygenError;

    move-object v1, v1

    .line 573776
    invoke-static {v1}, Lcom/facebook/http/tigon/Tigon4aRequestToken;->a(Lcom/facebook/proxygen/HTTPRequestError$ProxygenError;)I

    move-result v1

    .line 573777
    const-string v2, "LigerErrorDomain"

    .line 573778
    iget-object v3, v0, Lcom/facebook/proxygen/HTTPRequestError;->mErrCode:Lcom/facebook/proxygen/HTTPRequestError$ProxygenError;

    move-object v3, v3

    .line 573779
    invoke-virtual {v3}, Lcom/facebook/proxygen/HTTPRequestError$ProxygenError;->ordinal()I

    move-result v3

    .line 573780
    iget-object p1, v0, Lcom/facebook/proxygen/HTTPRequestError;->mErrMsg:Ljava/lang/String;

    move-object v0, p1

    .line 573781
    invoke-direct {p0, v1, v2, v3, v0}, Lcom/facebook/http/tigon/Tigon4aRequestToken;->onWillRetry(ILjava/lang/String;ILjava/lang/String;)V

    .line 573782
    :goto_0
    return-void

    .line 573783
    :cond_0
    invoke-static {p1}, Lcom/facebook/http/tigon/Tigon4aRequestToken;->c(Ljava/lang/Throwable;)I

    move-result v0

    .line 573784
    const-string v1, "Tigon4aErrorDomain"

    invoke-virtual {p1}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v1, v3, v2}, Lcom/facebook/http/tigon/Tigon4aRequestToken;->onWillRetry(ILjava/lang/String;ILjava/lang/String;)V

    goto :goto_0
.end method

.method public cancel()V
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 573769
    iget-object v0, p0, Lcom/facebook/http/tigon/Tigon4aRequestToken;->b:LX/1j2;

    invoke-virtual {v0}, LX/1j2;->b()V

    .line 573770
    return-void
.end method

.method public changePriority(I)V
    .locals 2
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 573767
    iget-object v0, p0, Lcom/facebook/http/tigon/Tigon4aRequestToken;->b:LX/1j2;

    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->DEFAULT_PRIORITY:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-static {p1, v1}, Lcom/facebook/http/interfaces/RequestPriority;->fromNumericValue(ILcom/facebook/http/interfaces/RequestPriority;)Lcom/facebook/http/interfaces/RequestPriority;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1j2;->a(Lcom/facebook/http/interfaces/RequestPriority;)V

    .line 573768
    return-void
.end method

.method public native onBody([BI)V
.end method

.method public native onEOM()V
.end method

.method public native onResponse(I[Ljava/lang/String;)V
.end method
