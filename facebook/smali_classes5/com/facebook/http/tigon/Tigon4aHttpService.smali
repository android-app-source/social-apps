.class public Lcom/facebook/http/tigon/Tigon4aHttpService;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements Lcom/facebook/tigon/javaservice/JavaBackedTigonService;


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static volatile c:Lcom/facebook/http/tigon/Tigon4aHttpService;


# instance fields
.field private final b:Lcom/facebook/http/common/FbHttpRequestProcessor;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 573820
    const-class v0, Lcom/facebook/http/tigon/Tigon4aHttpService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/http/tigon/Tigon4aHttpService;->a:Ljava/lang/String;

    .line 573821
    const-string v0, "tigon4a"

    invoke-static {v0}, LX/01L;->a(Ljava/lang/String;)V

    .line 573822
    return-void
.end method

.method public constructor <init>(Lcom/facebook/http/common/FbHttpRequestProcessor;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 573910
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 573911
    iput-object p1, p0, Lcom/facebook/http/tigon/Tigon4aHttpService;->b:Lcom/facebook/http/common/FbHttpRequestProcessor;

    .line 573912
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/http/tigon/Tigon4aHttpService;
    .locals 3

    .prologue
    .line 573900
    sget-object v0, Lcom/facebook/http/tigon/Tigon4aHttpService;->c:Lcom/facebook/http/tigon/Tigon4aHttpService;

    if-nez v0, :cond_1

    .line 573901
    const-class v1, Lcom/facebook/http/tigon/Tigon4aHttpService;

    monitor-enter v1

    .line 573902
    :try_start_0
    sget-object v0, Lcom/facebook/http/tigon/Tigon4aHttpService;->c:Lcom/facebook/http/tigon/Tigon4aHttpService;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 573903
    if-eqz v2, :cond_0

    .line 573904
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, Lcom/facebook/http/tigon/Tigon4aHttpService;->b(LX/0QB;)Lcom/facebook/http/tigon/Tigon4aHttpService;

    move-result-object v0

    sput-object v0, Lcom/facebook/http/tigon/Tigon4aHttpService;->c:Lcom/facebook/http/tigon/Tigon4aHttpService;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 573905
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 573906
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 573907
    :cond_1
    sget-object v0, Lcom/facebook/http/tigon/Tigon4aHttpService;->c:Lcom/facebook/http/tigon/Tigon4aHttpService;

    return-object v0

    .line 573908
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 573909
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Lcom/facebook/tigon/iface/TigonRequest;Lorg/apache/http/HttpEntity;)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 3
    .param p1    # Lorg/apache/http/HttpEntity;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 573890
    const-string v0, "GET"

    invoke-interface {p0}, Lcom/facebook/tigon/iface/TigonRequest;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 573891
    new-instance v0, Lorg/apache/http/client/methods/HttpGet;

    invoke-interface {p0}, Lcom/facebook/tigon/iface/TigonRequest;->b()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 573892
    invoke-interface {v0}, Lorg/apache/http/client/methods/HttpUriRequest;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lorg/apache/http/client/params/HttpClientParams;->setRedirecting(Lorg/apache/http/params/HttpParams;Z)V

    .line 573893
    :cond_0
    :goto_0
    invoke-interface {p0}, Lcom/facebook/tigon/iface/TigonRequest;->c()Ljava/util/Map;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/http/tigon/Tigon4aHttpService;->a(Ljava/util/Map;)[Lorg/apache/http/Header;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/apache/http/client/methods/HttpUriRequest;->setHeaders([Lorg/apache/http/Header;)V

    .line 573894
    return-object v0

    .line 573895
    :cond_1
    const-string v0, "POST"

    invoke-interface {p0}, Lcom/facebook/tigon/iface/TigonRequest;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 573896
    new-instance v0, Lorg/apache/http/client/methods/HttpPost;

    invoke-interface {p0}, Lcom/facebook/tigon/iface/TigonRequest;->b()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 573897
    if-eqz p1, :cond_0

    .line 573898
    invoke-virtual {v0, p1}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    goto :goto_0

    .line 573899
    :cond_2
    new-instance v0, Ljava/security/InvalidParameterException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported HTTP method "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p0}, Lcom/facebook/tigon/iface/TigonRequest;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static a(Ljava/util/ArrayList;)[Lorg/apache/http/Header;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)[",
            "Lorg/apache/http/Header;"
        }
    .end annotation

    .prologue
    .line 573880
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 573881
    and-int/lit8 v0, v3, 0x1

    if-eqz v0, :cond_0

    .line 573882
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Received odd number of strings; keys and vals unmatched"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 573883
    :cond_0
    div-int/lit8 v0, v3, 0x2

    invoke-static {v0}, LX/0R9;->a(I)Ljava/util/ArrayList;

    move-result-object v4

    .line 573884
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_2

    .line 573885
    invoke-virtual {p0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 573886
    const-string v1, "Content-Length"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 573887
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    add-int/lit8 v1, v2, 0x1

    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-direct {v5, v0, v1}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 573888
    :cond_1
    add-int/lit8 v0, v2, 0x2

    move v2, v0

    goto :goto_0

    .line 573889
    :cond_2
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lorg/apache/http/Header;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/apache/http/Header;

    return-object v0
.end method

.method private static a(Ljava/util/Map;)[Lorg/apache/http/Header;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)[",
            "Lorg/apache/http/Header;"
        }
    .end annotation

    .prologue
    .line 573875
    invoke-interface {p0}, Ljava/util/Map;->size()I

    move-result v0

    invoke-static {v0}, LX/0R9;->a(I)Ljava/util/ArrayList;

    move-result-object v1

    .line 573876
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 573877
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 573878
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 573879
    :cond_0
    invoke-static {v1}, Lcom/facebook/http/tigon/Tigon4aHttpService;->a(Ljava/util/ArrayList;)[Lorg/apache/http/Header;

    move-result-object v0

    return-object v0
.end method

.method private static b(LX/0QB;)Lcom/facebook/http/tigon/Tigon4aHttpService;
    .locals 2

    .prologue
    .line 573873
    new-instance v1, Lcom/facebook/http/tigon/Tigon4aHttpService;

    invoke-static {p0}, Lcom/facebook/http/common/FbHttpRequestProcessor;->a(LX/0QB;)Lcom/facebook/http/common/FbHttpRequestProcessor;

    move-result-object v0

    check-cast v0, Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-direct {v1, v0}, Lcom/facebook/http/tigon/Tigon4aHttpService;-><init>(Lcom/facebook/http/common/FbHttpRequestProcessor;)V

    .line 573874
    return-object v1
.end method


# virtual methods
.method public a(Lcom/facebook/tigon/iface/TigonRequest;Lorg/apache/http/HttpEntity;LX/2Ys;)LX/1j2;
    .locals 6
    .param p2    # Lorg/apache/http/HttpEntity;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/tigon/iface/TigonRequest;",
            "Lorg/apache/http/HttpEntity;",
            "LX/2Ys;",
            ")",
            "LX/1j2",
            "<",
            "LX/2aS;",
            ">;"
        }
    .end annotation

    .prologue
    .line 573837
    const/4 v2, 0x0

    .line 573838
    const-string v1, "unspecified tigon"

    .line 573839
    sget-object v0, LX/1iP;->b:LX/1he;

    invoke-interface {p1, v0}, Lcom/facebook/tigon/iface/TigonRequest;->a(LX/1he;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/tigon/iface/FacebookLoggingRequestInfo;

    .line 573840
    if-eqz v0, :cond_1

    .line 573841
    invoke-interface {v0}, Lcom/facebook/tigon/iface/FacebookLoggingRequestInfo;->logName()Ljava/lang/String;

    move-result-object v1

    .line 573842
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-interface {v0}, Lcom/facebook/tigon/iface/FacebookLoggingRequestInfo;->logNamespace()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    .line 573843
    :goto_0
    sget-object v2, LX/14Q;->FALLBACK_NOT_REQUIRED:LX/14Q;

    .line 573844
    const-string v3, "mobile_config_request:"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 573845
    sget-object v2, LX/14Q;->FALLBACK_REQUIRED:LX/14Q;

    .line 573846
    const/4 v3, 0x1

    .line 573847
    iput-boolean v3, p3, LX/2Ys;->c:Z

    .line 573848
    :cond_0
    invoke-static {}, LX/15D;->newBuilder()LX/15E;

    move-result-object v3

    invoke-static {p1, p2}, Lcom/facebook/http/tigon/Tigon4aHttpService;->a(Lcom/facebook/tigon/iface/TigonRequest;Lorg/apache/http/HttpEntity;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v4

    .line 573849
    iput-object v4, v3, LX/15E;->b:Lorg/apache/http/client/methods/HttpUriRequest;

    .line 573850
    move-object v3, v3

    .line 573851
    iput-object v0, v3, LX/15E;->c:Ljava/lang/String;

    .line 573852
    move-object v0, v3

    .line 573853
    iput-object v1, v0, LX/15E;->d:Lcom/facebook/common/callercontext/CallerContext;

    .line 573854
    move-object v0, v0

    .line 573855
    const-string v1, "Tigon"

    .line 573856
    iput-object v1, v0, LX/15E;->e:Ljava/lang/String;

    .line 573857
    move-object v0, v0

    .line 573858
    iput-object p3, v0, LX/15E;->g:Lorg/apache/http/client/ResponseHandler;

    .line 573859
    move-object v0, v0

    .line 573860
    iput-object v2, v0, LX/15E;->f:LX/14Q;

    .line 573861
    move-object v0, v0

    .line 573862
    sget-object v1, LX/14P;->CONSERVATIVE:LX/14P;

    .line 573863
    iput-object v1, v0, LX/15E;->j:LX/14P;

    .line 573864
    move-object v0, v0

    .line 573865
    invoke-interface {p1}, Lcom/facebook/tigon/iface/TigonRequest;->d()LX/1iO;

    move-result-object v1

    iget v1, v1, LX/1iO;->a:I

    sget-object v2, Lcom/facebook/http/interfaces/RequestPriority;->DEFAULT_PRIORITY:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-static {v1, v2}, Lcom/facebook/http/interfaces/RequestPriority;->fromNumericValue(ILcom/facebook/http/interfaces/RequestPriority;)Lcom/facebook/http/interfaces/RequestPriority;

    move-result-object v1

    .line 573866
    iput-object v1, v0, LX/15E;->k:Lcom/facebook/http/interfaces/RequestPriority;

    .line 573867
    move-object v0, v0

    .line 573868
    invoke-virtual {v0}, LX/15E;->a()LX/15D;

    move-result-object v0

    .line 573869
    iget-object v1, p0, Lcom/facebook/http/tigon/Tigon4aHttpService;->b:Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-virtual {v1, v0}, Lcom/facebook/http/common/FbHttpRequestProcessor;->b(LX/15D;)LX/1j2;

    move-result-object v0

    .line 573870
    iget-object v1, v0, LX/1j2;->b:Lcom/google/common/util/concurrent/ListenableFuture;

    move-object v1, v1

    .line 573871
    new-instance v2, LX/2b7;

    invoke-direct {v2, p0, p3}, LX/2b7;-><init>(Lcom/facebook/http/tigon/Tigon4aHttpService;LX/2Ys;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v3

    invoke-static {v1, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 573872
    return-object v0

    :cond_1
    move-object v0, v1

    move-object v1, v2

    goto :goto_0
.end method

.method public submitHttpRequest(Lcom/facebook/tigon/javaservice/AbstractRequestToken;Lcom/facebook/tigon/iface/TigonRequest;[B)V
    .locals 4
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 573823
    :try_start_0
    check-cast p1, Lcom/facebook/http/tigon/Tigon4aRequestToken;

    .line 573824
    new-instance v1, LX/2Ys;

    invoke-direct {v1, p1}, LX/2Ys;-><init>(Lcom/facebook/http/tigon/Tigon4aRequestToken;)V

    .line 573825
    const/4 v0, 0x0

    .line 573826
    if-eqz p3, :cond_0

    .line 573827
    new-instance v0, Lorg/apache/http/entity/ByteArrayEntity;

    invoke-direct {v0, p3}, Lorg/apache/http/entity/ByteArrayEntity;-><init>([B)V

    .line 573828
    :cond_0
    invoke-virtual {p0, p2, v0, v1}, Lcom/facebook/http/tigon/Tigon4aHttpService;->a(Lcom/facebook/tigon/iface/TigonRequest;Lorg/apache/http/HttpEntity;LX/2Ys;)LX/1j2;

    move-result-object v0

    .line 573829
    iput-object v0, p1, Lcom/facebook/http/tigon/Tigon4aRequestToken;->b:LX/1j2;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    .line 573830
    return-void

    .line 573831
    :catch_0
    move-exception v0

    .line 573832
    sget-object v1, Lcom/facebook/http/tigon/Tigon4aHttpService;->a:Ljava/lang/String;

    const-string v2, "submitHttpRequest"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 573833
    throw v0

    .line 573834
    :catch_1
    move-exception v0

    .line 573835
    sget-object v1, Lcom/facebook/http/tigon/Tigon4aHttpService;->a:Ljava/lang/String;

    const-string v2, "submitHttpRequest"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 573836
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
