.class public Lcom/facebook/abtest/qe/sessionlessqe/SyncSessionlessQuickExperimentBackgroundTask;
.super LX/2Tu;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile d:Lcom/facebook/abtest/qe/sessionlessqe/SyncSessionlessQuickExperimentBackgroundTask;


# instance fields
.field private final b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final c:LX/0pb;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 573943
    const-class v0, Lcom/facebook/abtest/qe/sessionlessqe/SyncSessionlessQuickExperimentBackgroundTask;

    sput-object v0, Lcom/facebook/abtest/qe/sessionlessqe/SyncSessionlessQuickExperimentBackgroundTask;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;LX/0pb;LX/0aG;Ljava/util/concurrent/ExecutorService;)V
    .locals 6
    .param p4    # LX/0aG;
        .annotation build Lcom/facebook/inject/NeedsApplicationInjector;
        .end annotation
    .end param
    .param p5    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 573939
    const-string v1, "SyncSessionlessQuickExperimentBackgroundTask"

    sget-object v2, Lcom/facebook/abtest/qe/sessionlessqe/SyncSessionlessQuickExperimentBackgroundTask;->a:Ljava/lang/Class;

    move-object v0, p0

    move-object v3, p2

    move-object v4, p5

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/2Tu;-><init>(Ljava/lang/String;Ljava/lang/Class;LX/0SG;Ljava/util/concurrent/ExecutorService;LX/0aG;)V

    .line 573940
    iput-object p3, p0, Lcom/facebook/abtest/qe/sessionlessqe/SyncSessionlessQuickExperimentBackgroundTask;->c:LX/0pb;

    .line 573941
    iput-object p1, p0, Lcom/facebook/abtest/qe/sessionlessqe/SyncSessionlessQuickExperimentBackgroundTask;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 573942
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/abtest/qe/sessionlessqe/SyncSessionlessQuickExperimentBackgroundTask;
    .locals 9

    .prologue
    .line 573926
    sget-object v0, Lcom/facebook/abtest/qe/sessionlessqe/SyncSessionlessQuickExperimentBackgroundTask;->d:Lcom/facebook/abtest/qe/sessionlessqe/SyncSessionlessQuickExperimentBackgroundTask;

    if-nez v0, :cond_1

    .line 573927
    const-class v1, Lcom/facebook/abtest/qe/sessionlessqe/SyncSessionlessQuickExperimentBackgroundTask;

    monitor-enter v1

    .line 573928
    :try_start_0
    sget-object v0, Lcom/facebook/abtest/qe/sessionlessqe/SyncSessionlessQuickExperimentBackgroundTask;->d:Lcom/facebook/abtest/qe/sessionlessqe/SyncSessionlessQuickExperimentBackgroundTask;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 573929
    if-eqz v2, :cond_0

    .line 573930
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 573931
    new-instance v3, Lcom/facebook/abtest/qe/sessionlessqe/SyncSessionlessQuickExperimentBackgroundTask;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v5

    check-cast v5, LX/0SG;

    invoke-static {v0}, LX/0pb;->a(LX/0QB;)LX/0pb;

    move-result-object v6

    check-cast v6, LX/0pb;

    invoke-interface {v0}, LX/0QB;->getApplicationInjector()LX/0QA;

    move-result-object v7

    invoke-static {v7}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v7

    check-cast v7, LX/0aG;

    invoke-static {v0}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v8

    check-cast v8, Ljava/util/concurrent/ExecutorService;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/abtest/qe/sessionlessqe/SyncSessionlessQuickExperimentBackgroundTask;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;LX/0pb;LX/0aG;Ljava/util/concurrent/ExecutorService;)V

    .line 573932
    move-object v0, v3

    .line 573933
    sput-object v0, Lcom/facebook/abtest/qe/sessionlessqe/SyncSessionlessQuickExperimentBackgroundTask;->d:Lcom/facebook/abtest/qe/sessionlessqe/SyncSessionlessQuickExperimentBackgroundTask;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 573934
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 573935
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 573936
    :cond_1
    sget-object v0, Lcom/facebook/abtest/qe/sessionlessqe/SyncSessionlessQuickExperimentBackgroundTask;->d:Lcom/facebook/abtest/qe/sessionlessqe/SyncSessionlessQuickExperimentBackgroundTask;

    return-object v0

    .line 573937
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 573938
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final d()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 573925
    const-class v0, Lcom/facebook/abtest/qe/service/module/QuickExperimentQueue;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final h()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "LX/2VD;",
            ">;"
        }
    .end annotation

    .prologue
    .line 573924
    sget-object v0, LX/2VD;->NETWORK_CONNECTIVITY:LX/2VD;

    invoke-static {v0}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    return-object v0
.end method

.method public final k()J
    .locals 4

    .prologue
    .line 573923
    iget-object v0, p0, Lcom/facebook/abtest/qe/sessionlessqe/SyncSessionlessQuickExperimentBackgroundTask;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2VF;->b:LX/0Tn;

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final l()Z
    .locals 3

    .prologue
    .line 573917
    invoke-static {}, LX/0pb;->a()Ljava/lang/String;

    move-result-object v0

    .line 573918
    iget-object v1, p0, Lcom/facebook/abtest/qe/sessionlessqe/SyncSessionlessQuickExperimentBackgroundTask;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/2VF;->c:LX/0Tn;

    invoke-interface {v1, v2, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 573919
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final m()J
    .locals 2

    .prologue
    .line 573922
    const-wide/32 v0, 0x5265c00

    return-wide v0
.end method

.method public final n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 573921
    const-string v0, "sync_sessionless_qe"

    return-object v0
.end method

.method public final o()Lcom/facebook/common/callercontext/CallerContext;
    .locals 1

    .prologue
    .line 573920
    const-class v0, Lcom/facebook/abtest/qe/sessionlessqe/SyncSessionlessQuickExperimentBackgroundTask;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    return-object v0
.end method
