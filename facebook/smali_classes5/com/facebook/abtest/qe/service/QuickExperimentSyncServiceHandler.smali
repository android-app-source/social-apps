.class public Lcom/facebook/abtest/qe/service/QuickExperimentSyncServiceHandler;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/1qM;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:LX/18W;

.field public final b:LX/2VH;

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2VM;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/18W;LX/2VH;LX/0Or;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/http/protocol/MethodBatcher;",
            "LX/2VH;",
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2VM;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 574858
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 574859
    iput-object p1, p0, Lcom/facebook/abtest/qe/service/QuickExperimentSyncServiceHandler;->a:LX/18W;

    .line 574860
    iput-object p2, p0, Lcom/facebook/abtest/qe/service/QuickExperimentSyncServiceHandler;->b:LX/2VH;

    .line 574861
    iput-object p3, p0, Lcom/facebook/abtest/qe/service/QuickExperimentSyncServiceHandler;->c:LX/0Or;

    .line 574862
    iput-object p4, p0, Lcom/facebook/abtest/qe/service/QuickExperimentSyncServiceHandler;->d:LX/0Ot;

    .line 574863
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/abtest/qe/service/QuickExperimentSyncServiceHandler;
    .locals 7

    .prologue
    .line 574840
    const-class v1, Lcom/facebook/abtest/qe/service/QuickExperimentSyncServiceHandler;

    monitor-enter v1

    .line 574841
    :try_start_0
    sget-object v0, Lcom/facebook/abtest/qe/service/QuickExperimentSyncServiceHandler;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 574842
    sput-object v2, Lcom/facebook/abtest/qe/service/QuickExperimentSyncServiceHandler;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 574843
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 574844
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 574845
    new-instance v5, Lcom/facebook/abtest/qe/service/QuickExperimentSyncServiceHandler;

    invoke-static {v0}, LX/18W;->a(LX/0QB;)LX/18W;

    move-result-object v3

    check-cast v3, LX/18W;

    .line 574846
    new-instance v6, LX/2VH;

    .line 574847
    new-instance v4, LX/2VI;

    invoke-direct {v4}, LX/2VI;-><init>()V

    .line 574848
    move-object v4, v4

    .line 574849
    move-object v4, v4

    .line 574850
    check-cast v4, LX/2VI;

    invoke-direct {v6, v4}, LX/2VH;-><init>(LX/2VI;)V

    .line 574851
    move-object v4, v6

    .line 574852
    check-cast v4, LX/2VH;

    const/16 v6, 0xb83

    invoke-static {v0, v6}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 p0, 0x5c

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v5, v3, v4, v6, p0}, Lcom/facebook/abtest/qe/service/QuickExperimentSyncServiceHandler;-><init>(LX/18W;LX/2VH;LX/0Or;LX/0Ot;)V

    .line 574853
    move-object v0, v5

    .line 574854
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 574855
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/abtest/qe/service/QuickExperimentSyncServiceHandler;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 574856
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 574857
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(LX/1qK;Z)Lcom/facebook/fbservice/service/OperationResult;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 574792
    iget-object v0, p0, Lcom/facebook/abtest/qe/service/QuickExperimentSyncServiceHandler;->a:LX/18W;

    invoke-virtual {v0}, LX/18W;->a()LX/2VK;

    move-result-object v2

    .line 574793
    iget-object v0, p0, Lcom/facebook/abtest/qe/service/QuickExperimentSyncServiceHandler;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2VM;

    .line 574794
    iget-object v3, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v3, v3

    .line 574795
    const-string v4, "chunk_count"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 574796
    iget-object v4, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v4, v4

    .line 574797
    const-string v5, "chunk_number"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v0, p2, v3, v4}, LX/2VM;->a(ZII)Ljava/util/Collection;

    move-result-object v3

    .line 574798
    invoke-interface {v3}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 574799
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 574800
    :goto_0
    return-object v0

    .line 574801
    :cond_0
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Vj;

    .line 574802
    invoke-interface {v2, v0}, LX/2VK;->a(LX/2Vj;)V

    goto :goto_1

    .line 574803
    :cond_1
    iget-object v0, p1, LX/1qK;->mCallerContext:Lcom/facebook/common/callercontext/CallerContext;

    move-object v0, v0

    .line 574804
    if-eqz v0, :cond_3

    .line 574805
    iget-object v0, p1, LX/1qK;->mCallerContext:Lcom/facebook/common/callercontext/CallerContext;

    move-object v0, v0

    .line 574806
    :goto_2
    iget-object v4, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v4, v4

    .line 574807
    if-eqz v4, :cond_2

    const-string v5, "force_refresh"

    invoke-virtual {v4, v5, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v1, 0x1

    .line 574808
    :cond_2
    new-instance v4, LX/14U;

    invoke-direct {v4}, LX/14U;-><init>()V

    .line 574809
    if-eqz v1, :cond_4

    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    .line 574810
    :goto_3
    iput-object v1, v4, LX/14U;->e:Lcom/facebook/http/interfaces/RequestPriority;

    .line 574811
    if-eqz p2, :cond_5

    const-string v1, "handleGetSessionlessQEs"

    .line 574812
    :goto_4
    invoke-interface {v2, v1, v0, v4}, LX/2VK;->a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;LX/14U;)V

    .line 574813
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v1

    .line 574814
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Vj;

    .line 574815
    iget-object v4, v0, LX/2Vj;->c:Ljava/lang/String;

    move-object v4, v4

    .line 574816
    iget-object v5, v0, LX/2Vj;->c:Ljava/lang/String;

    move-object v0, v5

    .line 574817
    invoke-interface {v2, v0}, LX/2VK;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v4, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    goto :goto_5

    .line 574818
    :cond_3
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    goto :goto_2

    .line 574819
    :cond_4
    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->CAN_WAIT:Lcom/facebook/http/interfaces/RequestPriority;

    goto :goto_3

    .line 574820
    :cond_5
    const-string v1, "handleGetQEs"

    goto :goto_4

    .line 574821
    :cond_6
    iget-object v0, p0, Lcom/facebook/abtest/qe/service/QuickExperimentSyncServiceHandler;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2VM;

    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, LX/2VM;->a(Ljava/util/Map;Z)V

    .line 574822
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 574823
    goto :goto_0
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 574824
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 574825
    const-string v1, "sync_qe"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 574826
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/abtest/qe/service/QuickExperimentSyncServiceHandler;->a(LX/1qK;Z)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 574827
    :goto_0
    return-object v0

    .line 574828
    :cond_0
    const-string v1, "sync_sessionless_qe"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 574829
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/facebook/abtest/qe/service/QuickExperimentSyncServiceHandler;->a(LX/1qK;Z)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0

    .line 574830
    :cond_1
    const-string v1, "log_to_qe"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 574831
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 574832
    const-string v1, "experiment_logging_params"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/abtest/qe/protocol/sync/QuickExperimentLoggingParams;

    .line 574833
    iget-object v1, p0, Lcom/facebook/abtest/qe/service/QuickExperimentSyncServiceHandler;->c:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/11H;

    iget-object v2, p0, Lcom/facebook/abtest/qe/service/QuickExperimentSyncServiceHandler;->b:LX/2VH;

    .line 574834
    iget-object v3, p1, LX/1qK;->mCallerContext:Lcom/facebook/common/callercontext/CallerContext;

    move-object v3, v3

    .line 574835
    invoke-virtual {v1, v2, v0, v3}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    .line 574836
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 574837
    move-object v0, v0

    .line 574838
    goto :goto_0

    .line 574839
    :cond_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown operation type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
