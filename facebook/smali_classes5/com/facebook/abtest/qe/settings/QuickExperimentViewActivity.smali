.class public Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;
.super Lcom/facebook/base/activity/FbPreferenceActivity;
.source ""


# instance fields
.field public a:LX/0ae;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0cb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Or;
    .annotation runtime Lcom/facebook/abtest/qe/annotations/IsUserTrustedWithQEInternals;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/3z2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/3yz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0TD;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:Ljava/lang/String;

.field private i:Z

.field public j:Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 662265
    invoke-direct {p0}, Lcom/facebook/base/activity/FbPreferenceActivity;-><init>()V

    .line 662266
    return-void
.end method

.method private a(LX/0oc;)LX/3yo;
    .locals 1

    .prologue
    .line 662262
    iget-boolean v0, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;->i:Z

    if-eqz v0, :cond_0

    .line 662263
    invoke-direct {p0, p1}, Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;->c(LX/0oc;)LX/3yo;

    move-result-object v0

    .line 662264
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, p1}, Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;->b(LX/0oc;)LX/3yo;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 662261
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "experiment_name"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private a()V
    .locals 4

    .prologue
    .line 662259
    iget-object v0, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;->f:LX/1Ck;

    const-string v1, "read_experiment_metainfo"

    iget-object v2, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;->g:LX/0TD;

    new-instance v3, LX/3z4;

    invoke-direct {v3, p0}, LX/3z4;-><init>(Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;)V

    invoke-interface {v2, v3}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    new-instance v3, LX/3z5;

    invoke-direct {v3, p0}, LX/3z5;-><init>(Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 662260
    return-void
.end method

.method private static a(Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;LX/0ae;LX/0cb;LX/0Or;LX/3z2;LX/3yz;LX/1Ck;LX/0TD;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;",
            "LX/0ae;",
            "LX/0cb;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/3z2;",
            "LX/3yz;",
            "LX/1Ck;",
            "LX/0TD;",
            ")V"
        }
    .end annotation

    .prologue
    .line 662258
    iput-object p1, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;->a:LX/0ae;

    iput-object p2, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;->b:LX/0cb;

    iput-object p3, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;->c:LX/0Or;

    iput-object p4, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;->d:LX/3z2;

    iput-object p5, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;->e:LX/3yz;

    iput-object p6, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;->f:LX/1Ck;

    iput-object p7, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;->g:LX/0TD;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 8

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v7

    move-object v0, p0

    check-cast v0, Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;

    invoke-static {v7}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v1

    check-cast v1, LX/0ae;

    invoke-static {v7}, LX/0ca;->a(LX/0QB;)LX/0ca;

    move-result-object v2

    check-cast v2, LX/0cb;

    const/16 v3, 0x1437

    invoke-static {v7, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static {v7}, LX/3z2;->a(LX/0QB;)LX/3z2;

    move-result-object v4

    check-cast v4, LX/3z2;

    invoke-static {v7}, LX/3yz;->a(LX/0QB;)LX/3yz;

    move-result-object v5

    check-cast v5, LX/3yz;

    invoke-static {v7}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v6

    check-cast v6, LX/1Ck;

    invoke-static {v7}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v7

    check-cast v7, LX/0TD;

    invoke-static/range {v0 .. v7}, Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;->a(Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;LX/0ae;LX/0cb;LX/0Or;LX/3z2;LX/3yz;LX/1Ck;LX/0TD;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/util/concurrent/Callable;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/concurrent/Callable",
            "<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 662147
    iget-object v0, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;->f:LX/1Ck;

    iget-object v1, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;->g:LX/0TD;

    invoke-interface {v1, p2}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance v2, LX/3zC;

    invoke-direct {v2, p0}, LX/3zC;-><init>(Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;)V

    invoke-virtual {v0, p1, v1, v2}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 662148
    return-void
.end method

.method public static a$redex0(Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 662256
    const-string v0, "clear_override"

    new-instance v1, LX/3z9;

    invoke-direct {v1, p0, p1}, LX/3z9;-><init>(Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;Ljava/lang/String;)V

    invoke-direct {p0, v0, v1}, Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;->a(Ljava/lang/String;Ljava/util/concurrent/Callable;)V

    .line 662257
    return-void
.end method

.method public static a$redex0(Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 662254
    const-string v0, "update_override"

    new-instance v1, LX/3zA;

    invoke-direct {v1, p0, p1, p2}, LX/3zA;-><init>(Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0, v1}, Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;->a(Ljava/lang/String;Ljava/util/concurrent/Callable;)V

    .line 662255
    return-void
.end method

.method private b(LX/0oc;)LX/3yo;
    .locals 5

    .prologue
    .line 662231
    sget-object v0, LX/3z3;->a:[I

    invoke-virtual {p1}, LX/0oc;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 662232
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 662233
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;->b:LX/0cb;

    iget-object v1, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;->h:Ljava/lang/String;

    invoke-interface {v0, v1}, LX/0cb;->c(Ljava/lang/String;)Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;

    move-result-object v0

    move-object v3, v0

    .line 662234
    :goto_0
    if-eqz v3, :cond_1

    .line 662235
    iget-boolean v0, v3, LX/2Wx;->c:Z

    move v0, v0

    .line 662236
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    move v2, v0

    .line 662237
    :goto_1
    if-eqz v3, :cond_2

    .line 662238
    iget-object v0, v3, LX/2Wx;->e:Ljava/lang/String;

    move-object v0, v0

    .line 662239
    if-eqz v0, :cond_2

    .line 662240
    iget-object v0, v3, LX/2Wx;->e:Ljava/lang/String;

    move-object v0, v0

    .line 662241
    move-object v1, v0

    .line 662242
    :goto_2
    if-nez v3, :cond_3

    const/4 v0, 0x0

    .line 662243
    :goto_3
    if-nez v0, :cond_0

    .line 662244
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    .line 662245
    :cond_0
    new-instance v3, LX/3yo;

    iget-object v4, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;->h:Ljava/lang/String;

    invoke-direct {v3, v4, v1, v2, v0}, LX/3yo;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/util/Map;)V

    return-object v3

    .line 662246
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;->b:LX/0cb;

    iget-object v1, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;->h:Ljava/lang/String;

    invoke-interface {v0, v1}, LX/0cb;->c(Ljava/lang/String;)Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;

    move-result-object v0

    move-object v3, v0

    .line 662247
    goto :goto_0

    .line 662248
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;->b:LX/0cb;

    iget-object v1, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;->h:Ljava/lang/String;

    invoke-interface {v0, v1}, LX/0cb;->b(Ljava/lang/String;)Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;

    move-result-object v0

    move-object v3, v0

    .line 662249
    goto :goto_0

    .line 662250
    :cond_1
    const/4 v0, 0x0

    move v2, v0

    goto :goto_1

    .line 662251
    :cond_2
    const-string v0, "local_default_group"

    move-object v1, v0

    goto :goto_2

    .line 662252
    :cond_3
    iget-object v0, v3, Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;->g:LX/0P1;

    move-object v0, v0

    .line 662253
    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private b()Z
    .locals 3

    .prologue
    .line 662228
    iget-boolean v0, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;->i:Z

    if-eqz v0, :cond_0

    .line 662229
    iget-object v0, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;->a:LX/0ae;

    iget-object v1, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;->h:Ljava/lang/String;

    invoke-interface {v0, v1}, LX/0ae;->c(Ljava/lang/String;)Z

    move-result v0

    .line 662230
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;->b:LX/0cb;

    iget-object v1, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;->h:Ljava/lang/String;

    invoke-interface {v0, v1}, LX/0cb;->c(Ljava/lang/String;)Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;->b:LX/0cb;

    iget-object v2, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;->h:Ljava/lang/String;

    invoke-interface {v1, v2}, LX/0cb;->b(Ljava/lang/String;)Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;

    move-result-object v1

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b$redex0(Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 662226
    const-string v0, "remove_from_experiment"

    new-instance v1, LX/3zB;

    invoke-direct {v1, p0, p1}, LX/3zB;-><init>(Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;Ljava/lang/String;)V

    invoke-direct {p0, v0, v1}, Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;->a(Ljava/lang/String;Ljava/util/concurrent/Callable;)V

    .line 662227
    return-void
.end method

.method private c(LX/0oc;)LX/3yo;
    .locals 5

    .prologue
    .line 662218
    iget-object v0, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;->a:LX/0ae;

    iget-object v1, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;->h:Ljava/lang/String;

    invoke-interface {v0, p1, v1}, LX/0ae;->b(LX/0oc;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 662219
    if-nez v0, :cond_0

    .line 662220
    const-string v0, "local_default_group"

    .line 662221
    :cond_0
    iget-object v1, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;->a:LX/0ae;

    iget-object v2, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;->h:Ljava/lang/String;

    invoke-interface {v1, p1, v2}, LX/0ae;->a(LX/0oc;Ljava/lang/String;)Z

    move-result v2

    .line 662222
    iget-object v1, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;->a:LX/0ae;

    iget-object v3, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;->h:Ljava/lang/String;

    invoke-interface {v1, p1, v3}, LX/0ae;->d(LX/0oc;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v1

    .line 662223
    if-nez v1, :cond_1

    .line 662224
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v1

    .line 662225
    :cond_1
    new-instance v3, LX/3yo;

    iget-object v4, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;->h:Ljava/lang/String;

    invoke-direct {v3, v4, v0, v2, v1}, LX/3yo;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/util/Map;)V

    return-object v3
.end method

.method public static d(Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;)V
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 662160
    invoke-virtual {p0}, Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/PreferenceManager;->createPreferenceScreen(Landroid/content/Context;)Landroid/preference/PreferenceScreen;

    move-result-object v4

    .line 662161
    sget-object v0, LX/0oc;->ASSIGNED:LX/0oc;

    invoke-direct {p0, v0}, Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;->a(LX/0oc;)LX/3yo;

    move-result-object v3

    .line 662162
    sget-object v0, LX/0oc;->OVERRIDE:LX/0oc;

    invoke-direct {p0, v0}, Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;->a(LX/0oc;)LX/3yo;

    move-result-object v5

    .line 662163
    sget-object v0, LX/0oc;->EFFECTIVE:LX/0oc;

    invoke-direct {p0, v0}, Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;->a(LX/0oc;)LX/3yo;

    move-result-object v6

    .line 662164
    iget-boolean v0, v3, LX/3yo;->c:Z

    .line 662165
    iget-boolean v7, v5, LX/3yo;->c:Z

    .line 662166
    invoke-direct {p0}, Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;->b()Z

    move-result v8

    .line 662167
    new-instance v9, LX/3zE;

    invoke-direct {v9, p0}, LX/3zE;-><init>(Landroid/content/Context;)V

    .line 662168
    if-nez v0, :cond_1

    if-eqz v7, :cond_1

    move v0, v1

    .line 662169
    :goto_0
    iput-object v6, v9, LX/3zE;->b:LX/3yo;

    .line 662170
    iput-boolean v8, v9, LX/3zE;->c:Z

    .line 662171
    iput-boolean v0, v9, LX/3zE;->d:Z

    .line 662172
    invoke-virtual {v4, v9}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 662173
    new-instance v7, Landroid/preference/PreferenceCategory;

    invoke-direct {v7, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 662174
    const-string v0, "Settings"

    invoke-virtual {v7, v0}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 662175
    invoke-virtual {v4, v7}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 662176
    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 662177
    const-string v8, "Server Assigned Group"

    invoke-virtual {v0, v8}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 662178
    if-eqz v3, :cond_0

    iget-object v8, v3, LX/3yo;->b:Ljava/lang/String;

    const-string v9, "local_default_group"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 662179
    :cond_0
    const-string v3, "Not In Any Group"

    invoke-virtual {v0, v3}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 662180
    :goto_1
    iget-object v3, v6, LX/3yo;->a:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    .line 662181
    invoke-virtual {v7, v0}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 662182
    iget-object v0, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;->j:Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationModel;

    if-eqz v0, :cond_5

    .line 662183
    new-instance v6, Landroid/preference/ListPreference;

    invoke-direct {v6, p0}, Landroid/preference/ListPreference;-><init>(Landroid/content/Context;)V

    .line 662184
    const-string v0, "Client Overwrite"

    invoke-virtual {v6, v0}, Landroid/preference/ListPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 662185
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 662186
    iget-object v3, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;->j:Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationModel;

    invoke-virtual {v3}, Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationModel;->e_()Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationParameterSetsConnectionModel;

    move-result-object v3

    if-eqz v3, :cond_6

    .line 662187
    iget-object v0, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;->j:Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationModel;

    invoke-virtual {v0}, Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationModel;->e_()Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationParameterSetsConnectionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationParameterSetsConnectionModel;->a()LX/0Px;

    move-result-object v0

    new-instance v3, LX/3z6;

    invoke-direct {v3, p0}, LX/3z6;-><init>(Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;)V

    invoke-static {v0, v3}, LX/0R9;->a(Ljava/util/List;LX/0QK;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    move-object v3, v0

    .line 662188
    :goto_2
    const-string v0, "<Unset Override>"

    invoke-interface {v3, v2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 662189
    const-string v0, "<Remove From Experiment>"

    invoke-interface {v3, v1, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 662190
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    .line 662191
    new-array v8, v1, [Ljava/lang/String;

    .line 662192
    new-array v9, v1, [Ljava/lang/String;

    .line 662193
    :goto_3
    if-ge v2, v1, :cond_3

    .line 662194
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 662195
    aput-object v0, v8, v2

    .line 662196
    invoke-static {v0}, LX/3yz;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v9, v2

    .line 662197
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_1
    move v0, v2

    .line 662198
    goto/16 :goto_0

    .line 662199
    :cond_2
    iget-object v3, v3, LX/3yo;->b:Ljava/lang/String;

    invoke-static {v3}, LX/3yz;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 662200
    :cond_3
    invoke-virtual {v6, v9}, Landroid/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    .line 662201
    invoke-virtual {v6, v8}, Landroid/preference/ListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 662202
    const-string v0, ""

    .line 662203
    if-eqz v5, :cond_4

    iget-boolean v1, v5, LX/3yo;->c:Z

    if-eqz v1, :cond_4

    .line 662204
    iget-object v0, v5, LX/3yo;->b:Ljava/lang/String;

    .line 662205
    :cond_4
    invoke-virtual {v6, v0}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 662206
    invoke-virtual {v6, v0}, Landroid/preference/ListPreference;->setDefaultValue(Ljava/lang/Object;)V

    .line 662207
    invoke-static {v0}, LX/3yz;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 662208
    iget-object v0, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;->h:Ljava/lang/String;

    invoke-virtual {v6, v0}, Landroid/preference/ListPreference;->setKey(Ljava/lang/String;)V

    .line 662209
    new-instance v0, LX/3z7;

    invoke-direct {v0, p0}, LX/3z7;-><init>(Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;)V

    invoke-virtual {v6, v0}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 662210
    invoke-virtual {v7, v6}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 662211
    :goto_4
    invoke-virtual {p0, v4}, Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;->setPreferenceScreen(Landroid/preference/PreferenceScreen;)V

    .line 662212
    return-void

    .line 662213
    :cond_5
    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 662214
    const-string v1, "Client Overwrite"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 662215
    const-string v1, "Config data is not available."

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 662216
    new-instance v1, LX/3z8;

    invoke-direct {v1, p0}, LX/3z8;-><init>(Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 662217
    invoke-virtual {v7, v0}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    goto :goto_4

    :cond_6
    move-object v3, v0

    goto :goto_2
.end method


# virtual methods
.method public final c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 662152
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbPreferenceActivity;->c(Landroid/os/Bundle;)V

    .line 662153
    invoke-static {p0, p0}, Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 662154
    iget-object v0, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 662155
    invoke-virtual {p0}, Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;->finish()V

    .line 662156
    :goto_0
    return-void

    .line 662157
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    const-string v1, "experiment_name"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;->h:Ljava/lang/String;

    .line 662158
    iget-object v0, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;->a:LX/0ae;

    iget-object v1, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;->h:Ljava/lang/String;

    invoke-interface {v0, v1}, LX/0ae;->b(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;->i:Z

    .line 662159
    invoke-direct {p0}, Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;->a()V

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x7077c98e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 662149
    iget-object v1, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;->f:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 662150
    invoke-super {p0}, Lcom/facebook/base/activity/FbPreferenceActivity;->onDestroy()V

    .line 662151
    const/16 v1, 0x23

    const v2, -0x6f2f7e5d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
