.class public Lcom/facebook/abtest/qe/settings/QuickExperimentSyncDialogFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final p:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public m:LX/0aG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/0kL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/3yp;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Z

.field public s:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 661981
    const-class v0, Lcom/facebook/abtest/qe/settings/QuickExperimentSyncDialogFragment;

    sput-object v0, Lcom/facebook/abtest/qe/settings/QuickExperimentSyncDialogFragment;->p:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 661982
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    .line 661983
    return-void
.end method

.method public static a$redex0(Lcom/facebook/abtest/qe/settings/QuickExperimentSyncDialogFragment;Landroid/os/Bundle;I)V
    .locals 8

    .prologue
    const/16 v1, 0xa

    .line 661984
    if-ne p2, v1, :cond_0

    .line 661985
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentSyncDialogFragment;->r:Z

    .line 661986
    invoke-static {p0}, Lcom/facebook/abtest/qe/settings/QuickExperimentSyncDialogFragment;->l(Lcom/facebook/abtest/qe/settings/QuickExperimentSyncDialogFragment;)V

    .line 661987
    :goto_0
    return-void

    .line 661988
    :cond_0
    const-string v0, "chunk_count"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 661989
    const-string v0, "chunk_number"

    invoke-virtual {p1, v0, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 661990
    iget-object v6, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentSyncDialogFragment;->n:LX/1Ck;

    const-string v7, "syncRegular"

    iget-object v0, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentSyncDialogFragment;->m:LX/0aG;

    const-string v1, "sync_qe"

    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    const-class v2, Lcom/facebook/abtest/qe/settings/QuickExperimentSyncDialogFragment;

    invoke-static {v2}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    const v5, 0x72349b11

    move-object v2, p1

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    new-instance v1, LX/3z1;

    invoke-direct {v1, p0, p1, p2}, LX/3z1;-><init>(Lcom/facebook/abtest/qe/settings/QuickExperimentSyncDialogFragment;Landroid/os/Bundle;I)V

    invoke-virtual {v6, v7, v0, v1}, LX/1Ck;->c(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_0
.end method

.method public static l(Lcom/facebook/abtest/qe/settings/QuickExperimentSyncDialogFragment;)V
    .locals 1

    .prologue
    .line 661991
    iget-boolean v0, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentSyncDialogFragment;->r:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentSyncDialogFragment;->s:Z

    if-eqz v0, :cond_0

    .line 661992
    invoke-virtual {p0}, Landroid/support/v4/app/DialogFragment;->d()V

    .line 661993
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 661994
    new-instance v0, LX/4BY;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/4BY;-><init>(Landroid/content/Context;)V

    .line 661995
    invoke-static {v0}, LX/4md;->a(Landroid/app/Dialog;)V

    .line 661996
    const/4 v1, 0x0

    .line 661997
    iput v1, v0, LX/4BY;->d:I

    .line 661998
    invoke-virtual {v0, v2}, LX/4BY;->a(Z)V

    .line 661999
    invoke-virtual {v0, v2}, LX/4BY;->setCancelable(Z)V

    .line 662000
    const-string v1, "Quick Experiments"

    invoke-virtual {v0, v1}, LX/4BY;->setTitle(Ljava/lang/CharSequence;)V

    .line 662001
    const-string v1, "Syncing..."

    invoke-virtual {v0, v1}, LX/2EJ;->a(Ljava/lang/CharSequence;)V

    .line 662002
    return-object v0
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 11

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x6b54ce49

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 662003
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 662004
    if-nez p1, :cond_0

    .line 662005
    const/4 v3, 0x0

    .line 662006
    iput-boolean v3, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentSyncDialogFragment;->r:Z

    .line 662007
    iput-boolean v3, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentSyncDialogFragment;->s:Z

    .line 662008
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 662009
    const-string v3, "force_refresh"

    const/4 v4, 0x1

    invoke-virtual {v5, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 662010
    iget-object v3, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentSyncDialogFragment;->n:LX/1Ck;

    invoke-virtual {v3}, LX/1Ck;->c()V

    .line 662011
    invoke-virtual {v5}, Landroid/os/Bundle;->clone()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/Bundle;

    const/4 v4, 0x0

    invoke-static {p0, v3, v4}, Lcom/facebook/abtest/qe/settings/QuickExperimentSyncDialogFragment;->a$redex0(Lcom/facebook/abtest/qe/settings/QuickExperimentSyncDialogFragment;Landroid/os/Bundle;I)V

    .line 662012
    iget-object v9, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentSyncDialogFragment;->n:LX/1Ck;

    const-string v10, "syncRegular"

    iget-object v3, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentSyncDialogFragment;->m:LX/0aG;

    const-string v4, "sync_sessionless_qe"

    sget-object v6, LX/1ME;->BY_EXCEPTION:LX/1ME;

    const-class v7, Lcom/facebook/abtest/qe/settings/QuickExperimentSyncDialogFragment;

    invoke-static {v7}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v7

    const v8, 0x7b96a3c1

    invoke-static/range {v3 .. v8}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v3

    invoke-interface {v3}, LX/1MF;->start()LX/1ML;

    move-result-object v3

    new-instance v4, LX/3z0;

    invoke-direct {v4, p0}, LX/3z0;-><init>(Lcom/facebook/abtest/qe/settings/QuickExperimentSyncDialogFragment;)V

    invoke-virtual {v9, v10, v3, v4}, LX/1Ck;->c(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 662013
    :goto_0
    const v1, 0x3f01c8b0

    invoke-static {v1, v0}, LX/02F;->f(II)V

    return-void

    .line 662014
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/DialogFragment;->d()V

    goto :goto_0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x1be3ec41

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 662015
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 662016
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/abtest/qe/settings/QuickExperimentSyncDialogFragment;

    invoke-static {v1}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v4

    check-cast v4, LX/0aG;

    invoke-static {v1}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object p1

    check-cast p1, LX/1Ck;

    invoke-static {v1}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v1

    check-cast v1, LX/0kL;

    iput-object v4, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentSyncDialogFragment;->m:LX/0aG;

    iput-object p1, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentSyncDialogFragment;->n:LX/1Ck;

    iput-object v1, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentSyncDialogFragment;->o:LX/0kL;

    .line 662017
    const/16 v1, 0x2b

    const v2, 0x4341c6d6

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDismiss(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 662018
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 662019
    iget-object v0, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentSyncDialogFragment;->n:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->c()V

    .line 662020
    iget-object v0, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentSyncDialogFragment;->q:LX/3yp;

    if-eqz v0, :cond_0

    .line 662021
    iget-object v0, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentSyncDialogFragment;->q:LX/3yp;

    invoke-virtual {v0}, LX/3yp;->a()V

    .line 662022
    :cond_0
    return-void
.end method
