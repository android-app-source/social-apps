.class public Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;
.super Lcom/facebook/base/activity/FbPreferenceActivity;
.source ""


# static fields
.field public static final j:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/3yR;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Or;
    .annotation runtime Lcom/facebook/abtest/qe/annotations/IsUserTrustedWithQEInternals;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/3yz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0SG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0TD;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:Ljava/util/concurrent/ScheduledExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/3z2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private k:Landroid/preference/Preference;

.field private l:Ljava/util/concurrent/ScheduledFuture;

.field private m:LX/0dN;

.field private n:Ljava/lang/String;

.field public o:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/3yQ;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 661932
    const-class v0, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;

    sput-object v0, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->j:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 661933
    invoke-direct {p0}, Lcom/facebook/base/activity/FbPreferenceActivity;-><init>()V

    .line 661934
    new-instance v0, LX/3yq;

    invoke-direct {v0, p0}, LX/3yq;-><init>(Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;)V

    iput-object v0, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->m:LX/0dN;

    .line 661935
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->n:Ljava/lang/String;

    return-void
.end method

.method public static a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 661931
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method

.method private a(Landroid/preference/PreferenceGroup;)V
    .locals 2

    .prologue
    .line 661925
    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->k:Landroid/preference/Preference;

    .line 661926
    iget-object v0, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->k:Landroid/preference/Preference;

    const-string v1, "Sync Quick Experiments Now"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 661927
    iget-object v0, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->k:Landroid/preference/Preference;

    new-instance v1, LX/3yx;

    invoke-direct {v1, p0}, LX/3yx;-><init>(Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 661928
    iget-object v0, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->k:Landroid/preference/Preference;

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 661929
    invoke-static {p0}, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->e(Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;)V

    .line 661930
    return-void
.end method

.method private a(Landroid/preference/PreferenceScreen;)V
    .locals 3

    .prologue
    .line 661913
    new-instance v0, LX/4om;

    invoke-direct {v0, p0}, LX/4om;-><init>(Landroid/content/Context;)V

    .line 661914
    iget-object v1, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/4om;->setText(Ljava/lang/String;)V

    .line 661915
    const-string v1, "Search for Experiments"

    invoke-virtual {v0, v1}, LX/4om;->setTitle(Ljava/lang/CharSequence;)V

    .line 661916
    invoke-virtual {v0}, LX/4om;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->b(Landroid/preference/Preference;Ljava/lang/String;)V

    .line 661917
    invoke-virtual {v0}, LX/4om;->getEditText()Landroid/widget/EditText;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setSelectAllOnFocus(Z)V

    .line 661918
    new-instance v1, LX/3yu;

    invoke-direct {v1, p0}, LX/3yu;-><init>(Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;)V

    invoke-virtual {v0, v1}, LX/4om;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 661919
    invoke-virtual {v0}, LX/4om;->getEditText()Landroid/widget/EditText;

    move-result-object v1

    .line 661920
    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setImeOptions(I)V

    .line 661921
    invoke-virtual {v1}, Landroid/widget/EditText;->setSingleLine()V

    .line 661922
    new-instance v2, LX/3yv;

    invoke-direct {v2, p0, v0}, LX/3yv;-><init>(Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;LX/4om;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 661923
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 661924
    return-void
.end method

.method private static a(Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;LX/3yR;LX/0Or;Lcom/facebook/prefs/shared/FbSharedPreferences;Lcom/facebook/content/SecureContextHelper;LX/3yz;LX/0SG;LX/0TD;Ljava/util/concurrent/ScheduledExecutorService;LX/3z2;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;",
            "LX/3yR;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/3yz;",
            "LX/0SG;",
            "LX/0TD;",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            "LX/3z2;",
            ")V"
        }
    .end annotation

    .prologue
    .line 661912
    iput-object p1, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->a:LX/3yR;

    iput-object p2, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->b:LX/0Or;

    iput-object p3, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object p4, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->d:Lcom/facebook/content/SecureContextHelper;

    iput-object p5, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->e:LX/3yz;

    iput-object p6, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->f:LX/0SG;

    iput-object p7, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->g:LX/0TD;

    iput-object p8, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->h:Ljava/util/concurrent/ScheduledExecutorService;

    iput-object p9, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->i:LX/3z2;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 10

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v9

    move-object v0, p0

    check-cast v0, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;

    invoke-static {v9}, LX/3yR;->a(LX/0QB;)LX/3yR;

    move-result-object v1

    check-cast v1, LX/3yR;

    const/16 v2, 0x1437

    invoke-static {v9, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-static {v9}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v9}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v9}, LX/3yz;->a(LX/0QB;)LX/3yz;

    move-result-object v5

    check-cast v5, LX/3yz;

    invoke-static {v9}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v6

    check-cast v6, LX/0SG;

    invoke-static {v9}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v7

    check-cast v7, LX/0TD;

    invoke-static {v9}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v8

    check-cast v8, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v9}, LX/3z2;->a(LX/0QB;)LX/3z2;

    move-result-object v9

    check-cast v9, LX/3z2;

    invoke-static/range {v0 .. v9}, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->a(Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;LX/3yR;LX/0Or;Lcom/facebook/prefs/shared/FbSharedPreferences;Lcom/facebook/content/SecureContextHelper;LX/3yz;LX/0SG;LX/0TD;Ljava/util/concurrent/ScheduledExecutorService;LX/3z2;)V

    return-void
.end method

.method private a(Landroid/preference/Preference;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 661907
    invoke-static {p1, p2}, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->b(Landroid/preference/Preference;Ljava/lang/String;)V

    .line 661908
    iget-object v0, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->n:Ljava/lang/String;

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 661909
    iput-object p2, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->n:Ljava/lang/String;

    .line 661910
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->b$redex0(Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;Z)V

    .line 661911
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public static synthetic a(Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;Landroid/preference/Preference;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 661906
    invoke-direct {p0, p1, p2}, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->a(Landroid/preference/Preference;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static a$redex0(Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;Z)V
    .locals 3

    .prologue
    .line 661903
    iget-object v0, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->g:LX/0TD;

    new-instance v1, LX/3yr;

    invoke-direct {v1, p0}, LX/3yr;-><init>(Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 661904
    new-instance v1, LX/3ys;

    invoke-direct {v1, p0, p1}, LX/3ys;-><init>(Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;Z)V

    iget-object v2, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->h:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 661905
    return-void
.end method

.method private b()LX/3yy;
    .locals 3

    .prologue
    .line 661936
    iget-object v0, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2ab;->f:LX/0Tn;

    sget-object v2, LX/3yy;->EXPERIMENTS_IM_IN:LX/3yy;

    invoke-virtual {v2}, LX/3yy;->name()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/3yy;->valueOf(Ljava/lang/String;)LX/3yy;

    move-result-object v0

    return-object v0
.end method

.method private static b(Landroid/preference/Preference;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 661900
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Show only some experiments"

    :goto_0
    invoke-virtual {p0, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 661901
    return-void

    .line 661902
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Showing only: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private b(Landroid/preference/PreferenceScreen;)V
    .locals 6

    .prologue
    .line 661885
    new-instance v1, LX/4ot;

    invoke-direct {v1, p0}, LX/4ot;-><init>(Landroid/content/Context;)V

    .line 661886
    sget-object v0, LX/2ab;->f:LX/0Tn;

    invoke-virtual {v1, v0}, LX/4or;->a(LX/0Tn;)V

    .line 661887
    const-string v0, "Filter Experiments"

    invoke-virtual {v1, v0}, LX/4ot;->setTitle(Ljava/lang/CharSequence;)V

    .line 661888
    sget-object v0, LX/3yy;->SHOW_ALL_EXPERIMENTS:LX/3yy;

    invoke-virtual {v0}, LX/3yy;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/4ot;->setDefaultValue(Ljava/lang/Object;)V

    .line 661889
    invoke-static {}, LX/3yy;->values()[LX/3yy;

    move-result-object v0

    array-length v0, v0

    new-array v2, v0, [Ljava/lang/CharSequence;

    .line 661890
    invoke-static {}, LX/3yy;->values()[LX/3yy;

    move-result-object v0

    array-length v0, v0

    new-array v3, v0, [Ljava/lang/CharSequence;

    .line 661891
    const/4 v0, 0x0

    :goto_0
    invoke-static {}, LX/3yy;->values()[LX/3yy;

    move-result-object v4

    array-length v4, v4

    if-ge v0, v4, :cond_0

    .line 661892
    invoke-static {}, LX/3yy;->values()[LX/3yy;

    move-result-object v4

    aget-object v4, v4, v0

    .line 661893
    invoke-virtual {v4}, LX/3yy;->getKey()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v0

    .line 661894
    invoke-virtual {v4}, LX/3yy;->getValue()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    .line 661895
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 661896
    :cond_0
    invoke-virtual {v1, v2}, LX/4ot;->setEntries([Ljava/lang/CharSequence;)V

    .line 661897
    invoke-virtual {v1, v3}, LX/4ot;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 661898
    invoke-virtual {p1, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 661899
    return-void
.end method

.method public static b$redex0(Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;Z)V
    .locals 11

    .prologue
    const/4 v2, 0x0

    .line 661846
    invoke-virtual {p0}, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/PreferenceManager;->createPreferenceScreen(Landroid/content/Context;)Landroid/preference/PreferenceScreen;

    move-result-object v3

    .line 661847
    invoke-direct {p0, v3}, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->a(Landroid/preference/PreferenceGroup;)V

    .line 661848
    invoke-direct {p0, v3}, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->a(Landroid/preference/PreferenceScreen;)V

    .line 661849
    invoke-direct {p0, v3}, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->b(Landroid/preference/PreferenceScreen;)V

    .line 661850
    invoke-direct {p0, v3}, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->c(Landroid/preference/PreferenceScreen;)V

    .line 661851
    invoke-direct {p0}, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->b()LX/3yy;

    move-result-object v4

    .line 661852
    new-instance v5, Landroid/preference/PreferenceCategory;

    invoke-direct {v5, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 661853
    const-string v0, "Experiments"

    invoke-virtual {v5, v0}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 661854
    invoke-virtual {v3, v5}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 661855
    iget-object v0, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->o:LX/0Px;

    if-nez v0, :cond_1

    .line 661856
    :cond_0
    :goto_0
    return-void

    .line 661857
    :cond_1
    iget-object v0, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->o:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v6

    move v1, v2

    :goto_1
    if-ge v1, v6, :cond_7

    iget-object v0, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->o:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3yQ;

    .line 661858
    iget-boolean v7, v0, LX/3yQ;->isInExperiment:Z

    if-nez v7, :cond_2

    invoke-virtual {v0}, LX/3yQ;->d()Z

    move-result v7

    if-nez v7, :cond_2

    sget-object v7, LX/3yy;->SHOW_ALL_EXPERIMENTS:LX/3yy;

    if-ne v4, v7, :cond_5

    .line 661859
    :cond_2
    invoke-virtual {v0}, LX/3yQ;->d()Z

    move-result v7

    if-nez v7, :cond_3

    sget-object v7, LX/3yy;->EXPERIMENTS_I_OVERRODE:LX/3yy;

    if-eq v4, v7, :cond_5

    .line 661860
    :cond_3
    iget-object v7, v0, LX/3yQ;->name:Ljava/lang/String;

    move-object v7, v7

    .line 661861
    invoke-static {v7}, LX/3yz;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v7

    .line 661862
    iget-object v8, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->n:Ljava/lang/String;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v8

    .line 661863
    iget-object v9, v0, LX/3yQ;->name:Ljava/lang/String;

    move-object v9, v9

    .line 661864
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_4

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 661865
    :cond_4
    new-instance v7, Landroid/preference/Preference;

    invoke-direct {v7, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 661866
    iget-object v8, v0, LX/3yQ;->name:Ljava/lang/String;

    move-object v8, v8

    .line 661867
    invoke-static {v8}, LX/3yz;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 661868
    iget-boolean v8, v0, LX/3yQ;->isInExperiment:Z

    if-eqz v8, :cond_6

    .line 661869
    invoke-virtual {v0}, LX/3yQ;->c()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, LX/3yz;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 661870
    :goto_2
    new-instance v8, LX/3yt;

    invoke-direct {v8, p0, v0}, LX/3yt;-><init>(Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;LX/3yQ;)V

    invoke-virtual {v7, v8}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 661871
    invoke-virtual {v5, v7}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 661872
    :cond_5
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 661873
    :cond_6
    const-string v8, "<Not In Experiment>"

    invoke-virtual {v7, v8}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 661874
    :cond_7
    invoke-virtual {v3}, Landroid/preference/PreferenceScreen;->getPreferenceCount()I

    move-result v0

    if-nez v0, :cond_8

    .line 661875
    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 661876
    const-string v1, "No experiment data has been downloaded yet."

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 661877
    invoke-virtual {v3, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 661878
    :cond_8
    invoke-virtual {p0, v3}, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->setPreferenceScreen(Landroid/preference/PreferenceScreen;)V

    .line 661879
    if-eqz p1, :cond_0

    .line 661880
    iget-object v0, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2ab;->g:LX/0Tn;

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v0

    .line 661881
    iget-object v1, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/2ab;->h:LX/0Tn;

    invoke-interface {v1, v3, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v1

    .line 661882
    invoke-virtual {p0}, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->getListView()Landroid/widget/ListView;

    move-result-object v2

    .line 661883
    invoke-virtual {v2}, Landroid/widget/ListView;->clearFocus()V

    .line 661884
    new-instance v3, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity$5;

    invoke-direct {v3, p0, v2, v0, v1}, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity$5;-><init>(Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;Landroid/widget/ListView;II)V

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0
.end method

.method private c(Landroid/preference/PreferenceScreen;)V
    .locals 2

    .prologue
    .line 661840
    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 661841
    const-string v1, "Remove All Overrides"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 661842
    const-string v1, "Reset all client overridden changes"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 661843
    new-instance v1, LX/3yw;

    invoke-direct {v1, p0}, LX/3yw;-><init>(Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 661844
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 661845
    return-void
.end method

.method private d()Z
    .locals 1

    .prologue
    .line 661839
    iget-object v0, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public static e(Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;)V
    .locals 5

    .prologue
    .line 661831
    iget-object v0, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->l:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_0

    .line 661832
    iget-object v0, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->l:Ljava/util/concurrent/ScheduledFuture;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 661833
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->l:Ljava/util/concurrent/ScheduledFuture;

    .line 661834
    :cond_0
    iget-object v0, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2ab;->c:LX/0Tn;

    const-wide/16 v2, -0x1

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    .line 661835
    iget-object v2, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->f:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    sub-long v0, v2, v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    invoke-static {v0, v1}, Landroid/text/format/DateUtils;->formatElapsedTime(J)Ljava/lang/String;

    move-result-object v0

    .line 661836
    iget-object v1, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->k:Landroid/preference/Preference;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Last sync was "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " ago"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 661837
    iget-object v0, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->h:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity$10;

    invoke-direct {v1, p0}, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity$10;-><init>(Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;)V

    const-wide/16 v2, 0x1

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->l:Ljava/util/concurrent/ScheduledFuture;

    .line 661838
    return-void
.end method

.method public static f(Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;)V
    .locals 3

    .prologue
    .line 661826
    new-instance v0, Lcom/facebook/abtest/qe/settings/QuickExperimentSyncDialogFragment;

    invoke-direct {v0}, Lcom/facebook/abtest/qe/settings/QuickExperimentSyncDialogFragment;-><init>()V

    .line 661827
    new-instance v1, LX/3yp;

    invoke-direct {v1, p0}, LX/3yp;-><init>(Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;)V

    .line 661828
    iput-object v1, v0, Lcom/facebook/abtest/qe/settings/QuickExperimentSyncDialogFragment;->q:LX/3yp;

    .line 661829
    invoke-virtual {p0}, Lcom/facebook/base/activity/FbPreferenceActivity;->c()LX/0gc;

    move-result-object v1

    const-string v2, "qe_refresh"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 661830
    return-void
.end method


# virtual methods
.method public final c(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 661821
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbPreferenceActivity;->c(Landroid/os/Bundle;)V

    .line 661822
    invoke-static {p0, p0}, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 661823
    invoke-direct {p0}, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 661824
    invoke-virtual {p0}, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->finish()V

    .line 661825
    :cond_0
    return-void
.end method

.method public final onPause()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x0

    const/16 v1, 0x22

    const v2, 0x3096244b

    invoke-static {v5, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 661806
    invoke-super {p0}, Lcom/facebook/base/activity/FbPreferenceActivity;->onPause()V

    .line 661807
    iget-object v1, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->l:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v1, :cond_0

    .line 661808
    iget-object v1, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->l:Ljava/util/concurrent/ScheduledFuture;

    const/4 v3, 0x1

    invoke-interface {v1, v3}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 661809
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->l:Ljava/util/concurrent/ScheduledFuture;

    .line 661810
    :cond_0
    iget-object v1, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/2ab;->f:LX/0Tn;

    iget-object v4, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->m:LX/0dN;

    invoke-interface {v1, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->b(LX/0Tn;LX/0dN;)V

    .line 661811
    invoke-virtual {p0}, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->getListView()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 661812
    if-eqz v3, :cond_1

    .line 661813
    invoke-virtual {p0}, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v1

    .line 661814
    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v0

    .line 661815
    :goto_0
    iget-object v3, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v3

    sget-object v4, LX/2ab;->g:LX/0Tn;

    invoke-interface {v3, v4, v1}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v1

    sget-object v3, LX/2ab;->h:LX/0Tn;

    invoke-interface {v1, v3, v0}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 661816
    const/16 v0, 0x23

    const v1, 0x7ba1548c

    invoke-static {v5, v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :cond_1
    move v1, v0

    goto :goto_0
.end method

.method public final onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x22

    const v1, 0x699b7ff6

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 661817
    invoke-super {p0}, Lcom/facebook/base/activity/FbPreferenceActivity;->onResume()V

    .line 661818
    const/4 v1, 0x1

    invoke-static {p0, v1}, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->a$redex0(Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;Z)V

    .line 661819
    iget-object v1, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/2ab;->f:LX/0Tn;

    iget-object v3, p0, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->m:LX/0dN;

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;LX/0dN;)V

    .line 661820
    const/16 v1, 0x23

    const v2, -0x7b49ca5

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
