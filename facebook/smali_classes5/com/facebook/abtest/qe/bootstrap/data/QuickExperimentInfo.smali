.class public Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;
.super LX/2Wx;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public g:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/2WT;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 575426
    new-instance v0, LX/2WS;

    invoke-direct {v0}, LX/2WS;-><init>()V

    sput-object v0, Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/2WQ;)V
    .locals 6

    .prologue
    .line 575422
    invoke-direct {p0, p1}, LX/2Wx;-><init>(LX/2WR;)V

    .line 575423
    iget-object v0, p1, LX/2WQ;->g:Ljava/util/Map;

    invoke-static {v0}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;->g:LX/0P1;

    .line 575424
    new-instance v0, LX/2WT;

    iget-object v1, p0, LX/2Wx;->a:Ljava/lang/String;

    iget-boolean v2, p0, LX/2Wx;->c:Z

    iget-boolean v3, p0, LX/2Wx;->d:Z

    iget-object v4, p0, LX/2Wx;->e:Ljava/lang/String;

    iget-object v5, p0, Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;->g:LX/0P1;

    invoke-direct/range {v0 .. v5}, LX/2WT;-><init>(Ljava/lang/String;ZZLjava/lang/String;LX/0P1;)V

    iput-object v0, p0, Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;->h:LX/2WT;

    .line 575425
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 575417
    new-instance v0, LX/2WR;

    invoke-direct {v0}, LX/2WR;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/2WR;->a(Ljava/lang/String;)LX/2WR;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/2WR;->b(Ljava/lang/String;)LX/2WR;

    move-result-object v3

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, LX/2WR;->a(Z)LX/2WR;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-ne v3, v1, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, LX/2WR;->b(Z)LX/2WR;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2WR;->c(Ljava/lang/String;)LX/2WR;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2WR;->d(Ljava/lang/String;)LX/2WR;

    move-result-object v0

    invoke-direct {p0, v0}, LX/2Wx;-><init>(LX/2WR;)V

    .line 575418
    const-class v0, Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readHashMap(Ljava/lang/ClassLoader;)Ljava/util/HashMap;

    move-result-object v0

    invoke-static {v0}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;->g:LX/0P1;

    .line 575419
    new-instance v0, LX/2WT;

    iget-object v1, p0, LX/2Wx;->a:Ljava/lang/String;

    iget-boolean v2, p0, LX/2Wx;->c:Z

    iget-boolean v3, p0, LX/2Wx;->d:Z

    iget-object v4, p0, LX/2Wx;->e:Ljava/lang/String;

    iget-object v5, p0, Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;->g:LX/0P1;

    invoke-direct/range {v0 .. v5}, LX/2WT;-><init>(Ljava/lang/String;ZZLjava/lang/String;LX/0P1;)V

    iput-object v0, p0, Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;->h:LX/2WT;

    .line 575420
    return-void

    :cond_0
    move v0, v2

    .line 575421
    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 575415
    iget-object v0, p0, LX/2Wx;->a:Ljava/lang/String;

    move-object v0, v0

    .line 575416
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 575348
    iget-object v0, p0, LX/2Wx;->e:Ljava/lang/String;

    move-object v0, v0

    .line 575349
    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 575413
    iget-object v0, p0, LX/2Wx;->b:Ljava/lang/String;

    move-object v0, v0

    .line 575414
    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 575427
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 575382
    instance-of v0, p1, Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;

    if-nez v0, :cond_0

    .line 575383
    const/4 v0, 0x0

    .line 575384
    :goto_0
    return v0

    .line 575385
    :cond_0
    check-cast p1, Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;

    .line 575386
    const/4 v0, 0x0

    .line 575387
    if-nez p0, :cond_2

    if-eqz p1, :cond_2

    .line 575388
    :cond_1
    :goto_1
    move v0, v0

    .line 575389
    goto :goto_0

    .line 575390
    :cond_2
    if-nez p1, :cond_3

    if-nez p0, :cond_1

    .line 575391
    :cond_3
    iget-object v1, p0, LX/2Wx;->e:Ljava/lang/String;

    move-object v1, v1

    .line 575392
    iget-object v2, p1, LX/2Wx;->e:Ljava/lang/String;

    move-object v2, v2

    .line 575393
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 575394
    iget-object v1, p0, LX/2Wx;->f:Ljava/lang/String;

    move-object v1, v1

    .line 575395
    iget-object v2, p1, LX/2Wx;->f:Ljava/lang/String;

    move-object v2, v2

    .line 575396
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 575397
    iget-object v1, p0, LX/2Wx;->a:Ljava/lang/String;

    move-object v1, v1

    .line 575398
    iget-object v2, p1, LX/2Wx;->a:Ljava/lang/String;

    move-object v2, v2

    .line 575399
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 575400
    iget-boolean v1, p0, LX/2Wx;->c:Z

    move v1, v1

    .line 575401
    iget-boolean v2, p1, LX/2Wx;->c:Z

    move v2, v2

    .line 575402
    if-ne v1, v2, :cond_1

    .line 575403
    iget-boolean v1, p0, LX/2Wx;->d:Z

    move v1, v1

    .line 575404
    iget-boolean v2, p1, LX/2Wx;->d:Z

    move v2, v2

    .line 575405
    if-ne v1, v2, :cond_1

    .line 575406
    iget-object v1, p0, Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;->g:LX/0P1;

    move-object v1, v1

    .line 575407
    iget-object v2, p1, Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;->g:LX/0P1;

    move-object v2, v2

    .line 575408
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 575409
    iget-object v1, p0, LX/2Wx;->b:Ljava/lang/String;

    move-object v1, v1

    .line 575410
    iget-object v2, p1, LX/2Wx;->b:Ljava/lang/String;

    move-object v2, v2

    .line 575411
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 575412
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public final f()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 575380
    iget-object v0, p0, Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;->g:LX/0P1;

    move-object v0, v0

    .line 575381
    return-object v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 575365
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    .line 575366
    iget-object v2, p0, LX/2Wx;->e:Ljava/lang/String;

    move-object v2, v2

    .line 575367
    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 575368
    iget-object v2, p0, LX/2Wx;->f:Ljava/lang/String;

    move-object v2, v2

    .line 575369
    aput-object v2, v0, v1

    const/4 v1, 0x2

    .line 575370
    iget-object v2, p0, LX/2Wx;->a:Ljava/lang/String;

    move-object v2, v2

    .line 575371
    aput-object v2, v0, v1

    const/4 v1, 0x3

    .line 575372
    iget-boolean v2, p0, LX/2Wx;->c:Z

    move v2, v2

    .line 575373
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    .line 575374
    iget-boolean v2, p0, LX/2Wx;->d:Z

    move v2, v2

    .line 575375
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    .line 575376
    iget-object v2, p0, Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;->g:LX/0P1;

    move-object v2, v2

    .line 575377
    aput-object v2, v0, v1

    const/4 v1, 0x6

    .line 575378
    iget-object v2, p0, LX/2Wx;->b:Ljava/lang/String;

    move-object v2, v2

    .line 575379
    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 575360
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 575361
    iget-object v0, p0, LX/2Wx;->a:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/Group:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/2Wx;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/Experiment:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, LX/2Wx;->c:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/InDeployGroup:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, LX/2Wx;->d:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/Locale:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/2Wx;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/customStrings: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 575362
    iget-object v0, p0, Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;->g:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 575363
    const-string v1, "\n"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ": "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 575364
    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 575350
    iget-object v0, p0, LX/2Wx;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 575351
    iget-object v0, p0, LX/2Wx;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 575352
    iget-boolean v0, p0, LX/2Wx;->c:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 575353
    iget-boolean v0, p0, LX/2Wx;->d:Z

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 575354
    iget-object v0, p0, LX/2Wx;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 575355
    iget-object v0, p0, LX/2Wx;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 575356
    iget-object v0, p0, Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;->g:LX/0P1;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    .line 575357
    return-void

    :cond_0
    move v0, v2

    .line 575358
    goto :goto_0

    :cond_1
    move v1, v2

    .line 575359
    goto :goto_1
.end method
