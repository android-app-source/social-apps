.class public Lcom/facebook/abtest/qe/protocol/sync/QuickExperimentLoggingParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static a:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/abtest/qe/protocol/sync/QuickExperimentLoggingParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 661136
    new-instance v0, LX/3ya;

    invoke-direct {v0}, LX/3ya;-><init>()V

    sput-object v0, Lcom/facebook/abtest/qe/protocol/sync/QuickExperimentLoggingParams;->a:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/3yb;)V
    .locals 1

    .prologue
    .line 661137
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 661138
    iget-object v0, p1, LX/3yb;->c:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/abtest/qe/protocol/sync/QuickExperimentLoggingParams;->d:Ljava/lang/String;

    .line 661139
    iget-object v0, p1, LX/3yb;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/abtest/qe/protocol/sync/QuickExperimentLoggingParams;->b:Ljava/lang/String;

    .line 661140
    iget-object v0, p1, LX/3yb;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/abtest/qe/protocol/sync/QuickExperimentLoggingParams;->e:Ljava/lang/String;

    .line 661141
    iget-object v0, p1, LX/3yb;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/abtest/qe/protocol/sync/QuickExperimentLoggingParams;->c:Ljava/lang/String;

    .line 661142
    iget-object v0, p1, LX/3yb;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/abtest/qe/protocol/sync/QuickExperimentLoggingParams;->f:Ljava/lang/String;

    .line 661143
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 661144
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 661145
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/abtest/qe/protocol/sync/QuickExperimentLoggingParams;->b:Ljava/lang/String;

    .line 661146
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/abtest/qe/protocol/sync/QuickExperimentLoggingParams;->c:Ljava/lang/String;

    .line 661147
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/abtest/qe/protocol/sync/QuickExperimentLoggingParams;->d:Ljava/lang/String;

    .line 661148
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/abtest/qe/protocol/sync/QuickExperimentLoggingParams;->e:Ljava/lang/String;

    .line 661149
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/abtest/qe/protocol/sync/QuickExperimentLoggingParams;->f:Ljava/lang/String;

    .line 661150
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 661151
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 661152
    iget-object v0, p0, Lcom/facebook/abtest/qe/protocol/sync/QuickExperimentLoggingParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 661153
    iget-object v0, p0, Lcom/facebook/abtest/qe/protocol/sync/QuickExperimentLoggingParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 661154
    iget-object v0, p0, Lcom/facebook/abtest/qe/protocol/sync/QuickExperimentLoggingParams;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 661155
    iget-object v0, p0, Lcom/facebook/abtest/qe/protocol/sync/QuickExperimentLoggingParams;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 661156
    iget-object v0, p0, Lcom/facebook/abtest/qe/protocol/sync/QuickExperimentLoggingParams;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 661157
    return-void
.end method
