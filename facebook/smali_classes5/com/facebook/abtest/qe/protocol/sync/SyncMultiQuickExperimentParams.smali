.class public Lcom/facebook/abtest/qe/protocol/sync/SyncMultiQuickExperimentParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/abtest/qe/protocol/sync/SyncMultiQuickExperimentParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/abtest/qe/protocol/sync/SyncQuickExperimentParams;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/lang/String;

.field public final c:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 575270
    new-instance v0, LX/2Vi;

    invoke-direct {v0}, LX/2Vi;-><init>()V

    sput-object v0, Lcom/facebook/abtest/qe/protocol/sync/SyncMultiQuickExperimentParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/2VX;)V
    .locals 1

    .prologue
    .line 575271
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 575272
    iget-object v0, p1, LX/2VX;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/abtest/qe/protocol/sync/SyncMultiQuickExperimentParams;->b:Ljava/lang/String;

    .line 575273
    iget-object v0, p1, LX/2VX;->c:LX/0Px;

    iput-object v0, p0, Lcom/facebook/abtest/qe/protocol/sync/SyncMultiQuickExperimentParams;->a:LX/0Px;

    .line 575274
    iget-boolean v0, p1, LX/2VX;->d:Z

    iput-boolean v0, p0, Lcom/facebook/abtest/qe/protocol/sync/SyncMultiQuickExperimentParams;->c:Z

    .line 575275
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 575251
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 575252
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/abtest/qe/protocol/sync/SyncMultiQuickExperimentParams;->b:Ljava/lang/String;

    .line 575253
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/abtest/qe/protocol/sync/SyncMultiQuickExperimentParams;->c:Z

    .line 575254
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 575255
    :goto_1
    invoke-virtual {p1}, Landroid/os/Parcel;->dataAvail()I

    move-result v1

    if-lez v1, :cond_1

    .line 575256
    new-instance v1, Lcom/facebook/abtest/qe/protocol/sync/SyncQuickExperimentParams;

    invoke-direct {v1, p1}, Lcom/facebook/abtest/qe/protocol/sync/SyncQuickExperimentParams;-><init>(Landroid/os/Parcel;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 575257
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 575258
    :cond_1
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/abtest/qe/protocol/sync/SyncMultiQuickExperimentParams;->a:LX/0Px;

    .line 575259
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 575269
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 575276
    instance-of v1, p1, Lcom/facebook/abtest/qe/protocol/sync/SyncMultiQuickExperimentParams;

    if-nez v1, :cond_1

    .line 575277
    :cond_0
    :goto_0
    return v0

    .line 575278
    :cond_1
    check-cast p1, Lcom/facebook/abtest/qe/protocol/sync/SyncMultiQuickExperimentParams;

    .line 575279
    iget-object v1, p0, Lcom/facebook/abtest/qe/protocol/sync/SyncMultiQuickExperimentParams;->b:Ljava/lang/String;

    .line 575280
    iget-object v2, p1, Lcom/facebook/abtest/qe/protocol/sync/SyncMultiQuickExperimentParams;->b:Ljava/lang/String;

    move-object v2, v2

    .line 575281
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/abtest/qe/protocol/sync/SyncMultiQuickExperimentParams;->a:LX/0Px;

    .line 575282
    iget-object v2, p1, Lcom/facebook/abtest/qe/protocol/sync/SyncMultiQuickExperimentParams;->a:LX/0Px;

    move-object v2, v2

    .line 575283
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 575268
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/abtest/qe/protocol/sync/SyncMultiQuickExperimentParams;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/abtest/qe/protocol/sync/SyncMultiQuickExperimentParams;->a:LX/0Px;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 575267
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/facebook/abtest/qe/protocol/sync/SyncMultiQuickExperimentParams;->a:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/abtest/qe/protocol/sync/SyncMultiQuickExperimentParams;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 575260
    iget-object v0, p0, Lcom/facebook/abtest/qe/protocol/sync/SyncMultiQuickExperimentParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 575261
    iget-boolean v0, p0, Lcom/facebook/abtest/qe/protocol/sync/SyncMultiQuickExperimentParams;->c:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 575262
    iget-object v0, p0, Lcom/facebook/abtest/qe/protocol/sync/SyncMultiQuickExperimentParams;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    :goto_1
    if-ge v1, v2, :cond_1

    iget-object v0, p0, Lcom/facebook/abtest/qe/protocol/sync/SyncMultiQuickExperimentParams;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/abtest/qe/protocol/sync/SyncQuickExperimentParams;

    .line 575263
    invoke-virtual {v0, p1, p2}, Lcom/facebook/abtest/qe/protocol/sync/SyncQuickExperimentParams;->writeToParcel(Landroid/os/Parcel;I)V

    .line 575264
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    move v0, v1

    .line 575265
    goto :goto_0

    .line 575266
    :cond_1
    return-void
.end method
