.class public Lcom/facebook/abtest/qe/protocol/sync/SyncQuickExperimentParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 575231
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/facebook/abtest/qe/protocol/sync/SyncQuickExperimentParams;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 575232
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 575233
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 575234
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/abtest/qe/protocol/sync/SyncQuickExperimentParams;->a:Ljava/lang/String;

    .line 575235
    iput-object p2, p0, Lcom/facebook/abtest/qe/protocol/sync/SyncQuickExperimentParams;->b:Ljava/lang/String;

    .line 575236
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 575237
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 575238
    instance-of v1, p1, Lcom/facebook/abtest/qe/protocol/sync/SyncQuickExperimentParams;

    if-nez v1, :cond_1

    .line 575239
    :cond_0
    :goto_0
    return v0

    .line 575240
    :cond_1
    check-cast p1, Lcom/facebook/abtest/qe/protocol/sync/SyncQuickExperimentParams;

    .line 575241
    iget-object v1, p0, Lcom/facebook/abtest/qe/protocol/sync/SyncQuickExperimentParams;->a:Ljava/lang/String;

    .line 575242
    iget-object v2, p1, Lcom/facebook/abtest/qe/protocol/sync/SyncQuickExperimentParams;->a:Ljava/lang/String;

    move-object v2, v2

    .line 575243
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/abtest/qe/protocol/sync/SyncQuickExperimentParams;->b:Ljava/lang/String;

    .line 575244
    iget-object v2, p1, Lcom/facebook/abtest/qe/protocol/sync/SyncQuickExperimentParams;->b:Ljava/lang/String;

    move-object v2, v2

    .line 575245
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 575246
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/abtest/qe/protocol/sync/SyncQuickExperimentParams;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/abtest/qe/protocol/sync/SyncQuickExperimentParams;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 575247
    iget-object v0, p0, Lcom/facebook/abtest/qe/protocol/sync/SyncQuickExperimentParams;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 575248
    iget-object v0, p0, Lcom/facebook/abtest/qe/protocol/sync/SyncQuickExperimentParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 575249
    iget-object v0, p0, Lcom/facebook/abtest/qe/protocol/sync/SyncQuickExperimentParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 575250
    return-void
.end method
