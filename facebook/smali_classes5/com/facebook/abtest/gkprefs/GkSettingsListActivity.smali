.class public Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;
.super Lcom/facebook/base/activity/FbPreferenceActivity;
.source ""


# static fields
.field private static final h:LX/0Tn;


# instance fields
.field public a:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Uh;
    .annotation runtime Lcom/facebook/gk/sessionless/Sessionless;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0UW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0UW;
    .annotation runtime Lcom/facebook/gk/sessionless/Sessionless;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:Lcom/facebook/gk/store/GatekeeperWriter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:Lcom/facebook/gk/store/GatekeeperWriter;
    .annotation runtime Lcom/facebook/gk/sessionless/Sessionless;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:Ljava/lang/String;

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 660769
    sget-object v0, LX/0Tm;->c:LX/0Tn;

    const-string v1, "gk_editor_history_v2/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;->h:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 660770
    invoke-direct {p0}, Lcom/facebook/base/activity/FbPreferenceActivity;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/String;Z)Landroid/preference/Preference;
    .locals 7

    .prologue
    .line 660691
    new-instance v6, Landroid/preference/Preference;

    invoke-direct {v6, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 660692
    if-eqz p2, :cond_0

    iget-object v2, p0, Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;->c:LX/0Uh;

    .line 660693
    :goto_0
    if-eqz p2, :cond_1

    iget-object v4, p0, Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;->g:Lcom/facebook/gk/store/GatekeeperWriter;

    .line 660694
    :goto_1
    new-instance v0, LX/3y5;

    move-object v1, p0

    move-object v3, p1

    move v5, p2

    invoke-direct/range {v0 .. v5}, LX/3y5;-><init>(Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;LX/0Uh;Ljava/lang/String;Lcom/facebook/gk/store/GatekeeperWriter;Z)V

    invoke-virtual {v6, v0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 660695
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-eqz p2, :cond_2

    const-string v0, " (sessionless)"

    :goto_2
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 660696
    invoke-virtual {v2, p1}, LX/0Uh;->a(Ljava/lang/String;)LX/03R;

    move-result-object v0

    invoke-virtual {v0}, LX/03R;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 660697
    return-object v6

    .line 660698
    :cond_0
    iget-object v2, p0, Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;->b:LX/0Uh;

    goto :goto_0

    .line 660699
    :cond_1
    iget-object v4, p0, Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;->f:Lcom/facebook/gk/store/GatekeeperWriter;

    goto :goto_1

    .line 660700
    :cond_2
    const-string v0, ""

    goto :goto_2
.end method

.method private static a(Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Uh;LX/0Uh;LX/0UW;LX/0UW;Lcom/facebook/gk/store/GatekeeperWriter;Lcom/facebook/gk/store/GatekeeperWriter;)V
    .locals 0

    .prologue
    .line 660768
    iput-object p1, p0, Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object p2, p0, Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;->b:LX/0Uh;

    iput-object p3, p0, Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;->c:LX/0Uh;

    iput-object p4, p0, Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;->d:LX/0UW;

    iput-object p5, p0, Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;->e:LX/0UW;

    iput-object p6, p0, Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;->f:Lcom/facebook/gk/store/GatekeeperWriter;

    iput-object p7, p0, Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;->g:Lcom/facebook/gk/store/GatekeeperWriter;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 8

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v7

    move-object v0, p0

    check-cast v0, Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;

    invoke-static {v7}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v7}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v2

    check-cast v2, LX/0Uh;

    invoke-static {v7}, LX/0WW;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-static {v7}, LX/0UT;->a(LX/0QB;)LX/0UW;

    move-result-object v4

    check-cast v4, LX/0UW;

    invoke-static {v7}, LX/0WX;->a(LX/0QB;)LX/0UW;

    move-result-object v5

    check-cast v5, LX/0UW;

    invoke-static {v7}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v6

    check-cast v6, Lcom/facebook/gk/store/GatekeeperWriter;

    invoke-static {v7}, LX/0WW;->a(LX/0QB;)LX/0Uh;

    move-result-object v7

    check-cast v7, Lcom/facebook/gk/store/GatekeeperWriter;

    invoke-static/range {v0 .. v7}, Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;->a(Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Uh;LX/0Uh;LX/0UW;LX/0UW;Lcom/facebook/gk/store/GatekeeperWriter;Lcom/facebook/gk/store/GatekeeperWriter;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 660738
    invoke-virtual {p0}, Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/PreferenceManager;->createPreferenceScreen(Landroid/content/Context;)Landroid/preference/PreferenceScreen;

    move-result-object v4

    .line 660739
    invoke-direct {p0}, Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;->b()Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 660740
    iget-object v0, p0, Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;->i:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v3, 0x3

    if-lt v0, v3, :cond_3

    .line 660741
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-direct {v0, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 660742
    iget-object v3, p0, Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;->i:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 660743
    invoke-virtual {v4, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 660744
    iget-object v0, p0, Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;->d:LX/0UW;

    invoke-interface {v0}, LX/0UW;->b()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v3, v2

    :goto_0
    if-ge v3, v6, :cond_1

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 660745
    iget-object v7, p0, Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;->i:Ljava/lang/String;

    invoke-virtual {v0, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 660746
    invoke-direct {p0, v0, v2}, Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;->a(Ljava/lang/String;Z)Landroid/preference/Preference;

    move-result-object v0

    .line 660747
    invoke-virtual {v4, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 660748
    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 660749
    :cond_1
    iget-object v0, p0, Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;->e:LX/0UW;

    invoke-interface {v0}, LX/0UW;->b()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v3, v2

    :goto_1
    if-ge v3, v6, :cond_3

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 660750
    iget-object v7, p0, Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;->i:Ljava/lang/String;

    invoke-virtual {v0, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 660751
    invoke-direct {p0, v0, v1}, Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;->a(Ljava/lang/String;Z)Landroid/preference/Preference;

    move-result-object v0

    .line 660752
    invoke-virtual {v4, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 660753
    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 660754
    :cond_3
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-direct {v0, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 660755
    const-string v3, "Recently Edited: "

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 660756
    invoke-virtual {v4, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 660757
    iget-object v0, p0, Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 660758
    const-string v5, ":"

    invoke-virtual {v0, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 660759
    aget-object v5, v0, v2

    aget-object v0, v0, v1

    const-string v6, "1"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    :goto_3
    invoke-direct {p0, v5, v0}, Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;->a(Ljava/lang/String;Z)Landroid/preference/Preference;

    move-result-object v0

    .line 660760
    invoke-virtual {v4, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    goto :goto_2

    :cond_4
    move v0, v2

    .line 660761
    goto :goto_3

    .line 660762
    :cond_5
    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 660763
    const-string v1, "Clear"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 660764
    new-instance v1, LX/3y2;

    invoke-direct {v1, p0}, LX/3y2;-><init>(Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 660765
    invoke-virtual {v4, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 660766
    invoke-virtual {p0, v4}, Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;->setPreferenceScreen(Landroid/preference/PreferenceScreen;)V

    .line 660767
    return-void
.end method

.method private b()Landroid/preference/Preference;
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 660771
    new-instance v0, LX/4om;

    invoke-direct {v0, p0}, LX/4om;-><init>(Landroid/content/Context;)V

    .line 660772
    iget-object v1, p0, Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/4om;->setText(Ljava/lang/String;)V

    .line 660773
    const-string v1, "Search Gatekeepers"

    invoke-virtual {v0, v1}, LX/4om;->setTitle(Ljava/lang/CharSequence;)V

    .line 660774
    iget-object v1, p0, Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;->i:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lt v1, v3, :cond_0

    .line 660775
    iget-object v1, p0, Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/4om;->setSummary(Ljava/lang/CharSequence;)V

    .line 660776
    :goto_0
    invoke-virtual {v0}, LX/4om;->getEditText()Landroid/widget/EditText;

    move-result-object v1

    .line 660777
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setSelectAllOnFocus(Z)V

    .line 660778
    invoke-virtual {v1}, Landroid/widget/EditText;->setSingleLine()V

    .line 660779
    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setImeOptions(I)V

    .line 660780
    new-instance v2, LX/3y3;

    invoke-direct {v2, p0, v0}, LX/3y3;-><init>(Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;LX/4om;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 660781
    new-instance v1, LX/3y4;

    invoke-direct {v1, p0}, LX/3y4;-><init>(Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;)V

    invoke-virtual {v0, v1}, LX/4om;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 660782
    return-object v0

    .line 660783
    :cond_0
    const-string v1, "press to start searching"

    invoke-virtual {v0, v1}, LX/4om;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public static b(Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;Ljava/lang/String;Z)V
    .locals 3

    .prologue
    .line 660727
    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;->c:LX/0Uh;

    .line 660728
    :goto_0
    invoke-virtual {v0, p1}, LX/0Uh;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 660729
    :cond_0
    :goto_1
    return-void

    .line 660730
    :cond_1
    iget-object v0, p0, Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;->b:LX/0Uh;

    goto :goto_0

    .line 660731
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-eqz p2, :cond_4

    const-string v0, ":1"

    :goto_2
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 660732
    iget-object v0, p0, Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 660733
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    goto :goto_1

    .line 660734
    :cond_4
    const-string v0, ":0"

    goto :goto_2

    .line 660735
    :cond_5
    iget-object v0, p0, Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;->j:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v0, v2, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 660736
    :goto_3
    iget-object v0, p0, Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/16 v1, 0xa

    if-le v0, v1, :cond_0

    .line 660737
    iget-object v0, p0, Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;->j:Ljava/util/List;

    iget-object v1, p0, Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;->j:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_3
.end method

.method private d()V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 660715
    iget-object v0, p0, Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;->h:LX/0Tn;

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->d(LX/0Tn;)Ljava/util/Set;

    move-result-object v0

    .line 660716
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 660717
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 660718
    sget-object v5, Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;->h:LX/0Tn;

    invoke-virtual {v0, v5}, LX/0To;->b(LX/0To;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 660719
    :cond_0
    invoke-static {v1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 660720
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 660721
    iget-object v5, p0, Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;->h:LX/0Tn;

    invoke-virtual {v1, v0}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v1

    check-cast v1, LX/0Tn;

    const-string v6, ""

    invoke-interface {v5, v1, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 660722
    const-string v5, ":"

    invoke-virtual {v1, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 660723
    aget-object v5, v1, v3

    aget-object v1, v1, v2

    const-string v6, "1"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    move v1, v2

    :goto_2
    invoke-static {p0, v5, v1}, Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;->b(Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;Ljava/lang/String;Z)V

    .line 660724
    iget-object v1, p0, Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v5, Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;->h:LX/0Tn;

    invoke-virtual {v5, v0}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    invoke-interface {v1, v0}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    goto :goto_1

    :cond_1
    move v1, v3

    .line 660725
    goto :goto_2

    .line 660726
    :cond_2
    return-void
.end method

.method private e()V
    .locals 4

    .prologue
    .line 660711
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    iget-object v0, p0, Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 660712
    iget-object v0, p0, Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v3

    sget-object v0, Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;->h:LX/0Tn;

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iget-object v1, p0, Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;->j:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v3, v0, v1}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 660713
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 660714
    :cond_0
    return-void
.end method


# virtual methods
.method public final c(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 660704
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbPreferenceActivity;->c(Landroid/os/Bundle;)V

    .line 660705
    invoke-static {p0, p0}, Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 660706
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;->i:Ljava/lang/String;

    .line 660707
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;->j:Ljava/util/List;

    .line 660708
    invoke-direct {p0}, Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;->d()V

    .line 660709
    invoke-static {p0}, Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;->a$redex0(Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;)V

    .line 660710
    return-void
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x6100d270

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 660701
    invoke-direct {p0}, Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;->e()V

    .line 660702
    invoke-super {p0}, Lcom/facebook/base/activity/FbPreferenceActivity;->onStop()V

    .line 660703
    const/16 v1, 0x23

    const v2, 0x79070950

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
