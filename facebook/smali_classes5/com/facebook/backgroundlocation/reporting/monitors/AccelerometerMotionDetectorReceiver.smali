.class public Lcom/facebook/backgroundlocation/reporting/monitors/AccelerometerMotionDetectorReceiver;
.super Landroid/content/BroadcastReceiver;
.source ""

# interfaces
.implements LX/0Ya;


# instance fields
.field public a:LX/2Fo;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 571305
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/backgroundlocation/reporting/monitors/AccelerometerMotionDetectorReceiver;LX/2Fo;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0

    .prologue
    .line 571301
    iput-object p1, p0, Lcom/facebook/backgroundlocation/reporting/monitors/AccelerometerMotionDetectorReceiver;->a:LX/2Fo;

    iput-object p2, p0, Lcom/facebook/backgroundlocation/reporting/monitors/AccelerometerMotionDetectorReceiver;->b:Lcom/facebook/content/SecureContextHelper;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/backgroundlocation/reporting/monitors/AccelerometerMotionDetectorReceiver;

    invoke-static {v1}, LX/2Fo;->b(LX/0QB;)LX/2Fo;

    move-result-object v0

    check-cast v0, LX/2Fo;

    invoke-static {v1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0, v0, v1}, Lcom/facebook/backgroundlocation/reporting/monitors/AccelerometerMotionDetectorReceiver;->a(Lcom/facebook/backgroundlocation/reporting/monitors/AccelerometerMotionDetectorReceiver;LX/2Fo;Lcom/facebook/content/SecureContextHelper;)V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x26

    const v1, 0x6c8db885

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 571302
    invoke-static {p0, p1}, Lcom/facebook/backgroundlocation/reporting/monitors/AccelerometerMotionDetectorReceiver;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 571303
    iget-object v1, p0, Lcom/facebook/backgroundlocation/reporting/monitors/AccelerometerMotionDetectorReceiver;->a:LX/2Fo;

    invoke-virtual {v1}, LX/2Fo;->d()V

    .line 571304
    const/16 v1, 0x27

    const v2, -0x27d0255a

    invoke-static {p2, v3, v1, v2, v0}, LX/02F;->a(Landroid/content/Intent;IIII)V

    return-void
.end method
