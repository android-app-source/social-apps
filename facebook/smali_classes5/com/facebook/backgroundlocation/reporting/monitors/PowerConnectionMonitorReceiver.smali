.class public Lcom/facebook/backgroundlocation/reporting/monitors/PowerConnectionMonitorReceiver;
.super Landroid/content/BroadcastReceiver;
.source ""

# interfaces
.implements LX/0Ya;


# instance fields
.field public a:LX/2Fu;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 571314
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/backgroundlocation/reporting/monitors/PowerConnectionMonitorReceiver;

    invoke-static {v0}, LX/2Fu;->b(LX/0QB;)LX/2Fu;

    move-result-object v0

    check-cast v0, LX/2Fu;

    iput-object v0, p0, Lcom/facebook/backgroundlocation/reporting/monitors/PowerConnectionMonitorReceiver;->a:LX/2Fu;

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x26

    const v1, -0x315df297

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 571315
    invoke-static {p0, p1}, Lcom/facebook/backgroundlocation/reporting/monitors/PowerConnectionMonitorReceiver;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 571316
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 571317
    if-eqz v1, :cond_0

    const-string v2, "android.intent.action.ACTION_POWER_CONNECTED"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "android.intent.action.ACTION_POWER_DISCONNECTED"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 571318
    :cond_0
    const/16 v1, 0x27

    const v2, -0x2020f85d

    invoke-static {p2, v3, v1, v2, v0}, LX/02F;->a(Landroid/content/Intent;IIII)V

    .line 571319
    :goto_0
    return-void

    .line 571320
    :cond_1
    iget-object v1, p0, Lcom/facebook/backgroundlocation/reporting/monitors/PowerConnectionMonitorReceiver;->a:LX/2Fu;

    const-string v2, "power_connection"

    invoke-virtual {v1, v2}, LX/2Fu;->a(Ljava/lang/String;)V

    .line 571321
    const v1, -0x6ac71ba

    invoke-static {p2, v1, v0}, LX/02F;->a(Landroid/content/Intent;II)V

    goto :goto_0
.end method
