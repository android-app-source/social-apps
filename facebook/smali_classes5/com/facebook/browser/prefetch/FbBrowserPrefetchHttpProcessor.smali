.class public Lcom/facebook/browser/prefetch/FbBrowserPrefetchHttpProcessor;
.super LX/1Bp;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final b:Ljava/lang/String;

.field private static volatile e:Lcom/facebook/browser/prefetch/FbBrowserPrefetchHttpProcessor;


# instance fields
.field public final c:LX/1Bn;

.field private final d:Lcom/facebook/http/common/FbHttpRequestProcessor;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 637776
    const-class v0, Lcom/facebook/browser/prefetch/FbBrowserPrefetchHttpProcessor;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/browser/prefetch/FbBrowserPrefetchHttpProcessor;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/1Bn;Lcom/facebook/http/common/FbHttpRequestProcessor;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 637729
    invoke-direct {p0}, LX/1Bp;-><init>()V

    .line 637730
    iput-object p1, p0, Lcom/facebook/browser/prefetch/FbBrowserPrefetchHttpProcessor;->c:LX/1Bn;

    .line 637731
    iput-object p2, p0, Lcom/facebook/browser/prefetch/FbBrowserPrefetchHttpProcessor;->d:Lcom/facebook/http/common/FbHttpRequestProcessor;

    .line 637732
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/browser/prefetch/FbBrowserPrefetchHttpProcessor;
    .locals 5

    .prologue
    .line 637763
    sget-object v0, Lcom/facebook/browser/prefetch/FbBrowserPrefetchHttpProcessor;->e:Lcom/facebook/browser/prefetch/FbBrowserPrefetchHttpProcessor;

    if-nez v0, :cond_1

    .line 637764
    const-class v1, Lcom/facebook/browser/prefetch/FbBrowserPrefetchHttpProcessor;

    monitor-enter v1

    .line 637765
    :try_start_0
    sget-object v0, Lcom/facebook/browser/prefetch/FbBrowserPrefetchHttpProcessor;->e:Lcom/facebook/browser/prefetch/FbBrowserPrefetchHttpProcessor;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 637766
    if-eqz v2, :cond_0

    .line 637767
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 637768
    new-instance p0, Lcom/facebook/browser/prefetch/FbBrowserPrefetchHttpProcessor;

    invoke-static {v0}, LX/1Bn;->a(LX/0QB;)LX/1Bn;

    move-result-object v3

    check-cast v3, LX/1Bn;

    invoke-static {v0}, Lcom/facebook/http/common/FbHttpRequestProcessor;->a(LX/0QB;)Lcom/facebook/http/common/FbHttpRequestProcessor;

    move-result-object v4

    check-cast v4, Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-direct {p0, v3, v4}, Lcom/facebook/browser/prefetch/FbBrowserPrefetchHttpProcessor;-><init>(LX/1Bn;Lcom/facebook/http/common/FbHttpRequestProcessor;)V

    .line 637769
    move-object v0, p0

    .line 637770
    sput-object v0, Lcom/facebook/browser/prefetch/FbBrowserPrefetchHttpProcessor;->e:Lcom/facebook/browser/prefetch/FbBrowserPrefetchHttpProcessor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 637771
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 637772
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 637773
    :cond_1
    sget-object v0, Lcom/facebook/browser/prefetch/FbBrowserPrefetchHttpProcessor;->e:Lcom/facebook/browser/prefetch/FbBrowserPrefetchHttpProcessor;

    return-object v0

    .line 637774
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 637775
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/3nA;)LX/1Bx;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 637777
    check-cast p1, LX/3n9;

    .line 637778
    iget-object v0, p0, Lcom/facebook/browser/prefetch/FbBrowserPrefetchHttpProcessor;->d:Lcom/facebook/http/common/FbHttpRequestProcessor;

    iget-object v1, p1, LX/3n9;->a:LX/15D;

    invoke-virtual {v0, v1}, Lcom/facebook/http/common/FbHttpRequestProcessor;->b(LX/15D;)LX/1j2;

    move-result-object v0

    iput-object v0, p1, LX/3n9;->c:LX/1j2;

    .line 637779
    iget-object v0, p1, LX/3n9;->c:LX/1j2;

    .line 637780
    iget-object v1, v0, LX/1j2;->b:Lcom/google/common/util/concurrent/ListenableFuture;

    move-object v0, v1

    .line 637781
    iput-object v0, p1, LX/3n9;->d:Ljava/util/concurrent/Future;

    .line 637782
    iget-object v0, p1, LX/3n9;->d:Ljava/util/concurrent/Future;

    const v1, -0x665b8b19

    invoke-static {v0, v1}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Bx;

    .line 637783
    iget-object v1, p1, LX/3n9;->a:LX/15D;

    .line 637784
    iget-object v2, v1, LX/15D;->i:LX/15F;

    move-object v1, v2

    .line 637785
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 637786
    iget-object v2, v1, LX/15F;->f:Ljava/util/Map;

    move-object v2, v2

    .line 637787
    if-eqz v2, :cond_0

    .line 637788
    const-string v1, "tigon_response_size"

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, v0, LX/1Bx;->g:Ljava/lang/String;

    .line 637789
    const-string v1, "tigon_response_cmp_size"

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, v0, LX/1Bx;->h:Ljava/lang/String;

    .line 637790
    :cond_0
    return-object v0
.end method

.method public final a(LX/1Bt;Ljava/lang/String;Ljava/lang/String;ZLjava/util/Map;)LX/3nA;
    .locals 8
    .param p1    # LX/1Bt;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Bt;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "LX/3nA;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 637740
    new-instance v2, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v2, p3}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 637741
    invoke-interface {p5}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 637742
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v1, v0}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 637743
    :cond_0
    invoke-virtual {v2}, Lorg/apache/http/client/methods/HttpGet;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v0

    invoke-static {v0, v6}, Lorg/apache/http/client/params/HttpClientParams;->setRedirecting(Lorg/apache/http/params/HttpParams;Z)V

    .line 637744
    invoke-static {}, LX/15D;->newBuilder()LX/15E;

    move-result-object v0

    .line 637745
    iput-object v2, v0, LX/15E;->b:Lorg/apache/http/client/methods/HttpUriRequest;

    .line 637746
    move-object v0, v0

    .line 637747
    const-class v1, LX/1Be;

    invoke-static {v1}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v1

    .line 637748
    iput-object v1, v0, LX/15E;->d:Lcom/facebook/common/callercontext/CallerContext;

    .line 637749
    move-object v0, v0

    .line 637750
    sget-object v1, Lcom/facebook/browser/prefetch/FbBrowserPrefetchHttpProcessor;->b:Ljava/lang/String;

    .line 637751
    iput-object v1, v0, LX/15E;->c:Ljava/lang/String;

    .line 637752
    move-object v0, v0

    .line 637753
    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->NON_INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    .line 637754
    iput-object v1, v0, LX/15E;->k:Lcom/facebook/http/interfaces/RequestPriority;

    .line 637755
    move-object v7, v0

    .line 637756
    new-instance v0, LX/3n8;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p1

    invoke-direct/range {v0 .. v6}, LX/3n8;-><init>(Lcom/facebook/browser/prefetch/FbBrowserPrefetchHttpProcessor;Ljava/lang/String;Ljava/lang/String;ZLX/1Bt;B)V

    .line 637757
    iput-object v0, v7, LX/15E;->g:Lorg/apache/http/client/ResponseHandler;

    .line 637758
    move-object v0, v7

    .line 637759
    invoke-virtual {v0}, LX/15E;->a()LX/15D;

    move-result-object v0

    .line 637760
    new-instance v1, LX/3n9;

    invoke-direct {v1, v0}, LX/3n9;-><init>(LX/15D;)V

    .line 637761
    iput-object p3, v1, LX/3n9;->b:Ljava/lang/String;

    .line 637762
    return-object v1
.end method

.method public final b(LX/3nA;)V
    .locals 1

    .prologue
    .line 637733
    check-cast p1, LX/3n9;

    .line 637734
    iget-object v0, p1, LX/3n9;->d:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/3n9;->d:Ljava/util/concurrent/Future;

    invoke-interface {v0}, Ljava/util/concurrent/Future;->isDone()Z

    move-result v0

    if-nez v0, :cond_0

    .line 637735
    iget-object v0, p1, LX/3n9;->a:LX/15D;

    .line 637736
    iget-object p0, v0, LX/15D;->b:Lorg/apache/http/client/methods/HttpUriRequest;

    move-object v0, p0

    .line 637737
    invoke-interface {v0}, Lorg/apache/http/client/methods/HttpUriRequest;->abort()V

    .line 637738
    iget-object v0, p1, LX/3n9;->c:LX/1j2;

    invoke-virtual {v0}, LX/1j2;->b()V

    .line 637739
    :cond_0
    return-void
.end method
