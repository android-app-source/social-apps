.class public final Lcom/facebook/growth/sem/graphql/UserSemResTrackingMutationsModels$UserSemResTrackingMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x28535e
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/growth/sem/graphql/UserSemResTrackingMutationsModels$UserSemResTrackingMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/growth/sem/graphql/UserSemResTrackingMutationsModels$UserSemResTrackingMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 584052
    const-class v0, Lcom/facebook/growth/sem/graphql/UserSemResTrackingMutationsModels$UserSemResTrackingMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 584051
    const-class v0, Lcom/facebook/growth/sem/graphql/UserSemResTrackingMutationsModels$UserSemResTrackingMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 584049
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 584050
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 584033
    iget-object v0, p0, Lcom/facebook/growth/sem/graphql/UserSemResTrackingMutationsModels$UserSemResTrackingMutationModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/growth/sem/graphql/UserSemResTrackingMutationsModels$UserSemResTrackingMutationModel;->e:Ljava/lang/String;

    .line 584034
    iget-object v0, p0, Lcom/facebook/growth/sem/graphql/UserSemResTrackingMutationsModels$UserSemResTrackingMutationModel;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 584043
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 584044
    invoke-direct {p0}, Lcom/facebook/growth/sem/graphql/UserSemResTrackingMutationsModels$UserSemResTrackingMutationModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 584045
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 584046
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 584047
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 584048
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 584040
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 584041
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 584042
    return-object p0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 584037
    new-instance v0, Lcom/facebook/growth/sem/graphql/UserSemResTrackingMutationsModels$UserSemResTrackingMutationModel;

    invoke-direct {v0}, Lcom/facebook/growth/sem/graphql/UserSemResTrackingMutationsModels$UserSemResTrackingMutationModel;-><init>()V

    .line 584038
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 584039
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 584036
    const v0, -0x6272f2a3

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 584035
    const v0, -0x679e8dda

    return v0
.end method
