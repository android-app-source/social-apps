.class public final Lcom/facebook/growth/campaign/AdvertisingIdLogger$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/2Ag;


# direct methods
.method public constructor <init>(LX/2Ag;)V
    .locals 0

    .prologue
    .line 569560
    iput-object p1, p0, Lcom/facebook/growth/campaign/AdvertisingIdLogger$1;->a:LX/2Ag;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 569561
    iget-object v0, p0, Lcom/facebook/growth/campaign/AdvertisingIdLogger$1;->a:LX/2Ag;

    .line 569562
    iget-object v1, v0, LX/2Ag;->c:LX/0WV;

    invoke-virtual {v1}, LX/0WV;->a()Ljava/lang/String;

    move-result-object v1

    .line 569563
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 569564
    :cond_0
    :goto_0
    return-void

    .line 569565
    :cond_1
    iget-object v2, v0, LX/2Ag;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/0dA;->b:LX/0Tn;

    const-string v4, "unknown"

    invoke-interface {v2, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 569566
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "unknown"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 569567
    :cond_2
    const/4 v2, 0x1

    .line 569568
    const/4 v1, 0x0

    .line 569569
    :try_start_0
    iget-object v3, v0, LX/2Ag;->a:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/gms/ads/identifier/AdvertisingIdClient;->a(Landroid/content/Context;)Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;

    move-result-object v4

    .line 569570
    iget-object v3, v4, Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;->a:Ljava/lang/String;

    move-object v3, v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 569571
    :try_start_1
    iget-boolean v1, v4, Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;->b:Z

    move v1, v1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 569572
    if-nez v1, :cond_4

    move v1, v2

    :goto_1
    move-object v4, v3

    move v3, v1

    .line 569573
    :goto_2
    iget-object v1, v0, LX/2Ag;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Zb;

    const-string p0, "app_new_install"

    invoke-interface {v1, p0, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v1

    .line 569574
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 569575
    const-string v2, "advertising_id"

    invoke-virtual {v1, v2, v4}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 569576
    const-string v2, "tracking_enabled"

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;Z)LX/0oG;

    .line 569577
    invoke-virtual {v1}, LX/0oG;->d()V

    .line 569578
    :cond_3
    iget-object v1, v0, LX/2Ag;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/1nR;->j:LX/0Tn;

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    goto :goto_0

    .line 569579
    :cond_4
    const/4 v1, 0x0

    goto :goto_1

    :catch_0
    :goto_3
    move v3, v2

    move-object v4, v1

    goto :goto_2

    :catch_1
    move-object v1, v3

    goto :goto_3
.end method
