.class public Lcom/facebook/placetips/pulsarcore/service/PulsarService;
.super LX/0te;
.source ""


# instance fields
.field public a:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JwS;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0bW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 634911
    invoke-direct {p0}, LX/0te;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/placetips/pulsarcore/service/PulsarService;Ljava/util/concurrent/Executor;LX/0Ot;LX/0bW;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/placetips/pulsarcore/service/PulsarService;",
            "Ljava/util/concurrent/Executor;",
            "LX/0Ot",
            "<",
            "LX/JwS;",
            ">;",
            "LX/0bW;",
            ")V"
        }
    .end annotation

    .prologue
    .line 634910
    iput-object p1, p0, Lcom/facebook/placetips/pulsarcore/service/PulsarService;->a:Ljava/util/concurrent/Executor;

    iput-object p2, p0, Lcom/facebook/placetips/pulsarcore/service/PulsarService;->b:LX/0Ot;

    iput-object p3, p0, Lcom/facebook/placetips/pulsarcore/service/PulsarService;->c:LX/0bW;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/placetips/pulsarcore/service/PulsarService;

    invoke-static {v1}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    const/16 v2, 0x2f6d

    invoke-static {v1, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-static {v1}, LX/0bU;->a(LX/0QB;)LX/0bU;

    move-result-object v1

    check-cast v1, LX/0bW;

    invoke-static {p0, v0, v2, v1}, Lcom/facebook/placetips/pulsarcore/service/PulsarService;->a(Lcom/facebook/placetips/pulsarcore/service/PulsarService;Ljava/util/concurrent/Executor;LX/0Ot;LX/0bW;)V

    return-void
.end method


# virtual methods
.method public final onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 634901
    const/4 v0, 0x0

    return-object v0
.end method

.method public final onFbCreate()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x24

    const v1, -0x685c8eb7

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 634902
    invoke-super {p0}, LX/0te;->onFbCreate()V

    .line 634903
    invoke-static {p0, p0}, Lcom/facebook/placetips/pulsarcore/service/PulsarService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 634904
    iget-object v0, p0, Lcom/facebook/placetips/pulsarcore/service/PulsarService;->c:LX/0bW;

    const-string v2, "Starting PulsarService"

    invoke-interface {v0, v2}, LX/0bW;->a(Ljava/lang/String;)V

    .line 634905
    iget-object v0, p0, Lcom/facebook/placetips/pulsarcore/service/PulsarService;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JwS;

    invoke-virtual {v0}, LX/JwS;->a()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v2, LX/JxQ;

    invoke-direct {v2, p0}, LX/JxQ;-><init>(Lcom/facebook/placetips/pulsarcore/service/PulsarService;)V

    iget-object v3, p0, Lcom/facebook/placetips/pulsarcore/service/PulsarService;->a:Ljava/util/concurrent/Executor;

    invoke-static {v0, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 634906
    const/16 v0, 0x25

    const v2, 0x3fd302e5

    invoke-static {v4, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onFbDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, -0x68aac14c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 634907
    iget-object v1, p0, Lcom/facebook/placetips/pulsarcore/service/PulsarService;->c:LX/0bW;

    const-string v2, "Stopping PulsarService"

    invoke-interface {v1, v2}, LX/0bW;->a(Ljava/lang/String;)V

    .line 634908
    invoke-super {p0}, LX/0te;->onFbDestroy()V

    .line 634909
    const/16 v1, 0x25

    const v2, 0x1270cf0e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
