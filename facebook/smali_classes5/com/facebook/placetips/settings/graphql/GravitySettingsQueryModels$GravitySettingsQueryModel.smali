.class public final Lcom/facebook/placetips/settings/graphql/GravitySettingsQueryModels$GravitySettingsQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x12f1a9bd
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/placetips/settings/graphql/GravitySettingsQueryModels$GravitySettingsQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/placetips/settings/graphql/GravitySettingsQueryModels$GravitySettingsQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 632605
    const-class v0, Lcom/facebook/placetips/settings/graphql/GravitySettingsQueryModels$GravitySettingsQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 632604
    const-class v0, Lcom/facebook/placetips/settings/graphql/GravitySettingsQueryModels$GravitySettingsQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 632602
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 632603
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 632596
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 632597
    invoke-virtual {p0}, Lcom/facebook/placetips/settings/graphql/GravitySettingsQueryModels$GravitySettingsQueryModel;->a()Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 632598
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 632599
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 632600
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 632601
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 632581
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 632582
    invoke-virtual {p0}, Lcom/facebook/placetips/settings/graphql/GravitySettingsQueryModels$GravitySettingsQueryModel;->a()Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 632583
    invoke-virtual {p0}, Lcom/facebook/placetips/settings/graphql/GravitySettingsQueryModels$GravitySettingsQueryModel;->a()Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;

    .line 632584
    invoke-virtual {p0}, Lcom/facebook/placetips/settings/graphql/GravitySettingsQueryModels$GravitySettingsQueryModel;->a()Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 632585
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/placetips/settings/graphql/GravitySettingsQueryModels$GravitySettingsQueryModel;

    .line 632586
    iput-object v0, v1, Lcom/facebook/placetips/settings/graphql/GravitySettingsQueryModels$GravitySettingsQueryModel;->e:Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;

    .line 632587
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 632588
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 632594
    iget-object v0, p0, Lcom/facebook/placetips/settings/graphql/GravitySettingsQueryModels$GravitySettingsQueryModel;->e:Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;

    iput-object v0, p0, Lcom/facebook/placetips/settings/graphql/GravitySettingsQueryModels$GravitySettingsQueryModel;->e:Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;

    .line 632595
    iget-object v0, p0, Lcom/facebook/placetips/settings/graphql/GravitySettingsQueryModels$GravitySettingsQueryModel;->e:Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 632591
    new-instance v0, Lcom/facebook/placetips/settings/graphql/GravitySettingsQueryModels$GravitySettingsQueryModel;

    invoke-direct {v0}, Lcom/facebook/placetips/settings/graphql/GravitySettingsQueryModels$GravitySettingsQueryModel;-><init>()V

    .line 632592
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 632593
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 632590
    const v0, 0xef46f6d

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 632589
    const v0, -0x6747e1ce

    return v0
.end method
