.class public Lcom/facebook/friending/components/partdefinition/FriendRequestRespondedPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/JZj;",
        "Ljava/lang/Void;",
        "LX/1Pq;",
        "Landroid/widget/LinearLayout;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Landroid/widget/LinearLayout;",
            ">;"
        }
    .end annotation
.end field

.field private static e:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/friending/components/partdefinition/FriendRequestFriendsCenterIntentPartDefinition;

.field public final c:Landroid/content/res/Resources;

.field private final d:Lcom/facebook/multirow/parts/TextPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 598617
    const v0, 0x7f0311f3

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/friending/components/partdefinition/FriendRequestRespondedPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/friending/components/partdefinition/FriendRequestFriendsCenterIntentPartDefinition;Landroid/content/res/Resources;Lcom/facebook/multirow/parts/TextPartDefinition;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 598589
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 598590
    iput-object p1, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestRespondedPartDefinition;->b:Lcom/facebook/friending/components/partdefinition/FriendRequestFriendsCenterIntentPartDefinition;

    .line 598591
    iput-object p2, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestRespondedPartDefinition;->c:Landroid/content/res/Resources;

    .line 598592
    iput-object p3, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestRespondedPartDefinition;->d:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 598593
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/friending/components/partdefinition/FriendRequestRespondedPartDefinition;
    .locals 6

    .prologue
    .line 598606
    const-class v1, Lcom/facebook/friending/components/partdefinition/FriendRequestRespondedPartDefinition;

    monitor-enter v1

    .line 598607
    :try_start_0
    sget-object v0, Lcom/facebook/friending/components/partdefinition/FriendRequestRespondedPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 598608
    sput-object v2, Lcom/facebook/friending/components/partdefinition/FriendRequestRespondedPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 598609
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 598610
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 598611
    new-instance p0, Lcom/facebook/friending/components/partdefinition/FriendRequestRespondedPartDefinition;

    invoke-static {v0}, Lcom/facebook/friending/components/partdefinition/FriendRequestFriendsCenterIntentPartDefinition;->a(LX/0QB;)Lcom/facebook/friending/components/partdefinition/FriendRequestFriendsCenterIntentPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/friending/components/partdefinition/FriendRequestFriendsCenterIntentPartDefinition;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/friending/components/partdefinition/FriendRequestRespondedPartDefinition;-><init>(Lcom/facebook/friending/components/partdefinition/FriendRequestFriendsCenterIntentPartDefinition;Landroid/content/res/Resources;Lcom/facebook/multirow/parts/TextPartDefinition;)V

    .line 598612
    move-object v0, p0

    .line 598613
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 598614
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/friending/components/partdefinition/FriendRequestRespondedPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 598615
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 598616
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Landroid/widget/LinearLayout;",
            ">;"
        }
    .end annotation

    .prologue
    .line 598605
    sget-object v0, Lcom/facebook/friending/components/partdefinition/FriendRequestRespondedPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 598598
    check-cast p2, LX/JZj;

    .line 598599
    iget-object v2, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestRespondedPartDefinition;->b:Lcom/facebook/friending/components/partdefinition/FriendRequestFriendsCenterIntentPartDefinition;

    new-instance v3, LX/JZh;

    iget-object v4, p2, LX/JZj;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    iget v0, p2, LX/JZj;->b:I

    if-nez v0, :cond_0

    sget-object v0, LX/5P0;->SUGGESTIONS:LX/5P0;

    :goto_0
    iget v1, p2, LX/JZj;->b:I

    if-nez v1, :cond_1

    sget-object v1, LX/Cfc;->VIEW_FRIEND_CENTER_SUGGESTIONS_TAP:LX/Cfc;

    :goto_1
    invoke-direct {v3, v4, v0, v1}, LX/JZh;-><init>(Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/5P0;LX/Cfc;)V

    invoke-interface {p1, v2, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 598600
    const v0, 0x7f0d2a2a

    iget-object v1, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestRespondedPartDefinition;->d:Lcom/facebook/multirow/parts/TextPartDefinition;

    iget v2, p2, LX/JZj;->b:I

    .line 598601
    if-nez v2, :cond_2

    iget-object v3, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestRespondedPartDefinition;->c:Landroid/content/res/Resources;

    const v4, 0x7f080f79

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    :goto_2
    move-object v2, v3

    .line 598602
    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 598603
    const/4 v0, 0x0

    return-object v0

    .line 598604
    :cond_0
    sget-object v0, LX/5P0;->REQUESTS:LX/5P0;

    goto :goto_0

    :cond_1
    sget-object v1, LX/Cfc;->VIEW_FRIEND_REQUESTS_TAP:LX/Cfc;

    goto :goto_1

    :cond_2
    iget-object v3, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestRespondedPartDefinition;->c:Landroid/content/res/Resources;

    const v4, 0x7f0f0059

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 p2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    aput-object p3, v5, p2

    invoke-virtual {v3, v4, v2, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_2
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 598594
    check-cast p1, LX/JZj;

    .line 598595
    iget-object v0, p1, LX/JZj;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 598596
    iget-object p0, v0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v0, p0

    .line 598597
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p1, LX/JZj;->b:I

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
