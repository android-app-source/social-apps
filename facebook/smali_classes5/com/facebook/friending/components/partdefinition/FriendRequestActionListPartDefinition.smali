.class public Lcom/facebook/friending/components/partdefinition/FriendRequestActionListPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "Ljava/lang/Void;",
        "LX/2kk;",
        "Landroid/widget/FrameLayout;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Landroid/widget/FrameLayout;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Landroid/widget/FrameLayout;",
            ">;"
        }
    .end annotation
.end field

.field private static g:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/friending/components/partdefinition/FriendRequestActionListButtonPartDefinition;

.field public final d:LX/0xW;

.field private final e:Lcom/facebook/multirow/parts/ViewColorPartDefinition;

.field private final f:Lcom/facebook/friending/components/partdefinition/SeeRequestsActionListButtonPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 598447
    const v0, 0x7f0311f0

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/friending/components/partdefinition/FriendRequestActionListPartDefinition;->a:LX/1Cz;

    .line 598448
    const v0, 0x7f0311f2

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/friending/components/partdefinition/FriendRequestActionListPartDefinition;->b:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/friending/components/partdefinition/FriendRequestActionListButtonPartDefinition;LX/0xW;Lcom/facebook/multirow/parts/ViewColorPartDefinition;Lcom/facebook/friending/components/partdefinition/SeeRequestsActionListButtonPartDefinition;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 598449
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 598450
    iput-object p1, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestActionListPartDefinition;->c:Lcom/facebook/friending/components/partdefinition/FriendRequestActionListButtonPartDefinition;

    .line 598451
    iput-object p2, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestActionListPartDefinition;->d:LX/0xW;

    .line 598452
    iput-object p3, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestActionListPartDefinition;->e:Lcom/facebook/multirow/parts/ViewColorPartDefinition;

    .line 598453
    iput-object p4, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestActionListPartDefinition;->f:Lcom/facebook/friending/components/partdefinition/SeeRequestsActionListButtonPartDefinition;

    .line 598454
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/friending/components/partdefinition/FriendRequestActionListPartDefinition;
    .locals 7

    .prologue
    .line 598455
    const-class v1, Lcom/facebook/friending/components/partdefinition/FriendRequestActionListPartDefinition;

    monitor-enter v1

    .line 598456
    :try_start_0
    sget-object v0, Lcom/facebook/friending/components/partdefinition/FriendRequestActionListPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 598457
    sput-object v2, Lcom/facebook/friending/components/partdefinition/FriendRequestActionListPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 598458
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 598459
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 598460
    new-instance p0, Lcom/facebook/friending/components/partdefinition/FriendRequestActionListPartDefinition;

    invoke-static {v0}, Lcom/facebook/friending/components/partdefinition/FriendRequestActionListButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/friending/components/partdefinition/FriendRequestActionListButtonPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/friending/components/partdefinition/FriendRequestActionListButtonPartDefinition;

    invoke-static {v0}, LX/0xW;->a(LX/0QB;)LX/0xW;

    move-result-object v4

    check-cast v4, LX/0xW;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ViewColorPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ViewColorPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/ViewColorPartDefinition;

    invoke-static {v0}, Lcom/facebook/friending/components/partdefinition/SeeRequestsActionListButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/friending/components/partdefinition/SeeRequestsActionListButtonPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/friending/components/partdefinition/SeeRequestsActionListButtonPartDefinition;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/friending/components/partdefinition/FriendRequestActionListPartDefinition;-><init>(Lcom/facebook/friending/components/partdefinition/FriendRequestActionListButtonPartDefinition;LX/0xW;Lcom/facebook/multirow/parts/ViewColorPartDefinition;Lcom/facebook/friending/components/partdefinition/SeeRequestsActionListButtonPartDefinition;)V

    .line 598461
    move-object v0, p0

    .line 598462
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 598463
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/friending/components/partdefinition/FriendRequestActionListPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 598464
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 598465
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Landroid/widget/FrameLayout;",
            ">;"
        }
    .end annotation

    .prologue
    .line 598466
    iget-object v0, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestActionListPartDefinition;->d:LX/0xW;

    invoke-virtual {v0}, LX/0xW;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/facebook/friending/components/partdefinition/FriendRequestActionListPartDefinition;->b:LX/1Cz;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/facebook/friending/components/partdefinition/FriendRequestActionListPartDefinition;->a:LX/1Cz;

    goto :goto_0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 598467
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 598468
    invoke-static {p2}, LX/E1q;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/2nq;

    move-result-object v1

    .line 598469
    invoke-interface {v1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aF()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->SEEN_AND_READ:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    .line 598470
    :goto_0
    iget-object v2, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v2, v2

    .line 598471
    invoke-interface {v2}, LX/9uc;->aD()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel;->a()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;

    move-result-object v2

    .line 598472
    iget-object v3, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestActionListPartDefinition;->e:Lcom/facebook/multirow/parts/ViewColorPartDefinition;

    if-eqz v0, :cond_1

    const v0, 0x7f0a004f

    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v3, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 598473
    const v0, 0x7f0d1285

    iget-object v3, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestActionListPartDefinition;->c:Lcom/facebook/friending/components/partdefinition/FriendRequestActionListButtonPartDefinition;

    new-instance v4, LX/JZe;

    invoke-interface {v1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v5

    sget-object v6, LX/2na;->CONFIRM:LX/2na;

    invoke-direct {v4, v5, p2, v2, v6}, LX/JZe;-><init>(Ljava/lang/String;Lcom/facebook/reaction/common/ReactionUnitComponentNode;Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;LX/2na;)V

    invoke-interface {p1, v0, v3, v4}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 598474
    iget-object v0, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestActionListPartDefinition;->d:LX/0xW;

    invoke-virtual {v0}, LX/0xW;->w()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 598475
    const v0, 0x7f0d1286

    iget-object v3, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestActionListPartDefinition;->c:Lcom/facebook/friending/components/partdefinition/FriendRequestActionListButtonPartDefinition;

    new-instance v4, LX/JZe;

    invoke-interface {v1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v1

    sget-object v5, LX/2na;->REJECT:LX/2na;

    invoke-direct {v4, v1, p2, v2, v5}, LX/JZe;-><init>(Ljava/lang/String;Lcom/facebook/reaction/common/ReactionUnitComponentNode;Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;LX/2na;)V

    invoke-interface {p1, v0, v3, v4}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 598476
    :goto_2
    const/4 v0, 0x0

    return-object v0

    .line 598477
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 598478
    :cond_1
    const v0, 0x7f0a0480

    goto :goto_1

    .line 598479
    :cond_2
    const v0, 0x7f0d1286

    iget-object v1, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestActionListPartDefinition;->f:Lcom/facebook/friending/components/partdefinition/SeeRequestsActionListButtonPartDefinition;

    invoke-interface {p1, v0, v1, p2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    goto :goto_2
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x3a7157ff    # -4565.0005f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 598480
    check-cast p4, Landroid/widget/FrameLayout;

    const/4 v2, 0x0

    .line 598481
    iget-object v1, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestActionListPartDefinition;->d:LX/0xW;

    invoke-virtual {v1}, LX/0xW;->t()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 598482
    invoke-virtual {p4, v2}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 598483
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    .line 598484
    :goto_0
    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result p1

    if-ge v2, p1, :cond_0

    .line 598485
    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 598486
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 598487
    :cond_0
    invoke-virtual {v1}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 598488
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move p1, v2

    :goto_1
    if-ltz p1, :cond_1

    .line 598489
    invoke-virtual {p2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 598490
    add-int/lit8 v2, p1, -0x1

    move p1, v2

    goto :goto_1

    .line 598491
    :cond_1
    const/16 v1, 0x1f

    const v2, 0x664a1bf7

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 598492
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 598493
    invoke-static {p1}, LX/E1q;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/2nq;

    move-result-object v0

    .line 598494
    iget-object v1, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v1

    .line 598495
    invoke-interface {v1}, LX/9uc;->aD()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel;

    move-result-object v1

    .line 598496
    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel;->a()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel;->a()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel;->a()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
