.class public Lcom/facebook/friending/components/partdefinition/FriendRequestPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/2kk;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "Ljava/lang/Void;",
        "TE;",
        "Landroid/widget/LinearLayout;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Landroid/widget/LinearLayout;",
            ">;"
        }
    .end annotation
.end field

.field private static i:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final c:Lcom/facebook/friending/components/partdefinition/FriendRequestActionListButtonPartDefinition;

.field public final d:LX/0xW;

.field private final e:Lcom/facebook/multirow/parts/TextPartDefinition;

.field public final f:LX/17W;

.field private final g:Lcom/facebook/multirow/parts/VisibilityPartDefinition;

.field private final h:Lcom/facebook/friending/components/partdefinition/SeeRequestsActionListButtonPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 598533
    const v0, 0x7f0311ef

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/friending/components/partdefinition/FriendRequestPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/friending/components/partdefinition/FriendRequestActionListButtonPartDefinition;Lcom/facebook/friending/components/partdefinition/SeeRequestsActionListButtonPartDefinition;LX/0xW;Lcom/facebook/multirow/parts/TextPartDefinition;LX/17W;Lcom/facebook/multirow/parts/VisibilityPartDefinition;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 598534
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 598535
    iput-object p1, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 598536
    iput-object p2, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestPartDefinition;->c:Lcom/facebook/friending/components/partdefinition/FriendRequestActionListButtonPartDefinition;

    .line 598537
    iput-object p4, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestPartDefinition;->d:LX/0xW;

    .line 598538
    iput-object p5, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestPartDefinition;->e:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 598539
    iput-object p6, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestPartDefinition;->f:LX/17W;

    .line 598540
    iput-object p7, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestPartDefinition;->g:Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    .line 598541
    iput-object p3, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestPartDefinition;->h:Lcom/facebook/friending/components/partdefinition/SeeRequestsActionListButtonPartDefinition;

    .line 598542
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/friending/components/partdefinition/FriendRequestPartDefinition;
    .locals 11

    .prologue
    .line 598543
    const-class v1, Lcom/facebook/friending/components/partdefinition/FriendRequestPartDefinition;

    monitor-enter v1

    .line 598544
    :try_start_0
    sget-object v0, Lcom/facebook/friending/components/partdefinition/FriendRequestPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 598545
    sput-object v2, Lcom/facebook/friending/components/partdefinition/FriendRequestPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 598546
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 598547
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 598548
    new-instance v3, Lcom/facebook/friending/components/partdefinition/FriendRequestPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/friending/components/partdefinition/FriendRequestActionListButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/friending/components/partdefinition/FriendRequestActionListButtonPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/friending/components/partdefinition/FriendRequestActionListButtonPartDefinition;

    invoke-static {v0}, Lcom/facebook/friending/components/partdefinition/SeeRequestsActionListButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/friending/components/partdefinition/SeeRequestsActionListButtonPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/friending/components/partdefinition/SeeRequestsActionListButtonPartDefinition;

    invoke-static {v0}, LX/0xW;->a(LX/0QB;)LX/0xW;

    move-result-object v7

    check-cast v7, LX/0xW;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v9

    check-cast v9, LX/17W;

    invoke-static {v0}, Lcom/facebook/multirow/parts/VisibilityPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    move-result-object v10

    check-cast v10, Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    invoke-direct/range {v3 .. v10}, Lcom/facebook/friending/components/partdefinition/FriendRequestPartDefinition;-><init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/friending/components/partdefinition/FriendRequestActionListButtonPartDefinition;Lcom/facebook/friending/components/partdefinition/SeeRequestsActionListButtonPartDefinition;LX/0xW;Lcom/facebook/multirow/parts/TextPartDefinition;LX/17W;Lcom/facebook/multirow/parts/VisibilityPartDefinition;)V

    .line 598549
    move-object v0, v3

    .line 598550
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 598551
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/friending/components/partdefinition/FriendRequestPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 598552
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 598553
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Landroid/widget/LinearLayout;",
            ">;"
        }
    .end annotation

    .prologue
    .line 598554
    sget-object v0, Lcom/facebook/friending/components/partdefinition/FriendRequestPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 598555
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/1Pq;

    const/4 v1, 0x0

    .line 598556
    invoke-static {p2}, LX/E1q;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/2nq;

    move-result-object v2

    .line 598557
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 598558
    invoke-interface {v0}, LX/9uc;->aD()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel;->a()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;

    move-result-object v3

    .line 598559
    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;->e()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel$SocialContextModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;->e()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel$SocialContextModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel$SocialContextModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 598560
    :goto_0
    iget-object v4, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 598561
    new-instance v5, LX/JZi;

    invoke-direct {v5, p0, p2, p3}, LX/JZi;-><init>(Lcom/facebook/friending/components/partdefinition/FriendRequestPartDefinition;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/1Pq;)V

    move-object v5, v5

    .line 598562
    invoke-interface {p1, v4, v5}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 598563
    const v4, 0x7f0d2a27

    iget-object v5, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestPartDefinition;->e:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;->d()Ljava/lang/String;

    move-result-object v6

    invoke-interface {p1, v4, v5, v6}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 598564
    const v4, 0x7f0d2a28

    iget-object v5, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestPartDefinition;->e:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-interface {p1, v4, v5, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 598565
    const v4, 0x7f0d2a28

    iget-object v5, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestPartDefinition;->g:Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0x8

    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v4, v5, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 598566
    const v0, 0x7f0d1285

    iget-object v4, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestPartDefinition;->c:Lcom/facebook/friending/components/partdefinition/FriendRequestActionListButtonPartDefinition;

    new-instance v5, LX/JZe;

    invoke-interface {v2}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v6

    sget-object v7, LX/2na;->CONFIRM:LX/2na;

    invoke-direct {v5, v6, p2, v3, v7}, LX/JZe;-><init>(Ljava/lang/String;Lcom/facebook/reaction/common/ReactionUnitComponentNode;Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;LX/2na;)V

    invoke-interface {p1, v0, v4, v5}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 598567
    iget-object v0, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestPartDefinition;->d:LX/0xW;

    invoke-virtual {v0}, LX/0xW;->w()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 598568
    const v0, 0x7f0d1286

    iget-object v4, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestPartDefinition;->c:Lcom/facebook/friending/components/partdefinition/FriendRequestActionListButtonPartDefinition;

    new-instance v5, LX/JZe;

    invoke-interface {v2}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v2

    sget-object v6, LX/2na;->REJECT:LX/2na;

    invoke-direct {v5, v2, p2, v3, v6}, LX/JZe;-><init>(Ljava/lang/String;Lcom/facebook/reaction/common/ReactionUnitComponentNode;Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;LX/2na;)V

    invoke-interface {p1, v0, v4, v5}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 598569
    :goto_2
    return-object v1

    :cond_0
    move-object v0, v1

    .line 598570
    goto :goto_0

    .line 598571
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 598572
    :cond_2
    const v0, 0x7f0d1286

    iget-object v2, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestPartDefinition;->h:Lcom/facebook/friending/components/partdefinition/SeeRequestsActionListButtonPartDefinition;

    invoke-interface {p1, v0, v2, p2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    goto :goto_2
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x4409e914

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 598573
    check-cast p4, Landroid/widget/LinearLayout;

    .line 598574
    iget-object v1, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestPartDefinition;->d:LX/0xW;

    invoke-virtual {v1}, LX/0xW;->t()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 598575
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 598576
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p4}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 598577
    invoke-virtual {p4, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 598578
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 598579
    :cond_0
    invoke-virtual {p4}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 598580
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    move v2, v1

    :goto_1
    if-ltz v2, :cond_1

    .line 598581
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {p4, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 598582
    add-int/lit8 v1, v2, -0x1

    move v2, v1

    goto :goto_1

    .line 598583
    :cond_1
    const/16 v1, 0x1f

    const v2, 0x43b1b5e2

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 598584
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 598585
    invoke-static {p1}, LX/E1q;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/2nq;

    move-result-object v0

    .line 598586
    iget-object v1, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v1

    .line 598587
    invoke-interface {v1}, LX/9uc;->aD()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel;

    move-result-object v1

    .line 598588
    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel;->a()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel;->a()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel;->a()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionFriendRequestComponentFragmentModel$FriendingPossibilityModel$RequesterModel;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
