.class public Lcom/facebook/friending/components/partdefinition/FriendRequestActionListRespondedPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/JZf;",
        "Ljava/lang/Void;",
        "LX/2kk;",
        "Landroid/widget/FrameLayout;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Landroid/widget/FrameLayout;",
            ">;"
        }
    .end annotation
.end field

.field private static f:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/friending/components/partdefinition/FriendRequestFriendsCenterIntentPartDefinition;

.field public final c:Landroid/content/res/Resources;

.field private final d:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final e:Lcom/facebook/multirow/parts/ViewColorPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 598521
    const v0, 0x7f0311f1

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/friending/components/partdefinition/FriendRequestActionListRespondedPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/friending/components/partdefinition/FriendRequestFriendsCenterIntentPartDefinition;Landroid/content/res/Resources;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/ViewColorPartDefinition;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 598515
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 598516
    iput-object p1, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestActionListRespondedPartDefinition;->b:Lcom/facebook/friending/components/partdefinition/FriendRequestFriendsCenterIntentPartDefinition;

    .line 598517
    iput-object p2, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestActionListRespondedPartDefinition;->c:Landroid/content/res/Resources;

    .line 598518
    iput-object p3, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestActionListRespondedPartDefinition;->d:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 598519
    iput-object p4, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestActionListRespondedPartDefinition;->e:Lcom/facebook/multirow/parts/ViewColorPartDefinition;

    .line 598520
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/friending/components/partdefinition/FriendRequestActionListRespondedPartDefinition;
    .locals 7

    .prologue
    .line 598522
    const-class v1, Lcom/facebook/friending/components/partdefinition/FriendRequestActionListRespondedPartDefinition;

    monitor-enter v1

    .line 598523
    :try_start_0
    sget-object v0, Lcom/facebook/friending/components/partdefinition/FriendRequestActionListRespondedPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 598524
    sput-object v2, Lcom/facebook/friending/components/partdefinition/FriendRequestActionListRespondedPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 598525
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 598526
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 598527
    new-instance p0, Lcom/facebook/friending/components/partdefinition/FriendRequestActionListRespondedPartDefinition;

    invoke-static {v0}, Lcom/facebook/friending/components/partdefinition/FriendRequestFriendsCenterIntentPartDefinition;->a(LX/0QB;)Lcom/facebook/friending/components/partdefinition/FriendRequestFriendsCenterIntentPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/friending/components/partdefinition/FriendRequestFriendsCenterIntentPartDefinition;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ViewColorPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ViewColorPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/ViewColorPartDefinition;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/friending/components/partdefinition/FriendRequestActionListRespondedPartDefinition;-><init>(Lcom/facebook/friending/components/partdefinition/FriendRequestFriendsCenterIntentPartDefinition;Landroid/content/res/Resources;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/ViewColorPartDefinition;)V

    .line 598528
    move-object v0, p0

    .line 598529
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 598530
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/friending/components/partdefinition/FriendRequestActionListRespondedPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 598531
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 598532
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Landroid/widget/FrameLayout;",
            ">;"
        }
    .end annotation

    .prologue
    .line 598514
    sget-object v0, Lcom/facebook/friending/components/partdefinition/FriendRequestActionListRespondedPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 598499
    check-cast p2, LX/JZf;

    .line 598500
    iget-object v0, p2, LX/JZf;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-static {v0}, LX/E1q;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/2nq;

    move-result-object v0

    .line 598501
    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aF()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->SEEN_AND_READ:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    .line 598502
    :goto_0
    iget-object v1, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestActionListRespondedPartDefinition;->e:Lcom/facebook/multirow/parts/ViewColorPartDefinition;

    if-eqz v0, :cond_1

    const v0, 0x7f0a004f

    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 598503
    iget-object v2, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestActionListRespondedPartDefinition;->b:Lcom/facebook/friending/components/partdefinition/FriendRequestFriendsCenterIntentPartDefinition;

    new-instance v3, LX/JZh;

    iget-object v4, p2, LX/JZf;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    iget v0, p2, LX/JZf;->b:I

    if-nez v0, :cond_2

    sget-object v0, LX/5P0;->SUGGESTIONS:LX/5P0;

    :goto_2
    iget v1, p2, LX/JZf;->b:I

    if-nez v1, :cond_3

    sget-object v1, LX/Cfc;->VIEW_FRIEND_CENTER_SUGGESTIONS_TAP:LX/Cfc;

    :goto_3
    invoke-direct {v3, v4, v0, v1}, LX/JZh;-><init>(Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/5P0;LX/Cfc;)V

    invoke-interface {p1, v2, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 598504
    const v0, 0x7f0d2a29

    iget-object v1, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestActionListRespondedPartDefinition;->d:Lcom/facebook/multirow/parts/TextPartDefinition;

    iget-object v2, p2, LX/JZf;->c:Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;

    .line 598505
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;->DELETED:Lcom/facebook/graphql/enums/GraphQLReactionFriendRequestState;

    if-ne v2, v3, :cond_4

    iget-object v3, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestActionListRespondedPartDefinition;->c:Landroid/content/res/Resources;

    const v4, 0x7f080f88

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    :goto_4
    move-object v2, v3

    .line 598506
    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 598507
    const v0, 0x7f0d2a2a

    iget-object v1, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestActionListRespondedPartDefinition;->d:Lcom/facebook/multirow/parts/TextPartDefinition;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "\u00a0\u00a0\u2022\u00a0\u00a0"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p2, LX/JZf;->b:I

    .line 598508
    if-nez v3, :cond_5

    iget-object v4, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestActionListRespondedPartDefinition;->c:Landroid/content/res/Resources;

    const v5, 0x7f080f7a

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    :goto_5
    move-object v3, v4

    .line 598509
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 598510
    const/4 v0, 0x0

    return-object v0

    .line 598511
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 598512
    :cond_1
    const v0, 0x7f0a0480

    goto :goto_1

    .line 598513
    :cond_2
    sget-object v0, LX/5P0;->REQUESTS:LX/5P0;

    goto :goto_2

    :cond_3
    sget-object v1, LX/Cfc;->VIEW_FRIEND_REQUESTS_TAP:LX/Cfc;

    goto :goto_3

    :cond_4
    iget-object v3, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestActionListRespondedPartDefinition;->c:Landroid/content/res/Resources;

    const v4, 0x7f080f87

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_4

    :cond_5
    iget-object v4, p0, Lcom/facebook/friending/components/partdefinition/FriendRequestActionListRespondedPartDefinition;->c:Landroid/content/res/Resources;

    const v5, 0x7f0f005a

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 p2, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    aput-object p3, v6, p2

    invoke-virtual {v4, v5, v3, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_5
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 598497
    check-cast p1, LX/JZf;

    .line 598498
    iget-object v0, p1, LX/JZf;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    if-eqz v0, :cond_0

    iget v0, p1, LX/JZf;->b:I

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
