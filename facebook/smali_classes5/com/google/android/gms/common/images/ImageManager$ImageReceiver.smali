.class public final Lcom/google/android/gms/common/images/ImageManager$ImageReceiver;
.super Landroid/os/ResultReceiver;
.source ""


# annotations
.annotation build Lcom/google/android/gms/common/annotation/KeepName;
.end annotation


# instance fields
.field public final synthetic a:LX/4sl;

.field private final b:Landroid/net/Uri;

.field public final c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/4sn;",
            ">;"
        }
    .end annotation
.end field


# virtual methods
.method public final onReceiveResult(ILandroid/os/Bundle;)V
    .locals 5

    const-string v0, "com.google.android.gms.extra.fileDescriptor"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/os/ParcelFileDescriptor;

    iget-object v1, p0, Lcom/google/android/gms/common/images/ImageManager$ImageReceiver;->a:LX/4sl;

    iget-object v1, v1, LX/4sl;->e:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/google/android/gms/common/images/ImageManager$zzc;

    iget-object v3, p0, Lcom/google/android/gms/common/images/ImageManager$ImageReceiver;->a:LX/4sl;

    iget-object v4, p0, Lcom/google/android/gms/common/images/ImageManager$ImageReceiver;->b:Landroid/net/Uri;

    invoke-direct {v2, v3, v4, v0}, Lcom/google/android/gms/common/images/ImageManager$zzc;-><init>(LX/4sl;Landroid/net/Uri;Landroid/os/ParcelFileDescriptor;)V

    const v0, -0x75824c24

    invoke-static {v1, v2, v0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    return-void
.end method
