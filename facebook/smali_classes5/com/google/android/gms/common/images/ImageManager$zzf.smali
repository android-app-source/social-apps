.class public final Lcom/google/android/gms/common/images/ImageManager$zzf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/4sl;

.field public final b:Landroid/net/Uri;

.field public final c:Landroid/graphics/Bitmap;

.field private final d:Ljava/util/concurrent/CountDownLatch;

.field private e:Z


# direct methods
.method public constructor <init>(LX/4sl;Landroid/net/Uri;Landroid/graphics/Bitmap;ZLjava/util/concurrent/CountDownLatch;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/common/images/ImageManager$zzf;->a:LX/4sl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/gms/common/images/ImageManager$zzf;->b:Landroid/net/Uri;

    iput-object p3, p0, Lcom/google/android/gms/common/images/ImageManager$zzf;->c:Landroid/graphics/Bitmap;

    iput-boolean p4, p0, Lcom/google/android/gms/common/images/ImageManager$zzf;->e:Z

    iput-object p5, p0, Lcom/google/android/gms/common/images/ImageManager$zzf;->d:Ljava/util/concurrent/CountDownLatch;

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 14

    const/4 v2, 0x0

    const-string v0, "OnBitmapLoadedRunnable must be executed in the main thread"

    invoke-static {v0}, LX/4t3;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/common/images/ImageManager$zzf;->c:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/common/images/ImageManager$zzf;->a:LX/4sl;

    iget-object v0, v0, LX/4sl;->f:Lcom/google/android/gms/common/images/ImageManager$zzb;

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/gms/common/images/ImageManager$zzf;->e:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/common/images/ImageManager$zzf;->a:LX/4sl;

    iget-object v0, v0, LX/4sl;->f:Lcom/google/android/gms/common/images/ImageManager$zzb;

    invoke-virtual {v0}, LX/0aq;->a()V

    invoke-static {}, Ljava/lang/System;->gc()V

    iput-boolean v2, p0, Lcom/google/android/gms/common/images/ImageManager$zzf;->e:Z

    iget-object v0, p0, Lcom/google/android/gms/common/images/ImageManager$zzf;->a:LX/4sl;

    iget-object v0, v0, LX/4sl;->d:Landroid/os/Handler;

    const v1, -0x50c1cb78

    invoke-static {v0, p0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    :goto_1
    return-void

    :cond_0
    move v1, v2

    goto :goto_0

    :cond_1
    if-eqz v1, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/common/images/ImageManager$zzf;->a:LX/4sl;

    iget-object v0, v0, LX/4sl;->f:Lcom/google/android/gms/common/images/ImageManager$zzb;

    new-instance v2, LX/4sm;

    iget-object v3, p0, Lcom/google/android/gms/common/images/ImageManager$zzf;->b:Landroid/net/Uri;

    invoke-direct {v2, v3}, LX/4sm;-><init>(Landroid/net/Uri;)V

    iget-object v3, p0, Lcom/google/android/gms/common/images/ImageManager$zzf;->c:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v2, v3}, LX/0aq;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/common/images/ImageManager$zzf;->a:LX/4sl;

    iget-object v0, v0, LX/4sl;->i:Ljava/util/Map;

    iget-object v2, p0, Lcom/google/android/gms/common/images/ImageManager$zzf;->b:Landroid/net/Uri;

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/images/ImageManager$ImageReceiver;

    if-eqz v0, :cond_6

    const/4 v6, 0x0

    iget-object v7, v0, Lcom/google/android/gms/common/images/ImageManager$ImageReceiver;->c:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v8

    move v5, v6

    :goto_2
    if-ge v5, v8, :cond_6

    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/4sn;

    if-eqz v1, :cond_4

    iget-object v9, p0, Lcom/google/android/gms/common/images/ImageManager$zzf;->a:LX/4sl;

    iget-object v9, v9, LX/4sl;->c:Landroid/content/Context;

    iget-object v10, p0, Lcom/google/android/gms/common/images/ImageManager$zzf;->c:Landroid/graphics/Bitmap;

    invoke-static {v10}, LX/4t3;->a(Ljava/lang/Object;)V

    new-instance v11, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    invoke-direct {v11, v12, v10}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    const/4 v12, 0x0

    const/4 v13, 0x1

    invoke-virtual {v4, v11, v12, v13}, LX/4sn;->a(Landroid/graphics/drawable/Drawable;ZZ)V

    :goto_3
    instance-of v9, v4, LX/4so;

    if-nez v9, :cond_3

    iget-object v9, p0, Lcom/google/android/gms/common/images/ImageManager$zzf;->a:LX/4sl;

    iget-object v9, v9, LX/4sl;->h:Ljava/util/Map;

    invoke-interface {v9, v4}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_2

    :cond_4
    iget-object v9, p0, Lcom/google/android/gms/common/images/ImageManager$zzf;->a:LX/4sl;

    iget-object v9, v9, LX/4sl;->j:Ljava/util/Map;

    iget-object v10, p0, Lcom/google/android/gms/common/images/ImageManager$zzf;->b:Landroid/net/Uri;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-interface {v9, v10, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v9, p0, Lcom/google/android/gms/common/images/ImageManager$zzf;->a:LX/4sl;

    iget-object v9, v9, LX/4sl;->c:Landroid/content/Context;

    const/4 v13, 0x0

    const/4 v11, 0x0

    iget v12, v4, LX/4sn;->b:I

    if-eqz v12, :cond_5

    iget v11, v4, LX/4sn;->b:I

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    invoke-virtual {v12, v11}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v12

    move-object v11, v12

    :cond_5
    invoke-virtual {v4, v11, v13, v13}, LX/4sn;->a(Landroid/graphics/drawable/Drawable;ZZ)V

    goto :goto_3

    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/common/images/ImageManager$zzf;->d:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    sget-object v1, LX/4sl;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/4sl;->b:Ljava/util/HashSet;

    iget-object v2, p0, Lcom/google/android/gms/common/images/ImageManager$zzf;->b:Landroid/net/Uri;

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    monitor-exit v1

    goto/16 :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
