.class public final Lcom/google/android/gms/internal/zzqx$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/2NW;

.field public final synthetic b:LX/4v2;


# direct methods
.method public constructor <init>(LX/4v2;LX/2NW;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/internal/zzqx$1;->b:LX/4v2;

    iput-object p2, p0, Lcom/google/android/gms/internal/zzqx$1;->a:LX/2NW;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    const/4 v4, 0x0

    :try_start_0
    sget-object v0, LX/2wf;->a:Ljava/lang/ThreadLocal;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/zzqx$1;->b:LX/4v2;

    iget-object v0, v0, LX/4v2;->a:LX/4sS;

    invoke-virtual {v0}, LX/4sS;->a()LX/2wg;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/internal/zzqx$1;->b:LX/4v2;

    iget-object v1, v1, LX/4v2;->h:LX/4v1;

    iget-object v2, p0, Lcom/google/android/gms/internal/zzqx$1;->b:LX/4v2;

    iget-object v2, v2, LX/4v2;->h:LX/4v1;

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v0}, LX/4v1;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/4v1;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sget-object v0, LX/2wf;->a:Ljava/lang/ThreadLocal;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/gms/internal/zzqx$1;->a:LX/2NW;

    invoke-static {v1}, LX/4v2;->b(LX/2NW;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/zzqx$1;->b:LX/4v2;

    iget-object v0, v0, LX/4v2;->g:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2wX;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/internal/zzqx$1;->b:LX/4v2;

    invoke-virtual {v0, v1}, LX/2wX;->b(LX/4v2;)V

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    :try_start_1
    iget-object v1, p0, Lcom/google/android/gms/internal/zzqx$1;->b:LX/4v2;

    iget-object v1, v1, LX/4v2;->h:LX/4v1;

    iget-object v2, p0, Lcom/google/android/gms/internal/zzqx$1;->b:LX/4v2;

    iget-object v2, v2, LX/4v2;->h:LX/4v1;

    const/4 v3, 0x1

    invoke-virtual {v2, v3, v0}, LX/4v1;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/4v1;->sendMessage(Landroid/os/Message;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    sget-object v0, LX/2wf;->a:Ljava/lang/ThreadLocal;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/gms/internal/zzqx$1;->a:LX/2NW;

    invoke-static {v1}, LX/4v2;->b(LX/2NW;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/zzqx$1;->b:LX/4v2;

    iget-object v0, v0, LX/4v2;->g:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2wX;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/internal/zzqx$1;->b:LX/4v2;

    invoke-virtual {v0, v1}, LX/2wX;->b(LX/4v2;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    move-object v1, v0

    sget-object v0, LX/2wf;->a:Ljava/lang/ThreadLocal;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    iget-object v2, p0, Lcom/google/android/gms/internal/zzqx$1;->a:LX/2NW;

    invoke-static {v2}, LX/4v2;->b(LX/2NW;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/zzqx$1;->b:LX/4v2;

    iget-object v0, v0, LX/4v2;->g:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2wX;

    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/google/android/gms/internal/zzqx$1;->b:LX/4v2;

    invoke-virtual {v0, v2}, LX/2wX;->b(LX/4v2;)V

    :cond_1
    throw v1
.end method
