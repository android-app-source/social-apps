.class public final Lcom/google/android/gms/internal/zzpn$zza;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/4uO;


# direct methods
.method public constructor <init>(LX/4uO;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/internal/zzpn$zza;->a:LX/4uO;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 9
    .annotation build Landroid/support/annotation/MainThread;
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/internal/zzpn$zza;->a:LX/4uO;

    iget-boolean v0, v0, LX/4uO;->b:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/internal/zzpn$zza;->a:LX/4uO;

    iget-object v0, v0, LX/4uO;->e:Lcom/google/android/gms/common/ConnectionResult;

    invoke-virtual {v0}, Lcom/google/android/gms/common/ConnectionResult;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/internal/zzpn$zza;->a:LX/4uO;

    iget-object v0, v0, LX/4uN;->a:LX/4ur;

    iget-object v1, p0, Lcom/google/android/gms/internal/zzpn$zza;->a:LX/4uO;

    invoke-virtual {v1}, LX/4uN;->a()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/internal/zzpn$zza;->a:LX/4uO;

    iget-object v2, v2, LX/4uO;->e:Lcom/google/android/gms/common/ConnectionResult;

    iget-object v3, v2, Lcom/google/android/gms/common/ConnectionResult;->d:Landroid/app/PendingIntent;

    move-object v2, v3

    iget-object v3, p0, Lcom/google/android/gms/internal/zzpn$zza;->a:LX/4uO;

    iget v3, v3, LX/4uO;->f:I

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/gms/common/api/GoogleApiActivity;->a(Landroid/content/Context;Landroid/app/PendingIntent;IZ)Landroid/content/Intent;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/4ur;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/internal/zzpn$zza;->a:LX/4uO;

    iget-object v0, v0, LX/4uO;->d:LX/1vX;

    iget-object v1, p0, Lcom/google/android/gms/internal/zzpn$zza;->a:LX/4uO;

    iget-object v1, v1, LX/4uO;->e:Lcom/google/android/gms/common/ConnectionResult;

    iget v2, v1, Lcom/google/android/gms/common/ConnectionResult;->c:I

    move v1, v2

    invoke-virtual {v0, v1}, LX/1od;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/internal/zzpn$zza;->a:LX/4uO;

    iget-object v0, v0, LX/4uO;->d:LX/1vX;

    iget-object v1, p0, Lcom/google/android/gms/internal/zzpn$zza;->a:LX/4uO;

    invoke-virtual {v1}, LX/4uN;->a()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/internal/zzpn$zza;->a:LX/4uO;

    iget-object v2, v2, LX/4uN;->a:LX/4ur;

    iget-object v3, p0, Lcom/google/android/gms/internal/zzpn$zza;->a:LX/4uO;

    iget-object v3, v3, LX/4uO;->e:Lcom/google/android/gms/common/ConnectionResult;

    iget v4, v3, Lcom/google/android/gms/common/ConnectionResult;->c:I

    move v3, v4

    const/4 v4, 0x2

    iget-object v5, p0, Lcom/google/android/gms/internal/zzpn$zza;->a:LX/4uO;

    const-string v6, "d"

    invoke-virtual {v0, v1, v3, v6}, LX/1od;->a(Landroid/content/Context;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    new-instance p0, LX/4t5;

    invoke-direct {p0, v6, v2, v4}, LX/4t5;-><init>(Landroid/content/Intent;LX/4ur;I)V

    move-object v6, p0

    invoke-static {v1, v3, v6, v5}, LX/1vX;->a(Landroid/content/Context;ILX/3Kg;Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/Dialog;

    move-result-object v6

    if-nez v6, :cond_4

    :goto_1
    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/internal/zzpn$zza;->a:LX/4uO;

    iget-object v0, v0, LX/4uO;->e:Lcom/google/android/gms/common/ConnectionResult;

    iget v1, v0, Lcom/google/android/gms/common/ConnectionResult;->c:I

    move v0, v1

    const/16 v1, 0x12

    if-ne v0, v1, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/internal/zzpn$zza;->a:LX/4uO;

    invoke-virtual {v1}, LX/4uN;->a()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/internal/zzpn$zza;->a:LX/4uO;

    const/4 v8, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    new-instance v0, Landroid/widget/ProgressBar;

    const v3, 0x101007a

    invoke-direct {v0, v1, v8, v3}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-virtual {v0, v6}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    invoke-virtual {v0, v7}, Landroid/widget/ProgressBar;->setVisibility(I)V

    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    invoke-static {v1}, LX/1oW;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0800bf

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v0, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    const v0, 0x7f0800c0

    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const-string v0, ""

    invoke-virtual {v3, v0, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    const-string v3, "GooglePlayServicesUpdatingDialog"

    invoke-static {v1, v0, v3, v2}, LX/1vX;->a(Landroid/app/Activity;Landroid/app/Dialog;Ljava/lang/String;Landroid/content/DialogInterface$OnCancelListener;)V

    move-object v0, v0

    iget-object v2, p0, Lcom/google/android/gms/internal/zzpn$zza;->a:LX/4uO;

    invoke-virtual {v2}, LX/4uN;->a()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    new-instance v3, LX/4uS;

    invoke-direct {v3, p0, v0}, LX/4uS;-><init>(Lcom/google/android/gms/internal/zzpn$zza;Landroid/app/Dialog;)V

    invoke-static {v2, v3}, LX/1vX;->a(Landroid/content/Context;LX/4uR;)LX/4un;

    goto/16 :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/internal/zzpn$zza;->a:LX/4uO;

    iget-object v1, p0, Lcom/google/android/gms/internal/zzpn$zza;->a:LX/4uO;

    iget-object v1, v1, LX/4uO;->e:Lcom/google/android/gms/common/ConnectionResult;

    iget-object v2, p0, Lcom/google/android/gms/internal/zzpn$zza;->a:LX/4uO;

    iget v2, v2, LX/4uO;->f:I

    invoke-virtual {v0, v1, v2}, LX/4uO;->a(Lcom/google/android/gms/common/ConnectionResult;I)V

    goto/16 :goto_0

    :cond_4
    const-string p0, "GooglePlayServicesErrorDialog"

    invoke-static {v1, v6, p0, v5}, LX/1vX;->a(Landroid/app/Activity;Landroid/app/Dialog;Ljava/lang/String;Landroid/content/DialogInterface$OnCancelListener;)V

    goto :goto_1
.end method
