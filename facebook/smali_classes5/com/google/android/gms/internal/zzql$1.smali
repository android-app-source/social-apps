.class public final Lcom/google/android/gms/internal/zzql$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/4uN;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/4us;


# direct methods
.method public constructor <init>(LX/4us;LX/4uN;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/internal/zzql$1;->c:LX/4us;

    iput-object p2, p0, Lcom/google/android/gms/internal/zzql$1;->a:LX/4uN;

    iput-object p3, p0, Lcom/google/android/gms/internal/zzql$1;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/internal/zzql$1;->c:LX/4us;

    iget v0, v0, LX/4us;->c:I

    if-lez v0, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/internal/zzql$1;->a:LX/4uN;

    iget-object v0, p0, Lcom/google/android/gms/internal/zzql$1;->c:LX/4us;

    iget-object v0, v0, LX/4us;->d:Landroid/os/Bundle;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/internal/zzql$1;->c:LX/4us;

    iget-object v0, v0, LX/4us;->d:Landroid/os/Bundle;

    iget-object v2, p0, Lcom/google/android/gms/internal/zzql$1;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, LX/4uN;->a(Landroid/os/Bundle;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/internal/zzql$1;->c:LX/4us;

    iget v0, v0, LX/4us;->c:I

    const/4 v1, 0x2

    if-lt v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/internal/zzql$1;->a:LX/4uN;

    invoke-virtual {v0}, LX/4uN;->b()V

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/internal/zzql$1;->c:LX/4us;

    iget v0, v0, LX/4us;->c:I

    const/4 v1, 0x3

    if-lt v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/internal/zzql$1;->a:LX/4uN;

    invoke-virtual {v0}, LX/4uN;->c()V

    :cond_2
    return-void

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method
