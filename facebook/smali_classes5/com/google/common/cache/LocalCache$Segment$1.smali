.class public final Lcom/google/common/cache/LocalCache$Segment$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/lang/Object;

.field public final synthetic b:I

.field public final synthetic c:LX/0SO;

.field public final synthetic d:Lcom/google/common/util/concurrent/ListenableFuture;

.field public final synthetic e:LX/0Qx;


# direct methods
.method public constructor <init>(LX/0Qx;Ljava/lang/Object;ILX/0SO;Lcom/google/common/util/concurrent/ListenableFuture;)V
    .locals 0

    .prologue
    .line 820057
    iput-object p1, p0, Lcom/google/common/cache/LocalCache$Segment$1;->e:LX/0Qx;

    iput-object p2, p0, Lcom/google/common/cache/LocalCache$Segment$1;->a:Ljava/lang/Object;

    iput p3, p0, Lcom/google/common/cache/LocalCache$Segment$1;->b:I

    iput-object p4, p0, Lcom/google/common/cache/LocalCache$Segment$1;->c:LX/0SO;

    iput-object p5, p0, Lcom/google/common/cache/LocalCache$Segment$1;->d:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 820058
    :try_start_0
    iget-object v0, p0, Lcom/google/common/cache/LocalCache$Segment$1;->e:LX/0Qx;

    iget-object v1, p0, Lcom/google/common/cache/LocalCache$Segment$1;->a:Ljava/lang/Object;

    iget v2, p0, Lcom/google/common/cache/LocalCache$Segment$1;->b:I

    iget-object v3, p0, Lcom/google/common/cache/LocalCache$Segment$1;->c:LX/0SO;

    iget-object v4, p0, Lcom/google/common/cache/LocalCache$Segment$1;->d:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/0Qx;->a(Ljava/lang/Object;ILX/0SO;Lcom/google/common/util/concurrent/ListenableFuture;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 820059
    :goto_0
    return-void

    .line 820060
    :catch_0
    move-exception v0

    .line 820061
    sget-object v1, LX/0Qd;->a:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v3, "Exception thrown during refresh"

    invoke-virtual {v1, v2, v3, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 820062
    iget-object v1, p0, Lcom/google/common/cache/LocalCache$Segment$1;->c:LX/0SO;

    invoke-virtual {v1, v0}, LX/0SO;->a(Ljava/lang/Throwable;)Z

    goto :goto_0
.end method
