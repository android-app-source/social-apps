.class public abstract Lcom/google/common/util/concurrent/Futures$AbstractCatchingFuture;
.super LX/0SP;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        "X:",
        "Ljava/lang/Throwable;",
        "F:",
        "Ljava/lang/Object;",
        ">",
        "LX/0SP",
        "<TV;>;",
        "Ljava/lang/Runnable;"
    }
.end annotation


# instance fields
.field public a:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<+TV;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TX;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TF;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/common/util/concurrent/ListenableFuture;Ljava/lang/Class;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<+TV;>;",
            "Ljava/lang/Class",
            "<TX;>;TF;)V"
        }
    .end annotation

    .prologue
    .line 825920
    invoke-direct {p0}, LX/0SP;-><init>()V

    .line 825921
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/util/concurrent/ListenableFuture;

    iput-object v0, p0, Lcom/google/common/util/concurrent/Futures$AbstractCatchingFuture;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 825922
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    iput-object v0, p0, Lcom/google/common/util/concurrent/Futures$AbstractCatchingFuture;->b:Ljava/lang/Class;

    .line 825923
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/util/concurrent/Futures$AbstractCatchingFuture;->c:Ljava/lang/Object;

    .line 825924
    return-void
.end method


# virtual methods
.method public abstract a(Ljava/lang/Object;Ljava/lang/Throwable;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TF;TX;)V"
        }
    .end annotation
.end method

.method public final done()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 825915
    iget-object v0, p0, Lcom/google/common/util/concurrent/Futures$AbstractCatchingFuture;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-virtual {p0, v0}, LX/0SQ;->maybePropagateCancellation(Ljava/util/concurrent/Future;)V

    .line 825916
    iput-object v1, p0, Lcom/google/common/util/concurrent/Futures$AbstractCatchingFuture;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 825917
    iput-object v1, p0, Lcom/google/common/util/concurrent/Futures$AbstractCatchingFuture;->b:Ljava/lang/Class;

    .line 825918
    iput-object v1, p0, Lcom/google/common/util/concurrent/Futures$AbstractCatchingFuture;->c:Ljava/lang/Object;

    .line 825919
    return-void
.end method

.method public final run()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 825896
    iget-object v4, p0, Lcom/google/common/util/concurrent/Futures$AbstractCatchingFuture;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 825897
    iget-object v5, p0, Lcom/google/common/util/concurrent/Futures$AbstractCatchingFuture;->b:Ljava/lang/Class;

    .line 825898
    iget-object v6, p0, Lcom/google/common/util/concurrent/Futures$AbstractCatchingFuture;->c:Ljava/lang/Object;

    .line 825899
    if-nez v4, :cond_0

    move v3, v0

    :goto_0
    if-nez v5, :cond_1

    move v2, v0

    :goto_1
    or-int/2addr v2, v3

    if-nez v6, :cond_2

    :goto_2
    or-int/2addr v0, v2

    invoke-virtual {p0}, LX/0SQ;->isCancelled()Z

    move-result v1

    or-int/2addr v0, v1

    if-eqz v0, :cond_3

    .line 825900
    :goto_3
    return-void

    :cond_0
    move v3, v1

    .line 825901
    goto :goto_0

    :cond_1
    move v2, v1

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2

    .line 825902
    :cond_3
    iput-object v7, p0, Lcom/google/common/util/concurrent/Futures$AbstractCatchingFuture;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 825903
    iput-object v7, p0, Lcom/google/common/util/concurrent/Futures$AbstractCatchingFuture;->b:Ljava/lang/Class;

    .line 825904
    iput-object v7, p0, Lcom/google/common/util/concurrent/Futures$AbstractCatchingFuture;->c:Ljava/lang/Object;

    .line 825905
    :try_start_0
    invoke-static {v4}, LX/0Sa;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0SQ;->set(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_3

    .line 825906
    :catch_0
    move-exception v0

    .line 825907
    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    .line 825908
    :goto_4
    :try_start_1
    invoke-virtual {v5, v0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v1

    move v1, v1

    .line 825909
    if-eqz v1, :cond_4

    .line 825910
    invoke-virtual {p0, v6, v0}, Lcom/google/common/util/concurrent/Futures$AbstractCatchingFuture;->a(Ljava/lang/Object;Ljava/lang/Throwable;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_3

    .line 825911
    :catch_1
    move-exception v0

    .line 825912
    invoke-virtual {p0, v0}, LX/0SQ;->setException(Ljava/lang/Throwable;)Z

    goto :goto_3

    .line 825913
    :cond_4
    :try_start_2
    invoke-virtual {p0, v0}, LX/0SQ;->setException(Ljava/lang/Throwable;)Z
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_3

    .line 825914
    :catch_2
    move-exception v0

    goto :goto_4
.end method
