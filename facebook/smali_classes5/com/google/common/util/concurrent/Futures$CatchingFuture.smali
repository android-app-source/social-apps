.class public final Lcom/google/common/util/concurrent/Futures$CatchingFuture;
.super Lcom/google/common/util/concurrent/Futures$AbstractCatchingFuture;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        "X:",
        "Ljava/lang/Throwable;",
        ">",
        "Lcom/google/common/util/concurrent/Futures$AbstractCatchingFuture",
        "<TV;TX;",
        "LX/0QK",
        "<-TX;+TV;>;>;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/google/common/util/concurrent/ListenableFuture;Ljava/lang/Class;LX/0QK;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<+TV;>;",
            "Ljava/lang/Class",
            "<TX;>;",
            "LX/0QK",
            "<-TX;+TV;>;)V"
        }
    .end annotation

    .prologue
    .line 825925
    invoke-direct {p0, p1, p2, p3}, Lcom/google/common/util/concurrent/Futures$AbstractCatchingFuture;-><init>(Lcom/google/common/util/concurrent/ListenableFuture;Ljava/lang/Class;Ljava/lang/Object;)V

    .line 825926
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 825927
    check-cast p1, LX/0QK;

    .line 825928
    invoke-interface {p1, p2}, LX/0QK;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 825929
    invoke-virtual {p0, v0}, LX/0SQ;->set(Ljava/lang/Object;)Z

    .line 825930
    return-void
.end method
