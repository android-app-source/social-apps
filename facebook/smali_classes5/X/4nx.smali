.class public final LX/4nx;
.super Landroid/os/Handler;
.source ""


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/widget/CyclingTextSwitcher;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/widget/CyclingTextSwitcher;)V
    .locals 1

    .prologue
    .line 808010
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 808011
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/4nx;->a:Ljava/lang/ref/WeakReference;

    .line 808012
    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    .line 808013
    iget-object v0, p0, LX/4nx;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CyclingTextSwitcher;

    .line 808014
    if-eqz v0, :cond_0

    .line 808015
    iget v1, p1, Landroid/os/Message;->arg1:I

    .line 808016
    iget v2, p1, Landroid/os/Message;->arg2:I

    .line 808017
    iget-boolean v3, v0, Lcom/facebook/widget/CyclingTextSwitcher;->a:Z

    if-eqz v3, :cond_1

    .line 808018
    iget-object v3, v0, Lcom/facebook/widget/CyclingTextSwitcher;->b:[Ljava/lang/CharSequence;

    aget-object v3, v3, v1

    invoke-virtual {v0, v3}, Lcom/facebook/widget/CyclingTextSwitcher;->setText(Ljava/lang/CharSequence;)V

    .line 808019
    :goto_0
    add-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Lcom/facebook/widget/CyclingTextSwitcher;->a(Lcom/facebook/widget/CyclingTextSwitcher;I)I

    move-result v1

    int-to-long v2, v2

    .line 808020
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/widget/CyclingTextSwitcher;->a$redex0(Lcom/facebook/widget/CyclingTextSwitcher;IJ)V

    .line 808021
    :cond_0
    return-void

    .line 808022
    :cond_1
    iget-object v3, v0, Lcom/facebook/widget/CyclingTextSwitcher;->b:[Ljava/lang/CharSequence;

    aget-object v3, v3, v1

    invoke-virtual {v0, v3}, Lcom/facebook/widget/CyclingTextSwitcher;->setCurrentText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
