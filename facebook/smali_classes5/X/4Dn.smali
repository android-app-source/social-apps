.class public final LX/4Dn;
.super LX/2vO;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 679081
    invoke-direct {p0}, LX/2vO;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Boolean;)LX/4Dn;
    .locals 1

    .prologue
    .line 679079
    const-string v0, "need_invitable_contacts"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 679080
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/4Dn;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ContactUploadSource;
        .end annotation
    .end param

    .prologue
    .line 679077
    const-string v0, "source"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 679078
    return-object p0
.end method

.method public final a(Ljava/util/List;)LX/4Dn;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/4Dj;",
            ">;)",
            "LX/4Dn;"
        }
    .end annotation

    .prologue
    .line 679075
    const-string v0, "contacts"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 679076
    return-object p0
.end method

.method public final b(Ljava/lang/Boolean;)LX/4Dn;
    .locals 1

    .prologue
    .line 679073
    const-string v0, "need_friendable_contacts"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 679074
    return-object p0
.end method

.method public final b(Ljava/lang/String;)LX/4Dn;
    .locals 1

    .prologue
    .line 679061
    const-string v0, "phone_id"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 679062
    return-object p0
.end method

.method public final c(Ljava/lang/String;)LX/4Dn;
    .locals 1

    .prologue
    .line 679071
    const-string v0, "sim_country_code"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 679072
    return-object p0
.end method

.method public final d(Ljava/lang/String;)LX/4Dn;
    .locals 1

    .prologue
    .line 679069
    const-string v0, "network_country_code"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 679070
    return-object p0
.end method

.method public final e(Ljava/lang/String;)LX/4Dn;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ContactUploadSessionType;
        .end annotation
    .end param

    .prologue
    .line 679067
    const-string v0, "contact_upload_session_type"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 679068
    return-object p0
.end method

.method public final f(Ljava/lang/String;)LX/4Dn;
    .locals 1

    .prologue
    .line 679065
    const-string v0, "minimal_base_hash"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 679066
    return-object p0
.end method

.method public final g(Ljava/lang/String;)LX/4Dn;
    .locals 1

    .prologue
    .line 679063
    const-string v0, "extended_base_hash"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 679064
    return-object p0
.end method
