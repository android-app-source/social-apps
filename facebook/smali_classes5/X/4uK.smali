.class public final LX/4uK;
.super LX/4uJ;
.source ""


# instance fields
.field public final c:LX/2we;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2we",
            "<+",
            "LX/2NW;",
            "LX/2wK;",
            ">;"
        }
    .end annotation
.end field


# virtual methods
.method public final a(LX/2wK;)V
    .locals 1

    iget-object v0, p0, LX/4uK;->c:LX/2we;

    invoke-virtual {v0, p1}, LX/2we;->a(LX/2wK;)V

    return-void
.end method

.method public final a(Landroid/util/SparseArray;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "LX/2wd;",
            ">;)V"
        }
    .end annotation

    iget v0, p0, LX/4uJ;->a:I

    invoke-virtual {p1, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2wd;

    if-eqz v0, :cond_0

    iget-object v1, p0, LX/4uK;->c:LX/2we;

    invoke-virtual {v0, v1}, LX/2wd;->a(LX/2we;)V

    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 1
    .param p1    # Lcom/google/android/gms/common/api/Status;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, LX/4uK;->c:LX/2we;

    invoke-virtual {v0, p1}, LX/2we;->b(Lcom/google/android/gms/common/api/Status;)V

    return-void
.end method

.method public final a()Z
    .locals 1

    iget-object v0, p0, LX/4uK;->c:LX/2we;

    invoke-virtual {v0}, LX/2wf;->i()Z

    move-result v0

    return v0
.end method
