.class public LX/3f2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/26t;


# instance fields
.field public a:LX/2U1;

.field private b:LX/0SG;


# direct methods
.method public constructor <init>(LX/2U1;LX/0SG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 621206
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 621207
    iput-object p1, p0, LX/3f2;->a:LX/2U1;

    .line 621208
    iput-object p2, p0, LX/3f2;->b:LX/0SG;

    .line 621209
    return-void
.end method

.method public static a(LX/3f2;Ljava/util/Iterator;Ljava/util/Map;J)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Iterator",
            "<+",
            "Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLInterfaces$AdminedPagesPrefetchQuery$$AdminedPages$$Nodes$;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;J)V"
        }
    .end annotation

    .prologue
    .line 621210
    new-instance v4, Ljava/util/LinkedList;

    invoke-direct {v4}, Ljava/util/LinkedList;-><init>()V

    .line 621211
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 621212
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 621213
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;

    .line 621214
    iget-object v1, p0, LX/3f2;->a:LX/2U1;

    invoke-virtual {v0}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/2U2;->a(Ljava/lang/Object;)LX/8FT;

    move-result-object v1

    if-nez v1, :cond_0

    const-wide/16 v2, -0x1

    .line 621215
    :goto_1
    cmp-long v1, v2, p3

    if-lez v1, :cond_1

    .line 621216
    invoke-virtual {v0}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->d()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 621217
    :cond_0
    iget-object v1, p0, LX/3f2;->a:LX/2U1;

    invoke-virtual {v0}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/2U2;->a(Ljava/lang/Object;)LX/8FT;

    move-result-object v1

    .line 621218
    iget-wide v7, v1, LX/8FT;->b:J

    move-wide v2, v7

    .line 621219
    goto :goto_1

    .line 621220
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->d()Ljava/lang/String;

    move-result-object v1

    .line 621221
    invoke-interface {p2, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 621222
    new-instance v2, LX/8Dk;

    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-direct {v2, v0, v1}, LX/8Dk;-><init>(Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;Ljava/lang/String;)V

    move-object v0, v2

    .line 621223
    :goto_2
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 621224
    :cond_2
    new-instance v1, LX/8Dk;

    invoke-direct {v1, v0}, LX/8Dk;-><init>(Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;)V

    move-object v0, v1

    goto :goto_2

    .line 621225
    :cond_3
    iget-object v0, p0, LX/3f2;->a:LX/2U1;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-virtual {v0, v1, p3, p4}, LX/2U1;->a(Ljava/util/Iterator;J)V

    .line 621226
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 621227
    iget-object v2, p0, LX/3f2;->a:LX/2U1;

    invoke-virtual {v2, v0}, LX/2U2;->b(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    .line 621228
    :cond_4
    iget-object v0, p0, LX/3f2;->a:LX/2U1;

    iget-object v1, p0, LX/3f2;->b:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LX/2U1;->a(J)V

    .line 621229
    return-void
.end method

.method public static b(LX/0QB;)LX/3f2;
    .locals 3

    .prologue
    .line 621230
    new-instance v2, LX/3f2;

    invoke-static {p0}, LX/2U1;->a(LX/0QB;)LX/2U1;

    move-result-object v0

    check-cast v0, LX/2U1;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v1

    check-cast v1, LX/0SG;

    invoke-direct {v2, v0, v1}, LX/3f2;-><init>(LX/2U1;LX/0SG;)V

    .line 621231
    return-object v2
.end method


# virtual methods
.method public final handleOperation(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 12

    .prologue
    .line 621232
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 621233
    const-string v1, "admined_pages_prefetch"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 621234
    const/4 v7, 0x0

    .line 621235
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v5

    .line 621236
    invoke-virtual {v5}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableListNullOk()Ljava/util/ArrayList;

    move-result-object v3

    .line 621237
    if-nez v3, :cond_1

    move-object v2, v5

    .line 621238
    :goto_0
    move-object v0, v2

    .line 621239
    :goto_1
    return-object v0

    :cond_0
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_1

    .line 621240
    :cond_1
    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchMethod$Result;

    .line 621241
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/pages/adminedpages/protocol/PagesAccessTokenPrefetchMethod$Result;

    .line 621242
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchMethod$Result;->a()LX/0am;

    move-result-object v4

    invoke-virtual {v4}, LX/0am;->isPresent()Z

    move-result v4

    if-nez v4, :cond_3

    :cond_2
    move-object v2, v5

    .line 621243
    goto :goto_0

    .line 621244
    :cond_3
    invoke-virtual {v2}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchMethod$Result;->a()LX/0am;

    move-result-object v4

    invoke-virtual {v4}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel;

    .line 621245
    invoke-virtual {v4}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel;->b()I

    move-result v6

    .line 621246
    iget-wide v8, v2, Lcom/facebook/fbservice/results/BaseResult;->clientTimeMs:J

    iget-wide v10, v3, Lcom/facebook/fbservice/results/BaseResult;->clientTimeMs:J

    invoke-static {v8, v9, v10, v11}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v8

    .line 621247
    iget-object v2, p0, LX/3f2;->a:LX/2U1;

    invoke-virtual {v2, v6}, LX/2U1;->a(I)V

    .line 621248
    iget-object v2, v3, Lcom/facebook/pages/adminedpages/protocol/PagesAccessTokenPrefetchMethod$Result;->a:Ljava/util/Map;

    move-object v3, v2

    .line 621249
    invoke-virtual {v4}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    .line 621250
    iget-object v6, p0, LX/3f2;->a:LX/2U1;

    invoke-virtual {v4}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;

    invoke-virtual {v2}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, LX/2U1;->a(Ljava/lang/String;)V

    .line 621251
    :cond_4
    invoke-virtual {v4}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel;->a()LX/0Px;

    move-result-object v2

    invoke-static {v2}, LX/0R9;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    .line 621252
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    invoke-static {p0, v2, v3, v8, v9}, LX/3f2;->a(LX/3f2;Ljava/util/Iterator;Ljava/util/Map;J)V

    move-object v2, v5

    .line 621253
    goto :goto_0
.end method
