.class public LX/3U3;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/0tX;


# direct methods
.method public constructor <init>(LX/0tX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 585335
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 585336
    iput-object p1, p0, LX/3U3;->a:LX/0tX;

    .line 585337
    return-void
.end method

.method public static a(LX/0QB;)LX/3U3;
    .locals 4

    .prologue
    .line 585338
    const-class v1, LX/3U3;

    monitor-enter v1

    .line 585339
    :try_start_0
    sget-object v0, LX/3U3;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 585340
    sput-object v2, LX/3U3;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 585341
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 585342
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 585343
    new-instance p0, LX/3U3;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-direct {p0, v3}, LX/3U3;-><init>(LX/0tX;)V

    .line 585344
    move-object v0, p0

    .line 585345
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 585346
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/3U3;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 585347
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 585348
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
