.class public LX/4Lf;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 684451
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    .line 684452
    const-wide/16 v8, 0x0

    .line 684453
    const/4 v7, 0x0

    .line 684454
    const/4 v6, 0x0

    .line 684455
    const/4 v5, 0x0

    .line 684456
    const/4 v4, 0x0

    .line 684457
    const/4 v3, 0x0

    .line 684458
    const/4 v2, 0x0

    .line 684459
    const/4 v1, 0x0

    .line 684460
    const/4 v0, 0x0

    .line 684461
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->START_OBJECT:LX/15z;

    if-eq v10, v11, :cond_b

    .line 684462
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 684463
    const/4 v0, 0x0

    .line 684464
    :goto_0
    return v0

    .line 684465
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v0

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v0, v4, :cond_7

    .line 684466
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v0

    .line 684467
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 684468
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_0

    if-eqz v0, :cond_0

    .line 684469
    const-string v4, "amount"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 684470
    const/4 v0, 0x1

    .line 684471
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v2

    move v1, v0

    goto :goto_1

    .line 684472
    :cond_1
    const-string v4, "amount_with_offset"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 684473
    const/4 v0, 0x1

    .line 684474
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v4

    move v7, v0

    move v12, v4

    goto :goto_1

    .line 684475
    :cond_2
    const-string v4, "currency"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 684476
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    move v11, v0

    goto :goto_1

    .line 684477
    :cond_3
    const-string v4, "formatted"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 684478
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    move v10, v0

    goto :goto_1

    .line 684479
    :cond_4
    const-string v4, "offset"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 684480
    const/4 v0, 0x1

    .line 684481
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v4

    move v6, v0

    move v9, v4

    goto :goto_1

    .line 684482
    :cond_5
    const-string v4, "offset_amount"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 684483
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    move v8, v0

    goto :goto_1

    .line 684484
    :cond_6
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 684485
    :cond_7
    const/4 v0, 0x6

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 684486
    if-eqz v1, :cond_8

    .line 684487
    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 684488
    :cond_8
    if-eqz v7, :cond_9

    .line 684489
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v12, v1}, LX/186;->a(III)V

    .line 684490
    :cond_9
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v11}, LX/186;->b(II)V

    .line 684491
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 684492
    if-eqz v6, :cond_a

    .line 684493
    const/4 v0, 0x4

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v9, v1}, LX/186;->a(III)V

    .line 684494
    :cond_a
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 684495
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :cond_b
    move v10, v5

    move v11, v6

    move v12, v7

    move v6, v0

    move v7, v1

    move v1, v2

    move v13, v3

    move-wide v2, v8

    move v9, v4

    move v8, v13

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    .line 684496
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 684497
    invoke-virtual {p0, p1, v3, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 684498
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_0

    .line 684499
    const-string v2, "amount"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 684500
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 684501
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 684502
    if-eqz v0, :cond_1

    .line 684503
    const-string v1, "amount_with_offset"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 684504
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 684505
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 684506
    if-eqz v0, :cond_2

    .line 684507
    const-string v1, "currency"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 684508
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 684509
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 684510
    if-eqz v0, :cond_3

    .line 684511
    const-string v1, "formatted"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 684512
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 684513
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 684514
    if-eqz v0, :cond_4

    .line 684515
    const-string v1, "offset"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 684516
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 684517
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 684518
    if-eqz v0, :cond_5

    .line 684519
    const-string v1, "offset_amount"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 684520
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 684521
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 684522
    return-void
.end method
