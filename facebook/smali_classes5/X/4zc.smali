.class public final LX/4zc;
.super LX/4za;
.source ""

# interfaces
.implements LX/0qF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LX/4za",
        "<TK;TV;>;",
        "LX/0qF",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field public volatile e:J

.field public f:LX/0qF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0qF",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public g:LX/0qF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0qF",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Object;ILX/0qF;)V
    .locals 2
    .param p3    # LX/0qF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;I",
            "LX/0qF",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 822960
    invoke-direct {p0, p1, p2, p3}, LX/4za;-><init>(Ljava/lang/Object;ILX/0qF;)V

    .line 822961
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, LX/4zc;->e:J

    .line 822962
    sget-object v0, LX/4zX;->INSTANCE:LX/4zX;

    move-object v0, v0

    .line 822963
    iput-object v0, p0, LX/4zc;->f:LX/0qF;

    .line 822964
    sget-object v0, LX/4zX;->INSTANCE:LX/4zX;

    move-object v0, v0

    .line 822965
    iput-object v0, p0, LX/4zc;->g:LX/0qF;

    .line 822966
    return-void
.end method


# virtual methods
.method public final getExpirationTime()J
    .locals 2

    .prologue
    .line 822975
    iget-wide v0, p0, LX/4zc;->e:J

    return-wide v0
.end method

.method public final getNextExpirable()LX/0qF;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0qF",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 822974
    iget-object v0, p0, LX/4zc;->f:LX/0qF;

    return-object v0
.end method

.method public final getPreviousExpirable()LX/0qF;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0qF",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 822973
    iget-object v0, p0, LX/4zc;->g:LX/0qF;

    return-object v0
.end method

.method public final setExpirationTime(J)V
    .locals 1

    .prologue
    .line 822971
    iput-wide p1, p0, LX/4zc;->e:J

    .line 822972
    return-void
.end method

.method public final setNextExpirable(LX/0qF;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0qF",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 822969
    iput-object p1, p0, LX/4zc;->f:LX/0qF;

    .line 822970
    return-void
.end method

.method public final setPreviousExpirable(LX/0qF;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0qF",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 822967
    iput-object p1, p0, LX/4zc;->g:LX/0qF;

    .line 822968
    return-void
.end method
