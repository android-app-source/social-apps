.class public final LX/47O;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/47M;


# instance fields
.field private final a:I

.field private final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Landroid/os/Bundle;


# direct methods
.method public constructor <init>(ILjava/lang/Class;Landroid/os/Bundle;)V
    .locals 0
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;",
            "Landroid/os/Bundle;",
            ")V"
        }
    .end annotation

    .prologue
    .line 672244
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 672245
    iput p1, p0, LX/47O;->a:I

    .line 672246
    iput-object p2, p0, LX/47O;->b:Ljava/lang/Class;

    .line 672247
    iput-object p3, p0, LX/47O;->c:Landroid/os/Bundle;

    .line 672248
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/os/Bundle;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 672249
    iget-object v0, p0, LX/47O;->b:Ljava/lang/Class;

    .line 672250
    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    invoke-static {v1, v0}, LX/4tq;->a(LX/0QB;Ljava/lang/Class;)Landroid/content/ComponentName;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    .line 672251
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    .line 672252
    const-string v1, "target_fragment"

    iget v2, p0, LX/47O;->a:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 672253
    iget-object v1, p0, LX/47O;->c:Landroid/os/Bundle;

    if-eqz v1, :cond_0

    .line 672254
    iget-object v1, p0, LX/47O;->c:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 672255
    :cond_0
    if-eqz p2, :cond_1

    .line 672256
    invoke-virtual {v0, p2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 672257
    :cond_1
    return-object v0
.end method
