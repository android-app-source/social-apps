.class public final LX/3in;
.super Landroid/view/View;
.source ""


# instance fields
.field public final synthetic a:LX/3if;


# direct methods
.method public constructor <init>(LX/3if;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 630558
    iput-object p1, p0, LX/3in;->a:LX/3if;

    .line 630559
    invoke-direct {p0, p2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 630560
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/3in;->setWillNotDraw(Z)V

    .line 630561
    return-void
.end method

.method private a(I)I
    .locals 3

    .prologue
    .line 630562
    iget-object v0, p0, LX/3in;->a:LX/3if;

    iget-object v0, v0, LX/3if;->i:LX/1P1;

    invoke-virtual {v0}, LX/1P1;->n()I

    move-result v0

    .line 630563
    iget-object v1, p0, LX/3in;->a:LX/3if;

    iget-object v1, v1, LX/3if;->h:Landroid/support/v7/widget/RecyclerView;

    .line 630564
    iget-object v2, v1, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v1, v2

    .line 630565
    invoke-virtual {v1}, LX/1OM;->ij_()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-eq v0, v1, :cond_1

    .line 630566
    :cond_0
    :goto_0
    return p1

    .line 630567
    :cond_1
    iget-object v1, p0, LX/3in;->a:LX/3if;

    iget-object v1, v1, LX/3if;->i:LX/1P1;

    invoke-virtual {v1, v0}, LX/1OR;->c(I)Landroid/view/View;

    move-result-object v0

    .line 630568
    if-eqz v0, :cond_0

    .line 630569
    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    .line 630570
    iget-object v1, p0, LX/3in;->a:LX/3if;

    iget-object v1, v1, LX/3if;->h:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->getTop()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, LX/3in;->a:LX/3if;

    invoke-virtual {v1}, LX/3if;->getHeight()I

    move-result v1

    sub-int/2addr v0, v1

    .line 630571
    neg-int v0, v0

    .line 630572
    invoke-static {v0, p1}, Ljava/lang/Math;->max(II)I

    move-result p1

    goto :goto_0
.end method


# virtual methods
.method public final offsetTopAndBottom(I)V
    .locals 5

    .prologue
    .line 630573
    invoke-direct {p0, p1}, LX/3in;->a(I)I

    move-result v0

    .line 630574
    invoke-super {p0, v0}, Landroid/view/View;->offsetTopAndBottom(I)V

    .line 630575
    iget-object v1, p0, LX/3in;->a:LX/3if;

    invoke-virtual {v1}, LX/3if;->invalidate()V

    .line 630576
    const/4 v1, 0x0

    .line 630577
    if-lez v0, :cond_9

    .line 630578
    iget-object v2, p0, LX/3in;->a:LX/3if;

    iget-object v2, v2, LX/3if;->i:LX/1P1;

    invoke-virtual {v2}, LX/1P1;->l()I

    move-result v2

    if-eqz v2, :cond_8

    move v2, v1

    .line 630579
    :goto_0
    if-eqz v0, :cond_0

    .line 630580
    iget-object v3, p0, LX/3in;->a:LX/3if;

    iget-object v3, v3, LX/3if;->h:Landroid/support/v7/widget/RecyclerView;

    neg-int v4, v0

    invoke-virtual {v3, v1, v4}, Landroid/support/v7/widget/RecyclerView;->scrollBy(II)V

    .line 630581
    :cond_0
    if-eqz v2, :cond_1

    .line 630582
    iget-object v3, p0, LX/3in;->a:LX/3if;

    iget-object v3, v3, LX/3if;->h:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v3, v2}, Landroid/support/v7/widget/RecyclerView;->offsetTopAndBottom(I)V

    .line 630583
    :cond_1
    if-nez v0, :cond_2

    if-eqz v2, :cond_3

    :cond_2
    const/4 v1, 0x1

    :cond_3
    move v0, v1

    .line 630584
    if-nez v0, :cond_5

    .line 630585
    if-gez p1, :cond_4

    .line 630586
    iget-object v0, p0, LX/3in;->a:LX/3if;

    iget-object v0, v0, LX/3if;->i:LX/1P1;

    invoke-virtual {v0}, LX/1P1;->o()I

    move-result v0

    .line 630587
    iget-object v1, p0, LX/3in;->a:LX/3if;

    iget-object v1, v1, LX/3if;->h:Landroid/support/v7/widget/RecyclerView;

    .line 630588
    iget-object v2, v1, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v1, v2

    .line 630589
    invoke-virtual {v1}, LX/1OM;->ij_()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_4

    iget-object v1, p0, LX/3in;->a:LX/3if;

    iget-object v1, v1, LX/3if;->i:LX/1P1;

    invoke-virtual {v1, v0}, LX/1OR;->c(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    iget-object v1, p0, LX/3in;->a:LX/3if;

    invoke-virtual {v1}, LX/3if;->getBottom()I

    move-result v1

    if-ne v0, v1, :cond_4

    .line 630590
    iget-object v0, p0, LX/3in;->a:LX/3if;

    iget-object v0, v0, LX/3if;->h:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x0

    invoke-virtual {p0}, LX/3in;->getContext()Landroid/content/Context;

    move-result-object v2

    const/high16 v3, 0x447a0000    # 1000.0f

    invoke-static {v2, v3}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/widget/RecyclerView;->scrollBy(II)V

    .line 630591
    :cond_4
    iget-object v0, p0, LX/3in;->a:LX/3if;

    iget-object v0, v0, LX/3if;->j:LX/3ij;

    .line 630592
    iget v1, v0, LX/3ij;->a:I

    move v0, v1

    .line 630593
    const/4 v1, 0x1

    if-eq v0, v1, :cond_5

    .line 630594
    iget-object v0, p0, LX/3in;->a:LX/3if;

    iget-object v0, v0, LX/3if;->j:LX/3ij;

    invoke-virtual {v0}, LX/3ij;->b()V

    .line 630595
    :cond_5
    iget-object v0, p0, LX/3in;->a:LX/3if;

    .line 630596
    iget-boolean v1, v0, LX/3if;->o:Z

    if-nez v1, :cond_6

    iget-object v1, v0, LX/3if;->j:LX/3ij;

    .line 630597
    iget v2, v1, LX/3ij;->a:I

    move v1, v2

    .line 630598
    const/4 v2, 0x2

    if-ne v1, v2, :cond_6

    .line 630599
    invoke-static {v0}, LX/3if;->e$redex0(LX/3if;)V

    .line 630600
    :cond_6
    invoke-virtual {v0}, LX/3if;->getBottom()I

    move-result v1

    if-lez v1, :cond_7

    .line 630601
    iget-object v1, v0, LX/3if;->h:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->getTop()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0}, LX/3if;->getBottom()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 630602
    const/high16 v2, 0x3f800000    # 1.0f

    sub-float v1, v2, v1

    const/high16 v2, 0x43190000    # 153.0f

    mul-float/2addr v1, v2

    float-to-int v1, v1

    .line 630603
    const/4 v2, 0x0

    const/16 v3, 0xff

    invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 630604
    invoke-virtual {v0}, LX/3if;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 630605
    :cond_7
    return-void

    .line 630606
    :cond_8
    iget-object v2, p0, LX/3in;->a:LX/3if;

    iget-object v2, v2, LX/3if;->i:LX/1P1;

    invoke-virtual {v2, v1}, LX/1OR;->c(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v2

    .line 630607
    neg-int v2, v2

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 630608
    sub-int v2, v0, v3

    move v0, v3

    .line 630609
    goto/16 :goto_0

    .line 630610
    :cond_9
    iget-object v2, p0, LX/3in;->a:LX/3if;

    iget-object v2, v2, LX/3if;->h:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->getTop()I

    move-result v2

    add-int/2addr v2, v0

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 630611
    iget-object v3, p0, LX/3in;->a:LX/3if;

    iget-object v3, v3, LX/3if;->h:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v3}, Landroid/support/v7/widget/RecyclerView;->getTop()I

    move-result v3

    sub-int/2addr v2, v3

    .line 630612
    sub-int/2addr v0, v2

    goto/16 :goto_0
.end method

.method public final onPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 2

    .prologue
    .line 630613
    invoke-super {p0, p1}, Landroid/view/View;->onPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 630614
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v0

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    .line 630615
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 630616
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 630617
    :cond_0
    return-void
.end method
