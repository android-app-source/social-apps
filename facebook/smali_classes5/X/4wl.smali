.class public final LX/4wl;
.super LX/2I9;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2I9",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/util/Map$Entry;

.field public final synthetic b:LX/4wm;


# direct methods
.method public constructor <init>(LX/4wm;Ljava/util/Map$Entry;)V
    .locals 0

    .prologue
    .line 820414
    iput-object p1, p0, LX/4wl;->b:LX/4wm;

    iput-object p2, p0, LX/4wl;->a:Ljava/util/Map$Entry;

    invoke-direct {p0}, LX/2I9;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/util/Map$Entry;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 820403
    iget-object v0, p0, LX/4wl;->a:Ljava/util/Map$Entry;

    return-object v0
.end method

.method public final synthetic e()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 820413
    invoke-virtual {p0}, LX/4wl;->a()Ljava/util/Map$Entry;

    move-result-object v0

    return-object v0
.end method

.method public final setValue(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)TV;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 820404
    iget-object v0, p0, LX/4wl;->b:LX/4wm;

    iget-object v0, v0, LX/4wm;->c:LX/4wn;

    invoke-virtual {v0, p0}, LX/4wn;->contains(Ljava/lang/Object;)Z

    move-result v0

    const-string v3, "entry no longer in map"

    invoke-static {v0, v3}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 820405
    invoke-virtual {p0}, LX/2I9;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-static {p1, v0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 820406
    :goto_0
    return-object p1

    .line 820407
    :cond_0
    iget-object v0, p0, LX/4wl;->b:LX/4wm;

    iget-object v0, v0, LX/4wm;->c:LX/4wn;

    iget-object v0, v0, LX/4wn;->b:LX/4wp;

    invoke-virtual {v0, p1}, LX/4wp;->containsValue(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    const-string v3, "value already present: %s"

    new-array v4, v1, [Ljava/lang/Object;

    aput-object p1, v4, v2

    invoke-static {v0, v3, v4}, LX/0PB;->checkArgument(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 820408
    iget-object v0, p0, LX/4wl;->a:Ljava/util/Map$Entry;

    invoke-interface {v0, p1}, Ljava/util/Map$Entry;->setValue(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 820409
    iget-object v2, p0, LX/4wl;->b:LX/4wm;

    iget-object v2, v2, LX/4wm;->c:LX/4wn;

    iget-object v2, v2, LX/4wn;->b:LX/4wp;

    invoke-virtual {p0}, LX/2I9;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/4wo;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-static {p1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    const-string v3, "entry no longer in map"

    invoke-static {v2, v3}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 820410
    iget-object v2, p0, LX/4wl;->b:LX/4wm;

    iget-object v2, v2, LX/4wm;->c:LX/4wn;

    iget-object v2, v2, LX/4wn;->b:LX/4wp;

    invoke-virtual {p0}, LX/2I9;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-static {v2, v3, v1, v0, p1}, LX/4wp;->a$redex0(LX/4wp;Ljava/lang/Object;ZLjava/lang/Object;Ljava/lang/Object;)V

    move-object p1, v0

    .line 820411
    goto :goto_0

    :cond_1
    move v0, v2

    .line 820412
    goto :goto_1
.end method
