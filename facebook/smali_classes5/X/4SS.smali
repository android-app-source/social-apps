.class public LX/4SS;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 714546
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 714547
    const/4 v2, 0x0

    .line 714548
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v4, :cond_c

    .line 714549
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 714550
    :goto_0
    return v0

    .line 714551
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_9

    .line 714552
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 714553
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 714554
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_0

    if-eqz v11, :cond_0

    .line 714555
    const-string v12, "hide_responses"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 714556
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v2

    move v10, v2

    move v2, v1

    goto :goto_1

    .line 714557
    :cond_1
    const-string v12, "id"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 714558
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 714559
    :cond_2
    const-string v12, "next_question"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 714560
    invoke-static {p0, p1}, LX/4SS;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 714561
    :cond_3
    const-string v12, "question_responders"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 714562
    invoke-static {p0, p1}, LX/4SU;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 714563
    :cond_4
    const-string v12, "question_type"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 714564
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLResearchPollQuestionType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLResearchPollQuestionType;

    move-result-object v0

    move-object v6, v0

    move v0, v1

    goto :goto_1

    .line 714565
    :cond_5
    const-string v12, "research_poll_question"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 714566
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 714567
    :cond_6
    const-string v12, "responses"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 714568
    invoke-static {p0, p1}, LX/4SV;->a(LX/15w;LX/186;)I

    move-result v4

    goto/16 :goto_1

    .line 714569
    :cond_7
    const-string v12, "url"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 714570
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto/16 :goto_1

    .line 714571
    :cond_8
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 714572
    :cond_9
    const/16 v11, 0x9

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 714573
    if-eqz v2, :cond_a

    .line 714574
    invoke-virtual {p1, v1, v10}, LX/186;->a(IZ)V

    .line 714575
    :cond_a
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v9}, LX/186;->b(II)V

    .line 714576
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v8}, LX/186;->b(II)V

    .line 714577
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 714578
    if-eqz v0, :cond_b

    .line 714579
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v6}, LX/186;->a(ILjava/lang/Enum;)V

    .line 714580
    :cond_b
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 714581
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 714582
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 714583
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :cond_c
    move v3, v0

    move v4, v0

    move v5, v0

    move-object v6, v2

    move v7, v0

    move v8, v0

    move v9, v0

    move v10, v0

    move v2, v0

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 714540
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 714541
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 714542
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2, p3}, LX/4SS;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 714543
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 714544
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 714545
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 714534
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 714535
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 714536
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 714537
    invoke-static {p0, p1}, LX/4SS;->a(LX/15w;LX/186;)I

    move-result v1

    .line 714538
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 714539
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x5

    .line 714499
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 714500
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 714501
    if-eqz v0, :cond_0

    .line 714502
    const-string v1, "hide_responses"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 714503
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 714504
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 714505
    if-eqz v0, :cond_1

    .line 714506
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 714507
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 714508
    :cond_1
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 714509
    if-eqz v0, :cond_2

    .line 714510
    const-string v1, "next_question"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 714511
    invoke-static {p0, v0, p2, p3}, LX/4SS;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 714512
    :cond_2
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 714513
    if-eqz v0, :cond_3

    .line 714514
    const-string v1, "question_responders"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 714515
    invoke-static {p0, v0, p2}, LX/4SU;->a(LX/15i;ILX/0nX;)V

    .line 714516
    :cond_3
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v2, v0}, LX/15i;->a(IIS)S

    move-result v0

    .line 714517
    if-eqz v0, :cond_4

    .line 714518
    const-string v0, "question_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 714519
    const-class v0, Lcom/facebook/graphql/enums/GraphQLResearchPollQuestionType;

    invoke-virtual {p0, p1, v2, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLResearchPollQuestionType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLResearchPollQuestionType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 714520
    :cond_4
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 714521
    if-eqz v0, :cond_5

    .line 714522
    const-string v1, "research_poll_question"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 714523
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 714524
    :cond_5
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 714525
    if-eqz v0, :cond_6

    .line 714526
    const-string v1, "responses"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 714527
    invoke-static {p0, v0, p2, p3}, LX/4SV;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 714528
    :cond_6
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 714529
    if-eqz v0, :cond_7

    .line 714530
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 714531
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 714532
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 714533
    return-void
.end method
