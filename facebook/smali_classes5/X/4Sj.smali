.class public LX/4Sj;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 715495
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 715496
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 715497
    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 715498
    if-eqz v0, :cond_0

    .line 715499
    const-string v1, "fbid"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 715500
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 715501
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 715502
    if-eqz v0, :cond_1

    .line 715503
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 715504
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 715505
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 715506
    if-eqz v0, :cond_2

    .line 715507
    const-string v1, "photo_uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 715508
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 715509
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 715510
    if-eqz v0, :cond_3

    .line 715511
    const-string v1, "electoral_vote_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 715512
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 715513
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 715514
    if-eqz v0, :cond_4

    .line 715515
    const-string v1, "is_winner"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 715516
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 715517
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 715518
    if-eqz v0, :cond_5

    .line 715519
    const-string v1, "last_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 715520
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 715521
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 715522
    if-eqz v0, :cond_6

    .line 715523
    const-string v1, "vote_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 715524
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 715525
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 715526
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 13

    .prologue
    .line 715447
    const/4 v9, 0x0

    .line 715448
    const/4 v8, 0x0

    .line 715449
    const/4 v7, 0x0

    .line 715450
    const/4 v6, 0x0

    .line 715451
    const/4 v5, 0x0

    .line 715452
    const/4 v4, 0x0

    .line 715453
    const/4 v3, 0x0

    .line 715454
    const/4 v2, 0x0

    .line 715455
    const/4 v1, 0x0

    .line 715456
    const/4 v0, 0x0

    .line 715457
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->START_OBJECT:LX/15z;

    if-eq v10, v11, :cond_1

    .line 715458
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 715459
    const/4 v0, 0x0

    .line 715460
    :goto_0
    return v0

    .line 715461
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 715462
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_8

    .line 715463
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 715464
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 715465
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_1

    if-eqz v10, :cond_1

    .line 715466
    const-string v11, "fbid"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 715467
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 715468
    :cond_2
    const-string v11, "name"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 715469
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 715470
    :cond_3
    const-string v11, "photo_uri"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 715471
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 715472
    :cond_4
    const-string v11, "electoral_vote_count"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 715473
    const/4 v2, 0x1

    .line 715474
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v6

    goto :goto_1

    .line 715475
    :cond_5
    const-string v11, "is_winner"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 715476
    const/4 v1, 0x1

    .line 715477
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v5

    goto :goto_1

    .line 715478
    :cond_6
    const-string v11, "last_name"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 715479
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 715480
    :cond_7
    const-string v11, "vote_count"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 715481
    const/4 v0, 0x1

    .line 715482
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v3

    goto/16 :goto_1

    .line 715483
    :cond_8
    const/4 v10, 0x7

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 715484
    const/4 v10, 0x0

    invoke-virtual {p1, v10, v9}, LX/186;->b(II)V

    .line 715485
    const/4 v9, 0x1

    invoke-virtual {p1, v9, v8}, LX/186;->b(II)V

    .line 715486
    const/4 v8, 0x2

    invoke-virtual {p1, v8, v7}, LX/186;->b(II)V

    .line 715487
    if-eqz v2, :cond_9

    .line 715488
    const/4 v2, 0x3

    const/4 v7, 0x0

    invoke-virtual {p1, v2, v6, v7}, LX/186;->a(III)V

    .line 715489
    :cond_9
    if-eqz v1, :cond_a

    .line 715490
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v5}, LX/186;->a(IZ)V

    .line 715491
    :cond_a
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 715492
    if-eqz v0, :cond_b

    .line 715493
    const/4 v0, 0x6

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v3, v1}, LX/186;->a(III)V

    .line 715494
    :cond_b
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0
.end method
