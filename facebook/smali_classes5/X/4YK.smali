.class public final LX/4YK;
.super LX/0ur;
.source ""


# instance fields
.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

.field public d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 776371
    invoke-direct {p0}, LX/0ur;-><init>()V

    .line 776372
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    iput-object v0, p0, LX/4YK;->c:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    .line 776373
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    iput-object v0, p0, LX/4YK;->e:Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    .line 776374
    instance-of v0, p0, LX/4YK;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 776375
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;)LX/4YK;
    .locals 2

    .prologue
    .line 776376
    new-instance v0, LX/4YK;

    invoke-direct {v0}, LX/4YK;-><init>()V

    .line 776377
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 776378
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;->a()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4YK;->b:LX/0Px;

    .line 776379
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;->b()Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    move-result-object v1

    iput-object v1, v0, LX/4YK;->c:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    .line 776380
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;->c()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4YK;->d:LX/0Px;

    .line 776381
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;->d()Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    move-result-object v1

    iput-object v1, v0, LX/4YK;->e:Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    .line 776382
    invoke-static {v0, p0}, LX/0ur;->a(LX/0ur;Lcom/facebook/graphql/modelutil/BaseModel;)V

    .line 776383
    return-object v0
.end method


# virtual methods
.method public final a()Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;
    .locals 2

    .prologue
    .line 776384
    new-instance v0, Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;

    invoke-direct {v0, p0}, Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;-><init>(LX/4YK;)V

    .line 776385
    return-object v0
.end method
