.class public abstract enum LX/45c;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/45c;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/45c;

.field public static final enum AFTER_CARRIAGE_RETURN:LX/45c;

.field public static final enum AFTER_FIRST_CHARACTER:LX/45c;

.field public static final enum SEPARATOR_CHARACTER_n:LX/45c;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 670447
    new-instance v0, LX/45d;

    const-string v1, "AFTER_CARRIAGE_RETURN"

    invoke-direct {v0, v1, v2}, LX/45d;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/45c;->AFTER_CARRIAGE_RETURN:LX/45c;

    .line 670448
    new-instance v0, LX/45e;

    const-string v1, "AFTER_FIRST_CHARACTER"

    invoke-direct {v0, v1, v3}, LX/45e;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/45c;->AFTER_FIRST_CHARACTER:LX/45c;

    .line 670449
    new-instance v0, LX/45f;

    const-string v1, "SEPARATOR_CHARACTER_n"

    invoke-direct {v0, v1, v4}, LX/45f;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/45c;->SEPARATOR_CHARACTER_n:LX/45c;

    .line 670450
    const/4 v0, 0x3

    new-array v0, v0, [LX/45c;

    sget-object v1, LX/45c;->AFTER_CARRIAGE_RETURN:LX/45c;

    aput-object v1, v0, v2

    sget-object v1, LX/45c;->AFTER_FIRST_CHARACTER:LX/45c;

    aput-object v1, v0, v3

    sget-object v1, LX/45c;->SEPARATOR_CHARACTER_n:LX/45c;

    aput-object v1, v0, v4

    sput-object v0, LX/45c;->$VALUES:[LX/45c;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 670446
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/45c;
    .locals 1

    .prologue
    .line 670442
    const-class v0, LX/45c;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/45c;

    return-object v0
.end method

.method public static values()[LX/45c;
    .locals 1

    .prologue
    .line 670445
    sget-object v0, LX/45c;->$VALUES:[LX/45c;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/45c;

    return-object v0
.end method


# virtual methods
.method public abstract handle(LX/45g;I)V
.end method

.method public onEnterState(LX/45g;)V
    .locals 0

    .prologue
    .line 670444
    return-void
.end method

.method public onExitState(LX/45g;)V
    .locals 0

    .prologue
    .line 670443
    return-void
.end method
