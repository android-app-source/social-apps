.class public LX/3R7;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final d:Ljava/lang/Object;


# instance fields
.field public final a:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

.field private final b:LX/0Sh;

.field public final c:LX/2P0;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 579003
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/3R7;->d:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;LX/0Sh;LX/2P0;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 578967
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 578968
    iput-object p1, p0, LX/3R7;->a:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    .line 578969
    iput-object p2, p0, LX/3R7;->b:LX/0Sh;

    .line 578970
    iput-object p3, p0, LX/3R7;->c:LX/2P0;

    .line 578971
    return-void
.end method

.method public static a(LX/0QB;)LX/3R7;
    .locals 9

    .prologue
    .line 578974
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 578975
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 578976
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 578977
    if-nez v1, :cond_0

    .line 578978
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 578979
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 578980
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 578981
    sget-object v1, LX/3R7;->d:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 578982
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 578983
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 578984
    :cond_1
    if-nez v1, :cond_4

    .line 578985
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 578986
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 578987
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 578988
    new-instance p0, LX/3R7;

    invoke-static {v0}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->a(LX/0QB;)Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v7

    check-cast v7, LX/0Sh;

    invoke-static {v0}, LX/2P0;->a(LX/0QB;)LX/2P0;

    move-result-object v8

    check-cast v8, LX/2P0;

    invoke-direct {p0, v1, v7, v8}, LX/3R7;-><init>(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;LX/0Sh;LX/2P0;)V

    .line 578989
    move-object v1, p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 578990
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 578991
    if-nez v1, :cond_2

    .line 578992
    sget-object v0, LX/3R7;->d:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3R7;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 578993
    :goto_1
    if-eqz v0, :cond_3

    .line 578994
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 578995
    :goto_3
    check-cast v0, LX/3R7;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 578996
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 578997
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 578998
    :catchall_1
    move-exception v0

    .line 578999
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 579000
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 579001
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 579002
    :cond_2
    :try_start_8
    sget-object v0, LX/3R7;->d:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3R7;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method public static a(LX/3R7;Lcom/facebook/ui/media/attachments/MediaResource;)V
    .locals 2

    .prologue
    .line 578972
    iget-object v0, p0, LX/3R7;->b:LX/0Sh;

    new-instance v1, Lcom/facebook/messaging/tincan/messenger/EncryptedAttachmentUploadRetryHandler$1;

    invoke-direct {v1, p0, p1}, Lcom/facebook/messaging/tincan/messenger/EncryptedAttachmentUploadRetryHandler$1;-><init>(LX/3R7;Lcom/facebook/ui/media/attachments/MediaResource;)V

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    .line 578973
    return-void
.end method
