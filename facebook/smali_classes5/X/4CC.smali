.class public LX/4CC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4CA;


# instance fields
.field private a:I

.field private b:LX/1FJ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 678566
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 678567
    const/4 v0, -0x1

    iput v0, p0, LX/4CC;->a:I

    return-void
.end method

.method private declared-synchronized d()V
    .locals 1

    .prologue
    .line 678561
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/4CC;->b:LX/1FJ;

    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    .line 678562
    const/4 v0, 0x0

    iput-object v0, p0, LX/4CC;->b:LX/1FJ;

    .line 678563
    const/4 v0, -0x1

    iput v0, p0, LX/4CC;->a:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 678564
    monitor-exit p0

    return-void

    .line 678565
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()LX/1FJ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 678560
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/4CC;->b:LX/1FJ;

    invoke-static {v0}, LX/1FJ;->b(LX/1FJ;)LX/1FJ;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(I)LX/1FJ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 678568
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/4CC;->a:I

    if-ne v0, p1, :cond_0

    .line 678569
    iget-object v0, p0, LX/4CC;->b:LX/1FJ;

    invoke-static {v0}, LX/1FJ;->b(LX/1FJ;)LX/1FJ;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 678570
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 678571
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(ILX/1FJ;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 678548
    monitor-enter p0

    :try_start_0
    iput p1, p0, LX/4CC;->a:I

    .line 678549
    if-eqz p2, :cond_0

    iget-object v0, p0, LX/4CC;->b:LX/1FJ;

    if-eqz v0, :cond_0

    invoke-virtual {p2}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    iget-object v1, p0, LX/4CC;->b:LX/1FJ;

    invoke-virtual {v1}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 678550
    :goto_0
    monitor-exit p0

    return-void

    .line 678551
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/4CC;->b:LX/1FJ;

    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    .line 678552
    invoke-static {p2}, LX/1FJ;->b(LX/1FJ;)LX/1FJ;

    move-result-object v0

    iput-object v0, p0, LX/4CC;->b:LX/1FJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 678553
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()LX/1FJ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 678554
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/4CC;->b:LX/1FJ;

    invoke-static {v0}, LX/1FJ;->b(LX/1FJ;)LX/1FJ;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 678555
    :try_start_1
    invoke-direct {p0}, LX/4CC;->d()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    :try_start_2
    invoke-direct {p0}, LX/4CC;->d()V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 678556
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()V
    .locals 1

    .prologue
    .line 678557
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LX/4CC;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 678558
    monitor-exit p0

    return-void

    .line 678559
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
