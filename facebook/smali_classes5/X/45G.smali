.class public LX/45G;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Appendable;


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field

.field public b:I

.field public c:I


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 670256
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 670257
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, LX/45G;->a:Ljava/util/List;

    .line 670258
    return-void
.end method

.method public static b(LX/45G;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 670255
    new-instance v0, LX/45E;

    iget-object v1, p0, LX/45G;->a:Ljava/util/List;

    iget v2, p0, LX/45G;->b:I

    iget-object v3, p0, LX/45G;->a:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    iget v4, p0, LX/45G;->c:I

    invoke-direct {v0, v1, v2, v3, v4}, LX/45E;-><init>(Ljava/util/List;III)V

    return-object v0
.end method


# virtual methods
.method public final append(C)Ljava/lang/Appendable;
    .locals 1

    .prologue
    .line 670254
    new-instance v0, LX/45F;

    invoke-direct {v0, p1}, LX/45F;-><init>(C)V

    invoke-virtual {p0, v0}, LX/45G;->append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;

    move-result-object v0

    return-object v0
.end method

.method public final append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;
    .locals 2

    .prologue
    .line 670251
    iget-object v0, p0, LX/45G;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 670252
    iget v0, p0, LX/45G;->c:I

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, LX/45G;->c:I

    .line 670253
    return-object p0
.end method

.method public final append(Ljava/lang/CharSequence;II)Ljava/lang/Appendable;
    .locals 1

    .prologue
    .line 670250
    invoke-interface {p1, p2, p3}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/45G;->append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 670249
    invoke-static {p0}, LX/45G;->b(LX/45G;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
