.class public final enum LX/4sD;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/4sD;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/4sD;

.field public static final enum REQUIRE_HEADER:LX/4sD;


# instance fields
.field public final _defaultState:Z

.field public final _mask:I


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 818236
    new-instance v0, LX/4sD;

    const-string v1, "REQUIRE_HEADER"

    invoke-direct {v0, v1, v2, v3}, LX/4sD;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/4sD;->REQUIRE_HEADER:LX/4sD;

    .line 818237
    new-array v0, v3, [LX/4sD;

    sget-object v1, LX/4sD;->REQUIRE_HEADER:LX/4sD;

    aput-object v1, v0, v2

    sput-object v0, LX/4sD;->$VALUES:[LX/4sD;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZ)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)V"
        }
    .end annotation

    .prologue
    .line 818231
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 818232
    iput-boolean p3, p0, LX/4sD;->_defaultState:Z

    .line 818233
    const/4 v0, 0x1

    invoke-virtual {p0}, LX/4sD;->ordinal()I

    move-result v1

    shl-int/2addr v0, v1

    iput v0, p0, LX/4sD;->_mask:I

    .line 818234
    return-void
.end method

.method public static collectDefaults()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 818226
    invoke-static {}, LX/4sD;->values()[LX/4sD;

    move-result-object v2

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 818227
    invoke-virtual {v4}, LX/4sD;->enabledByDefault()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 818228
    invoke-virtual {v4}, LX/4sD;->getMask()I

    move-result v4

    or-int/2addr v0, v4

    .line 818229
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 818230
    :cond_1
    return v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/4sD;
    .locals 1

    .prologue
    .line 818235
    const-class v0, LX/4sD;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/4sD;

    return-object v0
.end method

.method public static values()[LX/4sD;
    .locals 1

    .prologue
    .line 818223
    sget-object v0, LX/4sD;->$VALUES:[LX/4sD;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/4sD;

    return-object v0
.end method


# virtual methods
.method public final enabledByDefault()Z
    .locals 1

    .prologue
    .line 818225
    iget-boolean v0, p0, LX/4sD;->_defaultState:Z

    return v0
.end method

.method public final getMask()I
    .locals 1

    .prologue
    .line 818224
    iget v0, p0, LX/4sD;->_mask:I

    return v0
.end method
