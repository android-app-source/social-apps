.class public LX/3fh;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/3fh;


# instance fields
.field private final a:LX/0lC;


# direct methods
.method public constructor <init>(LX/0lC;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 623159
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 623160
    iput-object p1, p0, LX/3fh;->a:LX/0lC;

    .line 623161
    return-void
.end method

.method public static a(LX/0QB;)LX/3fh;
    .locals 4

    .prologue
    .line 623146
    sget-object v0, LX/3fh;->b:LX/3fh;

    if-nez v0, :cond_1

    .line 623147
    const-class v1, LX/3fh;

    monitor-enter v1

    .line 623148
    :try_start_0
    sget-object v0, LX/3fh;->b:LX/3fh;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 623149
    if-eqz v2, :cond_0

    .line 623150
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 623151
    new-instance p0, LX/3fh;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v3

    check-cast v3, LX/0lC;

    invoke-direct {p0, v3}, LX/3fh;-><init>(LX/0lC;)V

    .line 623152
    move-object v0, p0

    .line 623153
    sput-object v0, LX/3fh;->b:LX/3fh;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 623154
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 623155
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 623156
    :cond_1
    sget-object v0, LX/3fh;->b:LX/3fh;

    return-object v0

    .line 623157
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 623158
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/facebook/contacts/graphql/Contact;
    .locals 2

    .prologue
    .line 623162
    const-string v0, "deserializeContact"

    const v1, -0x21c968b

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 623163
    :try_start_0
    iget-object v0, p0, LX/3fh;->a:LX/0lC;

    const-class v1, Lcom/facebook/contacts/graphql/Contact;

    invoke-virtual {v0, p1, v1}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/graphql/Contact;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 623164
    const v1, -0x622a2380

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v0

    :catchall_0
    move-exception v0

    const v1, -0x7fc8732a

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final a(Lcom/facebook/contacts/graphql/Contact;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 623143
    const-string v0, "serializeContact"

    const v1, 0x53d79e72

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 623144
    :try_start_0
    iget-object v0, p0, LX/3fh;->a:LX/0lC;

    invoke-virtual {v0, p1}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 623145
    const v1, -0x344f87dd    # -2.3130182E7f

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v0

    :catchall_0
    move-exception v0

    const v1, 0x400745f3

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method
