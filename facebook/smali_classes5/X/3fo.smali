.class public LX/3fo;
.super LX/0ro;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0ro",
        "<",
        "Lcom/facebook/contacts/server/FetchDeltaContactsParams;",
        "Lcom/facebook/contacts/server/FetchDeltaContactsResult;",
        ">;"
    }
.end annotation


# static fields
.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final c:LX/3fn;

.field private final d:LX/3fb;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 623505
    const-class v0, LX/3fo;

    sput-object v0, LX/3fo;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/3fn;LX/3fb;LX/0sO;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 623506
    invoke-direct {p0, p3}, LX/0ro;-><init>(LX/0sO;)V

    .line 623507
    iput-object p1, p0, LX/3fo;->c:LX/3fn;

    .line 623508
    iput-object p2, p0, LX/3fo;->d:LX/3fb;

    .line 623509
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/1pN;LX/15w;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 623510
    const-class v0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchContactsDeltaQueryModel;

    invoke-virtual {p3, v0}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchContactsDeltaQueryModel;

    .line 623511
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 623512
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 623513
    invoke-virtual {v0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchContactsDeltaQueryModel;->a()Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchContactsDeltaQueryModel$MessengerContactsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchContactsDeltaQueryModel$MessengerContactsModel;->a()Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchContactsDeltaQueryModel$MessengerContactsModel$DeltasModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchContactsDeltaQueryModel$MessengerContactsModel$DeltasModel;->a()LX/0Px;

    move-result-object v5

    .line 623514
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v6, :cond_2

    invoke-virtual {v5, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchContactsDeltaQueryModel$MessengerContactsModel$DeltasModel$NodesModel;

    .line 623515
    invoke-virtual {v1}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchContactsDeltaQueryModel$MessengerContactsModel$DeltasModel$NodesModel;->a()Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;

    move-result-object v7

    .line 623516
    invoke-virtual {v1}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchContactsDeltaQueryModel$MessengerContactsModel$DeltasModel$NodesModel;->b()Ljava/lang/String;

    move-result-object v1

    .line 623517
    if-eqz v7, :cond_1

    .line 623518
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v8, "Deserializing node: "

    invoke-direct {v1, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 623519
    iget-object v1, p0, LX/3fo;->d:LX/3fb;

    invoke-virtual {v1, v7}, LX/3fb;->a(Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;)LX/3hB;

    move-result-object v1

    .line 623520
    invoke-virtual {v1}, LX/3hB;->O()Lcom/facebook/contacts/graphql/Contact;

    move-result-object v1

    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 623521
    :cond_0
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 623522
    :cond_1
    if-eqz v1, :cond_0

    .line 623523
    invoke-virtual {v4, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 623524
    :cond_2
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 623525
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    .line 623526
    invoke-virtual {v0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchContactsDeltaQueryModel;->a()Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchContactsDeltaQueryModel$MessengerContactsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchContactsDeltaQueryModel$MessengerContactsModel;->a()Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchContactsDeltaQueryModel$MessengerContactsModel$DeltasModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchContactsDeltaQueryModel$MessengerContactsModel$DeltasModel;->b()LX/3h9;

    move-result-object v0

    .line 623527
    if-nez v0, :cond_3

    .line 623528
    new-instance v0, LX/2Oo;

    const v1, 0x198f03

    const-string v2, "Request returned without page info"

    invoke-static {v1, v2}, Lcom/facebook/http/protocol/ApiErrorResult;->a(ILjava/lang/String;)LX/2AV;

    move-result-object v1

    invoke-virtual {v1}, LX/2AV;->a()Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v1

    invoke-direct {v0, v1}, LX/2Oo;-><init>(Lcom/facebook/http/protocol/ApiErrorResult;)V

    throw v0

    .line 623529
    :cond_3
    invoke-interface {v0}, LX/3h9;->a()Ljava/lang/String;

    move-result-object v4

    .line 623530
    invoke-interface {v0}, LX/3h9;->b()Z

    move-result v5

    .line 623531
    new-instance v0, Lcom/facebook/contacts/server/FetchDeltaContactsResult;

    sget-object v1, LX/0ta;->FROM_SERVER:LX/0ta;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-direct/range {v0 .. v7}, Lcom/facebook/contacts/server/FetchDeltaContactsResult;-><init>(LX/0ta;LX/0Px;LX/0Px;Ljava/lang/String;ZJ)V

    return-object v0
.end method

.method public final b(Ljava/lang/Object;LX/1pN;)I
    .locals 1

    .prologue
    .line 623532
    const/4 v0, 0x1

    return v0
.end method

.method public final f(Ljava/lang/Object;)LX/0gW;
    .locals 4

    .prologue
    .line 623533
    check-cast p1, Lcom/facebook/contacts/server/FetchDeltaContactsParams;

    .line 623534
    iget-object v0, p0, LX/3fo;->c:LX/3fn;

    .line 623535
    iget v1, p1, Lcom/facebook/contacts/server/FetchDeltaContactsParams;->a:I

    move v1, v1

    .line 623536
    iget-object v2, p1, Lcom/facebook/contacts/server/FetchDeltaContactsParams;->b:Ljava/lang/String;

    move-object v2, v2

    .line 623537
    sget-object v3, LX/3gq;->DELTA:LX/3gq;

    invoke-virtual {v0, v1, v2, v3}, LX/3fn;->a(ILjava/lang/String;LX/3gq;)LX/0gW;

    move-result-object v0

    return-object v0
.end method
