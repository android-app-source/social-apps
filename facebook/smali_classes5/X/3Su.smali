.class public LX/3Su;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/26t;


# instance fields
.field public final a:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

.field public final b:LX/1qv;


# direct methods
.method public constructor <init>(Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;LX/1qv;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 583096
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 583097
    iput-object p1, p0, LX/3Su;->a:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    .line 583098
    iput-object p2, p0, LX/3Su;->b:LX/1qv;

    .line 583099
    return-void
.end method

.method public static b(LX/0QB;)LX/3Su;
    .locals 3

    .prologue
    .line 583100
    new-instance v2, LX/3Su;

    invoke-static {p0}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a(LX/0QB;)Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    invoke-static {p0}, LX/1qv;->b(LX/0QB;)LX/1qv;

    move-result-object v1

    check-cast v1, LX/1qv;

    invoke-direct {v2, v0, v1}, LX/3Su;-><init>(Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;LX/1qv;)V

    .line 583101
    return-object v2
.end method


# virtual methods
.method public final handleOperation(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 8

    .prologue
    .line 583102
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 583103
    const-string v1, "fetch_gql_notifications"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 583104
    const/4 v4, 0x0

    .line 583105
    iget-object v2, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v2, v2

    .line 583106
    const-string v3, "fetchGraphQLNotificationsParams"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;

    .line 583107
    iget-object v3, p0, LX/3Su;->a:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    invoke-virtual {v2}, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->f()J

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->b(J)I

    move-result v3

    .line 583108
    iget-object v5, p0, LX/3Su;->b:LX/1qv;

    .line 583109
    iget-object v6, v5, LX/1qv;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v6

    .line 583110
    sget-object v7, LX/0hM;->d:LX/0Tn;

    invoke-interface {v6, v7, v3}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    .line 583111
    invoke-interface {v6}, LX/0hN;->commit()V

    .line 583112
    sget-object v5, LX/0rS;->STALE_DATA_OKAY:LX/0rS;

    .line 583113
    iget-object v6, v2, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->a:LX/0rS;

    move-object v6, v6

    .line 583114
    if-ne v5, v6, :cond_6

    .line 583115
    iget v5, v2, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->k:I

    move v5, v5

    .line 583116
    if-ge v5, v3, :cond_6

    const/4 v3, 0x1

    .line 583117
    :goto_0
    if-eqz v3, :cond_7

    .line 583118
    iget-boolean v3, v2, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->j:Z

    move v3, v3

    .line 583119
    if-eqz v3, :cond_8

    .line 583120
    iget-object v3, p0, LX/3Su;->a:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    invoke-virtual {v2}, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->f()J

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->e(J)LX/0Px;

    move-result-object v3

    .line 583121
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    .line 583122
    iget v5, v2, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->c:I

    move v5, v5

    .line 583123
    if-le v3, v5, :cond_8

    .line 583124
    iget-object v3, p0, LX/3Su;->a:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    invoke-virtual {v3, v2}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->b(Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;)Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;

    move-result-object v3

    .line 583125
    :goto_1
    if-nez v3, :cond_0

    .line 583126
    iget-object v3, p0, LX/3Su;->a:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    invoke-virtual {v3, v2}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a(Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;)Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;

    move-result-object v3

    .line 583127
    :cond_0
    sget-object v5, LX/0ta;->NO_DATA:LX/0ta;

    .line 583128
    iget-object v6, v3, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v6, v6

    .line 583129
    if-eq v5, v6, :cond_7

    .line 583130
    invoke-static {v3}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v3

    .line 583131
    :goto_2
    if-nez v3, :cond_2

    .line 583132
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v4

    .line 583133
    iget-boolean v3, v4, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    move v3, v3

    .line 583134
    if-eqz v3, :cond_1

    .line 583135
    invoke-virtual {v4}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;

    .line 583136
    iget-object v5, p0, LX/3Su;->a:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    .line 583137
    iget-boolean v6, v2, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->i:Z

    move v6, v6

    .line 583138
    invoke-virtual {v5, v3, v6}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a(Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;Z)V

    :cond_1
    move-object v3, v4

    .line 583139
    :cond_2
    iget-object v4, p0, LX/3Su;->a:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    invoke-virtual {v2}, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->f()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->b(J)I

    move-result v2

    .line 583140
    iget-object v4, p0, LX/3Su;->b:LX/1qv;

    .line 583141
    iget-object v5, v4, LX/1qv;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v5

    .line 583142
    sget-object v6, LX/0hM;->e:LX/0Tn;

    invoke-interface {v5, v6, v2}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    .line 583143
    invoke-interface {v5}, LX/0hN;->commit()V

    .line 583144
    move-object v0, v3

    .line 583145
    :goto_3
    return-object v0

    .line 583146
    :cond_3
    const-string v1, "graphNotifUpdateSeenState"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 583147
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 583148
    const-string v1, "graphNotifsUpdateSeenStatePrams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/server/NotificationsChangeSeenStateParams;

    .line 583149
    iget-object v1, p0, LX/3Su;->a:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    .line 583150
    iget-object v2, v0, Lcom/facebook/notifications/server/NotificationsChangeSeenStateParams;->a:LX/0Px;

    move-object v2, v2

    .line 583151
    iget-object v3, v0, Lcom/facebook/notifications/server/NotificationsChangeSeenStateParams;->b:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-object v0, v3

    .line 583152
    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a(Ljava/lang/Iterable;Lcom/facebook/graphql/enums/GraphQLStorySeenState;Z)I

    .line 583153
    :cond_4
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_3

    .line 583154
    :cond_5
    const-string v1, "fecthNotificationSeenStates"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 583155
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v1

    .line 583156
    iget-boolean v0, v1, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    move v0, v0

    .line 583157
    if-nez v0, :cond_9

    move-object v0, v1

    .line 583158
    :goto_4
    move-object v0, v0

    .line 583159
    goto :goto_3

    .line 583160
    :cond_6
    const/4 v3, 0x0

    goto/16 :goto_0

    :cond_7
    move-object v3, v4

    goto :goto_2

    :cond_8
    move-object v3, v4

    goto/16 :goto_1

    .line 583161
    :cond_9
    invoke-virtual {v1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/methods/FetchNotificationSeenStatesResult;

    .line 583162
    iget-object v2, p0, LX/3Su;->a:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    const/4 v4, 0x0

    .line 583163
    iget-object v3, v0, Lcom/facebook/notifications/protocol/methods/FetchNotificationSeenStatesResult;->a:Lcom/facebook/notifications/model/NotificationSeenStates;

    iget-object v6, v3, Lcom/facebook/notifications/model/NotificationSeenStates;->notificationSeenStatesList:LX/0Px;

    .line 583164
    new-instance p0, LX/2Q8;

    invoke-direct {p0}, LX/2Q8;-><init>()V

    .line 583165
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result p1

    move v5, v4

    :goto_5
    if-ge v5, p1, :cond_a

    invoke-virtual {v6, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/notifications/model/NotificationSeenStates$NotificationSeenState;

    .line 583166
    iget-object p2, v3, Lcom/facebook/notifications/model/NotificationSeenStates$NotificationSeenState;->seenState:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    iget-object v3, v3, Lcom/facebook/notifications/model/NotificationSeenStates$NotificationSeenState;->id:Ljava/lang/String;

    invoke-virtual {p0, p2, v3}, LX/2Q8;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/2Q8;

    .line 583167
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_5

    .line 583168
    :cond_a
    invoke-virtual {p0}, LX/2Q8;->b()LX/18f;

    move-result-object v3

    .line 583169
    iget-object v5, v3, LX/18f;->b:LX/0P1;

    move-object v3, v5

    .line 583170
    invoke-virtual {v3}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v3

    invoke-virtual {v3}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v5, v4

    :goto_6
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_b

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 583171
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Iterable;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    const/4 p0, 0x1

    invoke-virtual {v2, v4, v3, p0}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a(Ljava/lang/Iterable;Lcom/facebook/graphql/enums/GraphQLStorySeenState;Z)I

    move-result v3

    add-int/2addr v3, v5

    move v5, v3

    .line 583172
    goto :goto_6

    .line 583173
    :cond_b
    if-lez v5, :cond_c

    .line 583174
    iget-object v3, v2, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->f:Landroid/content/ContentResolver;

    iget-object v4, v2, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->e:LX/2A8;

    iget-object v4, v4, LX/2A8;->b:Landroid/net/Uri;

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v6}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 583175
    :cond_c
    move-object v0, v1

    .line 583176
    goto :goto_4
.end method
