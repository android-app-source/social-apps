.class public LX/3uk;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/3sU;",
            ">;"
        }
    .end annotation
.end field

.field private b:J

.field private c:Landroid/view/animation/Interpolator;

.field public d:LX/3oQ;

.field public e:Z

.field private final f:LX/3oR;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 649316
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 649317
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/3uk;->b:J

    .line 649318
    new-instance v0, LX/3uj;

    invoke-direct {v0, p0}, LX/3uj;-><init>(LX/3uk;)V

    iput-object v0, p0, LX/3uk;->f:LX/3oR;

    .line 649319
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/3uk;->a:Ljava/util/ArrayList;

    .line 649320
    return-void
.end method


# virtual methods
.method public final a(J)LX/3uk;
    .locals 1

    .prologue
    .line 649321
    iget-boolean v0, p0, LX/3uk;->e:Z

    if-nez v0, :cond_0

    .line 649322
    iput-wide p1, p0, LX/3uk;->b:J

    .line 649323
    :cond_0
    return-object p0
.end method

.method public final a(LX/3oQ;)LX/3uk;
    .locals 1

    .prologue
    .line 649324
    iget-boolean v0, p0, LX/3uk;->e:Z

    if-nez v0, :cond_0

    .line 649325
    iput-object p1, p0, LX/3uk;->d:LX/3oQ;

    .line 649326
    :cond_0
    return-object p0
.end method

.method public final a(LX/3sU;)LX/3uk;
    .locals 1

    .prologue
    .line 649327
    iget-boolean v0, p0, LX/3uk;->e:Z

    if-nez v0, :cond_0

    .line 649328
    iget-object v0, p0, LX/3uk;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 649329
    :cond_0
    return-object p0
.end method

.method public final a(Landroid/view/animation/Interpolator;)LX/3uk;
    .locals 1

    .prologue
    .line 649330
    iget-boolean v0, p0, LX/3uk;->e:Z

    if-nez v0, :cond_0

    .line 649331
    iput-object p1, p0, LX/3uk;->c:Landroid/view/animation/Interpolator;

    .line 649332
    :cond_0
    return-object p0
.end method

.method public final a()V
    .locals 6

    .prologue
    .line 649333
    iget-boolean v0, p0, LX/3uk;->e:Z

    if-eqz v0, :cond_0

    .line 649334
    :goto_0
    return-void

    .line 649335
    :cond_0
    iget-object v0, p0, LX/3uk;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3sU;

    .line 649336
    iget-wide v2, p0, LX/3uk;->b:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-ltz v2, :cond_1

    .line 649337
    iget-wide v2, p0, LX/3uk;->b:J

    invoke-virtual {v0, v2, v3}, LX/3sU;->a(J)LX/3sU;

    .line 649338
    :cond_1
    iget-object v2, p0, LX/3uk;->c:Landroid/view/animation/Interpolator;

    if-eqz v2, :cond_2

    .line 649339
    iget-object v2, p0, LX/3uk;->c:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v2}, LX/3sU;->a(Landroid/view/animation/Interpolator;)LX/3sU;

    .line 649340
    :cond_2
    iget-object v2, p0, LX/3uk;->d:LX/3oQ;

    if-eqz v2, :cond_3

    .line 649341
    iget-object v2, p0, LX/3uk;->f:LX/3oR;

    invoke-virtual {v0, v2}, LX/3sU;->a(LX/3oQ;)LX/3sU;

    .line 649342
    :cond_3
    invoke-virtual {v0}, LX/3sU;->b()V

    goto :goto_1

    .line 649343
    :cond_4
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/3uk;->e:Z

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 649344
    iget-boolean v0, p0, LX/3uk;->e:Z

    if-nez v0, :cond_0

    .line 649345
    :goto_0
    return-void

    .line 649346
    :cond_0
    iget-object v0, p0, LX/3uk;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3sU;

    .line 649347
    invoke-virtual {v0}, LX/3sU;->a()V

    goto :goto_1

    .line 649348
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/3uk;->e:Z

    goto :goto_0
.end method
