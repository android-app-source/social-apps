.class public LX/4MR;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 687838
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 20

    .prologue
    .line 687839
    const/4 v14, 0x0

    .line 687840
    const/4 v13, 0x0

    .line 687841
    const/4 v12, 0x0

    .line 687842
    const/4 v11, 0x0

    .line 687843
    const/4 v10, 0x0

    .line 687844
    const-wide/16 v8, 0x0

    .line 687845
    const/4 v7, 0x0

    .line 687846
    const/4 v6, 0x0

    .line 687847
    const/4 v5, 0x0

    .line 687848
    const/4 v4, 0x0

    .line 687849
    const/4 v3, 0x0

    .line 687850
    const/4 v2, 0x0

    .line 687851
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v15

    sget-object v16, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v16

    if-eq v15, v0, :cond_e

    .line 687852
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 687853
    const/4 v2, 0x0

    .line 687854
    :goto_0
    return v2

    .line 687855
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v16, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v16

    if-eq v3, v0, :cond_c

    .line 687856
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 687857
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 687858
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v16

    sget-object v17, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    if-eq v0, v1, :cond_0

    if-eqz v3, :cond_0

    .line 687859
    const-string v16, "cache_id"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_1

    .line 687860
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v15, v3

    goto :goto_1

    .line 687861
    :cond_1
    const-string v16, "debug_info"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_2

    .line 687862
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v14, v3

    goto :goto_1

    .line 687863
    :cond_2
    const-string v16, "eventsSuggestionItems"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_3

    .line 687864
    invoke-static/range {p0 .. p1}, LX/4MS;->a(LX/15w;LX/186;)I

    move-result v3

    move v13, v3

    goto :goto_1

    .line 687865
    :cond_3
    const-string v16, "eventsSuggestionTitle"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_4

    .line 687866
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v3

    move v7, v3

    goto :goto_1

    .line 687867
    :cond_4
    const-string v16, "events_suggestion_type"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_5

    .line 687868
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v6, v3

    goto :goto_1

    .line 687869
    :cond_5
    const-string v16, "fetchTimeMs"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_6

    .line 687870
    const/4 v2, 0x1

    .line 687871
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    goto/16 :goto_1

    .line 687872
    :cond_6
    const-string v16, "hideable_token"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_7

    .line 687873
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v12, v3

    goto/16 :goto_1

    .line 687874
    :cond_7
    const-string v16, "negative_feedback_actions"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_8

    .line 687875
    invoke-static/range {p0 .. p1}, LX/2bY;->a(LX/15w;LX/186;)I

    move-result v3

    move v11, v3

    goto/16 :goto_1

    .line 687876
    :cond_8
    const-string v16, "short_term_cache_key"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_9

    .line 687877
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v10, v3

    goto/16 :goto_1

    .line 687878
    :cond_9
    const-string v16, "title"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_a

    .line 687879
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v3

    move v9, v3

    goto/16 :goto_1

    .line 687880
    :cond_a
    const-string v16, "tracking"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 687881
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v8, v3

    goto/16 :goto_1

    .line 687882
    :cond_b
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 687883
    :cond_c
    const/16 v3, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 687884
    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v15}, LX/186;->b(II)V

    .line 687885
    const/4 v3, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v14}, LX/186;->b(II)V

    .line 687886
    const/4 v3, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v13}, LX/186;->b(II)V

    .line 687887
    const/4 v3, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 687888
    const/4 v3, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 687889
    if-eqz v2, :cond_d

    .line 687890
    const/4 v3, 0x5

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 687891
    :cond_d
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 687892
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 687893
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 687894
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 687895
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 687896
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_e
    move v15, v14

    move v14, v13

    move v13, v12

    move v12, v7

    move v7, v11

    move v11, v6

    move v6, v10

    move v10, v5

    move-wide/from16 v18, v8

    move v9, v4

    move v8, v3

    move-wide/from16 v4, v18

    goto/16 :goto_1
.end method

.method public static a(LX/15w;S)LX/15i;
    .locals 5

    .prologue
    .line 687897
    const/4 v0, 0x1

    const/4 v4, 0x0

    .line 687898
    new-instance v2, LX/186;

    const/16 v1, 0x80

    invoke-direct {v2, v1}, LX/186;-><init>(I)V

    .line 687899
    invoke-static {p0, v2}, LX/4MR;->a(LX/15w;LX/186;)I

    move-result v1

    .line 687900
    if-eqz v0, :cond_0

    .line 687901
    const/4 v3, 0x2

    invoke-virtual {v2, v3}, LX/186;->c(I)V

    .line 687902
    invoke-virtual {v2, v4, p1, v4}, LX/186;->a(ISI)V

    .line 687903
    const/4 v3, 0x1

    invoke-virtual {v2, v3, v1}, LX/186;->b(II)V

    .line 687904
    invoke-virtual {v2}, LX/186;->d()I

    move-result v1

    .line 687905
    :cond_0
    invoke-virtual {v2, v1}, LX/186;->d(I)V

    .line 687906
    invoke-static {v2}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v1

    move-object v0, v1

    .line 687907
    return-object v0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    const-wide/16 v2, 0x0

    .line 687908
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 687909
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 687910
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 687911
    const-string v0, "name"

    const-string v1, "EventsSuggestionFeedUnit"

    invoke-virtual {p2, v0, v1}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 687912
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 687913
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 687914
    if-eqz v0, :cond_0

    .line 687915
    const-string v1, "cache_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 687916
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 687917
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 687918
    if-eqz v0, :cond_1

    .line 687919
    const-string v1, "debug_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 687920
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 687921
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 687922
    if-eqz v0, :cond_3

    .line 687923
    const-string v1, "eventsSuggestionItems"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 687924
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 687925
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v4

    if-ge v1, v4, :cond_2

    .line 687926
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v4

    invoke-static {p0, v4, p2, p3}, LX/4MS;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 687927
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 687928
    :cond_2
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 687929
    :cond_3
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 687930
    if-eqz v0, :cond_4

    .line 687931
    const-string v1, "eventsSuggestionTitle"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 687932
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 687933
    :cond_4
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 687934
    if-eqz v0, :cond_5

    .line 687935
    const-string v1, "events_suggestion_type"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 687936
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 687937
    :cond_5
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 687938
    cmp-long v2, v0, v2

    if-eqz v2, :cond_6

    .line 687939
    const-string v2, "fetchTimeMs"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 687940
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 687941
    :cond_6
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 687942
    if-eqz v0, :cond_7

    .line 687943
    const-string v1, "hideable_token"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 687944
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 687945
    :cond_7
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 687946
    if-eqz v0, :cond_8

    .line 687947
    const-string v1, "negative_feedback_actions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 687948
    invoke-static {p0, v0, p2, p3}, LX/2bY;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 687949
    :cond_8
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 687950
    if-eqz v0, :cond_9

    .line 687951
    const-string v1, "short_term_cache_key"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 687952
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 687953
    :cond_9
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 687954
    if-eqz v0, :cond_a

    .line 687955
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 687956
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 687957
    :cond_a
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 687958
    if-eqz v0, :cond_b

    .line 687959
    const-string v1, "tracking"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 687960
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 687961
    :cond_b
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 687962
    return-void
.end method
