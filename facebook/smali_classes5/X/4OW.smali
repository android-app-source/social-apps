.class public LX/4OW;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 696973
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 18

    .prologue
    .line 696974
    const/4 v13, 0x0

    .line 696975
    const/4 v12, 0x0

    .line 696976
    const-wide/16 v10, 0x0

    .line 696977
    const/4 v9, 0x0

    .line 696978
    const/4 v8, 0x0

    .line 696979
    const/4 v7, 0x0

    .line 696980
    const/4 v6, 0x0

    .line 696981
    const/4 v5, 0x0

    .line 696982
    const/4 v4, 0x0

    .line 696983
    const/4 v3, 0x0

    .line 696984
    const/4 v2, 0x0

    .line 696985
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->START_OBJECT:LX/15z;

    if-eq v14, v15, :cond_d

    .line 696986
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 696987
    const/4 v2, 0x0

    .line 696988
    :goto_0
    return v2

    .line 696989
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v15, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v15, :cond_b

    .line 696990
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 696991
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 696992
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v15

    sget-object v16, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v16

    if-eq v15, v0, :cond_0

    if-eqz v3, :cond_0

    .line 696993
    const-string v15, "cache_id"

    invoke-virtual {v3, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_1

    .line 696994
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v7, v3

    goto :goto_1

    .line 696995
    :cond_1
    const-string v15, "debug_info"

    invoke-virtual {v3, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_2

    .line 696996
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v6, v3

    goto :goto_1

    .line 696997
    :cond_2
    const-string v15, "fetchTimeMs"

    invoke-virtual {v3, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 696998
    const/4 v2, 0x1

    .line 696999
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    goto :goto_1

    .line 697000
    :cond_3
    const-string v15, "gyscItems"

    invoke-virtual {v3, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_4

    .line 697001
    invoke-static/range {p0 .. p1}, LX/4OX;->a(LX/15w;LX/186;)I

    move-result v3

    move v14, v3

    goto :goto_1

    .line 697002
    :cond_4
    const-string v15, "gyscTitle"

    invoke-virtual {v3, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_5

    .line 697003
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v3

    move v13, v3

    goto :goto_1

    .line 697004
    :cond_5
    const-string v15, "hideable_token"

    invoke-virtual {v3, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_6

    .line 697005
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v12, v3

    goto :goto_1

    .line 697006
    :cond_6
    const-string v15, "items"

    invoke-virtual {v3, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_7

    .line 697007
    invoke-static/range {p0 .. p1}, LX/4OX;->a(LX/15w;LX/186;)I

    move-result v3

    move v11, v3

    goto/16 :goto_1

    .line 697008
    :cond_7
    const-string v15, "short_term_cache_key"

    invoke-virtual {v3, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_8

    .line 697009
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v10, v3

    goto/16 :goto_1

    .line 697010
    :cond_8
    const-string v15, "title"

    invoke-virtual {v3, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_9

    .line 697011
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v3

    move v9, v3

    goto/16 :goto_1

    .line 697012
    :cond_9
    const-string v15, "tracking"

    invoke-virtual {v3, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 697013
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v8, v3

    goto/16 :goto_1

    .line 697014
    :cond_a
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 697015
    :cond_b
    const/16 v3, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 697016
    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 697017
    const/4 v3, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 697018
    if-eqz v2, :cond_c

    .line 697019
    const/4 v3, 0x2

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 697020
    :cond_c
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 697021
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 697022
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 697023
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 697024
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 697025
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 697026
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 697027
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_d
    move v14, v9

    move v9, v4

    move/from16 v17, v5

    move-wide v4, v10

    move v11, v6

    move/from16 v10, v17

    move v6, v12

    move v12, v7

    move v7, v13

    move v13, v8

    move v8, v3

    goto/16 :goto_1
.end method

.method public static a(LX/15w;S)LX/15i;
    .locals 5

    .prologue
    .line 696962
    const/4 v0, 0x1

    const/4 v4, 0x0

    .line 696963
    new-instance v2, LX/186;

    const/16 v1, 0x80

    invoke-direct {v2, v1}, LX/186;-><init>(I)V

    .line 696964
    invoke-static {p0, v2}, LX/4OW;->a(LX/15w;LX/186;)I

    move-result v1

    .line 696965
    if-eqz v0, :cond_0

    .line 696966
    const/4 v3, 0x2

    invoke-virtual {v2, v3}, LX/186;->c(I)V

    .line 696967
    invoke-virtual {v2, v4, p1, v4}, LX/186;->a(ISI)V

    .line 696968
    const/4 v3, 0x1

    invoke-virtual {v2, v3, v1}, LX/186;->b(II)V

    .line 696969
    invoke-virtual {v2}, LX/186;->d()I

    move-result v1

    .line 696970
    :cond_0
    invoke-virtual {v2, v1}, LX/186;->d(I)V

    .line 696971
    invoke-static {v2}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v1

    move-object v0, v1

    .line 696972
    return-object v0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 696915
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 696916
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 696917
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 696918
    const-string v0, "name"

    const-string v1, "GroupsYouShouldCreateFeedUnit"

    invoke-virtual {p2, v0, v1}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 696919
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 696920
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 696921
    if-eqz v0, :cond_0

    .line 696922
    const-string v1, "cache_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 696923
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 696924
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 696925
    if-eqz v0, :cond_1

    .line 696926
    const-string v1, "debug_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 696927
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 696928
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 696929
    cmp-long v2, v0, v2

    if-eqz v2, :cond_2

    .line 696930
    const-string v2, "fetchTimeMs"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 696931
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 696932
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 696933
    if-eqz v0, :cond_3

    .line 696934
    const-string v1, "gyscItems"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 696935
    invoke-static {p0, v0, p2, p3}, LX/4OX;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 696936
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 696937
    if-eqz v0, :cond_4

    .line 696938
    const-string v1, "gyscTitle"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 696939
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 696940
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 696941
    if-eqz v0, :cond_5

    .line 696942
    const-string v1, "hideable_token"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 696943
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 696944
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 696945
    if-eqz v0, :cond_6

    .line 696946
    const-string v1, "items"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 696947
    invoke-static {p0, v0, p2, p3}, LX/4OX;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 696948
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 696949
    if-eqz v0, :cond_7

    .line 696950
    const-string v1, "short_term_cache_key"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 696951
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 696952
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 696953
    if-eqz v0, :cond_8

    .line 696954
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 696955
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 696956
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 696957
    if-eqz v0, :cond_9

    .line 696958
    const-string v1, "tracking"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 696959
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 696960
    :cond_9
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 696961
    return-void
.end method
