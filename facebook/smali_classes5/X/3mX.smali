.class public abstract LX/3mX;
.super LX/3mY;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<PageProp:",
        "Ljava/lang/Object;",
        "E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/3mY",
        "<",
        "Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;",
        "LX/3md;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<TPageProp;>;"
        }
    .end annotation
.end field

.field public final b:LX/1Pq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field private final c:I

.field private final d:LX/3mg;

.field private final e:LX/1OX;

.field public final f:LX/25M;

.field public g:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

.field public h:Z

.field public i:LX/25c;

.field public j:I

.field public k:LX/8yz;

.field public l:LX/3mj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/3mX",
            "<TPageProp;TE;>.FeedHScrollController;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Px;LX/1Pq;LX/25M;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Px",
            "<TPageProp;>;TE;",
            "LX/25M;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 636481
    new-instance v0, LX/3md;

    invoke-direct {v0}, LX/3md;-><init>()V

    invoke-direct {p0, p1, v0}, LX/3mY;-><init>(Landroid/content/Context;LX/3me;)V

    .line 636482
    iput v1, p0, LX/3mX;->j:I

    .line 636483
    new-instance v0, LX/3mg;

    invoke-direct {v0, p1, p0}, LX/3mg;-><init>(Landroid/content/Context;LX/3mX;)V

    iput-object v0, p0, LX/3mX;->d:LX/3mg;

    .line 636484
    new-instance v0, LX/3mi;

    invoke-direct {v0, p0}, LX/3mi;-><init>(LX/3mX;)V

    iput-object v0, p0, LX/3mX;->e:LX/1OX;

    .line 636485
    iput-object p4, p0, LX/3mX;->f:LX/25M;

    .line 636486
    iget-object v0, p0, LX/3mX;->d:LX/3mg;

    .line 636487
    iput-object v0, p0, LX/3mY;->h:LX/3mh;

    .line 636488
    iput-object p2, p0, LX/3mX;->a:LX/0Px;

    .line 636489
    iget-object v0, p0, LX/3mX;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    iget-object v0, p0, LX/3mX;->f:LX/25M;

    iget-boolean v0, v0, LX/25M;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v2

    iput v0, p0, LX/3mX;->c:I

    .line 636490
    iput-object p3, p0, LX/3mX;->b:LX/1Pq;

    .line 636491
    new-instance v0, LX/3mj;

    invoke-direct {v0, p0}, LX/3mj;-><init>(LX/3mX;)V

    iput-object v0, p0, LX/3mX;->l:LX/3mj;

    .line 636492
    return-void

    :cond_0
    move v0, v1

    .line 636493
    goto :goto_0
.end method

.method private c(Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;)V
    .locals 2

    .prologue
    .line 636494
    invoke-static {p0}, LX/3mX;->l(LX/3mX;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 636495
    :goto_0
    return-void

    .line 636496
    :cond_0
    iget-object v0, p0, LX/3mX;->i:LX/25c;

    .line 636497
    iget v1, v0, LX/25c;->a:I

    move v0, v1

    .line 636498
    iget-object v1, p0, LX/3mX;->i:LX/25c;

    .line 636499
    iget p0, v1, LX/25c;->b:I

    move v1, p0

    .line 636500
    invoke-virtual {p1, v0, v1}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->i(II)V

    goto :goto_0
.end method

.method public static d(LX/3mX;Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 636501
    invoke-static {p0}, LX/3mX;->l(LX/3mX;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 636502
    :cond_0
    :goto_0
    return-void

    .line 636503
    :cond_1
    iget v0, p0, LX/3mX;->j:I

    if-nez v0, :cond_2

    iget-object v0, p0, LX/3mX;->i:LX/25c;

    .line 636504
    iget v1, v0, LX/25c;->a:I

    move v0, v1

    .line 636505
    if-nez v0, :cond_0

    :cond_2
    iget-object v0, p0, LX/3mX;->g:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    .line 636506
    iget v1, v0, Landroid/support/v7/widget/RecyclerView;->M:I

    move v0, v1

    .line 636507
    if-nez v0, :cond_0

    .line 636508
    iget-object v0, p0, LX/3mX;->i:LX/25c;

    .line 636509
    iget v1, v0, LX/25c;->c:I

    move v0, v1

    .line 636510
    iget v1, p0, LX/3mX;->j:I

    if-ne v0, v1, :cond_3

    .line 636511
    iget-object v0, p0, LX/3mX;->i:LX/25c;

    .line 636512
    iput v2, v0, LX/25c;->c:I

    .line 636513
    goto :goto_0

    .line 636514
    :cond_3
    if-eq v0, v2, :cond_0

    iget-object v1, p0, LX/3mX;->i:LX/25c;

    .line 636515
    iget v2, v1, LX/25c;->a:I

    move v1, v2

    .line 636516
    if-eq v0, v1, :cond_0

    .line 636517
    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->b(IZ)V

    goto :goto_0
.end method

.method public static h(LX/3mX;I)V
    .locals 2

    .prologue
    .line 636518
    invoke-static {p0}, LX/3mX;->l(LX/3mX;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 636519
    :goto_0
    return-void

    .line 636520
    :cond_0
    iget-object v0, p0, LX/3mX;->i:LX/25c;

    .line 636521
    iput p1, v0, LX/25c;->a:I

    .line 636522
    iget-object v0, p0, LX/3mX;->i:LX/25c;

    iget-object v1, p0, LX/3mX;->g:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    invoke-virtual {v1}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->getOffset()I

    move-result v1

    .line 636523
    iput v1, v0, LX/25c;->b:I

    .line 636524
    goto :goto_0
.end method

.method public static l(LX/3mX;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 636464
    iget-object v0, p0, LX/3mX;->f:LX/25M;

    iget-object v0, v0, LX/25M;->d:LX/25L;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3mX;->f:LX/25M;

    iget-object v0, v0, LX/25M;->e:LX/0jW;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3mX;->i:LX/25c;

    if-nez v0, :cond_0

    .line 636465
    iget-object v0, p0, LX/3mX;->b:LX/1Pq;

    check-cast v0, LX/1Pr;

    iget-object v2, p0, LX/3mX;->f:LX/25M;

    iget-object v2, v2, LX/25M;->d:LX/25L;

    iget-object v3, p0, LX/3mX;->f:LX/25M;

    iget-object v3, v3, LX/25M;->e:LX/0jW;

    invoke-interface {v0, v2, v3}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/25c;

    iput-object v0, p0, LX/3mX;->i:LX/25c;

    move v0, v1

    .line 636466
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/3mX;->i:LX/25c;

    if-eqz v0, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public abstract a(LX/1De;)LX/1X1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation
.end method

.method public final a(LX/1De;I)LX/1X1;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "I)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 636531
    iget-object v0, p0, LX/3mX;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ne p2, v0, :cond_0

    iget-object v0, p0, LX/3mX;->f:LX/25M;

    iget-boolean v0, v0, LX/25M;->a:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, LX/3mX;->a(LX/1De;)LX/1X1;

    move-result-object v0

    .line 636532
    :goto_0
    iget-object v1, p0, LX/3mX;->f:LX/25M;

    iget v2, v1, LX/25M;->b:I

    .line 636533
    iget-object v1, p0, LX/3mX;->f:LX/25M;

    iget-object v1, v1, LX/25M;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/3mX;->f:LX/25M;

    iget-object v1, v1, LX/25M;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 636534
    :goto_1
    if-nez v2, :cond_2

    .line 636535
    :goto_2
    return-object v0

    .line 636536
    :cond_0
    iget-object v0, p0, LX/3mX;->a:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, LX/3mX;->a(LX/1De;Ljava/lang/Object;)LX/1X1;

    move-result-object v0

    goto :goto_0

    :cond_1
    move v1, v2

    .line 636537
    goto :goto_1

    .line 636538
    :cond_2
    const/4 v3, 0x0

    .line 636539
    new-instance v4, LX/3mu;

    invoke-direct {v4}, LX/3mu;-><init>()V

    .line 636540
    sget-object v5, LX/3mt;->b:LX/0Zi;

    invoke-virtual {v5}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/3mw;

    .line 636541
    if-nez v5, :cond_3

    .line 636542
    new-instance v5, LX/3mw;

    invoke-direct {v5}, LX/3mw;-><init>()V

    .line 636543
    :cond_3
    invoke-static {v5, p1, v3, v3, v4}, LX/3mw;->a$redex0(LX/3mw;LX/1De;IILX/3mu;)V

    .line 636544
    move-object v4, v5

    .line 636545
    move-object v3, v4

    .line 636546
    move-object v3, v3

    .line 636547
    if-nez p2, :cond_4

    .line 636548
    :goto_3
    iget-object v4, v3, LX/3mw;->a:LX/3mu;

    iput v1, v4, LX/3mu;->b:I

    .line 636549
    iget-object v4, v3, LX/3mw;->d:Ljava/util/BitSet;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Ljava/util/BitSet;->set(I)V

    .line 636550
    move-object v3, v3

    .line 636551
    iget v1, p0, LX/3mX;->c:I

    add-int/lit8 v1, v1, -0x1

    if-ne p2, v1, :cond_5

    move v1, v2

    .line 636552
    :goto_4
    iget-object v2, v3, LX/3mw;->a:LX/3mu;

    iput v1, v2, LX/3mu;->c:I

    .line 636553
    iget-object v2, v3, LX/3mw;->d:Ljava/util/BitSet;

    const/4 v4, 0x2

    invoke-virtual {v2, v4}, Ljava/util/BitSet;->set(I)V

    .line 636554
    move-object v1, v3

    .line 636555
    iget-object v3, v1, LX/3mw;->a:LX/3mu;

    if-nez v0, :cond_6

    const/4 v2, 0x0

    :goto_5
    iput-object v2, v3, LX/3mu;->a:LX/1X1;

    .line 636556
    iget-object v2, v1, LX/3mw;->d:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 636557
    move-object v0, v1

    .line 636558
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    goto :goto_2

    :cond_4
    div-int/lit8 v1, v2, 0x2

    goto :goto_3

    :cond_5
    div-int/lit8 v1, v2, 0x2

    goto :goto_4

    .line 636559
    :cond_6
    invoke-virtual {v0}, LX/1X1;->g()LX/1X1;

    move-result-object v2

    goto :goto_5
.end method

.method public abstract a(LX/1De;Ljava/lang/Object;)LX/1X1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "TPageProp;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation
.end method

.method public a(Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;)V
    .locals 1

    .prologue
    .line 636525
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/3mX;->h:Z

    .line 636526
    iput-object p1, p0, LX/3mX;->g:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    .line 636527
    iget-object v0, p0, LX/3mX;->d:LX/3mg;

    invoke-virtual {p1, v0}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 636528
    invoke-direct {p0, p1}, LX/3mX;->c(Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;)V

    .line 636529
    iget-object v0, p0, LX/3mX;->e:LX/1OX;

    invoke-virtual {p1, v0}, Landroid/support/v7/widget/RecyclerView;->setOnScrollListener(LX/1OX;)V

    .line 636530
    return-void
.end method

.method public b(Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 636474
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v0

    check-cast v0, LX/25T;

    .line 636475
    invoke-virtual {v0}, LX/1P1;->l()I

    move-result v0

    .line 636476
    invoke-static {p0, v0}, LX/3mX;->h(LX/3mX;I)V

    .line 636477
    iget-object v0, p0, LX/3mX;->g:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setOnScrollListener(LX/1OX;)V

    .line 636478
    iget-object v0, p0, LX/3mX;->g:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 636479
    iput-object v1, p0, LX/3mX;->g:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    .line 636480
    return-void
.end method

.method public final b(III)Z
    .locals 1

    .prologue
    .line 636473
    const/4 v0, 0x1

    return v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 636472
    iget v0, p0, LX/3mX;->c:I

    return v0
.end method

.method public synthetic e(Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 636471
    check-cast p1, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    invoke-virtual {p0, p1}, LX/3mX;->a(Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;)V

    return-void
.end method

.method public final f()V
    .locals 3

    .prologue
    .line 636469
    const/4 v0, 0x0

    invoke-virtual {p0}, LX/3mX;->e()I

    move-result v1

    const/4 v2, 0x3

    invoke-virtual {p0, v0, v1, v2}, LX/3mY;->a(III)V

    .line 636470
    return-void
.end method

.method public synthetic f(Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 636468
    check-cast p1, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    invoke-virtual {p0, p1}, LX/3mX;->b(Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;)V

    return-void
.end method

.method public g(I)V
    .locals 0

    .prologue
    .line 636467
    return-void
.end method
