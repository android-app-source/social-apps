.class public LX/3iO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2Pa;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/3iO;


# instance fields
.field private final a:Ljava/util/concurrent/Executor;

.field public final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/3iN;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;)V
    .locals 1
    .param p1    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 629251
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 629252
    iput-object p1, p0, LX/3iO;->a:Ljava/util/concurrent/Executor;

    .line 629253
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/3iO;->b:Ljava/util/Set;

    .line 629254
    return-void
.end method

.method public static a(LX/0QB;)LX/3iO;
    .locals 4

    .prologue
    .line 629255
    sget-object v0, LX/3iO;->c:LX/3iO;

    if-nez v0, :cond_1

    .line 629256
    const-class v1, LX/3iO;

    monitor-enter v1

    .line 629257
    :try_start_0
    sget-object v0, LX/3iO;->c:LX/3iO;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 629258
    if-eqz v2, :cond_0

    .line 629259
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 629260
    new-instance p0, LX/3iO;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/Executor;

    invoke-direct {p0, v3}, LX/3iO;-><init>(Ljava/util/concurrent/Executor;)V

    .line 629261
    move-object v0, p0

    .line 629262
    sput-object v0, LX/3iO;->c:LX/3iO;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 629263
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 629264
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 629265
    :cond_1
    sget-object v0, LX/3iO;->c:LX/3iO;

    return-object v0

    .line 629266
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 629267
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Ljava/util/concurrent/Executor;
    .locals 1

    .prologue
    .line 629268
    iget-object v0, p0, LX/3iO;->a:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method public final a(LX/3iN;)V
    .locals 2

    .prologue
    .line 629269
    iget-object v1, p0, LX/3iO;->b:Ljava/util/Set;

    monitor-enter v1

    .line 629270
    :try_start_0
    iget-object v0, p0, LX/3iO;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 629271
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(LX/3G4;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 629272
    iget-object v0, p1, LX/3G4;->h:Ljava/lang/Class;

    const-class v2, LX/58m;

    if-eq v0, v2, :cond_0

    move v0, v1

    .line 629273
    :goto_0
    return v0

    .line 629274
    :cond_0
    iget-object v0, p1, LX/3G4;->j:LX/0jT;

    check-cast v0, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;

    .line 629275
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;->a()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v0

    if-nez v0, :cond_2

    :cond_1
    move v0, v1

    .line 629276
    goto :goto_0

    .line 629277
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b(LX/3G4;)LX/0TF;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/3G4;",
            ")",
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 629278
    iget-object v0, p1, LX/3G4;->j:LX/0jT;

    check-cast v0, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;->a()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->H()Ljava/lang/String;

    move-result-object v0

    .line 629279
    new-instance v1, LX/8DV;

    invoke-direct {v1, p0, v0}, LX/8DV;-><init>(LX/3iO;Ljava/lang/String;)V

    return-object v1
.end method

.method public final b(LX/3iN;)V
    .locals 2

    .prologue
    .line 629280
    iget-object v1, p0, LX/3iO;->b:Ljava/util/Set;

    monitor-enter v1

    .line 629281
    :try_start_0
    iget-object v0, p0, LX/3iO;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 629282
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
