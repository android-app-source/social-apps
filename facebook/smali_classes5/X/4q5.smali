.class public final LX/4q5;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/2Ay;

.field public final b:Ljava/lang/reflect/Method;

.field public final c:LX/0lJ;

.field public d:Lcom/fasterxml/jackson/databind/JsonDeserializer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/2Ay;LX/2At;LX/0lJ;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2Ay;",
            "LX/2At;",
            "LX/0lJ;",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 812602
    iget-object v0, p2, LX/2At;->a:Ljava/lang/reflect/Method;

    move-object v0, v0

    .line 812603
    invoke-direct {p0, p1, v0, p3, p4}, LX/4q5;-><init>(LX/2Ay;Ljava/lang/reflect/Method;LX/0lJ;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 812604
    return-void
.end method

.method private constructor <init>(LX/2Ay;Ljava/lang/reflect/Method;LX/0lJ;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2Ay;",
            "Ljava/lang/reflect/Method;",
            "LX/0lJ;",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 812596
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 812597
    iput-object p1, p0, LX/4q5;->a:LX/2Ay;

    .line 812598
    iput-object p3, p0, LX/4q5;->c:LX/0lJ;

    .line 812599
    iput-object p2, p0, LX/4q5;->b:Ljava/lang/reflect/Method;

    .line 812600
    iput-object p4, p0, LX/4q5;->d:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    .line 812601
    return-void
.end method

.method private a(Ljava/lang/Exception;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 812578
    instance-of v0, p1, Ljava/lang/IllegalArgumentException;

    if-eqz v0, :cond_2

    .line 812579
    if-nez p3, :cond_0

    const-string v0, "[NULL]"

    .line 812580
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Problem deserializing \"any\" property \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 812581
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "\' of class "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct {p0}, LX/4q5;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " (expected type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, LX/4q5;->c:LX/0lJ;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 812582
    const-string v2, "; actual type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 812583
    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    .line 812584
    if-eqz v0, :cond_1

    .line 812585
    const-string v2, ", problem: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 812586
    :goto_1
    new-instance v0, LX/28E;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v4, p1}, LX/28E;-><init>(Ljava/lang/String;LX/28G;Ljava/lang/Throwable;)V

    throw v0

    .line 812587
    :cond_0
    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 812588
    :cond_1
    const-string v0, " (no error message provided)"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 812589
    :cond_2
    instance-of v0, p1, Ljava/io/IOException;

    if-eqz v0, :cond_3

    .line 812590
    check-cast p1, Ljava/io/IOException;

    throw p1

    .line 812591
    :cond_3
    instance-of v0, p1, Ljava/lang/RuntimeException;

    if-eqz v0, :cond_4

    .line 812592
    check-cast p1, Ljava/lang/RuntimeException;

    throw p1

    .line 812593
    :cond_4
    :goto_2
    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 812594
    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object p1

    goto :goto_2

    .line 812595
    :cond_5
    new-instance v0, LX/28E;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v4, p1}, LX/28E;-><init>(Ljava/lang/String;LX/28G;Ljava/lang/Throwable;)V

    throw v0
.end method

.method private d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 812577
    iget-object v0, p0, LX/4q5;->b:Ljava/lang/reflect/Method;

    invoke-virtual {v0}, Ljava/lang/reflect/Method;->getDeclaringClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/fasterxml/jackson/databind/JsonDeserializer;)LX/4q5;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<",
            "Ljava/lang/Object;",
            ">;)",
            "LX/4q5;"
        }
    .end annotation

    .prologue
    .line 812564
    new-instance v0, LX/4q5;

    iget-object v1, p0, LX/4q5;->a:LX/2Ay;

    iget-object v2, p0, LX/4q5;->b:Ljava/lang/reflect/Method;

    iget-object v3, p0, LX/4q5;->c:LX/0lJ;

    invoke-direct {v0, v1, v2, v3, p1}, LX/4q5;-><init>(LX/2Ay;Ljava/lang/reflect/Method;LX/0lJ;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    return-object v0
.end method

.method public final a(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 812573
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v0

    .line 812574
    sget-object v1, LX/15z;->VALUE_NULL:LX/15z;

    if-ne v0, v1, :cond_0

    .line 812575
    const/4 v0, 0x0

    .line 812576
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/4q5;->d:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    invoke-virtual {v0, p1, p2}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/15w;LX/0n3;Ljava/lang/Object;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 812571
    invoke-virtual {p0, p1, p2}, LX/4q5;->a(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, p3, p4, v0}, LX/4q5;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 812572
    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 812567
    :try_start_0
    iget-object v0, p0, LX/4q5;->b:Ljava/lang/reflect/Method;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    const/4 v2, 0x1

    aput-object p3, v1, v2

    invoke-virtual {v0, p1, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 812568
    :goto_0
    return-void

    .line 812569
    :catch_0
    move-exception v0

    .line 812570
    invoke-direct {p0, v0, p2, p3}, LX/4q5;->a(Ljava/lang/Exception;Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 812566
    iget-object v0, p0, LX/4q5;->d:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 812565
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[any property on class "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct {p0}, LX/4q5;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
