.class public LX/3ho;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile g:LX/3ho;


# instance fields
.field public final a:LX/0tX;

.field public final b:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/3hW;

.field public final d:I

.field private final e:I

.field public final f:I


# direct methods
.method public constructor <init>(LX/0tX;LX/1Ck;LX/3hW;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 628122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 628123
    iput-object p1, p0, LX/3ho;->a:LX/0tX;

    .line 628124
    iput-object p2, p0, LX/3ho;->b:LX/1Ck;

    .line 628125
    iput-object p3, p0, LX/3ho;->c:LX/3hW;

    .line 628126
    iget-object v0, p0, LX/3ho;->c:LX/3hW;

    .line 628127
    iget-object p1, v0, LX/3hW;->d:Ljava/lang/Integer;

    if-nez p1, :cond_0

    .line 628128
    iget-object p1, v0, LX/3hW;->a:LX/0ad;

    sget p2, LX/3hX;->d:I

    const/4 p3, 0x5

    invoke-interface {p1, p2, p3}, LX/0ad;->a(II)I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, v0, LX/3hW;->d:Ljava/lang/Integer;

    .line 628129
    :cond_0
    iget-object p1, v0, LX/3hW;->d:Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    move v0, p1

    .line 628130
    iput v0, p0, LX/3ho;->d:I

    .line 628131
    iget-object v0, p0, LX/3ho;->c:LX/3hW;

    .line 628132
    iget-object p1, v0, LX/3hW;->e:Ljava/lang/Integer;

    if-nez p1, :cond_1

    .line 628133
    iget-object p1, v0, LX/3hW;->a:LX/0ad;

    sget p2, LX/3hX;->a:I

    const/4 p3, 0x4

    invoke-interface {p1, p2, p3}, LX/0ad;->a(II)I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, v0, LX/3hW;->e:Ljava/lang/Integer;

    .line 628134
    :cond_1
    iget-object p1, v0, LX/3hW;->e:Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    move v0, p1

    .line 628135
    iput v0, p0, LX/3ho;->e:I

    .line 628136
    iget-object v0, p0, LX/3ho;->c:LX/3hW;

    .line 628137
    iget-object p1, v0, LX/3hW;->f:Ljava/lang/Integer;

    if-nez p1, :cond_2

    .line 628138
    iget-object p1, v0, LX/3hW;->a:LX/0ad;

    sget p2, LX/3hX;->c:I

    const/16 p3, 0x32

    invoke-interface {p1, p2, p3}, LX/0ad;->a(II)I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, v0, LX/3hW;->f:Ljava/lang/Integer;

    .line 628139
    :cond_2
    iget-object p1, v0, LX/3hW;->f:Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    move v0, p1

    .line 628140
    iput v0, p0, LX/3ho;->f:I

    .line 628141
    return-void
.end method

.method public static a(LX/0QB;)LX/3ho;
    .locals 6

    .prologue
    .line 628142
    sget-object v0, LX/3ho;->g:LX/3ho;

    if-nez v0, :cond_1

    .line 628143
    const-class v1, LX/3ho;

    monitor-enter v1

    .line 628144
    :try_start_0
    sget-object v0, LX/3ho;->g:LX/3ho;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 628145
    if-eqz v2, :cond_0

    .line 628146
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 628147
    new-instance p0, LX/3ho;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v4

    check-cast v4, LX/1Ck;

    invoke-static {v0}, LX/3hW;->a(LX/0QB;)LX/3hW;

    move-result-object v5

    check-cast v5, LX/3hW;

    invoke-direct {p0, v3, v4, v5}, LX/3ho;-><init>(LX/0tX;LX/1Ck;LX/3hW;)V

    .line 628148
    move-object v0, p0

    .line 628149
    sput-object v0, LX/3ho;->g:LX/3ho;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 628150
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 628151
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 628152
    :cond_1
    sget-object v0, LX/3ho;->g:LX/3ho;

    return-object v0

    .line 628153
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 628154
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/3ho;IILX/3he;)Z
    .locals 2

    .prologue
    .line 628155
    sub-int v0, p1, p2

    iget v1, p0, LX/3ho;->e:I

    if-gt v0, v1, :cond_0

    .line 628156
    iget-object v0, p3, LX/3he;->b:LX/3hc;

    .line 628157
    iget-boolean p3, v0, LX/3hc;->c:Z

    move v0, p3

    .line 628158
    move v0, v0

    .line 628159
    move v0, v0

    .line 628160
    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(LX/3he;)V
    .locals 2

    .prologue
    .line 628161
    invoke-static {p0}, LX/3ho;->e(LX/3he;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 628162
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/3he;->c(LX/3he;Z)V

    .line 628163
    invoke-static {p0}, LX/3he;->o(LX/3he;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 628164
    :cond_0
    :goto_0
    invoke-virtual {p0}, LX/3he;->c()V

    .line 628165
    const/4 v0, 0x0

    .line 628166
    iget-object v1, p0, LX/3he;->b:LX/3hc;

    .line 628167
    iput-boolean v0, v1, LX/3hc;->f:Z

    .line 628168
    :cond_1
    return-void

    .line 628169
    :cond_2
    invoke-static {p0}, LX/3he;->p(LX/3he;)LX/3hq;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 628170
    invoke-static {p0}, LX/3he;->p(LX/3he;)LX/3hq;

    move-result-object v0

    invoke-static {p0, v0}, LX/3he;->b(LX/3he;LX/3hq;)V

    .line 628171
    const/4 v0, 0x1

    .line 628172
    iget-object v1, p0, LX/3he;->b:LX/3hc;

    .line 628173
    iput-boolean v0, v1, LX/3hc;->e:Z

    .line 628174
    goto :goto_0
.end method

.method public static e(LX/3he;)Z
    .locals 1

    .prologue
    .line 628175
    iget-object v0, p0, LX/3he;->b:LX/3hc;

    .line 628176
    iget-boolean p0, v0, LX/3hc;->f:Z

    move v0, p0

    .line 628177
    move v0, v0

    .line 628178
    return v0
.end method

.method public static g(LX/3he;)V
    .locals 1

    .prologue
    .line 628179
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/3he;->a(Z)V

    .line 628180
    return-void
.end method
