.class public abstract LX/3rR;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field public b:LX/3rP;

.field private c:LX/3rQ;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 643025
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 643026
    iput-object p1, p0, LX/3rR;->a:Landroid/content/Context;

    .line 643027
    return-void
.end method


# virtual methods
.method public abstract a()Landroid/view/View;
.end method

.method public a(Landroid/view/MenuItem;)Landroid/view/View;
    .locals 1

    .prologue
    .line 643024
    invoke-virtual {p0}, LX/3rR;->a()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public a(LX/3rQ;)V
    .locals 3

    .prologue
    .line 643020
    iget-object v0, p0, LX/3rR;->c:LX/3rQ;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 643021
    const-string v0, "ActionProvider(support)"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setVisibilityListener: Setting a new ActionProvider.VisibilityListener when one is already set. Are you reusing this "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " instance while it is still in use somewhere else?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 643022
    :cond_0
    iput-object p1, p0, LX/3rR;->c:LX/3rQ;

    .line 643023
    return-void
.end method

.method public a(Landroid/view/SubMenu;)V
    .locals 0

    .prologue
    .line 643028
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 643017
    iget-object v0, p0, LX/3rR;->b:LX/3rP;

    if-eqz v0, :cond_0

    .line 643018
    iget-object v0, p0, LX/3rR;->b:LX/3rP;

    invoke-interface {v0, p1}, LX/3rP;->a(Z)V

    .line 643019
    :cond_0
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 643016
    const/4 v0, 0x0

    return v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 643013
    const/4 v0, 0x1

    return v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 643015
    const/4 v0, 0x0

    return v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 643014
    const/4 v0, 0x0

    return v0
.end method
