.class public LX/4UV;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 723112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 12

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 723084
    const/4 v0, 0x0

    .line 723085
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v4, :cond_9

    .line 723086
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 723087
    :goto_0
    return v1

    .line 723088
    :cond_0
    const-string v10, "rating"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 723089
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v3

    move v7, v3

    move v3, v2

    .line 723090
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_6

    .line 723091
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 723092
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 723093
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_1

    if-eqz v9, :cond_1

    .line 723094
    const-string v10, "page"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 723095
    invoke-static {p0, p1}, LX/2bc;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 723096
    :cond_2
    const-string v10, "title"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 723097
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 723098
    :cond_3
    const-string v10, "url"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 723099
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 723100
    :cond_4
    const-string v10, "link_type"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 723101
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v0

    move-object v4, v0

    move v0, v2

    goto :goto_1

    .line 723102
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 723103
    :cond_6
    const/4 v9, 0x5

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 723104
    invoke-virtual {p1, v1, v8}, LX/186;->b(II)V

    .line 723105
    if-eqz v3, :cond_7

    .line 723106
    invoke-virtual {p1, v2, v7, v1}, LX/186;->a(III)V

    .line 723107
    :cond_7
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 723108
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 723109
    if-eqz v0, :cond_8

    .line 723110
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->a(ILjava/lang/Enum;)V

    .line 723111
    :cond_8
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_9
    move v3, v1

    move-object v4, v0

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    move v0, v1

    goto/16 :goto_1
.end method
