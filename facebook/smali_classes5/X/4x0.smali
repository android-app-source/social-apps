.class public abstract LX/4x0;
.super LX/1M4;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "LX/1M4",
        "<TE;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 820690
    invoke-direct {p0}, LX/1M4;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a()LX/1M1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1M1",
            "<TE;>;"
        }
    .end annotation
.end method

.method public final clear()V
    .locals 1

    .prologue
    .line 820688
    invoke-virtual {p0}, LX/4x0;->a()LX/1M1;

    move-result-object v0

    invoke-interface {v0}, LX/1M1;->clear()V

    .line 820689
    return-void
.end method

.method public final contains(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 820687
    invoke-virtual {p0}, LX/4x0;->a()LX/1M1;

    move-result-object v0

    invoke-interface {v0, p1}, LX/1M1;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final containsAll(Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 820691
    invoke-virtual {p0}, LX/4x0;->a()LX/1M1;

    move-result-object v0

    invoke-interface {v0, p1}, LX/1M1;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public final isEmpty()Z
    .locals 1

    .prologue
    .line 820686
    invoke-virtual {p0}, LX/4x0;->a()LX/1M1;

    move-result-object v0

    invoke-interface {v0}, LX/1M1;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 820685
    new-instance v0, LX/50D;

    invoke-virtual {p0}, LX/4x0;->a()LX/1M1;

    move-result-object v1

    invoke-interface {v1}, LX/1M1;->a()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LX/50D;-><init>(LX/4x0;Ljava/util/Iterator;)V

    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 820684
    invoke-virtual {p0}, LX/4x0;->a()LX/1M1;

    move-result-object v0

    const v1, 0x7fffffff

    invoke-interface {v0, p1, v1}, LX/1M1;->b(Ljava/lang/Object;I)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 820683
    invoke-virtual {p0}, LX/4x0;->a()LX/1M1;

    move-result-object v0

    invoke-interface {v0}, LX/1M1;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    return v0
.end method
