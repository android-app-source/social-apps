.class public final LX/4wy;
.super LX/4wx;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/4wx",
        "<TE;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/util/Map$Entry;

.field public final synthetic b:LX/1M5;


# direct methods
.method public constructor <init>(LX/1M5;Ljava/util/Map$Entry;)V
    .locals 0

    .prologue
    .line 820659
    iput-object p1, p0, LX/4wy;->b:LX/1M5;

    iput-object p2, p0, LX/4wy;->a:Ljava/util/Map$Entry;

    invoke-direct {p0}, LX/4wx;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 820658
    iget-object v0, p0, LX/4wy;->a:Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final b()I
    .locals 3

    .prologue
    .line 820648
    iget-object v0, p0, LX/4wy;->a:Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4xL;

    .line 820649
    if-eqz v0, :cond_0

    .line 820650
    iget v1, v0, LX/4xL;->value:I

    move v1, v1

    .line 820651
    if-nez v1, :cond_1

    .line 820652
    :cond_0
    iget-object v1, p0, LX/4wy;->b:LX/1M5;

    iget-object v1, v1, LX/1M5;->c:LX/1Lz;

    iget-object v1, v1, LX/1Lz;->a:Ljava/util/Map;

    invoke-virtual {p0}, LX/4wy;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/4xL;

    .line 820653
    if-eqz v1, :cond_1

    .line 820654
    iget v0, v1, LX/4xL;->value:I

    move v0, v0

    .line 820655
    :goto_0
    return v0

    :cond_1
    if-nez v0, :cond_2

    const/4 v0, 0x0

    goto :goto_0

    .line 820656
    :cond_2
    iget v1, v0, LX/4xL;->value:I

    move v0, v1

    .line 820657
    goto :goto_0
.end method
