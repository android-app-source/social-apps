.class public LX/3kd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0i1;


# static fields
.field public static final a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

.field public static final b:LX/0Tn;


# instance fields
.field public final c:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final d:LX/3kf;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 633194
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PAGE_ACTIONBAR:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    sput-object v0, LX/3kd;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    .line 633195
    sget-object v0, LX/3ke;->b:LX/0Tn;

    const-string v1, "message_button_nux_seen"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3kd;->b:LX/0Tn;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/3kf;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 633196
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 633197
    iput-object p1, p0, LX/3kd;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 633198
    iput-object p2, p0, LX/3kd;->d:LX/3kf;

    .line 633199
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 633188
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/10S;
    .locals 2

    .prologue
    .line 633189
    iget-object v0, p1, Lcom/facebook/interstitial/manager/InterstitialTrigger;->action:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    sget-object v1, LX/3kd;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    iget-object v1, v1, Lcom/facebook/interstitial/manager/InterstitialTrigger;->action:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 633190
    iget-object v0, p0, LX/3kd;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/3kd;->b:LX/0Tn;

    const/4 p1, 0x0

    invoke-interface {v0, v1, p1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    move v0, v0

    .line 633191
    if-eqz v0, :cond_1

    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;

    :goto_1
    return-object v0

    .line 633192
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 633193
    :cond_1
    sget-object v0, LX/10S;->ELIGIBLE:LX/10S;

    goto :goto_1
.end method

.method public final a(J)V
    .locals 0

    .prologue
    .line 633187
    return-void
.end method

.method public final a(Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 633186
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 633185
    const-string v0, "3641"

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 633184
    sget-object v0, LX/3kd;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
