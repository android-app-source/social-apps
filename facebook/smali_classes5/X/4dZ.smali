.class public LX/4dZ;
.super LX/4dQ;
.source ""

# interfaces
.implements LX/4dH;


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static final b:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field private final c:LX/1bv;

.field private final d:LX/1bw;

.field private final e:Landroid/app/ActivityManager;

.field private final f:LX/0So;

.field public final g:LX/4dG;

.field private final h:LX/4dM;

.field private final i:LX/4dh;

.field private final j:LX/1FN;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1FN",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private final k:D

.field private final l:D

.field private final m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final n:LX/0YU;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0YU",
            "<",
            "LX/1eg",
            "<",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final o:LX/0YU;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0YU",
            "<",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final p:LX/4dj;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private q:I
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "ui-thread"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 796161
    const-class v0, LX/4dZ;

    sput-object v0, LX/4dZ;->a:Ljava/lang/Class;

    .line 796162
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    sput-object v0, LX/4dZ;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method public constructor <init>(LX/1bv;Landroid/app/ActivityManager;LX/1bw;LX/0So;LX/4dG;LX/4dM;)V
    .locals 3

    .prologue
    const/16 v2, 0xa

    .line 796078
    invoke-direct {p0, p5}, LX/4dQ;-><init>(LX/4dG;)V

    .line 796079
    iput-object p1, p0, LX/4dZ;->c:LX/1bv;

    .line 796080
    iput-object p2, p0, LX/4dZ;->e:Landroid/app/ActivityManager;

    .line 796081
    iput-object p3, p0, LX/4dZ;->d:LX/1bw;

    .line 796082
    iput-object p4, p0, LX/4dZ;->f:LX/0So;

    .line 796083
    iput-object p5, p0, LX/4dZ;->g:LX/4dG;

    .line 796084
    iput-object p6, p0, LX/4dZ;->h:LX/4dM;

    .line 796085
    iget v0, p6, LX/4dM;->d:I

    if-ltz v0, :cond_0

    iget v0, p6, LX/4dM;->d:I

    div-int/lit16 v0, v0, 0x400

    int-to-double v0, v0

    :goto_0
    iput-wide v0, p0, LX/4dZ;->k:D

    .line 796086
    new-instance v0, LX/4dh;

    new-instance v1, LX/4dV;

    invoke-direct {v1, p0}, LX/4dV;-><init>(LX/4dZ;)V

    invoke-direct {v0, p5, v1}, LX/4dh;-><init>(LX/4dG;LX/4CF;)V

    iput-object v0, p0, LX/4dZ;->i:LX/4dh;

    .line 796087
    new-instance v0, LX/4dW;

    invoke-direct {v0, p0}, LX/4dW;-><init>(LX/4dZ;)V

    iput-object v0, p0, LX/4dZ;->j:LX/1FN;

    .line 796088
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/4dZ;->m:Ljava/util/List;

    .line 796089
    new-instance v0, LX/0YU;

    invoke-direct {v0, v2}, LX/0YU;-><init>(I)V

    iput-object v0, p0, LX/4dZ;->n:LX/0YU;

    .line 796090
    new-instance v0, LX/0YU;

    invoke-direct {v0, v2}, LX/0YU;-><init>(I)V

    iput-object v0, p0, LX/4dZ;->o:LX/0YU;

    .line 796091
    new-instance v0, LX/4dj;

    iget-object v1, p0, LX/4dZ;->g:LX/4dG;

    invoke-interface {v1}, LX/4dG;->c()I

    move-result v1

    invoke-direct {v0, v1}, LX/4dj;-><init>(I)V

    iput-object v0, p0, LX/4dZ;->p:LX/4dj;

    .line 796092
    iget-object v0, p0, LX/4dZ;->g:LX/4dG;

    invoke-interface {v0}, LX/4dG;->g()I

    move-result v0

    iget-object v1, p0, LX/4dZ;->g:LX/4dG;

    invoke-interface {v1}, LX/4dG;->h()I

    move-result v1

    mul-int/2addr v0, v1

    div-int/lit16 v0, v0, 0x400

    iget-object v1, p0, LX/4dZ;->g:LX/4dG;

    invoke-interface {v1}, LX/4dG;->c()I

    move-result v1

    mul-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x4

    int-to-double v0, v0

    iput-wide v0, p0, LX/4dZ;->l:D

    .line 796093
    return-void

    .line 796094
    :cond_0
    invoke-virtual {p2}, Landroid/app/ActivityManager;->getMemoryClass()I

    move-result v0

    .line 796095
    const/16 v1, 0x20

    if-le v0, v1, :cond_1

    .line 796096
    const/high16 v0, 0x500000

    .line 796097
    :goto_1
    move v0, v0

    .line 796098
    div-int/lit16 v0, v0, 0x400

    int-to-double v0, v0

    goto :goto_0

    :cond_1
    const/high16 v0, 0x300000

    goto :goto_1
.end method

.method private a(IZ)LX/1FJ;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZ)",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 796099
    iget-object v0, p0, LX/4dZ;->f:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    .line 796100
    :try_start_0
    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 796101
    :try_start_1
    iget-object v0, p0, LX/4dZ;->p:LX/4dj;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, LX/4dj;->a(IZ)V

    .line 796102
    invoke-static {p0, p1}, LX/4dZ;->i(LX/4dZ;I)LX/1FJ;

    move-result-object v0

    .line 796103
    if-eqz v0, :cond_0

    .line 796104
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 796105
    iget-object v1, p0, LX/4dZ;->f:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    .line 796106
    :goto_0
    return-object v0

    .line 796107
    :cond_0
    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 796108
    if-eqz p2, :cond_1

    .line 796109
    :try_start_3
    invoke-static {p0}, LX/4dZ;->n(LX/4dZ;)LX/1FJ;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v1

    .line 796110
    :try_start_4
    iget-object v2, p0, LX/4dZ;->i:LX/4dh;

    invoke-virtual {v1}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v2, p1, v0}, LX/4dh;->a(ILandroid/graphics/Bitmap;)V

    .line 796111
    invoke-static {p0, p1, v1}, LX/4dZ;->a(LX/4dZ;ILX/1FJ;)V

    .line 796112
    invoke-virtual {v1}, LX/1FJ;->b()LX/1FJ;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    move-result-object v0

    .line 796113
    :try_start_5
    invoke-virtual {v1}, LX/1FJ;->close()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 796114
    iget-object v1, p0, LX/4dZ;->f:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    goto :goto_0

    .line 796115
    :catchall_0
    move-exception v0

    :try_start_6
    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :try_start_7
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 796116
    :catchall_1
    move-exception v0

    iget-object v1, p0, LX/4dZ;->f:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    .line 796117
    throw v0

    .line 796118
    :catchall_2
    move-exception v0

    :try_start_8
    invoke-virtual {v1}, LX/1FJ;->close()V

    throw v0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 796119
    :cond_1
    iget-object v0, p0, LX/4dZ;->f:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    .line 796120
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static declared-synchronized a(LX/4dZ;II)V
    .locals 4

    .prologue
    .line 796121
    monitor-enter p0

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, p2, :cond_1

    .line 796122
    add-int v0, p1, v1

    :try_start_0
    iget-object v2, p0, LX/4dZ;->g:LX/4dG;

    invoke-interface {v2}, LX/4dG;->c()I

    move-result v2

    rem-int v2, v0, v2

    .line 796123
    invoke-static {p0, v2}, LX/4dZ;->j(LX/4dZ;I)Z

    move-result v3

    .line 796124
    iget-object v0, p0, LX/4dZ;->n:LX/0YU;

    invoke-virtual {v0, v2}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1eg;

    .line 796125
    if-nez v3, :cond_0

    if-nez v0, :cond_0

    .line 796126
    new-instance v0, LX/4dX;

    invoke-direct {v0, p0, v2}, LX/4dX;-><init>(LX/4dZ;I)V

    iget-object v3, p0, LX/4dZ;->c:LX/1bv;

    invoke-static {v0, v3}, LX/1eg;->a(Ljava/util/concurrent/Callable;Ljava/util/concurrent/Executor;)LX/1eg;

    move-result-object v0

    .line 796127
    iget-object v3, p0, LX/4dZ;->n:LX/0YU;

    invoke-virtual {v3, v2, v0}, LX/0YU;->a(ILjava/lang/Object;)V

    .line 796128
    new-instance v3, LX/4dY;

    invoke-direct {v3, p0, v0, v2}, LX/4dY;-><init>(LX/4dZ;LX/1eg;I)V

    invoke-virtual {v0, v3}, LX/1eg;->a(LX/1ex;)LX/1eg;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 796129
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 796130
    :cond_1
    monitor-exit p0

    return-void

    .line 796131
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized a(LX/4dZ;ILX/1FJ;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 796132
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/4dZ;->p:LX/4dj;

    invoke-virtual {v0, p1}, LX/4dj;->a(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 796133
    :goto_0
    monitor-exit p0

    return-void

    .line 796134
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/4dZ;->o:LX/0YU;

    invoke-virtual {v0, p1}, LX/0YU;->g(I)I

    move-result v1

    .line 796135
    if-ltz v1, :cond_1

    .line 796136
    iget-object v0, p0, LX/4dZ;->o:LX/0YU;

    invoke-virtual {v0, v1}, LX/0YU;->f(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FJ;

    .line 796137
    invoke-virtual {v0}, LX/1FJ;->close()V

    .line 796138
    iget-object v0, p0, LX/4dZ;->o:LX/0YU;

    invoke-virtual {v0, v1}, LX/0YU;->d(I)V

    .line 796139
    :cond_1
    iget-object v0, p0, LX/4dZ;->o:LX/0YU;

    invoke-virtual {p2}, LX/1FJ;->b()LX/1FJ;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, LX/0YU;->a(ILjava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 796140
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static a$redex0(LX/4dZ;ILandroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 796141
    monitor-enter p0

    .line 796142
    :try_start_0
    iget-object v1, p0, LX/4dZ;->p:LX/4dj;

    invoke-virtual {v1, p1}, LX/4dj;->a(I)Z

    move-result v1

    .line 796143
    if-eqz v1, :cond_0

    .line 796144
    iget-object v1, p0, LX/4dZ;->o:LX/0YU;

    invoke-virtual {v1, p1}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 796145
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 796146
    if-eqz v0, :cond_1

    .line 796147
    invoke-static {p0, p1, p2}, LX/4dZ;->b(LX/4dZ;ILandroid/graphics/Bitmap;)V

    .line 796148
    :cond_1
    return-void

    .line 796149
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static declared-synchronized a$redex0(LX/4dZ;LX/1eg;I)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1eg",
            "<*>;I)V"
        }
    .end annotation

    .prologue
    .line 795959
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/4dZ;->n:LX/0YU;

    invoke-virtual {v0, p2}, LX/0YU;->g(I)I

    move-result v1

    .line 795960
    if-ltz v1, :cond_0

    .line 795961
    iget-object v0, p0, LX/4dZ;->n:LX/0YU;

    invoke-virtual {v0, v1}, LX/0YU;->f(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1eg;

    .line 795962
    if-ne v0, p1, :cond_0

    .line 795963
    iget-object v0, p0, LX/4dZ;->n:LX/0YU;

    invoke-virtual {v0, v1}, LX/0YU;->d(I)V

    .line 795964
    invoke-virtual {p1}, LX/1eg;->e()Ljava/lang/Exception;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 795965
    invoke-virtual {p1}, LX/1eg;->e()Ljava/lang/Exception;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 795966
    :cond_0
    monitor-exit p0

    return-void

    .line 795967
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized b(LX/4dZ;II)V
    .locals 2

    .prologue
    .line 796150
    monitor-enter p0

    const/4 v0, 0x0

    .line 796151
    :goto_0
    :try_start_0
    iget-object v1, p0, LX/4dZ;->n:LX/0YU;

    invoke-virtual {v1}, LX/0YU;->a()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 796152
    iget-object v1, p0, LX/4dZ;->n:LX/0YU;

    invoke-virtual {v1, v0}, LX/0YU;->e(I)I

    move-result v1

    .line 796153
    invoke-static {p1, p2, v1}, LX/1bw;->a(III)Z

    move-result v1

    .line 796154
    if-eqz v1, :cond_0

    .line 796155
    iget-object v1, p0, LX/4dZ;->n:LX/0YU;

    invoke-virtual {v1, v0}, LX/0YU;->f(I)Ljava/lang/Object;

    .line 796156
    iget-object v1, p0, LX/4dZ;->n:LX/0YU;

    invoke-virtual {v1, v0}, LX/0YU;->d(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 796157
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 796158
    :cond_0
    add-int/lit8 v0, v0, 0x1

    .line 796159
    goto :goto_0

    .line 796160
    :cond_1
    monitor-exit p0

    return-void
.end method

.method private static b(LX/4dZ;ILandroid/graphics/Bitmap;)V
    .locals 5

    .prologue
    .line 796169
    invoke-static {p0}, LX/4dZ;->n(LX/4dZ;)LX/1FJ;

    move-result-object v1

    .line 796170
    :try_start_0
    new-instance v2, Landroid/graphics/Canvas;

    invoke-virtual {v1}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-direct {v2, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 796171
    const/4 v0, 0x0

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->SRC:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v2, v0, v3}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 796172
    const/4 v0, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2, p2, v0, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 796173
    invoke-static {p0, p1, v1}, LX/4dZ;->a(LX/4dZ;ILX/1FJ;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 796174
    invoke-virtual {v1}, LX/1FJ;->close()V

    .line 796175
    return-void

    .line 796176
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, LX/1FJ;->close()V

    throw v0
.end method

.method public static h(LX/4dZ;I)V
    .locals 4

    .prologue
    .line 796177
    monitor-enter p0

    .line 796178
    :try_start_0
    iget-object v0, p0, LX/4dZ;->p:LX/4dj;

    invoke-virtual {v0, p1}, LX/4dj;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 796179
    monitor-exit p0

    .line 796180
    :goto_0
    return-void

    .line 796181
    :cond_0
    invoke-static {p0, p1}, LX/4dZ;->j(LX/4dZ;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 796182
    monitor-exit p0

    goto :goto_0

    .line 796183
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 796184
    iget-object v0, p0, LX/4dZ;->g:LX/4dG;

    invoke-interface {v0, p1}, LX/4dG;->e(I)LX/1FJ;

    move-result-object v1

    .line 796185
    if-eqz v1, :cond_2

    .line 796186
    :try_start_2
    invoke-static {p0, p1, v1}, LX/4dZ;->a(LX/4dZ;ILX/1FJ;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 796187
    :goto_1
    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    goto :goto_0

    .line 796188
    :cond_2
    :try_start_3
    invoke-static {p0}, LX/4dZ;->n(LX/4dZ;)LX/1FJ;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v2

    .line 796189
    :try_start_4
    iget-object v3, p0, LX/4dZ;->i:LX/4dh;

    invoke-virtual {v2}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v3, p1, v0}, LX/4dh;->a(ILandroid/graphics/Bitmap;)V

    .line 796190
    invoke-static {p0, p1, v2}, LX/4dZ;->a(LX/4dZ;ILX/1FJ;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 796191
    :try_start_5
    invoke-virtual {v2}, LX/1FJ;->close()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_1

    .line 796192
    :catchall_1
    move-exception v0

    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    throw v0

    .line 796193
    :catchall_2
    move-exception v0

    invoke-virtual {v2}, LX/1FJ;->close()V

    throw v0
.end method

.method public static declared-synchronized i(LX/4dZ;I)LX/1FJ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 796164
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/4dZ;->o:LX/0YU;

    invoke-virtual {v0, p1}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FJ;

    invoke-static {v0}, LX/1FJ;->b(LX/1FJ;)LX/1FJ;

    move-result-object v0

    .line 796165
    if-nez v0, :cond_0

    .line 796166
    iget-object v0, p0, LX/4dZ;->g:LX/4dG;

    invoke-interface {v0, p1}, LX/4dG;->e(I)LX/1FJ;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 796167
    :cond_0
    monitor-exit p0

    return-object v0

    .line 796168
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized j(LX/4dZ;I)Z
    .locals 1

    .prologue
    .line 796163
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/4dZ;->o:LX/0YU;

    invoke-virtual {v0, p1}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/4dZ;->g:LX/4dG;

    invoke-interface {v0, p1}, LX/4dG;->f(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static n(LX/4dZ;)LX/1FJ;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 796032
    monitor-enter p0

    .line 796033
    :try_start_0
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    .line 796034
    sget-object v2, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, 0x14

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v4, v5, v3}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v2

    add-long/2addr v2, v0

    .line 796035
    :goto_0
    iget-object v4, p0, LX/4dZ;->m:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    if-eqz v4, :cond_0

    cmp-long v4, v0, v2

    if-gez v4, :cond_0

    .line 796036
    :try_start_1
    sget-object v4, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    sub-long v0, v2, v0

    invoke-virtual {v4, p0, v0, v1}, Ljava/util/concurrent/TimeUnit;->timedWait(Ljava/lang/Object;J)V

    .line 796037
    invoke-static {}, Ljava/lang/System;->nanoTime()J
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v0

    goto :goto_0

    .line 796038
    :catch_0
    move-exception v0

    .line 796039
    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    .line 796040
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 796041
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 796042
    :cond_0
    :try_start_3
    iget-object v0, p0, LX/4dZ;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 796043
    sget-object v0, LX/4dZ;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 796044
    sget-object v0, LX/4dZ;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    .line 796045
    iget-object v0, p0, LX/4dZ;->g:LX/4dG;

    invoke-interface {v0}, LX/4dG;->g()I

    move-result v0

    iget-object v1, p0, LX/4dZ;->g:LX/4dG;

    invoke-interface {v1}, LX/4dG;->h()I

    move-result v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    move-object v0, v0

    .line 796046
    :goto_1
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 796047
    iget-object v1, p0, LX/4dZ;->j:LX/1FN;

    invoke-static {v0, v1}, LX/1FJ;->a(Ljava/lang/Object;LX/1FN;)LX/1FJ;

    move-result-object v0

    return-object v0

    .line 796048
    :cond_1
    :try_start_4
    iget-object v0, p0, LX/4dZ;->m:Ljava/util/List;

    iget-object v1, p0, LX/4dZ;->m:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1
.end method

.method private declared-synchronized o()V
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 796049
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, LX/4dZ;->g:LX/4dG;

    iget v3, p0, LX/4dZ;->q:I

    invoke-interface {v1, v3}, LX/4dG;->a(I)LX/4dL;

    move-result-object v1

    .line 796050
    iget-object v1, v1, LX/4dL;->g:LX/4dK;

    sget-object v3, LX/4dK;->DISPOSE_TO_PREVIOUS:LX/4dK;

    if-ne v1, v3, :cond_4

    move v4, v0

    .line 796051
    :goto_0
    const/4 v3, 0x0

    iget v5, p0, LX/4dZ;->q:I

    if-eqz v4, :cond_5

    move v1, v0

    :goto_1
    sub-int v1, v5, v1

    invoke-static {v3, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 796052
    iget-object v3, p0, LX/4dZ;->h:LX/4dM;

    iget-boolean v3, v3, LX/4dM;->c:Z

    if-eqz v3, :cond_6

    const/4 v3, 0x3

    .line 796053
    :goto_2
    if-eqz v4, :cond_7

    :goto_3
    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 796054
    add-int v0, v1, v2

    iget-object v3, p0, LX/4dZ;->g:LX/4dG;

    invoke-interface {v3}, LX/4dG;->c()I

    move-result v3

    rem-int/2addr v0, v3

    .line 796055
    invoke-static {p0, v1, v0}, LX/4dZ;->b(LX/4dZ;II)V

    .line 796056
    invoke-static {p0}, LX/4dZ;->p(LX/4dZ;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 796057
    iget-object v3, p0, LX/4dZ;->p:LX/4dj;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, LX/4dj;->a(Z)V

    .line 796058
    iget-object v3, p0, LX/4dZ;->p:LX/4dj;

    const/4 v5, 0x0

    .line 796059
    move v4, v5

    :goto_4
    iget-object v6, v3, LX/4dj;->a:[Z

    array-length v6, v6

    if-ge v4, v6, :cond_1

    .line 796060
    invoke-static {v1, v0, v4}, LX/1bw;->a(III)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 796061
    iget-object v6, v3, LX/4dj;->a:[Z

    aput-boolean v5, v6, v4

    .line 796062
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 796063
    :cond_1
    move v0, v1

    .line 796064
    :goto_5
    if-ltz v0, :cond_2

    .line 796065
    iget-object v3, p0, LX/4dZ;->o:LX/0YU;

    invoke-virtual {v3, v0}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_8

    .line 796066
    iget-object v3, p0, LX/4dZ;->p:LX/4dj;

    const/4 v4, 0x1

    invoke-virtual {v3, v0, v4}, LX/4dj;->a(IZ)V

    .line 796067
    :cond_2
    invoke-static {p0}, LX/4dZ;->q(LX/4dZ;)V

    .line 796068
    :cond_3
    iget-object v0, p0, LX/4dZ;->h:LX/4dM;

    iget-boolean v0, v0, LX/4dM;->c:Z

    if-eqz v0, :cond_9

    .line 796069
    invoke-static {p0, v1, v2}, LX/4dZ;->a(LX/4dZ;II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 796070
    :goto_6
    monitor-exit p0

    return-void

    :cond_4
    move v4, v2

    .line 796071
    goto :goto_0

    :cond_5
    move v1, v2

    .line 796072
    goto :goto_1

    :cond_6
    move v3, v2

    .line 796073
    goto :goto_2

    :cond_7
    move v0, v2

    .line 796074
    goto :goto_3

    .line 796075
    :cond_8
    add-int/lit8 v0, v0, -0x1

    goto :goto_5

    .line 796076
    :cond_9
    :try_start_1
    iget v0, p0, LX/4dZ;->q:I

    iget v1, p0, LX/4dZ;->q:I

    invoke-static {p0, v0, v1}, LX/4dZ;->b(LX/4dZ;II)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_6

    .line 796077
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static p(LX/4dZ;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 796030
    iget-object v1, p0, LX/4dZ;->h:LX/4dM;

    iget-boolean v1, v1, LX/4dM;->b:Z

    if-eqz v1, :cond_1

    .line 796031
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-wide v2, p0, LX/4dZ;->l:D

    iget-wide v4, p0, LX/4dZ;->k:D

    cmpg-double v1, v2, v4

    if-ltz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static declared-synchronized q(LX/4dZ;)V
    .locals 3

    .prologue
    .line 796018
    monitor-enter p0

    const/4 v0, 0x0

    move v1, v0

    .line 796019
    :goto_0
    :try_start_0
    iget-object v0, p0, LX/4dZ;->o:LX/0YU;

    invoke-virtual {v0}, LX/0YU;->a()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 796020
    iget-object v0, p0, LX/4dZ;->o:LX/0YU;

    invoke-virtual {v0, v1}, LX/0YU;->e(I)I

    move-result v0

    .line 796021
    iget-object v2, p0, LX/4dZ;->p:LX/4dj;

    invoke-virtual {v2, v0}, LX/4dj;->a(I)Z

    move-result v0

    .line 796022
    if-nez v0, :cond_0

    .line 796023
    iget-object v0, p0, LX/4dZ;->o:LX/0YU;

    invoke-virtual {v0, v1}, LX/0YU;->f(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FJ;

    .line 796024
    iget-object v2, p0, LX/4dZ;->o:LX/0YU;

    invoke-virtual {v2, v1}, LX/0YU;->d(I)V

    .line 796025
    invoke-virtual {v0}, LX/1FJ;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 796026
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 796027
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 796028
    goto :goto_0

    .line 796029
    :cond_1
    monitor-exit p0

    return-void
.end method


# virtual methods
.method public final synthetic a(Landroid/graphics/Rect;)LX/4dG;
    .locals 1

    .prologue
    .line 796017
    invoke-virtual {p0, p1}, LX/4dZ;->b(Landroid/graphics/Rect;)LX/4dH;

    move-result-object v0

    return-object v0
.end method

.method public final a(ILandroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 796016
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method

.method public final declared-synchronized a(Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 796013
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/4dZ;->m:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 796014
    monitor-exit p0

    return-void

    .line 796015
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Ljava/lang/StringBuilder;)V
    .locals 4

    .prologue
    .line 796004
    iget-object v0, p0, LX/4dZ;->h:LX/4dM;

    iget-boolean v0, v0, LX/4dM;->b:Z

    if-eqz v0, :cond_1

    .line 796005
    const-string v0, "Pinned To Memory"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 796006
    :goto_0
    invoke-static {p0}, LX/4dZ;->p(LX/4dZ;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/4dZ;->h:LX/4dM;

    iget-boolean v0, v0, LX/4dM;->c:Z

    if-eqz v0, :cond_0

    .line 796007
    const-string v0, " MT"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 796008
    :cond_0
    return-void

    .line 796009
    :cond_1
    iget-wide v0, p0, LX/4dZ;->l:D

    iget-wide v2, p0, LX/4dZ;->k:D

    cmpg-double v0, v0, v2

    if-gez v0, :cond_2

    .line 796010
    const-string v0, "within "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 796011
    :goto_1
    iget-wide v0, p0, LX/4dZ;->k:D

    double-to-int v0, v0

    invoke-static {p1, v0}, LX/1bw;->a(Ljava/lang/StringBuilder;I)V

    goto :goto_0

    .line 796012
    :cond_2
    const-string v0, "exceeds "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method public final b(Landroid/graphics/Rect;)LX/4dH;
    .locals 7

    .prologue
    .line 796001
    iget-object v0, p0, LX/4dZ;->g:LX/4dG;

    invoke-interface {v0, p1}, LX/4dG;->a(Landroid/graphics/Rect;)LX/4dG;

    move-result-object v5

    .line 796002
    iget-object v0, p0, LX/4dZ;->g:LX/4dG;

    if-ne v5, v0, :cond_0

    .line 796003
    :goto_0
    return-object p0

    :cond_0
    new-instance v0, LX/4dZ;

    iget-object v1, p0, LX/4dZ;->c:LX/1bv;

    iget-object v2, p0, LX/4dZ;->e:Landroid/app/ActivityManager;

    iget-object v3, p0, LX/4dZ;->d:LX/1bw;

    iget-object v4, p0, LX/4dZ;->f:LX/0So;

    iget-object v6, p0, LX/4dZ;->h:LX/4dM;

    invoke-direct/range {v0 .. v6}, LX/4dZ;-><init>(LX/1bv;Landroid/app/ActivityManager;LX/1bw;LX/0So;LX/4dG;LX/4dM;)V

    move-object p0, v0

    goto :goto_0
.end method

.method public final declared-synchronized finalize()V
    .locals 2

    .prologue
    .line 795995
    monitor-enter p0

    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 795996
    iget-object v0, p0, LX/4dZ;->o:LX/0YU;

    invoke-virtual {v0}, LX/0YU;->a()I

    .line 795997
    sget-object v0, LX/4dZ;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    iget-object v1, p0, LX/4dZ;->m:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    neg-int v1, v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->addAndGet(I)I

    .line 795998
    iget-object v0, p0, LX/4dZ;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 795999
    monitor-exit p0

    return-void

    .line 796000
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final g(I)LX/1FJ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 795991
    iput p1, p0, LX/4dZ;->q:I

    .line 795992
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/4dZ;->a(IZ)LX/1FJ;

    move-result-object v0

    .line 795993
    invoke-direct {p0}, LX/4dZ;->o()V

    .line 795994
    return-object v0
.end method

.method public final j()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 795979
    monitor-enter p0

    .line 795980
    :try_start_0
    iget-object v0, p0, LX/4dZ;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v1

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 795981
    invoke-static {v0}, LX/1bw;->a(Landroid/graphics/Bitmap;)I

    move-result v0

    add-int/2addr v2, v0

    .line 795982
    goto :goto_0

    .line 795983
    :cond_0
    :goto_1
    iget-object v0, p0, LX/4dZ;->o:LX/0YU;

    invoke-virtual {v0}, LX/0YU;->a()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 795984
    iget-object v0, p0, LX/4dZ;->o:LX/0YU;

    invoke-virtual {v0, v1}, LX/0YU;->f(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FJ;

    .line 795985
    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-static {v0}, LX/1bw;->a(Landroid/graphics/Bitmap;)I

    move-result v0

    add-int/2addr v2, v0

    .line 795986
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 795987
    :cond_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 795988
    iget-object v0, p0, LX/4dZ;->g:LX/4dG;

    invoke-interface {v0}, LX/4dG;->j()I

    move-result v0

    add-int/2addr v0, v2

    .line 795989
    return v0

    .line 795990
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final declared-synchronized k()V
    .locals 2

    .prologue
    .line 795969
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/4dZ;->p:LX/4dj;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/4dj;->a(Z)V

    .line 795970
    invoke-static {p0}, LX/4dZ;->q(LX/4dZ;)V

    .line 795971
    iget-object v0, p0, LX/4dZ;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 795972
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 795973
    sget-object v0, LX/4dZ;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 795974
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 795975
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/4dZ;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 795976
    iget-object v0, p0, LX/4dZ;->g:LX/4dG;

    invoke-interface {v0}, LX/4dG;->k()V

    .line 795977
    sget-object v0, LX/4dZ;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 795978
    monitor-exit p0

    return-void
.end method

.method public final l()LX/1FJ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 795968
    invoke-virtual {p0}, LX/4dQ;->a()LX/4dO;

    move-result-object v0

    invoke-virtual {v0}, LX/4dO;->c()LX/1FJ;

    move-result-object v0

    return-object v0
.end method
