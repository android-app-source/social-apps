.class public LX/4L7;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 681883
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 32

    .prologue
    .line 681890
    const/16 v26, 0x0

    .line 681891
    const/16 v25, 0x0

    .line 681892
    const/16 v24, 0x0

    .line 681893
    const/16 v23, 0x0

    .line 681894
    const/16 v22, 0x0

    .line 681895
    const/16 v21, 0x0

    .line 681896
    const/16 v20, 0x0

    .line 681897
    const/16 v19, 0x0

    .line 681898
    const/16 v18, 0x0

    .line 681899
    const/16 v17, 0x0

    .line 681900
    const/16 v16, 0x0

    .line 681901
    const/4 v15, 0x0

    .line 681902
    const/4 v14, 0x0

    .line 681903
    const-wide/16 v12, 0x0

    .line 681904
    const-wide/16 v10, 0x0

    .line 681905
    const/4 v9, 0x0

    .line 681906
    const/4 v8, 0x0

    .line 681907
    const/4 v7, 0x0

    .line 681908
    const/4 v6, 0x0

    .line 681909
    const/4 v5, 0x0

    .line 681910
    const/4 v4, 0x0

    .line 681911
    const/4 v3, 0x0

    .line 681912
    const/4 v2, 0x0

    .line 681913
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v27

    sget-object v28, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    if-eq v0, v1, :cond_19

    .line 681914
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 681915
    const/4 v2, 0x0

    .line 681916
    :goto_0
    return v2

    .line 681917
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v28, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v28

    if-eq v2, v0, :cond_11

    .line 681918
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 681919
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 681920
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v28

    sget-object v29, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    if-eq v0, v1, :cond_0

    if-eqz v2, :cond_0

    .line 681921
    const-string v28, "campaign_name"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_1

    .line 681922
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v27, v2

    goto :goto_1

    .line 681923
    :cond_1
    const-string v28, "campaign_status_title"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_2

    .line 681924
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v26, v2

    goto :goto_1

    .line 681925
    :cond_2
    const-string v28, "image_uri"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_3

    .line 681926
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v25, v2

    goto :goto_1

    .line 681927
    :cond_3
    const-string v28, "is_active"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_4

    .line 681928
    const/4 v2, 0x1

    .line 681929
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v11

    move/from16 v24, v11

    move v11, v2

    goto :goto_1

    .line 681930
    :cond_4
    const-string v28, "landing_uri"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_5

    .line 681931
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v23, v2

    goto/16 :goto_1

    .line 681932
    :cond_5
    const-string v28, "objective_result"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_6

    .line 681933
    const/4 v2, 0x1

    .line 681934
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v10

    move/from16 v22, v10

    move v10, v2

    goto/16 :goto_1

    .line 681935
    :cond_6
    const-string v28, "objective_result_title"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_7

    .line 681936
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v21, v2

    goto/16 :goto_1

    .line 681937
    :cond_7
    const-string v28, "people_reached"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_8

    .line 681938
    const/4 v2, 0x1

    .line 681939
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v9

    move/from16 v20, v9

    move v9, v2

    goto/16 :goto_1

    .line 681940
    :cond_8
    const-string v28, "people_reached_title"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_9

    .line 681941
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v19, v2

    goto/16 :goto_1

    .line 681942
    :cond_9
    const-string v28, "spent_meter_spent"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_a

    .line 681943
    const/4 v2, 0x1

    .line 681944
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v7

    move/from16 v18, v7

    move v7, v2

    goto/16 :goto_1

    .line 681945
    :cond_a
    const-string v28, "spent_meter_spent_title"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_b

    .line 681946
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v17, v2

    goto/16 :goto_1

    .line 681947
    :cond_b
    const-string v28, "spent_meter_total"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_c

    .line 681948
    const/4 v2, 0x1

    .line 681949
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move/from16 v16, v6

    move v6, v2

    goto/16 :goto_1

    .line 681950
    :cond_c
    const-string v28, "spent_meter_total_title"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_d

    .line 681951
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v13, v2

    goto/16 :goto_1

    .line 681952
    :cond_d
    const-string v28, "start_time"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_e

    .line 681953
    const/4 v2, 0x1

    .line 681954
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto/16 :goto_1

    .line 681955
    :cond_e
    const-string v28, "stop_time"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_f

    .line 681956
    const/4 v2, 0x1

    .line 681957
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v14

    move v8, v2

    goto/16 :goto_1

    .line 681958
    :cond_f
    const-string v28, "campaign_id"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 681959
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v12, v2

    goto/16 :goto_1

    .line 681960
    :cond_10
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 681961
    :cond_11
    const/16 v2, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 681962
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 681963
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 681964
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 681965
    if-eqz v11, :cond_12

    .line 681966
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 681967
    :cond_12
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 681968
    if-eqz v10, :cond_13

    .line 681969
    const/4 v2, 0x5

    const/4 v10, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1, v10}, LX/186;->a(III)V

    .line 681970
    :cond_13
    const/4 v2, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 681971
    if-eqz v9, :cond_14

    .line 681972
    const/4 v2, 0x7

    const/4 v9, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1, v9}, LX/186;->a(III)V

    .line 681973
    :cond_14
    const/16 v2, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 681974
    if-eqz v7, :cond_15

    .line 681975
    const/16 v2, 0x9

    const/4 v7, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1, v7}, LX/186;->a(III)V

    .line 681976
    :cond_15
    const/16 v2, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 681977
    if-eqz v6, :cond_16

    .line 681978
    const/16 v2, 0xb

    const/4 v6, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1, v6}, LX/186;->a(III)V

    .line 681979
    :cond_16
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 681980
    if-eqz v3, :cond_17

    .line 681981
    const/16 v3, 0xd

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 681982
    :cond_17
    if-eqz v8, :cond_18

    .line 681983
    const/16 v3, 0xe

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide v4, v14

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 681984
    :cond_18
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 681985
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_19
    move/from16 v27, v26

    move/from16 v26, v25

    move/from16 v25, v24

    move/from16 v24, v23

    move/from16 v23, v22

    move/from16 v22, v21

    move/from16 v21, v20

    move/from16 v20, v19

    move/from16 v19, v18

    move/from16 v18, v17

    move/from16 v17, v16

    move/from16 v16, v15

    move/from16 v30, v14

    move-wide v14, v10

    move v11, v8

    move v10, v7

    move v7, v5

    move v8, v2

    move/from16 v31, v6

    move v6, v4

    move-wide v4, v12

    move v12, v9

    move/from16 v13, v30

    move/from16 v9, v31

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    .line 681986
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 681987
    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 681988
    if-eqz v0, :cond_0

    .line 681989
    const-string v1, "campaign_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 681990
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 681991
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 681992
    if-eqz v0, :cond_1

    .line 681993
    const-string v1, "campaign_status_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 681994
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 681995
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 681996
    if-eqz v0, :cond_2

    .line 681997
    const-string v1, "image_uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 681998
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 681999
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 682000
    if-eqz v0, :cond_3

    .line 682001
    const-string v1, "is_active"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 682002
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 682003
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 682004
    if-eqz v0, :cond_4

    .line 682005
    const-string v1, "landing_uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 682006
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 682007
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 682008
    if-eqz v0, :cond_5

    .line 682009
    const-string v1, "objective_result"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 682010
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 682011
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 682012
    if-eqz v0, :cond_6

    .line 682013
    const-string v1, "objective_result_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 682014
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 682015
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 682016
    if-eqz v0, :cond_7

    .line 682017
    const-string v1, "people_reached"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 682018
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 682019
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 682020
    if-eqz v0, :cond_8

    .line 682021
    const-string v1, "people_reached_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 682022
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 682023
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 682024
    if-eqz v0, :cond_9

    .line 682025
    const-string v1, "spent_meter_spent"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 682026
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 682027
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 682028
    if-eqz v0, :cond_a

    .line 682029
    const-string v1, "spent_meter_spent_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 682030
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 682031
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 682032
    if-eqz v0, :cond_b

    .line 682033
    const-string v1, "spent_meter_total"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 682034
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 682035
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 682036
    if-eqz v0, :cond_c

    .line 682037
    const-string v1, "spent_meter_total_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 682038
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 682039
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 682040
    cmp-long v2, v0, v4

    if-eqz v2, :cond_d

    .line 682041
    const-string v2, "start_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 682042
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 682043
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 682044
    cmp-long v2, v0, v4

    if-eqz v2, :cond_e

    .line 682045
    const-string v2, "stop_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 682046
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 682047
    :cond_e
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 682048
    if-eqz v0, :cond_f

    .line 682049
    const-string v1, "campaign_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 682050
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 682051
    :cond_f
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 682052
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 681884
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 681885
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 681886
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 681887
    invoke-static {p0, p1}, LX/4L7;->a(LX/15w;LX/186;)I

    move-result v1

    .line 681888
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 681889
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method
