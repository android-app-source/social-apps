.class public LX/4d1;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Lorg/apache/http/client/methods/HttpUriRequest;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 795400
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Lorg/apache/http/client/methods/HttpUriRequest;)V
    .locals 1
    .param p1    # Lorg/apache/http/client/methods/HttpUriRequest;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 795407
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, LX/4d1;->a:Lorg/apache/http/client/methods/HttpUriRequest;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 795408
    monitor-exit p0

    return-void

    .line 795409
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a()Z
    .locals 1

    .prologue
    .line 795401
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/4d1;->a:Lorg/apache/http/client/methods/HttpUriRequest;

    if-eqz v0, :cond_0

    .line 795402
    iget-object v0, p0, LX/4d1;->a:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v0}, Lorg/apache/http/client/methods/HttpUriRequest;->abort()V

    .line 795403
    const/4 v0, 0x0

    iput-object v0, p0, LX/4d1;->a:Lorg/apache/http/client/methods/HttpUriRequest;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 795404
    const/4 v0, 0x1

    .line 795405
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 795406
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
