.class public LX/48r;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/csslayout/YogaDirection;

.field public b:Lcom/facebook/csslayout/YogaFlexDirection;

.field public c:Lcom/facebook/csslayout/YogaJustify;

.field public d:Lcom/facebook/csslayout/YogaAlign;

.field public e:Lcom/facebook/csslayout/YogaAlign;

.field public f:Lcom/facebook/csslayout/YogaAlign;

.field public g:Lcom/facebook/csslayout/YogaPositionType;

.field public h:Lcom/facebook/csslayout/YogaWrap;

.field public i:Lcom/facebook/csslayout/YogaOverflow;

.field public j:F

.field public k:F

.field public l:F

.field public m:LX/1mz;

.field public n:LX/1mz;

.field public o:LX/1mz;

.field public p:LX/1mz;

.field public q:[F

.field public r:F

.field public s:F

.field public t:F

.field public u:F


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/high16 v1, 0x7fc00000    # NaNf

    .line 673418
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 673419
    new-instance v0, LX/1mz;

    invoke-direct {v0}, LX/1mz;-><init>()V

    iput-object v0, p0, LX/48r;->m:LX/1mz;

    .line 673420
    new-instance v0, LX/1mz;

    invoke-direct {v0}, LX/1mz;-><init>()V

    iput-object v0, p0, LX/48r;->n:LX/1mz;

    .line 673421
    new-instance v0, LX/1mz;

    invoke-direct {v0}, LX/1mz;-><init>()V

    iput-object v0, p0, LX/48r;->o:LX/1mz;

    .line 673422
    new-instance v0, LX/1mz;

    invoke-direct {v0, v1}, LX/1mz;-><init>(F)V

    iput-object v0, p0, LX/48r;->p:LX/1mz;

    .line 673423
    const/4 v0, 0x2

    new-array v0, v0, [F

    iput-object v0, p0, LX/48r;->q:[F

    .line 673424
    iput v1, p0, LX/48r;->r:F

    .line 673425
    iput v1, p0, LX/48r;->s:F

    .line 673426
    iput v1, p0, LX/48r;->t:F

    .line 673427
    iput v1, p0, LX/48r;->u:F

    .line 673428
    invoke-virtual {p0}, LX/48r;->a()V

    .line 673429
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/high16 v1, 0x7fc00000    # NaNf

    .line 673430
    sget-object v0, Lcom/facebook/csslayout/YogaDirection;->INHERIT:Lcom/facebook/csslayout/YogaDirection;

    iput-object v0, p0, LX/48r;->a:Lcom/facebook/csslayout/YogaDirection;

    .line 673431
    sget-object v0, Lcom/facebook/csslayout/YogaFlexDirection;->COLUMN:Lcom/facebook/csslayout/YogaFlexDirection;

    iput-object v0, p0, LX/48r;->b:Lcom/facebook/csslayout/YogaFlexDirection;

    .line 673432
    sget-object v0, Lcom/facebook/csslayout/YogaJustify;->FLEX_START:Lcom/facebook/csslayout/YogaJustify;

    iput-object v0, p0, LX/48r;->c:Lcom/facebook/csslayout/YogaJustify;

    .line 673433
    sget-object v0, Lcom/facebook/csslayout/YogaAlign;->FLEX_START:Lcom/facebook/csslayout/YogaAlign;

    iput-object v0, p0, LX/48r;->d:Lcom/facebook/csslayout/YogaAlign;

    .line 673434
    sget-object v0, Lcom/facebook/csslayout/YogaAlign;->STRETCH:Lcom/facebook/csslayout/YogaAlign;

    iput-object v0, p0, LX/48r;->e:Lcom/facebook/csslayout/YogaAlign;

    .line 673435
    sget-object v0, Lcom/facebook/csslayout/YogaAlign;->AUTO:Lcom/facebook/csslayout/YogaAlign;

    iput-object v0, p0, LX/48r;->f:Lcom/facebook/csslayout/YogaAlign;

    .line 673436
    sget-object v0, Lcom/facebook/csslayout/YogaPositionType;->RELATIVE:Lcom/facebook/csslayout/YogaPositionType;

    iput-object v0, p0, LX/48r;->g:Lcom/facebook/csslayout/YogaPositionType;

    .line 673437
    sget-object v0, Lcom/facebook/csslayout/YogaWrap;->NO_WRAP:Lcom/facebook/csslayout/YogaWrap;

    iput-object v0, p0, LX/48r;->h:Lcom/facebook/csslayout/YogaWrap;

    .line 673438
    sget-object v0, Lcom/facebook/csslayout/YogaOverflow;->VISIBLE:Lcom/facebook/csslayout/YogaOverflow;

    iput-object v0, p0, LX/48r;->i:Lcom/facebook/csslayout/YogaOverflow;

    .line 673439
    iput v2, p0, LX/48r;->j:F

    .line 673440
    iput v2, p0, LX/48r;->k:F

    .line 673441
    iput v1, p0, LX/48r;->l:F

    .line 673442
    iget-object v0, p0, LX/48r;->m:LX/1mz;

    invoke-virtual {v0}, LX/1mz;->a()V

    .line 673443
    iget-object v0, p0, LX/48r;->n:LX/1mz;

    invoke-virtual {v0}, LX/1mz;->a()V

    .line 673444
    iget-object v0, p0, LX/48r;->o:LX/1mz;

    invoke-virtual {v0}, LX/1mz;->a()V

    .line 673445
    iget-object v0, p0, LX/48r;->p:LX/1mz;

    invoke-virtual {v0}, LX/1mz;->a()V

    .line 673446
    iget-object v0, p0, LX/48r;->q:[F

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([FF)V

    .line 673447
    iput v1, p0, LX/48r;->r:F

    .line 673448
    iput v1, p0, LX/48r;->s:F

    .line 673449
    iput v1, p0, LX/48r;->t:F

    .line 673450
    iput v1, p0, LX/48r;->u:F

    .line 673451
    return-void
.end method
