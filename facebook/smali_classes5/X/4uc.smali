.class public final LX/4uc;
.super LX/4uY;
.source ""


# instance fields
.field public final synthetic a:LX/2wr;

.field public final synthetic b:Lcom/google/android/gms/signin/internal/SignInResponse;

.field public final synthetic c:LX/4ug;


# direct methods
.method public constructor <init>(LX/4ug;LX/2wq;LX/2wr;Lcom/google/android/gms/signin/internal/SignInResponse;)V
    .locals 0

    iput-object p1, p0, LX/4uc;->c:LX/4ug;

    iput-object p3, p0, LX/4uc;->a:LX/2wr;

    iput-object p4, p0, LX/4uc;->b:Lcom/google/android/gms/signin/internal/SignInResponse;

    invoke-direct {p0, p2}, LX/4uY;-><init>(LX/2wq;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    iget-object v0, p0, LX/4uc;->a:LX/2wr;

    iget-object v1, p0, LX/4uc;->b:Lcom/google/android/gms/signin/internal/SignInResponse;

    const/4 v2, 0x0

    invoke-static {v0, v2}, LX/2wr;->b(LX/2wr;I)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, v1, Lcom/google/android/gms/signin/internal/SignInResponse;->b:Lcom/google/android/gms/common/ConnectionResult;

    move-object v2, v2

    invoke-virtual {v2}, Lcom/google/android/gms/common/ConnectionResult;->b()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v2, v1, Lcom/google/android/gms/signin/internal/SignInResponse;->c:Lcom/google/android/gms/common/internal/ResolveAccountResponse;

    move-object v2, v2

    iget-object v3, v2, Lcom/google/android/gms/common/internal/ResolveAccountResponse;->c:Lcom/google/android/gms/common/ConnectionResult;

    move-object v3, v3

    invoke-virtual {v3}, Lcom/google/android/gms/common/ConnectionResult;->b()Z

    move-result v4

    if-nez v4, :cond_1

    const-string v2, "GoogleApiClientConnecting"

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result p0

    add-int/lit8 p0, p0, 0x30

    invoke-direct {v5, p0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string p0, "Sign-in succeeded with resolve account failure: "

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/Exception;

    invoke-direct {v5}, Ljava/lang/Exception;-><init>()V

    invoke-static {v2, v4, v5}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-static {v0, v3}, LX/2wr;->c(LX/2wr;Lcom/google/android/gms/common/ConnectionResult;)V

    goto :goto_0

    :cond_1
    const/4 v3, 0x1

    iput-boolean v3, v0, LX/2wr;->n:Z

    invoke-virtual {v2}, Lcom/google/android/gms/common/internal/ResolveAccountResponse;->a()LX/4st;

    move-result-object v3

    iput-object v3, v0, LX/2wr;->o:LX/4st;

    iget-boolean v3, v2, Lcom/google/android/gms/common/internal/ResolveAccountResponse;->d:Z

    move v3, v3

    iput-boolean v3, v0, LX/2wr;->p:Z

    iget-boolean v3, v2, Lcom/google/android/gms/common/internal/ResolveAccountResponse;->e:Z

    move v2, v3

    iput-boolean v2, v0, LX/2wr;->q:Z

    invoke-static {v0}, LX/2wr;->e(LX/2wr;)V

    goto :goto_0

    :cond_2
    invoke-static {v0, v2}, LX/2wr;->b$redex0(LX/2wr;Lcom/google/android/gms/common/ConnectionResult;)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-static {v0}, LX/2wr;->h(LX/2wr;)V

    invoke-static {v0}, LX/2wr;->e(LX/2wr;)V

    goto :goto_0

    :cond_3
    invoke-static {v0, v2}, LX/2wr;->c(LX/2wr;Lcom/google/android/gms/common/ConnectionResult;)V

    goto :goto_0
.end method
