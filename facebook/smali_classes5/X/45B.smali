.class public final enum LX/45B;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/45B;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/45B;

.field public static final enum CONNECTED:LX/45B;

.field public static final enum DISCONNECTED:LX/45B;

.field public static final enum UNKNOWN:LX/45B;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 670191
    new-instance v0, LX/45B;

    const-string v1, "UNKNOWN"

    const-string v2, "unknown"

    invoke-direct {v0, v1, v3, v2}, LX/45B;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/45B;->UNKNOWN:LX/45B;

    .line 670192
    new-instance v0, LX/45B;

    const-string v1, "DISCONNECTED"

    const-string v2, "disconnected"

    invoke-direct {v0, v1, v4, v2}, LX/45B;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/45B;->DISCONNECTED:LX/45B;

    .line 670193
    new-instance v0, LX/45B;

    const-string v1, "CONNECTED"

    const-string v2, "connected"

    invoke-direct {v0, v1, v5, v2}, LX/45B;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/45B;->CONNECTED:LX/45B;

    .line 670194
    const/4 v0, 0x3

    new-array v0, v0, [LX/45B;

    sget-object v1, LX/45B;->UNKNOWN:LX/45B;

    aput-object v1, v0, v3

    sget-object v1, LX/45B;->DISCONNECTED:LX/45B;

    aput-object v1, v0, v4

    sget-object v1, LX/45B;->CONNECTED:LX/45B;

    aput-object v1, v0, v5

    sput-object v0, LX/45B;->$VALUES:[LX/45B;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 670195
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 670196
    iput-object p3, p0, LX/45B;->value:Ljava/lang/String;

    .line 670197
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/45B;
    .locals 1

    .prologue
    .line 670198
    const-class v0, LX/45B;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/45B;

    return-object v0
.end method

.method public static values()[LX/45B;
    .locals 1

    .prologue
    .line 670199
    sget-object v0, LX/45B;->$VALUES:[LX/45B;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/45B;

    return-object v0
.end method
