.class public LX/3T7;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/notifications/model/NotificationStories;

.field public final b:Lcom/facebook/notifications/model/NewNotificationStories;

.field private c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsDeltaFieldsModel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/notifications/model/NewNotificationStories;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 583803
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 583804
    iput-object v1, p0, LX/3T7;->a:Lcom/facebook/notifications/model/NotificationStories;

    .line 583805
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/model/NewNotificationStories;

    iput-object v0, p0, LX/3T7;->b:Lcom/facebook/notifications/model/NewNotificationStories;

    .line 583806
    iput-object v1, p0, LX/3T7;->c:LX/0Px;

    .line 583807
    return-void
.end method

.method public constructor <init>(Lcom/facebook/notifications/model/NotificationStories;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 583798
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 583799
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/model/NotificationStories;

    iput-object v0, p0, LX/3T7;->a:Lcom/facebook/notifications/model/NotificationStories;

    .line 583800
    iput-object v1, p0, LX/3T7;->b:Lcom/facebook/notifications/model/NewNotificationStories;

    .line 583801
    iput-object v1, p0, LX/3T7;->c:LX/0Px;

    .line 583802
    return-void
.end method

.method public static e(LX/3T7;)Z
    .locals 1

    .prologue
    .line 583797
    iget-object v0, p0, LX/3T7;->b:Lcom/facebook/notifications/model/NewNotificationStories;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 583783
    invoke-static {p0}, LX/3T7;->e(LX/3T7;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 583784
    iget-object v1, p0, LX/3T7;->b:Lcom/facebook/notifications/model/NewNotificationStories;

    invoke-virtual {v1}, Lcom/facebook/notifications/model/NewNotificationStories;->a()LX/0Px;

    move-result-object v1

    if-nez v1, :cond_1

    .line 583785
    :cond_0
    :goto_0
    return-void

    .line 583786
    :cond_1
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 583787
    iget-object v1, p0, LX/3T7;->b:Lcom/facebook/notifications/model/NewNotificationStories;

    invoke-virtual {v1}, Lcom/facebook/notifications/model/NewNotificationStories;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_2

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel;

    .line 583788
    invoke-static {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;->a(Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel;)Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 583789
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 583790
    :cond_2
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/3T7;->c:LX/0Px;

    goto :goto_0

    .line 583791
    :cond_3
    iget-object v1, p0, LX/3T7;->a:Lcom/facebook/notifications/model/NotificationStories;

    invoke-virtual {v1}, Lcom/facebook/notifications/model/NotificationStories;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 583792
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 583793
    iget-object v1, p0, LX/3T7;->a:Lcom/facebook/notifications/model/NotificationStories;

    invoke-virtual {v1}, Lcom/facebook/notifications/model/NotificationStories;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v1, v0

    :goto_2
    if-ge v1, v4, :cond_4

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2nq;

    .line 583794
    invoke-static {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;->a(LX/2nq;)Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 583795
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 583796
    :cond_4
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/3T7;->c:LX/0Px;

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/2nq;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 583780
    iget-object v0, p0, LX/3T7;->c:LX/0Px;

    if-nez v0, :cond_0

    .line 583781
    invoke-direct {p0}, LX/3T7;->f()V

    .line 583782
    :cond_0
    iget-object v0, p0, LX/3T7;->c:LX/0Px;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LX/3T7;->c:LX/0Px;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public final b()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 583777
    iget-object v0, p0, LX/3T7;->c:LX/0Px;

    if-nez v0, :cond_0

    .line 583778
    invoke-direct {p0}, LX/3T7;->f()V

    .line 583779
    :cond_0
    iget-object v0, p0, LX/3T7;->c:LX/0Px;

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsDeltaFieldsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 583740
    iget-object v0, p0, LX/3T7;->d:LX/0Px;

    if-nez v0, :cond_0

    .line 583741
    const/4 v0, 0x0

    .line 583742
    invoke-static {p0}, LX/3T7;->e(LX/3T7;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 583743
    iget-object v1, p0, LX/3T7;->b:Lcom/facebook/notifications/model/NewNotificationStories;

    invoke-virtual {v1}, Lcom/facebook/notifications/model/NewNotificationStories;->b()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaConnectionFieldsModel;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/3T7;->b:Lcom/facebook/notifications/model/NewNotificationStories;

    invoke-virtual {v1}, Lcom/facebook/notifications/model/NewNotificationStories;->b()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaConnectionFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaConnectionFieldsModel;->a()LX/0Px;

    move-result-object v1

    if-nez v1, :cond_1

    .line 583744
    :cond_0
    :goto_0
    iget-object v0, p0, LX/3T7;->d:LX/0Px;

    return-object v0

    .line 583745
    :cond_1
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 583746
    iget-object v1, p0, LX/3T7;->b:Lcom/facebook/notifications/model/NewNotificationStories;

    invoke-virtual {v1}, Lcom/facebook/notifications/model/NewNotificationStories;->b()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaConnectionFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaConnectionFieldsModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v1, v0

    .line 583747
    :goto_1
    if-ge v1, v4, :cond_2

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaFieldsModel;

    .line 583748
    if-nez v0, :cond_5

    .line 583749
    const/4 v5, 0x0

    .line 583750
    :goto_2
    move-object v0, v5

    .line 583751
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 583752
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 583753
    :cond_2
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/3T7;->d:LX/0Px;

    goto :goto_0

    .line 583754
    :cond_3
    iget-object v1, p0, LX/3T7;->a:Lcom/facebook/notifications/model/NotificationStories;

    invoke-virtual {v1}, Lcom/facebook/notifications/model/NotificationStories;->b()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsDeltaConnectionFieldsModel;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/3T7;->a:Lcom/facebook/notifications/model/NotificationStories;

    invoke-virtual {v1}, Lcom/facebook/notifications/model/NotificationStories;->b()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsDeltaConnectionFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsDeltaConnectionFieldsModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 583755
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 583756
    iget-object v1, p0, LX/3T7;->a:Lcom/facebook/notifications/model/NotificationStories;

    invoke-virtual {v1}, Lcom/facebook/notifications/model/NotificationStories;->b()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsDeltaConnectionFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsDeltaConnectionFieldsModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v1, v0

    .line 583757
    :goto_3
    if-ge v1, v4, :cond_4

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BAw;

    .line 583758
    if-nez v0, :cond_6

    .line 583759
    const/4 v0, 0x0

    .line 583760
    :goto_4
    move-object v0, v0

    .line 583761
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 583762
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 583763
    :cond_4
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/3T7;->d:LX/0Px;

    goto :goto_0

    .line 583764
    :cond_5
    new-instance v5, LX/BBD;

    invoke-direct {v5}, LX/BBD;-><init>()V

    .line 583765
    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaFieldsModel;->a()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;->a(Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel;)Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/BBD;->a(Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;)LX/BBD;

    .line 583766
    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaFieldsModel;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/BBD;->a(Ljava/lang/String;)LX/BBD;

    .line 583767
    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaFieldsModel;->c()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/BBD;->b(Ljava/lang/String;)LX/BBD;

    .line 583768
    invoke-virtual {v5}, LX/BBD;->a()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsDeltaFieldsModel;

    move-result-object v5

    goto :goto_2

    .line 583769
    :cond_6
    instance-of v5, v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsDeltaFieldsModel;

    if-eqz v5, :cond_7

    .line 583770
    check-cast v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsDeltaFieldsModel;

    goto :goto_4

    .line 583771
    :cond_7
    new-instance v5, LX/BBD;

    invoke-direct {v5}, LX/BBD;-><init>()V

    .line 583772
    invoke-interface {v0}, LX/BAw;->a()LX/2nq;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;->a(LX/2nq;)Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/BBD;->a(Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;)LX/BBD;

    .line 583773
    invoke-interface {v0}, LX/BAw;->e()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/BBD;->a(Ljava/lang/String;)LX/BBD;

    .line 583774
    invoke-interface {v0}, LX/BAw;->hY_()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/BBD;->b(Ljava/lang/String;)LX/BBD;

    .line 583775
    invoke-virtual {v5}, LX/BBD;->a()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsDeltaFieldsModel;

    move-result-object v0

    goto :goto_4
.end method

.method public final d()Lcom/facebook/graphql/model/GraphQLPageInfo;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 583776
    invoke-static {p0}, LX/3T7;->e(LX/3T7;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3T7;->b:Lcom/facebook/notifications/model/NewNotificationStories;

    invoke-virtual {v0}, Lcom/facebook/notifications/model/NewNotificationStories;->c()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/3T7;->a:Lcom/facebook/notifications/model/NotificationStories;

    invoke-virtual {v0}, Lcom/facebook/notifications/model/NotificationStories;->c()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    goto :goto_0
.end method
