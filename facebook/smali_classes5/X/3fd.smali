.class public LX/3fd;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile d:LX/3fd;


# instance fields
.field public a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/3gu;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 622769
    const-class v0, LX/3fd;

    sput-object v0, LX/3fd;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/3gu;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 622770
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 622771
    iput-object p1, p0, LX/3fd;->c:Ljava/util/Set;

    .line 622772
    return-void
.end method

.method public static a(LX/0QB;)LX/3fd;
    .locals 6

    .prologue
    .line 622773
    sget-object v0, LX/3fd;->d:LX/3fd;

    if-nez v0, :cond_1

    .line 622774
    const-class v1, LX/3fd;

    monitor-enter v1

    .line 622775
    :try_start_0
    sget-object v0, LX/3fd;->d:LX/3fd;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 622776
    if-eqz v2, :cond_0

    .line 622777
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 622778
    new-instance v3, LX/3fd;

    .line 622779
    new-instance v4, LX/0U8;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v5

    new-instance p0, LX/3fe;

    invoke-direct {p0, v0}, LX/3fe;-><init>(LX/0QB;)V

    invoke-direct {v4, v5, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v4, v4

    .line 622780
    invoke-direct {v3, v4}, LX/3fd;-><init>(Ljava/util/Set;)V

    .line 622781
    move-object v0, v3

    .line 622782
    sput-object v0, LX/3fd;->d:LX/3fd;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 622783
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 622784
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 622785
    :cond_1
    sget-object v0, LX/3fd;->d:LX/3fd;

    return-object v0

    .line 622786
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 622787
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
