.class public final LX/4wR;
.super LX/4wN;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LX/4wN",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field public volatile a:J

.field public b:LX/0R1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0R1",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public c:LX/0R1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0R1",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Object;ILX/0R1;)V
    .locals 2
    .param p3    # LX/0R1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;I",
            "LX/0R1",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 820152
    invoke-direct {p0, p1, p2, p3}, LX/4wN;-><init>(Ljava/lang/Object;ILX/0R1;)V

    .line 820153
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, LX/4wR;->a:J

    .line 820154
    sget-object v0, LX/0SZ;->INSTANCE:LX/0SZ;

    move-object v0, v0

    .line 820155
    iput-object v0, p0, LX/4wR;->b:LX/0R1;

    .line 820156
    sget-object v0, LX/0SZ;->INSTANCE:LX/0SZ;

    move-object v0, v0

    .line 820157
    iput-object v0, p0, LX/4wR;->c:LX/0R1;

    .line 820158
    return-void
.end method


# virtual methods
.method public final getNextInWriteQueue()LX/0R1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0R1",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 820151
    iget-object v0, p0, LX/4wR;->b:LX/0R1;

    return-object v0
.end method

.method public final getPreviousInWriteQueue()LX/0R1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0R1",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 820150
    iget-object v0, p0, LX/4wR;->c:LX/0R1;

    return-object v0
.end method

.method public final getWriteTime()J
    .locals 2

    .prologue
    .line 820149
    iget-wide v0, p0, LX/4wR;->a:J

    return-wide v0
.end method

.method public final setNextInWriteQueue(LX/0R1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0R1",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 820143
    iput-object p1, p0, LX/4wR;->b:LX/0R1;

    .line 820144
    return-void
.end method

.method public final setPreviousInWriteQueue(LX/0R1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0R1",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 820147
    iput-object p1, p0, LX/4wR;->c:LX/0R1;

    .line 820148
    return-void
.end method

.method public final setWriteTime(J)V
    .locals 1

    .prologue
    .line 820145
    iput-wide p1, p0, LX/4wR;->a:J

    .line 820146
    return-void
.end method
