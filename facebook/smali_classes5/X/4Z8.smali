.class public final LX/4Z8;
.super LX/0ur;
.source ""


# instance fields
.field public b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 787162
    invoke-direct {p0}, LX/0ur;-><init>()V

    .line 787163
    instance-of v0, p0, LX/4Z8;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 787164
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLImage;)LX/4Z8;
    .locals 0
    .param p1    # Lcom/facebook/graphql/model/GraphQLImage;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 787150
    iput-object p1, p0, LX/4Z8;->e:Lcom/facebook/graphql/model/GraphQLImage;

    .line 787151
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/4Z8;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 787160
    iput-object p1, p0, LX/4Z8;->b:Ljava/lang/String;

    .line 787161
    return-object p0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;
    .locals 2

    .prologue
    .line 787158
    new-instance v0, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;

    invoke-direct {v0, p0}, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;-><init>(LX/4Z8;)V

    .line 787159
    return-object v0
.end method

.method public final e(Ljava/lang/String;)LX/4Z8;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 787156
    iput-object p1, p0, LX/4Z8;->g:Ljava/lang/String;

    .line 787157
    return-object p0
.end method

.method public final g(Ljava/lang/String;)LX/4Z8;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 787154
    iput-object p1, p0, LX/4Z8;->i:Ljava/lang/String;

    .line 787155
    return-object p0
.end method

.method public final i(Ljava/lang/String;)LX/4Z8;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 787152
    iput-object p1, p0, LX/4Z8;->k:Ljava/lang/String;

    .line 787153
    return-object p0
.end method
