.class public LX/3mc;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/DCs;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinCoverItemComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 637015
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/3mc;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinCoverItemComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 637016
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 637017
    iput-object p1, p0, LX/3mc;->b:LX/0Ot;

    .line 637018
    return-void
.end method

.method public static a(LX/0QB;)LX/3mc;
    .locals 4

    .prologue
    .line 637019
    const-class v1, LX/3mc;

    monitor-enter v1

    .line 637020
    :try_start_0
    sget-object v0, LX/3mc;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 637021
    sput-object v2, LX/3mc;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 637022
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 637023
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 637024
    new-instance v3, LX/3mc;

    const/16 p0, 0x1ed6

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/3mc;-><init>(LX/0Ot;)V

    .line 637025
    move-object v0, v3

    .line 637026
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 637027
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/3mc;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 637028
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 637029
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 9

    .prologue
    .line 637030
    check-cast p2, LX/DCt;

    .line 637031
    iget-object v0, p0, LX/3mc;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinCoverItemComponentSpec;

    iget v1, p2, LX/DCt;->a:I

    iget-object v2, p2, LX/DCt;->b:Ljava/lang/String;

    iget-object v3, p2, LX/DCt;->c:Ljava/lang/String;

    .line 637032
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    const v5, 0x7f020a3f

    invoke-interface {v4, v5}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v4

    .line 637033
    invoke-static {p1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v5

    invoke-virtual {v5, v1}, LX/25Q;->h(I)LX/25Q;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    const v6, 0x7f0b0933

    invoke-interface {v5, v6}, LX/1Di;->i(I)LX/1Di;

    move-result-object v5

    const v6, 0x7f0b0f0a

    invoke-interface {v5, v6}, LX/1Di;->q(I)LX/1Di;

    move-result-object v5

    move-object v5, v5

    .line 637034
    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    const/4 p2, 0x3

    const/4 p0, 0x1

    const/4 v8, 0x0

    .line 637035
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, p0}, LX/1Dh;->C(I)LX/1Dh;

    move-result-object v5

    const v6, 0x7f0b0f0b

    invoke-interface {v5, v8, v6}, LX/1Dh;->x(II)LX/1Dh;

    move-result-object v5

    const v6, 0x7f0b0f10

    invoke-interface {v5, p0, v6}, LX/1Dh;->x(II)LX/1Dh;

    move-result-object v5

    const v6, 0x7f0b0f0e

    invoke-interface {v5, v6}, LX/1Dh;->G(I)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v8}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v5

    iget-object v6, v0, Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinCoverItemComponentSpec;->b:LX/1nu;

    invoke-virtual {v6, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v6

    sget-object v7, Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinCoverItemComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v6, v7}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v6

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v6

    sget-object v7, LX/1Up;->a:LX/1Up;

    invoke-virtual {v6, v7}, LX/1nw;->a(LX/1Up;)LX/1nw;

    move-result-object v6

    invoke-virtual {v6}, LX/1X5;->c()LX/1Di;

    move-result-object v6

    const v7, 0x7f0b0f11

    invoke-interface {v6, v7}, LX/1Di;->i(I)LX/1Di;

    move-result-object v6

    const v7, 0x7f0b0f12

    invoke-interface {v6, v7}, LX/1Di;->q(I)LX/1Di;

    move-result-object v6

    invoke-interface {v6}, LX/1Di;->k()LX/1Dg;

    move-result-object v6

    invoke-interface {v5, v6}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v5

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, v3}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, v8}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v6

    sget-object v7, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v6, v7}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v6

    const v7, 0x7f0b004f

    invoke-virtual {v6, v7}, LX/1ne;->q(I)LX/1ne;

    move-result-object v6

    .line 637036
    const/4 v7, -0x1

    if-ne v1, v7, :cond_0

    const v7, 0x7f0a00ab

    :goto_0
    move v7, v7

    .line 637037
    invoke-virtual {v6, v7}, LX/1ne;->n(I)LX/1ne;

    move-result-object v6

    const/4 v7, 0x2

    invoke-virtual {v6, v7}, LX/1ne;->j(I)LX/1ne;

    move-result-object v6

    sget-object v7, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v6, v7}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v6

    invoke-virtual {v6}, LX/1X5;->c()LX/1Di;

    move-result-object v6

    const v7, 0x7f020a8d

    invoke-interface {v6, v7}, LX/1Di;->x(I)LX/1Di;

    move-result-object v6

    invoke-interface {v6, p2, p2}, LX/1Di;->d(II)LX/1Di;

    move-result-object v6

    const/16 v7, 0xa

    invoke-interface {v6, p0, v7}, LX/1Di;->h(II)LX/1Di;

    move-result-object v6

    invoke-interface {v5, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v5

    move-object v5, v5

    .line 637038
    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    .line 637039
    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    move-object v0, v4

    .line 637040
    return-object v0

    :cond_0
    const v7, 0x7f0a00d5

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 637041
    invoke-static {}, LX/1dS;->b()V

    .line 637042
    const/4 v0, 0x0

    return-object v0
.end method
