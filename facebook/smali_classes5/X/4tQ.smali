.class public final LX/4tQ;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static a:LX/3KW;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/3KW",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static b:LX/3KW;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/3KW",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    const/16 v2, 0x64

    const-string v0, "gms:common:stats:max_num_of_events"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, LX/3KW;->a(Ljava/lang/String;Ljava/lang/Integer;)LX/3KW;

    move-result-object v0

    sput-object v0, LX/4tQ;->a:LX/3KW;

    const-string v0, "gms:common:stats:max_chunk_size"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, LX/3KW;->a(Ljava/lang/String;Ljava/lang/Integer;)LX/3KW;

    move-result-object v0

    sput-object v0, LX/4tQ;->b:LX/3KW;

    return-void
.end method
