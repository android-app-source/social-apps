.class public final LX/4BQ;
.super Landroid/widget/ArrayAdapter;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/CharSequence;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Landroid/widget/ListView;

.field public final synthetic b:LX/31a;


# direct methods
.method public constructor <init>(LX/31a;Landroid/content/Context;II[Ljava/lang/CharSequence;Landroid/widget/ListView;)V
    .locals 0

    .prologue
    .line 677718
    iput-object p1, p0, LX/4BQ;->b:LX/31a;

    iput-object p6, p0, LX/4BQ;->a:Landroid/widget/ListView;

    invoke-direct {p0, p2, p3, p4, p5}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;II[Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 677719
    invoke-super {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 677720
    iget-object v1, p0, LX/4BQ;->b:LX/31a;

    iget-object v1, v1, LX/31a;->E:[Z

    if-eqz v1, :cond_0

    .line 677721
    iget-object v1, p0, LX/4BQ;->b:LX/31a;

    iget-object v1, v1, LX/31a;->E:[Z

    aget-boolean v1, v1, p1

    .line 677722
    if-eqz v1, :cond_0

    .line 677723
    iget-object v1, p0, LX/4BQ;->a:Landroid/widget/ListView;

    const/4 v2, 0x1

    invoke-virtual {v1, p1, v2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 677724
    :cond_0
    return-object v0
.end method
