.class public LX/3mL;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/3ml;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3mq;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 636283
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/3mL;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/3mq;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 636280
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 636281
    iput-object p1, p0, LX/3mL;->b:LX/0Ot;

    .line 636282
    return-void
.end method

.method public static a(LX/0QB;)LX/3mL;
    .locals 4

    .prologue
    .line 636269
    const-class v1, LX/3mL;

    monitor-enter v1

    .line 636270
    :try_start_0
    sget-object v0, LX/3mL;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 636271
    sput-object v2, LX/3mL;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 636272
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 636273
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 636274
    new-instance v3, LX/3mL;

    const/16 p0, 0x3b1

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/3mL;-><init>(LX/0Ot;)V

    .line 636275
    move-object v0, v3

    .line 636276
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 636277
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/3mL;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 636278
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 636279
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 636267
    invoke-static {}, LX/1dS;->b()V

    .line 636268
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;LX/1Dg;IILX/1no;LX/1X1;)V
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/16 v0, 0x1e

    const v1, -0x787a1b3f

    invoke-static {v7, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 636260
    check-cast p6, LX/3mk;

    .line 636261
    iget-object v0, p0, LX/3mL;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3mq;

    iget-object v5, p6, LX/3mk;->a:LX/3mX;

    move-object v1, p1

    move v2, p3

    move v3, p4

    move-object v4, p5

    .line 636262
    invoke-virtual {v5}, LX/3mX;->e()I

    move-result p0

    if-nez p0, :cond_0

    .line 636263
    iget-object p0, v0, LX/3mq;->b:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/03V;

    const-string p1, "HScroll Empty"

    const-string p2, "HScrolls require at least one page"

    invoke-virtual {p0, p1, p2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 636264
    :cond_0
    const/4 p0, 0x0

    invoke-virtual {v5, v1, p0}, LX/3mY;->a(LX/1De;I)LX/1X1;

    move-result-object p0

    invoke-static {v1, p0}, LX/1cy;->a(LX/1De;LX/1X1;)LX/1me;

    move-result-object p0

    invoke-virtual {p0}, LX/1me;->b()LX/1dV;

    move-result-object p0

    .line 636265
    invoke-virtual {p0, v2, v3, v4}, LX/1dV;->a(IILX/1no;)V

    .line 636266
    const/16 v0, 0x1f

    const v1, -0x3650011c    # -1441756.5f

    invoke-static {v7, v0, v1, v6}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(LX/1De;LX/1Dg;LX/1X1;)V
    .locals 1

    .prologue
    .line 636256
    check-cast p3, LX/3mk;

    .line 636257
    iget-object v0, p0, LX/3mL;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p3, LX/3mk;->a:LX/3mX;

    .line 636258
    invoke-virtual {p2}, LX/1Dg;->c()I

    move-result p0

    invoke-virtual {p2}, LX/1Dg;->d()I

    move-result p1

    invoke-virtual {v0, p0, p1}, LX/3mY;->b(II)V

    .line 636259
    return-void
.end method

.method public final b(LX/1De;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 636253
    iget-object v0, p0, LX/3mL;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 636254
    new-instance v0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    invoke-direct {v0, p1}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;-><init>(Landroid/content/Context;)V

    move-object v0, v0

    .line 636255
    return-object v0
.end method

.method public final c(LX/1De;)LX/3ml;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 636284
    new-instance v1, LX/3mk;

    invoke-direct {v1, p0}, LX/3mk;-><init>(LX/3mL;)V

    .line 636285
    sget-object v2, LX/3mL;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/3ml;

    .line 636286
    if-nez v2, :cond_0

    .line 636287
    new-instance v2, LX/3ml;

    invoke-direct {v2}, LX/3ml;-><init>()V

    .line 636288
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/3ml;->a$redex0(LX/3ml;LX/1De;IILX/3mk;)V

    .line 636289
    move-object v1, v2

    .line 636290
    move-object v0, v1

    .line 636291
    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 636252
    const/4 v0, 0x1

    return v0
.end method

.method public final e(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 7

    .prologue
    .line 636241
    check-cast p3, LX/3mk;

    .line 636242
    iget-object v0, p0, LX/3mL;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-object v0, p2

    check-cast v0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    iget-object v1, p3, LX/3mk;->a:LX/3mX;

    iget-boolean v2, p3, LX/3mk;->b:Z

    iget-object v3, p3, LX/3mk;->c:LX/3x6;

    iget-object v4, p3, LX/3mk;->d:LX/1OX;

    iget-boolean v5, p3, LX/3mk;->e:Z

    iget-object v6, p3, LX/3mk;->f:LX/8yz;

    .line 636243
    invoke-virtual {v0, v2}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->setClipToPadding(Z)V

    .line 636244
    if-eqz v3, :cond_0

    .line 636245
    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x6;)V

    .line 636246
    :cond_0
    if-eqz v4, :cond_1

    .line 636247
    invoke-virtual {v0, v4}, Landroid/support/v7/widget/RecyclerView;->a(LX/1OX;)V

    .line 636248
    :cond_1
    invoke-virtual {v0, v5}, LX/25R;->setSnappingEnabled(Z)V

    .line 636249
    iput-object v6, v1, LX/3mX;->k:LX/8yz;

    .line 636250
    invoke-virtual {v1, v0}, LX/3mY;->a(Landroid/view/ViewGroup;)V

    .line 636251
    return-void
.end method

.method public final f()LX/1mv;
    .locals 1

    .prologue
    .line 636240
    sget-object v0, LX/1mv;->VIEW:LX/1mv;

    return-object v0
.end method

.method public final f(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 3

    .prologue
    .line 636229
    check-cast p3, LX/3mk;

    .line 636230
    iget-object v0, p0, LX/3mL;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    check-cast p2, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    iget-object v0, p3, LX/3mk;->a:LX/3mX;

    iget-object v1, p3, LX/3mk;->c:LX/3x6;

    iget-object v2, p3, LX/3mk;->d:LX/1OX;

    .line 636231
    invoke-virtual {v0, p2}, LX/3mY;->d(Landroid/view/ViewGroup;)V

    .line 636232
    const/4 p0, 0x0

    .line 636233
    iput-object p0, v0, LX/3mX;->k:LX/8yz;

    .line 636234
    if-eqz v1, :cond_0

    .line 636235
    invoke-virtual {p2, v1}, Landroid/support/v7/widget/RecyclerView;->b(LX/3x6;)V

    .line 636236
    :cond_0
    if-eqz v2, :cond_1

    .line 636237
    invoke-virtual {p2, v2}, Landroid/support/v7/widget/RecyclerView;->b(LX/1OX;)V

    .line 636238
    :cond_1
    const/4 p0, 0x1

    invoke-virtual {p2, p0}, LX/25R;->setSnappingEnabled(Z)V

    .line 636239
    return-void
.end method

.method public final g(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 2

    .prologue
    .line 636224
    check-cast p3, LX/3mk;

    .line 636225
    iget-object v0, p0, LX/3mL;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3mq;

    check-cast p2, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    iget-object v1, p3, LX/3mk;->a:LX/3mX;

    .line 636226
    iget-object p0, v0, LX/3mq;->a:LX/198;

    invoke-static {}, LX/25e;->c()LX/25e;

    move-result-object p1

    invoke-virtual {p0, p1}, LX/198;->c(LX/1YD;)V

    .line 636227
    invoke-virtual {v1, p2}, LX/3mY;->b(Landroid/view/ViewGroup;)V

    .line 636228
    return-void
.end method

.method public final h(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 1

    .prologue
    .line 636220
    check-cast p3, LX/3mk;

    .line 636221
    iget-object v0, p0, LX/3mL;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    check-cast p2, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    iget-object v0, p3, LX/3mk;->a:LX/3mX;

    .line 636222
    invoke-virtual {v0, p2}, LX/3mY;->c(Landroid/view/ViewGroup;)V

    .line 636223
    return-void
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 636219
    const/16 v0, 0xf

    return v0
.end method
