.class public final LX/4zj;
.super Ljava/lang/ref/WeakReference;
.source ""

# interfaces
.implements LX/0cp;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/ref/WeakReference",
        "<TV;>;",
        "LX/0cp",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field public final a:LX/0qF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0qF",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;LX/0qF;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/ReferenceQueue",
            "<TV;>;TV;",
            "LX/0qF",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 823071
    invoke-direct {p0, p2, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;Ljava/lang/ref/ReferenceQueue;)V

    .line 823072
    iput-object p3, p0, LX/4zj;->a:LX/0qF;

    .line 823073
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;LX/0qF;)LX/0cp;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/ReferenceQueue",
            "<TV;>;TV;",
            "LX/0qF",
            "<TK;TV;>;)",
            "LX/0cp",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 823070
    new-instance v0, LX/4zj;

    invoke-direct {v0, p1, p2, p3}, LX/4zj;-><init>(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;LX/0qF;)V

    return-object v0
.end method

.method public final a()LX/0qF;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0qF",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 823074
    iget-object v0, p0, LX/4zj;->a:LX/0qF;

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 823069
    const/4 v0, 0x0

    return v0
.end method

.method public final c()V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 823067
    invoke-virtual {p0}, LX/4zj;->clear()V

    .line 823068
    return-void
.end method
