.class public LX/3fs;
.super LX/0ro;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0ro",
        "<",
        "Lcom/facebook/contacts/server/FetchMultipleContactsByFbidParams;",
        "Lcom/facebook/contacts/server/FetchContactsResult;",
        ">;"
    }
.end annotation


# static fields
.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final c:LX/3fb;

.field private final d:LX/3fc;

.field private final e:LX/0SG;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 623689
    const-class v0, LX/3fs;

    sput-object v0, LX/3fs;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0sO;LX/3fb;LX/3fc;LX/0SG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 623690
    invoke-direct {p0, p1}, LX/0ro;-><init>(LX/0sO;)V

    .line 623691
    iput-object p2, p0, LX/3fs;->c:LX/3fb;

    .line 623692
    iput-object p3, p0, LX/3fs;->d:LX/3fc;

    .line 623693
    iput-object p4, p0, LX/3fs;->e:LX/0SG;

    .line 623694
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/1pN;LX/15w;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 623695
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 623696
    iget-object v0, p0, LX/0ro;->a:LX/0sO;

    const-class v2, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchContactsByProfileIdsQueryModel;

    invoke-static {v2}, LX/0w5;->b(Ljava/lang/Class;)LX/0w5;

    move-result-object v2

    invoke-virtual {v0, v2, p3}, LX/0sO;->a(LX/0w5;LX/15w;)Ljava/util/List;

    move-result-object v0

    .line 623697
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchContactsByProfileIdsQueryModel;

    .line 623698
    invoke-virtual {v0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchContactsByProfileIdsQueryModel;->a()Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;

    move-result-object v0

    .line 623699
    if-eqz v0, :cond_0

    .line 623700
    :try_start_0
    iget-object v3, p0, LX/3fs;->c:LX/3fb;

    invoke-virtual {v3, v0}, LX/3fb;->a(Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;)LX/3hB;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 623701
    invoke-virtual {v0}, LX/3hB;->O()Lcom/facebook/contacts/graphql/Contact;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 623702
    :catch_0
    move-exception v0

    .line 623703
    sget-object v1, LX/3fs;->b:Ljava/lang/Class;

    const-string v2, "Couldn\'t deserialize contact"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 623704
    throw v0

    .line 623705
    :cond_1
    new-instance v0, Lcom/facebook/contacts/server/FetchContactsResult;

    sget-object v2, LX/0ta;->FROM_SERVER:LX/0ta;

    iget-object v3, p0, LX/3fs;->e:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-direct {v0, v2, v4, v5, v1}, Lcom/facebook/contacts/server/FetchContactsResult;-><init>(LX/0ta;JLX/0Px;)V

    return-object v0
.end method

.method public final b(Ljava/lang/Object;LX/1pN;)I
    .locals 1

    .prologue
    .line 623706
    const/4 v0, 0x0

    return v0
.end method

.method public final f(Ljava/lang/Object;)LX/0gW;
    .locals 4

    .prologue
    .line 623707
    check-cast p1, Lcom/facebook/contacts/server/FetchMultipleContactsByFbidParams;

    .line 623708
    new-instance v0, LX/6MC;

    invoke-direct {v0}, LX/6MC;-><init>()V

    move-object v1, v0

    .line 623709
    iget-object v0, p0, LX/3fs;->d:LX/3fc;

    invoke-virtual {v0, v1}, LX/3fc;->a(LX/0gW;)V

    .line 623710
    iget-object v0, p0, LX/3fs;->d:LX/3fc;

    invoke-virtual {v0, v1}, LX/3fc;->c(LX/0gW;)V

    .line 623711
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 623712
    iget-object v0, p1, Lcom/facebook/contacts/server/FetchMultipleContactsByFbidParams;->a:LX/0Rf;

    move-object v0, v0

    .line 623713
    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserKey;

    .line 623714
    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 623715
    :cond_0
    const-string v0, "profile_ids"

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    .line 623716
    return-object v1
.end method
