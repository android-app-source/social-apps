.class public LX/4QS;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 704764
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 704772
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_4

    .line 704773
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 704774
    :goto_0
    return v1

    .line 704775
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_2

    .line 704776
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 704777
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 704778
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_0

    if-eqz v4, :cond_0

    .line 704779
    const-string v5, "count"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 704780
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v3, v0

    move v0, v2

    goto :goto_1

    .line 704781
    :cond_1
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 704782
    :cond_2
    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 704783
    if-eqz v0, :cond_3

    .line 704784
    invoke-virtual {p1, v1, v3, v1}, LX/186;->a(III)V

    .line 704785
    :cond_3
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move v3, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 704765
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 704766
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v0

    .line 704767
    if-eqz v0, :cond_0

    .line 704768
    const-string v1, "count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 704769
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 704770
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 704771
    return-void
.end method
