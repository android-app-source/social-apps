.class public LX/4dh;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/4dG;

.field private final b:LX/4CF;

.field private final c:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(LX/4dG;LX/4CF;)V
    .locals 3

    .prologue
    .line 796312
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 796313
    iput-object p1, p0, LX/4dh;->a:LX/4dG;

    .line 796314
    iput-object p2, p0, LX/4dh;->b:LX/4CF;

    .line 796315
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, LX/4dh;->c:Landroid/graphics/Paint;

    .line 796316
    iget-object v0, p0, LX/4dh;->c:Landroid/graphics/Paint;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 796317
    iget-object v0, p0, LX/4dh;->c:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 796318
    iget-object v0, p0, LX/4dh;->c:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 796319
    return-void
.end method

.method private a(ILandroid/graphics/Canvas;)I
    .locals 7

    .prologue
    .line 796320
    move v1, p1

    :goto_0
    if-ltz v1, :cond_3

    .line 796321
    iget-object v0, p0, LX/4dh;->a:LX/4dG;

    invoke-interface {v0, v1}, LX/4dG;->a(I)LX/4dL;

    move-result-object v0

    .line 796322
    iget-object v2, v0, LX/4dL;->g:LX/4dK;

    .line 796323
    sget-object v3, LX/4dK;->DISPOSE_DO_NOT:LX/4dK;

    if-ne v2, v3, :cond_4

    .line 796324
    sget-object v0, LX/4dg;->REQUIRED:LX/4dg;

    .line 796325
    :goto_1
    move-object v0, v0

    .line 796326
    sget-object v2, LX/4df;->a:[I

    invoke-virtual {v0}, LX/4dg;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    .line 796327
    :cond_0
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 796328
    :pswitch_0
    iget-object v0, p0, LX/4dh;->a:LX/4dG;

    invoke-interface {v0, v1}, LX/4dG;->a(I)LX/4dL;

    move-result-object v2

    .line 796329
    iget-object v0, p0, LX/4dh;->b:LX/4CF;

    invoke-interface {v0, v1}, LX/4CF;->a(I)LX/1FJ;

    move-result-object v3

    .line 796330
    if-eqz v3, :cond_2

    .line 796331
    :try_start_0
    invoke-virtual {v3}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {p2, v0, v4, v5, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 796332
    iget-object v0, v2, LX/4dL;->g:LX/4dK;

    sget-object v4, LX/4dK;->DISPOSE_TO_BACKGROUND:LX/4dK;

    if-ne v0, v4, :cond_1

    .line 796333
    invoke-static {p0, p2, v2}, LX/4dh;->a(LX/4dh;Landroid/graphics/Canvas;LX/4dL;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 796334
    :cond_1
    add-int/lit8 v0, v1, 0x1

    .line 796335
    invoke-virtual {v3}, LX/1FJ;->close()V

    .line 796336
    :goto_2
    return v0

    .line 796337
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, LX/1FJ;->close()V

    throw v0

    .line 796338
    :cond_2
    invoke-direct {p0, v1}, LX/4dh;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 796339
    goto :goto_2

    .line 796340
    :pswitch_1
    add-int/lit8 v0, v1, 0x1

    goto :goto_2

    :pswitch_2
    move v0, v1

    .line 796341
    goto :goto_2

    .line 796342
    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    .line 796343
    :cond_4
    sget-object v3, LX/4dK;->DISPOSE_TO_BACKGROUND:LX/4dK;

    if-ne v2, v3, :cond_6

    .line 796344
    invoke-static {p0, v0}, LX/4dh;->a(LX/4dh;LX/4dL;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 796345
    sget-object v0, LX/4dg;->NOT_REQUIRED:LX/4dg;

    goto :goto_1

    .line 796346
    :cond_5
    sget-object v0, LX/4dg;->REQUIRED:LX/4dg;

    goto :goto_1

    .line 796347
    :cond_6
    sget-object v0, LX/4dK;->DISPOSE_TO_PREVIOUS:LX/4dK;

    if-ne v2, v0, :cond_7

    .line 796348
    sget-object v0, LX/4dg;->SKIP:LX/4dg;

    goto :goto_1

    .line 796349
    :cond_7
    sget-object v0, LX/4dg;->ABORT:LX/4dg;

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static a(LX/4dh;Landroid/graphics/Canvas;LX/4dL;)V
    .locals 6

    .prologue
    .line 796350
    iget v0, p2, LX/4dL;->b:I

    int-to-float v1, v0

    iget v0, p2, LX/4dL;->c:I

    int-to-float v2, v0

    iget v0, p2, LX/4dL;->b:I

    iget v3, p2, LX/4dL;->d:I

    add-int/2addr v0, v3

    int-to-float v3, v0

    iget v0, p2, LX/4dL;->c:I

    iget v4, p2, LX/4dL;->e:I

    add-int/2addr v0, v4

    int-to-float v4, v0

    iget-object v5, p0, LX/4dh;->c:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 796351
    return-void
.end method

.method public static a(LX/4dh;LX/4dL;)Z
    .locals 2

    .prologue
    .line 796352
    iget v0, p1, LX/4dL;->b:I

    if-nez v0, :cond_0

    iget v0, p1, LX/4dL;->c:I

    if-nez v0, :cond_0

    iget v0, p1, LX/4dL;->d:I

    iget-object v1, p0, LX/4dh;->a:LX/4dG;

    invoke-interface {v1}, LX/4dG;->g()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget v0, p1, LX/4dL;->e:I

    iget-object v1, p0, LX/4dh;->a:LX/4dG;

    invoke-interface {v1}, LX/4dG;->h()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(I)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 796353
    if-nez p1, :cond_1

    .line 796354
    :cond_0
    :goto_0
    return v0

    .line 796355
    :cond_1
    iget-object v1, p0, LX/4dh;->a:LX/4dG;

    invoke-interface {v1, p1}, LX/4dG;->a(I)LX/4dL;

    move-result-object v1

    .line 796356
    iget-object v2, p0, LX/4dh;->a:LX/4dG;

    add-int/lit8 v3, p1, -0x1

    invoke-interface {v2, v3}, LX/4dG;->a(I)LX/4dL;

    move-result-object v2

    .line 796357
    iget-object v3, v1, LX/4dL;->f:LX/4dJ;

    sget-object v4, LX/4dJ;->NO_BLEND:LX/4dJ;

    if-ne v3, v4, :cond_2

    invoke-static {p0, v1}, LX/4dh;->a(LX/4dh;LX/4dL;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 796358
    :cond_2
    iget-object v1, v2, LX/4dL;->g:LX/4dK;

    sget-object v3, LX/4dK;->DISPOSE_TO_BACKGROUND:LX/4dK;

    if-ne v1, v3, :cond_3

    invoke-static {p0, v2}, LX/4dh;->a(LX/4dh;LX/4dL;)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(ILandroid/graphics/Bitmap;)V
    .locals 6

    .prologue
    .line 796359
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, p2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 796360
    const/4 v0, 0x0

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v1, v0, v2}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 796361
    invoke-direct {p0, p1}, LX/4dh;->b(I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 796362
    add-int/lit8 v0, p1, -0x1

    invoke-direct {p0, v0, v1}, LX/4dh;->a(ILandroid/graphics/Canvas;)I

    move-result v0

    .line 796363
    :goto_0
    if-ge v0, p1, :cond_3

    .line 796364
    iget-object v2, p0, LX/4dh;->a:LX/4dG;

    invoke-interface {v2, v0}, LX/4dG;->a(I)LX/4dL;

    move-result-object v2

    .line 796365
    iget-object v3, v2, LX/4dL;->g:LX/4dK;

    .line 796366
    sget-object v4, LX/4dK;->DISPOSE_TO_PREVIOUS:LX/4dK;

    if-eq v3, v4, :cond_1

    .line 796367
    iget-object v4, v2, LX/4dL;->f:LX/4dJ;

    sget-object v5, LX/4dJ;->NO_BLEND:LX/4dJ;

    if-ne v4, v5, :cond_0

    .line 796368
    invoke-static {p0, v1, v2}, LX/4dh;->a(LX/4dh;Landroid/graphics/Canvas;LX/4dL;)V

    .line 796369
    :cond_0
    iget-object v4, p0, LX/4dh;->a:LX/4dG;

    invoke-interface {v4, v0, v1}, LX/4dG;->a(ILandroid/graphics/Canvas;)V

    .line 796370
    iget-object v4, p0, LX/4dh;->b:LX/4CF;

    invoke-interface {v4, v0, p2}, LX/4CF;->a(ILandroid/graphics/Bitmap;)V

    .line 796371
    sget-object v4, LX/4dK;->DISPOSE_TO_BACKGROUND:LX/4dK;

    if-ne v3, v4, :cond_1

    .line 796372
    invoke-static {p0, v1, v2}, LX/4dh;->a(LX/4dh;Landroid/graphics/Canvas;LX/4dL;)V

    .line 796373
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, p1

    .line 796374
    goto :goto_0

    .line 796375
    :cond_3
    iget-object v0, p0, LX/4dh;->a:LX/4dG;

    invoke-interface {v0, p1}, LX/4dG;->a(I)LX/4dL;

    move-result-object v0

    .line 796376
    iget-object v2, v0, LX/4dL;->f:LX/4dJ;

    sget-object v3, LX/4dJ;->NO_BLEND:LX/4dJ;

    if-ne v2, v3, :cond_4

    .line 796377
    invoke-static {p0, v1, v0}, LX/4dh;->a(LX/4dh;Landroid/graphics/Canvas;LX/4dL;)V

    .line 796378
    :cond_4
    iget-object v0, p0, LX/4dh;->a:LX/4dG;

    invoke-interface {v0, p1, v1}, LX/4dG;->a(ILandroid/graphics/Canvas;)V

    .line 796379
    return-void
.end method
