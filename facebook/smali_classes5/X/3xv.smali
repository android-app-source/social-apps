.class public LX/3xv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field private final a:Ljava/lang/Object;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/3xu;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/concurrent/ScheduledExecutorService;

.field public d:Ljava/util/concurrent/ScheduledFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ScheduledFuture",
            "<*>;"
        }
    .end annotation
.end field

.field private e:Z

.field private f:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 660462
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 660463
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/3xv;->a:Ljava/lang/Object;

    .line 660464
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/3xv;->b:Ljava/util/List;

    .line 660465
    sget-object v0, LX/1eh;->a:LX/1eh;

    iget-object v0, v0, LX/1eh;->c:Ljava/util/concurrent/ScheduledExecutorService;

    move-object v0, v0

    .line 660466
    iput-object v0, p0, LX/3xv;->c:Ljava/util/concurrent/ScheduledExecutorService;

    .line 660467
    return-void
.end method

.method private static b(LX/3xv;)V
    .locals 2

    .prologue
    .line 660459
    iget-boolean v0, p0, LX/3xv;->f:Z

    if-eqz v0, :cond_0

    .line 660460
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Object already closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 660461
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/3xu;)V
    .locals 2

    .prologue
    .line 660455
    iget-object v1, p0, LX/3xv;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 660456
    :try_start_0
    invoke-static {p0}, LX/3xv;->b(LX/3xv;)V

    .line 660457
    iget-object v0, p0, LX/3xv;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 660458
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 660437
    iget-object v1, p0, LX/3xv;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 660438
    :try_start_0
    invoke-static {p0}, LX/3xv;->b(LX/3xv;)V

    .line 660439
    iget-boolean v0, p0, LX/3xv;->e:Z

    monitor-exit v1

    return v0

    .line 660440
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final close()V
    .locals 3

    .prologue
    .line 660442
    iget-object v1, p0, LX/3xv;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 660443
    :try_start_0
    iget-boolean v0, p0, LX/3xv;->f:Z

    if-eqz v0, :cond_0

    .line 660444
    monitor-exit v1

    .line 660445
    :goto_0
    return-void

    .line 660446
    :cond_0
    iget-object v0, p0, LX/3xv;->d:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_1

    .line 660447
    iget-object v0, p0, LX/3xv;->d:Ljava/util/concurrent/ScheduledFuture;

    const/4 v2, 0x1

    invoke-interface {v0, v2}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 660448
    const/4 v0, 0x0

    iput-object v0, p0, LX/3xv;->d:Ljava/util/concurrent/ScheduledFuture;

    .line 660449
    :cond_1
    iget-object v0, p0, LX/3xv;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3xu;

    .line 660450
    invoke-virtual {v0}, LX/3xu;->close()V

    goto :goto_1

    .line 660451
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 660452
    :cond_2
    :try_start_1
    iget-object v0, p0, LX/3xv;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 660453
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/3xv;->f:Z

    .line 660454
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 660441
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%s@%s[cancellationRequested=%s]"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-virtual {p0}, LX/3xv;->a()Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
