.class public LX/3zY;
.super LX/0Tv;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile b:LX/3zY;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 662490
    const-class v0, LX/3zY;

    sput-object v0, LX/3zY;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 662488
    const-string v0, "analytics"

    const/4 v1, 0x6

    new-instance v2, LX/3zX;

    invoke-direct {v2}, LX/3zX;-><init>()V

    new-instance v3, LX/3zW;

    invoke-direct {v3}, LX/3zW;-><init>()V

    invoke-static {v2, v3}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, LX/0Tv;-><init>(Ljava/lang/String;ILX/0Px;)V

    .line 662489
    return-void
.end method

.method public static a(LX/0QB;)LX/3zY;
    .locals 3

    .prologue
    .line 662476
    sget-object v0, LX/3zY;->b:LX/3zY;

    if-nez v0, :cond_1

    .line 662477
    const-class v1, LX/3zY;

    monitor-enter v1

    .line 662478
    :try_start_0
    sget-object v0, LX/3zY;->b:LX/3zY;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 662479
    if-eqz v2, :cond_0

    .line 662480
    :try_start_1
    new-instance v0, LX/3zY;

    invoke-direct {v0}, LX/3zY;-><init>()V

    .line 662481
    move-object v0, v0

    .line 662482
    sput-object v0, LX/3zY;->b:LX/3zY;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 662483
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 662484
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 662485
    :cond_1
    sget-object v0, LX/3zY;->b:LX/3zY;

    return-object v0

    .line 662486
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 662487
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 2

    .prologue
    .line 662473
    const-string v0, "batches"

    invoke-static {v0}, LX/0Tz;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const v1, 0x674f4bc8

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x456620c6

    invoke-static {v0}, LX/03h;->a(I)V

    .line 662474
    invoke-super {p0, p1, p2, p3}, LX/0Tv;->a(Landroid/database/sqlite/SQLiteDatabase;II)V

    .line 662475
    return-void
.end method
