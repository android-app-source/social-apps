.class public LX/4hz;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:[Ljava/lang/Class;

.field private static volatile c:LX/4hz;


# instance fields
.field private b:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 802274
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Class;

    const/4 v1, 0x0

    const-class v2, Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-class v2, Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-class v2, Ljava/lang/Long;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-class v2, Ljava/lang/Double;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-class v2, Ljava/lang/String;

    aput-object v2, v0, v1

    sput-object v0, LX/4hz;->a:[Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 802275
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 802276
    iput-object p1, p0, LX/4hz;->b:LX/03V;

    .line 802277
    return-void
.end method

.method public static a(LX/0QB;)LX/4hz;
    .locals 4

    .prologue
    .line 802278
    sget-object v0, LX/4hz;->c:LX/4hz;

    if-nez v0, :cond_1

    .line 802279
    const-class v1, LX/4hz;

    monitor-enter v1

    .line 802280
    :try_start_0
    sget-object v0, LX/4hz;->c:LX/4hz;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 802281
    if-eqz v2, :cond_0

    .line 802282
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 802283
    new-instance p0, LX/4hz;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-direct {p0, v3}, LX/4hz;-><init>(LX/03V;)V

    .line 802284
    move-object v0, p0

    .line 802285
    sput-object v0, LX/4hz;->c:LX/4hz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 802286
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 802287
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 802288
    :cond_1
    sget-object v0, LX/4hz;->c:LX/4hz;

    return-object v0

    .line 802289
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 802290
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Landroid/os/Bundle;[Ljava/lang/Class;)Lorg/json/JSONObject;
    .locals 14

    .prologue
    const/4 v6, 0x0

    const/4 v9, 0x0

    .line 802291
    if-nez p1, :cond_0

    .line 802292
    :goto_0
    return-object v6

    .line 802293
    :cond_0
    new-instance v7, Lorg/json/JSONObject;

    invoke-direct {v7}, Lorg/json/JSONObject;-><init>()V

    .line 802294
    invoke-virtual {p1}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_11

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 802295
    invoke-virtual {p1, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    .line 802296
    if-nez v4, :cond_1

    .line 802297
    sget-object v3, Lorg/json/JSONObject;->NULL:Ljava/lang/Object;

    .line 802298
    :goto_2
    if-eqz v3, :cond_10

    .line 802299
    :try_start_0
    invoke-virtual {v7, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 802300
    :catch_0
    move-exception v2

    .line 802301
    iget-object v4, p0, LX/4hz;->b:LX/03V;

    const-string v5, "PlatformBundleToJSONConverter"

    const-string v8, "Unsupported value for JSON : %s"

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v8, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v5, v3, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 802302
    :cond_1
    instance-of v3, v4, Landroid/os/Bundle;

    if-eqz v3, :cond_2

    move-object v3, v4

    .line 802303
    check-cast v3, Landroid/os/Bundle;

    move-object/from16 v0, p2

    invoke-direct {p0, v3, v0}, LX/4hz;->a(Landroid/os/Bundle;[Ljava/lang/Class;)Lorg/json/JSONObject;

    move-result-object v3

    goto :goto_2

    .line 802304
    :cond_2
    move-object/from16 v0, p2

    array-length v5, v0

    move v3, v9

    :goto_3
    if-ge v3, v5, :cond_12

    aget-object v8, p2, v3

    .line 802305
    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v11

    invoke-virtual {v8, v11}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 802306
    const/4 v3, 0x1

    .line 802307
    :goto_4
    if-eqz v3, :cond_4

    move-object v3, v4

    .line 802308
    goto :goto_2

    .line 802309
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 802310
    :cond_4
    new-instance v5, Lorg/json/JSONArray;

    invoke-direct {v5}, Lorg/json/JSONArray;-><init>()V

    .line 802311
    instance-of v3, v4, [I

    if-eqz v3, :cond_6

    move-object v3, v4

    .line 802312
    check-cast v3, [I

    check-cast v3, [I

    array-length v11, v3

    move v8, v9

    :goto_5
    if-ge v8, v11, :cond_5

    aget v12, v3, v8

    .line 802313
    invoke-virtual {v5, v12}, Lorg/json/JSONArray;->put(I)Lorg/json/JSONArray;

    .line 802314
    add-int/lit8 v8, v8, 0x1

    goto :goto_5

    :cond_5
    move-object v3, v5

    goto :goto_2

    .line 802315
    :cond_6
    instance-of v3, v4, [J

    if-eqz v3, :cond_8

    move-object v3, v4

    .line 802316
    check-cast v3, [J

    check-cast v3, [J

    array-length v11, v3

    move v8, v9

    :goto_6
    if-ge v8, v11, :cond_7

    aget-wide v12, v3, v8

    .line 802317
    invoke-virtual {v5, v12, v13}, Lorg/json/JSONArray;->put(J)Lorg/json/JSONArray;

    .line 802318
    add-int/lit8 v8, v8, 0x1

    goto :goto_6

    :cond_7
    move-object v3, v5

    goto :goto_2

    .line 802319
    :cond_8
    instance-of v3, v4, [D

    if-eqz v3, :cond_a

    move-object v3, v4

    .line 802320
    check-cast v3, [D

    check-cast v3, [D

    array-length v11, v3

    move v8, v9

    :goto_7
    if-ge v8, v11, :cond_9

    aget-wide v12, v3, v8

    .line 802321
    :try_start_1
    invoke-virtual {v5, v12, v13}, Lorg/json/JSONArray;->put(D)Lorg/json/JSONArray;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 802322
    :goto_8
    add-int/lit8 v8, v8, 0x1

    goto :goto_7

    .line 802323
    :catch_1
    invoke-virtual {v5, v9}, Lorg/json/JSONArray;->put(I)Lorg/json/JSONArray;

    goto :goto_8

    :cond_9
    move-object v3, v5

    .line 802324
    goto/16 :goto_2

    .line 802325
    :cond_a
    instance-of v3, v4, [Z

    if-eqz v3, :cond_c

    move-object v3, v4

    .line 802326
    check-cast v3, [Z

    check-cast v3, [Z

    array-length v11, v3

    move v8, v9

    :goto_9
    if-ge v8, v11, :cond_b

    aget-boolean v12, v3, v8

    .line 802327
    invoke-virtual {v5, v12}, Lorg/json/JSONArray;->put(Z)Lorg/json/JSONArray;

    .line 802328
    add-int/lit8 v8, v8, 0x1

    goto :goto_9

    :cond_b
    move-object v3, v5

    goto/16 :goto_2

    .line 802329
    :cond_c
    instance-of v3, v4, Ljava/util/List;

    if-eqz v3, :cond_e

    .line 802330
    :try_start_2
    move-object v0, v4

    check-cast v0, Ljava/util/List;

    move-object v3, v0

    .line 802331
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_a
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_f

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 802332
    if-nez v3, :cond_d

    sget-object v3, Lorg/json/JSONObject;->NULL:Ljava/lang/Object;

    :cond_d
    invoke-virtual {v5, v3}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_2
    .catch Ljava/lang/ClassCastException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_a

    :catch_2
    :cond_e
    move-object v3, v6

    .line 802333
    goto/16 :goto_2

    :cond_f
    move-object v3, v5

    .line 802334
    goto/16 :goto_2

    .line 802335
    :cond_10
    iget-object v2, p0, LX/4hz;->b:LX/03V;

    const-string v3, "PlatformBundleToJSONConverter"

    const-string v5, "Unsupported type for JSON : %s"

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_11
    move-object v6, v7

    .line 802336
    goto/16 :goto_0

    :cond_12
    move v3, v9

    goto/16 :goto_4
.end method


# virtual methods
.method public final a(Lorg/json/JSONObject;)Landroid/os/Bundle;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 802337
    if-nez p1, :cond_0

    move-object v0, v2

    .line 802338
    :goto_0
    return-object v0

    .line 802339
    :cond_0
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 802340
    invoke-virtual {p1}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v4

    .line 802341
    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 802342
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 802343
    :try_start_0
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 802344
    if-eqz v1, :cond_1

    .line 802345
    instance-of v5, v1, Ljava/lang/String;

    if-eqz v5, :cond_2

    .line 802346
    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 802347
    :catch_0
    move-exception v1

    .line 802348
    iget-object v5, p0, LX/4hz;->b:LX/03V;

    const-string v6, "PlatformBundleToJSONConverter"

    const-string v7, "JSONObject.keys() provided a problematic key : %s"

    invoke-static {v7, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v6, v0, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 802349
    :cond_2
    instance-of v5, v1, Ljava/lang/Integer;

    if-eqz v5, :cond_3

    .line 802350
    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_1

    .line 802351
    :cond_3
    instance-of v5, v1, Ljava/lang/Double;

    if-eqz v5, :cond_4

    .line 802352
    check-cast v1, Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    invoke-virtual {v3, v0, v6, v7}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    goto :goto_1

    .line 802353
    :cond_4
    instance-of v5, v1, Ljava/lang/Long;

    if-eqz v5, :cond_5

    .line 802354
    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v3, v0, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    goto :goto_1

    .line 802355
    :cond_5
    instance-of v5, v1, Ljava/lang/Boolean;

    if-eqz v5, :cond_6

    .line 802356
    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_1

    .line 802357
    :cond_6
    instance-of v5, v1, Lorg/json/JSONObject;

    if-eqz v5, :cond_7

    .line 802358
    check-cast v1, Lorg/json/JSONObject;

    invoke-virtual {p0, v1}, LX/4hz;->a(Lorg/json/JSONObject;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_1

    .line 802359
    :cond_7
    sget-object v5, Lorg/json/JSONObject;->NULL:Ljava/lang/Object;

    if-ne v1, v5, :cond_1

    .line 802360
    invoke-virtual {v3, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_8
    move-object v0, v3

    .line 802361
    goto/16 :goto_0
.end method

.method public final a(Landroid/os/Bundle;)Lorg/json/JSONObject;
    .locals 1

    .prologue
    .line 802362
    sget-object v0, LX/4hz;->a:[Ljava/lang/Class;

    invoke-direct {p0, p1, v0}, LX/4hz;->a(Landroid/os/Bundle;[Ljava/lang/Class;)Lorg/json/JSONObject;

    move-result-object v0

    return-object v0
.end method
