.class public final LX/3qx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Landroid/support/v4/media/MediaDescriptionCompat;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 642578
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 13

    .prologue
    .line 642579
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-ge v0, v1, :cond_0

    .line 642580
    new-instance v0, Landroid/support/v4/media/MediaDescriptionCompat;

    invoke-direct {v0, p1}, Landroid/support/v4/media/MediaDescriptionCompat;-><init>(Landroid/os/Parcel;)V

    .line 642581
    :goto_0
    return-object v0

    .line 642582
    :cond_0
    sget-object v0, Landroid/media/MediaDescription;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    move-object v0, v0

    .line 642583
    if-eqz v0, :cond_1

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x15

    if-ge v2, v3, :cond_2

    .line 642584
    :cond_1
    const/4 v2, 0x0

    .line 642585
    :goto_1
    move-object v0, v2

    .line 642586
    goto :goto_0

    .line 642587
    :cond_2
    new-instance v2, LX/3qy;

    invoke-direct {v2}, LX/3qy;-><init>()V

    .line 642588
    invoke-static {v0}, LX/3qz;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 642589
    iput-object v3, v2, LX/3qy;->a:Ljava/lang/String;

    .line 642590
    invoke-static {v0}, LX/3qz;->b(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v3

    .line 642591
    iput-object v3, v2, LX/3qy;->b:Ljava/lang/CharSequence;

    .line 642592
    invoke-static {v0}, LX/3qz;->c(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v3

    .line 642593
    iput-object v3, v2, LX/3qy;->c:Ljava/lang/CharSequence;

    .line 642594
    invoke-static {v0}, LX/3qz;->d(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v3

    .line 642595
    iput-object v3, v2, LX/3qy;->d:Ljava/lang/CharSequence;

    .line 642596
    invoke-static {v0}, LX/3qz;->e(Ljava/lang/Object;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 642597
    iput-object v3, v2, LX/3qy;->e:Landroid/graphics/Bitmap;

    .line 642598
    invoke-static {v0}, LX/3qz;->f(Ljava/lang/Object;)Landroid/net/Uri;

    move-result-object v3

    .line 642599
    iput-object v3, v2, LX/3qy;->f:Landroid/net/Uri;

    .line 642600
    invoke-static {v0}, LX/3qz;->g(Ljava/lang/Object;)Landroid/os/Bundle;

    move-result-object v3

    .line 642601
    iput-object v3, v2, LX/3qy;->g:Landroid/os/Bundle;

    .line 642602
    new-instance v4, Landroid/support/v4/media/MediaDescriptionCompat;

    iget-object v5, v2, LX/3qy;->a:Ljava/lang/String;

    iget-object v6, v2, LX/3qy;->b:Ljava/lang/CharSequence;

    iget-object v7, v2, LX/3qy;->c:Ljava/lang/CharSequence;

    iget-object v8, v2, LX/3qy;->d:Ljava/lang/CharSequence;

    iget-object v9, v2, LX/3qy;->e:Landroid/graphics/Bitmap;

    iget-object v10, v2, LX/3qy;->f:Landroid/net/Uri;

    iget-object v11, v2, LX/3qy;->g:Landroid/os/Bundle;

    const/4 v12, 0x0

    invoke-direct/range {v4 .. v12}, Landroid/support/v4/media/MediaDescriptionCompat;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/graphics/Bitmap;Landroid/net/Uri;Landroid/os/Bundle;B)V

    move-object v2, v4

    .line 642603
    iput-object v0, v2, Landroid/support/v4/media/MediaDescriptionCompat;->h:Ljava/lang/Object;

    goto :goto_1
.end method

.method public final newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 642604
    new-array v0, p1, [Landroid/support/v4/media/MediaDescriptionCompat;

    return-object v0
.end method
