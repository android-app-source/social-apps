.class public final LX/3gw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionsVectorDataQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel$AnimationModel;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/feedback/reactions/ui/FeedbackReactionsDownloader$PersistanceRunnable;


# direct methods
.method public constructor <init>(Lcom/facebook/feedback/reactions/ui/FeedbackReactionsDownloader$PersistanceRunnable;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 624934
    iput-object p1, p0, LX/3gw;->b:Lcom/facebook/feedback/reactions/ui/FeedbackReactionsDownloader$PersistanceRunnable;

    iput-object p2, p0, LX/3gw;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 624935
    check-cast p1, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionsVectorDataQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel$AnimationModel;

    .line 624936
    :try_start_0
    iget-object v1, p0, LX/3gw;->a:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionsVectorDataQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel$AnimationModel;->a()Ljava/lang/String;

    move-result-object v2

    .line 624937
    invoke-static {v1, v2}, Lcom/facebook/feedback/reactions/ui/FeedbackReactionsDownloader$PersistanceRunnable;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 624938
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 624939
    :goto_0
    return-object v0

    .line 624940
    :catch_0
    move-exception v0

    .line 624941
    sget-object v1, LX/1zn;->a:Ljava/lang/Class;

    const-string v2, "Failed to save vector - "

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 624942
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method
