.class public final LX/4Yb;
.super LX/0ur;
.source ""


# instance fields
.field public b:Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:J

.field public f:I

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 781684
    invoke-direct {p0}, LX/0ur;-><init>()V

    .line 781685
    const/4 v0, 0x0

    iput-object v0, p0, LX/4Yb;->n:LX/0x2;

    .line 781686
    instance-of v0, p0, LX/4Yb;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 781687
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;)LX/4Yb;
    .locals 4

    .prologue
    .line 781688
    new-instance v1, LX/4Yb;

    invoke-direct {v1}, LX/4Yb;-><init>()V

    .line 781689
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 781690
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsConnection;

    move-result-object v0

    iput-object v0, v1, LX/4Yb;->b:Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsConnection;

    .line 781691
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/4Yb;->c:Ljava/lang/String;

    .line 781692
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->E_()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/4Yb;->d:Ljava/lang/String;

    .line 781693
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->F_()J

    move-result-wide v2

    iput-wide v2, v1, LX/4Yb;->e:J

    .line 781694
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->o()I

    move-result v0

    iput v0, v1, LX/4Yb;->f:I

    .line 781695
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->r()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/4Yb;->g:Ljava/lang/String;

    .line 781696
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->w()LX/0Px;

    move-result-object v0

    iput-object v0, v1, LX/4Yb;->h:LX/0Px;

    .line 781697
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->s()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/4Yb;->i:Ljava/lang/String;

    .line 781698
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    iput-object v0, v1, LX/4Yb;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 781699
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    iput-object v0, v1, LX/4Yb;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 781700
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/4Yb;->l:Ljava/lang/String;

    .line 781701
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->v()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/4Yb;->m:Ljava/lang/String;

    .line 781702
    invoke-static {v1, p0}, LX/0ur;->a(LX/0ur;Lcom/facebook/graphql/modelutil/BaseModel;)V

    .line 781703
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;->L_()LX/0x2;

    move-result-object v0

    invoke-virtual {v0}, LX/0x2;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0x2;

    iput-object v0, v1, LX/4Yb;->n:LX/0x2;

    .line 781704
    return-object v1
.end method


# virtual methods
.method public final a()Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;
    .locals 2

    .prologue
    .line 781705
    new-instance v0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;

    invoke-direct {v0, p0}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnit;-><init>(LX/4Yb;)V

    .line 781706
    return-object v0
.end method
