.class public LX/3pz;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 641962
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 641963
    return-void
.end method

.method public static a(Landroid/app/Notification$Builder;LX/3pa;)V
    .locals 5

    .prologue
    .line 641964
    new-instance v1, Landroid/app/Notification$Action$Builder;

    invoke-virtual {p1}, LX/3pa;->a()I

    move-result v0

    invoke-virtual {p1}, LX/3pa;->b()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {p1}, LX/3pa;->c()Landroid/app/PendingIntent;

    move-result-object v3

    invoke-direct {v1, v0, v2, v3}, Landroid/app/Notification$Action$Builder;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 641965
    invoke-virtual {p1}, LX/3pa;->e()[LX/3qK;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 641966
    invoke-virtual {p1}, LX/3pa;->e()[LX/3qK;

    move-result-object v0

    invoke-static {v0}, LX/3qM;->a([LX/3qK;)[Landroid/app/RemoteInput;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 641967
    invoke-virtual {v1, v4}, Landroid/app/Notification$Action$Builder;->addRemoteInput(Landroid/app/RemoteInput;)Landroid/app/Notification$Action$Builder;

    .line 641968
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 641969
    :cond_0
    invoke-virtual {p1}, LX/3pa;->d()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 641970
    invoke-virtual {p1}, LX/3pa;->d()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/Notification$Action$Builder;->addExtras(Landroid/os/Bundle;)Landroid/app/Notification$Action$Builder;

    .line 641971
    :cond_1
    invoke-virtual {v1}, Landroid/app/Notification$Action$Builder;->build()Landroid/app/Notification$Action;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/app/Notification$Builder;->addAction(Landroid/app/Notification$Action;)Landroid/app/Notification$Builder;

    .line 641972
    return-void
.end method
