.class public final LX/3yw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;)V
    .locals 0

    .prologue
    .line 661780
    iput-object p1, p0, LX/3yw;->a:Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 661781
    iget-object v0, p0, LX/3yw;->a:Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;

    iget-object v0, v0, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->o:LX/0Px;

    if-nez v0, :cond_0

    .line 661782
    :goto_0
    return v5

    .line 661783
    :cond_0
    iget-object v0, p0, LX/3yw;->a:Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;

    iget-object v0, v0, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->o:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    move v1, v2

    :goto_1
    if-ge v1, v3, :cond_2

    iget-object v0, p0, LX/3yw;->a:Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;

    iget-object v0, v0, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->o:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3yQ;

    .line 661784
    invoke-virtual {v0}, LX/3yQ;->d()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 661785
    iget-object v4, p0, LX/3yw;->a:Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;

    iget-object v4, v4, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->i:LX/3z2;

    .line 661786
    iget-object p1, v0, LX/3yQ;->name:Ljava/lang/String;

    move-object v0, p1

    .line 661787
    invoke-virtual {v4, v0}, LX/3z2;->a(Ljava/lang/String;)V

    .line 661788
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 661789
    :cond_2
    iget-object v0, p0, LX/3yw;->a:Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;

    .line 661790
    invoke-static {v0, v2}, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->a$redex0(Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;Z)V

    .line 661791
    goto :goto_0
.end method
