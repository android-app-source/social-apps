.class public final LX/4Xw;
.super LX/0ur;
.source ""


# instance fields
.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsEdge;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lcom/facebook/graphql/model/GraphQLPageInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 773620
    invoke-direct {p0}, LX/0ur;-><init>()V

    .line 773621
    instance-of v0, p0, LX/4Xw;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 773622
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsConnection;)LX/4Xw;
    .locals 2

    .prologue
    .line 773625
    new-instance v0, LX/4Xw;

    invoke-direct {v0}, LX/4Xw;-><init>()V

    .line 773626
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 773627
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsConnection;->a()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4Xw;->b:LX/0Px;

    .line 773628
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsConnection;->j()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v1

    iput-object v1, v0, LX/4Xw;->c:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 773629
    invoke-static {v0, p0}, LX/0ur;->a(LX/0ur;Lcom/facebook/graphql/modelutil/BaseModel;)V

    .line 773630
    return-object v0
.end method


# virtual methods
.method public final a()Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsConnection;
    .locals 2

    .prologue
    .line 773623
    new-instance v0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsConnection;

    invoke-direct {v0, p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsConnection;-><init>(LX/4Xw;)V

    .line 773624
    return-object v0
.end method
