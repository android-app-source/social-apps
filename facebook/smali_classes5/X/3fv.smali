.class public LX/3fv;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final j:Ljava/lang/Object;


# instance fields
.field private final a:LX/3fw;

.field private final b:LX/3fr;

.field private final c:LX/3fX;

.field private final d:LX/0Xl;

.field private final e:LX/0tX;

.field private final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2Jp;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3fg;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6Nd;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/2RQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 623767
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/3fv;->j:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/3fw;LX/3fr;LX/3fX;LX/0Xl;LX/0tX;LX/0Or;LX/0Ot;LX/0Ot;LX/2RQ;)V
    .locals 0
    .param p4    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3fw;",
            "LX/3fr;",
            "LX/3fX;",
            "LX/0Xl;",
            "LX/0tX;",
            "LX/0Or",
            "<",
            "LX/2Jp;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/3fg;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/6Nd;",
            ">;",
            "LX/2RQ;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 623768
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 623769
    iput-object p1, p0, LX/3fv;->a:LX/3fw;

    .line 623770
    iput-object p2, p0, LX/3fv;->b:LX/3fr;

    .line 623771
    iput-object p3, p0, LX/3fv;->c:LX/3fX;

    .line 623772
    iput-object p6, p0, LX/3fv;->f:LX/0Or;

    .line 623773
    iput-object p7, p0, LX/3fv;->g:LX/0Ot;

    .line 623774
    iput-object p8, p0, LX/3fv;->h:LX/0Ot;

    .line 623775
    iput-object p4, p0, LX/3fv;->d:LX/0Xl;

    .line 623776
    iput-object p5, p0, LX/3fv;->e:LX/0tX;

    .line 623777
    iput-object p9, p0, LX/3fv;->i:LX/2RQ;

    .line 623778
    return-void
.end method

.method public static a(LX/0QB;)LX/3fv;
    .locals 7

    .prologue
    .line 623779
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 623780
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 623781
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 623782
    if-nez v1, :cond_0

    .line 623783
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 623784
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 623785
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 623786
    sget-object v1, LX/3fv;->j:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 623787
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 623788
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 623789
    :cond_1
    if-nez v1, :cond_4

    .line 623790
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 623791
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 623792
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/3fv;->b(LX/0QB;)LX/3fv;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 623793
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 623794
    if-nez v1, :cond_2

    .line 623795
    sget-object v0, LX/3fv;->j:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3fv;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 623796
    :goto_1
    if-eqz v0, :cond_3

    .line 623797
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 623798
    :goto_3
    check-cast v0, LX/3fv;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 623799
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 623800
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 623801
    :catchall_1
    move-exception v0

    .line 623802
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 623803
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 623804
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 623805
    :cond_2
    :try_start_8
    sget-object v0, LX/3fv;->j:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3fv;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private static b(LX/0QB;)LX/3fv;
    .locals 10

    .prologue
    .line 623806
    new-instance v0, LX/3fv;

    invoke-static {p0}, LX/3fw;->b(LX/0QB;)LX/3fw;

    move-result-object v1

    check-cast v1, LX/3fw;

    invoke-static {p0}, LX/3fr;->a(LX/0QB;)LX/3fr;

    move-result-object v2

    check-cast v2, LX/3fr;

    invoke-static {p0}, LX/3fX;->a(LX/0QB;)LX/3fX;

    move-result-object v3

    check-cast v3, LX/3fX;

    invoke-static {p0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v4

    check-cast v4, LX/0Xl;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v5

    check-cast v5, LX/0tX;

    const/16 v6, 0x438

    invoke-static {p0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 v7, 0x416

    invoke-static {p0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x1a18

    invoke-static {p0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {p0}, LX/2RQ;->b(LX/0QB;)LX/2RQ;

    move-result-object v9

    check-cast v9, LX/2RQ;

    invoke-direct/range {v0 .. v9}, LX/3fv;-><init>(LX/3fw;LX/3fr;LX/3fX;LX/0Xl;LX/0tX;LX/0Or;LX/0Ot;LX/0Ot;LX/2RQ;)V

    .line 623807
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/contacts/server/AddContactParams;)Lcom/facebook/contacts/server/AddContactResult;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 623808
    iget-object v0, p0, LX/3fv;->a:LX/3fw;

    .line 623809
    new-instance v2, LX/6M8;

    invoke-direct {v2}, LX/6M8;-><init>()V

    move-object v2, v2

    .line 623810
    iget-object v3, v0, LX/3fw;->d:LX/3fc;

    invoke-virtual {v3, v2}, LX/3fc;->a(LX/0gW;)V

    .line 623811
    iget-object v3, v0, LX/3fw;->d:LX/3fc;

    invoke-virtual {v3, v2}, LX/3fc;->c(LX/0gW;)V

    .line 623812
    const-string v3, "input"

    .line 623813
    new-instance v4, LX/4Dg;

    invoke-direct {v4}, LX/4Dg;-><init>()V

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v5

    .line 623814
    const-string v6, "client_mutation_id"

    invoke-virtual {v4, v6, v5}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 623815
    move-object v5, v4

    .line 623816
    iget-object v4, p1, Lcom/facebook/contacts/server/AddContactParams;->a:Ljava/lang/String;

    if-eqz v4, :cond_9

    .line 623817
    iget-object v4, p1, Lcom/facebook/contacts/server/AddContactParams;->a:Ljava/lang/String;

    .line 623818
    const-string v6, "profile_id"

    invoke-virtual {v5, v6, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 623819
    :cond_0
    :goto_0
    iget-object v4, p1, Lcom/facebook/contacts/server/AddContactParams;->c:Ljava/lang/String;

    if-eqz v4, :cond_1

    .line 623820
    iget-object v4, p1, Lcom/facebook/contacts/server/AddContactParams;->c:Ljava/lang/String;

    .line 623821
    const-string v6, "contact_surface"

    invoke-virtual {v5, v6, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 623822
    :cond_1
    iget-object v4, p1, Lcom/facebook/contacts/server/AddContactParams;->d:Ljava/lang/String;

    if-eqz v4, :cond_2

    .line 623823
    iget-object v4, p1, Lcom/facebook/contacts/server/AddContactParams;->d:Ljava/lang/String;

    .line 623824
    const-string v6, "messenger_contact_creation_source"

    invoke-virtual {v5, v6, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 623825
    :cond_2
    iget-boolean v4, p1, Lcom/facebook/contacts/server/AddContactParams;->e:Z

    if-eqz v4, :cond_3

    .line 623826
    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    .line 623827
    const-string v6, "send_admin_message"

    invoke-virtual {v5, v6, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 623828
    :cond_3
    move-object v4, v5

    .line 623829
    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 623830
    move-object v0, v2

    .line 623831
    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 623832
    iget-object v2, p0, LX/3fv;->e:LX/0tX;

    invoke-virtual {v2, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 623833
    const v2, -0x31971700    # -9.76896E8f

    :try_start_0
    invoke-static {v0, v2}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/executor/GraphQLResult;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 623834
    iget-object v2, p0, LX/3fv;->a:LX/3fw;

    .line 623835
    iget-object v3, v0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v3

    .line 623836
    check-cast v0, Lcom/facebook/contacts/graphql/ContactGraphQLModels$AddContactModel;

    invoke-virtual {v0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$AddContactModel;->a()Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/3fw;->a(Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;)Lcom/facebook/contacts/graphql/Contact;

    move-result-object v3

    .line 623837
    invoke-virtual {v3}, Lcom/facebook/contacts/graphql/Contact;->c()Ljava/lang/String;

    move-result-object v0

    .line 623838
    if-eqz v0, :cond_8

    .line 623839
    iget-object v2, p0, LX/3fv;->b:LX/3fr;

    iget-object v4, p0, LX/3fv;->i:LX/2RQ;

    invoke-virtual {v4, v0}, LX/2RQ;->a(Ljava/lang/String;)LX/2RR;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/3fr;->a(LX/2RR;)LX/6N1;

    move-result-object v4

    const/4 v2, 0x0

    .line 623840
    :try_start_1
    invoke-interface {v4}, LX/6N1;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v4}, LX/6N1;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/graphql/Contact;

    invoke-virtual {v0}, Lcom/facebook/contacts/graphql/Contact;->v()Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    .line 623841
    :goto_1
    if-eqz v4, :cond_4

    invoke-interface {v4}, LX/6N1;->close()V

    .line 623842
    :cond_4
    invoke-virtual {p0, v3}, LX/3fv;->a(Lcom/facebook/contacts/graphql/Contact;)V

    .line 623843
    :goto_2
    new-instance v1, Lcom/facebook/contacts/server/AddContactResult;

    invoke-direct {v1, v3, v0}, Lcom/facebook/contacts/server/AddContactResult;-><init>(Lcom/facebook/contacts/graphql/Contact;Z)V

    return-object v1

    .line 623844
    :catch_0
    move-exception v0

    .line 623845
    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, Ljava/lang/Exception;

    throw v0

    :cond_5
    move v0, v1

    .line 623846
    goto :goto_1

    .line 623847
    :catch_1
    move-exception v0

    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 623848
    :catchall_0
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    :goto_3
    if-eqz v4, :cond_6

    if-eqz v1, :cond_7

    :try_start_3
    invoke-interface {v4}, LX/6N1;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_2

    :cond_6
    :goto_4
    throw v0

    :catch_2
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_4

    :cond_7
    invoke-interface {v4}, LX/6N1;->close()V

    goto :goto_4

    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_3

    :cond_8
    move v0, v1

    goto :goto_2

    .line 623849
    :cond_9
    iget-object v4, p1, Lcom/facebook/contacts/server/AddContactParams;->b:Ljava/lang/String;

    if-eqz v4, :cond_0

    .line 623850
    iget-object v4, v0, LX/3fw;->b:LX/3fx;

    iget-object v6, p1, Lcom/facebook/contacts/server/AddContactParams;->b:Ljava/lang/String;

    invoke-virtual {v4, v6}, LX/3fx;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 623851
    const-string v6, "phone"

    invoke-virtual {v5, v6, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 623852
    iget-object v4, v0, LX/3fw;->c:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 623853
    const-string v6, "country_code"

    invoke-virtual {v5, v6, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 623854
    goto/16 :goto_0
.end method

.method public final a(Lcom/facebook/contacts/graphql/Contact;)V
    .locals 2

    .prologue
    .line 623855
    sget-object v1, LX/6Mt;->a:[I

    iget-object v0, p0, LX/3fv;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Jp;

    invoke-virtual {v0}, LX/2Jp;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 623856
    :goto_0
    iget-object v0, p0, LX/3fv;->c:LX/3fX;

    invoke-virtual {v0, p1}, LX/3fX;->a(Lcom/facebook/contacts/graphql/Contact;)V

    .line 623857
    return-void

    .line 623858
    :pswitch_0
    iget-object v0, p0, LX/3fv;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3fg;

    sget-object v1, LX/0ta;->FROM_SERVER:LX/0ta;

    invoke-virtual {v0, p1, v1}, LX/3fg;->a(Lcom/facebook/contacts/graphql/Contact;LX/0ta;)J

    .line 623859
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.facebook.contacts.ACTION_CONTACT_ADDED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 623860
    iget-object v1, p0, LX/3fv;->d:LX/0Xl;

    invoke-interface {v1, v0}, LX/0Xl;->a(Landroid/content/Intent;)V

    goto :goto_0

    .line 623861
    :pswitch_1
    iget-object v0, p0, LX/3fv;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Nd;

    invoke-virtual {v0, p1}, LX/6Nd;->a(Lcom/facebook/contacts/graphql/Contact;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
