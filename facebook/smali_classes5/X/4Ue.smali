.class public LX/4Ue;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4Ud;


# instance fields
.field private final a:LX/4Ud;

.field private final b:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<[[I>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/4Ud;)V
    .locals 1

    .prologue
    .line 741623
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 741624
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, LX/4Ue;->b:Landroid/util/SparseArray;

    .line 741625
    iput-object p1, p0, LX/4Ue;->a:LX/4Ud;

    .line 741626
    return-void
.end method


# virtual methods
.method public final a(I)[[I
    .locals 2

    .prologue
    .line 741627
    iget-object v0, p0, LX/4Ue;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[I

    .line 741628
    if-eqz v0, :cond_0

    .line 741629
    :goto_0
    return-object v0

    .line 741630
    :cond_0
    iget-object v0, p0, LX/4Ue;->a:LX/4Ud;

    invoke-interface {v0, p1}, LX/4Ud;->a(I)[[I

    move-result-object v0

    .line 741631
    iget-object v1, p0, LX/4Ue;->b:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_0
.end method
