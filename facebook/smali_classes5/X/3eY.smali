.class public LX/3eY;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/3eY;


# instance fields
.field public final a:LX/0W3;


# direct methods
.method public constructor <init>(LX/0W3;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 620312
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 620313
    iput-object p1, p0, LX/3eY;->a:LX/0W3;

    .line 620314
    return-void
.end method

.method public static a(LX/0QB;)LX/3eY;
    .locals 4

    .prologue
    .line 620315
    sget-object v0, LX/3eY;->b:LX/3eY;

    if-nez v0, :cond_1

    .line 620316
    const-class v1, LX/3eY;

    monitor-enter v1

    .line 620317
    :try_start_0
    sget-object v0, LX/3eY;->b:LX/3eY;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 620318
    if-eqz v2, :cond_0

    .line 620319
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 620320
    new-instance p0, LX/3eY;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v3

    check-cast v3, LX/0W3;

    invoke-direct {p0, v3}, LX/3eY;-><init>(LX/0W3;)V

    .line 620321
    move-object v0, p0

    .line 620322
    sput-object v0, LX/3eY;->b:LX/3eY;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 620323
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 620324
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 620325
    :cond_1
    sget-object v0, LX/3eY;->b:LX/3eY;

    return-object v0

    .line 620326
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 620327
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
