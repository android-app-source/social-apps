.class public abstract LX/4th;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "LX/4td;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public a:LX/4td;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field public b:Landroid/os/Bundle;

.field public c:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "LX/3K8;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/3K9;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/3K9",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, LX/3KA;

    invoke-direct {v0, p0}, LX/3KA;-><init>(LX/4th;)V

    iput-object v0, p0, LX/4th;->d:LX/3K9;

    return-void
.end method

.method public static a(LX/4th;I)V
    .locals 1

    :goto_0
    iget-object v0, p0, LX/4th;->c:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/4th;->c:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3K8;

    invoke-interface {v0}, LX/3K8;->a()I

    move-result v0

    if-lt v0, p1, :cond_0

    iget-object v0, p0, LX/4th;->c:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeLast()Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-void
.end method

.method public static a(LX/4th;Landroid/os/Bundle;LX/3K8;)V
    .locals 1

    iget-object v0, p0, LX/4th;->a:LX/4td;

    if-eqz v0, :cond_0

    invoke-interface {p2}, LX/3K8;->b()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, LX/4th;->c:Ljava/util/LinkedList;

    if-nez v0, :cond_1

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/4th;->c:Ljava/util/LinkedList;

    :cond_1
    iget-object v0, p0, LX/4th;->c:Ljava/util/LinkedList;

    invoke-virtual {v0, p2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    if-eqz p1, :cond_2

    iget-object v0, p0, LX/4th;->b:Landroid/os/Bundle;

    if-nez v0, :cond_3

    invoke-virtual {p1}, Landroid/os/Bundle;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    iput-object v0, p0, LX/4th;->b:Landroid/os/Bundle;

    :cond_2
    :goto_1
    iget-object v0, p0, LX/4th;->d:LX/3K9;

    invoke-virtual {p0, v0}, LX/4th;->a(LX/3K9;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, LX/4th;->b:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    goto :goto_1
.end method


# virtual methods
.method public abstract a(LX/3K9;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3K9",
            "<TT;>;)V"
        }
    .end annotation
.end method
