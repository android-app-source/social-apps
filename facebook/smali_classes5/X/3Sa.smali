.class public abstract LX/3Sa;
.super LX/39O;
.source ""


# instance fields
.field private transient a:LX/2uF;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 582857
    invoke-direct {p0}, LX/39O;-><init>()V

    .line 582858
    return-void
.end method


# virtual methods
.method public a(LX/3Sd;I)I
    .locals 6

    .prologue
    .line 582845
    invoke-virtual {p0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 582846
    invoke-interface {v1}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    .line 582847
    iget-object v2, v0, LX/1vs;->a:LX/15i;

    .line 582848
    iget v3, v0, LX/1vs;->b:I

    .line 582849
    iget v4, v0, LX/1vs;->c:I

    .line 582850
    add-int/lit8 v0, p2, 0x1

    .line 582851
    sget-object v5, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v5

    .line 582852
    :try_start_0
    invoke-virtual {p1, p2, v2}, LX/3Sd;->a(ILX/15i;)LX/15i;

    .line 582853
    invoke-virtual {p1, p2, v3}, LX/3Sd;->a(II)I

    .line 582854
    invoke-virtual {p1, p2, v4}, LX/3Sd;->b(II)I

    .line 582855
    monitor-exit v5

    move p2, v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 582856
    :cond_0
    return p2
.end method

.method public final a(LX/15i;II)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 582844
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a(LX/39P;)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 582843
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public synthetic b()LX/2sN;
    .locals 1

    .prologue
    .line 582842
    invoke-virtual {p0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/3Sd;
    .locals 2

    .prologue
    .line 582838
    invoke-virtual {p0}, LX/39O;->c()I

    move-result v0

    .line 582839
    invoke-static {v0}, LX/3Sd;->a(I)LX/3Sd;

    move-result-object v0

    .line 582840
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/3Sa;->a(LX/3Sd;I)I

    .line 582841
    return-object v0
.end method

.method public abstract e()LX/3Sh;
.end method

.method public f()LX/2uF;
    .locals 3

    .prologue
    .line 582827
    iget-object v0, p0, LX/3Sa;->a:LX/2uF;

    .line 582828
    if-nez v0, :cond_0

    .line 582829
    invoke-virtual {p0}, LX/39O;->c()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 582830
    invoke-static {p0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_0
    move-object v0, v0

    .line 582831
    iput-object v0, p0, LX/3Sa;->a:LX/2uF;

    :cond_0
    return-object v0

    .line 582832
    :pswitch_0
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_0

    .line 582833
    :pswitch_1
    invoke-virtual {p0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v0

    invoke-virtual {v0}, LX/3Sh;->b()LX/1vs;

    move-result-object v0

    .line 582834
    iget-object v1, v0, LX/1vs;->a:LX/15i;

    .line 582835
    iget v2, v0, LX/1vs;->b:I

    .line 582836
    iget v0, v0, LX/1vs;->c:I

    .line 582837
    invoke-static {v1, v2, v0}, LX/2uF;->b(LX/15i;II)LX/2uF;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public abstract g()Z
.end method
