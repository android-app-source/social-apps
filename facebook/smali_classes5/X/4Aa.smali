.class public final enum LX/4Aa;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/4Aa;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/4Aa;

.field public static final enum BITMAP_ONLY:LX/4Aa;

.field public static final enum OVERLAY_COLOR:LX/4Aa;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 676565
    new-instance v0, LX/4Aa;

    const-string v1, "OVERLAY_COLOR"

    invoke-direct {v0, v1, v2}, LX/4Aa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4Aa;->OVERLAY_COLOR:LX/4Aa;

    .line 676566
    new-instance v0, LX/4Aa;

    const-string v1, "BITMAP_ONLY"

    invoke-direct {v0, v1, v3}, LX/4Aa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4Aa;->BITMAP_ONLY:LX/4Aa;

    .line 676567
    const/4 v0, 0x2

    new-array v0, v0, [LX/4Aa;

    sget-object v1, LX/4Aa;->OVERLAY_COLOR:LX/4Aa;

    aput-object v1, v0, v2

    sget-object v1, LX/4Aa;->BITMAP_ONLY:LX/4Aa;

    aput-object v1, v0, v3

    sput-object v0, LX/4Aa;->$VALUES:[LX/4Aa;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 676568
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/4Aa;
    .locals 1

    .prologue
    .line 676569
    const-class v0, LX/4Aa;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/4Aa;

    return-object v0
.end method

.method public static values()[LX/4Aa;
    .locals 1

    .prologue
    .line 676570
    sget-object v0, LX/4Aa;->$VALUES:[LX/4Aa;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/4Aa;

    return-object v0
.end method
