.class public LX/45Z;
.super Ljava/io/FilterInputStream;
.source ""


# instance fields
.field private a:J

.field private b:J

.field private c:Z


# direct methods
.method public constructor <init>(Ljava/io/InputStream;JZ)V
    .locals 2

    .prologue
    .line 670395
    invoke-direct {p0, p1}, Ljava/io/FilterInputStream;-><init>(Ljava/io/InputStream;)V

    .line 670396
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/45Z;->b:J

    .line 670397
    iput-wide p2, p0, LX/45Z;->a:J

    .line 670398
    iput-boolean p4, p0, LX/45Z;->c:Z

    .line 670399
    return-void
.end method


# virtual methods
.method public final close()V
    .locals 1

    .prologue
    .line 670428
    iget-boolean v0, p0, LX/45Z;->c:Z

    if-eqz v0, :cond_0

    .line 670429
    invoke-super {p0}, Ljava/io/FilterInputStream;->close()V

    .line 670430
    :cond_0
    return-void
.end method

.method public final declared-synchronized mark(I)V
    .locals 2

    .prologue
    .line 670424
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Ljava/io/FilterInputStream;->mark(I)V

    .line 670425
    iget-wide v0, p0, LX/45Z;->a:J

    iput-wide v0, p0, LX/45Z;->b:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 670426
    monitor-exit p0

    return-void

    .line 670427
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final read()I
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 670431
    new-array v0, v2, [B

    .line 670432
    invoke-virtual {p0, v0, v3, v2}, LX/45Z;->read([BII)I

    move-result v1

    if-ne v1, v2, :cond_0

    .line 670433
    aget-byte v0, v0, v3

    .line 670434
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final read([BII)I
    .locals 6

    .prologue
    .line 670415
    iget-wide v0, p0, LX/45Z;->a:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_2

    .line 670416
    if-nez p3, :cond_1

    const/4 v0, 0x0

    .line 670417
    :cond_0
    :goto_0
    return v0

    .line 670418
    :cond_1
    const/4 v0, -0x1

    goto :goto_0

    .line 670419
    :cond_2
    iget-wide v0, p0, LX/45Z;->a:J

    int-to-long v2, p3

    cmp-long v0, v0, v2

    if-gez v0, :cond_3

    .line 670420
    iget-wide v0, p0, LX/45Z;->a:J

    long-to-int p3, v0

    .line 670421
    :cond_3
    invoke-super {p0, p1, p2, p3}, Ljava/io/FilterInputStream;->read([BII)I

    move-result v0

    .line 670422
    if-ltz v0, :cond_0

    .line 670423
    iget-wide v2, p0, LX/45Z;->a:J

    int-to-long v4, v0

    sub-long/2addr v2, v4

    iput-wide v2, p0, LX/45Z;->a:J

    goto :goto_0
.end method

.method public final declared-synchronized reset()V
    .locals 2

    .prologue
    .line 670410
    monitor-enter p0

    :try_start_0
    invoke-super {p0}, Ljava/io/FilterInputStream;->reset()V

    .line 670411
    iget-wide v0, p0, LX/45Z;->b:J

    iput-wide v0, p0, LX/45Z;->a:J

    .line 670412
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/45Z;->b:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 670413
    monitor-exit p0

    return-void

    .line 670414
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final skip(J)J
    .locals 5

    .prologue
    const-wide/16 v0, 0x0

    .line 670400
    iget-wide v2, p0, LX/45Z;->a:J

    cmp-long v2, v2, v0

    if-gtz v2, :cond_1

    .line 670401
    cmp-long v2, p1, v0

    if-nez v2, :cond_0

    .line 670402
    :goto_0
    return-wide v0

    .line 670403
    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0

    .line 670404
    :cond_1
    iget-wide v2, p0, LX/45Z;->a:J

    cmp-long v2, v2, p1

    if-gez v2, :cond_2

    .line 670405
    iget-wide p1, p0, LX/45Z;->a:J

    .line 670406
    :cond_2
    invoke-super {p0, p1, p2}, Ljava/io/FilterInputStream;->skip(J)J

    move-result-wide v2

    .line 670407
    cmp-long v0, v2, v0

    if-ltz v0, :cond_3

    .line 670408
    iget-wide v0, p0, LX/45Z;->a:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, LX/45Z;->a:J

    :cond_3
    move-wide v0, v2

    .line 670409
    goto :goto_0
.end method
