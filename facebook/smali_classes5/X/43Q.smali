.class public final enum LX/43Q;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/43Q;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/43Q;

.field public static final enum EXIF:LX/43Q;

.field public static final enum JAVA_RESIZER:LX/43Q;

.field public static final enum MEDIASTORE_IMAGE:LX/43Q;

.field public static final enum NATIVE_RESIZER:LX/43Q;

.field public static final enum VIDEO:LX/43Q;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 669041
    new-instance v0, LX/43Q;

    const-string v1, "VIDEO"

    invoke-direct {v0, v1, v2}, LX/43Q;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/43Q;->VIDEO:LX/43Q;

    .line 669042
    new-instance v0, LX/43Q;

    const-string v1, "EXIF"

    invoke-direct {v0, v1, v3}, LX/43Q;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/43Q;->EXIF:LX/43Q;

    .line 669043
    new-instance v0, LX/43Q;

    const-string v1, "NATIVE_RESIZER"

    invoke-direct {v0, v1, v4}, LX/43Q;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/43Q;->NATIVE_RESIZER:LX/43Q;

    .line 669044
    new-instance v0, LX/43Q;

    const-string v1, "JAVA_RESIZER"

    invoke-direct {v0, v1, v5}, LX/43Q;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/43Q;->JAVA_RESIZER:LX/43Q;

    .line 669045
    new-instance v0, LX/43Q;

    const-string v1, "MEDIASTORE_IMAGE"

    invoke-direct {v0, v1, v6}, LX/43Q;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/43Q;->MEDIASTORE_IMAGE:LX/43Q;

    .line 669046
    const/4 v0, 0x5

    new-array v0, v0, [LX/43Q;

    sget-object v1, LX/43Q;->VIDEO:LX/43Q;

    aput-object v1, v0, v2

    sget-object v1, LX/43Q;->EXIF:LX/43Q;

    aput-object v1, v0, v3

    sget-object v1, LX/43Q;->NATIVE_RESIZER:LX/43Q;

    aput-object v1, v0, v4

    sget-object v1, LX/43Q;->JAVA_RESIZER:LX/43Q;

    aput-object v1, v0, v5

    sget-object v1, LX/43Q;->MEDIASTORE_IMAGE:LX/43Q;

    aput-object v1, v0, v6

    sput-object v0, LX/43Q;->$VALUES:[LX/43Q;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 669047
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/43Q;
    .locals 1

    .prologue
    .line 669048
    const-class v0, LX/43Q;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/43Q;

    return-object v0
.end method

.method public static values()[LX/43Q;
    .locals 1

    .prologue
    .line 669049
    sget-object v0, LX/43Q;->$VALUES:[LX/43Q;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/43Q;

    return-object v0
.end method
