.class public final LX/3xF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Landroid/support/v7/widget/SearchView;


# direct methods
.method public constructor <init>(Landroid/support/v7/widget/SearchView;)V
    .locals 0

    .prologue
    .line 656773
    iput-object p1, p0, LX/3xF;->a:Landroid/support/v7/widget/SearchView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x2201a873

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 656774
    iget-object v1, p0, LX/3xF;->a:Landroid/support/v7/widget/SearchView;

    iget-object v1, v1, Landroid/support/v7/widget/SearchView;->mSearchButton:Landroid/widget/ImageView;

    if-ne p1, v1, :cond_1

    .line 656775
    iget-object v1, p0, LX/3xF;->a:Landroid/support/v7/widget/SearchView;

    invoke-static {v1}, Landroid/support/v7/widget/SearchView;->onSearchClicked(Landroid/support/v7/widget/SearchView;)V

    .line 656776
    :cond_0
    :goto_0
    const v1, 0x7a3af03c

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 656777
    :cond_1
    iget-object v1, p0, LX/3xF;->a:Landroid/support/v7/widget/SearchView;

    iget-object v1, v1, Landroid/support/v7/widget/SearchView;->mCloseButton:Landroid/widget/ImageView;

    if-ne p1, v1, :cond_2

    .line 656778
    iget-object v1, p0, LX/3xF;->a:Landroid/support/v7/widget/SearchView;

    invoke-static {v1}, Landroid/support/v7/widget/SearchView;->onCloseClicked(Landroid/support/v7/widget/SearchView;)V

    goto :goto_0

    .line 656779
    :cond_2
    iget-object v1, p0, LX/3xF;->a:Landroid/support/v7/widget/SearchView;

    iget-object v1, v1, Landroid/support/v7/widget/SearchView;->mGoButton:Landroid/widget/ImageView;

    if-ne p1, v1, :cond_3

    .line 656780
    iget-object v1, p0, LX/3xF;->a:Landroid/support/v7/widget/SearchView;

    invoke-static {v1}, Landroid/support/v7/widget/SearchView;->onSubmitQuery(Landroid/support/v7/widget/SearchView;)V

    goto :goto_0

    .line 656781
    :cond_3
    iget-object v1, p0, LX/3xF;->a:Landroid/support/v7/widget/SearchView;

    iget-object v1, v1, Landroid/support/v7/widget/SearchView;->mVoiceButton:Landroid/widget/ImageView;

    if-ne p1, v1, :cond_4

    .line 656782
    iget-object v1, p0, LX/3xF;->a:Landroid/support/v7/widget/SearchView;

    invoke-static {v1}, Landroid/support/v7/widget/SearchView;->onVoiceClicked(Landroid/support/v7/widget/SearchView;)V

    goto :goto_0

    .line 656783
    :cond_4
    iget-object v1, p0, LX/3xF;->a:Landroid/support/v7/widget/SearchView;

    iget-object v1, v1, Landroid/support/v7/widget/SearchView;->mSearchSrcTextView:Landroid/support/v7/widget/SearchView$SearchAutoComplete;

    if-ne p1, v1, :cond_0

    .line 656784
    iget-object v1, p0, LX/3xF;->a:Landroid/support/v7/widget/SearchView;

    invoke-static {v1}, Landroid/support/v7/widget/SearchView;->forceSuggestionQuery(Landroid/support/v7/widget/SearchView;)V

    goto :goto_0
.end method
