.class public final LX/3ra;
.super LX/3rZ;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 643273
    invoke-direct {p0}, LX/3rZ;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)I
    .locals 1

    .prologue
    .line 643271
    invoke-static {p1}, Landroid/view/KeyEvent;->normalizeMetaState(I)I

    move-result v0

    move v0, v0

    .line 643272
    return v0
.end method

.method public final a(II)Z
    .locals 1

    .prologue
    .line 643267
    invoke-static {p1, p2}, Landroid/view/KeyEvent;->metaStateHasModifiers(II)Z

    move-result v0

    move v0, v0

    .line 643268
    return v0
.end method

.method public final b(I)Z
    .locals 1

    .prologue
    .line 643269
    invoke-static {p1}, Landroid/view/KeyEvent;->metaStateHasNoModifiers(I)Z

    move-result v0

    move v0, v0

    .line 643270
    return v0
.end method
