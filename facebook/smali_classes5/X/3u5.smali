.class public final LX/3u5;
.super LX/3u4;
.source ""


# instance fields
.field public final synthetic a:LX/3u6;


# direct methods
.method public constructor <init>(LX/3u6;Landroid/view/Window$Callback;)V
    .locals 0

    .prologue
    .line 647428
    iput-object p1, p0, LX/3u5;->a:LX/3u6;

    .line 647429
    invoke-direct {p0, p2}, LX/3u4;-><init>(Landroid/view/Window$Callback;)V

    .line 647430
    return-void
.end method


# virtual methods
.method public final dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 647425
    iget-object v0, p0, LX/3u5;->a:LX/3u6;

    invoke-virtual {v0, p1}, LX/3u6;->a(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 647426
    const/4 v0, 0x1

    .line 647427
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, LX/3u4;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final dispatchKeyShortcutEvent(Landroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    .line 647399
    iget-object v0, p0, LX/3u5;->a:LX/3u6;

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    invoke-virtual {v0, v1, p1}, LX/3u6;->a(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 647400
    const/4 v0, 0x1

    .line 647401
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, LX/3u4;->dispatchKeyShortcutEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final onContentChanged()V
    .locals 0

    .prologue
    .line 647424
    return-void
.end method

.method public final onCreatePanelMenu(ILandroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 647421
    if-nez p1, :cond_0

    instance-of v0, p2, LX/3v0;

    if-nez v0, :cond_0

    .line 647422
    const/4 v0, 0x0

    .line 647423
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, LX/3u4;->onCreatePanelMenu(ILandroid/view/Menu;)Z

    move-result v0

    goto :goto_0
.end method

.method public final onMenuOpened(ILandroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 647418
    iget-object v0, p0, LX/3u5;->a:LX/3u6;

    invoke-virtual {v0, p1}, LX/3u6;->d(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 647419
    const/4 v0, 0x1

    .line 647420
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, LX/3u4;->onMenuOpened(ILandroid/view/Menu;)Z

    move-result v0

    goto :goto_0
.end method

.method public final onPanelClosed(ILandroid/view/Menu;)V
    .locals 1

    .prologue
    .line 647415
    iget-object v0, p0, LX/3u5;->a:LX/3u6;

    invoke-virtual {v0, p1}, LX/3u6;->c(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 647416
    :goto_0
    return-void

    .line 647417
    :cond_0
    invoke-super {p0, p1, p2}, LX/3u4;->onPanelClosed(ILandroid/view/Menu;)V

    goto :goto_0
.end method

.method public final onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 647402
    if-nez p1, :cond_1

    instance-of v1, p3, LX/3v0;

    if-nez v1, :cond_1

    .line 647403
    :cond_0
    :goto_0
    return v0

    .line 647404
    :cond_1
    if-nez p1, :cond_4

    const/4 v1, 0x1

    .line 647405
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-ge v2, v3, :cond_5

    iget-object v2, p0, LX/3u5;->a:LX/3u6;

    iget-object v2, v2, LX/3u6;->c:Landroid/view/Window$Callback;

    instance-of v2, v2, Landroid/app/Activity;

    if-eqz v2, :cond_5

    .line 647406
    :cond_2
    :goto_1
    move v1, v1

    .line 647407
    if-eqz v1, :cond_4

    .line 647408
    iget-object v1, p0, LX/3u5;->a:LX/3u6;

    iget-object v1, v1, LX/3u6;->c:Landroid/view/Window$Callback;

    instance-of v1, v1, Landroid/app/Activity;

    if-eqz v1, :cond_3

    .line 647409
    iget-object v0, p0, LX/3u5;->a:LX/3u6;

    iget-object v0, v0, LX/3u6;->c:Landroid/view/Window$Callback;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0, p3}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    goto :goto_0

    .line 647410
    :cond_3
    iget-object v1, p0, LX/3u5;->a:LX/3u6;

    iget-object v1, v1, LX/3u6;->c:Landroid/view/Window$Callback;

    instance-of v1, v1, Landroid/app/Dialog;

    if-eqz v1, :cond_0

    .line 647411
    iget-object v0, p0, LX/3u5;->a:LX/3u6;

    iget-object v0, v0, LX/3u6;->c:Landroid/view/Window$Callback;

    check-cast v0, Landroid/app/Dialog;

    invoke-virtual {v0, p3}, Landroid/app/Dialog;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    goto :goto_0

    .line 647412
    :cond_4
    invoke-super {p0, p1, p2, p3}, LX/3u4;->onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z

    move-result v0

    goto :goto_0

    .line 647413
    :cond_5
    iget-object v2, p0, LX/3u5;->a:LX/3u6;

    iget-object v2, v2, LX/3u6;->c:Landroid/view/Window$Callback;

    instance-of v2, v2, Landroid/app/Dialog;

    if-nez v2, :cond_2

    .line 647414
    const/4 v1, 0x0

    goto :goto_1
.end method
