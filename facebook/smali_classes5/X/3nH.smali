.class public LX/3nH;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 638112
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 638113
    return-void
.end method

.method public static a(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)LX/1qM;
    .locals 2
    .param p0    # LX/0Ot;
        .annotation runtime Lcom/facebook/feed/annotations/ForNewsfeed;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/feed/annotations/ForNewsfeed;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Amh;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Amb;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/api/ufiservices/UFIServicesHandler;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/composer/publish/ComposerPublishServiceHandler;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2c3;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/26s;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/3nP;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/3nO;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/3nM;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/3nL;",
            ">;)",
            "LX/1qM;"
        }
    .end annotation

    .prologue
    .line 638114
    new-instance v0, LX/3nK;

    invoke-direct {v0, p0, p1}, LX/3nK;-><init>(LX/0Ot;LX/0Ot;)V

    .line 638115
    new-instance v1, LX/3nK;

    invoke-direct {v1, p2, v0}, LX/3nK;-><init>(LX/0Ot;LX/1qM;)V

    .line 638116
    new-instance v0, LX/3nK;

    invoke-direct {v0, p3, v1}, LX/3nK;-><init>(LX/0Ot;LX/1qM;)V

    .line 638117
    new-instance v1, LX/3nK;

    invoke-direct {v1, p4, v0}, LX/3nK;-><init>(LX/0Ot;LX/1qM;)V

    .line 638118
    new-instance v0, LX/3nK;

    invoke-direct {v0, p5, v1}, LX/3nK;-><init>(LX/0Ot;LX/1qM;)V

    .line 638119
    new-instance v1, LX/3nK;

    invoke-direct {v1, p6, v0}, LX/3nK;-><init>(LX/0Ot;LX/1qM;)V

    .line 638120
    new-instance v0, LX/3nK;

    invoke-direct {v0, p7, v1}, LX/3nK;-><init>(LX/0Ot;LX/1qM;)V

    .line 638121
    new-instance v1, LX/3nK;

    invoke-direct {v1, p8, v0}, LX/3nK;-><init>(LX/0Ot;LX/1qM;)V

    .line 638122
    new-instance v0, LX/3nK;

    invoke-direct {v0, p9, v1}, LX/3nK;-><init>(LX/0Ot;LX/1qM;)V

    return-object v0
.end method

.method public static a(LX/0Ot;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;LX/0Or;)LX/Amh;
    .locals 6
    .annotation runtime Lcom/facebook/feed/annotations/ForNewsfeed;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/BNw;",
            ">;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;",
            "LX/0Or",
            "<",
            "LX/826;",
            ">;)",
            "LX/Amh;"
        }
    .end annotation

    .prologue
    .line 638123
    new-instance v0, LX/Amh;

    const-string v5, "update_timeline_app_collection_in_newsfeed"

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, LX/Amh;-><init>(LX/0Ot;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;LX/0Or;Ljava/lang/String;)V

    return-object v0
.end method

.method public static d()LX/3y7;
    .locals 1
    .annotation runtime Lcom/facebook/feed/annotations/ForNewsfeed;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 638124
    new-instance v0, LX/AlB;

    invoke-direct {v0}, LX/AlB;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 638125
    return-void
.end method
