.class public final LX/4mO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# instance fields
.field public final synthetic a:LX/4mQ;


# direct methods
.method public constructor <init>(LX/4mQ;)V
    .locals 0

    .prologue
    .line 805673
    iput-object p1, p0, LX/4mO;->a:LX/4mQ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationCancel(Landroid/animation/Animator;)V
    .locals 5

    .prologue
    .line 805674
    iget-object v0, p0, LX/4mO;->a:LX/4mQ;

    invoke-virtual {v0}, LX/4mQ;->getListeners()Ljava/util/ArrayList;

    move-result-object v0

    .line 805675
    if-nez v0, :cond_1

    .line 805676
    :cond_0
    return-void

    .line 805677
    :cond_1
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    .line 805678
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/Animator$AnimatorListener;

    .line 805679
    iget-object v4, p0, LX/4mO;->a:LX/4mQ;

    invoke-interface {v0, v4}, Landroid/animation/Animator$AnimatorListener;->onAnimationCancel(Landroid/animation/Animator;)V

    .line 805680
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public final onAnimationEnd(Landroid/animation/Animator;)V
    .locals 5

    .prologue
    .line 805681
    iget-object v0, p0, LX/4mO;->a:LX/4mQ;

    invoke-virtual {v0}, LX/4mQ;->getListeners()Ljava/util/ArrayList;

    move-result-object v0

    .line 805682
    if-nez v0, :cond_1

    .line 805683
    :cond_0
    return-void

    .line 805684
    :cond_1
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    .line 805685
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/Animator$AnimatorListener;

    .line 805686
    iget-object v4, p0, LX/4mO;->a:LX/4mQ;

    invoke-interface {v0, v4}, Landroid/animation/Animator$AnimatorListener;->onAnimationEnd(Landroid/animation/Animator;)V

    .line 805687
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public final onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 5

    .prologue
    .line 805688
    iget-object v0, p0, LX/4mO;->a:LX/4mQ;

    invoke-virtual {v0}, LX/4mQ;->getListeners()Ljava/util/ArrayList;

    move-result-object v0

    .line 805689
    if-nez v0, :cond_1

    .line 805690
    :cond_0
    return-void

    .line 805691
    :cond_1
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    .line 805692
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/Animator$AnimatorListener;

    .line 805693
    iget-object v4, p0, LX/4mO;->a:LX/4mQ;

    invoke-interface {v0, v4}, Landroid/animation/Animator$AnimatorListener;->onAnimationRepeat(Landroid/animation/Animator;)V

    .line 805694
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public final onAnimationStart(Landroid/animation/Animator;)V
    .locals 5

    .prologue
    .line 805695
    iget-object v0, p0, LX/4mO;->a:LX/4mQ;

    invoke-virtual {v0}, LX/4mQ;->getListeners()Ljava/util/ArrayList;

    move-result-object v0

    .line 805696
    if-nez v0, :cond_1

    .line 805697
    :cond_0
    return-void

    .line 805698
    :cond_1
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    .line 805699
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/Animator$AnimatorListener;

    .line 805700
    iget-object v4, p0, LX/4mO;->a:LX/4mQ;

    invoke-interface {v0, v4}, Landroid/animation/Animator$AnimatorListener;->onAnimationStart(Landroid/animation/Animator;)V

    .line 805701
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method
