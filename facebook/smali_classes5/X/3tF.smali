.class public final LX/3tF;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:I

.field public b:I

.field public c:F

.field public d:F

.field public e:J

.field public f:J

.field public g:I

.field public h:I

.field public i:J

.field public j:F

.field public k:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 644485
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 644486
    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, LX/3tF;->e:J

    .line 644487
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/3tF;->i:J

    .line 644488
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/3tF;->f:J

    .line 644489
    iput v2, p0, LX/3tF;->g:I

    .line 644490
    iput v2, p0, LX/3tF;->h:I

    .line 644491
    return-void
.end method

.method public static a(LX/3tF;J)F
    .locals 7

    .prologue
    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v0, 0x0

    .line 644492
    iget-wide v2, p0, LX/3tF;->e:J

    cmp-long v1, p1, v2

    if-gez v1, :cond_0

    .line 644493
    :goto_0
    return v0

    .line 644494
    :cond_0
    iget-wide v2, p0, LX/3tF;->i:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-ltz v1, :cond_1

    iget-wide v2, p0, LX/3tF;->i:J

    cmp-long v1, p1, v2

    if-gez v1, :cond_2

    .line 644495
    :cond_1
    iget-wide v2, p0, LX/3tF;->e:J

    sub-long v2, p1, v2

    .line 644496
    const/high16 v1, 0x3f000000    # 0.5f

    long-to-float v2, v2

    iget v3, p0, LX/3tF;->a:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    invoke-static {v2, v0, v6}, LX/3tG;->b(FFF)F

    move-result v0

    mul-float/2addr v0, v1

    goto :goto_0

    .line 644497
    :cond_2
    iget-wide v2, p0, LX/3tF;->i:J

    sub-long v2, p1, v2

    .line 644498
    iget v1, p0, LX/3tF;->j:F

    sub-float v1, v6, v1

    iget v4, p0, LX/3tF;->j:F

    long-to-float v2, v2

    iget v3, p0, LX/3tF;->k:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    invoke-static {v2, v0, v6}, LX/3tG;->b(FFF)F

    move-result v0

    mul-float/2addr v0, v4

    add-float/2addr v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final d()V
    .locals 6

    .prologue
    .line 644499
    iget-wide v0, p0, LX/3tF;->f:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 644500
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Cannot compute scroll delta before calling start()"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 644501
    :cond_0
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v0

    .line 644502
    invoke-static {p0, v0, v1}, LX/3tF;->a(LX/3tF;J)F

    move-result v2

    .line 644503
    const/high16 v3, -0x3f800000    # -4.0f

    mul-float/2addr v3, v2

    mul-float/2addr v3, v2

    const/high16 v4, 0x40800000    # 4.0f

    mul-float/2addr v4, v2

    add-float/2addr v3, v4

    move v2, v3

    .line 644504
    iget-wide v4, p0, LX/3tF;->f:J

    sub-long v4, v0, v4

    .line 644505
    iput-wide v0, p0, LX/3tF;->f:J

    .line 644506
    long-to-float v0, v4

    mul-float/2addr v0, v2

    iget v1, p0, LX/3tF;->c:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, LX/3tF;->g:I

    .line 644507
    long-to-float v0, v4

    mul-float/2addr v0, v2

    iget v1, p0, LX/3tF;->d:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, LX/3tF;->h:I

    .line 644508
    return-void
.end method
