.class public final LX/4xz;
.super LX/1r7;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1r7",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1Ei;


# direct methods
.method public constructor <init>(LX/1Ei;)V
    .locals 0

    .prologue
    .line 821510
    iput-object p1, p0, LX/4xz;->a:LX/1Ei;

    .line 821511
    invoke-direct {p0, p1}, LX/1r7;-><init>(Ljava/util/Map;)V

    .line 821512
    return-void
.end method


# virtual methods
.method public final iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 821513
    new-instance v0, LX/4xy;

    invoke-direct {v0, p0}, LX/4xy;-><init>(LX/4xz;)V

    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 821514
    iget-object v0, p0, LX/4xz;->a:LX/1Ei;

    invoke-static {p1}, LX/0PC;->a(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v0, p1, v1}, LX/1Ei;->a$redex0(LX/1Ei;Ljava/lang/Object;I)LX/1Ek;

    move-result-object v0

    .line 821515
    if-nez v0, :cond_0

    .line 821516
    const/4 v0, 0x0

    .line 821517
    :goto_0
    return v0

    .line 821518
    :cond_0
    iget-object v1, p0, LX/4xz;->a:LX/1Ei;

    invoke-static {v1, v0}, LX/1Ei;->a$redex0(LX/1Ei;LX/1Ek;)V

    .line 821519
    iput-object v2, v0, LX/1Ek;->prevInKeyInsertionOrder:LX/1Ek;

    .line 821520
    iput-object v2, v0, LX/1Ek;->nextInKeyInsertionOrder:LX/1Ek;

    .line 821521
    const/4 v0, 0x1

    goto :goto_0
.end method
