.class public LX/4S2;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 711250
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 27

    .prologue
    .line 711251
    const/16 v21, 0x0

    .line 711252
    const/16 v20, 0x0

    .line 711253
    const/16 v19, 0x0

    .line 711254
    const/16 v18, 0x0

    .line 711255
    const/4 v9, 0x0

    .line 711256
    const-wide/16 v16, 0x0

    .line 711257
    const-wide/16 v14, 0x0

    .line 711258
    const-wide/16 v12, 0x0

    .line 711259
    const-wide/16 v10, 0x0

    .line 711260
    const/4 v8, 0x0

    .line 711261
    const/4 v7, 0x0

    .line 711262
    const/4 v6, 0x0

    .line 711263
    const/4 v5, 0x0

    .line 711264
    const/4 v4, 0x0

    .line 711265
    const/4 v3, 0x0

    .line 711266
    const/4 v2, 0x0

    .line 711267
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v22

    sget-object v23, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    if-eq v0, v1, :cond_12

    .line 711268
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 711269
    const/4 v2, 0x0

    .line 711270
    :goto_0
    return v2

    .line 711271
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v23, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v23

    if-eq v2, v0, :cond_a

    .line 711272
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 711273
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 711274
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v23

    sget-object v24, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    if-eq v0, v1, :cond_0

    if-eqz v2, :cond_0

    .line 711275
    const-string v23, "alternate_text"

    move-object/from16 v0, v23

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_1

    .line 711276
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v22, v2

    goto :goto_1

    .line 711277
    :cond_1
    const-string v23, "count"

    move-object/from16 v0, v23

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_2

    .line 711278
    const/4 v2, 0x1

    .line 711279
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v11

    move/from16 v21, v11

    move v11, v2

    goto :goto_1

    .line 711280
    :cond_2
    const-string v23, "duration"

    move-object/from16 v0, v23

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_3

    .line 711281
    const/4 v2, 0x1

    .line 711282
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v7

    move/from16 v20, v7

    move v7, v2

    goto :goto_1

    .line 711283
    :cond_3
    const-string v23, "subtext"

    move-object/from16 v0, v23

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_4

    .line 711284
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v19, v2

    goto :goto_1

    .line 711285
    :cond_4
    const-string v23, "threshold"

    move-object/from16 v0, v23

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_5

    .line 711286
    const/4 v2, 0x1

    .line 711287
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move/from16 v18, v6

    move v6, v2

    goto :goto_1

    .line 711288
    :cond_5
    const-string v23, "velocity"

    move-object/from16 v0, v23

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_6

    .line 711289
    const/4 v2, 0x1

    .line 711290
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v4

    move v3, v2

    goto/16 :goto_1

    .line 711291
    :cond_6
    const-string v23, "percent_counter_height"

    move-object/from16 v0, v23

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_7

    .line 711292
    const/4 v2, 0x1

    .line 711293
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v16

    move v10, v2

    goto/16 :goto_1

    .line 711294
    :cond_7
    const-string v23, "percent_height"

    move-object/from16 v0, v23

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_8

    .line 711295
    const/4 v2, 0x1

    .line 711296
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v14

    move v9, v2

    goto/16 :goto_1

    .line 711297
    :cond_8
    const-string v23, "percent_top"

    move-object/from16 v0, v23

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 711298
    const/4 v2, 0x1

    .line 711299
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v12

    move v8, v2

    goto/16 :goto_1

    .line 711300
    :cond_9
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 711301
    :cond_a
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 711302
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 711303
    if-eqz v11, :cond_b

    .line 711304
    const/4 v2, 0x1

    const/4 v11, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1, v11}, LX/186;->a(III)V

    .line 711305
    :cond_b
    if-eqz v7, :cond_c

    .line 711306
    const/4 v2, 0x2

    const/4 v7, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1, v7}, LX/186;->a(III)V

    .line 711307
    :cond_c
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 711308
    if-eqz v6, :cond_d

    .line 711309
    const/4 v2, 0x4

    const/4 v6, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1, v6}, LX/186;->a(III)V

    .line 711310
    :cond_d
    if-eqz v3, :cond_e

    .line 711311
    const/4 v3, 0x5

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 711312
    :cond_e
    if-eqz v10, :cond_f

    .line 711313
    const/4 v3, 0x6

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v16

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 711314
    :cond_f
    if-eqz v9, :cond_10

    .line 711315
    const/4 v3, 0x7

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide v4, v14

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 711316
    :cond_10
    if-eqz v8, :cond_11

    .line 711317
    const/16 v3, 0x8

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide v4, v12

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 711318
    :cond_11
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_12
    move/from16 v22, v21

    move/from16 v21, v20

    move/from16 v20, v19

    move/from16 v19, v18

    move/from16 v18, v9

    move v9, v3

    move v3, v5

    move-wide/from16 v25, v14

    move-wide v14, v12

    move-wide v12, v10

    move v11, v8

    move v10, v4

    move-wide/from16 v4, v16

    move v8, v2

    move-wide/from16 v16, v25

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const-wide/16 v4, 0x0

    .line 711319
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 711320
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 711321
    if-eqz v0, :cond_0

    .line 711322
    const-string v1, "alternate_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 711323
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 711324
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 711325
    if-eqz v0, :cond_1

    .line 711326
    const-string v1, "count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 711327
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 711328
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 711329
    if-eqz v0, :cond_2

    .line 711330
    const-string v1, "duration"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 711331
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 711332
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 711333
    if-eqz v0, :cond_3

    .line 711334
    const-string v1, "subtext"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 711335
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 711336
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 711337
    if-eqz v0, :cond_4

    .line 711338
    const-string v1, "threshold"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 711339
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 711340
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 711341
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_5

    .line 711342
    const-string v2, "velocity"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 711343
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 711344
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 711345
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_6

    .line 711346
    const-string v2, "percent_counter_height"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 711347
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 711348
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 711349
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_7

    .line 711350
    const-string v2, "percent_height"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 711351
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 711352
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 711353
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_8

    .line 711354
    const-string v2, "percent_top"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 711355
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 711356
    :cond_8
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 711357
    return-void
.end method
