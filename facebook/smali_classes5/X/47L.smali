.class public final LX/47L;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/398;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/398;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method private constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 672228
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 672229
    iput-object p1, p0, LX/47L;->a:LX/0QB;

    .line 672230
    return-void
.end method

.method public static a(LX/0QB;)LX/0Or;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QB;",
            ")",
            "LX/0Or",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/398;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 672231
    new-instance v0, LX/47L;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1}, LX/47L;-><init>(LX/0QB;)V

    return-object v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 672232
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/47L;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 672233
    packed-switch p2, :pswitch_data_0

    .line 672234
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 672235
    :pswitch_0
    invoke-static {p1}, LX/FQd;->a(LX/0QB;)LX/FQd;

    move-result-object v0

    return-object v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 672236
    const/4 v0, 0x1

    return v0
.end method
