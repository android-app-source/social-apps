.class public final LX/4qE;
.super LX/32s;
.source ""


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final _objectIdReader:LX/4qD;


# direct methods
.method public constructor <init>(LX/4qD;Z)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 813218
    iget-object v1, p1, LX/4qD;->propertyName:Ljava/lang/String;

    iget-object v2, p1, LX/4qD;->idType:LX/0lJ;

    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    move v6, p2

    invoke-direct/range {v0 .. v6}, LX/32s;-><init>(Ljava/lang/String;LX/0lJ;LX/2Vb;LX/4qw;LX/0lQ;Z)V

    .line 813219
    iput-object p1, p0, LX/4qE;->_objectIdReader:LX/4qD;

    .line 813220
    iget-object v0, p1, LX/4qD;->deserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    iput-object v0, p0, LX/4qE;->_valueDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    .line 813221
    return-void
.end method

.method private constructor <init>(LX/4qE;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4qE;",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 813215
    invoke-direct {p0, p1, p2}, LX/32s;-><init>(LX/32s;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 813216
    iget-object v0, p1, LX/4qE;->_objectIdReader:LX/4qD;

    iput-object v0, p0, LX/4qE;->_objectIdReader:LX/4qD;

    .line 813217
    return-void
.end method

.method private constructor <init>(LX/4qE;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 813192
    invoke-direct {p0, p1, p2}, LX/32s;-><init>(LX/32s;Ljava/lang/String;)V

    .line 813193
    iget-object v0, p1, LX/4qE;->_objectIdReader:LX/4qD;

    iput-object v0, p0, LX/4qE;->_objectIdReader:LX/4qD;

    .line 813194
    return-void
.end method

.method private a(Lcom/fasterxml/jackson/databind/JsonDeserializer;)LX/4qE;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<*>;)",
            "LX/4qE;"
        }
    .end annotation

    .prologue
    .line 813214
    new-instance v0, LX/4qE;

    invoke-direct {v0, p0, p1}, LX/4qE;-><init>(LX/4qE;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    return-object v0
.end method

.method private c(Ljava/lang/String;)LX/4qE;
    .locals 1

    .prologue
    .line 813213
    new-instance v0, LX/4qE;

    invoke-direct {v0, p0, p1}, LX/4qE;-><init>(LX/4qE;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/String;)LX/32s;
    .locals 1

    .prologue
    .line 813212
    invoke-direct {p0, p1}, LX/4qE;->c(Ljava/lang/String;)LX/4qE;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15w;LX/0n3;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 813210
    invoke-virtual {p0, p1, p2, p3}, LX/32s;->b(LX/15w;LX/0n3;Ljava/lang/Object;)Ljava/lang/Object;

    .line 813211
    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 813208
    invoke-virtual {p0, p1, p2}, LX/32s;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 813209
    return-void
.end method

.method public final b()LX/2An;
    .locals 1

    .prologue
    .line 813207
    const/4 v0, 0x0

    return-object v0
.end method

.method public final synthetic b(Lcom/fasterxml/jackson/databind/JsonDeserializer;)LX/32s;
    .locals 1

    .prologue
    .line 813206
    invoke-direct {p0, p1}, LX/4qE;->a(Lcom/fasterxml/jackson/databind/JsonDeserializer;)LX/4qE;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15w;LX/0n3;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 813199
    iget-object v0, p0, LX/32s;->_valueDeserializer:Lcom/fasterxml/jackson/databind/JsonDeserializer;

    invoke-virtual {v0, p1, p2}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    .line 813200
    iget-object v1, p0, LX/4qE;->_objectIdReader:LX/4qD;

    iget-object v1, v1, LX/4qD;->generator:LX/4pS;

    invoke-virtual {p2, v0, v1}, LX/0n3;->a(Ljava/lang/Object;LX/4pS;)LX/4qM;

    move-result-object v1

    .line 813201
    invoke-virtual {v1, p3}, LX/4qM;->a(Ljava/lang/Object;)V

    .line 813202
    iget-object v1, p0, LX/4qE;->_objectIdReader:LX/4qD;

    iget-object v1, v1, LX/4qD;->idProperty:LX/32s;

    .line 813203
    if-eqz v1, :cond_0

    .line 813204
    invoke-virtual {v1, p3, v0}, LX/32s;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p3

    .line 813205
    :cond_0
    return-object p3
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 813195
    iget-object v0, p0, LX/4qE;->_objectIdReader:LX/4qD;

    iget-object v0, v0, LX/4qD;->idProperty:LX/32s;

    .line 813196
    if-nez v0, :cond_0

    .line 813197
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Should not call set() on ObjectIdProperty that has no SettableBeanProperty"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 813198
    :cond_0
    invoke-virtual {v0, p1, p2}, LX/32s;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
