.class public final LX/4f9;
.super LX/1eP;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1eP",
        "<",
        "LX/1FJ",
        "<",
        "LX/1ln;",
        ">;",
        "LX/1FJ",
        "<",
        "LX/1ln;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/1bh;

.field private final b:Z

.field private final c:LX/1Fh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Fh",
            "<",
            "LX/1bh;",
            "LX/1ln;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1cd;LX/1bh;ZLX/1Fh;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/imagepipeline/producers/Consumer",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;",
            "LX/1bh;",
            "Z",
            "LX/1Fh",
            "<",
            "LX/1bh;",
            "LX/1ln;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 797999
    invoke-direct {p0, p1}, LX/1eP;-><init>(LX/1cd;)V

    .line 798000
    iput-object p2, p0, LX/4f9;->a:LX/1bh;

    .line 798001
    iput-boolean p3, p0, LX/4f9;->b:Z

    .line 798002
    iput-object p4, p0, LX/4f9;->c:LX/1Fh;

    .line 798003
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Z)V
    .locals 3

    .prologue
    .line 798004
    check-cast p1, LX/1FJ;

    .line 798005
    if-nez p1, :cond_1

    .line 798006
    if-eqz p2, :cond_0

    .line 798007
    iget-object v0, p0, LX/1eP;->a:LX/1cd;

    move-object v0, v0

    .line 798008
    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LX/1cd;->b(Ljava/lang/Object;Z)V

    .line 798009
    :cond_0
    :goto_0
    return-void

    .line 798010
    :cond_1
    if-nez p2, :cond_2

    iget-boolean v0, p0, LX/4f9;->b:Z

    if-eqz v0, :cond_0

    .line 798011
    :cond_2
    iget-object v0, p0, LX/4f9;->c:LX/1Fh;

    iget-object v1, p0, LX/4f9;->a:LX/1bh;

    invoke-interface {v0, v1, p1}, LX/1Fh;->a(Ljava/lang/Object;LX/1FJ;)LX/1FJ;

    move-result-object v1

    .line 798012
    :try_start_0
    iget-object v0, p0, LX/1eP;->a:LX/1cd;

    move-object v0, v0

    .line 798013
    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v0, v2}, LX/1cd;->b(F)V

    .line 798014
    iget-object v0, p0, LX/1eP;->a:LX/1cd;

    move-object v0, v0

    .line 798015
    if-eqz v1, :cond_3

    move-object p1, v1

    :cond_3
    invoke-virtual {v0, p1, p2}, LX/1cd;->b(Ljava/lang/Object;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 798016
    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    throw v0
.end method
