.class public LX/44B;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 669530
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0V8;)Ljava/util/Map;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0V8;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    const-wide/16 v6, 0x0

    .line 669497
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 669498
    if-eqz p0, :cond_0

    .line 669499
    sget-object v1, LX/0VA;->INTERNAL:LX/0VA;

    .line 669500
    invoke-static {p0}, LX/0V8;->c(LX/0V8;)V

    .line 669501
    invoke-static {p0}, LX/0V8;->d(LX/0V8;)V

    .line 669502
    sget-object v8, LX/0VA;->INTERNAL:LX/0VA;

    if-ne v1, v8, :cond_1

    iget-object v8, p0, LX/0V8;->c:Landroid/os/StatFs;

    .line 669503
    :goto_0
    if-eqz v8, :cond_3

    .line 669504
    sget v9, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v10, 0x12

    if-lt v9, v10, :cond_2

    .line 669505
    invoke-virtual {v8}, Landroid/os/StatFs;->getBlockSizeLong()J

    move-result-wide v10

    .line 669506
    invoke-virtual {v8}, Landroid/os/StatFs;->getFreeBlocksLong()J

    move-result-wide v8

    .line 669507
    :goto_1
    mul-long/2addr v8, v10

    .line 669508
    :goto_2
    move-wide v2, v8

    .line 669509
    sget-object v1, LX/0VA;->INTERNAL:LX/0VA;

    .line 669510
    invoke-static {p0}, LX/0V8;->c(LX/0V8;)V

    .line 669511
    invoke-static {p0}, LX/0V8;->d(LX/0V8;)V

    .line 669512
    sget-object v8, LX/0VA;->INTERNAL:LX/0VA;

    if-ne v1, v8, :cond_4

    iget-object v8, p0, LX/0V8;->c:Landroid/os/StatFs;

    .line 669513
    :goto_3
    if-eqz v8, :cond_6

    .line 669514
    sget v9, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v10, 0x12

    if-lt v9, v10, :cond_5

    .line 669515
    invoke-virtual {v8}, Landroid/os/StatFs;->getBlockSizeLong()J

    move-result-wide v10

    .line 669516
    invoke-virtual {v8}, Landroid/os/StatFs;->getBlockCountLong()J

    move-result-wide v8

    .line 669517
    :goto_4
    mul-long/2addr v8, v10

    .line 669518
    :goto_5
    move-wide v4, v8

    .line 669519
    cmp-long v1, v2, v6

    if-ltz v1, :cond_0

    cmp-long v1, v4, v6

    if-lez v1, :cond_0

    .line 669520
    const-string v1, "free_disk_percent"

    const-wide/16 v6, 0x64

    mul-long/2addr v2, v6

    div-long/2addr v2, v4

    long-to-int v2, v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 669521
    :cond_0
    return-object v0

    .line 669522
    :cond_1
    iget-object v8, p0, LX/0V8;->e:Landroid/os/StatFs;

    goto :goto_0

    .line 669523
    :cond_2
    invoke-virtual {v8}, Landroid/os/StatFs;->getBlockSize()I

    move-result v9

    int-to-long v10, v9

    .line 669524
    invoke-virtual {v8}, Landroid/os/StatFs;->getFreeBlocks()I

    move-result v8

    int-to-long v8, v8

    goto :goto_1

    .line 669525
    :cond_3
    const-wide/16 v8, -0x1

    goto :goto_2

    .line 669526
    :cond_4
    iget-object v8, p0, LX/0V8;->e:Landroid/os/StatFs;

    goto :goto_3

    .line 669527
    :cond_5
    invoke-virtual {v8}, Landroid/os/StatFs;->getBlockSize()I

    move-result v9

    int-to-long v10, v9

    .line 669528
    invoke-virtual {v8}, Landroid/os/StatFs;->getBlockCount()I

    move-result v8

    int-to-long v8, v8

    goto :goto_4

    .line 669529
    :cond_6
    const-wide/16 v8, -0x1

    goto :goto_5
.end method
