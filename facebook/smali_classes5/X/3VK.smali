.class public LX/3VK;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# static fields
.field public static final a:LX/1Cz;


# instance fields
.field private final b:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 587741
    new-instance v0, LX/3VL;

    invoke-direct {v0}, LX/3VL;-><init>()V

    sput-object v0, LX/3VK;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 587742
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 587743
    const v0, 0x7f030c71

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 587744
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/3VK;->setOrientation(I)V

    .line 587745
    const v0, 0x7f0d175f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/3VK;->b:Lcom/facebook/resources/ui/FbTextView;

    .line 587746
    return-void
.end method


# virtual methods
.method public setFooterText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 587747
    iget-object v0, p0, LX/3VK;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 587748
    return-void
.end method

.method public setFooterTextColor(I)V
    .locals 1

    .prologue
    .line 587749
    iget-object v0, p0, LX/3VK;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 587750
    return-void
.end method
