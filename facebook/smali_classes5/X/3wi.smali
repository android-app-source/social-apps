.class public final LX/3wi;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Landroid/graphics/PorterDuff$Mode;

.field public static b:LX/3wi;

.field private static final c:LX/3wh;

.field private static final d:[I

.field public static final e:[I

.field private static final f:[I

.field private static final g:[I

.field public static final h:[I

.field public static final i:[I


# instance fields
.field public j:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Landroid/content/Context;",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/content/res/ColorStateList;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 656032
    sget-object v0, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    sput-object v0, LX/3wi;->a:Landroid/graphics/PorterDuff$Mode;

    .line 656033
    new-instance v0, LX/3wh;

    const/4 v1, 0x6

    invoke-direct {v0, v1}, LX/3wh;-><init>(I)V

    sput-object v0, LX/3wi;->c:LX/3wh;

    .line 656034
    new-array v0, v6, [I

    const v1, 0x7f02003b

    aput v1, v0, v3

    const v1, 0x7f020039

    aput v1, v0, v4

    const v1, 0x7f020002

    aput v1, v0, v5

    sput-object v0, LX/3wi;->d:[I

    .line 656035
    const/16 v0, 0xc

    new-array v0, v0, [I

    const v1, 0x7f020014

    aput v1, v0, v3

    const v1, 0x7f020017

    aput v1, v0, v4

    const v1, 0x7f02001e

    aput v1, v0, v5

    const v1, 0x7f020016

    aput v1, v0, v6

    const v1, 0x7f020015

    aput v1, v0, v7

    const/4 v1, 0x5

    const v2, 0x7f02001d

    aput v2, v0, v1

    const/4 v1, 0x6

    const v2, 0x7f020018

    aput v2, v0, v1

    const/4 v1, 0x7

    const v2, 0x7f020019

    aput v2, v0, v1

    const/16 v1, 0x8

    const v2, 0x7f02001c

    aput v2, v0, v1

    const/16 v1, 0x9

    const v2, 0x7f02001b

    aput v2, v0, v1

    const/16 v1, 0xa

    const v2, 0x7f02001a

    aput v2, v0, v1

    const/16 v1, 0xb

    const v2, 0x7f02001f

    aput v2, v0, v1

    sput-object v0, LX/3wi;->e:[I

    .line 656036
    new-array v0, v7, [I

    const v1, 0x7f020038

    aput v1, v0, v3

    const v1, 0x7f02003a

    aput v1, v0, v4

    const v1, 0x7f020012

    aput v1, v0, v5

    const v1, 0x7f020036

    aput v1, v0, v6

    sput-object v0, LX/3wi;->f:[I

    .line 656037
    new-array v0, v6, [I

    const v1, 0x7f02002e

    aput v1, v0, v3

    const v1, 0x7f020010

    aput v1, v0, v4

    const v1, 0x7f02002d

    aput v1, v0, v5

    sput-object v0, LX/3wi;->g:[I

    .line 656038
    const/16 v0, 0xa

    new-array v0, v0, [I

    const v1, 0x7f020013

    aput v1, v0, v3

    const v1, 0x7f020034

    aput v1, v0, v4

    const v1, 0x7f02003c

    aput v1, v0, v5

    const v1, 0x7f020030

    aput v1, v0, v6

    const v1, 0x7f020031

    aput v1, v0, v7

    const/4 v1, 0x5

    const v2, 0x7f02002f

    aput v2, v0, v1

    const/4 v1, 0x6

    const v2, 0x7f020033

    aput v2, v0, v1

    const/4 v1, 0x7

    const v2, 0x7f020032

    aput v2, v0, v1

    const/16 v1, 0x8

    const v2, 0x7f020008

    aput v2, v0, v1

    const/16 v1, 0x9

    const v2, 0x7f020003

    aput v2, v0, v1

    sput-object v0, LX/3wi;->h:[I

    .line 656039
    new-array v0, v5, [I

    const v1, 0x7f020004

    aput v1, v0, v3

    const v1, 0x7f020009

    aput v1, v0, v4

    sput-object v0, LX/3wi;->i:[I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 656040
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 656041
    return-void
.end method

.method public static a([II)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 656042
    array-length v2, p0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    aget v3, p0, v1

    .line 656043
    if-ne v3, p1, :cond_1

    .line 656044
    const/4 v0, 0x1

    .line 656045
    :cond_0
    return v0

    .line 656046
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static c(Landroid/content/Context;I)Landroid/content/res/ColorStateList;
    .locals 9

    .prologue
    const/4 v1, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 656047
    new-array v0, v1, [[I

    .line 656048
    new-array v1, v1, [I

    .line 656049
    invoke-static {p0, p1}, LX/3xW;->a(Landroid/content/Context;I)I

    move-result v2

    .line 656050
    const v3, 0x7f010056

    invoke-static {p0, v3}, LX/3xW;->a(Landroid/content/Context;I)I

    move-result v3

    .line 656051
    sget-object v4, LX/3xW;->a:[I

    aput-object v4, v0, v5

    .line 656052
    const v4, 0x7f010057

    invoke-static {p0, v4}, LX/3xW;->c(Landroid/content/Context;I)I

    move-result v4

    aput v4, v1, v5

    .line 656053
    sget-object v4, LX/3xW;->d:[I

    aput-object v4, v0, v6

    .line 656054
    invoke-static {v3, v2}, LX/3qk;->a(II)I

    move-result v4

    aput v4, v1, v6

    .line 656055
    sget-object v4, LX/3xW;->b:[I

    aput-object v4, v0, v7

    .line 656056
    invoke-static {v3, v2}, LX/3qk;->a(II)I

    move-result v3

    aput v3, v1, v7

    .line 656057
    sget-object v3, LX/3xW;->h:[I

    aput-object v3, v0, v8

    .line 656058
    aput v2, v1, v8

    .line 656059
    new-instance v2, Landroid/content/res/ColorStateList;

    invoke-direct {v2, v0, v1}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    return-object v2
.end method
