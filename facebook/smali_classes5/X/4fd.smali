.class public final LX/4fd;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/0Ot",
            "<",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Object;",
            ">;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 798655
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 798656
    invoke-static {}, LX/0R9;->b()Ljava/util/LinkedList;

    move-result-object v0

    iput-object v0, p0, LX/4fd;->a:Ljava/util/List;

    return-void
.end method

.method public static a(LX/4fd;)LX/0Ot;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Ot",
            "<",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;>;>;"
        }
    .end annotation

    .prologue
    .line 798657
    iget-object v0, p0, LX/4fd;->a:Ljava/util/List;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    .line 798658
    new-instance v1, LX/4fc;

    invoke-direct {v1, p0, v0}, LX/4fc;-><init>(LX/4fd;LX/0Px;)V

    return-object v1
.end method


# virtual methods
.method public final a(LX/0Ot;)LX/4fd;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<+",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;>;)",
            "LX/4fd;"
        }
    .end annotation

    .prologue
    .line 798659
    iget-object v0, p0, LX/4fd;->a:Ljava/util/List;

    new-instance v1, LX/4fa;

    invoke-direct {v1, p0, p1}, LX/4fa;-><init>(LX/4fd;LX/0Ot;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 798660
    return-object p0
.end method
