.class public final LX/4Y6;
.super LX/0ur;
.source ""


# instance fields
.field public A:Lcom/facebook/graphql/enums/GraphQLPlaceType;

.field public B:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public C:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public D:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public E:Lcom/facebook/graphql/model/GraphQLPhoto;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public F:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public G:Z

.field public H:Lcom/facebook/graphql/model/GraphQLPageStarRatersConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public I:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLRedirectionInfo;",
            ">;"
        }
    .end annotation
.end field

.field public J:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPhoto;",
            ">;"
        }
    .end annotation
.end field

.field public K:Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public L:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public M:Z

.field public N:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public O:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public P:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Q:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public R:Lcom/facebook/graphql/enums/GraphQLSavedState;

.field public S:Lcom/facebook/graphql/model/GraphQLViewerVisitsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public T:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public U:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Lcom/facebook/graphql/model/GraphQLStreetAddress;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Z

.field public d:Z

.field public e:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/enums/GraphQLPageCategoryType;

.field public h:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Z

.field public k:Z

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTimeRange;",
            ">;"
        }
    .end annotation
.end field

.field public n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Z

.field public p:Lcom/facebook/graphql/model/GraphQLLocation;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Lcom/facebook/graphql/model/GraphQLGeoRectangle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Lcom/facebook/graphql/model/GraphQLRating;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Lcom/facebook/graphql/model/GraphQLPageLikersConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Lcom/facebook/graphql/model/GraphQLPageVisitsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Lcom/facebook/graphql/enums/GraphQLPermanentlyClosedStatus;

.field public w:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

.field public z:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 774794
    invoke-direct {p0}, LX/0ur;-><init>()V

    .line 774795
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageCategoryType;

    iput-object v0, p0, LX/4Y6;->g:Lcom/facebook/graphql/enums/GraphQLPageCategoryType;

    .line 774796
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPermanentlyClosedStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPermanentlyClosedStatus;

    iput-object v0, p0, LX/4Y6;->v:Lcom/facebook/graphql/enums/GraphQLPermanentlyClosedStatus;

    .line 774797
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    iput-object v0, p0, LX/4Y6;->y:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    .line 774798
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    iput-object v0, p0, LX/4Y6;->A:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    .line 774799
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    iput-object v0, p0, LX/4Y6;->O:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 774800
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSavedState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    iput-object v0, p0, LX/4Y6;->R:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 774801
    const/4 v0, 0x0

    iput-object v0, p0, LX/4Y6;->U:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 774802
    instance-of v0, p0, LX/4Y6;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 774803
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPlace;)LX/4Y6;
    .locals 2

    .prologue
    .line 774804
    new-instance v0, LX/4Y6;

    invoke-direct {v0}, LX/4Y6;-><init>()V

    .line 774805
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 774806
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->k()Lcom/facebook/graphql/model/GraphQLStreetAddress;

    move-result-object v1

    iput-object v1, v0, LX/4Y6;->b:Lcom/facebook/graphql/model/GraphQLStreetAddress;

    .line 774807
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->l()Z

    move-result v1

    iput-boolean v1, v0, LX/4Y6;->c:Z

    .line 774808
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->m()Z

    move-result v1

    iput-boolean v1, v0, LX/4Y6;->d:Z

    .line 774809
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4Y6;->e:Lcom/facebook/graphql/model/GraphQLImage;

    .line 774810
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->o()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4Y6;->f:LX/0Px;

    .line 774811
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->p()Lcom/facebook/graphql/enums/GraphQLPageCategoryType;

    move-result-object v1

    iput-object v1, v0, LX/4Y6;->g:Lcom/facebook/graphql/enums/GraphQLPageCategoryType;

    .line 774812
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->q()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v1

    iput-object v1, v0, LX/4Y6;->h:Lcom/facebook/graphql/model/GraphQLPage;

    .line 774813
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->r()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Y6;->i:Ljava/lang/String;

    .line 774814
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->s()Z

    move-result v1

    iput-boolean v1, v0, LX/4Y6;->j:Z

    .line 774815
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->t()Z

    move-result v1

    iput-boolean v1, v0, LX/4Y6;->k:Z

    .line 774816
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->u()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Y6;->l:Ljava/lang/String;

    .line 774817
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->v()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4Y6;->m:LX/0Px;

    .line 774818
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->w()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Y6;->n:Ljava/lang/String;

    .line 774819
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->x()Z

    move-result v1

    iput-boolean v1, v0, LX/4Y6;->o:Z

    .line 774820
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->y()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v1

    iput-object v1, v0, LX/4Y6;->p:Lcom/facebook/graphql/model/GraphQLLocation;

    .line 774821
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->z()Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    move-result-object v1

    iput-object v1, v0, LX/4Y6;->q:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    .line 774822
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->A()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Y6;->r:Ljava/lang/String;

    .line 774823
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->B()Lcom/facebook/graphql/model/GraphQLRating;

    move-result-object v1

    iput-object v1, v0, LX/4Y6;->s:Lcom/facebook/graphql/model/GraphQLRating;

    .line 774824
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->C()Lcom/facebook/graphql/model/GraphQLPageLikersConnection;

    move-result-object v1

    iput-object v1, v0, LX/4Y6;->t:Lcom/facebook/graphql/model/GraphQLPageLikersConnection;

    .line 774825
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->D()Lcom/facebook/graphql/model/GraphQLPageVisitsConnection;

    move-result-object v1

    iput-object v1, v0, LX/4Y6;->u:Lcom/facebook/graphql/model/GraphQLPageVisitsConnection;

    .line 774826
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->E()Lcom/facebook/graphql/enums/GraphQLPermanentlyClosedStatus;

    move-result-object v1

    iput-object v1, v0, LX/4Y6;->v:Lcom/facebook/graphql/enums/GraphQLPermanentlyClosedStatus;

    .line 774827
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->F()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4Y6;->w:Lcom/facebook/graphql/model/GraphQLImage;

    .line 774828
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->G()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/4Y6;->x:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 774829
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->H()Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    move-result-object v1

    iput-object v1, v0, LX/4Y6;->y:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    .line 774830
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->I()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Y6;->z:Ljava/lang/String;

    .line 774831
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->J()Lcom/facebook/graphql/enums/GraphQLPlaceType;

    move-result-object v1

    iput-object v1, v0, LX/4Y6;->A:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    .line 774832
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->K()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Y6;->B:Ljava/lang/String;

    .line 774833
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4Y6;->C:Lcom/facebook/graphql/model/GraphQLImage;

    .line 774834
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->M()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4Y6;->D:Lcom/facebook/graphql/model/GraphQLImage;

    .line 774835
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->N()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v1

    iput-object v1, v0, LX/4Y6;->E:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 774836
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4Y6;->F:Lcom/facebook/graphql/model/GraphQLImage;

    .line 774837
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->P()Z

    move-result v1

    iput-boolean v1, v0, LX/4Y6;->G:Z

    .line 774838
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->Q()Lcom/facebook/graphql/model/GraphQLPageStarRatersConnection;

    move-result-object v1

    iput-object v1, v0, LX/4Y6;->H:Lcom/facebook/graphql/model/GraphQLPageStarRatersConnection;

    .line 774839
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->R()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4Y6;->I:LX/0Px;

    .line 774840
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->S()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4Y6;->J:LX/0Px;

    .line 774841
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->T()Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    move-result-object v1

    iput-object v1, v0, LX/4Y6;->K:Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    .line 774842
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->U()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4Y6;->L:LX/0Px;

    .line 774843
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->V()Z

    move-result v1

    iput-boolean v1, v0, LX/4Y6;->M:Z

    .line 774844
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->W()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4Y6;->N:LX/0Px;

    .line 774845
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->X()Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    move-result-object v1

    iput-object v1, v0, LX/4Y6;->O:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 774846
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->Y()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Y6;->P:Ljava/lang/String;

    .line 774847
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->Z()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4Y6;->Q:LX/0Px;

    .line 774848
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->aa()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v1

    iput-object v1, v0, LX/4Y6;->R:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 774849
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->ab()Lcom/facebook/graphql/model/GraphQLViewerVisitsConnection;

    move-result-object v1

    iput-object v1, v0, LX/4Y6;->S:Lcom/facebook/graphql/model/GraphQLViewerVisitsConnection;

    .line 774850
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->ac()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4Y6;->T:LX/0Px;

    .line 774851
    invoke-static {v0, p0}, LX/0ur;->a(LX/0ur;Lcom/facebook/graphql/modelutil/BaseModel;)V

    .line 774852
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    iput-object v1, v0, LX/4Y6;->U:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 774853
    return-object v0
.end method


# virtual methods
.method public final a()Lcom/facebook/graphql/model/GraphQLPlace;
    .locals 2

    .prologue
    .line 774792
    new-instance v0, Lcom/facebook/graphql/model/GraphQLPlace;

    invoke-direct {v0, p0}, Lcom/facebook/graphql/model/GraphQLPlace;-><init>(LX/4Y6;)V

    .line 774793
    return-object v0
.end method
