.class public LX/4TW;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 718648
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 13

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 718680
    const/4 v0, 0x0

    .line 718681
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v4, :cond_a

    .line 718682
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 718683
    :goto_0
    return v1

    .line 718684
    :cond_0
    const-string v11, "open_action"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 718685
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;

    move-result-object v0

    move-object v7, v0

    move v0, v2

    .line 718686
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_8

    .line 718687
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 718688
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 718689
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_1

    if-eqz v10, :cond_1

    .line 718690
    const-string v11, "button_text"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 718691
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 718692
    :cond_2
    const-string v11, "node"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 718693
    invoke-static {p0, p1}, LX/4TU;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 718694
    :cond_3
    const-string v11, "prefill_text"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 718695
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 718696
    :cond_4
    const-string v11, "preview_call_to_action_text"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 718697
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 718698
    :cond_5
    const-string v11, "preview_text"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 718699
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 718700
    :cond_6
    const-string v11, "preview_text_with_entities"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 718701
    invoke-static {p0, p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 718702
    :cond_7
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 718703
    :cond_8
    const/4 v10, 0x7

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 718704
    invoke-virtual {p1, v1, v9}, LX/186;->b(II)V

    .line 718705
    invoke-virtual {p1, v2, v8}, LX/186;->b(II)V

    .line 718706
    if-eqz v0, :cond_9

    .line 718707
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v7}, LX/186;->a(ILjava/lang/Enum;)V

    .line 718708
    :cond_9
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 718709
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 718710
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 718711
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 718712
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_a
    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move-object v7, v0

    move v8, v1

    move v9, v1

    move v0, v1

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 718649
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 718650
    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 718651
    if-eqz v0, :cond_0

    .line 718652
    const-string v1, "button_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 718653
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 718654
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 718655
    if-eqz v0, :cond_1

    .line 718656
    const-string v1, "node"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 718657
    invoke-static {p0, v0, p2, p3}, LX/4TU;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 718658
    :cond_1
    invoke-virtual {p0, p1, v3, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 718659
    if-eqz v0, :cond_2

    .line 718660
    const-string v0, "open_action"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 718661
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;

    invoke-virtual {p0, p1, v3, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 718662
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 718663
    if-eqz v0, :cond_3

    .line 718664
    const-string v1, "prefill_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 718665
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 718666
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 718667
    if-eqz v0, :cond_4

    .line 718668
    const-string v1, "preview_call_to_action_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 718669
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 718670
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 718671
    if-eqz v0, :cond_5

    .line 718672
    const-string v1, "preview_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 718673
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 718674
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 718675
    if-eqz v0, :cond_6

    .line 718676
    const-string v1, "preview_text_with_entities"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 718677
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 718678
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 718679
    return-void
.end method
