.class public final LX/3f5;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0U1;

.field public static final b:LX/0U1;

.field public static final c:LX/0U1;

.field public static final d:LX/0U1;

.field public static final e:LX/0U1;

.field public static final f:LX/0U1;

.field public static final g:LX/0U1;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 621382
    new-instance v0, LX/0U1;

    const-string v1, "page_id"

    const-string v2, "TEXT PRIMARY KEY"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3f5;->a:LX/0U1;

    .line 621383
    new-instance v0, LX/0U1;

    const-string v1, "page_name"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3f5;->b:LX/0U1;

    .line 621384
    new-instance v0, LX/0U1;

    const-string v1, "profile_pic_url"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3f5;->c:LX/0U1;

    .line 621385
    new-instance v0, LX/0U1;

    const-string v1, "cover_photo_url"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3f5;->d:LX/0U1;

    .line 621386
    new-instance v0, LX/0U1;

    const-string v1, "viewer_profile_permissions"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3f5;->e:LX/0U1;

    .line 621387
    new-instance v0, LX/0U1;

    const-string v1, "can_viewer_promote"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3f5;->f:LX/0U1;

    .line 621388
    new-instance v0, LX/0U1;

    const-string v1, "access_token"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3f5;->g:LX/0U1;

    return-void
.end method
