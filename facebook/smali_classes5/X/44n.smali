.class public LX/44n;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "LX/1fX;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/1fX;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 669955
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/1fX;
    .locals 6

    .prologue
    .line 669943
    sget-object v0, LX/44n;->a:LX/1fX;

    if-nez v0, :cond_1

    .line 669944
    const-class v1, LX/44n;

    monitor-enter v1

    .line 669945
    :try_start_0
    sget-object v0, LX/44n;->a:LX/1fX;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 669946
    if-eqz v2, :cond_0

    .line 669947
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 669948
    invoke-static {v0}, LX/0TJ;->a(LX/0QB;)LX/0TJ;

    move-result-object v3

    check-cast v3, LX/0TJ;

    invoke-static {v0}, LX/0Si;->a(LX/0QB;)LX/0Si;

    move-result-object v4

    check-cast v4, LX/0Sj;

    const/16 v5, 0x1a1

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/0TR;->a(LX/0QB;)LX/0TR;

    move-result-object v5

    check-cast v5, LX/0TR;

    invoke-static {v3, v4, p0, v5}, LX/0Su;->e(LX/0TJ;LX/0Sj;LX/0Or;LX/0TR;)LX/1fX;

    move-result-object v3

    move-object v0, v3

    .line 669949
    sput-object v0, LX/44n;->a:LX/1fX;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 669950
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 669951
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 669952
    :cond_1
    sget-object v0, LX/44n;->a:LX/1fX;

    return-object v0

    .line 669953
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 669954
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 669956
    invoke-static {p0}, LX/0TJ;->a(LX/0QB;)LX/0TJ;

    move-result-object v0

    check-cast v0, LX/0TJ;

    invoke-static {p0}, LX/0Si;->a(LX/0QB;)LX/0Si;

    move-result-object v1

    check-cast v1, LX/0Sj;

    const/16 v2, 0x1a1

    invoke-static {p0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static {p0}, LX/0TR;->a(LX/0QB;)LX/0TR;

    move-result-object v2

    check-cast v2, LX/0TR;

    invoke-static {v0, v1, v3, v2}, LX/0Su;->e(LX/0TJ;LX/0Sj;LX/0Or;LX/0TR;)LX/1fX;

    move-result-object v0

    return-object v0
.end method
