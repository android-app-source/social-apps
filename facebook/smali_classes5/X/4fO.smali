.class public abstract LX/4fO;
.super LX/3hi;
.source ""

# interfaces
.implements LX/33B;


# instance fields
.field private b:LX/4fE;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 798409
    invoke-direct {p0}, LX/3hi;-><init>()V

    return-void
.end method

.method private declared-synchronized d()LX/4fE;
    .locals 1

    .prologue
    .line 798414
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/4fO;->b:LX/4fE;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(LX/4fE;)V
    .locals 1

    .prologue
    .line 798415
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, LX/4fO;->b:LX/4fE;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 798416
    monitor-exit p0

    return-void

    .line 798417
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 798410
    invoke-direct {p0}, LX/4fO;->d()LX/4fE;

    move-result-object v0

    .line 798411
    if-eqz v0, :cond_0

    .line 798412
    invoke-virtual {v0}, LX/4fE;->c()V

    .line 798413
    :cond_0
    return-void
.end method
