.class public final LX/50b;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 823982
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/util/Comparator;Ljava/lang/Iterable;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Comparator",
            "<*>;",
            "Ljava/lang/Iterable",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 823983
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 823984
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 823985
    instance-of v0, p1, Ljava/util/SortedSet;

    if-eqz v0, :cond_1

    .line 823986
    check-cast p1, Ljava/util/SortedSet;

    .line 823987
    invoke-interface {p1}, Ljava/util/SortedSet;->comparator()Ljava/util/Comparator;

    move-result-object v0

    .line 823988
    if-nez v0, :cond_0

    .line 823989
    sget-object v0, LX/1zb;->a:LX/1zb;

    move-object v0, v0

    .line 823990
    :cond_0
    move-object v0, v0

    .line 823991
    :goto_0
    invoke-interface {p0, v0}, Ljava/util/Comparator;->equals(Ljava/lang/Object;)Z

    move-result v0

    :goto_1
    return v0

    .line 823992
    :cond_1
    instance-of v0, p1, LX/0dY;

    if-eqz v0, :cond_2

    .line 823993
    check-cast p1, LX/0dY;

    invoke-interface {p1}, LX/0dY;->comparator()Ljava/util/Comparator;

    move-result-object v0

    goto :goto_0

    .line 823994
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method
