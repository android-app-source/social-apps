.class public LX/3mZ;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinPageComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/3mZ",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinPageComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 636911
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 636912
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/3mZ;->b:LX/0Zi;

    .line 636913
    iput-object p1, p0, LX/3mZ;->a:LX/0Ot;

    .line 636914
    return-void
.end method

.method public static a(LX/0QB;)LX/3mZ;
    .locals 4

    .prologue
    .line 636900
    const-class v1, LX/3mZ;

    monitor-enter v1

    .line 636901
    :try_start_0
    sget-object v0, LX/3mZ;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 636902
    sput-object v2, LX/3mZ;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 636903
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 636904
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 636905
    new-instance v3, LX/3mZ;

    const/16 p0, 0x8a5

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/3mZ;-><init>(LX/0Ot;)V

    .line 636906
    move-object v0, v3

    .line 636907
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 636908
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/3mZ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 636909
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 636910
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static d(LX/1De;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 636899
    const v0, -0x35fd56d

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 13

    .prologue
    .line 636830
    check-cast p2, LX/3mr;

    .line 636831
    iget-object v0, p0, LX/3mZ;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinPageComponentSpec;

    iget-object v1, p2, LX/3mr;->a:LX/3mR;

    iget-object v2, p2, LX/3mr;->b:LX/1Po;

    const/16 p0, 0x8

    const/4 v3, 0x0

    const/4 v5, 0x0

    .line 636832
    iget v4, v1, LX/3mR;->d:I

    if-eqz v4, :cond_0

    .line 636833
    sget-object v4, Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinPageComponentSpec;->a:Ljava/lang/String;

    const-string v5, "Wrong card type, GYSJ_CARD_TYPE is expected, found %s"

    iget v6, v1, LX/3mR;->d:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 636834
    :goto_0
    move-object v0, v3

    .line 636835
    return-object v0

    .line 636836
    :cond_0
    iget-object v4, v1, LX/3mR;->b:LX/254;

    if-nez v4, :cond_1

    .line 636837
    sget-object v4, Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinPageComponentSpec;->a:Ljava/lang/String;

    const-string v5, "gysjProps.item is null as unexpected"

    invoke-static {v4, v5}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 636838
    :cond_1
    iget-object v4, v1, LX/3mR;->c:Lcom/facebook/graphql/model/GraphQLProfile;

    if-nez v4, :cond_2

    .line 636839
    sget-object v4, Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinPageComponentSpec;->a:Ljava/lang/String;

    const-string v5, "Profile (Group) must be not null in GroupsYouShouldJoinPageComponent, found it is null."

    invoke-static {v4, v5}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 636840
    :cond_2
    iget-object v7, v1, LX/3mR;->c:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 636841
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLProfile;->x()I

    move-result v8

    .line 636842
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLProfile;->w()Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;

    move-result-object v3

    if-eqz v3, :cond_6

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLProfile;->w()Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;->a()I

    move-result v3

    move v4, v3

    .line 636843
    :goto_1
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLProfile;->aa()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    if-eqz v3, :cond_7

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLProfile;->aa()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v3

    move-object v6, v3

    .line 636844
    :goto_2
    const/4 v3, 0x0

    .line 636845
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLProfile;->Z()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v9

    if-eqz v9, :cond_4

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLProfile;->Z()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLGroup;->l()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v9

    sget-object v10, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->CITY:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    if-eq v9, v10, :cond_3

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLProfile;->Z()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLGroup;->l()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v9

    sget-object v10, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->COLLEGE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    if-ne v9, v10, :cond_4

    :cond_3
    iget-object v9, v0, Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinPageComponentSpec;->g:LX/0Uh;

    const/16 v10, 0x318

    invoke-virtual {v9, v10, v3}, LX/0Uh;->a(IZ)Z

    move-result v9

    if-eqz v9, :cond_4

    const/4 v3, 0x1

    :cond_4
    move v9, v3

    .line 636846
    move-object v3, v2

    .line 636847
    check-cast v3, LX/1Pr;

    new-instance v10, LX/3n0;

    iget-object v11, v1, LX/3mR;->b:LX/254;

    invoke-direct {v10, v11}, LX/3n0;-><init>(LX/254;)V

    iget-object v11, v1, LX/3mR;->a:Lcom/facebook/graphql/model/GroupsYouShouldJoinFeedUnit;

    invoke-interface {v3, v10, v11}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/3n2;

    .line 636848
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v10

    invoke-interface {v10, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v10

    const v11, 0x7f020a3c

    invoke-interface {v10, v11}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v10

    invoke-static {v0, p1, v7, v1}, Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinPageComponentSpec;->a(Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinPageComponentSpec;LX/1De;Lcom/facebook/graphql/model/GraphQLProfile;LX/3mR;)LX/1Dh;

    move-result-object v11

    invoke-interface {v10, v11}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v10

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v11

    const v12, 0x7f0b0933

    invoke-interface {v11, v12}, LX/1Dh;->G(I)LX/1Dh;

    move-result-object v11

    const v12, 0x7f0b0936

    invoke-interface {v11, v12}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v11

    const/4 v12, 0x7

    invoke-interface {v11, v12, p0}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v11

    invoke-interface {v11, v5, p0}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v11

    const/4 v12, 0x2

    invoke-interface {v11, v12}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v11

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v12

    invoke-interface {v12, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v5

    const/4 v12, 0x1

    invoke-interface {v5, v12}, LX/1Dh;->B(I)LX/1Dh;

    move-result-object v5

    const/high16 v12, 0x3f800000    # 1.0f

    invoke-interface {v5, v12}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v12

    iget-object v5, v1, LX/3mR;->b:LX/254;

    .line 636849
    iget-object p0, v0, Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinPageComponentSpec;->f:LX/3my;

    invoke-interface {v5}, LX/254;->a()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object p2

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLGroup;->l()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object p2

    invoke-virtual {p0, p2}, LX/3my;->b(Lcom/facebook/graphql/enums/GraphQLGroupCategory;)Z

    move-result p0

    move v5, p0

    .line 636850
    if-eqz v5, :cond_8

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLProfile;->D()Ljava/lang/String;

    move-result-object v5

    invoke-static {p1, v5}, Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinPageComponentSpec;->a(LX/1De;Ljava/lang/String;)LX/1Dh;

    move-result-object v5

    :goto_3
    invoke-interface {v12, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v7

    if-eqz v9, :cond_9

    const/4 p0, 0x3

    .line 636851
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v6}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v5

    const/4 v12, 0x0

    invoke-virtual {v5, v12}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v5

    const v12, 0x7f0b004e

    invoke-virtual {v5, v12}, LX/1ne;->q(I)LX/1ne;

    move-result-object v5

    const v12, 0x7f0a0428

    invoke-virtual {v5, v12}, LX/1ne;->n(I)LX/1ne;

    move-result-object v5

    const/4 v12, 0x1

    invoke-virtual {v5, v12}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v5

    sget-object v12, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v5, v12}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    const v12, 0x7f020a8d

    invoke-interface {v5, v12}, LX/1Di;->x(I)LX/1Di;

    move-result-object v5

    invoke-interface {v5, p0, p0}, LX/1Di;->d(II)LX/1Di;

    move-result-object v5

    move-object v5, v5

    .line 636852
    :goto_4
    invoke-interface {v7, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v5

    if-eqz v9, :cond_a

    invoke-static {p1, v8, v4}, Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinPageComponentSpec;->a(LX/1De;II)LX/1ne;

    move-result-object v4

    :goto_5
    invoke-interface {v5, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/3mZ;->d(LX/1De;)LX/1dQ;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v4

    invoke-interface {v11, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    .line 636853
    invoke-static {p1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v5

    const v6, 0x7f0a0046

    invoke-virtual {v5, v6}, LX/25Q;->i(I)LX/25Q;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    const v6, 0x7f0b0033

    invoke-interface {v5, v6}, LX/1Di;->i(I)LX/1Di;

    move-result-object v5

    move-object v5, v5

    .line 636854
    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    iget-object v5, v0, Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinPageComponentSpec;->d:LX/3mx;

    const/4 v6, 0x0

    .line 636855
    new-instance v7, LX/3n4;

    invoke-direct {v7, v5}, LX/3n4;-><init>(LX/3mx;)V

    .line 636856
    iget-object v8, v5, LX/3mx;->b:LX/0Zi;

    invoke-virtual {v8}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/3n5;

    .line 636857
    if-nez v8, :cond_5

    .line 636858
    new-instance v8, LX/3n5;

    invoke-direct {v8, v5}, LX/3n5;-><init>(LX/3mx;)V

    .line 636859
    :cond_5
    invoke-static {v8, p1, v6, v6, v7}, LX/3n5;->a$redex0(LX/3n5;LX/1De;IILX/3n4;)V

    .line 636860
    move-object v7, v8

    .line 636861
    move-object v6, v7

    .line 636862
    move-object v5, v6

    .line 636863
    iget-object v6, v5, LX/3n5;->a:LX/3n4;

    iput-object v2, v6, LX/3n4;->c:LX/1Po;

    .line 636864
    iget-object v6, v5, LX/3n5;->e:Ljava/util/BitSet;

    const/4 v7, 0x2

    invoke-virtual {v6, v7}, Ljava/util/BitSet;->set(I)V

    .line 636865
    move-object v5, v5

    .line 636866
    iget-object v6, v5, LX/3n5;->a:LX/3n4;

    iput-object v3, v6, LX/3n4;->b:LX/3n2;

    .line 636867
    iget-object v6, v5, LX/3n5;->e:Ljava/util/BitSet;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Ljava/util/BitSet;->set(I)V

    .line 636868
    move-object v3, v5

    .line 636869
    iget-object v5, v3, LX/3n5;->a:LX/3n4;

    iput-object v1, v5, LX/3n4;->a:LX/3mR;

    .line 636870
    iget-object v5, v3, LX/3n5;->e:Ljava/util/BitSet;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Ljava/util/BitSet;->set(I)V

    .line 636871
    move-object v3, v3

    .line 636872
    invoke-interface {v4, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    invoke-interface {v10, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    goto/16 :goto_0

    :cond_6
    move v4, v5

    .line 636873
    goto/16 :goto_1

    .line 636874
    :cond_7
    const-string v3, ""

    move-object v6, v3

    goto/16 :goto_2

    .line 636875
    :cond_8
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLProfile;->D()Ljava/lang/String;

    move-result-object v5

    invoke-static {p1, v5}, Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinPageComponentSpec;->b(LX/1De;Ljava/lang/String;)LX/1Di;

    move-result-object v5

    goto/16 :goto_3

    :cond_9
    invoke-static {p1, v8}, Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinPageComponentSpec;->a(LX/1De;I)LX/1Di;

    move-result-object v5

    goto/16 :goto_4

    :cond_a
    const/4 p2, 0x1

    const/4 p0, 0x0

    .line 636876
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v6

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0f00cf

    new-array v9, p2, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v9, p0

    invoke-virtual {v7, v8, v4, v9}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, p0}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v6

    const v7, 0x7f0b004b

    invoke-virtual {v6, v7}, LX/1ne;->q(I)LX/1ne;

    move-result-object v6

    const v7, 0x7f0a015d

    invoke-virtual {v6, v7}, LX/1ne;->n(I)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, p2}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v6

    sget-object v7, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v6, v7}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v6

    move-object v4, v6

    .line 636877
    goto/16 :goto_5
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 636878
    invoke-static {}, LX/1dS;->b()V

    .line 636879
    iget v0, p1, LX/1dQ;->b:I

    .line 636880
    sparse-switch v0, :sswitch_data_0

    .line 636881
    :goto_0
    return-object v2

    .line 636882
    :sswitch_0
    check-cast p2, LX/3Ae;

    .line 636883
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 636884
    check-cast v1, LX/3mr;

    .line 636885
    iget-object v3, p0, LX/3mZ;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinPageComponentSpec;

    iget-object p1, v1, LX/3mr;->a:LX/3mR;

    .line 636886
    iget-object p2, v3, Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinPageComponentSpec;->e:LX/3mE;

    .line 636887
    iget-object p0, p1, LX/3mR;->a:Lcom/facebook/graphql/model/GroupsYouShouldJoinFeedUnit;

    iget-object v1, p1, LX/3mR;->b:LX/254;

    const-string v3, "gysj_profile"

    invoke-static {p2, p0, v1, v3}, LX/3mE;->a(LX/3mE;Lcom/facebook/graphql/model/GroupsYouShouldJoinFeedUnit;LX/254;Ljava/lang/String;)V

    .line 636888
    iget-object p0, p1, LX/3mR;->b:LX/254;

    invoke-static {p0}, LX/25D;->a(LX/16q;)Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object p0

    .line 636889
    iget-object v1, p2, LX/3mE;->f:LX/1nA;

    invoke-static {p0}, LX/3Bd;->a(Lcom/facebook/graphql/model/GraphQLProfile;)LX/1y5;

    move-result-object p0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, p0, v3}, LX/1nA;->a(Landroid/view/View;LX/1y5;Landroid/os/Bundle;)V

    .line 636890
    goto :goto_0

    .line 636891
    :sswitch_1
    iget-object v0, p1, LX/1dQ;->a:LX/1X1;

    .line 636892
    check-cast v0, LX/3mr;

    .line 636893
    iget-object v1, p0, LX/3mZ;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinPageComponentSpec;

    iget-object v3, v0, LX/3mr;->a:LX/3mR;

    .line 636894
    iget-object p1, v1, Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinPageComponentSpec;->e:LX/3mE;

    iget-object p0, v3, LX/3mR;->a:Lcom/facebook/graphql/model/GroupsYouShouldJoinFeedUnit;

    iget-object v0, v3, LX/3mR;->b:LX/254;

    .line 636895
    const-string v1, "gysj_xout"

    invoke-static {p1, p0, v0, v1}, LX/3mE;->a(LX/3mE;Lcom/facebook/graphql/model/GroupsYouShouldJoinFeedUnit;LX/254;Ljava/lang/String;)V

    .line 636896
    invoke-static {v0}, LX/25D;->a(LX/16q;)Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v1

    .line 636897
    iget-object v3, p1, LX/3mE;->g:Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

    invoke-virtual {v3, p0, v1}, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->a(Lcom/facebook/graphql/model/GroupsYouShouldJoinFeedUnit;Ljava/lang/String;)V

    .line 636898
    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x35fd56d -> :sswitch_0
        0x55bbb778 -> :sswitch_1
    .end sparse-switch
.end method
