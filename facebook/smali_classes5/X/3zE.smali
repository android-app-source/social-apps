.class public LX/3zE;
.super Landroid/preference/Preference;
.source ""


# instance fields
.field private final a:LX/3yz;

.field public b:LX/3yo;

.field public c:Z

.field public d:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 662279
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/3zE;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 662280
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 662312
    const/4 v0, -0x1

    invoke-direct {p0, p1, p2, v0}, LX/3zE;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 662313
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 662314
    invoke-direct {p0, p1, p2, p3}, Landroid/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 662315
    const v0, 0x7f0310be

    invoke-virtual {p0, v0}, LX/3zE;->setLayoutResource(I)V

    .line 662316
    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    .line 662317
    invoke-static {v0}, LX/3yz;->a(LX/0QB;)LX/3yz;

    move-result-object v0

    check-cast v0, LX/3yz;

    iput-object v0, p0, LX/3zE;->a:LX/3yz;

    .line 662318
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/3zE;->setSelectable(Z)V

    .line 662319
    return-void
.end method


# virtual methods
.method public final onBindView(Landroid/view/View;)V
    .locals 8

    .prologue
    const/16 v7, 0x21

    const/16 v2, 0x8

    const/4 v3, 0x0

    .line 662281
    invoke-super {p0, p1}, Landroid/preference/Preference;->onBindView(Landroid/view/View;)V

    .line 662282
    iget-object v0, p0, LX/3zE;->b:LX/3yo;

    if-eqz v0, :cond_0

    .line 662283
    const v0, 0x7f0d27c9

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 662284
    iget-object v1, p0, LX/3zE;->b:LX/3yo;

    iget-object v1, v1, LX/3yo;->a:Ljava/lang/String;

    invoke-static {v1}, LX/3yz;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 662285
    iget-boolean v0, p0, LX/3zE;->c:Z

    if-eqz v0, :cond_1

    const-string v0, "#ED9A00"

    :goto_0
    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v4

    .line 662286
    const v0, 0x7f0d27cb

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 662287
    const v1, 0x7f0d27cc

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 662288
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 662289
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 662290
    const-string v5, "local_default_group"

    iget-object v6, p0, LX/3zE;->b:LX/3yo;

    iget-object v6, v6, LX/3yo;->b:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 662291
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 662292
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 662293
    :goto_1
    const v0, 0x7f0d27ca

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 662294
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 662295
    iget-object v1, p0, LX/3zE;->b:LX/3yo;

    iget-boolean v1, v1, LX/3yo;->c:Z

    if-eqz v1, :cond_3

    move v1, v2

    :goto_2
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 662296
    const v0, 0x7f0d27cd

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 662297
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 662298
    iget-boolean v1, p0, LX/3zE;->d:Z

    if-eqz v1, :cond_4

    .line 662299
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 662300
    :cond_0
    :goto_3
    return-void

    .line 662301
    :cond_1
    const-string v0, "#009EED"

    goto :goto_0

    .line 662302
    :cond_2
    new-instance v0, LX/3zD;

    invoke-direct {v0, p0}, LX/3zD;-><init>(LX/3zE;)V

    .line 662303
    new-instance v5, Landroid/text/SpannableString;

    iget-object v6, p0, LX/3zE;->b:LX/3yo;

    iget-object v6, v6, LX/3yo;->b:Ljava/lang/String;

    invoke-static {v6}, LX/3yz;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 662304
    invoke-interface {v5}, Landroid/text/Spannable;->length()I

    move-result v6

    invoke-interface {v5, v0, v3, v6, v7}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 662305
    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v0, v4}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-interface {v5}, Landroid/text/Spannable;->length()I

    move-result v6

    invoke-interface {v5, v0, v3, v6, v7}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 662306
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    const-string v6, "Current Group: "

    invoke-virtual {v0, v6}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    .line 662307
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 662308
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 662309
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    :cond_3
    move v1, v3

    .line 662310
    goto :goto_2

    .line 662311
    :cond_4
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_3
.end method
