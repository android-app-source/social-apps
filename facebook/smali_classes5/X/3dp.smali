.class public final LX/3dp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Vj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Vj",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:I

.field public final synthetic b:Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;


# direct methods
.method public constructor <init>(Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;I)V
    .locals 0

    .prologue
    .line 617396
    iput-object p1, p0, LX/3dp;->b:Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;

    iput p2, p0, LX/3dp;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 13

    .prologue
    .line 617397
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 617398
    iget-boolean v0, p1, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    move v0, v0

    .line 617399
    if-nez v0, :cond_0

    .line 617400
    iget-object v0, p1, Lcom/facebook/fbservice/service/OperationResult;->errorThrowable:Ljava/lang/Throwable;

    move-object v0, v0

    .line 617401
    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 617402
    :cond_0
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 617403
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/facebook/stickers/service/FetchStickerPacksAndStickersResult;

    .line 617404
    iget-object v11, v6, Lcom/facebook/stickers/service/FetchStickerPacksAndStickersResult;->b:LX/0Px;

    invoke-virtual {v11}, LX/0Px;->size()I

    move-result v12

    move v9, v8

    :goto_0
    if-ge v9, v12, :cond_4

    invoke-virtual {v11, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/model/StickerPack;

    .line 617405
    iget-object v1, v6, Lcom/facebook/stickers/service/FetchStickerPacksAndStickersResult;->c:LX/0P1;

    .line 617406
    iget-object v2, v0, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v2, v2

    .line 617407
    invoke-virtual {v1, v2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Px;

    .line 617408
    iget-object v2, p0, LX/3dp;->b:Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;

    iget-object v2, v2, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->o:LX/2V6;

    invoke-virtual {v2}, LX/2V6;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 617409
    iget-object v2, v0, Lcom/facebook/stickers/model/StickerPack;->f:Landroid/net/Uri;

    move-object v2, v2

    .line 617410
    if-nez v2, :cond_2

    move v2, v7

    .line 617411
    :goto_1
    if-nez v2, :cond_1

    iget-object v2, p0, LX/3dp;->b:Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;

    invoke-static {v2, v1}, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->a$redex0(Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;LX/0Px;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 617412
    :cond_1
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 617413
    const-string v1, "stickerPack"

    invoke-virtual {v2, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 617414
    iget-object v0, p0, LX/3dp;->b:Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;

    iget-object v0, v0, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->j:LX/0aG;

    const-string v1, "download_sticker_pack_assets"

    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    sget-object v4, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->e:Lcom/facebook/common/callercontext/CallerContext;

    const v5, -0x698c84d2

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 617415
    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 617416
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v0

    iget v1, p0, LX/3dp;->a:I

    if-ne v0, v1, :cond_3

    .line 617417
    invoke-interface {v10}, Ljava/util/List;->size()I

    .line 617418
    invoke-static {v10}, LX/0Vg;->a(Ljava/lang/Iterable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 617419
    :goto_2
    return-object v0

    :cond_2
    move v2, v8

    .line 617420
    goto :goto_1

    .line 617421
    :cond_3
    add-int/lit8 v0, v9, 0x1

    move v9, v0

    goto :goto_0

    .line 617422
    :cond_4
    invoke-interface {v10}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 617423
    invoke-interface {v10}, Ljava/util/List;->size()I

    .line 617424
    invoke-static {v10}, LX/0Vg;->a(Ljava/lang/Iterable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_2

    .line 617425
    :cond_5
    iget-object v0, p0, LX/3dp;->b:Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;

    invoke-static {v0}, Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;->n(Lcom/facebook/stickers/background/StickersAssetsDownloadBackgroundTask;)V

    .line 617426
    new-array v0, v7, [Lcom/facebook/fbservice/service/OperationResult;

    .line 617427
    sget-object v1, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v1, v1

    .line 617428
    aput-object v1, v0, v8

    invoke-static {v0}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    .line 617429
    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_2
.end method
