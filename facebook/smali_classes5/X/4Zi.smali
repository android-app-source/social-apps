.class public LX/4Zi;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/Long;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field private static final b:Ljava/lang/String;

.field private static final l:LX/4Zc;

.field private static final m:LX/4Zc;


# instance fields
.field public c:LX/4Zg;

.field private d:I

.field private e:I

.field private f:LX/4Zh;

.field public g:J

.field public h:J

.field public i:J

.field private j:J

.field public k:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 790149
    const-wide/16 v0, 0x3e8

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, LX/4Zi;->a:Ljava/lang/Long;

    .line 790150
    const-class v0, LX/4Zi;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/4Zi;->b:Ljava/lang/String;

    .line 790151
    new-instance v0, LX/4Zd;

    invoke-direct {v0}, LX/4Zd;-><init>()V

    sput-object v0, LX/4Zi;->l:LX/4Zc;

    .line 790152
    new-instance v0, LX/4Ze;

    invoke-direct {v0}, LX/4Ze;-><init>()V

    sput-object v0, LX/4Zi;->m:LX/4Zc;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const-wide/16 v0, 0x2710

    .line 790197
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 790198
    iput-wide v0, p0, LX/4Zi;->g:J

    .line 790199
    iput-wide v0, p0, LX/4Zi;->h:J

    .line 790200
    iput-wide v0, p0, LX/4Zi;->i:J

    .line 790201
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/4Zi;->k:Z

    .line 790202
    invoke-virtual {p0}, LX/4Zi;->a()V

    .line 790203
    return-void
.end method

.method private a(JLX/4Zc;J)I
    .locals 7

    .prologue
    .line 790195
    iget v3, p0, LX/4Zi;->d:I

    iget-object v6, p0, LX/4Zi;->f:LX/4Zh;

    move-object v0, p3

    move-wide v1, p1

    move-wide v4, p4

    invoke-virtual/range {v0 .. v6}, LX/4Zc;->a(JIJLX/4Zh;)I

    move-result v0

    .line 790196
    iget v1, p0, LX/4Zi;->e:I

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, LX/4Zi;->e:I

    return v0
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 790189
    iput v4, p0, LX/4Zi;->d:I

    .line 790190
    const/4 v0, -0x1

    iput v0, p0, LX/4Zi;->e:I

    .line 790191
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/4Zi;->j:J

    .line 790192
    new-instance v0, LX/4Zh;

    const-wide/16 v2, 0x0

    invoke-direct {v0, v2, v3, v4}, LX/4Zh;-><init>(JI)V

    iput-object v0, p0, LX/4Zi;->f:LX/4Zh;

    .line 790193
    sget-object v0, LX/4Zg;->BEFORE_START:LX/4Zg;

    iput-object v0, p0, LX/4Zi;->c:LX/4Zg;

    .line 790194
    return-void
.end method

.method public final a(JI)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 790179
    invoke-virtual {p0}, LX/4Zi;->d()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Already started"

    invoke-static {v0, v3}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 790180
    const/16 v0, 0x3e8

    if-gt p3, v0, :cond_1

    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "progressLimit is too high: "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 790181
    iput p3, p0, LX/4Zi;->d:I

    .line 790182
    const/4 v0, -0x1

    iput v0, p0, LX/4Zi;->e:I

    .line 790183
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/4Zi;->j:J

    .line 790184
    new-instance v0, LX/4Zh;

    invoke-direct {v0, p1, p2, v2}, LX/4Zh;-><init>(JI)V

    iput-object v0, p0, LX/4Zi;->f:LX/4Zh;

    .line 790185
    sget-object v0, LX/4Zg;->RUNNING:LX/4Zg;

    iput-object v0, p0, LX/4Zi;->c:LX/4Zg;

    .line 790186
    return-void

    :cond_0
    move v0, v2

    .line 790187
    goto :goto_0

    :cond_1
    move v1, v2

    .line 790188
    goto :goto_1
.end method

.method public final b(J)I
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/16 v6, 0x3e8

    .line 790169
    sget-object v1, LX/4Zf;->a:[I

    iget-object v2, p0, LX/4Zi;->c:LX/4Zg;

    invoke-virtual {v2}, LX/4Zg;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 790170
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown mode: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/4Zi;->c:LX/4Zg;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    move v0, v6

    .line 790171
    :cond_0
    :goto_0
    :pswitch_1
    return v0

    .line 790172
    :pswitch_2
    sget-object v3, LX/4Zi;->l:LX/4Zc;

    iget-wide v4, p0, LX/4Zi;->g:J

    move-object v0, p0

    move-wide v1, p1

    invoke-direct/range {v0 .. v5}, LX/4Zi;->a(JLX/4Zc;J)I

    move-result v0

    goto :goto_0

    .line 790173
    :pswitch_3
    iget v1, p0, LX/4Zi;->d:I

    if-ne v1, v6, :cond_1

    const/4 v0, 0x1

    :cond_1
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 790174
    sget-object v3, LX/4Zi;->m:LX/4Zc;

    sget-object v0, LX/4Zi;->a:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    move-object v0, p0

    move-wide v1, p1

    invoke-direct/range {v0 .. v5}, LX/4Zi;->a(JLX/4Zc;J)I

    move-result v0

    .line 790175
    if-ne v0, v6, :cond_0

    .line 790176
    sget-object v1, LX/4Zg;->ALMOST_DONE:LX/4Zg;

    iput-object v1, p0, LX/4Zi;->c:LX/4Zg;

    goto :goto_0

    .line 790177
    :pswitch_4
    sget-object v0, LX/4Zg;->DONE:LX/4Zg;

    iput-object v0, p0, LX/4Zi;->c:LX/4Zg;

    move v0, v6

    .line 790178
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 790168
    iget-object v0, p0, LX/4Zi;->c:LX/4Zg;

    sget-object v1, LX/4Zg;->RUNNING:LX/4Zg;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/4Zi;->c:LX/4Zg;

    sget-object v1, LX/4Zg;->FINISHING_UP:LX/4Zg;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/4Zi;->c:LX/4Zg;

    sget-object v1, LX/4Zg;->ALMOST_DONE:LX/4Zg;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(JI)V
    .locals 5

    .prologue
    .line 790155
    invoke-virtual {p0}, LX/4Zi;->d()Z

    move-result v0

    const-string v1, "Not running"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 790156
    const/16 v0, 0x3e8

    if-gt p3, v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "progressLimit is too high: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 790157
    iget v0, p0, LX/4Zi;->d:I

    if-ge p3, v0, :cond_2

    .line 790158
    sget-object v0, LX/4Zi;->b:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Progress limit decreased! old="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, LX/4Zi;->d:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", new="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 790159
    :cond_0
    :goto_1
    return-void

    .line 790160
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 790161
    :cond_2
    iget v0, p0, LX/4Zi;->d:I

    if-eq p3, v0, :cond_0

    .line 790162
    invoke-virtual {p0, p1, p2}, LX/4Zi;->b(J)I

    move-result v0

    .line 790163
    new-instance v1, LX/4Zh;

    invoke-direct {v1, p1, p2, v0}, LX/4Zh;-><init>(JI)V

    iput-object v1, p0, LX/4Zi;->f:LX/4Zh;

    .line 790164
    iput p3, p0, LX/4Zi;->d:I

    .line 790165
    iget-wide v0, p0, LX/4Zi;->j:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_3

    .line 790166
    iget-wide v0, p0, LX/4Zi;->j:J

    sub-long v0, p1, v0

    iget-wide v2, p0, LX/4Zi;->h:J

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    iget-wide v2, p0, LX/4Zi;->i:J

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iput-wide v0, p0, LX/4Zi;->g:J

    .line 790167
    :cond_3
    iput-wide p1, p0, LX/4Zi;->j:J

    goto :goto_1
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 790154
    iget-object v0, p0, LX/4Zi;->c:LX/4Zg;

    sget-object v1, LX/4Zg;->BEFORE_START:LX/4Zg;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 790153
    iget-object v0, p0, LX/4Zi;->c:LX/4Zg;

    sget-object v1, LX/4Zg;->RUNNING:LX/4Zg;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
