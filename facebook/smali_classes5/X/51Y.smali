.class public final LX/51Y;
.super LX/4x4;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<C::",
        "Ljava/lang/Comparable",
        "<*>;>",
        "LX/4x4",
        "<",
        "LX/4xM",
        "<TC;>;",
        "LX/50M",
        "<TC;>;>;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/NavigableMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/NavigableMap",
            "<",
            "LX/4xM",
            "<TC;>;",
            "LX/50M",
            "<TC;>;>;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/NavigableMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/NavigableMap",
            "<",
            "LX/4xM",
            "<TC;>;",
            "LX/50M",
            "<TC;>;>;"
        }
    .end annotation
.end field

.field public final c:LX/50M;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/50M",
            "<",
            "LX/4xM",
            "<TC;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/NavigableMap;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/NavigableMap",
            "<",
            "LX/4xM",
            "<TC;>;",
            "LX/50M",
            "<TC;>;>;)V"
        }
    .end annotation

    .prologue
    .line 825200
    sget-object v0, LX/50M;->d:LX/50M;

    move-object v0, v0

    .line 825201
    invoke-direct {p0, p1, v0}, LX/51Y;-><init>(Ljava/util/NavigableMap;LX/50M;)V

    .line 825202
    return-void
.end method

.method private constructor <init>(Ljava/util/NavigableMap;LX/50M;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/NavigableMap",
            "<",
            "LX/4xM",
            "<TC;>;",
            "LX/50M",
            "<TC;>;>;",
            "LX/50M",
            "<",
            "LX/4xM",
            "<TC;>;>;)V"
        }
    .end annotation

    .prologue
    .line 825256
    invoke-direct {p0}, LX/4x4;-><init>()V

    .line 825257
    iput-object p1, p0, LX/51Y;->a:Ljava/util/NavigableMap;

    .line 825258
    new-instance v0, LX/51b;

    invoke-direct {v0, p1}, LX/51b;-><init>(Ljava/util/NavigableMap;)V

    iput-object v0, p0, LX/51Y;->b:Ljava/util/NavigableMap;

    .line 825259
    iput-object p2, p0, LX/51Y;->c:LX/50M;

    .line 825260
    return-void
.end method

.method private a(Ljava/lang/Object;)LX/50M;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "LX/50M",
            "<TC;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 825248
    instance-of v0, p1, LX/4xM;

    if-eqz v0, :cond_0

    .line 825249
    :try_start_0
    check-cast p1, LX/4xM;

    .line 825250
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, LX/51Y;->b(LX/4xM;Z)Ljava/util/NavigableMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/NavigableMap;->firstEntry()Ljava/util/Map$Entry;

    move-result-object v2

    .line 825251
    if-eqz v2, :cond_0

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4xM;

    invoke-virtual {v0, p1}, LX/4xM;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 825252
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/50M;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 825253
    :goto_0
    return-object v0

    .line 825254
    :catch_0
    move-object v0, v1

    goto :goto_0

    :cond_0
    move-object v0, v1

    .line 825255
    goto :goto_0
.end method

.method private a(LX/50M;)Ljava/util/NavigableMap;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/50M",
            "<",
            "LX/4xM",
            "<TC;>;>;)",
            "Ljava/util/NavigableMap",
            "<",
            "LX/4xM",
            "<TC;>;",
            "LX/50M",
            "<TC;>;>;"
        }
    .end annotation

    .prologue
    .line 825243
    iget-object v0, p0, LX/51Y;->c:LX/50M;

    invoke-virtual {v0, p1}, LX/50M;->b(LX/50M;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 825244
    sget-object v0, LX/146;->b:LX/146;

    move-object v0, v0

    .line 825245
    :goto_0
    return-object v0

    .line 825246
    :cond_0
    iget-object v0, p0, LX/51Y;->c:LX/50M;

    invoke-virtual {p1, v0}, LX/50M;->c(LX/50M;)LX/50M;

    move-result-object v1

    .line 825247
    new-instance v0, LX/51Y;

    iget-object v2, p0, LX/51Y;->a:Ljava/util/NavigableMap;

    invoke-direct {v0, v2, v1}, LX/51Y;-><init>(Ljava/util/NavigableMap;LX/50M;)V

    goto :goto_0
.end method

.method private b(LX/4xM;Z)Ljava/util/NavigableMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4xM",
            "<TC;>;Z)",
            "Ljava/util/NavigableMap",
            "<",
            "LX/4xM",
            "<TC;>;",
            "LX/50M",
            "<TC;>;>;"
        }
    .end annotation

    .prologue
    .line 825242
    invoke-static {p2}, LX/4xG;->forBoolean(Z)LX/4xG;

    move-result-object v0

    invoke-static {p1, v0}, LX/50M;->b(Ljava/lang/Comparable;LX/4xG;)LX/50M;

    move-result-object v0

    invoke-direct {p0, v0}, LX/51Y;->a(LX/50M;)Ljava/util/NavigableMap;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/util/Iterator;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/util/Map$Entry",
            "<",
            "LX/4xM",
            "<TC;>;",
            "LX/50M",
            "<TC;>;>;>;"
        }
    .end annotation

    .prologue
    .line 825226
    iget-object v0, p0, LX/51Y;->c:LX/50M;

    invoke-virtual {v0}, LX/50M;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/51Y;->c:LX/50M;

    invoke-virtual {v0}, LX/50M;->g()Ljava/lang/Comparable;

    move-result-object v0

    check-cast v0, LX/4xM;

    .line 825227
    :goto_0
    iget-object v1, p0, LX/51Y;->c:LX/50M;

    invoke-virtual {v1}, LX/50M;->f()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/51Y;->c:LX/50M;

    .line 825228
    iget-object v2, v1, LX/50M;->upperBound:LX/4xM;

    invoke-virtual {v2}, LX/4xM;->b()LX/4xG;

    move-result-object v2

    move-object v1, v2

    .line 825229
    sget-object v2, LX/4xG;->CLOSED:LX/4xG;

    if-ne v1, v2, :cond_1

    const/4 v1, 0x1

    .line 825230
    :goto_1
    iget-object v2, p0, LX/51Y;->b:Ljava/util/NavigableMap;

    invoke-interface {v2, v0, v1}, Ljava/util/NavigableMap;->headMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/NavigableMap;->descendingMap()Ljava/util/NavigableMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/NavigableMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/0RZ;->i(Ljava/util/Iterator;)LX/4yu;

    move-result-object v2

    .line 825231
    invoke-interface {v2}, LX/4yu;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 825232
    invoke-interface {v2}, LX/4yu;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/50M;

    iget-object v0, v0, LX/50M;->upperBound:LX/4xM;

    sget-object v1, LX/4xN;->a:LX/4xN;

    if-ne v0, v1, :cond_2

    invoke-interface {v2}, LX/4yu;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/50M;

    iget-object v0, v0, LX/50M;->lowerBound:LX/4xM;

    .line 825233
    :goto_2
    sget-object v1, LX/4xN;->a:LX/4xN;

    invoke-static {v0, v1}, LX/0Qh;->firstNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4xM;

    .line 825234
    new-instance v1, LX/51X;

    invoke-direct {v1, p0, v0, v2}, LX/51X;-><init>(LX/51Y;LX/4xM;LX/4yu;)V

    move-object v0, v1

    :goto_3
    return-object v0

    .line 825235
    :cond_0
    sget-object v0, LX/4xN;->a:LX/4xN;

    goto :goto_0

    .line 825236
    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 825237
    :cond_2
    iget-object v1, p0, LX/51Y;->a:Ljava/util/NavigableMap;

    invoke-interface {v2}, LX/4yu;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/50M;

    iget-object v0, v0, LX/50M;->upperBound:LX/4xM;

    invoke-interface {v1, v0}, Ljava/util/NavigableMap;->higherKey(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4xM;

    goto :goto_2

    .line 825238
    :cond_3
    iget-object v0, p0, LX/51Y;->c:LX/50M;

    sget-object v1, LX/4xP;->a:LX/4xP;

    invoke-virtual {v0, v1}, LX/50M;->a(Ljava/lang/Comparable;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/51Y;->a:Ljava/util/NavigableMap;

    sget-object v1, LX/4xP;->a:LX/4xP;

    invoke-interface {v0, v1}, Ljava/util/NavigableMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 825239
    :cond_4
    sget-object v0, LX/0RZ;->a:LX/0Rb;

    move-object v0, v0

    .line 825240
    goto :goto_3

    .line 825241
    :cond_5
    iget-object v0, p0, LX/51Y;->a:Ljava/util/NavigableMap;

    sget-object v1, LX/4xP;->a:LX/4xP;

    invoke-interface {v0, v1}, Ljava/util/NavigableMap;->higherKey(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4xM;

    goto :goto_2
.end method

.method public final comparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<-",
            "LX/4xM",
            "<TC;>;>;"
        }
    .end annotation

    .prologue
    .line 825224
    sget-object v0, LX/1zb;->a:LX/1zb;

    move-object v0, v0

    .line 825225
    return-object v0
.end method

.method public final containsKey(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 825261
    invoke-direct {p0, p1}, LX/51Y;->a(Ljava/lang/Object;)LX/50M;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Ljava/util/Iterator;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/util/Map$Entry",
            "<",
            "LX/4xM",
            "<TC;>;",
            "LX/50M",
            "<TC;>;>;>;"
        }
    .end annotation

    .prologue
    .line 825210
    iget-object v0, p0, LX/51Y;->c:LX/50M;

    invoke-virtual {v0}, LX/50M;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 825211
    iget-object v1, p0, LX/51Y;->b:Ljava/util/NavigableMap;

    iget-object v0, p0, LX/51Y;->c:LX/50M;

    invoke-virtual {v0}, LX/50M;->d()Ljava/lang/Comparable;

    move-result-object v2

    iget-object v0, p0, LX/51Y;->c:LX/50M;

    .line 825212
    iget-object v3, v0, LX/50M;->lowerBound:LX/4xM;

    invoke-virtual {v3}, LX/4xM;->a()LX/4xG;

    move-result-object v3

    move-object v0, v3

    .line 825213
    sget-object v3, LX/4xG;->CLOSED:LX/4xG;

    if-ne v0, v3, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1, v2, v0}, Ljava/util/NavigableMap;->tailMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/NavigableMap;->values()Ljava/util/Collection;

    move-result-object v0

    .line 825214
    :goto_1
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/0RZ;->i(Ljava/util/Iterator;)LX/4yu;

    move-result-object v2

    .line 825215
    iget-object v0, p0, LX/51Y;->c:LX/50M;

    sget-object v1, LX/4xP;->a:LX/4xP;

    invoke-virtual {v0, v1}, LX/50M;->a(Ljava/lang/Comparable;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, LX/4yu;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, LX/4yu;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/50M;

    iget-object v0, v0, LX/50M;->lowerBound:LX/4xM;

    sget-object v1, LX/4xP;->a:LX/4xP;

    if-eq v0, v1, :cond_3

    .line 825216
    :cond_0
    sget-object v0, LX/4xP;->a:LX/4xP;

    .line 825217
    :goto_2
    new-instance v1, LX/51W;

    invoke-direct {v1, p0, v0, v2}, LX/51W;-><init>(LX/51Y;LX/4xM;LX/4yu;)V

    move-object v0, v1

    :goto_3
    return-object v0

    .line 825218
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 825219
    :cond_2
    iget-object v0, p0, LX/51Y;->b:Ljava/util/NavigableMap;

    invoke-interface {v0}, Ljava/util/NavigableMap;->values()Ljava/util/Collection;

    move-result-object v0

    goto :goto_1

    .line 825220
    :cond_3
    invoke-interface {v2}, LX/4yu;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 825221
    invoke-interface {v2}, LX/4yu;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/50M;

    iget-object v0, v0, LX/50M;->upperBound:LX/4xM;

    goto :goto_2

    .line 825222
    :cond_4
    sget-object v0, LX/0RZ;->a:LX/0Rb;

    move-object v0, v0

    .line 825223
    goto :goto_3
.end method

.method public final synthetic get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 825209
    invoke-direct {p0, p1}, LX/51Y;->a(Ljava/lang/Object;)LX/50M;

    move-result-object v0

    return-object v0
.end method

.method public final headMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;
    .locals 1

    .prologue
    .line 825207
    check-cast p1, LX/4xM;

    .line 825208
    invoke-static {p2}, LX/4xG;->forBoolean(Z)LX/4xG;

    move-result-object v0

    invoke-static {p1, v0}, LX/50M;->a(Ljava/lang/Comparable;LX/4xG;)LX/50M;

    move-result-object v0

    invoke-direct {p0, v0}, LX/51Y;->a(LX/50M;)Ljava/util/NavigableMap;

    move-result-object v0

    return-object v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 825206
    invoke-virtual {p0}, LX/51Y;->d()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/0RZ;->b(Ljava/util/Iterator;)I

    move-result v0

    return v0
.end method

.method public final subMap(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableMap;
    .locals 2

    .prologue
    .line 825204
    check-cast p1, LX/4xM;

    check-cast p3, LX/4xM;

    .line 825205
    invoke-static {p2}, LX/4xG;->forBoolean(Z)LX/4xG;

    move-result-object v0

    invoke-static {p4}, LX/4xG;->forBoolean(Z)LX/4xG;

    move-result-object v1

    invoke-static {p1, v0, p3, v1}, LX/50M;->a(Ljava/lang/Comparable;LX/4xG;Ljava/lang/Comparable;LX/4xG;)LX/50M;

    move-result-object v0

    invoke-direct {p0, v0}, LX/51Y;->a(LX/50M;)Ljava/util/NavigableMap;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic tailMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;
    .locals 1

    .prologue
    .line 825203
    check-cast p1, LX/4xM;

    invoke-direct {p0, p1, p2}, LX/51Y;->b(LX/4xM;Z)Ljava/util/NavigableMap;

    move-result-object v0

    return-object v0
.end method
