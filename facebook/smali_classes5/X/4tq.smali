.class public final LX/4tq;
.super LX/38J;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/38J",
        "<",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 819606
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_6

    .line 819607
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 819608
    :goto_0
    return v1

    .line 819609
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 819610
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_5

    .line 819611
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 819612
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 819613
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_1

    if-eqz v4, :cond_1

    .line 819614
    const-string v5, "description"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 819615
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 819616
    :cond_2
    const-string v5, "name"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 819617
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 819618
    :cond_3
    const-string v5, "parameters"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 819619
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 819620
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->START_ARRAY:LX/15z;

    if-ne v4, v5, :cond_4

    .line 819621
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_ARRAY:LX/15z;

    if-eq v4, v5, :cond_4

    .line 819622
    invoke-static {p0, p1}, LX/38I;->b(LX/15w;LX/186;)I

    move-result v4

    .line 819623
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 819624
    :cond_4
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    move v0, v0

    .line 819625
    goto :goto_1

    .line 819626
    :cond_5
    const/4 v4, 0x3

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 819627
    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 819628
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 819629
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 819630
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_6
    move v0, v1

    move v2, v1

    move v3, v1

    goto/16 :goto_1
.end method

.method public static a(Landroid/content/Context;)LX/0ew;
    .locals 2

    .prologue
    .line 819702
    const-class v0, LX/0ew;

    invoke-static {p0, v0}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ew;

    .line 819703
    if-nez v0, :cond_0

    .line 819704
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Context has no associated FragmentManagerHost"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 819705
    :cond_0
    return-object v0
.end method

.method public static a(LX/0QB;Ljava/lang/Class;)Landroid/content/ComponentName;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QB;",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)",
            "Landroid/content/ComponentName;"
        }
    .end annotation

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    invoke-static {v0}, LX/0QA;->a(Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :sswitch_0
    const-string v1, "com.facebook.base.activity.ReactFragmentActivity"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/2lC;->b(LX/0QB;)Landroid/content/ComponentName;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    goto :goto_0

    :sswitch_1
    const-string v1, "com.facebook.katana.login.LogoutActivityComponent"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/2lQ;->b(LX/0QB;)Landroid/content/ComponentName;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    goto :goto_0

    :sswitch_2
    const-string v1, "com.facebook.base.activity.FragmentChromeActivity"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/2lB;->b(LX/0QB;)Landroid/content/ComponentName;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    goto :goto_0

    :sswitch_3
    const-string v1, "com.facebook.katana.login.LoginActivityComponent"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/27F;->b(LX/0QB;)Landroid/content/ComponentName;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    goto :goto_0

    :sswitch_4
    const-string v1, "com.facebook.bugreporter.annotations.InternalSettingsActivity"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/38K;->b(LX/0QB;)Landroid/content/ComponentName;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    goto :goto_0

    :sswitch_5
    const-string v1, "com.facebook.base.activity.FragmentBaseActivity"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/2dd;->a(LX/0QB;)Landroid/content/ComponentName;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    goto :goto_0

    :sswitch_6
    const-string v1, "com.facebook.katana.login.WorkOnboardingFlowComponent"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/38L;->b(LX/0QB;)Landroid/content/ComponentName;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    goto :goto_0

    :sswitch_7
    const-string v1, "com.facebook.messaging.annotations.ForSecureIntentHandlerActivity"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/38M;->b(LX/0QB;)Landroid/content/ComponentName;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x6a6735bf -> :sswitch_0
        -0x5b773d9e -> :sswitch_1
        -0x51160aaa -> :sswitch_2
        -0x2c7c0999 -> :sswitch_3
        -0x11102833 -> :sswitch_4
        -0x2519613 -> :sswitch_5
        0x78c7e3e1 -> :sswitch_6
        0x7e2f804c -> :sswitch_7
    .end sparse-switch
.end method

.method public static a(Ljava/lang/Integer;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 819701
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-static {p0}, LX/4tq;->b(Ljava/lang/Integer;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 2

    .prologue
    .line 819654
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 819655
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 819656
    if-eqz v0, :cond_0

    .line 819657
    const-string v1, "is_add_to_home_screen_enabled"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 819658
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 819659
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 819660
    if-eqz v0, :cond_1

    .line 819661
    const-string v1, "is_autofill_settings_enabled"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 819662
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 819663
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 819664
    if-eqz v0, :cond_2

    .line 819665
    const-string v1, "is_copy_link_enabled"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 819666
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 819667
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 819668
    if-eqz v0, :cond_3

    .line 819669
    const-string v1, "is_log_out_enabled"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 819670
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 819671
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 819672
    if-eqz v0, :cond_4

    .line 819673
    const-string v1, "is_manage_permissions_enabled"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 819674
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 819675
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 819676
    if-eqz v0, :cond_5

    .line 819677
    const-string v1, "is_overflow_menu_enabled"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 819678
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 819679
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 819680
    if-eqz v0, :cond_6

    .line 819681
    const-string v1, "is_payment_enabled"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 819682
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 819683
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 819684
    if-eqz v0, :cond_7

    .line 819685
    const-string v1, "is_product_history_enabled"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 819686
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 819687
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 819688
    if-eqz v0, :cond_8

    .line 819689
    const-string v1, "is_save_autofill_data_banner_enabled"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 819690
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 819691
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 819692
    if-eqz v0, :cond_9

    .line 819693
    const-string v1, "is_scrollable_autofill_bar_enabled"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 819694
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 819695
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 819696
    if-eqz v0, :cond_a

    .line 819697
    const-string v1, "is_webview_chrome_enabled"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 819698
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 819699
    :cond_a
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 819700
    return-void
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 819635
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 819636
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 819637
    if-eqz v0, :cond_0

    .line 819638
    const-string v1, "description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 819639
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 819640
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 819641
    if-eqz v0, :cond_1

    .line 819642
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 819643
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 819644
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 819645
    if-eqz v0, :cond_3

    .line 819646
    const-string v1, "parameters"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 819647
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 819648
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result p1

    if-ge v1, p1, :cond_2

    .line 819649
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result p1

    invoke-static {p0, p1, p2}, LX/38I;->a(LX/15i;ILX/0nX;)V

    .line 819650
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 819651
    :cond_2
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 819652
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 819653
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 819633
    check-cast p0, Landroid/media/MediaDescription$Builder;

    invoke-virtual {p0, p1}, Landroid/media/MediaDescription$Builder;->setIconBitmap(Landroid/graphics/Bitmap;)Landroid/media/MediaDescription$Builder;

    .line 819634
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 819631
    check-cast p0, Landroid/media/MediaDescription$Builder;

    invoke-virtual {p0, p1}, Landroid/media/MediaDescription$Builder;->setIconUri(Landroid/net/Uri;)Landroid/media/MediaDescription$Builder;

    .line 819632
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 819604
    check-cast p0, Landroid/media/MediaDescription$Builder;

    invoke-virtual {p0, p1}, Landroid/media/MediaDescription$Builder;->setExtras(Landroid/os/Bundle;)Landroid/media/MediaDescription$Builder;

    .line 819605
    return-void
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 819706
    check-cast p0, Landroid/media/MediaDescription$Builder;

    invoke-virtual {p0, p1}, Landroid/media/MediaDescription$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/media/MediaDescription$Builder;

    .line 819707
    return-void
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 819602
    check-cast p0, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {p0, p1}, Landroid/media/MediaRouter$RouteInfo;->setTag(Ljava/lang/Object;)V

    .line 819603
    return-void
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 819573
    check-cast p0, Landroid/media/MediaDescription$Builder;

    invoke-virtual {p0, p1}, Landroid/media/MediaDescription$Builder;->setMediaId(Ljava/lang/String;)Landroid/media/MediaDescription$Builder;

    .line 819574
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 819589
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_3

    .line 819590
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 819591
    :goto_0
    return v1

    .line 819592
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 819593
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v3, :cond_2

    .line 819594
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 819595
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 819596
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v3, v4, :cond_1

    if-eqz v2, :cond_1

    .line 819597
    const-string v3, "node"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 819598
    invoke-static {p0, p1}, LX/38I;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 819599
    :cond_2
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 819600
    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 819601
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public static b(Ljava/lang/Object;)Landroid/view/Display;
    .locals 1

    .prologue
    .line 819588
    check-cast p0, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {p0}, Landroid/media/MediaRouter$RouteInfo;->getPresentationDisplay()Landroid/view/Display;

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/lang/Integer;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 819587
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :pswitch_0
    const-string v0, "DEFAULT"

    :goto_0
    return-object v0

    :pswitch_1
    const-string v0, "FORCE_OLD"

    goto :goto_0

    :pswitch_2
    const-string v0, "FORCE_NEW"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 819580
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 819581
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 819582
    if-eqz v0, :cond_0

    .line 819583
    const-string v1, "node"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 819584
    invoke-static {p0, v0, p2, p3}, LX/38I;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 819585
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 819586
    return-void
.end method

.method public static b(Ljava/lang/Object;Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 819578
    check-cast p0, Landroid/media/MediaDescription$Builder;

    invoke-virtual {p0, p1}, Landroid/media/MediaDescription$Builder;->setSubtitle(Ljava/lang/CharSequence;)Landroid/media/MediaDescription$Builder;

    .line 819579
    return-void
.end method

.method public static c(Ljava/lang/Object;Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 819576
    check-cast p0, Landroid/media/MediaDescription$Builder;

    invoke-virtual {p0, p1}, Landroid/media/MediaDescription$Builder;->setDescription(Ljava/lang/CharSequence;)Landroid/media/MediaDescription$Builder;

    .line 819577
    return-void
.end method

.method public static d(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 819575
    check-cast p0, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {p0}, Landroid/media/MediaRouter$RouteInfo;->getVolume()I

    move-result v0

    return v0
.end method
