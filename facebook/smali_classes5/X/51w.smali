.class public final LX/51w;
.super LX/51l;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private final bytes:I

.field private final prototype:Ljava/security/MessageDigest;

.field private final supportsClone:Z

.field private final toString:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 825556
    invoke-direct {p0}, LX/51l;-><init>()V

    .line 825557
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/51w;->toString:Ljava/lang/String;

    .line 825558
    invoke-static {p1}, LX/51w;->a(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v0

    iput-object v0, p0, LX/51w;->prototype:Ljava/security/MessageDigest;

    .line 825559
    iget-object v0, p0, LX/51w;->prototype:Ljava/security/MessageDigest;

    invoke-virtual {v0}, Ljava/security/MessageDigest;->getDigestLength()I

    move-result v3

    .line 825560
    const/4 v0, 0x4

    if-lt p2, v0, :cond_0

    if-gt p2, v3, :cond_0

    move v0, v1

    :goto_0
    const-string v4, "bytes (%s) must be >= 4 and < %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v1

    invoke-static {v0, v4, v5}, LX/0PB;->checkArgument(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 825561
    iput p2, p0, LX/51w;->bytes:I

    .line 825562
    invoke-direct {p0}, LX/51w;->b()Z

    move-result v0

    iput-boolean v0, p0, LX/51w;->supportsClone:Z

    .line 825563
    return-void

    :cond_0
    move v0, v2

    .line 825564
    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 825542
    invoke-direct {p0}, LX/51l;-><init>()V

    .line 825543
    invoke-static {p1}, LX/51w;->a(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v0

    iput-object v0, p0, LX/51w;->prototype:Ljava/security/MessageDigest;

    .line 825544
    iget-object v0, p0, LX/51w;->prototype:Ljava/security/MessageDigest;

    invoke-virtual {v0}, Ljava/security/MessageDigest;->getDigestLength()I

    move-result v0

    iput v0, p0, LX/51w;->bytes:I

    .line 825545
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/51w;->toString:Ljava/lang/String;

    .line 825546
    invoke-direct {p0}, LX/51w;->b()Z

    move-result v0

    iput-boolean v0, p0, LX/51w;->supportsClone:Z

    .line 825547
    return-void
.end method

.method private static a(Ljava/lang/String;)Ljava/security/MessageDigest;
    .locals 2

    .prologue
    .line 825553
    :try_start_0
    invoke-static {p0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 825554
    :catch_0
    move-exception v0

    .line 825555
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method private b()Z
    .locals 1

    .prologue
    .line 825565
    :try_start_0
    iget-object v0, p0, LX/51w;->prototype:Ljava/security/MessageDigest;

    invoke-virtual {v0}, Ljava/security/MessageDigest;->clone()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 825566
    const/4 v0, 0x1

    .line 825567
    :goto_0
    return v0

    :catch_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/51h;
    .locals 5

    .prologue
    .line 825550
    iget-boolean v0, p0, LX/51w;->supportsClone:Z

    if-eqz v0, :cond_0

    .line 825551
    :try_start_0
    new-instance v1, LX/51u;

    iget-object v0, p0, LX/51w;->prototype:Ljava/security/MessageDigest;

    invoke-virtual {v0}, Ljava/security/MessageDigest;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/security/MessageDigest;

    iget v2, p0, LX/51w;->bytes:I

    invoke-direct {v1, v0, v2}, LX/51u;-><init>(Ljava/security/MessageDigest;I)V
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    .line 825552
    :goto_0
    return-object v0

    :catch_0
    :cond_0
    new-instance v0, LX/51u;

    iget-object v1, p0, LX/51w;->prototype:Ljava/security/MessageDigest;

    invoke-virtual {v1}, Ljava/security/MessageDigest;->getAlgorithm()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/51w;->a(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v1

    iget v2, p0, LX/51w;->bytes:I

    invoke-direct {v0, v1, v2}, LX/51u;-><init>(Ljava/security/MessageDigest;I)V

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 825549
    iget-object v0, p0, LX/51w;->toString:Ljava/lang/String;

    return-object v0
.end method

.method public final writeReplace()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 825548
    new-instance v0, LX/51v;

    iget-object v1, p0, LX/51w;->prototype:Ljava/security/MessageDigest;

    invoke-virtual {v1}, Ljava/security/MessageDigest;->getAlgorithm()Ljava/lang/String;

    move-result-object v1

    iget v2, p0, LX/51w;->bytes:I

    iget-object v3, p0, LX/51w;->toString:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, LX/51v;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    return-object v0
.end method
