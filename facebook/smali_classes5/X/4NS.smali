.class public LX/4NS;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 692784
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const-wide/16 v4, 0x0

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 692785
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_8

    .line 692786
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 692787
    :goto_0
    return v1

    .line 692788
    :cond_0
    const-string v12, "fetchTimeMs"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 692789
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    move v0, v6

    .line 692790
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_6

    .line 692791
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 692792
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 692793
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_1

    if-eqz v11, :cond_1

    .line 692794
    const-string v12, "anniversary_campaign"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 692795
    invoke-static {p0, p1}, LX/4NL;->a(LX/15w;LX/186;)I

    move-result v10

    goto :goto_1

    .line 692796
    :cond_2
    const-string v12, "color_palette"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 692797
    invoke-static {p0, p1}, LX/4Nd;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 692798
    :cond_3
    const-string v12, "subtitle"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 692799
    invoke-static {p0, p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 692800
    :cond_4
    const-string v12, "title"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 692801
    invoke-static {p0, p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 692802
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 692803
    :cond_6
    const/4 v11, 0x5

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 692804
    invoke-virtual {p1, v1, v10}, LX/186;->b(II)V

    .line 692805
    invoke-virtual {p1, v6, v9}, LX/186;->b(II)V

    .line 692806
    if-eqz v0, :cond_7

    .line 692807
    const/4 v1, 0x2

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 692808
    :cond_7
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 692809
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 692810
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_8
    move v0, v1

    move v7, v1

    move v8, v1

    move-wide v2, v4

    move v9, v1

    move v10, v1

    goto :goto_1
.end method

.method public static a(LX/15w;S)LX/15i;
    .locals 5

    .prologue
    .line 692811
    const/4 v0, 0x1

    const/4 v4, 0x0

    .line 692812
    new-instance v2, LX/186;

    const/16 v1, 0x80

    invoke-direct {v2, v1}, LX/186;-><init>(I)V

    .line 692813
    invoke-static {p0, v2}, LX/4NS;->a(LX/15w;LX/186;)I

    move-result v1

    .line 692814
    if-eqz v0, :cond_0

    .line 692815
    const/4 v3, 0x2

    invoke-virtual {v2, v3}, LX/186;->c(I)V

    .line 692816
    invoke-virtual {v2, v4, p1, v4}, LX/186;->a(ISI)V

    .line 692817
    const/4 v3, 0x1

    invoke-virtual {v2, v3, v1}, LX/186;->b(II)V

    .line 692818
    invoke-virtual {v2}, LX/186;->d()I

    move-result v1

    .line 692819
    :cond_0
    invoke-virtual {v2, v1}, LX/186;->d(I)V

    .line 692820
    invoke-static {v2}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v1

    move-object v0, v1

    .line 692821
    return-object v0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 692822
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 692823
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 692824
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 692825
    const-string v0, "name"

    const-string v1, "GoodwillThrowbackAnniversaryCampaignStory"

    invoke-virtual {p2, v0, v1}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 692826
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 692827
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 692828
    if-eqz v0, :cond_0

    .line 692829
    const-string v1, "anniversary_campaign"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 692830
    invoke-static {p0, v0, p2, p3}, LX/4NL;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 692831
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 692832
    if-eqz v0, :cond_1

    .line 692833
    const-string v1, "color_palette"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 692834
    invoke-static {p0, v0, p2}, LX/4Nd;->a(LX/15i;ILX/0nX;)V

    .line 692835
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 692836
    cmp-long v2, v0, v2

    if-eqz v2, :cond_2

    .line 692837
    const-string v2, "fetchTimeMs"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 692838
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 692839
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 692840
    if-eqz v0, :cond_3

    .line 692841
    const-string v1, "subtitle"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 692842
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 692843
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 692844
    if-eqz v0, :cond_4

    .line 692845
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 692846
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 692847
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 692848
    return-void
.end method
