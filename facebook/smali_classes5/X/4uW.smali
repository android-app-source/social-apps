.class public final LX/4uW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2wn;


# instance fields
.field private final a:Landroid/content/Context;

.field public final b:LX/2wW;

.field private final c:Landroid/os/Looper;

.field public final d:LX/2wm;

.field public final e:LX/2wm;

.field private final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/2vo",
            "<*>;",
            "LX/2wm;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/4uz;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/2wJ;

.field public i:Landroid/os/Bundle;

.field public j:Lcom/google/android/gms/common/ConnectionResult;

.field public k:Lcom/google/android/gms/common/ConnectionResult;

.field public l:Z

.field public final m:Ljava/util/concurrent/locks/Lock;

.field public n:I


# direct methods
.method private constructor <init>(Landroid/content/Context;LX/2wW;Ljava/util/concurrent/locks/Lock;Landroid/os/Looper;LX/1od;Ljava/util/Map;Ljava/util/Map;LX/2wA;LX/2vq;LX/2wJ;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/Map;Ljava/util/Map;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/2wW;",
            "Ljava/util/concurrent/locks/Lock;",
            "Landroid/os/Looper;",
            "LX/1od;",
            "Ljava/util/Map",
            "<",
            "LX/2vo",
            "<*>;",
            "LX/2wJ;",
            ">;",
            "Ljava/util/Map",
            "<",
            "LX/2vo",
            "<*>;",
            "LX/2wJ;",
            ">;",
            "LX/2wA;",
            "LX/2vq",
            "<+",
            "LX/3KI;",
            "LX/2w4;",
            ">;",
            "LX/2wJ;",
            "Ljava/util/ArrayList",
            "<",
            "LX/2wE;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "LX/2wE;",
            ">;",
            "Ljava/util/Map",
            "<",
            "LX/2vs",
            "<*>;",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/Map",
            "<",
            "LX/2vs",
            "<*>;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v1, Ljava/util/WeakHashMap;

    invoke-direct {v1}, Ljava/util/WeakHashMap;-><init>()V

    invoke-static {v1}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v1

    iput-object v1, p0, LX/4uW;->g:Ljava/util/Set;

    const/4 v1, 0x0

    iput-object v1, p0, LX/4uW;->j:Lcom/google/android/gms/common/ConnectionResult;

    const/4 v1, 0x0

    iput-object v1, p0, LX/4uW;->k:Lcom/google/android/gms/common/ConnectionResult;

    const/4 v1, 0x0

    iput-boolean v1, p0, LX/4uW;->l:Z

    const/4 v1, 0x0

    iput v1, p0, LX/4uW;->n:I

    iput-object p1, p0, LX/4uW;->a:Landroid/content/Context;

    iput-object p2, p0, LX/4uW;->b:LX/2wW;

    move-object/from16 v0, p3

    iput-object v0, p0, LX/4uW;->m:Ljava/util/concurrent/locks/Lock;

    move-object/from16 v0, p4

    iput-object v0, p0, LX/4uW;->c:Landroid/os/Looper;

    move-object/from16 v0, p10

    iput-object v0, p0, LX/4uW;->h:LX/2wJ;

    new-instance v1, LX/2wm;

    iget-object v3, p0, LX/4uW;->b:LX/2wW;

    const/4 v8, 0x0

    const/4 v10, 0x0

    new-instance v12, LX/4uU;

    invoke-direct {v12, p0}, LX/4uU;-><init>(LX/4uW;)V

    move-object v2, p1

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p7

    move-object/from16 v9, p14

    move-object/from16 v11, p12

    invoke-direct/range {v1 .. v12}, LX/2wm;-><init>(Landroid/content/Context;LX/2wW;Ljava/util/concurrent/locks/Lock;Landroid/os/Looper;LX/1od;Ljava/util/Map;LX/2wA;Ljava/util/Map;LX/2vq;Ljava/util/ArrayList;LX/2wY;)V

    iput-object v1, p0, LX/4uW;->d:LX/2wm;

    new-instance v1, LX/2wm;

    iget-object v3, p0, LX/4uW;->b:LX/2wW;

    new-instance v12, LX/4uV;

    invoke-direct {v12, p0}, LX/4uV;-><init>(LX/4uW;)V

    move-object v2, p1

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p8

    move-object/from16 v9, p13

    move-object/from16 v10, p9

    move-object/from16 v11, p11

    invoke-direct/range {v1 .. v12}, LX/2wm;-><init>(Landroid/content/Context;LX/2wW;Ljava/util/concurrent/locks/Lock;Landroid/os/Looper;LX/1od;Ljava/util/Map;LX/2wA;Ljava/util/Map;LX/2vq;Ljava/util/ArrayList;LX/2wY;)V

    iput-object v1, p0, LX/4uW;->e:LX/2wm;

    new-instance v2, LX/026;

    invoke-direct {v2}, LX/026;-><init>()V

    invoke-interface/range {p7 .. p7}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2vo;

    iget-object v4, p0, LX/4uW;->d:LX/2wm;

    invoke-virtual {v2, v1, v4}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    invoke-interface/range {p6 .. p6}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2vo;

    iget-object v4, p0, LX/4uW;->e:LX/2wm;

    invoke-virtual {v2, v1, v4}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_1
    invoke-static {v2}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    iput-object v1, p0, LX/4uW;->f:Ljava/util/Map;

    return-void
.end method

.method public static a(Landroid/content/Context;LX/2wW;Ljava/util/concurrent/locks/Lock;Landroid/os/Looper;LX/1od;Ljava/util/Map;LX/2wA;Ljava/util/Map;LX/2vq;Ljava/util/ArrayList;)LX/4uW;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/2wW;",
            "Ljava/util/concurrent/locks/Lock;",
            "Landroid/os/Looper;",
            "LX/1od;",
            "Ljava/util/Map",
            "<",
            "LX/2vo",
            "<*>;",
            "LX/2wJ;",
            ">;",
            "LX/2wA;",
            "Ljava/util/Map",
            "<",
            "LX/2vs",
            "<*>;",
            "Ljava/lang/Integer;",
            ">;",
            "LX/2vq",
            "<+",
            "LX/3KI;",
            "LX/2w4;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "LX/2wE;",
            ">;)",
            "LX/4uW;"
        }
    .end annotation

    const/4 v11, 0x0

    new-instance v7, LX/026;

    invoke-direct {v7}, LX/026;-><init>()V

    new-instance v8, LX/026;

    invoke-direct {v8}, LX/026;-><init>()V

    invoke-interface/range {p5 .. p5}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2wJ;

    invoke-interface {v2}, LX/2wJ;->p()Z

    move-result v4

    if-eqz v4, :cond_0

    move-object v11, v2

    :cond_0
    invoke-interface {v2}, LX/2wJ;->n()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2vo;

    invoke-interface {v7, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2vo;

    invoke-interface {v8, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    invoke-interface {v7}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    const/4 v1, 0x1

    :goto_1
    const-string v2, "CompositeGoogleApiClient should not be used without any APIs that require sign-in."

    invoke-static {v1, v2}, LX/1ol;->a(ZLjava/lang/Object;)V

    new-instance v14, LX/026;

    invoke-direct {v14}, LX/026;-><init>()V

    new-instance v15, LX/026;

    invoke-direct {v15}, LX/026;-><init>()V

    invoke-interface/range {p7 .. p7}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2vs;

    invoke-virtual {v1}, LX/2vs;->d()LX/2vo;

    move-result-object v2

    invoke-interface {v7, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    move-object/from16 v0, p7

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-interface {v14, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_3
    const/4 v1, 0x0

    goto :goto_1

    :cond_4
    invoke-interface {v8, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    move-object/from16 v0, p7

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-interface {v15, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_5
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Each API in the apiTypeMap must have a corresponding client in the clients map."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_6
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual/range {p9 .. p9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2wE;

    iget-object v3, v1, LX/2wE;->a:LX/2vs;

    invoke-interface {v14, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-virtual {v12, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_7
    iget-object v3, v1, LX/2wE;->a:LX/2vs;

    invoke-interface {v15, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-virtual {v13, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_8
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Each ClientCallbacks must have a corresponding API in the apiTypeMap"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_9
    new-instance v1, LX/4uW;

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v9, p6

    move-object/from16 v10, p8

    invoke-direct/range {v1 .. v15}, LX/4uW;-><init>(Landroid/content/Context;LX/2wW;Ljava/util/concurrent/locks/Lock;Landroid/os/Looper;LX/1od;Ljava/util/Map;Ljava/util/Map;LX/2wA;LX/2vq;LX/2wJ;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/Map;Ljava/util/Map;)V

    return-object v1
.end method

.method private a(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 3

    iget v0, p0, LX/4uW;->n:I

    packed-switch v0, :pswitch_data_0

    const-string v0, "CompositeGAC"

    const-string v1, "Attempted to call failure callbacks in CONNECTION_MODE_NONE. Callbacks should be disabled via GmsClientSupervisor"

    new-instance v2, Ljava/lang/Exception;

    invoke-direct {v2}, Ljava/lang/Exception;-><init>()V

    invoke-static {v0, v1, v2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_0
    const/4 v0, 0x0

    iput v0, p0, LX/4uW;->n:I

    return-void

    :pswitch_0
    iget-object v0, p0, LX/4uW;->b:LX/2wW;

    invoke-virtual {v0, p1}, LX/2wW;->a(Lcom/google/android/gms/common/ConnectionResult;)V

    :pswitch_1
    invoke-static {p0}, LX/4uW;->l(LX/4uW;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static a$redex0(LX/4uW;IZ)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, LX/4uW;->b:LX/2wW;

    invoke-virtual {v0, p1, p2}, LX/2wW;->a(IZ)V

    iput-object v1, p0, LX/4uW;->k:Lcom/google/android/gms/common/ConnectionResult;

    iput-object v1, p0, LX/4uW;->j:Lcom/google/android/gms/common/ConnectionResult;

    return-void
.end method

.method private static b(Lcom/google/android/gms/common/ConnectionResult;)Z
    .locals 1

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/common/ConnectionResult;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(LX/2we;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2we",
            "<+",
            "LX/2NW;",
            "+",
            "LX/2wK;",
            ">;)Z"
        }
    .end annotation

    iget-object v0, p1, LX/2we;->d:LX/2vo;

    move-object v0, v0

    iget-object v1, p0, LX/4uW;->f:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    const-string v2, "GoogleApiClient is not configured to use the API required for this call."

    invoke-static {v1, v2}, LX/1ol;->b(ZLjava/lang/Object;)V

    iget-object v1, p0, LX/4uW;->f:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2wm;

    iget-object v1, p0, LX/4uW;->e:LX/2wm;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private h()Z
    .locals 1

    iget-object v0, p0, LX/4uW;->e:LX/2wm;

    invoke-virtual {v0}, LX/2wm;->d()Z

    move-result v0

    return v0
.end method

.method public static j(LX/4uW;)V
    .locals 3

    iget-object v0, p0, LX/4uW;->j:Lcom/google/android/gms/common/ConnectionResult;

    invoke-static {v0}, LX/4uW;->b(Lcom/google/android/gms/common/ConnectionResult;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/4uW;->k:Lcom/google/android/gms/common/ConnectionResult;

    invoke-static {v0}, LX/4uW;->b(Lcom/google/android/gms/common/ConnectionResult;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, LX/4uW;->m()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    iget v0, p0, LX/4uW;->n:I

    packed-switch v0, :pswitch_data_0

    const-string v0, "CompositeGAC"

    const-string v1, "Attempted to call success callbacks in CONNECTION_MODE_NONE. Callbacks should be disabled via GmsClientSupervisor"

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    invoke-static {v0, v1, v2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_0
    const/4 v0, 0x0

    iput v0, p0, LX/4uW;->n:I

    :cond_1
    :goto_1
    return-void

    :cond_2
    iget-object v0, p0, LX/4uW;->k:Lcom/google/android/gms/common/ConnectionResult;

    if-eqz v0, :cond_1

    iget v0, p0, LX/4uW;->n:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    invoke-static {p0}, LX/4uW;->l(LX/4uW;)V

    goto :goto_1

    :cond_3
    iget-object v0, p0, LX/4uW;->k:Lcom/google/android/gms/common/ConnectionResult;

    invoke-direct {p0, v0}, LX/4uW;->a(Lcom/google/android/gms/common/ConnectionResult;)V

    iget-object v0, p0, LX/4uW;->d:LX/2wm;

    invoke-virtual {v0}, LX/2wm;->c()V

    goto :goto_1

    :cond_4
    iget-object v0, p0, LX/4uW;->j:Lcom/google/android/gms/common/ConnectionResult;

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/4uW;->k:Lcom/google/android/gms/common/ConnectionResult;

    invoke-static {v0}, LX/4uW;->b(Lcom/google/android/gms/common/ConnectionResult;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/4uW;->e:LX/2wm;

    invoke-virtual {v0}, LX/2wm;->c()V

    iget-object v0, p0, LX/4uW;->j:Lcom/google/android/gms/common/ConnectionResult;

    invoke-direct {p0, v0}, LX/4uW;->a(Lcom/google/android/gms/common/ConnectionResult;)V

    goto :goto_1

    :cond_5
    iget-object v0, p0, LX/4uW;->j:Lcom/google/android/gms/common/ConnectionResult;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/4uW;->k:Lcom/google/android/gms/common/ConnectionResult;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/4uW;->j:Lcom/google/android/gms/common/ConnectionResult;

    iget-object v1, p0, LX/4uW;->e:LX/2wm;

    iget v1, v1, LX/2wm;->f:I

    iget-object v2, p0, LX/4uW;->d:LX/2wm;

    iget v2, v2, LX/2wm;->f:I

    if-ge v1, v2, :cond_6

    iget-object v0, p0, LX/4uW;->k:Lcom/google/android/gms/common/ConnectionResult;

    :cond_6
    invoke-direct {p0, v0}, LX/4uW;->a(Lcom/google/android/gms/common/ConnectionResult;)V

    goto :goto_1

    :pswitch_0
    iget-object v0, p0, LX/4uW;->b:LX/2wW;

    iget-object v1, p0, LX/4uW;->i:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, LX/2wW;->a(Landroid/os/Bundle;)V

    :pswitch_1
    invoke-static {p0}, LX/4uW;->l(LX/4uW;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static l(LX/4uW;)V
    .locals 2

    iget-object v0, p0, LX/4uW;->g:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4uz;

    invoke-interface {v0}, LX/4uz;->kW_()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, LX/4uW;->g:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    return-void
.end method

.method private m()Z
    .locals 2

    iget-object v0, p0, LX/4uW;->k:Lcom/google/android/gms/common/ConnectionResult;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/4uW;->k:Lcom/google/android/gms/common/ConnectionResult;

    iget v1, v0, Lcom/google/android/gms/common/ConnectionResult;->c:I

    move v0, v1

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private n()Landroid/app/PendingIntent;
    .locals 4
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    iget-object v0, p0, LX/4uW;->h:LX/2wJ;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/4uW;->a:Landroid/content/Context;

    iget-object v1, p0, LX/4uW;->b:LX/2wW;

    invoke-virtual {v1}, LX/2wW;->p()I

    move-result v1

    iget-object v2, p0, LX/4uW;->h:LX/2wJ;

    invoke-interface {v2}, LX/2wJ;->q()Landroid/content/Intent;

    move-result-object v2

    const/high16 v3, 0x8000000

    invoke-static {v0, v1, v2, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/2we;)LX/2we;
    .locals 4
    .param p1    # LX/2we;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A::",
            "LX/2wK;",
            "R::",
            "LX/2NW;",
            "T:",
            "LX/2we",
            "<TR;TA;>;>(TT;)TT;"
        }
    .end annotation

    invoke-direct {p0, p1}, LX/4uW;->c(LX/2we;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, LX/4uW;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/4 v1, 0x4

    const/4 v2, 0x0

    invoke-direct {p0}, LX/4uW;->n()Landroid/app/PendingIntent;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    invoke-virtual {p1, v0}, LX/2we;->b(Lcom/google/android/gms/common/api/Status;)V

    :goto_0
    return-object p1

    :cond_0
    iget-object v0, p0, LX/4uW;->e:LX/2wm;

    invoke-virtual {v0, p1}, LX/2wm;->a(LX/2we;)LX/2we;

    move-result-object p1

    goto :goto_0

    :cond_1
    iget-object v0, p0, LX/4uW;->d:LX/2wm;

    invoke-virtual {v0, p1}, LX/2wm;->a(LX/2we;)LX/2we;

    move-result-object p1

    goto :goto_0
.end method

.method public final a(JLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/common/ConnectionResult;
    .locals 1
    .param p3    # Ljava/util/concurrent/TimeUnit;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a()V
    .locals 1

    const/4 v0, 0x2

    iput v0, p0, LX/4uW;->n:I

    const/4 v0, 0x0

    iput-boolean v0, p0, LX/4uW;->l:Z

    const/4 v0, 0x0

    iput-object v0, p0, LX/4uW;->k:Lcom/google/android/gms/common/ConnectionResult;

    iput-object v0, p0, LX/4uW;->j:Lcom/google/android/gms/common/ConnectionResult;

    iget-object v0, p0, LX/4uW;->d:LX/2wm;

    invoke-virtual {v0}, LX/2wm;->a()V

    iget-object v0, p0, LX/4uW;->e:LX/2wm;

    invoke-virtual {v0}, LX/2wm;->a()V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 3

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    const-string v1, "authClient"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v0, p0, LX/4uW;->e:LX/2wm;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "  "

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2, p3, p4}, LX/2wm;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    const-string v1, "anonClient"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v0, p0, LX/4uW;->d:LX/2wm;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "  "

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2, p3, p4}, LX/2wm;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    return-void
.end method

.method public final a(LX/4uz;)Z
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, LX/4uW;->m:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    invoke-virtual {p0}, LX/4uW;->e()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, LX/4uW;->d()Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    invoke-direct {p0}, LX/4uW;->h()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, LX/4uW;->g:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget v1, p0, LX/4uW;->n:I

    if-nez v1, :cond_1

    const/4 v1, 0x1

    iput v1, p0, LX/4uW;->n:I

    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, LX/4uW;->k:Lcom/google/android/gms/common/ConnectionResult;

    iget-object v1, p0, LX/4uW;->e:LX/2wm;

    invoke-virtual {v1}, LX/2wm;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, LX/4uW;->m:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    :goto_0
    return v0

    :cond_2
    iget-object v0, p0, LX/4uW;->m:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/4uW;->m:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public final b(LX/2we;)LX/2we;
    .locals 4
    .param p1    # LX/2we;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A::",
            "LX/2wK;",
            "T:",
            "LX/2we",
            "<+",
            "LX/2NW;",
            "TA;>;>(TT;)TT;"
        }
    .end annotation

    invoke-direct {p0, p1}, LX/4uW;->c(LX/2we;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, LX/4uW;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/4 v1, 0x4

    const/4 v2, 0x0

    invoke-direct {p0}, LX/4uW;->n()Landroid/app/PendingIntent;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    invoke-virtual {p1, v0}, LX/2we;->b(Lcom/google/android/gms/common/api/Status;)V

    :goto_0
    return-object p1

    :cond_0
    iget-object v0, p0, LX/4uW;->e:LX/2wm;

    invoke-virtual {v0, p1}, LX/2wm;->b(LX/2we;)LX/2we;

    move-result-object p1

    goto :goto_0

    :cond_1
    iget-object v0, p0, LX/4uW;->d:LX/2wm;

    invoke-virtual {v0, p1}, LX/2wm;->b(LX/2we;)LX/2we;

    move-result-object p1

    goto :goto_0
.end method

.method public final b()Lcom/google/android/gms/common/ConnectionResult;
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final c()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, LX/4uW;->k:Lcom/google/android/gms/common/ConnectionResult;

    iput-object v0, p0, LX/4uW;->j:Lcom/google/android/gms/common/ConnectionResult;

    const/4 v0, 0x0

    iput v0, p0, LX/4uW;->n:I

    iget-object v0, p0, LX/4uW;->d:LX/2wm;

    invoke-virtual {v0}, LX/2wm;->c()V

    iget-object v0, p0, LX/4uW;->e:LX/2wm;

    invoke-virtual {v0}, LX/2wm;->c()V

    invoke-static {p0}, LX/4uW;->l(LX/4uW;)V

    return-void
.end method

.method public final d()Z
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, LX/4uW;->m:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget-object v1, p0, LX/4uW;->d:LX/2wm;

    invoke-virtual {v1}, LX/2wm;->d()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-direct {p0}, LX/4uW;->h()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0}, LX/4uW;->m()Z

    move-result v1

    if-nez v1, :cond_0

    iget v1, p0, LX/4uW;->n:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v1, v0, :cond_1

    :cond_0
    :goto_0
    iget-object v1, p0, LX/4uW;->m:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/4uW;->m:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public final e()Z
    .locals 2

    iget-object v0, p0, LX/4uW;->m:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget v0, p0, LX/4uW;->n:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, LX/4uW;->m:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/4uW;->m:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public final f()V
    .locals 1

    iget-object v0, p0, LX/4uW;->d:LX/2wm;

    invoke-virtual {v0}, LX/2wm;->f()V

    iget-object v0, p0, LX/4uW;->e:LX/2wm;

    invoke-virtual {v0}, LX/2wm;->f()V

    return-void
.end method

.method public final g()V
    .locals 3

    iget-object v0, p0, LX/4uW;->m:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    invoke-virtual {p0}, LX/4uW;->e()Z

    move-result v0

    iget-object v1, p0, LX/4uW;->e:LX/2wm;

    invoke-virtual {v1}, LX/2wm;->c()V

    new-instance v1, Lcom/google/android/gms/common/ConnectionResult;

    const/4 v2, 0x4

    invoke-direct {v1, v2}, Lcom/google/android/gms/common/ConnectionResult;-><init>(I)V

    iput-object v1, p0, LX/4uW;->k:Lcom/google/android/gms/common/ConnectionResult;

    if-eqz v0, :cond_0

    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, LX/4uW;->c:Landroid/os/Looper;

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/google/android/gms/internal/zzpq$1;

    invoke-direct {v1, p0}, Lcom/google/android/gms/internal/zzpq$1;-><init>(LX/4uW;)V

    const v2, 0x22381194

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    iget-object v0, p0, LX/4uW;->m:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-void

    :cond_0
    :try_start_1
    invoke-static {p0}, LX/4uW;->l(LX/4uW;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/4uW;->m:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method
