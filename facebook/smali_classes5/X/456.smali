.class public final enum LX/456;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/456;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/456;

.field public static final enum ATT:LX/456;

.field public static final enum UNKNOWN:LX/456;


# instance fields
.field private final mCountryCode:I

.field private final mNetworkCode:I


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 670161
    new-instance v0, LX/456;

    const-string v1, "ATT"

    const/16 v2, 0x136

    const/16 v3, 0x19a

    invoke-direct {v0, v1, v4, v2, v3}, LX/456;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/456;->ATT:LX/456;

    .line 670162
    new-instance v0, LX/456;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v5, v4, v4}, LX/456;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/456;->UNKNOWN:LX/456;

    .line 670163
    const/4 v0, 0x2

    new-array v0, v0, [LX/456;

    sget-object v1, LX/456;->ATT:LX/456;

    aput-object v1, v0, v4

    sget-object v1, LX/456;->UNKNOWN:LX/456;

    aput-object v1, v0, v5

    sput-object v0, LX/456;->$VALUES:[LX/456;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 670157
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 670158
    iput p3, p0, LX/456;->mCountryCode:I

    .line 670159
    iput p4, p0, LX/456;->mNetworkCode:I

    .line 670160
    return-void
.end method

.method public static fromMncMcc(II)LX/456;
    .locals 5

    .prologue
    .line 670149
    invoke-static {}, LX/456;->values()[LX/456;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 670150
    invoke-virtual {v0}, LX/456;->getCountryCode()I

    move-result v4

    if-ne v4, p0, :cond_0

    invoke-virtual {v0}, LX/456;->getNetworkCode()I

    move-result v4

    if-ne v4, p1, :cond_0

    .line 670151
    :goto_1
    return-object v0

    .line 670152
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 670153
    :cond_1
    sget-object v0, LX/456;->UNKNOWN:LX/456;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LX/456;
    .locals 1

    .prologue
    .line 670164
    const-class v0, LX/456;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/456;

    return-object v0
.end method

.method public static values()[LX/456;
    .locals 1

    .prologue
    .line 670156
    sget-object v0, LX/456;->$VALUES:[LX/456;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/456;

    return-object v0
.end method


# virtual methods
.method public final getCountryCode()I
    .locals 1

    .prologue
    .line 670155
    iget v0, p0, LX/456;->mCountryCode:I

    return v0
.end method

.method public final getNetworkCode()I
    .locals 1

    .prologue
    .line 670154
    iget v0, p0, LX/456;->mNetworkCode:I

    return v0
.end method
