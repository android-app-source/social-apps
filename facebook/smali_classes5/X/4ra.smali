.class public final LX/4ra;
.super LX/0lI;
.source ""


# static fields
.field private static final serialVersionUID:J = 0x7d74bce994c9ddf5L


# instance fields
.field public final _componentType:LX/0lJ;

.field public final _emptyArray:Ljava/lang/Object;


# direct methods
.method private constructor <init>(LX/0lJ;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Z)V
    .locals 6

    .prologue
    .line 816806
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, LX/0lJ;->hashCode()I

    move-result v2

    move-object v0, p0

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, LX/0lI;-><init>(Ljava/lang/Class;ILjava/lang/Object;Ljava/lang/Object;Z)V

    .line 816807
    iput-object p1, p0, LX/4ra;->_componentType:LX/0lJ;

    .line 816808
    iput-object p2, p0, LX/4ra;->_emptyArray:Ljava/lang/Object;

    .line 816809
    return-void
.end method

.method public static a(LX/0lJ;)LX/4ra;
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v5, 0x0

    .line 816810
    iget-object v0, p0, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v0, v0

    .line 816811
    invoke-static {v0, v5}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v2

    .line 816812
    new-instance v0, LX/4ra;

    move-object v1, p0

    move-object v4, v3

    invoke-direct/range {v0 .. v5}, LX/4ra;-><init>(LX/0lJ;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Z)V

    return-object v0
.end method

.method private e(Ljava/lang/Object;)LX/4ra;
    .locals 6

    .prologue
    .line 816813
    iget-object v0, p0, LX/0lJ;->_typeHandler:Ljava/lang/Object;

    if-ne p1, v0, :cond_0

    .line 816814
    :goto_0
    return-object p0

    :cond_0
    new-instance v0, LX/4ra;

    iget-object v1, p0, LX/4ra;->_componentType:LX/0lJ;

    iget-object v2, p0, LX/4ra;->_emptyArray:Ljava/lang/Object;

    iget-object v3, p0, LX/0lJ;->_valueHandler:Ljava/lang/Object;

    iget-boolean v5, p0, LX/0lJ;->_asStatic:Z

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, LX/4ra;-><init>(LX/0lJ;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Z)V

    move-object p0, v0

    goto :goto_0
.end method

.method private f(Ljava/lang/Object;)LX/4ra;
    .locals 6

    .prologue
    .line 816815
    iget-object v0, p0, LX/4ra;->_componentType:LX/0lJ;

    invoke-virtual {v0}, LX/0lJ;->u()Ljava/lang/Object;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 816816
    :goto_0
    return-object p0

    :cond_0
    new-instance v0, LX/4ra;

    iget-object v1, p0, LX/4ra;->_componentType:LX/0lJ;

    invoke-virtual {v1, p1}, LX/0lJ;->a(Ljava/lang/Object;)LX/0lJ;

    move-result-object v1

    iget-object v2, p0, LX/4ra;->_emptyArray:Ljava/lang/Object;

    iget-object v3, p0, LX/0lJ;->_valueHandler:Ljava/lang/Object;

    iget-object v4, p0, LX/0lJ;->_typeHandler:Ljava/lang/Object;

    iget-boolean v5, p0, LX/0lJ;->_asStatic:Z

    invoke-direct/range {v0 .. v5}, LX/4ra;-><init>(LX/0lJ;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Z)V

    move-object p0, v0

    goto :goto_0
.end method

.method private g(Ljava/lang/Object;)LX/4ra;
    .locals 6

    .prologue
    .line 816817
    iget-object v0, p0, LX/0lJ;->_valueHandler:Ljava/lang/Object;

    if-ne p1, v0, :cond_0

    .line 816818
    :goto_0
    return-object p0

    :cond_0
    new-instance v0, LX/4ra;

    iget-object v1, p0, LX/4ra;->_componentType:LX/0lJ;

    iget-object v2, p0, LX/4ra;->_emptyArray:Ljava/lang/Object;

    iget-object v4, p0, LX/0lJ;->_typeHandler:Ljava/lang/Object;

    iget-boolean v5, p0, LX/0lJ;->_asStatic:Z

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, LX/4ra;-><init>(LX/0lJ;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Z)V

    move-object p0, v0

    goto :goto_0
.end method

.method private h(Ljava/lang/Object;)LX/4ra;
    .locals 6

    .prologue
    .line 816819
    iget-object v0, p0, LX/4ra;->_componentType:LX/0lJ;

    invoke-virtual {v0}, LX/0lJ;->t()Ljava/lang/Object;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 816820
    :goto_0
    return-object p0

    :cond_0
    new-instance v0, LX/4ra;

    iget-object v1, p0, LX/4ra;->_componentType:LX/0lJ;

    invoke-virtual {v1, p1}, LX/0lJ;->c(Ljava/lang/Object;)LX/0lJ;

    move-result-object v1

    iget-object v2, p0, LX/4ra;->_emptyArray:Ljava/lang/Object;

    iget-object v3, p0, LX/0lJ;->_valueHandler:Ljava/lang/Object;

    iget-object v4, p0, LX/0lJ;->_typeHandler:Ljava/lang/Object;

    iget-boolean v5, p0, LX/0lJ;->_asStatic:Z

    invoke-direct/range {v0 .. v5}, LX/4ra;-><init>(LX/0lJ;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Z)V

    move-object p0, v0

    goto :goto_0
.end method

.method private w()LX/4ra;
    .locals 6

    .prologue
    .line 816821
    iget-boolean v0, p0, LX/0lJ;->_asStatic:Z

    if-eqz v0, :cond_0

    .line 816822
    :goto_0
    return-object p0

    :cond_0
    new-instance v0, LX/4ra;

    iget-object v1, p0, LX/4ra;->_componentType:LX/0lJ;

    invoke-virtual {v1}, LX/0lJ;->b()LX/0lJ;

    move-result-object v1

    iget-object v2, p0, LX/4ra;->_emptyArray:Ljava/lang/Object;

    iget-object v3, p0, LX/0lJ;->_valueHandler:Ljava/lang/Object;

    iget-object v4, p0, LX/0lJ;->_typeHandler:Ljava/lang/Object;

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, LX/4ra;-><init>(LX/0lJ;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Z)V

    move-object p0, v0

    goto :goto_0
.end method


# virtual methods
.method public final a(I)LX/0lJ;
    .locals 1

    .prologue
    .line 816823
    if-nez p1, :cond_0

    iget-object v0, p0, LX/4ra;->_componentType:LX/0lJ;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic a(Ljava/lang/Object;)LX/0lJ;
    .locals 1

    .prologue
    .line 816824
    invoke-direct {p0, p1}, LX/4ra;->e(Ljava/lang/Object;)LX/4ra;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b()LX/0lJ;
    .locals 1

    .prologue
    .line 816825
    invoke-direct {p0}, LX/4ra;->w()LX/4ra;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Ljava/lang/Object;)LX/0lJ;
    .locals 1

    .prologue
    .line 816826
    invoke-direct {p0, p1}, LX/4ra;->f(Ljava/lang/Object;)LX/4ra;

    move-result-object v0

    return-object v0
.end method

.method public final b(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 816827
    if-nez p1, :cond_0

    const-string v0, "E"

    .line 816828
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic c(Ljava/lang/Object;)LX/0lJ;
    .locals 1

    .prologue
    .line 816799
    invoke-direct {p0, p1}, LX/4ra;->g(Ljava/lang/Object;)LX/4ra;

    move-result-object v0

    return-object v0
.end method

.method public final d(Ljava/lang/Class;)LX/0lJ;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "LX/0lJ;"
        }
    .end annotation

    .prologue
    .line 816800
    invoke-virtual {p1}, Ljava/lang/Class;->isArray()Z

    move-result v0

    if-nez v0, :cond_0

    .line 816801
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Incompatible narrowing operation: trying to narrow "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/4ra;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to class "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 816802
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v0

    .line 816803
    sget-object v1, LX/0li;->a:LX/0li;

    move-object v1, v1

    .line 816804
    invoke-virtual {v1, v0}, LX/0li;->a(Ljava/lang/reflect/Type;)LX/0lJ;

    move-result-object v0

    .line 816805
    invoke-static {v0}, LX/4ra;->a(LX/0lJ;)LX/4ra;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d(Ljava/lang/Object;)LX/0lJ;
    .locals 1

    .prologue
    .line 816798
    invoke-direct {p0, p1}, LX/4ra;->h(Ljava/lang/Object;)LX/4ra;

    move-result-object v0

    return-object v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 816797
    const/4 v0, 0x0

    return v0
.end method

.method public final e(Ljava/lang/Class;)LX/0lJ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "LX/0lJ;"
        }
    .end annotation

    .prologue
    .line 816793
    iget-object v0, p0, LX/4ra;->_componentType:LX/0lJ;

    .line 816794
    iget-object v1, v0, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v0, v1

    .line 816795
    if-ne p1, v0, :cond_0

    .line 816796
    :goto_0
    return-object p0

    :cond_0
    iget-object v0, p0, LX/4ra;->_componentType:LX/0lJ;

    invoke-virtual {v0, p1}, LX/0lJ;->a(Ljava/lang/Class;)LX/0lJ;

    move-result-object v0

    invoke-static {v0}, LX/4ra;->a(LX/0lJ;)LX/4ra;

    move-result-object p0

    goto :goto_0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 816792
    const/4 v0, 0x1

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 816786
    if-ne p1, p0, :cond_1

    const/4 v0, 0x1

    .line 816787
    :cond_0
    :goto_0
    return v0

    .line 816788
    :cond_1
    if-eqz p1, :cond_0

    .line 816789
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-ne v1, v2, :cond_0

    .line 816790
    check-cast p1, LX/4ra;

    .line 816791
    iget-object v0, p0, LX/4ra;->_componentType:LX/0lJ;

    iget-object v1, p1, LX/4ra;->_componentType:LX/0lJ;

    invoke-virtual {v0, v1}, LX/0lJ;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final f(Ljava/lang/Class;)LX/0lJ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "LX/0lJ;"
        }
    .end annotation

    .prologue
    .line 816782
    iget-object v0, p0, LX/4ra;->_componentType:LX/0lJ;

    .line 816783
    iget-object v1, v0, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v0, v1

    .line 816784
    if-ne p1, v0, :cond_0

    .line 816785
    :goto_0
    return-object p0

    :cond_0
    iget-object v0, p0, LX/4ra;->_componentType:LX/0lJ;

    invoke-virtual {v0, p1}, LX/0lJ;->c(Ljava/lang/Class;)LX/0lJ;

    move-result-object v0

    invoke-static {v0}, LX/4ra;->a(LX/0lJ;)LX/4ra;

    move-result-object p0

    goto :goto_0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 816781
    const/4 v0, 0x1

    return v0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 816780
    const/4 v0, 0x1

    return v0
.end method

.method public final p()Z
    .locals 1

    .prologue
    .line 816779
    iget-object v0, p0, LX/4ra;->_componentType:LX/0lJ;

    invoke-virtual {v0}, LX/0lJ;->p()Z

    move-result v0

    return v0
.end method

.method public final r()LX/0lJ;
    .locals 1

    .prologue
    .line 816778
    iget-object v0, p0, LX/4ra;->_componentType:LX/0lJ;

    return-object v0
.end method

.method public final s()I
    .locals 1

    .prologue
    .line 816777
    const/4 v0, 0x1

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 816776
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[array type, component type: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/4ra;->_componentType:LX/0lJ;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final v()Ljava/lang/String;
    .locals 1

    .prologue
    .line 816775
    iget-object v0, p0, LX/0lJ;->_class:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
