.class public LX/4nr;
.super Landroid/widget/TextSwitcher;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field public a:Z

.field public b:I

.field public c:Z

.field public d:I

.field public e:Z

.field public f:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 807929
    invoke-direct {p0, p1}, Landroid/widget/TextSwitcher;-><init>(Landroid/content/Context;)V

    .line 807930
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/4nr;->a(Landroid/util/AttributeSet;)V

    .line 807931
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 807932
    invoke-direct {p0, p1, p2}, Landroid/widget/TextSwitcher;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 807933
    invoke-direct {p0, p2}, LX/4nr;->a(Landroid/util/AttributeSet;)V

    .line 807934
    return-void
.end method

.method private a(Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 807935
    invoke-virtual {p0}, LX/4nr;->getInAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    if-nez v0, :cond_0

    .line 807936
    invoke-virtual {p0}, LX/4nr;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f040028

    invoke-virtual {p0, v0, v1}, LX/4nr;->setInAnimation(Landroid/content/Context;I)V

    .line 807937
    :cond_0
    invoke-virtual {p0}, LX/4nr;->getOutAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    if-nez v0, :cond_1

    .line 807938
    invoke-virtual {p0}, LX/4nr;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f040029

    invoke-virtual {p0, v0, v1}, LX/4nr;->setOutAnimation(Landroid/content/Context;I)V

    .line 807939
    :cond_1
    if-eqz p1, :cond_2

    .line 807940
    invoke-virtual {p0}, LX/4nr;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, LX/03r;->CustomTextSwitcher:[I

    invoke-virtual {v0, p1, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 807941
    const/16 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    iput-boolean v1, p0, LX/4nr;->c:Z

    .line 807942
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, LX/4nr;->d:I

    .line 807943
    const/16 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    iput-boolean v1, p0, LX/4nr;->e:Z

    .line 807944
    const/16 v1, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, LX/4nr;->f:F

    .line 807945
    const/16 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    iput-boolean v1, p0, LX/4nr;->a:Z

    .line 807946
    const/16 v1, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, LX/4nr;->b:I

    .line 807947
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 807948
    :cond_2
    new-instance v0, LX/4nq;

    invoke-direct {v0, p0}, LX/4nq;-><init>(LX/4nr;)V

    invoke-super {p0, v0}, Landroid/widget/TextSwitcher;->setFactory(Landroid/widget/ViewSwitcher$ViewFactory;)V

    .line 807949
    return-void
.end method
