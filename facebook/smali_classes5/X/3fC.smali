.class public LX/3fC;
.super LX/0ro;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0ro",
        "<",
        "Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchMethod$Params;",
        "Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchMethod$Result;",
        ">;"
    }
.end annotation


# instance fields
.field private final b:LX/0SG;


# direct methods
.method public constructor <init>(LX/0sO;LX/0SG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 621600
    invoke-direct {p0, p1}, LX/0ro;-><init>(LX/0sO;)V

    .line 621601
    iput-object p2, p0, LX/3fC;->b:LX/0SG;

    .line 621602
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/1pN;LX/15w;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 621603
    const-class v0, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel;

    invoke-virtual {p3, v0}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel;

    .line 621604
    if-nez v0, :cond_0

    .line 621605
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Invalid JSON result"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 621606
    :cond_0
    new-instance v1, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchMethod$Result;

    sget-object v2, LX/0ta;->FROM_SERVER:LX/0ta;

    iget-object v3, p0, LX/3fC;->b:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    invoke-direct {v1, v2, v4, v5, v0}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchMethod$Result;-><init>(LX/0ta;JLcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel;)V

    return-object v1
.end method

.method public final b(Ljava/lang/Object;LX/1pN;)I
    .locals 1

    .prologue
    .line 621607
    const/4 v0, 0x1

    return v0
.end method

.method public final f(Ljava/lang/Object;)LX/0gW;
    .locals 3

    .prologue
    .line 621608
    check-cast p1, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchMethod$Params;

    .line 621609
    new-instance v0, LX/3fF;

    invoke-direct {v0}, LX/3fF;-><init>()V

    move-object v0, v0

    .line 621610
    const/4 v1, 0x1

    .line 621611
    iput-boolean v1, v0, LX/0gW;->l:Z

    .line 621612
    move-object v0, v0

    .line 621613
    const-string v1, "num_most_recently_used_pages"

    .line 621614
    iget v2, p1, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchMethod$Params;->a:I

    move v2, v2

    .line 621615
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    return-object v0
.end method
