.class public final LX/3gz;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 625029
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_5

    .line 625030
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 625031
    :goto_0
    return v1

    .line 625032
    :cond_0
    const-string v6, "key"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 625033
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v3, v0

    move v0, v2

    .line 625034
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_3

    .line 625035
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 625036
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 625037
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_1

    if-eqz v5, :cond_1

    .line 625038
    const-string v6, "animation"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 625039
    invoke-static {p0, p1}, LX/3Fr;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 625040
    :cond_2
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 625041
    :cond_3
    const/4 v5, 0x2

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 625042
    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 625043
    if-eqz v0, :cond_4

    .line 625044
    invoke-virtual {p1, v2, v3, v1}, LX/186;->a(III)V

    .line 625045
    :cond_4
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v3, v1

    move v4, v1

    goto :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 625046
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 625047
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 625048
    if-eqz v0, :cond_0

    .line 625049
    const-string v1, "animation"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 625050
    invoke-static {p0, v0, p2}, LX/3Fr;->a(LX/15i;ILX/0nX;)V

    .line 625051
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 625052
    if-eqz v0, :cond_1

    .line 625053
    const-string v1, "key"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 625054
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 625055
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 625056
    return-void
.end method
