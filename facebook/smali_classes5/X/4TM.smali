.class public LX/4TM;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 718061
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 718062
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v2, :cond_1

    .line 718063
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 718064
    :goto_0
    return v0

    .line 718065
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 718066
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_OBJECT:LX/15z;

    if-eq v1, v2, :cond_2

    .line 718067
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v1

    .line 718068
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 718069
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v2, v3, :cond_1

    if-eqz v1, :cond_1

    .line 718070
    const-string v2, "followable_topic"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 718071
    invoke-static {p0, p1}, LX/4Mk;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 718072
    :cond_2
    const/4 v1, 0x3

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 718073
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 718074
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 718075
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 718076
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 718077
    if-eqz v0, :cond_0

    .line 718078
    const-string v1, "followable_topic"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 718079
    invoke-static {p0, v0, p2}, LX/4Mk;->a(LX/15i;ILX/0nX;)V

    .line 718080
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 718081
    return-void
.end method
