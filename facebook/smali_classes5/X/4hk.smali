.class public LX/4hk;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private d:Z

.field private e:LX/4hY;

.field public f:LX/0Yb;

.field public g:Z

.field public h:Landroid/app/Activity;

.field public i:Lcom/facebook/platform/common/action/PlatformAppCall;

.field public j:Landroid/content/Intent;

.field public k:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public l:J

.field public m:Ljava/lang/String;

.field public n:Z

.field private o:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private p:I

.field private final q:LX/0Xl;
    .annotation runtime Lcom/facebook/base/broadcast/CrossFbProcessBroadcast;
    .end annotation
.end field

.field public final r:LX/03V;

.field public final s:LX/0WJ;

.field private final t:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/4hc;",
            ">;"
        }
    .end annotation
.end field

.field private final u:LX/4hi;

.field public final v:LX/3N2;

.field public final w:LX/4i1;

.field private final x:LX/HXx;

.field private final y:LX/GvB;

.field private final z:LX/42l;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const v3, 0x1332b3a

    .line 801971
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    sput-object v0, LX/4hk;->a:Ljava/util/Map;

    .line 801972
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    sput-object v0, LX/4hk;->b:Ljava/util/Map;

    .line 801973
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    sput-object v0, LX/4hk;->c:Ljava/util/Set;

    .line 801974
    sget-object v0, LX/4hk;->a:Ljava/util/Map;

    const-string v1, "com.facebook.platform.action.request.FEED_DIALOG"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 801975
    sget-object v0, LX/4hk;->a:Ljava/util/Map;

    const-string v1, "com.facebook.platform.action.request.OGACTIONPUBLISH_DIALOG"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 801976
    sget-object v0, LX/4hk;->b:Ljava/util/Map;

    const-string v1, "com.facebook.platform.action.request.LOGIN_DIALOG"

    new-instance v2, LX/4hf;

    const-string v3, "ServiceDisabled"

    const-string v4, "Please fall back to the previous version of the SSO Login Dialog"

    invoke-direct {v2, v3, v4}, LX/4hf;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 801977
    iget-object v3, v2, LX/4hf;->a:Landroid/os/Bundle;

    move-object v2, v3

    .line 801978
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 801979
    sget-object v0, LX/4hk;->c:Ljava/util/Set;

    const-string v1, "com.facebook.katana"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 801980
    sget-object v0, LX/4hk;->c:Ljava/util/Set;

    const-string v1, "com.facebook.orca"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 801981
    sget-object v0, LX/4hk;->c:Ljava/util/Set;

    const-string v1, "com.facebook.wakizashi"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 801982
    sget-object v0, LX/4hk;->c:Ljava/util/Set;

    const-string v1, "com.facebook.work"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 801983
    return-void
.end method

.method public constructor <init>(LX/0Xl;LX/03V;LX/0WJ;Ljava/util/Set;LX/4hi;LX/3N2;LX/4i1;LX/HXx;LX/GvB;LX/42l;)V
    .locals 4
    .param p1    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/CrossFbProcessBroadcast;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Xl;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Lcom/facebook/auth/datastore/LoggedInUserAuthDataStore;",
            "Ljava/util/Set",
            "<",
            "LX/4hc;",
            ">;",
            "LX/4hi;",
            "LX/3N2;",
            "LX/4i1;",
            "Lcom/facebook/platform/common/webdialogs/PlatformWebDialogsController;",
            "Lcom/facebook/auth/login/ipc/RedirectableLaunchAuthActivityUtil;",
            "LX/42l;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 802034
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 802035
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/4hk;->g:Z

    .line 802036
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/4hk;->t:Ljava/util/Map;

    .line 802037
    iput-object p1, p0, LX/4hk;->q:LX/0Xl;

    .line 802038
    iput-object p2, p0, LX/4hk;->r:LX/03V;

    .line 802039
    iput-object p3, p0, LX/4hk;->s:LX/0WJ;

    .line 802040
    iput-object p5, p0, LX/4hk;->u:LX/4hi;

    .line 802041
    iput-object p6, p0, LX/4hk;->v:LX/3N2;

    .line 802042
    iput-object p7, p0, LX/4hk;->w:LX/4i1;

    .line 802043
    iput-object p8, p0, LX/4hk;->x:LX/HXx;

    .line 802044
    iput-object p9, p0, LX/4hk;->y:LX/GvB;

    .line 802045
    iput-object p10, p0, LX/4hk;->z:LX/42l;

    .line 802046
    invoke-interface {p4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4hc;

    .line 802047
    invoke-interface {v0}, LX/4hc;->a()Ljava/lang/String;

    move-result-object v2

    .line 802048
    iget-object v3, p0, LX/4hk;->t:Ljava/util/Map;

    invoke-interface {v3, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 802049
    :cond_0
    return-void
.end method

.method private a(Landroid/content/Intent;)LX/4hY;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 801984
    iget-object v0, p0, LX/4hk;->i:Lcom/facebook/platform/common/action/PlatformAppCall;

    if-nez v0, :cond_1

    .line 801985
    :cond_0
    :goto_0
    return-object v2

    .line 801986
    :cond_1
    iget-boolean v0, p0, LX/4hk;->d:Z

    if-nez v0, :cond_b

    iget-object v0, p0, LX/4hk;->x:LX/HXx;

    if-eqz v0, :cond_b

    .line 801987
    iget-object v0, p0, LX/4hk;->x:LX/HXx;

    iget-object v1, p0, LX/4hk;->h:Landroid/app/Activity;

    iget-object v3, p0, LX/4hk;->i:Lcom/facebook/platform/common/action/PlatformAppCall;

    const/4 v4, 0x0

    .line 801988
    iget-object v5, v3, Lcom/facebook/platform/common/action/PlatformAppCall;->i:Ljava/lang/String;

    move-object v5, v5

    .line 801989
    iget-object v6, v0, LX/HXx;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 801990
    iget-object v6, v0, LX/HXx;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v7, LX/2QS;->c:LX/0Tn;

    invoke-interface {v6, v7, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v4

    .line 801991
    :cond_2
    if-eqz v4, :cond_3

    .line 801992
    iget-object v4, v0, LX/HXx;->h:LX/2QQ;

    invoke-virtual {v4}, LX/2QQ;->b()V

    .line 801993
    :cond_3
    iget-object v4, v0, LX/HXx;->h:LX/2QQ;

    invoke-virtual {v4, v5}, LX/2QQ;->a(Ljava/lang/String;)Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;

    move-result-object v4

    .line 801994
    if-nez v4, :cond_c

    .line 801995
    iget-object v4, v0, LX/HXx;->h:LX/2QQ;

    invoke-virtual {v4}, LX/2QQ;->a()V

    .line 801996
    const/4 v4, 0x0

    .line 801997
    :goto_1
    move-object v1, v4

    .line 801998
    if-eqz v1, :cond_4

    .line 801999
    iput-object p0, v1, LX/4hY;->a:LX/4hk;

    .line 802000
    move-object v2, v1

    .line 802001
    goto :goto_0

    .line 802002
    :cond_4
    iget-object v0, p0, LX/4hk;->x:LX/HXx;

    iget-object v1, p0, LX/4hk;->h:Landroid/app/Activity;

    iget-object v3, p0, LX/4hk;->i:Lcom/facebook/platform/common/action/PlatformAppCall;

    .line 802003
    invoke-static {v0, v1, p1, v3}, LX/HXx;->c(LX/HXx;Landroid/app/Activity;Landroid/content/Intent;Lcom/facebook/platform/common/action/PlatformAppCall;)LX/HXw;

    move-result-object v4

    move-object v0, v4

    .line 802004
    move-object v1, v0

    .line 802005
    :goto_2
    iget-object v0, p0, LX/4hk;->i:Lcom/facebook/platform/common/action/PlatformAppCall;

    .line 802006
    iget-object v3, v0, Lcom/facebook/platform/common/action/PlatformAppCall;->i:Ljava/lang/String;

    move-object v3, v3

    .line 802007
    sget-object v0, LX/4hk;->b:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 802008
    sget-object v0, LX/4hk;->b:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    invoke-static {p0, v0}, LX/4hk;->e(LX/4hk;Landroid/os/Bundle;)V

    goto :goto_0

    .line 802009
    :cond_5
    iget-object v0, p0, LX/4hk;->j:Landroid/content/Intent;

    const-string v4, "com.facebook.platform.protocol.PROTOCOL_VERSION"

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 802010
    sget-object v0, LX/4hk;->a:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    sget-object v0, LX/4hk;->a:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ge v4, v0, :cond_6

    .line 802011
    invoke-direct {p0, v3}, LX/4hk;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 802012
    :cond_6
    iget-object v0, p0, LX/4hk;->t:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4hc;

    .line 802013
    if-nez v0, :cond_8

    .line 802014
    if-nez v1, :cond_7

    .line 802015
    invoke-direct {p0, v3}, LX/4hk;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 802016
    :cond_7
    iput-object p0, v1, LX/4hY;->a:LX/4hk;

    .line 802017
    move-object v2, v1

    .line 802018
    goto/16 :goto_0

    .line 802019
    :cond_8
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/4hk;->d:Z

    .line 802020
    :try_start_0
    invoke-interface {v0}, LX/4hc;->b()LX/4hh;
    :try_end_0
    .catch LX/4he; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 802021
    if-nez v1, :cond_9

    .line 802022
    invoke-direct {p0, v3}, LX/4hk;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 802023
    :catch_0
    move-exception v0

    .line 802024
    iget-object v1, v0, LX/4he;->mErrorBundle:Landroid/os/Bundle;

    move-object v0, v1

    .line 802025
    invoke-static {p0, v0}, LX/4hk;->e(LX/4hk;Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 802026
    :catch_1
    invoke-direct {p0, v3}, LX/4hk;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 802027
    :cond_9
    iget-object v3, p0, LX/4hk;->i:Lcom/facebook/platform/common/action/PlatformAppCall;

    invoke-virtual {v1, v3, p1}, LX/4hh;->a(Lcom/facebook/platform/common/action/PlatformAppCall;Landroid/content/Intent;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 802028
    iget-object v2, p0, LX/4hk;->h:Landroid/app/Activity;

    invoke-interface {v0, v2, v1}, LX/4hc;->a(Landroid/app/Activity;LX/4hh;)LX/4hY;

    move-result-object v2

    .line 802029
    if-eqz v2, :cond_0

    .line 802030
    iput-object p0, v2, LX/4hY;->a:LX/4hk;

    .line 802031
    goto/16 :goto_0

    .line 802032
    :cond_a
    iget-object v0, v1, LX/4hh;->b:Landroid/os/Bundle;

    move-object v0, v0

    .line 802033
    invoke-static {p0, v0}, LX/4hk;->e(LX/4hk;Landroid/os/Bundle;)V

    goto/16 :goto_0

    :cond_b
    move-object v1, v2

    goto/16 :goto_2

    :cond_c
    invoke-static {v0, v1, p1, v3}, LX/HXx;->c(LX/HXx;Landroid/app/Activity;Landroid/content/Intent;Lcom/facebook/platform/common/action/PlatformAppCall;)LX/HXw;

    move-result-object v4

    goto/16 :goto_1
.end method

.method public static a(LX/4hk;Landroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v3, -0x1

    const/4 v1, 0x0

    .line 801918
    if-nez p1, :cond_0

    if-eqz p2, :cond_2

    :cond_0
    move v0, v2

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 801919
    if-nez p2, :cond_3

    .line 801920
    :goto_1
    iget-boolean v0, p0, LX/4hk;->n:Z

    if-eqz v0, :cond_4

    .line 801921
    iget-object v0, p0, LX/4hk;->h:Landroid/app/Activity;

    if-eqz v2, :cond_1

    move v1, v3

    :cond_1
    invoke-virtual {v0, v1}, Landroid/app/Activity;->setResult(I)V

    .line 801922
    iget-object v0, p0, LX/4hk;->h:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 801923
    :goto_2
    return-void

    :cond_2
    move v0, v1

    .line 801924
    goto :goto_0

    :cond_3
    move v2, v1

    .line 801925
    goto :goto_1

    .line 801926
    :cond_4
    iget-object v0, p0, LX/4hk;->j:Landroid/content/Intent;

    .line 801927
    const-string v4, "com.facebook.platform.protocol.PROTOCOL_VERSION"

    invoke-virtual {v0, v4, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    move v4, v4

    .line 801928
    iget-object v0, p0, LX/4hk;->i:Lcom/facebook/platform/common/action/PlatformAppCall;

    if-eqz v0, :cond_d

    .line 801929
    iget-object v0, p0, LX/4hk;->i:Lcom/facebook/platform/common/action/PlatformAppCall;

    .line 801930
    iget-object v5, v0, Lcom/facebook/platform/common/action/PlatformAppCall;->i:Ljava/lang/String;

    move-object v5, v5

    .line 801931
    iget-object v0, p0, LX/4hk;->i:Lcom/facebook/platform/common/action/PlatformAppCall;

    .line 801932
    iget-boolean v6, v0, Lcom/facebook/platform/common/action/PlatformAppCall;->c:Z

    move v0, v6

    .line 801933
    if-eqz v0, :cond_9

    .line 801934
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 801935
    iget-object v6, p0, LX/4hk;->j:Landroid/content/Intent;

    invoke-static {v6}, Lcom/facebook/platform/common/action/PlatformAppCall;->a(Landroid/content/Intent;)Landroid/os/Bundle;

    move-result-object v6

    .line 801936
    if-eqz p2, :cond_5

    .line 801937
    const-string v7, "error"

    invoke-virtual {v6, v7, p2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 801938
    :cond_5
    const-string v7, "action_id"

    iget-object v8, p0, LX/4hk;->i:Lcom/facebook/platform/common/action/PlatformAppCall;

    .line 801939
    iget-object p2, v8, Lcom/facebook/platform/common/action/PlatformAppCall;->a:Ljava/lang/String;

    move-object v8, p2

    .line 801940
    invoke-virtual {v6, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 801941
    const-string v7, "com.facebook.platform.protocol.BRIDGE_ARGS"

    invoke-virtual {v0, v7, v6}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 801942
    const-string v6, "com.facebook.platform.extra.APPLICATION_ID"

    iget-object v7, p0, LX/4hk;->i:Lcom/facebook/platform/common/action/PlatformAppCall;

    .line 801943
    iget-object v8, v7, Lcom/facebook/platform/common/action/PlatformAppCall;->e:Ljava/lang/String;

    move-object v7, v8

    .line 801944
    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 801945
    if-eqz v5, :cond_6

    .line 801946
    const-string v6, "com.facebook.platform.protocol.PROTOCOL_ACTION"

    invoke-virtual {v0, v6, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 801947
    :cond_6
    if-eqz p1, :cond_7

    .line 801948
    const-string v5, "com.facebook.platform.protocol.RESULT_ARGS"

    invoke-virtual {v0, v5, p1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_7
    move-object p2, v0

    .line 801949
    :cond_8
    :goto_3
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 801950
    const-string v5, "com.facebook.platform.protocol.PROTOCOL_VERSION"

    invoke-virtual {p2, v5, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 801951
    invoke-virtual {v0, p2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 801952
    if-eqz v2, :cond_f

    .line 801953
    :goto_4
    iget-object v1, p0, LX/4hk;->h:Landroid/app/Activity;

    invoke-virtual {v1, v3, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 801954
    iget-object v0, p0, LX/4hk;->h:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_2

    .line 801955
    :cond_9
    if-eqz p1, :cond_a

    move-object p2, p1

    .line 801956
    :cond_a
    if-nez p2, :cond_b

    .line 801957
    new-instance p2, Landroid/os/Bundle;

    invoke-direct {p2}, Landroid/os/Bundle;-><init>()V

    .line 801958
    :cond_b
    if-eqz v5, :cond_c

    .line 801959
    const-string v0, "com.facebook.platform.protocol.PROTOCOL_ACTION"

    invoke-virtual {p2, v0, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 801960
    :cond_c
    const v0, 0x1332ac6

    if-lt v4, v0, :cond_8

    .line 801961
    const-string v0, "com.facebook.platform.protocol.CALL_ID"

    iget-object v5, p0, LX/4hk;->i:Lcom/facebook/platform/common/action/PlatformAppCall;

    .line 801962
    iget-object v6, v5, Lcom/facebook/platform/common/action/PlatformAppCall;->a:Ljava/lang/String;

    move-object v5, v6

    .line 801963
    invoke-virtual {p2, v0, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 801964
    :cond_d
    if-nez p2, :cond_e

    .line 801965
    const/4 v0, 0x0

    const-string v5, "UnknownError"

    const-string v6, "Unknown error in processing the incoming intent"

    invoke-static {v0, v5, v6}, LX/4hW;->a(Lcom/facebook/platform/common/action/PlatformAppCall;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object p2

    .line 801966
    :cond_e
    iget-object v0, p0, LX/4hk;->j:Landroid/content/Intent;

    invoke-static {v0}, Lcom/facebook/platform/common/action/PlatformAppCall;->a(Landroid/content/Intent;)Landroid/os/Bundle;

    move-result-object v0

    .line 801967
    if-eqz v0, :cond_8

    .line 801968
    const-string v5, "error"

    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6, p2}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 801969
    const-string v5, "com.facebook.platform.protocol.BRIDGE_ARGS"

    invoke-virtual {p2, v5, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_3

    :cond_f
    move v3, v1

    .line 801970
    goto :goto_4
.end method

.method public static a(LX/4hk;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 801916
    iget-object v0, p0, LX/4hk;->i:Lcom/facebook/platform/common/action/PlatformAppCall;

    const-string v1, "ProtocolError"

    invoke-static {v0, v1, p1}, LX/4hW;->a(Lcom/facebook/platform/common/action/PlatformAppCall;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    invoke-static {p0, v0}, LX/4hk;->e(LX/4hk;Landroid/os/Bundle;)V

    .line 801917
    return-void
.end method

.method public static b(LX/0QB;)LX/4hk;
    .locals 11

    .prologue
    .line 801912
    new-instance v0, LX/4hk;

    invoke-static {p0}, LX/0a5;->a(LX/0QB;)LX/0a5;

    move-result-object v1

    check-cast v1, LX/0Xl;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v2

    check-cast v2, LX/03V;

    invoke-static {p0}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v3

    check-cast v3, LX/0WJ;

    .line 801913
    new-instance v4, LX/0U8;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v5

    new-instance v6, LX/4hl;

    invoke-direct {v6, p0}, LX/4hl;-><init>(LX/0QB;)V

    invoke-direct {v4, v5, v6}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v4, v4

    .line 801914
    invoke-static {p0}, LX/4hi;->a(LX/0QB;)LX/4hi;

    move-result-object v5

    check-cast v5, LX/4hi;

    invoke-static {p0}, LX/3N2;->b(LX/0QB;)LX/3N2;

    move-result-object v6

    check-cast v6, LX/3N2;

    invoke-static {p0}, LX/4i1;->a(LX/0QB;)LX/4i1;

    move-result-object v7

    check-cast v7, LX/4i1;

    invoke-static {p0}, LX/HXx;->b(LX/0QB;)LX/HXx;

    move-result-object v8

    check-cast v8, LX/HXx;

    invoke-static {p0}, LX/GvB;->b(LX/0QB;)LX/GvB;

    move-result-object v9

    check-cast v9, LX/GvB;

    invoke-static {p0}, LX/42l;->a(LX/0QB;)LX/42l;

    move-result-object v10

    check-cast v10, LX/42l;

    invoke-direct/range {v0 .. v10}, LX/4hk;-><init>(LX/0Xl;LX/03V;LX/0WJ;Ljava/util/Set;LX/4hi;LX/3N2;LX/4i1;LX/HXx;LX/GvB;LX/42l;)V

    .line 801915
    return-object v0
.end method

.method private b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 802050
    if-nez p1, :cond_0

    .line 802051
    const-string v0, "Expected non-null \'%s\' extra."

    const-string v1, "com.facebook.platform.protocol.PROTOCOL_ACTION"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 802052
    :goto_0
    invoke-static {p0, v0}, LX/4hk;->a(LX/4hk;Ljava/lang/String;)V

    .line 802053
    return-void

    .line 802054
    :cond_0
    const-string v0, "Unrecognized \'%s\' extra: \'%s\'."

    const-string v1, "com.facebook.platform.protocol.PROTOCOL_ACTION"

    invoke-static {v0, v1, p1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private b(Z)V
    .locals 3

    .prologue
    .line 801906
    iget-object v0, p0, LX/4hk;->y:LX/GvB;

    if-eqz v0, :cond_0

    .line 801907
    iget-object v0, p0, LX/4hk;->y:LX/GvB;

    iget-object v1, p0, LX/4hk;->h:Landroid/app/Activity;

    invoke-virtual {v0, v1, p1}, LX/GvB;->a(Landroid/app/Activity;Z)V

    .line 801908
    :goto_0
    return-void

    .line 801909
    :cond_0
    new-instance v0, LX/4hf;

    const-string v1, "PermissionDenied"

    const-string v2, "No user is logged in and app is unable to display login screen"

    invoke-direct {v0, v1, v2}, LX/4hf;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 801910
    iget-object v1, v0, LX/4hf;->a:Landroid/os/Bundle;

    move-object v0, v1

    .line 801911
    invoke-static {p0, v0}, LX/4hk;->e(LX/4hk;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public static e(LX/4hk;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 801903
    const/4 v0, 0x0

    if-eqz p1, :cond_0

    :goto_0
    invoke-static {p0, v0, p1}, LX/4hk;->a(LX/4hk;Landroid/os/Bundle;Landroid/os/Bundle;)V

    .line 801904
    return-void

    .line 801905
    :cond_0
    new-instance p1, Landroid/os/Bundle;

    invoke-direct {p1}, Landroid/os/Bundle;-><init>()V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 801896
    iget-object v0, p0, LX/4hk;->f:LX/0Yb;

    if-eqz v0, :cond_0

    .line 801897
    iget-object v0, p0, LX/4hk;->f:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->c()V

    .line 801898
    :cond_0
    iget-object v0, p0, LX/4hk;->z:LX/42l;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/4hk;->h:Landroid/app/Activity;

    if-eqz v0, :cond_1

    .line 801899
    iget-object v0, p0, LX/4hk;->z:LX/42l;

    iget-object v1, p0, LX/4hk;->h:Landroid/app/Activity;

    iget v2, p0, LX/4hk;->p:I

    const-class v3, Lcom/facebook/platform/common/annotations/TaskRunningInPlatformContext;

    invoke-virtual {v0, v1, v2, v3}, LX/42l;->a(Landroid/app/Activity;ILjava/lang/Object;)V

    .line 801900
    :cond_1
    iget-object v0, p0, LX/4hk;->e:LX/4hY;

    if-eqz v0, :cond_2

    .line 801901
    iget-object v0, p0, LX/4hk;->e:LX/4hY;

    invoke-virtual {v0}, LX/4hY;->a()V

    .line 801902
    :cond_2
    return-void
.end method

.method public final a(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 801813
    iget-boolean v0, p0, LX/4hk;->g:Z

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    .line 801814
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/4hk;->g:Z

    .line 801815
    iget-object v0, p0, LX/4hk;->e:LX/4hY;

    invoke-virtual {v0}, LX/4hY;->a()V

    .line 801816
    iput-object v1, p0, LX/4hk;->e:LX/4hY;

    .line 801817
    const/4 v0, 0x1

    invoke-direct {p0, v0}, LX/4hk;->b(Z)V

    .line 801818
    :goto_0
    return-void

    .line 801819
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 801820
    iget-object v0, p0, LX/4hk;->e:LX/4hY;

    invoke-virtual {v0, p1, p2, p3}, LX/4hY;->a(IILandroid/content/Intent;)V

    goto :goto_0

    .line 801821
    :pswitch_0
    if-nez p2, :cond_1

    .line 801822
    iget-object v0, p0, LX/4hk;->i:Lcom/facebook/platform/common/action/PlatformAppCall;

    const-string v1, "User canceled login"

    invoke-static {v0, v1}, LX/4hW;->a(Lcom/facebook/platform/common/action/PlatformAppCall;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    invoke-static {p0, v0}, LX/4hk;->e(LX/4hk;Landroid/os/Bundle;)V

    goto :goto_0

    .line 801823
    :cond_1
    iget-object v0, p0, LX/4hk;->e:LX/4hY;

    if-nez v0, :cond_2

    .line 801824
    iget-object v0, p0, LX/4hk;->j:Landroid/content/Intent;

    invoke-direct {p0, v0}, LX/4hk;->a(Landroid/content/Intent;)LX/4hY;

    move-result-object v0

    iput-object v0, p0, LX/4hk;->e:LX/4hY;

    .line 801825
    :cond_2
    iget-object v0, p0, LX/4hk;->e:LX/4hY;

    invoke-virtual {v0, v1}, LX/4hY;->a(Landroid/os/Bundle;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x8a2
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Landroid/os/Bundle;Landroid/app/Activity;Landroid/content/Intent;ZJ)V
    .locals 9

    .prologue
    .line 801832
    iput-object p2, p0, LX/4hk;->h:Landroid/app/Activity;

    .line 801833
    iput-object p3, p0, LX/4hk;->j:Landroid/content/Intent;

    .line 801834
    iput-wide p5, p0, LX/4hk;->l:J

    .line 801835
    iget-object v0, p0, LX/4hk;->h:Landroid/app/Activity;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    iput-object v0, p0, LX/4hk;->o:Ljava/lang/Class;

    .line 801836
    iget-object v0, p0, LX/4hk;->u:LX/4hi;

    invoke-virtual {v0}, LX/4hi;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 801837
    iget-object v0, p0, LX/4hk;->o:Ljava/lang/Class;

    const-string v1, "Api requests exceed the rate limit"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 801838
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/4hk;->e(LX/4hk;Landroid/os/Bundle;)V

    .line 801839
    :cond_0
    :goto_0
    return-void

    .line 801840
    :cond_1
    iget-object v0, p0, LX/4hk;->q:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "ACTION_MQTT_NO_AUTH"

    new-instance v2, LX/4hj;

    invoke-direct {v2, p0}, LX/4hj;-><init>(LX/4hk;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, LX/4hk;->f:LX/0Yb;

    .line 801841
    iget-object v0, p0, LX/4hk;->f:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 801842
    if-eqz p1, :cond_2

    .line 801843
    const-string v0, "calling_package"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/4hk;->m:Ljava/lang/String;

    .line 801844
    const-string v0, "disallow_web_dialog"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, LX/4hk;->d:Z

    .line 801845
    const-string v0, "platform_app_call"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/platform/common/action/PlatformAppCall;

    iput-object v0, p0, LX/4hk;->i:Lcom/facebook/platform/common/action/PlatformAppCall;

    .line 801846
    :goto_1
    iget-object v0, p0, LX/4hk;->z:LX/42l;

    iget-object v1, p0, LX/4hk;->h:Landroid/app/Activity;

    const-class v2, Lcom/facebook/platform/common/annotations/TaskRunningInPlatformContext;

    invoke-virtual {v0, v1, v2}, LX/42l;->a(Landroid/app/Activity;Ljava/lang/Object;)V

    .line 801847
    iget-object v0, p0, LX/4hk;->h:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getTaskId()I

    move-result v0

    iput v0, p0, LX/4hk;->p:I

    .line 801848
    iget-object v0, p0, LX/4hk;->j:Landroid/content/Intent;

    invoke-direct {p0, v0}, LX/4hk;->a(Landroid/content/Intent;)LX/4hY;

    move-result-object v0

    iput-object v0, p0, LX/4hk;->e:LX/4hY;

    .line 801849
    iget-object v0, p0, LX/4hk;->e:LX/4hY;

    if-eqz v0, :cond_0

    .line 801850
    iget-object v0, p0, LX/4hk;->s:LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->b()Z

    move-result v0

    move v0, v0

    .line 801851
    if-nez v0, :cond_8

    .line 801852
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/4hk;->b(Z)V

    goto :goto_0

    .line 801853
    :cond_2
    iget-object v3, p0, LX/4hk;->h:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v3

    .line 801854
    if-eqz v3, :cond_9

    invoke-virtual {v3}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    .line 801855
    :goto_2
    if-eqz p4, :cond_3

    sget-object v5, LX/4hk;->c:Ljava/util/Set;

    invoke-interface {v5, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 801856
    :cond_3
    iget-object v3, p0, LX/4hk;->j:Landroid/content/Intent;

    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    .line 801857
    if-eqz v5, :cond_6

    .line 801858
    const-string v3, "calling_package_key"

    invoke-virtual {v5, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, LX/4hk;->m:Ljava/lang/String;

    .line 801859
    const-string v3, "platform_launch_time_ms"

    invoke-virtual {v5, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 801860
    const-string v3, "platform_launch_time_ms"

    invoke-virtual {v5, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v7

    iput-wide v7, p0, LX/4hk;->l:J

    .line 801861
    :cond_4
    const-string v3, "platform_launch_extras"

    invoke-virtual {v5, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 801862
    const-string v3, "platform_launch_extras"

    invoke-virtual {v5, v3}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v3

    check-cast v3, Ljava/util/Map;

    iput-object v3, p0, LX/4hk;->k:Ljava/util/Map;

    .line 801863
    :cond_5
    const-string v3, "should_set_simple_result"

    invoke-virtual {v5, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    iput-boolean v3, p0, LX/4hk;->n:Z

    .line 801864
    :cond_6
    :goto_3
    iget-object v3, p0, LX/4hk;->m:Ljava/lang/String;

    if-nez v3, :cond_7

    .line 801865
    sget-boolean v3, LX/007;->i:Z

    move v3, v3

    .line 801866
    if-eqz v3, :cond_b

    iget-object v3, p0, LX/4hk;->j:Landroid/content/Intent;

    const-string v5, "internal_calling_package"

    invoke-virtual {v3, v5}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 801867
    iget-object v3, p0, LX/4hk;->j:Landroid/content/Intent;

    const-string v5, "internal_calling_package"

    invoke-virtual {v3, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, LX/4hk;->m:Ljava/lang/String;

    .line 801868
    :cond_7
    iget-object v3, p0, LX/4hk;->j:Landroid/content/Intent;

    const/4 v5, 0x0

    .line 801869
    iget-object v6, p0, LX/4hk;->v:LX/3N2;

    iget-object v7, p0, LX/4hk;->m:Ljava/lang/String;

    invoke-virtual {v6, v7}, LX/3N2;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 801870
    if-nez v6, :cond_d

    .line 801871
    const-string v6, "Application key hash could not be computed"

    invoke-static {p0, v6}, LX/4hk;->a(LX/4hk;Ljava/lang/String;)V

    .line 801872
    :goto_4
    move-object v3, v5

    .line 801873
    iput-object v3, p0, LX/4hk;->i:Lcom/facebook/platform/common/action/PlatformAppCall;

    .line 801874
    iget-object v3, p0, LX/4hk;->i:Lcom/facebook/platform/common/action/PlatformAppCall;

    if-nez v3, :cond_c

    .line 801875
    :goto_5
    goto/16 :goto_1

    .line 801876
    :cond_8
    iget-object v0, p0, LX/4hk;->e:LX/4hY;

    invoke-virtual {v0, p1}, LX/4hY;->a(Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 801877
    :cond_9
    const/4 v3, 0x0

    goto/16 :goto_2

    .line 801878
    :cond_a
    iput-object v3, p0, LX/4hk;->m:Ljava/lang/String;

    goto :goto_3

    .line 801879
    :cond_b
    iget-object v3, p0, LX/4hk;->r:LX/03V;

    const-string v5, "sso"

    const-string v6, "getCallingPackage==null; finish() called. see t1118578"

    invoke-virtual {v3, v5, v6}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 801880
    const-string v3, "The calling package was null"

    invoke-static {p0, v3}, LX/4hk;->a(LX/4hk;Ljava/lang/String;)V

    .line 801881
    goto :goto_5

    .line 801882
    :cond_c
    iget-object v3, p0, LX/4hk;->w:LX/4i1;

    iget-wide v5, p0, LX/4hk;->l:J

    iget-object v4, p0, LX/4hk;->k:Ljava/util/Map;

    .line 801883
    iget-boolean v7, v3, LX/4i1;->a:Z

    invoke-virtual {v3, v5, v6, v7, v4}, LX/4i1;->a(JZLjava/util/Map;)V

    .line 801884
    const/4 v7, 0x0

    iput-boolean v7, v3, LX/4i1;->a:Z

    .line 801885
    goto :goto_5

    .line 801886
    :cond_d
    :try_start_0
    new-instance v7, LX/4ha;

    invoke-direct {v7, v3}, LX/4ha;-><init>(Landroid/content/Intent;)V

    .line 801887
    iput-object v6, v7, LX/4ha;->g:Ljava/lang/String;

    .line 801888
    move-object v6, v7

    .line 801889
    iget-object v8, p0, LX/4hk;->m:Ljava/lang/String;

    .line 801890
    iput-object v8, v6, LX/4ha;->d:Ljava/lang/String;

    .line 801891
    new-instance v6, Lcom/facebook/platform/common/action/PlatformAppCall;

    invoke-direct {v6, v7}, Lcom/facebook/platform/common/action/PlatformAppCall;-><init>(LX/4ha;)V

    move-object v5, v6
    :try_end_0
    .catch LX/4he; {:try_start_0 .. :try_end_0} :catch_0

    .line 801892
    goto :goto_4

    .line 801893
    :catch_0
    move-exception v6

    .line 801894
    iget-object v7, v6, LX/4he;->mErrorBundle:Landroid/os/Bundle;

    move-object v6, v7

    .line 801895
    invoke-static {p0, v6}, LX/4hk;->e(LX/4hk;Landroid/os/Bundle;)V

    goto :goto_4
.end method

.method public final c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 801826
    const-string v0, "disallow_web_dialog"

    iget-boolean v1, p0, LX/4hk;->d:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 801827
    const-string v0, "calling_package"

    iget-object v1, p0, LX/4hk;->m:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 801828
    const-string v0, "platform_app_call"

    iget-object v1, p0, LX/4hk;->i:Lcom/facebook/platform/common/action/PlatformAppCall;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 801829
    iget-object v0, p0, LX/4hk;->e:LX/4hY;

    if-eqz v0, :cond_0

    .line 801830
    iget-object v0, p0, LX/4hk;->e:LX/4hY;

    invoke-virtual {v0, p1}, LX/4hY;->b(Landroid/os/Bundle;)V

    .line 801831
    :cond_0
    return-void
.end method
