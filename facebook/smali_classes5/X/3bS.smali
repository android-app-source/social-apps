.class public LX/3bS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1TE;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/3bS;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 611075
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 611076
    return-void
.end method

.method public static a(LX/0QB;)LX/3bS;
    .locals 3

    .prologue
    .line 611077
    sget-object v0, LX/3bS;->a:LX/3bS;

    if-nez v0, :cond_1

    .line 611078
    const-class v1, LX/3bS;

    monitor-enter v1

    .line 611079
    :try_start_0
    sget-object v0, LX/3bS;->a:LX/3bS;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 611080
    if-eqz v2, :cond_0

    .line 611081
    :try_start_1
    new-instance v0, LX/3bS;

    invoke-direct {v0}, LX/3bS;-><init>()V

    .line 611082
    move-object v0, v0

    .line 611083
    sput-object v0, LX/3bS;->a:LX/3bS;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 611084
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 611085
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 611086
    :cond_1
    sget-object v0, LX/3bS;->a:LX/3bS;

    return-object v0

    .line 611087
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 611088
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1KB;)V
    .locals 7

    .prologue
    .line 611089
    sget-object v0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainHeaderPartDefinition;->a:LX/1Cz;

    sget-object v1, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentHeaderPartDefinition;->a:LX/1Cz;

    sget-object v2, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainPhotoAttachmentPartDefinition;->a:LX/1Cz;

    sget-object v3, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentPhotoPartDefinition;->a:LX/1Cz;

    sget-object v4, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainVideoAttachmentPartDefinition;->a:LX/1Cz;

    sget-object v5, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentVideoPartDefinition;->a:LX/1Cz;

    sget-object v6, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedGapPartDefinition;->a:LX/1Cz;

    invoke-static/range {v0 .. v6}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 611090
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Cz;

    .line 611091
    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 611092
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 611093
    :cond_0
    return-void
.end method
