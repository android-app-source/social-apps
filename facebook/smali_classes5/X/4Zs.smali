.class public LX/4Zs;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/Object;

.field public d:I

.field public e:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;II)V
    .locals 0

    .prologue
    .line 790313
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 790314
    iput-object p1, p0, LX/4Zs;->a:Ljava/lang/String;

    .line 790315
    iput-object p2, p0, LX/4Zs;->b:Ljava/lang/String;

    .line 790316
    iput-object p3, p0, LX/4Zs;->c:Ljava/lang/Object;

    .line 790317
    iput p4, p0, LX/4Zs;->d:I

    .line 790318
    iput p5, p0, LX/4Zs;->e:I

    .line 790319
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 790320
    if-ne p0, p1, :cond_1

    .line 790321
    :cond_0
    :goto_0
    return v0

    .line 790322
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 790323
    goto :goto_0

    .line 790324
    :cond_3
    check-cast p1, LX/4Zs;

    .line 790325
    iget v2, p0, LX/4Zs;->d:I

    iget v3, p1, LX/4Zs;->d:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 790326
    goto :goto_0

    .line 790327
    :cond_4
    iget v2, p0, LX/4Zs;->e:I

    iget v3, p1, LX/4Zs;->e:I

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 790328
    iget v0, p0, LX/4Zs;->d:I

    .line 790329
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, LX/4Zs;->e:I

    add-int/2addr v0, v1

    .line 790330
    return v0
.end method
