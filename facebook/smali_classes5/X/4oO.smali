.class public final enum LX/4oO;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/4oO;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/4oO;

.field public static final enum CENTER:LX/4oO;

.field public static final enum CENTER_CROP:LX/4oO;

.field public static final enum MATRIX:LX/4oO;

.field public static final enum NONE:LX/4oO;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 809607
    new-instance v0, LX/4oO;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, LX/4oO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4oO;->NONE:LX/4oO;

    .line 809608
    new-instance v0, LX/4oO;

    const-string v1, "CENTER"

    invoke-direct {v0, v1, v3}, LX/4oO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4oO;->CENTER:LX/4oO;

    .line 809609
    new-instance v0, LX/4oO;

    const-string v1, "CENTER_CROP"

    invoke-direct {v0, v1, v4}, LX/4oO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4oO;->CENTER_CROP:LX/4oO;

    .line 809610
    new-instance v0, LX/4oO;

    const-string v1, "MATRIX"

    invoke-direct {v0, v1, v5}, LX/4oO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4oO;->MATRIX:LX/4oO;

    .line 809611
    const/4 v0, 0x4

    new-array v0, v0, [LX/4oO;

    sget-object v1, LX/4oO;->NONE:LX/4oO;

    aput-object v1, v0, v2

    sget-object v1, LX/4oO;->CENTER:LX/4oO;

    aput-object v1, v0, v3

    sget-object v1, LX/4oO;->CENTER_CROP:LX/4oO;

    aput-object v1, v0, v4

    sget-object v1, LX/4oO;->MATRIX:LX/4oO;

    aput-object v1, v0, v5

    sput-object v0, LX/4oO;->$VALUES:[LX/4oO;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 809606
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/4oO;
    .locals 1

    .prologue
    .line 809613
    const-class v0, LX/4oO;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/4oO;

    return-object v0
.end method

.method public static values()[LX/4oO;
    .locals 1

    .prologue
    .line 809612
    sget-object v0, LX/4oO;->$VALUES:[LX/4oO;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/4oO;

    return-object v0
.end method
