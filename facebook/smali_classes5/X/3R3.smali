.class public LX/3R3;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/03V;

.field public final b:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(LX/03V;Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 578488
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 578489
    iput-object p1, p0, LX/3R3;->a:LX/03V;

    .line 578490
    iput-object p2, p0, LX/3R3;->b:Landroid/content/res/Resources;

    .line 578491
    return-void
.end method

.method private static a(LX/3R3;Ljava/util/List;Lcom/facebook/graphql/model/GraphQLPrivacyOption;)LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyOption;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLPrivacyOption;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyOption;",
            ">;"
        }
    .end annotation

    .prologue
    .line 578479
    if-nez p1, :cond_0

    .line 578480
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 578481
    :goto_0
    return-object v0

    .line 578482
    :cond_0
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 578483
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 578484
    invoke-static {p0, v0, p2}, LX/3R3;->a(LX/3R3;Lcom/facebook/graphql/model/GraphQLPrivacyOption;Lcom/facebook/graphql/model/GraphQLPrivacyOption;)Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    .line 578485
    if-eqz v0, :cond_1

    .line 578486
    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 578487
    :cond_2
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(LX/3R3;Lcom/facebook/graphql/model/GraphQLPrivacyOption;Lcom/facebook/graphql/model/GraphQLPrivacyOption;)Lcom/facebook/graphql/model/GraphQLPrivacyOption;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 578463
    if-nez p1, :cond_1

    .line 578464
    :cond_0
    :goto_0
    return-object v0

    .line 578465
    :cond_1
    invoke-static {p1}, LX/8QP;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)LX/8QP;

    move-result-object v1

    .line 578466
    invoke-static {p1}, LX/3R3;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 578467
    sget-object v2, LX/2h3;->b:[I

    invoke-static {p1}, LX/2cA;->a(LX/1oV;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 578468
    invoke-static {p1, p2}, LX/2cA;->a(LX/1oS;LX/1oS;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 578469
    iget-object v2, p0, LX/3R3;->b:Landroid/content/res/Resources;

    const v3, 0x7f0812dc

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 578470
    :goto_1
    move-object v2, v2

    .line 578471
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 578472
    invoke-virtual {v1, v2}, LX/8QP;->d(Ljava/lang/String;)LX/8QP;

    .line 578473
    :cond_2
    invoke-virtual {v1}, LX/8QP;->b()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    goto :goto_0

    .line 578474
    :pswitch_0
    iget-object v2, p0, LX/3R3;->b:Landroid/content/res/Resources;

    const v3, 0x7f0812d2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 578475
    :pswitch_1
    iget-object v2, p0, LX/3R3;->b:Landroid/content/res/Resources;

    const v3, 0x7f0812d3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 578476
    :pswitch_2
    iget-object v2, p0, LX/3R3;->b:Landroid/content/res/Resources;

    const v3, 0x7f0812d5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 578477
    :pswitch_3
    iget-object v2, p0, LX/3R3;->b:Landroid/content/res/Resources;

    const v3, 0x7f0812d8

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 578478
    :cond_3
    const/4 v2, 0x0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerConnection;Z)Lcom/facebook/privacy/model/PrivacyOptionsResult;
    .locals 15

    .prologue
    .line 578415
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 578416
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 578417
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v9

    .line 578418
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v10

    .line 578419
    const/4 v5, -0x1

    .line 578420
    const/4 v6, 0x0

    .line 578421
    const/4 v7, -0x1

    .line 578422
    const/4 v8, 0x0

    .line 578423
    if-nez p1, :cond_0

    .line 578424
    iget-object v0, p0, LX/3R3;->a:LX/03V;

    const-string v1, "null_privacy_option_edges"

    const-string v2, "null passed to PrivacyOptionsResultFactory.fromOptionEdges, returning empty result"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 578425
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "null passed to fromOptionEdges"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 578426
    :cond_0
    const/4 v1, -0x1

    .line 578427
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerConnection;->a()LX/0Px;

    move-result-object v11

    invoke-virtual {v11}, LX/0Px;->size()I

    move-result v12

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v12, :cond_9

    invoke-virtual {v11, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerEdge;

    .line 578428
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerEdge;->l()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v13

    if-nez v13, :cond_2

    .line 578429
    const-string v0, "null_privacy_option_received"

    invoke-direct {p0, v0}, LX/3R3;->a(Ljava/lang/String;)V

    .line 578430
    :cond_1
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 578431
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerEdge;->m()Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    move-result-object v13

    if-eqz v13, :cond_3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerEdge;->m()Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    move-result-object v13

    sget-object v14, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    if-ne v13, v14, :cond_4

    .line 578432
    :cond_3
    const-string v0, "null_privacy_option_type_received"

    invoke-direct {p0, v0}, LX/3R3;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 578433
    :cond_4
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerEdge;->l()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v13

    const-string v14, "composer"

    invoke-direct {p0, v13, v14}, LX/3R3;->a(LX/1oU;Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_1

    .line 578434
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerEdge;->j()Z

    move-result v13

    if-eqz v13, :cond_5

    .line 578435
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerEdge;->l()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v8

    .line 578436
    :cond_5
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerEdge;->m()Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    move-result-object v13

    .line 578437
    sget-object v14, LX/2h3;->a:[I

    invoke-virtual {v13}, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;->ordinal()I

    move-result v13

    aget v13, v14, v13

    packed-switch v13, :pswitch_data_0

    .line 578438
    :pswitch_0
    add-int/lit8 v1, v1, 0x1

    .line 578439
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerEdge;->l()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v13

    invoke-virtual {v3, v13}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 578440
    :goto_2
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerEdge;->k()Z

    move-result v13

    if-eqz v13, :cond_6

    .line 578441
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v9, v13}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 578442
    :cond_6
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerEdge;->a()Z

    move-result v13

    if-eqz v13, :cond_7

    .line 578443
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerEdge;->l()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v6

    move v5, v1

    .line 578444
    :cond_7
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerEdge;->j()Z

    move-result v13

    if-eqz v13, :cond_1

    .line 578445
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerEdge;->l()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v8

    move v7, v1

    goto :goto_1

    .line 578446
    :pswitch_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerEdge;->a()Z

    move-result v13

    if-eqz v13, :cond_8

    .line 578447
    add-int/lit8 v1, v1, 0x1

    .line 578448
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerEdge;->l()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v13

    invoke-virtual {v3, v13}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 578449
    :cond_8
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerEdge;->l()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v13

    invoke-virtual {v4, v13}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_2

    .line 578450
    :pswitch_2
    add-int/lit8 v1, v1, 0x1

    .line 578451
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerEdge;->l()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v13

    invoke-virtual {v3, v13}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 578452
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v10, v13}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_2

    .line 578453
    :cond_9
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 578454
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 578455
    invoke-static {v1}, LX/3R3;->a(Ljava/util/List;)V

    .line 578456
    invoke-static {v2}, LX/3R3;->a(Ljava/util/List;)V

    .line 578457
    if-nez v6, :cond_a

    .line 578458
    const-string v0, "invalid_selected_composer_privacy_option_received"

    invoke-direct {p0, v0}, LX/3R3;->a(Ljava/lang/String;)V

    .line 578459
    const-string v0, "invalid_selected_composer_privacy_option_received"

    invoke-static {v6, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 578460
    :cond_a
    invoke-virtual {v1, v6}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_b

    invoke-virtual {v2, v6}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    :cond_b
    const/4 v0, 0x1

    :goto_3
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 578461
    new-instance v0, Lcom/facebook/privacy/model/PrivacyOptionsResult;

    invoke-virtual {v9}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    invoke-virtual {v10}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    const/4 v9, 0x0

    move/from16 v10, p2

    invoke-direct/range {v0 .. v10}, Lcom/facebook/privacy/model/PrivacyOptionsResult;-><init>(LX/0Px;LX/0Px;LX/0Px;LX/0Px;ILcom/facebook/graphql/model/GraphQLPrivacyOption;ILcom/facebook/graphql/model/GraphQLPrivacyOption;ZZ)V

    return-object v0

    .line 578462
    :cond_c
    const/4 v0, 0x0

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 578409
    const-string v0, "Got some null options"

    invoke-static {p1, v0}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v0

    const/16 v1, 0xa

    .line 578410
    iput v1, v0, LX/0VK;->e:I

    .line 578411
    move-object v0, v0

    .line 578412
    invoke-virtual {v0}, LX/0VK;->g()LX/0VG;

    move-result-object v0

    .line 578413
    iget-object v1, p0, LX/3R3;->a:LX/03V;

    invoke-virtual {v1, v0}, LX/03V;->a(LX/0VG;)V

    .line 578414
    return-void
.end method

.method private static a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyOption;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 578406
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 578407
    invoke-static {v0}, LX/3R3;->a(LX/1oU;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    goto :goto_0

    .line 578408
    :cond_0
    return-void
.end method

.method public static a(LX/1oU;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 578492
    if-nez p0, :cond_1

    .line 578493
    :cond_0
    :goto_0
    return v0

    .line 578494
    :cond_1
    invoke-interface {p0}, LX/1oU;->c()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 578495
    invoke-interface {p0}, LX/1oU;->d()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 578496
    invoke-interface {p0}, LX/1oU;->b()LX/1Fd;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, LX/1oU;->b()LX/1Fd;

    move-result-object v1

    invoke-interface {v1}, LX/1Fd;->d()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 578497
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private a(LX/1oU;Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 578400
    invoke-static {p1}, LX/3R3;->b(LX/1oU;)Ljava/lang/String;

    move-result-object v0

    .line 578401
    if-nez v0, :cond_0

    .line 578402
    const/4 v0, 0x1

    .line 578403
    :goto_0
    return v0

    .line 578404
    :cond_0
    iget-object v1, p0, LX/3R3;->a:LX/03V;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "_"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "Invalid privacy option"

    invoke-virtual {v1, v0, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 578405
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)Z
    .locals 1

    .prologue
    .line 578397
    if-nez p0, :cond_0

    .line 578398
    const/4 v0, 0x0

    .line 578399
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/3R3;
    .locals 3

    .prologue
    .line 578395
    new-instance v2, LX/3R3;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v1

    check-cast v1, Landroid/content/res/Resources;

    invoke-direct {v2, v0, v1}, LX/3R3;-><init>(LX/03V;Landroid/content/res/Resources;)V

    .line 578396
    return-object v2
.end method

.method public static b(LX/1oU;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 578385
    if-nez p0, :cond_0

    .line 578386
    const-string v0, "privacy_option_invalid_object_null"

    .line 578387
    :goto_0
    return-object v0

    .line 578388
    :cond_0
    invoke-interface {p0}, LX/1oU;->c()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 578389
    const-string v0, "privacy_option_invalid_json_null"

    goto :goto_0

    .line 578390
    :cond_1
    invoke-interface {p0}, LX/1oU;->d()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    .line 578391
    const-string v0, "privacy_option_invalid_name_null"

    goto :goto_0

    .line 578392
    :cond_2
    invoke-interface {p0}, LX/1oU;->b()LX/1Fd;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-interface {p0}, LX/1oU;->b()LX/1Fd;

    move-result-object v0

    invoke-interface {v0}, LX/1Fd;->d()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_4

    .line 578393
    :cond_3
    const-string v0, "privacy_option_invalid_icon_null"

    goto :goto_0

    .line 578394
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0Px;Z)LX/8QJ;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentEdge;",
            ">;Z)",
            "LX/8QJ;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 578356
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 578357
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 578358
    const/4 v1, 0x0

    .line 578359
    if-nez p1, :cond_0

    .line 578360
    iget-object v0, p0, LX/3R3;->a:LX/03V;

    const-string v1, "null_privacy_option_edges"

    const-string v2, "null passed to PrivacyOptionsResultFactory.fromOptionEdges, returning empty result"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 578361
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "null passed to fromOptionEdges"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 578362
    :cond_0
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v6

    move v3, v2

    :goto_0
    if-ge v3, v6, :cond_6

    invoke-virtual {p1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentEdge;

    .line 578363
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentEdge;->j()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v7

    if-nez v7, :cond_2

    .line 578364
    const-string v0, "null_privacy_option_received"

    invoke-direct {p0, v0}, LX/3R3;->a(Ljava/lang/String;)V

    .line 578365
    :cond_1
    :goto_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 578366
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentEdge;->k()Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    move-result-object v7

    if-eqz v7, :cond_3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentEdge;->k()Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    move-result-object v7

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    if-ne v7, v8, :cond_4

    .line 578367
    :cond_3
    const-string v0, "null_privacy_option_type_received"

    invoke-direct {p0, v0}, LX/3R3;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 578368
    :cond_4
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentEdge;->j()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v7

    const-string v8, "content"

    invoke-direct {p0, v7, v8}, LX/3R3;->a(LX/1oU;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 578369
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentEdge;->a()Z

    move-result v7

    if-eqz v7, :cond_5

    .line 578370
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentEdge;->j()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v1

    .line 578371
    :cond_5
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentEdge;->k()Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    move-result-object v7

    .line 578372
    sget-object v8, LX/2h3;->a:[I

    invoke-virtual {v7}, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;->ordinal()I

    move-result v7

    aget v7, v8, v7

    packed-switch v7, :pswitch_data_0

    .line 578373
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentEdge;->j()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 578374
    :pswitch_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentEdge;->j()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    invoke-virtual {v5, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 578375
    :cond_6
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    .line 578376
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    .line 578377
    invoke-static {v3}, LX/3R3;->a(Ljava/util/List;)V

    .line 578378
    invoke-static {v4}, LX/3R3;->a(Ljava/util/List;)V

    .line 578379
    if-nez v1, :cond_7

    .line 578380
    const-string v0, "invalid_selected_content_privacy_option_received"

    invoke-direct {p0, v0}, LX/3R3;->a(Ljava/lang/String;)V

    .line 578381
    const-string v0, "invalid_selected_content_privacy_option_received"

    invoke-static {v1, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 578382
    :cond_7
    invoke-virtual {v3, v1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    invoke-virtual {v4, v1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    :cond_8
    const/4 v0, 0x1

    :goto_2
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 578383
    new-instance v0, LX/8QJ;

    invoke-direct {v0, v3, v4, v1, p2}, LX/8QJ;-><init>(LX/0Px;LX/0Px;Lcom/facebook/graphql/model/GraphQLPrivacyOption;Z)V

    return-object v0

    :cond_9
    move v0, v2

    .line 578384
    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLViewer;Z)Lcom/facebook/privacy/model/PrivacyOptionsResult;
    .locals 1

    .prologue
    .line 578355
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLViewer;->j()Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerConnection;

    move-result-object v0

    invoke-direct {p0, v0, p2}, LX/3R3;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOptionsComposerConnection;Z)Lcom/facebook/privacy/model/PrivacyOptionsResult;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/privacy/model/PrivacyOptionsResult;)Lcom/facebook/privacy/model/PrivacyOptionsResult;
    .locals 12

    .prologue
    .line 578332
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 578333
    iget-object v0, p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;->basicPrivacyOptions:LX/0Px;

    if-eqz v0, :cond_2

    .line 578334
    iget-object v4, p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;->basicPrivacyOptions:LX/0Px;

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_2

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 578335
    invoke-static {v0}, LX/3R3;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 578336
    :goto_1
    move v0, v0

    .line 578337
    if-eqz v0, :cond_0

    .line 578338
    iget-object v0, p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;->basicPrivacyOptions:LX/0Px;

    iget-object v1, p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-static {p0, v0, v1}, LX/3R3;->a(LX/3R3;Ljava/util/List;Lcom/facebook/graphql/model/GraphQLPrivacyOption;)LX/0Px;

    move-result-object v1

    .line 578339
    iget-object v0, p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;->friendListPrivacyOptions:LX/0Px;

    iget-object v2, p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-static {p0, v0, v2}, LX/3R3;->a(LX/3R3;Ljava/util/List;Lcom/facebook/graphql/model/GraphQLPrivacyOption;)LX/0Px;

    move-result-object v2

    .line 578340
    iget-object v0, p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iget-object v3, p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-static {p0, v0, v3}, LX/3R3;->a(LX/3R3;Lcom/facebook/graphql/model/GraphQLPrivacyOption;Lcom/facebook/graphql/model/GraphQLPrivacyOption;)Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v11

    .line 578341
    iget-object v0, p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;->recentPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iget-object v3, p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-static {p0, v0, v3}, LX/3R3;->a(LX/3R3;Lcom/facebook/graphql/model/GraphQLPrivacyOption;Lcom/facebook/graphql/model/GraphQLPrivacyOption;)Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v8

    .line 578342
    new-instance v0, Lcom/facebook/privacy/model/PrivacyOptionsResult;

    iget-object v3, p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;->primaryOptionIndices:LX/0Px;

    iget-object v4, p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;->expandablePrivacyOptionIndices:LX/0Px;

    iget v5, p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOptionIndex:I

    const/4 v6, 0x0

    iget v7, p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;->recentPrivacyOptionIndex:I

    iget-boolean v9, p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;->isSelectedOptionExternal:Z

    const/4 v10, 0x0

    invoke-direct/range {v0 .. v10}, Lcom/facebook/privacy/model/PrivacyOptionsResult;-><init>(LX/0Px;LX/0Px;LX/0Px;LX/0Px;ILcom/facebook/graphql/model/GraphQLPrivacyOption;ILcom/facebook/graphql/model/GraphQLPrivacyOption;ZZ)V

    .line 578343
    iget-object v1, p0, LX/3R3;->a:LX/03V;

    const-string v2, "migrated_privacy_options"

    const-string v3, "Migrating privacy options as some are missing names."

    invoke-virtual {v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 578344
    invoke-static {v0}, Lcom/facebook/privacy/model/PrivacyOptionsResult;->a(Lcom/facebook/privacy/model/PrivacyOptionsResult;)LX/2br;

    move-result-object v0

    invoke-virtual {v0, v11}, LX/2br;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)LX/2br;

    move-result-object v0

    invoke-virtual {v0}, LX/2br;->b()Lcom/facebook/privacy/model/PrivacyOptionsResult;

    move-result-object p1

    .line 578345
    :cond_0
    return-object p1

    .line 578346
    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 578347
    :cond_2
    iget-object v0, p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;->friendListPrivacyOptions:LX/0Px;

    if-eqz v0, :cond_4

    .line 578348
    iget-object v4, p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;->friendListPrivacyOptions:LX/0Px;

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v3, v2

    :goto_2
    if-ge v3, v5, :cond_4

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 578349
    invoke-static {v0}, LX/3R3;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    .line 578350
    goto :goto_1

    .line 578351
    :cond_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    .line 578352
    :cond_4
    iget-object v0, p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-static {v0}, LX/3R3;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;->recentPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-static {v0}, LX/3R3;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_5
    move v0, v1

    .line 578353
    goto :goto_1

    :cond_6
    move v0, v2

    .line 578354
    goto :goto_1
.end method
