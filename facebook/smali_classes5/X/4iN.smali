.class public final LX/4iN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "Lcom/facebook/proxy/ProxyWrapper;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "Lcom/facebook/proxy/ProxyWrapper;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 803041
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 803042
    iput-object p1, p0, LX/4iN;->a:LX/0QB;

    .line 803043
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 803044
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/4iN;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 803045
    packed-switch p2, :pswitch_data_0

    .line 803046
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 803047
    :pswitch_0
    new-instance p0, LX/4cf;

    invoke-static {p1}, LX/0dt;->b(LX/0QB;)LX/0dx;

    move-result-object v0

    check-cast v0, LX/0dx;

    invoke-static {p1}, LX/1gz;->b(LX/0QB;)LX/1h1;

    move-result-object v1

    check-cast v1, LX/1h1;

    invoke-direct {p0, v0, v1}, LX/4cf;-><init>(LX/0dx;LX/1h1;)V

    .line 803048
    move-object v0, p0

    .line 803049
    return-object v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 803050
    const/4 v0, 0x1

    return v0
.end method
