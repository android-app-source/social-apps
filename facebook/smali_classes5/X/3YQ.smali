.class public LX/3YQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1TE;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/3YR;


# direct methods
.method public constructor <init>(LX/3YR;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 595976
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 595977
    iput-object p1, p0, LX/3YQ;->a:LX/3YR;

    .line 595978
    return-void
.end method

.method public static a(LX/0QB;)LX/3YQ;
    .locals 4

    .prologue
    .line 595979
    const-class v1, LX/3YQ;

    monitor-enter v1

    .line 595980
    :try_start_0
    sget-object v0, LX/3YQ;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 595981
    sput-object v2, LX/3YQ;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 595982
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 595983
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 595984
    new-instance p0, LX/3YQ;

    invoke-static {v0}, LX/3YR;->a(LX/0QB;)LX/3YR;

    move-result-object v3

    check-cast v3, LX/3YR;

    invoke-direct {p0, v3}, LX/3YQ;-><init>(LX/3YR;)V

    .line 595985
    move-object v0, p0

    .line 595986
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 595987
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/3YQ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 595988
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 595989
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1KB;)V
    .locals 1

    .prologue
    .line 595973
    sget-object v0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewSquarePartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 595974
    sget-object v0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewWidePartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 595975
    return-void
.end method
