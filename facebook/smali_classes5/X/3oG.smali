.class public final LX/3oG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Handler$Callback;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 640019
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    .line 640020
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 640021
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 640022
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LX/3oW;

    .line 640023
    iget-object v2, v0, LX/3oW;->d:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    invoke-virtual {v2}, Landroid/support/design/widget/Snackbar$SnackbarLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-nez v2, :cond_1

    .line 640024
    iget-object v2, v0, LX/3oW;->d:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    invoke-virtual {v2}, Landroid/support/design/widget/Snackbar$SnackbarLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 640025
    instance-of v3, v2, LX/1uK;

    if-eqz v3, :cond_0

    .line 640026
    new-instance v3, Landroid/support/design/widget/Snackbar$Behavior;

    invoke-direct {v3, v0}, Landroid/support/design/widget/Snackbar$Behavior;-><init>(LX/3oW;)V

    .line 640027
    const v4, 0x3dcccccd    # 0.1f

    .line 640028
    const/4 p0, 0x0

    const/high16 p1, 0x3f800000    # 1.0f

    invoke-static {p0, v4, p1}, Landroid/support/design/widget/SwipeDismissBehavior;->c(FFF)F

    move-result p0

    iput p0, v3, Landroid/support/design/widget/SwipeDismissBehavior;->h:F

    .line 640029
    const v4, 0x3f19999a    # 0.6f

    .line 640030
    const/4 p0, 0x0

    const/high16 p1, 0x3f800000    # 1.0f

    invoke-static {p0, v4, p1}, Landroid/support/design/widget/SwipeDismissBehavior;->c(FFF)F

    move-result p0

    iput p0, v3, Landroid/support/design/widget/SwipeDismissBehavior;->i:F

    .line 640031
    const/4 v4, 0x0

    .line 640032
    iput v4, v3, Landroid/support/design/widget/SwipeDismissBehavior;->f:I

    .line 640033
    new-instance v4, LX/3oL;

    invoke-direct {v4, v0}, LX/3oL;-><init>(LX/3oW;)V

    .line 640034
    iput-object v4, v3, Landroid/support/design/widget/SwipeDismissBehavior;->b:LX/3oK;

    .line 640035
    check-cast v2, LX/1uK;

    invoke-virtual {v2, v3}, LX/1uK;->a(Landroid/support/design/widget/CoordinatorLayout$Behavior;)V

    .line 640036
    :cond_0
    iget-object v2, v0, LX/3oW;->b:Landroid/view/ViewGroup;

    iget-object v3, v0, LX/3oW;->d:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 640037
    :cond_1
    iget-object v2, v0, LX/3oW;->d:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    new-instance v3, LX/3oN;

    invoke-direct {v3, v0}, LX/3oN;-><init>(LX/3oW;)V

    .line 640038
    iput-object v3, v2, Landroid/support/design/widget/Snackbar$SnackbarLayout;->f:LX/3oM;

    .line 640039
    iget-object v2, v0, LX/3oW;->d:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    invoke-static {v2}, LX/0vv;->E(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 640040
    invoke-static {v0}, LX/3oW;->h(LX/3oW;)V

    .line 640041
    :goto_1
    move v0, v1

    .line 640042
    goto :goto_0

    .line 640043
    :pswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LX/3oW;

    iget v2, p1, Landroid/os/Message;->arg1:I

    .line 640044
    iget-object v3, v0, LX/3oW;->d:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    invoke-virtual {v3}, Landroid/support/design/widget/Snackbar$SnackbarLayout;->getVisibility()I

    move-result v3

    if-nez v3, :cond_2

    const/4 v4, 0x0

    .line 640045
    iget-object v3, v0, LX/3oW;->d:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    invoke-virtual {v3}, Landroid/support/design/widget/Snackbar$SnackbarLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    .line 640046
    instance-of v5, v3, LX/1uK;

    if-eqz v5, :cond_6

    .line 640047
    check-cast v3, LX/1uK;

    .line 640048
    iget-object v5, v3, LX/1uK;->a:Landroid/support/design/widget/CoordinatorLayout$Behavior;

    move-object v3, v5

    .line 640049
    instance-of v5, v3, Landroid/support/design/widget/SwipeDismissBehavior;

    if-eqz v5, :cond_6

    .line 640050
    check-cast v3, Landroid/support/design/widget/SwipeDismissBehavior;

    .line 640051
    iget-object v5, v3, Landroid/support/design/widget/SwipeDismissBehavior;->a:LX/3ty;

    if-eqz v5, :cond_7

    iget-object v5, v3, Landroid/support/design/widget/SwipeDismissBehavior;->a:LX/3ty;

    .line 640052
    iget v3, v5, LX/3ty;->a:I

    move v5, v3

    .line 640053
    :goto_2
    move v3, v5

    .line 640054
    if-eqz v3, :cond_5

    const/4 v3, 0x1

    .line 640055
    :goto_3
    move v3, v3

    .line 640056
    if-eqz v3, :cond_4

    .line 640057
    :cond_2
    invoke-static {v0, v2}, LX/3oW;->f(LX/3oW;I)V

    .line 640058
    :goto_4
    move v0, v1

    .line 640059
    goto/16 :goto_0

    .line 640060
    :cond_3
    iget-object v2, v0, LX/3oW;->d:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    new-instance v3, LX/3oP;

    invoke-direct {v3, v0}, LX/3oP;-><init>(LX/3oW;)V

    .line 640061
    iput-object v3, v2, Landroid/support/design/widget/Snackbar$SnackbarLayout;->e:LX/3oO;

    .line 640062
    goto :goto_1

    .line 640063
    :cond_4
    const-wide/16 v6, 0xfa

    .line 640064
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0xe

    if-lt v4, v5, :cond_8

    .line 640065
    iget-object v4, v0, LX/3oW;->d:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    invoke-static {v4}, LX/0vv;->v(Landroid/view/View;)LX/3sU;

    move-result-object v4

    iget-object v5, v0, LX/3oW;->d:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    invoke-virtual {v5}, Landroid/support/design/widget/Snackbar$SnackbarLayout;->getHeight()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v4, v5}, LX/3sU;->c(F)LX/3sU;

    move-result-object v4

    sget-object v5, LX/3ns;->b:Landroid/view/animation/Interpolator;

    invoke-virtual {v4, v5}, LX/3sU;->a(Landroid/view/animation/Interpolator;)LX/3sU;

    move-result-object v4

    invoke-virtual {v4, v6, v7}, LX/3sU;->a(J)LX/3sU;

    move-result-object v4

    new-instance v5, LX/3oU;

    invoke-direct {v5, v0, v2}, LX/3oU;-><init>(LX/3oW;I)V

    invoke-virtual {v4, v5}, LX/3sU;->a(LX/3oQ;)LX/3sU;

    move-result-object v4

    invoke-virtual {v4}, LX/3sU;->b()V

    .line 640066
    :goto_5
    goto :goto_4

    :cond_5
    move v3, v4

    .line 640067
    goto :goto_3

    :cond_6
    move v3, v4

    .line 640068
    goto :goto_3

    :cond_7
    const/4 v5, 0x0

    goto :goto_2

    .line 640069
    :cond_8
    iget-object v4, v0, LX/3oW;->d:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    invoke-virtual {v4}, Landroid/support/design/widget/Snackbar$SnackbarLayout;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f04002d

    invoke-static {v4, v5}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v4

    .line 640070
    sget-object v5, LX/3ns;->b:Landroid/view/animation/Interpolator;

    invoke-virtual {v4, v5}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 640071
    invoke-virtual {v4, v6, v7}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 640072
    new-instance v5, LX/3oF;

    invoke-direct {v5, v0, v2}, LX/3oF;-><init>(LX/3oW;I)V

    invoke-virtual {v4, v5}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 640073
    iget-object v5, v0, LX/3oW;->d:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    invoke-virtual {v5, v4}, Landroid/support/design/widget/Snackbar$SnackbarLayout;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_5

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
