.class public final LX/42i;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/42g;


# instance fields
.field public final synthetic a:LX/42j;


# direct methods
.method public constructor <init>(LX/42j;)V
    .locals 0

    .prologue
    .line 667812
    iput-object p1, p0, LX/42i;->a:LX/42j;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Class;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<+TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 667813
    iget-object v0, p0, LX/42i;->a:LX/42j;

    .line 667814
    invoke-virtual {p1, v0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    .line 667815
    :goto_0
    move-object v0, v0

    .line 667816
    return-object v0

    :cond_0
    iget-object p0, v0, LX/42j;->b:LX/42g;

    invoke-interface {p0, p1}, LX/42g;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 667817
    iget-object v0, p0, LX/42i;->a:LX/42j;

    .line 667818
    iget-object p0, v0, LX/42j;->b:LX/42g;

    invoke-interface {p0, p1}, LX/42g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    move-object v0, p0

    .line 667819
    return-object v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 667820
    iget-object v0, p0, LX/42i;->a:LX/42j;

    invoke-virtual {v0}, LX/42j;->e()V

    .line 667821
    return-void
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 667822
    iget-object v0, p0, LX/42i;->a:LX/42j;

    .line 667823
    iget-object p0, v0, LX/42j;->b:LX/42g;

    invoke-interface {p0, p1}, LX/42g;->a(I)V

    .line 667824
    return-void
.end method

.method public final a(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 667808
    iget-object v0, p0, LX/42i;->a:LX/42j;

    invoke-virtual {v0, p1, p2, p3}, LX/42j;->a(IILandroid/content/Intent;)V

    .line 667809
    return-void
.end method

.method public final a(ILandroid/app/Dialog;)V
    .locals 1

    .prologue
    .line 667825
    iget-object v0, p0, LX/42i;->a:LX/42j;

    .line 667826
    iget-object p0, v0, LX/42j;->b:LX/42g;

    invoke-interface {p0, p1, p2}, LX/42g;->a(ILandroid/app/Dialog;)V

    .line 667827
    return-void
.end method

.method public final a(LX/0T2;)V
    .locals 1

    .prologue
    .line 667828
    iget-object v0, p0, LX/42i;->a:LX/42j;

    .line 667829
    iget-object p0, v0, LX/42j;->b:LX/42g;

    invoke-interface {p0, p1}, LX/0ex;->a(LX/0T2;)V

    .line 667830
    return-void
.end method

.method public final a(LX/0je;)V
    .locals 1

    .prologue
    .line 667831
    iget-object v0, p0, LX/42i;->a:LX/42j;

    .line 667832
    iget-object p0, v0, LX/42j;->b:LX/42g;

    invoke-interface {p0, p1}, LX/42g;->a(LX/0je;)V

    .line 667833
    return-void
.end method

.method public final a(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 667834
    iget-object v0, p0, LX/42i;->a:LX/42j;

    .line 667835
    iget-object p0, v0, LX/42j;->b:LX/42g;

    invoke-interface {p0, p1}, LX/42g;->a(Landroid/app/Activity;)V

    .line 667836
    return-void
.end method

.method public final a(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 667837
    iget-object v0, p0, LX/42i;->a:LX/42j;

    invoke-virtual {v0, p1}, LX/42j;->a(Landroid/content/Intent;)V

    .line 667838
    return-void
.end method

.method public final a(Landroid/content/Intent;I)V
    .locals 1

    .prologue
    .line 667839
    iget-object v0, p0, LX/42i;->a:LX/42j;

    invoke-virtual {v0, p1, p2}, LX/42j;->a(Landroid/content/Intent;I)V

    .line 667840
    return-void
.end method

.method public final a(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 667841
    iget-object v0, p0, LX/42i;->a:LX/42j;

    invoke-virtual {v0, p1}, LX/42j;->a(Landroid/content/res/Configuration;)V

    .line 667842
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 667843
    iget-object v0, p0, LX/42i;->a:LX/42j;

    .line 667844
    iget-object p0, v0, LX/42j;->b:LX/42g;

    invoke-interface {p0, p1}, LX/42g;->a(Landroid/os/Bundle;)V

    .line 667845
    return-void
.end method

.method public final a(Landroid/support/v4/app/Fragment;)V
    .locals 1

    .prologue
    .line 667846
    iget-object v0, p0, LX/42i;->a:LX/42j;

    .line 667847
    iget-object p0, v0, LX/42j;->b:LX/42g;

    invoke-interface {p0, p1}, LX/42g;->a(Landroid/support/v4/app/Fragment;)V

    .line 667848
    return-void
.end method

.method public final a(Landroid/support/v4/app/Fragment;Landroid/content/Intent;I)V
    .locals 1

    .prologue
    .line 667852
    iget-object v0, p0, LX/42i;->a:LX/42j;

    .line 667853
    iget-object p0, v0, LX/42j;->b:LX/42g;

    invoke-interface {p0, p1, p2, p3}, LX/42g;->a(Landroid/support/v4/app/Fragment;Landroid/content/Intent;I)V

    .line 667854
    return-void
.end method

.method public final a(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 1

    .prologue
    .line 667849
    iget-object v0, p0, LX/42i;->a:LX/42j;

    .line 667850
    iget-object p0, v0, LX/42j;->b:LX/42g;

    invoke-interface {p0, p1, p2, p3}, LX/42g;->a(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    .line 667851
    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 667885
    iget-object v0, p0, LX/42i;->a:LX/42j;

    .line 667886
    iget-object p0, v0, LX/42j;->b:LX/42g;

    invoke-interface {p0, p1, p2}, LX/42g;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 667887
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 667882
    iget-object v0, p0, LX/42i;->a:LX/42j;

    .line 667883
    iget-object p0, v0, LX/42j;->b:LX/42g;

    invoke-interface {p0, p1}, LX/42g;->a(Z)V

    .line 667884
    return-void
.end method

.method public final a(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 667879
    iget-object v0, p0, LX/42i;->a:LX/42j;

    .line 667880
    iget-object p0, v0, LX/42j;->b:LX/42g;

    invoke-interface {p0, p1, p2}, LX/42g;->a(ILandroid/view/KeyEvent;)Z

    move-result p0

    move v0, p0

    .line 667881
    return v0
.end method

.method public final a(Landroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 667876
    iget-object v0, p0, LX/42i;->a:LX/42j;

    .line 667877
    iget-object p0, v0, LX/42j;->b:LX/42g;

    invoke-interface {p0, p1}, LX/42g;->a(Landroid/view/KeyEvent;)Z

    move-result p0

    move v0, p0

    .line 667878
    return v0
.end method

.method public final a(Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 667873
    iget-object v0, p0, LX/42i;->a:LX/42j;

    .line 667874
    iget-object p0, v0, LX/42j;->b:LX/42g;

    invoke-interface {p0, p1}, LX/42g;->a(Landroid/view/Menu;)Z

    move-result p0

    move v0, p0

    .line 667875
    return v0
.end method

.method public final a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 667870
    iget-object v0, p0, LX/42i;->a:LX/42j;

    .line 667871
    iget-object p0, v0, LX/42j;->b:LX/42g;

    invoke-interface {p0, p1}, LX/42g;->a(Landroid/view/MenuItem;)Z

    move-result p0

    move v0, p0

    .line 667872
    return v0
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 667888
    iget-object v0, p0, LX/42i;->a:LX/42j;

    .line 667889
    iget-object p0, v0, LX/42j;->b:LX/42g;

    invoke-interface {p0, p1}, LX/42g;->a(Landroid/view/MotionEvent;)Z

    move-result p0

    move v0, p0

    .line 667890
    return v0
.end method

.method public final a(Ljava/lang/Throwable;)Z
    .locals 1

    .prologue
    .line 667867
    iget-object v0, p0, LX/42i;->a:LX/42j;

    .line 667868
    iget-object p0, v0, LX/42j;->b:LX/42g;

    invoke-interface {p0, p1}, LX/42g;->a(Ljava/lang/Throwable;)Z

    move-result p0

    move v0, p0

    .line 667869
    return v0
.end method

.method public final b(I)Landroid/app/Dialog;
    .locals 1

    .prologue
    .line 667864
    iget-object v0, p0, LX/42i;->a:LX/42j;

    .line 667865
    iget-object p0, v0, LX/42j;->b:LX/42g;

    invoke-interface {p0, p1}, LX/42g;->b(I)Landroid/app/Dialog;

    move-result-object p0

    move-object v0, p0

    .line 667866
    return-object v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 667862
    iget-object v0, p0, LX/42i;->a:LX/42j;

    invoke-virtual {v0}, LX/42j;->h()V

    .line 667863
    return-void
.end method

.method public final b(LX/0T2;)V
    .locals 1

    .prologue
    .line 667859
    iget-object v0, p0, LX/42i;->a:LX/42j;

    .line 667860
    iget-object p0, v0, LX/42j;->b:LX/42g;

    invoke-interface {p0, p1}, LX/0ex;->b(LX/0T2;)V

    .line 667861
    return-void
.end method

.method public final b(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 667856
    iget-object v0, p0, LX/42i;->a:LX/42j;

    .line 667857
    iget-object p0, v0, LX/42j;->b:LX/42g;

    invoke-interface {p0, p1}, LX/42g;->b(Landroid/content/Intent;)V

    .line 667858
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 667810
    iget-object v0, p0, LX/42i;->a:LX/42j;

    invoke-virtual {v0, p1}, LX/42j;->b(Landroid/os/Bundle;)V

    .line 667811
    return-void
.end method

.method public final b(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 667855
    iget-object v0, p0, LX/42i;->a:LX/42j;

    invoke-virtual {v0, p1, p2}, LX/42j;->b(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public final b(Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 667760
    iget-object v0, p0, LX/42i;->a:LX/42j;

    .line 667761
    iget-object p0, v0, LX/42j;->b:LX/42g;

    invoke-interface {p0, p1}, LX/42g;->b(Landroid/view/Menu;)Z

    move-result p0

    move v0, p0

    .line 667762
    return v0
.end method

.method public final b(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 667771
    iget-object v0, p0, LX/42i;->a:LX/42j;

    .line 667772
    iget-object p0, v0, LX/42j;->b:LX/42g;

    invoke-interface {p0, p1}, LX/42g;->b(Landroid/view/MenuItem;)Z

    move-result p0

    move v0, p0

    .line 667773
    return v0
.end method

.method public final c(I)Landroid/view/View;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(I)TT;"
        }
    .end annotation

    .prologue
    .line 667770
    iget-object v0, p0, LX/42i;->a:LX/42j;

    invoke-virtual {v0, p1}, LX/42j;->c(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 667767
    iget-object v0, p0, LX/42i;->a:LX/42j;

    .line 667768
    iget-object p0, v0, LX/42j;->b:LX/42g;

    invoke-interface {p0}, LX/42g;->c()V

    .line 667769
    return-void
.end method

.method public final c(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 667765
    iget-object v0, p0, LX/42i;->a:LX/42j;

    invoke-virtual {v0, p1}, LX/42j;->c(Landroid/content/Intent;)V

    .line 667766
    return-void
.end method

.method public final c(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 667763
    iget-object v0, p0, LX/42i;->a:LX/42j;

    invoke-virtual {v0, p1}, LX/42j;->c(Landroid/os/Bundle;)V

    .line 667764
    return-void
.end method

.method public final d(I)Landroid/view/View;
    .locals 1

    .prologue
    .line 667757
    iget-object v0, p0, LX/42i;->a:LX/42j;

    .line 667758
    iget-object p0, v0, LX/42j;->b:LX/42g;

    invoke-interface {p0, p1}, LX/42g;->d(I)Landroid/view/View;

    move-result-object p0

    move-object v0, p0

    .line 667759
    return-object v0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 667755
    iget-object v0, p0, LX/42i;->a:LX/42j;

    invoke-virtual {v0}, LX/42j;->j()V

    .line 667756
    return-void
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 667753
    iget-object v0, p0, LX/42i;->a:LX/42j;

    invoke-virtual {v0, p1}, LX/42j;->d(Landroid/os/Bundle;)V

    .line 667754
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 667751
    iget-object v0, p0, LX/42i;->a:LX/42j;

    invoke-virtual {v0}, LX/42j;->k()V

    .line 667752
    return-void
.end method

.method public final e(I)V
    .locals 1

    .prologue
    .line 667749
    iget-object v0, p0, LX/42i;->a:LX/42j;

    invoke-virtual {v0, p1}, LX/42j;->e(I)V

    .line 667750
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 667746
    iget-object v0, p0, LX/42i;->a:LX/42j;

    .line 667747
    iget-object p0, v0, LX/42j;->b:LX/42g;

    invoke-interface {p0}, LX/42g;->f()V

    .line 667748
    return-void
.end method

.method public final f(I)V
    .locals 1

    .prologue
    .line 667743
    iget-object v0, p0, LX/42i;->a:LX/42j;

    .line 667744
    iget-object p0, v0, LX/42j;->b:LX/42g;

    invoke-interface {p0, p1}, LX/42g;->f(I)V

    .line 667745
    return-void
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 667740
    iget-object v0, p0, LX/42i;->a:LX/42j;

    .line 667741
    iget-object p0, v0, LX/42j;->b:LX/42g;

    invoke-interface {p0}, LX/42g;->g()V

    .line 667742
    return-void
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 667738
    iget-object v0, p0, LX/42i;->a:LX/42j;

    invoke-virtual {v0}, LX/42j;->n()V

    .line 667739
    return-void
.end method

.method public final i()V
    .locals 1

    .prologue
    .line 667774
    iget-object v0, p0, LX/42i;->a:LX/42j;

    .line 667775
    iget-object p0, v0, LX/42j;->b:LX/42g;

    invoke-interface {p0}, LX/42g;->i()V

    .line 667776
    return-void
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 667777
    iget-object v0, p0, LX/42i;->a:LX/42j;

    .line 667778
    iget-object p0, v0, LX/42j;->b:LX/42g;

    invoke-interface {p0}, LX/42g;->j()Z

    move-result p0

    move v0, p0

    .line 667779
    return v0
.end method

.method public final k()V
    .locals 1

    .prologue
    .line 667780
    iget-object v0, p0, LX/42i;->a:LX/42j;

    invoke-virtual {v0}, LX/42j;->q()V

    .line 667781
    return-void
.end method

.method public final l()V
    .locals 1

    .prologue
    .line 667782
    iget-object v0, p0, LX/42i;->a:LX/42j;

    .line 667783
    iget-object p0, v0, LX/42j;->b:LX/42g;

    invoke-interface {p0}, LX/42g;->l()V

    .line 667784
    return-void
.end method

.method public final m()V
    .locals 1

    .prologue
    .line 667785
    iget-object v0, p0, LX/42i;->a:LX/42j;

    invoke-virtual {v0}, LX/42j;->s()V

    .line 667786
    return-void
.end method

.method public final n()LX/0QA;
    .locals 1

    .prologue
    .line 667787
    iget-object v0, p0, LX/42i;->a:LX/42j;

    .line 667788
    iget-object p0, v0, LX/42j;->b:LX/42g;

    invoke-interface {p0}, LX/42g;->n()LX/0QA;

    move-result-object p0

    move-object v0, p0

    .line 667789
    return-object v0
.end method

.method public final o()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "LX/0T2;",
            ">;"
        }
    .end annotation

    .prologue
    .line 667790
    iget-object v0, p0, LX/42i;->a:LX/42j;

    .line 667791
    iget-object p0, v0, LX/42j;->b:LX/42g;

    invoke-interface {p0}, LX/42g;->o()Ljava/util/Set;

    move-result-object p0

    move-object v0, p0

    .line 667792
    return-object v0
.end method

.method public final p()V
    .locals 1

    .prologue
    .line 667793
    iget-object v0, p0, LX/42i;->a:LX/42j;

    invoke-virtual {v0}, LX/42j;->iA_()V

    .line 667794
    return-void
.end method

.method public final q()V
    .locals 1

    .prologue
    .line 667795
    iget-object v0, p0, LX/42i;->a:LX/42j;

    .line 667796
    iget-object p0, v0, LX/42j;->b:LX/42g;

    invoke-interface {p0}, LX/42g;->q()V

    .line 667797
    return-void
.end method

.method public final r()LX/0gc;
    .locals 1

    .prologue
    .line 667798
    iget-object v0, p0, LX/42i;->a:LX/42j;

    invoke-virtual {v0}, LX/42j;->iC_()LX/0gc;

    move-result-object v0

    return-object v0
.end method

.method public final s()Landroid/view/Window;
    .locals 1

    .prologue
    .line 667799
    iget-object v0, p0, LX/42i;->a:LX/42j;

    invoke-virtual {v0}, LX/42j;->mt_()Landroid/view/Window;

    move-result-object v0

    return-object v0
.end method

.method public final t()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 667800
    iget-object v0, p0, LX/42i;->a:LX/42j;

    invoke-virtual {v0}, LX/42j;->mu_()Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final u()Landroid/content/res/Resources;
    .locals 1

    .prologue
    .line 667801
    iget-object v0, p0, LX/42i;->a:LX/42j;

    invoke-virtual {v0}, LX/42j;->y()Landroid/content/res/Resources;

    move-result-object v0

    return-object v0
.end method

.method public final v()Landroid/view/MenuInflater;
    .locals 1

    .prologue
    .line 667802
    iget-object v0, p0, LX/42i;->a:LX/42j;

    .line 667803
    iget-object p0, v0, LX/42j;->b:LX/42g;

    invoke-interface {p0}, LX/42g;->v()Landroid/view/MenuInflater;

    move-result-object p0

    move-object v0, p0

    .line 667804
    return-object v0
.end method

.method public final w()Z
    .locals 1

    .prologue
    .line 667805
    iget-object v0, p0, LX/42i;->a:LX/42j;

    .line 667806
    iget-object p0, v0, LX/42j;->b:LX/42g;

    invoke-interface {p0}, LX/42g;->w()Z

    move-result p0

    move v0, p0

    .line 667807
    return v0
.end method
