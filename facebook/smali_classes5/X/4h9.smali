.class public final enum LX/4h9;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/4h9;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/4h9;

.field public static final enum FAILED:LX/4h9;

.field public static final enum MISMATCH_NOT_UPDATED:LX/4h9;

.field public static final enum MISMATCH_UPDATED:LX/4h9;

.field public static final enum NEW:LX/4h9;

.field public static final enum NEWER:LX/4h9;

.field public static final enum NO_RESPONSE:LX/4h9;

.field public static final enum NULL:LX/4h9;

.field public static final enum OLDER:LX/4h9;

.field public static final enum SAME:LX/4h9;


# instance fields
.field private mStatus:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 800503
    new-instance v0, LX/4h9;

    const-string v1, "MISMATCH_UPDATED"

    const-string v2, "mismatch_updated"

    invoke-direct {v0, v1, v4, v2}, LX/4h9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/4h9;->MISMATCH_UPDATED:LX/4h9;

    .line 800504
    new-instance v0, LX/4h9;

    const-string v1, "MISMATCH_NOT_UPDATED"

    const-string v2, "mismatch_not_updated"

    invoke-direct {v0, v1, v5, v2}, LX/4h9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/4h9;->MISMATCH_NOT_UPDATED:LX/4h9;

    .line 800505
    new-instance v0, LX/4h9;

    const-string v1, "FAILED"

    const-string v2, "failed"

    invoke-direct {v0, v1, v6, v2}, LX/4h9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/4h9;->FAILED:LX/4h9;

    .line 800506
    new-instance v0, LX/4h9;

    const-string v1, "NO_RESPONSE"

    const-string v2, "no_response"

    invoke-direct {v0, v1, v7, v2}, LX/4h9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/4h9;->NO_RESPONSE:LX/4h9;

    .line 800507
    new-instance v0, LX/4h9;

    const-string v1, "NULL"

    const-string v2, "null"

    invoke-direct {v0, v1, v8, v2}, LX/4h9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/4h9;->NULL:LX/4h9;

    .line 800508
    new-instance v0, LX/4h9;

    const-string v1, "SAME"

    const/4 v2, 0x5

    const-string v3, "same"

    invoke-direct {v0, v1, v2, v3}, LX/4h9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/4h9;->SAME:LX/4h9;

    .line 800509
    new-instance v0, LX/4h9;

    const-string v1, "OLDER"

    const/4 v2, 0x6

    const-string v3, "older"

    invoke-direct {v0, v1, v2, v3}, LX/4h9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/4h9;->OLDER:LX/4h9;

    .line 800510
    new-instance v0, LX/4h9;

    const-string v1, "NEW"

    const/4 v2, 0x7

    const-string v3, "new"

    invoke-direct {v0, v1, v2, v3}, LX/4h9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/4h9;->NEW:LX/4h9;

    .line 800511
    new-instance v0, LX/4h9;

    const-string v1, "NEWER"

    const/16 v2, 0x8

    const-string v3, "newer"

    invoke-direct {v0, v1, v2, v3}, LX/4h9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/4h9;->NEWER:LX/4h9;

    .line 800512
    const/16 v0, 0x9

    new-array v0, v0, [LX/4h9;

    sget-object v1, LX/4h9;->MISMATCH_UPDATED:LX/4h9;

    aput-object v1, v0, v4

    sget-object v1, LX/4h9;->MISMATCH_NOT_UPDATED:LX/4h9;

    aput-object v1, v0, v5

    sget-object v1, LX/4h9;->FAILED:LX/4h9;

    aput-object v1, v0, v6

    sget-object v1, LX/4h9;->NO_RESPONSE:LX/4h9;

    aput-object v1, v0, v7

    sget-object v1, LX/4h9;->NULL:LX/4h9;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/4h9;->SAME:LX/4h9;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/4h9;->OLDER:LX/4h9;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/4h9;->NEW:LX/4h9;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/4h9;->NEWER:LX/4h9;

    aput-object v2, v0, v1

    sput-object v0, LX/4h9;->$VALUES:[LX/4h9;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 800513
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 800514
    iput-object p3, p0, LX/4h9;->mStatus:Ljava/lang/String;

    .line 800515
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/4h9;
    .locals 1

    .prologue
    .line 800502
    const-class v0, LX/4h9;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/4h9;

    return-object v0
.end method

.method public static values()[LX/4h9;
    .locals 1

    .prologue
    .line 800501
    sget-object v0, LX/4h9;->$VALUES:[LX/4h9;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/4h9;

    return-object v0
.end method


# virtual methods
.method public final getStatus()Ljava/lang/String;
    .locals 1

    .prologue
    .line 800500
    iget-object v0, p0, LX/4h9;->mStatus:Ljava/lang/String;

    return-object v0
.end method
