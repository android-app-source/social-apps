.class public LX/44w;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T0:",
        "Ljava/lang/Object;",
        "T1:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT0;"
        }
    .end annotation
.end field

.field public final b:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT1;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT0;TT1;)V"
        }
    .end annotation

    .prologue
    .line 670045
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 670046
    iput-object p1, p0, LX/44w;->a:Ljava/lang/Object;

    .line 670047
    iput-object p2, p0, LX/44w;->b:Ljava/lang/Object;

    .line 670048
    return-void
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/Object;)LX/44w;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T0:",
            "Ljava/lang/Object;",
            "T1:",
            "Ljava/lang/Object;",
            ">(TT0;TT1;)",
            "LX/44w",
            "<TT0;TT1;>;"
        }
    .end annotation

    .prologue
    .line 670049
    new-instance v0, LX/44w;

    invoke-direct {v0, p0, p1}, LX/44w;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 670050
    if-eqz p1, :cond_0

    instance-of v1, p1, LX/44w;

    if-nez v1, :cond_1

    .line 670051
    :cond_0
    :goto_0
    return v0

    .line 670052
    :cond_1
    check-cast p1, LX/44w;

    .line 670053
    iget-object v1, p0, LX/44w;->a:Ljava/lang/Object;

    iget-object v2, p1, LX/44w;->a:Ljava/lang/Object;

    if-eq v1, v2, :cond_2

    iget-object v1, p0, LX/44w;->a:Ljava/lang/Object;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/44w;->a:Ljava/lang/Object;

    iget-object v2, p1, LX/44w;->a:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 670054
    :cond_2
    iget-object v1, p0, LX/44w;->b:Ljava/lang/Object;

    iget-object v2, p1, LX/44w;->b:Ljava/lang/Object;

    if-eq v1, v2, :cond_3

    iget-object v1, p0, LX/44w;->b:Ljava/lang/Object;

    if-eqz v1, :cond_3

    iget-object v1, p0, LX/44w;->b:Ljava/lang/Object;

    iget-object v2, p1, LX/44w;->b:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 670055
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 670056
    const/4 v0, 0x0

    .line 670057
    iget-object v1, p0, LX/44w;->a:Ljava/lang/Object;

    if-eqz v1, :cond_0

    .line 670058
    iget-object v0, p0, LX/44w;->a:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    xor-int/lit8 v0, v0, 0x0

    .line 670059
    :cond_0
    iget-object v1, p0, LX/44w;->b:Ljava/lang/Object;

    if-eqz v1, :cond_1

    .line 670060
    iget-object v1, p0, LX/44w;->b:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 670061
    :cond_1
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 670062
    const-string v0, "<"

    .line 670063
    iget-object v1, p0, LX/44w;->a:Ljava/lang/Object;

    if-eqz v1, :cond_0

    .line 670064
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/44w;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 670065
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 670066
    iget-object v1, p0, LX/44w;->b:Ljava/lang/Object;

    if-eqz v1, :cond_1

    .line 670067
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/44w;->b:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 670068
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 670069
    return-object v0
.end method
