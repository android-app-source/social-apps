.class public LX/4l6;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "Lcom/facebook/quicklog/StatsLogger;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/4l4;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 803740
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/4l4;
    .locals 3

    .prologue
    .line 803727
    sget-object v0, LX/4l6;->a:LX/4l4;

    if-nez v0, :cond_1

    .line 803728
    const-class v1, LX/4l6;

    monitor-enter v1

    .line 803729
    :try_start_0
    sget-object v0, LX/4l6;->a:LX/4l4;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 803730
    if-eqz v2, :cond_0

    .line 803731
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 803732
    invoke-static {v0}, LX/1B6;->a(LX/0QB;)LX/1BA;

    move-result-object p0

    check-cast p0, LX/1BA;

    invoke-static {p0}, LX/0Xd;->a(LX/1BA;)LX/4l4;

    move-result-object p0

    move-object v0, p0

    .line 803733
    sput-object v0, LX/4l6;->a:LX/4l4;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 803734
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 803735
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 803736
    :cond_1
    sget-object v0, LX/4l6;->a:LX/4l4;

    return-object v0

    .line 803737
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 803738
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 803739
    invoke-static {p0}, LX/1B6;->a(LX/0QB;)LX/1BA;

    move-result-object v0

    check-cast v0, LX/1BA;

    invoke-static {v0}, LX/0Xd;->a(LX/1BA;)LX/4l4;

    move-result-object v0

    return-object v0
.end method
