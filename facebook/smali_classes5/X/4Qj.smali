.class public LX/4Qj;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 705993
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 705962
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_9

    .line 705963
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 705964
    :goto_0
    return v1

    .line 705965
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 705966
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_8

    .line 705967
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 705968
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 705969
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_1

    if-eqz v8, :cond_1

    .line 705970
    const-string v9, "content_set"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 705971
    invoke-static {p0, p1}, LX/4Ql;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 705972
    :cond_2
    const-string v9, "hideable_token"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 705973
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 705974
    :cond_3
    const-string v9, "like_sentence"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 705975
    invoke-static {p0, p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 705976
    :cond_4
    const-string v9, "node"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 705977
    invoke-static {p0, p1}, LX/2bc;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 705978
    :cond_5
    const-string v9, "social_context"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 705979
    invoke-static {p0, p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 705980
    :cond_6
    const-string v9, "sponsored_data"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 705981
    invoke-static {p0, p1}, LX/2uY;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 705982
    :cond_7
    const-string v9, "tracking"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 705983
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 705984
    :cond_8
    const/4 v8, 0x7

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 705985
    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 705986
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 705987
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 705988
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 705989
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 705990
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 705991
    const/4 v1, 0x6

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 705992
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_9
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 705931
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 705932
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 705933
    if-eqz v0, :cond_0

    .line 705934
    const-string v1, "content_set"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 705935
    invoke-static {p0, v0, p2, p3}, LX/4Ql;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 705936
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 705937
    if-eqz v0, :cond_1

    .line 705938
    const-string v1, "hideable_token"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 705939
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 705940
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 705941
    if-eqz v0, :cond_2

    .line 705942
    const-string v1, "like_sentence"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 705943
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 705944
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 705945
    if-eqz v0, :cond_3

    .line 705946
    const-string v1, "node"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 705947
    invoke-static {p0, v0, p2, p3}, LX/2bc;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 705948
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 705949
    if-eqz v0, :cond_4

    .line 705950
    const-string v1, "social_context"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 705951
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 705952
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 705953
    if-eqz v0, :cond_5

    .line 705954
    const-string v1, "sponsored_data"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 705955
    invoke-static {p0, v0, p2, p3}, LX/2uY;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 705956
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 705957
    if-eqz v0, :cond_6

    .line 705958
    const-string v1, "tracking"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 705959
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 705960
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 705961
    return-void
.end method
