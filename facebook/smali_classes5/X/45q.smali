.class public final LX/45q;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/support/annotation/VisibleForTesting;
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Lcom/google/android/gms/gcm/Task;

.field public final c:I


# direct methods
.method public constructor <init>(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 670608
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 670609
    const-string v0, "job_tag"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 670610
    if-nez v0, :cond_0

    .line 670611
    new-instance v0, LX/45s;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid job_tag: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "job_tag"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/45s;-><init>(Ljava/lang/String;)V

    throw v0

    .line 670612
    :cond_0
    iput-object v0, p0, LX/45q;->a:Ljava/lang/String;

    .line 670613
    const-string v0, "task"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/gcm/Task;

    .line 670614
    if-nez v0, :cond_1

    .line 670615
    new-instance v0, LX/45s;

    const-string v1, "Missing task"

    invoke-direct {v0, v1}, LX/45s;-><init>(Ljava/lang/String;)V

    throw v0

    .line 670616
    :cond_1
    const-string v1, "num_failures"

    const/4 v2, -0x1

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, LX/45q;->c:I

    .line 670617
    iget v1, p0, LX/45q;->c:I

    if-gtz v1, :cond_2

    .line 670618
    new-instance v0, LX/45s;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "invalid num_failures: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, LX/45q;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/45s;-><init>(Ljava/lang/String;)V

    throw v0

    .line 670619
    :cond_2
    iput-object v0, p0, LX/45q;->b:Lcom/google/android/gms/gcm/Task;

    .line 670620
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/gcm/Task;I)V
    .locals 1

    .prologue
    .line 670621
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 670622
    iget-object v0, p1, Lcom/google/android/gms/gcm/Task;->b:Ljava/lang/String;

    move-object v0, v0

    .line 670623
    iput-object v0, p0, LX/45q;->a:Ljava/lang/String;

    .line 670624
    iput-object p1, p0, LX/45q;->b:Lcom/google/android/gms/gcm/Task;

    .line 670625
    iput p2, p0, LX/45q;->c:I

    .line 670626
    return-void
.end method
