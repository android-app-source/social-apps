.class public LX/4Lg;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 684665
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 43

    .prologue
    .line 684666
    const/16 v38, 0x0

    .line 684667
    const/16 v37, 0x0

    .line 684668
    const/16 v36, 0x0

    .line 684669
    const/16 v35, 0x0

    .line 684670
    const/16 v34, 0x0

    .line 684671
    const-wide/16 v32, 0x0

    .line 684672
    const/16 v31, 0x0

    .line 684673
    const/16 v30, 0x0

    .line 684674
    const/16 v27, 0x0

    .line 684675
    const-wide/16 v28, 0x0

    .line 684676
    const/16 v26, 0x0

    .line 684677
    const/16 v25, 0x0

    .line 684678
    const/16 v24, 0x0

    .line 684679
    const/16 v23, 0x0

    .line 684680
    const/16 v22, 0x0

    .line 684681
    const/16 v21, 0x0

    .line 684682
    const/16 v20, 0x0

    .line 684683
    const/16 v19, 0x0

    .line 684684
    const/16 v18, 0x0

    .line 684685
    const/16 v17, 0x0

    .line 684686
    const/16 v16, 0x0

    .line 684687
    const/4 v15, 0x0

    .line 684688
    const/4 v14, 0x0

    .line 684689
    const/4 v13, 0x0

    .line 684690
    const/4 v12, 0x0

    .line 684691
    const/4 v11, 0x0

    .line 684692
    const/4 v10, 0x0

    .line 684693
    const/4 v9, 0x0

    .line 684694
    const/4 v8, 0x0

    .line 684695
    const/4 v7, 0x0

    .line 684696
    const/4 v6, 0x0

    .line 684697
    const/4 v5, 0x0

    .line 684698
    const/4 v4, 0x0

    .line 684699
    const/4 v3, 0x0

    .line 684700
    const/4 v2, 0x0

    .line 684701
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v39

    sget-object v40, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v39

    move-object/from16 v1, v40

    if-eq v0, v1, :cond_25

    .line 684702
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 684703
    const/4 v2, 0x0

    .line 684704
    :goto_0
    return v2

    .line 684705
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_20

    .line 684706
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 684707
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 684708
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_0

    if-eqz v2, :cond_0

    .line 684709
    const-string v6, "action_links"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 684710
    invoke-static/range {p0 .. p1}, LX/2bb;->b(LX/15w;LX/186;)I

    move-result v2

    move/from16 v41, v2

    goto :goto_1

    .line 684711
    :cond_1
    const-string v6, "actors"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 684712
    invoke-static/range {p0 .. p1}, LX/2bM;->b(LX/15w;LX/186;)I

    move-result v2

    move/from16 v40, v2

    goto :goto_1

    .line 684713
    :cond_2
    const-string v6, "app_icon"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 684714
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v39, v2

    goto :goto_1

    .line 684715
    :cond_3
    const-string v6, "attachments"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 684716
    invoke-static/range {p0 .. p1}, LX/2as;->b(LX/15w;LX/186;)I

    move-result v2

    move/from16 v38, v2

    goto :goto_1

    .line 684717
    :cond_4
    const-string v6, "cache_id"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 684718
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v37, v2

    goto :goto_1

    .line 684719
    :cond_5
    const-string v6, "creation_time"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 684720
    const/4 v2, 0x1

    .line 684721
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 684722
    :cond_6
    const-string v6, "debug_info"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 684723
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v36, v2

    goto/16 :goto_1

    .line 684724
    :cond_7
    const-string v6, "feedback"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 684725
    invoke-static/range {p0 .. p1}, LX/2bG;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v35, v2

    goto/16 :goto_1

    .line 684726
    :cond_8
    const-string v6, "feedback_context"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 684727
    invoke-static/range {p0 .. p1}, LX/2aq;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v34, v2

    goto/16 :goto_1

    .line 684728
    :cond_9
    const-string v6, "fetchTimeMs"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 684729
    const/4 v2, 0x1

    .line 684730
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v6

    move v10, v2

    move-wide/from16 v32, v6

    goto/16 :goto_1

    .line 684731
    :cond_a
    const-string v6, "hideable_token"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 684732
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v31, v2

    goto/16 :goto_1

    .line 684733
    :cond_b
    const-string v6, "id"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 684734
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v30, v2

    goto/16 :goto_1

    .line 684735
    :cond_c
    const-string v6, "local_last_negative_feedback_action_type"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_d

    .line 684736
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v29, v2

    goto/16 :goto_1

    .line 684737
    :cond_d
    const-string v6, "local_story_visibility"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_e

    .line 684738
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v28, v2

    goto/16 :goto_1

    .line 684739
    :cond_e
    const-string v6, "local_story_visible_height"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_f

    .line 684740
    const/4 v2, 0x1

    .line 684741
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move v9, v2

    move/from16 v27, v6

    goto/16 :goto_1

    .line 684742
    :cond_f
    const-string v6, "message"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_10

    .line 684743
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v26, v2

    goto/16 :goto_1

    .line 684744
    :cond_10
    const-string v6, "negative_feedback_actions"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_11

    .line 684745
    invoke-static/range {p0 .. p1}, LX/2bY;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v25, v2

    goto/16 :goto_1

    .line 684746
    :cond_11
    const-string v6, "privacy_scope"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_12

    .line 684747
    invoke-static/range {p0 .. p1}, LX/2bo;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v24, v2

    goto/16 :goto_1

    .line 684748
    :cond_12
    const-string v6, "seen_state"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_13

    .line 684749
    const/4 v2, 0x1

    .line 684750
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v6

    move v8, v2

    move-object/from16 v23, v6

    goto/16 :goto_1

    .line 684751
    :cond_13
    const-string v6, "shareable"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_14

    .line 684752
    invoke-static/range {p0 .. p1}, LX/2ap;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v22, v2

    goto/16 :goto_1

    .line 684753
    :cond_14
    const-string v6, "short_term_cache_key"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_15

    .line 684754
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v21, v2

    goto/16 :goto_1

    .line 684755
    :cond_15
    const-string v6, "social_context"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_16

    .line 684756
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v20, v2

    goto/16 :goto_1

    .line 684757
    :cond_16
    const-string v6, "story_header"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_17

    .line 684758
    invoke-static/range {p0 .. p1}, LX/32O;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v19, v2

    goto/16 :goto_1

    .line 684759
    :cond_17
    const-string v6, "story_title"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_18

    .line 684760
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v18, v2

    goto/16 :goto_1

    .line 684761
    :cond_18
    const-string v6, "substories_grouping_reasons"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_19

    .line 684762
    const-class v2, Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v2}, LX/2gu;->a(LX/15w;LX/186;Ljava/lang/Class;)I

    move-result v2

    move/from16 v17, v2

    goto/16 :goto_1

    .line 684763
    :cond_19
    const-string v6, "subtitle"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1a

    .line 684764
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v16, v2

    goto/16 :goto_1

    .line 684765
    :cond_1a
    const-string v6, "title"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1b

    .line 684766
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v2

    move v15, v2

    goto/16 :goto_1

    .line 684767
    :cond_1b
    const-string v6, "titleForSummary"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1c

    .line 684768
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v2

    move v14, v2

    goto/16 :goto_1

    .line 684769
    :cond_1c
    const-string v6, "tracking"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1d

    .line 684770
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v13, v2

    goto/16 :goto_1

    .line 684771
    :cond_1d
    const-string v6, "url"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1e

    .line 684772
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v12, v2

    goto/16 :goto_1

    .line 684773
    :cond_1e
    const-string v6, "native_template_view"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1f

    .line 684774
    invoke-static/range {p0 .. p1}, LX/4Pn;->a(LX/15w;LX/186;)I

    move-result v2

    move v11, v2

    goto/16 :goto_1

    .line 684775
    :cond_1f
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 684776
    :cond_20
    const/16 v2, 0x20

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 684777
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 684778
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v40

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 684779
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 684780
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 684781
    const/4 v2, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 684782
    if-eqz v3, :cond_21

    .line 684783
    const/4 v3, 0x6

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 684784
    :cond_21
    const/4 v2, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 684785
    const/16 v2, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 684786
    const/16 v2, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 684787
    if-eqz v10, :cond_22

    .line 684788
    const/16 v3, 0xa

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v32

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 684789
    :cond_22
    const/16 v2, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 684790
    const/16 v2, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 684791
    const/16 v2, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 684792
    const/16 v2, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 684793
    if-eqz v9, :cond_23

    .line 684794
    const/16 v2, 0xf

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 684795
    :cond_23
    const/16 v2, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 684796
    const/16 v2, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 684797
    const/16 v2, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 684798
    if-eqz v8, :cond_24

    .line 684799
    const/16 v2, 0x13

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 684800
    :cond_24
    const/16 v2, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 684801
    const/16 v2, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 684802
    const/16 v2, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 684803
    const/16 v2, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 684804
    const/16 v2, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 684805
    const/16 v2, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 684806
    const/16 v2, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 684807
    const/16 v2, 0x1b

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 684808
    const/16 v2, 0x1c

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 684809
    const/16 v2, 0x1d

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 684810
    const/16 v2, 0x1e

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 684811
    const/16 v2, 0x1f

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 684812
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_25
    move/from16 v39, v36

    move/from16 v40, v37

    move/from16 v41, v38

    move/from16 v36, v31

    move/from16 v37, v34

    move/from16 v38, v35

    move/from16 v31, v26

    move/from16 v34, v27

    move/from16 v35, v30

    move/from16 v26, v21

    move/from16 v27, v22

    move/from16 v30, v25

    move/from16 v21, v16

    move/from16 v22, v17

    move/from16 v25, v20

    move/from16 v20, v15

    move/from16 v16, v11

    move/from16 v17, v12

    move v15, v10

    move v11, v6

    move v12, v7

    move v10, v4

    move/from16 v42, v24

    move/from16 v24, v19

    move/from16 v19, v14

    move v14, v9

    move v9, v3

    move v3, v5

    move-wide/from16 v4, v32

    move-wide/from16 v32, v28

    move/from16 v29, v42

    move/from16 v28, v23

    move-object/from16 v23, v18

    move/from16 v18, v13

    move v13, v8

    move v8, v2

    goto/16 :goto_1
.end method

.method public static a(LX/15w;S)LX/15i;
    .locals 5

    .prologue
    .line 684523
    const/4 v0, 0x1

    const/4 v4, 0x0

    .line 684524
    new-instance v2, LX/186;

    const/16 v1, 0x80

    invoke-direct {v2, v1}, LX/186;-><init>(I)V

    .line 684525
    invoke-static {p0, v2}, LX/4Lg;->a(LX/15w;LX/186;)I

    move-result v1

    .line 684526
    if-eqz v0, :cond_0

    .line 684527
    const/4 v3, 0x2

    invoke-virtual {v2, v3}, LX/186;->c(I)V

    .line 684528
    invoke-virtual {v2, v4, p1, v4}, LX/186;->a(ISI)V

    .line 684529
    const/4 v3, 0x1

    invoke-virtual {v2, v3, v1}, LX/186;->b(II)V

    .line 684530
    invoke-virtual {v2}, LX/186;->d()I

    move-result v1

    .line 684531
    :cond_0
    invoke-virtual {v2, v1}, LX/186;->d(I)V

    .line 684532
    invoke-static {v2}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v1

    move-object v0, v1

    .line 684533
    return-object v0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 8

    .prologue
    const/16 v7, 0x19

    const/16 v6, 0x13

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    .line 684534
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 684535
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 684536
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 684537
    const-string v0, "name"

    const-string v1, "CustomizedStory"

    invoke-virtual {p2, v0, v1}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 684538
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 684539
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 684540
    if-eqz v0, :cond_0

    .line 684541
    const-string v1, "action_links"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 684542
    invoke-static {p0, v0, p2, p3}, LX/2bb;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 684543
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 684544
    if-eqz v0, :cond_1

    .line 684545
    const-string v1, "actors"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 684546
    invoke-static {p0, v0, p2, p3}, LX/2bM;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 684547
    :cond_1
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 684548
    if-eqz v0, :cond_2

    .line 684549
    const-string v1, "app_icon"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 684550
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 684551
    :cond_2
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 684552
    if-eqz v0, :cond_3

    .line 684553
    const-string v1, "attachments"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 684554
    invoke-static {p0, v0, p2, p3}, LX/2as;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 684555
    :cond_3
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 684556
    if-eqz v0, :cond_4

    .line 684557
    const-string v1, "cache_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 684558
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 684559
    :cond_4
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 684560
    cmp-long v2, v0, v4

    if-eqz v2, :cond_5

    .line 684561
    const-string v2, "creation_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 684562
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 684563
    :cond_5
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 684564
    if-eqz v0, :cond_6

    .line 684565
    const-string v1, "debug_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 684566
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 684567
    :cond_6
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 684568
    if-eqz v0, :cond_7

    .line 684569
    const-string v1, "feedback"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 684570
    invoke-static {p0, v0, p2, p3}, LX/2bG;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 684571
    :cond_7
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 684572
    if-eqz v0, :cond_8

    .line 684573
    const-string v1, "feedback_context"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 684574
    invoke-static {p0, v0, p2, p3}, LX/2aq;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 684575
    :cond_8
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 684576
    cmp-long v2, v0, v4

    if-eqz v2, :cond_9

    .line 684577
    const-string v2, "fetchTimeMs"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 684578
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 684579
    :cond_9
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 684580
    if-eqz v0, :cond_a

    .line 684581
    const-string v1, "hideable_token"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 684582
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 684583
    :cond_a
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 684584
    if-eqz v0, :cond_b

    .line 684585
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 684586
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 684587
    :cond_b
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 684588
    if-eqz v0, :cond_c

    .line 684589
    const-string v1, "local_last_negative_feedback_action_type"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 684590
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 684591
    :cond_c
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 684592
    if-eqz v0, :cond_d

    .line 684593
    const-string v1, "local_story_visibility"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 684594
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 684595
    :cond_d
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 684596
    if-eqz v0, :cond_e

    .line 684597
    const-string v1, "local_story_visible_height"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 684598
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 684599
    :cond_e
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 684600
    if-eqz v0, :cond_f

    .line 684601
    const-string v1, "message"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 684602
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 684603
    :cond_f
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 684604
    if-eqz v0, :cond_10

    .line 684605
    const-string v1, "negative_feedback_actions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 684606
    invoke-static {p0, v0, p2, p3}, LX/2bY;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 684607
    :cond_10
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 684608
    if-eqz v0, :cond_11

    .line 684609
    const-string v1, "privacy_scope"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 684610
    invoke-static {p0, v0, p2, p3}, LX/2bo;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 684611
    :cond_11
    invoke-virtual {p0, p1, v6, v3}, LX/15i;->a(IIS)S

    move-result v0

    .line 684612
    if-eqz v0, :cond_12

    .line 684613
    const-string v0, "seen_state"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 684614
    const-class v0, Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-virtual {p0, p1, v6, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 684615
    :cond_12
    const/16 v0, 0x14

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 684616
    if-eqz v0, :cond_13

    .line 684617
    const-string v1, "shareable"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 684618
    invoke-static {p0, v0, p2, p3}, LX/2ap;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 684619
    :cond_13
    const/16 v0, 0x15

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 684620
    if-eqz v0, :cond_14

    .line 684621
    const-string v1, "short_term_cache_key"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 684622
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 684623
    :cond_14
    const/16 v0, 0x16

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 684624
    if-eqz v0, :cond_15

    .line 684625
    const-string v1, "social_context"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 684626
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 684627
    :cond_15
    const/16 v0, 0x17

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 684628
    if-eqz v0, :cond_16

    .line 684629
    const-string v1, "story_header"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 684630
    invoke-static {p0, v0, p2, p3}, LX/32O;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 684631
    :cond_16
    const/16 v0, 0x18

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 684632
    if-eqz v0, :cond_17

    .line 684633
    const-string v1, "story_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 684634
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 684635
    :cond_17
    invoke-virtual {p0, p1, v7}, LX/15i;->g(II)I

    move-result v0

    .line 684636
    if-eqz v0, :cond_18

    .line 684637
    const-string v0, "substories_grouping_reasons"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 684638
    const-class v0, Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;

    invoke-virtual {p0, p1, v7, v0}, LX/15i;->b(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->b(Ljava/util/Iterator;LX/0nX;)V

    .line 684639
    :cond_18
    const/16 v0, 0x1a

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 684640
    if-eqz v0, :cond_19

    .line 684641
    const-string v1, "subtitle"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 684642
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 684643
    :cond_19
    const/16 v0, 0x1b

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 684644
    if-eqz v0, :cond_1a

    .line 684645
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 684646
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 684647
    :cond_1a
    const/16 v0, 0x1c

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 684648
    if-eqz v0, :cond_1b

    .line 684649
    const-string v1, "titleForSummary"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 684650
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 684651
    :cond_1b
    const/16 v0, 0x1d

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 684652
    if-eqz v0, :cond_1c

    .line 684653
    const-string v1, "tracking"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 684654
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 684655
    :cond_1c
    const/16 v0, 0x1e

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 684656
    if-eqz v0, :cond_1d

    .line 684657
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 684658
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 684659
    :cond_1d
    const/16 v0, 0x1f

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 684660
    if-eqz v0, :cond_1e

    .line 684661
    const-string v1, "native_template_view"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 684662
    invoke-static {p0, v0, p2, p3}, LX/4Pn;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 684663
    :cond_1e
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 684664
    return-void
.end method
