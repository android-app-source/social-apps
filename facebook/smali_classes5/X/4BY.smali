.class public LX/4BY;
.super LX/2EJ;
.source ""


# instance fields
.field public b:Landroid/widget/ProgressBar;

.field private c:Landroid/widget/TextView;

.field public d:I

.field public e:Landroid/widget/TextView;

.field public f:Ljava/lang/String;

.field public g:Landroid/widget/TextView;

.field public h:Ljava/text/NumberFormat;

.field private i:I

.field private j:I

.field public k:I

.field public l:I

.field public m:I

.field public n:Landroid/graphics/drawable/Drawable;

.field public o:Landroid/graphics/drawable/Drawable;

.field private p:Ljava/lang/CharSequence;

.field private q:Z

.field public r:Landroid/graphics/ColorFilter;

.field private s:Z

.field private t:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 677981
    invoke-direct {p0, p1}, LX/2EJ;-><init>(Landroid/content/Context;)V

    .line 677982
    const/4 v0, 0x0

    iput v0, p0, LX/4BY;->d:I

    .line 677983
    const-string v0, "%1d/%2d"

    iput-object v0, p0, LX/4BY;->f:Ljava/lang/String;

    .line 677984
    invoke-static {}, Ljava/text/NumberFormat;->getPercentInstance()Ljava/text/NumberFormat;

    move-result-object v0

    iput-object v0, p0, LX/4BY;->h:Ljava/text/NumberFormat;

    .line 677985
    iget-object v0, p0, LX/4BY;->h:Ljava/text/NumberFormat;

    const/4 p1, 0x0

    invoke-virtual {v0, p1}, Ljava/text/NumberFormat;->setMaximumFractionDigits(I)V

    .line 677986
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)LX/4BY;
    .locals 6

    .prologue
    .line 677987
    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    invoke-static/range {v0 .. v5}, LX/4BY;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZLandroid/content/DialogInterface$OnCancelListener;)LX/4BY;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZ)LX/4BY;
    .locals 6

    .prologue
    .line 677996
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    invoke-static/range {v0 .. v5}, LX/4BY;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZLandroid/content/DialogInterface$OnCancelListener;)LX/4BY;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZLandroid/content/DialogInterface$OnCancelListener;)LX/4BY;
    .locals 1

    .prologue
    .line 677988
    new-instance v0, LX/4BY;

    invoke-direct {v0, p0}, LX/4BY;-><init>(Landroid/content/Context;)V

    .line 677989
    invoke-virtual {v0, p1}, LX/4BY;->setTitle(Ljava/lang/CharSequence;)V

    .line 677990
    invoke-virtual {v0, p2}, LX/2EJ;->a(Ljava/lang/CharSequence;)V

    .line 677991
    invoke-virtual {v0, p3}, LX/4BY;->a(Z)V

    .line 677992
    invoke-virtual {v0, p4}, LX/4BY;->setCancelable(Z)V

    .line 677993
    invoke-virtual {v0, p5}, LX/4BY;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 677994
    invoke-virtual {v0}, LX/4BY;->show()V

    .line 677995
    return-object v0
.end method

.method public static c(LX/4BY;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 677997
    iget v0, p0, LX/4BY;->d:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 677998
    iget-object v0, p0, LX/4BY;->t:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/4BY;->t:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 677999
    iget-object v0, p0, LX/4BY;->t:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 678000
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 677972
    iget-object v0, p0, LX/4BY;->b:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_1

    .line 677973
    iget v0, p0, LX/4BY;->d:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 677974
    invoke-super {p0, p1}, LX/2EJ;->a(Ljava/lang/CharSequence;)V

    .line 677975
    :goto_0
    return-void

    .line 677976
    :cond_0
    iget-object v0, p0, LX/4BY;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 677977
    :cond_1
    iput-object p1, p0, LX/4BY;->p:Ljava/lang/CharSequence;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 677978
    iput-object p1, p0, LX/4BY;->f:Ljava/lang/String;

    .line 677979
    invoke-static {p0}, LX/4BY;->c(LX/4BY;)V

    .line 677980
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 677968
    iget-object v0, p0, LX/4BY;->b:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    .line 677969
    iget-object v0, p0, LX/4BY;->b:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 677970
    :goto_0
    return-void

    .line 677971
    :cond_0
    iput-boolean p1, p0, LX/4BY;->q:Z

    goto :goto_0
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 677963
    iget-boolean v0, p0, LX/4BY;->s:Z

    if-eqz v0, :cond_0

    .line 677964
    iget-object v0, p0, LX/4BY;->b:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 677965
    invoke-static {p0}, LX/4BY;->c(LX/4BY;)V

    .line 677966
    :goto_0
    return-void

    .line 677967
    :cond_0
    iput p1, p0, LX/4BY;->j:I

    goto :goto_0
.end method

.method public final c(I)V
    .locals 1

    .prologue
    .line 677958
    iget-object v0, p0, LX/4BY;->b:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    .line 677959
    iget-object v0, p0, LX/4BY;->b:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 677960
    invoke-static {p0}, LX/4BY;->c(LX/4BY;)V

    .line 677961
    :goto_0
    return-void

    .line 677962
    :cond_0
    iput p1, p0, LX/4BY;->i:I

    goto :goto_0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 677901
    invoke-virtual {p0}, LX/4BY;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 677902
    invoke-virtual {p0}, LX/4BY;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, LX/03r;->AlertDialog:[I

    const v3, 0x7f010221

    const/4 v4, 0x0

    invoke-virtual {v1, v5, v2, v3, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 677903
    iget v2, p0, LX/4BY;->d:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_a

    .line 677904
    new-instance v2, LX/4BX;

    invoke-direct {v2, p0}, LX/4BX;-><init>(LX/4BY;)V

    iput-object v2, p0, LX/4BY;->t:Landroid/os/Handler;

    .line 677905
    const/16 v2, 0x6

    const v3, 0x7f031434

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    invoke-virtual {v0, v2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 677906
    const v0, 0x7f0d054d

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, LX/4BY;->b:Landroid/widget/ProgressBar;

    .line 677907
    const v0, 0x7f0d10d9

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/4BY;->e:Landroid/widget/TextView;

    .line 677908
    const v0, 0x7f0d10d8

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/4BY;->g:Landroid/widget/TextView;

    .line 677909
    invoke-virtual {p0, v2}, LX/2EJ;->a(Landroid/view/View;)V

    .line 677910
    :goto_0
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 677911
    iget v0, p0, LX/4BY;->i:I

    if-lez v0, :cond_0

    .line 677912
    iget v0, p0, LX/4BY;->i:I

    invoke-virtual {p0, v0}, LX/4BY;->c(I)V

    .line 677913
    :cond_0
    iget v0, p0, LX/4BY;->j:I

    if-lez v0, :cond_1

    .line 677914
    iget v0, p0, LX/4BY;->j:I

    invoke-virtual {p0, v0}, LX/4BY;->b(I)V

    .line 677915
    :cond_1
    iget v0, p0, LX/4BY;->k:I

    if-lez v0, :cond_2

    .line 677916
    iget v0, p0, LX/4BY;->k:I

    .line 677917
    iget-object v1, p0, LX/4BY;->b:Landroid/widget/ProgressBar;

    if-eqz v1, :cond_b

    .line 677918
    iget-object v1, p0, LX/4BY;->b:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setSecondaryProgress(I)V

    .line 677919
    invoke-static {p0}, LX/4BY;->c(LX/4BY;)V

    .line 677920
    :cond_2
    :goto_1
    iget v0, p0, LX/4BY;->l:I

    if-lez v0, :cond_3

    .line 677921
    iget v0, p0, LX/4BY;->l:I

    .line 677922
    iget-object v1, p0, LX/4BY;->b:Landroid/widget/ProgressBar;

    if-eqz v1, :cond_c

    .line 677923
    iget-object v1, p0, LX/4BY;->b:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->incrementProgressBy(I)V

    .line 677924
    invoke-static {p0}, LX/4BY;->c(LX/4BY;)V

    .line 677925
    :cond_3
    :goto_2
    iget v0, p0, LX/4BY;->m:I

    if-lez v0, :cond_4

    .line 677926
    iget v0, p0, LX/4BY;->m:I

    .line 677927
    iget-object v1, p0, LX/4BY;->b:Landroid/widget/ProgressBar;

    if-eqz v1, :cond_d

    .line 677928
    iget-object v1, p0, LX/4BY;->b:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->incrementSecondaryProgressBy(I)V

    .line 677929
    invoke-static {p0}, LX/4BY;->c(LX/4BY;)V

    .line 677930
    :cond_4
    :goto_3
    iget-object v0, p0, LX/4BY;->n:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_5

    .line 677931
    iget-object v0, p0, LX/4BY;->n:Landroid/graphics/drawable/Drawable;

    .line 677932
    iget-object v1, p0, LX/4BY;->b:Landroid/widget/ProgressBar;

    if-eqz v1, :cond_e

    .line 677933
    iget-object v1, p0, LX/4BY;->b:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setProgressDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 677934
    :cond_5
    :goto_4
    iget-object v0, p0, LX/4BY;->o:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_6

    .line 677935
    iget-object v0, p0, LX/4BY;->o:Landroid/graphics/drawable/Drawable;

    .line 677936
    iget-object v1, p0, LX/4BY;->b:Landroid/widget/ProgressBar;

    if-eqz v1, :cond_f

    .line 677937
    iget-object v1, p0, LX/4BY;->b:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setIndeterminateDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 677938
    :cond_6
    :goto_5
    iget-object v0, p0, LX/4BY;->p:Ljava/lang/CharSequence;

    if-eqz v0, :cond_7

    .line 677939
    iget-object v0, p0, LX/4BY;->p:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, LX/2EJ;->a(Ljava/lang/CharSequence;)V

    .line 677940
    :cond_7
    iget-boolean v0, p0, LX/4BY;->q:Z

    invoke-virtual {p0, v0}, LX/4BY;->a(Z)V

    .line 677941
    iget-object v0, p0, LX/4BY;->r:Landroid/graphics/ColorFilter;

    if-eqz v0, :cond_9

    .line 677942
    iget-object v0, p0, LX/4BY;->r:Landroid/graphics/ColorFilter;

    .line 677943
    iget-object v1, p0, LX/4BY;->b:Landroid/widget/ProgressBar;

    if-eqz v1, :cond_8

    iget-object v1, p0, LX/4BY;->b:Landroid/widget/ProgressBar;

    invoke-virtual {v1}, Landroid/widget/ProgressBar;->getIndeterminateDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_8

    .line 677944
    iget-object v1, p0, LX/4BY;->b:Landroid/widget/ProgressBar;

    invoke-virtual {v1}, Landroid/widget/ProgressBar;->getIndeterminateDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 677945
    :cond_8
    iput-object v0, p0, LX/4BY;->r:Landroid/graphics/ColorFilter;

    .line 677946
    :cond_9
    invoke-static {p0}, LX/4BY;->c(LX/4BY;)V

    .line 677947
    invoke-super {p0, p1}, LX/2EJ;->onCreate(Landroid/os/Bundle;)V

    .line 677948
    return-void

    .line 677949
    :cond_a
    const/16 v2, 0x5

    const v3, 0x7f031435

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    invoke-virtual {v0, v2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 677950
    const v0, 0x7f0d054d

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, LX/4BY;->b:Landroid/widget/ProgressBar;

    .line 677951
    const v0, 0x7f0d0578

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/4BY;->c:Landroid/widget/TextView;

    .line 677952
    invoke-virtual {p0, v2}, LX/2EJ;->a(Landroid/view/View;)V

    goto/16 :goto_0

    .line 677953
    :cond_b
    iput v0, p0, LX/4BY;->k:I

    goto/16 :goto_1

    .line 677954
    :cond_c
    iget v1, p0, LX/4BY;->l:I

    add-int/2addr v1, v0

    iput v1, p0, LX/4BY;->l:I

    goto/16 :goto_2

    .line 677955
    :cond_d
    iget v1, p0, LX/4BY;->m:I

    add-int/2addr v1, v0

    iput v1, p0, LX/4BY;->m:I

    goto/16 :goto_3

    .line 677956
    :cond_e
    iput-object v0, p0, LX/4BY;->n:Landroid/graphics/drawable/Drawable;

    goto :goto_4

    .line 677957
    :cond_f
    iput-object v0, p0, LX/4BY;->o:Landroid/graphics/drawable/Drawable;

    goto :goto_5
.end method

.method public final onStart()V
    .locals 1

    .prologue
    .line 677898
    invoke-super {p0}, LX/2EJ;->onStart()V

    .line 677899
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4BY;->s:Z

    .line 677900
    return-void
.end method

.method public final onStop()V
    .locals 1

    .prologue
    .line 677895
    invoke-super {p0}, LX/2EJ;->onStop()V

    .line 677896
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/4BY;->s:Z

    .line 677897
    return-void
.end method
