.class public final LX/4iQ;
.super Ljava/io/BufferedOutputStream;
.source ""


# instance fields
.field private mClosed:Z


# direct methods
.method public constructor <init>(Ljava/io/OutputStream;)V
    .locals 0

    .prologue
    .line 803112
    invoke-direct {p0, p1}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 803113
    return-void
.end method

.method private checkNotClosed()V
    .locals 2

    .prologue
    .line 803127
    iget-boolean v0, p0, LX/4iQ;->mClosed:Z

    if-eqz v0, :cond_0

    .line 803128
    new-instance v0, Ljava/io/IOException;

    const-string v1, "stream already closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 803129
    :cond_0
    return-void
.end method


# virtual methods
.method public close()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 803120
    iget-boolean v0, p0, LX/4iQ;->mClosed:Z

    if-eqz v0, :cond_0

    .line 803121
    :goto_0
    return-void

    .line 803122
    :cond_0
    :try_start_0
    invoke-super {p0}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 803123
    iput-boolean v1, p0, LX/4iQ;->mClosed:Z

    goto :goto_0

    :catchall_0
    move-exception v0

    iput-boolean v1, p0, LX/4iQ;->mClosed:Z

    throw v0
.end method

.method public flush()V
    .locals 0

    .prologue
    .line 803124
    invoke-direct {p0}, LX/4iQ;->checkNotClosed()V

    .line 803125
    invoke-super {p0}, Ljava/io/BufferedOutputStream;->flush()V

    .line 803126
    return-void
.end method

.method public write(I)V
    .locals 0

    .prologue
    .line 803117
    invoke-direct {p0}, LX/4iQ;->checkNotClosed()V

    .line 803118
    invoke-super {p0, p1}, Ljava/io/BufferedOutputStream;->write(I)V

    .line 803119
    return-void
.end method

.method public write([BII)V
    .locals 0

    .prologue
    .line 803114
    invoke-direct {p0}, LX/4iQ;->checkNotClosed()V

    .line 803115
    invoke-super {p0, p1, p2, p3}, Ljava/io/BufferedOutputStream;->write([BII)V

    .line 803116
    return-void
.end method
