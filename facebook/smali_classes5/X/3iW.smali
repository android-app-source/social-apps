.class public LX/3iW;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/189;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0SG;

.field public final c:LX/0Uh;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0SG;LX/0Uh;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/189;",
            ">;",
            "LX/0SG;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 629845
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 629846
    iput-object p1, p0, LX/3iW;->a:LX/0Ot;

    .line 629847
    iput-object p2, p0, LX/3iW;->b:LX/0SG;

    .line 629848
    iput-object p3, p0, LX/3iW;->c:LX/0Uh;

    .line 629849
    return-void
.end method

.method public static a(LX/0QB;)LX/3iW;
    .locals 1

    .prologue
    .line 629854
    invoke-static {p0}, LX/3iW;->b(LX/0QB;)LX/3iW;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/3iW;
    .locals 4

    .prologue
    .line 629852
    new-instance v2, LX/3iW;

    const/16 v0, 0x473

    invoke-static {p0, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v1

    check-cast v1, LX/0Uh;

    invoke-direct {v2, v3, v0, v1}, LX/3iW;-><init>(LX/0Ot;LX/0SG;LX/0Uh;)V

    .line 629853
    return-object v2
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/5vL;)LX/6P1;
    .locals 6
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 629851
    new-instance v0, LX/6PD;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/6PD;-><init>(LX/3iW;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/5vL;)V

    return-object v0
.end method

.method public final b(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLSavedState;)LX/3Bq;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 629850
    new-instance v0, LX/6P8;

    invoke-direct {v0, p0, p1, p2}, LX/6P8;-><init>(LX/3iW;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLSavedState;)V

    return-object v0
.end method
