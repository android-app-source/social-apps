.class public LX/4r8;
.super LX/4r0;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x4a2f47f9ad71b962L


# direct methods
.method public constructor <init>(LX/0lJ;LX/4qx;Ljava/lang/String;Z)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lJ;",
            "LX/4qx;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 815161
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, LX/4r0;-><init>(LX/0lJ;LX/4qx;Ljava/lang/String;ZLjava/lang/Class;)V

    .line 815162
    return-void
.end method

.method private constructor <init>(LX/4r8;LX/2Ay;)V
    .locals 0

    .prologue
    .line 815159
    invoke-direct {p0, p1, p2}, LX/4r0;-><init>(LX/4r0;LX/2Ay;)V

    .line 815160
    return-void
.end method

.method private final e(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 815141
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v1, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v1, :cond_0

    .line 815142
    sget-object v0, LX/15z;->START_OBJECT:LX/15z;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "need JSON Object to contain As.WRAPPER_OBJECT type information for class "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/4r0;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/0n3;->a(LX/15w;LX/15z;Ljava/lang/String;)LX/28E;

    move-result-object v0

    throw v0

    .line 815143
    :cond_0
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v0

    sget-object v1, LX/15z;->FIELD_NAME:LX/15z;

    if-eq v0, v1, :cond_1

    .line 815144
    sget-object v0, LX/15z;->FIELD_NAME:LX/15z;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "need JSON String that contains type id (for subtype of "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/4r0;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/0n3;->a(LX/15w;LX/15z;Ljava/lang/String;)LX/28E;

    move-result-object v0

    throw v0

    .line 815145
    :cond_1
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    .line 815146
    invoke-virtual {p0, p2, v0}, LX/4r0;->a(LX/0n3;Ljava/lang/String;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v1

    .line 815147
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 815148
    iget-boolean v2, p0, LX/4r0;->_typeIdVisible:Z

    if-eqz v2, :cond_2

    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-ne v2, v3, :cond_2

    .line 815149
    new-instance v2, LX/0nW;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, LX/0nW;-><init>(LX/0lD;)V

    .line 815150
    invoke-virtual {v2}, LX/0nX;->f()V

    .line 815151
    iget-object v3, p0, LX/4r0;->_typePropertyName:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 815152
    invoke-virtual {v2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 815153
    invoke-virtual {v2, p1}, LX/0nW;->a(LX/15w;)LX/15w;

    move-result-object v0

    invoke-static {v0, p1}, LX/4pj;->a(LX/15w;LX/15w;)LX/4pj;

    move-result-object p1

    .line 815154
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 815155
    :cond_2
    invoke-virtual {v1, p1, p2}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    .line 815156
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_OBJECT:LX/15z;

    if-eq v1, v2, :cond_3

    .line 815157
    sget-object v0, LX/15z;->END_OBJECT:LX/15z;

    const-string v1, "expected closing END_OBJECT after type information and deserialized value"

    invoke-static {p1, v0, v1}, LX/0n3;->a(LX/15w;LX/15z;Ljava/lang/String;)LX/28E;

    move-result-object v0

    throw v0

    .line 815158
    :cond_3
    return-object v0
.end method


# virtual methods
.method public final a()LX/4pO;
    .locals 1

    .prologue
    .line 815140
    sget-object v0, LX/4pO;->WRAPPER_OBJECT:LX/4pO;

    return-object v0
.end method

.method public final a(LX/2Ay;)LX/4qw;
    .locals 1

    .prologue
    .line 815138
    iget-object v0, p0, LX/4r0;->_property:LX/2Ay;

    if-ne p1, v0, :cond_0

    .line 815139
    :goto_0
    return-object p0

    :cond_0
    new-instance v0, LX/4r8;

    invoke-direct {v0, p0, p1}, LX/4r8;-><init>(LX/4r8;LX/2Ay;)V

    move-object p0, v0

    goto :goto_0
.end method

.method public final a(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 815134
    invoke-direct {p0, p1, p2}, LX/4r8;->e(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 815137
    invoke-direct {p0, p1, p2}, LX/4r8;->e(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final c(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 815136
    invoke-direct {p0, p1, p2}, LX/4r8;->e(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final d(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 815135
    invoke-direct {p0, p1, p2}, LX/4r8;->e(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
