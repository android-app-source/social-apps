.class public LX/4ik;
.super Ljava/io/OutputStream;
.source ""


# instance fields
.field private final mBuffer:[B

.field private mClosed:Z

.field private mFallbackOutputStream:Ljava/io/OutputStream;

.field public final mHandlerInterface:LX/4iS;

.field private mPosition:I

.field private final mRequest:Lorg/apache/http/HttpEntityEnclosingRequest;

.field private final mStreamingBufferSize:I


# direct methods
.method public constructor <init>(LX/4iS;Lorg/apache/http/HttpEntityEnclosingRequest;[B)V
    .locals 1

    .prologue
    .line 803524
    const/16 v0, 0x1fa0

    invoke-direct {p0, p1, p2, p3, v0}, LX/4ik;-><init>(LX/4iS;Lorg/apache/http/HttpEntityEnclosingRequest;[BI)V

    .line 803525
    return-void
.end method

.method public constructor <init>(LX/4iS;Lorg/apache/http/HttpEntityEnclosingRequest;[BI)V
    .locals 0

    .prologue
    .line 803518
    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    .line 803519
    iput-object p1, p0, LX/4ik;->mHandlerInterface:LX/4iS;

    .line 803520
    iput-object p2, p0, LX/4ik;->mRequest:Lorg/apache/http/HttpEntityEnclosingRequest;

    .line 803521
    iput-object p3, p0, LX/4ik;->mBuffer:[B

    .line 803522
    iput p4, p0, LX/4ik;->mStreamingBufferSize:I

    .line 803523
    return-void
.end method

.method private canBufferMoreBytes(I)Z
    .locals 2

    .prologue
    .line 803517
    iget-object v0, p0, LX/4ik;->mBuffer:[B

    array-length v0, v0

    iget v1, p0, LX/4ik;->mPosition:I

    sub-int/2addr v0, v1

    if-gt p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkStreamIsNotClosed(LX/4ik;)V
    .locals 2

    .prologue
    .line 803514
    iget-boolean v0, p0, LX/4ik;->mClosed:Z

    if-eqz v0, :cond_0

    .line 803515
    new-instance v0, Ljava/io/IOException;

    const-string v1, "writeEomIfNecessary was already called on the stream"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 803516
    :cond_0
    return-void
.end method

.method private fallbackToStreaming([BII)V
    .locals 4

    .prologue
    .line 803526
    iget-object v0, p0, LX/4ik;->mHandlerInterface:LX/4iS;

    iget-object v1, p0, LX/4ik;->mRequest:Lorg/apache/http/HttpEntityEnclosingRequest;

    invoke-interface {v0, v1}, LX/4iS;->sendHeaders(Lorg/apache/http/HttpEntityEnclosingRequest;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 803527
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0

    .line 803528
    :cond_0
    new-instance v0, Ljava/io/BufferedOutputStream;

    new-instance v1, LX/4ij;

    invoke-direct {v1, p0}, LX/4ij;-><init>(LX/4ik;)V

    iget v2, p0, LX/4ik;->mStreamingBufferSize:I

    invoke-direct {v0, v1, v2}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;I)V

    iput-object v0, p0, LX/4ik;->mFallbackOutputStream:Ljava/io/OutputStream;

    .line 803529
    iget-object v0, p0, LX/4ik;->mFallbackOutputStream:Ljava/io/OutputStream;

    iget-object v1, p0, LX/4ik;->mBuffer:[B

    const/4 v2, 0x0

    iget v3, p0, LX/4ik;->mPosition:I

    invoke-virtual {v0, v1, v2, v3}, Ljava/io/OutputStream;->write([BII)V

    .line 803530
    iget-object v0, p0, LX/4ik;->mFallbackOutputStream:Ljava/io/OutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V

    .line 803531
    return-void
.end method


# virtual methods
.method public declared-synchronized write(I)V
    .locals 3

    .prologue
    .line 803505
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/4ik;->checkStreamIsNotClosed(LX/4ik;)V

    .line 803506
    iget-object v0, p0, LX/4ik;->mFallbackOutputStream:Ljava/io/OutputStream;

    if-eqz v0, :cond_0

    .line 803507
    iget-object v0, p0, LX/4ik;->mFallbackOutputStream:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 803508
    :goto_0
    monitor-exit p0

    return-void

    .line 803509
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    invoke-direct {p0, v0}, LX/4ik;->canBufferMoreBytes(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 803510
    const/4 v0, 0x1

    new-array v0, v0, [B

    const/4 v1, 0x0

    int-to-byte v2, p1

    aput-byte v2, v0, v1

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, LX/4ik;->fallbackToStreaming([BII)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 803511
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 803512
    :cond_1
    :try_start_2
    iget-object v0, p0, LX/4ik;->mBuffer:[B

    iget v1, p0, LX/4ik;->mPosition:I

    int-to-byte v2, p1

    aput-byte v2, v0, v1

    .line 803513
    iget v0, p0, LX/4ik;->mPosition:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/4ik;->mPosition:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public declared-synchronized write([B)V
    .locals 2

    .prologue
    .line 803501
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/4ik;->checkStreamIsNotClosed(LX/4ik;)V

    .line 803502
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, LX/4ik;->write([BII)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 803503
    monitor-exit p0

    return-void

    .line 803504
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized write([BII)V
    .locals 2

    .prologue
    .line 803492
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/4ik;->checkStreamIsNotClosed(LX/4ik;)V

    .line 803493
    iget-object v0, p0, LX/4ik;->mFallbackOutputStream:Ljava/io/OutputStream;

    if-eqz v0, :cond_0

    .line 803494
    iget-object v0, p0, LX/4ik;->mFallbackOutputStream:Ljava/io/OutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 803495
    :goto_0
    monitor-exit p0

    return-void

    .line 803496
    :cond_0
    :try_start_1
    invoke-direct {p0, p3}, LX/4ik;->canBufferMoreBytes(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 803497
    invoke-direct {p0, p1, p2, p3}, LX/4ik;->fallbackToStreaming([BII)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 803498
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 803499
    :cond_1
    :try_start_2
    iget-object v0, p0, LX/4ik;->mBuffer:[B

    iget v1, p0, LX/4ik;->mPosition:I

    invoke-static {p1, p2, v0, v1, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 803500
    iget v0, p0, LX/4ik;->mPosition:I

    add-int/2addr v0, p3

    iput v0, p0, LX/4ik;->mPosition:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public declared-synchronized writeEomIfNecessary()V
    .locals 5

    .prologue
    .line 803482
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/4ik;->checkStreamIsNotClosed(LX/4ik;)V

    .line 803483
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4ik;->mClosed:Z

    .line 803484
    iget-object v0, p0, LX/4ik;->mFallbackOutputStream:Ljava/io/OutputStream;

    if-eqz v0, :cond_0

    .line 803485
    iget-object v0, p0, LX/4ik;->mFallbackOutputStream:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    .line 803486
    iget-object v0, p0, LX/4ik;->mHandlerInterface:LX/4iS;

    invoke-interface {v0}, LX/4iS;->sendEOM()Z

    move-result v0

    if-nez v0, :cond_1

    .line 803487
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 803488
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 803489
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/4ik;->mHandlerInterface:LX/4iS;

    iget-object v1, p0, LX/4ik;->mRequest:Lorg/apache/http/HttpEntityEnclosingRequest;

    iget-object v2, p0, LX/4ik;->mBuffer:[B

    const/4 v3, 0x0

    iget v4, p0, LX/4ik;->mPosition:I

    invoke-interface {v0, v1, v2, v3, v4}, LX/4iS;->sendRequestWithBodyAndEom(Lorg/apache/http/HttpEntityEnclosingRequest;[BII)Z

    move-result v0

    if-nez v0, :cond_1

    .line 803490
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 803491
    :cond_1
    monitor-exit p0

    return-void
.end method
