.class public final enum LX/46h;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/46h;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/46h;

.field public static final enum PREFER_SDCARD:LX/46h;

.field public static final enum REQUIRE_PRIVATE:LX/46h;

.field public static final enum REQUIRE_SDCARD:LX/46h;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 671500
    new-instance v0, LX/46h;

    const-string v1, "REQUIRE_PRIVATE"

    invoke-direct {v0, v1, v2}, LX/46h;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/46h;->REQUIRE_PRIVATE:LX/46h;

    .line 671501
    new-instance v0, LX/46h;

    const-string v1, "REQUIRE_SDCARD"

    invoke-direct {v0, v1, v3}, LX/46h;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/46h;->REQUIRE_SDCARD:LX/46h;

    .line 671502
    new-instance v0, LX/46h;

    const-string v1, "PREFER_SDCARD"

    invoke-direct {v0, v1, v4}, LX/46h;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/46h;->PREFER_SDCARD:LX/46h;

    .line 671503
    const/4 v0, 0x3

    new-array v0, v0, [LX/46h;

    sget-object v1, LX/46h;->REQUIRE_PRIVATE:LX/46h;

    aput-object v1, v0, v2

    sget-object v1, LX/46h;->REQUIRE_SDCARD:LX/46h;

    aput-object v1, v0, v3

    sget-object v1, LX/46h;->PREFER_SDCARD:LX/46h;

    aput-object v1, v0, v4

    sput-object v0, LX/46h;->$VALUES:[LX/46h;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 671499
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/46h;
    .locals 1

    .prologue
    .line 671498
    const-class v0, LX/46h;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/46h;

    return-object v0
.end method

.method public static values()[LX/46h;
    .locals 1

    .prologue
    .line 671497
    sget-object v0, LX/46h;->$VALUES:[LX/46h;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/46h;

    return-object v0
.end method
