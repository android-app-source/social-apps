.class public final LX/4ug;
.super LX/4uf;
.source ""


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/2wr;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/2wr;)V
    .locals 1

    invoke-direct {p0}, LX/4uf;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/4ug;->a:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/signin/internal/SignInResponse;)V
    .locals 3
    .annotation build Landroid/support/annotation/BinderThread;
    .end annotation

    iget-object v0, p0, LX/4ug;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2wr;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, v0, LX/2wr;->a:LX/2wm;

    new-instance v2, LX/4uc;

    invoke-direct {v2, p0, v0, v0, p1}, LX/4uc;-><init>(LX/4ug;LX/2wq;LX/2wr;Lcom/google/android/gms/signin/internal/SignInResponse;)V

    invoke-virtual {v1, v2}, LX/2wm;->a(LX/4uY;)V

    goto :goto_0
.end method
