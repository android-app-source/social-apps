.class public final LX/4xY;
.super LX/0Rr;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Rr",
        "<",
        "Ljava/util/Map$Entry",
        "<TK;",
        "Ljava/util/Collection",
        "<TV;>;>;>;"
    }
.end annotation


# instance fields
.field public final a:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;",
            "Ljava/util/Collection",
            "<TV;>;>;>;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/4xZ;


# direct methods
.method public constructor <init>(LX/4xZ;)V
    .locals 1

    .prologue
    .line 821099
    iput-object p1, p0, LX/4xY;->b:LX/4xZ;

    invoke-direct {p0}, LX/0Rr;-><init>()V

    .line 821100
    iget-object v0, p0, LX/4xY;->b:LX/4xZ;

    iget-object v0, v0, LX/4xZ;->a:LX/4xd;

    iget-object v0, v0, LX/4xd;->a:LX/4xj;

    iget-object v0, v0, LX/4xj;->a:LX/0Xu;

    invoke-interface {v0}, LX/0Xu;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, LX/4xY;->a:Ljava/util/Iterator;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 821101
    :cond_0
    iget-object v0, p0, LX/4xY;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 821102
    iget-object v0, p0, LX/4xY;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 821103
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    .line 821104
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    new-instance v2, LX/4xh;

    iget-object v3, p0, LX/4xY;->b:LX/4xZ;

    iget-object v3, v3, LX/4xZ;->a:LX/4xd;

    iget-object v3, v3, LX/4xd;->a:LX/4xj;

    invoke-direct {v2, v3, v1}, LX/4xh;-><init>(LX/4xj;Ljava/lang/Object;)V

    invoke-static {v0, v2}, LX/4xj;->a(Ljava/util/Collection;LX/0Rl;)Ljava/util/Collection;

    move-result-object v0

    .line 821105
    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 821106
    invoke-static {v1, v0}, LX/0PM;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    .line 821107
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, LX/0Rr;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    goto :goto_0
.end method
