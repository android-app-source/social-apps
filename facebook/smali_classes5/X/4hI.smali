.class public final enum LX/4hI;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/4hI;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/4hI;

.field public static final enum INVALID_COUNTRY_CODE:LX/4hI;

.field public static final enum IS_POSSIBLE:LX/4hI;

.field public static final enum TOO_LONG:LX/4hI;

.field public static final enum TOO_SHORT:LX/4hI;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 800846
    new-instance v0, LX/4hI;

    const-string v1, "IS_POSSIBLE"

    invoke-direct {v0, v1, v2}, LX/4hI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4hI;->IS_POSSIBLE:LX/4hI;

    .line 800847
    new-instance v0, LX/4hI;

    const-string v1, "INVALID_COUNTRY_CODE"

    invoke-direct {v0, v1, v3}, LX/4hI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4hI;->INVALID_COUNTRY_CODE:LX/4hI;

    .line 800848
    new-instance v0, LX/4hI;

    const-string v1, "TOO_SHORT"

    invoke-direct {v0, v1, v4}, LX/4hI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4hI;->TOO_SHORT:LX/4hI;

    .line 800849
    new-instance v0, LX/4hI;

    const-string v1, "TOO_LONG"

    invoke-direct {v0, v1, v5}, LX/4hI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4hI;->TOO_LONG:LX/4hI;

    .line 800850
    const/4 v0, 0x4

    new-array v0, v0, [LX/4hI;

    sget-object v1, LX/4hI;->IS_POSSIBLE:LX/4hI;

    aput-object v1, v0, v2

    sget-object v1, LX/4hI;->INVALID_COUNTRY_CODE:LX/4hI;

    aput-object v1, v0, v3

    sget-object v1, LX/4hI;->TOO_SHORT:LX/4hI;

    aput-object v1, v0, v4

    sget-object v1, LX/4hI;->TOO_LONG:LX/4hI;

    aput-object v1, v0, v5

    sput-object v0, LX/4hI;->$VALUES:[LX/4hI;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 800851
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/4hI;
    .locals 1

    .prologue
    .line 800852
    const-class v0, LX/4hI;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/4hI;

    return-object v0
.end method

.method public static values()[LX/4hI;
    .locals 1

    .prologue
    .line 800853
    sget-object v0, LX/4hI;->$VALUES:[LX/4hI;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/4hI;

    return-object v0
.end method
