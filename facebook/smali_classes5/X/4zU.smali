.class public final LX/4zU;
.super LX/4zN;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/4zN",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field public a:LX/0qF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0qF",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public b:LX/0qF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0qF",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public final synthetic c:LX/4zW;


# direct methods
.method public constructor <init>(LX/4zW;)V
    .locals 0

    .prologue
    .line 822830
    iput-object p1, p0, LX/4zU;->c:LX/4zW;

    invoke-direct {p0}, LX/4zN;-><init>()V

    .line 822831
    iput-object p0, p0, LX/4zU;->a:LX/0qF;

    .line 822832
    iput-object p0, p0, LX/4zU;->b:LX/0qF;

    return-void
.end method


# virtual methods
.method public final getExpirationTime()J
    .locals 2

    .prologue
    .line 822833
    const-wide v0, 0x7fffffffffffffffL

    return-wide v0
.end method

.method public final getNextExpirable()LX/0qF;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0qF",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 822829
    iget-object v0, p0, LX/4zU;->a:LX/0qF;

    return-object v0
.end method

.method public final getPreviousExpirable()LX/0qF;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0qF",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 822834
    iget-object v0, p0, LX/4zU;->b:LX/0qF;

    return-object v0
.end method

.method public final setExpirationTime(J)V
    .locals 0

    .prologue
    .line 822828
    return-void
.end method

.method public final setNextExpirable(LX/0qF;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0qF",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 822824
    iput-object p1, p0, LX/4zU;->a:LX/0qF;

    .line 822825
    return-void
.end method

.method public final setPreviousExpirable(LX/0qF;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0qF",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 822826
    iput-object p1, p0, LX/4zU;->b:LX/0qF;

    .line 822827
    return-void
.end method
