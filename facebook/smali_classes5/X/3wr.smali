.class public abstract LX/3wr;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/util/SparseIntArray;

.field public b:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 656178
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 656179
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, LX/3wr;->a:Landroid/util/SparseIntArray;

    .line 656180
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/3wr;->b:Z

    return-void
.end method

.method private b(I)I
    .locals 4

    .prologue
    .line 656181
    const/4 v1, 0x0

    .line 656182
    iget-object v0, p0, LX/3wr;->a:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 656183
    :goto_0
    if-gt v1, v0, :cond_1

    .line 656184
    add-int v2, v1, v0

    ushr-int/lit8 v2, v2, 0x1

    .line 656185
    iget-object v3, p0, LX/3wr;->a:Landroid/util/SparseIntArray;

    invoke-virtual {v3, v2}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v3

    .line 656186
    if-ge v3, p1, :cond_0

    .line 656187
    add-int/lit8 v1, v2, 0x1

    goto :goto_0

    .line 656188
    :cond_0
    add-int/lit8 v0, v2, -0x1

    .line 656189
    goto :goto_0

    .line 656190
    :cond_1
    add-int/lit8 v0, v1, -0x1

    .line 656191
    if-ltz v0, :cond_2

    iget-object v1, p0, LX/3wr;->a:Landroid/util/SparseIntArray;

    invoke-virtual {v1}, Landroid/util/SparseIntArray;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 656192
    iget-object v1, p0, LX/3wr;->a:Landroid/util/SparseIntArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v0

    .line 656193
    :goto_1
    return v0

    :cond_2
    const/4 v0, -0x1

    goto :goto_1
.end method


# virtual methods
.method public abstract a(I)I
.end method

.method public a(II)I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 656194
    invoke-virtual {p0, p1}, LX/3wr;->a(I)I

    move-result v4

    .line 656195
    if-ne v4, p2, :cond_1

    .line 656196
    :cond_0
    :goto_0
    return v1

    .line 656197
    :cond_1
    iget-boolean v0, p0, LX/3wr;->b:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/3wr;->a:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->size()I

    move-result v0

    if-lez v0, :cond_5

    .line 656198
    invoke-direct {p0, p1}, LX/3wr;->b(I)I

    move-result v0

    .line 656199
    if-ltz v0, :cond_5

    .line 656200
    iget-object v2, p0, LX/3wr;->a:Landroid/util/SparseIntArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseIntArray;->get(I)I

    move-result v2

    invoke-virtual {p0, v0}, LX/3wr;->a(I)I

    move-result v3

    add-int/2addr v2, v3

    .line 656201
    add-int/lit8 v0, v0, 0x1

    :goto_1
    move v3, v0

    .line 656202
    :goto_2
    if-ge v3, p1, :cond_4

    .line 656203
    invoke-virtual {p0, v3}, LX/3wr;->a(I)I

    move-result v0

    .line 656204
    add-int/2addr v2, v0

    .line 656205
    if-ne v2, p2, :cond_3

    move v0, v1

    .line 656206
    :cond_2
    :goto_3
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v0

    goto :goto_2

    .line 656207
    :cond_3
    if-gt v2, p2, :cond_2

    move v0, v2

    goto :goto_3

    .line 656208
    :cond_4
    add-int v0, v2, v4

    if-gt v0, p2, :cond_0

    move v1, v2

    .line 656209
    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    goto :goto_1
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 656210
    iget-object v0, p0, LX/3wr;->a:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->clear()V

    .line 656211
    return-void
.end method

.method public final b(II)I
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 656212
    iget-boolean v0, p0, LX/3wr;->b:Z

    if-nez v0, :cond_1

    .line 656213
    invoke-virtual {p0, p1, p2}, LX/3wr;->a(II)I

    move-result v0

    .line 656214
    :cond_0
    :goto_0
    return v0

    .line 656215
    :cond_1
    iget-object v0, p0, LX/3wr;->a:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p1, v1}, Landroid/util/SparseIntArray;->get(II)I

    move-result v0

    .line 656216
    if-ne v0, v1, :cond_0

    .line 656217
    invoke-virtual {p0, p1, p2}, LX/3wr;->a(II)I

    move-result v0

    .line 656218
    iget-object v1, p0, LX/3wr;->a:Landroid/util/SparseIntArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseIntArray;->put(II)V

    goto :goto_0
.end method

.method public final c(II)I
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 656219
    invoke-virtual {p0, p1}, LX/3wr;->a(I)I

    move-result v5

    move v4, v2

    move v0, v2

    move v3, v2

    .line 656220
    :goto_0
    if-ge v4, p1, :cond_1

    .line 656221
    invoke-virtual {p0, v4}, LX/3wr;->a(I)I

    move-result v1

    .line 656222
    add-int/2addr v3, v1

    .line 656223
    if-ne v3, p2, :cond_0

    .line 656224
    add-int/lit8 v0, v0, 0x1

    move v1, v2

    .line 656225
    :goto_1
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move v3, v1

    goto :goto_0

    .line 656226
    :cond_0
    if-le v3, p2, :cond_3

    .line 656227
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 656228
    :cond_1
    add-int v1, v3, v5

    if-le v1, p2, :cond_2

    .line 656229
    add-int/lit8 v0, v0, 0x1

    .line 656230
    :cond_2
    return v0

    :cond_3
    move v1, v3

    goto :goto_1
.end method
