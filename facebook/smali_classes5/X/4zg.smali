.class public final LX/4zg;
.super LX/0qE;
.source ""

# interfaces
.implements LX/0qF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LX/0qE",
        "<TK;TV;>;",
        "LX/0qF",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field public d:LX/0qF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0qF",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public e:LX/0qF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0qF",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;ILX/0qF;)V
    .locals 1
    .param p4    # LX/0qF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/ReferenceQueue",
            "<TK;>;TK;I",
            "LX/0qF",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 823013
    invoke-direct {p0, p1, p2, p3, p4}, LX/0qE;-><init>(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;ILX/0qF;)V

    .line 823014
    sget-object v0, LX/4zX;->INSTANCE:LX/4zX;

    move-object v0, v0

    .line 823015
    iput-object v0, p0, LX/4zg;->d:LX/0qF;

    .line 823016
    sget-object v0, LX/4zX;->INSTANCE:LX/4zX;

    move-object v0, v0

    .line 823017
    iput-object v0, p0, LX/4zg;->e:LX/0qF;

    .line 823018
    return-void
.end method


# virtual methods
.method public final getNextEvictable()LX/0qF;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0qF",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 823023
    iget-object v0, p0, LX/4zg;->d:LX/0qF;

    return-object v0
.end method

.method public final getPreviousEvictable()LX/0qF;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0qF",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 823024
    iget-object v0, p0, LX/4zg;->e:LX/0qF;

    return-object v0
.end method

.method public final setNextEvictable(LX/0qF;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0qF",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 823021
    iput-object p1, p0, LX/4zg;->d:LX/0qF;

    .line 823022
    return-void
.end method

.method public final setPreviousEvictable(LX/0qF;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0qF",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 823019
    iput-object p1, p0, LX/4zg;->e:LX/0qF;

    .line 823020
    return-void
.end method
