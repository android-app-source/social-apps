.class public final LX/4yv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4yu;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/4yu",
        "<TE;>;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<+TE;>;"
        }
    .end annotation
.end field

.field private b:Z

.field private c:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Iterator;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Iterator",
            "<+TE;>;)V"
        }
    .end annotation

    .prologue
    .line 822072
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 822073
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Iterator;

    iput-object v0, p0, LX/4yv;->a:Ljava/util/Iterator;

    .line 822074
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 822075
    iget-boolean v0, p0, LX/4yv;->b:Z

    if-nez v0, :cond_0

    .line 822076
    iget-object v0, p0, LX/4yv;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, LX/4yv;->c:Ljava/lang/Object;

    .line 822077
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4yv;->b:Z

    .line 822078
    :cond_0
    iget-object v0, p0, LX/4yv;->c:Ljava/lang/Object;

    return-object v0
.end method

.method public final hasNext()Z
    .locals 1

    .prologue
    .line 822079
    iget-boolean v0, p0, LX/4yv;->b:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/4yv;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final next()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 822080
    iget-boolean v0, p0, LX/4yv;->b:Z

    if-nez v0, :cond_0

    .line 822081
    iget-object v0, p0, LX/4yv;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 822082
    :goto_0
    return-object v0

    .line 822083
    :cond_0
    iget-object v0, p0, LX/4yv;->c:Ljava/lang/Object;

    .line 822084
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/4yv;->b:Z

    .line 822085
    const/4 v1, 0x0

    iput-object v1, p0, LX/4yv;->c:Ljava/lang/Object;

    goto :goto_0
.end method

.method public final remove()V
    .locals 2

    .prologue
    .line 822086
    iget-boolean v0, p0, LX/4yv;->b:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Can\'t remove after you\'ve peeked at next"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 822087
    iget-object v0, p0, LX/4yv;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 822088
    return-void

    .line 822089
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
