.class public final LX/4Aj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field public final synthetic this$0:LX/4Al;


# direct methods
.method public constructor <init>(LX/4Al;)V
    .locals 0

    .prologue
    .line 676926
    iput-object p1, p0, LX/4Aj;->this$0:LX/4Al;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2

    .prologue
    .line 676927
    iget-object v0, p0, LX/4Aj;->this$0:LX/4Al;

    iget-boolean v0, v0, LX/4Al;->disposed:Z

    if-nez v0, :cond_0

    .line 676928
    iget-object v0, p0, LX/4Aj;->this$0:LX/4Al;

    invoke-static {p2}, LX/1mK;->asInterface(Landroid/os/IBinder;)LX/1mL;

    move-result-object v1

    .line 676929
    iput-object v1, v0, LX/4Al;->blueService:LX/1mL;

    .line 676930
    iget-object v0, p0, LX/4Aj;->this$0:LX/4Al;

    invoke-static {v0}, LX/4Al;->maybeStartAndRegister(LX/4Al;)V

    .line 676931
    :cond_0
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    .prologue
    .line 676921
    iget-object v0, p0, LX/4Aj;->this$0:LX/4Al;

    const/4 v1, 0x0

    .line 676922
    iput-object v1, v0, LX/4Al;->blueService:LX/1mL;

    .line 676923
    iget-object v0, p0, LX/4Aj;->this$0:LX/4Al;

    const/4 v1, 0x0

    .line 676924
    iput-boolean v1, v0, LX/4Al;->registeredForCompletion:Z

    .line 676925
    return-void
.end method
