.class public final LX/3RK;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:I

.field private static final b:Ljava/lang/Object;

.field private static c:Ljava/lang/String;

.field private static d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final g:Ljava/lang/Object;

.field private static h:LX/3S7;

.field private static final i:LX/3RP;


# instance fields
.field private final e:Landroid/content/Context;

.field private final f:Landroid/app/NotificationManager;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 580271
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/3RK;->b:Ljava/lang/Object;

    .line 580272
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, LX/3RK;->d:Ljava/util/Set;

    .line 580273
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/3RK;->g:Ljava/lang/Object;

    .line 580274
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    .line 580275
    new-instance v0, LX/3RL;

    invoke-direct {v0}, LX/3RL;-><init>()V

    sput-object v0, LX/3RK;->i:LX/3RP;

    .line 580276
    :goto_0
    sget-object v0, LX/3RK;->i:LX/3RP;

    invoke-interface {v0}, LX/3RP;->a()I

    move-result v0

    sput v0, LX/3RK;->a:I

    .line 580277
    return-void

    .line 580278
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_1

    .line 580279
    new-instance v0, LX/3RM;

    invoke-direct {v0}, LX/3RM;-><init>()V

    sput-object v0, LX/3RK;->i:LX/3RP;

    goto :goto_0

    .line 580280
    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v1, 0x5

    if-lt v0, v1, :cond_2

    .line 580281
    new-instance v0, LX/3RN;

    invoke-direct {v0}, LX/3RN;-><init>()V

    sput-object v0, LX/3RK;->i:LX/3RP;

    goto :goto_0

    .line 580282
    :cond_2
    new-instance v0, LX/3RO;

    invoke-direct {v0}, LX/3RO;-><init>()V

    sput-object v0, LX/3RK;->i:LX/3RP;

    goto :goto_0
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 580267
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 580268
    iput-object p1, p0, LX/3RK;->e:Landroid/content/Context;

    .line 580269
    iget-object v0, p0, LX/3RK;->e:Landroid/content/Context;

    const-string v1, "notification"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, LX/3RK;->f:Landroid/app/NotificationManager;

    .line 580270
    return-void
.end method

.method public static a(Landroid/content/Context;)LX/3RK;
    .locals 1

    .prologue
    .line 580266
    new-instance v0, LX/3RK;

    invoke-direct {v0, p0}, LX/3RK;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private a(LX/3S6;)V
    .locals 3

    .prologue
    .line 580260
    sget-object v1, LX/3RK;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 580261
    :try_start_0
    sget-object v0, LX/3RK;->h:LX/3S7;

    if-nez v0, :cond_0

    .line 580262
    new-instance v0, LX/3S7;

    iget-object v2, p0, LX/3RK;->e:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, LX/3S7;-><init>(Landroid/content/Context;)V

    sput-object v0, LX/3RK;->h:LX/3S7;

    .line 580263
    :cond_0
    sget-object v0, LX/3RK;->h:LX/3S7;

    .line 580264
    iget-object v2, v0, LX/3S7;->c:Landroid/os/Handler;

    const/4 p0, 0x0

    invoke-virtual {v2, p0, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    .line 580265
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static b(Landroid/content/Context;)Ljava/util/Set;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 580246
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "enabled_notification_listeners"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 580247
    sget-object v2, LX/3RK;->b:Ljava/lang/Object;

    monitor-enter v2

    .line 580248
    if-eqz v1, :cond_2

    :try_start_0
    sget-object v0, LX/3RK;->c:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 580249
    const-string v0, ":"

    invoke-virtual {v1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 580250
    new-instance v4, Ljava/util/HashSet;

    array-length v0, v3

    invoke-direct {v4, v0}, Ljava/util/HashSet;-><init>(I)V

    .line 580251
    array-length v5, v3

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v5, :cond_1

    aget-object v6, v3, v0

    .line 580252
    invoke-static {v6}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v6

    .line 580253
    if-eqz v6, :cond_0

    .line 580254
    invoke-virtual {v6}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 580255
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 580256
    :cond_1
    sput-object v4, LX/3RK;->d:Ljava/util/Set;

    .line 580257
    sput-object v1, LX/3RK;->c:Ljava/lang/String;

    .line 580258
    :cond_2
    sget-object v0, LX/3RK;->d:Ljava/util/Set;

    monitor-exit v2

    return-object v0

    .line 580259
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .prologue
    .line 580244
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, LX/3RK;->a(Ljava/lang/String;I)V

    .line 580245
    return-void
.end method

.method public final a(ILandroid/app/Notification;)V
    .locals 1

    .prologue
    .line 580231
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1, p2}, LX/3RK;->a(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 580232
    return-void
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 580240
    sget-object v0, LX/3RK;->i:LX/3RP;

    iget-object v1, p0, LX/3RK;->f:Landroid/app/NotificationManager;

    invoke-interface {v0, v1, p1, p2}, LX/3RP;->a(Landroid/app/NotificationManager;Ljava/lang/String;I)V

    .line 580241
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-gt v0, v1, :cond_0

    .line 580242
    new-instance v0, LX/3S5;

    iget-object v1, p0, LX/3RK;->e:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p2, p1}, LX/3S5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    invoke-direct {p0, v0}, LX/3RK;->a(LX/3S6;)V

    .line 580243
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;ILandroid/app/Notification;)V
    .locals 2

    .prologue
    .line 580233
    sget-object v0, LX/3px;->a:LX/3pn;

    invoke-interface {v0, p3}, LX/3pn;->a(Landroid/app/Notification;)Landroid/os/Bundle;

    move-result-object v0

    move-object v0, v0

    .line 580234
    if-eqz v0, :cond_1

    const-string v1, "android.support.useSideChannel"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 580235
    if-eqz v0, :cond_0

    .line 580236
    new-instance v0, LX/3qA;

    iget-object v1, p0, LX/3RK;->e:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p2, p1, p3}, LX/3qA;-><init>(Ljava/lang/String;ILjava/lang/String;Landroid/app/Notification;)V

    invoke-direct {p0, v0}, LX/3RK;->a(LX/3S6;)V

    .line 580237
    sget-object v0, LX/3RK;->i:LX/3RP;

    iget-object v1, p0, LX/3RK;->f:Landroid/app/NotificationManager;

    invoke-interface {v0, v1, p1, p2}, LX/3RP;->a(Landroid/app/NotificationManager;Ljava/lang/String;I)V

    .line 580238
    :goto_1
    return-void

    .line 580239
    :cond_0
    sget-object v0, LX/3RK;->i:LX/3RP;

    iget-object v1, p0, LX/3RK;->f:Landroid/app/NotificationManager;

    invoke-interface {v0, v1, p1, p2, p3}, LX/3RP;->a(Landroid/app/NotificationManager;Ljava/lang/String;ILandroid/app/Notification;)V

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
