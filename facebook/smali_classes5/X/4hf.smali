.class public LX/4hf;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/os/Bundle;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 801708
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 801709
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, LX/4hf;->a:Landroid/os/Bundle;

    .line 801710
    iget-object v0, p0, LX/4hf;->a:Landroid/os/Bundle;

    const-string v1, "com.facebook.platform.protocol.PROTOCOL_VERSION"

    const v2, 0x133060d

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 801711
    iget-object v0, p0, LX/4hf;->a:Landroid/os/Bundle;

    const-string v1, "com.facebook.platform.status.ERROR_TYPE"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 801712
    iget-object v0, p0, LX/4hf;->a:Landroid/os/Bundle;

    const-string v1, "com.facebook.platform.status.ERROR_DESCRIPTION"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 801713
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)LX/4hf;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/Object;",
            ")",
            "LX/4hf;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 801699
    if-nez p2, :cond_0

    .line 801700
    const-string v0, "Expected non-null \'%s\' extra, actual value was null."

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p0, v1, v2

    invoke-static {v0, v1}, LX/4hf;->a(Ljava/lang/String;[Ljava/lang/Object;)LX/4hf;

    move-result-object v0

    .line 801701
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "Expected \'%s\' extra to be type \'%s\', actual value was type \'%s\'."

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p0, v1, v2

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    const/4 v2, 0x2

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, LX/4hf;->a(Ljava/lang/String;[Ljava/lang/Object;)LX/4hf;

    move-result-object v0

    goto :goto_0
.end method

.method public static varargs a(Ljava/lang/String;[Ljava/lang/Object;)LX/4hf;
    .locals 3

    .prologue
    .line 801705
    invoke-static {p0, p1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 801706
    new-instance v1, LX/4hf;

    const-string v2, "ProtocolError"

    invoke-direct {v1, v2, v0}, LX/4hf;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 801707
    return-object v1
.end method

.method public static b(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)LX/4hf;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/Object;",
            ")",
            "LX/4hf;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 801702
    if-nez p2, :cond_0

    .line 801703
    const-string v0, "Expected non-null items in \'%s\' ArrayList extra, actual item was null."

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p0, v1, v2

    invoke-static {v0, v1}, LX/4hf;->a(Ljava/lang/String;[Ljava/lang/Object;)LX/4hf;

    move-result-object v0

    .line 801704
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "Expected \'%s\' ArrayList extra to contain items of type \'%s\', actual was type \'%s\'."

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p0, v1, v2

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    const/4 v2, 0x2

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, LX/4hf;->a(Ljava/lang/String;[Ljava/lang/Object;)LX/4hf;

    move-result-object v0

    goto :goto_0
.end method
