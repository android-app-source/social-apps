.class public abstract LX/3um;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field private final a:F

.field private final b:I

.field private final c:I

.field public final d:Landroid/view/View;

.field private e:Ljava/lang/Runnable;

.field private f:Ljava/lang/Runnable;

.field public g:Z

.field public h:Z

.field private i:I

.field public final j:[I


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 649474
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 649475
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, LX/3um;->j:[I

    .line 649476
    iput-object p1, p0, LX/3um;->d:Landroid/view/View;

    .line 649477
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, LX/3um;->a:F

    .line 649478
    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    move-result v0

    iput v0, p0, LX/3um;->b:I

    .line 649479
    iget v0, p0, LX/3um;->b:I

    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result v1

    add-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, LX/3um;->c:I

    .line 649480
    return-void
.end method

.method private a(Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 649481
    iget-object v2, p0, LX/3um;->d:Landroid/view/View;

    .line 649482
    invoke-virtual {v2}, Landroid/view/View;->isEnabled()Z

    move-result v3

    if-nez v3, :cond_1

    .line 649483
    :cond_0
    :goto_0
    return v0

    .line 649484
    :cond_1
    invoke-static {p1}, LX/2xd;->a(Landroid/view/MotionEvent;)I

    move-result v3

    .line 649485
    packed-switch v3, :pswitch_data_0

    goto :goto_0

    .line 649486
    :pswitch_0
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v1

    iput v1, p0, LX/3um;->i:I

    .line 649487
    iput-boolean v0, p0, LX/3um;->h:Z

    .line 649488
    iget-object v1, p0, LX/3um;->e:Ljava/lang/Runnable;

    if-nez v1, :cond_2

    .line 649489
    new-instance v1, Landroid/support/v7/widget/ListPopupWindow$ForwardingListener$DisallowIntercept;

    invoke-direct {v1, p0}, Landroid/support/v7/widget/ListPopupWindow$ForwardingListener$DisallowIntercept;-><init>(LX/3um;)V

    iput-object v1, p0, LX/3um;->e:Ljava/lang/Runnable;

    .line 649490
    :cond_2
    iget-object v1, p0, LX/3um;->e:Ljava/lang/Runnable;

    iget v3, p0, LX/3um;->b:I

    int-to-long v4, v3

    invoke-virtual {v2, v1, v4, v5}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 649491
    iget-object v1, p0, LX/3um;->f:Ljava/lang/Runnable;

    if-nez v1, :cond_3

    .line 649492
    new-instance v1, Landroid/support/v7/widget/ListPopupWindow$ForwardingListener$TriggerLongPress;

    invoke-direct {v1, p0}, Landroid/support/v7/widget/ListPopupWindow$ForwardingListener$TriggerLongPress;-><init>(LX/3um;)V

    iput-object v1, p0, LX/3um;->f:Ljava/lang/Runnable;

    .line 649493
    :cond_3
    iget-object v1, p0, LX/3um;->f:Ljava/lang/Runnable;

    iget v3, p0, LX/3um;->c:I

    int-to-long v4, v3

    invoke-virtual {v2, v1, v4, v5}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 649494
    :pswitch_1
    iget v3, p0, LX/3um;->i:I

    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v3

    .line 649495
    if-ltz v3, :cond_0

    .line 649496
    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getX(I)F

    move-result v4

    .line 649497
    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getY(I)F

    move-result v3

    .line 649498
    iget v5, p0, LX/3um;->a:F

    .line 649499
    neg-float v6, v5

    cmpl-float v6, v4, v6

    if-ltz v6, :cond_4

    neg-float v6, v5

    cmpl-float v6, v3, v6

    if-ltz v6, :cond_4

    invoke-virtual {v2}, Landroid/view/View;->getRight()I

    move-result v6

    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result p1

    sub-int/2addr v6, p1

    int-to-float v6, v6

    add-float/2addr v6, v5

    cmpg-float v6, v4, v6

    if-gez v6, :cond_4

    invoke-virtual {v2}, Landroid/view/View;->getBottom()I

    move-result v6

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result p1

    sub-int/2addr v6, p1

    int-to-float v6, v6

    add-float/2addr v6, v5

    cmpg-float v6, v3, v6

    if-gez v6, :cond_4

    const/4 v6, 0x1

    :goto_1
    move v3, v6

    .line 649500
    if-nez v3, :cond_0

    .line 649501
    invoke-static {p0}, LX/3um;->d(LX/3um;)V

    .line 649502
    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    move v0, v1

    .line 649503
    goto/16 :goto_0

    .line 649504
    :pswitch_2
    invoke-static {p0}, LX/3um;->d(LX/3um;)V

    goto/16 :goto_0

    :cond_4
    const/4 v6, 0x0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private b(Landroid/view/MotionEvent;)Z
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 649447
    iget-object v2, p0, LX/3um;->d:Landroid/view/View;

    .line 649448
    invoke-virtual {p0}, LX/3um;->a()LX/3w1;

    move-result-object v3

    .line 649449
    if-eqz v3, :cond_0

    invoke-virtual {v3}, LX/3w1;->b()Z

    move-result v4

    if-nez v4, :cond_2

    :cond_0
    move v0, v1

    .line 649450
    :cond_1
    :goto_0
    return v0

    .line 649451
    :cond_2
    iget-object v3, v3, LX/3w1;->f:LX/3wy;

    .line 649452
    if-eqz v3, :cond_3

    invoke-virtual {v3}, LX/3wy;->isShown()Z

    move-result v4

    if-nez v4, :cond_4

    :cond_3
    move v0, v1

    .line 649453
    goto :goto_0

    .line 649454
    :cond_4
    invoke-static {p1}, Landroid/view/MotionEvent;->obtainNoHistory(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v4

    .line 649455
    const/4 v7, 0x1

    .line 649456
    iget-object v5, p0, LX/3um;->j:[I

    .line 649457
    invoke-virtual {v2, v5}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 649458
    const/4 v6, 0x0

    aget v6, v5, v6

    int-to-float v6, v6

    aget v5, v5, v7

    int-to-float v5, v5

    invoke-virtual {v4, v6, v5}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 649459
    const/4 v6, 0x1

    .line 649460
    iget-object v2, p0, LX/3um;->j:[I

    .line 649461
    invoke-virtual {v3, v2}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 649462
    const/4 v5, 0x0

    aget v5, v2, v5

    neg-int v5, v5

    int-to-float v5, v5

    aget v2, v2, v6

    neg-int v2, v2

    int-to-float v2, v2

    invoke-virtual {v4, v5, v2}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 649463
    iget v2, p0, LX/3um;->i:I

    invoke-virtual {v3, v4, v2}, LX/3wy;->a(Landroid/view/MotionEvent;I)Z

    move-result v3

    .line 649464
    invoke-virtual {v4}, Landroid/view/MotionEvent;->recycle()V

    .line 649465
    invoke-static {p1}, LX/2xd;->a(Landroid/view/MotionEvent;)I

    move-result v2

    .line 649466
    if-eq v2, v0, :cond_6

    const/4 v4, 0x3

    if-eq v2, v4, :cond_6

    move v2, v0

    .line 649467
    :goto_1
    if-eqz v3, :cond_5

    if-nez v2, :cond_1

    :cond_5
    move v0, v1

    goto :goto_0

    :cond_6
    move v2, v1

    .line 649468
    goto :goto_1
.end method

.method public static d(LX/3um;)V
    .locals 2

    .prologue
    .line 649469
    iget-object v0, p0, LX/3um;->f:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 649470
    iget-object v0, p0, LX/3um;->d:Landroid/view/View;

    iget-object v1, p0, LX/3um;->f:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 649471
    :cond_0
    iget-object v0, p0, LX/3um;->e:Ljava/lang/Runnable;

    if-eqz v0, :cond_1

    .line 649472
    iget-object v0, p0, LX/3um;->d:Landroid/view/View;

    iget-object v1, p0, LX/3um;->e:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 649473
    :cond_1
    return-void
.end method


# virtual methods
.method public abstract a()LX/3w1;
.end method

.method public b()Z
    .locals 2

    .prologue
    .line 649443
    invoke-virtual {p0}, LX/3um;->a()LX/3w1;

    move-result-object v0

    .line 649444
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/3w1;->b()Z

    move-result v1

    if-nez v1, :cond_0

    .line 649445
    invoke-virtual {v0}, LX/3w1;->c()V

    .line 649446
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public c()Z
    .locals 2

    .prologue
    .line 649439
    invoke-virtual {p0}, LX/3um;->a()LX/3w1;

    move-result-object v0

    .line 649440
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/3w1;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 649441
    invoke-virtual {v0}, LX/3w1;->a()V

    .line 649442
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 11

    .prologue
    const/4 v5, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 649425
    iget-boolean v10, p0, LX/3um;->g:Z

    .line 649426
    if-eqz v10, :cond_5

    .line 649427
    iget-boolean v0, p0, LX/3um;->h:Z

    if-eqz v0, :cond_2

    .line 649428
    invoke-direct {p0, p2}, LX/3um;->b(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 649429
    :goto_0
    iput-boolean v0, p0, LX/3um;->g:Z

    .line 649430
    if-nez v0, :cond_0

    if-eqz v10, :cond_1

    :cond_0
    move v7, v8

    :cond_1
    return v7

    .line 649431
    :cond_2
    invoke-direct {p0, p2}, LX/3um;->b(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p0}, LX/3um;->c()Z

    move-result v0

    if-nez v0, :cond_4

    :cond_3
    move v0, v8

    goto :goto_0

    :cond_4
    move v0, v7

    goto :goto_0

    .line 649432
    :cond_5
    invoke-direct {p0, p2}, LX/3um;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p0}, LX/3um;->b()Z

    move-result v0

    if-eqz v0, :cond_7

    move v9, v8

    .line 649433
    :goto_1
    if-eqz v9, :cond_6

    .line 649434
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 649435
    const/4 v4, 0x3

    move-wide v2, v0

    move v6, v5

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v0

    .line 649436
    iget-object v1, p0, LX/3um;->d:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 649437
    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    :cond_6
    move v0, v9

    goto :goto_0

    :cond_7
    move v9, v7

    .line 649438
    goto :goto_1
.end method
