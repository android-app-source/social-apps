.class public LX/3fu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/contacts/server/DeleteContactParams;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 623760
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 623761
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 623764
    check-cast p1, Lcom/facebook/contacts/server/DeleteContactParams;

    .line 623765
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 623766
    new-instance v0, LX/14N;

    const-string v1, "deleteContact"

    const-string v2, "DELETE"

    iget-object v3, p1, Lcom/facebook/contacts/server/DeleteContactParams;->a:Lcom/facebook/contacts/graphql/Contact;

    invoke-virtual {v3}, Lcom/facebook/contacts/graphql/Contact;->d()Ljava/lang/String;

    move-result-object v3

    sget-object v5, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 623762
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 623763
    const/4 v0, 0x0

    return-object v0
.end method
