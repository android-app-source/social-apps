.class public LX/3Rj;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/0SG;

.field private final c:LX/3Rk;

.field private final d:LX/0Zb;

.field private final e:LX/13O;

.field private final f:LX/297;

.field private final g:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final h:LX/3Rl;

.field private final i:Landroid/content/DialogInterface$OnClickListener;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/297;Landroid/content/Context;LX/0SG;LX/3Rk;LX/0Zb;LX/13O;LX/3Rl;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 581451
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 581452
    new-instance v0, LX/3Rm;

    invoke-direct {v0, p0}, LX/3Rm;-><init>(LX/3Rj;)V

    iput-object v0, p0, LX/3Rj;->i:Landroid/content/DialogInterface$OnClickListener;

    .line 581453
    iput-object p1, p0, LX/3Rj;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 581454
    iput-object p2, p0, LX/3Rj;->f:LX/297;

    .line 581455
    iput-object p3, p0, LX/3Rj;->a:Landroid/content/Context;

    .line 581456
    iput-object p4, p0, LX/3Rj;->b:LX/0SG;

    .line 581457
    iput-object p5, p0, LX/3Rj;->c:LX/3Rk;

    .line 581458
    iput-object p6, p0, LX/3Rj;->d:LX/0Zb;

    .line 581459
    iput-object p7, p0, LX/3Rj;->e:LX/13O;

    .line 581460
    iput-object p8, p0, LX/3Rj;->h:LX/3Rl;

    .line 581461
    return-void
.end method

.method public static a(LX/0QB;)LX/3Rj;
    .locals 10

    .prologue
    .line 581462
    new-instance v1, LX/3Rj;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v2

    check-cast v2, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/297;->b(LX/0QB;)LX/297;

    move-result-object v3

    check-cast v3, LX/297;

    const-class v4, Landroid/content/Context;

    invoke-interface {p0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v5

    check-cast v5, LX/0SG;

    invoke-static {p0}, LX/3Rk;->a(LX/0QB;)LX/3Rk;

    move-result-object v6

    check-cast v6, LX/3Rk;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v7

    check-cast v7, LX/0Zb;

    invoke-static {p0}, LX/13O;->a(LX/0QB;)LX/13O;

    move-result-object v8

    check-cast v8, LX/13O;

    invoke-static {p0}, LX/3Rl;->a(LX/0QB;)LX/3Rl;

    move-result-object v9

    check-cast v9, LX/3Rl;

    invoke-direct/range {v1 .. v9}, LX/3Rj;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/297;Landroid/content/Context;LX/0SG;LX/3Rk;LX/0Zb;LX/13O;LX/3Rl;)V

    .line 581463
    move-object v0, v1

    .line 581464
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Ljava/util/List;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ")",
            "Ljava/util/List",
            "<",
            "LX/Jui;",
            ">;"
        }
    .end annotation

    .prologue
    const-wide/32 v12, 0x5265c00

    const/4 v10, 0x0

    .line 581465
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 581466
    new-instance v1, LX/Jui;

    iget-object v2, p0, LX/3Rj;->a:Landroid/content/Context;

    const v3, 0x7f080298

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/3Rj;->a:Landroid/content/Context;

    const v4, 0x7f0802a1

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/util/Date;

    iget-object v5, p0, LX/3Rj;->b:LX/0SG;

    invoke-interface {v5}, LX/0SG;->a()J

    move-result-wide v6

    const-wide/32 v8, 0x36ee80

    add-long/2addr v6, v8

    invoke-direct {v4, v6, v7}, Ljava/util/Date;-><init>(J)V

    invoke-direct {v1, v2, v3, v4}, LX/Jui;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 581467
    new-instance v1, LX/Jui;

    iget-object v2, p0, LX/3Rj;->a:Landroid/content/Context;

    const v3, 0x7f080297

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/3Rj;->a:Landroid/content/Context;

    const v4, 0x7f0802a0

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/util/Date;

    iget-object v5, p0, LX/3Rj;->b:LX/0SG;

    invoke-interface {v5}, LX/0SG;->a()J

    move-result-wide v6

    const-wide/32 v8, 0xdbba0

    add-long/2addr v6, v8

    invoke-direct {v4, v6, v7}, Ljava/util/Date;-><init>(J)V

    invoke-direct {v1, v2, v3, v4}, LX/Jui;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;)V

    invoke-interface {v0, v10, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 581468
    new-instance v1, LX/Jui;

    iget-object v2, p0, LX/3Rj;->a:Landroid/content/Context;

    const v3, 0x7f080299

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/3Rj;->a:Landroid/content/Context;

    const v4, 0x7f0802a2

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/util/Date;

    iget-object v5, p0, LX/3Rj;->b:LX/0SG;

    invoke-interface {v5}, LX/0SG;->a()J

    move-result-wide v6

    const-wide/32 v8, 0x1b77400

    add-long/2addr v6, v8

    invoke-direct {v4, v6, v7}, Ljava/util/Date;-><init>(J)V

    invoke-direct {v1, v2, v3, v4}, LX/Jui;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 581469
    new-instance v1, LX/Jui;

    iget-object v2, p0, LX/3Rj;->a:Landroid/content/Context;

    const v3, 0x7f08029a

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/3Rj;->a:Landroid/content/Context;

    const v4, 0x7f0802a3

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/util/Date;

    iget-object v5, p0, LX/3Rj;->b:LX/0SG;

    invoke-interface {v5}, LX/0SG;->a()J

    move-result-wide v6

    add-long/2addr v6, v12

    invoke-direct {v4, v6, v7}, Ljava/util/Date;-><init>(J)V

    invoke-direct {v1, v2, v3, v4}, LX/Jui;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 581470
    iget-object v1, p0, LX/3Rj;->b:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    .line 581471
    iget-object v1, p0, LX/3Rj;->c:LX/3Rk;

    invoke-virtual {v1}, LX/3Rk;->a()Ljava/util/Date;

    move-result-object v1

    .line 581472
    new-instance v4, Ljava/util/Date;

    add-long/2addr v2, v12

    invoke-direct {v4, v2, v3}, Ljava/util/Date;-><init>(J)V

    .line 581473
    if-eqz v1, :cond_0

    invoke-virtual {v1, v4}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 581474
    iget-object v2, p0, LX/3Rj;->a:Landroid/content/Context;

    const v3, 0x7f08029c

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, LX/3Rj;->a:Landroid/content/Context;

    invoke-static {v5}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v10

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 581475
    new-instance v3, LX/Jui;

    iget-object v4, p0, LX/3Rj;->a:Landroid/content/Context;

    const v5, 0x7f0802a5

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v2, v4, v1}, LX/Jui;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 581476
    :cond_0
    if-eqz p1, :cond_1

    .line 581477
    new-instance v1, LX/Jui;

    iget-object v2, p0, LX/3Rj;->a:Landroid/content/Context;

    const v3, 0x7f080296

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/3Rj;->a:Landroid/content/Context;

    const v4, 0x7f0802a6

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/facebook/messaging/model/threads/NotificationSetting;->b:Lcom/facebook/messaging/model/threads/NotificationSetting;

    invoke-direct {v1, v2, v3, v4}, LX/Jui;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/messaging/model/threads/NotificationSetting;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 581478
    :cond_1
    return-object v0
.end method
