.class public final LX/4eG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/43d;


# instance fields
.field public final synthetic a:LX/4eH;


# direct methods
.method public constructor <init>(LX/4eH;)V
    .locals 0

    .prologue
    .line 797109
    iput-object p1, p0, LX/4eG;->a:LX/4eH;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 12

    .prologue
    .line 797110
    check-cast p1, LX/35g;

    check-cast p2, LX/35g;

    .line 797111
    iget-object v0, p0, LX/4eG;->a:LX/4eH;

    invoke-static {v0, p1}, LX/4eH;->a$redex0(LX/4eH;LX/35g;)I

    move-result v2

    .line 797112
    iget-object v0, p0, LX/4eG;->a:LX/4eH;

    invoke-static {v0, p2}, LX/4eH;->a$redex0(LX/4eH;LX/35g;)I

    move-result v3

    .line 797113
    invoke-virtual {p1}, LX/35g;->b()J

    move-result-wide v4

    .line 797114
    invoke-virtual {p2}, LX/35g;->b()J

    move-result-wide v6

    .line 797115
    iget-object v1, p0, LX/4eG;->a:LX/4eH;

    invoke-static/range {v1 .. v7}, LX/4eH;->a(LX/4eH;IIJJ)I

    move-result v0

    .line 797116
    if-eqz v0, :cond_1

    .line 797117
    :cond_0
    :goto_0
    :pswitch_0
    return v0

    .line 797118
    :cond_1
    sub-long v8, v4, v6

    invoke-static {v8, v9}, Ljava/lang/Math;->abs(J)J

    move-result-wide v8

    const-wide/16 v10, 0x2710

    cmp-long v8, v8, v10

    if-gez v8, :cond_2

    .line 797119
    const/4 v8, 0x0

    .line 797120
    :goto_1
    move v1, v8

    .line 797121
    invoke-virtual {p1}, LX/35g;->d()J

    move-result-wide v2

    .line 797122
    invoke-virtual {p2}, LX/35g;->d()J

    move-result-wide v4

    .line 797123
    sub-long v8, v2, v4

    invoke-static {v8, v9}, Ljava/lang/Math;->abs(J)J

    move-result-wide v8

    const-wide/16 v10, 0x2800

    cmp-long v8, v8, v10

    if-gez v8, :cond_4

    .line 797124
    const/4 v8, 0x0

    .line 797125
    :goto_2
    move v0, v8

    .line 797126
    iget-object v2, p0, LX/4eG;->a:LX/4eH;

    iget v2, v2, LX/4eH;->d:I

    packed-switch v2, :pswitch_data_0

    move v0, v1

    .line 797127
    goto :goto_0

    .line 797128
    :pswitch_1
    if-eqz v1, :cond_0

    move v0, v1

    goto :goto_0

    .line 797129
    :pswitch_2
    if-nez v0, :cond_0

    move v0, v1

    goto :goto_0

    :cond_2
    cmp-long v8, v4, v6

    if-gez v8, :cond_3

    const/4 v8, -0x1

    goto :goto_1

    :cond_3
    const/4 v8, 0x1

    goto :goto_1

    :cond_4
    cmp-long v8, v2, v4

    if-gez v8, :cond_5

    const/4 v8, -0x1

    goto :goto_2

    :cond_5
    const/4 v8, 0x1

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
