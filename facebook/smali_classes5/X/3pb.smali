.class public final LX/3pb;
.super LX/3pa;
.source ""


# static fields
.field public static final d:LX/3pV;


# instance fields
.field public a:I

.field public b:Ljava/lang/CharSequence;

.field public c:Landroid/app/PendingIntent;

.field private final e:Landroid/os/Bundle;

.field private final f:[LX/3qL;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 641649
    new-instance v0, LX/3pW;

    invoke-direct {v0}, LX/3pW;-><init>()V

    sput-object v0, LX/3pb;->d:LX/3pV;

    return-void
.end method

.method public constructor <init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V
    .locals 6

    .prologue
    .line 641663
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    const/4 v5, 0x0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, LX/3pb;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;Landroid/os/Bundle;[LX/3qL;)V

    .line 641664
    return-void
.end method

.method private constructor <init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;Landroid/os/Bundle;[LX/3qL;)V
    .locals 1

    .prologue
    .line 641655
    invoke-direct {p0}, LX/3pa;-><init>()V

    .line 641656
    iput p1, p0, LX/3pb;->a:I

    .line 641657
    invoke-static {p2}, LX/2HB;->f(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, LX/3pb;->b:Ljava/lang/CharSequence;

    .line 641658
    iput-object p3, p0, LX/3pb;->c:Landroid/app/PendingIntent;

    .line 641659
    if-eqz p4, :cond_0

    :goto_0
    iput-object p4, p0, LX/3pb;->e:Landroid/os/Bundle;

    .line 641660
    iput-object p5, p0, LX/3pb;->f:[LX/3qL;

    .line 641661
    return-void

    .line 641662
    :cond_0
    new-instance p4, Landroid/os/Bundle;

    invoke-direct {p4}, Landroid/os/Bundle;-><init>()V

    goto :goto_0
.end method

.method public synthetic constructor <init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;Landroid/os/Bundle;[LX/3qL;B)V
    .locals 0

    .prologue
    .line 641654
    invoke-direct/range {p0 .. p5}, LX/3pb;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;Landroid/os/Bundle;[LX/3qL;)V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 641665
    iget v0, p0, LX/3pb;->a:I

    return v0
.end method

.method public final b()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 641653
    iget-object v0, p0, LX/3pb;->b:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final c()Landroid/app/PendingIntent;
    .locals 1

    .prologue
    .line 641652
    iget-object v0, p0, LX/3pb;->c:Landroid/app/PendingIntent;

    return-object v0
.end method

.method public final d()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 641651
    iget-object v0, p0, LX/3pb;->e:Landroid/os/Bundle;

    return-object v0
.end method

.method public final e()[LX/3qK;
    .locals 1

    .prologue
    .line 641650
    iget-object v0, p0, LX/3pb;->f:[LX/3qL;

    return-object v0
.end method
