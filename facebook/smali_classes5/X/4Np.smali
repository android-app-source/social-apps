.class public LX/4Np;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 694678
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 29

    .prologue
    .line 694679
    const/16 v25, 0x0

    .line 694680
    const/16 v24, 0x0

    .line 694681
    const/16 v23, 0x0

    .line 694682
    const/16 v22, 0x0

    .line 694683
    const/16 v21, 0x0

    .line 694684
    const/16 v20, 0x0

    .line 694685
    const/16 v19, 0x0

    .line 694686
    const/16 v18, 0x0

    .line 694687
    const/16 v17, 0x0

    .line 694688
    const/16 v16, 0x0

    .line 694689
    const/4 v15, 0x0

    .line 694690
    const/4 v14, 0x0

    .line 694691
    const/4 v13, 0x0

    .line 694692
    const/4 v12, 0x0

    .line 694693
    const/4 v11, 0x0

    .line 694694
    const/4 v10, 0x0

    .line 694695
    const/4 v9, 0x0

    .line 694696
    const/4 v8, 0x0

    .line 694697
    const/4 v7, 0x0

    .line 694698
    const/4 v6, 0x0

    .line 694699
    const/4 v5, 0x0

    .line 694700
    const/4 v4, 0x0

    .line 694701
    const/4 v3, 0x0

    .line 694702
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v26

    sget-object v27, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    if-eq v0, v1, :cond_1

    .line 694703
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 694704
    const/4 v3, 0x0

    .line 694705
    :goto_0
    return v3

    .line 694706
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 694707
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v26

    sget-object v27, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    if-eq v0, v1, :cond_16

    .line 694708
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v26

    .line 694709
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 694710
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v27

    sget-object v28, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    if-eq v0, v1, :cond_1

    if-eqz v26, :cond_1

    .line 694711
    const-string v27, "estimated_results"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_2

    .line 694712
    const/4 v4, 0x1

    .line 694713
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v25

    goto :goto_1

    .line 694714
    :cond_2
    const-string v27, "filtered_query"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_3

    .line 694715
    invoke-static/range {p0 .. p1}, LX/4Np;->a(LX/15w;LX/186;)I

    move-result v24

    goto :goto_1

    .line 694716
    :cond_3
    const-string v27, "icon_name"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_4

    .line 694717
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v23

    goto :goto_1

    .line 694718
    :cond_4
    const-string v27, "id"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_5

    .line 694719
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v22

    goto :goto_1

    .line 694720
    :cond_5
    const-string v27, "modules"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_6

    .line 694721
    invoke-static/range {p0 .. p1}, LX/4No;->a(LX/15w;LX/186;)I

    move-result v21

    goto :goto_1

    .line 694722
    :cond_6
    const-string v27, "name"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_7

    .line 694723
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v20

    goto/16 :goto_1

    .line 694724
    :cond_7
    const-string v27, "name_search_tokens"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_8

    .line 694725
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v19

    goto/16 :goto_1

    .line 694726
    :cond_8
    const-string v27, "needle_filters"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_9

    .line 694727
    invoke-static/range {p0 .. p1}, LX/4Ns;->a(LX/15w;LX/186;)I

    move-result v18

    goto/16 :goto_1

    .line 694728
    :cond_9
    const-string v27, "post_search_intent_log"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_a

    .line 694729
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v17

    goto/16 :goto_1

    .line 694730
    :cond_a
    const-string v27, "query_display_style"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_b

    .line 694731
    const/4 v3, 0x1

    .line 694732
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Lcom/facebook/graphql/enums/GraphQLGraphSearchQueryDisplayStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGraphSearchQueryDisplayStyle;

    move-result-object v16

    goto/16 :goto_1

    .line 694733
    :cond_b
    const-string v27, "query_function"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_c

    .line 694734
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    goto/16 :goto_1

    .line 694735
    :cond_c
    const-string v27, "query_role"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_d

    .line 694736
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    goto/16 :goto_1

    .line 694737
    :cond_d
    const-string v27, "query_title"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_e

    .line 694738
    invoke-static/range {p0 .. p1}, LX/4Nx;->a(LX/15w;LX/186;)I

    move-result v13

    goto/16 :goto_1

    .line 694739
    :cond_e
    const-string v27, "results"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_f

    .line 694740
    invoke-static/range {p0 .. p1}, LX/4Nz;->a(LX/15w;LX/186;)I

    move-result v12

    goto/16 :goto_1

    .line 694741
    :cond_f
    const-string v27, "search_result_style_list"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_10

    .line 694742
    const-class v11, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v11}, LX/2gu;->a(LX/15w;LX/186;Ljava/lang/Class;)I

    move-result v11

    goto/16 :goto_1

    .line 694743
    :cond_10
    const-string v27, "session_id"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_11

    .line 694744
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto/16 :goto_1

    .line 694745
    :cond_11
    const-string v27, "top_filters"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_12

    .line 694746
    invoke-static/range {p0 .. p1}, LX/4Ns;->a(LX/15w;LX/186;)I

    move-result v9

    goto/16 :goto_1

    .line 694747
    :cond_12
    const-string v27, "url"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_13

    .line 694748
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto/16 :goto_1

    .line 694749
    :cond_13
    const-string v27, "vertical_to_log"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_14

    .line 694750
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto/16 :goto_1

    .line 694751
    :cond_14
    const-string v27, "filter_type_sets"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_15

    .line 694752
    invoke-static/range {p0 .. p1}, LX/4Nt;->a(LX/15w;LX/186;)I

    move-result v6

    goto/16 :goto_1

    .line 694753
    :cond_15
    const-string v27, "logging_unit_id"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_0

    .line 694754
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto/16 :goto_1

    .line 694755
    :cond_16
    const/16 v26, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 694756
    if-eqz v4, :cond_17

    .line 694757
    const/4 v4, 0x1

    const/16 v26, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v25

    move/from16 v2, v26

    invoke-virtual {v0, v4, v1, v2}, LX/186;->a(III)V

    .line 694758
    :cond_17
    const/4 v4, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 694759
    const/4 v4, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 694760
    const/4 v4, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 694761
    const/4 v4, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 694762
    const/4 v4, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 694763
    const/4 v4, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 694764
    const/16 v4, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 694765
    const/16 v4, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 694766
    if-eqz v3, :cond_18

    .line 694767
    const/16 v3, 0xa

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v3, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 694768
    :cond_18
    const/16 v3, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v15}, LX/186;->b(II)V

    .line 694769
    const/16 v3, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v14}, LX/186;->b(II)V

    .line 694770
    const/16 v3, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v13}, LX/186;->b(II)V

    .line 694771
    const/16 v3, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v12}, LX/186;->b(II)V

    .line 694772
    const/16 v3, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, LX/186;->b(II)V

    .line 694773
    const/16 v3, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10}, LX/186;->b(II)V

    .line 694774
    const/16 v3, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 694775
    const/16 v3, 0x13

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 694776
    const/16 v3, 0x14

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 694777
    const/16 v3, 0x15

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 694778
    const/16 v3, 0x16

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v5}, LX/186;->b(II)V

    .line 694779
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const/16 v5, 0xf

    const/16 v4, 0xa

    const/4 v3, 0x7

    const/4 v2, 0x0

    .line 694591
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 694592
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 694593
    if-eqz v0, :cond_0

    .line 694594
    const-string v1, "estimated_results"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 694595
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 694596
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 694597
    if-eqz v0, :cond_1

    .line 694598
    const-string v1, "filtered_query"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 694599
    invoke-static {p0, v0, p2, p3}, LX/4Np;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 694600
    :cond_1
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 694601
    if-eqz v0, :cond_2

    .line 694602
    const-string v1, "icon_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 694603
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 694604
    :cond_2
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 694605
    if-eqz v0, :cond_3

    .line 694606
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 694607
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 694608
    :cond_3
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 694609
    if-eqz v0, :cond_4

    .line 694610
    const-string v1, "modules"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 694611
    invoke-static {p0, v0, p2, p3}, LX/4No;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 694612
    :cond_4
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 694613
    if-eqz v0, :cond_5

    .line 694614
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 694615
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 694616
    :cond_5
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 694617
    if-eqz v0, :cond_6

    .line 694618
    const-string v0, "name_search_tokens"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 694619
    invoke-virtual {p0, p1, v3}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 694620
    :cond_6
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 694621
    if-eqz v0, :cond_7

    .line 694622
    const-string v1, "needle_filters"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 694623
    invoke-static {p0, v0, p2, p3}, LX/4Ns;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 694624
    :cond_7
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 694625
    if-eqz v0, :cond_8

    .line 694626
    const-string v1, "post_search_intent_log"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 694627
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 694628
    :cond_8
    invoke-virtual {p0, p1, v4, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 694629
    if-eqz v0, :cond_9

    .line 694630
    const-string v0, "query_display_style"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 694631
    const-class v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchQueryDisplayStyle;

    invoke-virtual {p0, p1, v4, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchQueryDisplayStyle;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLGraphSearchQueryDisplayStyle;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 694632
    :cond_9
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 694633
    if-eqz v0, :cond_a

    .line 694634
    const-string v1, "query_function"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 694635
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 694636
    :cond_a
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 694637
    if-eqz v0, :cond_b

    .line 694638
    const-string v1, "query_role"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 694639
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 694640
    :cond_b
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 694641
    if-eqz v0, :cond_c

    .line 694642
    const-string v1, "query_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 694643
    invoke-static {p0, v0, p2}, LX/4Nx;->a(LX/15i;ILX/0nX;)V

    .line 694644
    :cond_c
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 694645
    if-eqz v0, :cond_d

    .line 694646
    const-string v1, "results"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 694647
    invoke-static {p0, v0, p2, p3}, LX/4Nz;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 694648
    :cond_d
    invoke-virtual {p0, p1, v5}, LX/15i;->g(II)I

    move-result v0

    .line 694649
    if-eqz v0, :cond_e

    .line 694650
    const-string v0, "search_result_style_list"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 694651
    const-class v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-virtual {p0, p1, v5, v0}, LX/15i;->b(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->b(Ljava/util/Iterator;LX/0nX;)V

    .line 694652
    :cond_e
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 694653
    if-eqz v0, :cond_f

    .line 694654
    const-string v1, "session_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 694655
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 694656
    :cond_f
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 694657
    if-eqz v0, :cond_10

    .line 694658
    const-string v1, "top_filters"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 694659
    invoke-static {p0, v0, p2, p3}, LX/4Ns;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 694660
    :cond_10
    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 694661
    if-eqz v0, :cond_11

    .line 694662
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 694663
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 694664
    :cond_11
    const/16 v0, 0x14

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 694665
    if-eqz v0, :cond_12

    .line 694666
    const-string v1, "vertical_to_log"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 694667
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 694668
    :cond_12
    const/16 v0, 0x15

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 694669
    if-eqz v0, :cond_13

    .line 694670
    const-string v1, "filter_type_sets"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 694671
    invoke-static {p0, v0, p2, p3}, LX/4Nt;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 694672
    :cond_13
    const/16 v0, 0x16

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 694673
    if-eqz v0, :cond_14

    .line 694674
    const-string v1, "logging_unit_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 694675
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 694676
    :cond_14
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 694677
    return-void
.end method
