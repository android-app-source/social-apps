.class public LX/4CO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4C5;
.implements LX/4C7;


# instance fields
.field private final a:Landroid/graphics/Paint;

.field private b:LX/4dH;

.field private volatile c:LX/1FJ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private d:Landroid/graphics/Rect;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/4dH;)V
    .locals 2

    .prologue
    .line 678819
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 678820
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x6

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, LX/4CO;->a:Landroid/graphics/Paint;

    .line 678821
    iput-object p1, p0, LX/4CO;->b:LX/4dH;

    .line 678822
    return-void
.end method

.method private a(LX/1FJ;Landroid/graphics/Canvas;Landroid/graphics/Rect;)Z
    .locals 3
    .param p3    # Landroid/graphics/Rect;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Landroid/graphics/Canvas;",
            "Landroid/graphics/Rect;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 678809
    invoke-static {p1}, LX/1FJ;->a(LX/1FJ;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 678810
    const/4 v0, 0x0

    .line 678811
    :goto_0
    return v0

    .line 678812
    :cond_0
    if-nez p3, :cond_2

    .line 678813
    invoke-virtual {p1}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    iget-object v1, p0, LX/4CO;->a:Landroid/graphics/Paint;

    invoke-virtual {p2, v0, v2, v2, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 678814
    :goto_1
    iget-object v0, p0, LX/4CO;->c:LX/1FJ;

    if-eq p1, v0, :cond_1

    .line 678815
    iget-object v0, p0, LX/4CO;->c:LX/1FJ;

    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    .line 678816
    iput-object p1, p0, LX/4CO;->c:LX/1FJ;

    .line 678817
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 678818
    :cond_2
    invoke-virtual {p1}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    iget-object v2, p0, LX/4CO;->a:Landroid/graphics/Paint;

    invoke-virtual {p2, v0, v1, p3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_1
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 678808
    iget-object v0, p0, LX/4CO;->b:LX/4dH;

    invoke-interface {v0}, LX/4dG;->e()I

    move-result v0

    return v0
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 678806
    iget-object v0, p0, LX/4CO;->a:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 678807
    return-void
.end method

.method public final a(Landroid/graphics/ColorFilter;)V
    .locals 1

    .prologue
    .line 678804
    iget-object v0, p0, LX/4CO;->a:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 678805
    return-void
.end method

.method public final a(Landroid/graphics/Rect;)V
    .locals 2
    .param p1    # Landroid/graphics/Rect;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 678795
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/4CO;->d:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 678796
    :goto_0
    return-void

    .line 678797
    :cond_0
    iput-object p1, p0, LX/4CO;->d:Landroid/graphics/Rect;

    .line 678798
    iget-object v0, p0, LX/4CO;->b:LX/4dH;

    invoke-interface {v0, p1}, LX/4dH;->b(Landroid/graphics/Rect;)LX/4dH;

    move-result-object v0

    .line 678799
    iget-object v1, p0, LX/4CO;->b:LX/4dH;

    if-eq v0, v1, :cond_1

    .line 678800
    iget-object v1, p0, LX/4CO;->b:LX/4dH;

    invoke-interface {v1}, LX/4dG;->k()V

    .line 678801
    :cond_1
    iput-object v0, p0, LX/4CO;->b:LX/4dH;

    .line 678802
    iget-object v0, p0, LX/4CO;->c:LX/1FJ;

    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    .line 678803
    const/4 v0, 0x0

    iput-object v0, p0, LX/4CO;->c:LX/1FJ;

    goto :goto_0
.end method

.method public final a(Landroid/graphics/drawable/Drawable;Landroid/graphics/Canvas;I)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 678789
    iget-object v1, p0, LX/4CO;->b:LX/4dH;

    invoke-interface {v1, p3}, LX/4dH;->g(I)LX/1FJ;

    move-result-object v1

    iget-object v2, p0, LX/4CO;->d:Landroid/graphics/Rect;

    invoke-direct {p0, v1, p2, v2}, LX/4CO;->a(LX/1FJ;Landroid/graphics/Canvas;Landroid/graphics/Rect;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 678790
    :goto_0
    return v0

    .line 678791
    :cond_0
    iget-object v1, p0, LX/4CO;->c:LX/1FJ;

    if-eqz v1, :cond_2

    .line 678792
    iget-object v0, p0, LX/4CO;->c:LX/1FJ;

    iget-object v1, p0, LX/4CO;->d:Landroid/graphics/Rect;

    invoke-direct {p0, v0, p2, v1}, LX/4CO;->a(LX/1FJ;Landroid/graphics/Canvas;Landroid/graphics/Rect;)Z

    .line 678793
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 678794
    :cond_2
    iget-object v1, p0, LX/4CO;->b:LX/4dH;

    invoke-interface {v1}, LX/4dH;->l()LX/1FJ;

    move-result-object v1

    iget-object v2, p0, LX/4CO;->d:Landroid/graphics/Rect;

    invoke-direct {p0, v1, p2, v2}, LX/4CO;->a(LX/1FJ;Landroid/graphics/Canvas;Landroid/graphics/Rect;)Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 678788
    iget-object v0, p0, LX/4CO;->b:LX/4dH;

    invoke-interface {v0}, LX/4dG;->f()I

    move-result v0

    return v0
.end method

.method public final b(I)I
    .locals 1

    .prologue
    .line 678778
    iget-object v0, p0, LX/4CO;->b:LX/4dH;

    invoke-interface {v0, p1}, LX/4dG;->d(I)I

    move-result v0

    return v0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 678783
    iget-object v0, p0, LX/4CO;->b:LX/4dH;

    if-eqz v0, :cond_0

    .line 678784
    iget-object v0, p0, LX/4CO;->b:LX/4dH;

    invoke-interface {v0}, LX/4dG;->k()V

    .line 678785
    :cond_0
    iget-object v0, p0, LX/4CO;->c:LX/1FJ;

    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    .line 678786
    const/4 v0, 0x0

    iput-object v0, p0, LX/4CO;->c:LX/1FJ;

    .line 678787
    return-void
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 678782
    iget-object v0, p0, LX/4CO;->b:LX/4dH;

    invoke-interface {v0}, LX/4dG;->c()I

    move-result v0

    return v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 678781
    iget-object v0, p0, LX/4CO;->b:LX/4dH;

    invoke-interface {v0}, LX/4dG;->d()I

    move-result v0

    return v0
.end method

.method public final f()V
    .locals 0

    .prologue
    .line 678779
    invoke-virtual {p0}, LX/4CO;->c()V

    .line 678780
    return-void
.end method
