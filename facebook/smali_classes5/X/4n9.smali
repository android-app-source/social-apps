.class public LX/4n9;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Landroid/net/Uri;

.field public final b:LX/4n3;

.field public final c:LX/4nA;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:LX/4n2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:Ljava/lang/String;

.field public final f:LX/4n5;

.field public final g:Z

.field public final h:Z

.field public final i:Z

.field public final j:Z

.field public volatile k:LX/4n7;

.field public volatile l:LX/4n7;


# direct methods
.method public constructor <init>(LX/4n8;)V
    .locals 5

    .prologue
    .line 806560
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 806561
    iget-object v0, p1, LX/4n8;->a:Landroid/net/Uri;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 806562
    iget-object v0, p1, LX/4n8;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->isAbsolute()Z

    move-result v0

    const-string v1, "Url %s is not absolute"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p1, LX/4n8;->a:Landroid/net/Uri;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/0PB;->checkArgument(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 806563
    iget-object v0, p1, LX/4n8;->e:LX/4n5;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 806564
    iget-object v0, p1, LX/4n8;->a:Landroid/net/Uri;

    iput-object v0, p0, LX/4n9;->a:Landroid/net/Uri;

    .line 806565
    iget-object v0, p1, LX/4n8;->c:LX/4n3;

    iput-object v0, p0, LX/4n9;->b:LX/4n3;

    .line 806566
    iget-object v0, p1, LX/4n8;->b:LX/4nA;

    iput-object v0, p0, LX/4n9;->c:LX/4nA;

    .line 806567
    iget-object v0, p1, LX/4n8;->d:LX/4n2;

    iput-object v0, p0, LX/4n9;->d:LX/4n2;

    .line 806568
    iget-object v0, p1, LX/4n8;->e:LX/4n5;

    iput-object v0, p0, LX/4n9;->f:LX/4n5;

    .line 806569
    iget-boolean v0, p1, LX/4n8;->f:Z

    iput-boolean v0, p0, LX/4n9;->g:Z

    .line 806570
    iget-boolean v0, p1, LX/4n8;->g:Z

    iput-boolean v0, p0, LX/4n9;->h:Z

    .line 806571
    iget-boolean v0, p1, LX/4n8;->h:Z

    iput-boolean v0, p0, LX/4n9;->i:Z

    .line 806572
    iget-boolean v0, p1, LX/4n8;->i:Z

    iput-boolean v0, p0, LX/4n9;->j:Z

    .line 806573
    iget-object v0, p0, LX/4n9;->d:LX/4n2;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/4n9;->d:LX/4n2;

    invoke-virtual {v0}, LX/4n2;->a()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, LX/4n9;->e:Ljava/lang/String;

    .line 806574
    return-void

    .line 806575
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/net/Uri;)LX/4n8;
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 806576
    new-instance v0, LX/4n8;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, LX/4n8;-><init>(Landroid/net/Uri;LX/4nA;)V

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 806577
    iget-object v0, p0, LX/4n9;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
