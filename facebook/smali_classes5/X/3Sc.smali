.class public final LX/3Sc;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/3Sd;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "EMPTY_ARRAY"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 582868
    const/4 v0, 0x0

    invoke-static {v0}, LX/3Sd;->a(I)LX/3Sd;

    move-result-object v0

    sput-object v0, LX/3Sc;->a:LX/3Sd;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 582859
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(LX/15i;III)LX/1vs;
    .locals 3

    .prologue
    .line 582869
    if-nez p1, :cond_0

    .line 582870
    new-instance v0, Ljava/lang/NullPointerException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "at index "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 582871
    :cond_0
    invoke-static {p0, p1, p2}, LX/1vs;->a(LX/15i;II)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/3Sd;I)LX/3Sd;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 582865
    invoke-static {p1}, LX/3Sd;->a(I)LX/3Sd;

    move-result-object v0

    .line 582866
    iget v1, p0, LX/3Sd;->b:I

    invoke-static {v1, p1}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {p0, v2, v0, v2, v1}, LX/3Sd;->a(LX/3Sd;ILX/3Sd;II)V

    .line 582867
    return-object v0
.end method

.method public static b(LX/3Sd;I)LX/3Sd;
    .locals 5

    .prologue
    .line 582860
    const/4 v0, 0x0

    :goto_0
    if-ge v0, p1, :cond_0

    .line 582861
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p0, v0}, LX/3Sd;->b(I)LX/15i;

    move-result-object v2

    invoke-virtual {p0, v0}, LX/3Sd;->c(I)I

    move-result v3

    invoke-virtual {p0, v0}, LX/3Sd;->d(I)I

    move-result v4

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {v2, v3, v4, v0}, LX/3Sc;->a(LX/15i;III)LX/1vs;

    .line 582862
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 582863
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 582864
    :cond_0
    return-object p0
.end method
