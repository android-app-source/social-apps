.class public LX/4r6;
.super LX/4r1;
.source ""


# static fields
.field private static final serialVersionUID:J = 0x1L


# direct methods
.method public constructor <init>(LX/0lJ;LX/4qx;Ljava/lang/String;ZLjava/lang/Class;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lJ;",
            "LX/4qx;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 815115
    invoke-direct/range {p0 .. p5}, LX/4r1;-><init>(LX/0lJ;LX/4qx;Ljava/lang/String;ZLjava/lang/Class;)V

    .line 815116
    return-void
.end method

.method private constructor <init>(LX/4r6;LX/2Ay;)V
    .locals 0

    .prologue
    .line 815113
    invoke-direct {p0, p1, p2}, LX/4r1;-><init>(LX/4r1;LX/2Ay;)V

    .line 815114
    return-void
.end method

.method private a(LX/15w;LX/0n3;LX/0nW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 815064
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    .line 815065
    invoke-virtual {p0, p2, v0}, LX/4r0;->a(LX/0n3;Ljava/lang/String;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v1

    .line 815066
    iget-boolean v2, p0, LX/4r0;->_typeIdVisible:Z

    if-eqz v2, :cond_1

    .line 815067
    if-nez p3, :cond_0

    .line 815068
    new-instance p3, LX/0nW;

    const/4 v2, 0x0

    invoke-direct {p3, v2}, LX/0nW;-><init>(LX/0lD;)V

    .line 815069
    :cond_0
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 815070
    invoke-virtual {p3, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 815071
    :cond_1
    if-eqz p3, :cond_2

    .line 815072
    invoke-virtual {p3, p1}, LX/0nW;->a(LX/15w;)LX/15w;

    move-result-object v0

    invoke-static {v0, p1}, LX/4pj;->a(LX/15w;LX/15w;)LX/4pj;

    move-result-object p1

    .line 815073
    :cond_2
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 815074
    invoke-virtual {v1, p1, p2}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private b(LX/15w;LX/0n3;LX/0nW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 815100
    invoke-virtual {p0, p2}, LX/4r0;->a(LX/0n3;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v0

    .line 815101
    if-eqz v0, :cond_2

    .line 815102
    if-eqz p3, :cond_0

    .line 815103
    invoke-virtual {p3}, LX/0nX;->g()V

    .line 815104
    invoke-virtual {p3, p1}, LX/0nW;->a(LX/15w;)LX/15w;

    move-result-object p1

    .line 815105
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 815106
    :cond_0
    invoke-virtual {v0, p1, p2}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    .line 815107
    :cond_1
    :goto_0
    return-object v0

    .line 815108
    :cond_2
    iget-object v0, p0, LX/4r0;->_baseType:LX/0lJ;

    invoke-static {p1, p2, v0}, LX/4qw;->a(LX/15w;LX/0n3;LX/0lJ;)Ljava/lang/Object;

    move-result-object v0

    .line 815109
    if-nez v0, :cond_1

    .line 815110
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v1, LX/15z;->START_ARRAY:LX/15z;

    if-ne v0, v1, :cond_3

    .line 815111
    invoke-super {p0, p1, p2}, LX/4r1;->d(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 815112
    :cond_3
    sget-object v0, LX/15z;->FIELD_NAME:LX/15z;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "missing property \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/4r0;->_typePropertyName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' that is to contain type id  (for class "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, LX/4r0;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/0n3;->a(LX/15w;LX/15z;Ljava/lang/String;)LX/28E;

    move-result-object v0

    throw v0
.end method


# virtual methods
.method public final a()LX/4pO;
    .locals 1

    .prologue
    .line 815099
    sget-object v0, LX/4pO;->PROPERTY:LX/4pO;

    return-object v0
.end method

.method public final a(LX/2Ay;)LX/4qw;
    .locals 1

    .prologue
    .line 815097
    iget-object v0, p0, LX/4r0;->_property:LX/2Ay;

    if-ne p1, v0, :cond_0

    .line 815098
    :goto_0
    return-object p0

    :cond_0
    new-instance v0, LX/4r6;

    invoke-direct {v0, p0, p1}, LX/4r6;-><init>(LX/4r6;LX/2Ay;)V

    move-object p0, v0

    goto :goto_0
.end method

.method public final a(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 815078
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v0

    .line 815079
    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-ne v0, v2, :cond_1

    .line 815080
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v0

    :cond_0
    move-object v2, v0

    move-object v0, v1

    .line 815081
    :goto_0
    sget-object v3, LX/15z;->FIELD_NAME:LX/15z;

    if-ne v2, v3, :cond_5

    .line 815082
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 815083
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 815084
    iget-object v3, p0, LX/4r0;->_typePropertyName:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 815085
    invoke-direct {p0, p1, p2, v0}, LX/4r6;->a(LX/15w;LX/0n3;LX/0nW;)Ljava/lang/Object;

    move-result-object v0

    .line 815086
    :goto_1
    return-object v0

    .line 815087
    :cond_1
    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v0, v2, :cond_2

    .line 815088
    invoke-direct {p0, p1, p2, v1}, LX/4r6;->b(LX/15w;LX/0n3;LX/0nW;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_1

    .line 815089
    :cond_2
    sget-object v2, LX/15z;->FIELD_NAME:LX/15z;

    if-eq v0, v2, :cond_0

    .line 815090
    invoke-direct {p0, p1, p2, v1}, LX/4r6;->b(LX/15w;LX/0n3;LX/0nW;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_1

    .line 815091
    :cond_3
    if-nez v0, :cond_4

    .line 815092
    new-instance v0, LX/0nW;

    invoke-direct {v0, v1}, LX/0nW;-><init>(LX/0lD;)V

    .line 815093
    :cond_4
    invoke-virtual {v0, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 815094
    invoke-virtual {v0, p1}, LX/0nW;->b(LX/15w;)V

    .line 815095
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v2

    goto :goto_0

    .line 815096
    :cond_5
    invoke-direct {p0, p1, p2, v0}, LX/4r6;->b(LX/15w;LX/0n3;LX/0nW;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_1
.end method

.method public final d(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 815075
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v1, LX/15z;->START_ARRAY:LX/15z;

    if-ne v0, v1, :cond_0

    .line 815076
    invoke-super {p0, p1, p2}, LX/4r1;->b(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    .line 815077
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, p1, p2}, LX/4qw;->a(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method
