.class public LX/4iW;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final EMPTY_BYTE_ARRAY:[B


# instance fields
.field private mBodyBuffersPool:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<[B>;"
        }
    .end annotation
.end field

.field public mDelegate:Lcom/facebook/proxygen/JniHandler;

.field private mHandlerInterface:LX/4iT;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 803218
    const/4 v0, 0x0

    new-array v0, v0, [B

    sput-object v0, LX/4iW;->EMPTY_BYTE_ARRAY:[B

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 803214
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 803215
    new-instance v0, LX/4iT;

    invoke-direct {v0, p0}, LX/4iT;-><init>(LX/4iW;)V

    iput-object v0, p0, LX/4iW;->mHandlerInterface:LX/4iT;

    .line 803216
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0x14

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, LX/4iW;->mBodyBuffersPool:Ljava/util/ArrayList;

    .line 803217
    return-void
.end method

.method private static declared-synchronized acquireBodyBuffer(LX/4iW;)[B
    .locals 2

    .prologue
    .line 803210
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/4iW;->mBodyBuffersPool:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 803211
    const/16 v0, 0x1000

    new-array v0, v0, [B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 803212
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    iget-object v0, p0, LX/4iW;->mBodyBuffersPool:Ljava/util/ArrayList;

    iget-object v1, p0, LX/4iW;->mBodyBuffersPool:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 803213
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private isChunkedRequest(Lorg/apache/http/HttpEntityEnclosingRequest;)Z
    .locals 2

    .prologue
    .line 803206
    const-string v0, "Transfer-Encoding"

    invoke-interface {p1, v0}, Lorg/apache/http/HttpEntityEnclosingRequest;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v0

    .line 803207
    if-eqz v0, :cond_0

    const-string v1, "chunked"

    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 803208
    const/4 v0, 0x1

    .line 803209
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static processEntityRequest(LX/4iW;Lorg/apache/http/HttpEntityEnclosingRequest;)V
    .locals 5

    .prologue
    .line 803198
    invoke-interface {p1}, Lorg/apache/http/HttpEntityEnclosingRequest;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    .line 803199
    if-eqz v0, :cond_0

    .line 803200
    new-instance v1, LX/4iR;

    new-instance v2, LX/4iQ;

    new-instance v3, LX/4iU;

    invoke-direct {v3, p0}, LX/4iU;-><init>(LX/4iW;)V

    invoke-direct {v2, v3}, LX/4iQ;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {v1, v2}, LX/4iR;-><init>(Ljava/io/OutputStream;)V

    .line 803201
    :try_start_0
    invoke-interface {v0, v1}, Lorg/apache/http/HttpEntity;->writeTo(Ljava/io/OutputStream;)V

    .line 803202
    invoke-virtual {v1}, LX/4iR;->flush()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 803203
    invoke-virtual {v1}, LX/4iR;->reallyClose()V

    .line 803204
    :cond_0
    return-void

    .line 803205
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, LX/4iR;->reallyClose()V

    throw v0
.end method

.method private static declared-synchronized releaseBodyBuffer(LX/4iW;[B)V
    .locals 2

    .prologue
    .line 803194
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/4iW;->mBodyBuffersPool:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    const/16 v1, 0x14

    if-ne v0, v1, :cond_0

    .line 803195
    :goto_0
    monitor-exit p0

    return-void

    .line 803196
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/4iW;->mBodyBuffersPool:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 803197
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static sendBody(LX/4iW;[BII)Z
    .locals 1

    .prologue
    .line 803219
    iget-object v0, p0, LX/4iW;->mDelegate:Lcom/facebook/proxygen/JniHandler;

    .line 803220
    if-eqz v0, :cond_0

    .line 803221
    invoke-virtual {v0, p1, p2, p3}, Lcom/facebook/proxygen/JniHandler;->sendBody([BII)Z

    move-result v0

    .line 803222
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static sendEOM(LX/4iW;)Z
    .locals 1

    .prologue
    .line 803190
    iget-object v0, p0, LX/4iW;->mDelegate:Lcom/facebook/proxygen/JniHandler;

    .line 803191
    if-eqz v0, :cond_0

    .line 803192
    invoke-virtual {v0}, Lcom/facebook/proxygen/JniHandler;->sendEOM()Z

    move-result v0

    .line 803193
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 803185
    iget-object v0, p0, LX/4iW;->mDelegate:Lcom/facebook/proxygen/JniHandler;

    .line 803186
    if-eqz v0, :cond_0

    .line 803187
    invoke-virtual {v0}, Lcom/facebook/proxygen/JniHandler;->cancel()V

    .line 803188
    const/4 v0, 0x0

    iput-object v0, p0, LX/4iW;->mDelegate:Lcom/facebook/proxygen/JniHandler;

    .line 803189
    :cond_0
    return-void
.end method

.method public execute(Lorg/apache/http/client/methods/HttpUriRequest;)V
    .locals 1

    .prologue
    .line 803156
    invoke-virtual {p0, p1}, LX/4iW;->sendHeaders(Lorg/apache/http/client/methods/HttpUriRequest;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 803157
    :goto_0
    return-void

    .line 803158
    :cond_0
    invoke-virtual {p0, p1}, LX/4iW;->sendRequestBody(Lorg/apache/http/client/methods/HttpUriRequest;)V

    .line 803159
    invoke-static {p0}, LX/4iW;->sendEOM(LX/4iW;)Z

    goto :goto_0
.end method

.method public executeWithDefragmentation(Lorg/apache/http/client/methods/HttpUriRequest;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 803175
    instance-of v0, p1, Lorg/apache/http/HttpEntityEnclosingRequest;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lorg/apache/http/HttpEntityEnclosingRequest;

    invoke-interface {v0}, Lorg/apache/http/HttpEntityEnclosingRequest;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 803176
    :cond_0
    sget-object v0, LX/4iW;->EMPTY_BYTE_ARRAY:[B

    invoke-virtual {p0, p1, v0, v1, v1}, LX/4iW;->sendHeadersWithBodyAndEom(Lorg/apache/http/client/methods/HttpUriRequest;[BII)Z

    .line 803177
    :goto_0
    return-void

    .line 803178
    :cond_1
    check-cast p1, Lorg/apache/http/HttpEntityEnclosingRequest;

    .line 803179
    invoke-interface {p1}, Lorg/apache/http/HttpEntityEnclosingRequest;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    .line 803180
    invoke-static {p0}, LX/4iW;->acquireBodyBuffer(LX/4iW;)[B

    move-result-object v1

    .line 803181
    :try_start_0
    new-instance v2, LX/4ik;

    iget-object v3, p0, LX/4iW;->mHandlerInterface:LX/4iT;

    invoke-direct {v2, v3, p1, v1}, LX/4ik;-><init>(LX/4iS;Lorg/apache/http/HttpEntityEnclosingRequest;[B)V

    .line 803182
    invoke-interface {v0, v2}, Lorg/apache/http/HttpEntity;->writeTo(Ljava/io/OutputStream;)V

    .line 803183
    invoke-virtual {v2}, LX/4ik;->writeEomIfNecessary()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 803184
    invoke-static {p0, v1}, LX/4iW;->releaseBodyBuffer(LX/4iW;[B)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {p0, v1}, LX/4iW;->releaseBodyBuffer(LX/4iW;[B)V

    throw v0
.end method

.method public sendHeaders(Lorg/apache/http/client/methods/HttpUriRequest;)Z
    .locals 1

    .prologue
    .line 803171
    iget-object v0, p0, LX/4iW;->mDelegate:Lcom/facebook/proxygen/JniHandler;

    .line 803172
    if-eqz v0, :cond_0

    .line 803173
    invoke-virtual {v0, p1}, Lcom/facebook/proxygen/JniHandler;->sendHeaders(Lorg/apache/http/client/methods/HttpUriRequest;)Z

    move-result v0

    .line 803174
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public sendHeadersWithBodyAndEom(Lorg/apache/http/client/methods/HttpUriRequest;[BII)Z
    .locals 1

    .prologue
    .line 803167
    iget-object v0, p0, LX/4iW;->mDelegate:Lcom/facebook/proxygen/JniHandler;

    .line 803168
    if-eqz v0, :cond_0

    .line 803169
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/facebook/proxygen/JniHandler;->sendRequestWithBodyAndEom(Lorg/apache/http/client/methods/HttpUriRequest;[BII)Z

    move-result v0

    .line 803170
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public sendRequestBody(Lorg/apache/http/client/methods/HttpUriRequest;)V
    .locals 1

    .prologue
    .line 803160
    instance-of v0, p1, Lorg/apache/http/HttpEntityEnclosingRequest;

    if-nez v0, :cond_0

    .line 803161
    :goto_0
    return-void

    .line 803162
    :cond_0
    :try_start_0
    check-cast p1, Lorg/apache/http/HttpEntityEnclosingRequest;

    invoke-static {p0, p1}, LX/4iW;->processEntityRequest(LX/4iW;Lorg/apache/http/HttpEntityEnclosingRequest;)V
    :try_end_0
    .catch LX/4iV; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 803163
    :catch_0
    goto :goto_0

    .line 803164
    :catch_1
    move-exception v0

    .line 803165
    invoke-virtual {p0}, LX/4iW;->cancel()V

    .line 803166
    throw v0
.end method
