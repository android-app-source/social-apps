.class public LX/3ZU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1TE;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 600336
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 600337
    return-void
.end method

.method public static a(LX/0QB;)LX/3ZU;
    .locals 1

    .prologue
    .line 600338
    new-instance v0, LX/3ZU;

    invoke-direct {v0}, LX/3ZU;-><init>()V

    .line 600339
    move-object v0, v0

    .line 600340
    return-object v0
.end method


# virtual methods
.method public final a(LX/1KB;)V
    .locals 1

    .prologue
    .line 600341
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PagesMapUnitComponentPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 600342
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageInfoDescriptionUnitComponentPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 600343
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageInfoRowUnitComponentPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 600344
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageInfoRowUnitComponentComponentPartDefinition;->d:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 600345
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageAboutInfoGridUnitComponentPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 600346
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageAboutDescriptionUnitComponentPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 600347
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageNuxUnitComponentPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 600348
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageOpenHoursUnitComponentPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 600349
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageAboutOpenHoursGridUnitComponentPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 600350
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageOpenHoursUnitComponentComponentPartDefinition;->d:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 600351
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageAddressNavigationUnitComponentPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 600352
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageAddressNavigationUnitComponentComponentPartDefinition;->d:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 600353
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageContactInfoStackUnitComponentPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 600354
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageInfoRatingsAndReviewsUnitComponentPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 600355
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageInfoWriteFirstReviewUnitComponentPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 600356
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageInfoWriteFirstReviewUnitComponentComponentPartDefinition;->d:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 600357
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageSocialContextImageBlockUnitComponentPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 600358
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageSocialContextImageBlockUnitComponentComponentPartDefinition;->d:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 600359
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 600360
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageCreatePhotoAlbumUnitComponentPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 600361
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageCreatePhotoAlbumUnitComponentComponentPartDefinition;->d:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 600362
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumUnitComponentPartDefinition;->b:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 600363
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumsUnitComponentPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 600364
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumsUnitComponentComponentPartDefinition;->d:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 600365
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumUnitComponentComponentPartDefinition;->d:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 600366
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PagePhotosUnitComponentPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 600367
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PagePhotosUnitComponentComponentPartDefinition;->d:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 600368
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PagesInsightsAYMTUnitComponentPartDefinition;->d:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 600369
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageInsightsMetricWithChartUnitComponentPartDefinition;->d:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 600370
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PagesInsightsOverviewCardHeaderUnitComponentPartDefinition;->d:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 600371
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageInsightsOverviewCardMetricUnitComponentPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 600372
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageAppointmentStatusUnitComponentPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 600373
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageInlineComposerUnitComponentPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 600374
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageInlineComposerUnitComponentComponentPartDefinition;->d:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 600375
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageInsightsOverviewCardMetricUnitComponentPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 600376
    sget-object v0, Lcom/facebook/pages/common/reaction/components/ReactionFullWidthActionButtonComponentPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 600377
    sget-object v0, Lcom/facebook/pages/common/reaction/components/LargeProfileImageBlockComponentPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 600378
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PromotionBlockComponentPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 600379
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageAdminFeedStoryComponentPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 600380
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PagesFeaturedServiceItemsUnitComponentPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 600381
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PagesServiceItemUnitComponentPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 600382
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageMapWithDistanceUnitComponentPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 600383
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageAdminTipUnitComponentPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 600384
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageInfoRowWithButtonUnitComponentPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 600385
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageCategoryBasedRecommendationsComponentPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 600386
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageInlineUpsellComponentPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 600387
    return-void
.end method
