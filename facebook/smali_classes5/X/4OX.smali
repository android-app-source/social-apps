.class public LX/4OX;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 697028
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 697029
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 697030
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 697031
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 697032
    invoke-static {p0, p1}, LX/4OX;->b(LX/15w;LX/186;)I

    move-result v1

    .line 697033
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 697034
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 697035
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 697036
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 697037
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2, p3}, LX/4OX;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 697038
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 697039
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 697040
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 14

    .prologue
    .line 697041
    const/4 v10, 0x0

    .line 697042
    const/4 v9, 0x0

    .line 697043
    const/4 v8, 0x0

    .line 697044
    const/4 v7, 0x0

    .line 697045
    const/4 v6, 0x0

    .line 697046
    const/4 v5, 0x0

    .line 697047
    const/4 v4, 0x0

    .line 697048
    const/4 v3, 0x0

    .line 697049
    const/4 v2, 0x0

    .line 697050
    const/4 v1, 0x0

    .line 697051
    const/4 v0, 0x0

    .line 697052
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->START_OBJECT:LX/15z;

    if-eq v11, v12, :cond_1

    .line 697053
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 697054
    const/4 v0, 0x0

    .line 697055
    :goto_0
    return v0

    .line 697056
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 697057
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_a

    .line 697058
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 697059
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 697060
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_1

    if-eqz v11, :cond_1

    .line 697061
    const-string v12, "default_group_name"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 697062
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto :goto_1

    .line 697063
    :cond_2
    const-string v12, "default_members"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 697064
    invoke-static {p0, p1}, LX/4OC;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 697065
    :cond_3
    const-string v12, "default_visibility"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 697066
    const/4 v1, 0x1

    .line 697067
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v8

    goto :goto_1

    .line 697068
    :cond_4
    const-string v12, "suggested_members"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 697069
    invoke-static {p0, p1}, LX/2bO;->b(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 697070
    :cond_5
    const-string v12, "suggestion_cover_image"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 697071
    invoke-static {p0, p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 697072
    :cond_6
    const-string v12, "suggestion_identifier"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 697073
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 697074
    :cond_7
    const-string v12, "suggestion_type"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_8

    .line 697075
    const/4 v0, 0x1

    .line 697076
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    move-result-object v4

    goto/16 :goto_1

    .line 697077
    :cond_8
    const-string v12, "tracking"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_9

    .line 697078
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto/16 :goto_1

    .line 697079
    :cond_9
    const-string v12, "extra_setting"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 697080
    invoke-static {p0, p1}, LX/4OE;->a(LX/15w;LX/186;)I

    move-result v2

    goto/16 :goto_1

    .line 697081
    :cond_a
    const/16 v11, 0x9

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 697082
    const/4 v11, 0x0

    invoke-virtual {p1, v11, v10}, LX/186;->b(II)V

    .line 697083
    const/4 v10, 0x1

    invoke-virtual {p1, v10, v9}, LX/186;->b(II)V

    .line 697084
    if-eqz v1, :cond_b

    .line 697085
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v8}, LX/186;->a(ILjava/lang/Enum;)V

    .line 697086
    :cond_b
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 697087
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 697088
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 697089
    if-eqz v0, :cond_c

    .line 697090
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v4}, LX/186;->a(ILjava/lang/Enum;)V

    .line 697091
    :cond_c
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 697092
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 697093
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    const/4 v4, 0x6

    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 697094
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 697095
    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 697096
    if-eqz v0, :cond_0

    .line 697097
    const-string v1, "default_group_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 697098
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 697099
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 697100
    if-eqz v0, :cond_1

    .line 697101
    const-string v1, "default_members"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 697102
    invoke-static {p0, v0, p2, p3}, LX/4OC;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 697103
    :cond_1
    invoke-virtual {p0, p1, v3, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 697104
    if-eqz v0, :cond_2

    .line 697105
    const-string v0, "default_visibility"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 697106
    const-class v0, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-virtual {p0, p1, v3, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 697107
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 697108
    if-eqz v0, :cond_3

    .line 697109
    const-string v1, "suggested_members"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 697110
    invoke-static {p0, v0, p2, p3}, LX/2bO;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 697111
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 697112
    if-eqz v0, :cond_4

    .line 697113
    const-string v1, "suggestion_cover_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 697114
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 697115
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 697116
    if-eqz v0, :cond_5

    .line 697117
    const-string v1, "suggestion_identifier"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 697118
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 697119
    :cond_5
    invoke-virtual {p0, p1, v4, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 697120
    if-eqz v0, :cond_6

    .line 697121
    const-string v0, "suggestion_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 697122
    const-class v0, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    invoke-virtual {p0, p1, v4, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 697123
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 697124
    if-eqz v0, :cond_7

    .line 697125
    const-string v1, "tracking"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 697126
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 697127
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 697128
    if-eqz v0, :cond_8

    .line 697129
    const-string v1, "extra_setting"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 697130
    invoke-static {p0, v0, p2, p3}, LX/4OE;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 697131
    :cond_8
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 697132
    return-void
.end method
