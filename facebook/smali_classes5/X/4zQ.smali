.class public final LX/4zQ;
.super LX/2GF;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2GF",
        "<",
        "Ljava/util/Map$Entry",
        "<TK;TV;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0cn;


# direct methods
.method public constructor <init>(LX/0cn;)V
    .locals 1

    .prologue
    .line 822771
    iput-object p1, p0, LX/4zQ;->a:LX/0cn;

    invoke-direct {p0}, LX/2GF;-><init>()V

    return-void
.end method


# virtual methods
.method public final clear()V
    .locals 1

    .prologue
    .line 822769
    iget-object v0, p0, LX/4zQ;->a:LX/0cn;

    invoke-virtual {v0}, LX/0cn;->clear()V

    .line 822770
    return-void
.end method

.method public final contains(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 822762
    instance-of v1, p1, Ljava/util/Map$Entry;

    if-nez v1, :cond_1

    .line 822763
    :cond_0
    :goto_0
    return v0

    .line 822764
    :cond_1
    check-cast p1, Ljava/util/Map$Entry;

    .line 822765
    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    .line 822766
    if-eqz v1, :cond_0

    .line 822767
    iget-object v2, p0, LX/4zQ;->a:LX/0cn;

    invoke-virtual {v2, v1}, LX/0cn;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 822768
    if-eqz v1, :cond_0

    iget-object v2, p0, LX/4zQ;->a:LX/0cn;

    iget-object v2, v2, LX/0cn;->valueEquivalence:LX/0Qj;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, LX/0Qj;->equivalent(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final isEmpty()Z
    .locals 1

    .prologue
    .line 822772
    iget-object v0, p0, LX/4zQ;->a:LX/0cn;

    invoke-virtual {v0}, LX/0cn;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 822761
    new-instance v0, LX/4zP;

    iget-object v1, p0, LX/4zQ;->a:LX/0cn;

    invoke-direct {v0, v1}, LX/4zP;-><init>(LX/0cn;)V

    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 822756
    instance-of v1, p1, Ljava/util/Map$Entry;

    if-nez v1, :cond_1

    .line 822757
    :cond_0
    :goto_0
    return v0

    .line 822758
    :cond_1
    check-cast p1, Ljava/util/Map$Entry;

    .line 822759
    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    .line 822760
    if-eqz v1, :cond_0

    iget-object v2, p0, LX/4zQ;->a:LX/0cn;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, LX/0cn;->remove(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 822755
    iget-object v0, p0, LX/4zQ;->a:LX/0cn;

    invoke-virtual {v0}, LX/0cn;->size()I

    move-result v0

    return v0
.end method
