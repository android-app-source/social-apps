.class public final LX/50P;
.super LX/0Rh;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Rh",
        "<TV;TK;>;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:LX/0Rg;


# direct methods
.method public constructor <init>(LX/0Rg;)V
    .locals 0

    .prologue
    .line 823691
    iput-object p1, p0, LX/50P;->this$0:LX/0Rg;

    invoke-direct {p0}, LX/0Rh;-><init>()V

    .line 823692
    return-void
.end method


# virtual methods
.method public final synthetic a_()LX/0Ri;
    .locals 1

    .prologue
    .line 823698
    invoke-virtual {p0}, LX/50P;->e()LX/0Rh;

    move-result-object v0

    return-object v0
.end method

.method public final createEntrySet()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/util/Map$Entry",
            "<TV;TK;>;>;"
        }
    .end annotation

    .prologue
    .line 823697
    new-instance v0, LX/50O;

    invoke-direct {v0, p0}, LX/50O;-><init>(LX/50P;)V

    return-object v0
.end method

.method public final e()LX/0Rh;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rh",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 823696
    iget-object v0, p0, LX/50P;->this$0:LX/0Rg;

    return-object v0
.end method

.method public final get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TK;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 823699
    if-eqz p1, :cond_0

    iget-object v1, p0, LX/50P;->this$0:LX/0Rg;

    iget-object v1, v1, LX/0Rg;->c:[LX/0P3;

    if-nez v1, :cond_1

    .line 823700
    :cond_0
    :goto_0
    return-object v0

    .line 823701
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-static {v1}, LX/0PC;->a(I)I

    move-result v1

    iget-object v2, p0, LX/50P;->this$0:LX/0Rg;

    iget v2, v2, LX/0Rg;->e:I

    and-int/2addr v1, v2

    .line 823702
    iget-object v2, p0, LX/50P;->this$0:LX/0Rg;

    iget-object v2, v2, LX/0Rg;->c:[LX/0P3;

    aget-object v1, v2, v1

    .line 823703
    :goto_1
    if-eqz v1, :cond_0

    .line 823704
    invoke-virtual {v1}, LX/0P4;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 823705
    invoke-virtual {v1}, LX/0P4;->getKey()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 823706
    :cond_2
    invoke-virtual {v1}, LX/0P3;->b()LX/0P3;

    move-result-object v1

    goto :goto_1
.end method

.method public final isPartialView()Z
    .locals 1

    .prologue
    .line 823695
    const/4 v0, 0x0

    return v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 823694
    invoke-virtual {p0}, LX/50P;->e()LX/0Rh;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rh;->size()I

    move-result v0

    return v0
.end method

.method public final writeReplace()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 823693
    new-instance v0, LX/50Q;

    iget-object v1, p0, LX/50P;->this$0:LX/0Rg;

    invoke-direct {v0, v1}, LX/50Q;-><init>(LX/0Rh;)V

    return-object v0
.end method
