.class public final LX/4o1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field public final synthetic a:Lcom/facebook/widget/HorizontalImageGallery;

.field private b:Z

.field private c:F

.field private d:I

.field private e:Z


# direct methods
.method public constructor <init>(Lcom/facebook/widget/HorizontalImageGallery;)V
    .locals 1

    .prologue
    .line 808351
    iput-object p1, p0, LX/4o1;->a:Lcom/facebook/widget/HorizontalImageGallery;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 808352
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4o1;->e:Z

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/widget/HorizontalImageGallery;B)V
    .locals 0

    .prologue
    .line 808316
    invoke-direct {p0, p1}, LX/4o1;-><init>(Lcom/facebook/widget/HorizontalImageGallery;)V

    return-void
.end method

.method private a(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 808345
    iget-object v0, p0, LX/4o1;->a:Lcom/facebook/widget/HorizontalImageGallery;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    .line 808346
    iput v1, v0, Lcom/facebook/widget/HorizontalImageGallery;->h:F

    .line 808347
    iget-object v0, p0, LX/4o1;->a:Lcom/facebook/widget/HorizontalImageGallery;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    .line 808348
    iput v1, v0, Lcom/facebook/widget/HorizontalImageGallery;->i:F

    .line 808349
    iput-boolean v2, p0, LX/4o1;->e:Z

    .line 808350
    return v2
.end method

.method private b(Landroid/view/MotionEvent;)Z
    .locals 15

    .prologue
    .line 808353
    iget-object v0, p0, LX/4o1;->a:Lcom/facebook/widget/HorizontalImageGallery;

    iget v0, v0, Lcom/facebook/widget/HorizontalImageGallery;->h:F

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    int-to-float v1, v1

    sub-float v1, v0, v1

    .line 808354
    const/4 v0, 0x0

    cmpg-float v0, v1, v0

    if-gez v0, :cond_1

    .line 808355
    iget v0, p0, LX/4o1;->c:F

    const/high16 v2, 0x40800000    # 4.0f

    add-float/2addr v0, v2

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    .line 808356
    const/4 v0, 0x1

    .line 808357
    :goto_0
    iget v2, p0, LX/4o1;->d:I

    if-eq v0, v2, :cond_3

    iget-boolean v2, p0, LX/4o1;->e:Z

    if-nez v2, :cond_3

    .line 808358
    iget-object v1, p0, LX/4o1;->a:Lcom/facebook/widget/HorizontalImageGallery;

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-static {v1, v2}, Lcom/facebook/widget/HorizontalImageGallery;->a(Lcom/facebook/widget/HorizontalImageGallery;F)F

    .line 808359
    iget-object v1, p0, LX/4o1;->a:Lcom/facebook/widget/HorizontalImageGallery;

    iget v1, v1, Lcom/facebook/widget/HorizontalImageGallery;->h:F

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    sub-float/2addr v1, v2

    iput v1, p0, LX/4o1;->c:F

    .line 808360
    :goto_1
    iput v0, p0, LX/4o1;->d:I

    .line 808361
    iget-object v0, p0, LX/4o1;->a:Lcom/facebook/widget/HorizontalImageGallery;

    iget-boolean v0, v0, Lcom/facebook/widget/HorizontalImageGallery;->n:Z

    if-eqz v0, :cond_4

    .line 808362
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4o1;->b:Z

    .line 808363
    iget-object v14, p0, LX/4o1;->a:Lcom/facebook/widget/HorizontalImageGallery;

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v0

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    const/4 v4, 0x0

    iget-object v5, p0, LX/4o1;->a:Lcom/facebook/widget/HorizontalImageGallery;

    iget v5, v5, Lcom/facebook/widget/HorizontalImageGallery;->h:F

    iget-object v6, p0, LX/4o1;->a:Lcom/facebook/widget/HorizontalImageGallery;

    iget v6, v6, Lcom/facebook/widget/HorizontalImageGallery;->i:F

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getPressure()F

    move-result v7

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getSize()F

    move-result v8

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getMetaState()I

    move-result v9

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getXPrecision()F

    move-result v10

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getYPrecision()F

    move-result v11

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getDeviceId()I

    move-result v12

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEdgeFlags()I

    move-result v13

    invoke-static/range {v0 .. v13}, Landroid/view/MotionEvent;->obtain(JJIFFFFIFFII)Landroid/view/MotionEvent;

    move-result-object v0

    invoke-virtual {v14, v0}, Lcom/facebook/widget/HorizontalImageGallery;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 808364
    iget-object v0, p0, LX/4o1;->a:Lcom/facebook/widget/HorizontalImageGallery;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/widget/HorizontalImageGallery;->a(Lcom/facebook/widget/HorizontalImageGallery;Z)Z

    .line 808365
    const/4 v0, 0x1

    .line 808366
    :goto_2
    return v0

    .line 808367
    :cond_0
    const/4 v0, 0x2

    goto :goto_0

    .line 808368
    :cond_1
    iget v0, p0, LX/4o1;->c:F

    const/high16 v2, 0x40800000    # 4.0f

    sub-float/2addr v0, v2

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_2

    .line 808369
    const/4 v0, 0x1

    goto :goto_0

    .line 808370
    :cond_2
    const/4 v0, 0x2

    goto :goto_0

    .line 808371
    :cond_3
    iput v1, p0, LX/4o1;->c:F

    goto :goto_1

    .line 808372
    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method

.method private c(Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/high16 v5, 0x3f800000    # 1.0f

    .line 808317
    iget-object v0, p0, LX/4o1;->a:Lcom/facebook/widget/HorizontalImageGallery;

    iget v0, v0, Lcom/facebook/widget/HorizontalImageGallery;->f:I

    if-gtz v0, :cond_0

    .line 808318
    :goto_0
    return v6

    .line 808319
    :cond_0
    iget-object v0, p0, LX/4o1;->a:Lcom/facebook/widget/HorizontalImageGallery;

    invoke-virtual {v0}, Lcom/facebook/widget/HorizontalImageGallery;->getScrollX()I

    move-result v0

    int-to-float v1, v0

    .line 808320
    iget-object v0, p0, LX/4o1;->a:Lcom/facebook/widget/HorizontalImageGallery;

    iget-object v0, v0, Lcom/facebook/widget/HorizontalImageGallery;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v0

    iget-object v2, p0, LX/4o1;->a:Lcom/facebook/widget/HorizontalImageGallery;

    iget v2, v2, Lcom/facebook/widget/HorizontalImageGallery;->f:I

    div-int/2addr v0, v2

    int-to-float v0, v0

    .line 808321
    iget-object v2, p0, LX/4o1;->a:Lcom/facebook/widget/HorizontalImageGallery;

    iget v2, v2, Lcom/facebook/widget/HorizontalImageGallery;->f:I

    int-to-float v2, v2

    div-float v2, v1, v2

    .line 808322
    iget v3, p0, LX/4o1;->d:I

    if-ne v3, v6, :cond_5

    .line 808323
    iget v3, p0, LX/4o1;->c:F

    iget-object v4, p0, LX/4o1;->a:Lcom/facebook/widget/HorizontalImageGallery;

    iget v4, v4, Lcom/facebook/widget/HorizontalImageGallery;->c:I

    int-to-float v4, v4

    cmpl-float v3, v3, v4

    if-lez v3, :cond_3

    .line 808324
    iget-object v3, p0, LX/4o1;->a:Lcom/facebook/widget/HorizontalImageGallery;

    iget v3, v3, Lcom/facebook/widget/HorizontalImageGallery;->g:I

    int-to-float v3, v3

    sub-float/2addr v0, v5

    cmpg-float v0, v3, v0

    if-gez v0, :cond_2

    .line 808325
    add-float v0, v2, v5

    iget-object v2, p0, LX/4o1;->a:Lcom/facebook/widget/HorizontalImageGallery;

    iget v2, v2, Lcom/facebook/widget/HorizontalImageGallery;->f:I

    int-to-float v2, v2

    mul-float/2addr v0, v2

    .line 808326
    :goto_1
    float-to-int v0, v0

    iget-object v2, p0, LX/4o1;->a:Lcom/facebook/widget/HorizontalImageGallery;

    iget v2, v2, Lcom/facebook/widget/HorizontalImageGallery;->f:I

    div-int/2addr v0, v2

    .line 808327
    iget-object v2, p0, LX/4o1;->a:Lcom/facebook/widget/HorizontalImageGallery;

    iget v2, v2, Lcom/facebook/widget/HorizontalImageGallery;->g:I

    if-ne v0, v2, :cond_1

    iget-object v2, p0, LX/4o1;->a:Lcom/facebook/widget/HorizontalImageGallery;

    iget-object v2, v2, Lcom/facebook/widget/HorizontalImageGallery;->l:LX/4o2;

    if-eqz v2, :cond_1

    .line 808328
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    add-float/2addr v1, v2

    iget-object v2, p0, LX/4o1;->a:Lcom/facebook/widget/HorizontalImageGallery;

    iget v2, v2, Lcom/facebook/widget/HorizontalImageGallery;->f:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    float-to-int v1, v1

    .line 808329
    iget-object v2, p0, LX/4o1;->a:Lcom/facebook/widget/HorizontalImageGallery;

    iget-object v2, v2, Lcom/facebook/widget/HorizontalImageGallery;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    .line 808330
    :cond_1
    iget-object v1, p0, LX/4o1;->a:Lcom/facebook/widget/HorizontalImageGallery;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/HorizontalImageGallery;->a(I)V

    .line 808331
    iput-boolean v6, p0, LX/4o1;->e:Z

    .line 808332
    const/4 v0, 0x0

    iput v0, p0, LX/4o1;->c:F

    .line 808333
    iget-object v0, p0, LX/4o1;->a:Lcom/facebook/widget/HorizontalImageGallery;

    const/4 v1, 0x0

    .line 808334
    iput v1, v0, Lcom/facebook/widget/HorizontalImageGallery;->m:I

    .line 808335
    goto :goto_0

    .line 808336
    :cond_2
    iget-object v0, p0, LX/4o1;->a:Lcom/facebook/widget/HorizontalImageGallery;

    iget v0, v0, Lcom/facebook/widget/HorizontalImageGallery;->g:I

    iget-object v2, p0, LX/4o1;->a:Lcom/facebook/widget/HorizontalImageGallery;

    iget v2, v2, Lcom/facebook/widget/HorizontalImageGallery;->f:I

    mul-int/2addr v0, v2

    int-to-float v0, v0

    goto :goto_1

    .line 808337
    :cond_3
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v3

    int-to-float v3, v3

    sub-float/2addr v0, v5

    cmpl-float v0, v3, v0

    if-nez v0, :cond_4

    .line 808338
    add-float v0, v2, v5

    float-to-int v0, v0

    iget-object v2, p0, LX/4o1;->a:Lcom/facebook/widget/HorizontalImageGallery;

    iget v2, v2, Lcom/facebook/widget/HorizontalImageGallery;->f:I

    mul-int/2addr v0, v2

    int-to-float v0, v0

    goto :goto_1

    .line 808339
    :cond_4
    iget-object v0, p0, LX/4o1;->a:Lcom/facebook/widget/HorizontalImageGallery;

    iget v0, v0, Lcom/facebook/widget/HorizontalImageGallery;->g:I

    iget-object v2, p0, LX/4o1;->a:Lcom/facebook/widget/HorizontalImageGallery;

    iget v2, v2, Lcom/facebook/widget/HorizontalImageGallery;->f:I

    mul-int/2addr v0, v2

    int-to-float v0, v0

    goto :goto_1

    .line 808340
    :cond_5
    iget v0, p0, LX/4o1;->c:F

    iget-object v3, p0, LX/4o1;->a:Lcom/facebook/widget/HorizontalImageGallery;

    iget v3, v3, Lcom/facebook/widget/HorizontalImageGallery;->c:I

    neg-int v3, v3

    int-to-float v3, v3

    cmpg-float v0, v0, v3

    if-gez v0, :cond_6

    .line 808341
    float-to-int v0, v2

    iget-object v2, p0, LX/4o1;->a:Lcom/facebook/widget/HorizontalImageGallery;

    iget v2, v2, Lcom/facebook/widget/HorizontalImageGallery;->f:I

    mul-int/2addr v0, v2

    int-to-float v0, v0

    goto :goto_1

    .line 808342
    :cond_6
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v0

    if-nez v0, :cond_7

    .line 808343
    float-to-int v0, v2

    iget-object v2, p0, LX/4o1;->a:Lcom/facebook/widget/HorizontalImageGallery;

    iget v2, v2, Lcom/facebook/widget/HorizontalImageGallery;->f:I

    mul-int/2addr v0, v2

    int-to-float v0, v0

    goto :goto_1

    .line 808344
    :cond_7
    iget-object v0, p0, LX/4o1;->a:Lcom/facebook/widget/HorizontalImageGallery;

    iget v0, v0, Lcom/facebook/widget/HorizontalImageGallery;->g:I

    iget-object v2, p0, LX/4o1;->a:Lcom/facebook/widget/HorizontalImageGallery;

    iget v2, v2, Lcom/facebook/widget/HorizontalImageGallery;->f:I

    mul-int/2addr v0, v2

    int-to-float v0, v0

    goto/16 :goto_1
.end method


# virtual methods
.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 808303
    iget-object v2, p0, LX/4o1;->a:Lcom/facebook/widget/HorizontalImageGallery;

    iget-object v2, v2, Lcom/facebook/widget/HorizontalImageGallery;->j:Landroid/view/View$OnTouchListener;

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/4o1;->a:Lcom/facebook/widget/HorizontalImageGallery;

    iget-boolean v2, v2, Lcom/facebook/widget/HorizontalImageGallery;->n:Z

    if-eqz v2, :cond_1

    :cond_0
    iget-object v2, p0, LX/4o1;->a:Lcom/facebook/widget/HorizontalImageGallery;

    iget-object v2, v2, Lcom/facebook/widget/HorizontalImageGallery;->j:Landroid/view/View$OnTouchListener;

    if-eqz v2, :cond_3

    iget-boolean v2, p0, LX/4o1;->b:Z

    if-eqz v2, :cond_3

    .line 808304
    :cond_1
    iget-object v2, p0, LX/4o1;->a:Lcom/facebook/widget/HorizontalImageGallery;

    iget-object v2, v2, Lcom/facebook/widget/HorizontalImageGallery;->j:Landroid/view/View$OnTouchListener;

    invoke-interface {v2, p1, p2}, Landroid/view/View$OnTouchListener;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 808305
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_2

    .line 808306
    invoke-direct {p0, p2}, LX/4o1;->c(Landroid/view/MotionEvent;)Z

    .line 808307
    :cond_2
    :goto_0
    return v0

    .line 808308
    :cond_3
    iget-boolean v0, p0, LX/4o1;->b:Z

    if-eqz v0, :cond_4

    .line 808309
    iput-boolean v1, p0, LX/4o1;->b:Z

    move v0, v1

    .line 808310
    goto :goto_0

    .line 808311
    :cond_4
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    move v0, v1

    .line 808312
    goto :goto_0

    .line 808313
    :pswitch_0
    invoke-direct {p0, p2}, LX/4o1;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0

    .line 808314
    :pswitch_1
    invoke-direct {p0, p2}, LX/4o1;->b(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0

    .line 808315
    :pswitch_2
    invoke-direct {p0, p2}, LX/4o1;->c(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
