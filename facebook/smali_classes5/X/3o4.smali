.class public LX/3o4;
.super Landroid/widget/FrameLayout;
.source ""


# instance fields
.field private a:Z

.field private b:I

.field private c:Landroid/support/v7/widget/Toolbar;

.field private d:Landroid/view/View;

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private final i:Landroid/graphics/Rect;

.field public final j:LX/3o0;

.field private k:Z

.field private l:Z

.field public m:Landroid/graphics/drawable/Drawable;

.field public n:Landroid/graphics/drawable/Drawable;

.field public o:I

.field public p:Z

.field public q:LX/3oj;

.field private r:LX/3nw;

.field public s:I

.field public t:LX/3sc;


# direct methods
.method public static a(LX/3o4;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 639790
    iget-boolean v0, p0, LX/3o4;->a:Z

    if-nez v0, :cond_0

    .line 639791
    :goto_0
    return-void

    .line 639792
    :cond_0
    invoke-virtual {p0}, LX/3o4;->getChildCount()I

    move-result v5

    move v3, v4

    move-object v1, v2

    :goto_1
    if-ge v3, v5, :cond_5

    .line 639793
    invoke-virtual {p0, v3}, LX/3o4;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 639794
    instance-of v6, v0, Landroid/support/v7/widget/Toolbar;

    if-eqz v6, :cond_4

    .line 639795
    iget v6, p0, LX/3o4;->b:I

    const/4 v7, -0x1

    if-eq v6, v7, :cond_2

    .line 639796
    iget v6, p0, LX/3o4;->b:I

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v7

    if-ne v6, v7, :cond_1

    .line 639797
    check-cast v0, Landroid/support/v7/widget/Toolbar;

    .line 639798
    :goto_2
    if-nez v0, :cond_3

    .line 639799
    :goto_3
    iput-object v1, p0, LX/3o4;->c:Landroid/support/v7/widget/Toolbar;

    .line 639800
    invoke-direct {p0}, LX/3o4;->b()V

    .line 639801
    iput-boolean v4, p0, LX/3o4;->a:Z

    goto :goto_0

    .line 639802
    :cond_1
    if-nez v1, :cond_4

    .line 639803
    check-cast v0, Landroid/support/v7/widget/Toolbar;

    .line 639804
    :goto_4
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move-object v1, v0

    goto :goto_1

    .line 639805
    :cond_2
    check-cast v0, Landroid/support/v7/widget/Toolbar;

    goto :goto_2

    :cond_3
    move-object v1, v0

    goto :goto_3

    :cond_4
    move-object v0, v1

    goto :goto_4

    :cond_5
    move-object v0, v2

    goto :goto_2
.end method

.method public static b(Landroid/view/View;)LX/1uh;
    .locals 2

    .prologue
    .line 639806
    const v0, 0x7f0d000f

    invoke-virtual {p0, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1uh;

    .line 639807
    if-nez v0, :cond_0

    .line 639808
    new-instance v0, LX/1uh;

    invoke-direct {v0, p0}, LX/1uh;-><init>(Landroid/view/View;)V

    .line 639809
    const v1, 0x7f0d000f

    invoke-virtual {p0, v1, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 639810
    :cond_0
    return-object v0
.end method

.method private b()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 639811
    iget-boolean v0, p0, LX/3o4;->k:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/3o4;->d:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 639812
    iget-object v0, p0, LX/3o4;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 639813
    instance-of v1, v0, Landroid/view/ViewGroup;

    if-eqz v1, :cond_0

    .line 639814
    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, LX/3o4;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 639815
    :cond_0
    iget-boolean v0, p0, LX/3o4;->k:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/3o4;->c:Landroid/support/v7/widget/Toolbar;

    if-eqz v0, :cond_2

    .line 639816
    iget-object v0, p0, LX/3o4;->d:Landroid/view/View;

    if-nez v0, :cond_1

    .line 639817
    new-instance v0, Landroid/view/View;

    invoke-virtual {p0}, LX/3o4;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/3o4;->d:Landroid/view/View;

    .line 639818
    :cond_1
    iget-object v0, p0, LX/3o4;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_2

    .line 639819
    iget-object v0, p0, LX/3o4;->c:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, LX/3o4;->d:Landroid/view/View;

    invoke-virtual {v0, v1, v2, v2}, Landroid/support/v7/widget/Toolbar;->addView(Landroid/view/View;II)V

    .line 639820
    :cond_2
    return-void
.end method

.method private c()LX/3o2;
    .locals 2

    .prologue
    .line 639821
    new-instance v0, LX/3o2;

    invoke-super {p0}, Landroid/widget/FrameLayout;->generateDefaultLayoutParams()Landroid/widget/FrameLayout$LayoutParams;

    move-result-object v1

    invoke-direct {v0, v1}, LX/3o2;-><init>(Landroid/widget/FrameLayout$LayoutParams;)V

    return-object v0
.end method

.method public static setScrimAlpha(LX/3o4;I)V
    .locals 1

    .prologue
    .line 639822
    iget v0, p0, LX/3o4;->o:I

    if-eq p1, v0, :cond_1

    .line 639823
    iget-object v0, p0, LX/3o4;->m:Landroid/graphics/drawable/Drawable;

    .line 639824
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3o4;->c:Landroid/support/v7/widget/Toolbar;

    if-eqz v0, :cond_0

    .line 639825
    iget-object v0, p0, LX/3o4;->c:Landroid/support/v7/widget/Toolbar;

    invoke-static {v0}, LX/0vv;->d(Landroid/view/View;)V

    .line 639826
    :cond_0
    iput p1, p0, LX/3o4;->o:I

    .line 639827
    invoke-static {p0}, LX/0vv;->d(Landroid/view/View;)V

    .line 639828
    :cond_1
    return-void
.end method


# virtual methods
.method public final checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1

    .prologue
    .line 639829
    instance-of v0, p1, LX/3o2;

    return v0
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 639830
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->draw(Landroid/graphics/Canvas;)V

    .line 639831
    invoke-static {p0}, LX/3o4;->a(LX/3o4;)V

    .line 639832
    iget-object v0, p0, LX/3o4;->c:Landroid/support/v7/widget/Toolbar;

    if-nez v0, :cond_0

    iget-object v0, p0, LX/3o4;->m:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget v0, p0, LX/3o4;->o:I

    if-lez v0, :cond_0

    .line 639833
    iget-object v0, p0, LX/3o4;->m:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget v2, p0, LX/3o4;->o:I

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 639834
    iget-object v0, p0, LX/3o4;->m:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 639835
    :cond_0
    iget-boolean v0, p0, LX/3o4;->k:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, LX/3o4;->l:Z

    if-eqz v0, :cond_1

    .line 639836
    iget-object v0, p0, LX/3o4;->j:LX/3o0;

    invoke-virtual {v0, p1}, LX/3o0;->a(Landroid/graphics/Canvas;)V

    .line 639837
    :cond_1
    iget-object v0, p0, LX/3o4;->n:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_2

    iget v0, p0, LX/3o4;->o:I

    if-lez v0, :cond_2

    .line 639838
    iget-object v0, p0, LX/3o4;->t:LX/3sc;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/3o4;->t:LX/3sc;

    invoke-virtual {v0}, LX/3sc;->b()I

    move-result v0

    .line 639839
    :goto_0
    if-lez v0, :cond_2

    .line 639840
    iget-object v2, p0, LX/3o4;->n:Landroid/graphics/drawable/Drawable;

    iget v3, p0, LX/3o4;->s:I

    neg-int v3, v3

    invoke-virtual {p0}, LX/3o4;->getWidth()I

    move-result v4

    iget v5, p0, LX/3o4;->s:I

    sub-int/2addr v0, v5

    invoke-virtual {v2, v1, v3, v4, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 639841
    iget-object v0, p0, LX/3o4;->n:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget v1, p0, LX/3o4;->o:I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 639842
    iget-object v0, p0, LX/3o4;->n:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 639843
    :cond_2
    return-void

    :cond_3
    move v0, v1

    .line 639844
    goto :goto_0
.end method

.method public final drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
    .locals 3

    .prologue
    .line 639845
    invoke-static {p0}, LX/3o4;->a(LX/3o4;)V

    .line 639846
    iget-object v0, p0, LX/3o4;->c:Landroid/support/v7/widget/Toolbar;

    if-ne p2, v0, :cond_0

    iget-object v0, p0, LX/3o4;->m:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget v0, p0, LX/3o4;->o:I

    if-lez v0, :cond_0

    .line 639847
    iget-object v0, p0, LX/3o4;->m:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget v1, p0, LX/3o4;->o:I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 639848
    iget-object v0, p0, LX/3o4;->m:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 639849
    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v0

    return v0
.end method

.method public final synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 639850
    invoke-direct {p0}, LX/3o4;->c()LX/3o2;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic generateDefaultLayoutParams()Landroid/widget/FrameLayout$LayoutParams;
    .locals 1

    .prologue
    .line 639851
    invoke-direct {p0}, LX/3o4;->c()LX/3o2;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 639852
    invoke-virtual {p0, p1}, LX/3o4;->generateLayoutParams(Landroid/util/AttributeSet;)Landroid/widget/FrameLayout$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method public final generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 639854
    new-instance v0, LX/3o2;

    invoke-direct {v0, p1}, LX/3o2;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method public final generateLayoutParams(Landroid/util/AttributeSet;)Landroid/widget/FrameLayout$LayoutParams;
    .locals 2

    .prologue
    .line 639870
    new-instance v0, LX/3o2;

    invoke-virtual {p0}, LX/3o4;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, LX/3o2;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method public getCollapsedTitleGravity()I
    .locals 1

    .prologue
    .line 639867
    iget-object v0, p0, LX/3o4;->j:LX/3o0;

    .line 639868
    iget p0, v0, LX/3o0;->j:I

    move v0, p0

    .line 639869
    return v0
.end method

.method public getCollapsedTitleTypeface()Landroid/graphics/Typeface;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 639866
    iget-object v0, p0, LX/3o4;->j:LX/3o0;

    invoke-virtual {v0}, LX/3o0;->c()Landroid/graphics/Typeface;

    move-result-object v0

    return-object v0
.end method

.method public getContentScrim()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 639865
    iget-object v0, p0, LX/3o4;->m:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getExpandedTitleGravity()I
    .locals 1

    .prologue
    .line 639862
    iget-object v0, p0, LX/3o4;->j:LX/3o0;

    .line 639863
    iget p0, v0, LX/3o0;->i:I

    move v0, p0

    .line 639864
    return v0
.end method

.method public getExpandedTitleMarginBottom()I
    .locals 1

    .prologue
    .line 639861
    iget v0, p0, LX/3o4;->h:I

    return v0
.end method

.method public getExpandedTitleMarginEnd()I
    .locals 1

    .prologue
    .line 639860
    iget v0, p0, LX/3o4;->g:I

    return v0
.end method

.method public getExpandedTitleMarginStart()I
    .locals 1

    .prologue
    .line 639853
    iget v0, p0, LX/3o4;->e:I

    return v0
.end method

.method public getExpandedTitleMarginTop()I
    .locals 1

    .prologue
    .line 639859
    iget v0, p0, LX/3o4;->f:I

    return v0
.end method

.method public getExpandedTitleTypeface()Landroid/graphics/Typeface;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 639856
    iget-object v0, p0, LX/3o4;->j:LX/3o0;

    .line 639857
    iget-object p0, v0, LX/3o0;->v:Landroid/graphics/Typeface;

    if-eqz p0, :cond_0

    iget-object p0, v0, LX/3o0;->v:Landroid/graphics/Typeface;

    :goto_0
    move-object v0, p0

    .line 639858
    return-object v0

    :cond_0
    sget-object p0, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    goto :goto_0
.end method

.method public getStatusBarScrim()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 639855
    iget-object v0, p0, LX/3o4;->n:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getTitle()Ljava/lang/CharSequence;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 639780
    iget-boolean v0, p0, LX/3o4;->k:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3o4;->j:LX/3o0;

    .line 639781
    iget-object p0, v0, LX/3o0;->x:Ljava/lang/CharSequence;

    move-object v0, p0

    .line 639782
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onAttachedToWindow()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2c

    const v1, 0x65b61272

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 639783
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 639784
    invoke-virtual {p0}, LX/3o4;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 639785
    instance-of v2, v0, Landroid/support/design/widget/AppBarLayout;

    if-eqz v2, :cond_1

    .line 639786
    iget-object v2, p0, LX/3o4;->r:LX/3nw;

    if-nez v2, :cond_0

    .line 639787
    new-instance v2, LX/3o3;

    invoke-direct {v2, p0}, LX/3o3;-><init>(LX/3o4;)V

    iput-object v2, p0, LX/3o4;->r:LX/3nw;

    .line 639788
    :cond_0
    check-cast v0, Landroid/support/design/widget/AppBarLayout;

    iget-object v2, p0, LX/3o4;->r:LX/3nw;

    invoke-virtual {v0, v2}, Landroid/support/design/widget/AppBarLayout;->a(LX/3nw;)V

    .line 639789
    :cond_1
    const/16 v0, 0x2d

    const v2, 0x420f7b8c

    invoke-static {v4, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x46337279

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 639694
    invoke-virtual {p0}, LX/3o4;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 639695
    iget-object v2, p0, LX/3o4;->r:LX/3nw;

    if-eqz v2, :cond_0

    instance-of v2, v0, Landroid/support/design/widget/AppBarLayout;

    if-eqz v2, :cond_0

    .line 639696
    check-cast v0, Landroid/support/design/widget/AppBarLayout;

    iget-object v2, p0, LX/3o4;->r:LX/3nw;

    invoke-virtual {v0, v2}, Landroid/support/design/widget/AppBarLayout;->b(LX/3nw;)V

    .line 639697
    :cond_0
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 639698
    const/16 v0, 0x2d

    const v2, -0xd6c7f87

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onLayout(ZIIII)V
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 639659
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    .line 639660
    iget-boolean v2, p0, LX/3o4;->k:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/3o4;->d:Landroid/view/View;

    if-eqz v2, :cond_0

    .line 639661
    iget-object v2, p0, LX/3o4;->d:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->isShown()Z

    move-result v2

    iput-boolean v2, p0, LX/3o4;->l:Z

    .line 639662
    iget-boolean v2, p0, LX/3o4;->l:Z

    if-eqz v2, :cond_0

    .line 639663
    iget-object v2, p0, LX/3o4;->d:Landroid/view/View;

    iget-object v3, p0, LX/3o4;->i:Landroid/graphics/Rect;

    invoke-static {p0, v2, v3}, LX/1ui;->a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/graphics/Rect;)V

    .line 639664
    iget-object v2, p0, LX/3o4;->j:LX/3o0;

    iget-object v3, p0, LX/3o4;->i:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget-object v4, p0, LX/3o4;->i:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    sub-int v4, p5, v4

    iget-object v5, p0, LX/3o4;->i:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    invoke-virtual {v2, v3, v4, v5, p5}, LX/3o0;->b(IIII)V

    .line 639665
    invoke-static {p0}, LX/0vv;->h(Landroid/view/View;)I

    move-result v2

    if-ne v2, v0, :cond_2

    .line 639666
    :goto_0
    iget-object v3, p0, LX/3o4;->j:LX/3o0;

    if-eqz v0, :cond_3

    iget v2, p0, LX/3o4;->g:I

    :goto_1
    iget-object v4, p0, LX/3o4;->i:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    iget v5, p0, LX/3o4;->f:I

    add-int/2addr v4, v5

    sub-int v5, p4, p2

    if-eqz v0, :cond_4

    iget v0, p0, LX/3o4;->e:I

    :goto_2
    sub-int v0, v5, v0

    sub-int v5, p5, p3

    iget v6, p0, LX/3o4;->h:I

    sub-int/2addr v5, v6

    invoke-virtual {v3, v2, v4, v0, v5}, LX/3o0;->a(IIII)V

    .line 639667
    iget-object v0, p0, LX/3o4;->j:LX/3o0;

    invoke-virtual {v0}, LX/3o0;->g()V

    .line 639668
    :cond_0
    invoke-virtual {p0}, LX/3o4;->getChildCount()I

    move-result v0

    :goto_3
    if-ge v1, v0, :cond_5

    .line 639669
    invoke-virtual {p0, v1}, LX/3o4;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 639670
    iget-object v3, p0, LX/3o4;->t:LX/3sc;

    if-eqz v3, :cond_1

    invoke-static {v2}, LX/0vv;->A(Landroid/view/View;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 639671
    iget-object v3, p0, LX/3o4;->t:LX/3sc;

    invoke-virtual {v3}, LX/3sc;->b()I

    move-result v3

    .line 639672
    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v4

    if-ge v4, v3, :cond_1

    .line 639673
    invoke-virtual {v2, v3}, Landroid/view/View;->offsetTopAndBottom(I)V

    .line 639674
    :cond_1
    invoke-static {v2}, LX/3o4;->b(Landroid/view/View;)LX/1uh;

    move-result-object v2

    invoke-virtual {v2}, LX/1uh;->a()V

    .line 639675
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_2
    move v0, v1

    .line 639676
    goto :goto_0

    .line 639677
    :cond_3
    iget v2, p0, LX/3o4;->e:I

    goto :goto_1

    :cond_4
    iget v0, p0, LX/3o4;->g:I

    goto :goto_2

    .line 639678
    :cond_5
    iget-object v0, p0, LX/3o4;->c:Landroid/support/v7/widget/Toolbar;

    if-eqz v0, :cond_7

    .line 639679
    iget-boolean v0, p0, LX/3o4;->k:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, LX/3o4;->j:LX/3o0;

    .line 639680
    iget-object v1, v0, LX/3o0;->x:Ljava/lang/CharSequence;

    move-object v0, v1

    .line 639681
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 639682
    iget-object v0, p0, LX/3o4;->j:LX/3o0;

    iget-object v1, p0, LX/3o4;->c:Landroid/support/v7/widget/Toolbar;

    .line 639683
    iget-object v2, v1, Landroid/support/v7/widget/Toolbar;->v:Ljava/lang/CharSequence;

    move-object v1, v2

    .line 639684
    invoke-virtual {v0, v1}, LX/3o0;->a(Ljava/lang/CharSequence;)V

    .line 639685
    :cond_6
    iget-object v0, p0, LX/3o4;->c:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->getHeight()I

    move-result v0

    invoke-virtual {p0, v0}, LX/3o4;->setMinimumHeight(I)V

    .line 639686
    :cond_7
    return-void
.end method

.method public final onMeasure(II)V
    .locals 0

    .prologue
    .line 639687
    invoke-static {p0}, LX/3o4;->a(LX/3o4;)V

    .line 639688
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 639689
    return-void
.end method

.method public final onSizeChanged(IIII)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/16 v0, 0x2c

    const v1, 0x2849b766

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 639690
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->onSizeChanged(IIII)V

    .line 639691
    iget-object v1, p0, LX/3o4;->m:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    .line 639692
    iget-object v1, p0, LX/3o4;->m:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v2, v2, p1, p2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 639693
    :cond_0
    const/16 v1, 0x2d

    const v2, -0xd5abcb7

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setCollapsedTitleGravity(I)V
    .locals 1

    .prologue
    .line 639778
    iget-object v0, p0, LX/3o4;->j:LX/3o0;

    invoke-virtual {v0, p1}, LX/3o0;->d(I)V

    .line 639779
    return-void
.end method

.method public setCollapsedTitleTextAppearance(I)V
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/StyleRes;
        .end annotation
    .end param

    .prologue
    .line 639699
    iget-object v0, p0, LX/3o4;->j:LX/3o0;

    invoke-virtual {v0, p1}, LX/3o0;->e(I)V

    .line 639700
    return-void
.end method

.method public setCollapsedTitleTextColor(I)V
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 639701
    iget-object v0, p0, LX/3o4;->j:LX/3o0;

    invoke-virtual {v0, p1}, LX/3o0;->a(I)V

    .line 639702
    return-void
.end method

.method public setCollapsedTitleTypeface(Landroid/graphics/Typeface;)V
    .locals 1
    .param p1    # Landroid/graphics/Typeface;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 639703
    iget-object v0, p0, LX/3o4;->j:LX/3o0;

    .line 639704
    iget-object p0, v0, LX/3o0;->u:Landroid/graphics/Typeface;

    if-eq p0, p1, :cond_0

    .line 639705
    iput-object p1, v0, LX/3o0;->u:Landroid/graphics/Typeface;

    .line 639706
    invoke-virtual {v0}, LX/3o0;->g()V

    .line 639707
    :cond_0
    return-void
.end method

.method public setContentScrim(Landroid/graphics/drawable/Drawable;)V
    .locals 3
    .param p1    # Landroid/graphics/drawable/Drawable;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 639708
    iget-object v0, p0, LX/3o4;->m:Landroid/graphics/drawable/Drawable;

    if-eq v0, p1, :cond_1

    .line 639709
    iget-object v0, p0, LX/3o4;->m:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 639710
    iget-object v0, p0, LX/3o4;->m:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 639711
    :cond_0
    if-eqz p1, :cond_2

    .line 639712
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, LX/3o4;->m:Landroid/graphics/drawable/Drawable;

    .line 639713
    invoke-virtual {p0}, LX/3o4;->getWidth()I

    move-result v0

    invoke-virtual {p0}, LX/3o4;->getHeight()I

    move-result v1

    invoke-virtual {p1, v2, v2, v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 639714
    invoke-virtual {p1, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 639715
    iget v0, p0, LX/3o4;->o:I

    invoke-virtual {p1, v0}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 639716
    :goto_0
    invoke-static {p0}, LX/0vv;->d(Landroid/view/View;)V

    .line 639717
    :cond_1
    return-void

    .line 639718
    :cond_2
    iput-object v1, p0, LX/3o4;->m:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method public setContentScrimColor(I)V
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 639719
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v0, p1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v0}, LX/3o4;->setContentScrim(Landroid/graphics/drawable/Drawable;)V

    .line 639720
    return-void
.end method

.method public setContentScrimResource(I)V
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param

    .prologue
    .line 639721
    invoke-virtual {p0}, LX/3o4;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, LX/1ED;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/3o4;->setContentScrim(Landroid/graphics/drawable/Drawable;)V

    .line 639722
    return-void
.end method

.method public setExpandedTitleColor(I)V
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 639723
    iget-object v0, p0, LX/3o4;->j:LX/3o0;

    invoke-virtual {v0, p1}, LX/3o0;->b(I)V

    .line 639724
    return-void
.end method

.method public setExpandedTitleGravity(I)V
    .locals 1

    .prologue
    .line 639725
    iget-object v0, p0, LX/3o4;->j:LX/3o0;

    invoke-virtual {v0, p1}, LX/3o0;->c(I)V

    .line 639726
    return-void
.end method

.method public setExpandedTitleMarginBottom(I)V
    .locals 0

    .prologue
    .line 639727
    iput p1, p0, LX/3o4;->h:I

    .line 639728
    invoke-virtual {p0}, LX/3o4;->requestLayout()V

    .line 639729
    return-void
.end method

.method public setExpandedTitleMarginEnd(I)V
    .locals 0

    .prologue
    .line 639730
    iput p1, p0, LX/3o4;->g:I

    .line 639731
    invoke-virtual {p0}, LX/3o4;->requestLayout()V

    .line 639732
    return-void
.end method

.method public setExpandedTitleMarginStart(I)V
    .locals 0

    .prologue
    .line 639733
    iput p1, p0, LX/3o4;->e:I

    .line 639734
    invoke-virtual {p0}, LX/3o4;->requestLayout()V

    .line 639735
    return-void
.end method

.method public setExpandedTitleMarginTop(I)V
    .locals 0

    .prologue
    .line 639736
    iput p1, p0, LX/3o4;->f:I

    .line 639737
    invoke-virtual {p0}, LX/3o4;->requestLayout()V

    .line 639738
    return-void
.end method

.method public setExpandedTitleTextAppearance(I)V
    .locals 4
    .param p1    # I
        .annotation build Landroid/support/annotation/StyleRes;
        .end annotation
    .end param

    .prologue
    .line 639739
    iget-object v0, p0, LX/3o4;->j:LX/3o0;

    const/4 p0, 0x0

    .line 639740
    iget-object v1, v0, LX/3o0;->c:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, LX/03r;->TextAppearance:[I

    invoke-virtual {v1, p1, v2}, Landroid/content/Context;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 639741
    const/16 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 639742
    const/16 v2, 0x3

    iget v3, v0, LX/3o0;->m:I

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    iput v2, v0, LX/3o0;->m:I

    .line 639743
    :cond_0
    const/16 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 639744
    const/16 v2, 0x0

    iget v3, v0, LX/3o0;->k:F

    float-to-int v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    int-to-float v2, v2

    iput v2, v0, LX/3o0;->k:F

    .line 639745
    :cond_1
    const/16 v2, 0x4

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    iput v2, v0, LX/3o0;->S:I

    .line 639746
    const/16 v2, 0x5

    invoke-virtual {v1, v2, p0}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v2

    iput v2, v0, LX/3o0;->Q:F

    .line 639747
    const/16 v2, 0x6

    invoke-virtual {v1, v2, p0}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v2

    iput v2, v0, LX/3o0;->R:F

    .line 639748
    const/16 v2, 0x7

    invoke-virtual {v1, v2, p0}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v2

    iput v2, v0, LX/3o0;->P:F

    .line 639749
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 639750
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-lt v1, v2, :cond_2

    .line 639751
    invoke-static {v0, p1}, LX/3o0;->g(LX/3o0;I)Landroid/graphics/Typeface;

    move-result-object v1

    iput-object v1, v0, LX/3o0;->v:Landroid/graphics/Typeface;

    .line 639752
    :cond_2
    invoke-virtual {v0}, LX/3o0;->g()V

    .line 639753
    return-void
.end method

.method public setExpandedTitleTypeface(Landroid/graphics/Typeface;)V
    .locals 1
    .param p1    # Landroid/graphics/Typeface;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 639754
    iget-object v0, p0, LX/3o4;->j:LX/3o0;

    .line 639755
    iget-object p0, v0, LX/3o0;->v:Landroid/graphics/Typeface;

    if-eq p0, p1, :cond_0

    .line 639756
    iput-object p1, v0, LX/3o0;->v:Landroid/graphics/Typeface;

    .line 639757
    invoke-virtual {v0}, LX/3o0;->g()V

    .line 639758
    :cond_0
    return-void
.end method

.method public setStatusBarScrim(Landroid/graphics/drawable/Drawable;)V
    .locals 2
    .param p1    # Landroid/graphics/drawable/Drawable;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 639759
    iget-object v0, p0, LX/3o4;->n:Landroid/graphics/drawable/Drawable;

    if-eq v0, p1, :cond_1

    .line 639760
    iget-object v0, p0, LX/3o4;->n:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 639761
    iget-object v0, p0, LX/3o4;->n:Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 639762
    :cond_0
    iput-object p1, p0, LX/3o4;->n:Landroid/graphics/drawable/Drawable;

    .line 639763
    invoke-virtual {p1, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 639764
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget v1, p0, LX/3o4;->o:I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 639765
    invoke-static {p0}, LX/0vv;->d(Landroid/view/View;)V

    .line 639766
    :cond_1
    return-void
.end method

.method public setStatusBarScrimColor(I)V
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 639767
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v0, p1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v0}, LX/3o4;->setStatusBarScrim(Landroid/graphics/drawable/Drawable;)V

    .line 639768
    return-void
.end method

.method public setStatusBarScrimResource(I)V
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param

    .prologue
    .line 639769
    invoke-virtual {p0}, LX/3o4;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, LX/1ED;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/3o4;->setStatusBarScrim(Landroid/graphics/drawable/Drawable;)V

    .line 639770
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 639771
    iget-object v0, p0, LX/3o4;->j:LX/3o0;

    invoke-virtual {v0, p1}, LX/3o0;->a(Ljava/lang/CharSequence;)V

    .line 639772
    return-void
.end method

.method public setTitleEnabled(Z)V
    .locals 1

    .prologue
    .line 639773
    iget-boolean v0, p0, LX/3o4;->k:Z

    if-eq p1, v0, :cond_0

    .line 639774
    iput-boolean p1, p0, LX/3o4;->k:Z

    .line 639775
    invoke-direct {p0}, LX/3o4;->b()V

    .line 639776
    invoke-virtual {p0}, LX/3o4;->requestLayout()V

    .line 639777
    :cond_0
    return-void
.end method
