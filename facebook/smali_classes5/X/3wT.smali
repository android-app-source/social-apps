.class public LX/3wT;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "LX/3wS;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 655203
    new-instance v0, LX/3wM;

    invoke-direct {v0}, LX/3wM;-><init>()V

    sput-object v0, LX/3wT;->a:Ljava/util/Comparator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 655204
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 655205
    return-void
.end method

.method public static a(LX/3wN;)LX/3wP;
    .locals 13

    .prologue
    .line 655206
    const/4 v0, 0x1

    const/4 v4, 0x0

    .line 655207
    invoke-virtual {p0}, LX/3wN;->a()I

    move-result v1

    .line 655208
    invoke-virtual {p0}, LX/3wN;->b()I

    move-result v2

    .line 655209
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 655210
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 655211
    new-instance v3, LX/3wR;

    invoke-direct {v3, v4, v1, v4, v2}, LX/3wR;-><init>(IIII)V

    invoke-interface {v11, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 655212
    add-int v3, v1, v2

    sub-int/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    add-int v8, v3, v1

    .line 655213
    mul-int/lit8 v1, v8, 0x2

    new-array v6, v1, [I

    .line 655214
    mul-int/lit8 v1, v8, 0x2

    new-array v7, v1, [I

    .line 655215
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 655216
    :goto_0
    invoke-interface {v11}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_7

    .line 655217
    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v11, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, LX/3wR;

    .line 655218
    iget v2, v9, LX/3wR;->a:I

    iget v3, v9, LX/3wR;->b:I

    iget v4, v9, LX/3wR;->c:I

    iget v5, v9, LX/3wR;->d:I

    move-object v1, p0

    invoke-static/range {v1 .. v8}, LX/3wT;->a(LX/3wN;IIII[I[II)LX/3wS;

    move-result-object v2

    .line 655219
    if-eqz v2, :cond_6

    .line 655220
    iget v1, v2, LX/3wS;->c:I

    if-lez v1, :cond_0

    .line 655221
    invoke-interface {v10, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 655222
    :cond_0
    iget v1, v2, LX/3wS;->a:I

    iget v3, v9, LX/3wR;->a:I

    add-int/2addr v1, v3

    iput v1, v2, LX/3wS;->a:I

    .line 655223
    iget v1, v2, LX/3wS;->b:I

    iget v3, v9, LX/3wR;->c:I

    add-int/2addr v1, v3

    iput v1, v2, LX/3wS;->b:I

    .line 655224
    invoke-interface {v12}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, LX/3wR;

    invoke-direct {v1}, LX/3wR;-><init>()V

    .line 655225
    :goto_1
    iget v3, v9, LX/3wR;->a:I

    iput v3, v1, LX/3wR;->a:I

    .line 655226
    iget v3, v9, LX/3wR;->c:I

    iput v3, v1, LX/3wR;->c:I

    .line 655227
    iget-boolean v3, v2, LX/3wS;->e:Z

    if-eqz v3, :cond_2

    .line 655228
    iget v3, v2, LX/3wS;->a:I

    iput v3, v1, LX/3wR;->b:I

    .line 655229
    iget v3, v2, LX/3wS;->b:I

    iput v3, v1, LX/3wR;->d:I

    .line 655230
    :goto_2
    invoke-interface {v11, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 655231
    iget-boolean v1, v2, LX/3wS;->e:Z

    if-eqz v1, :cond_5

    .line 655232
    iget-boolean v1, v2, LX/3wS;->d:Z

    if-eqz v1, :cond_4

    .line 655233
    iget v1, v2, LX/3wS;->a:I

    iget v3, v2, LX/3wS;->c:I

    add-int/2addr v1, v3

    add-int/lit8 v1, v1, 0x1

    iput v1, v9, LX/3wR;->a:I

    .line 655234
    iget v1, v2, LX/3wS;->b:I

    iget v2, v2, LX/3wS;->c:I

    add-int/2addr v1, v2

    iput v1, v9, LX/3wR;->c:I

    .line 655235
    :goto_3
    invoke-interface {v11, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 655236
    :cond_1
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v12, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3wR;

    goto :goto_1

    .line 655237
    :cond_2
    iget-boolean v3, v2, LX/3wS;->d:Z

    if-eqz v3, :cond_3

    .line 655238
    iget v3, v2, LX/3wS;->a:I

    add-int/lit8 v3, v3, -0x1

    iput v3, v1, LX/3wR;->b:I

    .line 655239
    iget v3, v2, LX/3wS;->b:I

    iput v3, v1, LX/3wR;->d:I

    goto :goto_2

    .line 655240
    :cond_3
    iget v3, v2, LX/3wS;->a:I

    iput v3, v1, LX/3wR;->b:I

    .line 655241
    iget v3, v2, LX/3wS;->b:I

    add-int/lit8 v3, v3, -0x1

    iput v3, v1, LX/3wR;->d:I

    goto :goto_2

    .line 655242
    :cond_4
    iget v1, v2, LX/3wS;->a:I

    iget v3, v2, LX/3wS;->c:I

    add-int/2addr v1, v3

    iput v1, v9, LX/3wR;->a:I

    .line 655243
    iget v1, v2, LX/3wS;->b:I

    iget v2, v2, LX/3wS;->c:I

    add-int/2addr v1, v2

    add-int/lit8 v1, v1, 0x1

    iput v1, v9, LX/3wR;->c:I

    goto :goto_3

    .line 655244
    :cond_5
    iget v1, v2, LX/3wS;->a:I

    iget v3, v2, LX/3wS;->c:I

    add-int/2addr v1, v3

    iput v1, v9, LX/3wR;->a:I

    .line 655245
    iget v1, v2, LX/3wS;->b:I

    iget v2, v2, LX/3wS;->c:I

    add-int/2addr v1, v2

    iput v1, v9, LX/3wR;->c:I

    goto :goto_3

    .line 655246
    :cond_6
    invoke-interface {v12, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 655247
    :cond_7
    sget-object v1, LX/3wT;->a:Ljava/util/Comparator;

    invoke-static {v10, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 655248
    new-instance v3, LX/3wP;

    move-object v4, p0

    move-object v5, v10

    move v8, v0

    invoke-direct/range {v3 .. v8}, LX/3wP;-><init>(LX/3wN;Ljava/util/List;[I[IZ)V

    move-object v0, v3

    .line 655249
    return-object v0
.end method

.method public static a(LX/3wN;IIII[I[II)LX/3wS;
    .locals 15

    .prologue
    .line 655250
    sub-int v7, p2, p1

    .line 655251
    sub-int v8, p4, p3

    .line 655252
    if-lez v7, :cond_0

    if-gtz v8, :cond_1

    .line 655253
    :cond_0
    const/4 v1, 0x0

    .line 655254
    :goto_0
    return-object v1

    .line 655255
    :cond_1
    sub-int v9, v7, v8

    .line 655256
    add-int v1, v7, v8

    add-int/lit8 v1, v1, 0x1

    div-int/lit8 v10, v1, 0x2

    .line 655257
    sub-int v1, p7, v10

    add-int/lit8 v1, v1, -0x1

    add-int v2, p7, v10

    add-int/lit8 v2, v2, 0x1

    const/4 v3, 0x0

    move-object/from16 v0, p5

    invoke-static {v0, v1, v2, v3}, Ljava/util/Arrays;->fill([IIII)V

    .line 655258
    sub-int v1, p7, v10

    add-int/lit8 v1, v1, -0x1

    add-int/2addr v1, v9

    add-int v2, p7, v10

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v2, v9

    move-object/from16 v0, p6

    invoke-static {v0, v1, v2, v7}, Ljava/util/Arrays;->fill([IIII)V

    .line 655259
    rem-int/lit8 v1, v9, 0x2

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    .line 655260
    :goto_1
    const/4 v2, 0x0

    move v6, v2

    :goto_2
    if-gt v6, v10, :cond_d

    .line 655261
    neg-int v2, v6

    move v5, v2

    :goto_3
    if-gt v5, v6, :cond_7

    .line 655262
    neg-int v2, v6

    if-eq v5, v2, :cond_2

    if-eq v5, v6, :cond_4

    add-int v2, p7, v5

    add-int/lit8 v2, v2, -0x1

    aget v2, p5, v2

    add-int v3, p7, v5

    add-int/lit8 v3, v3, 0x1

    aget v3, p5, v3

    if-ge v2, v3, :cond_4

    .line 655263
    :cond_2
    add-int v2, p7, v5

    add-int/lit8 v2, v2, 0x1

    aget v3, p5, v2

    .line 655264
    const/4 v2, 0x0

    .line 655265
    :goto_4
    sub-int v4, v3, v5

    move v14, v4

    move v4, v3

    move v3, v14

    .line 655266
    :goto_5
    if-ge v4, v7, :cond_5

    if-ge v3, v8, :cond_5

    add-int v11, p1, v4

    add-int v12, p3, v3

    invoke-virtual {p0, v11, v12}, LX/3wN;->a(II)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 655267
    add-int/lit8 v4, v4, 0x1

    .line 655268
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    .line 655269
    :cond_3
    const/4 v1, 0x0

    goto :goto_1

    .line 655270
    :cond_4
    add-int v2, p7, v5

    add-int/lit8 v2, v2, -0x1

    aget v2, p5, v2

    add-int/lit8 v3, v2, 0x1

    .line 655271
    const/4 v2, 0x1

    goto :goto_4

    .line 655272
    :cond_5
    add-int v3, p7, v5

    aput v4, p5, v3

    .line 655273
    if-eqz v1, :cond_6

    sub-int v3, v9, v6

    add-int/lit8 v3, v3, 0x1

    if-lt v5, v3, :cond_6

    add-int v3, v9, v6

    add-int/lit8 v3, v3, -0x1

    if-gt v5, v3, :cond_6

    .line 655274
    add-int v3, p7, v5

    aget v3, p5, v3

    add-int v4, p7, v5

    aget v4, p6, v4

    if-lt v3, v4, :cond_6

    .line 655275
    new-instance v1, LX/3wS;

    invoke-direct {v1}, LX/3wS;-><init>()V

    .line 655276
    add-int v3, p7, v5

    aget v3, p6, v3

    iput v3, v1, LX/3wS;->a:I

    .line 655277
    iget v3, v1, LX/3wS;->a:I

    sub-int/2addr v3, v5

    iput v3, v1, LX/3wS;->b:I

    .line 655278
    add-int v3, p7, v5

    aget v3, p5, v3

    add-int v4, p7, v5

    aget v4, p6, v4

    sub-int/2addr v3, v4

    iput v3, v1, LX/3wS;->c:I

    .line 655279
    iput-boolean v2, v1, LX/3wS;->d:Z

    .line 655280
    const/4 v2, 0x0

    iput-boolean v2, v1, LX/3wS;->e:Z

    goto/16 :goto_0

    .line 655281
    :cond_6
    add-int/lit8 v2, v5, 0x2

    move v5, v2

    goto/16 :goto_3

    .line 655282
    :cond_7
    neg-int v2, v6

    move v5, v2

    :goto_6
    if-gt v5, v6, :cond_c

    .line 655283
    add-int v11, v5, v9

    .line 655284
    add-int v2, v6, v9

    if-eq v11, v2, :cond_8

    neg-int v2, v6

    add-int/2addr v2, v9

    if-eq v11, v2, :cond_9

    add-int v2, p7, v11

    add-int/lit8 v2, v2, -0x1

    aget v2, p6, v2

    add-int v3, p7, v11

    add-int/lit8 v3, v3, 0x1

    aget v3, p6, v3

    if-ge v2, v3, :cond_9

    .line 655285
    :cond_8
    add-int v2, p7, v11

    add-int/lit8 v2, v2, -0x1

    aget v3, p6, v2

    .line 655286
    const/4 v2, 0x0

    .line 655287
    :goto_7
    sub-int v4, v3, v11

    move v14, v4

    move v4, v3

    move v3, v14

    .line 655288
    :goto_8
    if-lez v4, :cond_a

    if-lez v3, :cond_a

    add-int v12, p1, v4

    add-int/lit8 v12, v12, -0x1

    add-int v13, p3, v3

    add-int/lit8 v13, v13, -0x1

    invoke-virtual {p0, v12, v13}, LX/3wN;->a(II)Z

    move-result v12

    if-eqz v12, :cond_a

    .line 655289
    add-int/lit8 v4, v4, -0x1

    .line 655290
    add-int/lit8 v3, v3, -0x1

    goto :goto_8

    .line 655291
    :cond_9
    add-int v2, p7, v11

    add-int/lit8 v2, v2, 0x1

    aget v2, p6, v2

    add-int/lit8 v3, v2, -0x1

    .line 655292
    const/4 v2, 0x1

    goto :goto_7

    .line 655293
    :cond_a
    add-int v3, p7, v11

    aput v4, p6, v3

    .line 655294
    if-nez v1, :cond_b

    add-int v3, v5, v9

    neg-int v4, v6

    if-lt v3, v4, :cond_b

    add-int v3, v5, v9

    if-gt v3, v6, :cond_b

    .line 655295
    add-int v3, p7, v11

    aget v3, p5, v3

    add-int v4, p7, v11

    aget v4, p6, v4

    if-lt v3, v4, :cond_b

    .line 655296
    new-instance v1, LX/3wS;

    invoke-direct {v1}, LX/3wS;-><init>()V

    .line 655297
    add-int v3, p7, v11

    aget v3, p6, v3

    iput v3, v1, LX/3wS;->a:I

    .line 655298
    iget v3, v1, LX/3wS;->a:I

    sub-int/2addr v3, v11

    iput v3, v1, LX/3wS;->b:I

    .line 655299
    add-int v3, p7, v11

    aget v3, p5, v3

    add-int v4, p7, v11

    aget v4, p6, v4

    sub-int/2addr v3, v4

    iput v3, v1, LX/3wS;->c:I

    .line 655300
    iput-boolean v2, v1, LX/3wS;->d:Z

    .line 655301
    const/4 v2, 0x1

    iput-boolean v2, v1, LX/3wS;->e:Z

    goto/16 :goto_0

    .line 655302
    :cond_b
    add-int/lit8 v2, v5, 0x2

    move v5, v2

    goto/16 :goto_6

    .line 655303
    :cond_c
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    goto/16 :goto_2

    .line 655304
    :cond_d
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "DiffUtil hit an unexpected case while trying to calculate the optimal path. Please make sure your data is not changing during the diff calculation."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
