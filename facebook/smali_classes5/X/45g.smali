.class public final LX/45g;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/45h;

.field public b:LX/45c;

.field public c:I


# direct methods
.method public constructor <init>(LX/45h;)V
    .locals 1

    .prologue
    .line 670477
    iput-object p1, p0, LX/45g;->a:LX/45h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 670478
    sget-object v0, LX/45c;->AFTER_CARRIAGE_RETURN:LX/45c;

    iput-object v0, p0, LX/45g;->b:LX/45c;

    .line 670479
    const/4 v0, -0x1

    iput v0, p0, LX/45g;->c:I

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 670480
    iget-object v0, p0, LX/45g;->a:LX/45h;

    iget-object v0, v0, LX/45h;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/45c;)V
    .locals 1

    .prologue
    .line 670481
    iget-object v0, p0, LX/45g;->b:LX/45c;

    if-eq v0, p1, :cond_0

    .line 670482
    iget-object v0, p0, LX/45g;->b:LX/45c;

    invoke-virtual {v0, p0}, LX/45c;->onExitState(LX/45g;)V

    .line 670483
    iput-object p1, p0, LX/45g;->b:LX/45c;

    .line 670484
    invoke-virtual {p1, p0}, LX/45c;->onEnterState(LX/45g;)V

    .line 670485
    :cond_0
    return-void
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 670486
    iget-object v0, p0, LX/45g;->a:LX/45h;

    invoke-static {v0}, LX/45h;->d(LX/45h;)Ljava/io/Writer;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/io/Writer;->write(I)V

    .line 670487
    return-void
.end method
