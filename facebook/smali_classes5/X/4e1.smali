.class public final LX/4e1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1cj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1cj",
        "<",
        "LX/1FJ",
        "<TT;>;>;"
    }
.end annotation


# instance fields
.field public a:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "InternalDataSubscriber.this"
    .end annotation
.end field

.field public final synthetic b:LX/4e2;


# direct methods
.method public constructor <init>(LX/4e2;)V
    .locals 1

    .prologue
    .line 796576
    iput-object p1, p0, LX/4e1;->b:LX/4e2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 796577
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/4e1;->a:Z

    return-void
.end method

.method private declared-synchronized a()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 796571
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, LX/4e1;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    .line 796572
    const/4 v0, 0x0

    .line 796573
    :goto_0
    monitor-exit p0

    return v0

    .line 796574
    :cond_0
    const/4 v1, 0x1

    :try_start_1
    iput-boolean v1, p0, LX/4e1;->a:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 796575
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a(LX/1ca;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<TT;>;>;)V"
        }
    .end annotation

    .prologue
    .line 796566
    invoke-interface {p1}, LX/1ca;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, LX/4e1;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 796567
    iget-object v0, p0, LX/4e1;->b:LX/4e2;

    .line 796568
    invoke-static {v0}, LX/4e2;->j(LX/4e2;)Z

    move-result p0

    if-eqz p0, :cond_0

    .line 796569
    const/4 p0, 0x0

    const/4 p1, 0x1

    invoke-virtual {v0, p0, p1}, LX/1cZ;->a(Ljava/lang/Object;Z)Z

    .line 796570
    :cond_0
    return-void
.end method

.method public final b(LX/1ca;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<TT;>;>;)V"
        }
    .end annotation

    .prologue
    .line 796563
    iget-object v0, p0, LX/4e1;->b:LX/4e2;

    .line 796564
    invoke-interface {p1}, LX/1ca;->e()Ljava/lang/Throwable;

    move-result-object p0

    invoke-virtual {v0, p0}, LX/1cZ;->a(Ljava/lang/Throwable;)Z

    .line 796565
    return-void
.end method

.method public final c(LX/1ca;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<TT;>;>;)V"
        }
    .end annotation

    .prologue
    .line 796553
    iget-object v0, p0, LX/4e1;->b:LX/4e2;

    .line 796554
    new-instance p0, Ljava/util/concurrent/CancellationException;

    invoke-direct {p0}, Ljava/util/concurrent/CancellationException;-><init>()V

    invoke-virtual {v0, p0}, LX/1cZ;->a(Ljava/lang/Throwable;)Z

    .line 796555
    return-void
.end method

.method public final d(LX/1ca;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<TT;>;>;)V"
        }
    .end annotation

    .prologue
    .line 796556
    iget-object v0, p0, LX/4e1;->b:LX/4e2;

    .line 796557
    const/4 v2, 0x0

    .line 796558
    iget-object v3, v0, LX/4e2;->a:[LX/1ca;

    array-length p0, v3

    const/4 v1, 0x0

    :goto_0
    if-ge v1, p0, :cond_0

    aget-object p1, v3, v1

    .line 796559
    invoke-interface {p1}, LX/1ca;->f()F

    move-result p1

    add-float/2addr v2, p1

    .line 796560
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 796561
    :cond_0
    iget-object v1, v0, LX/4e2;->a:[LX/1ca;

    array-length v1, v1

    int-to-float v1, v1

    div-float v1, v2, v1

    invoke-virtual {v0, v1}, LX/1cZ;->a(F)Z

    .line 796562
    return-void
.end method
