.class public LX/45h;
.super Ljava/io/FilterWriter;
.source ""


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "BadMethodUse-java.lang.String.length",
        "BadMethodUse-java.lang.String.charAt"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field private final d:LX/45g;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 670510
    const-string v0, "line.separator"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/45h;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/io/Writer;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 670508
    sget-object v0, LX/45h;->a:Ljava/lang/String;

    invoke-direct {p0, p1, p2, v0}, LX/45h;-><init>(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    .line 670509
    return-void
.end method

.method private constructor <init>(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 670503
    invoke-direct {p0, p1}, Ljava/io/FilterWriter;-><init>(Ljava/io/Writer;)V

    .line 670504
    iput-object p2, p0, LX/45h;->b:Ljava/lang/String;

    .line 670505
    iput-object p3, p0, LX/45h;->c:Ljava/lang/String;

    .line 670506
    new-instance v0, LX/45g;

    invoke-direct {v0, p0}, LX/45g;-><init>(LX/45h;)V

    iput-object v0, p0, LX/45h;->d:LX/45g;

    .line 670507
    return-void
.end method

.method private a(I)V
    .locals 1

    .prologue
    .line 670500
    iget-object v0, p0, LX/45h;->d:LX/45g;

    .line 670501
    iget-object p0, v0, LX/45g;->b:LX/45c;

    invoke-virtual {p0, v0, p1}, LX/45c;->handle(LX/45g;I)V

    .line 670502
    return-void
.end method

.method public static synthetic c(LX/45h;)Ljava/io/Writer;
    .locals 1

    .prologue
    .line 670511
    iget-object v0, p0, Ljava/io/FilterWriter;->out:Ljava/io/Writer;

    return-object v0
.end method

.method public static synthetic d(LX/45h;)Ljava/io/Writer;
    .locals 1

    .prologue
    .line 670499
    iget-object v0, p0, Ljava/io/FilterWriter;->out:Ljava/io/Writer;

    return-object v0
.end method


# virtual methods
.method public final write(I)V
    .locals 2

    .prologue
    .line 670496
    iget-object v1, p0, Ljava/io/Writer;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 670497
    :try_start_0
    invoke-direct {p0, p1}, LX/45h;->a(I)V

    .line 670498
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final write(Ljava/lang/String;II)V
    .locals 4

    .prologue
    .line 670492
    iget-object v2, p0, Ljava/io/Writer;->lock:Ljava/lang/Object;

    monitor-enter v2

    .line 670493
    :goto_0
    add-int/lit8 v0, p3, -0x1

    if-lez p3, :cond_0

    .line 670494
    add-int/lit8 v1, p2, 0x1

    :try_start_0
    invoke-virtual {p1, p2}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-direct {p0, v3}, LX/45h;->a(I)V

    move p3, v0

    move p2, v1

    goto :goto_0

    .line 670495
    :cond_0
    monitor-exit v2

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final write([CII)V
    .locals 4

    .prologue
    .line 670488
    iget-object v2, p0, Ljava/io/Writer;->lock:Ljava/lang/Object;

    monitor-enter v2

    .line 670489
    :goto_0
    add-int/lit8 v0, p3, -0x1

    if-lez p3, :cond_0

    .line 670490
    add-int/lit8 v1, p2, 0x1

    :try_start_0
    aget-char v3, p1, p2

    invoke-direct {p0, v3}, LX/45h;->a(I)V

    move p3, v0

    move p2, v1

    goto :goto_0

    .line 670491
    :cond_0
    monitor-exit v2

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
