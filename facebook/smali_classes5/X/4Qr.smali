.class public LX/4Qr;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 706542
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 18

    .prologue
    .line 706543
    const-wide/16 v12, 0x0

    .line 706544
    const-wide/16 v10, 0x0

    .line 706545
    const-wide/16 v8, 0x0

    .line 706546
    const-wide/16 v6, 0x0

    .line 706547
    const/4 v5, 0x0

    .line 706548
    const/4 v4, 0x0

    .line 706549
    const/4 v3, 0x0

    .line 706550
    const/4 v2, 0x0

    .line 706551
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->START_OBJECT:LX/15z;

    if-eq v14, v15, :cond_a

    .line 706552
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 706553
    const/4 v2, 0x0

    .line 706554
    :goto_0
    return v2

    .line 706555
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_5

    .line 706556
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 706557
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 706558
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_0

    if-eqz v2, :cond_0

    .line 706559
    const-string v6, "w"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 706560
    const/4 v2, 0x1

    .line 706561
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 706562
    :cond_1
    const-string v6, "x"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 706563
    const/4 v2, 0x1

    .line 706564
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v10, v2

    move-wide/from16 v16, v6

    goto :goto_1

    .line 706565
    :cond_2
    const-string v6, "y"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 706566
    const/4 v2, 0x1

    .line 706567
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v9, v2

    move-wide v14, v6

    goto :goto_1

    .line 706568
    :cond_3
    const-string v6, "z"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 706569
    const/4 v2, 0x1

    .line 706570
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v8, v2

    move-wide v12, v6

    goto :goto_1

    .line 706571
    :cond_4
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 706572
    :cond_5
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 706573
    if-eqz v3, :cond_6

    .line 706574
    const/4 v3, 0x0

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 706575
    :cond_6
    if-eqz v10, :cond_7

    .line 706576
    const/4 v3, 0x1

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v16

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 706577
    :cond_7
    if-eqz v9, :cond_8

    .line 706578
    const/4 v3, 0x2

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide v4, v14

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 706579
    :cond_8
    if-eqz v8, :cond_9

    .line 706580
    const/4 v3, 0x3

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide v4, v12

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 706581
    :cond_9
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_a
    move-wide v14, v8

    move-wide/from16 v16, v10

    move v10, v4

    move v8, v2

    move v9, v3

    move v3, v5

    move-wide v4, v12

    move-wide v12, v6

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 706582
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 706583
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 706584
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_0

    .line 706585
    const-string v2, "w"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 706586
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 706587
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 706588
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_1

    .line 706589
    const-string v2, "x"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 706590
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 706591
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 706592
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_2

    .line 706593
    const-string v2, "y"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 706594
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 706595
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 706596
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_3

    .line 706597
    const-string v2, "z"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 706598
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 706599
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 706600
    return-void
.end method
