.class public final LX/3Ty;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/11X;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/executor/GraphQLBatchRunner;


# direct methods
.method public constructor <init>(Lcom/facebook/graphql/executor/GraphQLBatchRunner;)V
    .locals 0

    .prologue
    .line 585265
    iput-object p1, p0, LX/3Ty;->a:Lcom/facebook/graphql/executor/GraphQLBatchRunner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 585266
    iget-object v0, p0, LX/3Ty;->a:Lcom/facebook/graphql/executor/GraphQLBatchRunner;

    iget-object v0, v0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->i:LX/3U2;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/1NB;->a(Lcom/facebook/graphql/executor/GraphQLResult;)Lcom/facebook/graphql/executor/GraphQLResult;

    .line 585267
    iget-object v0, p0, LX/3Ty;->a:Lcom/facebook/graphql/executor/GraphQLBatchRunner;

    iget-object v0, v0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->h:LX/0v6;

    invoke-virtual {v0}, LX/0v6;->g()V

    .line 585268
    iget-object v0, p0, LX/3Ty;->a:Lcom/facebook/graphql/executor/GraphQLBatchRunner;

    iget-object v0, v0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->i:LX/3U2;

    invoke-virtual {v0}, LX/1NB;->b()V

    .line 585269
    return-void
.end method

.method public final a(Lcom/facebook/graphql/executor/GraphQLResult;LX/0zO;)V
    .locals 3

    .prologue
    .line 585270
    iget-object v0, p0, LX/3Ty;->a:Lcom/facebook/graphql/executor/GraphQLBatchRunner;

    .line 585271
    iget v1, v0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->q:I

    add-int/lit8 v2, v1, 0x1

    iput v2, v0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->q:I

    .line 585272
    iget-object v0, p0, LX/3Ty;->a:Lcom/facebook/graphql/executor/GraphQLBatchRunner;

    iget-object v0, v0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->s:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3U1;

    .line 585273
    iget-object v1, v0, LX/3U1;->a:LX/1kt;

    invoke-interface {v1, p1}, LX/1kt;->c(Lcom/facebook/graphql/executor/GraphQLResult;)Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v1

    .line 585274
    iget-object v2, p0, LX/3Ty;->a:Lcom/facebook/graphql/executor/GraphQLBatchRunner;

    iget-object v2, v2, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->i:LX/3U2;

    invoke-virtual {v2, v1}, LX/1NB;->a(Lcom/facebook/graphql/executor/GraphQLResult;)Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v1

    .line 585275
    iget-object v0, v0, LX/3U1;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 585276
    invoke-static {v1}, LX/1lO;->a(Lcom/facebook/graphql/executor/GraphQLResult;)LX/1lO;

    move-result-object v0

    iget-object v1, p0, LX/3Ty;->a:Lcom/facebook/graphql/executor/GraphQLBatchRunner;

    iget-object v1, v1, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->i:LX/3U2;

    .line 585277
    iput-object v1, v0, LX/1lO;->d:LX/1NB;

    .line 585278
    move-object v0, v0

    .line 585279
    invoke-virtual {v0}, LX/1lO;->a()Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v0

    .line 585280
    iget-object v1, p0, LX/3Ty;->a:Lcom/facebook/graphql/executor/GraphQLBatchRunner;

    iget-object v1, v1, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->i:LX/3U2;

    invoke-virtual {v1, v0}, LX/1NB;->c(Lcom/facebook/graphql/executor/GraphQLResult;)Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v0

    .line 585281
    iget-object v1, p0, LX/3Ty;->a:Lcom/facebook/graphql/executor/GraphQLBatchRunner;

    iget-object v1, v1, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->h:LX/0v6;

    invoke-virtual {v1, p2, v0}, LX/0v6;->a(LX/0zO;Lcom/facebook/graphql/executor/GraphQLResult;)V

    .line 585282
    iget-object v0, p0, LX/3Ty;->a:Lcom/facebook/graphql/executor/GraphQLBatchRunner;

    iget-object v0, v0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->i:LX/3U2;

    invoke-virtual {v0}, LX/1NB;->b()V

    .line 585283
    return-void
.end method

.method public final a(Ljava/lang/Exception;LX/0zO;)V
    .locals 4

    .prologue
    .line 585284
    iget-object v0, p0, LX/3Ty;->a:Lcom/facebook/graphql/executor/GraphQLBatchRunner;

    .line 585285
    iget v1, v0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->r:I

    add-int/lit8 v2, v1, 0x1

    iput v2, v0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->r:I

    .line 585286
    iget-object v0, p0, LX/3Ty;->a:Lcom/facebook/graphql/executor/GraphQLBatchRunner;

    iget-object v0, v0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->i:LX/3U2;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/1NB;->a(Lcom/facebook/graphql/executor/GraphQLResult;)Lcom/facebook/graphql/executor/GraphQLResult;

    .line 585287
    iget-object v0, p0, LX/3Ty;->a:Lcom/facebook/graphql/executor/GraphQLBatchRunner;

    iget-object v0, v0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->h:LX/0v6;

    invoke-virtual {v0, p2, p1}, LX/0v6;->a(LX/0zO;Ljava/lang/Exception;)V

    .line 585288
    iget-object v0, p0, LX/3Ty;->a:Lcom/facebook/graphql/executor/GraphQLBatchRunner;

    iget-object v0, v0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->i:LX/3U2;

    invoke-virtual {v0}, LX/1NB;->b()V

    .line 585289
    iget-object v0, p0, LX/3Ty;->a:Lcom/facebook/graphql/executor/GraphQLBatchRunner;

    iget-object v0, v0, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->j:LX/03V;

    const-string v1, "GraphQLBatchResultCallback.onException"

    iget-object v2, p0, LX/3Ty;->a:Lcom/facebook/graphql/executor/GraphQLBatchRunner;

    iget-object v2, v2, Lcom/facebook/graphql/executor/GraphQLBatchRunner;->h:LX/0v6;

    .line 585290
    iget-object v3, v2, LX/0v6;->c:Ljava/lang/String;

    move-object v2, v3

    .line 585291
    const/16 v3, 0x7d0

    invoke-virtual {v0, v1, v2, p1, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;I)V

    .line 585292
    return-void
.end method
