.class public final LX/3o3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3nw;


# instance fields
.field public final synthetic a:LX/3o4;


# direct methods
.method public constructor <init>(LX/3o4;)V
    .locals 0

    .prologue
    .line 639612
    iput-object p1, p0, LX/3o3;->a:LX/3o4;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/support/design/widget/AppBarLayout;I)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 639613
    iget-object v0, p0, LX/3o3;->a:LX/3o4;

    .line 639614
    iput p2, v0, LX/3o4;->s:I

    .line 639615
    iget-object v0, p0, LX/3o3;->a:LX/3o4;

    iget-object v0, v0, LX/3o4;->t:LX/3sc;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/3o3;->a:LX/3o4;

    iget-object v0, v0, LX/3o4;->t:LX/3sc;

    invoke-virtual {v0}, LX/3sc;->b()I

    move-result v0

    move v1, v0

    .line 639616
    :goto_0
    invoke-virtual {p1}, Landroid/support/design/widget/AppBarLayout;->getTotalScrollRange()I

    move-result v4

    .line 639617
    iget-object v0, p0, LX/3o3;->a:LX/3o4;

    invoke-virtual {v0}, LX/3o4;->getChildCount()I

    move-result v5

    move v3, v2

    :goto_1
    if-ge v3, v5, :cond_2

    .line 639618
    iget-object v0, p0, LX/3o3;->a:LX/3o4;

    invoke-virtual {v0, v3}, LX/3o4;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 639619
    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/3o2;

    .line 639620
    invoke-static {v6}, LX/3o4;->b(Landroid/view/View;)LX/1uh;

    move-result-object v7

    .line 639621
    iget v8, v0, LX/3o2;->a:I

    packed-switch v8, :pswitch_data_0

    .line 639622
    :cond_0
    :goto_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_1
    move v1, v2

    .line 639623
    goto :goto_0

    .line 639624
    :pswitch_0
    iget-object v0, p0, LX/3o3;->a:LX/3o4;

    invoke-virtual {v0}, LX/3o4;->getHeight()I

    move-result v0

    sub-int/2addr v0, v1

    add-int/2addr v0, p2

    invoke-virtual {v6}, Landroid/view/View;->getHeight()I

    move-result v6

    if-lt v0, v6, :cond_0

    .line 639625
    neg-int v0, p2

    invoke-virtual {v7, v0}, LX/1uh;->a(I)Z

    goto :goto_2

    .line 639626
    :pswitch_1
    neg-int v6, p2

    int-to-float v6, v6

    iget v0, v0, LX/3o2;->b:F

    mul-float/2addr v0, v6

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    invoke-virtual {v7, v0}, LX/1uh;->a(I)Z

    goto :goto_2

    .line 639627
    :cond_2
    iget-object v0, p0, LX/3o3;->a:LX/3o4;

    iget-object v0, v0, LX/3o4;->m:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_3

    iget-object v0, p0, LX/3o3;->a:LX/3o4;

    iget-object v0, v0, LX/3o4;->n:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_6

    .line 639628
    :cond_3
    iget-object v0, p0, LX/3o3;->a:LX/3o4;

    iget-object v3, p0, LX/3o3;->a:LX/3o4;

    invoke-virtual {v3}, LX/3o4;->getHeight()I

    move-result v3

    add-int/2addr v3, p2

    iget-object v5, p0, LX/3o3;->a:LX/3o4;

    .line 639629
    invoke-static {v5}, LX/0vv;->u(Landroid/view/View;)I

    move-result v6

    mul-int/lit8 v6, v6, 0x2

    move v5, v6

    .line 639630
    add-int/2addr v5, v1

    if-ge v3, v5, :cond_4

    const/4 v2, 0x1

    .line 639631
    :cond_4
    invoke-static {v0}, LX/0vv;->E(Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-virtual {v0}, LX/3o4;->isInEditMode()Z

    move-result v3

    if-nez v3, :cond_9

    const/4 v3, 0x1

    :goto_3
    const/16 v5, 0xff

    const/4 v6, 0x0

    .line 639632
    iget-boolean v7, v0, LX/3o4;->p:Z

    if-eq v7, v2, :cond_6

    .line 639633
    if-eqz v3, :cond_b

    .line 639634
    if-eqz v2, :cond_a

    .line 639635
    :goto_4
    invoke-static {v0}, LX/3o4;->a(LX/3o4;)V

    .line 639636
    iget-object v6, v0, LX/3o4;->q:LX/3oj;

    if-nez v6, :cond_e

    .line 639637
    invoke-static {}, LX/1uL;->a()LX/3oj;

    move-result-object v6

    iput-object v6, v0, LX/3o4;->q:LX/3oj;

    .line 639638
    iget-object v6, v0, LX/3o4;->q:LX/3oj;

    const/16 v7, 0x258

    invoke-virtual {v6, v7}, LX/3oj;->a(I)V

    .line 639639
    iget-object v7, v0, LX/3o4;->q:LX/3oj;

    iget v6, v0, LX/3o4;->o:I

    if-le v5, v6, :cond_d

    sget-object v6, LX/3ns;->c:Landroid/view/animation/Interpolator;

    :goto_5
    invoke-virtual {v7, v6}, LX/3oj;->a(Landroid/view/animation/Interpolator;)V

    .line 639640
    iget-object v6, v0, LX/3o4;->q:LX/3oj;

    new-instance v7, LX/3o1;

    invoke-direct {v7, v0}, LX/3o1;-><init>(LX/3o4;)V

    invoke-virtual {v6, v7}, LX/3oj;->a(LX/3nt;)V

    .line 639641
    :cond_5
    :goto_6
    iget-object v6, v0, LX/3o4;->q:LX/3oj;

    iget v7, v0, LX/3o4;->o:I

    invoke-virtual {v6, v7, v5}, LX/3oj;->a(II)V

    .line 639642
    iget-object v6, v0, LX/3o4;->q:LX/3oj;

    invoke-virtual {v6}, LX/3oj;->a()V

    .line 639643
    :goto_7
    iput-boolean v2, v0, LX/3o4;->p:Z

    .line 639644
    :cond_6
    iget-object v0, p0, LX/3o3;->a:LX/3o4;

    iget-object v0, v0, LX/3o4;->n:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_7

    if-lez v1, :cond_7

    .line 639645
    iget-object v0, p0, LX/3o3;->a:LX/3o4;

    invoke-static {v0}, LX/0vv;->d(Landroid/view/View;)V

    .line 639646
    :cond_7
    iget-object v0, p0, LX/3o3;->a:LX/3o4;

    invoke-virtual {v0}, LX/3o4;->getHeight()I

    move-result v0

    iget-object v2, p0, LX/3o3;->a:LX/3o4;

    invoke-static {v2}, LX/0vv;->u(Landroid/view/View;)I

    move-result v2

    sub-int/2addr v0, v2

    sub-int/2addr v0, v1

    .line 639647
    iget-object v1, p0, LX/3o3;->a:LX/3o4;

    iget-object v1, v1, LX/3o4;->j:LX/3o0;

    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    int-to-float v2, v2

    int-to-float v0, v0

    div-float v0, v2, v0

    invoke-virtual {v1, v0}, LX/3o0;->b(F)V

    .line 639648
    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result v0

    if-ne v0, v4, :cond_8

    .line 639649
    iget v0, p1, Landroid/support/design/widget/AppBarLayout;->e:F

    move v0, v0

    .line 639650
    invoke-static {p1, v0}, LX/0vv;->f(Landroid/view/View;F)V

    .line 639651
    :goto_8
    return-void

    .line 639652
    :cond_8
    const/4 v0, 0x0

    invoke-static {p1, v0}, LX/0vv;->f(Landroid/view/View;F)V

    goto :goto_8

    .line 639653
    :cond_9
    const/4 v3, 0x0

    goto :goto_3

    :cond_a
    move v5, v6

    .line 639654
    goto :goto_4

    .line 639655
    :cond_b
    if-eqz v2, :cond_c

    :goto_9
    invoke-static {v0, v5}, LX/3o4;->setScrimAlpha(LX/3o4;I)V

    goto :goto_7

    :cond_c
    move v5, v6

    goto :goto_9

    .line 639656
    :cond_d
    sget-object v6, LX/3ns;->d:Landroid/view/animation/Interpolator;

    goto :goto_5

    .line 639657
    :cond_e
    iget-object v6, v0, LX/3o4;->q:LX/3oj;

    invoke-virtual {v6}, LX/3oj;->b()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 639658
    iget-object v6, v0, LX/3o4;->q:LX/3oj;

    invoke-virtual {v6}, LX/3oj;->e()V

    goto :goto_6

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
