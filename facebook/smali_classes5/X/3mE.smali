.class public LX/3mE;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static k:LX/0Xm;


# instance fields
.field private final a:LX/3mF;

.field private final b:Ljava/util/concurrent/Executor;

.field public final c:LX/3mG;

.field private final d:LX/17Q;

.field public final e:LX/0Zb;

.field public final f:LX/1nA;

.field public final g:Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

.field public final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation
.end field

.field public final i:Lcom/facebook/content/SecureContextHelper;

.field private final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DOL;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/17Q;LX/0Zb;LX/3mF;Ljava/util/concurrent/Executor;LX/3mG;LX/1nA;Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;LX/0Or;Lcom/facebook/content/SecureContextHelper;LX/0Ot;)V
    .locals 0
    .param p4    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p8    # LX/0Or;
        .annotation runtime Lcom/facebook/base/activity/ReactFragmentActivity;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/17Q;",
            "LX/0Zb;",
            "LX/3mF;",
            "Ljava/util/concurrent/Executor;",
            "LX/3mG;",
            "LX/1nA;",
            "Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;",
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0Ot",
            "<",
            "LX/DOL;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 635626
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 635627
    iput-object p1, p0, LX/3mE;->d:LX/17Q;

    .line 635628
    iput-object p2, p0, LX/3mE;->e:LX/0Zb;

    .line 635629
    iput-object p3, p0, LX/3mE;->a:LX/3mF;

    .line 635630
    iput-object p4, p0, LX/3mE;->b:Ljava/util/concurrent/Executor;

    .line 635631
    iput-object p5, p0, LX/3mE;->c:LX/3mG;

    .line 635632
    iput-object p6, p0, LX/3mE;->f:LX/1nA;

    .line 635633
    iput-object p7, p0, LX/3mE;->g:Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

    .line 635634
    iput-object p8, p0, LX/3mE;->h:LX/0Or;

    .line 635635
    iput-object p9, p0, LX/3mE;->i:Lcom/facebook/content/SecureContextHelper;

    .line 635636
    iput-object p10, p0, LX/3mE;->j:LX/0Ot;

    .line 635637
    return-void
.end method

.method public static a(LX/0QB;)LX/3mE;
    .locals 14

    .prologue
    .line 635615
    const-class v1, LX/3mE;

    monitor-enter v1

    .line 635616
    :try_start_0
    sget-object v0, LX/3mE;->k:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 635617
    sput-object v2, LX/3mE;->k:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 635618
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 635619
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 635620
    new-instance v3, LX/3mE;

    invoke-static {v0}, LX/17Q;->a(LX/0QB;)LX/17Q;

    move-result-object v4

    check-cast v4, LX/17Q;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v5

    check-cast v5, LX/0Zb;

    invoke-static {v0}, LX/3mF;->b(LX/0QB;)LX/3mF;

    move-result-object v6

    check-cast v6, LX/3mF;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v7

    check-cast v7, Ljava/util/concurrent/Executor;

    invoke-static {v0}, LX/3mG;->b(LX/0QB;)LX/3mG;

    move-result-object v8

    check-cast v8, LX/3mG;

    invoke-static {v0}, LX/1nA;->a(LX/0QB;)LX/1nA;

    move-result-object v9

    check-cast v9, LX/1nA;

    invoke-static {v0}, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->a(LX/0QB;)Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

    move-result-object v10

    check-cast v10, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

    const/16 v11, 0xd

    invoke-static {v0, v11}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v12

    check-cast v12, Lcom/facebook/content/SecureContextHelper;

    const/16 v13, 0x240a

    invoke-static {v0, v13}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v13

    invoke-direct/range {v3 .. v13}, LX/3mE;-><init>(LX/17Q;LX/0Zb;LX/3mF;Ljava/util/concurrent/Executor;LX/3mG;LX/1nA;Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;LX/0Or;Lcom/facebook/content/SecureContextHelper;LX/0Ot;)V

    .line 635621
    move-object v0, v3

    .line 635622
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 635623
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/3mE;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 635624
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 635625
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/3mE;Lcom/facebook/graphql/model/GroupsYouShouldJoinFeedUnit;LX/254;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 635584
    new-instance v1, LX/162;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v1, v0}, LX/162;-><init>(LX/0mC;)V

    .line 635585
    invoke-interface {p2}, LX/16h;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 635586
    if-eqz p1, :cond_1

    .line 635587
    const/4 v0, 0x0

    .line 635588
    instance-of v2, p1, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;

    if-eqz v2, :cond_2

    .line 635589
    check-cast p1, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;->c()Ljava/lang/String;

    move-result-object v0

    .line 635590
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    .line 635591
    invoke-virtual {v1, v0}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 635592
    :cond_1
    move-object v0, v1

    .line 635593
    invoke-static {v0}, LX/17Q;->F(LX/0lF;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 635594
    const/4 v1, 0x0

    .line 635595
    :goto_1
    move-object v0, v1

    .line 635596
    iget-object v1, p0, LX/3mE;->e:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 635597
    return-void

    .line 635598
    :cond_2
    instance-of v2, p1, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;

    if-eqz v2, :cond_0

    .line 635599
    check-cast p1, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 635600
    :cond_3
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v1, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v2, "tracking"

    invoke-virtual {v1, v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "native_newsfeed"

    .line 635601
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 635602
    move-object v1, v1

    .line 635603
    goto :goto_1
.end method


# virtual methods
.method public final a(LX/1Po;Lcom/facebook/graphql/model/GroupsYouShouldJoinFeedUnit;LX/254;LX/3n2;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;",
            "Lcom/facebook/graphql/model/GroupsYouShouldJoinFeedUnit;",
            "LX/254;",
            "LX/3n2;",
            ")V"
        }
    .end annotation

    .prologue
    .line 635607
    iget-boolean v0, p4, LX/3n2;->a:Z

    move v1, v0

    .line 635608
    if-eqz v1, :cond_1

    iget-object v0, p0, LX/3mE;->a:LX/3mF;

    invoke-static {p3}, LX/25D;->a(LX/16q;)Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v2

    const-string v3, "gysj"

    const-string v4, "ALLOW_READD"

    invoke-virtual {v0, v2, v3, v4}, LX/3mF;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 635609
    :goto_0
    if-nez v1, :cond_0

    .line 635610
    const-string v2, "gysj_join"

    invoke-static {p0, p2, p3, v2}, LX/3mE;->a(LX/3mE;Lcom/facebook/graphql/model/GroupsYouShouldJoinFeedUnit;LX/254;Ljava/lang/String;)V

    .line 635611
    :cond_0
    new-instance v5, LX/DCr;

    move-object v6, p0

    move-object v7, p4

    move v8, v1

    move-object v9, p2

    move-object v10, p1

    invoke-direct/range {v5 .. v10}, LX/DCr;-><init>(LX/3mE;LX/3n2;ZLcom/facebook/graphql/model/GroupsYouShouldJoinFeedUnit;LX/1Po;)V

    move-object v1, v5

    .line 635612
    iget-object v2, p0, LX/3mE;->b:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 635613
    return-void

    .line 635614
    :cond_1
    iget-object v0, p0, LX/3mE;->a:LX/3mF;

    invoke-static {p3}, LX/25D;->a(LX/16q;)Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v2

    const-string v3, "gysj"

    invoke-virtual {v0, v2, v3}, LX/3mF;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 635604
    iget-object v0, p0, LX/3mE;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DOL;

    invoke-virtual {v0, p2, p3}, LX/DOL;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 635605
    iget-object v1, p0, LX/3mE;->i:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 635606
    return-void
.end method
