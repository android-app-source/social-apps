.class public LX/3g7;
.super LX/2aB;
.source ""


# instance fields
.field private final a:LX/2CB;

.field public final b:LX/29r;

.field public final c:LX/3g8;

.field private final d:LX/2ZE;


# direct methods
.method public constructor <init>(LX/2CB;LX/29r;LX/3g8;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 624060
    invoke-direct {p0}, LX/2aB;-><init>()V

    .line 624061
    new-instance v0, LX/3g9;

    invoke-direct {v0, p0}, LX/3g9;-><init>(LX/3g7;)V

    iput-object v0, p0, LX/3g7;->d:LX/2ZE;

    .line 624062
    iput-object p1, p0, LX/3g7;->a:LX/2CB;

    .line 624063
    iput-object p2, p0, LX/3g7;->b:LX/29r;

    .line 624064
    iput-object p3, p0, LX/3g7;->c:LX/3g8;

    .line 624065
    return-void
.end method


# virtual methods
.method public final c()LX/2ZE;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 624066
    iget-object v1, p0, LX/3g7;->b:LX/29r;

    invoke-virtual {v1}, LX/29r;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 624067
    :cond_0
    :goto_0
    return-object v0

    .line 624068
    :cond_1
    iget-object v1, p0, LX/3g7;->a:LX/2CB;

    .line 624069
    invoke-static {v1}, LX/2CB;->d(LX/2CB;)V

    .line 624070
    iget-object v1, p0, LX/3g7;->b:LX/29r;

    const/4 v2, 0x0

    .line 624071
    iget-boolean v3, v1, LX/29r;->u:Z

    if-nez v3, :cond_2

    iget-object v3, v1, LX/29r;->c:LX/29s;

    invoke-virtual {v3}, LX/29s;->d()Z

    move-result v3

    if-nez v3, :cond_2

    invoke-static {v1}, LX/29r;->k(LX/29r;)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, v1, LX/29r;->c:LX/29s;

    invoke-virtual {v3}, LX/29s;->e()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 624072
    :cond_2
    :goto_1
    move v1, v2

    .line 624073
    if-eqz v1, :cond_0

    .line 624074
    iget-object v0, p0, LX/3g7;->d:LX/2ZE;

    goto :goto_0

    .line 624075
    :cond_3
    invoke-virtual {v1}, LX/29r;->d()Lcom/facebook/appirater/api/FetchISRConfigResult;

    move-result-object v3

    .line 624076
    if-eqz v3, :cond_4

    invoke-virtual {v3}, Lcom/facebook/appirater/api/FetchISRConfigResult;->a()Z

    move-result v4

    if-eqz v4, :cond_4

    const-wide/32 v4, 0x6ddd00

    invoke-virtual {v3, v4, v5}, Lcom/facebook/appirater/api/FetchISRConfigResult;->a(J)Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_4
    const/4 v2, 0x1

    goto :goto_1
.end method

.method public final dr_()J
    .locals 2

    .prologue
    .line 624077
    const-wide/32 v0, 0x6ddd00

    return-wide v0
.end method
