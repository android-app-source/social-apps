.class public LX/3em;
.super LX/0ro;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0ro",
        "<",
        "Lcom/facebook/messaging/media/imageurirequest/FetchImageParams;",
        "Lcom/facebook/messaging/media/imageurirequest/graphql/DownloadImageFragmentInterfaces$DownloadImageFragment;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(LX/0sO;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 620958
    invoke-direct {p0, p1}, LX/0ro;-><init>(LX/0sO;)V

    .line 620959
    return-void
.end method

.method public static a(LX/0QB;)LX/3em;
    .locals 1

    .prologue
    .line 620957
    invoke-static {p0}, LX/3em;->b(LX/0QB;)LX/3em;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/3em;
    .locals 2

    .prologue
    .line 620955
    new-instance v1, LX/3em;

    invoke-static {p0}, LX/0sO;->a(LX/0QB;)LX/0sO;

    move-result-object v0

    check-cast v0, LX/0sO;

    invoke-direct {v1, v0}, LX/3em;-><init>(LX/0sO;)V

    .line 620956
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/1pN;LX/15w;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 620946
    const-class v0, Lcom/facebook/messaging/media/imageurirequest/graphql/DownloadImageFragmentModels$DownloadImageFragmentModel;

    invoke-virtual {p3, v0}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/media/imageurirequest/graphql/DownloadImageFragmentModels$DownloadImageFragmentModel;

    return-object v0
.end method

.method public final b(Ljava/lang/Object;LX/1pN;)I
    .locals 1

    .prologue
    .line 620954
    const/4 v0, 0x1

    return v0
.end method

.method public final f(Ljava/lang/Object;)LX/0gW;
    .locals 3

    .prologue
    .line 620947
    check-cast p1, Lcom/facebook/messaging/media/imageurirequest/FetchImageParams;

    .line 620948
    new-instance v0, LX/8CL;

    invoke-direct {v0}, LX/8CL;-><init>()V

    move-object v0, v0

    .line 620949
    const-string v1, "fbid"

    .line 620950
    iget-object v2, p1, Lcom/facebook/messaging/media/imageurirequest/FetchImageParams;->a:Ljava/lang/String;

    move-object v2, v2

    .line 620951
    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "img_size"

    .line 620952
    iget v2, p1, Lcom/facebook/messaging/media/imageurirequest/FetchImageParams;->b:I

    move v2, v2

    .line 620953
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    return-object v0
.end method
