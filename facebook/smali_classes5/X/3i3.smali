.class public LX/3i3;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/7gI;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 628357
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 628358
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLStoryActionLink;)LX/7gI;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "LX/1PW;",
            ">(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;",
            ")",
            "LX/7gI",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 628359
    new-instance v0, LX/7gI;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    .line 628360
    new-instance v1, LX/6CD;

    invoke-direct {v1}, LX/6CD;-><init>()V

    .line 628361
    const/16 v2, 0xac0

    invoke-static {p0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v5, 0x1ec

    invoke-static {p0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x1822

    invoke-static {p0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x182a

    invoke-static {p0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x1f1

    invoke-static {p0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x3df

    invoke-static {p0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x3e5

    invoke-static {p0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    .line 628362
    iput-object v2, v1, LX/6CD;->a:LX/0Ot;

    iput-object v5, v1, LX/6CD;->b:LX/0Ot;

    iput-object v6, v1, LX/6CD;->c:LX/0Ot;

    iput-object v7, v1, LX/6CD;->d:LX/0Ot;

    iput-object v8, v1, LX/6CD;->e:LX/0Ot;

    iput-object v9, v1, LX/6CD;->f:LX/0Ot;

    iput-object v10, v1, LX/6CD;->g:LX/0Ot;

    .line 628363
    move-object v5, v1

    .line 628364
    check-cast v5, LX/6CD;

    invoke-static {p0}, LX/2ms;->b(LX/0QB;)LX/2ms;

    move-result-object v6

    check-cast v6, LX/2ms;

    invoke-static {p0}, LX/6CH;->b(LX/0QB;)LX/6CH;

    move-result-object v7

    check-cast v7, LX/6CH;

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v7}, LX/7gI;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLStoryActionLink;LX/03V;Lcom/facebook/content/SecureContextHelper;LX/6CD;LX/2ms;LX/6CH;)V

    .line 628365
    return-object v0
.end method
