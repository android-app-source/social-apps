.class public final LX/4zz;
.super LX/4xb;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LX/4xb",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field public a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public b:LX/0Rl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rl",
            "<-",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Map;Ljava/util/Map;LX/0Rl;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<TK;TV;>;",
            "Ljava/util/Map",
            "<TK;TV;>;",
            "LX/0Rl",
            "<-",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;)V"
        }
    .end annotation

    .prologue
    .line 823197
    invoke-direct {p0, p1}, LX/4xb;-><init>(Ljava/util/Map;)V

    .line 823198
    iput-object p2, p0, LX/4zz;->a:Ljava/util/Map;

    .line 823199
    iput-object p3, p0, LX/4zz;->b:LX/0Rl;

    .line 823200
    return-void
.end method

.method private a(LX/0Rl;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Rl",
            "<-TV;>;)Z"
        }
    .end annotation

    .prologue
    .line 823196
    iget-object v0, p0, LX/4zz;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, LX/4zz;->b:LX/0Rl;

    invoke-static {p1}, LX/0PM;->b(LX/0Rl;)LX/0Rl;

    move-result-object v2

    invoke-static {v1, v2}, LX/0Rj;->and(LX/0Rl;LX/0Rl;)LX/0Rl;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Ph;->a(Ljava/lang/Iterable;LX/0Rl;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final remove(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 823195
    iget-object v0, p0, LX/4zz;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, LX/4zz;->b:LX/0Rl;

    invoke-static {p1}, LX/0Rj;->equalTo(Ljava/lang/Object;)LX/0Rl;

    move-result-object v2

    invoke-static {v2}, LX/0PM;->b(LX/0Rl;)LX/0Rl;

    move-result-object v2

    invoke-static {v1, v2}, LX/0Rj;->and(LX/0Rl;LX/0Rl;)LX/0Rl;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Ph;->b(Ljava/lang/Iterable;LX/0Rl;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final removeAll(Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 823191
    invoke-static {p1}, LX/0Rj;->in(Ljava/util/Collection;)LX/0Rl;

    move-result-object v0

    invoke-direct {p0, v0}, LX/4zz;->a(LX/0Rl;)Z

    move-result v0

    return v0
.end method

.method public final retainAll(Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 823194
    invoke-static {p1}, LX/0Rj;->in(Ljava/util/Collection;)LX/0Rl;

    move-result-object v0

    invoke-static {v0}, LX/0Rj;->not(LX/0Rl;)LX/0Rl;

    move-result-object v0

    invoke-direct {p0, v0}, LX/4zz;->a(LX/0Rl;)Z

    move-result v0

    return v0
.end method

.method public final toArray()[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 823193
    invoke-virtual {p0}, LX/4xb;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/0R9;->a(Ljava/util/Iterator;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;)[TT;"
        }
    .end annotation

    .prologue
    .line 823192
    invoke-virtual {p0}, LX/4zz;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/0R9;->a(Ljava/util/Iterator;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
