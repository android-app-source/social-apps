.class public final LX/3oW;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Landroid/os/Handler;


# instance fields
.field public final b:Landroid/view/ViewGroup;

.field private final c:Landroid/content/Context;

.field public final d:Landroid/support/design/widget/Snackbar$SnackbarLayout;

.field public e:I

.field public f:LX/3oV;

.field public final g:LX/3oI;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 640295
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    new-instance v2, LX/3oG;

    invoke-direct {v2}, LX/3oG;-><init>()V

    invoke-direct {v0, v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    sput-object v0, LX/3oW;->a:Landroid/os/Handler;

    .line 640296
    return-void
.end method

.method private constructor <init>(Landroid/view/ViewGroup;)V
    .locals 4

    .prologue
    .line 640287
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 640288
    new-instance v0, LX/3oJ;

    invoke-direct {v0, p0}, LX/3oJ;-><init>(LX/3oW;)V

    iput-object v0, p0, LX/3oW;->g:LX/3oI;

    .line 640289
    iput-object p1, p0, LX/3oW;->b:Landroid/view/ViewGroup;

    .line 640290
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, LX/3oW;->c:Landroid/content/Context;

    .line 640291
    iget-object v0, p0, LX/3oW;->c:Landroid/content/Context;

    invoke-static {v0}, LX/1uI;->a(Landroid/content/Context;)V

    .line 640292
    iget-object v0, p0, LX/3oW;->c:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 640293
    const v1, 0x7f030400

    iget-object v2, p0, LX/3oW;->b:Landroid/view/ViewGroup;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/Snackbar$SnackbarLayout;

    iput-object v0, p0, LX/3oW;->d:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    .line 640294
    return-void
.end method

.method public static a(Landroid/view/View;II)LX/3oW;
    .locals 1
    .param p0    # Landroid/view/View;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 640286
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {p0, v0, p2}, LX/3oW;->a(Landroid/view/View;Ljava/lang/CharSequence;I)LX/3oW;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/view/View;Ljava/lang/CharSequence;I)LX/3oW;
    .locals 5
    .param p0    # Landroid/view/View;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 640267
    new-instance v0, LX/3oW;

    const/4 v3, 0x0

    .line 640268
    move-object v2, v3

    move-object v1, p0

    .line 640269
    :cond_0
    instance-of v4, v1, Landroid/support/design/widget/CoordinatorLayout;

    if-eqz v4, :cond_1

    .line 640270
    check-cast v1, Landroid/view/ViewGroup;

    .line 640271
    :goto_0
    move-object v1, v1

    .line 640272
    invoke-direct {v0, v1}, LX/3oW;-><init>(Landroid/view/ViewGroup;)V

    .line 640273
    invoke-virtual {v0, p1}, LX/3oW;->a(Ljava/lang/CharSequence;)LX/3oW;

    .line 640274
    iput p2, v0, LX/3oW;->e:I

    .line 640275
    return-object v0

    .line 640276
    :cond_1
    instance-of v4, v1, Landroid/widget/FrameLayout;

    if-eqz v4, :cond_3

    .line 640277
    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v2

    const v4, 0x1020002

    if-ne v2, v4, :cond_2

    .line 640278
    check-cast v1, Landroid/view/ViewGroup;

    goto :goto_0

    :cond_2
    move-object v2, v1

    .line 640279
    check-cast v2, Landroid/view/ViewGroup;

    .line 640280
    :cond_3
    if-eqz v1, :cond_4

    .line 640281
    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    .line 640282
    instance-of v4, v1, Landroid/view/View;

    if-eqz v4, :cond_5

    check-cast v1, Landroid/view/View;

    .line 640283
    :cond_4
    :goto_1
    if-nez v1, :cond_0

    move-object v1, v2

    .line 640284
    goto :goto_0

    :cond_5
    move-object v1, v3

    .line 640285
    goto :goto_1
.end method

.method public static d(LX/3oW;I)V
    .locals 2

    .prologue
    .line 640265
    invoke-static {}, LX/3oZ;->a()LX/3oZ;

    move-result-object v0

    iget-object v1, p0, LX/3oW;->g:LX/3oI;

    invoke-virtual {v0, v1, p1}, LX/3oZ;->a(LX/3oI;I)V

    .line 640266
    return-void
.end method

.method public static f(LX/3oW;I)V
    .locals 2

    .prologue
    .line 640297
    invoke-static {}, LX/3oZ;->a()LX/3oZ;

    move-result-object v0

    iget-object v1, p0, LX/3oW;->g:LX/3oI;

    invoke-virtual {v0, v1}, LX/3oZ;->a(LX/3oI;)V

    .line 640298
    iget-object v0, p0, LX/3oW;->f:LX/3oV;

    if-eqz v0, :cond_0

    .line 640299
    iget-object v0, p0, LX/3oW;->f:LX/3oV;

    invoke-virtual {v0, p0, p1}, LX/3oV;->a(LX/3oW;I)V

    .line 640300
    :cond_0
    iget-object v0, p0, LX/3oW;->d:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    invoke-virtual {v0}, Landroid/support/design/widget/Snackbar$SnackbarLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 640301
    instance-of v1, v0, Landroid/view/ViewGroup;

    if-eqz v1, :cond_1

    .line 640302
    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, LX/3oW;->d:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 640303
    :cond_1
    return-void
.end method

.method public static h(LX/3oW;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0xfa

    .line 640256
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    .line 640257
    iget-object v0, p0, LX/3oW;->d:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    iget-object v1, p0, LX/3oW;->d:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    invoke-virtual {v1}, Landroid/support/design/widget/Snackbar$SnackbarLayout;->getHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-static {v0, v1}, LX/0vv;->b(Landroid/view/View;F)V

    .line 640258
    iget-object v0, p0, LX/3oW;->d:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    invoke-static {v0}, LX/0vv;->v(Landroid/view/View;)LX/3sU;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/3sU;->c(F)LX/3sU;

    move-result-object v0

    sget-object v1, LX/3ns;->b:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, LX/3sU;->a(Landroid/view/animation/Interpolator;)LX/3sU;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, LX/3sU;->a(J)LX/3sU;

    move-result-object v0

    new-instance v1, LX/3oS;

    invoke-direct {v1, p0}, LX/3oS;-><init>(LX/3oW;)V

    invoke-virtual {v0, v1}, LX/3sU;->a(LX/3oQ;)LX/3sU;

    move-result-object v0

    invoke-virtual {v0}, LX/3sU;->b()V

    .line 640259
    :goto_0
    return-void

    .line 640260
    :cond_0
    iget-object v0, p0, LX/3oW;->d:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    invoke-virtual {v0}, Landroid/support/design/widget/Snackbar$SnackbarLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f04002c

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 640261
    sget-object v1, LX/3ns;->b:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 640262
    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 640263
    new-instance v1, LX/3oT;

    invoke-direct {v1, p0}, LX/3oT;-><init>(LX/3oW;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 640264
    iget-object v1, p0, LX/3oW;->d:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    invoke-virtual {v1, v0}, Landroid/support/design/widget/Snackbar$SnackbarLayout;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(I)LX/3oW;
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 640252
    iget-object v0, p0, LX/3oW;->d:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    .line 640253
    iget-object v1, v0, Landroid/support/design/widget/Snackbar$SnackbarLayout;->b:Landroid/widget/Button;

    move-object v0, v1

    .line 640254
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 640255
    return-object p0
.end method

.method public final a(ILandroid/view/View$OnClickListener;)LX/3oW;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 640251
    iget-object v0, p0, LX/3oW;->c:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, LX/3oW;->a(Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)LX/3oW;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/CharSequence;)LX/3oW;
    .locals 2
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 640247
    iget-object v0, p0, LX/3oW;->d:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    .line 640248
    iget-object v1, v0, Landroid/support/design/widget/Snackbar$SnackbarLayout;->a:Landroid/widget/TextView;

    move-object v0, v1

    .line 640249
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 640250
    return-object p0
.end method

.method public final a(Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)LX/3oW;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 640238
    iget-object v0, p0, LX/3oW;->d:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    .line 640239
    iget-object v1, v0, Landroid/support/design/widget/Snackbar$SnackbarLayout;->b:Landroid/widget/Button;

    move-object v0, v1

    .line 640240
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    if-nez p2, :cond_1

    .line 640241
    :cond_0
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 640242
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 640243
    :goto_0
    return-object p0

    .line 640244
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 640245
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 640246
    new-instance v1, LX/3oH;

    invoke-direct {v1, p0, p2}, LX/3oH;-><init>(LX/3oW;Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 640236
    invoke-static {}, LX/3oZ;->a()LX/3oZ;

    move-result-object v0

    iget v1, p0, LX/3oW;->e:I

    iget-object v2, p0, LX/3oW;->g:LX/3oI;

    invoke-virtual {v0, v1, v2}, LX/3oZ;->a(ILX/3oI;)V

    .line 640237
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 640234
    const/4 v0, 0x3

    invoke-static {p0, v0}, LX/3oW;->d(LX/3oW;I)V

    .line 640235
    return-void
.end method
