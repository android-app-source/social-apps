.class public LX/4Qp;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 706395
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 20

    .prologue
    .line 706396
    const/16 v16, 0x0

    .line 706397
    const/4 v15, 0x0

    .line 706398
    const/4 v14, 0x0

    .line 706399
    const/4 v13, 0x0

    .line 706400
    const/4 v12, 0x0

    .line 706401
    const/4 v11, 0x0

    .line 706402
    const/4 v10, 0x0

    .line 706403
    const/4 v9, 0x0

    .line 706404
    const/4 v8, 0x0

    .line 706405
    const/4 v7, 0x0

    .line 706406
    const/4 v6, 0x0

    .line 706407
    const/4 v5, 0x0

    .line 706408
    const/4 v4, 0x0

    .line 706409
    const/4 v3, 0x0

    .line 706410
    const/4 v2, 0x0

    .line 706411
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v17

    sget-object v18, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    if-eq v0, v1, :cond_1

    .line 706412
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 706413
    const/4 v2, 0x0

    .line 706414
    :goto_0
    return v2

    .line 706415
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 706416
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v17

    sget-object v18, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    if-eq v0, v1, :cond_a

    .line 706417
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v17

    .line 706418
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 706419
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v18

    sget-object v19, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-eq v0, v1, :cond_1

    if-eqz v17, :cond_1

    .line 706420
    const-string v18, "animation_type"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_2

    .line 706421
    const/4 v7, 0x1

    .line 706422
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Lcom/facebook/graphql/enums/GraphQLParticleEffectAnimationTypeEnum;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLParticleEffectAnimationTypeEnum;

    move-result-object v16

    goto :goto_1

    .line 706423
    :cond_2
    const-string v18, "asset_image"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_3

    .line 706424
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v15

    goto :goto_1

    .line 706425
    :cond_3
    const-string v18, "col_of_sprites"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_4

    .line 706426
    const/4 v6, 0x1

    .line 706427
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v14

    goto :goto_1

    .line 706428
    :cond_4
    const-string v18, "frame_rate"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_5

    .line 706429
    const/4 v5, 0x1

    .line 706430
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v13

    goto :goto_1

    .line 706431
    :cond_5
    const-string v18, "id"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_6

    .line 706432
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    goto :goto_1

    .line 706433
    :cond_6
    const-string v18, "num_of_sprites"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_7

    .line 706434
    const/4 v4, 0x1

    .line 706435
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v11

    goto :goto_1

    .line 706436
    :cond_7
    const-string v18, "play_count"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_8

    .line 706437
    const/4 v3, 0x1

    .line 706438
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v10

    goto/16 :goto_1

    .line 706439
    :cond_8
    const-string v18, "row_of_sprites"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_9

    .line 706440
    const/4 v2, 0x1

    .line 706441
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v9

    goto/16 :goto_1

    .line 706442
    :cond_9
    const-string v18, "url"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_0

    .line 706443
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto/16 :goto_1

    .line 706444
    :cond_a
    const/16 v17, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 706445
    if-eqz v7, :cond_b

    .line 706446
    const/4 v7, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v7, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 706447
    :cond_b
    const/4 v7, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v15}, LX/186;->b(II)V

    .line 706448
    if-eqz v6, :cond_c

    .line 706449
    const/4 v6, 0x2

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v14, v7}, LX/186;->a(III)V

    .line 706450
    :cond_c
    if-eqz v5, :cond_d

    .line 706451
    const/4 v5, 0x3

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v13, v6}, LX/186;->a(III)V

    .line 706452
    :cond_d
    const/4 v5, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v12}, LX/186;->b(II)V

    .line 706453
    if-eqz v4, :cond_e

    .line 706454
    const/4 v4, 0x5

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v11, v5}, LX/186;->a(III)V

    .line 706455
    :cond_e
    if-eqz v3, :cond_f

    .line 706456
    const/4 v3, 0x6

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10, v4}, LX/186;->a(III)V

    .line 706457
    :cond_f
    if-eqz v2, :cond_10

    .line 706458
    const/4 v2, 0x7

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9, v3}, LX/186;->a(III)V

    .line 706459
    :cond_10
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 706460
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 706461
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 706462
    invoke-virtual {p0, p1, v2, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 706463
    if-eqz v0, :cond_0

    .line 706464
    const-string v0, "animation_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 706465
    const-class v0, Lcom/facebook/graphql/enums/GraphQLParticleEffectAnimationTypeEnum;

    invoke-virtual {p0, p1, v2, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLParticleEffectAnimationTypeEnum;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLParticleEffectAnimationTypeEnum;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 706466
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 706467
    if-eqz v0, :cond_1

    .line 706468
    const-string v1, "asset_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 706469
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 706470
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 706471
    if-eqz v0, :cond_2

    .line 706472
    const-string v1, "col_of_sprites"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 706473
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 706474
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 706475
    if-eqz v0, :cond_3

    .line 706476
    const-string v1, "frame_rate"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 706477
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 706478
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 706479
    if-eqz v0, :cond_4

    .line 706480
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 706481
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 706482
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 706483
    if-eqz v0, :cond_5

    .line 706484
    const-string v1, "num_of_sprites"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 706485
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 706486
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 706487
    if-eqz v0, :cond_6

    .line 706488
    const-string v1, "play_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 706489
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 706490
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 706491
    if-eqz v0, :cond_7

    .line 706492
    const-string v1, "row_of_sprites"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 706493
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 706494
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 706495
    if-eqz v0, :cond_8

    .line 706496
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 706497
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 706498
    :cond_8
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 706499
    return-void
.end method
