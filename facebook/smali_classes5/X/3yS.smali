.class public LX/3yS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1MT;


# instance fields
.field public final a:LX/3yR;


# direct methods
.method public constructor <init>(LX/3yR;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 661072
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 661073
    iput-object p1, p0, LX/3yS;->a:LX/3yR;

    .line 661074
    return-void
.end method


# virtual methods
.method public final getFilesFromWorkerThread(Ljava/io/File;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;",
            ">;"
        }
    .end annotation

    .prologue
    .line 661075
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 661076
    iget-object v1, p0, LX/3yS;->a:LX/3yR;

    invoke-virtual {v1}, LX/3yR;->a()LX/0Px;

    move-result-object v1

    .line 661077
    invoke-static {}, LX/0PM;->d()Ljava/util/LinkedHashMap;

    move-result-object v2

    .line 661078
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3yQ;

    .line 661079
    iget-object v4, v1, LX/3yQ;->name:Ljava/lang/String;

    move-object v4, v4

    .line 661080
    invoke-virtual {v1}, LX/3yQ;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v4, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 661081
    :cond_0
    move-object v1, v2

    .line 661082
    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 661083
    const-string v2, "quick_experiment_pairs.txt"

    invoke-static {p1, v2, v1}, LX/4l9;->a(Ljava/io/File;Ljava/lang/String;Ljava/util/Map;)Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 661084
    :cond_1
    return-object v0
.end method
