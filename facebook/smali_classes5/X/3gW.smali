.class public final LX/3gW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2ZE;


# instance fields
.field public final synthetic a:LX/3gU;


# direct methods
.method public constructor <init>(LX/3gU;)V
    .locals 0

    .prologue
    .line 624554
    iput-object p1, p0, LX/3gW;->a:LX/3gU;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Iterable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "LX/2Vj;",
            ">;"
        }
    .end annotation

    .prologue
    .line 624555
    iget-object v0, p0, LX/3gW;->a:LX/3gU;

    iget-object v0, v0, LX/3gU;->b:LX/3gV;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v0

    const-string v1, "mqtt_config"

    .line 624556
    iput-object v1, v0, LX/2Vk;->c:Ljava/lang/String;

    .line 624557
    move-object v0, v0

    .line 624558
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/2Vk;->a(Z)LX/2Vk;

    move-result-object v0

    invoke-virtual {v0}, LX/2Vk;->a()LX/2Vj;

    move-result-object v0

    .line 624559
    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/util/Map;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 624560
    const-string v0, "mqtt_config"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 624561
    const-string v1, ""

    .line 624562
    if-eqz v0, :cond_0

    .line 624563
    const-string v2, "mqtt_config"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 624564
    if-eqz v0, :cond_0

    .line 624565
    invoke-virtual {v0}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v1

    .line 624566
    iget-object v2, p0, LX/3gW;->a:LX/3gU;

    iget-object v3, p0, LX/3gW;->a:LX/3gU;

    iget-wide v4, v3, LX/3gU;->a:J

    const-string v3, "fetch_delay_hours"

    invoke-virtual {v0, v3}, LX/0lF;->b(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    iget-object v3, p0, LX/3gW;->a:LX/3gU;

    iget-wide v6, v3, LX/3gU;->a:J

    invoke-virtual {v0, v6, v7}, LX/0lF;->a(J)J

    move-result-wide v6

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    iput-wide v4, v2, LX/3gU;->a:J

    :cond_0
    move-object v0, v1

    .line 624567
    iget-object v1, p0, LX/3gW;->a:LX/3gU;

    .line 624568
    iget-object v2, v1, LX/3gU;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a()Z

    move-result v2

    if-nez v2, :cond_2

    .line 624569
    :cond_1
    :goto_0
    return-void

    .line 624570
    :cond_2
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 624571
    iget-object v2, v1, LX/3gU;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v3, LX/1p8;->b:LX/0Tn;

    invoke-interface {v2, v3, v0}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    goto :goto_0
.end method
