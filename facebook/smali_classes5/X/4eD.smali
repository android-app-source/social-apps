.class public LX/4eD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1FG;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field private static final c:[B


# instance fields
.field public final a:LX/0Zi;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "Ljava/nio/ByteBuffer;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/4eM;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 796997
    const/4 v0, 0x2

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, LX/4eD;->c:[B

    return-void

    nop

    :array_0
    .array-data 1
        -0x1t
        -0x27t
    .end array-data
.end method

.method public constructor <init>(LX/4eM;ILX/0Zi;)V
    .locals 3

    .prologue
    .line 796998
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 796999
    iput-object p1, p0, LX/4eD;->b:LX/4eM;

    .line 797000
    iput-object p3, p0, LX/4eD;->a:LX/0Zi;

    .line 797001
    const/4 v0, 0x0

    :goto_0
    if-ge v0, p2, :cond_0

    .line 797002
    iget-object v1, p0, LX/4eD;->a:LX/0Zi;

    const/16 v2, 0x4000

    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 797003
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 797004
    :cond_0
    return-void
.end method

.method private static b(LX/1FL;Landroid/graphics/Bitmap$Config;)Landroid/graphics/BitmapFactory$Options;
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x1

    .line 797005
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 797006
    iget v1, p0, LX/1FL;->g:I

    move v1, v1

    .line 797007
    iput v1, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 797008
    iput-boolean v3, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 797009
    invoke-virtual {p0}, LX/1FL;->b()Ljava/io/InputStream;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2, v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 797010
    iget v1, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    if-eq v1, v4, :cond_0

    iget v1, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    if-ne v1, v4, :cond_1

    .line 797011
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 797012
    :cond_1
    const/4 v1, 0x0

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 797013
    iput-boolean v3, v0, Landroid/graphics/BitmapFactory$Options;->inDither:Z

    .line 797014
    iput-object p1, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 797015
    iput-boolean v3, v0, Landroid/graphics/BitmapFactory$Options;->inMutable:Z

    .line 797016
    return-object v0
.end method


# virtual methods
.method public final a(LX/1FL;Landroid/graphics/Bitmap$Config;)LX/1FJ;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1FL;",
            "Landroid/graphics/Bitmap$Config;",
            ")",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 797017
    invoke-static {p1, p2}, LX/4eD;->b(LX/1FL;Landroid/graphics/Bitmap$Config;)Landroid/graphics/BitmapFactory$Options;

    move-result-object v1

    .line 797018
    iget-object v0, v1, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    if-eq v0, v2, :cond_0

    const/4 v0, 0x1

    .line 797019
    :goto_0
    :try_start_0
    invoke-virtual {p1}, LX/1FL;->b()Ljava/io/InputStream;

    move-result-object v2

    invoke-virtual {p0, v2, v1}, LX/4eD;->a(Ljava/io/InputStream;Landroid/graphics/BitmapFactory$Options;)LX/1FJ;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 797020
    :goto_1
    return-object v0

    .line 797021
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 797022
    :catch_0
    move-exception v1

    .line 797023
    if-eqz v0, :cond_1

    .line 797024
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {p0, p1, v0}, LX/4eD;->a(LX/1FL;Landroid/graphics/Bitmap$Config;)LX/1FJ;

    move-result-object v0

    goto :goto_1

    .line 797025
    :cond_1
    throw v1
.end method

.method public final a(LX/1FL;Landroid/graphics/Bitmap$Config;I)LX/1FJ;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1FL;",
            "Landroid/graphics/Bitmap$Config;",
            "I)",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 797026
    const/4 v1, 0x1

    .line 797027
    iget-object v0, p1, LX/1FL;->c:LX/1lW;

    sget-object v2, LX/1ld;->a:LX/1lW;

    if-eq v0, v2, :cond_4

    move v0, v1

    .line 797028
    :goto_0
    move v2, v0

    .line 797029
    invoke-static {p1, p2}, LX/4eD;->b(LX/1FL;Landroid/graphics/Bitmap$Config;)Landroid/graphics/BitmapFactory$Options;

    move-result-object v3

    .line 797030
    invoke-virtual {p1}, LX/1FL;->b()Ljava/io/InputStream;

    move-result-object v0

    .line 797031
    invoke-static {v0}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 797032
    invoke-virtual {p1}, LX/1FL;->i()I

    move-result v1

    if-le v1, p3, :cond_3

    .line 797033
    new-instance v1, LX/46c;

    invoke-direct {v1, v0, p3}, LX/46c;-><init>(Ljava/io/InputStream;I)V

    .line 797034
    :goto_1
    if-nez v2, :cond_2

    .line 797035
    new-instance v0, LX/46d;

    sget-object v2, LX/4eD;->c:[B

    invoke-direct {v0, v1, v2}, LX/46d;-><init>(Ljava/io/InputStream;[B)V

    .line 797036
    :goto_2
    iget-object v1, v3, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    if-eq v1, v2, :cond_0

    const/4 v1, 0x1

    .line 797037
    :goto_3
    :try_start_0
    invoke-virtual {p0, v0, v3}, LX/4eD;->a(Ljava/io/InputStream;Landroid/graphics/BitmapFactory$Options;)LX/1FJ;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 797038
    :goto_4
    return-object v0

    .line 797039
    :cond_0
    const/4 v1, 0x0

    goto :goto_3

    .line 797040
    :catch_0
    move-exception v0

    .line 797041
    if-eqz v1, :cond_1

    .line 797042
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {p0, p1, v0}, LX/4eD;->a(LX/1FL;Landroid/graphics/Bitmap$Config;)LX/1FJ;

    move-result-object v0

    goto :goto_4

    .line 797043
    :cond_1
    throw v0

    :cond_2
    move-object v0, v1

    goto :goto_2

    :cond_3
    move-object v1, v0

    goto :goto_1

    .line 797044
    :cond_4
    iget-object v0, p1, LX/1FL;->b:LX/1Gd;

    if-eqz v0, :cond_5

    move v0, v1

    .line 797045
    goto :goto_0

    .line 797046
    :cond_5
    iget-object v0, p1, LX/1FL;->a:LX/1FJ;

    invoke-static {v0}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 797047
    iget-object v0, p1, LX/1FL;->a:LX/1FJ;

    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FK;

    .line 797048
    add-int/lit8 v2, p3, -0x2

    invoke-virtual {v0, v2}, LX/1FK;->a(I)B

    move-result v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_6

    add-int/lit8 v2, p3, -0x1

    invoke-virtual {v0, v2}, LX/1FK;->a(I)B

    move-result v0

    const/16 v2, -0x27

    if-ne v0, v2, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/io/InputStream;Landroid/graphics/BitmapFactory$Options;)LX/1FJ;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/InputStream;",
            "Landroid/graphics/BitmapFactory$Options;",
            ")",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 797049
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 797050
    iget v0, p2, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v1, p2, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    iget-object v2, p2, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, LX/1le;->a(IILandroid/graphics/Bitmap$Config;)I

    move-result v0

    .line 797051
    iget-object v1, p0, LX/4eD;->b:LX/4eM;

    invoke-virtual {v1, v0}, LX/1FR;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 797052
    if-nez v0, :cond_0

    .line 797053
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "BitmapPool.get returned null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 797054
    :cond_0
    iput-object v0, p2, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    .line 797055
    iget-object v1, p0, LX/4eD;->a:LX/0Zi;

    invoke-virtual {v1}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/nio/ByteBuffer;

    .line 797056
    if-nez v1, :cond_2

    .line 797057
    const/16 v1, 0x4000

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    move-object v2, v1

    .line 797058
    :goto_0
    :try_start_0
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    iput-object v1, p2, Landroid/graphics/BitmapFactory$Options;->inTempStorage:[B

    .line 797059
    const/4 v1, 0x0

    invoke-static {p1, v1, p2}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 797060
    iget-object v3, p0, LX/4eD;->a:LX/0Zi;

    invoke-virtual {v3, v2}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 797061
    if-eq v0, v1, :cond_1

    .line 797062
    iget-object v2, p0, LX/4eD;->b:LX/4eM;

    invoke-virtual {v2, v0}, LX/1FR;->a(Ljava/lang/Object;)V

    .line 797063
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 797064
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 797065
    :catch_0
    move-exception v1

    .line 797066
    :try_start_1
    iget-object v3, p0, LX/4eD;->b:LX/4eM;

    invoke-virtual {v3, v0}, LX/1FR;->a(Ljava/lang/Object;)V

    .line 797067
    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 797068
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/4eD;->a:LX/0Zi;

    invoke-virtual {v1, v2}, LX/0Zj;->a(Ljava/lang/Object;)Z

    throw v0

    .line 797069
    :cond_1
    iget-object v0, p0, LX/4eD;->b:LX/4eM;

    invoke-static {v1, v0}, LX/1FJ;->a(Ljava/lang/Object;LX/1FN;)LX/1FJ;

    move-result-object v0

    return-object v0

    :cond_2
    move-object v2, v1

    goto :goto_0
.end method
