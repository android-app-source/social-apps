.class public LX/3iH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field public final a:LX/2U1;

.field private final b:LX/0SI;

.field private final c:Lcom/facebook/auth/viewercontext/ViewerContext;

.field public d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8Fa;",
            ">;"
        }
    .end annotation
.end field

.field private f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/2U1;LX/0SI;Lcom/facebook/auth/viewercontext/ViewerContext;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .param p4    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2U1;",
            "LX/0SI;",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/8Fa;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 629098
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 629099
    iput-object p1, p0, LX/3iH;->a:LX/2U1;

    .line 629100
    iput-object p2, p0, LX/3iH;->b:LX/0SI;

    .line 629101
    iput-object p3, p0, LX/3iH;->c:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 629102
    iput-object p4, p0, LX/3iH;->f:LX/0Ot;

    .line 629103
    iput-object p5, p0, LX/3iH;->d:LX/0Ot;

    .line 629104
    iput-object p6, p0, LX/3iH;->e:LX/0Ot;

    .line 629105
    iput-object p7, p0, LX/3iH;->g:LX/0Ot;

    .line 629106
    return-void
.end method

.method public static a(LX/0QB;)LX/3iH;
    .locals 1

    .prologue
    .line 629097
    invoke-static {p0}, LX/3iH;->b(LX/0QB;)LX/3iH;

    move-result-object v0

    return-object v0
.end method

.method private b(Ljava/lang/String;)LX/0am;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 629089
    iget-object v0, p0, LX/3iH;->a:LX/2U1;

    invoke-virtual {v0, p1}, LX/2U2;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Dk;

    .line 629090
    if-eqz v0, :cond_0

    .line 629091
    iget-object v1, v0, LX/8Dk;->b:LX/0am;

    move-object v1, v1

    .line 629092
    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v1

    if-nez v1, :cond_1

    .line 629093
    :cond_0
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    .line 629094
    :goto_0
    return-object v0

    .line 629095
    :cond_1
    iget-object v1, v0, LX/8Dk;->b:LX/0am;

    move-object v0, v1

    .line 629096
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/3iH;
    .locals 8

    .prologue
    .line 629087
    new-instance v0, LX/3iH;

    invoke-static {p0}, LX/2U1;->a(LX/0QB;)LX/2U1;

    move-result-object v1

    check-cast v1, LX/2U1;

    invoke-static {p0}, LX/0WG;->b(LX/0QB;)LX/0SI;

    move-result-object v2

    check-cast v2, LX/0SI;

    invoke-static {p0}, LX/0eQ;->b(LX/0QB;)Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v3

    check-cast v3, Lcom/facebook/auth/viewercontext/ViewerContext;

    const/16 v4, 0x140f

    invoke-static {p0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0xb83

    invoke-static {p0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x2c0c

    invoke-static {p0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0xbc

    invoke-static {p0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, LX/3iH;-><init>(LX/2U1;LX/0SI;Lcom/facebook/auth/viewercontext/ViewerContext;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 629088
    return-object v0
.end method

.method private c(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 629084
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v1

    .line 629085
    iget-object v0, p0, LX/3iH;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/facebook/pages/common/viewercontextutils/ViewerContextUtil$3;

    invoke-direct {v2, p0, p1, v1}, Lcom/facebook/pages/common/viewercontextutils/ViewerContextUtil$3;-><init>(LX/3iH;Ljava/lang/String;Lcom/google/common/util/concurrent/SettableFuture;)V

    const v3, 0x47cc33a7

    invoke-static {v0, v2, v3}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 629086
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/auth/viewercontext/ViewerContext;
    .locals 3

    .prologue
    .line 629066
    iget-object v0, p0, LX/3iH;->b:LX/0SI;

    invoke-interface {v0}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    .line 629067
    invoke-static {}, Lcom/facebook/auth/viewercontext/ViewerContext;->newBuilder()LX/0SK;

    move-result-object v1

    const/4 v2, 0x1

    .line 629068
    iput-boolean v2, v1, LX/0SK;->d:Z

    .line 629069
    move-object v1, v1

    .line 629070
    iget-object v2, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->c:Ljava/lang/String;

    move-object v2, v2

    .line 629071
    iput-object v2, v1, LX/0SK;->c:Ljava/lang/String;

    .line 629072
    move-object v1, v1

    .line 629073
    iget-object v2, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->f:Ljava/lang/String;

    move-object v2, v2

    .line 629074
    iput-object v2, v1, LX/0SK;->f:Ljava/lang/String;

    .line 629075
    move-object v1, v1

    .line 629076
    iget-object v2, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->e:Ljava/lang/String;

    move-object v0, v2

    .line 629077
    iput-object v0, v1, LX/0SK;->e:Ljava/lang/String;

    .line 629078
    move-object v0, v1

    .line 629079
    iput-object p1, v0, LX/0SK;->a:Ljava/lang/String;

    .line 629080
    move-object v0, v0

    .line 629081
    iput-object p2, v0, LX/0SK;->b:Ljava/lang/String;

    .line 629082
    move-object v0, v0

    .line 629083
    invoke-virtual {v0}, LX/0SK;->h()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;"
        }
    .end annotation

    .prologue
    .line 629037
    iget-object v0, p0, LX/3iH;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    const-string v1, "viewer_context_util_requested"

    invoke-interface {v0, v1}, LX/0Zb;->a(Ljava/lang/String;)V

    .line 629038
    iget-object v0, p0, LX/3iH;->c:Lcom/facebook/auth/viewercontext/ViewerContext;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3iH;->c:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 629039
    iget-boolean v1, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move v0, v1

    .line 629040
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3iH;->c:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 629041
    iget-object v1, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v0, v1

    .line 629042
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 629043
    iget-object v0, p0, LX/3iH;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    const-string v1, "viewer_context_util_available_immediately"

    invoke-interface {v0, v1}, LX/0Zb;->a(Ljava/lang/String;)V

    .line 629044
    iget-object v0, p0, LX/3iH;->c:Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 629045
    :goto_0
    return-object v0

    .line 629046
    :cond_0
    invoke-direct {p0, p1}, LX/3iH;->b(Ljava/lang/String;)LX/0am;

    move-result-object v1

    .line 629047
    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 629048
    iget-object v0, p0, LX/3iH;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    const-string v2, "viewer_context_util_available_immediately"

    invoke-interface {v0, v2}, LX/0Zb;->a(Ljava/lang/String;)V

    .line 629049
    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, p1, v0}, LX/3iH;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0

    .line 629050
    :cond_1
    invoke-direct {p0, p1}, LX/3iH;->c(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, LX/8Fg;

    invoke-direct {v1, p0, p1}, LX/8Fg;-><init>(LX/3iH;Ljava/lang/String;)V

    invoke-static {v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;LX/8E8;Ljava/util/concurrent/Executor;)V
    .locals 2

    .prologue
    .line 629051
    iget-object v0, p0, LX/3iH;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    const-string v1, "viewer_context_util_requested"

    invoke-interface {v0, v1}, LX/0Zb;->a(Ljava/lang/String;)V

    .line 629052
    iget-object v0, p0, LX/3iH;->c:Lcom/facebook/auth/viewercontext/ViewerContext;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3iH;->c:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 629053
    iget-boolean v1, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move v0, v1

    .line 629054
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3iH;->c:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 629055
    iget-object v1, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v0, v1

    .line 629056
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 629057
    iget-object v0, p0, LX/3iH;->c:Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-interface {p2, v0}, LX/8E8;->a(Lcom/facebook/auth/viewercontext/ViewerContext;)V

    .line 629058
    iget-object v0, p0, LX/3iH;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    const-string v1, "viewer_context_util_available_immediately"

    invoke-interface {v0, v1}, LX/0Zb;->a(Ljava/lang/String;)V

    .line 629059
    :goto_0
    return-void

    .line 629060
    :cond_0
    invoke-direct {p0, p1}, LX/3iH;->b(Ljava/lang/String;)LX/0am;

    move-result-object v0

    .line 629061
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 629062
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, p1, v0}, LX/3iH;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    invoke-interface {p2, v0}, LX/8E8;->a(Lcom/facebook/auth/viewercontext/ViewerContext;)V

    .line 629063
    iget-object v0, p0, LX/3iH;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    const-string v1, "viewer_context_util_available_immediately"

    invoke-interface {v0, v1}, LX/0Zb;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 629064
    :cond_1
    invoke-interface {p2}, LX/8E8;->a()V

    .line 629065
    invoke-direct {p0, p1}, LX/3iH;->c(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, LX/8Ff;

    invoke-direct {v1, p0, p2, p1}, LX/8Ff;-><init>(LX/3iH;LX/8E8;Ljava/lang/String;)V

    invoke-static {v0, v1, p3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method
