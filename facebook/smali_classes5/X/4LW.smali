.class public LX/4LW;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 683761
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 31

    .prologue
    .line 683762
    const-wide/16 v22, 0x0

    .line 683763
    const-wide/16 v20, 0x0

    .line 683764
    const-wide/16 v18, 0x0

    .line 683765
    const-wide/16 v16, 0x0

    .line 683766
    const-wide/16 v14, 0x0

    .line 683767
    const-wide/16 v12, 0x0

    .line 683768
    const-wide/16 v10, 0x0

    .line 683769
    const/4 v9, 0x0

    .line 683770
    const/4 v8, 0x0

    .line 683771
    const/4 v7, 0x0

    .line 683772
    const/4 v6, 0x0

    .line 683773
    const/4 v5, 0x0

    .line 683774
    const/4 v4, 0x0

    .line 683775
    const/4 v3, 0x0

    .line 683776
    const/4 v2, 0x0

    .line 683777
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v24

    sget-object v25, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    if-eq v0, v1, :cond_11

    .line 683778
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 683779
    const/4 v2, 0x0

    .line 683780
    :goto_0
    return v2

    .line 683781
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_9

    .line 683782
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 683783
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 683784
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_0

    if-eqz v2, :cond_0

    .line 683785
    const-string v6, "accuracy_meters"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 683786
    const/4 v2, 0x1

    .line 683787
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 683788
    :cond_1
    const-string v6, "latitude"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 683789
    const/4 v2, 0x1

    .line 683790
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v13, v2

    move-wide/from16 v26, v6

    goto :goto_1

    .line 683791
    :cond_2
    const-string v6, "longitude"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 683792
    const/4 v2, 0x1

    .line 683793
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v12, v2

    move-wide/from16 v24, v6

    goto :goto_1

    .line 683794
    :cond_3
    const-string v6, "altitude_accuracy_meters"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 683795
    const/4 v2, 0x1

    .line 683796
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v11, v2

    move-wide/from16 v22, v6

    goto :goto_1

    .line 683797
    :cond_4
    const-string v6, "altitude_meters"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 683798
    const/4 v2, 0x1

    .line 683799
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v10, v2

    move-wide/from16 v20, v6

    goto :goto_1

    .line 683800
    :cond_5
    const-string v6, "bearing_degrees"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 683801
    const/4 v2, 0x1

    .line 683802
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v9, v2

    move-wide/from16 v18, v6

    goto :goto_1

    .line 683803
    :cond_6
    const-string v6, "speed_meters_per_second"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 683804
    const/4 v2, 0x1

    .line 683805
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v8, v2

    move-wide/from16 v16, v6

    goto/16 :goto_1

    .line 683806
    :cond_7
    const-string v6, "timestamp_milliseconds"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 683807
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v14, v2

    goto/16 :goto_1

    .line 683808
    :cond_8
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 683809
    :cond_9
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 683810
    if-eqz v3, :cond_a

    .line 683811
    const/4 v3, 0x0

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 683812
    :cond_a
    if-eqz v13, :cond_b

    .line 683813
    const/4 v3, 0x1

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v26

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 683814
    :cond_b
    if-eqz v12, :cond_c

    .line 683815
    const/4 v3, 0x2

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v24

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 683816
    :cond_c
    if-eqz v11, :cond_d

    .line 683817
    const/4 v3, 0x3

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v22

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 683818
    :cond_d
    if-eqz v10, :cond_e

    .line 683819
    const/4 v3, 0x4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v20

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 683820
    :cond_e
    if-eqz v9, :cond_f

    .line 683821
    const/4 v3, 0x5

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v18

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 683822
    :cond_f
    if-eqz v8, :cond_10

    .line 683823
    const/4 v3, 0x6

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v16

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 683824
    :cond_10
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 683825
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_11
    move-wide/from16 v24, v18

    move-wide/from16 v26, v20

    move-wide/from16 v18, v12

    move-wide/from16 v20, v14

    move v12, v6

    move v13, v7

    move v14, v9

    move v9, v3

    move v3, v8

    move v8, v2

    move/from16 v28, v5

    move-wide/from16 v29, v16

    move-wide/from16 v16, v10

    move/from16 v11, v28

    move v10, v4

    move-wide/from16 v4, v22

    move-wide/from16 v22, v29

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 683826
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 683827
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 683828
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_0

    .line 683829
    const-string v2, "accuracy_meters"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 683830
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 683831
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 683832
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_1

    .line 683833
    const-string v2, "latitude"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 683834
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 683835
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 683836
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_2

    .line 683837
    const-string v2, "longitude"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 683838
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 683839
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 683840
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_3

    .line 683841
    const-string v2, "altitude_accuracy_meters"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 683842
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 683843
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 683844
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_4

    .line 683845
    const-string v2, "altitude_meters"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 683846
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 683847
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 683848
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_5

    .line 683849
    const-string v2, "bearing_degrees"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 683850
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 683851
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 683852
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_6

    .line 683853
    const-string v2, "speed_meters_per_second"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 683854
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 683855
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 683856
    if-eqz v0, :cond_7

    .line 683857
    const-string v1, "timestamp_milliseconds"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 683858
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 683859
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 683860
    return-void
.end method
