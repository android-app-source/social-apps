.class public final LX/3ge;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2ZE;


# instance fields
.field public final synthetic a:LX/3gc;


# direct methods
.method public constructor <init>(LX/3gc;)V
    .locals 0

    .prologue
    .line 624652
    iput-object p1, p0, LX/3ge;->a:LX/3gc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Iterable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "LX/2Vj;",
            ">;"
        }
    .end annotation

    .prologue
    .line 624644
    iget-object v0, p0, LX/3ge;->a:LX/3gc;

    iget-object v0, v0, LX/3gc;->a:LX/3gd;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v0

    const-string v1, "fetchStickyGuardrailInfo"

    .line 624645
    iput-object v1, v0, LX/2Vk;->c:Ljava/lang/String;

    .line 624646
    move-object v0, v0

    .line 624647
    invoke-virtual {v0}, LX/2Vk;->a()LX/2Vj;

    move-result-object v0

    .line 624648
    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 624649
    const-string v0, "fetchStickyGuardrailInfo"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/protocol/FetchComposerPrivacyGuardrailInfoModels$FetchComposerPrivacyGuardrailInfoModel;

    .line 624650
    iget-object v1, p0, LX/3ge;->a:LX/3gc;

    iget-object v1, v1, LX/3gc;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3lZ;

    invoke-static {v0}, LX/3lb;->a(Lcom/facebook/privacy/protocol/FetchComposerPrivacyGuardrailInfoModels$FetchComposerPrivacyGuardrailInfoModel;)Lcom/facebook/graphql/model/GraphQLViewer;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/3lZ;->a(Lcom/facebook/graphql/model/GraphQLViewer;)V

    .line 624651
    return-void
.end method
