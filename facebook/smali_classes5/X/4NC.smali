.class public LX/4NC;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 691569
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 35

    .prologue
    .line 691462
    const/16 v29, 0x0

    .line 691463
    const/16 v28, 0x0

    .line 691464
    const/16 v27, 0x0

    .line 691465
    const/16 v26, 0x0

    .line 691466
    const/16 v25, 0x0

    .line 691467
    const/16 v24, 0x0

    .line 691468
    const/16 v23, 0x0

    .line 691469
    const/16 v22, 0x0

    .line 691470
    const/16 v21, 0x0

    .line 691471
    const/16 v20, 0x0

    .line 691472
    const/16 v19, 0x0

    .line 691473
    const/16 v18, 0x0

    .line 691474
    const/16 v17, 0x0

    .line 691475
    const/16 v16, 0x0

    .line 691476
    const/4 v15, 0x0

    .line 691477
    const/4 v14, 0x0

    .line 691478
    const/4 v13, 0x0

    .line 691479
    const/4 v12, 0x0

    .line 691480
    const/4 v9, 0x0

    .line 691481
    const-wide/16 v10, 0x0

    .line 691482
    const/4 v8, 0x0

    .line 691483
    const/4 v7, 0x0

    .line 691484
    const/4 v6, 0x0

    .line 691485
    const/4 v5, 0x0

    .line 691486
    const/4 v4, 0x0

    .line 691487
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v30

    sget-object v31, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v30

    move-object/from16 v1, v31

    if-eq v0, v1, :cond_1c

    .line 691488
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 691489
    const/4 v4, 0x0

    .line 691490
    :goto_0
    return v4

    .line 691491
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 691492
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v30

    sget-object v31, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v30

    move-object/from16 v1, v31

    if-eq v0, v1, :cond_17

    .line 691493
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v30

    .line 691494
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 691495
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v31

    sget-object v32, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v31

    move-object/from16 v1, v32

    if-eq v0, v1, :cond_1

    if-eqz v30, :cond_1

    .line 691496
    const-string v31, "__type__"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-nez v31, :cond_2

    const-string v31, "__typename"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_3

    .line 691497
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, p1

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/String;)I

    move-result v29

    goto :goto_1

    .line 691498
    :cond_3
    const-string v31, "campaign_title"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_4

    .line 691499
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v28

    goto :goto_1

    .line 691500
    :cond_4
    const-string v31, "can_donate"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_5

    .line 691501
    const/4 v9, 0x1

    .line 691502
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v27

    goto :goto_1

    .line 691503
    :cond_5
    const-string v31, "can_invite_to_campaign"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_6

    .line 691504
    const/4 v8, 0x1

    .line 691505
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v26

    goto :goto_1

    .line 691506
    :cond_6
    const-string v31, "full_width_post_donation_image"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_7

    .line 691507
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v25

    goto :goto_1

    .line 691508
    :cond_7
    const-string v31, "fundraiser_detailed_progress_text"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_8

    .line 691509
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v24

    goto/16 :goto_1

    .line 691510
    :cond_8
    const-string v31, "fundraiser_for_charity_text"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_9

    .line 691511
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v23

    goto/16 :goto_1

    .line 691512
    :cond_9
    const-string v31, "id"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_a

    .line 691513
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v22

    goto/16 :goto_1

    .line 691514
    :cond_a
    const-string v31, "logo_image"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_b

    .line 691515
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v21

    goto/16 :goto_1

    .line 691516
    :cond_b
    const-string v31, "mobile_donate_url"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_c

    .line 691517
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v20

    goto/16 :goto_1

    .line 691518
    :cond_c
    const-string v31, "charity_interface"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_d

    .line 691519
    invoke-static/range {p0 .. p1}, LX/4LB;->a(LX/15w;LX/186;)I

    move-result v19

    goto/16 :goto_1

    .line 691520
    :cond_d
    const-string v31, "all_donations_summary_text"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_e

    .line 691521
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v18

    goto/16 :goto_1

    .line 691522
    :cond_e
    const-string v31, "amount_raised_text"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_f

    .line 691523
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v17

    goto/16 :goto_1

    .line 691524
    :cond_f
    const-string v31, "campaign_goal_text"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_10

    .line 691525
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    goto/16 :goto_1

    .line 691526
    :cond_10
    const-string v31, "days_left_text"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_11

    .line 691527
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v15

    goto/16 :goto_1

    .line 691528
    :cond_11
    const-string v31, "description"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_12

    .line 691529
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    goto/16 :goto_1

    .line 691530
    :cond_12
    const-string v31, "detailed_amount_raised_text"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_13

    .line 691531
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    goto/16 :goto_1

    .line 691532
    :cond_13
    const-string v31, "fundraiser_by_owner_text"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_14

    .line 691533
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v12

    goto/16 :goto_1

    .line 691534
    :cond_14
    const-string v31, "has_viewer_donated"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_15

    .line 691535
    const/4 v5, 0x1

    .line 691536
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v11

    goto/16 :goto_1

    .line 691537
    :cond_15
    const-string v31, "percent_of_goal_reached"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_16

    .line 691538
    const/4 v4, 0x1

    .line 691539
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    goto/16 :goto_1

    .line 691540
    :cond_16
    const-string v31, "total_donated_amount_by_viewer"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_0

    .line 691541
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto/16 :goto_1

    .line 691542
    :cond_17
    const/16 v30, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 691543
    const/16 v30, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v30

    move/from16 v2, v29

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 691544
    const/16 v29, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v29

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 691545
    if-eqz v9, :cond_18

    .line 691546
    const/4 v9, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v9, v1}, LX/186;->a(IZ)V

    .line 691547
    :cond_18
    if-eqz v8, :cond_19

    .line 691548
    const/4 v8, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v8, v1}, LX/186;->a(IZ)V

    .line 691549
    :cond_19
    const/4 v8, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 691550
    const/4 v8, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 691551
    const/4 v8, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 691552
    const/16 v8, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 691553
    const/16 v8, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 691554
    const/16 v8, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 691555
    const/16 v8, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 691556
    const/16 v8, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 691557
    const/16 v8, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 691558
    const/16 v8, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 691559
    const/16 v8, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v15}, LX/186;->b(II)V

    .line 691560
    const/16 v8, 0x13

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v14}, LX/186;->b(II)V

    .line 691561
    const/16 v8, 0x14

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v13}, LX/186;->b(II)V

    .line 691562
    const/16 v8, 0x15

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v12}, LX/186;->b(II)V

    .line 691563
    if-eqz v5, :cond_1a

    .line 691564
    const/16 v5, 0x16

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v11}, LX/186;->a(IZ)V

    .line 691565
    :cond_1a
    if-eqz v4, :cond_1b

    .line 691566
    const/16 v5, 0x17

    const-wide/16 v8, 0x0

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, LX/186;->a(IDD)V

    .line 691567
    :cond_1b
    const/16 v4, 0x18

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v10}, LX/186;->b(II)V

    .line 691568
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v4

    goto/16 :goto_0

    :cond_1c
    move/from16 v33, v6

    move/from16 v34, v7

    move-wide v6, v10

    move v10, v8

    move v11, v9

    move/from16 v9, v34

    move/from16 v8, v33

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    .line 691570
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 691571
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 691572
    if-eqz v0, :cond_0

    .line 691573
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691574
    invoke-static {p0, p1, v1, p2}, LX/2bt;->a(LX/15i;IILX/0nX;)V

    .line 691575
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 691576
    if-eqz v0, :cond_1

    .line 691577
    const-string v1, "campaign_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691578
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 691579
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 691580
    if-eqz v0, :cond_2

    .line 691581
    const-string v1, "can_donate"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691582
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 691583
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 691584
    if-eqz v0, :cond_3

    .line 691585
    const-string v1, "can_invite_to_campaign"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691586
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 691587
    :cond_3
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 691588
    if-eqz v0, :cond_4

    .line 691589
    const-string v1, "full_width_post_donation_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691590
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 691591
    :cond_4
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 691592
    if-eqz v0, :cond_5

    .line 691593
    const-string v1, "fundraiser_detailed_progress_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691594
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 691595
    :cond_5
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 691596
    if-eqz v0, :cond_6

    .line 691597
    const-string v1, "fundraiser_for_charity_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691598
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 691599
    :cond_6
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 691600
    if-eqz v0, :cond_7

    .line 691601
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691602
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 691603
    :cond_7
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 691604
    if-eqz v0, :cond_8

    .line 691605
    const-string v1, "logo_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691606
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 691607
    :cond_8
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 691608
    if-eqz v0, :cond_9

    .line 691609
    const-string v1, "mobile_donate_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691610
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 691611
    :cond_9
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 691612
    if-eqz v0, :cond_a

    .line 691613
    const-string v1, "charity_interface"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691614
    invoke-static {p0, v0, p2, p3}, LX/4LB;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 691615
    :cond_a
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 691616
    if-eqz v0, :cond_b

    .line 691617
    const-string v1, "all_donations_summary_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691618
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 691619
    :cond_b
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 691620
    if-eqz v0, :cond_c

    .line 691621
    const-string v1, "amount_raised_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691622
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 691623
    :cond_c
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 691624
    if-eqz v0, :cond_d

    .line 691625
    const-string v1, "campaign_goal_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691626
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 691627
    :cond_d
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 691628
    if-eqz v0, :cond_e

    .line 691629
    const-string v1, "days_left_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691630
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 691631
    :cond_e
    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 691632
    if-eqz v0, :cond_f

    .line 691633
    const-string v1, "description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691634
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 691635
    :cond_f
    const/16 v0, 0x14

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 691636
    if-eqz v0, :cond_10

    .line 691637
    const-string v1, "detailed_amount_raised_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691638
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 691639
    :cond_10
    const/16 v0, 0x15

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 691640
    if-eqz v0, :cond_11

    .line 691641
    const-string v1, "fundraiser_by_owner_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691642
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 691643
    :cond_11
    const/16 v0, 0x16

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 691644
    if-eqz v0, :cond_12

    .line 691645
    const-string v1, "has_viewer_donated"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691646
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 691647
    :cond_12
    const/16 v0, 0x17

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 691648
    cmpl-double v2, v0, v2

    if-eqz v2, :cond_13

    .line 691649
    const-string v2, "percent_of_goal_reached"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691650
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 691651
    :cond_13
    const/16 v0, 0x18

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 691652
    if-eqz v0, :cond_14

    .line 691653
    const-string v1, "total_donated_amount_by_viewer"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 691654
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 691655
    :cond_14
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 691656
    return-void
.end method
