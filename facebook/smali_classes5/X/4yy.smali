.class public final LX/4yy;
.super LX/0P4;
.source ""

# interfaces
.implements LX/4yx;


# annotations
.annotation build Lcom/google/common/annotations/VisibleForTesting;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LX/0P4",
        "<TK;TV;>;",
        "LX/4yx",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field public nextInValueBucket:LX/4yy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4yy",
            "<TK;TV;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public predecessorInMultimap:LX/4yy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4yy",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public predecessorInValueSet:LX/4yx;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4yx",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public final smearedValueHash:I

.field public successorInMultimap:LX/4yy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4yy",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public successorInValueSet:LX/4yx;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4yx",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Object;Ljava/lang/Object;ILX/4yy;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/4yy;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;I",
            "LX/4yy",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 822111
    invoke-direct {p0, p1, p2}, LX/0P4;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 822112
    iput p3, p0, LX/4yy;->smearedValueHash:I

    .line 822113
    iput-object p4, p0, LX/4yy;->nextInValueBucket:LX/4yy;

    .line 822114
    return-void
.end method


# virtual methods
.method public final a()LX/4yx;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/4yx",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 822110
    iget-object v0, p0, LX/4yy;->predecessorInValueSet:LX/4yx;

    return-object v0
.end method

.method public final a(LX/4yx;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4yx",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 822108
    iput-object p1, p0, LX/4yy;->predecessorInValueSet:LX/4yx;

    .line 822109
    return-void
.end method

.method public final a(Ljava/lang/Object;I)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 822107
    iget v0, p0, LX/4yy;->smearedValueHash:I

    if-ne v0, p2, :cond_0

    invoke-virtual {p0}, LX/0P4;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()LX/4yx;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/4yx",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 822106
    iget-object v0, p0, LX/4yy;->successorInValueSet:LX/4yx;

    return-object v0
.end method

.method public final b(LX/4yx;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4yx",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 822104
    iput-object p1, p0, LX/4yy;->successorInValueSet:LX/4yx;

    .line 822105
    return-void
.end method
