.class public LX/4ou;
.super Landroid/preference/RingtonePreference;
.source ""


# instance fields
.field public final a:LX/2qy;

.field public b:LX/2qx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 810478
    invoke-direct {p0, p1}, Landroid/preference/RingtonePreference;-><init>(Landroid/content/Context;)V

    .line 810479
    const-class v0, LX/4ou;

    invoke-static {v0, p0}, LX/4ou;->a(Ljava/lang/Class;Landroid/preference/Preference;)V

    .line 810480
    iget-object v0, p0, LX/4ou;->b:LX/2qx;

    invoke-virtual {v0, p0}, LX/2qx;->a(Landroid/preference/Preference;)LX/2qy;

    move-result-object v0

    iput-object v0, p0, LX/4ou;->a:LX/2qy;

    .line 810481
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/preference/Preference;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/preference/Preference;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, LX/4ou;

    const-class p0, LX/2qx;

    invoke-interface {v1, p0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/2qx;

    iput-object v1, p1, LX/4ou;->b:LX/2qx;

    return-void
.end method


# virtual methods
.method public final getPersistedString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 810482
    iget-object v0, p0, LX/4ou;->a:LX/2qy;

    invoke-virtual {v0, p1}, LX/2qy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getSharedPreferences()Landroid/content/SharedPreferences;
    .locals 1

    .prologue
    .line 810483
    iget-object v0, p0, LX/4ou;->a:LX/2qy;

    .line 810484
    iget-object p0, v0, LX/2qy;->b:Landroid/content/SharedPreferences;

    move-object v0, p0

    .line 810485
    return-object v0
.end method

.method public final persistString(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 810486
    iget-object v0, p0, LX/4ou;->a:LX/2qy;

    invoke-virtual {v0, p1}, LX/2qy;->b(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
