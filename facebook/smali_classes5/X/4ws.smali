.class public final LX/4ws;
.super LX/305;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/305",
        "<TV;>;"
    }
.end annotation


# instance fields
.field public final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<TV;>;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/4wp;


# direct methods
.method public constructor <init>(LX/4wp;)V
    .locals 1

    .prologue
    .line 820552
    iput-object p1, p0, LX/4ws;->b:LX/4wp;

    invoke-direct {p0}, LX/305;-><init>()V

    .line 820553
    iget-object v0, p0, LX/4ws;->b:LX/4wp;

    iget-object v0, v0, LX/4wp;->a:LX/4wp;

    invoke-virtual {v0}, LX/4wo;->keySet()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, LX/4ws;->a:Ljava/util/Set;

    return-void
.end method


# virtual methods
.method public final a()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 820554
    iget-object v0, p0, LX/4ws;->a:Ljava/util/Set;

    return-object v0
.end method

.method public final synthetic b()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 820555
    invoke-virtual {p0}, LX/4ws;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic e()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 820556
    invoke-virtual {p0}, LX/4ws;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 820557
    iget-object v0, p0, LX/4ws;->b:LX/4wp;

    invoke-virtual {v0}, LX/4wo;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/0PM;->b(Ljava/util/Iterator;)Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public final toArray()[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 820558
    invoke-virtual {p0}, LX/306;->o()[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;)[TT;"
        }
    .end annotation

    .prologue
    .line 820559
    invoke-virtual {p0, p1}, LX/306;->a([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 820560
    invoke-virtual {p0}, LX/306;->m()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
