.class public final LX/3mn;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/3mm;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:LX/1dQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 637272
    invoke-static {}, LX/3mm;->q()LX/3mm;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 637273
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 637274
    const-string v0, "OneButtonFooterComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 637275
    if-ne p0, p1, :cond_1

    .line 637276
    :cond_0
    :goto_0
    return v0

    .line 637277
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 637278
    goto :goto_0

    .line 637279
    :cond_3
    check-cast p1, LX/3mn;

    .line 637280
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 637281
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 637282
    if-eq v2, v3, :cond_0

    .line 637283
    iget-object v2, p0, LX/3mn;->a:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/3mn;->a:Ljava/lang/String;

    iget-object v3, p1, LX/3mn;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 637284
    goto :goto_0

    .line 637285
    :cond_5
    iget-object v2, p1, LX/3mn;->a:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 637286
    :cond_6
    iget-object v2, p0, LX/3mn;->b:LX/1dQ;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/3mn;->b:LX/1dQ;

    iget-object v3, p1, LX/3mn;->b:LX/1dQ;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 637287
    goto :goto_0

    .line 637288
    :cond_7
    iget-object v2, p1, LX/3mn;->b:LX/1dQ;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
