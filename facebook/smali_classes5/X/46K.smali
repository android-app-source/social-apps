.class public LX/46K;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final c:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile h:LX/46K;


# instance fields
.field public a:I

.field public b:Z

.field private final d:LX/16I;

.field private final e:LX/0kb;

.field private final f:LX/0Uh;

.field private final g:Lcom/facebook/common/networkreachability/AndroidReachabilityListener;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 670945
    const-class v0, LX/46K;

    sput-object v0, LX/46K;->c:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/0Or;LX/0Uh;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/16I;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0kb;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 670954
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 670955
    invoke-interface {p1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/16I;

    iput-object v0, p0, LX/46K;->d:LX/16I;

    .line 670956
    invoke-interface {p2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kb;

    iput-object v0, p0, LX/46K;->e:LX/0kb;

    .line 670957
    iput-object p3, p0, LX/46K;->f:LX/0Uh;

    .line 670958
    invoke-virtual {p0}, LX/46K;->a()I

    move-result v0

    iput v0, p0, LX/46K;->a:I

    .line 670959
    iput-boolean v3, p0, LX/46K;->b:Z

    .line 670960
    iget-object v0, p0, LX/46K;->d:LX/16I;

    sget-object v1, LX/1Ed;->CONNECTED:LX/1Ed;

    new-instance v2, Lcom/facebook/common/network/FBReachabilityListener$1;

    invoke-direct {v2, p0}, Lcom/facebook/common/network/FBReachabilityListener$1;-><init>(LX/46K;)V

    invoke-virtual {v0, v1, v2}, LX/16I;->a(LX/1Ed;Ljava/lang/Runnable;)LX/0Yb;

    .line 670961
    iget-object v0, p0, LX/46K;->d:LX/16I;

    sget-object v1, LX/1Ed;->NO_INTERNET:LX/1Ed;

    new-instance v2, Lcom/facebook/common/network/FBReachabilityListener$2;

    invoke-direct {v2, p0}, Lcom/facebook/common/network/FBReachabilityListener$2;-><init>(LX/46K;)V

    invoke-virtual {v0, v1, v2}, LX/16I;->a(LX/1Ed;Ljava/lang/Runnable;)LX/0Yb;

    .line 670962
    iget-object v0, p0, LX/46K;->f:LX/0Uh;

    const/16 v1, 0x350

    invoke-virtual {v0, v1, v3}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 670963
    iget-object v0, p0, LX/46K;->d:LX/16I;

    sget-object v1, LX/1Ed;->CONNECTED:LX/1Ed;

    new-instance v2, LX/46J;

    invoke-direct {v2, p0}, LX/46J;-><init>(LX/46K;)V

    .line 670964
    iget-object v3, v0, LX/16I;->a:LX/0Xl;

    invoke-interface {v3}, LX/0Xl;->a()LX/0YX;

    move-result-object v3

    const-string p1, "com.facebook.common.hardware.ACTION_INET_CONDITION_CHANGED"

    new-instance p2, LX/46M;

    invoke-direct {p2, v0, v1, v2}, LX/46M;-><init>(LX/16I;LX/1Ed;LX/46J;)V

    invoke-interface {v3, p1, p2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v3

    invoke-interface {v3}, LX/0YX;->a()LX/0Yb;

    move-result-object v3

    .line 670965
    invoke-virtual {v3}, LX/0Yb;->b()V

    .line 670966
    :cond_0
    new-instance v0, Lcom/facebook/common/networkreachability/AndroidReachabilityListener;

    invoke-direct {v0, p0}, Lcom/facebook/common/networkreachability/AndroidReachabilityListener;-><init>(LX/46K;)V

    iput-object v0, p0, LX/46K;->g:Lcom/facebook/common/networkreachability/AndroidReachabilityListener;

    .line 670967
    return-void
.end method

.method public static a(LX/0QB;)LX/46K;
    .locals 6

    .prologue
    .line 670968
    sget-object v0, LX/46K;->h:LX/46K;

    if-nez v0, :cond_1

    .line 670969
    const-class v1, LX/46K;

    monitor-enter v1

    .line 670970
    :try_start_0
    sget-object v0, LX/46K;->h:LX/46K;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 670971
    if-eqz v2, :cond_0

    .line 670972
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 670973
    new-instance v4, LX/46K;

    const/16 v3, 0x2cc

    invoke-static {v0, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v3, 0x2ca

    invoke-static {v0, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-direct {v4, v5, p0, v3}, LX/46K;-><init>(LX/0Or;LX/0Or;LX/0Uh;)V

    .line 670974
    move-object v0, v4

    .line 670975
    sput-object v0, LX/46K;->h:LX/46K;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 670976
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 670977
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 670978
    :cond_1
    sget-object v0, LX/46K;->h:LX/46K;

    return-object v0

    .line 670979
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 670980
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static b(LX/46K;)V
    .locals 3

    .prologue
    .line 670949
    iget v0, p0, LX/46K;->a:I

    .line 670950
    invoke-virtual {p0}, LX/46K;->a()I

    move-result v1

    iput v1, p0, LX/46K;->a:I

    .line 670951
    iget v1, p0, LX/46K;->a:I

    if-eq v1, v0, :cond_0

    .line 670952
    iget-object v1, p0, LX/46K;->g:Lcom/facebook/common/networkreachability/AndroidReachabilityListener;

    iget v2, p0, LX/46K;->a:I

    invoke-virtual {v1, v2, v0}, Lcom/facebook/common/networkreachability/AndroidReachabilityListener;->networkStateChanged(II)V

    .line 670953
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 670946
    iget-object v0, p0, LX/46K;->e:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->i()Landroid/net/NetworkInfo;

    move-result-object v0

    if-nez v0, :cond_0

    .line 670947
    const/4 v0, -0x1

    .line 670948
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/46K;->e:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->i()Landroid/net/NetworkInfo;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    goto :goto_0
.end method
