.class public final LX/4wm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Ljava/util/Map$Entry",
        "<TK;TV;>;>;"
    }
.end annotation


# instance fields
.field public a:Ljava/util/Map$Entry;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map$Entry",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public final synthetic b:Ljava/util/Iterator;

.field public final synthetic c:LX/4wn;


# direct methods
.method public constructor <init>(LX/4wn;Ljava/util/Iterator;)V
    .locals 0

    .prologue
    .line 820415
    iput-object p1, p0, LX/4wm;->c:LX/4wn;

    iput-object p2, p0, LX/4wm;->b:Ljava/util/Iterator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final hasNext()Z
    .locals 1

    .prologue
    .line 820416
    iget-object v0, p0, LX/4wm;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    return v0
.end method

.method public final next()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 820417
    iget-object v0, p0, LX/4wm;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    iput-object v0, p0, LX/4wm;->a:Ljava/util/Map$Entry;

    .line 820418
    iget-object v0, p0, LX/4wm;->a:Ljava/util/Map$Entry;

    .line 820419
    new-instance v1, LX/4wl;

    invoke-direct {v1, p0, v0}, LX/4wl;-><init>(LX/4wm;Ljava/util/Map$Entry;)V

    return-object v1
.end method

.method public final remove()V
    .locals 2

    .prologue
    .line 820420
    iget-object v0, p0, LX/4wm;->a:Ljava/util/Map$Entry;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0P6;->a(Z)V

    .line 820421
    iget-object v0, p0, LX/4wm;->a:Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    .line 820422
    iget-object v1, p0, LX/4wm;->b:Ljava/util/Iterator;

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 820423
    iget-object v1, p0, LX/4wm;->c:LX/4wn;

    iget-object v1, v1, LX/4wn;->b:LX/4wp;

    invoke-static {v1, v0}, LX/4wp;->d(LX/4wp;Ljava/lang/Object;)V

    .line 820424
    return-void

    .line 820425
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
