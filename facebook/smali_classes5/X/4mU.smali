.class public LX/4mU;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Landroid/animation/Animator$AnimatorListener;

.field public c:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/4mR;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Z

.field public e:I


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 805899
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 805900
    new-instance v0, LX/4mT;

    invoke-direct {v0, p0}, LX/4mT;-><init>(LX/4mU;)V

    iput-object v0, p0, LX/4mU;->b:Landroid/animation/Animator$AnimatorListener;

    .line 805901
    iput-boolean v1, p0, LX/4mU;->d:Z

    .line 805902
    iput v1, p0, LX/4mU;->e:I

    .line 805903
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/4mU;->a:Ljava/lang/ref/WeakReference;

    .line 805904
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, LX/4mU;->b:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 805905
    return-void
.end method

.method private b()V
    .locals 1

    .prologue
    .line 805964
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4mU;->d:Z

    .line 805965
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 805960
    iget-object v0, p0, LX/4mU;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 805961
    if-nez v0, :cond_0

    .line 805962
    :goto_0
    return-void

    .line 805963
    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    goto :goto_0
.end method

.method public final a(F)V
    .locals 1

    .prologue
    .line 805956
    iget-object v0, p0, LX/4mU;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 805957
    if-nez v0, :cond_0

    .line 805958
    :goto_0
    return-void

    .line 805959
    :cond_0
    invoke-virtual {v0, p1}, Landroid/view/View;->setScaleX(F)V

    goto :goto_0
.end method

.method public final a(FF)V
    .locals 1

    .prologue
    .line 805951
    iget-object v0, p0, LX/4mU;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 805952
    if-nez v0, :cond_0

    .line 805953
    :goto_0
    return-void

    .line 805954
    :cond_0
    invoke-virtual {v0, p1}, Landroid/view/View;->setPivotX(F)V

    .line 805955
    invoke-virtual {v0, p2}, Landroid/view/View;->setPivotY(F)V

    goto :goto_0
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 805947
    iget-object v0, p0, LX/4mU;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 805948
    if-nez v0, :cond_0

    .line 805949
    :goto_0
    return-void

    .line 805950
    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    goto :goto_0
.end method

.method public final a(LX/4mR;)V
    .locals 1
    .param p1    # LX/4mR;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 805943
    if-nez p1, :cond_0

    .line 805944
    const/4 v0, 0x0

    iput-object v0, p0, LX/4mU;->c:Ljava/lang/ref/WeakReference;

    .line 805945
    :goto_0
    return-void

    .line 805946
    :cond_0
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/4mU;->c:Ljava/lang/ref/WeakReference;

    goto :goto_0
.end method

.method public final b(F)V
    .locals 1

    .prologue
    .line 805938
    invoke-direct {p0}, LX/4mU;->b()V

    .line 805939
    iget-object v0, p0, LX/4mU;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 805940
    if-nez v0, :cond_0

    .line 805941
    :goto_0
    return-void

    .line 805942
    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    goto :goto_0
.end method

.method public final c(F)V
    .locals 1

    .prologue
    .line 805966
    iget-object v0, p0, LX/4mU;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 805967
    if-nez v0, :cond_0

    .line 805968
    :goto_0
    return-void

    .line 805969
    :cond_0
    invoke-virtual {v0, p1}, Landroid/view/View;->setScaleY(F)V

    goto :goto_0
.end method

.method public final d(F)V
    .locals 1

    .prologue
    .line 805933
    invoke-direct {p0}, LX/4mU;->b()V

    .line 805934
    iget-object v0, p0, LX/4mU;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 805935
    if-nez v0, :cond_0

    .line 805936
    :goto_0
    return-void

    .line 805937
    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    goto :goto_0
.end method

.method public final e(F)V
    .locals 1

    .prologue
    .line 805929
    iget-object v0, p0, LX/4mU;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 805930
    if-nez v0, :cond_0

    .line 805931
    :goto_0
    return-void

    .line 805932
    :cond_0
    invoke-virtual {v0, p1}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0
.end method

.method public final f(F)V
    .locals 1

    .prologue
    .line 805924
    invoke-direct {p0}, LX/4mU;->b()V

    .line 805925
    iget-object v0, p0, LX/4mU;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 805926
    if-nez v0, :cond_0

    .line 805927
    :goto_0
    return-void

    .line 805928
    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    goto :goto_0
.end method

.method public final g(F)V
    .locals 1

    .prologue
    .line 805920
    iget-object v0, p0, LX/4mU;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 805921
    if-nez v0, :cond_0

    .line 805922
    :goto_0
    return-void

    .line 805923
    :cond_0
    invoke-virtual {v0, p1}, Landroid/view/View;->setTranslationX(F)V

    goto :goto_0
.end method

.method public final h(F)V
    .locals 1

    .prologue
    .line 805915
    invoke-direct {p0}, LX/4mU;->b()V

    .line 805916
    iget-object v0, p0, LX/4mU;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 805917
    if-nez v0, :cond_0

    .line 805918
    :goto_0
    return-void

    .line 805919
    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    goto :goto_0
.end method

.method public final i(F)V
    .locals 1

    .prologue
    .line 805911
    iget-object v0, p0, LX/4mU;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 805912
    if-nez v0, :cond_0

    .line 805913
    :goto_0
    return-void

    .line 805914
    :cond_0
    invoke-virtual {v0, p1}, Landroid/view/View;->setTranslationY(F)V

    goto :goto_0
.end method

.method public final j(F)V
    .locals 1

    .prologue
    .line 805906
    invoke-direct {p0}, LX/4mU;->b()V

    .line 805907
    iget-object v0, p0, LX/4mU;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 805908
    if-nez v0, :cond_0

    .line 805909
    :goto_0
    return-void

    .line 805910
    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    goto :goto_0
.end method
