.class public LX/3iP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;
.implements LX/3iN;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/3iP;


# instance fields
.field private a:LX/3iO;

.field private b:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/3iO;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 629321
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 629322
    iput-object p1, p0, LX/3iP;->a:LX/3iO;

    .line 629323
    iget-object v0, p0, LX/3iP;->a:LX/3iO;

    invoke-virtual {v0, p0}, LX/3iO;->a(LX/3iN;)V

    .line 629324
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/3iP;->b:Ljava/util/LinkedList;

    .line 629325
    return-void
.end method

.method public static a(LX/0QB;)LX/3iP;
    .locals 4

    .prologue
    .line 629283
    sget-object v0, LX/3iP;->c:LX/3iP;

    if-nez v0, :cond_1

    .line 629284
    const-class v1, LX/3iP;

    monitor-enter v1

    .line 629285
    :try_start_0
    sget-object v0, LX/3iP;->c:LX/3iP;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 629286
    if-eqz v2, :cond_0

    .line 629287
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 629288
    new-instance p0, LX/3iP;

    invoke-static {v0}, LX/3iO;->a(LX/0QB;)LX/3iO;

    move-result-object v3

    check-cast v3, LX/3iO;

    invoke-direct {p0, v3}, LX/3iP;-><init>(LX/3iO;)V

    .line 629289
    move-object v0, p0

    .line 629290
    sput-object v0, LX/3iP;->c:LX/3iP;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 629291
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 629292
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 629293
    :cond_1
    sget-object v0, LX/3iP;->c:LX/3iP;

    return-object v0

    .line 629294
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 629295
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private declared-synchronized a()V
    .locals 1

    .prologue
    .line 629318
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/3iP;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 629319
    monitor-exit p0

    return-void

    .line 629320
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;)LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 629312
    monitor-enter p0

    :try_start_0
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 629313
    iget-object v0, p0, LX/3iP;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    .line 629314
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 629315
    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 629316
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 629317
    :cond_1
    :try_start_1
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLComment;)V
    .locals 0

    .prologue
    .line 629310
    invoke-virtual {p0, p1}, LX/3iP;->c(Lcom/facebook/graphql/model/GraphQLComment;)V

    .line 629311
    return-void
.end method

.method public final declared-synchronized b(Lcom/facebook/graphql/model/GraphQLComment;)V
    .locals 2

    .prologue
    .line 629305
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/3iP;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/16 v1, 0xa

    if-lt v0, v1, :cond_0

    .line 629306
    iget-object v0, p0, LX/3iP;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    .line 629307
    :cond_0
    iget-object v0, p0, LX/3iP;->b:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 629308
    monitor-exit p0

    return-void

    .line 629309
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c(Lcom/facebook/graphql/model/GraphQLComment;)V
    .locals 2

    .prologue
    .line 629298
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/3iP;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 629299
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 629300
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    .line 629301
    invoke-virtual {v0, p1}, Lcom/facebook/graphql/model/GraphQLComment;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 629302
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 629303
    :cond_1
    monitor-exit p0

    return-void

    .line 629304
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final clearUserData()V
    .locals 0

    .prologue
    .line 629296
    invoke-direct {p0}, LX/3iP;->a()V

    .line 629297
    return-void
.end method
