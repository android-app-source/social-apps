.class public final LX/4Uj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Bq;


# instance fields
.field public final b:LX/3d2;

.field public final synthetic c:Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

.field public final synthetic d:Ljava/lang/String;

.field public final synthetic e:LX/2di;


# direct methods
.method public constructor <init>(LX/2di;Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 741689
    iput-object p1, p0, LX/4Uj;->e:LX/2di;

    iput-object p2, p0, LX/4Uj;->c:Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    iput-object p3, p0, LX/4Uj;->d:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 741690
    new-instance v0, LX/3d2;

    iget-object v1, p0, LX/4Uj;->c:Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    new-instance v2, LX/4Ui;

    invoke-direct {v2, p0}, LX/4Ui;-><init>(LX/4Uj;)V

    invoke-direct {v0, v1, v2}, LX/3d2;-><init>(Ljava/lang/Class;LX/3d4;)V

    iput-object v0, p0, LX/4Uj;->b:LX/3d2;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Z)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;Z)TT;"
        }
    .end annotation

    .prologue
    .line 741688
    iget-object v0, p0, LX/4Uj;->b:LX/3d2;

    invoke-virtual {v0, p1}, LX/3d2;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 741691
    iget-object v0, p0, LX/4Uj;->d:Ljava/lang/String;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 741687
    const-string v0, "DeprecatedPaginationBridge"

    return-object v0
.end method
