.class public final LX/4dd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1bh;


# annotations
.annotation build Lcom/facebook/common/internal/VisibleForTesting;
.end annotation


# instance fields
.field private final a:LX/1bh;

.field private final b:I


# direct methods
.method public constructor <init>(LX/1bh;I)V
    .locals 0

    .prologue
    .line 796280
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 796281
    iput-object p1, p0, LX/4dd;->a:LX/1bh;

    .line 796282
    iput p2, p0, LX/4dd;->b:I

    .line 796283
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 796270
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Landroid/net/Uri;)Z
    .locals 1

    .prologue
    .line 796279
    iget-object v0, p0, LX/4dd;->a:LX/1bh;

    invoke-interface {v0, p1}, LX/1bh;->a(Landroid/net/Uri;)Z

    move-result v0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 796273
    if-ne p1, p0, :cond_1

    .line 796274
    :cond_0
    :goto_0
    return v0

    .line 796275
    :cond_1
    instance-of v2, p1, LX/4dd;

    if-eqz v2, :cond_3

    .line 796276
    check-cast p1, LX/4dd;

    .line 796277
    iget-object v2, p0, LX/4dd;->a:LX/1bh;

    iget-object v3, p1, LX/4dd;->a:LX/1bh;

    if-ne v2, v3, :cond_2

    iget v2, p0, LX/4dd;->b:I

    iget v3, p1, LX/4dd;->b:I

    if-eq v2, v3, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 796278
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 796272
    iget-object v0, p0, LX/4dd;->a:LX/1bh;

    invoke-interface {v0}, LX/1bh;->hashCode()I

    move-result v0

    mul-int/lit16 v0, v0, 0x3f5

    iget v1, p0, LX/4dd;->b:I

    add-int/2addr v0, v1

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 796271
    invoke-static {p0}, LX/15f;->a(Ljava/lang/Object;)LX/15g;

    move-result-object v0

    const-string v1, "imageCacheKey"

    iget-object v2, p0, LX/4dd;->a:LX/1bh;

    invoke-virtual {v0, v1, v2}, LX/15g;->a(Ljava/lang/String;Ljava/lang/Object;)LX/15g;

    move-result-object v0

    const-string v1, "frameIndex"

    iget v2, p0, LX/4dd;->b:I

    invoke-virtual {v0, v1, v2}, LX/15g;->a(Ljava/lang/String;I)LX/15g;

    move-result-object v0

    invoke-virtual {v0}, LX/15g;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
