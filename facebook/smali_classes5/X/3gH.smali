.class public LX/3gH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Ljava/lang/Void;",
        "LX/3kP;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 624253
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 624254
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 624255
    new-instance v0, LX/14N;

    const-string v1, "getGlobalKillSwitchForContactsUpload"

    const-string v2, "GET"

    const-string v3, "me/contactsmessengersync"

    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    sget-object v5, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 624256
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 624257
    const-string v1, "enabled"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 624258
    new-instance v1, LX/3kP;

    invoke-static {v0}, LX/16N;->g(LX/0lF;)Z

    move-result v0

    invoke-direct {v1, v0}, LX/3kP;-><init>(Z)V

    return-object v1
.end method
