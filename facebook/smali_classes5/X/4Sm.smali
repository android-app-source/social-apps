.class public LX/4Sm;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 715596
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 12

    .prologue
    .line 715597
    const/4 v8, 0x0

    .line 715598
    const-wide/16 v6, 0x0

    .line 715599
    const/4 v5, 0x0

    .line 715600
    const/4 v4, 0x0

    .line 715601
    const-wide/16 v2, 0x0

    .line 715602
    const/4 v1, 0x0

    .line 715603
    const/4 v0, 0x0

    .line 715604
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->START_OBJECT:LX/15z;

    if-eq v9, v10, :cond_a

    .line 715605
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 715606
    const/4 v0, 0x0

    .line 715607
    :goto_0
    return v0

    .line 715608
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v0

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v0, v4, :cond_7

    .line 715609
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v0

    .line 715610
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 715611
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_0

    if-eqz v0, :cond_0

    .line 715612
    const-string v4, "candidates"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 715613
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 715614
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->START_ARRAY:LX/15z;

    if-ne v4, v5, :cond_1

    .line 715615
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_ARRAY:LX/15z;

    if-eq v4, v5, :cond_1

    .line 715616
    invoke-static {p0, p1}, LX/4Si;->b(LX/15w;LX/186;)I

    move-result v4

    .line 715617
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 715618
    :cond_1
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    move v0, v0

    .line 715619
    move v11, v0

    goto :goto_1

    .line 715620
    :cond_2
    const-string v4, "polling_percentage"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 715621
    const/4 v0, 0x1

    .line 715622
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v2

    move v1, v0

    goto :goto_1

    .line 715623
    :cond_3
    const-string v4, "race_name"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 715624
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    move v10, v0

    goto :goto_1

    .line 715625
    :cond_4
    const-string v4, "state_postal"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 715626
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    move v7, v0

    goto :goto_1

    .line 715627
    :cond_5
    const-string v4, "total_electoral_votes"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 715628
    const/4 v0, 0x1

    .line 715629
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v4

    move v6, v0

    move-wide v8, v4

    goto/16 :goto_1

    .line 715630
    :cond_6
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 715631
    :cond_7
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 715632
    const/4 v0, 0x0

    invoke-virtual {p1, v0, v11}, LX/186;->b(II)V

    .line 715633
    if-eqz v1, :cond_8

    .line 715634
    const/4 v1, 0x1

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 715635
    :cond_8
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 715636
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 715637
    if-eqz v6, :cond_9

    .line 715638
    const/4 v1, 0x4

    const-wide/16 v4, 0x0

    move-object v0, p1

    move-wide v2, v8

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 715639
    :cond_9
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :cond_a
    move v10, v5

    move v11, v8

    move-wide v8, v2

    move-wide v2, v6

    move v6, v0

    move v7, v4

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 715640
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 715641
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 715642
    if-eqz v0, :cond_1

    .line 715643
    const-string v1, "candidates"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 715644
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 715645
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 715646
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2}, LX/4Si;->a(LX/15i;ILX/0nX;)V

    .line 715647
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 715648
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 715649
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 715650
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_2

    .line 715651
    const-string v2, "polling_percentage"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 715652
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 715653
    :cond_2
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 715654
    if-eqz v0, :cond_3

    .line 715655
    const-string v1, "race_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 715656
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 715657
    :cond_3
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 715658
    if-eqz v0, :cond_4

    .line 715659
    const-string v1, "state_postal"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 715660
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 715661
    :cond_4
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 715662
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_5

    .line 715663
    const-string v2, "total_electoral_votes"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 715664
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 715665
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 715666
    return-void
.end method
