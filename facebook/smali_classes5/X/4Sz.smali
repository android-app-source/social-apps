.class public LX/4Sz;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 716182
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 12

    .prologue
    const/4 v6, 0x1

    const-wide/16 v4, 0x0

    const/4 v1, 0x0

    .line 716198
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_6

    .line 716199
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 716200
    :goto_0
    return v1

    .line 716201
    :cond_0
    const-string v10, "threshold"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 716202
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v2

    move v0, v6

    .line 716203
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_4

    .line 716204
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 716205
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 716206
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_1

    if-eqz v9, :cond_1

    .line 716207
    const-string v10, "model"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 716208
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 716209
    :cond_2
    const-string v10, "model_id"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 716210
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 716211
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 716212
    :cond_4
    const/4 v9, 0x3

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 716213
    invoke-virtual {p1, v1, v8}, LX/186;->b(II)V

    .line 716214
    invoke-virtual {p1, v6, v7}, LX/186;->b(II)V

    .line 716215
    if-eqz v0, :cond_5

    .line 716216
    const/4 v1, 0x2

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 716217
    :cond_5
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move v0, v1

    move-wide v2, v4

    move v7, v1

    move v8, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 716183
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 716184
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 716185
    if-eqz v0, :cond_0

    .line 716186
    const-string v1, "model"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 716187
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 716188
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 716189
    if-eqz v0, :cond_1

    .line 716190
    const-string v1, "model_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 716191
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 716192
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 716193
    cmpl-double v2, v0, v2

    if-eqz v2, :cond_2

    .line 716194
    const-string v2, "threshold"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 716195
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 716196
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 716197
    return-void
.end method
