.class public LX/3Yv;
.super Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;
.source ""

# interfaces
.implements LX/2eZ;


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "LX/3Yv;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:Z

.field private c:Landroid/view/ViewStub;

.field private d:Landroid/view/ViewStub;

.field public e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public f:Lcom/facebook/feedplugins/saved/rows/ui/SavedCollectionInlineVideoView;

.field public g:Landroid/widget/TextView;

.field public h:Landroid/widget/TextView;

.field public i:Landroid/widget/TextView;

.field private j:Landroid/view/ViewGroup;

.field private k:Landroid/view/ViewGroup;

.field private l:Landroid/view/View;

.field public m:Lcom/facebook/attachments/angora/actionbutton/SaveButton;

.field public final n:Landroid/view/View;

.field private o:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 597548
    new-instance v0, LX/3Yw;

    invoke-direct {v0}, LX/3Yw;-><init>()V

    sput-object v0, LX/3Yv;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 597616
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/3Yv;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 597617
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 597602
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 597603
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/3Yv;->o:Z

    .line 597604
    const v0, 0x7f031255

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 597605
    const v0, 0x7f0d2b22

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, LX/3Yv;->c:Landroid/view/ViewStub;

    .line 597606
    const v0, 0x7f0d2b25

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, LX/3Yv;->d:Landroid/view/ViewStub;

    .line 597607
    const v0, 0x7f0d2b2a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/3Yv;->g:Landroid/widget/TextView;

    .line 597608
    const v0, 0x7f0d2b2b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/3Yv;->h:Landroid/widget/TextView;

    .line 597609
    const v0, 0x7f0d2b2c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/3Yv;->i:Landroid/widget/TextView;

    .line 597610
    const v0, 0x7f0d2b28

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, LX/3Yv;->j:Landroid/view/ViewGroup;

    .line 597611
    const v0, 0x7f0d2b27

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, LX/3Yv;->k:Landroid/view/ViewGroup;

    .line 597612
    const v0, 0x7f0d2b29

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/3Yv;->l:Landroid/view/View;

    .line 597613
    const v0, 0x7f0d2b2d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/attachments/angora/actionbutton/SaveButton;

    iput-object v0, p0, LX/3Yv;->m:Lcom/facebook/attachments/angora/actionbutton/SaveButton;

    .line 597614
    const v0, 0x7f0d2b24

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/3Yv;->n:Landroid/view/View;

    .line 597615
    return-void
.end method

.method public static b(LX/3Yv;)V
    .locals 2

    .prologue
    .line 597598
    iget-object v0, p0, LX/3Yv;->i:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/3Yv;->h:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 597599
    :cond_0
    iget-object v0, p0, LX/3Yv;->g:Landroid/widget/TextView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 597600
    :goto_0
    return-void

    .line 597601
    :cond_1
    iget-object v0, p0, LX/3Yv;->g:Landroid/widget/TextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxLines(I)V

    goto :goto_0
.end method

.method public static e(LX/3Yv;)V
    .locals 1

    .prologue
    .line 597594
    iget-object v0, p0, LX/3Yv;->f:Lcom/facebook/feedplugins/saved/rows/ui/SavedCollectionInlineVideoView;

    if-nez v0, :cond_0

    .line 597595
    iget-object v0, p0, LX/3Yv;->d:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/saved/rows/ui/SavedCollectionInlineVideoView;

    iput-object v0, p0, LX/3Yv;->f:Lcom/facebook/feedplugins/saved/rows/ui/SavedCollectionInlineVideoView;

    .line 597596
    :cond_0
    iget-boolean v0, p0, LX/3Yv;->o:Z

    invoke-virtual {p0, v0}, LX/3Yv;->setFullWidth(Z)V

    .line 597597
    return-void
.end method

.method private f()V
    .locals 3

    .prologue
    .line 597589
    iget-object v0, p0, LX/3Yv;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-nez v0, :cond_0

    .line 597590
    iget-object v0, p0, LX/3Yv;->c:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/3Yv;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 597591
    iget-object v0, p0, LX/3Yv;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    new-instance v1, LX/1Uo;

    invoke-virtual {p0}, LX/3Yv;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    invoke-virtual {v1}, LX/1Uo;->u()LX/1af;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 597592
    :cond_0
    iget-boolean v0, p0, LX/3Yv;->o:Z

    invoke-virtual {p0, v0}, LX/3Yv;->setFullWidth(Z)V

    .line 597593
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 597588
    iget-boolean v0, p0, LX/3Yv;->b:Z

    return v0
.end method

.method public getInlineVideoView()Lcom/facebook/feedplugins/saved/rows/ui/SavedCollectionInlineVideoView;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 597587
    iget-object v0, p0, LX/3Yv;->f:Lcom/facebook/feedplugins/saved/rows/ui/SavedCollectionInlineVideoView;

    return-object v0
.end method

.method public getMainImageView()Lcom/facebook/drawee/view/DraweeView;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 597586
    iget-object v0, p0, LX/3Yv;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    return-object v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x29a662de

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 597582
    invoke-super {p0}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;->onAttachedToWindow()V

    .line 597583
    const/4 v1, 0x1

    .line 597584
    iput-boolean v1, p0, LX/3Yv;->b:Z

    .line 597585
    const/16 v1, 0x2d

    const v2, 0x65709a0e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x4bdc218a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 597578
    invoke-super {p0}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;->onDetachedFromWindow()V

    .line 597579
    const/4 v1, 0x0

    .line 597580
    iput-boolean v1, p0, LX/3Yv;->b:Z

    .line 597581
    const/16 v1, 0x2d

    const v2, 0x486baac4

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setFullWidth(Z)V
    .locals 6

    .prologue
    .line 597565
    iput-boolean p1, p0, LX/3Yv;->o:Z

    .line 597566
    iget-boolean v0, p0, LX/3Yv;->o:Z

    if-eqz v0, :cond_2

    .line 597567
    invoke-virtual {p0}, LX/3Yv;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 597568
    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 597569
    int-to-double v2, v1

    const-wide/high16 v4, 0x3ff8000000000000L    # 1.5

    div-double/2addr v2, v4

    double-to-int v0, v2

    .line 597570
    :goto_0
    iget-object v2, p0, LX/3Yv;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-eqz v2, :cond_0

    .line 597571
    iget-object v2, p0, LX/3Yv;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v3, v1, v0}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 597572
    :cond_0
    iget-object v2, p0, LX/3Yv;->f:Lcom/facebook/feedplugins/saved/rows/ui/SavedCollectionInlineVideoView;

    if-eqz v2, :cond_1

    .line 597573
    iget-object v2, p0, LX/3Yv;->f:Lcom/facebook/feedplugins/saved/rows/ui/SavedCollectionInlineVideoView;

    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v3, v1, v0}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, Lcom/facebook/feedplugins/saved/rows/ui/SavedCollectionInlineVideoView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 597574
    :cond_1
    iget-object v0, p0, LX/3Yv;->k:Landroid/view/ViewGroup;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    invoke-virtual {p0}, LX/3Yv;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0928

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 597575
    return-void

    .line 597576
    :cond_2
    invoke-virtual {p0}, LX/3Yv;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0923

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 597577
    invoke-virtual {p0}, LX/3Yv;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b0923

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    goto :goto_0
.end method

.method public setMainImageController(LX/1aZ;)V
    .locals 2
    .param p1    # LX/1aZ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 597559
    invoke-direct {p0}, LX/3Yv;->f()V

    .line 597560
    iget-object v0, p0, LX/3Yv;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 597561
    iget-object v0, p0, LX/3Yv;->f:Lcom/facebook/feedplugins/saved/rows/ui/SavedCollectionInlineVideoView;

    if-eqz v0, :cond_0

    .line 597562
    iget-object v0, p0, LX/3Yv;->f:Lcom/facebook/feedplugins/saved/rows/ui/SavedCollectionInlineVideoView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/feedplugins/saved/rows/ui/SavedCollectionInlineVideoView;->setVisibility(I)V

    .line 597563
    :cond_0
    iget-object v0, p0, LX/3Yv;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 597564
    return-void
.end method

.method public setMainImageOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1    # Landroid/view/View$OnClickListener;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 597554
    if-eqz p1, :cond_0

    .line 597555
    invoke-direct {p0}, LX/3Yv;->f()V

    .line 597556
    :cond_0
    iget-object v0, p0, LX/3Yv;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-eqz v0, :cond_1

    .line 597557
    iget-object v0, p0, LX/3Yv;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 597558
    :cond_1
    return-void
.end method

.method public setSaveButtonVisibility(I)V
    .locals 1

    .prologue
    .line 597551
    iget-object v0, p0, LX/3Yv;->l:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 597552
    iget-object v0, p0, LX/3Yv;->m:Lcom/facebook/attachments/angora/actionbutton/SaveButton;

    invoke-virtual {v0, p1}, Lcom/facebook/attachments/angora/actionbutton/SaveButton;->setVisibility(I)V

    .line 597553
    return-void
.end method

.method public setTextContainerOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1    # Landroid/view/View$OnClickListener;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 597549
    iget-object v0, p0, LX/3Yv;->j:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 597550
    return-void
.end method
