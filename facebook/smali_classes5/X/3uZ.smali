.class public LX/3uZ;
.super LX/3u1;
.source ""

# interfaces
.implements LX/3uY;


# static fields
.field public static final synthetic h:Z

.field private static final i:Z


# instance fields
.field public A:I

.field public B:Z

.field private C:I

.field public D:Z

.field public E:Z

.field public F:Z

.field public G:Z

.field private H:Z

.field public I:LX/3uk;

.field private J:Z

.field public a:LX/3uW;

.field public b:LX/3uV;

.field public c:LX/3uG;

.field public d:Z

.field public final e:LX/3oQ;

.field public final f:LX/3oQ;

.field public final g:LX/3sb;

.field public j:Landroid/content/Context;

.field private k:Landroid/content/Context;

.field private l:Landroid/app/Activity;

.field private m:Landroid/app/Dialog;

.field public n:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

.field public o:Landroid/support/v7/internal/widget/ActionBarContainer;

.field public p:LX/3vk;

.field public q:Landroid/support/v7/internal/widget/ActionBarContextView;

.field public r:Landroid/support/v7/internal/widget/ActionBarContainer;

.field public s:Landroid/view/View;

.field public t:LX/3vu;

.field private u:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/3uX;",
            ">;"
        }
    .end annotation
.end field

.field public v:LX/3uX;

.field private w:I

.field private x:Z

.field private y:Z

.field private z:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 648733
    const-class v0, LX/3uZ;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, LX/3uZ;->h:Z

    .line 648734
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xe

    if-lt v0, v3, :cond_1

    :goto_1
    sput-boolean v1, LX/3uZ;->i:Z

    return-void

    :cond_0
    move v0, v2

    .line 648735
    goto :goto_0

    :cond_1
    move v1, v2

    .line 648736
    goto :goto_1
.end method

.method public constructor <init>(Landroid/app/Activity;Z)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 648737
    invoke-direct {p0}, LX/3u1;-><init>()V

    .line 648738
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/3uZ;->u:Ljava/util/ArrayList;

    .line 648739
    const/4 v0, -0x1

    iput v0, p0, LX/3uZ;->w:I

    .line 648740
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/3uZ;->z:Ljava/util/ArrayList;

    .line 648741
    const/4 v0, 0x0

    iput v0, p0, LX/3uZ;->C:I

    .line 648742
    iput-boolean v1, p0, LX/3uZ;->D:Z

    .line 648743
    iput-boolean v1, p0, LX/3uZ;->H:Z

    .line 648744
    new-instance v0, LX/3uS;

    invoke-direct {v0, p0}, LX/3uS;-><init>(LX/3uZ;)V

    iput-object v0, p0, LX/3uZ;->e:LX/3oQ;

    .line 648745
    new-instance v0, LX/3uT;

    invoke-direct {v0, p0}, LX/3uT;-><init>(LX/3uZ;)V

    iput-object v0, p0, LX/3uZ;->f:LX/3oQ;

    .line 648746
    new-instance v0, LX/3uU;

    invoke-direct {v0, p0}, LX/3uU;-><init>(LX/3uZ;)V

    iput-object v0, p0, LX/3uZ;->g:LX/3sb;

    .line 648747
    iput-object p1, p0, LX/3uZ;->l:Landroid/app/Activity;

    .line 648748
    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 648749
    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    .line 648750
    invoke-direct {p0, v0}, LX/3uZ;->b(Landroid/view/View;)V

    .line 648751
    if-nez p2, :cond_0

    .line 648752
    const v1, 0x1020002

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/3uZ;->s:Landroid/view/View;

    .line 648753
    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/app/Dialog;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 648754
    invoke-direct {p0}, LX/3u1;-><init>()V

    .line 648755
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/3uZ;->u:Ljava/util/ArrayList;

    .line 648756
    const/4 v0, -0x1

    iput v0, p0, LX/3uZ;->w:I

    .line 648757
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/3uZ;->z:Ljava/util/ArrayList;

    .line 648758
    const/4 v0, 0x0

    iput v0, p0, LX/3uZ;->C:I

    .line 648759
    iput-boolean v1, p0, LX/3uZ;->D:Z

    .line 648760
    iput-boolean v1, p0, LX/3uZ;->H:Z

    .line 648761
    new-instance v0, LX/3uS;

    invoke-direct {v0, p0}, LX/3uS;-><init>(LX/3uZ;)V

    iput-object v0, p0, LX/3uZ;->e:LX/3oQ;

    .line 648762
    new-instance v0, LX/3uT;

    invoke-direct {v0, p0}, LX/3uT;-><init>(LX/3uZ;)V

    iput-object v0, p0, LX/3uZ;->f:LX/3oQ;

    .line 648763
    new-instance v0, LX/3uU;

    invoke-direct {v0, p0}, LX/3uU;-><init>(LX/3uZ;)V

    iput-object v0, p0, LX/3uZ;->g:LX/3sb;

    .line 648764
    iput-object p1, p0, LX/3uZ;->m:Landroid/app/Dialog;

    .line 648765
    invoke-virtual {p1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, LX/3uZ;->b(Landroid/view/View;)V

    .line 648766
    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 648767
    invoke-direct {p0}, LX/3u1;-><init>()V

    .line 648768
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/3uZ;->u:Ljava/util/ArrayList;

    .line 648769
    const/4 v0, -0x1

    iput v0, p0, LX/3uZ;->w:I

    .line 648770
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/3uZ;->z:Ljava/util/ArrayList;

    .line 648771
    const/4 v0, 0x0

    iput v0, p0, LX/3uZ;->C:I

    .line 648772
    iput-boolean v1, p0, LX/3uZ;->D:Z

    .line 648773
    iput-boolean v1, p0, LX/3uZ;->H:Z

    .line 648774
    new-instance v0, LX/3uS;

    invoke-direct {v0, p0}, LX/3uS;-><init>(LX/3uZ;)V

    iput-object v0, p0, LX/3uZ;->e:LX/3oQ;

    .line 648775
    new-instance v0, LX/3uT;

    invoke-direct {v0, p0}, LX/3uT;-><init>(LX/3uZ;)V

    iput-object v0, p0, LX/3uZ;->f:LX/3oQ;

    .line 648776
    new-instance v0, LX/3uU;

    invoke-direct {v0, p0}, LX/3uU;-><init>(LX/3uZ;)V

    iput-object v0, p0, LX/3uZ;->g:LX/3sb;

    .line 648777
    sget-boolean v0, LX/3uZ;->h:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 648778
    :cond_0
    invoke-direct {p0, p1}, LX/3uZ;->b(Landroid/view/View;)V

    .line 648779
    return-void
.end method

.method private b(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 648780
    const v0, 0x7f0d0349

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    iput-object v0, p0, LX/3uZ;->n:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    .line 648781
    iget-object v0, p0, LX/3uZ;->n:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    if-eqz v0, :cond_0

    .line 648782
    iget-object v0, p0, LX/3uZ;->n:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    invoke-virtual {v0, p0}, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->setActionBarVisibilityCallback(LX/3uY;)V

    .line 648783
    :cond_0
    const v0, 0x7f0d034b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, LX/3uZ;->c(Landroid/view/View;)LX/3vk;

    move-result-object v0

    iput-object v0, p0, LX/3uZ;->p:LX/3vk;

    .line 648784
    const v0, 0x7f0d034c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ActionBarContextView;

    iput-object v0, p0, LX/3uZ;->q:Landroid/support/v7/internal/widget/ActionBarContextView;

    .line 648785
    const v0, 0x7f0d034a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ActionBarContainer;

    iput-object v0, p0, LX/3uZ;->o:Landroid/support/v7/internal/widget/ActionBarContainer;

    .line 648786
    const v0, 0x7f0d0004

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ActionBarContainer;

    iput-object v0, p0, LX/3uZ;->r:Landroid/support/v7/internal/widget/ActionBarContainer;

    .line 648787
    iget-object v0, p0, LX/3uZ;->p:LX/3vk;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/3uZ;->q:Landroid/support/v7/internal/widget/ActionBarContextView;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/3uZ;->o:Landroid/support/v7/internal/widget/ActionBarContainer;

    if-nez v0, :cond_2

    .line 648788
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " can only be used with a compatible window decor layout"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 648789
    :cond_2
    iget-object v0, p0, LX/3uZ;->p:LX/3vk;

    invoke-interface {v0}, LX/3vk;->b()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, LX/3uZ;->j:Landroid/content/Context;

    .line 648790
    iget-object v0, p0, LX/3uZ;->p:LX/3vk;

    invoke-interface {v0}, LX/3vk;->c()Z

    move-result v0

    if-eqz v0, :cond_7

    move v0, v1

    :goto_0
    iput v0, p0, LX/3uZ;->A:I

    .line 648791
    iget-object v0, p0, LX/3uZ;->p:LX/3vk;

    invoke-interface {v0}, LX/3vk;->n()I

    move-result v0

    .line 648792
    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_8

    move v0, v1

    .line 648793
    :goto_1
    if-eqz v0, :cond_3

    .line 648794
    iput-boolean v1, p0, LX/3uZ;->x:Z

    .line 648795
    :cond_3
    iget-object v0, p0, LX/3uZ;->j:Landroid/content/Context;

    invoke-static {v0}, LX/3ub;->a(Landroid/content/Context;)LX/3ub;

    move-result-object v0

    .line 648796
    iget-object v3, v0, LX/3ub;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    iget v3, v3, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    const/16 v4, 0xe

    if-ge v3, v4, :cond_9

    .line 648797
    :goto_2
    invoke-virtual {v0}, LX/3ub;->d()Z

    move-result v0

    const/4 v6, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 648798
    iput-boolean v0, p0, LX/3uZ;->B:Z

    .line 648799
    iget-boolean v3, p0, LX/3uZ;->B:Z

    if-nez v3, :cond_a

    .line 648800
    iget-object v3, p0, LX/3uZ;->p:LX/3vk;

    invoke-interface {v3, v6}, LX/3vk;->a(LX/3vu;)V

    .line 648801
    iget-object v3, p0, LX/3uZ;->o:Landroid/support/v7/internal/widget/ActionBarContainer;

    iget-object v6, p0, LX/3uZ;->t:LX/3vu;

    invoke-virtual {v3, v6}, Landroid/support/v7/internal/widget/ActionBarContainer;->setTabContainer(LX/3vu;)V

    .line 648802
    :goto_3
    invoke-static {p0}, LX/3uZ;->n(LX/3uZ;)I

    move-result v3

    const/4 v6, 0x2

    if-ne v3, v6, :cond_b

    move v3, v4

    .line 648803
    :goto_4
    iget-object v6, p0, LX/3uZ;->t:LX/3vu;

    if-eqz v6, :cond_4

    .line 648804
    if-eqz v3, :cond_c

    .line 648805
    iget-object v6, p0, LX/3uZ;->t:LX/3vu;

    invoke-virtual {v6, v5}, LX/3vu;->setVisibility(I)V

    .line 648806
    iget-object v6, p0, LX/3uZ;->n:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    if-eqz v6, :cond_4

    .line 648807
    iget-object v6, p0, LX/3uZ;->n:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    invoke-static {v6}, LX/0vv;->z(Landroid/view/View;)V

    .line 648808
    :cond_4
    :goto_5
    iget-object p1, p0, LX/3uZ;->p:LX/3vk;

    iget-boolean v6, p0, LX/3uZ;->B:Z

    if-nez v6, :cond_d

    if-eqz v3, :cond_d

    move v6, v4

    :goto_6
    invoke-interface {p1, v6}, LX/3vk;->a(Z)V

    .line 648809
    iget-object v6, p0, LX/3uZ;->n:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    iget-boolean p1, p0, LX/3uZ;->B:Z

    if-nez p1, :cond_e

    if-eqz v3, :cond_e

    .line 648810
    :goto_7
    iput-boolean v4, v6, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->k:Z

    .line 648811
    iget-object v0, p0, LX/3uZ;->j:Landroid/content/Context;

    const/4 v3, 0x0

    sget-object v4, LX/03r;->ActionBar:[I

    const v5, 0x7f010011

    invoke-virtual {v0, v3, v4, v5, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 648812
    const/16 v3, 0x11

    invoke-virtual {v0, v3, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 648813
    invoke-virtual {p0, v1}, LX/3u1;->c(Z)V

    .line 648814
    :cond_5
    const/16 v1, 0x16

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    .line 648815
    if-eqz v1, :cond_6

    .line 648816
    int-to-float v1, v1

    invoke-virtual {p0, v1}, LX/3u1;->a(F)V

    .line 648817
    :cond_6
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 648818
    return-void

    :cond_7
    move v0, v2

    .line 648819
    goto/16 :goto_0

    :cond_8
    move v0, v2

    .line 648820
    goto/16 :goto_1

    :cond_9
    goto :goto_2

    .line 648821
    :cond_a
    iget-object v3, p0, LX/3uZ;->o:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v3, v6}, Landroid/support/v7/internal/widget/ActionBarContainer;->setTabContainer(LX/3vu;)V

    .line 648822
    iget-object v3, p0, LX/3uZ;->p:LX/3vk;

    iget-object v6, p0, LX/3uZ;->t:LX/3vu;

    invoke-interface {v3, v6}, LX/3vk;->a(LX/3vu;)V

    goto :goto_3

    :cond_b
    move v3, v5

    .line 648823
    goto :goto_4

    .line 648824
    :cond_c
    iget-object v6, p0, LX/3uZ;->t:LX/3vu;

    const/16 p1, 0x8

    invoke-virtual {v6, p1}, LX/3vu;->setVisibility(I)V

    goto :goto_5

    :cond_d
    move v6, v5

    .line 648825
    goto :goto_6

    :cond_e
    move v4, v5

    .line 648826
    goto :goto_7
.end method

.method public static b(ZZZ)Z
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 648827
    if-eqz p2, :cond_1

    .line 648828
    :cond_0
    :goto_0
    return v0

    .line 648829
    :cond_1
    if-nez p0, :cond_2

    if-eqz p1, :cond_0

    .line 648830
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c(Landroid/view/View;)LX/3vk;
    .locals 3

    .prologue
    .line 648831
    instance-of v0, p0, LX/3vk;

    if-eqz v0, :cond_0

    .line 648832
    check-cast p0, LX/3vk;

    .line 648833
    :goto_0
    return-object p0

    .line 648834
    :cond_0
    instance-of v0, p0, Landroid/support/v7/widget/Toolbar;

    if-eqz v0, :cond_1

    .line 648835
    check-cast p0, Landroid/support/v7/widget/Toolbar;

    invoke-virtual {p0}, Landroid/support/v7/widget/Toolbar;->getWrapper()LX/3vk;

    move-result-object p0

    goto :goto_0

    .line 648836
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can\'t make a decor toolbar out of "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private f(I)V
    .locals 2

    .prologue
    .line 648837
    iget-object v0, p0, LX/3uZ;->p:LX/3vk;

    invoke-interface {v0}, LX/3vk;->o()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 648838
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "setSelectedNavigationIndex not valid for current navigation mode"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 648839
    :pswitch_0
    iget-object v0, p0, LX/3uZ;->u:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3u0;

    invoke-virtual {p0, v0}, LX/3uZ;->a(LX/3u0;)V

    .line 648840
    :goto_0
    return-void

    .line 648841
    :pswitch_1
    iget-object v0, p0, LX/3uZ;->p:LX/3vk;

    invoke-interface {v0, p1}, LX/3vk;->e(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static i(LX/3uZ;Z)V
    .locals 3

    .prologue
    .line 648842
    iget-boolean v0, p0, LX/3uZ;->E:Z

    iget-boolean v1, p0, LX/3uZ;->F:Z

    iget-boolean v2, p0, LX/3uZ;->G:Z

    invoke-static {v0, v1, v2}, LX/3uZ;->b(ZZZ)Z

    move-result v0

    .line 648843
    if-eqz v0, :cond_1

    .line 648844
    iget-boolean v0, p0, LX/3uZ;->H:Z

    if-nez v0, :cond_0

    .line 648845
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/3uZ;->H:Z

    .line 648846
    invoke-direct {p0, p1}, LX/3uZ;->j(Z)V

    .line 648847
    :cond_0
    :goto_0
    return-void

    .line 648848
    :cond_1
    iget-boolean v0, p0, LX/3uZ;->H:Z

    if-eqz v0, :cond_0

    .line 648849
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/3uZ;->H:Z

    .line 648850
    invoke-direct {p0, p1}, LX/3uZ;->k(Z)V

    goto :goto_0
.end method

.method private j(Z)V
    .locals 7

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 648851
    iget-object v0, p0, LX/3uZ;->I:LX/3uk;

    if-eqz v0, :cond_0

    .line 648852
    iget-object v0, p0, LX/3uZ;->I:LX/3uk;

    invoke-virtual {v0}, LX/3uk;->b()V

    .line 648853
    :cond_0
    iget-object v0, p0, LX/3uZ;->o:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0, v5}, Landroid/support/v7/internal/widget/ActionBarContainer;->setVisibility(I)V

    .line 648854
    iget v0, p0, LX/3uZ;->C:I

    if-nez v0, :cond_6

    sget-boolean v0, LX/3uZ;->i:Z

    if-eqz v0, :cond_6

    iget-boolean v0, p0, LX/3uZ;->J:Z

    if-nez v0, :cond_1

    if-eqz p1, :cond_6

    .line 648855
    :cond_1
    iget-object v0, p0, LX/3uZ;->o:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v0, v4}, LX/0vv;->b(Landroid/view/View;F)V

    .line 648856
    iget-object v0, p0, LX/3uZ;->o:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContainer;->getHeight()I

    move-result v0

    neg-int v0, v0

    int-to-float v0, v0

    .line 648857
    if-eqz p1, :cond_2

    .line 648858
    const/4 v1, 0x2

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    .line 648859
    iget-object v2, p0, LX/3uZ;->o:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v2, v1}, Landroid/support/v7/internal/widget/ActionBarContainer;->getLocationInWindow([I)V

    .line 648860
    aget v1, v1, v6

    int-to-float v1, v1

    sub-float/2addr v0, v1

    .line 648861
    :cond_2
    iget-object v1, p0, LX/3uZ;->o:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v1, v0}, LX/0vv;->b(Landroid/view/View;F)V

    .line 648862
    new-instance v1, LX/3uk;

    invoke-direct {v1}, LX/3uk;-><init>()V

    .line 648863
    iget-object v2, p0, LX/3uZ;->o:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v2}, LX/0vv;->v(Landroid/view/View;)LX/3sU;

    move-result-object v2

    invoke-virtual {v2, v4}, LX/3sU;->c(F)LX/3sU;

    move-result-object v2

    .line 648864
    iget-object v3, p0, LX/3uZ;->g:LX/3sb;

    invoke-virtual {v2, v3}, LX/3sU;->a(LX/3sb;)LX/3sU;

    .line 648865
    invoke-virtual {v1, v2}, LX/3uk;->a(LX/3sU;)LX/3uk;

    .line 648866
    iget-boolean v2, p0, LX/3uZ;->D:Z

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/3uZ;->s:Landroid/view/View;

    if-eqz v2, :cond_3

    .line 648867
    iget-object v2, p0, LX/3uZ;->s:Landroid/view/View;

    invoke-static {v2, v0}, LX/0vv;->b(Landroid/view/View;F)V

    .line 648868
    iget-object v0, p0, LX/3uZ;->s:Landroid/view/View;

    invoke-static {v0}, LX/0vv;->v(Landroid/view/View;)LX/3sU;

    move-result-object v0

    invoke-virtual {v0, v4}, LX/3sU;->c(F)LX/3sU;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/3uk;->a(LX/3sU;)LX/3uk;

    .line 648869
    :cond_3
    iget-object v0, p0, LX/3uZ;->r:Landroid/support/v7/internal/widget/ActionBarContainer;

    if-eqz v0, :cond_4

    iget v0, p0, LX/3uZ;->A:I

    if-ne v0, v6, :cond_4

    .line 648870
    iget-object v0, p0, LX/3uZ;->r:Landroid/support/v7/internal/widget/ActionBarContainer;

    iget-object v2, p0, LX/3uZ;->r:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v2}, Landroid/support/v7/internal/widget/ActionBarContainer;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-static {v0, v2}, LX/0vv;->b(Landroid/view/View;F)V

    .line 648871
    iget-object v0, p0, LX/3uZ;->r:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0, v5}, Landroid/support/v7/internal/widget/ActionBarContainer;->setVisibility(I)V

    .line 648872
    iget-object v0, p0, LX/3uZ;->r:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v0}, LX/0vv;->v(Landroid/view/View;)LX/3sU;

    move-result-object v0

    invoke-virtual {v0, v4}, LX/3sU;->c(F)LX/3sU;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/3uk;->a(LX/3sU;)LX/3uk;

    .line 648873
    :cond_4
    iget-object v0, p0, LX/3uZ;->j:Landroid/content/Context;

    const v2, 0x10a0006

    invoke-static {v0, v2}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/3uk;->a(Landroid/view/animation/Interpolator;)LX/3uk;

    .line 648874
    const-wide/16 v2, 0xfa

    invoke-virtual {v1, v2, v3}, LX/3uk;->a(J)LX/3uk;

    .line 648875
    iget-object v0, p0, LX/3uZ;->f:LX/3oQ;

    invoke-virtual {v1, v0}, LX/3uk;->a(LX/3oQ;)LX/3uk;

    .line 648876
    iput-object v1, p0, LX/3uZ;->I:LX/3uk;

    .line 648877
    invoke-virtual {v1}, LX/3uk;->a()V

    .line 648878
    :goto_0
    iget-object v0, p0, LX/3uZ;->n:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    if-eqz v0, :cond_5

    .line 648879
    iget-object v0, p0, LX/3uZ;->n:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    invoke-static {v0}, LX/0vv;->z(Landroid/view/View;)V

    .line 648880
    :cond_5
    return-void

    .line 648881
    :cond_6
    iget-object v0, p0, LX/3uZ;->o:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v0, v1}, LX/0vv;->c(Landroid/view/View;F)V

    .line 648882
    iget-object v0, p0, LX/3uZ;->o:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v0, v4}, LX/0vv;->b(Landroid/view/View;F)V

    .line 648883
    iget-boolean v0, p0, LX/3uZ;->D:Z

    if-eqz v0, :cond_7

    iget-object v0, p0, LX/3uZ;->s:Landroid/view/View;

    if-eqz v0, :cond_7

    .line 648884
    iget-object v0, p0, LX/3uZ;->s:Landroid/view/View;

    invoke-static {v0, v4}, LX/0vv;->b(Landroid/view/View;F)V

    .line 648885
    :cond_7
    iget-object v0, p0, LX/3uZ;->r:Landroid/support/v7/internal/widget/ActionBarContainer;

    if-eqz v0, :cond_8

    iget v0, p0, LX/3uZ;->A:I

    if-ne v0, v6, :cond_8

    .line 648886
    iget-object v0, p0, LX/3uZ;->r:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v0, v1}, LX/0vv;->c(Landroid/view/View;F)V

    .line 648887
    iget-object v0, p0, LX/3uZ;->r:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v0, v4}, LX/0vv;->b(Landroid/view/View;F)V

    .line 648888
    iget-object v0, p0, LX/3uZ;->r:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0, v5}, Landroid/support/v7/internal/widget/ActionBarContainer;->setVisibility(I)V

    .line 648889
    :cond_8
    iget-object v0, p0, LX/3uZ;->f:LX/3oQ;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/3oQ;->b(Landroid/view/View;)V

    goto :goto_0

    nop

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method private k(Z)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/high16 v4, 0x3f800000    # 1.0f

    .line 648890
    iget-object v0, p0, LX/3uZ;->I:LX/3uk;

    if-eqz v0, :cond_0

    .line 648891
    iget-object v0, p0, LX/3uZ;->I:LX/3uk;

    invoke-virtual {v0}, LX/3uk;->b()V

    .line 648892
    :cond_0
    iget v0, p0, LX/3uZ;->C:I

    if-nez v0, :cond_5

    sget-boolean v0, LX/3uZ;->i:Z

    if-eqz v0, :cond_5

    iget-boolean v0, p0, LX/3uZ;->J:Z

    if-nez v0, :cond_1

    if-eqz p1, :cond_5

    .line 648893
    :cond_1
    iget-object v0, p0, LX/3uZ;->o:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v0, v4}, LX/0vv;->c(Landroid/view/View;F)V

    .line 648894
    iget-object v0, p0, LX/3uZ;->o:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0, v5}, Landroid/support/v7/internal/widget/ActionBarContainer;->setTransitioning(Z)V

    .line 648895
    new-instance v1, LX/3uk;

    invoke-direct {v1}, LX/3uk;-><init>()V

    .line 648896
    iget-object v0, p0, LX/3uZ;->o:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContainer;->getHeight()I

    move-result v0

    neg-int v0, v0

    int-to-float v0, v0

    .line 648897
    if-eqz p1, :cond_2

    .line 648898
    const/4 v2, 0x2

    new-array v2, v2, [I

    fill-array-data v2, :array_0

    .line 648899
    iget-object v3, p0, LX/3uZ;->o:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v3, v2}, Landroid/support/v7/internal/widget/ActionBarContainer;->getLocationInWindow([I)V

    .line 648900
    aget v2, v2, v5

    int-to-float v2, v2

    sub-float/2addr v0, v2

    .line 648901
    :cond_2
    iget-object v2, p0, LX/3uZ;->o:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v2}, LX/0vv;->v(Landroid/view/View;)LX/3sU;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/3sU;->c(F)LX/3sU;

    move-result-object v2

    .line 648902
    iget-object v3, p0, LX/3uZ;->g:LX/3sb;

    invoke-virtual {v2, v3}, LX/3sU;->a(LX/3sb;)LX/3sU;

    .line 648903
    invoke-virtual {v1, v2}, LX/3uk;->a(LX/3sU;)LX/3uk;

    .line 648904
    iget-boolean v2, p0, LX/3uZ;->D:Z

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/3uZ;->s:Landroid/view/View;

    if-eqz v2, :cond_3

    .line 648905
    iget-object v2, p0, LX/3uZ;->s:Landroid/view/View;

    invoke-static {v2}, LX/0vv;->v(Landroid/view/View;)LX/3sU;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/3sU;->c(F)LX/3sU;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/3uk;->a(LX/3sU;)LX/3uk;

    .line 648906
    :cond_3
    iget-object v0, p0, LX/3uZ;->r:Landroid/support/v7/internal/widget/ActionBarContainer;

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/3uZ;->r:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContainer;->getVisibility()I

    move-result v0

    if-nez v0, :cond_4

    .line 648907
    iget-object v0, p0, LX/3uZ;->r:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v0, v4}, LX/0vv;->c(Landroid/view/View;F)V

    .line 648908
    iget-object v0, p0, LX/3uZ;->r:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v0}, LX/0vv;->v(Landroid/view/View;)LX/3sU;

    move-result-object v0

    iget-object v2, p0, LX/3uZ;->r:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v2}, Landroid/support/v7/internal/widget/ActionBarContainer;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0, v2}, LX/3sU;->c(F)LX/3sU;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/3uk;->a(LX/3sU;)LX/3uk;

    .line 648909
    :cond_4
    iget-object v0, p0, LX/3uZ;->j:Landroid/content/Context;

    const v2, 0x10a0005

    invoke-static {v0, v2}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/3uk;->a(Landroid/view/animation/Interpolator;)LX/3uk;

    .line 648910
    const-wide/16 v2, 0xfa

    invoke-virtual {v1, v2, v3}, LX/3uk;->a(J)LX/3uk;

    .line 648911
    iget-object v0, p0, LX/3uZ;->e:LX/3oQ;

    invoke-virtual {v1, v0}, LX/3uk;->a(LX/3oQ;)LX/3uk;

    .line 648912
    iput-object v1, p0, LX/3uZ;->I:LX/3uk;

    .line 648913
    invoke-virtual {v1}, LX/3uk;->a()V

    .line 648914
    :goto_0
    return-void

    .line 648915
    :cond_5
    iget-object v0, p0, LX/3uZ;->e:LX/3oQ;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/3oQ;->b(Landroid/view/View;)V

    goto :goto_0

    nop

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method public static n(LX/3uZ;)I
    .locals 1

    .prologue
    .line 648916
    iget-object v0, p0, LX/3uZ;->p:LX/3vk;

    invoke-interface {v0}, LX/3vk;->o()I

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(LX/3uG;)LX/3uV;
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 648917
    iget-object v0, p0, LX/3uZ;->a:LX/3uW;

    if-eqz v0, :cond_0

    .line 648918
    iget-object v0, p0, LX/3uZ;->a:LX/3uW;

    invoke-virtual {v0}, LX/3uV;->c()V

    .line 648919
    :cond_0
    iget-object v0, p0, LX/3uZ;->n:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    invoke-virtual {v0, v2}, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->setHideOnContentScrollEnabled(Z)V

    .line 648920
    iget-object v0, p0, LX/3uZ;->q:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->c()V

    .line 648921
    new-instance v0, LX/3uW;

    iget-object v1, p0, LX/3uZ;->q:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v1}, Landroid/support/v7/internal/widget/ActionBarContextView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1}, LX/3uW;-><init>(LX/3uZ;Landroid/content/Context;LX/3uG;)V

    .line 648922
    invoke-virtual {v0}, LX/3uW;->e()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 648923
    invoke-virtual {v0}, LX/3uV;->d()V

    .line 648924
    iget-object v1, p0, LX/3uZ;->q:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v1, v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->a(LX/3uV;)V

    .line 648925
    invoke-virtual {p0, v3}, LX/3uZ;->g(Z)V

    .line 648926
    iget-object v1, p0, LX/3uZ;->r:Landroid/support/v7/internal/widget/ActionBarContainer;

    if-eqz v1, :cond_1

    iget v1, p0, LX/3uZ;->A:I

    if-ne v1, v3, :cond_1

    .line 648927
    iget-object v1, p0, LX/3uZ;->r:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v1}, Landroid/support/v7/internal/widget/ActionBarContainer;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_1

    .line 648928
    iget-object v1, p0, LX/3uZ;->r:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v1, v2}, Landroid/support/v7/internal/widget/ActionBarContainer;->setVisibility(I)V

    .line 648929
    iget-object v1, p0, LX/3uZ;->n:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    if-eqz v1, :cond_1

    .line 648930
    iget-object v1, p0, LX/3uZ;->n:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    invoke-static {v1}, LX/0vv;->z(Landroid/view/View;)V

    .line 648931
    :cond_1
    iget-object v1, p0, LX/3uZ;->q:Landroid/support/v7/internal/widget/ActionBarContextView;

    const/16 v2, 0x20

    invoke-virtual {v1, v2}, Landroid/support/v7/internal/widget/ActionBarContextView;->sendAccessibilityEvent(I)V

    .line 648932
    iput-object v0, p0, LX/3uZ;->a:LX/3uW;

    .line 648933
    :goto_0
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()Landroid/view/View;
    .locals 1

    .prologue
    .line 648934
    iget-object v0, p0, LX/3uZ;->p:LX/3vk;

    invoke-interface {v0}, LX/3vk;->q()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a(F)V
    .locals 1

    .prologue
    .line 648935
    iget-object v0, p0, LX/3uZ;->o:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v0, p1}, LX/0vv;->f(Landroid/view/View;F)V

    .line 648936
    iget-object v0, p0, LX/3uZ;->r:Landroid/support/v7/internal/widget/ActionBarContainer;

    if-eqz v0, :cond_0

    .line 648937
    iget-object v0, p0, LX/3uZ;->r:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v0, p1}, LX/0vv;->f(Landroid/view/View;F)V

    .line 648938
    :cond_0
    return-void
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 648939
    invoke-virtual {p0}, LX/3u1;->e()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget-object v1, p0, LX/3uZ;->p:LX/3vk;

    invoke-interface {v1}, LX/3vk;->a()Landroid/view/ViewGroup;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/3u1;->a(Landroid/view/View;)V

    .line 648940
    return-void
.end method

.method public final a(II)V
    .locals 4

    .prologue
    .line 648941
    iget-object v0, p0, LX/3uZ;->p:LX/3vk;

    invoke-interface {v0}, LX/3vk;->n()I

    move-result v0

    .line 648942
    and-int/lit8 v1, p2, 0x4

    if-eqz v1, :cond_0

    .line 648943
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/3uZ;->x:Z

    .line 648944
    :cond_0
    iget-object v1, p0, LX/3uZ;->p:LX/3vk;

    and-int v2, p1, p2

    xor-int/lit8 v3, p2, -0x1

    and-int/2addr v0, v3

    or-int/2addr v0, v2

    invoke-interface {v1, v0}, LX/3vk;->c(I)V

    .line 648945
    return-void
.end method

.method public final a(LX/3u0;)V
    .locals 3

    .prologue
    const/4 v1, -0x1

    .line 648946
    invoke-static {p0}, LX/3uZ;->n(LX/3uZ;)I

    move-result v0

    const/4 v2, 0x2

    if-eq v0, v2, :cond_2

    .line 648947
    if-eqz p1, :cond_1

    invoke-virtual {p1}, LX/3u0;->a()I

    move-result v0

    :goto_0
    iput v0, p0, LX/3uZ;->w:I

    .line 648948
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 648949
    goto :goto_0

    .line 648950
    :cond_2
    iget-object v0, p0, LX/3uZ;->l:Landroid/app/Activity;

    instance-of v0, v0, Landroid/support/v4/app/FragmentActivity;

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/3uZ;->p:LX/3vk;

    invoke-interface {v0}, LX/3vk;->a()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewGroup;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_4

    .line 648951
    iget-object v0, p0, LX/3uZ;->l:Landroid/app/Activity;

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->a()LX/0hH;

    move-result-object v0

    .line 648952
    :goto_2
    iget-object v2, p0, LX/3uZ;->v:LX/3uX;

    if-ne v2, p1, :cond_5

    .line 648953
    iget-object v1, p0, LX/3uZ;->v:LX/3uX;

    if-eqz v1, :cond_3

    .line 648954
    iget-object v1, p0, LX/3uZ;->t:LX/3vu;

    invoke-virtual {p1}, LX/3u0;->a()I

    move-result v2

    invoke-virtual {v1, v2}, LX/3vu;->a(I)V

    .line 648955
    :cond_3
    :goto_3
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/0hH;->e()Z

    move-result v1

    if-nez v1, :cond_0

    .line 648956
    invoke-virtual {v0}, LX/0hH;->b()I

    goto :goto_1

    .line 648957
    :cond_4
    const/4 v0, 0x0

    goto :goto_2

    .line 648958
    :cond_5
    iget-object v2, p0, LX/3uZ;->t:LX/3vu;

    if-eqz p1, :cond_6

    invoke-virtual {p1}, LX/3u0;->a()I

    move-result v1

    :cond_6
    invoke-virtual {v2, v1}, LX/3vu;->setTabSelected(I)V

    .line 648959
    check-cast p1, LX/3uX;

    iput-object p1, p0, LX/3uZ;->v:LX/3uX;

    goto :goto_3
.end method

.method public final a(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 648577
    iget-object v0, p0, LX/3uZ;->p:LX/3vk;

    invoke-interface {v0, p1}, LX/3vk;->b(Landroid/graphics/drawable/Drawable;)V

    .line 648578
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 648960
    iget-object v0, p0, LX/3uZ;->p:LX/3vk;

    invoke-interface {v0, p1}, LX/3vk;->a(Landroid/view/View;)V

    .line 648961
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 648728
    iget-object v0, p0, LX/3uZ;->p:LX/3vk;

    invoke-interface {v0, p1}, LX/3vk;->b(Ljava/lang/CharSequence;)V

    .line 648729
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 648730
    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p0, v0, v1}, LX/3u1;->a(II)V

    .line 648731
    return-void

    .line 648732
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 648574
    iget-object v0, p0, LX/3uZ;->p:LX/3vk;

    invoke-interface {v0}, LX/3vk;->n()I

    move-result v0

    return v0
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 648575
    iget-object v0, p0, LX/3uZ;->j:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/3u1;->a(Ljava/lang/CharSequence;)V

    .line 648576
    return-void
.end method

.method public final b(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 648579
    iget-object v0, p0, LX/3uZ;->p:LX/3vk;

    invoke-interface {v0, p1}, LX/3vk;->c(Ljava/lang/CharSequence;)V

    .line 648580
    return-void
.end method

.method public final b(Z)V
    .locals 2

    .prologue
    const/16 v1, 0x10

    .line 648581
    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p0, v0, v1}, LX/3u1;->a(II)V

    .line 648582
    return-void

    .line 648583
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 648584
    iget-boolean v0, p0, LX/3uZ;->E:Z

    if-eqz v0, :cond_0

    .line 648585
    iput-boolean v1, p0, LX/3uZ;->E:Z

    .line 648586
    invoke-static {p0, v1}, LX/3uZ;->i(LX/3uZ;Z)V

    .line 648587
    :cond_0
    return-void
.end method

.method public final c(I)V
    .locals 8

    .prologue
    const/4 v6, 0x2

    const/4 v1, 0x1

    const/4 v5, -0x1

    const/4 v2, 0x0

    .line 648588
    iget-object v0, p0, LX/3uZ;->p:LX/3vk;

    invoke-interface {v0}, LX/3vk;->o()I

    move-result v0

    .line 648589
    packed-switch v0, :pswitch_data_0

    .line 648590
    :goto_0
    if-eq v0, p1, :cond_0

    iget-boolean v0, p0, LX/3uZ;->B:Z

    if-nez v0, :cond_0

    .line 648591
    iget-object v0, p0, LX/3uZ;->n:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    if-eqz v0, :cond_0

    .line 648592
    iget-object v0, p0, LX/3uZ;->n:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    invoke-static {v0}, LX/0vv;->z(Landroid/view/View;)V

    .line 648593
    :cond_0
    iget-object v0, p0, LX/3uZ;->p:LX/3vk;

    invoke-interface {v0, p1}, LX/3vk;->d(I)V

    .line 648594
    packed-switch p1, :pswitch_data_1

    .line 648595
    :cond_1
    :goto_1
    iget-object v3, p0, LX/3uZ;->p:LX/3vk;

    if-ne p1, v6, :cond_3

    iget-boolean v0, p0, LX/3uZ;->B:Z

    if-nez v0, :cond_3

    move v0, v1

    :goto_2
    invoke-interface {v3, v0}, LX/3vk;->a(Z)V

    .line 648596
    iget-object v0, p0, LX/3uZ;->n:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    if-ne p1, v6, :cond_4

    iget-boolean v3, p0, LX/3uZ;->B:Z

    if-nez v3, :cond_4

    .line 648597
    :goto_3
    iput-boolean v1, v0, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->k:Z

    .line 648598
    return-void

    .line 648599
    :pswitch_0
    const/4 v3, -0x1

    .line 648600
    iget-object v4, p0, LX/3uZ;->p:LX/3vk;

    invoke-interface {v4}, LX/3vk;->o()I

    move-result v4

    packed-switch v4, :pswitch_data_2

    .line 648601
    :cond_2
    :goto_4
    move v3, v3

    .line 648602
    iput v3, p0, LX/3uZ;->w:I

    .line 648603
    const/4 v3, 0x0

    invoke-virtual {p0, v3}, LX/3uZ;->a(LX/3u0;)V

    .line 648604
    iget-object v3, p0, LX/3uZ;->t:LX/3vu;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, LX/3vu;->setVisibility(I)V

    goto :goto_0

    .line 648605
    :pswitch_1
    const/4 v7, 0x0

    .line 648606
    iget-object v0, p0, LX/3uZ;->t:LX/3vu;

    if-eqz v0, :cond_5

    .line 648607
    :goto_5
    iget-object v0, p0, LX/3uZ;->t:LX/3vu;

    invoke-virtual {v0, v2}, LX/3vu;->setVisibility(I)V

    .line 648608
    iget v0, p0, LX/3uZ;->w:I

    if-eq v0, v5, :cond_1

    .line 648609
    iget v0, p0, LX/3uZ;->w:I

    invoke-direct {p0, v0}, LX/3uZ;->f(I)V

    .line 648610
    iput v5, p0, LX/3uZ;->w:I

    goto :goto_1

    :cond_3
    move v0, v2

    .line 648611
    goto :goto_2

    :cond_4
    move v1, v2

    .line 648612
    goto :goto_3

    .line 648613
    :pswitch_2
    iget-object v4, p0, LX/3uZ;->v:LX/3uX;

    if-eqz v4, :cond_2

    iget-object v3, p0, LX/3uZ;->v:LX/3uX;

    invoke-virtual {v3}, LX/3u0;->a()I

    move-result v3

    goto :goto_4

    .line 648614
    :pswitch_3
    iget-object v3, p0, LX/3uZ;->p:LX/3vk;

    invoke-interface {v3}, LX/3vk;->p()I

    move-result v3

    goto :goto_4

    .line 648615
    :cond_5
    new-instance v0, LX/3vu;

    iget-object v3, p0, LX/3uZ;->j:Landroid/content/Context;

    invoke-direct {v0, v3}, LX/3vu;-><init>(Landroid/content/Context;)V

    .line 648616
    iget-boolean v3, p0, LX/3uZ;->B:Z

    if-eqz v3, :cond_6

    .line 648617
    invoke-virtual {v0, v7}, LX/3vu;->setVisibility(I)V

    .line 648618
    iget-object v3, p0, LX/3uZ;->p:LX/3vk;

    invoke-interface {v3, v0}, LX/3vk;->a(LX/3vu;)V

    .line 648619
    :goto_6
    iput-object v0, p0, LX/3uZ;->t:LX/3vu;

    goto :goto_5

    .line 648620
    :cond_6
    invoke-static {p0}, LX/3uZ;->n(LX/3uZ;)I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_8

    .line 648621
    invoke-virtual {v0, v7}, LX/3vu;->setVisibility(I)V

    .line 648622
    iget-object v3, p0, LX/3uZ;->n:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    if-eqz v3, :cond_7

    .line 648623
    iget-object v3, p0, LX/3uZ;->n:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    invoke-static {v3}, LX/0vv;->z(Landroid/view/View;)V

    .line 648624
    :cond_7
    :goto_7
    iget-object v3, p0, LX/3uZ;->o:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v3, v0}, Landroid/support/v7/internal/widget/ActionBarContainer;->setTabContainer(LX/3vu;)V

    goto :goto_6

    .line 648625
    :cond_8
    const/16 v3, 0x8

    invoke-virtual {v0, v3}, LX/3vu;->setVisibility(I)V

    goto :goto_7

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public final c(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 648626
    iget-object v0, p0, LX/3uZ;->p:LX/3vk;

    invoke-interface {v0, p1}, LX/3vk;->a(Ljava/lang/CharSequence;)V

    .line 648627
    return-void
.end method

.method public final c(Z)V
    .locals 2

    .prologue
    .line 648628
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/3uZ;->n:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    .line 648629
    iget-boolean v1, v0, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->j:Z

    move v0, v1

    .line 648630
    if-nez v0, :cond_0

    .line 648631
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Action bar must be in overlay mode (Window.FEATURE_OVERLAY_ACTION_BAR) to enable hide on content scroll"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 648632
    :cond_0
    iput-boolean p1, p0, LX/3uZ;->d:Z

    .line 648633
    iget-object v0, p0, LX/3uZ;->n:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    invoke-virtual {v0, p1}, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->setHideOnContentScrollEnabled(Z)V

    .line 648634
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 648635
    iget-boolean v0, p0, LX/3uZ;->E:Z

    if-nez v0, :cond_0

    .line 648636
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/3uZ;->E:Z

    .line 648637
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/3uZ;->i(LX/3uZ;Z)V

    .line 648638
    :cond_0
    return-void
.end method

.method public final d(I)V
    .locals 1

    .prologue
    .line 648639
    iget-object v0, p0, LX/3uZ;->p:LX/3vk;

    invoke-interface {v0, p1}, LX/3vk;->g(I)V

    .line 648640
    return-void
.end method

.method public final d(Z)V
    .locals 1

    .prologue
    .line 648641
    iget-boolean v0, p0, LX/3uZ;->x:Z

    if-nez v0, :cond_0

    .line 648642
    invoke-virtual {p0, p1}, LX/3u1;->a(Z)V

    .line 648643
    :cond_0
    return-void
.end method

.method public final e()Landroid/content/Context;
    .locals 4

    .prologue
    .line 648644
    iget-object v0, p0, LX/3uZ;->k:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 648645
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 648646
    iget-object v1, p0, LX/3uZ;->j:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    .line 648647
    const v2, 0x7f010014

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 648648
    iget v0, v0, Landroid/util/TypedValue;->resourceId:I

    .line 648649
    if-eqz v0, :cond_1

    .line 648650
    new-instance v1, Landroid/view/ContextThemeWrapper;

    iget-object v2, p0, LX/3uZ;->j:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, LX/3uZ;->k:Landroid/content/Context;

    .line 648651
    :cond_0
    :goto_0
    iget-object v0, p0, LX/3uZ;->k:Landroid/content/Context;

    return-object v0

    .line 648652
    :cond_1
    iget-object v0, p0, LX/3uZ;->j:Landroid/content/Context;

    iput-object v0, p0, LX/3uZ;->k:Landroid/content/Context;

    goto :goto_0
.end method

.method public final e(I)V
    .locals 0

    .prologue
    .line 648653
    iput p1, p0, LX/3uZ;->C:I

    .line 648654
    return-void
.end method

.method public final e(Z)V
    .locals 3

    .prologue
    .line 648655
    iget-boolean v0, p0, LX/3uZ;->y:Z

    if-ne p1, v0, :cond_1

    .line 648656
    :cond_0
    return-void

    .line 648657
    :cond_1
    iput-boolean p1, p0, LX/3uZ;->y:Z

    .line 648658
    iget-object v0, p0, LX/3uZ;->z:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 648659
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 648660
    iget-object v2, p0, LX/3uZ;->z:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 648661
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public final f(Z)V
    .locals 0

    .prologue
    .line 648662
    iput-boolean p1, p0, LX/3uZ;->D:Z

    .line 648663
    return-void
.end method

.method public final g(Z)V
    .locals 11

    .prologue
    const/16 v1, 0x8

    const/4 v2, 0x0

    .line 648664
    if-eqz p1, :cond_4

    .line 648665
    const/4 v3, 0x1

    .line 648666
    iget-boolean v0, p0, LX/3uZ;->G:Z

    if-nez v0, :cond_1

    .line 648667
    iput-boolean v3, p0, LX/3uZ;->G:Z

    .line 648668
    iget-object v0, p0, LX/3uZ;->n:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    if-eqz v0, :cond_0

    .line 648669
    :cond_0
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/3uZ;->i(LX/3uZ;Z)V

    .line 648670
    :cond_1
    :goto_0
    iget-object v3, p0, LX/3uZ;->p:LX/3vk;

    if-eqz p1, :cond_7

    move v0, v1

    :goto_1
    invoke-interface {v3, v0}, LX/3vk;->f(I)V

    .line 648671
    iget-object v0, p0, LX/3uZ;->q:Landroid/support/v7/internal/widget/ActionBarContextView;

    if-eqz p1, :cond_8

    :goto_2
    const/high16 v10, 0x3f800000    # 1.0f

    const-wide/16 v8, 0xc8

    const/4 v7, 0x0

    .line 648672
    iget-object v4, v0, LX/3vJ;->i:LX/3sU;

    if-eqz v4, :cond_2

    .line 648673
    iget-object v4, v0, LX/3vJ;->i:LX/3sU;

    invoke-virtual {v4}, LX/3sU;->a()V

    .line 648674
    :cond_2
    if-nez v2, :cond_a

    .line 648675
    invoke-virtual {v0}, LX/3vJ;->getVisibility()I

    move-result v4

    if-eqz v4, :cond_3

    .line 648676
    invoke-static {v0, v7}, LX/0vv;->c(Landroid/view/View;F)V

    .line 648677
    iget-object v4, v0, LX/3vJ;->e:Landroid/view/ViewGroup;

    if-eqz v4, :cond_3

    iget-object v4, v0, LX/3vJ;->c:Landroid/support/v7/widget/ActionMenuView;

    if-eqz v4, :cond_3

    .line 648678
    iget-object v4, v0, LX/3vJ;->c:Landroid/support/v7/widget/ActionMenuView;

    invoke-static {v4, v7}, LX/0vv;->c(Landroid/view/View;F)V

    .line 648679
    :cond_3
    invoke-static {v0}, LX/0vv;->v(Landroid/view/View;)LX/3sU;

    move-result-object v4

    invoke-virtual {v4, v10}, LX/3sU;->a(F)LX/3sU;

    move-result-object v4

    .line 648680
    invoke-virtual {v4, v8, v9}, LX/3sU;->a(J)LX/3sU;

    .line 648681
    sget-object v5, LX/3vJ;->j:Landroid/view/animation/Interpolator;

    invoke-virtual {v4, v5}, LX/3sU;->a(Landroid/view/animation/Interpolator;)LX/3sU;

    .line 648682
    iget-object v5, v0, LX/3vJ;->e:Landroid/view/ViewGroup;

    if-eqz v5, :cond_9

    iget-object v5, v0, LX/3vJ;->c:Landroid/support/v7/widget/ActionMenuView;

    if-eqz v5, :cond_9

    .line 648683
    new-instance v5, LX/3uk;

    invoke-direct {v5}, LX/3uk;-><init>()V

    .line 648684
    iget-object v6, v0, LX/3vJ;->c:Landroid/support/v7/widget/ActionMenuView;

    invoke-static {v6}, LX/0vv;->v(Landroid/view/View;)LX/3sU;

    move-result-object v6

    invoke-virtual {v6, v10}, LX/3sU;->a(F)LX/3sU;

    move-result-object v6

    .line 648685
    invoke-virtual {v6, v8, v9}, LX/3sU;->a(J)LX/3sU;

    .line 648686
    iget-object v7, v0, LX/3vJ;->a:LX/3vI;

    invoke-virtual {v7, v4, v2}, LX/3vI;->a(LX/3sU;I)LX/3vI;

    move-result-object v7

    invoke-virtual {v5, v7}, LX/3uk;->a(LX/3oQ;)LX/3uk;

    .line 648687
    invoke-virtual {v5, v4}, LX/3uk;->a(LX/3sU;)LX/3uk;

    move-result-object v4

    invoke-virtual {v4, v6}, LX/3uk;->a(LX/3sU;)LX/3uk;

    .line 648688
    invoke-virtual {v5}, LX/3uk;->a()V

    .line 648689
    :goto_3
    return-void

    .line 648690
    :cond_4
    const/4 v3, 0x0

    .line 648691
    iget-boolean v0, p0, LX/3uZ;->G:Z

    if-eqz v0, :cond_6

    .line 648692
    iput-boolean v3, p0, LX/3uZ;->G:Z

    .line 648693
    iget-object v0, p0, LX/3uZ;->n:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    if-eqz v0, :cond_5

    .line 648694
    :cond_5
    invoke-static {p0, v3}, LX/3uZ;->i(LX/3uZ;Z)V

    .line 648695
    :cond_6
    goto :goto_0

    :cond_7
    move v0, v2

    .line 648696
    goto :goto_1

    :cond_8
    move v2, v1

    .line 648697
    goto :goto_2

    .line 648698
    :cond_9
    iget-object v5, v0, LX/3vJ;->a:LX/3vI;

    invoke-virtual {v5, v4, v2}, LX/3vI;->a(LX/3sU;I)LX/3vI;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/3sU;->a(LX/3oQ;)LX/3sU;

    .line 648699
    invoke-virtual {v4}, LX/3sU;->b()V

    goto :goto_3

    .line 648700
    :cond_a
    invoke-static {v0}, LX/0vv;->v(Landroid/view/View;)LX/3sU;

    move-result-object v4

    invoke-virtual {v4, v7}, LX/3sU;->a(F)LX/3sU;

    move-result-object v4

    .line 648701
    invoke-virtual {v4, v8, v9}, LX/3sU;->a(J)LX/3sU;

    .line 648702
    sget-object v5, LX/3vJ;->j:Landroid/view/animation/Interpolator;

    invoke-virtual {v4, v5}, LX/3sU;->a(Landroid/view/animation/Interpolator;)LX/3sU;

    .line 648703
    iget-object v5, v0, LX/3vJ;->e:Landroid/view/ViewGroup;

    if-eqz v5, :cond_b

    iget-object v5, v0, LX/3vJ;->c:Landroid/support/v7/widget/ActionMenuView;

    if-eqz v5, :cond_b

    .line 648704
    new-instance v5, LX/3uk;

    invoke-direct {v5}, LX/3uk;-><init>()V

    .line 648705
    iget-object v6, v0, LX/3vJ;->c:Landroid/support/v7/widget/ActionMenuView;

    invoke-static {v6}, LX/0vv;->v(Landroid/view/View;)LX/3sU;

    move-result-object v6

    invoke-virtual {v6, v7}, LX/3sU;->a(F)LX/3sU;

    move-result-object v6

    .line 648706
    invoke-virtual {v6, v8, v9}, LX/3sU;->a(J)LX/3sU;

    .line 648707
    iget-object v7, v0, LX/3vJ;->a:LX/3vI;

    invoke-virtual {v7, v4, v2}, LX/3vI;->a(LX/3sU;I)LX/3vI;

    move-result-object v7

    invoke-virtual {v5, v7}, LX/3uk;->a(LX/3oQ;)LX/3uk;

    .line 648708
    invoke-virtual {v5, v4}, LX/3uk;->a(LX/3sU;)LX/3uk;

    move-result-object v4

    invoke-virtual {v4, v6}, LX/3uk;->a(LX/3sU;)LX/3uk;

    .line 648709
    invoke-virtual {v5}, LX/3uk;->a()V

    goto :goto_3

    .line 648710
    :cond_b
    iget-object v5, v0, LX/3vJ;->a:LX/3vI;

    invoke-virtual {v5, v4, v2}, LX/3vI;->a(LX/3sU;I)LX/3vI;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/3sU;->a(LX/3oQ;)LX/3sU;

    .line 648711
    invoke-virtual {v4}, LX/3sU;->b()V

    goto :goto_3
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 648712
    iget-object v0, p0, LX/3uZ;->p:LX/3vk;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3uZ;->p:LX/3vk;

    invoke-interface {v0}, LX/3vk;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 648713
    iget-object v0, p0, LX/3uZ;->p:LX/3vk;

    invoke-interface {v0}, LX/3vk;->e()V

    .line 648714
    const/4 v0, 0x1

    .line 648715
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()V
    .locals 1

    .prologue
    .line 648716
    iget-boolean v0, p0, LX/3uZ;->F:Z

    if-eqz v0, :cond_0

    .line 648717
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/3uZ;->F:Z

    .line 648718
    const/4 v0, 0x1

    invoke-static {p0, v0}, LX/3uZ;->i(LX/3uZ;Z)V

    .line 648719
    :cond_0
    return-void
.end method

.method public final k()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 648720
    iget-boolean v0, p0, LX/3uZ;->F:Z

    if-nez v0, :cond_0

    .line 648721
    iput-boolean v1, p0, LX/3uZ;->F:Z

    .line 648722
    invoke-static {p0, v1}, LX/3uZ;->i(LX/3uZ;Z)V

    .line 648723
    :cond_0
    return-void
.end method

.method public final l()V
    .locals 1

    .prologue
    .line 648724
    iget-object v0, p0, LX/3uZ;->I:LX/3uk;

    if-eqz v0, :cond_0

    .line 648725
    iget-object v0, p0, LX/3uZ;->I:LX/3uk;

    invoke-virtual {v0}, LX/3uk;->b()V

    .line 648726
    const/4 v0, 0x0

    iput-object v0, p0, LX/3uZ;->I:LX/3uk;

    .line 648727
    :cond_0
    return-void
.end method
