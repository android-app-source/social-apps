.class public final LX/40h;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 664172
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_3

    .line 664173
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 664174
    :goto_0
    return v1

    .line 664175
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 664176
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v3, :cond_2

    .line 664177
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 664178
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 664179
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v3, v4, :cond_1

    if-eqz v2, :cond_1

    .line 664180
    const-string v3, "section_type"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 664181
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    goto :goto_1

    .line 664182
    :cond_2
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 664183
    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 664184
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 664185
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 664186
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 664187
    if-eqz v0, :cond_0

    .line 664188
    const-string v0, "section_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 664189
    invoke-virtual {p0, p1, v1}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 664190
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 664191
    return-void
.end method
