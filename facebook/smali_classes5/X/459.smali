.class public LX/459;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 670183
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 670184
    iput-object p1, p0, LX/459;->a:Landroid/content/Context;

    .line 670185
    return-void
.end method

.method public static a(LX/0QB;)LX/459;
    .locals 2

    .prologue
    .line 670180
    new-instance v1, LX/459;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v1, v0}, LX/459;-><init>(Landroid/content/Context;)V

    .line 670181
    move-object v0, v1

    .line 670182
    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 670179
    iget-object v0, p0, LX/459;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->hasPermanentMenuKey()Z

    move-result v0

    return v0
.end method
