.class public LX/4Rs;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 710871
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 710872
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v2, :cond_5

    .line 710873
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 710874
    :goto_0
    return v0

    .line 710875
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 710876
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_4

    .line 710877
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 710878
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 710879
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 710880
    const-string v4, "associated_video"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 710881
    invoke-static {p0, p1}, LX/4UG;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 710882
    :cond_2
    const-string v4, "id"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 710883
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    goto :goto_1

    .line 710884
    :cond_3
    const-string v4, "url"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 710885
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 710886
    :cond_4
    const/4 v3, 0x4

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 710887
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v2}, LX/186;->b(II)V

    .line 710888
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v1}, LX/186;->b(II)V

    .line 710889
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 710890
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto :goto_0

    :cond_5
    move v1, v0

    move v2, v0

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 710891
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 710892
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 710893
    if-eqz v0, :cond_0

    .line 710894
    const-string v1, "associated_video"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 710895
    invoke-static {p0, v0, p2, p3}, LX/4UG;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 710896
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 710897
    if-eqz v0, :cond_1

    .line 710898
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 710899
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 710900
    :cond_1
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 710901
    if-eqz v0, :cond_2

    .line 710902
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 710903
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 710904
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 710905
    return-void
.end method
