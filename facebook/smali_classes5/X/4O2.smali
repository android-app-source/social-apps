.class public LX/4O2;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 695368
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 695396
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v2, :cond_8

    .line 695397
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 695398
    :goto_0
    return v0

    .line 695399
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 695400
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_7

    .line 695401
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 695402
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 695403
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_1

    if-eqz v6, :cond_1

    .line 695404
    const-string v7, "greeting_card_template"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 695405
    invoke-static {p0, p1}, LX/4O7;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 695406
    :cond_2
    const-string v7, "id"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 695407
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 695408
    :cond_3
    const-string v7, "primary_image"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 695409
    invoke-static {p0, p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 695410
    :cond_4
    const-string v7, "slides"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 695411
    invoke-static {p0, p1}, LX/4O6;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 695412
    :cond_5
    const-string v7, "theme"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 695413
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    goto :goto_1

    .line 695414
    :cond_6
    const-string v7, "url"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 695415
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 695416
    :cond_7
    const/4 v6, 0x7

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 695417
    const/4 v6, 0x1

    invoke-virtual {p1, v6, v5}, LX/186;->b(II)V

    .line 695418
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, LX/186;->b(II)V

    .line 695419
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, LX/186;->b(II)V

    .line 695420
    const/4 v3, 0x4

    invoke-virtual {p1, v3, v2}, LX/186;->b(II)V

    .line 695421
    const/4 v2, 0x5

    invoke-virtual {p1, v2, v1}, LX/186;->b(II)V

    .line 695422
    const/4 v1, 0x6

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 695423
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :cond_8
    move v1, v0

    move v2, v0

    move v3, v0

    move v4, v0

    move v5, v0

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 695369
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 695370
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 695371
    if-eqz v0, :cond_0

    .line 695372
    const-string v1, "greeting_card_template"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 695373
    invoke-static {p0, v0, p2, p3}, LX/4O7;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 695374
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 695375
    if-eqz v0, :cond_1

    .line 695376
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 695377
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 695378
    :cond_1
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 695379
    if-eqz v0, :cond_2

    .line 695380
    const-string v1, "primary_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 695381
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 695382
    :cond_2
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 695383
    if-eqz v0, :cond_3

    .line 695384
    const-string v1, "slides"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 695385
    invoke-static {p0, v0, p2, p3}, LX/4O6;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 695386
    :cond_3
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 695387
    if-eqz v0, :cond_4

    .line 695388
    const-string v1, "theme"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 695389
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 695390
    :cond_4
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 695391
    if-eqz v0, :cond_5

    .line 695392
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 695393
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 695394
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 695395
    return-void
.end method
