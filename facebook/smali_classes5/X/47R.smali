.class public final LX/47R;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/47M;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Landroid/os/Bundle;


# direct methods
.method public constructor <init>(LX/0Or;Landroid/os/Bundle;)V
    .locals 0
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;",
            "Landroid/os/Bundle;",
            ")V"
        }
    .end annotation

    .prologue
    .line 672293
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 672294
    iput-object p1, p0, LX/47R;->a:LX/0Or;

    .line 672295
    iput-object p2, p0, LX/47R;->b:Landroid/os/Bundle;

    .line 672296
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/os/Bundle;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 672286
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 672287
    iget-object v0, p0, LX/47R;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 672288
    iget-object v0, p0, LX/47R;->b:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    .line 672289
    iget-object v0, p0, LX/47R;->b:Landroid/os/Bundle;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 672290
    :cond_0
    if-eqz p2, :cond_1

    .line 672291
    invoke-virtual {v1, p2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 672292
    :cond_1
    return-object v1
.end method
