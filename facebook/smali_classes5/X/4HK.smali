.class public final LX/4HK;
.super LX/2vO;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 679583
    invoke-direct {p0}, LX/2vO;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Integer;)LX/4HK;
    .locals 1

    .prologue
    .line 679581
    const-string v0, "suggested_time"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 679582
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/4HK;
    .locals 1

    .prologue
    .line 679579
    const-string v0, "client_mutation_id"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 679580
    return-object p0
.end method

.method public final b(Ljava/lang/Integer;)LX/4HK;
    .locals 1

    .prologue
    .line 679577
    const-string v0, "suggested_end_time"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 679578
    return-object p0
.end method

.method public final b(Ljava/lang/String;)LX/4HK;
    .locals 1

    .prologue
    .line 679575
    const-string v0, "actor_id"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 679576
    return-object p0
.end method

.method public final c(Ljava/lang/String;)LX/4HK;
    .locals 1

    .prologue
    .line 679569
    const-string v0, "request_id"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 679570
    return-object p0
.end method

.method public final d(Ljava/lang/String;)LX/4HK;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/NativeBookingRequestStatusUpdateAction;
        .end annotation
    .end param

    .prologue
    .line 679573
    const-string v0, "action"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 679574
    return-object p0
.end method

.method public final f(Ljava/lang/String;)LX/4HK;
    .locals 1

    .prologue
    .line 679571
    const-string v0, "message_text"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 679572
    return-object p0
.end method
