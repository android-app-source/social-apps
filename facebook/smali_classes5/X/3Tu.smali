.class public LX/3Tu;
.super LX/2ja;
.source ""

# interfaces
.implements LX/2jc;


# instance fields
.field private final a:LX/0gh;

.field private final b:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

.field private final c:LX/3Tt;

.field private final d:LX/2Yg;

.field private final e:LX/1rn;

.field private final f:LX/0Sh;

.field private final g:LX/1rU;


# direct methods
.method public constructor <init>(LX/2jY;LX/1P1;LX/3Tt;LX/03V;LX/0gh;Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;LX/2Yg;LX/1rn;LX/2j3;LX/1vC;LX/0Sh;LX/1rU;)V
    .locals 7
    .param p1    # LX/2jY;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/1P1;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/3Tt;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 584885
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p4

    move-object/from16 v5, p9

    move-object/from16 v6, p10

    invoke-direct/range {v1 .. v6}, LX/2ja;-><init>(LX/2jY;LX/1P1;LX/03V;LX/2j3;LX/1vC;)V

    .line 584886
    iput-object p5, p0, LX/3Tu;->a:LX/0gh;

    .line 584887
    iput-object p6, p0, LX/3Tu;->b:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    .line 584888
    iput-object p3, p0, LX/3Tu;->c:LX/3Tt;

    .line 584889
    iput-object p7, p0, LX/3Tu;->d:LX/2Yg;

    .line 584890
    iput-object p8, p0, LX/3Tu;->e:LX/1rn;

    .line 584891
    move-object/from16 v0, p11

    iput-object v0, p0, LX/3Tu;->f:LX/0Sh;

    .line 584892
    move-object/from16 v0, p12

    iput-object v0, p0, LX/3Tu;->g:LX/1rU;

    .line 584893
    return-void
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 584873
    new-instance v1, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    invoke-direct {v1}, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;-><init>()V

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->c()Ljava/lang/String;

    move-result-object v2

    .line 584874
    iput-object v2, v1, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->i:Ljava/lang/String;

    .line 584875
    move-object v1, v1

    .line 584876
    iput-boolean v0, v1, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->j:Z

    .line 584877
    move-object v1, v1

    .line 584878
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->aF()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->a(Lcom/facebook/graphql/enums/GraphQLStorySeenState;)Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->SEEN_AND_READ:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->aF()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 584879
    :goto_0
    iput-boolean v0, v1, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->k:Z

    .line 584880
    move-object v0, v1

    .line 584881
    iget-object v1, p0, LX/3Tu;->c:LX/3Tt;

    invoke-static {p1}, LX/BCw;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, LX/3Tt;->b(Ljava/lang/String;)I

    move-result v1

    .line 584882
    iput v1, v0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->u:I

    .line 584883
    move-object v0, v0

    .line 584884
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)V
    .locals 4
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 584852
    iget-object v0, p0, LX/3Tu;->d:LX/2Yg;

    sget-object v1, LX/3B2;->rich_notification_curation:LX/3B2;

    invoke-direct {p0, p1}, LX/3Tu;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, p2, v3}, LX/2Yg;->a(LX/3B2;Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;Ljava/lang/String;Ljava/lang/String;)V

    .line 584853
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1    # Lcom/facebook/graphql/model/GraphQLStory;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 584869
    iget-object v0, p0, LX/3Tu;->a:LX/0gh;

    const-string v1, "tap_notification_jewel"

    invoke-virtual {v0, v1}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 584870
    iget-object v1, p0, LX/3Tu;->d:LX/2Yg;

    sget-object v2, LX/3B2;->graph_notification_click:LX/3B2;

    if-eqz p1, :cond_0

    invoke-direct {p0, p1}, LX/3Tu;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v0, p2, p3}, LX/2Yg;->a(LX/3B2;Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;Ljava/lang/String;Ljava/lang/String;)V

    .line 584871
    return-void

    .line 584872
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/Cfc;)V
    .locals 3

    .prologue
    .line 584863
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v0, v0

    .line 584864
    iget-object v1, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v1, v1

    .line 584865
    iget-object v2, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v2, v2

    .line 584866
    invoke-interface {v2}, LX/9uc;->S()Ljava/lang/String;

    move-result-object v2

    invoke-super {p0, v0, v1, v2, p2}, LX/2ja;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Cfc;)V

    .line 584867
    iget-object v0, p2, LX/Cfc;->name:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, LX/3Tu;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;Ljava/lang/String;Ljava/lang/String;)V

    .line 584868
    return-void
.end method

.method public final a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 584854
    invoke-static {p1}, LX/E1q;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/2nq;

    move-result-object v1

    .line 584855
    if-eqz v1, :cond_0

    invoke-interface {v1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-nez v0, :cond_1

    .line 584856
    :cond_0
    :goto_0
    return-void

    .line 584857
    :cond_1
    iget-object v0, p0, LX/3Tu;->g:LX/1rU;

    invoke-virtual {v0}, LX/1rU;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 584858
    :goto_1
    if-eqz v0, :cond_3

    .line 584859
    invoke-virtual {p0, v0, p2, p3}, LX/3Tu;->a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 584860
    :cond_2
    iget-object v0, p0, LX/3Tu;->b:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    invoke-interface {v1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->b(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    goto :goto_1

    .line 584861
    :cond_3
    iget-object v0, p0, LX/3Tu;->e:LX/1rn;

    invoke-interface {v1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, LX/1rn;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 584862
    iget-object v1, p0, LX/3Tu;->f:LX/0Sh;

    new-instance v2, LX/H54;

    invoke-direct {v2, p0, p2, p3}, LX/H54;-><init>(LX/3Tu;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0, v2}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    goto :goto_0
.end method
