.class public LX/3eQ;
.super LX/0ro;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0ro",
        "<",
        "Lcom/facebook/stickers/service/FetchStickerPackIdsParams;",
        "Lcom/facebook/stickers/service/FetchStickerPackIdsResult;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/3eQ;


# direct methods
.method public constructor <init>(LX/0sO;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 620008
    invoke-direct {p0, p1}, LX/0ro;-><init>(LX/0sO;)V

    .line 620009
    return-void
.end method

.method public static a(LX/0QB;)LX/3eQ;
    .locals 4

    .prologue
    .line 620065
    sget-object v0, LX/3eQ;->b:LX/3eQ;

    if-nez v0, :cond_1

    .line 620066
    const-class v1, LX/3eQ;

    monitor-enter v1

    .line 620067
    :try_start_0
    sget-object v0, LX/3eQ;->b:LX/3eQ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 620068
    if-eqz v2, :cond_0

    .line 620069
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 620070
    new-instance p0, LX/3eQ;

    invoke-static {v0}, LX/0sO;->a(LX/0QB;)LX/0sO;

    move-result-object v3

    check-cast v3, LX/0sO;

    invoke-direct {p0, v3}, LX/3eQ;-><init>(LX/0sO;)V

    .line 620071
    move-object v0, p0

    .line 620072
    sput-object v0, LX/3eQ;->b:LX/3eQ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 620073
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 620074
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 620075
    :cond_1
    sget-object v0, LX/3eQ;->b:LX/3eQ;

    return-object v0

    .line 620076
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 620077
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/1pN;LX/15w;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 620033
    check-cast p1, Lcom/facebook/stickers/service/FetchStickerPackIdsParams;

    const/4 v2, 0x0

    .line 620034
    sget-object v0, LX/8lu;->a:[I

    .line 620035
    iget-object v1, p1, Lcom/facebook/stickers/service/FetchStickerPackIdsParams;->a:LX/3do;

    move-object v1, v1

    .line 620036
    invoke-virtual {v1}, LX/3do;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 620037
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unrecognized sticker pack type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 620038
    iget-object v2, p1, Lcom/facebook/stickers/service/FetchStickerPackIdsParams;->a:LX/3do;

    move-object v2, v2

    .line 620039
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 620040
    :pswitch_0
    const-class v0, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchDownloadedStickerPackIdsQueryModel;

    invoke-virtual {p3, v0}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchDownloadedStickerPackIdsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchDownloadedStickerPackIdsQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v0

    const-class v3, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerPackIdFieldsModel;

    invoke-virtual {v1, v0, v2, v3}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_0
    move-object v1, v0

    .line 620041
    :goto_1
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 620042
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v4

    :goto_2
    if-ge v2, v4, :cond_6

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerPackIdFieldsModel;

    .line 620043
    iget-boolean v5, p1, Lcom/facebook/stickers/service/FetchStickerPackIdsParams;->c:Z

    move v5, v5

    .line 620044
    if-eqz v5, :cond_0

    .line 620045
    invoke-virtual {v0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerPackIdFieldsModel;->l()J

    move-result-wide v6

    .line 620046
    invoke-virtual {v0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerPackIdFieldsModel;->k()J

    move-result-wide v8

    .line 620047
    cmp-long v5, v6, v8

    if-eqz v5, :cond_1

    .line 620048
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerPackIdFieldsModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 620049
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 620050
    :cond_2
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 620051
    goto :goto_0

    .line 620052
    :pswitch_1
    const-class v0, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchOwnedStickerPackIdsQueryModel;

    invoke-virtual {p3, v0}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchOwnedStickerPackIdsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchOwnedStickerPackIdsQueryModel;->a()Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchOwnedStickerPackIdsQueryModel$StickerStoreModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchOwnedStickerPackIdsQueryModel$StickerStoreModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const-class v3, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerPackIdFieldsModel;

    invoke-virtual {v1, v0, v2, v3}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_3
    move-object v1, v0

    .line 620053
    goto :goto_1

    .line 620054
    :cond_3
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 620055
    goto :goto_3

    .line 620056
    :pswitch_2
    const-class v0, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchStoreStickerPackIdsQueryModel;

    invoke-virtual {p3, v0}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchStoreStickerPackIdsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchStoreStickerPackIdsQueryModel;->a()Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchStoreStickerPackIdsQueryModel$StickerStoreModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchStoreStickerPackIdsQueryModel$StickerStoreModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const-class v3, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerPackIdFieldsModel;

    invoke-virtual {v1, v0, v2, v3}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_4
    move-object v1, v0

    .line 620057
    goto :goto_1

    .line 620058
    :cond_4
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 620059
    goto :goto_4

    .line 620060
    :pswitch_3
    const-class v0, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchStoreStickerPackIdsQueryModel;

    invoke-virtual {p3, v0}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchStoreStickerPackIdsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchStoreStickerPackIdsQueryModel;->a()Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchStoreStickerPackIdsQueryModel$StickerStoreModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchStoreStickerPackIdsQueryModel$StickerStoreModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const-class v3, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerPackIdFieldsModel;

    invoke-virtual {v1, v0, v2, v3}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_5
    move-object v1, v0

    .line 620061
    goto/16 :goto_1

    .line 620062
    :cond_5
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 620063
    goto :goto_5

    .line 620064
    :cond_6
    new-instance v0, Lcom/facebook/stickers/service/FetchStickerPackIdsResult;

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/stickers/service/FetchStickerPackIdsResult;-><init>(LX/0Px;)V

    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final b(Ljava/lang/Object;LX/1pN;)I
    .locals 1

    .prologue
    .line 620032
    const/4 v0, 0x1

    return v0
.end method

.method public final f(Ljava/lang/Object;)LX/0gW;
    .locals 6

    .prologue
    .line 620010
    check-cast p1, Lcom/facebook/stickers/service/FetchStickerPackIdsParams;

    .line 620011
    sget-object v0, LX/8lu;->a:[I

    .line 620012
    iget-object v1, p1, Lcom/facebook/stickers/service/FetchStickerPackIdsParams;->a:LX/3do;

    move-object v1, v1

    .line 620013
    invoke-virtual {v1}, LX/3do;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 620014
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unrecognized sticker pack type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 620015
    iget-object v2, p1, Lcom/facebook/stickers/service/FetchStickerPackIdsParams;->a:LX/3do;

    move-object v2, v2

    .line 620016
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 620017
    :pswitch_0
    new-instance v0, LX/8jg;

    invoke-direct {v0}, LX/8jg;-><init>()V

    move-object v0, v0

    .line 620018
    const-string v1, "update_time"

    .line 620019
    iget-wide v4, p1, Lcom/facebook/stickers/service/FetchStickerPackIdsParams;->b:J

    move-wide v2, v4

    .line 620020
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    .line 620021
    :goto_0
    return-object v0

    .line 620022
    :pswitch_1
    new-instance v0, LX/8ji;

    invoke-direct {v0}, LX/8ji;-><init>()V

    move-object v0, v0

    .line 620023
    const-string v1, "update_time"

    .line 620024
    iget-wide v4, p1, Lcom/facebook/stickers/service/FetchStickerPackIdsParams;->b:J

    move-wide v2, v4

    .line 620025
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    goto :goto_0

    .line 620026
    :pswitch_2
    invoke-static {}, LX/8js;->j()LX/8jn;

    move-result-object v0

    const-string v1, "update_time"

    .line 620027
    iget-wide v4, p1, Lcom/facebook/stickers/service/FetchStickerPackIdsParams;->b:J

    move-wide v2, v4

    .line 620028
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    goto :goto_0

    .line 620029
    :pswitch_3
    invoke-static {}, LX/8js;->j()LX/8jn;

    move-result-object v0

    const-string v1, "update_time"

    .line 620030
    iget-wide v4, p1, Lcom/facebook/stickers/service/FetchStickerPackIdsParams;->b:J

    move-wide v2, v4

    .line 620031
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
