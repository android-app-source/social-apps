.class public LX/3g1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0dc;


# static fields
.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;

.field public static final c:LX/0Tn;

.field public static final d:LX/0Tn;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 623947
    sget-object v0, LX/0Tm;->d:LX/0Tn;

    const-string v1, "configuration_last_fetch_time_attempt_ms"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3g1;->a:LX/0Tn;

    .line 623948
    sget-object v0, LX/0Tm;->d:LX/0Tn;

    const-string v1, "configuration_last_fetch_time_success_ms"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3g1;->b:LX/0Tn;

    .line 623949
    sget-object v0, LX/0Tm;->d:LX/0Tn;

    const-string v1, "component_last_attempt/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3g1;->c:LX/0Tn;

    .line 623950
    sget-object v0, LX/0Tm;->d:LX/0Tn;

    const-string v1, "sync_component_last_attempt/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3g1;->d:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 623951
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 623952
    return-void
.end method


# virtual methods
.method public final b()LX/0Rf;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "LX/0Tn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 623953
    sget-object v0, LX/3g1;->a:LX/0Tn;

    sget-object v1, LX/3g1;->b:LX/0Tn;

    sget-object v2, LX/3g1;->c:LX/0Tn;

    sget-object v3, LX/3g1;->d:LX/0Tn;

    invoke-static {v0, v1, v2, v3}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method
