.class public LX/4La;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 684095
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 24

    .prologue
    .line 684096
    const-wide/16 v18, 0x0

    .line 684097
    const-wide/16 v16, 0x0

    .line 684098
    const/4 v14, 0x0

    .line 684099
    const-wide/16 v12, 0x0

    .line 684100
    const/4 v11, 0x0

    .line 684101
    const/4 v10, 0x0

    .line 684102
    const-wide/16 v8, 0x0

    .line 684103
    const/4 v7, 0x0

    .line 684104
    const/4 v6, 0x0

    .line 684105
    const/4 v5, 0x0

    .line 684106
    const/4 v4, 0x0

    .line 684107
    const/4 v3, 0x0

    .line 684108
    const/4 v2, 0x0

    .line 684109
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v15

    sget-object v20, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v20

    if-eq v15, v0, :cond_f

    .line 684110
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 684111
    const/4 v2, 0x0

    .line 684112
    :goto_0
    return v2

    .line 684113
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_9

    .line 684114
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 684115
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 684116
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_0

    if-eqz v2, :cond_0

    .line 684117
    const-string v6, "brightness"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 684118
    const/4 v2, 0x1

    .line 684119
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 684120
    :cond_1
    const-string v6, "contrast"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 684121
    const/4 v2, 0x1

    .line 684122
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v11, v2

    move-wide/from16 v20, v6

    goto :goto_1

    .line 684123
    :cond_2
    const-string v6, "display_name"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 684124
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v17, v2

    goto :goto_1

    .line 684125
    :cond_3
    const-string v6, "hue"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 684126
    const/4 v2, 0x1

    .line 684127
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v10, v2

    move-wide/from16 v18, v6

    goto :goto_1

    .line 684128
    :cond_4
    const-string v6, "hue_colorize"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 684129
    const/4 v2, 0x1

    .line 684130
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move v9, v2

    move/from16 v16, v6

    goto :goto_1

    .line 684131
    :cond_5
    const-string v6, "id"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 684132
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v13, v2

    goto/16 :goto_1

    .line 684133
    :cond_6
    const-string v6, "saturation"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 684134
    const/4 v2, 0x1

    .line 684135
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v8, v2

    move-wide v14, v6

    goto/16 :goto_1

    .line 684136
    :cond_7
    const-string v6, "url"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 684137
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v12, v2

    goto/16 :goto_1

    .line 684138
    :cond_8
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 684139
    :cond_9
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 684140
    if-eqz v3, :cond_a

    .line 684141
    const/4 v3, 0x0

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 684142
    :cond_a
    if-eqz v11, :cond_b

    .line 684143
    const/4 v3, 0x1

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v20

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 684144
    :cond_b
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 684145
    if-eqz v10, :cond_c

    .line 684146
    const/4 v3, 0x3

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v18

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 684147
    :cond_c
    if-eqz v9, :cond_d

    .line 684148
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 684149
    :cond_d
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 684150
    if-eqz v8, :cond_e

    .line 684151
    const/4 v3, 0x6

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide v4, v14

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 684152
    :cond_e
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 684153
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_f
    move-wide/from16 v20, v16

    move/from16 v16, v11

    move/from16 v17, v14

    move v11, v5

    move-wide v14, v8

    move v9, v3

    move v8, v2

    move v3, v6

    move-wide/from16 v22, v12

    move v12, v7

    move v13, v10

    move v10, v4

    move-wide/from16 v4, v18

    move-wide/from16 v18, v22

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 684154
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 684155
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 684156
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_0

    .line 684157
    const-string v2, "brightness"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 684158
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 684159
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 684160
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_1

    .line 684161
    const-string v2, "contrast"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 684162
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 684163
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 684164
    if-eqz v0, :cond_2

    .line 684165
    const-string v1, "display_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 684166
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 684167
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 684168
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_3

    .line 684169
    const-string v2, "hue"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 684170
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 684171
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 684172
    if-eqz v0, :cond_4

    .line 684173
    const-string v1, "hue_colorize"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 684174
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 684175
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 684176
    if-eqz v0, :cond_5

    .line 684177
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 684178
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 684179
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 684180
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_6

    .line 684181
    const-string v2, "saturation"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 684182
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 684183
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 684184
    if-eqz v0, :cond_7

    .line 684185
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 684186
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 684187
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 684188
    return-void
.end method
