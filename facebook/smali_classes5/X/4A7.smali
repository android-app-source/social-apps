.class public final LX/4A7;
.super LX/2uF;
.source ""


# instance fields
.field public final transient a:LX/15i;

.field public final transient b:I

.field public final transient c:I


# direct methods
.method public constructor <init>(LX/15i;II)V
    .locals 4

    .prologue
    .line 675292
    invoke-direct {p0}, LX/2uF;-><init>()V

    .line 675293
    invoke-static {p1, p2, p3}, LX/3Sm;->a(LX/15i;II)LX/1vs;

    move-result-object v0

    .line 675294
    iget-object v1, v0, LX/1vs;->a:LX/15i;

    .line 675295
    iget v2, v0, LX/1vs;->b:I

    .line 675296
    iget v0, v0, LX/1vs;->c:I

    .line 675297
    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    .line 675298
    :try_start_0
    iput-object v1, p0, LX/4A7;->a:LX/15i;

    .line 675299
    iput v2, p0, LX/4A7;->b:I

    .line 675300
    iput v0, p0, LX/4A7;->c:I

    .line 675301
    monitor-exit v3

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a(LX/3Sd;I)I
    .locals 4

    .prologue
    .line 675312
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 675313
    :try_start_0
    iget-object v0, p0, LX/4A7;->a:LX/15i;

    .line 675314
    iget v2, p0, LX/4A7;->b:I

    .line 675315
    iget v3, p0, LX/4A7;->c:I

    .line 675316
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 675317
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 675318
    :try_start_1
    invoke-virtual {p1, p2, v0}, LX/3Sd;->a(ILX/15i;)LX/15i;

    .line 675319
    invoke-virtual {p1, p2, v2}, LX/3Sd;->a(II)I

    .line 675320
    invoke-virtual {p1, p2, v3}, LX/3Sd;->b(II)I

    .line 675321
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 675322
    add-int/lit8 v0, p2, 0x1

    return v0

    .line 675323
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 675324
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public final a(I)LX/1vs;
    .locals 4

    .prologue
    .line 675304
    const/4 v0, 0x1

    invoke-static {p1, v0}, LX/0PB;->checkElementIndex(II)I

    .line 675305
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 675306
    :try_start_0
    iget-object v0, p0, LX/4A7;->a:LX/15i;

    .line 675307
    iget v2, p0, LX/4A7;->b:I

    .line 675308
    iget v3, p0, LX/4A7;->c:I

    .line 675309
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 675310
    invoke-static {v0, v2, v3}, LX/1vs;->a(LX/15i;II)LX/1vs;

    move-result-object v0

    return-object v0

    .line 675311
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(II)LX/2uF;
    .locals 1

    .prologue
    .line 675302
    const/4 v0, 0x1

    invoke-static {p1, p2, v0}, LX/0PB;->checkPositionIndexes(III)V

    .line 675303
    if-ne p1, p2, :cond_0

    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object p0

    :cond_0
    return-object p0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 675253
    const/4 v0, 0x0

    return v0
.end method

.method public final synthetic b()LX/2sN;
    .locals 1

    .prologue
    .line 675291
    invoke-virtual {p0}, LX/4A7;->e()LX/3Sh;

    move-result-object v0

    return-object v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 675325
    const/4 v0, 0x1

    return v0
.end method

.method public final e()LX/3Sh;
    .locals 4

    .prologue
    .line 675283
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 675284
    :try_start_0
    iget-object v0, p0, LX/4A7;->a:LX/15i;

    .line 675285
    iget v2, p0, LX/4A7;->b:I

    .line 675286
    iget v3, p0, LX/4A7;->c:I

    .line 675287
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 675288
    new-instance v1, LX/4A5;

    invoke-direct {v1, v0, v2, v3}, LX/4A5;-><init>(LX/15i;II)V

    move-object v0, v1

    .line 675289
    return-object v0

    .line 675290
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 675269
    if-ne p1, p0, :cond_1

    move v0, v1

    .line 675270
    :cond_0
    :goto_0
    return v0

    .line 675271
    :cond_1
    instance-of v2, p1, LX/4AK;

    if-eqz v2, :cond_0

    .line 675272
    check-cast p1, LX/3Sb;

    .line 675273
    invoke-interface {p1}, LX/3Sb;->c()I

    move-result v2

    if-ne v2, v1, :cond_0

    .line 675274
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 675275
    :try_start_0
    iget-object v2, p0, LX/4A7;->a:LX/15i;

    .line 675276
    iget v3, p0, LX/4A7;->b:I

    .line 675277
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 675278
    invoke-interface {p1, v0}, LX/3Sb;->a(I)LX/1vs;

    move-result-object v0

    .line 675279
    iget-object v1, v0, LX/1vs;->a:LX/15i;

    .line 675280
    iget v4, v0, LX/1vs;->b:I

    .line 675281
    invoke-static {v2, v3, v1, v4}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    goto :goto_0

    .line 675282
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 675268
    const/4 v0, 0x0

    return v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 675262
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 675263
    :try_start_0
    iget-object v0, p0, LX/4A7;->a:LX/15i;

    .line 675264
    iget v2, p0, LX/4A7;->b:I

    .line 675265
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 675266
    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x1f

    return v0

    .line 675267
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 675254
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 675255
    :try_start_0
    iget-object v0, p0, LX/4A7;->a:LX/15i;

    .line 675256
    iget v2, p0, LX/4A7;->b:I

    .line 675257
    iget v3, p0, LX/4A7;->c:I

    .line 675258
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 675259
    invoke-static {v0, v2, v3}, LX/1vu;->a(LX/15i;II)Ljava/lang/String;

    move-result-object v0

    .line 675260
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const/16 v2, 0x5b

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x5d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 675261
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
