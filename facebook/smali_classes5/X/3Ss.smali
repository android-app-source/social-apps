.class public final LX/3Ss;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1mW;


# instance fields
.field public final synthetic b:Lcom/facebook/auth/viewercontext/ViewerContext;

.field public final synthetic c:LX/0X3;


# direct methods
.method public constructor <init>(LX/0X3;Lcom/facebook/auth/viewercontext/ViewerContext;)V
    .locals 0

    .prologue
    .line 583075
    iput-object p1, p0, LX/3Ss;->c:LX/0X3;

    iput-object p2, p0, LX/3Ss;->b:Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final close()V
    .locals 3

    .prologue
    .line 583076
    iget-object v0, p0, LX/3Ss;->c:LX/0X3;

    invoke-virtual {v0}, LX/0X3;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    .line 583077
    iget-object v1, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v0, v1

    .line 583078
    iget-object v1, p0, LX/3Ss;->b:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 583079
    iget-object v2, v1, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v1, v2

    .line 583080
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 583081
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Attempting to close a PushedViewerContext while  another was pushed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 583082
    :cond_0
    iget-object v0, p0, LX/3Ss;->c:LX/0X3;

    invoke-virtual {v0}, LX/0X3;->f()V

    .line 583083
    return-void
.end method
