.class public abstract enum LX/50c;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/50c;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/50c;

.field public static final enum INVERTED_INSERTION_INDEX:LX/50c;

.field public static final enum NEXT_HIGHER:LX/50c;

.field public static final enum NEXT_LOWER:LX/50c;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 823996
    new-instance v0, LX/50d;

    const-string v1, "NEXT_LOWER"

    invoke-direct {v0, v1, v2}, LX/50d;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/50c;->NEXT_LOWER:LX/50c;

    .line 823997
    new-instance v0, LX/50e;

    const-string v1, "NEXT_HIGHER"

    invoke-direct {v0, v1, v3}, LX/50e;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/50c;->NEXT_HIGHER:LX/50c;

    .line 823998
    new-instance v0, LX/50f;

    const-string v1, "INVERTED_INSERTION_INDEX"

    invoke-direct {v0, v1, v4}, LX/50f;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/50c;->INVERTED_INSERTION_INDEX:LX/50c;

    .line 823999
    const/4 v0, 0x3

    new-array v0, v0, [LX/50c;

    sget-object v1, LX/50c;->NEXT_LOWER:LX/50c;

    aput-object v1, v0, v2

    sget-object v1, LX/50c;->NEXT_HIGHER:LX/50c;

    aput-object v1, v0, v3

    sget-object v1, LX/50c;->INVERTED_INSERTION_INDEX:LX/50c;

    aput-object v1, v0, v4

    sput-object v0, LX/50c;->$VALUES:[LX/50c;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 824000
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/50c;
    .locals 1

    .prologue
    .line 824001
    const-class v0, LX/50c;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/50c;

    return-object v0
.end method

.method public static values()[LX/50c;
    .locals 1

    .prologue
    .line 823995
    sget-object v0, LX/50c;->$VALUES:[LX/50c;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/50c;

    return-object v0
.end method


# virtual methods
.method public abstract resultIndex(I)I
.end method
