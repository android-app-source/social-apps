.class public abstract LX/4u6;
.super LX/4u5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<M:",
        "LX/4u6",
        "<TM;>;>",
        "LX/4u5;"
    }
.end annotation


# instance fields
.field public a:LX/4u8;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, LX/4u5;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, LX/4u6;->a:LX/4u8;

    if-eqz v1, :cond_0

    move v1, v0

    :goto_0
    iget-object v2, p0, LX/4u6;->a:LX/4u8;

    iget v3, v2, LX/4u8;->e:I

    move v2, v3

    if-ge v0, v2, :cond_1

    iget-object v2, p0, LX/4u6;->a:LX/4u8;

    invoke-virtual {v2, v0}, LX/4u8;->b(I)LX/4u9;

    move-result-object v2

    invoke-virtual {v2}, LX/4u9;->a()I

    move-result v2

    add-int/2addr v1, v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move v1, v0

    :cond_1
    return v1
.end method

.method public a(LX/4u4;)V
    .locals 3

    iget-object v0, p0, LX/4u6;->a:LX/4u8;

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, LX/4u6;->a:LX/4u8;

    iget v2, v1, LX/4u8;->e:I

    move v1, v2

    if-ge v0, v1, :cond_0

    iget-object v1, p0, LX/4u6;->a:LX/4u8;

    invoke-virtual {v1, v0}, LX/4u8;->b(I)LX/4u9;

    move-result-object v1

    invoke-virtual {v1, p1}, LX/4u9;->a(LX/4u4;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public final a(LX/4u2;I)Z
    .locals 7

    invoke-virtual {p1}, LX/4u2;->l()I

    move-result v0

    invoke-virtual {p1, p2}, LX/4u2;->b(I)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    ushr-int/lit8 v1, p2, 0x3

    move v1, v1

    invoke-virtual {p1}, LX/4u2;->l()I

    move-result v2

    sub-int/2addr v2, v0

    if-nez v2, :cond_4

    sget-object v3, LX/4uE;->h:[B

    :goto_1
    move-object v0, v3

    new-instance v2, LX/4uD;

    invoke-direct {v2, p2, v0}, LX/4uD;-><init>(I[B)V

    const/4 v0, 0x0

    iget-object v3, p0, LX/4u6;->a:LX/4u8;

    if-nez v3, :cond_2

    new-instance v3, LX/4u8;

    invoke-direct {v3}, LX/4u8;-><init>()V

    iput-object v3, p0, LX/4u6;->a:LX/4u8;

    :goto_2
    if-nez v0, :cond_1

    new-instance v0, LX/4u9;

    invoke-direct {v0}, LX/4u9;-><init>()V

    iget-object v3, p0, LX/4u6;->a:LX/4u8;

    const/4 p2, 0x0

    invoke-static {v3, v1}, LX/4u8;->e(LX/4u8;I)I

    move-result v4

    if-ltz v4, :cond_5

    iget-object v5, v3, LX/4u8;->d:[LX/4u9;

    aput-object v0, v5, v4

    :cond_1
    :goto_3
    iget-object v1, v0, LX/4u9;->c:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    iget-object v0, p0, LX/4u6;->a:LX/4u8;

    invoke-static {v0, v1}, LX/4u8;->e(LX/4u8;I)I

    move-result v3

    if-ltz v3, :cond_3

    iget-object v4, v0, LX/4u8;->d:[LX/4u9;

    aget-object v4, v4, v3

    sget-object v5, LX/4u8;->a:LX/4u9;

    if-ne v4, v5, :cond_9

    :cond_3
    const/4 v3, 0x0

    :goto_4
    move-object v0, v3

    goto :goto_2

    :cond_4
    new-array v3, v2, [B

    iget v4, p1, LX/4u2;->b:I

    add-int/2addr v4, v0

    iget-object v5, p1, LX/4u2;->a:[B

    const/4 v6, 0x0

    invoke-static {v5, v4, v3, v6, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_1

    :cond_5
    xor-int/lit8 v4, v4, -0x1

    iget v5, v3, LX/4u8;->e:I

    if-ge v4, v5, :cond_6

    iget-object v5, v3, LX/4u8;->d:[LX/4u9;

    aget-object v5, v5, v4

    sget-object v6, LX/4u8;->a:LX/4u9;

    if-ne v5, v6, :cond_6

    iget-object v5, v3, LX/4u8;->c:[I

    aput v1, v5, v4

    iget-object v5, v3, LX/4u8;->d:[LX/4u9;

    aput-object v0, v5, v4

    goto :goto_3

    :cond_6
    iget v5, v3, LX/4u8;->e:I

    iget-object v6, v3, LX/4u8;->c:[I

    array-length v6, v6

    if-lt v5, v6, :cond_7

    iget v5, v3, LX/4u8;->e:I

    add-int/lit8 v5, v5, 0x1

    invoke-static {v5}, LX/4u8;->c(I)I

    move-result v5

    new-array v6, v5, [I

    new-array v5, v5, [LX/4u9;

    iget-object p0, v3, LX/4u8;->c:[I

    iget-object p1, v3, LX/4u8;->c:[I

    array-length p1, p1

    invoke-static {p0, p2, v6, p2, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object p0, v3, LX/4u8;->d:[LX/4u9;

    iget-object p1, v3, LX/4u8;->d:[LX/4u9;

    array-length p1, p1

    invoke-static {p0, p2, v5, p2, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v6, v3, LX/4u8;->c:[I

    iput-object v5, v3, LX/4u8;->d:[LX/4u9;

    :cond_7
    iget v5, v3, LX/4u8;->e:I

    sub-int/2addr v5, v4

    if-eqz v5, :cond_8

    iget-object v5, v3, LX/4u8;->c:[I

    iget-object v6, v3, LX/4u8;->c:[I

    add-int/lit8 p0, v4, 0x1

    iget p1, v3, LX/4u8;->e:I

    sub-int/2addr p1, v4

    invoke-static {v5, v4, v6, p0, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v5, v3, LX/4u8;->d:[LX/4u9;

    iget-object v6, v3, LX/4u8;->d:[LX/4u9;

    add-int/lit8 p0, v4, 0x1

    iget p1, v3, LX/4u8;->e:I

    sub-int/2addr p1, v4

    invoke-static {v5, v4, v6, p0, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_8
    iget-object v5, v3, LX/4u8;->c:[I

    aput v1, v5, v4

    iget-object v5, v3, LX/4u8;->d:[LX/4u9;

    aput-object v0, v5, v4

    iget v4, v3, LX/4u8;->e:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v3, LX/4u8;->e:I

    goto/16 :goto_3

    :cond_9
    iget-object v4, v0, LX/4u8;->d:[LX/4u9;

    aget-object v3, v4, v3

    goto :goto_4
.end method

.method public final synthetic b()LX/4u5;
    .locals 1

    invoke-virtual {p0}, LX/4u5;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4u6;

    return-object v0
.end method

.method public final clone()Ljava/lang/Object;
    .locals 2

    invoke-super {p0}, LX/4u5;->b()LX/4u5;

    move-result-object v0

    check-cast v0, LX/4u6;

    iget-object v1, p0, LX/4u6;->a:LX/4u8;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/4u6;->a:LX/4u8;

    invoke-virtual {v1}, LX/4u8;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/4u8;

    iput-object v1, v0, LX/4u6;->a:LX/4u8;

    :cond_0
    return-object v0
.end method
