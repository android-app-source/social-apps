.class public final LX/3hJ;
.super LX/3Jv;
.source ""


# direct methods
.method public constructor <init>(LX/3Jx;[F)V
    .locals 0

    .prologue
    .line 626541
    invoke-direct {p0, p1, p2}, LX/3Jv;-><init>(LX/3Jx;[F)V

    .line 626542
    return-void
.end method


# virtual methods
.method public final a(LX/9Ua;)V
    .locals 2

    .prologue
    .line 626543
    iget-object v0, p0, LX/3Jv;->a:LX/3Jx;

    iget-object v1, p0, LX/3Jv;->b:[F

    invoke-virtual {p0, p1, v0, v1}, LX/3hJ;->a(LX/9Ua;LX/3Jx;[F)V

    .line 626544
    return-void
.end method

.method public final a(LX/9Ua;LX/3Jx;[F)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 626545
    sget-object v0, LX/3Jy;->b:[I

    invoke-virtual {p2}, LX/3Jx;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 626546
    new-instance v0, Ljava/lang/IllegalArgumentException;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "No such argument format %s"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p2, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 626547
    :pswitch_0
    aget v0, p3, v4

    aget v1, p3, v3

    .line 626548
    iget-object v2, p1, LX/9Ua;->a:Landroid/graphics/Path;

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 626549
    invoke-static {p1, v0, v1}, LX/9Ua;->f(LX/9Ua;FF)V

    .line 626550
    :goto_0
    return-void

    .line 626551
    :pswitch_1
    aget v0, p3, v4

    aget v1, p3, v3

    .line 626552
    iget-object v2, p1, LX/9Ua;->a:Landroid/graphics/Path;

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 626553
    invoke-static {p1, v0, v1}, LX/9Ua;->e(LX/9Ua;FF)V

    .line 626554
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
