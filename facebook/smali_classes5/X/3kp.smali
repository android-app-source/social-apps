.class public LX/3kp;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public b:I

.field private c:LX/0Tn;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 633302
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 633303
    const/4 v0, 0x1

    iput v0, p0, LX/3kp;->b:I

    .line 633304
    iput-object p1, p0, LX/3kp;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 633305
    return-void
.end method

.method public static a(LX/0QB;)LX/3kp;
    .locals 1

    .prologue
    .line 633316
    invoke-static {p0}, LX/3kp;->b(LX/0QB;)LX/3kp;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/3kp;
    .locals 2

    .prologue
    .line 633314
    new-instance v1, LX/3kp;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {v1, v0}, LX/3kp;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 633315
    return-object v1
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    .line 633317
    iget-object v0, p0, LX/3kp;->c:LX/0Tn;

    const-string v1, "prefKey was not set!"

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 633318
    iget-object v0, p0, LX/3kp;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    iget-object v1, p0, LX/3kp;->c:LX/0Tn;

    iget-object v2, p0, LX/3kp;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v3, p0, LX/3kp;->c:LX/0Tn;

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 633319
    return-void
.end method

.method public final a(LX/0Tn;)V
    .locals 0

    .prologue
    .line 633311
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 633312
    iput-object p1, p0, LX/3kp;->c:LX/0Tn;

    .line 633313
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 633308
    iget-object v0, p0, LX/3kp;->c:LX/0Tn;

    const-string v1, "prefKey was not set!"

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 633309
    iget-object v0, p0, LX/3kp;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    iget-object v1, p0, LX/3kp;->c:LX/0Tn;

    iget v2, p0, LX/3kp;->b:I

    invoke-interface {v0, v1, v2}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 633310
    return-void
.end method

.method public final c()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 633306
    iget-object v1, p0, LX/3kp;->c:LX/0Tn;

    const-string v2, "prefKey was not set!"

    invoke-static {v1, v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 633307
    iget v1, p0, LX/3kp;->b:I

    iget-object v2, p0, LX/3kp;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v3, p0, LX/3kp;->c:LX/0Tn;

    invoke-interface {v2, v3, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v2

    if-le v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method
