.class public abstract LX/3Wq;
.super Lcom/facebook/fbui/widget/layout/ImageBlockLayout;
.source ""

# interfaces
.implements LX/3Wr;
.implements LX/3Ws;
.implements LX/3Wt;


# static fields
.field public static A:Landroid/graphics/drawable/Drawable;

.field public static B:Landroid/graphics/drawable/Drawable;

.field private static C:Landroid/graphics/drawable/Drawable;

.field public static D:Landroid/graphics/drawable/Drawable;


# instance fields
.field public j:LX/1Uf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/work/groups/multicompany/bridge/MultiCompanyGroupIconProvider;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0wM;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:Lcom/facebook/translation/ui/TranslatableTextView;

.field public n:Ljava/lang/String;

.field public o:Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

.field public p:LX/8qo;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/9CG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/1zC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final s:LX/9HK;

.field public t:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public u:Lcom/facebook/resources/ui/FbTextView;

.field public v:Lcom/facebook/resources/ui/FbTextView;

.field private w:Landroid/view/View$OnTouchListener;

.field private x:Z

.field private y:Z

.field public z:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 591550
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 591551
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p3

    move-object v3, p0

    check-cast v3, LX/3Wq;

    invoke-static {p3}, LX/AjZ;->b(LX/0QB;)LX/AjZ;

    move-result-object v4

    check-cast v4, LX/8qo;

    invoke-static {p3}, LX/9CG;->a(LX/0QB;)LX/9CG;

    move-result-object v5

    check-cast v5, LX/9CG;

    invoke-static {p3}, LX/1zC;->a(LX/0QB;)LX/1zC;

    move-result-object v6

    check-cast v6, LX/1zC;

    invoke-static {p3}, LX/1Uf;->a(LX/0QB;)LX/1Uf;

    move-result-object v7

    check-cast v7, LX/1Uf;

    const/16 p2, 0x13b2

    invoke-static {p3, p2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p2

    const/16 v0, 0x5c8

    invoke-static {p3, v0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p3

    iput-object v4, v3, LX/3Wq;->p:LX/8qo;

    iput-object v5, v3, LX/3Wq;->q:LX/9CG;

    iput-object v6, v3, LX/3Wq;->r:LX/1zC;

    iput-object v7, v3, LX/3Wq;->j:LX/1Uf;

    iput-object p2, v3, LX/3Wq;->k:LX/0Ot;

    iput-object p3, v3, LX/3Wq;->l:LX/0Ot;

    .line 591552
    new-instance v0, LX/9HK;

    invoke-direct {v0, p1}, LX/9HK;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/3Wq;->s:LX/9HK;

    .line 591553
    iput-boolean v1, p0, LX/3Wq;->x:Z

    .line 591554
    iput-boolean v1, p0, LX/3Wq;->y:Z

    .line 591555
    iput-boolean v1, p0, LX/3Wq;->z:Z

    .line 591556
    return-void
.end method

.method public static g(LX/3Wq;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 591546
    iget-boolean v0, p0, LX/3Wq;->x:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/3Wq;->y:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/3Wq;->z:Z

    if-eqz v0, :cond_1

    .line 591547
    :cond_0
    new-instance v0, LX/46t;

    sget-object v2, LX/46s;->HORIZONTAL:LX/46s;

    sget-object v3, LX/46r;->CENTER:LX/46r;

    const/4 v4, 0x3

    new-array v4, v4, [Landroid/graphics/drawable/Drawable;

    const/4 v5, 0x0

    sget-object v6, LX/3Wq;->A:Landroid/graphics/drawable/Drawable;

    aput-object v6, v4, v5

    sget-object v5, LX/3Wq;->B:Landroid/graphics/drawable/Drawable;

    aput-object v5, v4, v7

    const/4 v5, 0x2

    sget-object v6, LX/3Wq;->C:Landroid/graphics/drawable/Drawable;

    aput-object v6, v4, v5

    invoke-direct {v0, v2, v3, v7, v4}, LX/46t;-><init>(LX/46s;LX/46r;I[Landroid/graphics/drawable/Drawable;)V

    .line 591548
    :goto_0
    iget-object v2, p0, LX/3Wq;->u:Lcom/facebook/resources/ui/FbTextView;

    invoke-static {v2, v1, v1, v0, v1}, LX/4lM;->a(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 591549
    return-void

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public static j(LX/3Wq;)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 591541
    sget-object v0, LX/3Wq;->C:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 591542
    iget-object v0, p0, LX/3Wq;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Cq;

    .line 591543
    const/4 p0, 0x0

    move-object v0, p0

    .line 591544
    sput-object v0, LX/3Wq;->C:Landroid/graphics/drawable/Drawable;

    .line 591545
    :cond_0
    sget-object v0, LX/3Wq;->C:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 3

    .prologue
    .line 591538
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0, p1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 591539
    iget-object v1, p0, LX/3Wq;->r:LX/1zC;

    iget-object v2, p0, LX/3Wq;->m:Lcom/facebook/translation/ui/TranslatableTextView;

    invoke-virtual {v2}, Lcom/facebook/translation/ui/TranslatableTextView;->getTextSize()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v1, v0, v2}, LX/1zC;->a(Landroid/text/Editable;I)Z

    .line 591540
    return-object v0
.end method

.method public final a(LX/9CP;)V
    .locals 1

    .prologue
    .line 591536
    iget-object v0, p0, LX/3Wq;->s:LX/9HK;

    invoke-virtual {v0, p1}, LX/9HK;->a(LX/9CP;)V

    .line 591537
    return-void
.end method

.method public final a(Landroid/animation/ValueAnimator;)V
    .locals 1

    .prologue
    .line 591520
    invoke-virtual {p0}, LX/3Wq;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-static {v0, p1}, LX/9CG;->a(Landroid/graphics/drawable/Drawable;Landroid/animation/ValueAnimator;)V

    .line 591521
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLProfile;Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 6
    .param p1    # Lcom/facebook/graphql/model/GraphQLProfile;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 591530
    iget-object v0, p0, LX/3Wq;->t:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p2, p3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 591531
    iget-object v0, p0, LX/3Wq;->p:LX/8qo;

    iget-object v1, p0, LX/3Wq;->t:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v2, 0x0

    invoke-interface {v0, v1, p1, v2}, LX/8qo;->a(Landroid/view/View;Lcom/facebook/graphql/model/GraphQLProfile;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 591532
    if-eqz p1, :cond_0

    .line 591533
    iget-object v0, p0, LX/3Wq;->t:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p0}, LX/3Wq;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f081233

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProfile;->D()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 591534
    :goto_0
    return-void

    .line 591535
    :cond_0
    iget-object v0, p0, LX/3Wq;->t:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 591528
    iget-object v0, p0, LX/3Wq;->s:LX/9HK;

    invoke-virtual {v0}, LX/9HK;->a()V

    .line 591529
    return-void
.end method

.method public final dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 591527
    iget-object v0, p0, LX/3Wq;->s:LX/9HK;

    invoke-virtual {v0, p1}, LX/9HK;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 591524
    invoke-super {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->draw(Landroid/graphics/Canvas;)V

    .line 591525
    iget-object v0, p0, LX/3Wq;->s:LX/9HK;

    invoke-virtual {v0, p1}, LX/9HK;->a(Landroid/graphics/Canvas;)V

    .line 591526
    return-void
.end method

.method public final hv_()V
    .locals 1

    .prologue
    .line 591522
    iget-object v0, p0, LX/3Wq;->m:Lcom/facebook/translation/ui/TranslatableTextView;

    invoke-virtual {v0}, Lcom/facebook/translation/ui/TranslatableTextView;->a()V

    .line 591523
    return-void
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 591516
    invoke-super {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 591517
    iget-object v0, p0, LX/3Wq;->w:Landroid/view/View$OnTouchListener;

    if-eqz v0, :cond_0

    .line 591518
    iget-object v0, p0, LX/3Wq;->w:Landroid/view/View$OnTouchListener;

    invoke-interface {v0, p0, p1}, Landroid/view/View$OnTouchListener;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    .line 591519
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public abstract setBody(Ljava/lang/CharSequence;)V
.end method

.method public setOnTouchListener(Landroid/view/View$OnTouchListener;)V
    .locals 0

    .prologue
    .line 591513
    invoke-super {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 591514
    iput-object p1, p0, LX/3Wq;->w:Landroid/view/View$OnTouchListener;

    .line 591515
    return-void
.end method

.method public setPinnedIconVisibility(Z)V
    .locals 1

    .prologue
    .line 591507
    iput-boolean p1, p0, LX/3Wq;->y:Z

    .line 591508
    if-eqz p1, :cond_0

    .line 591509
    sget-object v0, LX/3Wq;->B:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 591510
    invoke-virtual {p0}, LX/3Wq;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const p1, 0x7f020996

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sput-object v0, LX/3Wq;->B:Landroid/graphics/drawable/Drawable;

    .line 591511
    :cond_0
    invoke-static {p0}, LX/3Wq;->g(LX/3Wq;)V

    .line 591512
    return-void
.end method

.method public setVerifiedBadgeVisibility(Z)V
    .locals 1

    .prologue
    .line 591501
    iput-boolean p1, p0, LX/3Wq;->x:Z

    .line 591502
    if-eqz p1, :cond_0

    .line 591503
    sget-object v0, LX/3Wq;->A:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 591504
    invoke-virtual {p0}, LX/3Wq;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const p1, 0x7f021a25

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sput-object v0, LX/3Wq;->A:Landroid/graphics/drawable/Drawable;

    .line 591505
    :cond_0
    invoke-static {p0}, LX/3Wq;->g(LX/3Wq;)V

    .line 591506
    return-void
.end method
