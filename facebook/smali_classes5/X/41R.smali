.class public final LX/41R;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 666284
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;)Lcom/facebook/auth/login/CheckApprovedMachineMethod$ApprovalStatus;
    .locals 3

    .prologue
    .line 666285
    new-instance v0, Lcom/facebook/auth/login/CheckApprovedMachineMethod$ApprovalStatus;

    invoke-direct {v0}, Lcom/facebook/auth/login/CheckApprovedMachineMethod$ApprovalStatus;-><init>()V

    .line 666286
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v2, :cond_1

    .line 666287
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 666288
    const/4 v0, 0x0

    .line 666289
    :cond_0
    return-object v0

    .line 666290
    :cond_1
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_OBJECT:LX/15z;

    if-eq v1, v2, :cond_0

    .line 666291
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v1

    .line 666292
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 666293
    const-string v2, "approved"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 666294
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v0, Lcom/facebook/auth/login/CheckApprovedMachineMethod$ApprovalStatus;->a:Ljava/lang/Boolean;

    .line 666295
    :goto_1
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_0

    :cond_2
    goto :goto_1
.end method
