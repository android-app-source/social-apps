.class public final LX/3Se;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/3Sg;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "EMPTY_LIST_ITERATOR"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 582896
    new-instance v0, LX/3Sf;

    invoke-direct {v0}, LX/3Sf;-><init>()V

    sput-object v0, LX/3Se;->a:LX/3Sg;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 582897
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/2sN;LX/2sN;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 582898
    :cond_0
    invoke-interface {p0}, LX/2sN;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 582899
    invoke-interface {p1}, LX/2sN;->a()Z

    move-result v1

    if-nez v1, :cond_2

    .line 582900
    :cond_1
    :goto_0
    return v0

    .line 582901
    :cond_2
    invoke-interface {p0}, LX/2sN;->b()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v3, v1, LX/1vs;->b:I

    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 582902
    invoke-interface {p1}, LX/2sN;->b()LX/1vs;

    move-result-object v1

    iget-object v4, v1, LX/1vs;->a:LX/15i;

    iget v5, v1, LX/1vs;->b:I

    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 582903
    invoke-static {v2, v3, v4, v5}, LX/4A1;->a(LX/15i;ILX/15i;I)Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0

    .line 582904
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 582905
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    .line 582906
    :cond_3
    invoke-interface {p1}, LX/2sN;->a()Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v0, 0x1

    goto :goto_0
.end method
