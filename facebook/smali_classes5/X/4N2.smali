.class public LX/4N2;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 690173
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 36

    .prologue
    .line 690174
    const/16 v30, 0x0

    .line 690175
    const/16 v29, 0x0

    .line 690176
    const/16 v28, 0x0

    .line 690177
    const/16 v27, 0x0

    .line 690178
    const/16 v26, 0x0

    .line 690179
    const/16 v25, 0x0

    .line 690180
    const/16 v24, 0x0

    .line 690181
    const/16 v23, 0x0

    .line 690182
    const/16 v22, 0x0

    .line 690183
    const/16 v21, 0x0

    .line 690184
    const/16 v20, 0x0

    .line 690185
    const/16 v19, 0x0

    .line 690186
    const/16 v18, 0x0

    .line 690187
    const/16 v17, 0x0

    .line 690188
    const/16 v16, 0x0

    .line 690189
    const/4 v13, 0x0

    .line 690190
    const-wide/16 v14, 0x0

    .line 690191
    const/4 v12, 0x0

    .line 690192
    const/4 v11, 0x0

    .line 690193
    const/4 v10, 0x0

    .line 690194
    const/4 v9, 0x0

    .line 690195
    const/4 v8, 0x0

    .line 690196
    const/4 v7, 0x0

    .line 690197
    const/4 v6, 0x0

    .line 690198
    const/4 v5, 0x0

    .line 690199
    const/4 v4, 0x0

    .line 690200
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v31

    sget-object v32, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v31

    move-object/from16 v1, v32

    if-eq v0, v1, :cond_1c

    .line 690201
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 690202
    const/4 v4, 0x0

    .line 690203
    :goto_0
    return v4

    .line 690204
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 690205
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v31

    sget-object v32, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v31

    move-object/from16 v1, v32

    if-eq v0, v1, :cond_17

    .line 690206
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v31

    .line 690207
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 690208
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v32

    sget-object v33, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v32

    move-object/from16 v1, v33

    if-eq v0, v1, :cond_1

    if-eqz v31, :cond_1

    .line 690209
    const-string v32, "blurredCoverPhoto"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_2

    .line 690210
    invoke-static/range {p0 .. p1}, LX/2sX;->a(LX/15w;LX/186;)I

    move-result v30

    goto :goto_1

    .line 690211
    :cond_2
    const-string v32, "campaign_title"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_3

    .line 690212
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, p1

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v29

    goto :goto_1

    .line 690213
    :cond_3
    const-string v32, "can_donate"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_4

    .line 690214
    const/4 v9, 0x1

    .line 690215
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v28

    goto :goto_1

    .line 690216
    :cond_4
    const-string v32, "can_invite_to_campaign"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_5

    .line 690217
    const/4 v8, 0x1

    .line 690218
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v27

    goto :goto_1

    .line 690219
    :cond_5
    const-string v32, "cover_photo"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_6

    .line 690220
    invoke-static/range {p0 .. p1}, LX/2sX;->a(LX/15w;LX/186;)I

    move-result v26

    goto :goto_1

    .line 690221
    :cond_6
    const-string v32, "full_width_post_donation_image"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_7

    .line 690222
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v25

    goto :goto_1

    .line 690223
    :cond_7
    const-string v32, "fundraiser_detailed_progress_text"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_8

    .line 690224
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v24

    goto/16 :goto_1

    .line 690225
    :cond_8
    const-string v32, "fundraiser_for_charity_text"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_9

    .line 690226
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v23

    goto/16 :goto_1

    .line 690227
    :cond_9
    const-string v32, "fundraiser_page_subtitle"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_a

    .line 690228
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v22

    goto/16 :goto_1

    .line 690229
    :cond_a
    const-string v32, "fundraiser_progress_text"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_b

    .line 690230
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v21

    goto/16 :goto_1

    .line 690231
    :cond_b
    const-string v32, "has_goal_amount"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_c

    .line 690232
    const/4 v5, 0x1

    .line 690233
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v20

    goto/16 :goto_1

    .line 690234
    :cond_c
    const-string v32, "id"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_d

    .line 690235
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v19

    goto/16 :goto_1

    .line 690236
    :cond_d
    const-string v32, "imageHighOrig"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_e

    .line 690237
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v18

    goto/16 :goto_1

    .line 690238
    :cond_e
    const-string v32, "logo_image"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_f

    .line 690239
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v17

    goto/16 :goto_1

    .line 690240
    :cond_f
    const-string v32, "mobile_donate_url"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_10

    .line 690241
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    goto/16 :goto_1

    .line 690242
    :cond_10
    const-string v32, "name"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_11

    .line 690243
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    goto/16 :goto_1

    .line 690244
    :cond_11
    const-string v32, "percent_of_goal_reached"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_12

    .line 690245
    const/4 v4, 0x1

    .line 690246
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    goto/16 :goto_1

    .line 690247
    :cond_12
    const-string v32, "profilePictureHighRes"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_13

    .line 690248
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v14

    goto/16 :goto_1

    .line 690249
    :cond_13
    const-string v32, "profile_picture"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_14

    .line 690250
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v13

    goto/16 :goto_1

    .line 690251
    :cond_14
    const-string v32, "url"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_15

    .line 690252
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    goto/16 :goto_1

    .line 690253
    :cond_15
    const-string v32, "charity_interface"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_16

    .line 690254
    invoke-static/range {p0 .. p1}, LX/4LB;->a(LX/15w;LX/186;)I

    move-result v11

    goto/16 :goto_1

    .line 690255
    :cond_16
    const-string v32, "owner"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_0

    .line 690256
    invoke-static/range {p0 .. p1}, LX/2bM;->a(LX/15w;LX/186;)I

    move-result v10

    goto/16 :goto_1

    .line 690257
    :cond_17
    const/16 v31, 0x21

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 690258
    const/16 v31, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v31

    move/from16 v2, v30

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 690259
    const/16 v30, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v30

    move/from16 v2, v29

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 690260
    if-eqz v9, :cond_18

    .line 690261
    const/4 v9, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v9, v1}, LX/186;->a(IZ)V

    .line 690262
    :cond_18
    if-eqz v8, :cond_19

    .line 690263
    const/4 v8, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v8, v1}, LX/186;->a(IZ)V

    .line 690264
    :cond_19
    const/4 v8, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 690265
    const/16 v8, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 690266
    const/16 v8, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 690267
    const/16 v8, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 690268
    const/16 v8, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 690269
    const/16 v8, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 690270
    if-eqz v5, :cond_1a

    .line 690271
    const/16 v5, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v5, v1}, LX/186;->a(IZ)V

    .line 690272
    :cond_1a
    const/16 v5, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 690273
    const/16 v5, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 690274
    const/16 v5, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 690275
    const/16 v5, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 690276
    const/16 v5, 0x16

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v15}, LX/186;->b(II)V

    .line 690277
    if-eqz v4, :cond_1b

    .line 690278
    const/16 v5, 0x17

    const-wide/16 v8, 0x0

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, LX/186;->a(IDD)V

    .line 690279
    :cond_1b
    const/16 v4, 0x18

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v14}, LX/186;->b(II)V

    .line 690280
    const/16 v4, 0x19

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v13}, LX/186;->b(II)V

    .line 690281
    const/16 v4, 0x1a

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v12}, LX/186;->b(II)V

    .line 690282
    const/16 v4, 0x1b

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v11}, LX/186;->b(II)V

    .line 690283
    const/16 v4, 0x20

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v10}, LX/186;->b(II)V

    .line 690284
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v4

    goto/16 :goto_0

    :cond_1c
    move/from16 v34, v6

    move/from16 v35, v7

    move-wide v6, v14

    move v14, v12

    move v15, v13

    move v12, v10

    move v13, v11

    move v11, v9

    move v10, v8

    move/from16 v9, v35

    move/from16 v8, v34

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 690285
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 690286
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 690287
    if-eqz v0, :cond_0

    .line 690288
    const-string v1, "blurredCoverPhoto"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 690289
    invoke-static {p0, v0, p2, p3}, LX/2sX;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 690290
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 690291
    if-eqz v0, :cond_1

    .line 690292
    const-string v1, "campaign_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 690293
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 690294
    :cond_1
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 690295
    if-eqz v0, :cond_2

    .line 690296
    const-string v1, "can_donate"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 690297
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 690298
    :cond_2
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 690299
    if-eqz v0, :cond_3

    .line 690300
    const-string v1, "can_invite_to_campaign"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 690301
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 690302
    :cond_3
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 690303
    if-eqz v0, :cond_4

    .line 690304
    const-string v1, "cover_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 690305
    invoke-static {p0, v0, p2, p3}, LX/2sX;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 690306
    :cond_4
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 690307
    if-eqz v0, :cond_5

    .line 690308
    const-string v1, "full_width_post_donation_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 690309
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 690310
    :cond_5
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 690311
    if-eqz v0, :cond_6

    .line 690312
    const-string v1, "fundraiser_detailed_progress_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 690313
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 690314
    :cond_6
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 690315
    if-eqz v0, :cond_7

    .line 690316
    const-string v1, "fundraiser_for_charity_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 690317
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 690318
    :cond_7
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 690319
    if-eqz v0, :cond_8

    .line 690320
    const-string v1, "fundraiser_page_subtitle"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 690321
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 690322
    :cond_8
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 690323
    if-eqz v0, :cond_9

    .line 690324
    const-string v1, "fundraiser_progress_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 690325
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 690326
    :cond_9
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 690327
    if-eqz v0, :cond_a

    .line 690328
    const-string v1, "has_goal_amount"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 690329
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 690330
    :cond_a
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 690331
    if-eqz v0, :cond_b

    .line 690332
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 690333
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 690334
    :cond_b
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 690335
    if-eqz v0, :cond_c

    .line 690336
    const-string v1, "imageHighOrig"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 690337
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 690338
    :cond_c
    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 690339
    if-eqz v0, :cond_d

    .line 690340
    const-string v1, "logo_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 690341
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 690342
    :cond_d
    const/16 v0, 0x15

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 690343
    if-eqz v0, :cond_e

    .line 690344
    const-string v1, "mobile_donate_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 690345
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 690346
    :cond_e
    const/16 v0, 0x16

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 690347
    if-eqz v0, :cond_f

    .line 690348
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 690349
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 690350
    :cond_f
    const/16 v0, 0x17

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 690351
    cmpl-double v2, v0, v2

    if-eqz v2, :cond_10

    .line 690352
    const-string v2, "percent_of_goal_reached"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 690353
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 690354
    :cond_10
    const/16 v0, 0x18

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 690355
    if-eqz v0, :cond_11

    .line 690356
    const-string v1, "profilePictureHighRes"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 690357
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 690358
    :cond_11
    const/16 v0, 0x19

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 690359
    if-eqz v0, :cond_12

    .line 690360
    const-string v1, "profile_picture"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 690361
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 690362
    :cond_12
    const/16 v0, 0x1a

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 690363
    if-eqz v0, :cond_13

    .line 690364
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 690365
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 690366
    :cond_13
    const/16 v0, 0x1b

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 690367
    if-eqz v0, :cond_14

    .line 690368
    const-string v1, "charity_interface"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 690369
    invoke-static {p0, v0, p2, p3}, LX/4LB;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 690370
    :cond_14
    const/16 v0, 0x20

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 690371
    if-eqz v0, :cond_15

    .line 690372
    const-string v1, "owner"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 690373
    invoke-static {p0, v0, p2, p3}, LX/2bM;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 690374
    :cond_15
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 690375
    return-void
.end method
