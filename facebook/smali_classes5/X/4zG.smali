.class public LX/4zG;
.super Ljava/util/AbstractList;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/AbstractList",
        "<TT;>;"
    }
.end annotation


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 822614
    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    .line 822615
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, LX/4zG;->a:Ljava/util/List;

    .line 822616
    return-void
.end method

.method private a(I)I
    .locals 1

    .prologue
    .line 822599
    invoke-virtual {p0}, LX/4zG;->size()I

    move-result v0

    .line 822600
    invoke-static {p1, v0}, LX/0PB;->checkElementIndex(II)I

    .line 822601
    add-int/lit8 v0, v0, -0x1

    sub-int/2addr v0, p1

    return v0
.end method

.method public static b(LX/4zG;I)I
    .locals 1

    .prologue
    .line 822617
    invoke-virtual {p0}, LX/4zG;->size()I

    move-result v0

    .line 822618
    invoke-static {p1, v0}, LX/0PB;->checkPositionIndex(II)I

    .line 822619
    sub-int/2addr v0, p1

    return v0
.end method


# virtual methods
.method public final add(ILjava/lang/Object;)V
    .locals 2
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITT;)V"
        }
    .end annotation

    .prologue
    .line 822622
    iget-object v0, p0, LX/4zG;->a:Ljava/util/List;

    invoke-static {p0, p1}, LX/4zG;->b(LX/4zG;I)I

    move-result v1

    invoke-interface {v0, v1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 822623
    return-void
.end method

.method public final clear()V
    .locals 1

    .prologue
    .line 822620
    iget-object v0, p0, LX/4zG;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 822621
    return-void
.end method

.method public final get(I)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    .prologue
    .line 822612
    iget-object v0, p0, LX/4zG;->a:Ljava/util/List;

    invoke-direct {p0, p1}, LX/4zG;->a(I)I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 822613
    invoke-virtual {p0}, LX/4zG;->listIterator()Ljava/util/ListIterator;

    move-result-object v0

    return-object v0
.end method

.method public final listIterator(I)Ljava/util/ListIterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ListIterator",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 822609
    invoke-static {p0, p1}, LX/4zG;->b(LX/4zG;I)I

    move-result v0

    .line 822610
    iget-object v1, p0, LX/4zG;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v0

    .line 822611
    new-instance v1, LX/4zI;

    invoke-direct {v1, p0, v0}, LX/4zI;-><init>(LX/4zG;Ljava/util/ListIterator;)V

    return-object v1
.end method

.method public final remove(I)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    .prologue
    .line 822608
    iget-object v0, p0, LX/4zG;->a:Ljava/util/List;

    invoke-direct {p0, p1}, LX/4zG;->a(I)I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final removeRange(II)V
    .locals 1

    .prologue
    .line 822606
    invoke-virtual {p0, p1, p2}, LX/4zG;->subList(II)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 822607
    return-void
.end method

.method public final set(ILjava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITT;)TT;"
        }
    .end annotation

    .prologue
    .line 822605
    iget-object v0, p0, LX/4zG;->a:Ljava/util/List;

    invoke-direct {p0, p1}, LX/4zG;->a(I)I

    move-result v1

    invoke-interface {v0, v1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 822604
    iget-object v0, p0, LX/4zG;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final subList(II)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 822602
    invoke-virtual {p0}, LX/4zG;->size()I

    move-result v0

    invoke-static {p1, p2, v0}, LX/0PB;->checkPositionIndexes(III)V

    .line 822603
    iget-object v0, p0, LX/4zG;->a:Ljava/util/List;

    invoke-static {p0, p2}, LX/4zG;->b(LX/4zG;I)I

    move-result v1

    invoke-static {p0, p1}, LX/4zG;->b(LX/4zG;I)I

    move-result v2

    invoke-interface {v0, v1, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, LX/0R9;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
