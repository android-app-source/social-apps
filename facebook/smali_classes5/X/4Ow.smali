.class public LX/4Ow;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 699128
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 12

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 699129
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v3, :cond_a

    .line 699130
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 699131
    :goto_0
    return v0

    .line 699132
    :cond_0
    const-string v10, "is_checked_by_default"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 699133
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v2

    move v6, v2

    move v2, v1

    .line 699134
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_7

    .line 699135
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 699136
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 699137
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_1

    if-eqz v9, :cond_1

    .line 699138
    const-string v10, "checkbox_body"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 699139
    invoke-static {p0, p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 699140
    :cond_2
    const-string v10, "id"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 699141
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 699142
    :cond_3
    const-string v10, "is_required"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 699143
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v5, v0

    move v0, v1

    goto :goto_1

    .line 699144
    :cond_4
    const-string v10, "token_key"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 699145
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 699146
    :cond_5
    const-string v10, "url"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 699147
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 699148
    :cond_6
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 699149
    :cond_7
    const/4 v9, 0x7

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 699150
    invoke-virtual {p1, v1, v8}, LX/186;->b(II)V

    .line 699151
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 699152
    if-eqz v2, :cond_8

    .line 699153
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v6}, LX/186;->a(IZ)V

    .line 699154
    :cond_8
    if-eqz v0, :cond_9

    .line 699155
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v5}, LX/186;->a(IZ)V

    .line 699156
    :cond_9
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 699157
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 699158
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :cond_a
    move v2, v0

    move v3, v0

    move v4, v0

    move v5, v0

    move v6, v0

    move v7, v0

    move v8, v0

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 699159
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 699160
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 699161
    if-eqz v0, :cond_0

    .line 699162
    const-string v1, "checkbox_body"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 699163
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 699164
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 699165
    if-eqz v0, :cond_1

    .line 699166
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 699167
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 699168
    :cond_1
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 699169
    if-eqz v0, :cond_2

    .line 699170
    const-string v1, "is_checked_by_default"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 699171
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 699172
    :cond_2
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 699173
    if-eqz v0, :cond_3

    .line 699174
    const-string v1, "is_required"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 699175
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 699176
    :cond_3
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 699177
    if-eqz v0, :cond_4

    .line 699178
    const-string v1, "token_key"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 699179
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 699180
    :cond_4
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 699181
    if-eqz v0, :cond_5

    .line 699182
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 699183
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 699184
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 699185
    return-void
.end method
