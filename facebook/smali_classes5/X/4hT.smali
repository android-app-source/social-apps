.class public final LX/4hT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field public countryCodeSource_:LX/4hS;

.field public countryCode_:I

.field public extension_:Ljava/lang/String;

.field public hasCountryCode:Z

.field public hasCountryCodeSource:Z

.field public hasExtension:Z

.field public hasItalianLeadingZero:Z

.field public hasNationalNumber:Z

.field public hasNumberOfLeadingZeros:Z

.field public hasPreferredDomesticCarrierCode:Z

.field public hasRawInput:Z

.field public italianLeadingZero_:Z

.field public nationalNumber_:J

.field public numberOfLeadingZeros_:I

.field public preferredDomesticCarrierCode_:Ljava/lang/String;

.field public rawInput_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 801394
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 801395
    iput v2, p0, LX/4hT;->countryCode_:I

    .line 801396
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/4hT;->nationalNumber_:J

    .line 801397
    const-string v0, ""

    iput-object v0, p0, LX/4hT;->extension_:Ljava/lang/String;

    .line 801398
    iput-boolean v2, p0, LX/4hT;->italianLeadingZero_:Z

    .line 801399
    const/4 v0, 0x1

    iput v0, p0, LX/4hT;->numberOfLeadingZeros_:I

    .line 801400
    const-string v0, ""

    iput-object v0, p0, LX/4hT;->rawInput_:Ljava/lang/String;

    .line 801401
    const-string v0, ""

    iput-object v0, p0, LX/4hT;->preferredDomesticCarrierCode_:Ljava/lang/String;

    .line 801402
    sget-object v0, LX/4hS;->FROM_NUMBER_WITH_PLUS_SIGN:LX/4hS;

    iput-object v0, p0, LX/4hT;->countryCodeSource_:LX/4hS;

    .line 801403
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 7

    .prologue
    .line 801384
    instance-of v0, p1, LX/4hT;

    if-eqz v0, :cond_1

    check-cast p1, LX/4hT;

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 801385
    if-nez p1, :cond_2

    .line 801386
    :cond_0
    :goto_0
    move v0, v1

    .line 801387
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 801388
    :cond_2
    if-ne p0, p1, :cond_3

    move v1, v2

    .line 801389
    goto :goto_0

    .line 801390
    :cond_3
    iget v3, p0, LX/4hT;->countryCode_:I

    iget v4, p1, LX/4hT;->countryCode_:I

    if-ne v3, v4, :cond_0

    iget-wide v3, p0, LX/4hT;->nationalNumber_:J

    iget-wide v5, p1, LX/4hT;->nationalNumber_:J

    cmp-long v3, v3, v5

    if-nez v3, :cond_0

    iget-object v3, p0, LX/4hT;->extension_:Ljava/lang/String;

    iget-object v4, p1, LX/4hT;->extension_:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-boolean v3, p0, LX/4hT;->italianLeadingZero_:Z

    iget-boolean v4, p1, LX/4hT;->italianLeadingZero_:Z

    if-ne v3, v4, :cond_0

    iget v3, p0, LX/4hT;->numberOfLeadingZeros_:I

    iget v4, p1, LX/4hT;->numberOfLeadingZeros_:I

    if-ne v3, v4, :cond_0

    iget-object v3, p0, LX/4hT;->rawInput_:Ljava/lang/String;

    iget-object v4, p1, LX/4hT;->rawInput_:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, LX/4hT;->countryCodeSource_:LX/4hS;

    iget-object v4, p1, LX/4hT;->countryCodeSource_:LX/4hS;

    if-ne v3, v4, :cond_0

    iget-object v3, p0, LX/4hT;->preferredDomesticCarrierCode_:Ljava/lang/String;

    iget-object v4, p1, LX/4hT;->preferredDomesticCarrierCode_:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 801391
    iget-boolean v3, p0, LX/4hT;->hasPreferredDomesticCarrierCode:Z

    move v3, v3

    .line 801392
    iget-boolean v4, p1, LX/4hT;->hasPreferredDomesticCarrierCode:Z

    move v4, v4

    .line 801393
    if-ne v3, v4, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public getCountryCode()I
    .locals 1

    .prologue
    .line 801345
    iget v0, p0, LX/4hT;->countryCode_:I

    return v0
.end method

.method public getNationalNumber()J
    .locals 2

    .prologue
    .line 801383
    iget-wide v0, p0, LX/4hT;->nationalNumber_:J

    return-wide v0
.end method

.method public hashCode()I
    .locals 8

    .prologue
    const/16 v2, 0x4d5

    const/16 v1, 0x4cf

    .line 801354
    iget v0, p0, LX/4hT;->countryCode_:I

    move v0, v0

    .line 801355
    add-int/lit16 v0, v0, 0x87d

    .line 801356
    mul-int/lit8 v0, v0, 0x35

    .line 801357
    iget-wide v6, p0, LX/4hT;->nationalNumber_:J

    move-wide v4, v6

    .line 801358
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->hashCode()I

    move-result v3

    add-int/2addr v0, v3

    .line 801359
    mul-int/lit8 v0, v0, 0x35

    .line 801360
    iget-object v3, p0, LX/4hT;->extension_:Ljava/lang/String;

    move-object v3, v3

    .line 801361
    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    add-int/2addr v0, v3

    .line 801362
    mul-int/lit8 v3, v0, 0x35

    .line 801363
    iget-boolean v0, p0, LX/4hT;->italianLeadingZero_:Z

    move v0, v0

    .line 801364
    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v3

    .line 801365
    mul-int/lit8 v0, v0, 0x35

    .line 801366
    iget v3, p0, LX/4hT;->numberOfLeadingZeros_:I

    move v3, v3

    .line 801367
    add-int/2addr v0, v3

    .line 801368
    mul-int/lit8 v0, v0, 0x35

    .line 801369
    iget-object v3, p0, LX/4hT;->rawInput_:Ljava/lang/String;

    move-object v3, v3

    .line 801370
    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    add-int/2addr v0, v3

    .line 801371
    mul-int/lit8 v0, v0, 0x35

    .line 801372
    iget-object v3, p0, LX/4hT;->countryCodeSource_:LX/4hS;

    move-object v3, v3

    .line 801373
    invoke-virtual {v3}, LX/4hS;->hashCode()I

    move-result v3

    add-int/2addr v0, v3

    .line 801374
    mul-int/lit8 v0, v0, 0x35

    .line 801375
    iget-object v3, p0, LX/4hT;->preferredDomesticCarrierCode_:Ljava/lang/String;

    move-object v3, v3

    .line 801376
    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    add-int/2addr v0, v3

    .line 801377
    mul-int/lit8 v0, v0, 0x35

    .line 801378
    iget-boolean v3, p0, LX/4hT;->hasPreferredDomesticCarrierCode:Z

    move v3, v3

    .line 801379
    if-eqz v3, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 801380
    return v0

    :cond_0
    move v0, v2

    .line 801381
    goto :goto_0

    :cond_1
    move v1, v2

    .line 801382
    goto :goto_1
.end method

.method public setCountryCode(I)LX/4hT;
    .locals 1

    .prologue
    .line 801351
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4hT;->hasCountryCode:Z

    .line 801352
    iput p1, p0, LX/4hT;->countryCode_:I

    .line 801353
    return-object p0
.end method

.method public setCountryCodeSource(LX/4hS;)LX/4hT;
    .locals 1

    .prologue
    .line 801346
    if-nez p1, :cond_0

    .line 801347
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 801348
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4hT;->hasCountryCodeSource:Z

    .line 801349
    iput-object p1, p0, LX/4hT;->countryCodeSource_:LX/4hS;

    .line 801350
    return-object p0
.end method

.method public setExtension(Ljava/lang/String;)LX/4hT;
    .locals 1

    .prologue
    .line 801404
    if-nez p1, :cond_0

    .line 801405
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 801406
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4hT;->hasExtension:Z

    .line 801407
    iput-object p1, p0, LX/4hT;->extension_:Ljava/lang/String;

    .line 801408
    return-object p0
.end method

.method public setItalianLeadingZero(Z)LX/4hT;
    .locals 1

    .prologue
    .line 801311
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4hT;->hasItalianLeadingZero:Z

    .line 801312
    iput-boolean p1, p0, LX/4hT;->italianLeadingZero_:Z

    .line 801313
    return-object p0
.end method

.method public setNationalNumber(J)LX/4hT;
    .locals 1

    .prologue
    .line 801305
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4hT;->hasNationalNumber:Z

    .line 801306
    iput-wide p1, p0, LX/4hT;->nationalNumber_:J

    .line 801307
    return-object p0
.end method

.method public setNumberOfLeadingZeros(I)LX/4hT;
    .locals 1

    .prologue
    .line 801308
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4hT;->hasNumberOfLeadingZeros:Z

    .line 801309
    iput p1, p0, LX/4hT;->numberOfLeadingZeros_:I

    .line 801310
    return-object p0
.end method

.method public setPreferredDomesticCarrierCode(Ljava/lang/String;)LX/4hT;
    .locals 1

    .prologue
    .line 801314
    if-nez p1, :cond_0

    .line 801315
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 801316
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4hT;->hasPreferredDomesticCarrierCode:Z

    .line 801317
    iput-object p1, p0, LX/4hT;->preferredDomesticCarrierCode_:Ljava/lang/String;

    .line 801318
    return-object p0
.end method

.method public setRawInput(Ljava/lang/String;)LX/4hT;
    .locals 1

    .prologue
    .line 801319
    if-nez p1, :cond_0

    .line 801320
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 801321
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4hT;->hasRawInput:Z

    .line 801322
    iput-object p1, p0, LX/4hT;->rawInput_:Ljava/lang/String;

    .line 801323
    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 801324
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 801325
    const-string v1, "Country Code: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, LX/4hT;->countryCode_:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 801326
    const-string v1, " National Number: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, LX/4hT;->nationalNumber_:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 801327
    iget-boolean v1, p0, LX/4hT;->hasItalianLeadingZero:Z

    move v1, v1

    .line 801328
    if-eqz v1, :cond_0

    .line 801329
    iget-boolean v1, p0, LX/4hT;->italianLeadingZero_:Z

    move v1, v1

    .line 801330
    if-eqz v1, :cond_0

    .line 801331
    const-string v1, " Leading Zero(s): true"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 801332
    :cond_0
    iget-boolean v1, p0, LX/4hT;->hasNumberOfLeadingZeros:Z

    move v1, v1

    .line 801333
    if-eqz v1, :cond_1

    .line 801334
    const-string v1, " Number of leading zeros: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, LX/4hT;->numberOfLeadingZeros_:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 801335
    :cond_1
    iget-boolean v1, p0, LX/4hT;->hasExtension:Z

    move v1, v1

    .line 801336
    if-eqz v1, :cond_2

    .line 801337
    const-string v1, " Extension: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/4hT;->extension_:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 801338
    :cond_2
    iget-boolean v1, p0, LX/4hT;->hasCountryCodeSource:Z

    move v1, v1

    .line 801339
    if-eqz v1, :cond_3

    .line 801340
    const-string v1, " Country Code Source: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/4hT;->countryCodeSource_:LX/4hS;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 801341
    :cond_3
    iget-boolean v1, p0, LX/4hT;->hasPreferredDomesticCarrierCode:Z

    move v1, v1

    .line 801342
    if-eqz v1, :cond_4

    .line 801343
    const-string v1, " Preferred Domestic Carrier Code: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/4hT;->preferredDomesticCarrierCode_:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 801344
    :cond_4
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
