.class public final enum LX/3bj;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/3bj;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/3bj;

.field public static final enum ns_interest:LX/3bj;

.field public static final enum ns_local:LX/3bj;

.field public static final enum ns_pulse:LX/3bj;

.field public static final enum ns_social:LX/3bj;

.field public static final enum ns_suggested:LX/3bj;

.field public static final enum ns_top:LX/3bj;

.field public static final enum ns_trending:LX/3bj;

.field public static final enum recent_search:LX/3bj;

.field public static final enum unset:LX/3bj;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 611999
    new-instance v0, LX/3bj;

    const-string v1, "ns_pulse"

    invoke-direct {v0, v1, v3}, LX/3bj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3bj;->ns_pulse:LX/3bj;

    .line 612000
    new-instance v0, LX/3bj;

    const-string v1, "ns_trending"

    invoke-direct {v0, v1, v4}, LX/3bj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3bj;->ns_trending:LX/3bj;

    .line 612001
    new-instance v0, LX/3bj;

    const-string v1, "ns_social"

    invoke-direct {v0, v1, v5}, LX/3bj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3bj;->ns_social:LX/3bj;

    .line 612002
    new-instance v0, LX/3bj;

    const-string v1, "ns_interest"

    invoke-direct {v0, v1, v6}, LX/3bj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3bj;->ns_interest:LX/3bj;

    .line 612003
    new-instance v0, LX/3bj;

    const-string v1, "ns_local"

    invoke-direct {v0, v1, v7}, LX/3bj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3bj;->ns_local:LX/3bj;

    .line 612004
    new-instance v0, LX/3bj;

    const-string v1, "ns_suggested"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/3bj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3bj;->ns_suggested:LX/3bj;

    .line 612005
    new-instance v0, LX/3bj;

    const-string v1, "recent_search"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/3bj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3bj;->recent_search:LX/3bj;

    .line 612006
    new-instance v0, LX/3bj;

    const-string v1, "ns_top"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/3bj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3bj;->ns_top:LX/3bj;

    .line 612007
    new-instance v0, LX/3bj;

    const-string v1, "unset"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/3bj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3bj;->unset:LX/3bj;

    .line 612008
    const/16 v0, 0x9

    new-array v0, v0, [LX/3bj;

    sget-object v1, LX/3bj;->ns_pulse:LX/3bj;

    aput-object v1, v0, v3

    sget-object v1, LX/3bj;->ns_trending:LX/3bj;

    aput-object v1, v0, v4

    sget-object v1, LX/3bj;->ns_social:LX/3bj;

    aput-object v1, v0, v5

    sget-object v1, LX/3bj;->ns_interest:LX/3bj;

    aput-object v1, v0, v6

    sget-object v1, LX/3bj;->ns_local:LX/3bj;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/3bj;->ns_suggested:LX/3bj;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/3bj;->recent_search:LX/3bj;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/3bj;->ns_top:LX/3bj;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/3bj;->unset:LX/3bj;

    aput-object v2, v0, v1

    sput-object v0, LX/3bj;->$VALUES:[LX/3bj;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 612009
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/3bj;
    .locals 1

    .prologue
    .line 612010
    const-class v0, LX/3bj;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/3bj;

    return-object v0
.end method

.method public static values()[LX/3bj;
    .locals 1

    .prologue
    .line 612011
    sget-object v0, LX/3bj;->$VALUES:[LX/3bj;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/3bj;

    return-object v0
.end method
