.class public LX/4Og;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 697653
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 16

    .prologue
    .line 697654
    const/4 v12, 0x0

    .line 697655
    const/4 v11, 0x0

    .line 697656
    const/4 v10, 0x0

    .line 697657
    const/4 v9, 0x0

    .line 697658
    const/4 v8, 0x0

    .line 697659
    const/4 v7, 0x0

    .line 697660
    const/4 v6, 0x0

    .line 697661
    const/4 v5, 0x0

    .line 697662
    const/4 v4, 0x0

    .line 697663
    const/4 v3, 0x0

    .line 697664
    const/4 v2, 0x0

    .line 697665
    const/4 v1, 0x0

    .line 697666
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->START_OBJECT:LX/15z;

    if-eq v13, v14, :cond_1

    .line 697667
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 697668
    const/4 v1, 0x0

    .line 697669
    :goto_0
    return v1

    .line 697670
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 697671
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->END_OBJECT:LX/15z;

    if-eq v13, v14, :cond_d

    .line 697672
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v13

    .line 697673
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 697674
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v14, v15, :cond_1

    if-eqz v13, :cond_1

    .line 697675
    const-string v14, "feedback"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 697676
    invoke-static/range {p0 .. p1}, LX/2bG;->a(LX/15w;LX/186;)I

    move-result v12

    goto :goto_1

    .line 697677
    :cond_2
    const-string v14, "fullLatestVersion"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 697678
    invoke-static/range {p0 .. p1}, LX/4Oh;->a(LX/15w;LX/186;)I

    move-result v11

    goto :goto_1

    .line 697679
    :cond_3
    const-string v14, "global_share"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_4

    .line 697680
    invoke-static/range {p0 .. p1}, LX/4MV;->a(LX/15w;LX/186;)I

    move-result v10

    goto :goto_1

    .line 697681
    :cond_4
    const-string v14, "id"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 697682
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 697683
    :cond_5
    const-string v14, "instant_article_edge"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 697684
    invoke-static/range {p0 .. p1}, LX/4Oh;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 697685
    :cond_6
    const-string v14, "latest_version"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_7

    .line 697686
    invoke-static/range {p0 .. p1}, LX/4Oh;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 697687
    :cond_7
    const-string v14, "messenger_content_subscription_option"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_8

    .line 697688
    invoke-static/range {p0 .. p1}, LX/4PW;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 697689
    :cond_8
    const-string v14, "owner_id"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_9

    .line 697690
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto/16 :goto_1

    .line 697691
    :cond_9
    const-string v14, "relatedArticleVersion"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_a

    .line 697692
    invoke-static/range {p0 .. p1}, LX/4Oh;->a(LX/15w;LX/186;)I

    move-result v4

    goto/16 :goto_1

    .line 697693
    :cond_a
    const-string v14, "url"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_b

    .line 697694
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto/16 :goto_1

    .line 697695
    :cond_b
    const-string v14, "relatedArticleBlingBarSummary"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_c

    .line 697696
    invoke-static/range {p0 .. p1}, LX/2bG;->a(LX/15w;LX/186;)I

    move-result v2

    goto/16 :goto_1

    .line 697697
    :cond_c
    const-string v14, "viewer_last_read_block_id"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 697698
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    goto/16 :goto_1

    .line 697699
    :cond_d
    const/16 v13, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->c(I)V

    .line 697700
    const/4 v13, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v12}, LX/186;->b(II)V

    .line 697701
    const/4 v12, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v11}, LX/186;->b(II)V

    .line 697702
    const/4 v11, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v10}, LX/186;->b(II)V

    .line 697703
    const/4 v10, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v9}, LX/186;->b(II)V

    .line 697704
    const/4 v9, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v8}, LX/186;->b(II)V

    .line 697705
    const/4 v8, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v7}, LX/186;->b(II)V

    .line 697706
    const/4 v7, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v6}, LX/186;->b(II)V

    .line 697707
    const/16 v6, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v5}, LX/186;->b(II)V

    .line 697708
    const/16 v5, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v4}, LX/186;->b(II)V

    .line 697709
    const/16 v4, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 697710
    const/16 v3, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->b(II)V

    .line 697711
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 697712
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 697713
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 697714
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 697715
    if-eqz v0, :cond_0

    .line 697716
    const-string v1, "feedback"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 697717
    invoke-static {p0, v0, p2, p3}, LX/2bG;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 697718
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 697719
    if-eqz v0, :cond_1

    .line 697720
    const-string v1, "fullLatestVersion"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 697721
    invoke-static {p0, v0, p2, p3}, LX/4Oh;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 697722
    :cond_1
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 697723
    if-eqz v0, :cond_2

    .line 697724
    const-string v1, "global_share"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 697725
    invoke-static {p0, v0, p2, p3}, LX/4MV;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 697726
    :cond_2
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 697727
    if-eqz v0, :cond_3

    .line 697728
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 697729
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 697730
    :cond_3
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 697731
    if-eqz v0, :cond_4

    .line 697732
    const-string v1, "instant_article_edge"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 697733
    invoke-static {p0, v0, p2, p3}, LX/4Oh;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 697734
    :cond_4
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 697735
    if-eqz v0, :cond_5

    .line 697736
    const-string v1, "latest_version"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 697737
    invoke-static {p0, v0, p2, p3}, LX/4Oh;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 697738
    :cond_5
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 697739
    if-eqz v0, :cond_6

    .line 697740
    const-string v1, "messenger_content_subscription_option"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 697741
    invoke-static {p0, v0, p2, p3}, LX/4PW;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 697742
    :cond_6
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 697743
    if-eqz v0, :cond_7

    .line 697744
    const-string v1, "owner_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 697745
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 697746
    :cond_7
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 697747
    if-eqz v0, :cond_8

    .line 697748
    const-string v1, "relatedArticleVersion"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 697749
    invoke-static {p0, v0, p2, p3}, LX/4Oh;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 697750
    :cond_8
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 697751
    if-eqz v0, :cond_9

    .line 697752
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 697753
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 697754
    :cond_9
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 697755
    if-eqz v0, :cond_a

    .line 697756
    const-string v1, "relatedArticleBlingBarSummary"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 697757
    invoke-static {p0, v0, p2, p3}, LX/2bG;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 697758
    :cond_a
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 697759
    if-eqz v0, :cond_b

    .line 697760
    const-string v1, "viewer_last_read_block_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 697761
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 697762
    :cond_b
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 697763
    return-void
.end method
