.class public abstract LX/46Z;
.super LX/1ci;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1ci",
        "<",
        "LX/1FJ",
        "<",
        "LX/1ln;",
        ">;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 671293
    invoke-direct {p0}, LX/1ci;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a(Landroid/graphics/Bitmap;)V
    .param p1    # Landroid/graphics/Bitmap;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
.end method

.method public final e(LX/1ca;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 671294
    invoke-interface {p1}, LX/1ca;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 671295
    :goto_0
    return-void

    .line 671296
    :cond_0
    invoke-interface {p1}, LX/1ca;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FJ;

    .line 671297
    const/4 v1, 0x0

    .line 671298
    if-eqz v0, :cond_1

    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v2

    instance-of v2, v2, LX/1lm;

    if-eqz v2, :cond_1

    .line 671299
    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1lm;

    invoke-virtual {v1}, LX/1lm;->a()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 671300
    :cond_1
    :try_start_0
    invoke-virtual {p0, v1}, LX/46Z;->a(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 671301
    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    throw v1
.end method
