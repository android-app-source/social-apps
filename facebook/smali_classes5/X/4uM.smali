.class public final LX/4uM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1qg;


# instance fields
.field public final a:I

.field public final b:LX/2wX;

.field public final c:LX/1qg;

.field public final synthetic d:LX/4uP;


# direct methods
.method public constructor <init>(LX/4uP;ILX/2wX;LX/1qg;)V
    .locals 0

    iput-object p1, p0, LX/4uM;->d:LX/4uP;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, LX/4uM;->a:I

    iput-object p3, p0, LX/4uM;->b:LX/2wX;

    iput-object p4, p0, LX/4uM;->c:LX/1qg;

    invoke-virtual {p3, p0}, LX/2wX;->a(LX/1qg;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 3
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1b

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "beginFailureResolution for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, LX/4uM;->d:LX/4uP;

    iget v1, p0, LX/4uM;->a:I

    invoke-virtual {v0, p1, v1}, LX/4uO;->b(Lcom/google/android/gms/common/ConnectionResult;I)V

    return-void
.end method
