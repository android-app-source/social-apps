.class public LX/4Pv;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 702129
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 10

    .prologue
    const-wide/16 v4, 0x0

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 702130
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 702131
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 702132
    :goto_0
    return v1

    .line 702133
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_2

    .line 702134
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 702135
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 702136
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_0

    if-eqz v7, :cond_0

    .line 702137
    const-string v8, "fetchTimeMs"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 702138
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    move v0, v6

    goto :goto_1

    .line 702139
    :cond_1
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 702140
    :cond_2
    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 702141
    if-eqz v0, :cond_3

    move-object v0, p1

    .line 702142
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 702143
    :cond_3
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move-wide v2, v4

    goto :goto_1
.end method

.method public static a(LX/15w;S)LX/15i;
    .locals 5

    .prologue
    .line 702118
    const/4 v0, 0x1

    const/4 v4, 0x0

    .line 702119
    new-instance v2, LX/186;

    const/16 v1, 0x80

    invoke-direct {v2, v1}, LX/186;-><init>(I)V

    .line 702120
    invoke-static {p0, v2}, LX/4Pv;->a(LX/15w;LX/186;)I

    move-result v1

    .line 702121
    if-eqz v0, :cond_0

    .line 702122
    const/4 v3, 0x2

    invoke-virtual {v2, v3}, LX/186;->c(I)V

    .line 702123
    invoke-virtual {v2, v4, p1, v4}, LX/186;->a(ISI)V

    .line 702124
    const/4 v3, 0x1

    invoke-virtual {v2, v3, v1}, LX/186;->b(II)V

    .line 702125
    invoke-virtual {v2}, LX/186;->d()I

    move-result v1

    .line 702126
    :cond_0
    invoke-virtual {v2, v1}, LX/186;->d(I)V

    .line 702127
    invoke-static {v2}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v1

    move-object v0, v1

    .line 702128
    return-object v0
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 702107
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 702108
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 702109
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 702110
    const-string v0, "name"

    const-string v1, "NoContentGoodFriendsFeedUnit"

    invoke-virtual {p2, v0, v1}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 702111
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 702112
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 702113
    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    .line 702114
    const-string v2, "fetchTimeMs"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 702115
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 702116
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 702117
    return-void
.end method
