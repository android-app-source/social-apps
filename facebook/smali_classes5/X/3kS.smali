.class public LX/3kS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0i1;


# static fields
.field public static final a:LX/0Tn;


# instance fields
.field private b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private c:Landroid/content/res/Resources;

.field public d:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 632950
    sget-object v0, LX/0Tm;->g:LX/0Tn;

    const-string v1, "memorialization/has_seen_manage_nux"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3kS;->a:LX/0Tn;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;Landroid/content/res/Resources;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 632970
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 632971
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/3kS;->d:Z

    .line 632972
    iput-object p1, p0, LX/3kS;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 632973
    iput-object p2, p0, LX/3kS;->c:Landroid/content/res/Resources;

    .line 632974
    return-void
.end method

.method public static b(LX/0QB;)LX/3kS;
    .locals 3

    .prologue
    .line 632968
    new-instance v2, LX/3kS;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v1

    check-cast v1, Landroid/content/res/Resources;

    invoke-direct {v2, v0, v1}, LX/3kS;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;Landroid/content/res/Resources;)V

    .line 632969
    return-object v2
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 632967
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/10S;
    .locals 3

    .prologue
    .line 632966
    iget-boolean v0, p0, LX/3kS;->d:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3kS;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/3kS;->a:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;

    :goto_0
    return-object v0

    :cond_1
    sget-object v0, LX/10S;->ELIGIBLE:LX/10S;

    goto :goto_0
.end method

.method public final a(J)V
    .locals 0

    .prologue
    .line 632965
    return-void
.end method

.method public final a(Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 632964
    return-void
.end method

.method public final a(Landroid/view/View;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 632955
    new-instance v0, LX/0hs;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 632956
    const/4 v1, -0x1

    .line 632957
    iput v1, v0, LX/0hs;->t:I

    .line 632958
    iget-object v1, p0, LX/3kS;->c:Landroid/content/res/Resources;

    const v2, 0x7f081544

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 632959
    invoke-virtual {v0, v1}, LX/0hs;->a(Ljava/lang/CharSequence;)V

    .line 632960
    const v1, 0x7f081545

    invoke-virtual {v0, v1}, LX/0hs;->b(I)V

    .line 632961
    const v1, 0x3e4ccccd    # 0.2f

    invoke-virtual {v0, v1}, LX/0ht;->b(F)V

    .line 632962
    invoke-virtual {v0, p1}, LX/0ht;->a(Landroid/view/View;)V

    .line 632963
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 632954
    const-string v0, "3226"

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 632953
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->TIMELINE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 632951
    iget-object v0, p0, LX/3kS;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/3kS;->a:LX/0Tn;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 632952
    return-void
.end method
