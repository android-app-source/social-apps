.class public LX/4c1;
.super Ljava/io/FilterInputStream;
.source ""


# instance fields
.field private final a:LX/4c4;

.field private b:Z


# direct methods
.method public constructor <init>(Ljava/io/InputStream;LX/4c4;)V
    .locals 0

    .prologue
    .line 794476
    invoke-direct {p0, p1}, Ljava/io/FilterInputStream;-><init>(Ljava/io/InputStream;)V

    .line 794477
    iput-object p2, p0, LX/4c1;->a:LX/4c4;

    .line 794478
    return-void
.end method

.method private a(I)I
    .locals 1

    .prologue
    .line 794444
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 794445
    invoke-static {p0}, LX/4c1;->a(LX/4c1;)V

    .line 794446
    :cond_0
    return p1
.end method

.method public static a(LX/4c1;)V
    .locals 1

    .prologue
    .line 794471
    iget-boolean v0, p0, LX/4c1;->b:Z

    if-nez v0, :cond_0

    .line 794472
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4c1;->b:Z

    .line 794473
    iget-object v0, p0, LX/4c1;->a:LX/4c4;

    .line 794474
    iget-object p0, v0, LX/4c4;->a:LX/4c5;

    iget-object p0, p0, LX/4c5;->a:LX/4c3;

    invoke-virtual {p0}, LX/4c3;->b()V

    .line 794475
    :cond_0
    return-void
.end method

.method private a(Ljava/io/IOException;)V
    .locals 1

    .prologue
    .line 794466
    iget-boolean v0, p0, LX/4c1;->b:Z

    if-nez v0, :cond_0

    .line 794467
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4c1;->b:Z

    .line 794468
    iget-object v0, p0, LX/4c1;->a:LX/4c4;

    .line 794469
    iget-object p0, v0, LX/4c4;->a:LX/4c5;

    iget-object p0, p0, LX/4c5;->a:LX/4c3;

    invoke-virtual {p0, p1}, LX/4c3;->a(Ljava/io/IOException;)V

    .line 794470
    :cond_0
    return-void
.end method


# virtual methods
.method public final close()V
    .locals 0

    .prologue
    .line 794463
    invoke-static {p0}, LX/4c1;->a(LX/4c1;)V

    .line 794464
    invoke-super {p0}, Ljava/io/FilterInputStream;->close()V

    .line 794465
    return-void
.end method

.method public final read()I
    .locals 1

    .prologue
    .line 794459
    :try_start_0
    iget-object v0, p0, Ljava/io/FilterInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->read()I

    move-result v0

    invoke-direct {p0, v0}, LX/4c1;->a(I)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    .line 794460
    :catch_0
    move-exception v0

    .line 794461
    invoke-direct {p0, v0}, LX/4c1;->a(Ljava/io/IOException;)V

    .line 794462
    throw v0
.end method

.method public final read([BII)I
    .locals 1

    .prologue
    .line 794455
    :try_start_0
    iget-object v0, p0, Ljava/io/FilterInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/InputStream;->read([BII)I

    move-result v0

    invoke-direct {p0, v0}, LX/4c1;->a(I)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    .line 794456
    :catch_0
    move-exception v0

    .line 794457
    invoke-direct {p0, v0}, LX/4c1;->a(Ljava/io/IOException;)V

    .line 794458
    throw v0
.end method

.method public final skip(J)J
    .locals 6

    .prologue
    .line 794447
    :try_start_0
    iget-object v0, p0, Ljava/io/FilterInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v0, p1, p2}, Ljava/io/InputStream;->skip(J)J

    move-result-wide v0

    .line 794448
    const-wide/16 v3, -0x1

    cmp-long v3, v0, v3

    if-nez v3, :cond_0

    .line 794449
    invoke-static {p0}, LX/4c1;->a(LX/4c1;)V

    .line 794450
    :cond_0
    move-wide v0, v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 794451
    return-wide v0

    .line 794452
    :catch_0
    move-exception v0

    .line 794453
    invoke-direct {p0, v0}, LX/4c1;->a(Ljava/io/IOException;)V

    .line 794454
    throw v0
.end method
