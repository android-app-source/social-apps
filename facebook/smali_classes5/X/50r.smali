.class public final LX/50r;
.super LX/2I9;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2I9",
        "<TC;TV;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/util/Map$Entry;

.field public final synthetic b:LX/50s;


# direct methods
.method public constructor <init>(LX/50s;Ljava/util/Map$Entry;)V
    .locals 0

    .prologue
    .line 824124
    iput-object p1, p0, LX/50r;->b:LX/50s;

    iput-object p2, p0, LX/50r;->a:Ljava/util/Map$Entry;

    invoke-direct {p0}, LX/2I9;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/util/Map$Entry;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map$Entry",
            "<TC;TV;>;"
        }
    .end annotation

    .prologue
    .line 824133
    iget-object v0, p0, LX/50r;->a:Ljava/util/Map$Entry;

    return-object v0
.end method

.method public final synthetic e()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 824132
    invoke-virtual {p0}, LX/50r;->a()Ljava/util/Map$Entry;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 824126
    const/4 v0, 0x0

    .line 824127
    instance-of v1, p1, Ljava/util/Map$Entry;

    if-eqz v1, :cond_0

    .line 824128
    check-cast p1, Ljava/util/Map$Entry;

    .line 824129
    invoke-virtual {p0}, LX/2I9;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, LX/2I9;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 824130
    :cond_0
    move v0, v0

    .line 824131
    return v0
.end method

.method public final setValue(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)TV;"
        }
    .end annotation

    .prologue
    .line 824125
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-super {p0, v0}, LX/2I9;->setValue(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
