.class public LX/3gR;
.super LX/37T;
.source ""


# instance fields
.field private final a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/3gP;

.field private final c:LX/1KL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1KL",
            "<",
            "Ljava/lang/String;",
            "LX/1yT;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/graphql/model/GraphQLTextWithEntities;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/3gP;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/3gP;",
            ")V"
        }
    .end annotation

    .prologue
    .line 624429
    invoke-direct {p0}, LX/37T;-><init>()V

    .line 624430
    iput-object p1, p0, LX/3gR;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 624431
    iput-object p2, p0, LX/3gR;->b:LX/3gP;

    .line 624432
    new-instance v0, LX/3gS;

    iget-object v1, p0, LX/3gR;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {v0, p0, v1}, LX/3gS;-><init>(LX/3gR;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    iput-object v0, p0, LX/3gR;->c:LX/1KL;

    .line 624433
    iget-object v0, p0, LX/3gR;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 624434
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 624435
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->V()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    iput-object v0, p0, LX/3gR;->d:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 624436
    return-void
.end method


# virtual methods
.method public final a(Landroid/text/Spannable;)I
    .locals 1

    .prologue
    .line 624437
    const/4 v0, 0x0

    return v0
.end method

.method public final a()LX/1KL;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1KL",
            "<",
            "Ljava/lang/String;",
            "LX/1yT;",
            ">;"
        }
    .end annotation

    .prologue
    .line 624438
    iget-object v0, p0, LX/3gR;->c:LX/1KL;

    return-object v0
.end method

.method public final b()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 624439
    iget-object v0, p0, LX/3gR;->d:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final c()LX/0jW;
    .locals 1

    .prologue
    .line 624440
    iget-object v0, p0, LX/3gR;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 624441
    iget-object p0, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, p0

    .line 624442
    check-cast v0, LX/0jW;

    return-object v0
.end method
