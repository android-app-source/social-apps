.class public final LX/4wk;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final b:Lsun/misc/Unsafe;

.field private static final c:J


# instance fields
.field public volatile a:J


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 820395
    :try_start_0
    invoke-static {}, LX/4wg;->c()Lsun/misc/Unsafe;

    move-result-object v0

    sput-object v0, LX/4wk;->b:Lsun/misc/Unsafe;

    .line 820396
    const-class v0, LX/4wk;

    .line 820397
    sget-object v1, LX/4wk;->b:Lsun/misc/Unsafe;

    const-string v2, "value"

    invoke-virtual {v0, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    invoke-virtual {v1, v0}, Lsun/misc/Unsafe;->objectFieldOffset(Ljava/lang/reflect/Field;)J

    move-result-wide v0

    sput-wide v0, LX/4wk;->c:J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 820398
    return-void

    .line 820399
    :catch_0
    move-exception v0

    .line 820400
    new-instance v1, Ljava/lang/Error;

    invoke-direct {v1, v0}, Ljava/lang/Error;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public constructor <init>(J)V
    .locals 1

    .prologue
    .line 820401
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, LX/4wk;->a:J

    return-void
.end method


# virtual methods
.method public final a(JJ)Z
    .locals 9

    .prologue
    .line 820402
    sget-object v0, LX/4wk;->b:Lsun/misc/Unsafe;

    sget-wide v2, LX/4wk;->c:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-virtual/range {v0 .. v7}, Lsun/misc/Unsafe;->compareAndSwapLong(Ljava/lang/Object;JJJ)Z

    move-result v0

    return v0
.end method
