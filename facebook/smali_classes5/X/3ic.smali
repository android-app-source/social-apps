.class public LX/3ic;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0tX;


# direct methods
.method public constructor <init>(LX/0tX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 629890
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 629891
    iput-object p1, p0, LX/3ic;->a:LX/0tX;

    .line 629892
    return-void
.end method

.method public static a(LX/0QB;)LX/3ic;
    .locals 2

    .prologue
    .line 629893
    new-instance v1, LX/3ic;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-direct {v1, v0}, LX/3ic;-><init>(LX/0tX;)V

    .line 629894
    move-object v0, v1

    .line 629895
    return-object v0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentHideUnhideFragmentModel;
    .locals 8

    .prologue
    .line 629896
    new-instance v0, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentHideUnhideFragmentModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentHideUnhideFragmentModel;-><init>()V

    .line 629897
    new-instance v1, LX/58w;

    invoke-direct {v1}, LX/58w;-><init>()V

    .line 629898
    invoke-virtual {v0}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentHideUnhideFragmentModel;->j()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/58w;->a:Ljava/lang/String;

    .line 629899
    invoke-virtual {v0}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentHideUnhideFragmentModel;->k()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/58w;->b:Ljava/lang/String;

    .line 629900
    move-object v0, v1

    .line 629901
    iput-object p0, v0, LX/58w;->a:Ljava/lang/String;

    .line 629902
    move-object v0, v0

    .line 629903
    iput-object p1, v0, LX/58w;->b:Ljava/lang/String;

    .line 629904
    move-object v0, v0

    .line 629905
    const/4 v5, 0x1

    const/4 v7, 0x0

    const/4 v3, 0x0

    .line 629906
    new-instance v1, LX/186;

    const/16 v2, 0x80

    invoke-direct {v1, v2}, LX/186;-><init>(I)V

    .line 629907
    iget-object v2, v0, LX/58w;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 629908
    iget-object v4, v0, LX/58w;->b:Ljava/lang/String;

    invoke-virtual {v1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 629909
    const/4 v6, 0x2

    invoke-virtual {v1, v6}, LX/186;->c(I)V

    .line 629910
    invoke-virtual {v1, v7, v2}, LX/186;->b(II)V

    .line 629911
    invoke-virtual {v1, v5, v4}, LX/186;->b(II)V

    .line 629912
    invoke-virtual {v1}, LX/186;->d()I

    move-result v2

    .line 629913
    invoke-virtual {v1, v2}, LX/186;->d(I)V

    .line 629914
    invoke-virtual {v1}, LX/186;->e()[B

    move-result-object v1

    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 629915
    invoke-virtual {v2, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 629916
    new-instance v1, LX/15i;

    move-object v4, v3

    move-object v6, v3

    invoke-direct/range {v1 .. v6}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 629917
    new-instance v2, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentHideUnhideFragmentModel;

    invoke-direct {v2, v1}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentHideUnhideFragmentModel;-><init>(LX/15i;)V

    .line 629918
    move-object v0, v2

    .line 629919
    return-object v0
.end method

.method private static a(LX/3ic;LX/0zP;LX/2vO;Ljava/lang/Object;LX/6Ow;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "Q:",
            "LX/0zP",
            "<TT;>;>(TQ;",
            "LX/2vO;",
            "TT;",
            "LX/6Ow",
            "<TT;>;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentHideUnhideFragmentModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 629920
    const-string v0, "input"

    invoke-virtual {p1, v0, p2}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 629921
    new-instance v1, Lcom/facebook/graphql/executor/GraphQLResult;

    sget-object v0, LX/0ta;->FROM_SERVER:LX/0ta;

    .line 629922
    sget-object v2, LX/0SF;->a:LX/0SF;

    move-object v2, v2

    .line 629923
    invoke-virtual {v2}, LX/0SF;->a()J

    move-result-wide v2

    invoke-direct {v1, p3, v0, v2, v3}, Lcom/facebook/graphql/executor/GraphQLResult;-><init>(Ljava/lang/Object;LX/0ta;J)V

    .line 629924
    new-instance v0, LX/399;

    invoke-direct {v0, p1}, LX/399;-><init>(LX/0zP;)V

    .line 629925
    iget-object v2, p0, LX/3ic;->a:LX/0tX;

    new-instance v3, LX/3G1;

    invoke-direct {v3}, LX/3G1;-><init>()V

    invoke-virtual {v3, v0}, LX/3G1;->a(LX/399;)LX/3G1;

    move-result-object v0

    sget-object v3, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, 0x1

    invoke-virtual {v3, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    .line 629926
    iput-wide v4, v0, LX/3G2;->d:J

    .line 629927
    move-object v0, v0

    .line 629928
    const/16 v3, 0x64

    .line 629929
    iput v3, v0, LX/3G2;->f:I

    .line 629930
    move-object v0, v0

    .line 629931
    invoke-virtual {v0}, LX/3G2;->a()LX/3G3;

    move-result-object v0

    check-cast v0, LX/3G4;

    new-instance v3, LX/3G0;

    invoke-direct {v3, v1}, LX/3G0;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v2, v0, v3}, LX/0tX;->a(LX/3G4;LX/3Fz;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 629932
    new-instance v1, LX/6Oz;

    invoke-direct {v1, p0, p4}, LX/6Oz;-><init>(LX/3ic;LX/6Ow;)V

    .line 629933
    sget-object v2, LX/131;->INSTANCE:LX/131;

    move-object v2, v2

    .line 629934
    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLComment;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentHideUnhideFragmentModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 629935
    new-instance v0, LX/4DQ;

    invoke-direct {v0}, LX/4DQ;-><init>()V

    .line 629936
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v1

    .line 629937
    const-string v2, "comment_id"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 629938
    new-instance v1, LX/58p;

    invoke-direct {v1}, LX/58p;-><init>()V

    new-instance v2, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentHideMutationModel;

    invoke-direct {v2}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentHideMutationModel;-><init>()V

    .line 629939
    new-instance v3, LX/58v;

    invoke-direct {v3}, LX/58v;-><init>()V

    .line 629940
    invoke-virtual {v2}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentHideMutationModel;->a()Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentHideUnhideFragmentModel;

    move-result-object v4

    iput-object v4, v3, LX/58v;->a:Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentHideUnhideFragmentModel;

    .line 629941
    move-object v2, v3

    .line 629942
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v3

    const-string v4, "spam"

    invoke-static {v3, v4}, LX/3ic;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentHideUnhideFragmentModel;

    move-result-object v3

    .line 629943
    iput-object v3, v2, LX/58v;->a:Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentHideUnhideFragmentModel;

    .line 629944
    move-object v2, v2

    .line 629945
    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 629946
    new-instance v5, LX/186;

    const/16 v6, 0x80

    invoke-direct {v5, v6}, LX/186;-><init>(I)V

    .line 629947
    iget-object v6, v2, LX/58v;->a:Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentHideUnhideFragmentModel;

    invoke-static {v5, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 629948
    invoke-virtual {v5, v9}, LX/186;->c(I)V

    .line 629949
    invoke-virtual {v5, v8, v6}, LX/186;->b(II)V

    .line 629950
    invoke-virtual {v5}, LX/186;->d()I

    move-result v6

    .line 629951
    invoke-virtual {v5, v6}, LX/186;->d(I)V

    .line 629952
    invoke-virtual {v5}, LX/186;->e()[B

    move-result-object v5

    invoke-static {v5}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v6

    .line 629953
    invoke-virtual {v6, v8}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 629954
    new-instance v5, LX/15i;

    move-object v8, v7

    move-object v10, v7

    invoke-direct/range {v5 .. v10}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 629955
    new-instance v6, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentHideMutationModel;

    invoke-direct {v6, v5}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentHideMutationModel;-><init>(LX/15i;)V

    .line 629956
    move-object v2, v6

    .line 629957
    new-instance v3, LX/6Ox;

    invoke-direct {v3, p0}, LX/6Ox;-><init>(LX/3ic;)V

    invoke-static {p0, v1, v0, v2, v3}, LX/3ic;->a(LX/3ic;LX/0zP;LX/2vO;Ljava/lang/Object;LX/6Ow;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLComment;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentHideUnhideFragmentModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 629958
    new-instance v0, LX/4DU;

    invoke-direct {v0}, LX/4DU;-><init>()V

    .line 629959
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v1

    .line 629960
    const-string v2, "comment_id"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 629961
    new-instance v1, LX/58q;

    invoke-direct {v1}, LX/58q;-><init>()V

    new-instance v2, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentUnhideMutationModel;

    invoke-direct {v2}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentUnhideMutationModel;-><init>()V

    .line 629962
    new-instance v3, LX/58z;

    invoke-direct {v3}, LX/58z;-><init>()V

    .line 629963
    invoke-virtual {v2}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentUnhideMutationModel;->a()Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentHideUnhideFragmentModel;

    move-result-object v4

    iput-object v4, v3, LX/58z;->a:Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentHideUnhideFragmentModel;

    .line 629964
    move-object v2, v3

    .line 629965
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v3

    const-string v4, "none"

    invoke-static {v3, v4}, LX/3ic;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentHideUnhideFragmentModel;

    move-result-object v3

    .line 629966
    iput-object v3, v2, LX/58z;->a:Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentHideUnhideFragmentModel;

    .line 629967
    move-object v2, v2

    .line 629968
    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 629969
    new-instance v5, LX/186;

    const/16 v6, 0x80

    invoke-direct {v5, v6}, LX/186;-><init>(I)V

    .line 629970
    iget-object v6, v2, LX/58z;->a:Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentHideUnhideFragmentModel;

    invoke-static {v5, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 629971
    invoke-virtual {v5, v9}, LX/186;->c(I)V

    .line 629972
    invoke-virtual {v5, v8, v6}, LX/186;->b(II)V

    .line 629973
    invoke-virtual {v5}, LX/186;->d()I

    move-result v6

    .line 629974
    invoke-virtual {v5, v6}, LX/186;->d(I)V

    .line 629975
    invoke-virtual {v5}, LX/186;->e()[B

    move-result-object v5

    invoke-static {v5}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v6

    .line 629976
    invoke-virtual {v6, v8}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 629977
    new-instance v5, LX/15i;

    move-object v8, v7

    move-object v10, v7

    invoke-direct/range {v5 .. v10}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 629978
    new-instance v6, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentUnhideMutationModel;

    invoke-direct {v6, v5}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentUnhideMutationModel;-><init>(LX/15i;)V

    .line 629979
    move-object v2, v6

    .line 629980
    new-instance v3, LX/6Oy;

    invoke-direct {v3, p0}, LX/6Oy;-><init>(LX/3ic;)V

    invoke-static {p0, v1, v0, v2, v3}, LX/3ic;->a(LX/3ic;LX/0zP;LX/2vO;Ljava/lang/Object;LX/6Ow;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
