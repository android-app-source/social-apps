.class public final LX/4YH;
.super LX/0ur;
.source ""


# instance fields
.field public b:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

.field public c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;",
            ">;"
        }
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;",
            ">;"
        }
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 776218
    invoke-direct {p0}, LX/0ur;-><init>()V

    .line 776219
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    iput-object v0, p0, LX/4YH;->b:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    .line 776220
    instance-of v0, p0, LX/4YH;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 776221
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)LX/4YH;
    .locals 2

    .prologue
    .line 776200
    new-instance v0, LX/4YH;

    invoke-direct {v0}, LX/4YH;-><init>()V

    .line 776201
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 776202
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->e()Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    move-result-object v1

    iput-object v1, v0, LX/4YH;->b:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    .line 776203
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->x_()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4YH;->c:LX/0Px;

    .line 776204
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->y_()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4YH;->d:Ljava/lang/String;

    .line 776205
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4YH;->e:Lcom/facebook/graphql/model/GraphQLImage;

    .line 776206
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->o()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4YH;->f:Ljava/lang/String;

    .line 776207
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->j()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4YH;->g:LX/0Px;

    .line 776208
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->c()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4YH;->h:Ljava/lang/String;

    .line 776209
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4YH;->i:Ljava/lang/String;

    .line 776210
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->p()Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;

    move-result-object v1

    iput-object v1, v0, LX/4YH;->j:Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;

    .line 776211
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->l()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4YH;->k:LX/0Px;

    .line 776212
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->m()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4YH;->l:Ljava/lang/String;

    .line 776213
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->q()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4YH;->m:Ljava/lang/String;

    .line 776214
    invoke-static {v0, p0}, LX/0ur;->a(LX/0ur;Lcom/facebook/graphql/modelutil/BaseModel;)V

    .line 776215
    return-object v0
.end method


# virtual methods
.method public final a()Lcom/facebook/graphql/model/GraphQLPrivacyOption;
    .locals 2

    .prologue
    .line 776216
    new-instance v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-direct {v0, p0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;-><init>(LX/4YH;)V

    .line 776217
    return-object v0
.end method
