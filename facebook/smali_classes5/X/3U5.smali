.class public LX/3U5;
.super LX/3U6;
.source ""

# interfaces
.implements LX/2kk;
.implements LX/2kl;


# instance fields
.field private final n:Landroid/content/Context;

.field private final o:LX/0gh;

.field private final p:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

.field private final q:LX/3Tt;

.field private final r:LX/3Tu;

.field private final s:LX/2Yg;

.field private final t:LX/1rn;

.field private final u:LX/0Sh;

.field private final v:LX/0SI;

.field private final w:LX/2ks;

.field private final x:LX/1QW;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1PT;LX/1SX;Ljava/lang/Runnable;Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;LX/3Tv;LX/3Tu;LX/2jY;LX/1PY;LX/3Tt;LX/1QW;LX/3UA;LX/3UB;LX/2kq;LX/2kr;Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;LX/0gh;LX/2Yg;LX/1rn;LX/0Sh;LX/0SI;LX/2ks;)V
    .locals 15
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/1PT;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/1SX;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/Runnable;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # LX/3Tv;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # LX/3Tu;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # LX/2jY;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p9    # LX/1PY;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p10    # LX/3Tt;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p11    # LX/1QW;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 585406
    move-object v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p12

    move-object/from16 v12, p13

    move-object/from16 v13, p14

    move-object/from16 v14, p15

    invoke-direct/range {v1 .. v14}, LX/3U6;-><init>(Landroid/content/Context;LX/1PT;LX/1SX;Ljava/lang/Runnable;Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;LX/3Tv;LX/2ja;LX/2jY;LX/1PY;LX/3UA;LX/3UB;LX/2kq;LX/2kr;)V

    .line 585407
    move-object/from16 v0, p1

    iput-object v0, p0, LX/3U5;->n:Landroid/content/Context;

    .line 585408
    move-object/from16 v0, p17

    iput-object v0, p0, LX/3U5;->o:LX/0gh;

    .line 585409
    move-object/from16 v0, p16

    iput-object v0, p0, LX/3U5;->p:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    .line 585410
    move-object/from16 v0, p10

    iput-object v0, p0, LX/3U5;->q:LX/3Tt;

    .line 585411
    move-object/from16 v0, p7

    iput-object v0, p0, LX/3U5;->r:LX/3Tu;

    .line 585412
    move-object/from16 v0, p18

    iput-object v0, p0, LX/3U5;->s:LX/2Yg;

    .line 585413
    move-object/from16 v0, p19

    iput-object v0, p0, LX/3U5;->t:LX/1rn;

    .line 585414
    move-object/from16 v0, p20

    iput-object v0, p0, LX/3U5;->u:LX/0Sh;

    .line 585415
    move-object/from16 v0, p21

    iput-object v0, p0, LX/3U5;->v:LX/0SI;

    .line 585416
    move-object/from16 v0, p22

    iput-object v0, p0, LX/3U5;->w:LX/2ks;

    .line 585417
    move-object/from16 v0, p11

    iput-object v0, p0, LX/3U5;->x:LX/1QW;

    .line 585418
    return-void
.end method

.method private c(Ljava/lang/String;)LX/2nq;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 585419
    iget-object v0, p0, LX/3U5;->q:LX/3Tt;

    invoke-interface {v0, p1}, LX/3Tt;->c(Ljava/lang/String;)LX/2nq;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Landroid/graphics/drawable/Drawable;
    .locals 3

    .prologue
    .line 585420
    invoke-static {p1}, LX/E1q;->b(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Ljava/lang/String;

    move-result-object v0

    .line 585421
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 585422
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    iget-object v1, p0, LX/3U5;->n:Landroid/content/Context;

    const v2, 0x7f0a0480

    invoke-static {v1, v2}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 585423
    :goto_0
    return-object v0

    .line 585424
    :cond_0
    iget-object v1, p0, LX/3U5;->q:LX/3Tt;

    invoke-interface {v1, v0}, LX/3Tt;->a(Ljava/lang/String;)LX/2nq;

    move-result-object v0

    .line 585425
    if-eqz v0, :cond_1

    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aF()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->SEEN_AND_READ:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    .line 585426
    :goto_1
    if-eqz v0, :cond_2

    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    iget-object v1, p0, LX/3U5;->n:Landroid/content/Context;

    const v2, 0x7f0a00d5

    invoke-static {v1, v2}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    goto :goto_0

    .line 585427
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 585428
    :cond_2
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    iget-object v1, p0, LX/3U5;->n:Landroid/content/Context;

    const v2, 0x7f0a0480

    invoke-static {v1, v2}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    goto :goto_0
.end method

.method public final a(LX/1Rb;)V
    .locals 1

    .prologue
    .line 585429
    iget-object v0, p0, LX/3U5;->x:LX/1QW;

    invoke-virtual {v0, p1}, LX/1QW;->a(LX/1Rb;)V

    .line 585430
    return-void
.end method

.method public final a(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 1

    .prologue
    .line 585459
    iget-object v0, p0, LX/3U5;->x:LX/1QW;

    invoke-virtual {v0, p1, p2}, LX/1QW;->a(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 585460
    return-void
.end method

.method public final a(LX/1f9;LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 1

    .prologue
    .line 585431
    iget-object v0, p0, LX/3U5;->x:LX/1QW;

    invoke-virtual {v0, p1, p2, p3}, LX/1QW;->a(LX/1f9;LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 585432
    return-void
.end method

.method public final a(LX/2nq;)V
    .locals 5

    .prologue
    .line 585433
    invoke-interface {p1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3U5;->q:LX/3Tt;

    invoke-interface {p1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/3Tt;->a(Ljava/lang/String;)LX/2nq;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->SEEN_AND_READ:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-interface {p1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->aF()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 585434
    :cond_0
    :goto_0
    return-void

    .line 585435
    :cond_1
    iget-object v0, p0, LX/3U5;->q:LX/3Tt;

    invoke-interface {p1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/3Tt;->a(Ljava/lang/String;)LX/2nq;

    move-result-object v0

    .line 585436
    iget-object v1, p0, LX/3U5;->t:LX/1rn;

    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->SEEN_AND_READ:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    iget-object v4, p0, LX/3U5;->v:LX/0SI;

    invoke-interface {v4}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, LX/1rn;->a(Ljava/util/List;Lcom/facebook/graphql/enums/GraphQLStorySeenState;Lcom/facebook/auth/viewercontext/ViewerContext;)V

    .line 585437
    iget-object v1, p0, LX/3U5;->q:LX/3Tt;

    invoke-interface {v1, v0}, LX/3Tt;->a(LX/2nq;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 585438
    iget-object v1, p0, LX/3U5;->p:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->SEEN_AND_READ:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLStorySeenState;)Z

    .line 585439
    :goto_1
    iget-object v1, p0, LX/3U5;->q:LX/3Tt;

    invoke-static {v0}, LX/BDX;->a(LX/2nq;)LX/2nq;

    move-result-object v2

    invoke-interface {v1, v0, v2}, LX/3Tt;->a(LX/2nq;LX/2nq;)V

    goto :goto_0

    .line 585440
    :cond_2
    iget-object v1, p0, LX/3U5;->p:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->SEEN_AND_READ:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->b(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLStorySeenState;)Z

    goto :goto_1
.end method

.method public final a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/9uc;)V
    .locals 5

    .prologue
    .line 585441
    invoke-static {p1}, LX/E1q;->b(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Ljava/lang/String;

    move-result-object v0

    .line 585442
    iget-object v1, p0, LX/3U5;->q:LX/3Tt;

    invoke-interface {v1, v0}, LX/3Tt;->a(Ljava/lang/String;)LX/2nq;

    move-result-object v1

    .line 585443
    if-eqz v1, :cond_0

    invoke-interface {v1}, LX/2nq;->o()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, LX/2nq;->o()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;->b()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v2

    if-nez v2, :cond_1

    .line 585444
    :cond_0
    :goto_0
    return-void

    .line 585445
    :cond_1
    invoke-interface {v1}, LX/2nq;->o()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;->b()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v2

    invoke-interface {p2}, LX/9uc;->R()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, p2, v3}, LX/Cfu;->a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;LX/9uc;Ljava/lang/String;)Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v2

    .line 585446
    iget-object v3, p0, LX/3U5;->t:LX/1rn;

    .line 585447
    iget-object v4, v3, LX/1rn;->d:LX/0TD;

    new-instance p1, Lcom/facebook/notifications/util/NotificationsUtils$3;

    invoke-direct {p1, v3, v0, v2}, Lcom/facebook/notifications/util/NotificationsUtils$3;-><init>(LX/1rn;Ljava/lang/String;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;)V

    const p2, 0x3215dd37

    invoke-static {v4, p1, p2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 585448
    iget-object v0, p0, LX/3U5;->q:LX/3Tt;

    invoke-interface {v0, v1}, LX/3Tt;->a(LX/2nq;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 585449
    iget-object v0, p0, LX/3U5;->p:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    invoke-interface {v1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v3

    .line 585450
    iget-object v4, v0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->i:LX/2AD;

    const/4 p1, 0x0

    .line 585451
    iput-boolean p1, v4, LX/2AF;->b:Z

    .line 585452
    iget-object v4, v0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->i:LX/2AD;

    invoke-virtual {v4, v3, v2}, LX/2AD;->a(Ljava/lang/String;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;)Z

    .line 585453
    iget-object p1, v0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->i:LX/2AD;

    const/4 p2, 0x1

    .line 585454
    iput-boolean p2, p1, LX/2AF;->b:Z

    .line 585455
    :goto_1
    iget-object v0, p0, LX/3U5;->q:LX/3Tt;

    invoke-static {v1, v2}, LX/BDX;->a(LX/2nq;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;)LX/2nq;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/3Tt;->a(LX/2nq;LX/2nq;)V

    goto :goto_0

    .line 585456
    :cond_2
    iget-object v0, p0, LX/3U5;->p:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    invoke-interface {v1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v3

    .line 585457
    iget-object v4, v0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->h:LX/2AD;

    invoke-virtual {v4, v3, v2}, LX/2AD;->a(Ljava/lang/String;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;)Z

    .line 585458
    goto :goto_1
.end method

.method public final a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;Ljava/lang/String;)V
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 585396
    invoke-static {p1}, LX/E1q;->b(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Ljava/lang/String;

    move-result-object v0

    .line 585397
    iget-object v1, p0, LX/3U5;->q:LX/3Tt;

    invoke-interface {v1, v0}, LX/3Tt;->a(Ljava/lang/String;)LX/2nq;

    move-result-object v0

    .line 585398
    if-nez v0, :cond_0

    .line 585399
    :goto_0
    return-void

    .line 585400
    :cond_0
    iget-object v1, p0, LX/3U5;->o:LX/0gh;

    const-string v2, "tap_notification_jewel"

    invoke-virtual {v1, v2}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 585401
    invoke-virtual {p0, v0}, LX/3U5;->a(LX/2nq;)V

    .line 585402
    iget-object v0, p0, LX/3U5;->r:LX/3Tu;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, LX/3Tu;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;LX/Cfl;)V
    .locals 1
    .param p3    # LX/Cfl;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 585403
    invoke-direct {p0, p1}, LX/3U5;->c(Ljava/lang/String;)LX/2nq;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/3U5;->a(LX/2nq;)V

    .line 585404
    invoke-super {p0, p1, p2, p3}, LX/3U6;->a(Ljava/lang/String;Ljava/lang/String;LX/Cfl;)V

    .line 585405
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Cfl;)V
    .locals 1
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/Cfl;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 585393
    invoke-direct {p0, p1}, LX/3U5;->c(Ljava/lang/String;)LX/2nq;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/3U5;->a(LX/2nq;)V

    .line 585394
    invoke-super {p0, p1, p2, p3, p4}, LX/3U6;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Cfl;)V

    .line 585395
    return-void
.end method

.method public final a(LX/2nq;Z)Z
    .locals 9

    .prologue
    .line 585366
    invoke-interface {p1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3U5;->q:LX/3Tt;

    invoke-interface {p1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/3Tt;->a(Ljava/lang/String;)LX/2nq;

    move-result-object v0

    if-nez v0, :cond_1

    .line 585367
    :cond_0
    const/4 v0, 0x0

    .line 585368
    :goto_0
    return v0

    .line 585369
    :cond_1
    iget-object v0, p0, LX/3U5;->q:LX/3Tt;

    invoke-interface {p1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/3Tt;->a(Ljava/lang/String;)LX/2nq;

    move-result-object v7

    .line 585370
    new-instance v0, LX/3D5;

    invoke-interface {v7}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/3D5;-><init>(Ljava/lang/String;)V

    invoke-interface {v7}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/1Qj;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3D6;

    .line 585371
    invoke-static {p2}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v1

    .line 585372
    iput-object v1, v0, LX/3D6;->a:LX/03R;

    .line 585373
    iget-object v0, p0, LX/3U5;->t:LX/1rn;

    invoke-interface {v7}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    .line 585374
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 585375
    iget-object v2, v0, LX/1rn;->d:LX/0TD;

    new-instance v3, Lcom/facebook/notifications/util/NotificationsUtils$4;

    invoke-direct {v3, v0, v1, p2}, Lcom/facebook/notifications/util/NotificationsUtils$4;-><init>(LX/1rn;Ljava/lang/String;Z)V

    const v4, -0x19b552dd

    invoke-static {v2, v3, v4}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 585376
    iget-object v0, p0, LX/3U5;->q:LX/3Tt;

    invoke-interface {v0, v7}, LX/3Tt;->a(LX/2nq;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 585377
    iget-object v1, p0, LX/3U5;->p:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    invoke-interface {v7}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v7}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v7}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->U()J

    move-result-wide v4

    move v6, p2

    .line 585378
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 585379
    iget-object v8, v1, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->n:Ljava/util/Map;

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-interface {v8, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 585380
    iget-object v0, v1, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->i:LX/2AD;

    const/4 v8, 0x0

    .line 585381
    iput-boolean v8, v0, LX/2AF;->b:Z

    .line 585382
    iget-object v0, v1, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->i:LX/2AD;

    invoke-virtual {v0, v3, v6}, LX/2AD;->a(Ljava/lang/String;Z)Z

    .line 585383
    iget-object v8, v1, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->i:LX/2AD;

    const/4 p1, 0x1

    .line 585384
    iput-boolean p1, v8, LX/2AF;->b:Z

    .line 585385
    :goto_1
    iget-object v0, p0, LX/3U5;->q:LX/3Tt;

    invoke-static {v7, p2}, LX/BDX;->a(LX/2nq;Z)LX/2nq;

    move-result-object v1

    invoke-interface {v0, v7, v1}, LX/3Tt;->a(LX/2nq;LX/2nq;)V

    .line 585386
    invoke-virtual {p0}, LX/1Qj;->iN_()V

    .line 585387
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 585388
    :cond_2
    iget-object v1, p0, LX/3U5;->p:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    invoke-interface {v7}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v7}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v7}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->U()J

    move-result-wide v4

    move v6, p2

    .line 585389
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 585390
    iget-object v8, v1, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->n:Ljava/util/Map;

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-interface {v8, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 585391
    iget-object v0, v1, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->h:LX/2AD;

    invoke-virtual {v0, v3, v6}, LX/2AD;->a(Ljava/lang/String;Z)Z

    .line 585392
    goto :goto_1
.end method

.method public final b(LX/2nq;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 585365
    iget-object v0, p0, LX/3U5;->w:LX/2ks;

    invoke-virtual {v0, p1, p0}, LX/2ks;->a(LX/2nq;Ljava/lang/Object;)LX/3D9;

    move-result-object v0

    return-object v0
.end method

.method public final e_(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 585364
    iget-object v0, p0, LX/3U5;->q:LX/3Tt;

    invoke-interface {v0, p1}, LX/3Tt;->b(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final m()LX/1f9;
    .locals 1

    .prologue
    .line 585363
    iget-object v0, p0, LX/3U5;->x:LX/1QW;

    invoke-virtual {v0}, LX/1QW;->m()LX/1f9;

    move-result-object v0

    return-object v0
.end method

.method public final md_()LX/2jc;
    .locals 1

    .prologue
    .line 585362
    iget-object v0, p0, LX/3U5;->r:LX/3Tu;

    return-object v0
.end method

.method public final o()LX/1Rb;
    .locals 1

    .prologue
    .line 585361
    iget-object v0, p0, LX/3U5;->x:LX/1QW;

    invoke-virtual {v0}, LX/1QW;->o()LX/1Rb;

    move-result-object v0

    return-object v0
.end method

.method public final p()V
    .locals 1

    .prologue
    .line 585359
    iget-object v0, p0, LX/3U5;->x:LX/1QW;

    invoke-virtual {v0}, LX/1QW;->p()V

    .line 585360
    return-void
.end method

.method public final q()Z
    .locals 1

    .prologue
    .line 585358
    iget-object v0, p0, LX/3U5;->x:LX/1QW;

    invoke-virtual {v0}, LX/1QW;->q()Z

    move-result v0

    return v0
.end method
