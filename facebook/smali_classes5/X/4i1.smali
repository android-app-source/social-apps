.class public LX/4i1;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/4i1;


# instance fields
.field public a:Z

.field private final b:LX/0Uo;

.field private final c:LX/0So;

.field public final d:LX/11i;

.field public final e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/4i0;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Uo;LX/0So;LX/11i;Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Uo;",
            "LX/0So;",
            "LX/11i;",
            "Ljava/util/Set",
            "<",
            "LX/4i0;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 802403
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 802404
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4i1;->a:Z

    .line 802405
    iput-object p1, p0, LX/4i1;->b:LX/0Uo;

    .line 802406
    iput-object p2, p0, LX/4i1;->c:LX/0So;

    .line 802407
    iput-object p3, p0, LX/4i1;->d:LX/11i;

    .line 802408
    iput-object p4, p0, LX/4i1;->e:Ljava/util/Set;

    .line 802409
    return-void
.end method

.method public static a(LX/0QB;)LX/4i1;
    .locals 9

    .prologue
    .line 802388
    sget-object v0, LX/4i1;->f:LX/4i1;

    if-nez v0, :cond_1

    .line 802389
    const-class v1, LX/4i1;

    monitor-enter v1

    .line 802390
    :try_start_0
    sget-object v0, LX/4i1;->f:LX/4i1;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 802391
    if-eqz v2, :cond_0

    .line 802392
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 802393
    new-instance v6, LX/4i1;

    invoke-static {v0}, LX/0Uo;->a(LX/0QB;)LX/0Uo;

    move-result-object v3

    check-cast v3, LX/0Uo;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v4

    check-cast v4, LX/0So;

    invoke-static {v0}, LX/11h;->a(LX/0QB;)LX/11h;

    move-result-object v5

    check-cast v5, LX/11i;

    .line 802394
    new-instance v7, LX/0U8;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v8

    new-instance p0, LX/4i3;

    invoke-direct {p0, v0}, LX/4i3;-><init>(LX/0QB;)V

    invoke-direct {v7, v8, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v7, v7

    .line 802395
    invoke-direct {v6, v3, v4, v5, v7}, LX/4i1;-><init>(LX/0Uo;LX/0So;LX/11i;Ljava/util/Set;)V

    .line 802396
    move-object v0, v6

    .line 802397
    sput-object v0, LX/4i1;->f:LX/4i1;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 802398
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 802399
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 802400
    :cond_1
    sget-object v0, LX/4i1;->f:LX/4i1;

    return-object v0

    .line 802401
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 802402
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(JZLjava/util/Map;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JZ",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 802381
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gtz v0, :cond_1

    .line 802382
    :cond_0
    return-void

    .line 802383
    :cond_1
    new-instance v0, LX/0P2;

    invoke-direct {v0}, LX/0P2;-><init>()V

    iget-object v1, p0, LX/4i1;->c:LX/0So;

    iget-object v3, p0, LX/4i1;->b:LX/0Uo;

    invoke-static {p1, p2, v1, v3, p3}, LX/4i2;->a(JLX/0So;LX/0Uo;Z)LX/0P1;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0P2;->a(Ljava/util/Map;)LX/0P2;

    move-result-object v7

    .line 802384
    if-eqz p4, :cond_2

    .line 802385
    invoke-virtual {v7, p4}, LX/0P2;->a(Ljava/util/Map;)LX/0P2;

    .line 802386
    :cond_2
    iget-object v0, p0, LX/4i1;->e:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4i0;

    .line 802387
    iget-object v1, p0, LX/4i1;->d:LX/11i;

    invoke-virtual {v7}, LX/0P2;->b()LX/0P1;

    move-result-object v3

    invoke-interface {v1, v0, v3, p1, p2}, LX/11i;->a(LX/0Pq;LX/0P1;J)LX/11o;

    move-result-object v0

    const-string v1, "PlatformLaunchPhase"

    const v6, -0x70bbab83

    move-object v3, v2

    move-wide v4, p1

    invoke-static/range {v0 .. v6}, LX/096;->a(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;JI)LX/11o;

    goto :goto_0
.end method

.method public final a(LX/4i0;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "LX/4i0;",
            ">(TT;)V"
        }
    .end annotation

    .prologue
    .line 802374
    iget-object v0, p0, LX/4i1;->d:LX/11i;

    invoke-interface {v0, p1}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v0

    .line 802375
    if-eqz v0, :cond_0

    .line 802376
    const-string v1, "PlatformLaunchPhase"

    const v2, 0x228060b5

    invoke-static {v0, v1, v2}, LX/096;->b(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 802377
    :cond_0
    iget-object v0, p0, LX/4i1;->e:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4i0;

    .line 802378
    if-eq v0, p1, :cond_1

    .line 802379
    iget-object v2, p0, LX/4i1;->d:LX/11i;

    invoke-interface {v2, v0}, LX/11i;->d(LX/0Pq;)V

    goto :goto_0

    .line 802380
    :cond_2
    return-void
.end method
