.class public LX/4BW;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public A:Landroid/graphics/drawable/Drawable;

.field public B:Landroid/widget/TextView;

.field public C:Ljava/lang/CharSequence;

.field public D:Landroid/view/View;

.field public E:Landroid/widget/ListAdapter;

.field public F:I

.field public G:I

.field public H:I

.field public I:I

.field public J:I

.field public K:I

.field public L:Z

.field public M:Landroid/os/Handler;

.field public final N:Landroid/view/View$OnClickListener;

.field public final a:Landroid/content/Context;

.field public final b:Landroid/content/DialogInterface;

.field public final c:Landroid/view/Window;

.field public d:Ljava/lang/CharSequence;

.field public e:Ljava/lang/CharSequence;

.field public f:Landroid/widget/ListView;

.field public g:Landroid/view/View;

.field public h:I

.field public i:I

.field public j:I

.field public k:I

.field public l:Z

.field public m:Landroid/widget/Button;

.field public n:Ljava/lang/CharSequence;

.field public o:Landroid/os/Message;

.field public p:Landroid/widget/Button;

.field public q:Ljava/lang/CharSequence;

.field public r:Landroid/os/Message;

.field public s:Landroid/widget/Button;

.field public t:Ljava/lang/CharSequence;

.field public u:Landroid/os/Message;

.field public v:Landroid/widget/ScrollView;

.field public w:Landroid/widget/TextView;

.field public x:Landroid/widget/TextView;

.field public y:Landroid/view/View;

.field public z:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/DialogInterface;Landroid/view/Window;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 677798
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 677799
    iput-boolean v3, p0, LX/4BW;->l:Z

    .line 677800
    const/4 v0, -0x1

    iput v0, p0, LX/4BW;->F:I

    .line 677801
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4BW;->L:Z

    .line 677802
    new-instance v0, LX/4BN;

    invoke-direct {v0, p0}, LX/4BN;-><init>(LX/4BW;)V

    iput-object v0, p0, LX/4BW;->N:Landroid/view/View$OnClickListener;

    .line 677803
    iput-object p1, p0, LX/4BW;->a:Landroid/content/Context;

    .line 677804
    iput-object p2, p0, LX/4BW;->b:Landroid/content/DialogInterface;

    .line 677805
    iput-object p3, p0, LX/4BW;->c:Landroid/view/Window;

    .line 677806
    new-instance v0, LX/4BU;

    invoke-direct {v0, p2}, LX/4BU;-><init>(Landroid/content/DialogInterface;)V

    iput-object v0, p0, LX/4BW;->M:Landroid/os/Handler;

    .line 677807
    const/4 v0, 0x0

    sget-object v1, LX/03r;->AlertDialog:[I

    const v2, 0x7f010221

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 677808
    const/16 v1, 0x0

    const v2, 0x7f031433

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, LX/4BW;->G:I

    .line 677809
    const/16 v1, 0x1

    const v2, 0x7f031436

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, LX/4BW;->H:I

    .line 677810
    const/16 v1, 0x2

    const v2, 0x7f031438

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, LX/4BW;->I:I

    .line 677811
    const/16 v1, 0x3

    const v2, 0x7f031439

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, LX/4BW;->J:I

    .line 677812
    const/16 v1, 0x4

    const v2, 0x7f031437

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, LX/4BW;->K:I

    .line 677813
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 677814
    return-void
.end method

.method public static a(Landroid/widget/Button;)V
    .locals 2

    .prologue
    .line 677793
    invoke-virtual {p0}, Landroid/widget/Button;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 677794
    const/4 v1, 0x1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 677795
    const/high16 v1, 0x3f000000    # 0.5f

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 677796
    invoke-virtual {p0, v0}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 677797
    return-void
.end method

.method public static b(Landroid/view/View;Landroid/view/View;Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 677786
    if-eqz p1, :cond_0

    .line 677787
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Landroid/view/View;->canScrollVertically(I)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 677788
    :cond_0
    if-eqz p2, :cond_1

    .line 677789
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/view/View;->canScrollVertically(I)Z

    move-result v0

    if-eqz v0, :cond_3

    :goto_1
    invoke-virtual {p2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 677790
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 677791
    goto :goto_0

    :cond_3
    move v1, v2

    .line 677792
    goto :goto_1
.end method

.method public static d(Landroid/view/View;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 677775
    invoke-virtual {p0}, Landroid/view/View;->onCheckIsTextEditor()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 677776
    :goto_0
    return v0

    .line 677777
    :cond_0
    instance-of v2, p0, Landroid/view/ViewGroup;

    if-nez v2, :cond_1

    move v0, v1

    .line 677778
    goto :goto_0

    .line 677779
    :cond_1
    check-cast p0, Landroid/view/ViewGroup;

    .line 677780
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    .line 677781
    :cond_2
    if-lez v2, :cond_3

    .line 677782
    add-int/lit8 v2, v2, -0x1

    .line 677783
    invoke-virtual {p0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 677784
    invoke-static {v3}, LX/4BW;->d(Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_0

    :cond_3
    move v0, v1

    .line 677785
    goto :goto_0
.end method


# virtual methods
.method public final a(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;Landroid/os/Message;)V
    .locals 2

    .prologue
    .line 677815
    if-nez p4, :cond_0

    if-eqz p3, :cond_0

    .line 677816
    iget-object v0, p0, LX/4BW;->M:Landroid/os/Handler;

    invoke-virtual {v0, p1, p3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object p4

    .line 677817
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 677818
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Button does not exist"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 677819
    :pswitch_0
    iput-object p2, p0, LX/4BW;->n:Ljava/lang/CharSequence;

    .line 677820
    iput-object p4, p0, LX/4BW;->o:Landroid/os/Message;

    .line 677821
    :goto_0
    return-void

    .line 677822
    :pswitch_1
    iput-object p2, p0, LX/4BW;->q:Ljava/lang/CharSequence;

    .line 677823
    iput-object p4, p0, LX/4BW;->r:Landroid/os/Message;

    goto :goto_0

    .line 677824
    :pswitch_2
    iput-object p2, p0, LX/4BW;->t:Ljava/lang/CharSequence;

    .line 677825
    iput-object p4, p0, LX/4BW;->u:Landroid/os/Message;

    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Landroid/view/View;IIII)V
    .locals 1

    .prologue
    .line 677768
    iput-object p1, p0, LX/4BW;->g:Landroid/view/View;

    .line 677769
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4BW;->l:Z

    .line 677770
    iput p2, p0, LX/4BW;->h:I

    .line 677771
    iput p3, p0, LX/4BW;->i:I

    .line 677772
    iput p4, p0, LX/4BW;->j:I

    .line 677773
    iput p5, p0, LX/4BW;->k:I

    .line 677774
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 677764
    iput-object p1, p0, LX/4BW;->d:Ljava/lang/CharSequence;

    .line 677765
    iget-object v0, p0, LX/4BW;->w:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 677766
    iget-object v0, p0, LX/4BW;->w:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 677767
    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 677760
    iput-object p1, p0, LX/4BW;->e:Ljava/lang/CharSequence;

    .line 677761
    iget-object v0, p0, LX/4BW;->x:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 677762
    iget-object v0, p0, LX/4BW;->x:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 677763
    :cond_0
    return-void
.end method

.method public final c(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 677757
    iput-object p1, p0, LX/4BW;->g:Landroid/view/View;

    .line 677758
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/4BW;->l:Z

    .line 677759
    return-void
.end method
