.class public final LX/4q6;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/4q7;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 812890
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 812891
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/4q6;->a:Ljava/util/ArrayList;

    .line 812892
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/4q6;->b:Ljava/util/HashMap;

    return-void
.end method


# virtual methods
.method public final a()LX/4q8;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 812900
    new-instance v1, LX/4q8;

    iget-object v0, p0, LX/4q6;->a:Ljava/util/ArrayList;

    iget-object v2, p0, LX/4q6;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [LX/4q7;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/4q7;

    iget-object v2, p0, LX/4q6;->b:Ljava/util/HashMap;

    invoke-direct {v1, v0, v2, v3, v3}, LX/4q8;-><init>([LX/4q7;Ljava/util/HashMap;[Ljava/lang/String;[LX/0nW;)V

    return-object v1
.end method

.method public final a(LX/32s;LX/4qw;)V
    .locals 3

    .prologue
    .line 812893
    iget-object v0, p0, LX/4q6;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 812894
    iget-object v1, p0, LX/4q6;->a:Ljava/util/ArrayList;

    new-instance v2, LX/4q7;

    invoke-direct {v2, p1, p2}, LX/4q7;-><init>(LX/32s;LX/4qw;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 812895
    iget-object v1, p0, LX/4q6;->b:Ljava/util/HashMap;

    .line 812896
    iget-object v2, p1, LX/32s;->_propName:Ljava/lang/String;

    move-object v2, v2

    .line 812897
    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 812898
    iget-object v1, p0, LX/4q6;->b:Ljava/util/HashMap;

    invoke-virtual {p2}, LX/4qw;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 812899
    return-void
.end method
