.class public LX/3iQ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/3iR;

.field public final b:LX/20i;

.field private final c:LX/3iS;

.field public final d:LX/1Ck;

.field private final e:LX/3iU;

.field private final f:LX/1nK;

.field private final g:LX/0SI;

.field public final h:LX/20j;

.field public final i:LX/3iV;

.field public final j:LX/3ib;

.field public final k:LX/3H7;

.field public final l:LX/0ie;

.field private final m:LX/0pf;

.field public final n:LX/2L1;

.field public final o:LX/0Uh;

.field public final p:Ljava/util/concurrent/ExecutorService;

.field public final q:LX/2xs;

.field public final r:LX/3ic;


# direct methods
.method public constructor <init>(LX/3iR;LX/20i;LX/3iS;LX/1Ck;LX/3iU;LX/1nK;LX/0SI;LX/20j;LX/3iV;LX/3ib;LX/3H7;LX/0ie;LX/0pf;LX/2L1;LX/0Uh;Ljava/util/concurrent/ExecutorService;LX/2xs;LX/3ic;)V
    .locals 1
    .param p16    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 629379
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 629380
    iput-object p1, p0, LX/3iQ;->a:LX/3iR;

    .line 629381
    iput-object p2, p0, LX/3iQ;->b:LX/20i;

    .line 629382
    iput-object p3, p0, LX/3iQ;->c:LX/3iS;

    .line 629383
    iput-object p4, p0, LX/3iQ;->d:LX/1Ck;

    .line 629384
    iput-object p5, p0, LX/3iQ;->e:LX/3iU;

    .line 629385
    iput-object p6, p0, LX/3iQ;->f:LX/1nK;

    .line 629386
    iput-object p7, p0, LX/3iQ;->g:LX/0SI;

    .line 629387
    iput-object p8, p0, LX/3iQ;->h:LX/20j;

    .line 629388
    iput-object p9, p0, LX/3iQ;->i:LX/3iV;

    .line 629389
    iput-object p10, p0, LX/3iQ;->j:LX/3ib;

    .line 629390
    iput-object p11, p0, LX/3iQ;->k:LX/3H7;

    .line 629391
    iput-object p12, p0, LX/3iQ;->l:LX/0ie;

    .line 629392
    iput-object p13, p0, LX/3iQ;->m:LX/0pf;

    .line 629393
    iput-object p14, p0, LX/3iQ;->n:LX/2L1;

    .line 629394
    move-object/from16 v0, p15

    iput-object v0, p0, LX/3iQ;->o:LX/0Uh;

    .line 629395
    move-object/from16 v0, p16

    iput-object v0, p0, LX/3iQ;->p:Ljava/util/concurrent/ExecutorService;

    .line 629396
    move-object/from16 v0, p17

    iput-object v0, p0, LX/3iQ;->q:LX/2xs;

    .line 629397
    move-object/from16 v0, p18

    iput-object v0, p0, LX/3iQ;->r:LX/3ic;

    .line 629398
    return-void
.end method

.method public static a(LX/0QB;)LX/3iQ;
    .locals 1

    .prologue
    .line 629378
    invoke-static {p0}, LX/3iQ;->b(LX/0QB;)LX/3iQ;

    move-result-object v0

    return-object v0
.end method

.method private static b(LX/0QB;)LX/3iQ;
    .locals 20

    .prologue
    .line 629376
    new-instance v1, LX/3iQ;

    invoke-static/range {p0 .. p0}, LX/3iR;->a(LX/0QB;)LX/3iR;

    move-result-object v2

    check-cast v2, LX/3iR;

    invoke-static/range {p0 .. p0}, LX/20i;->a(LX/0QB;)LX/20i;

    move-result-object v3

    check-cast v3, LX/20i;

    invoke-static/range {p0 .. p0}, LX/3iS;->a(LX/0QB;)LX/3iS;

    move-result-object v4

    check-cast v4, LX/3iS;

    invoke-static/range {p0 .. p0}, LX/1Ck;->a(LX/0QB;)LX/1Ck;

    move-result-object v5

    check-cast v5, LX/1Ck;

    invoke-static/range {p0 .. p0}, LX/3iU;->a(LX/0QB;)LX/3iU;

    move-result-object v6

    check-cast v6, LX/3iU;

    invoke-static/range {p0 .. p0}, LX/1nK;->a(LX/0QB;)LX/1nK;

    move-result-object v7

    check-cast v7, LX/1nK;

    invoke-static/range {p0 .. p0}, LX/0WG;->a(LX/0QB;)LX/0SI;

    move-result-object v8

    check-cast v8, LX/0SI;

    invoke-static/range {p0 .. p0}, LX/20j;->a(LX/0QB;)LX/20j;

    move-result-object v9

    check-cast v9, LX/20j;

    invoke-static/range {p0 .. p0}, LX/3iV;->a(LX/0QB;)LX/3iV;

    move-result-object v10

    check-cast v10, LX/3iV;

    const-class v11, LX/3ib;

    move-object/from16 v0, p0

    invoke-interface {v0, v11}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v11

    check-cast v11, LX/3ib;

    invoke-static/range {p0 .. p0}, LX/3H7;->a(LX/0QB;)LX/3H7;

    move-result-object v12

    check-cast v12, LX/3H7;

    invoke-static/range {p0 .. p0}, LX/0ie;->a(LX/0QB;)LX/0ie;

    move-result-object v13

    check-cast v13, LX/0ie;

    invoke-static/range {p0 .. p0}, LX/0pf;->a(LX/0QB;)LX/0pf;

    move-result-object v14

    check-cast v14, LX/0pf;

    invoke-static/range {p0 .. p0}, LX/2L1;->a(LX/0QB;)LX/2L1;

    move-result-object v15

    check-cast v15, LX/2L1;

    invoke-static/range {p0 .. p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v16

    check-cast v16, LX/0Uh;

    invoke-static/range {p0 .. p0}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v17

    check-cast v17, Ljava/util/concurrent/ExecutorService;

    invoke-static/range {p0 .. p0}, LX/2xs;->a(LX/0QB;)LX/2xs;

    move-result-object v18

    check-cast v18, LX/2xs;

    invoke-static/range {p0 .. p0}, LX/3ic;->a(LX/0QB;)LX/3ic;

    move-result-object v19

    check-cast v19, LX/3ic;

    invoke-direct/range {v1 .. v19}, LX/3iQ;-><init>(LX/3iR;LX/20i;LX/3iS;LX/1Ck;LX/3iU;LX/1nK;LX/0SI;LX/20j;LX/3iV;LX/3ib;LX/3H7;LX/0ie;LX/0pf;LX/2L1;LX/0Uh;Ljava/util/concurrent/ExecutorService;LX/2xs;LX/3ic;)V

    .line 629377
    return-object v1
.end method

.method public static c(LX/3iQ;Lcom/facebook/graphql/model/GraphQLComment;LX/1L9;)LX/2h1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            "LX/1L9",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ">;)",
            "LX/2h1",
            "<",
            "Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentHideUnhideFragmentModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 629399
    new-instance v0, LX/95u;

    invoke-direct {v0, p0, p2, p1}, LX/95u;-><init>(LX/3iQ;LX/1L9;Lcom/facebook/graphql/model/GraphQLComment;)V

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLComment;Ljava/lang/String;LX/1L9;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            "Ljava/lang/String;",
            "LX/1L9",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 629343
    new-instance v0, LX/5Gu;

    invoke-direct {v0}, LX/5Gu;-><init>()V

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v1

    .line 629344
    iput-object v1, v0, LX/5Gu;->a:Ljava/lang/String;

    .line 629345
    move-object v0, v0

    .line 629346
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v1

    .line 629347
    iput-object v1, v0, LX/5Gu;->b:Ljava/lang/String;

    .line 629348
    move-object v0, v0

    .line 629349
    iput-object p2, v0, LX/5Gu;->c:Ljava/lang/String;

    .line 629350
    move-object v0, v0

    .line 629351
    invoke-static {}, LX/1fz;->a()LX/162;

    move-result-object v1

    invoke-virtual {v1}, LX/162;->toString()Ljava/lang/String;

    move-result-object v1

    .line 629352
    iput-object v1, v0, LX/5Gu;->d:Ljava/lang/String;

    .line 629353
    move-object v0, v0

    .line 629354
    invoke-virtual {v0}, LX/5Gu;->a()Lcom/facebook/api/ufiservices/common/DeleteCommentParams;

    move-result-object v0

    .line 629355
    iget-object v1, p0, LX/3iQ;->d:LX/1Ck;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "delete_comment_"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/3iQ;->i:LX/3iV;

    .line 629356
    iget-object v4, v0, Lcom/facebook/api/ufiservices/common/DeleteCommentParams;->a:Ljava/lang/String;

    invoke-static {v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 629357
    new-instance v4, LX/58o;

    invoke-direct {v4}, LX/58o;-><init>()V

    .line 629358
    new-instance v5, LX/4DP;

    invoke-direct {v5}, LX/4DP;-><init>()V

    .line 629359
    iget-object v6, v0, Lcom/facebook/api/ufiservices/common/DeleteCommentParams;->a:Ljava/lang/String;

    .line 629360
    const-string v7, "comment_id"

    invoke-virtual {v5, v7, v6}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 629361
    const-string v6, "input"

    invoke-virtual {v4, v6, v5}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 629362
    iget-object v5, v3, LX/3iV;->l:LX/0tE;

    invoke-virtual {v5, v4}, LX/0tE;->b(LX/0gW;)V

    .line 629363
    new-instance v5, LX/399;

    invoke-direct {v5, v4}, LX/399;-><init>(LX/0zP;)V

    .line 629364
    iget-object v6, v3, LX/3iV;->b:LX/0tX;

    new-instance v4, LX/3G1;

    invoke-direct {v4}, LX/3G1;-><init>()V

    invoke-virtual {v4, v5}, LX/3G1;->a(LX/399;)LX/3G1;

    move-result-object v4

    sget-object v5, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v8, 0x1

    invoke-virtual {v5, v8, v9}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v8

    .line 629365
    iput-wide v8, v4, LX/3G2;->d:J

    .line 629366
    move-object v4, v4

    .line 629367
    const/16 v5, 0x64

    .line 629368
    iput v5, v4, LX/3G2;->f:I

    .line 629369
    move-object v4, v4

    .line 629370
    invoke-virtual {v4}, LX/3G2;->a()LX/3G3;

    move-result-object v4

    check-cast v4, LX/3G4;

    sget-object v5, LX/3Fz;->c:LX/3Fz;

    invoke-virtual {v6, v4, v5}, LX/0tX;->a(LX/3G4;LX/3Fz;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    .line 629371
    new-instance v5, LX/6PM;

    invoke-direct {v5, v3}, LX/6PM;-><init>(LX/3iV;)V

    .line 629372
    sget-object v6, LX/131;->INSTANCE:LX/131;

    move-object v6, v6

    .line 629373
    invoke-static {v4, v5, v6}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    move-object v0, v4

    .line 629374
    new-instance v3, LX/95v;

    invoke-direct {v3, p0, p3, p1}, LX/95v;-><init>(LX/3iQ;LX/1L9;Lcom/facebook/graphql/model/GraphQLComment;)V

    invoke-virtual {v1, v2, v0, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 629375
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;ZLX/1L9;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            "Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;",
            "Z",
            "LX/1L9",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 629326
    iget-object v1, p0, LX/3iQ;->f:LX/1nK;

    invoke-virtual {v1}, LX/1nK;->H()V

    .line 629327
    iget-object v2, p0, LX/3iQ;->a:LX/3iR;

    if-eqz p5, :cond_3

    sget-object v1, LX/6W6;->MANUAL_RETRY:LX/6W6;

    :goto_0
    iget-object v3, p2, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->c:Ljava/lang/String;

    move-object/from16 v0, p4

    invoke-virtual {v2, v1, v3, v0}, LX/3iR;->a(LX/6W6;Ljava/lang/String;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)V

    .line 629328
    iget-object v1, p0, LX/3iQ;->g:LX/0SI;

    invoke-interface {v1}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    if-eqz v1, :cond_4

    iget-object v1, p0, LX/3iQ;->g:LX/0SI;

    invoke-interface {v1}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/auth/viewercontext/ViewerContext;->d()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLFeedback;->R()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLFeedback;->R()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v1

    invoke-static {v1}, LX/6X4;->a(Lcom/facebook/graphql/model/GraphQLPage;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    .line 629329
    :goto_1
    iget-object v2, p0, LX/3iQ;->m:LX/0pf;

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0pf;->a(Ljava/lang/String;)LX/1g0;

    move-result-object v2

    .line 629330
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/1g0;->o()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 629331
    invoke-virtual {v2}, LX/1g0;->o()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    .line 629332
    :cond_0
    iget-object v3, p0, LX/3iQ;->c:LX/3iS;

    invoke-virtual {v3, p1, v1, p2}, LX/3iS;->a(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLActor;Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;)Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v6

    .line 629333
    iget-object v1, p0, LX/3iQ;->f:LX/1nK;

    invoke-virtual {v1}, LX/1nK;->I()V

    .line 629334
    if-eqz p6, :cond_1

    .line 629335
    move-object/from16 v0, p6

    invoke-interface {v0, v6}, LX/1L9;->a(Ljava/lang/Object;)V

    .line 629336
    :cond_1
    invoke-static {}, Lcom/facebook/api/ufiservices/common/AddCommentParams;->a()LX/5Gr;

    move-result-object v1

    iget-object v3, p2, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->a:Ljava/lang/String;

    invoke-virtual {v1, v3}, LX/5Gr;->a(Ljava/lang/String;)LX/5Gr;

    move-result-object v1

    iget-object v3, p2, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->b:Ljava/lang/String;

    invoke-virtual {v1, v3}, LX/5Gr;->b(Ljava/lang/String;)LX/5Gr;

    move-result-object v1

    iget-object v3, p2, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->c:Ljava/lang/String;

    invoke-virtual {v1, v3}, LX/5Gr;->c(Ljava/lang/String;)LX/5Gr;

    move-result-object v1

    invoke-virtual {v1, v6}, LX/5Gr;->a(Lcom/facebook/graphql/model/GraphQLComment;)LX/5Gr;

    move-result-object v1

    move-object/from16 v0, p4

    invoke-virtual {v1, v0}, LX/5Gr;->a(Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)LX/5Gr;

    move-result-object v1

    invoke-virtual {v1, p1}, LX/5Gr;->d(Ljava/lang/String;)LX/5Gr;

    move-result-object v1

    iget-boolean v3, p2, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->d:Z

    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/5Gr;->f(Ljava/lang/String;)LX/5Gr;

    move-result-object v1

    iget-boolean v3, p2, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->d:Z

    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/5Gr;->g(Ljava/lang/String;)LX/5Gr;

    move-result-object v1

    iget-boolean v3, p2, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->h:Z

    invoke-virtual {v1, v3}, LX/5Gr;->a(Z)LX/5Gr;

    move-result-object v1

    iget v3, p2, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->i:I

    invoke-virtual {v1, v3}, LX/5Gr;->a(I)LX/5Gr;

    move-result-object v1

    iget-object v3, p0, LX/3iQ;->g:LX/0SI;

    invoke-interface {v3}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/5Gr;->a(Lcom/facebook/auth/viewercontext/ViewerContext;)LX/5Gr;

    move-result-object v8

    .line 629337
    if-eqz v2, :cond_2

    invoke-virtual {v2}, LX/1g0;->p()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 629338
    invoke-virtual {v2}, LX/1g0;->p()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    invoke-virtual {v8, v1}, LX/5Gr;->a(Lcom/facebook/auth/viewercontext/ViewerContext;)LX/5Gr;

    .line 629339
    :cond_2
    iget-object v9, p0, LX/3iQ;->d:LX/1Ck;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "post_comment_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    iget-object v1, p0, LX/3iQ;->e:LX/3iU;

    const/4 v2, 0x1

    invoke-virtual {v1, p1, p2, v8, v2}, LX/3iU;->a(Ljava/lang/String;Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;LX/5Gr;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v11

    new-instance v1, LX/95q;

    move-object v2, p0

    move-object v3, p2

    move-object/from16 v4, p4

    move-object/from16 v5, p6

    move-object v7, p1

    invoke-direct/range {v1 .. v8}, LX/95q;-><init>(LX/3iQ;Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;LX/1L9;Lcom/facebook/graphql/model/GraphQLComment;Ljava/lang/String;LX/5Gr;)V

    invoke-virtual {v9, v10, v11, v1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 629340
    return-void

    .line 629341
    :cond_3
    sget-object v1, LX/6W6;->REQUEST:LX/6W6;

    goto/16 :goto_0

    .line 629342
    :cond_4
    iget-object v1, p0, LX/3iQ;->b:LX/20i;

    invoke-virtual {v1}, LX/20i;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    goto/16 :goto_1
.end method
