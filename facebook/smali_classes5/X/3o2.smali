.class public final LX/3o2;
.super Landroid/widget/FrameLayout$LayoutParams;
.source ""


# instance fields
.field public a:I

.field public b:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/high16 v2, 0x3f000000    # 0.5f

    .line 639595
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 639596
    iput v3, p0, LX/3o2;->a:I

    .line 639597
    iput v2, p0, LX/3o2;->b:F

    .line 639598
    sget-object v0, LX/03r;->CollapsingAppBarLayout_LayoutParams:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 639599
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, LX/3o2;->a:I

    .line 639600
    const/16 v1, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v1

    .line 639601
    iput v1, p0, LX/3o2;->b:F

    .line 639602
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 639603
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    .prologue
    .line 639608
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 639609
    const/4 v0, 0x0

    iput v0, p0, LX/3o2;->a:I

    .line 639610
    const/high16 v0, 0x3f000000    # 0.5f

    iput v0, p0, LX/3o2;->b:F

    .line 639611
    return-void
.end method

.method public constructor <init>(Landroid/widget/FrameLayout$LayoutParams;)V
    .locals 1

    .prologue
    .line 639604
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(Landroid/widget/FrameLayout$LayoutParams;)V

    .line 639605
    const/4 v0, 0x0

    iput v0, p0, LX/3o2;->a:I

    .line 639606
    const/high16 v0, 0x3f000000    # 0.5f

    iput v0, p0, LX/3o2;->b:F

    .line 639607
    return-void
.end method
