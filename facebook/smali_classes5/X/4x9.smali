.class public interface abstract LX/4x9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0dY;
.implements LX/4x8;


# annotations
.annotation build Lcom/google/common/annotations/Beta;
.end annotation

.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/0dY",
        "<TE;>;",
        "LX/4x8",
        "<TE;>;"
    }
.end annotation


# virtual methods
.method public abstract a(Ljava/lang/Object;LX/4xG;)LX/4x9;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;",
            "LX/4xG;",
            ")",
            "LX/4x9",
            "<TE;>;"
        }
    .end annotation
.end method

.method public abstract a(Ljava/lang/Object;LX/4xG;Ljava/lang/Object;LX/4xG;)LX/4x9;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;",
            "LX/4xG;",
            "TE;",
            "LX/4xG;",
            ")",
            "LX/4x9",
            "<TE;>;"
        }
    .end annotation
.end method

.method public abstract a()Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/google/common/collect/Multiset$Entry",
            "<TE;>;>;"
        }
    .end annotation
.end method

.method public abstract b(Ljava/lang/Object;LX/4xG;)LX/4x9;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;",
            "LX/4xG;",
            ")",
            "LX/4x9",
            "<TE;>;"
        }
    .end annotation
.end method

.method public abstract comparator()Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<-TE;>;"
        }
    .end annotation
.end method

.method public abstract g()Ljava/util/NavigableSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/NavigableSet",
            "<TE;>;"
        }
    .end annotation
.end method

.method public abstract h()LX/4wx;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/Multiset$Entry",
            "<TE;>;"
        }
    .end annotation
.end method

.method public abstract i()LX/4wx;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/Multiset$Entry",
            "<TE;>;"
        }
    .end annotation
.end method

.method public abstract j()LX/4wx;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/Multiset$Entry",
            "<TE;>;"
        }
    .end annotation
.end method

.method public abstract k()LX/4wx;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/Multiset$Entry",
            "<TE;>;"
        }
    .end annotation
.end method

.method public abstract n()LX/4x9;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/4x9",
            "<TE;>;"
        }
    .end annotation
.end method
