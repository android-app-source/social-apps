.class public final LX/4Yv;
.super LX/0ur;
.source ""


# instance fields
.field public b:Lcom/facebook/graphql/model/GraphQLSavable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:I

.field public d:I

.field public e:Lcom/facebook/graphql/enums/GraphQLStorySaveNuxType;

.field public f:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

.field public g:Lcom/facebook/graphql/enums/GraphQLSavedState;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 784800
    invoke-direct {p0}, LX/0ur;-><init>()V

    .line 784801
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStorySaveNuxType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStorySaveNuxType;

    iput-object v0, p0, LX/4Yv;->e:Lcom/facebook/graphql/enums/GraphQLStorySaveNuxType;

    .line 784802
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStorySaveType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    iput-object v0, p0, LX/4Yv;->f:Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    .line 784803
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSavedState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    iput-object v0, p0, LX/4Yv;->g:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 784804
    instance-of v0, p0, LX/4Yv;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 784805
    return-void
.end method
