.class public final LX/3wX;
.super Landroid/support/v7/internal/widget/TintImageView;
.source ""

# interfaces
.implements LX/3ur;


# instance fields
.field public final synthetic a:LX/3wb;

.field private final b:[F


# direct methods
.method public constructor <init>(LX/3wb;Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 655374
    iput-object p1, p0, LX/3wX;->a:LX/3wb;

    .line 655375
    const/4 v0, 0x0

    const v1, 0x7f01000e

    invoke-direct {p0, p2, v0, v1}, Landroid/support/v7/internal/widget/TintImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 655376
    const/4 v0, 0x2

    new-array v0, v0, [F

    iput-object v0, p0, LX/3wX;->b:[F

    .line 655377
    invoke-virtual {p0, v2}, LX/3wX;->setClickable(Z)V

    .line 655378
    invoke-virtual {p0, v2}, LX/3wX;->setFocusable(Z)V

    .line 655379
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/3wX;->setVisibility(I)V

    .line 655380
    invoke-virtual {p0, v2}, LX/3wX;->setEnabled(Z)V

    .line 655381
    new-instance v0, LX/3wW;

    invoke-direct {v0, p0, p0, p1}, LX/3wW;-><init>(LX/3wX;Landroid/view/View;LX/3wb;)V

    invoke-virtual {p0, v0}, LX/3wX;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 655382
    return-void
.end method


# virtual methods
.method public final c()Z
    .locals 1

    .prologue
    .line 655373
    const/4 v0, 0x0

    return v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 655383
    const/4 v0, 0x0

    return v0
.end method

.method public final performClick()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 655369
    invoke-super {p0}, Landroid/support/v7/internal/widget/TintImageView;->performClick()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 655370
    :goto_0
    return v1

    .line 655371
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/3wX;->playSoundEffect(I)V

    .line 655372
    iget-object v0, p0, LX/3wX;->a:LX/3wb;

    invoke-virtual {v0}, LX/3wb;->d()Z

    goto :goto_0
.end method

.method public final setFrame(IIII)Z
    .locals 8

    .prologue
    .line 655356
    invoke-super {p0, p1, p2, p3, p4}, Landroid/support/v7/internal/widget/TintImageView;->setFrame(IIII)Z

    move-result v0

    .line 655357
    invoke-virtual {p0}, LX/3wX;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 655358
    invoke-virtual {p0}, LX/3wX;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 655359
    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    .line 655360
    invoke-virtual {p0}, LX/3wX;->getWidth()I

    move-result v1

    .line 655361
    invoke-virtual {p0}, LX/3wX;->getHeight()I

    move-result v3

    .line 655362
    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    .line 655363
    invoke-virtual {p0}, LX/3wX;->getPaddingLeft()I

    move-result v5

    invoke-virtual {p0}, LX/3wX;->getPaddingRight()I

    move-result v6

    sub-int/2addr v5, v6

    .line 655364
    invoke-virtual {p0}, LX/3wX;->getPaddingTop()I

    move-result v6

    invoke-virtual {p0}, LX/3wX;->getPaddingBottom()I

    move-result v7

    sub-int/2addr v6, v7

    .line 655365
    add-int/2addr v1, v5

    div-int/lit8 v1, v1, 0x2

    .line 655366
    add-int/2addr v3, v6

    div-int/lit8 v3, v3, 0x2

    .line 655367
    sub-int v5, v1, v4

    sub-int v6, v3, v4

    add-int/2addr v1, v4

    add-int/2addr v3, v4

    invoke-static {v2, v5, v6, v1, v3}, LX/1d5;->a(Landroid/graphics/drawable/Drawable;IIII)V

    .line 655368
    :cond_0
    return v0
.end method
