.class public final LX/3y5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field public final synthetic a:LX/0Uh;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Lcom/facebook/gk/store/GatekeeperWriter;

.field public final synthetic d:Z

.field public final synthetic e:Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;LX/0Uh;Ljava/lang/String;Lcom/facebook/gk/store/GatekeeperWriter;Z)V
    .locals 0

    .prologue
    .line 660526
    iput-object p1, p0, LX/3y5;->e:Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;

    iput-object p2, p0, LX/3y5;->a:LX/0Uh;

    iput-object p3, p0, LX/3y5;->b:Ljava/lang/String;

    iput-object p4, p0, LX/3y5;->c:Lcom/facebook/gk/store/GatekeeperWriter;

    iput-boolean p5, p0, LX/3y5;->d:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 660527
    iget-object v0, p0, LX/3y5;->a:LX/0Uh;

    iget-object v3, p0, LX/3y5;->b:Ljava/lang/String;

    invoke-virtual {v0, v3}, LX/0Uh;->a(Ljava/lang/String;)LX/03R;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 660528
    :goto_0
    iget-object v3, p0, LX/3y5;->c:Lcom/facebook/gk/store/GatekeeperWriter;

    invoke-interface {v3}, Lcom/facebook/gk/store/GatekeeperWriter;->e()LX/2LD;

    move-result-object v3

    iget-object v4, p0, LX/3y5;->b:Ljava/lang/String;

    invoke-interface {v3, v4, v0}, LX/2LD;->a(Ljava/lang/String;Z)LX/2LD;

    move-result-object v3

    invoke-interface {v3, v1}, LX/2LD;->a(Z)V

    .line 660529
    const-string v3, "%1$s has been updated to %2$s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, LX/3y5;->b:Ljava/lang/String;

    aput-object v5, v4, v2

    invoke-static {v0}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v1

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 660530
    iget-object v1, p0, LX/3y5;->e:Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;

    invoke-virtual {v1}, Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 660531
    iget-object v0, p0, LX/3y5;->e:Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;

    iget-object v1, p0, LX/3y5;->b:Ljava/lang/String;

    iget-boolean v3, p0, LX/3y5;->d:Z

    invoke-static {v0, v1, v3}, Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;->b(Lcom/facebook/abtest/gkprefs/GkSettingsListActivity;Ljava/lang/String;Z)V

    .line 660532
    iget-object v0, p0, LX/3y5;->a:LX/0Uh;

    iget-object v1, p0, LX/3y5;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0Uh;->a(Ljava/lang/String;)LX/03R;

    move-result-object v0

    invoke-virtual {v0}, LX/03R;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 660533
    return v2

    :cond_0
    move v0, v2

    .line 660534
    goto :goto_0
.end method
