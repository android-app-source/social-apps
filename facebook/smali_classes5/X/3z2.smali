.class public final LX/3z2;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/3z2;


# instance fields
.field public final a:LX/0cb;

.field private final b:LX/2VP;

.field private final c:LX/2VU;

.field private final d:LX/0TD;

.field private final e:LX/0ae;


# direct methods
.method public constructor <init>(LX/0cb;LX/2VP;LX/2VU;LX/0TD;LX/0ae;)V
    .locals 0
    .param p4    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 662062
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 662063
    iput-object p1, p0, LX/3z2;->a:LX/0cb;

    .line 662064
    iput-object p2, p0, LX/3z2;->b:LX/2VP;

    .line 662065
    iput-object p3, p0, LX/3z2;->c:LX/2VU;

    .line 662066
    iput-object p4, p0, LX/3z2;->d:LX/0TD;

    .line 662067
    iput-object p5, p0, LX/3z2;->e:LX/0ae;

    .line 662068
    return-void
.end method

.method public static a(LX/0QB;)LX/3z2;
    .locals 9

    .prologue
    .line 662049
    sget-object v0, LX/3z2;->f:LX/3z2;

    if-nez v0, :cond_1

    .line 662050
    const-class v1, LX/3z2;

    monitor-enter v1

    .line 662051
    :try_start_0
    sget-object v0, LX/3z2;->f:LX/3z2;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 662052
    if-eqz v2, :cond_0

    .line 662053
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 662054
    new-instance v3, LX/3z2;

    invoke-static {v0}, LX/0ca;->a(LX/0QB;)LX/0ca;

    move-result-object v4

    check-cast v4, LX/0cb;

    invoke-static {v0}, LX/2VP;->b(LX/0QB;)LX/2VP;

    move-result-object v5

    check-cast v5, LX/2VP;

    invoke-static {v0}, LX/2VU;->b(LX/0QB;)LX/2VU;

    move-result-object v6

    check-cast v6, LX/2VU;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v7

    check-cast v7, LX/0TD;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ae;

    invoke-direct/range {v3 .. v8}, LX/3z2;-><init>(LX/0cb;LX/2VP;LX/2VU;LX/0TD;LX/0ae;)V

    .line 662055
    move-object v0, v3

    .line 662056
    sput-object v0, LX/3z2;->f:LX/3z2;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 662057
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 662058
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 662059
    :cond_1
    sget-object v0, LX/3z2;->f:LX/3z2;

    return-object v0

    .line 662060
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 662061
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 662069
    iget-object v0, p0, LX/3z2;->a:LX/0cb;

    invoke-interface {v0, p1}, LX/0cb;->b(Ljava/lang/String;)Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;

    move-result-object v3

    .line 662070
    iget-object v0, p0, LX/3z2;->a:LX/0cb;

    invoke-interface {v0, p1}, LX/0cb;->a(Ljava/lang/String;)Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationModel;

    move-result-object v0

    .line 662071
    if-nez v0, :cond_0

    .line 662072
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The meta info has not been sync\'d"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 662073
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationModel;->e_()Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationParameterSetsConnectionModel;

    move-result-object v0

    const-string v1, "Parameter sets not found. Missing GraphQL data."

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationParameterSetsConnectionModel;

    .line 662074
    invoke-virtual {v0}, Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationParameterSetsConnectionModel;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    const/4 v1, 0x0

    move v4, v1

    :goto_0
    if-ge v4, v6, :cond_3

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationParameterSetsConnectionModel$EdgesModel;

    .line 662075
    invoke-virtual {v1}, Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationParameterSetsConnectionModel$EdgesModel;->a()Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationParameterSetModel;

    move-result-object v1

    .line 662076
    if-eqz v1, :cond_2

    .line 662077
    invoke-virtual {v1}, Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationParameterSetModel;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 662078
    :goto_1
    move-object v0, v1

    .line 662079
    const-string v1, "Parameter set with name %s not found."

    new-array v4, v8, [Ljava/lang/Object;

    aput-object p2, v4, v2

    invoke-static {v0, v1, v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationParameterSetModel;

    .line 662080
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 662081
    invoke-virtual {v0}, Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationParameterSetModel;->f_()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v1, v2

    :goto_2
    if-ge v1, v6, :cond_1

    invoke-virtual {v5, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$QueryStringConfigurationParameterModel;

    .line 662082
    invoke-virtual {v0}, Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$QueryStringConfigurationParameterModel;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0}, Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$QueryStringConfigurationParameterModel;->g_()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v7, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 662083
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 662084
    :cond_1
    new-instance v0, LX/2WQ;

    invoke-direct {v0}, LX/2WQ;-><init>()V

    .line 662085
    iget-object v1, v3, LX/2Wx;->a:Ljava/lang/String;

    move-object v1, v1

    .line 662086
    invoke-virtual {v0, v1}, LX/2WQ;->e(Ljava/lang/String;)LX/2WQ;

    move-result-object v0

    .line 662087
    iget-object v1, v3, LX/2Wx;->b:Ljava/lang/String;

    move-object v1, v1

    .line 662088
    invoke-virtual {v0, v1}, LX/2WQ;->g(Ljava/lang/String;)LX/2WQ;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/2WQ;->f(Ljava/lang/String;)LX/2WQ;

    move-result-object v0

    invoke-virtual {v0, v8}, LX/2WQ;->c(Z)LX/2WQ;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/2WQ;->d(Z)LX/2WQ;

    move-result-object v0

    .line 662089
    iget-object v1, v3, LX/2Wx;->f:Ljava/lang/String;

    move-object v1, v1

    .line 662090
    invoke-virtual {v0, v1}, LX/2WQ;->h(Ljava/lang/String;)LX/2WQ;

    move-result-object v0

    .line 662091
    iput-object v4, v0, LX/2WQ;->g:Ljava/util/Map;

    .line 662092
    move-object v0, v0

    .line 662093
    invoke-virtual {v0}, LX/2WQ;->a()Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;

    move-result-object v0

    return-object v0

    .line 662094
    :cond_2
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_0

    .line 662095
    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static c(LX/3z2;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 662028
    iget-object v0, p0, LX/3z2;->e:LX/0ae;

    invoke-interface {v0, p1}, LX/0ae;->b(Ljava/lang/String;)Z

    move-result v0

    .line 662029
    if-eqz p2, :cond_2

    .line 662030
    invoke-direct {p0, p1, p2}, LX/3z2;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;

    move-result-object v1

    .line 662031
    if-eqz v0, :cond_0

    .line 662032
    iget-object v2, p0, LX/3z2;->e:LX/0ae;

    invoke-interface {v2, v1}, LX/0ae;->a(Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;)V

    .line 662033
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 662034
    iget-object v0, p0, LX/3z2;->c:LX/2VU;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/2VU;->a(Z)V

    .line 662035
    :cond_1
    return-void

    .line 662036
    :cond_2
    const/4 p2, 0x0

    .line 662037
    iget-object v1, p0, LX/3z2;->a:LX/0cb;

    invoke-interface {v1, p1}, LX/0cb;->b(Ljava/lang/String;)Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;

    move-result-object v1

    .line 662038
    new-instance v2, LX/2WQ;

    invoke-direct {v2}, LX/2WQ;-><init>()V

    invoke-virtual {v2, p1}, LX/2WQ;->e(Ljava/lang/String;)LX/2WQ;

    move-result-object v2

    .line 662039
    iget-object v3, v1, LX/2Wx;->b:Ljava/lang/String;

    move-object v3, v3

    .line 662040
    invoke-virtual {v2, v3}, LX/2WQ;->g(Ljava/lang/String;)LX/2WQ;

    move-result-object v2

    const-string v3, "local_default_group"

    invoke-virtual {v2, v3}, LX/2WQ;->f(Ljava/lang/String;)LX/2WQ;

    move-result-object v2

    invoke-virtual {v2, p2}, LX/2WQ;->c(Z)LX/2WQ;

    move-result-object v2

    invoke-virtual {v2, p2}, LX/2WQ;->d(Z)LX/2WQ;

    move-result-object v2

    .line 662041
    iget-object v3, v1, LX/2Wx;->f:Ljava/lang/String;

    move-object v1, v3

    .line 662042
    invoke-virtual {v2, v1}, LX/2WQ;->h(Ljava/lang/String;)LX/2WQ;

    move-result-object v1

    .line 662043
    sget-object v2, LX/0Rg;->a:LX/0Rg;

    move-object v2, v2

    .line 662044
    iput-object v2, v1, LX/2WQ;->g:Ljava/util/Map;

    .line 662045
    move-object v1, v1

    .line 662046
    invoke-virtual {v1}, LX/2WQ;->a()Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;

    .line 662047
    if-eqz v0, :cond_0

    .line 662048
    iget-object v1, p0, LX/3z2;->e:LX/0ae;

    invoke-interface {v1, p1}, LX/0ae;->e(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 662023
    iget-object v0, p0, LX/3z2;->e:LX/0ae;

    invoke-interface {v0, p1}, LX/0ae;->b(Ljava/lang/String;)Z

    move-result v0

    .line 662024
    if-eqz v0, :cond_0

    .line 662025
    iget-object v0, p0, LX/3z2;->e:LX/0ae;

    invoke-interface {v0, p1}, LX/0ae;->d(Ljava/lang/String;)V

    .line 662026
    :goto_0
    return-void

    .line 662027
    :cond_0
    iget-object v0, p0, LX/3z2;->c:LX/2VU;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/2VU;->a(Z)V

    goto :goto_0
.end method
