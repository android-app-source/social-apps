.class public LX/3mH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Zd;
.implements LX/0Ze;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/3mH;


# instance fields
.field private final a:LX/0So;

.field private final b:LX/0Yw;


# direct methods
.method public constructor <init>(LX/0So;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 636125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 636126
    iput-object p1, p0, LX/3mH;->a:LX/0So;

    .line 636127
    new-instance v0, LX/0Yw;

    const v1, 0x7fffffff

    const/16 v2, 0x3c

    invoke-direct {v0, v1, v2}, LX/0Yw;-><init>(II)V

    iput-object v0, p0, LX/3mH;->b:LX/0Yw;

    .line 636128
    return-void
.end method

.method public static a(LX/0QB;)LX/3mH;
    .locals 4

    .prologue
    .line 636112
    sget-object v0, LX/3mH;->c:LX/3mH;

    if-nez v0, :cond_1

    .line 636113
    const-class v1, LX/3mH;

    monitor-enter v1

    .line 636114
    :try_start_0
    sget-object v0, LX/3mH;->c:LX/3mH;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 636115
    if-eqz v2, :cond_0

    .line 636116
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 636117
    new-instance p0, LX/3mH;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v3

    check-cast v3, LX/0So;

    invoke-direct {p0, v3}, LX/3mH;-><init>(LX/0So;)V

    .line 636118
    move-object v0, p0

    .line 636119
    sput-object v0, LX/3mH;->c:LX/3mH;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 636120
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 636121
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 636122
    :cond_1
    sget-object v0, LX/3mH;->c:LX/3mH;

    return-object v0

    .line 636123
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 636124
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/Object;ZLjava/lang/String;LX/AkV;)V
    .locals 6
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 636129
    iget-object v0, p0, LX/3mH;->b:LX/0Yw;

    const-string v1, "{\'%s\', \'%s\', \'%s\', \'%s\', \'%s\', \'%s\'}"

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LX/3mH;->a:LX/0So;

    invoke-interface {v4}, LX/0So;->now()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    const/4 v3, 0x2

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    aput-object p2, v2, v3

    const/4 v3, 0x4

    aput-object p4, v2, v3

    const/4 v3, 0x5

    aput-object p5, v2, v3

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Yw;->a(Ljava/lang/String;)V

    .line 636130
    return-void
.end method

.method public final b()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 636111
    invoke-virtual {p0}, LX/3mH;->d()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 636110
    const/4 v0, 0x0

    return-object v0
.end method

.method public final d()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 636109
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    const-string v1, "story_like_history"

    iget-object v2, p0, LX/3mH;->b:LX/0Yw;

    invoke-virtual {v2}, LX/0Yw;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    return-object v0
.end method
