.class public LX/3f1;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "LX/1qM;",
        ">;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 621194
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/1qM;
    .locals 5

    .prologue
    .line 621196
    const-class v1, LX/3f1;

    monitor-enter v1

    .line 621197
    :try_start_0
    sget-object v0, LX/3f1;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 621198
    sput-object v2, LX/3f1;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 621199
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 621200
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 621201
    invoke-static {v0}, LX/3f2;->b(LX/0QB;)LX/3f2;

    move-result-object v3

    check-cast v3, LX/3f2;

    invoke-static {v0}, LX/3f3;->b(LX/0QB;)LX/3f3;

    move-result-object v4

    check-cast v4, LX/3f3;

    invoke-static {v0}, Lcom/facebook/pages/adminedpages/service/AdminedPagesWebServiceHandler;->b(LX/0QB;)Lcom/facebook/pages/adminedpages/service/AdminedPagesWebServiceHandler;

    move-result-object p0

    check-cast p0, Lcom/facebook/pages/adminedpages/service/AdminedPagesWebServiceHandler;

    invoke-static {v3, v4, p0}, LX/2Uw;->a(LX/3f2;LX/3f3;Lcom/facebook/pages/adminedpages/service/AdminedPagesWebServiceHandler;)LX/1qM;

    move-result-object v3

    move-object v0, v3

    .line 621202
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 621203
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1qM;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 621204
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 621205
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 621195
    invoke-static {p0}, LX/3f2;->b(LX/0QB;)LX/3f2;

    move-result-object v0

    check-cast v0, LX/3f2;

    invoke-static {p0}, LX/3f3;->b(LX/0QB;)LX/3f3;

    move-result-object v1

    check-cast v1, LX/3f3;

    invoke-static {p0}, Lcom/facebook/pages/adminedpages/service/AdminedPagesWebServiceHandler;->b(LX/0QB;)Lcom/facebook/pages/adminedpages/service/AdminedPagesWebServiceHandler;

    move-result-object v2

    check-cast v2, Lcom/facebook/pages/adminedpages/service/AdminedPagesWebServiceHandler;

    invoke-static {v0, v1, v2}, LX/2Uw;->a(LX/3f2;LX/3f3;Lcom/facebook/pages/adminedpages/service/AdminedPagesWebServiceHandler;)LX/1qM;

    move-result-object v0

    return-object v0
.end method
