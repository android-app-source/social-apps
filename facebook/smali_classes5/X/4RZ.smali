.class public LX/4RZ;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 709254
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 21

    .prologue
    .line 709255
    const/4 v15, 0x0

    .line 709256
    const/4 v14, 0x0

    .line 709257
    const-wide/16 v12, 0x0

    .line 709258
    const/4 v11, 0x0

    .line 709259
    const/4 v10, 0x0

    .line 709260
    const/4 v9, 0x0

    .line 709261
    const/4 v8, 0x0

    .line 709262
    const/4 v7, 0x0

    .line 709263
    const/4 v6, 0x0

    .line 709264
    const/4 v5, 0x0

    .line 709265
    const/4 v4, 0x0

    .line 709266
    const/4 v3, 0x0

    .line 709267
    const/4 v2, 0x0

    .line 709268
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v16

    sget-object v17, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    if-eq v0, v1, :cond_f

    .line 709269
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 709270
    const/4 v2, 0x0

    .line 709271
    :goto_0
    return v2

    .line 709272
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v17, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v17

    if-eq v3, v0, :cond_d

    .line 709273
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 709274
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 709275
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v17

    sget-object v18, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    if-eq v0, v1, :cond_0

    if-eqz v3, :cond_0

    .line 709276
    const-string v17, "cache_id"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_1

    .line 709277
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v7, v3

    goto :goto_1

    .line 709278
    :cond_1
    const-string v17, "debug_info"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_2

    .line 709279
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v6, v3

    goto :goto_1

    .line 709280
    :cond_2
    const-string v17, "fetchTimeMs"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_3

    .line 709281
    const/4 v2, 0x1

    .line 709282
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    goto :goto_1

    .line 709283
    :cond_3
    const-string v17, "id"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_4

    .line 709284
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move/from16 v16, v3

    goto :goto_1

    .line 709285
    :cond_4
    const-string v17, "politicalIssuePivotItems"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_5

    .line 709286
    invoke-static/range {p0 .. p1}, LX/4Ra;->a(LX/15w;LX/186;)I

    move-result v3

    move v15, v3

    goto/16 :goto_1

    .line 709287
    :cond_5
    const-string v17, "politicalIssuePivotTitle"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_6

    .line 709288
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v3

    move v14, v3

    goto/16 :goto_1

    .line 709289
    :cond_6
    const-string v17, "short_term_cache_key"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_7

    .line 709290
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v13, v3

    goto/16 :goto_1

    .line 709291
    :cond_7
    const-string v17, "title"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_8

    .line 709292
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v3

    move v12, v3

    goto/16 :goto_1

    .line 709293
    :cond_8
    const-string v17, "titleForSummary"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_9

    .line 709294
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v3

    move v11, v3

    goto/16 :goto_1

    .line 709295
    :cond_9
    const-string v17, "tracking"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_a

    .line 709296
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v10, v3

    goto/16 :goto_1

    .line 709297
    :cond_a
    const-string v17, "url"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_b

    .line 709298
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v9, v3

    goto/16 :goto_1

    .line 709299
    :cond_b
    const-string v17, "politicalIssueLinkText"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 709300
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v3

    move v8, v3

    goto/16 :goto_1

    .line 709301
    :cond_c
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 709302
    :cond_d
    const/16 v3, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 709303
    const/4 v3, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 709304
    const/4 v3, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 709305
    if-eqz v2, :cond_e

    .line 709306
    const/4 v3, 0x3

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 709307
    :cond_e
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 709308
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 709309
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 709310
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 709311
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 709312
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 709313
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 709314
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 709315
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 709316
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_f
    move/from16 v16, v11

    move v11, v6

    move v6, v14

    move v14, v9

    move v9, v4

    move/from16 v19, v8

    move v8, v3

    move/from16 v20, v7

    move v7, v15

    move v15, v10

    move v10, v5

    move-wide v4, v12

    move/from16 v13, v19

    move/from16 v12, v20

    goto/16 :goto_1
.end method

.method public static a(LX/15w;S)LX/15i;
    .locals 5

    .prologue
    .line 709317
    const/4 v0, 0x1

    const/4 v4, 0x0

    .line 709318
    new-instance v2, LX/186;

    const/16 v1, 0x80

    invoke-direct {v2, v1}, LX/186;-><init>(I)V

    .line 709319
    invoke-static {p0, v2}, LX/4RZ;->a(LX/15w;LX/186;)I

    move-result v1

    .line 709320
    if-eqz v0, :cond_0

    .line 709321
    const/4 v3, 0x2

    invoke-virtual {v2, v3}, LX/186;->c(I)V

    .line 709322
    invoke-virtual {v2, v4, p1, v4}, LX/186;->a(ISI)V

    .line 709323
    const/4 v3, 0x1

    invoke-virtual {v2, v3, v1}, LX/186;->b(II)V

    .line 709324
    invoke-virtual {v2}, LX/186;->d()I

    move-result v1

    .line 709325
    :cond_0
    invoke-virtual {v2, v1}, LX/186;->d(I)V

    .line 709326
    invoke-static {v2}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v1

    move-object v0, v1

    .line 709327
    return-object v0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 709328
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 709329
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 709330
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 709331
    const-string v0, "name"

    const-string v1, "PoliticalIssuePivotFeedUnit"

    invoke-virtual {p2, v0, v1}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 709332
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 709333
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 709334
    if-eqz v0, :cond_0

    .line 709335
    const-string v1, "cache_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 709336
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 709337
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 709338
    if-eqz v0, :cond_1

    .line 709339
    const-string v1, "debug_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 709340
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 709341
    :cond_1
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 709342
    cmp-long v2, v0, v2

    if-eqz v2, :cond_2

    .line 709343
    const-string v2, "fetchTimeMs"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 709344
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 709345
    :cond_2
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 709346
    if-eqz v0, :cond_3

    .line 709347
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 709348
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 709349
    :cond_3
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 709350
    if-eqz v0, :cond_4

    .line 709351
    const-string v1, "politicalIssuePivotItems"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 709352
    invoke-static {p0, v0, p2, p3}, LX/4Ra;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 709353
    :cond_4
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 709354
    if-eqz v0, :cond_5

    .line 709355
    const-string v1, "politicalIssuePivotTitle"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 709356
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 709357
    :cond_5
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 709358
    if-eqz v0, :cond_6

    .line 709359
    const-string v1, "short_term_cache_key"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 709360
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 709361
    :cond_6
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 709362
    if-eqz v0, :cond_7

    .line 709363
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 709364
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 709365
    :cond_7
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 709366
    if-eqz v0, :cond_8

    .line 709367
    const-string v1, "titleForSummary"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 709368
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 709369
    :cond_8
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 709370
    if-eqz v0, :cond_9

    .line 709371
    const-string v1, "tracking"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 709372
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 709373
    :cond_9
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 709374
    if-eqz v0, :cond_a

    .line 709375
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 709376
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 709377
    :cond_a
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 709378
    if-eqz v0, :cond_b

    .line 709379
    const-string v1, "politicalIssueLinkText"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 709380
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 709381
    :cond_b
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 709382
    return-void
.end method
