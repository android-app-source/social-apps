.class public LX/3ba;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1TE;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/1Cz;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile b:LX/3ba;


# direct methods
.method public static constructor <clinit>()V
    .locals 15

    .prologue
    .line 611275
    sget-object v0, Lcom/facebook/search/typeahead/rows/UnsupportedSearchTypeaheadPartDefinition;->a:LX/1Cz;

    sget-object v1, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEntityPartDefinition;->a:LX/1Cz;

    sget-object v2, Lcom/facebook/search/typeahead/rows/SearchTypeaheadSimpleEntityPartDefinition;->a:LX/1Cz;

    sget-object v3, Lcom/facebook/search/typeahead/rows/SearchTypeaheadNeueTrendingEntityPartDefinition;->a:LX/1Cz;

    sget-object v4, Lcom/facebook/search/typeahead/rows/SearchTypeaheadDividerPartDefinition;->a:LX/1Cz;

    sget-object v5, Lcom/facebook/search/typeahead/rows/SearchTypeaheadQRCodePartDefinition;->a:LX/1Cz;

    sget-object v6, Lcom/facebook/search/typeahead/rows/SearchTypeaheadHeaderPartDefinition;->a:LX/1Cz;

    sget-object v7, Lcom/facebook/search/typeahead/rows/SearchTypeaheadRecentSeeMorePartDefinition;->a:LX/1Cz;

    sget-object v8, Lcom/facebook/search/typeahead/rows/SearchTypeaheadNullStateSuggestionPartDefinition;->a:LX/1Cz;

    sget-object v9, Lcom/facebook/search/typeahead/rows/SearchTypeaheadPlaceTipsPartDefinition;->a:LX/1Cz;

    sget-object v10, Lcom/facebook/search/typeahead/rows/SearchTypeaheadFindMorePartDefinition;->a:LX/1Cz;

    sget-object v11, Lcom/facebook/search/typeahead/rows/SearchTypeaheadKeywordPartDefinition;->a:LX/1Cz;

    const/16 v12, 0xa

    new-array v12, v12, [LX/1Cz;

    const/4 v13, 0x0

    sget-object v14, Lcom/facebook/search/typeahead/rows/SearchTypeaheadShortcutPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/4 v13, 0x1

    sget-object v14, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEmptyScopedNullStateDefaultPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/4 v13, 0x2

    sget-object v14, Lcom/facebook/search/typeahead/rows/HScrollSearchSpotlightCardPagePartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/4 v13, 0x3

    sget-object v14, Lcom/facebook/search/typeahead/rows/nullstate/NullStateFigSeeMorePartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/4 v13, 0x4

    sget-object v14, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleItemPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/4 v13, 0x5

    sget-object v14, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleItemPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/4 v13, 0x6

    sget-object v14, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleSeeMorePartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/4 v13, 0x7

    sget-object v14, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleGapPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x8

    sget-object v14, Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleTitlePartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x9

    sget-object v14, Lcom/facebook/search/typeahead/rows/SearchTypeaheadGapPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    invoke-static/range {v0 .. v12}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/3ba;->a:LX/0Px;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 611276
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 611277
    return-void
.end method

.method public static a(LX/0QB;)LX/3ba;
    .locals 3

    .prologue
    .line 611278
    sget-object v0, LX/3ba;->b:LX/3ba;

    if-nez v0, :cond_1

    .line 611279
    const-class v1, LX/3ba;

    monitor-enter v1

    .line 611280
    :try_start_0
    sget-object v0, LX/3ba;->b:LX/3ba;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 611281
    if-eqz v2, :cond_0

    .line 611282
    :try_start_1
    new-instance v0, LX/3ba;

    invoke-direct {v0}, LX/3ba;-><init>()V

    .line 611283
    move-object v0, v0

    .line 611284
    sput-object v0, LX/3ba;->b:LX/3ba;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 611285
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 611286
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 611287
    :cond_1
    sget-object v0, LX/3ba;->b:LX/3ba;

    return-object v0

    .line 611288
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 611289
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1KB;)V
    .locals 3

    .prologue
    .line 611290
    sget-object v0, LX/3ba;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    sget-object v0, LX/3ba;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Cz;

    .line 611291
    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 611292
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 611293
    :cond_0
    return-void
.end method
