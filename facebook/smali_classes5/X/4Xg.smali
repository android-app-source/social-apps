.class public final LX/4Xg;
.super LX/0ur;
.source ""


# instance fields
.field public b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:J

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/model/GraphQLGroup;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 772043
    invoke-direct {p0}, LX/0ur;-><init>()V

    .line 772044
    const/4 v0, 0x0

    iput-object v0, p0, LX/4Xg;->o:LX/0x2;

    .line 772045
    instance-of v0, p0, LX/4Xg;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 772046
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;)LX/4Xg;
    .locals 4

    .prologue
    .line 772047
    new-instance v1, LX/4Xg;

    invoke-direct {v1}, LX/4Xg;-><init>()V

    .line 772048
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 772049
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/4Xg;->b:Ljava/lang/String;

    .line 772050
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;->E_()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/4Xg;->c:Ljava/lang/String;

    .line 772051
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;->F_()J

    move-result-wide v2

    iput-wide v2, v1, LX/4Xg;->d:J

    .line 772052
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;->o()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/4Xg;->e:Ljava/lang/String;

    .line 772053
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;->r()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/4Xg;->f:Ljava/lang/String;

    .line 772054
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;->x()LX/0Px;

    move-result-object v0

    iput-object v0, v1, LX/4Xg;->g:LX/0Px;

    .line 772055
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;->s()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/4Xg;->h:Ljava/lang/String;

    .line 772056
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;->t()Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnection;

    move-result-object v0

    iput-object v0, v1, LX/4Xg;->i:Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnection;

    .line 772057
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;->y()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v0

    iput-object v0, v1, LX/4Xg;->j:Lcom/facebook/graphql/model/GraphQLGroup;

    .line 772058
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    iput-object v0, v1, LX/4Xg;->k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 772059
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;->v()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    iput-object v0, v1, LX/4Xg;->l:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 772060
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/4Xg;->m:Ljava/lang/String;

    .line 772061
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;->w()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/4Xg;->n:Ljava/lang/String;

    .line 772062
    invoke-static {v1, p0}, LX/0ur;->a(LX/0ur;Lcom/facebook/graphql/modelutil/BaseModel;)V

    .line 772063
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;->L_()LX/0x2;

    move-result-object v0

    invoke-virtual {v0}, LX/0x2;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0x2;

    iput-object v0, v1, LX/4Xg;->o:LX/0x2;

    .line 772064
    return-object v1
.end method


# virtual methods
.method public final a()Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;
    .locals 2

    .prologue
    .line 772065
    new-instance v0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;

    invoke-direct {v0, p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;-><init>(LX/4Xg;)V

    .line 772066
    return-object v0
.end method
