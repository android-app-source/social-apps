.class public final LX/4yw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Ljava/util/Map$Entry",
        "<TK;TV;>;>;"
    }
.end annotation


# instance fields
.field public a:LX/4yy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4yy",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public b:LX/4yy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4yy",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public final synthetic c:LX/4z1;


# direct methods
.method public constructor <init>(LX/4z1;)V
    .locals 1

    .prologue
    .line 822102
    iput-object p1, p0, LX/4yw;->c:LX/4z1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 822103
    iget-object v0, p0, LX/4yw;->c:LX/4z1;

    iget-object v0, v0, LX/4z1;->b:LX/4yy;

    iget-object v0, v0, LX/4yy;->successorInMultimap:LX/4yy;

    iput-object v0, p0, LX/4yw;->a:LX/4yy;

    return-void
.end method


# virtual methods
.method public final hasNext()Z
    .locals 2

    .prologue
    .line 822101
    iget-object v0, p0, LX/4yw;->a:LX/4yy;

    iget-object v1, p0, LX/4yw;->c:LX/4z1;

    iget-object v1, v1, LX/4z1;->b:LX/4yy;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final next()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 822090
    invoke-virtual {p0}, LX/4yw;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 822091
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 822092
    :cond_0
    iget-object v0, p0, LX/4yw;->a:LX/4yy;

    .line 822093
    iput-object v0, p0, LX/4yw;->b:LX/4yy;

    .line 822094
    iget-object v1, p0, LX/4yw;->a:LX/4yy;

    iget-object v1, v1, LX/4yy;->successorInMultimap:LX/4yy;

    iput-object v1, p0, LX/4yw;->a:LX/4yy;

    .line 822095
    return-object v0
.end method

.method public final remove()V
    .locals 3

    .prologue
    .line 822096
    iget-object v0, p0, LX/4yw;->b:LX/4yy;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0P6;->a(Z)V

    .line 822097
    iget-object v0, p0, LX/4yw;->c:LX/4z1;

    iget-object v1, p0, LX/4yw;->b:LX/4yy;

    invoke-virtual {v1}, LX/0P4;->getKey()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LX/4yw;->b:LX/4yy;

    invoke-virtual {v2}, LX/0P4;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0Xt;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 822098
    const/4 v0, 0x0

    iput-object v0, p0, LX/4yw;->b:LX/4yy;

    .line 822099
    return-void

    .line 822100
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
