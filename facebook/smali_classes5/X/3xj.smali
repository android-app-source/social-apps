.class public abstract LX/3xj;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:LX/3xn;

.field private static final b:Landroid/view/animation/Interpolator;

.field private static final c:Landroid/view/animation/Interpolator;


# instance fields
.field public d:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 659869
    new-instance v0, LX/3xh;

    invoke-direct {v0}, LX/3xh;-><init>()V

    sput-object v0, LX/3xj;->b:Landroid/view/animation/Interpolator;

    .line 659870
    new-instance v0, LX/3xi;

    invoke-direct {v0}, LX/3xi;-><init>()V

    sput-object v0, LX/3xj;->c:Landroid/view/animation/Interpolator;

    .line 659871
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 659872
    new-instance v0, LX/3xq;

    invoke-direct {v0}, LX/3xq;-><init>()V

    sput-object v0, LX/3xj;->a:LX/3xn;

    .line 659873
    :goto_0
    return-void

    .line 659874
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_1

    .line 659875
    new-instance v0, LX/3xp;

    invoke-direct {v0}, LX/3xp;-><init>()V

    sput-object v0, LX/3xj;->a:LX/3xn;

    goto :goto_0

    .line 659876
    :cond_1
    new-instance v0, LX/3xo;

    invoke-direct {v0}, LX/3xo;-><init>()V

    sput-object v0, LX/3xj;->a:LX/3xn;

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 659877
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 659878
    const/4 v0, -0x1

    iput v0, p0, LX/3xj;->d:I

    return-void
.end method

.method public static a(II)I
    .locals 5

    .prologue
    const v4, 0xc0c0c

    .line 659879
    and-int v0, p0, v4

    .line 659880
    if-nez v0, :cond_0

    .line 659881
    :goto_0
    return p0

    .line 659882
    :cond_0
    xor-int/lit8 v1, v0, -0x1

    and-int/2addr v1, p0

    .line 659883
    if-nez p1, :cond_1

    .line 659884
    shl-int/lit8 v0, v0, 0x2

    or-int p0, v1, v0

    .line 659885
    goto :goto_0

    .line 659886
    :cond_1
    shl-int/lit8 v2, v0, 0x1

    const v3, -0xc0c0d

    and-int/2addr v2, v3

    or-int/2addr v1, v2

    .line 659887
    shl-int/lit8 v0, v0, 0x1

    and-int/2addr v0, v4

    shl-int/lit8 v0, v0, 0x2

    or-int p0, v1, v0

    .line 659888
    goto :goto_0
.end method

.method public static a(Landroid/support/v7/widget/RecyclerView;I)J
    .locals 4

    .prologue
    const/16 v1, 0x8

    .line 659889
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->d:LX/1Of;

    move-object v0, v0

    .line 659890
    if-nez v0, :cond_1

    .line 659891
    if-ne p1, v1, :cond_0

    const-wide/16 v0, 0xc8

    .line 659892
    :goto_0
    return-wide v0

    .line 659893
    :cond_0
    const-wide/16 v0, 0xfa

    goto :goto_0

    .line 659894
    :cond_1
    if-ne p1, v1, :cond_2

    .line 659895
    iget-wide v2, v0, LX/1Of;->e:J

    move-wide v0, v2

    .line 659896
    goto :goto_0

    .line 659897
    :cond_2
    iget-wide v2, v0, LX/1Of;->d:J

    move-wide v0, v2

    .line 659898
    goto :goto_0
.end method

.method private static a(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;LX/1a1;FFIZ)V
    .locals 8

    .prologue
    .line 659899
    sget-object v0, LX/3xj;->a:LX/3xn;

    iget-object v3, p2, LX/1a1;->a:Landroid/view/View;

    move-object v1, p0

    move-object v2, p1

    move v4, p3

    move v5, p4

    move v6, p5

    move v7, p6

    invoke-interface/range {v0 .. v7}, LX/3xn;->a(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;Landroid/view/View;FFIZ)V

    .line 659900
    return-void
.end method

.method public static a(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;LX/1a1;Ljava/util/List;IFF)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Canvas;",
            "Landroid/support/v7/widget/RecyclerView;",
            "LX/1a1;",
            "Ljava/util/List",
            "<",
            "LX/3xe;",
            ">;IFF)V"
        }
    .end annotation

    .prologue
    .line 659937
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v8

    .line 659938
    const/4 v0, 0x0

    move v7, v0

    :goto_0
    if-ge v7, v8, :cond_0

    .line 659939
    invoke-interface {p3, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3xe;

    .line 659940
    invoke-virtual {v0}, LX/3xe;->d()V

    .line 659941
    invoke-virtual {p0}, Landroid/graphics/Canvas;->save()I

    move-result v9

    .line 659942
    iget-object v2, v0, LX/3xe;->h:LX/1a1;

    iget v3, v0, LX/3xe;->k:F

    iget v4, v0, LX/3xe;->l:F

    iget v5, v0, LX/3xe;->i:I

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-static/range {v0 .. v6}, LX/3xj;->a(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;LX/1a1;FFIZ)V

    .line 659943
    invoke-virtual {p0, v9}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 659944
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_0

    .line 659945
    :cond_0
    if-eqz p2, :cond_1

    .line 659946
    invoke-virtual {p0}, Landroid/graphics/Canvas;->save()I

    move-result v7

    .line 659947
    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move/from16 v3, p5

    move/from16 v4, p6

    move v5, p4

    invoke-static/range {v0 .. v6}, LX/3xj;->a(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;LX/1a1;FFIZ)V

    .line 659948
    invoke-virtual {p0, v7}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 659949
    :cond_1
    return-void
.end method

.method public static b(II)I
    .locals 2

    .prologue
    .line 659901
    const/4 v0, 0x0

    or-int v1, p1, p0

    invoke-static {v0, v1}, LX/3xj;->d(II)I

    move-result v0

    const/4 v1, 0x1

    invoke-static {v1, p1}, LX/3xj;->d(II)I

    move-result v1

    or-int/2addr v0, v1

    const/4 v1, 0x2

    invoke-static {v1, p0}, LX/3xj;->d(II)I

    move-result v1

    or-int/2addr v0, v1

    return v0
.end method

.method private static b(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;LX/1a1;FFIZ)V
    .locals 7

    .prologue
    .line 659902
    sget-object v0, LX/3xj;->a:LX/3xn;

    iget-object v3, p2, LX/1a1;->a:Landroid/view/View;

    move-object v1, p0

    move-object v2, p1

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-interface/range {v0 .. v6}, LX/3xn;->a(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;Landroid/view/View;FFI)V

    .line 659903
    return-void
.end method

.method public static b(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;LX/1a1;Ljava/util/List;IFF)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Canvas;",
            "Landroid/support/v7/widget/RecyclerView;",
            "LX/1a1;",
            "Ljava/util/List",
            "<",
            "LX/3xe;",
            ">;IFF)V"
        }
    .end annotation

    .prologue
    .line 659904
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v8

    .line 659905
    const/4 v0, 0x0

    move v7, v0

    :goto_0
    if-ge v7, v8, :cond_0

    .line 659906
    invoke-interface {p3, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3xe;

    .line 659907
    invoke-virtual {p0}, Landroid/graphics/Canvas;->save()I

    move-result v9

    .line 659908
    iget-object v2, v0, LX/3xe;->h:LX/1a1;

    iget v3, v0, LX/3xe;->k:F

    iget v4, v0, LX/3xe;->l:F

    iget v5, v0, LX/3xe;->i:I

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-static/range {v0 .. v6}, LX/3xj;->b(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;LX/1a1;FFIZ)V

    .line 659909
    invoke-virtual {p0, v9}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 659910
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_0

    .line 659911
    :cond_0
    if-eqz p2, :cond_1

    .line 659912
    invoke-virtual {p0}, Landroid/graphics/Canvas;->save()I

    move-result v7

    .line 659913
    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move/from16 v3, p5

    move/from16 v4, p6

    move v5, p4

    invoke-static/range {v0 .. v6}, LX/3xj;->b(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;LX/1a1;FFIZ)V

    .line 659914
    invoke-virtual {p0, v7}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 659915
    :cond_1
    const/4 v1, 0x0

    .line 659916
    add-int/lit8 v0, v8, -0x1

    move v2, v0

    :goto_1
    if-ltz v2, :cond_3

    .line 659917
    invoke-interface {p3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3xe;

    .line 659918
    iget-boolean v3, v0, LX/3xe;->c:Z

    if-eqz v3, :cond_2

    iget-boolean v3, v0, LX/3xe;->j:Z

    if-nez v3, :cond_2

    .line 659919
    invoke-interface {p3, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 659920
    iget-object v0, v0, LX/3xe;->h:LX/1a1;

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/1a1;->a(Z)V

    move v0, v1

    .line 659921
    :goto_2
    add-int/lit8 v1, v2, -0x1

    move v2, v1

    move v1, v0

    goto :goto_1

    .line 659922
    :cond_2
    iget-boolean v0, v0, LX/3xe;->c:Z

    if-nez v0, :cond_5

    .line 659923
    const/4 v0, 0x1

    goto :goto_2

    .line 659924
    :cond_3
    if-eqz v1, :cond_4

    .line 659925
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->invalidate()V

    .line 659926
    :cond_4
    return-void

    :cond_5
    move v0, v1

    goto :goto_2
.end method

.method public static c(II)I
    .locals 5

    .prologue
    const v4, 0x303030

    .line 659927
    and-int v0, p0, v4

    .line 659928
    if-nez v0, :cond_0

    .line 659929
    :goto_0
    return p0

    .line 659930
    :cond_0
    xor-int/lit8 v1, v0, -0x1

    and-int/2addr v1, p0

    .line 659931
    if-nez p1, :cond_1

    .line 659932
    shr-int/lit8 v0, v0, 0x2

    or-int p0, v1, v0

    .line 659933
    goto :goto_0

    .line 659934
    :cond_1
    shr-int/lit8 v2, v0, 0x1

    const v3, -0x303031

    and-int/2addr v2, v3

    or-int/2addr v1, v2

    .line 659935
    shr-int/lit8 v0, v0, 0x1

    and-int/2addr v0, v4

    shr-int/lit8 v0, v0, 0x2

    or-int p0, v1, v0

    .line 659936
    goto :goto_0
.end method

.method public static c(LX/3xj;Landroid/support/v7/widget/RecyclerView;LX/1a1;)Z
    .locals 2

    .prologue
    .line 659866
    invoke-virtual {p0, p1, p2}, LX/3xj;->a(Landroid/support/v7/widget/RecyclerView;LX/1a1;)I

    move-result v0

    .line 659867
    const/high16 v1, 0xff0000

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static d(II)I
    .locals 1

    .prologue
    .line 659868
    mul-int/lit8 v0, p0, 0x8

    shl-int v0, p1, v0

    return v0
.end method


# virtual methods
.method public abstract a(LX/1a1;)I
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView;IIJ)I
    .locals 6

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    .line 659852
    iget v1, p0, LX/3xj;->d:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 659853
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00b3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, LX/3xj;->d:I

    .line 659854
    :cond_0
    iget v1, p0, LX/3xj;->d:I

    move v1, v1

    .line 659855
    invoke-static {p3}, Ljava/lang/Math;->abs(I)I

    move-result v2

    .line 659856
    int-to-float v3, p3

    invoke-static {v3}, Ljava/lang/Math;->signum(F)F

    move-result v3

    float-to-int v3, v3

    .line 659857
    int-to-float v2, v2

    mul-float/2addr v2, v0

    int-to-float v4, p2

    div-float/2addr v2, v4

    invoke-static {v0, v2}, Ljava/lang/Math;->min(FF)F

    move-result v2

    .line 659858
    mul-int/2addr v1, v3

    int-to-float v1, v1

    sget-object v3, LX/3xj;->c:Landroid/view/animation/Interpolator;

    invoke-interface {v3, v2}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    .line 659859
    const-wide/16 v2, 0x7d0

    cmp-long v2, p4, v2

    if-lez v2, :cond_2

    .line 659860
    :goto_0
    int-to-float v1, v1

    sget-object v2, LX/3xj;->b:Landroid/view/animation/Interpolator;

    invoke-interface {v2, v0}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v0

    mul-float/2addr v0, v1

    float-to-int v0, v0

    .line 659861
    if-nez v0, :cond_1

    .line 659862
    if-lez p3, :cond_3

    const/4 v0, 0x1

    .line 659863
    :cond_1
    :goto_1
    return v0

    .line 659864
    :cond_2
    long-to-float v0, p4

    const/high16 v2, 0x44fa0000    # 2000.0f

    div-float/2addr v0, v2

    goto :goto_0

    .line 659865
    :cond_3
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView;LX/1a1;)I
    .locals 2

    .prologue
    .line 659850
    invoke-virtual {p0, p2}, LX/3xj;->a(LX/1a1;)I

    move-result v0

    .line 659851
    invoke-static {p1}, LX/0vv;->h(Landroid/view/View;)I

    move-result v1

    invoke-static {v0, v1}, LX/3xj;->c(II)I

    move-result v0

    return v0
.end method

.method public a(LX/1a1;Ljava/util/List;II)LX/1a1;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1a1;",
            "Ljava/util/List",
            "<",
            "LX/1a1;",
            ">;II)",
            "LX/1a1;"
        }
    .end annotation

    .prologue
    .line 659819
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    add-int v5, p3, v0

    .line 659820
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    add-int v6, p4, v0

    .line 659821
    const/4 v2, 0x0

    .line 659822
    const/4 v1, -0x1

    .line 659823
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    sub-int v7, p3, v0

    .line 659824
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    sub-int v8, p4, v0

    .line 659825
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v9

    .line 659826
    const/4 v0, 0x0

    move v4, v0

    :goto_0
    if-ge v4, v9, :cond_0

    .line 659827
    invoke-interface {p2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1a1;

    .line 659828
    if-lez v7, :cond_1

    .line 659829
    iget-object v3, v0, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getRight()I

    move-result v3

    sub-int/2addr v3, v5

    .line 659830
    if-gez v3, :cond_1

    iget-object v10, v0, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v10}, Landroid/view/View;->getRight()I

    move-result v10

    iget-object v11, p1, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v11}, Landroid/view/View;->getRight()I

    move-result v11

    if-le v10, v11, :cond_1

    .line 659831
    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    .line 659832
    if-le v3, v1, :cond_1

    move v2, v3

    move-object v3, v0

    .line 659833
    :goto_1
    if-gez v7, :cond_4

    .line 659834
    iget-object v1, v0, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v1

    sub-int v1, v1, p3

    .line 659835
    if-lez v1, :cond_4

    iget-object v10, v0, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v10}, Landroid/view/View;->getLeft()I

    move-result v10

    iget-object v11, p1, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v11}, Landroid/view/View;->getLeft()I

    move-result v11

    if-ge v10, v11, :cond_4

    .line 659836
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 659837
    if-le v1, v2, :cond_4

    move-object v3, v0

    .line 659838
    :goto_2
    if-gez v8, :cond_3

    .line 659839
    iget-object v2, v0, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v2

    sub-int v2, v2, p4

    .line 659840
    if-lez v2, :cond_3

    iget-object v10, v0, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v10}, Landroid/view/View;->getTop()I

    move-result v10

    iget-object v11, p1, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v11}, Landroid/view/View;->getTop()I

    move-result v11

    if-ge v10, v11, :cond_3

    .line 659841
    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    .line 659842
    if-le v2, v1, :cond_3

    move-object v3, v0

    .line 659843
    :goto_3
    if-lez v8, :cond_2

    .line 659844
    iget-object v1, v0, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v1

    sub-int/2addr v1, v6

    .line 659845
    if-gez v1, :cond_2

    iget-object v10, v0, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v10}, Landroid/view/View;->getBottom()I

    move-result v10

    iget-object v11, p1, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v11}, Landroid/view/View;->getBottom()I

    move-result v11

    if-le v10, v11, :cond_2

    .line 659846
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 659847
    if-le v1, v2, :cond_2

    move v12, v1

    move-object v1, v0

    move v0, v12

    .line 659848
    :goto_4
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move-object v2, v1

    move v1, v0

    goto/16 :goto_0

    .line 659849
    :cond_0
    return-object v2

    :cond_1
    move-object v3, v2

    move v2, v1

    goto :goto_1

    :cond_2
    move v0, v2

    move-object v1, v3

    goto :goto_4

    :cond_3
    move v2, v1

    goto :goto_3

    :cond_4
    move v1, v2

    goto :goto_2
.end method

.method public a(LX/1a1;I)V
    .locals 2

    .prologue
    .line 659816
    if-eqz p1, :cond_0

    .line 659817
    sget-object v0, LX/3xj;->a:LX/3xn;

    iget-object v1, p1, LX/1a1;->a:Landroid/view/View;

    invoke-interface {v0, v1}, LX/3xn;->b(Landroid/view/View;)V

    .line 659818
    :cond_0
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 659815
    const/4 v0, 0x1

    return v0
.end method

.method public abstract a(LX/1a1;LX/1a1;)Z
.end method

.method public b(Landroid/support/v7/widget/RecyclerView;LX/1a1;)V
    .locals 2

    .prologue
    .line 659813
    sget-object v0, LX/3xj;->a:LX/3xn;

    iget-object v1, p2, LX/1a1;->a:Landroid/view/View;

    invoke-interface {v0, v1}, LX/3xn;->a(Landroid/view/View;)V

    .line 659814
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 659812
    const/4 v0, 0x1

    return v0
.end method

.method public b(LX/1a1;)Z
    .locals 1

    .prologue
    .line 659811
    const/4 v0, 0x1

    return v0
.end method

.method public abstract f()V
.end method
