.class public LX/4oD;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""


# instance fields
.field private a:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 809048
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 809049
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 809050
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 809051
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 809052
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 809053
    return-void
.end method


# virtual methods
.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 809054
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 809055
    iget-object v0, p0, LX/4oD;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 809056
    iget-object v0, p0, LX/4oD;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, LX/4oD;->getWidth()I

    move-result v1

    invoke-virtual {p0}, LX/4oD;->getHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 809057
    iget-object v0, p0, LX/4oD;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 809058
    :cond_0
    return-void
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 809059
    iget-object v0, p0, LX/4oD;->a:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x1fecfaad

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 809060
    iget-object v0, p0, LX/4oD;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 809061
    const/4 v0, 0x0

    const v2, 0x7c3f5ec3

    invoke-static {v3, v3, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 809062
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    const v2, 0x183f8e26

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto :goto_0
.end method

.method public setOverlay(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 809063
    iput-object p1, p0, LX/4oD;->a:Landroid/graphics/drawable/Drawable;

    .line 809064
    return-void
.end method
