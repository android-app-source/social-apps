.class public final enum LX/3UG;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/3UG;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/3UG;

.field public static final enum FAILURE:LX/3UG;

.field public static final enum LOADING:LX/3UG;

.field public static final enum SUCCESS:LX/3UG;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 585712
    new-instance v0, LX/3UG;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v2}, LX/3UG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3UG;->SUCCESS:LX/3UG;

    .line 585713
    new-instance v0, LX/3UG;

    const-string v1, "FAILURE"

    invoke-direct {v0, v1, v3}, LX/3UG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3UG;->FAILURE:LX/3UG;

    .line 585714
    new-instance v0, LX/3UG;

    const-string v1, "LOADING"

    invoke-direct {v0, v1, v4}, LX/3UG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3UG;->LOADING:LX/3UG;

    .line 585715
    const/4 v0, 0x3

    new-array v0, v0, [LX/3UG;

    sget-object v1, LX/3UG;->SUCCESS:LX/3UG;

    aput-object v1, v0, v2

    sget-object v1, LX/3UG;->FAILURE:LX/3UG;

    aput-object v1, v0, v3

    sget-object v1, LX/3UG;->LOADING:LX/3UG;

    aput-object v1, v0, v4

    sput-object v0, LX/3UG;->$VALUES:[LX/3UG;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 585711
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/3UG;
    .locals 1

    .prologue
    .line 585709
    const-class v0, LX/3UG;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/3UG;

    return-object v0
.end method

.method public static values()[LX/3UG;
    .locals 1

    .prologue
    .line 585710
    sget-object v0, LX/3UG;->$VALUES:[LX/3UG;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/3UG;

    return-object v0
.end method
