.class public LX/3ui;
.super Landroid/view/MenuInflater;
.source ""


# static fields
.field public static final a:[Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static final b:[Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final c:[Ljava/lang/Object;

.field public final d:[Ljava/lang/Object;

.field public e:Landroid/content/Context;

.field public f:Ljava/lang/Object;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 649192
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Class;

    const/4 v1, 0x0

    const-class v2, Landroid/content/Context;

    aput-object v2, v0, v1

    .line 649193
    sput-object v0, LX/3ui;->a:[Ljava/lang/Class;

    sput-object v0, LX/3ui;->b:[Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 649194
    invoke-direct {p0, p1}, Landroid/view/MenuInflater;-><init>(Landroid/content/Context;)V

    .line 649195
    iput-object p1, p0, LX/3ui;->e:Landroid/content/Context;

    .line 649196
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    iput-object v0, p0, LX/3ui;->c:[Ljava/lang/Object;

    .line 649197
    iget-object v0, p0, LX/3ui;->c:[Ljava/lang/Object;

    iput-object v0, p0, LX/3ui;->d:[Ljava/lang/Object;

    .line 649198
    return-void
.end method

.method public static a(LX/3ui;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 649199
    instance-of v0, p1, Landroid/app/Activity;

    if-eqz v0, :cond_1

    .line 649200
    :cond_0
    :goto_0
    return-object p1

    .line 649201
    :cond_1
    instance-of v0, p1, Landroid/content/ContextWrapper;

    if-eqz v0, :cond_0

    .line 649202
    check-cast p1, Landroid/content/ContextWrapper;

    invoke-virtual {p1}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p0, v0}, LX/3ui;->a(LX/3ui;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    goto :goto_0
.end method

.method private a(Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/view/Menu;)V
    .locals 12

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x1

    const/4 v6, 0x0

    .line 649203
    new-instance v7, LX/3uh;

    invoke-direct {v7, p0, p3}, LX/3uh;-><init>(LX/3ui;Landroid/view/Menu;)V

    .line 649204
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v0

    .line 649205
    :cond_0
    const/4 v2, 0x2

    if-ne v0, v2, :cond_3

    .line 649206
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 649207
    const-string v2, "menu"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 649208
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    :goto_0
    move-object v2, v4

    move v5, v6

    move v3, v0

    move v0, v6

    .line 649209
    :goto_1
    if-nez v0, :cond_d

    .line 649210
    packed-switch v3, :pswitch_data_0

    :cond_1
    move v3, v5

    .line 649211
    :goto_2
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v5

    move v9, v3

    move v3, v5

    move v5, v9

    goto :goto_1

    .line 649212
    :cond_2
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Expecting menu, got "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 649213
    :cond_3
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    .line 649214
    if-ne v0, v1, :cond_0

    goto :goto_0

    .line 649215
    :pswitch_0
    if-nez v5, :cond_1

    .line 649216
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v3

    .line 649217
    const-string v8, "group"

    invoke-virtual {v3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 649218
    const/4 p3, 0x1

    const/4 v9, 0x0

    .line 649219
    iget-object v3, v7, LX/3uh;->a:LX/3ui;

    iget-object v3, v3, LX/3ui;->e:Landroid/content/Context;

    sget-object v8, LX/03r;->MenuGroup:[I

    invoke-virtual {v3, p2, v8}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v3

    .line 649220
    const/16 v8, 0x1

    invoke-virtual {v3, v8, v9}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v8

    iput v8, v7, LX/3uh;->c:I

    .line 649221
    const/16 v8, 0x3

    invoke-virtual {v3, v8, v9}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v8

    iput v8, v7, LX/3uh;->d:I

    .line 649222
    const/16 v8, 0x4

    invoke-virtual {v3, v8, v9}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v8

    iput v8, v7, LX/3uh;->e:I

    .line 649223
    const/16 v8, 0x5

    invoke-virtual {v3, v8, v9}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v8

    iput v8, v7, LX/3uh;->f:I

    .line 649224
    const/16 v8, 0x2

    invoke-virtual {v3, v8, p3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v8

    iput-boolean v8, v7, LX/3uh;->g:Z

    .line 649225
    const/16 v8, 0x0

    invoke-virtual {v3, v8, p3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v8

    iput-boolean v8, v7, LX/3uh;->h:Z

    .line 649226
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    .line 649227
    move v3, v5

    goto :goto_2

    .line 649228
    :cond_4
    const-string v8, "item"

    invoke-virtual {v3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 649229
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 649230
    iget-object v3, v7, LX/3uh;->a:LX/3ui;

    iget-object v3, v3, LX/3ui;->e:Landroid/content/Context;

    sget-object v10, LX/03r;->MenuItem:[I

    invoke-virtual {v3, p2, v10}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v10

    .line 649231
    const/16 v3, 0x2

    invoke-virtual {v10, v3, v9}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    iput v3, v7, LX/3uh;->j:I

    .line 649232
    const/16 v3, 0x5

    iget v11, v7, LX/3uh;->d:I

    invoke-virtual {v10, v3, v11}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v3

    .line 649233
    const/16 v11, 0x6

    iget p3, v7, LX/3uh;->e:I

    invoke-virtual {v10, v11, p3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v11

    .line 649234
    const/high16 p3, -0x10000

    and-int/2addr v3, p3

    const p3, 0xffff

    and-int/2addr v11, p3

    or-int/2addr v3, v11

    iput v3, v7, LX/3uh;->k:I

    .line 649235
    const/16 v3, 0x7

    invoke-virtual {v10, v3, v9}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    .line 649236
    if-eqz v3, :cond_5

    .line 649237
    iget-object v11, v7, LX/3uh;->a:LX/3ui;

    iget-object v11, v11, LX/3ui;->e:Landroid/content/Context;

    invoke-virtual {v11, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v7, LX/3uh;->l:Ljava/lang/CharSequence;

    .line 649238
    :cond_5
    const/16 v3, 0x8

    invoke-virtual {v10, v3, v9}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    .line 649239
    if-eqz v3, :cond_6

    .line 649240
    iget-object v11, v7, LX/3uh;->a:LX/3ui;

    iget-object v11, v11, LX/3ui;->e:Landroid/content/Context;

    invoke-virtual {v11, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v7, LX/3uh;->m:Ljava/lang/CharSequence;

    .line 649241
    :cond_6
    const/16 v3, 0x0

    invoke-virtual {v10, v3, v9}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    iput v3, v7, LX/3uh;->n:I

    .line 649242
    const/16 v3, 0x9

    invoke-virtual {v10, v3}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/3uh;->a(Ljava/lang/String;)C

    move-result v3

    iput-char v3, v7, LX/3uh;->o:C

    .line 649243
    const/16 v3, 0xa

    invoke-virtual {v10, v3}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/3uh;->a(Ljava/lang/String;)C

    move-result v3

    iput-char v3, v7, LX/3uh;->p:C

    .line 649244
    const/16 v3, 0xb

    invoke-virtual {v10, v3}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v3

    if-eqz v3, :cond_f

    .line 649245
    const/16 v3, 0xb

    invoke-virtual {v10, v3, v9}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v3

    if-eqz v3, :cond_e

    move v3, v8

    :goto_3
    iput v3, v7, LX/3uh;->q:I

    .line 649246
    :goto_4
    const/16 v3, 0x3

    invoke-virtual {v10, v3, v9}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v3

    iput-boolean v3, v7, LX/3uh;->r:Z

    .line 649247
    const/16 v3, 0x4

    iget-boolean v11, v7, LX/3uh;->g:Z

    invoke-virtual {v10, v3, v11}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v3

    iput-boolean v3, v7, LX/3uh;->s:Z

    .line 649248
    const/16 v3, 0x1

    iget-boolean v11, v7, LX/3uh;->h:Z

    invoke-virtual {v10, v3, v11}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v3

    iput-boolean v3, v7, LX/3uh;->t:Z

    .line 649249
    const/16 v3, 0xd

    const/4 v11, -0x1

    invoke-virtual {v10, v3, v11}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v3

    iput v3, v7, LX/3uh;->u:I

    .line 649250
    const/16 v3, 0xc

    invoke-virtual {v10, v3}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v7, LX/3uh;->y:Ljava/lang/String;

    .line 649251
    const/16 v3, 0xe

    invoke-virtual {v10, v3, v9}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    iput v3, v7, LX/3uh;->v:I

    .line 649252
    const/16 v3, 0xf

    invoke-virtual {v10, v3}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v7, LX/3uh;->w:Ljava/lang/String;

    .line 649253
    const/16 v3, 0x10

    invoke-virtual {v10, v3}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v7, LX/3uh;->x:Ljava/lang/String;

    .line 649254
    iget-object v3, v7, LX/3uh;->x:Ljava/lang/String;

    if-eqz v3, :cond_10

    .line 649255
    :goto_5
    if-eqz v8, :cond_11

    iget v3, v7, LX/3uh;->v:I

    if-nez v3, :cond_11

    iget-object v3, v7, LX/3uh;->w:Ljava/lang/String;

    if-nez v3, :cond_11

    .line 649256
    iget-object v3, v7, LX/3uh;->x:Ljava/lang/String;

    sget-object v8, LX/3ui;->b:[Ljava/lang/Class;

    iget-object v11, v7, LX/3uh;->a:LX/3ui;

    iget-object v11, v11, LX/3ui;->d:[Ljava/lang/Object;

    invoke-static {v7, v3, v8, v11}, LX/3uh;->a(LX/3uh;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/3rR;

    iput-object v3, v7, LX/3uh;->z:LX/3rR;

    .line 649257
    :goto_6
    invoke-virtual {v10}, Landroid/content/res/TypedArray;->recycle()V

    .line 649258
    iput-boolean v9, v7, LX/3uh;->i:Z

    .line 649259
    move v3, v5

    goto/16 :goto_2

    .line 649260
    :cond_7
    const-string v8, "menu"

    invoke-virtual {v3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 649261
    invoke-virtual {v7}, LX/3uh;->c()Landroid/view/SubMenu;

    move-result-object v3

    .line 649262
    invoke-direct {p0, p1, p2, v3}, LX/3ui;->a(Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/view/Menu;)V

    move v3, v5

    .line 649263
    goto/16 :goto_2

    :cond_8
    move-object v2, v3

    move v3, v1

    .line 649264
    goto/16 :goto_2

    .line 649265
    :pswitch_1
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v3

    .line 649266
    if-eqz v5, :cond_9

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_9

    move-object v2, v4

    move v3, v6

    .line 649267
    goto/16 :goto_2

    .line 649268
    :cond_9
    const-string v8, "group"

    invoke-virtual {v3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_a

    .line 649269
    invoke-virtual {v7}, LX/3uh;->a()V

    move v3, v5

    goto/16 :goto_2

    .line 649270
    :cond_a
    const-string v8, "item"

    invoke-virtual {v3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_c

    .line 649271
    iget-boolean v3, v7, LX/3uh;->i:Z

    move v3, v3

    .line 649272
    if-nez v3, :cond_1

    .line 649273
    iget-object v3, v7, LX/3uh;->z:LX/3rR;

    if-eqz v3, :cond_b

    iget-object v3, v7, LX/3uh;->z:LX/3rR;

    invoke-virtual {v3}, LX/3rR;->e()Z

    move-result v3

    if-eqz v3, :cond_b

    .line 649274
    invoke-virtual {v7}, LX/3uh;->c()Landroid/view/SubMenu;

    move v3, v5

    goto/16 :goto_2

    .line 649275
    :cond_b
    const/4 v3, 0x1

    iput-boolean v3, v7, LX/3uh;->i:Z

    .line 649276
    iget-object v3, v7, LX/3uh;->b:Landroid/view/Menu;

    iget v8, v7, LX/3uh;->c:I

    iget v9, v7, LX/3uh;->j:I

    iget v10, v7, LX/3uh;->k:I

    iget-object v11, v7, LX/3uh;->l:Ljava/lang/CharSequence;

    invoke-interface {v3, v8, v9, v10, v11}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v3

    invoke-static {v7, v3}, LX/3uh;->a(LX/3uh;Landroid/view/MenuItem;)V

    .line 649277
    move v3, v5

    goto/16 :goto_2

    .line 649278
    :cond_c
    const-string v8, "menu"

    invoke-virtual {v3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    move v0, v1

    move v3, v5

    .line 649279
    goto/16 :goto_2

    .line 649280
    :pswitch_2
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unexpected end of document"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 649281
    :cond_d
    return-void

    :cond_e
    move v3, v9

    .line 649282
    goto/16 :goto_3

    .line 649283
    :cond_f
    iget v3, v7, LX/3uh;->f:I

    iput v3, v7, LX/3uh;->q:I

    goto/16 :goto_4

    :cond_10
    move v8, v9

    .line 649284
    goto/16 :goto_5

    .line 649285
    :cond_11
    if-eqz v8, :cond_12

    .line 649286
    const-string v3, "SupportMenuInflater"

    const-string v8, "Ignoring attribute \'actionProviderClass\'. Action view already specified."

    invoke-static {v3, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 649287
    :cond_12
    const/4 v3, 0x0

    iput-object v3, v7, LX/3uh;->z:LX/3rR;

    goto/16 :goto_6

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final inflate(ILandroid/view/Menu;)V
    .locals 4

    .prologue
    .line 649288
    const/4 v1, 0x0

    .line 649289
    :try_start_0
    iget-object v0, p0, LX/3ui;->e:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getLayout(I)Landroid/content/res/XmlResourceParser;

    move-result-object v1

    .line 649290
    invoke-static {v1}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    move-result-object v0

    .line 649291
    invoke-direct {p0, v1, v0, p2}, LX/3ui;->a(Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/view/Menu;)V
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 649292
    if-eqz v1, :cond_0

    invoke-interface {v1}, Landroid/content/res/XmlResourceParser;->close()V

    .line 649293
    :cond_0
    return-void

    .line 649294
    :catch_0
    move-exception v0

    .line 649295
    :try_start_1
    new-instance v2, Landroid/view/InflateException;

    const-string v3, "Error inflating menu XML"

    invoke-direct {v2, v3, v0}, Landroid/view/InflateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 649296
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/content/res/XmlResourceParser;->close()V

    :cond_1
    throw v0

    .line 649297
    :catch_1
    move-exception v0

    .line 649298
    new-instance v2, Landroid/view/InflateException;

    const-string v3, "Error inflating menu XML"

    invoke-direct {v2, v3, v0}, Landroid/view/InflateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method
