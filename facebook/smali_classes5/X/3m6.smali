.class public LX/3m6;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 635350
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/3m6;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 635351
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 635356
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/3m6;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 635357
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 635352
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 635353
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p2

    const p3, 0x7f0315e5

    invoke-virtual {p2, p3, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 635354
    const/4 p2, 0x0

    invoke-virtual {p0, p2}, LX/3m6;->setOrientation(I)V

    .line 635355
    return-void
.end method
