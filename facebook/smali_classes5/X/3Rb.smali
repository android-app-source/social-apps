.class public LX/3Rb;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final c:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile h:LX/3Rb;


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0WJ;",
            ">;"
        }
    .end annotation
.end field

.field public volatile b:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0s9;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2Oi;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/03V;

.field private final f:Z

.field private final g:LX/3Rc;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 580936
    const-class v0, LX/3Rb;

    sput-object v0, LX/3Rb;->c:Ljava/lang/Class;

    return-void
.end method

.method private constructor <init>(LX/0Or;LX/03V;Ljava/lang/Boolean;LX/3Rc;)V
    .locals 1
    .param p3    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/2Oi;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Ljava/lang/Boolean;",
            "LX/3Rc;",
            ")V"
        }
    .end annotation

    .prologue
    .line 580827
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 580828
    iput-object p1, p0, LX/3Rb;->d:LX/0Or;

    .line 580829
    iput-object p2, p0, LX/3Rb;->e:LX/03V;

    .line 580830
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/3Rb;->f:Z

    .line 580831
    iput-object p4, p0, LX/3Rb;->g:LX/3Rc;

    .line 580832
    return-void
.end method

.method private static a(I)LX/1bf;
    .locals 2

    .prologue
    .line 580931
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "res:///"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const v1, 0x7f02184a

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/8t9;I)LX/1bf;
    .locals 2

    .prologue
    .line 580926
    if-eqz p0, :cond_0

    .line 580927
    iget-object v0, p0, LX/8t9;->b:LX/8t8;

    move-object v0, v0

    .line 580928
    sget-object v1, LX/8t8;->ADDRESS_BOOK_CONTACT:LX/8t8;

    if-ne v0, v1, :cond_0

    .line 580929
    invoke-static {p1}, LX/3Rb;->a(I)LX/1bf;

    move-result-object v0

    .line 580930
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/facebook/user/model/PicSquare;IILcom/facebook/user/model/UserKey;)LX/1bf;
    .locals 7

    .prologue
    .line 580910
    invoke-static {p1, p3}, LX/3Rb;->a(Lcom/facebook/user/model/PicSquare;I)Lcom/facebook/user/model/PicSquareUrlWithSize;

    move-result-object v0

    .line 580911
    if-eqz p4, :cond_0

    invoke-virtual {p4}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    .line 580912
    :cond_0
    iget-object v1, v0, Lcom/facebook/user/model/PicSquareUrlWithSize;->url:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 580913
    invoke-virtual {v1}, Landroid/net/Uri;->isAbsolute()Z

    move-result v2

    if-nez v2, :cond_3

    .line 580914
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 580915
    const-string v1, "Invalid PicSquareUri:"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, v0, Lcom/facebook/user/model/PicSquareUrlWithSize;->url:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 580916
    if-eqz p4, :cond_1

    .line 580917
    const-string v0, "|user="

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p4}, Lcom/facebook/user/model/UserKey;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 580918
    :cond_1
    const-string v0, "|tw="

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 580919
    const-string v0, "|th="

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 580920
    invoke-virtual {p1}, Lcom/facebook/user/model/PicSquare;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_2

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/PicSquareUrlWithSize;

    .line 580921
    const-string v5, "|url_"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v0, Lcom/facebook/user/model/PicSquareUrlWithSize;->size:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ":"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v0, v0, Lcom/facebook/user/model/PicSquareUrlWithSize;->url:Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 580922
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 580923
    :cond_2
    iget-object v0, p0, LX/3Rb;->e:LX/03V;

    sget-object v1, LX/3Rb;->c:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 580924
    const/4 v0, 0x0

    .line 580925
    :goto_1
    return-object v0

    :cond_3
    invoke-static {v1}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v0

    goto :goto_1
.end method

.method private a(Lcom/facebook/user/model/User;Lcom/facebook/user/model/UserKey;II)LX/1bf;
    .locals 3
    .param p1    # Lcom/facebook/user/model/User;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 580897
    if-eqz p1, :cond_4

    .line 580898
    invoke-virtual {p1}, Lcom/facebook/user/model/User;->w()Lcom/facebook/user/model/PicSquare;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 580899
    invoke-virtual {p1}, Lcom/facebook/user/model/User;->w()Lcom/facebook/user/model/PicSquare;

    move-result-object v0

    invoke-direct {p0, v0, p3, p4, p2}, LX/3Rb;->a(Lcom/facebook/user/model/PicSquare;IILcom/facebook/user/model/UserKey;)LX/1bf;

    move-result-object v0

    .line 580900
    :goto_0
    if-eqz v0, :cond_1

    .line 580901
    :cond_0
    :goto_1
    return-object v0

    .line 580902
    :cond_1
    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lcom/facebook/user/model/UserKey;->a()LX/0XG;

    move-result-object v0

    sget-object v2, LX/0XG;->FACEBOOK:LX/0XG;

    if-ne v0, v2, :cond_2

    invoke-virtual {p2}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v0

    const-string v2, "0"

    invoke-static {v0, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 580903
    invoke-virtual {p2}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p3, p4}, LX/3Rb;->a(Ljava/lang/String;II)Landroid/net/Uri;

    move-result-object v0

    .line 580904
    invoke-static {v0}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v0

    goto :goto_1

    .line 580905
    :cond_2
    if-eqz p2, :cond_3

    invoke-virtual {p2}, Lcom/facebook/user/model/UserKey;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 580906
    invoke-virtual {p2}, Lcom/facebook/user/model/UserKey;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/3Rb;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v0

    .line 580907
    if-nez v0, :cond_0

    .line 580908
    invoke-static {p3}, LX/3Rb;->a(I)LX/1bf;

    move-result-object v0

    goto :goto_1

    :cond_3
    move-object v0, v1

    .line 580909
    goto :goto_1

    :cond_4
    move-object v0, v1

    goto :goto_0
.end method

.method public static a(Lcom/facebook/user/model/UserKey;I)LX/1bf;
    .locals 1

    .prologue
    .line 580894
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/user/model/UserKey;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 580895
    invoke-static {p1}, LX/3Rb;->a(I)LX/1bf;

    move-result-object v0

    .line 580896
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/facebook/user/model/UserKey;II)LX/1bf;
    .locals 1

    .prologue
    .line 580892
    iget-object v0, p0, LX/3Rb;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Oi;

    invoke-virtual {v0, p1}, LX/2Oi;->a(Lcom/facebook/user/model/UserKey;)Lcom/facebook/user/model/User;

    move-result-object v0

    .line 580893
    invoke-direct {p0, v0, p1, p2, p3}, LX/3Rb;->a(Lcom/facebook/user/model/User;Lcom/facebook/user/model/UserKey;II)LX/1bf;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/facebook/user/model/UserKey;IILcom/facebook/user/model/PicSquare;)LX/1bf;
    .locals 1

    .prologue
    .line 580932
    iget-object v0, p0, LX/3Rb;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Oi;

    invoke-virtual {v0, p1}, LX/2Oi;->a(Lcom/facebook/user/model/UserKey;)Lcom/facebook/user/model/User;

    move-result-object v0

    .line 580933
    if-eqz v0, :cond_0

    .line 580934
    invoke-direct {p0, v0, p1, p2, p3}, LX/3Rb;->a(Lcom/facebook/user/model/User;Lcom/facebook/user/model/UserKey;II)LX/1bf;

    move-result-object v0

    .line 580935
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, p4, p2, p3, p1}, LX/3Rb;->a(Lcom/facebook/user/model/PicSquare;IILcom/facebook/user/model/UserKey;)LX/1bf;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;)LX/1bf;
    .locals 2

    .prologue
    .line 580888
    invoke-static {p0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 580889
    sget-object v0, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 580890
    invoke-static {v0}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v0

    .line 580891
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/3Rb;
    .locals 3

    .prologue
    .line 580878
    sget-object v0, LX/3Rb;->h:LX/3Rb;

    if-nez v0, :cond_1

    .line 580879
    const-class v1, LX/3Rb;

    monitor-enter v1

    .line 580880
    :try_start_0
    sget-object v0, LX/3Rb;->h:LX/3Rb;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 580881
    if-eqz v2, :cond_0

    .line 580882
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/3Rb;->b(LX/0QB;)LX/3Rb;

    move-result-object v0

    sput-object v0, LX/3Rb;->h:LX/3Rb;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 580883
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 580884
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 580885
    :cond_1
    sget-object v0, LX/3Rb;->h:LX/3Rb;

    return-object v0

    .line 580886
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 580887
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Lcom/facebook/user/model/PicSquare;I)Lcom/facebook/user/model/PicSquareUrlWithSize;
    .locals 4

    .prologue
    .line 580873
    invoke-virtual {p0, p1}, Lcom/facebook/user/model/PicSquare;->a(I)Lcom/facebook/user/model/PicSquareUrlWithSize;

    move-result-object v1

    .line 580874
    mul-int/lit8 v0, p1, 0x19

    div-int/lit8 v0, v0, 0x64

    sub-int v0, p1, v0

    .line 580875
    invoke-virtual {p0, v0}, Lcom/facebook/user/model/PicSquare;->a(I)Lcom/facebook/user/model/PicSquareUrlWithSize;

    move-result-object v0

    .line 580876
    iget v2, v0, Lcom/facebook/user/model/PicSquareUrlWithSize;->size:I

    sub-int v2, p1, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    iget v3, v1, Lcom/facebook/user/model/PicSquareUrlWithSize;->size:I

    sub-int v3, p1, v3

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 580877
    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method private static a(LX/3Rb;LX/0Or;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3Rb;",
            "LX/0Or",
            "<",
            "LX/0WJ;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0s9;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 580872
    iput-object p1, p0, LX/3Rb;->a:LX/0Or;

    iput-object p2, p0, LX/3Rb;->b:LX/0Or;

    return-void
.end method

.method private static b(LX/0QB;)LX/3Rb;
    .locals 5

    .prologue
    .line 580869
    new-instance v3, LX/3Rb;

    const/16 v0, 0x12c7

    invoke-static {p0, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-static {p0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-static {p0}, LX/3Rc;->a(LX/0QB;)LX/3Rc;

    move-result-object v2

    check-cast v2, LX/3Rc;

    invoke-direct {v3, v4, v0, v1, v2}, LX/3Rb;-><init>(LX/0Or;LX/03V;Ljava/lang/Boolean;LX/3Rc;)V

    .line 580870
    const/16 v0, 0x17d

    invoke-static {p0, v0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v0

    const/16 v1, 0xb5a

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-static {v3, v0, v1}, LX/3Rb;->a(LX/3Rb;LX/0Or;LX/0Or;)V

    .line 580871
    return-object v3
.end method


# virtual methods
.method public final a(LX/8t9;)I
    .locals 2
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation

    .prologue
    .line 580861
    iget-object v0, p1, LX/8t9;->e:LX/8ue;

    move-object v0, v0

    .line 580862
    sget-object v1, LX/8ue;->SMS:LX/8ue;

    if-ne v0, v1, :cond_0

    .line 580863
    iget-object v0, p0, LX/3Rb;->g:LX/3Rc;

    .line 580864
    iget v1, p1, LX/8t9;->h:I

    move v1, v1

    .line 580865
    invoke-virtual {v0, v1}, LX/3Rc;->a(I)I

    move-result v0

    .line 580866
    :goto_0
    return v0

    .line 580867
    :cond_0
    iget v0, p1, LX/8t9;->h:I

    move v0, v0

    .line 580868
    goto :goto_0
.end method

.method public final a(LX/8t9;II)LX/1bf;
    .locals 2

    .prologue
    .line 580842
    if-nez p1, :cond_1

    .line 580843
    const/4 v0, 0x0

    .line 580844
    :cond_0
    :goto_0
    return-object v0

    .line 580845
    :cond_1
    sget-object v0, LX/8t7;->a:[I

    .line 580846
    iget-object v1, p1, LX/8t9;->b:LX/8t8;

    move-object v1, v1

    .line 580847
    invoke-virtual {v1}, LX/8t8;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 580848
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "not reached"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 580849
    :pswitch_0
    iget-object v0, p1, LX/8t9;->d:Lcom/facebook/user/model/PicSquare;

    move-object v0, v0

    .line 580850
    iget-object v1, p1, LX/8t9;->c:Lcom/facebook/user/model/UserKey;

    move-object v1, v1

    .line 580851
    invoke-direct {p0, v0, p2, p3, v1}, LX/3Rb;->a(Lcom/facebook/user/model/PicSquare;IILcom/facebook/user/model/UserKey;)LX/1bf;

    move-result-object v0

    goto :goto_0

    .line 580852
    :pswitch_1
    iget-object v0, p1, LX/8t9;->c:Lcom/facebook/user/model/UserKey;

    move-object v0, v0

    .line 580853
    invoke-direct {p0, v0, p2, p3}, LX/3Rb;->a(Lcom/facebook/user/model/UserKey;II)LX/1bf;

    move-result-object v0

    goto :goto_0

    .line 580854
    :pswitch_2
    iget-object v0, p1, LX/8t9;->c:Lcom/facebook/user/model/UserKey;

    move-object v0, v0

    .line 580855
    iget-object v1, p1, LX/8t9;->d:Lcom/facebook/user/model/PicSquare;

    move-object v1, v1

    .line 580856
    invoke-direct {p0, v0, p2, p3, v1}, LX/3Rb;->a(Lcom/facebook/user/model/UserKey;IILcom/facebook/user/model/PicSquare;)LX/1bf;

    move-result-object v0

    goto :goto_0

    .line 580857
    :pswitch_3
    iget-object v0, p1, LX/8t9;->f:Ljava/lang/String;

    move-object v0, v0

    .line 580858
    invoke-static {v0}, LX/3Rb;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v0

    .line 580859
    if-nez v0, :cond_0

    .line 580860
    invoke-static {p2}, LX/3Rb;->a(I)LX/1bf;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;II)Landroid/net/Uri;
    .locals 3

    .prologue
    .line 580833
    iget-object v0, p0, LX/3Rb;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0s9;

    .line 580834
    invoke-interface {v0}, LX/0s9;->b()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "picture"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "type"

    const-string v2, "square"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "width"

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "height"

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    .line 580835
    iget-boolean v0, p0, LX/3Rb;->f:Z

    if-eqz v0, :cond_0

    .line 580836
    iget-object v0, p0, LX/3Rb;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0WJ;

    .line 580837
    invoke-virtual {v0}, LX/0WJ;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 580838
    const-string v2, "access_token"

    invoke-virtual {v0}, LX/0WJ;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    .line 580839
    iget-object p0, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->b:Ljava/lang/String;

    move-object v0, p0

    .line 580840
    invoke-virtual {v1, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 580841
    :cond_0
    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method
