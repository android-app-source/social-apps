.class public final LX/3xS;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public final d:I

.field public final synthetic e:LX/3xT;

.field public f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/3xT;I)V
    .locals 2

    .prologue
    const/high16 v1, -0x80000000

    .line 657699
    iput-object p1, p0, LX/3xS;->e:LX/3xT;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 657700
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/3xS;->f:Ljava/util/ArrayList;

    .line 657701
    iput v1, p0, LX/3xS;->a:I

    .line 657702
    iput v1, p0, LX/3xS;->b:I

    .line 657703
    const/4 v0, 0x0

    iput v0, p0, LX/3xS;->c:I

    .line 657704
    iput p2, p0, LX/3xS;->d:I

    .line 657705
    return-void
.end method

.method private static c(Landroid/view/View;)LX/3xO;
    .locals 1

    .prologue
    .line 657706
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/3xO;

    return-object v0
.end method

.method private g()V
    .locals 3

    .prologue
    .line 657707
    iget-object v0, p0, LX/3xS;->f:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 657708
    invoke-static {v0}, LX/3xS;->c(Landroid/view/View;)LX/3xO;

    move-result-object v1

    .line 657709
    iget-object v2, p0, LX/3xS;->e:LX/3xT;

    iget-object v2, v2, LX/3xT;->a:LX/1P5;

    invoke-virtual {v2, v0}, LX/1P5;->a(Landroid/view/View;)I

    move-result v0

    iput v0, p0, LX/3xS;->a:I

    .line 657710
    iget-boolean v0, v1, LX/3xO;->f:Z

    if-eqz v0, :cond_0

    .line 657711
    iget-object v0, p0, LX/3xS;->e:LX/3xT;

    iget-object v0, v0, LX/3xT;->f:LX/3xQ;

    invoke-virtual {v1}, LX/1a3;->e()I

    move-result v1

    invoke-virtual {v0, v1}, LX/3xQ;->d(I)Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;

    move-result-object v0

    .line 657712
    if-eqz v0, :cond_0

    iget v1, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;->b:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 657713
    iget v1, p0, LX/3xS;->a:I

    iget v2, p0, LX/3xS;->d:I

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;->a(I)I

    move-result v0

    sub-int v0, v1, v0

    iput v0, p0, LX/3xS;->a:I

    .line 657714
    :cond_0
    return-void
.end method

.method private h()V
    .locals 3

    .prologue
    .line 657715
    iget-object v0, p0, LX/3xS;->f:Ljava/util/ArrayList;

    iget-object v1, p0, LX/3xS;->f:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 657716
    invoke-static {v0}, LX/3xS;->c(Landroid/view/View;)LX/3xO;

    move-result-object v1

    .line 657717
    iget-object v2, p0, LX/3xS;->e:LX/3xT;

    iget-object v2, v2, LX/3xT;->a:LX/1P5;

    invoke-virtual {v2, v0}, LX/1P5;->b(Landroid/view/View;)I

    move-result v0

    iput v0, p0, LX/3xS;->b:I

    .line 657718
    iget-boolean v0, v1, LX/3xO;->f:Z

    if-eqz v0, :cond_0

    .line 657719
    iget-object v0, p0, LX/3xS;->e:LX/3xT;

    iget-object v0, v0, LX/3xT;->f:LX/3xQ;

    invoke-virtual {v1}, LX/1a3;->e()I

    move-result v1

    invoke-virtual {v0, v1}, LX/3xQ;->d(I)Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;

    move-result-object v0

    .line 657720
    if-eqz v0, :cond_0

    iget v1, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;->b:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 657721
    iget v1, p0, LX/3xS;->b:I

    iget v2, p0, LX/3xS;->d:I

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;->a(I)I

    move-result v0

    add-int/2addr v0, v1

    iput v0, p0, LX/3xS;->b:I

    .line 657722
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 657723
    iget v0, p0, LX/3xS;->a:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 657724
    iget v0, p0, LX/3xS;->a:I

    .line 657725
    :goto_0
    return v0

    .line 657726
    :cond_0
    invoke-direct {p0}, LX/3xS;->g()V

    .line 657727
    iget v0, p0, LX/3xS;->a:I

    goto :goto_0
.end method

.method public final a(I)I
    .locals 2

    .prologue
    .line 657728
    iget v0, p0, LX/3xS;->a:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_1

    .line 657729
    iget p1, p0, LX/3xS;->a:I

    .line 657730
    :cond_0
    :goto_0
    return p1

    .line 657731
    :cond_1
    iget-object v0, p0, LX/3xS;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_0

    .line 657732
    invoke-direct {p0}, LX/3xS;->g()V

    .line 657733
    iget p1, p0, LX/3xS;->a:I

    goto :goto_0
.end method

.method public final a(Landroid/view/View;)V
    .locals 4

    .prologue
    const/high16 v3, -0x80000000

    .line 657690
    invoke-static {p1}, LX/3xS;->c(Landroid/view/View;)LX/3xO;

    move-result-object v0

    .line 657691
    iput-object p0, v0, LX/3xO;->e:LX/3xS;

    .line 657692
    iget-object v1, p0, LX/3xS;->f:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 657693
    iput v3, p0, LX/3xS;->a:I

    .line 657694
    iget-object v1, p0, LX/3xS;->f:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 657695
    iput v3, p0, LX/3xS;->b:I

    .line 657696
    :cond_0
    invoke-virtual {v0}, LX/1a3;->c()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, LX/1a3;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 657697
    :cond_1
    iget v0, p0, LX/3xS;->c:I

    iget-object v1, p0, LX/3xS;->e:LX/3xT;

    iget-object v1, v1, LX/3xT;->a:LX/1P5;

    invoke-virtual {v1, p1}, LX/1P5;->c(Landroid/view/View;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, LX/3xS;->c:I

    .line 657698
    :cond_2
    return-void
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 657734
    iget v0, p0, LX/3xS;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 657735
    iget v0, p0, LX/3xS;->b:I

    .line 657736
    :goto_0
    return v0

    .line 657737
    :cond_0
    invoke-direct {p0}, LX/3xS;->h()V

    .line 657738
    iget v0, p0, LX/3xS;->b:I

    goto :goto_0
.end method

.method public final b(I)I
    .locals 2

    .prologue
    .line 657683
    iget v0, p0, LX/3xS;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_1

    .line 657684
    iget p1, p0, LX/3xS;->b:I

    .line 657685
    :cond_0
    :goto_0
    return p1

    .line 657686
    :cond_1
    iget-object v0, p0, LX/3xS;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 657687
    if-eqz v0, :cond_0

    .line 657688
    invoke-direct {p0}, LX/3xS;->h()V

    .line 657689
    iget p1, p0, LX/3xS;->b:I

    goto :goto_0
.end method

.method public final b(Landroid/view/View;)V
    .locals 4

    .prologue
    const/high16 v3, -0x80000000

    .line 657642
    invoke-static {p1}, LX/3xS;->c(Landroid/view/View;)LX/3xO;

    move-result-object v0

    .line 657643
    iput-object p0, v0, LX/3xO;->e:LX/3xS;

    .line 657644
    iget-object v1, p0, LX/3xS;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 657645
    iput v3, p0, LX/3xS;->b:I

    .line 657646
    iget-object v1, p0, LX/3xS;->f:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 657647
    iput v3, p0, LX/3xS;->a:I

    .line 657648
    :cond_0
    invoke-virtual {v0}, LX/1a3;->c()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, LX/1a3;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 657649
    :cond_1
    iget v0, p0, LX/3xS;->c:I

    iget-object v1, p0, LX/3xS;->e:LX/3xT;

    iget-object v1, v1, LX/3xT;->a:LX/1P5;

    invoke-virtual {v1, p1}, LX/1P5;->c(Landroid/view/View;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, LX/3xS;->c:I

    .line 657650
    :cond_2
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 657677
    iget-object v0, p0, LX/3xS;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 657678
    const/high16 v0, -0x80000000

    .line 657679
    iput v0, p0, LX/3xS;->a:I

    .line 657680
    iput v0, p0, LX/3xS;->b:I

    .line 657681
    const/4 v0, 0x0

    iput v0, p0, LX/3xS;->c:I

    .line 657682
    return-void
.end method

.method public final c(I)V
    .locals 0

    .prologue
    .line 657675
    iput p1, p0, LX/3xS;->a:I

    iput p1, p0, LX/3xS;->b:I

    .line 657676
    return-void
.end method

.method public final d()V
    .locals 5

    .prologue
    const/high16 v4, -0x80000000

    .line 657665
    iget-object v0, p0, LX/3xS;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 657666
    iget-object v0, p0, LX/3xS;->f:Ljava/util/ArrayList;

    add-int/lit8 v2, v1, -0x1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 657667
    invoke-static {v0}, LX/3xS;->c(Landroid/view/View;)LX/3xO;

    move-result-object v2

    .line 657668
    const/4 v3, 0x0

    iput-object v3, v2, LX/3xO;->e:LX/3xS;

    .line 657669
    invoke-virtual {v2}, LX/1a3;->c()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v2}, LX/1a3;->d()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 657670
    :cond_0
    iget v2, p0, LX/3xS;->c:I

    iget-object v3, p0, LX/3xS;->e:LX/3xT;

    iget-object v3, v3, LX/3xT;->a:LX/1P5;

    invoke-virtual {v3, v0}, LX/1P5;->c(Landroid/view/View;)I

    move-result v0

    sub-int v0, v2, v0

    iput v0, p0, LX/3xS;->c:I

    .line 657671
    :cond_1
    const/4 v0, 0x1

    if-ne v1, v0, :cond_2

    .line 657672
    iput v4, p0, LX/3xS;->a:I

    .line 657673
    :cond_2
    iput v4, p0, LX/3xS;->b:I

    .line 657674
    return-void
.end method

.method public final d(I)V
    .locals 2

    .prologue
    const/high16 v1, -0x80000000

    .line 657660
    iget v0, p0, LX/3xS;->a:I

    if-eq v0, v1, :cond_0

    .line 657661
    iget v0, p0, LX/3xS;->a:I

    add-int/2addr v0, p1

    iput v0, p0, LX/3xS;->a:I

    .line 657662
    :cond_0
    iget v0, p0, LX/3xS;->b:I

    if-eq v0, v1, :cond_1

    .line 657663
    iget v0, p0, LX/3xS;->b:I

    add-int/2addr v0, p1

    iput v0, p0, LX/3xS;->b:I

    .line 657664
    :cond_1
    return-void
.end method

.method public final e()V
    .locals 4

    .prologue
    const/high16 v3, -0x80000000

    .line 657651
    iget-object v0, p0, LX/3xS;->f:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 657652
    invoke-static {v0}, LX/3xS;->c(Landroid/view/View;)LX/3xO;

    move-result-object v1

    .line 657653
    const/4 v2, 0x0

    iput-object v2, v1, LX/3xO;->e:LX/3xS;

    .line 657654
    iget-object v2, p0, LX/3xS;->f:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_0

    .line 657655
    iput v3, p0, LX/3xS;->b:I

    .line 657656
    :cond_0
    invoke-virtual {v1}, LX/1a3;->c()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v1}, LX/1a3;->d()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 657657
    :cond_1
    iget v1, p0, LX/3xS;->c:I

    iget-object v2, p0, LX/3xS;->e:LX/3xT;

    iget-object v2, v2, LX/3xT;->a:LX/1P5;

    invoke-virtual {v2, v0}, LX/1P5;->c(Landroid/view/View;)I

    move-result v0

    sub-int v0, v1, v0

    iput v0, p0, LX/3xS;->c:I

    .line 657658
    :cond_2
    iput v3, p0, LX/3xS;->a:I

    .line 657659
    return-void
.end method
