.class public LX/3bQ;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/CustomLinearLayout;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 610970
    const v0, 0x7f031289

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, LX/3bQ;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 610971
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/1Ps;)LX/1X6;
    .locals 4

    .prologue
    .line 610972
    invoke-interface {p0}, LX/1Ps;->h()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 610973
    :goto_0
    new-instance v1, LX/1X6;

    const/4 v2, 0x0

    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v3

    .line 610974
    iput v0, v3, LX/1UY;->c:F

    .line 610975
    move-object v0, v3

    .line 610976
    const/high16 v3, 0x40200000    # 2.5f

    .line 610977
    iput v3, v0, LX/1UY;->b:F

    .line 610978
    move-object v0, v0

    .line 610979
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v3

    invoke-interface {p0}, LX/1Ps;->iM_()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-result-object v0

    if-eqz v0, :cond_1

    sget-object v0, LX/1X9;->MIDDLE:LX/1X9;

    :goto_1
    invoke-direct {v1, v2, v3, v0}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    return-object v1

    .line 610980
    :cond_0
    const/high16 v0, 0x40c00000    # 6.0f

    goto :goto_0

    .line 610981
    :cond_1
    sget-object v0, LX/1X9;->TOP:LX/1X9;

    goto :goto_1
.end method
