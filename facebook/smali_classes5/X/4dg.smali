.class public final enum LX/4dg;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/4dg;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/4dg;

.field public static final enum ABORT:LX/4dg;

.field public static final enum NOT_REQUIRED:LX/4dg;

.field public static final enum REQUIRED:LX/4dg;

.field public static final enum SKIP:LX/4dg;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 796305
    new-instance v0, LX/4dg;

    const-string v1, "REQUIRED"

    invoke-direct {v0, v1, v2}, LX/4dg;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4dg;->REQUIRED:LX/4dg;

    .line 796306
    new-instance v0, LX/4dg;

    const-string v1, "NOT_REQUIRED"

    invoke-direct {v0, v1, v3}, LX/4dg;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4dg;->NOT_REQUIRED:LX/4dg;

    .line 796307
    new-instance v0, LX/4dg;

    const-string v1, "SKIP"

    invoke-direct {v0, v1, v4}, LX/4dg;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4dg;->SKIP:LX/4dg;

    .line 796308
    new-instance v0, LX/4dg;

    const-string v1, "ABORT"

    invoke-direct {v0, v1, v5}, LX/4dg;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4dg;->ABORT:LX/4dg;

    .line 796309
    const/4 v0, 0x4

    new-array v0, v0, [LX/4dg;

    sget-object v1, LX/4dg;->REQUIRED:LX/4dg;

    aput-object v1, v0, v2

    sget-object v1, LX/4dg;->NOT_REQUIRED:LX/4dg;

    aput-object v1, v0, v3

    sget-object v1, LX/4dg;->SKIP:LX/4dg;

    aput-object v1, v0, v4

    sget-object v1, LX/4dg;->ABORT:LX/4dg;

    aput-object v1, v0, v5

    sput-object v0, LX/4dg;->$VALUES:[LX/4dg;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 796304
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/4dg;
    .locals 1

    .prologue
    .line 796311
    const-class v0, LX/4dg;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/4dg;

    return-object v0
.end method

.method public static values()[LX/4dg;
    .locals 1

    .prologue
    .line 796310
    sget-object v0, LX/4dg;->$VALUES:[LX/4dg;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/4dg;

    return-object v0
.end method
