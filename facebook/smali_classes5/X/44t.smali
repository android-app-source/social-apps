.class public LX/44t;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/44t;


# instance fields
.field public a:LX/0jo;


# direct methods
.method public constructor <init>(LX/0jo;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 670015
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 670016
    iput-object p1, p0, LX/44t;->a:LX/0jo;

    .line 670017
    return-void
.end method

.method public static a(LX/0QB;)LX/44t;
    .locals 4

    .prologue
    .line 670018
    sget-object v0, LX/44t;->b:LX/44t;

    if-nez v0, :cond_1

    .line 670019
    const-class v1, LX/44t;

    monitor-enter v1

    .line 670020
    :try_start_0
    sget-object v0, LX/44t;->b:LX/44t;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 670021
    if-eqz v2, :cond_0

    .line 670022
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 670023
    new-instance p0, LX/44t;

    invoke-static {v0}, LX/0oR;->a(LX/0QB;)LX/0oR;

    move-result-object v3

    check-cast v3, LX/0jo;

    invoke-direct {p0, v3}, LX/44t;-><init>(LX/0jo;)V

    .line 670024
    move-object v0, p0

    .line 670025
    sput-object v0, LX/44t;->b:LX/44t;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 670026
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 670027
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 670028
    :cond_1
    sget-object v0, LX/44t;->b:LX/44t;

    return-object v0

    .line 670029
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 670030
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static c(Landroid/content/Intent;)I
    .locals 2

    .prologue
    .line 670031
    const-string v0, "target_fragment"

    const/4 v1, -0x1

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 670032
    if-gez v0, :cond_0

    .line 670033
    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_1

    .line 670034
    sget-object v0, LX/0cQ;->NATIVE_NEWS_FEED_FRAGMENT:LX/0cQ;

    invoke-virtual {v0}, LX/0cQ;->ordinal()I

    move-result v0

    .line 670035
    :cond_0
    :goto_0
    return v0

    .line 670036
    :cond_1
    sget-object v0, LX/0cQ;->FACEWEB_FRAGMENT:LX/0cQ;

    invoke-virtual {v0}, LX/0cQ;->ordinal()I

    move-result v0

    goto :goto_0
.end method
