.class public final LX/3lD;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/3lD;


# instance fields
.field private final a:LX/20l;

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/9Aw;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/C4P;


# direct methods
.method public constructor <init>(LX/20l;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/20l;",
            "LX/0Or",
            "<",
            "LX/9Aw;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 633628
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 633629
    iput-object p1, p0, LX/3lD;->a:LX/20l;

    .line 633630
    iput-object p2, p0, LX/3lD;->b:LX/0Or;

    .line 633631
    return-void
.end method

.method public static a()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 633632
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->UFI_CLICKED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/3lD;
    .locals 5

    .prologue
    .line 633633
    sget-object v0, LX/3lD;->d:LX/3lD;

    if-nez v0, :cond_1

    .line 633634
    const-class v1, LX/3lD;

    monitor-enter v1

    .line 633635
    :try_start_0
    sget-object v0, LX/3lD;->d:LX/3lD;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 633636
    if-eqz v2, :cond_0

    .line 633637
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 633638
    new-instance v4, LX/3lD;

    invoke-static {v0}, LX/20l;->a(LX/0QB;)LX/20l;

    move-result-object v3

    check-cast v3, LX/20l;

    const/16 p0, 0x1d84

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v4, v3, p0}, LX/3lD;-><init>(LX/20l;LX/0Or;)V

    .line 633639
    move-object v0, v4

    .line 633640
    sput-object v0, LX/3lD;->d:LX/3lD;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 633641
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 633642
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 633643
    :cond_1
    sget-object v0, LX/3lD;->d:LX/3lD;

    return-object v0

    .line 633644
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 633645
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final c()LX/10S;
    .locals 1

    .prologue
    .line 633646
    iget-object v0, p0, LX/3lD;->a:LX/20l;

    invoke-virtual {v0}, LX/20l;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/10S;->ELIGIBLE:LX/10S;

    goto :goto_0
.end method
