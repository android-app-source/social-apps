.class public final LX/3gC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2ZE;


# instance fields
.field public final synthetic a:LX/3gA;


# direct methods
.method public constructor <init>(LX/3gA;)V
    .locals 0

    .prologue
    .line 624132
    iput-object p1, p0, LX/3gC;->a:LX/3gA;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Iterable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "LX/2Vj;",
            ">;"
        }
    .end annotation

    .prologue
    .line 624133
    iget-object v0, p0, LX/3gC;->a:LX/3gA;

    iget-object v0, v0, LX/3gA;->b:LX/3gD;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v0

    const-string v1, "fetchFacebookEmployeeStatus"

    .line 624134
    iput-object v1, v0, LX/2Vk;->c:Ljava/lang/String;

    .line 624135
    move-object v0, v0

    .line 624136
    invoke-virtual {v0}, LX/2Vk;->a()LX/2Vj;

    move-result-object v0

    .line 624137
    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 624138
    const-string v0, "fetchFacebookEmployeeStatus"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 624139
    if-eqz v0, :cond_0

    .line 624140
    iget-object v1, p0, LX/3gC;->a:LX/3gA;

    iget-object v1, v1, LX/3gA;->a:LX/0WJ;

    invoke-virtual {v1}, LX/0WJ;->c()Lcom/facebook/user/model/User;

    move-result-object v1

    .line 624141
    if-eqz v1, :cond_0

    .line 624142
    new-instance v2, LX/0XI;

    invoke-direct {v2}, LX/0XI;-><init>()V

    .line 624143
    invoke-virtual {v2, v1}, LX/0XI;->a(Lcom/facebook/user/model/User;)LX/0XI;

    .line 624144
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 624145
    iput-boolean v0, v2, LX/0XI;->v:Z

    .line 624146
    iget-object v0, p0, LX/3gC;->a:LX/3gA;

    iget-object v0, v0, LX/3gA;->a:LX/0WJ;

    invoke-virtual {v2}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0WJ;->c(Lcom/facebook/user/model/User;)V

    .line 624147
    :cond_0
    return-void
.end method
