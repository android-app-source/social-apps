.class public LX/4c7;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/4c7;


# instance fields
.field private final a:LX/4cD;


# direct methods
.method public constructor <init>(LX/4cD;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 794633
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 794634
    iput-object p1, p0, LX/4c7;->a:LX/4cD;

    .line 794635
    return-void
.end method

.method public static a(LX/0QB;)LX/4c7;
    .locals 4

    .prologue
    .line 794636
    sget-object v0, LX/4c7;->b:LX/4c7;

    if-nez v0, :cond_1

    .line 794637
    const-class v1, LX/4c7;

    monitor-enter v1

    .line 794638
    :try_start_0
    sget-object v0, LX/4c7;->b:LX/4c7;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 794639
    if-eqz v2, :cond_0

    .line 794640
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 794641
    new-instance p0, LX/4c7;

    invoke-static {v0}, LX/4cD;->a(LX/0QB;)LX/4cD;

    move-result-object v3

    check-cast v3, LX/4cD;

    invoke-direct {p0, v3}, LX/4c7;-><init>(LX/4cD;)V

    .line 794642
    move-object v0, p0

    .line 794643
    sput-object v0, LX/4c7;->b:LX/4c7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 794644
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 794645
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 794646
    :cond_1
    sget-object v0, LX/4c7;->b:LX/4c7;

    return-object v0

    .line 794647
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 794648
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/4bV;LX/4bP;LX/4c8;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 794624
    iget-object v2, p0, LX/4c7;->a:LX/4cD;

    invoke-virtual {v2, p1}, LX/4cD;->a(LX/4bV;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 794625
    :cond_0
    :goto_0
    return v0

    .line 794626
    :cond_1
    iget-object v2, p1, LX/4bV;->c:LX/15D;

    invoke-virtual {v2}, LX/15D;->r()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 794627
    iget-object v2, p2, LX/4bP;->a:Ljava/util/ArrayList;

    invoke-static {v2}, Lcom/facebook/http/common/FbHttpUtils;->b(Ljava/util/ArrayList;)I

    move-result v2

    move v2, v2

    .line 794628
    iget v3, p3, LX/4c8;->a:I

    if-lt v2, v3, :cond_2

    move v0, v1

    .line 794629
    goto :goto_0

    .line 794630
    :cond_2
    invoke-virtual {p1}, LX/4bV;->a()Lcom/facebook/http/interfaces/RequestPriority;

    move-result-object v2

    sget-object v3, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    if-ne v2, v3, :cond_3

    .line 794631
    invoke-virtual {p2}, LX/4bP;->b()I

    move-result v2

    iget v3, p3, LX/4c8;->b:I

    if-lt v2, v3, :cond_0

    move v0, v1

    goto :goto_0

    .line 794632
    :cond_3
    invoke-virtual {p2}, LX/4bP;->b()I

    move-result v2

    iget v3, p3, LX/4c8;->c:I

    if-lt v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method
