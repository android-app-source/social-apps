.class public final LX/3mt;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/3mt;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/3mw;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/3mv;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 637377
    const/4 v0, 0x0

    sput-object v0, LX/3mt;->a:LX/3mt;

    .line 637378
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/3mt;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 637379
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 637380
    new-instance v0, LX/3mv;

    invoke-direct {v0}, LX/3mv;-><init>()V

    iput-object v0, p0, LX/3mt;->c:LX/3mv;

    .line 637381
    return-void
.end method

.method public static declared-synchronized q()LX/3mt;
    .locals 2

    .prologue
    .line 637382
    const-class v1, LX/3mt;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/3mt;->a:LX/3mt;

    if-nez v0, :cond_0

    .line 637383
    new-instance v0, LX/3mt;

    invoke-direct {v0}, LX/3mt;-><init>()V

    sput-object v0, LX/3mt;->a:LX/3mt;

    .line 637384
    :cond_0
    sget-object v0, LX/3mt;->a:LX/3mt;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 637385
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 3

    .prologue
    .line 637386
    check-cast p2, LX/3mu;

    .line 637387
    iget-object v0, p2, LX/3mu;->a:LX/1X1;

    iget v1, p2, LX/3mu;->b:I

    iget v2, p2, LX/3mu;->c:I

    .line 637388
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object p0

    const/4 p2, 0x4

    invoke-interface {p0, p2, v1}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object p0

    const/4 p2, 0x5

    invoke-interface {p0, p2, v2}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object p0

    invoke-interface {p0, v0}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    move-result-object p0

    invoke-interface {p0}, LX/1Di;->k()LX/1Dg;

    move-result-object p0

    move-object v0, p0

    .line 637389
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 637390
    invoke-static {}, LX/1dS;->b()V

    .line 637391
    const/4 v0, 0x0

    return-object v0
.end method
