.class public final LX/4rO;
.super LX/2Ax;
.source ""


# instance fields
.field public final t:LX/2Ax;

.field public final u:[Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/2Ax;[Ljava/lang/Class;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2Ax;",
            "[",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 815582
    invoke-direct {p0, p1}, LX/2Ax;-><init>(LX/2Ax;)V

    .line 815583
    iput-object p1, p0, LX/4rO;->t:LX/2Ax;

    .line 815584
    iput-object p2, p0, LX/4rO;->u:[Ljava/lang/Class;

    .line 815585
    return-void
.end method

.method private c(LX/4ro;)LX/4rO;
    .locals 3

    .prologue
    .line 815581
    new-instance v0, LX/4rO;

    iget-object v1, p0, LX/4rO;->t:LX/2Ax;

    invoke-virtual {v1, p1}, LX/2Ax;->a(LX/4ro;)LX/2Ax;

    move-result-object v1

    iget-object v2, p0, LX/4rO;->u:[Ljava/lang/Class;

    invoke-direct {v0, v1, v2}, LX/4rO;-><init>(LX/2Ax;[Ljava/lang/Class;)V

    return-object v0
.end method


# virtual methods
.method public final synthetic a(LX/4ro;)LX/2Ax;
    .locals 1

    .prologue
    .line 815557
    invoke-direct {p0, p1}, LX/4rO;->c(LX/4ro;)LX/4rO;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/fasterxml/jackson/databind/JsonSerializer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 815579
    iget-object v0, p0, LX/4rO;->t:LX/2Ax;

    invoke-virtual {v0, p1}, LX/2Ax;->a(Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 815580
    return-void
.end method

.method public final a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 815570
    iget-object v0, p3, LX/0my;->_serializationView:Ljava/lang/Class;

    move-object v1, v0

    .line 815571
    if-eqz v1, :cond_1

    .line 815572
    const/4 v0, 0x0

    iget-object v2, p0, LX/4rO;->u:[Ljava/lang/Class;

    array-length v2, v2

    .line 815573
    :goto_0
    if-ge v0, v2, :cond_0

    .line 815574
    iget-object v3, p0, LX/4rO;->u:[Ljava/lang/Class;

    aget-object v3, v3, v0

    invoke-virtual {v3, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 815575
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 815576
    :cond_0
    if-ne v0, v2, :cond_1

    .line 815577
    :goto_1
    return-void

    .line 815578
    :cond_1
    iget-object v0, p0, LX/4rO;->t:LX/2Ax;

    invoke-virtual {v0, p1, p2, p3}, LX/2Ax;->a(Ljava/lang/Object;LX/0nX;LX/0my;)V

    goto :goto_1
.end method

.method public final b(Lcom/fasterxml/jackson/databind/JsonSerializer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 815568
    iget-object v0, p0, LX/4rO;->t:LX/2Ax;

    invoke-virtual {v0, p1}, LX/2Ax;->b(Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 815569
    return-void
.end method

.method public final b(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 815558
    iget-object v0, p3, LX/0my;->_serializationView:Ljava/lang/Class;

    move-object v1, v0

    .line 815559
    if-eqz v1, :cond_1

    .line 815560
    const/4 v0, 0x0

    iget-object v2, p0, LX/4rO;->u:[Ljava/lang/Class;

    array-length v2, v2

    .line 815561
    :goto_0
    if-ge v0, v2, :cond_0

    .line 815562
    iget-object v3, p0, LX/4rO;->u:[Ljava/lang/Class;

    aget-object v3, v3, v0

    invoke-virtual {v3, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 815563
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 815564
    :cond_0
    if-ne v0, v2, :cond_1

    .line 815565
    iget-object v0, p0, LX/4rO;->t:LX/2Ax;

    invoke-virtual {v0, p2, p3}, LX/2Ax;->a(LX/0nX;LX/0my;)V

    .line 815566
    :goto_1
    return-void

    .line 815567
    :cond_1
    iget-object v0, p0, LX/4rO;->t:LX/2Ax;

    invoke-virtual {v0, p1, p2, p3}, LX/2Ax;->b(Ljava/lang/Object;LX/0nX;LX/0my;)V

    goto :goto_1
.end method
