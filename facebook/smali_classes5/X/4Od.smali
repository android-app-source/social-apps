.class public LX/4Od;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 697468
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 697498
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 697499
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 697500
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 697501
    invoke-static {p0, p1}, LX/4Od;->b(LX/15w;LX/186;)I

    move-result v1

    .line 697502
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 697503
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 697504
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 697505
    invoke-virtual {p0, p1, v2, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 697506
    if-eqz v0, :cond_0

    .line 697507
    const-string v0, "inline_style"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 697508
    const-class v0, Lcom/facebook/graphql/enums/GraphQLInlineStyle;

    invoke-virtual {p0, p1, v2, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLInlineStyle;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLInlineStyle;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 697509
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 697510
    if-eqz v0, :cond_1

    .line 697511
    const-string v1, "length"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 697512
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 697513
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 697514
    if-eqz v0, :cond_2

    .line 697515
    const-string v1, "offset"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 697516
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 697517
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 697518
    return-void
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 697492
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 697493
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 697494
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2}, LX/4Od;->a(LX/15i;ILX/0nX;)V

    .line 697495
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 697496
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 697497
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 11

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 697469
    const/4 v0, 0x0

    .line 697470
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v4, :cond_8

    .line 697471
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 697472
    :goto_0
    return v1

    .line 697473
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_4

    .line 697474
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 697475
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 697476
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_0

    if-eqz v8, :cond_0

    .line 697477
    const-string v9, "inline_style"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 697478
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/graphql/enums/GraphQLInlineStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLInlineStyle;

    move-result-object v4

    move-object v7, v4

    move v4, v2

    goto :goto_1

    .line 697479
    :cond_1
    const-string v9, "length"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 697480
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v3

    move v6, v3

    move v3, v2

    goto :goto_1

    .line 697481
    :cond_2
    const-string v9, "offset"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 697482
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v5, v0

    move v0, v2

    goto :goto_1

    .line 697483
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 697484
    :cond_4
    const/4 v8, 0x3

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 697485
    if-eqz v4, :cond_5

    .line 697486
    invoke-virtual {p1, v1, v7}, LX/186;->a(ILjava/lang/Enum;)V

    .line 697487
    :cond_5
    if-eqz v3, :cond_6

    .line 697488
    invoke-virtual {p1, v2, v6, v1}, LX/186;->a(III)V

    .line 697489
    :cond_6
    if-eqz v0, :cond_7

    .line 697490
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v5, v1}, LX/186;->a(III)V

    .line 697491
    :cond_7
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_8
    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move-object v7, v0

    move v0, v1

    goto :goto_1
.end method
