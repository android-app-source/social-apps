.class public LX/4lu;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static a:Ljava/lang/reflect/Field;

.field private static b:Ljava/lang/reflect/Field;

.field public static c:Z

.field private static volatile d:LX/4lu;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 804961
    const/4 v0, 0x0

    sput-boolean v0, LX/4lu;->c:Z

    .line 804962
    :try_start_0
    const-string v0, "org.apache.harmony.xnet.provider.jsse.SSLParametersImpl"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    .line 804963
    const-string v0, "org.apache.harmony.xnet.provider.jsse.OpenSSLSocketFactoryImpl"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    .line 804964
    const-class v0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSocketFactoryImpl;

    const-string v1, "sslParameters"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 804965
    sput-object v0, LX/4lu;->a:Ljava/lang/reflect/Field;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 804966
    const-class v0, Lorg/apache/harmony/xnet/provider/jsse/SSLParametersImpl;

    const-string v1, "clientSessionContext"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 804967
    sput-object v0, LX/4lu;->b:Ljava/lang/reflect/Field;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 804968
    const/4 v0, 0x1

    sput-boolean v0, LX/4lu;->c:Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 804969
    :goto_0
    return-void

    :catch_0
    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 804988
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 804989
    return-void
.end method

.method public static a(LX/0QB;)LX/4lu;
    .locals 3

    .prologue
    .line 804976
    sget-object v0, LX/4lu;->d:LX/4lu;

    if-nez v0, :cond_1

    .line 804977
    const-class v1, LX/4lu;

    monitor-enter v1

    .line 804978
    :try_start_0
    sget-object v0, LX/4lu;->d:LX/4lu;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 804979
    if-eqz v2, :cond_0

    .line 804980
    :try_start_1
    new-instance v0, LX/4lu;

    invoke-direct {v0}, LX/4lu;-><init>()V

    .line 804981
    move-object v0, v0

    .line 804982
    sput-object v0, LX/4lu;->d:LX/4lu;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 804983
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 804984
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 804985
    :cond_1
    sget-object v0, LX/4lu;->d:LX/4lu;

    return-object v0

    .line 804986
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 804987
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Ljavax/net/ssl/SSLSocketFactory;)Lorg/apache/harmony/xnet/provider/jsse/SSLParametersImpl;
    .locals 2

    .prologue
    .line 804970
    :try_start_0
    sget-object v0, LX/4lu;->a:Ljava/lang/reflect/Field;

    invoke-virtual {v0, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/harmony/xnet/provider/jsse/SSLParametersImpl;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    .line 804971
    return-object v0

    .line 804972
    :catch_0
    move-exception v0

    .line 804973
    new-instance v1, LX/4ll;

    invoke-direct {v1, v0}, LX/4ll;-><init>(Ljava/lang/Exception;)V

    throw v1

    .line 804974
    :catch_1
    move-exception v0

    .line 804975
    new-instance v1, LX/4ll;

    invoke-direct {v1, v0}, LX/4ll;-><init>(Ljava/lang/Exception;)V

    throw v1
.end method
