.class public abstract LX/3nE;
.super Landroid/os/AsyncTask;
.source ""

# interfaces
.implements LX/0Ya;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Params:",
        "Ljava/lang/Object;",
        "Progress:",
        "Ljava/lang/Object;",
        "Result:",
        "Ljava/lang/Object;",
        ">",
        "Landroid/os/AsyncTask",
        "<TParams;TProgress;TResult;>;",
        "LX/0Ya;"
    }
.end annotation


# instance fields
.field private a:LX/0cV;

.field public b:LX/0Sj;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 638080
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/0Ya;Landroid/content/Context;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0Ya;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    invoke-static {p2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/3nE;

    invoke-static {p0}, LX/0Si;->a(LX/0QB;)LX/0Si;

    move-result-object p0

    check-cast p0, LX/0Sj;

    iput-object p0, p1, LX/3nE;->b:LX/0Sj;

    return-void
.end method


# virtual methods
.method public final varargs a(LX/0Sj;[Ljava/lang/Object;)LX/3nE;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Sj;",
            "[TParams;)",
            "LX/3nE",
            "<TParams;TProgress;TResult;>;"
        }
    .end annotation

    .prologue
    .line 638081
    const-string v0, "AsyncTask"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LX/0Sj;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0cV;

    move-result-object v0

    iput-object v0, p0, LX/3nE;->a:LX/0cV;

    .line 638082
    sget-object v0, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    invoke-virtual {p0, v0, p2}, LX/3nE;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 638083
    return-object p0
.end method

.method public final varargs a(Landroid/content/Context;[Ljava/lang/Object;)LX/3nE;
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "FbInjectorGet"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "[TParams;)",
            "LX/3nE",
            "<TParams;TProgress;TResult;>;"
        }
    .end annotation

    .prologue
    .line 638084
    const-class v0, LX/3nE;

    invoke-static {v0, p0, p1}, LX/3nE;->a(Ljava/lang/Class;LX/0Ya;Landroid/content/Context;)V

    .line 638085
    iget-object v0, p0, LX/3nE;->b:LX/0Sj;

    invoke-virtual {p0, v0, p2}, LX/3nE;->a(LX/0Sj;[Ljava/lang/Object;)LX/3nE;

    move-result-object v0

    return-object v0
.end method

.method public varargs abstract a([Ljava/lang/Object;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TParams;)TResult;"
        }
    .end annotation
.end method

.method public final varargs doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TParams;)TResult;"
        }
    .end annotation

    .prologue
    .line 638086
    iget-object v0, p0, LX/3nE;->a:LX/0cV;

    if-eqz v0, :cond_0

    .line 638087
    iget-object v0, p0, LX/3nE;->a:LX/0cV;

    invoke-interface {v0}, LX/0cV;->a()V

    .line 638088
    :cond_0
    :try_start_0
    invoke-virtual {p0, p1}, LX/3nE;->a([Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 638089
    iget-object v1, p0, LX/3nE;->a:LX/0cV;

    if-eqz v1, :cond_1

    .line 638090
    iget-object v1, p0, LX/3nE;->a:LX/0cV;

    invoke-interface {v1}, LX/0cV;->b()V

    :cond_1
    return-object v0

    .line 638091
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/3nE;->a:LX/0cV;

    if-eqz v1, :cond_2

    .line 638092
    iget-object v1, p0, LX/3nE;->a:LX/0cV;

    invoke-interface {v1}, LX/0cV;->b()V

    :cond_2
    throw v0
.end method
