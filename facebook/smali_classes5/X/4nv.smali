.class public abstract LX/4nv;
.super Landroid/view/View;
.source ""


# instance fields
.field public a:Landroid/content/Context;

.field private b:I

.field private c:LX/0QA;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 807992
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 807993
    const/4 v0, -0x1

    iput v0, p0, LX/4nv;->b:I

    .line 807994
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/4nv;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 807995
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 807988
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 807989
    const/4 v0, -0x1

    iput v0, p0, LX/4nv;->b:I

    .line 807990
    invoke-direct {p0, p1, p2}, LX/4nv;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 807991
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 807959
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 807960
    const/4 v0, -0x1

    iput v0, p0, LX/4nv;->b:I

    .line 807961
    invoke-direct {p0, p1, p2}, LX/4nv;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 807962
    return-void
.end method

.method private final a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    .line 807981
    iput-object p1, p0, LX/4nv;->a:Landroid/content/Context;

    .line 807982
    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    iput-object v0, p0, LX/4nv;->c:LX/0QA;

    .line 807983
    if-eqz p2, :cond_0

    .line 807984
    sget-object v0, LX/03r;->CustomViewStub:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 807985
    const/16 v1, 0x0

    iget v2, p0, LX/4nv;->b:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, LX/4nv;->b:I

    .line 807986
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 807987
    :cond_0
    return-void
.end method


# virtual methods
.method public a()Landroid/view/View;
    .locals 5

    .prologue
    const/4 v2, -0x2

    .line 807964
    invoke-virtual {p0}, LX/4nv;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    .line 807965
    if-nez v1, :cond_0

    .line 807966
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "CustomViewStub has no parent. The view could have been already inflated."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 807967
    :cond_0
    instance-of v0, v1, Landroid/view/ViewGroup;

    if-nez v0, :cond_1

    .line 807968
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "CustomViewStub is in an invalid parent"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 807969
    :cond_1
    invoke-virtual {p0}, LX/4nv;->getInflatedLayout()Landroid/view/View;

    move-result-object v3

    .line 807970
    if-nez v3, :cond_2

    .line 807971
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "CustomViewStub.getInflatedLayout returned null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 807972
    :cond_2
    iget v0, p0, LX/4nv;->b:I

    invoke-virtual {v3, v0}, Landroid/view/View;->setId(I)V

    move-object v0, v1

    .line 807973
    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v4

    .line 807974
    invoke-virtual {p0}, LX/4nv;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, LX/4nv;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    move-object v2, v0

    :goto_0
    move-object v0, v1

    .line 807975
    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->removeViewAt(I)V

    .line 807976
    instance-of v0, v1, LX/0h1;

    if-eqz v0, :cond_4

    instance-of v0, v3, LX/2eZ;

    if-eqz v0, :cond_4

    .line 807977
    check-cast v1, LX/0h1;

    invoke-interface {v1, v3, v4, v2}, LX/0h1;->attachRecyclableViewToParent(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 807978
    :goto_1
    return-object v3

    .line 807979
    :cond_3
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v0, v2, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    move-object v2, v0

    goto :goto_0

    .line 807980
    :cond_4
    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1, v3, v4, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    goto :goto_1
.end method

.method public abstract getInflatedLayout()Landroid/view/View;
.end method

.method public getInjector()LX/0QA;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 807963
    iget-object v0, p0, LX/4nv;->c:LX/0QA;

    return-object v0
.end method
