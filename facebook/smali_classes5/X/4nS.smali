.class public LX/4nS;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/2hV;

.field public final c:LX/4nR;

.field public d:Landroid/view/View;

.field public e:Landroid/widget/PopupWindow;

.field public f:I

.field public g:I

.field private final h:Ljava/lang/Runnable;

.field public i:Landroid/view/View;

.field public final j:Landroid/view/View$OnTouchListener;

.field public final k:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 806804
    const-class v0, LX/4nS;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/4nS;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/2hV;LX/4nR;)V
    .locals 1

    .prologue
    .line 806805
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 806806
    const/16 v0, 0xbb8

    iput v0, p0, LX/4nS;->f:I

    .line 806807
    const v0, 0x1030004

    iput v0, p0, LX/4nS;->g:I

    .line 806808
    new-instance v0, Lcom/facebook/ui/toaster/ClickableToast$1;

    invoke-direct {v0, p0}, Lcom/facebook/ui/toaster/ClickableToast$1;-><init>(LX/4nS;)V

    iput-object v0, p0, LX/4nS;->h:Ljava/lang/Runnable;

    .line 806809
    new-instance v0, LX/4nO;

    invoke-direct {v0, p0}, LX/4nO;-><init>(LX/4nS;)V

    iput-object v0, p0, LX/4nS;->j:Landroid/view/View$OnTouchListener;

    .line 806810
    new-instance v0, LX/4nP;

    invoke-direct {v0, p0}, LX/4nP;-><init>(LX/4nS;)V

    iput-object v0, p0, LX/4nS;->k:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 806811
    iput-object p1, p0, LX/4nS;->b:LX/2hV;

    .line 806812
    iput-object p2, p0, LX/4nS;->c:LX/4nR;

    .line 806813
    return-void
.end method

.method public static e(LX/4nS;)V
    .locals 4

    .prologue
    .line 806814
    iget-object v0, p0, LX/4nS;->d:Landroid/view/View;

    iget-object v1, p0, LX/4nS;->h:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 806815
    iget v0, p0, LX/4nS;->f:I

    const/4 v1, -0x2

    if-eq v0, v1, :cond_0

    .line 806816
    iget-object v0, p0, LX/4nS;->d:Landroid/view/View;

    iget-object v1, p0, LX/4nS;->h:Ljava/lang/Runnable;

    iget v2, p0, LX/4nS;->f:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 806817
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 806818
    iget-object v0, p0, LX/4nS;->b:LX/2hV;

    .line 806819
    iget-object v1, v0, LX/2hV;->a:Ljava/util/Queue;

    invoke-interface {v1, p0}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    .line 806820
    iget-boolean v1, v0, LX/2hV;->b:Z

    if-nez v1, :cond_0

    .line 806821
    invoke-static {v0}, LX/2hV;->a(LX/2hV;)V

    .line 806822
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 806823
    iget-object v0, p0, LX/4nS;->b:LX/2hV;

    .line 806824
    iget-object v1, v0, LX/2hV;->c:Landroid/os/Handler;

    new-instance v2, Lcom/facebook/ui/toaster/ClickableToastCoordinator$1;

    invoke-direct {v2, v0, p0}, Lcom/facebook/ui/toaster/ClickableToastCoordinator$1;-><init>(LX/2hV;LX/4nS;)V

    const v3, 0x4e3d4aa0    # 7.9394611E8f

    invoke-static {v1, v2, v3}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 806825
    iget-object v1, v0, LX/2hV;->a:Ljava/util/Queue;

    invoke-interface {v1, p0}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    .line 806826
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/2hV;->b:Z

    .line 806827
    invoke-static {v0}, LX/2hV;->a(LX/2hV;)V

    .line 806828
    return-void
.end method

.method public final c()V
    .locals 6

    .prologue
    const/4 v4, -0x1

    .line 806829
    iget-object v0, p0, LX/4nS;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Landroid/app/Activity;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 806830
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 806831
    :cond_0
    :goto_0
    return-void

    .line 806832
    :cond_1
    iget-object v1, p0, LX/4nS;->i:Landroid/view/View;

    if-nez v1, :cond_4

    .line 806833
    const v1, 0x1020002

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 806834
    if-eqz v1, :cond_3

    .line 806835
    :goto_1
    move-object v1, v1

    .line 806836
    iget-object v0, p0, LX/4nS;->e:Landroid/widget/PopupWindow;

    if-nez v0, :cond_2

    .line 806837
    const/4 v3, 0x0

    .line 806838
    iget-object v0, p0, LX/4nS;->d:Landroid/view/View;

    invoke-static {v3, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-static {v3, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/view/View;->measure(II)V

    .line 806839
    new-instance v0, Landroid/widget/PopupWindow;

    iget-object v2, p0, LX/4nS;->d:Landroid/view/View;

    iget-object v3, p0, LX/4nS;->c:LX/4nR;

    invoke-virtual {v3}, LX/4nR;->getWidth()I

    move-result v3

    iget-object v4, p0, LX/4nS;->c:LX/4nR;

    invoke-virtual {v4}, LX/4nR;->getHeight()I

    move-result v4

    invoke-direct {v0, v2, v3, v4}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;II)V

    iput-object v0, p0, LX/4nS;->e:Landroid/widget/PopupWindow;

    .line 806840
    iget-object v0, p0, LX/4nS;->e:Landroid/widget/PopupWindow;

    iget v2, p0, LX/4nS;->g:I

    invoke-virtual {v0, v2}, Landroid/widget/PopupWindow;->setAnimationStyle(I)V

    .line 806841
    iget-object v0, p0, LX/4nS;->d:Landroid/view/View;

    iget-object v2, p0, LX/4nS;->j:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 806842
    iget-object v0, p0, LX/4nS;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v2, p0, LX/4nS;->c:LX/4nR;

    invoke-virtual {v2}, LX/4nR;->getYOffsetResId()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 806843
    new-instance v2, Lcom/facebook/ui/toaster/ClickableToast$4;

    invoke-direct {v2, p0, v1, v0}, Lcom/facebook/ui/toaster/ClickableToast$4;-><init>(LX/4nS;Landroid/view/View;I)V

    invoke-virtual {v1, v2}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 806844
    iget-object v0, p0, LX/4nS;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v2, p0, LX/4nS;->k:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v2}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 806845
    invoke-static {p0}, LX/4nS;->e(LX/4nS;)V

    goto :goto_0

    .line 806846
    :cond_2
    sget-object v0, LX/4nQ;->a:[I

    iget-object v2, p0, LX/4nS;->c:LX/4nR;

    invoke-virtual {v2}, LX/4nR;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 806847
    :pswitch_0
    iget-object v0, p0, LX/4nS;->e:Landroid/widget/PopupWindow;

    iget-object v1, p0, LX/4nS;->c:LX/4nR;

    invoke-virtual {v1}, LX/4nR;->getWidth()I

    move-result v1

    iget-object v2, p0, LX/4nS;->c:LX/4nR;

    invoke-virtual {v2}, LX/4nR;->getHeight()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/PopupWindow;->update(II)V

    goto/16 :goto_0

    .line 806848
    :pswitch_1
    iget-object v0, p0, LX/4nS;->e:Landroid/widget/PopupWindow;

    const/4 v2, 0x0

    iget-object v3, p0, LX/4nS;->d:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    neg-int v3, v3

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/widget/PopupWindow;->update(Landroid/view/View;IIII)V

    goto/16 :goto_0

    .line 806849
    :cond_3
    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    goto/16 :goto_1

    .line 806850
    :cond_4
    iget-object v1, p0, LX/4nS;->i:Landroid/view/View;

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
