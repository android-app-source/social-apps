.class public final LX/52D;
.super LX/0SS;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 825888
    invoke-direct {p0}, LX/0SS;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0SU;LX/0SU;)V
    .locals 0

    .prologue
    .line 825886
    iput-object p2, p1, LX/0SU;->c:LX/0SU;

    .line 825887
    return-void
.end method

.method public final a(LX/0SU;Ljava/lang/Thread;)V
    .locals 0

    .prologue
    .line 825884
    iput-object p2, p1, LX/0SU;->b:Ljava/lang/Thread;

    .line 825885
    return-void
.end method

.method public final a(LX/0SQ;LX/0SU;LX/0SU;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0SQ",
            "<*>;",
            "LX/0SU;",
            "LX/0SU;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 825878
    monitor-enter p1

    .line 825879
    :try_start_0
    iget-object v0, p1, LX/0SQ;->waiters:LX/0SU;

    if-ne v0, p2, :cond_0

    .line 825880
    iput-object p3, p1, LX/0SQ;->waiters:LX/0SU;

    .line 825881
    const/4 v0, 0x1

    monitor-exit p1

    .line 825882
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    monitor-exit p1

    goto :goto_0

    .line 825883
    :catchall_0
    move-exception v0

    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(LX/0SQ;LX/0SV;LX/0SV;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0SQ",
            "<*>;",
            "LX/0SV;",
            "LX/0SV;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 825866
    monitor-enter p1

    .line 825867
    :try_start_0
    iget-object v0, p1, LX/0SQ;->listeners:LX/0SV;

    if-ne v0, p2, :cond_0

    .line 825868
    iput-object p3, p1, LX/0SQ;->listeners:LX/0SV;

    .line 825869
    const/4 v0, 0x1

    monitor-exit p1

    .line 825870
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    monitor-exit p1

    goto :goto_0

    .line 825871
    :catchall_0
    move-exception v0

    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(LX/0SQ;Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0SQ",
            "<*>;",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 825872
    monitor-enter p1

    .line 825873
    :try_start_0
    iget-object v0, p1, LX/0SQ;->value:Ljava/lang/Object;

    if-ne v0, p2, :cond_0

    .line 825874
    iput-object p3, p1, LX/0SQ;->value:Ljava/lang/Object;

    .line 825875
    const/4 v0, 0x1

    monitor-exit p1

    .line 825876
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    monitor-exit p1

    goto :goto_0

    .line 825877
    :catchall_0
    move-exception v0

    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
