.class public final LX/4XB;
.super LX/0ur;
.source ""


# instance fields
.field public A:J

.field public B:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public C:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public D:Z

.field public E:Lcom/facebook/graphql/model/GraphQLPlace;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public F:Lcom/facebook/graphql/model/GraphQLPhotoFaceBoxesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public G:Lcom/facebook/graphql/model/GraphQLFeedback;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public H:Lcom/facebook/graphql/model/GraphQLVect2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public I:D

.field public J:Lcom/facebook/graphql/model/GraphQLVideoGuidedTour;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public K:Z

.field public L:Z

.field public M:Z

.field public N:I

.field public O:I

.field public P:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public R:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public S:I

.field public T:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public U:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public V:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public W:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public X:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Y:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Z:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aA:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aB:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aC:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aD:Lcom/facebook/graphql/model/GraphQLAlbum;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aE:I

.field public aF:I

.field public aG:Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aH:I

.field public aI:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aJ:I

.field public aK:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aL:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aM:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLImage;",
            ">;"
        }
    .end annotation
.end field

.field public aN:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aO:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aP:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aQ:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aR:D

.field public aS:Lcom/facebook/graphql/model/GraphQLActor;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aT:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aU:Lcom/facebook/graphql/model/GraphQLVideo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aV:Lcom/facebook/graphql/model/GraphQLPlace;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aW:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPhotoEncoding;",
            ">;"
        }
    .end annotation
.end field

.field public aX:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aY:I

.field public aZ:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aa:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ab:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ac:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ad:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ae:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public af:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ag:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ah:I

.field public ai:I

.field public aj:I

.field public ak:Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public al:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLInstreamVideoAdBreak;",
            ">;"
        }
    .end annotation
.end field

.field public am:Z

.field public an:Z

.field public ao:Z

.field public ap:Z

.field public aq:Z

.field public ar:Z

.field public as:Z

.field public at:Z

.field public au:Z

.field public av:Z

.field public aw:Z

.field public ax:Z

.field public ay:Z

.field public az:Z

.field public b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bA:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bB:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bC:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bD:Lcom/facebook/graphql/model/GraphQLStreamingImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bE:Lcom/facebook/graphql/model/GraphQLStreamingImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bF:Z

.field public bG:Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bH:I

.field public bI:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public bJ:Lcom/facebook/graphql/model/GraphQLVideoChannel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bK:I

.field public bL:Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bM:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

.field public bN:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bO:I

.field public bP:Lcom/facebook/graphql/model/GraphQLWithTagsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bQ:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ba:I

.field public bb:I

.field public bc:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bd:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public be:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bf:I

.field public bg:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bh:Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bi:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bj:Lcom/facebook/graphql/model/GraphQLImageOverlay;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bk:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bl:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bm:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bn:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bo:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bp:Z

.field public bq:Z

.field public br:Z

.field public bs:Z

.field public bt:Z

.field public bu:Z

.field public bv:D

.field public bw:D

.field public bx:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public by:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bz:I

.field public c:Lcom/facebook/graphql/model/GraphQLAlbum;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:I

.field public f:Lcom/facebook/graphql/model/GraphQLApplication;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/enums/GraphQLAudioAvailability;

.field public i:J

.field public j:I

.field public k:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

.field public l:Z

.field public m:Z

.field public n:Z

.field public o:Z

.field public p:Z

.field public q:Z

.field public r:Z

.field public s:Z

.field public t:Z

.field public u:Z

.field public v:Z

.field public w:Z

.field public x:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:Lcom/facebook/graphql/model/GraphQLCopyrightBlockInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 765100
    invoke-direct {p0}, LX/0ur;-><init>()V

    .line 765101
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAudioAvailability;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAudioAvailability;

    iput-object v0, p0, LX/4XB;->h:Lcom/facebook/graphql/enums/GraphQLAudioAvailability;

    .line 765102
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    iput-object v0, p0, LX/4XB;->k:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 765103
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    iput-object v0, p0, LX/4XB;->bM:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    .line 765104
    const/4 v0, 0x0

    iput-object v0, p0, LX/4XB;->bQ:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 765105
    instance-of v0, p0, LX/4XB;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 765106
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLMedia;)LX/4XB;
    .locals 4

    .prologue
    .line 765107
    new-instance v0, LX/4XB;

    invoke-direct {v0}, LX/4XB;-><init>()V

    .line 765108
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 765109
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->k()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->b:Ljava/lang/String;

    .line 765110
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->l()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->c:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 765111
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->d:Lcom/facebook/graphql/model/GraphQLImage;

    .line 765112
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->n()I

    move-result v1

    iput v1, v0, LX/4XB;->e:I

    .line 765113
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->o()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->f:Lcom/facebook/graphql/model/GraphQLApplication;

    .line 765114
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->p()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->g:Ljava/lang/String;

    .line 765115
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bJ()Lcom/facebook/graphql/enums/GraphQLAudioAvailability;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->h:Lcom/facebook/graphql/enums/GraphQLAudioAvailability;

    .line 765116
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->q()J

    move-result-wide v2

    iput-wide v2, v0, LX/4XB;->i:J

    .line 765117
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->r()I

    move-result v1

    iput v1, v0, LX/4XB;->j:I

    .line 765118
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->s()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->k:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 765119
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->t()Z

    move-result v1

    iput-boolean v1, v0, LX/4XB;->l:Z

    .line 765120
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->u()Z

    move-result v1

    iput-boolean v1, v0, LX/4XB;->m:Z

    .line 765121
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bP()Z

    move-result v1

    iput-boolean v1, v0, LX/4XB;->n:Z

    .line 765122
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->v()Z

    move-result v1

    iput-boolean v1, v0, LX/4XB;->o:Z

    .line 765123
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->w()Z

    move-result v1

    iput-boolean v1, v0, LX/4XB;->p:Z

    .line 765124
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->x()Z

    move-result v1

    iput-boolean v1, v0, LX/4XB;->q:Z

    .line 765125
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->y()Z

    move-result v1

    iput-boolean v1, v0, LX/4XB;->r:Z

    .line 765126
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->z()Z

    move-result v1

    iput-boolean v1, v0, LX/4XB;->s:Z

    .line 765127
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->A()Z

    move-result v1

    iput-boolean v1, v0, LX/4XB;->t:Z

    .line 765128
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bW()Z

    move-result v1

    iput-boolean v1, v0, LX/4XB;->u:Z

    .line 765129
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->B()Z

    move-result v1

    iput-boolean v1, v0, LX/4XB;->v:Z

    .line 765130
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->C()Z

    move-result v1

    iput-boolean v1, v0, LX/4XB;->w:Z

    .line 765131
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->D()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->x:Ljava/lang/String;

    .line 765132
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->E()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->y:Lcom/facebook/graphql/model/GraphQLStory;

    .line 765133
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bL()Lcom/facebook/graphql/model/GraphQLCopyrightBlockInfo;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->z:Lcom/facebook/graphql/model/GraphQLCopyrightBlockInfo;

    .line 765134
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->F()J

    move-result-wide v2

    iput-wide v2, v0, LX/4XB;->A:J

    .line 765135
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->G()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->B:Lcom/facebook/graphql/model/GraphQLStory;

    .line 765136
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bD()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->C:Ljava/lang/String;

    .line 765137
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bM()Z

    move-result v1

    iput-boolean v1, v0, LX/4XB;->D:Z

    .line 765138
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->H()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->E:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 765139
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->I()Lcom/facebook/graphql/model/GraphQLPhotoFaceBoxesConnection;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->F:Lcom/facebook/graphql/model/GraphQLPhotoFaceBoxesConnection;

    .line 765140
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->J()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->G:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 765141
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->K()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->H:Lcom/facebook/graphql/model/GraphQLVect2;

    .line 765142
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bN()D

    move-result-wide v2

    iput-wide v2, v0, LX/4XB;->I:D

    .line 765143
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->L()Lcom/facebook/graphql/model/GraphQLVideoGuidedTour;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->J:Lcom/facebook/graphql/model/GraphQLVideoGuidedTour;

    .line 765144
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->M()Z

    move-result v1

    iput-boolean v1, v0, LX/4XB;->K:Z

    .line 765145
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->N()Z

    move-result v1

    iput-boolean v1, v0, LX/4XB;->L:Z

    .line 765146
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->O()Z

    move-result v1

    iput-boolean v1, v0, LX/4XB;->M:Z

    .line 765147
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->P()I

    move-result v1

    iput v1, v0, LX/4XB;->N:I

    .line 765148
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->Q()I

    move-result v1

    iput v1, v0, LX/4XB;->O:I

    .line 765149
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bS()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->P:Ljava/lang/String;

    .line 765150
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bT()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->Q:Ljava/lang/String;

    .line 765151
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->R()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->R:Ljava/lang/String;

    .line 765152
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->S()I

    move-result v1

    iput v1, v0, LX/4XB;->S:I

    .line 765153
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->T:Ljava/lang/String;

    .line 765154
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->U:Lcom/facebook/graphql/model/GraphQLImage;

    .line 765155
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->V()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->V:Lcom/facebook/graphql/model/GraphQLImage;

    .line 765156
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->W()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->W:Lcom/facebook/graphql/model/GraphQLImage;

    .line 765157
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->X()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->X:Lcom/facebook/graphql/model/GraphQLImage;

    .line 765158
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->Y()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->Y:Lcom/facebook/graphql/model/GraphQLImage;

    .line 765159
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->Z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->Z:Lcom/facebook/graphql/model/GraphQLImage;

    .line 765160
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bF()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->aa:Lcom/facebook/graphql/model/GraphQLImage;

    .line 765161
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bG()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->ab:Lcom/facebook/graphql/model/GraphQLImage;

    .line 765162
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aa()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->ac:Lcom/facebook/graphql/model/GraphQLImage;

    .line 765163
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ab()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->ad:Lcom/facebook/graphql/model/GraphQLImage;

    .line 765164
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ac()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->ae:Lcom/facebook/graphql/model/GraphQLImage;

    .line 765165
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ad()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->af:Lcom/facebook/graphql/model/GraphQLImage;

    .line 765166
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ae()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->ag:Lcom/facebook/graphql/model/GraphQLImage;

    .line 765167
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->af()I

    move-result v1

    iput v1, v0, LX/4XB;->ah:I

    .line 765168
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ag()I

    move-result v1

    iput v1, v0, LX/4XB;->ai:I

    .line 765169
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ah()I

    move-result v1

    iput v1, v0, LX/4XB;->aj:I

    .line 765170
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ai()Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->ak:Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    .line 765171
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aj()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->al:LX/0Px;

    .line 765172
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ak()Z

    move-result v1

    iput-boolean v1, v0, LX/4XB;->am:Z

    .line 765173
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->al()Z

    move-result v1

    iput-boolean v1, v0, LX/4XB;->an:Z

    .line 765174
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->am()Z

    move-result v1

    iput-boolean v1, v0, LX/4XB;->ao:Z

    .line 765175
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->an()Z

    move-result v1

    iput-boolean v1, v0, LX/4XB;->ap:Z

    .line 765176
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bQ()Z

    move-result v1

    iput-boolean v1, v0, LX/4XB;->aq:Z

    .line 765177
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ao()Z

    move-result v1

    iput-boolean v1, v0, LX/4XB;->ar:Z

    .line 765178
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ap()Z

    move-result v1

    iput-boolean v1, v0, LX/4XB;->as:Z

    .line 765179
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aq()Z

    move-result v1

    iput-boolean v1, v0, LX/4XB;->at:Z

    .line 765180
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bY()Z

    move-result v1

    iput-boolean v1, v0, LX/4XB;->au:Z

    .line 765181
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ar()Z

    move-result v1

    iput-boolean v1, v0, LX/4XB;->av:Z

    .line 765182
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->as()Z

    move-result v1

    iput-boolean v1, v0, LX/4XB;->aw:Z

    .line 765183
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->at()Z

    move-result v1

    iput-boolean v1, v0, LX/4XB;->ax:Z

    .line 765184
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->au()Z

    move-result v1

    iput-boolean v1, v0, LX/4XB;->ay:Z

    .line 765185
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->av()Z

    move-result v1

    iput-boolean v1, v0, LX/4XB;->az:Z

    .line 765186
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aw()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->aA:Lcom/facebook/graphql/model/GraphQLImage;

    .line 765187
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ax()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->aB:Lcom/facebook/graphql/model/GraphQLImage;

    .line 765188
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ay()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->aC:Lcom/facebook/graphql/model/GraphQLImage;

    .line 765189
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bE()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->aD:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 765190
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bK()I

    move-result v1

    iput v1, v0, LX/4XB;->aE:I

    .line 765191
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->az()I

    move-result v1

    iput v1, v0, LX/4XB;->aF:I

    .line 765192
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aA()Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->aG:Lcom/facebook/graphql/model/GraphQLPlaceSuggestionInfo;

    .line 765193
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aB()I

    move-result v1

    iput v1, v0, LX/4XB;->aH:I

    .line 765194
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aC()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->aI:Lcom/facebook/graphql/model/GraphQLImage;

    .line 765195
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bX()I

    move-result v1

    iput v1, v0, LX/4XB;->aJ:I

    .line 765196
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aD()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->aK:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 765197
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aE()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->aL:Ljava/lang/String;

    .line 765198
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aF()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->aM:LX/0Px;

    .line 765199
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aG()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->aN:Lcom/facebook/graphql/model/GraphQLImage;

    .line 765200
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aH()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->aO:Ljava/lang/String;

    .line 765201
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aI()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->aP:Lcom/facebook/graphql/model/GraphQLImage;

    .line 765202
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aJ()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->aQ:Lcom/facebook/graphql/model/GraphQLImage;

    .line 765203
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bO()D

    move-result-wide v2

    iput-wide v2, v0, LX/4XB;->aR:D

    .line 765204
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aK()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->aS:Lcom/facebook/graphql/model/GraphQLActor;

    .line 765205
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bH()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->aT:Ljava/lang/String;

    .line 765206
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aL()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->aU:Lcom/facebook/graphql/model/GraphQLVideo;

    .line 765207
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aM()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->aV:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 765208
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aN()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->aW:LX/0Px;

    .line 765209
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bI()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->aX:Ljava/lang/String;

    .line 765210
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aO()I

    move-result v1

    iput v1, v0, LX/4XB;->aY:I

    .line 765211
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aP()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->aZ:Ljava/lang/String;

    .line 765212
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aQ()I

    move-result v1

    iput v1, v0, LX/4XB;->ba:I

    .line 765213
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aR()I

    move-result v1

    iput v1, v0, LX/4XB;->bb:I

    .line 765214
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aS()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->bc:Ljava/lang/String;

    .line 765215
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aT()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->bd:Ljava/lang/String;

    .line 765216
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aU()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->be:Lcom/facebook/graphql/model/GraphQLImage;

    .line 765217
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aV()I

    move-result v1

    iput v1, v0, LX/4XB;->bf:I

    .line 765218
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aW()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->bg:Ljava/lang/String;

    .line 765219
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aX()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->bh:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 765220
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bR()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->bi:Ljava/lang/String;

    .line 765221
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aY()Lcom/facebook/graphql/model/GraphQLImageOverlay;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->bj:Lcom/facebook/graphql/model/GraphQLImageOverlay;

    .line 765222
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aZ()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->bk:Ljava/lang/String;

    .line 765223
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->ba()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->bl:Ljava/lang/String;

    .line 765224
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bb()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->bm:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 765225
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bc()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->bn:Lcom/facebook/graphql/model/GraphQLImage;

    .line 765226
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bU()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->bo:Ljava/lang/String;

    .line 765227
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bd()Z

    move-result v1

    iput-boolean v1, v0, LX/4XB;->bp:Z

    .line 765228
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->be()Z

    move-result v1

    iput-boolean v1, v0, LX/4XB;->bq:Z

    .line 765229
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bf()Z

    move-result v1

    iput-boolean v1, v0, LX/4XB;->br:Z

    .line 765230
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bg()Z

    move-result v1

    iput-boolean v1, v0, LX/4XB;->bs:Z

    .line 765231
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bh()Z

    move-result v1

    iput-boolean v1, v0, LX/4XB;->bt:Z

    .line 765232
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bi()Z

    move-result v1

    iput-boolean v1, v0, LX/4XB;->bu:Z

    .line 765233
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bj()D

    move-result-wide v2

    iput-wide v2, v0, LX/4XB;->bv:D

    .line 765234
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bk()D

    move-result-wide v2

    iput-wide v2, v0, LX/4XB;->bw:D

    .line 765235
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bl()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->bx:Ljava/lang/String;

    .line 765236
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bm()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->by:Ljava/lang/String;

    .line 765237
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bn()I

    move-result v1

    iput v1, v0, LX/4XB;->bz:I

    .line 765238
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bV()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->bA:Ljava/lang/String;

    .line 765239
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bo()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->bB:Lcom/facebook/graphql/model/GraphQLPage;

    .line 765240
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bp()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->bC:Lcom/facebook/graphql/model/GraphQLImage;

    .line 765241
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bq()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->bD:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    .line 765242
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->br()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->bE:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    .line 765243
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bs()Z

    move-result v1

    iput-boolean v1, v0, LX/4XB;->bF:Z

    .line 765244
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bt()Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->bG:Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;

    .line 765245
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bu()I

    move-result v1

    iput v1, v0, LX/4XB;->bH:I

    .line 765246
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bv()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->bI:LX/0Px;

    .line 765247
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bw()Lcom/facebook/graphql/model/GraphQLVideoChannel;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->bJ:Lcom/facebook/graphql/model/GraphQLVideoChannel;

    .line 765248
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bx()I

    move-result v1

    iput v1, v0, LX/4XB;->bK:I

    .line 765249
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->by()Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->bL:Lcom/facebook/graphql/model/GraphQLVideoSocialContextInfo;

    .line 765250
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bz()Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->bM:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    .line 765251
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bA()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->bN:Lcom/facebook/graphql/model/GraphQLImage;

    .line 765252
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bB()I

    move-result v1

    iput v1, v0, LX/4XB;->bO:I

    .line 765253
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->bC()Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->bP:Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    .line 765254
    invoke-static {v0, p0}, LX/0ur;->a(LX/0ur;Lcom/facebook/graphql/modelutil/BaseModel;)V

    .line 765255
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    iput-object v1, v0, LX/4XB;->bQ:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 765256
    return-object v0
.end method


# virtual methods
.method public final a()Lcom/facebook/graphql/model/GraphQLMedia;
    .locals 2

    .prologue
    .line 765257
    new-instance v0, Lcom/facebook/graphql/model/GraphQLMedia;

    invoke-direct {v0, p0}, Lcom/facebook/graphql/model/GraphQLMedia;-><init>(LX/4XB;)V

    .line 765258
    return-object v0
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLImage;)LX/4XB;
    .locals 0
    .param p1    # Lcom/facebook/graphql/model/GraphQLImage;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 765259
    iput-object p1, p0, LX/4XB;->U:Lcom/facebook/graphql/model/GraphQLImage;

    .line 765260
    return-object p0
.end method
