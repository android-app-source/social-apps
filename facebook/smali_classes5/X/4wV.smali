.class public final LX/4wV;
.super LX/0SY;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LX/0SY",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field public volatile a:J

.field public b:LX/0R1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0R1",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public c:LX/0R1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0R1",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;ILX/0R1;)V
    .locals 2
    .param p4    # LX/0R1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/ReferenceQueue",
            "<TK;>;TK;I",
            "LX/0R1",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 820202
    invoke-direct {p0, p1, p2, p3, p4}, LX/0SY;-><init>(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;ILX/0R1;)V

    .line 820203
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, LX/4wV;->a:J

    .line 820204
    sget-object v0, LX/0SZ;->INSTANCE:LX/0SZ;

    move-object v0, v0

    .line 820205
    iput-object v0, p0, LX/4wV;->b:LX/0R1;

    .line 820206
    sget-object v0, LX/0SZ;->INSTANCE:LX/0SZ;

    move-object v0, v0

    .line 820207
    iput-object v0, p0, LX/4wV;->c:LX/0R1;

    .line 820208
    return-void
.end method


# virtual methods
.method public final getNextInWriteQueue()LX/0R1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0R1",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 820217
    iget-object v0, p0, LX/4wV;->b:LX/0R1;

    return-object v0
.end method

.method public final getPreviousInWriteQueue()LX/0R1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0R1",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 820215
    iget-object v0, p0, LX/4wV;->c:LX/0R1;

    return-object v0
.end method

.method public final getWriteTime()J
    .locals 2

    .prologue
    .line 820216
    iget-wide v0, p0, LX/4wV;->a:J

    return-wide v0
.end method

.method public final setNextInWriteQueue(LX/0R1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0R1",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 820213
    iput-object p1, p0, LX/4wV;->b:LX/0R1;

    .line 820214
    return-void
.end method

.method public final setPreviousInWriteQueue(LX/0R1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0R1",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 820211
    iput-object p1, p0, LX/4wV;->c:LX/0R1;

    .line 820212
    return-void
.end method

.method public final setWriteTime(J)V
    .locals 1

    .prologue
    .line 820209
    iput-wide p1, p0, LX/4wV;->a:J

    .line 820210
    return-void
.end method
