.class public abstract LX/3uv;
.super LX/3uu;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "LX/3uu",
        "<TT;>;"
    }
.end annotation


# instance fields
.field public final a:Landroid/content/Context;

.field public c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/3qv;",
            "Landroid/view/MenuItem;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/3qw;",
            "Landroid/view/SubMenu;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "TT;)V"
        }
    .end annotation

    .prologue
    .line 649735
    invoke-direct {p0, p2}, LX/3uu;-><init>(Ljava/lang/Object;)V

    .line 649736
    iput-object p1, p0, LX/3uv;->a:Landroid/content/Context;

    .line 649737
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/MenuItem;)Landroid/view/MenuItem;
    .locals 3

    .prologue
    .line 649747
    instance-of v0, p1, LX/3qv;

    if-eqz v0, :cond_2

    move-object v0, p1

    .line 649748
    check-cast v0, LX/3qv;

    .line 649749
    iget-object v1, p0, LX/3uv;->c:Ljava/util/Map;

    if-nez v1, :cond_0

    .line 649750
    new-instance v1, LX/026;

    invoke-direct {v1}, LX/026;-><init>()V

    iput-object v1, p0, LX/3uv;->c:Ljava/util/Map;

    .line 649751
    :cond_0
    iget-object v1, p0, LX/3uv;->c:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/MenuItem;

    .line 649752
    if-nez v1, :cond_1

    .line 649753
    iget-object v1, p0, LX/3uv;->a:Landroid/content/Context;

    invoke-static {v1, v0}, LX/3vE;->a(Landroid/content/Context;LX/3qv;)Landroid/view/MenuItem;

    move-result-object v1

    .line 649754
    iget-object v2, p0, LX/3uv;->c:Ljava/util/Map;

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 649755
    :cond_1
    :goto_0
    return-object v1

    :cond_2
    move-object v1, p1

    goto :goto_0
.end method

.method public final a(Landroid/view/SubMenu;)Landroid/view/SubMenu;
    .locals 2

    .prologue
    .line 649738
    instance-of v0, p1, LX/3qw;

    if-eqz v0, :cond_2

    .line 649739
    check-cast p1, LX/3qw;

    .line 649740
    iget-object v0, p0, LX/3uv;->d:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 649741
    new-instance v0, LX/026;

    invoke-direct {v0}, LX/026;-><init>()V

    iput-object v0, p0, LX/3uv;->d:Ljava/util/Map;

    .line 649742
    :cond_0
    iget-object v0, p0, LX/3uv;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/SubMenu;

    .line 649743
    if-nez v0, :cond_1

    .line 649744
    iget-object v0, p0, LX/3uv;->a:Landroid/content/Context;

    invoke-static {v0, p1}, LX/3vE;->a(Landroid/content/Context;LX/3qw;)Landroid/view/SubMenu;

    move-result-object v0

    .line 649745
    iget-object v1, p0, LX/3uv;->d:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 649746
    :cond_1
    :goto_0
    return-object v0

    :cond_2
    move-object v0, p1

    goto :goto_0
.end method
