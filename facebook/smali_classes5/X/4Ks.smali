.class public LX/4Ks;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 681027
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 25

    .prologue
    .line 681028
    const/16 v21, 0x0

    .line 681029
    const/16 v20, 0x0

    .line 681030
    const/16 v19, 0x0

    .line 681031
    const/16 v18, 0x0

    .line 681032
    const/16 v17, 0x0

    .line 681033
    const/16 v16, 0x0

    .line 681034
    const/4 v15, 0x0

    .line 681035
    const/4 v14, 0x0

    .line 681036
    const/4 v13, 0x0

    .line 681037
    const/4 v12, 0x0

    .line 681038
    const/4 v11, 0x0

    .line 681039
    const/4 v10, 0x0

    .line 681040
    const/4 v9, 0x0

    .line 681041
    const/4 v8, 0x0

    .line 681042
    const/4 v7, 0x0

    .line 681043
    const/4 v6, 0x0

    .line 681044
    const/4 v5, 0x0

    .line 681045
    const/4 v4, 0x0

    .line 681046
    const/4 v3, 0x0

    .line 681047
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v22

    sget-object v23, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    if-eq v0, v1, :cond_1

    .line 681048
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 681049
    const/4 v3, 0x0

    .line 681050
    :goto_0
    return v3

    .line 681051
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 681052
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v22

    sget-object v23, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    if-eq v0, v1, :cond_11

    .line 681053
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v22

    .line 681054
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 681055
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v23

    sget-object v24, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    if-eq v0, v1, :cond_1

    if-eqz v22, :cond_1

    .line 681056
    const-string v23, "app_store_identifier"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_2

    .line 681057
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v21

    goto :goto_1

    .line 681058
    :cond_2
    const-string v23, "artifact_size_description"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_3

    .line 681059
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v20

    goto :goto_1

    .line 681060
    :cond_3
    const-string v23, "banner_screenshots"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_4

    .line 681061
    invoke-static/range {p0 .. p1}, LX/2ax;->b(LX/15w;LX/186;)I

    move-result v19

    goto :goto_1

    .line 681062
    :cond_4
    const-string v23, "description"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_5

    .line 681063
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v18

    goto :goto_1

    .line 681064
    :cond_5
    const-string v23, "download_connectivity_policy"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_6

    .line 681065
    const/4 v5, 0x1

    .line 681066
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/facebook/graphql/enums/GraphQLAppStoreDownloadConnectivityPolicy;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLAppStoreDownloadConnectivityPolicy;

    move-result-object v17

    goto :goto_1

    .line 681067
    :cond_6
    const-string v23, "install_id"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_7

    .line 681068
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    goto/16 :goto_1

    .line 681069
    :cond_7
    const-string v23, "install_state"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_8

    .line 681070
    const/4 v4, 0x1

    .line 681071
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    move-result-object v15

    goto/16 :goto_1

    .line 681072
    :cond_8
    const-string v23, "likes_context_sentence"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_9

    .line 681073
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v14

    goto/16 :goto_1

    .line 681074
    :cond_9
    const-string v23, "permissions"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_a

    .line 681075
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v13

    goto/16 :goto_1

    .line 681076
    :cond_a
    const-string v23, "phone_screenshots"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_b

    .line 681077
    invoke-static/range {p0 .. p1}, LX/2ax;->b(LX/15w;LX/186;)I

    move-result v12

    goto/16 :goto_1

    .line 681078
    :cond_b
    const-string v23, "platform_application"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_c

    .line 681079
    invoke-static/range {p0 .. p1}, LX/2uk;->a(LX/15w;LX/186;)I

    move-result v11

    goto/16 :goto_1

    .line 681080
    :cond_c
    const-string v23, "publisher"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_d

    .line 681081
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto/16 :goto_1

    .line 681082
    :cond_d
    const-string v23, "supported_app_stores"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_e

    .line 681083
    const-class v9, Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v9}, LX/2gu;->a(LX/15w;LX/186;Ljava/lang/Class;)I

    move-result v9

    goto/16 :goto_1

    .line 681084
    :cond_e
    const-string v23, "usage_context_sentence"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_f

    .line 681085
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v8

    goto/16 :goto_1

    .line 681086
    :cond_f
    const-string v23, "version_code"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_10

    .line 681087
    const/4 v3, 0x1

    .line 681088
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v7

    goto/16 :goto_1

    .line 681089
    :cond_10
    const-string v23, "version_name"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_0

    .line 681090
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_1

    .line 681091
    :cond_11
    const/16 v22, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 681092
    const/16 v22, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v22

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 681093
    const/16 v21, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v21

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 681094
    const/16 v20, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 681095
    const/16 v19, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 681096
    if-eqz v5, :cond_12

    .line 681097
    const/4 v5, 0x4

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v5, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 681098
    :cond_12
    const/4 v5, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 681099
    if-eqz v4, :cond_13

    .line 681100
    const/4 v4, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v15}, LX/186;->a(ILjava/lang/Enum;)V

    .line 681101
    :cond_13
    const/4 v4, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v14}, LX/186;->b(II)V

    .line 681102
    const/16 v4, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v13}, LX/186;->b(II)V

    .line 681103
    const/16 v4, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v12}, LX/186;->b(II)V

    .line 681104
    const/16 v4, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v11}, LX/186;->b(II)V

    .line 681105
    const/16 v4, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v10}, LX/186;->b(II)V

    .line 681106
    const/16 v4, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v9}, LX/186;->b(II)V

    .line 681107
    const/16 v4, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v8}, LX/186;->b(II)V

    .line 681108
    if-eqz v3, :cond_14

    .line 681109
    const/16 v3, 0xe

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7, v4}, LX/186;->a(III)V

    .line 681110
    :cond_14
    const/16 v3, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 681111
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 7

    .prologue
    const/16 v6, 0xc

    const/16 v5, 0x8

    const/4 v4, 0x6

    const/4 v3, 0x4

    const/4 v2, 0x0

    .line 681112
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 681113
    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 681114
    if-eqz v0, :cond_0

    .line 681115
    const-string v1, "app_store_identifier"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 681116
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 681117
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 681118
    if-eqz v0, :cond_1

    .line 681119
    const-string v1, "artifact_size_description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 681120
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 681121
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 681122
    if-eqz v0, :cond_2

    .line 681123
    const-string v1, "banner_screenshots"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 681124
    invoke-static {p0, v0, p2, p3}, LX/2ax;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 681125
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 681126
    if-eqz v0, :cond_3

    .line 681127
    const-string v1, "description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 681128
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 681129
    :cond_3
    invoke-virtual {p0, p1, v3, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 681130
    if-eqz v0, :cond_4

    .line 681131
    const-string v0, "download_connectivity_policy"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 681132
    const-class v0, Lcom/facebook/graphql/enums/GraphQLAppStoreDownloadConnectivityPolicy;

    invoke-virtual {p0, p1, v3, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLAppStoreDownloadConnectivityPolicy;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLAppStoreDownloadConnectivityPolicy;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 681133
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 681134
    if-eqz v0, :cond_5

    .line 681135
    const-string v1, "install_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 681136
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 681137
    :cond_5
    invoke-virtual {p0, p1, v4, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 681138
    if-eqz v0, :cond_6

    .line 681139
    const-string v0, "install_state"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 681140
    const-class v0, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    invoke-virtual {p0, p1, v4, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 681141
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 681142
    if-eqz v0, :cond_7

    .line 681143
    const-string v1, "likes_context_sentence"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 681144
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 681145
    :cond_7
    invoke-virtual {p0, p1, v5}, LX/15i;->g(II)I

    move-result v0

    .line 681146
    if-eqz v0, :cond_8

    .line 681147
    const-string v0, "permissions"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 681148
    invoke-virtual {p0, p1, v5}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 681149
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 681150
    if-eqz v0, :cond_9

    .line 681151
    const-string v1, "phone_screenshots"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 681152
    invoke-static {p0, v0, p2, p3}, LX/2ax;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 681153
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 681154
    if-eqz v0, :cond_a

    .line 681155
    const-string v1, "platform_application"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 681156
    invoke-static {p0, v0, p2, p3}, LX/2uk;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 681157
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 681158
    if-eqz v0, :cond_b

    .line 681159
    const-string v1, "publisher"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 681160
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 681161
    :cond_b
    invoke-virtual {p0, p1, v6}, LX/15i;->g(II)I

    move-result v0

    .line 681162
    if-eqz v0, :cond_c

    .line 681163
    const-string v0, "supported_app_stores"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 681164
    const-class v0, Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;

    invoke-virtual {p0, p1, v6, v0}, LX/15i;->b(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->b(Ljava/util/Iterator;LX/0nX;)V

    .line 681165
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 681166
    if-eqz v0, :cond_d

    .line 681167
    const-string v1, "usage_context_sentence"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 681168
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 681169
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 681170
    if-eqz v0, :cond_e

    .line 681171
    const-string v1, "version_code"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 681172
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 681173
    :cond_e
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 681174
    if-eqz v0, :cond_f

    .line 681175
    const-string v1, "version_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 681176
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 681177
    :cond_f
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 681178
    return-void
.end method
