.class public LX/4eI;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 797186
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 797187
    return-void
.end method

.method public static a(LX/4n9;Landroid/content/res/Resources;)LX/1bX;
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v4, -0x2

    .line 797159
    iget-object v1, p0, LX/4n9;->f:LX/4n5;

    move-object v2, v1

    .line 797160
    if-eqz v2, :cond_6

    if-eqz p1, :cond_6

    .line 797161
    iget v1, v2, LX/4n5;->c:I

    .line 797162
    iget v0, v2, LX/4n5;->d:I

    .line 797163
    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    .line 797164
    if-ne v1, v4, :cond_0

    .line 797165
    iget v1, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 797166
    :cond_0
    if-ne v0, v4, :cond_1

    .line 797167
    iget v0, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 797168
    :cond_1
    :goto_0
    iget-object v3, p0, LX/4n9;->a:Landroid/net/Uri;

    move-object v3, v3

    .line 797169
    invoke-static {v3}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v4

    .line 797170
    iget-boolean v5, p0, LX/4n9;->h:Z

    move v5, v5

    .line 797171
    if-eqz v5, :cond_2

    .line 797172
    sget-object v5, LX/1bY;->BITMAP_MEMORY_CACHE:LX/1bY;

    .line 797173
    iput-object v5, v4, LX/1bX;->b:LX/1bY;

    .line 797174
    :cond_2
    invoke-static {v3}, LX/1be;->a(Landroid/net/Uri;)Z

    move-result v5

    if-nez v5, :cond_3

    if-lez v1, :cond_3

    if-lez v0, :cond_3

    .line 797175
    new-instance v5, LX/1o9;

    invoke-direct {v5, v1, v0}, LX/1o9;-><init>(II)V

    .line 797176
    iput-object v5, v4, LX/1bX;->c:LX/1o9;

    .line 797177
    :cond_3
    invoke-static {v3}, LX/1be;->a(Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_4

    if-eqz v2, :cond_4

    .line 797178
    iget-boolean v0, v2, LX/4n5;->f:Z

    invoke-virtual {v4, v0}, LX/1bX;->a(Z)LX/1bX;

    .line 797179
    :cond_4
    invoke-static {}, LX/4eB;->newBuilder()LX/4eC;

    move-result-object v0

    .line 797180
    if-eqz v2, :cond_5

    .line 797181
    iget v1, v2, LX/4n5;->b:I

    .line 797182
    iput v1, v0, LX/4eC;->b:I

    .line 797183
    :cond_5
    invoke-virtual {v0}, LX/4eC;->l()LX/4eB;

    move-result-object v0

    .line 797184
    iput-object v0, v4, LX/1bX;->e:LX/1bZ;

    .line 797185
    return-object v4

    :cond_6
    move v1, v0

    goto :goto_0
.end method
