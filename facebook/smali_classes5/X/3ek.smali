.class public LX/3ek;
.super LX/3AP;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/3ek;


# instance fields
.field public a:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/3el;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/http/common/FbHttpRequestProcessor;LX/1Gm;LX/0Zb;LX/1Gk;LX/1Gl;LX/1Go;LX/13t;)V
    .locals 10
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 620788
    const-string v3, "sticker"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, LX/3AP;-><init>(Landroid/content/Context;Lcom/facebook/http/common/FbHttpRequestProcessor;Ljava/lang/String;LX/1Gm;LX/0Zb;LX/1Gk;LX/1Gl;LX/1Go;LX/13t;)V

    .line 620789
    return-void
.end method

.method public static a(LX/0QB;)LX/3ek;
    .locals 12

    .prologue
    .line 620790
    sget-object v0, LX/3ek;->c:LX/3ek;

    if-nez v0, :cond_1

    .line 620791
    const-class v1, LX/3ek;

    monitor-enter v1

    .line 620792
    :try_start_0
    sget-object v0, LX/3ek;->c:LX/3ek;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 620793
    if-eqz v2, :cond_0

    .line 620794
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 620795
    new-instance v3, LX/3ek;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, Lcom/facebook/http/common/FbHttpRequestProcessor;->a(LX/0QB;)Lcom/facebook/http/common/FbHttpRequestProcessor;

    move-result-object v5

    check-cast v5, Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-static {v0}, LX/1Gm;->a(LX/0QB;)LX/1Gm;

    move-result-object v6

    check-cast v6, LX/1Gm;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v7

    check-cast v7, LX/0Zb;

    invoke-static {v0}, LX/1Gk;->a(LX/0QB;)LX/1Gk;

    move-result-object v8

    check-cast v8, LX/1Gk;

    invoke-static {v0}, LX/1Gl;->a(LX/0QB;)LX/1Gl;

    move-result-object v9

    check-cast v9, LX/1Gl;

    invoke-static {v0}, LX/1Gn;->a(LX/0QB;)LX/1Gn;

    move-result-object v10

    check-cast v10, LX/1Go;

    invoke-static {v0}, LX/13t;->a(LX/0QB;)LX/13t;

    move-result-object v11

    check-cast v11, LX/13t;

    invoke-direct/range {v3 .. v11}, LX/3ek;-><init>(Landroid/content/Context;Lcom/facebook/http/common/FbHttpRequestProcessor;LX/1Gm;LX/0Zb;LX/1Gk;LX/1Gl;LX/1Go;LX/13t;)V

    .line 620796
    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-static {v0}, LX/3el;->a(LX/0QB;)LX/3el;

    move-result-object v5

    check-cast v5, LX/3el;

    .line 620797
    iput-object v4, v3, LX/3ek;->a:LX/0Uh;

    iput-object v5, v3, LX/3ek;->b:LX/3el;

    .line 620798
    move-object v0, v3

    .line 620799
    sput-object v0, LX/3ek;->c:LX/3ek;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 620800
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 620801
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 620802
    :cond_1
    sget-object v0, LX/3ek;->c:LX/3ek;

    return-object v0

    .line 620803
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 620804
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private d(LX/34X;)Ljava/lang/Object;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/34X",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 620805
    iget-object v0, p0, LX/3ek;->b:LX/3el;

    .line 620806
    iget-object v2, p1, LX/34X;->b:Landroid/net/Uri;

    move-object v2, v2

    .line 620807
    invoke-static {v2}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v2

    .line 620808
    iget-object v3, p1, LX/34X;->d:Lcom/facebook/common/callercontext/CallerContext;

    move-object v3, v3

    .line 620809
    invoke-virtual {v0, v2, v3}, LX/3el;->a(LX/1bX;Lcom/facebook/common/callercontext/CallerContext;)LX/1ca;

    move-result-object v3

    .line 620810
    invoke-static {v3}, LX/24r;->a(LX/1ca;)LX/24r;

    move-result-object v0

    .line 620811
    :try_start_0
    invoke-static {v0}, LX/0Sa;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FJ;

    .line 620812
    if-nez v0, :cond_1

    .line 620813
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "Failed to download image request. Network might be down."

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 620814
    :catch_0
    move-exception v0

    .line 620815
    :goto_0
    :try_start_1
    new-instance v2, Ljava/io/IOException;

    invoke-direct {v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 620816
    :catchall_0
    move-exception v0

    :goto_1
    invoke-interface {v3}, LX/1ca;->g()Z

    .line 620817
    if-eqz v1, :cond_0

    .line 620818
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    :cond_0
    throw v0

    .line 620819
    :cond_1
    :try_start_2
    new-instance v2, LX/1lZ;

    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FK;

    invoke-direct {v2, v0}, LX/1lZ;-><init>(LX/1FK;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 620820
    :try_start_3
    iget-object v0, p1, LX/34X;->g:LX/1uy;

    move-object v0, v0

    .line 620821
    const-wide/16 v4, -0x1

    sget-object v6, LX/2ur;->NO_HEADER:LX/2ur;

    invoke-interface {v0, v2, v4, v5, v6}, LX/1uy;->a(Ljava/io/InputStream;JLX/2ur;)Ljava/lang/Object;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 620822
    invoke-interface {v3}, LX/1ca;->g()Z

    .line 620823
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 620824
    return-object v1

    .line 620825
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_1

    .line 620826
    :catch_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/34X;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/34X",
            "<TT;>;)TT;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 620780
    const/4 v0, 0x0

    .line 620781
    iget-object v1, p1, LX/34X;->c:LX/3AO;

    move-object v1, v1

    .line 620782
    sget-object v2, LX/3AO;->CONTENT:LX/3AO;

    if-eq v1, v2, :cond_0

    .line 620783
    iget-object v1, p1, LX/34X;->c:LX/3AO;

    move-object v1, v1

    .line 620784
    sget-object v2, LX/3AO;->FILE:LX/3AO;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, LX/3ek;->a:LX/0Uh;

    const/16 v2, 0x1dd

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    move v0, v0

    .line 620785
    if-eqz v0, :cond_1

    .line 620786
    invoke-direct {p0, p1}, LX/3ek;->d(LX/34X;)Ljava/lang/Object;

    move-result-object v0

    .line 620787
    :goto_0
    return-object v0

    :cond_1
    invoke-super {p0, p1}, LX/3AP;->a(LX/34X;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method
