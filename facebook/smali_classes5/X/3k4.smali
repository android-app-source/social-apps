.class public final LX/3k4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/util/List",
        "<",
        "Ljava/lang/Boolean;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/feedback/reactions/ui/FeedbackReactionsDownloader$PersistanceRunnable;


# direct methods
.method public constructor <init>(Lcom/facebook/feedback/reactions/ui/FeedbackReactionsDownloader$PersistanceRunnable;)V
    .locals 0

    .prologue
    .line 632680
    iput-object p1, p0, LX/3k4;->a:Lcom/facebook/feedback/reactions/ui/FeedbackReactionsDownloader$PersistanceRunnable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 632681
    sget-object v0, LX/1zn;->a:Ljava/lang/Class;

    const-string v1, "Failed to fetch reactions from the server - "

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 632682
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 632683
    iget-object v0, p0, LX/3k4;->a:Lcom/facebook/feedback/reactions/ui/FeedbackReactionsDownloader$PersistanceRunnable;

    iget-object v0, v0, Lcom/facebook/feedback/reactions/ui/FeedbackReactionsDownloader$PersistanceRunnable;->a:LX/1zn;

    iget-object v0, v0, LX/1zn;->j:LX/1zq;

    invoke-virtual {v0}, LX/1zq;->b()V

    .line 632684
    iget-object v0, p0, LX/3k4;->a:Lcom/facebook/feedback/reactions/ui/FeedbackReactionsDownloader$PersistanceRunnable;

    iget-object v0, v0, Lcom/facebook/feedback/reactions/ui/FeedbackReactionsDownloader$PersistanceRunnable;->a:LX/1zn;

    iget-object v0, v0, LX/1zn;->d:LX/1zf;

    .line 632685
    iget-object p0, v0, LX/1zf;->a:LX/1zi;

    invoke-static {v0, p0}, LX/1zf;->a(LX/1zf;LX/1zi;)LX/0Px;

    move-result-object p0

    iput-object p0, v0, LX/1zf;->c:LX/0Px;

    .line 632686
    return-void
.end method
