.class public LX/484;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1gC;


# instance fields
.field private final a:Lcom/facebook/compactdisk/DiskCacheEvent;


# direct methods
.method public constructor <init>(Lcom/facebook/compactdisk/DiskCacheEvent;)V
    .locals 0

    .prologue
    .line 672673
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 672674
    iput-object p1, p0, LX/484;->a:Lcom/facebook/compactdisk/DiskCacheEvent;

    .line 672675
    return-void
.end method


# virtual methods
.method public final a()LX/1bh;
    .locals 2

    .prologue
    .line 672672
    new-instance v0, LX/1ed;

    iget-object v1, p0, LX/484;->a:Lcom/facebook/compactdisk/DiskCacheEvent;

    invoke-virtual {v1}, Lcom/facebook/compactdisk/DiskCacheEvent;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1ed;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 672671
    iget-object v0, p0, LX/484;->a:Lcom/facebook/compactdisk/DiskCacheEvent;

    invoke-virtual {v0}, Lcom/facebook/compactdisk/DiskCacheEvent;->getKey()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()J
    .locals 2

    .prologue
    .line 672658
    iget-object v0, p0, LX/484;->a:Lcom/facebook/compactdisk/DiskCacheEvent;

    invoke-virtual {v0}, Lcom/facebook/compactdisk/DiskCacheEvent;->getSize()Ljava/lang/Long;

    move-result-object v0

    .line 672659
    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    goto :goto_0
.end method

.method public final d()J
    .locals 2

    .prologue
    .line 672669
    iget-object v0, p0, LX/484;->a:Lcom/facebook/compactdisk/DiskCacheEvent;

    invoke-virtual {v0}, Lcom/facebook/compactdisk/DiskCacheEvent;->getCacheSize()Ljava/lang/Long;

    move-result-object v0

    .line 672670
    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    goto :goto_0
.end method

.method public final e()J
    .locals 2

    .prologue
    .line 672668
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final f()Ljava/io/IOException;
    .locals 1

    .prologue
    .line 672667
    iget-object v0, p0, LX/484;->a:Lcom/facebook/compactdisk/DiskCacheEvent;

    invoke-virtual {v0}, Lcom/facebook/compactdisk/DiskCacheEvent;->getException()Ljava/io/IOException;

    move-result-object v0

    return-object v0
.end method

.method public final g()LX/37E;
    .locals 4

    .prologue
    .line 672660
    iget-object v0, p0, LX/484;->a:Lcom/facebook/compactdisk/DiskCacheEvent;

    invoke-virtual {v0}, Lcom/facebook/compactdisk/DiskCacheEvent;->getEvictionReason()Lcom/facebook/compactdisk/EvictionReason;

    move-result-object v0

    .line 672661
    sget-object v1, LX/483;->a:[I

    invoke-virtual {v0}, Lcom/facebook/compactdisk/EvictionReason;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 672662
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unrecognized EvictionReason: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 672663
    :pswitch_0
    sget-object v0, LX/37E;->CACHE_FULL:LX/37E;

    .line 672664
    :goto_0
    return-object v0

    .line 672665
    :pswitch_1
    sget-object v0, LX/37E;->CONTENT_STALE:LX/37E;

    goto :goto_0

    .line 672666
    :pswitch_2
    sget-object v0, LX/37E;->USER_FORCED:LX/37E;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
