.class public LX/4ez;
.super LX/4ep;
.source ""

# interfaces
.implements LX/4ey;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/4ep;",
        "LX/4ey",
        "<",
        "LX/1FL;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final b:[Ljava/lang/String;

.field private static final c:[Ljava/lang/String;

.field public static final d:Landroid/graphics/Rect;

.field public static final e:Landroid/graphics/Rect;


# instance fields
.field private final f:Landroid/content/ContentResolver;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/16 v4, 0x60

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 797825
    const-class v0, LX/4ez;

    sput-object v0, LX/4ez;->a:Ljava/lang/Class;

    .line 797826
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "_data"

    aput-object v1, v0, v2

    sput-object v0, LX/4ez;->b:[Ljava/lang/String;

    .line 797827
    new-array v0, v2, [Ljava/lang/String;

    const-string v1, "_data"

    aput-object v1, v0, v3

    sput-object v0, LX/4ez;->c:[Ljava/lang/String;

    .line 797828
    new-instance v0, Landroid/graphics/Rect;

    const/16 v1, 0x200

    const/16 v2, 0x180

    invoke-direct {v0, v3, v3, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    sput-object v0, LX/4ez;->d:Landroid/graphics/Rect;

    .line 797829
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v3, v3, v4, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    sput-object v0, LX/4ez;->e:Landroid/graphics/Rect;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/Executor;LX/1Fj;Landroid/content/ContentResolver;Z)V
    .locals 0

    .prologue
    .line 797822
    invoke-direct {p0, p1, p2, p4}, LX/4ep;-><init>(Ljava/util/concurrent/Executor;LX/1Fj;Z)V

    .line 797823
    iput-object p3, p0, LX/4ez;->f:Landroid/content/ContentResolver;

    .line 797824
    return-void
.end method

.method private static a(LX/4ez;LX/1o9;I)LX/1FL;
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 797796
    sget-object v1, LX/4ez;->e:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    sget-object v2, LX/4ez;->e:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    invoke-static {v1, v2, p1}, LX/4fN;->a(IILX/1o9;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 797797
    const/4 v1, 0x3

    .line 797798
    :goto_0
    move v1, v1

    .line 797799
    if-nez v1, :cond_1

    .line 797800
    :cond_0
    :goto_1
    return-object v0

    .line 797801
    :cond_1
    :try_start_0
    iget-object v2, p0, LX/4ez;->f:Landroid/content/ContentResolver;

    int-to-long v4, p2

    sget-object v3, LX/4ez;->c:[Ljava/lang/String;

    invoke-static {v2, v4, v5, v1, v3}, Landroid/provider/MediaStore$Images$Thumbnails;->queryMiniThumbnail(Landroid/content/ContentResolver;JI[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 797802
    if-nez v1, :cond_2

    .line 797803
    if-eqz v1, :cond_0

    .line 797804
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 797805
    :cond_2
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 797806
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_3

    .line 797807
    const-string v2, "_data"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 797808
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 797809
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, v2}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 797810
    if-nez v2, :cond_7

    const/4 v7, -0x1

    :goto_2
    move v2, v7

    .line 797811
    invoke-virtual {p0, v0, v2}, LX/4ep;->b(Ljava/io/InputStream;I)LX/1FL;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 797812
    if-eqz v1, :cond_0

    .line 797813
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 797814
    :cond_3
    if-eqz v1, :cond_0

    .line 797815
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 797816
    :catchall_0
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    :goto_3
    if-eqz v1, :cond_4

    .line 797817
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    .line 797818
    :catchall_1
    move-exception v0

    goto :goto_3

    .line 797819
    :cond_5
    sget-object v1, LX/4ez;->d:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    sget-object v2, LX/4ez;->d:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    invoke-static {v1, v2, p1}, LX/4fN;->a(IILX/1o9;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 797820
    const/4 v1, 0x1

    goto :goto_0

    .line 797821
    :cond_6
    const/4 v1, 0x0

    goto :goto_0

    :cond_7
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/io/File;->length()J

    move-result-wide v7

    long-to-int v7, v7

    goto :goto_2
.end method

.method private a(Landroid/net/Uri;LX/1o9;)LX/1FL;
    .locals 7
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 797765
    iget-object v0, p0, LX/4ez;->f:Landroid/content/ContentResolver;

    sget-object v2, LX/4ez;->b:[Ljava/lang/String;

    move-object v1, p1

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 797766
    if-nez v1, :cond_0

    .line 797767
    :goto_0
    return-object v3

    .line 797768
    :cond_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 797769
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 797770
    :cond_1
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 797771
    const-string v0, "_data"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 797772
    if-eqz p2, :cond_3

    .line 797773
    const-string v0, "_id"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 797774
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {p0, p2, v0}, LX/4ez;->a(LX/4ez;LX/1o9;I)LX/1FL;

    move-result-object v0

    .line 797775
    if-eqz v0, :cond_3

    .line 797776
    const/4 p0, 0x1

    const/4 v3, 0x0

    .line 797777
    if-eqz v2, :cond_2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 797778
    :try_start_2
    new-instance v4, Landroid/media/ExifInterface;

    invoke-direct {v4, v2}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    .line 797779
    const-string v5, "Orientation"

    const/4 v6, 0x1

    invoke-virtual {v4, v5, v6}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v4

    invoke-static {v4}, LX/1li;->a(I)I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    move-result v3

    .line 797780
    :cond_2
    :goto_1
    move v2, v3

    .line 797781
    iput v2, v0, LX/1FL;->d:I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 797782
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move-object v3, v0

    goto :goto_0

    :cond_3
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    .line 797783
    :catch_0
    move-exception v4

    .line 797784
    sget-object v5, LX/4ez;->a:Ljava/lang/Class;

    const-string v6, "Unable to retrieve thumbnail rotation for %s"

    new-array p0, p0, [Ljava/lang/Object;

    aput-object v2, p0, v3

    .line 797785
    sget-object p1, LX/03J;->a:LX/03G;

    const/4 p2, 0x6

    invoke-interface {p1, p2}, LX/03G;->b(I)Z

    move-result p1

    if-eqz p1, :cond_4

    .line 797786
    sget-object p1, LX/03J;->a:LX/03G;

    invoke-static {v5}, LX/03J;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object p2

    invoke-static {v6, p0}, LX/03J;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, p2, v2, v4}, LX/03G;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 797787
    :cond_4
    goto :goto_1
.end method


# virtual methods
.method public final a(LX/1bf;)LX/1FL;
    .locals 2

    .prologue
    .line 797790
    iget-object v0, p1, LX/1bf;->b:Landroid/net/Uri;

    move-object v0, v0

    .line 797791
    invoke-static {v0}, LX/1be;->e(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 797792
    iget-object v1, p1, LX/1bf;->h:LX/1o9;

    move-object v1, v1

    .line 797793
    invoke-direct {p0, v0, v1}, LX/4ez;->a(Landroid/net/Uri;LX/1o9;)LX/1FL;

    move-result-object v0

    .line 797794
    if-eqz v0, :cond_0

    .line 797795
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 797789
    const-string v0, "LocalContentUriThumbnailFetchProducer"

    return-object v0
.end method

.method public final a(LX/1o9;)Z
    .locals 2

    .prologue
    .line 797788
    sget-object v0, LX/4ez;->d:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    sget-object v1, LX/4ez;->d:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    invoke-static {v0, v1, p1}, LX/4fN;->a(IILX/1o9;)Z

    move-result v0

    return v0
.end method
