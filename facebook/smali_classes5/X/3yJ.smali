.class public LX/3yJ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile i:LX/3yJ;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/3yM;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/3yP;",
            ">;>;"
        }
    .end annotation
.end field

.field private c:LX/00H;

.field private d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private e:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private g:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/Class",
            "<+",
            "LX/2Ff;",
            ">;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/Class",
            "<+",
            "LX/2Ff;",
            ">;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/00H;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/3yM;",
            ">;>;",
            "LX/00H;",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/3yP;",
            ">;>;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 660899
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 660900
    iput-object p1, p0, LX/3yJ;->a:LX/0Ot;

    .line 660901
    iput-object p2, p0, LX/3yJ;->c:LX/00H;

    .line 660902
    iput-object p4, p0, LX/3yJ;->d:LX/0Ot;

    .line 660903
    iput-object p3, p0, LX/3yJ;->b:LX/0Ot;

    .line 660904
    return-void
.end method

.method public static a(LX/0QB;)LX/3yJ;
    .locals 7

    .prologue
    .line 660905
    sget-object v0, LX/3yJ;->i:LX/3yJ;

    if-nez v0, :cond_1

    .line 660906
    const-class v1, LX/3yJ;

    monitor-enter v1

    .line 660907
    :try_start_0
    sget-object v0, LX/3yJ;->i:LX/3yJ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 660908
    if-eqz v2, :cond_0

    .line 660909
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 660910
    new-instance v4, LX/3yJ;

    .line 660911
    new-instance v3, LX/3yN;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v5

    invoke-direct {v3, v5}, LX/3yN;-><init>(LX/0QB;)V

    move-object v3, v3

    .line 660912
    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v5

    invoke-static {v3, v5}, LX/0Sr;->a(LX/0Or;LX/0R7;)LX/0Ot;

    move-result-object v3

    move-object v5, v3

    .line 660913
    const-class v3, LX/00H;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/00H;

    .line 660914
    new-instance v6, LX/3yO;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object p0

    invoke-direct {v6, p0}, LX/3yO;-><init>(LX/0QB;)V

    move-object v6, v6

    .line 660915
    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object p0

    invoke-static {v6, p0}, LX/0Sr;->a(LX/0Or;LX/0R7;)LX/0Ot;

    move-result-object v6

    move-object v6, v6

    .line 660916
    const/16 p0, 0x259

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v4, v5, v3, v6, p0}, LX/3yJ;-><init>(LX/0Ot;LX/00H;LX/0Ot;LX/0Ot;)V

    .line 660917
    move-object v0, v4

    .line 660918
    sput-object v0, LX/3yJ;->i:LX/3yJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 660919
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 660920
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 660921
    :cond_1
    sget-object v0, LX/3yJ;->i:LX/3yJ;

    return-object v0

    .line 660922
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 660923
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/3yJ;LX/0cA;Ljava/util/Map;LX/0Rf;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0cA",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<+",
            "LX/2Ff;",
            ">;",
            "Ljava/lang/String;",
            ">;",
            "LX/0Rf",
            "<",
            "LX/3yK;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 660924
    invoke-virtual {p3}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3yK;

    .line 660925
    iget-boolean v1, v0, LX/3yK;->b:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/3yJ;->c:LX/00H;

    .line 660926
    iget-object v3, v1, LX/00H;->j:LX/01T;

    move-object v1, v3

    .line 660927
    sget-object v3, LX/01T;->MESSENGER:LX/01T;

    if-eq v1, v3, :cond_0

    .line 660928
    :cond_1
    invoke-virtual {v0}, LX/3yK;->a()LX/0Rf;

    move-result-object v1

    invoke-virtual {v1}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Class;

    .line 660929
    iget-object v4, v0, LX/3yK;->a:Ljava/lang/String;

    invoke-interface {p2, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 660930
    :cond_2
    iget-object v0, v0, LX/3yK;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    goto :goto_0

    .line 660931
    :cond_3
    return-void
.end method

.method private declared-synchronized c()V
    .locals 4

    .prologue
    .line 660932
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/3yJ;->e:LX/0Rf;

    if-nez v0, :cond_1

    .line 660933
    iget-object v0, p0, LX/3yJ;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 660934
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v1

    .line 660935
    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    invoke-static {v2}, LX/0PM;->a(I)Ljava/util/HashMap;

    move-result-object v2

    .line 660936
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3yM;

    .line 660937
    invoke-interface {v0}, LX/3yM;->a()LX/0Rf;

    move-result-object v0

    invoke-static {p0, v1, v2, v0}, LX/3yJ;->a(LX/3yJ;LX/0cA;Ljava/util/Map;LX/0Rf;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 660938
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 660939
    :cond_0
    :try_start_1
    invoke-virtual {v1}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    iput-object v0, p0, LX/3yJ;->e:LX/0Rf;

    .line 660940
    invoke-static {v2}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    iput-object v0, p0, LX/3yJ;->g:LX/0P1;

    .line 660941
    const/4 v0, 0x0

    iput-object v0, p0, LX/3yJ;->a:LX/0Ot;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 660942
    :cond_1
    monitor-exit p0

    return-void
.end method

.method public static declared-synchronized d(LX/3yJ;)V
    .locals 4

    .prologue
    .line 660943
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/3yJ;->f:LX/0Rf;

    if-nez v0, :cond_1

    .line 660944
    iget-object v0, p0, LX/3yJ;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 660945
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v1

    .line 660946
    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    invoke-static {v2}, LX/0PM;->a(I)Ljava/util/HashMap;

    move-result-object v2

    .line 660947
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3yP;

    .line 660948
    invoke-interface {v0}, LX/3yP;->a()LX/0Rf;

    move-result-object v0

    invoke-static {p0, v1, v2, v0}, LX/3yJ;->a(LX/3yJ;LX/0cA;Ljava/util/Map;LX/0Rf;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 660949
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 660950
    :cond_0
    :try_start_1
    invoke-virtual {v1}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    iput-object v0, p0, LX/3yJ;->f:LX/0Rf;

    .line 660951
    invoke-static {v2}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    iput-object v0, p0, LX/3yJ;->h:LX/0P1;

    .line 660952
    const/4 v0, 0x0

    iput-object v0, p0, LX/3yJ;->b:LX/0Ot;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 660953
    :cond_1
    monitor-exit p0

    return-void
.end method


# virtual methods
.method public final a()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 660954
    invoke-direct {p0}, LX/3yJ;->c()V

    .line 660955
    iget-object v0, p0, LX/3yJ;->e:LX/0Rf;

    return-object v0
.end method
