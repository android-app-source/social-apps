.class public LX/4lP;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/4lO;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 804281
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 804282
    new-instance v0, LX/4lO;

    invoke-direct {v0}, LX/4lO;-><init>()V

    iput-object v0, p0, LX/4lP;->a:LX/4lO;

    .line 804283
    return-void
.end method

.method public static a(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 804284
    instance-of v0, p0, Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 804285
    sget-object v0, LX/4lR;->b:Ljava/lang/String;

    .line 804286
    :goto_0
    return-object v0

    .line 804287
    :cond_0
    instance-of v0, p0, Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 804288
    sget-object v0, LX/4lR;->c:Ljava/lang/String;

    goto :goto_0

    .line 804289
    :cond_1
    instance-of v0, p0, Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 804290
    sget-object v0, LX/4lR;->g:Ljava/lang/String;

    goto :goto_0

    .line 804291
    :cond_2
    instance-of v0, p0, Ljava/lang/Double;

    if-eqz v0, :cond_3

    .line 804292
    sget-object v0, LX/4lR;->d:Ljava/lang/String;

    goto :goto_0

    .line 804293
    :cond_3
    instance-of v0, p0, Ljava/lang/Float;

    if-eqz v0, :cond_4

    .line 804294
    sget-object v0, LX/4lR;->e:Ljava/lang/String;

    goto :goto_0

    .line 804295
    :cond_4
    instance-of v0, p0, Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 804296
    sget-object v0, LX/4lR;->f:Ljava/lang/String;

    goto :goto_0

    .line 804297
    :cond_5
    instance-of v0, p0, Ljava/util/Map;

    if-eqz v0, :cond_6

    .line 804298
    sget-object v0, LX/4lR;->h:Ljava/lang/String;

    goto :goto_0

    .line 804299
    :cond_6
    instance-of v0, p0, Ljava/util/List;

    if-eqz v0, :cond_7

    .line 804300
    sget-object v0, LX/4lR;->i:Ljava/lang/String;

    goto :goto_0

    .line 804301
    :cond_7
    sget-object v0, LX/4lR;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method public static a(LX/4lP;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 804302
    const/4 v0, 0x0

    .line 804303
    if-nez p2, :cond_4

    .line 804304
    :cond_0
    :goto_0
    move v0, v0

    .line 804305
    if-eqz v0, :cond_3

    .line 804306
    invoke-interface {p4, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v1, 0x0

    const/4 v3, 0x1

    .line 804307
    const/4 v2, -0x1

    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result p4

    sparse-switch p4, :sswitch_data_0

    :cond_1
    :goto_1
    packed-switch v2, :pswitch_data_0

    .line 804308
    :cond_2
    :goto_2
    return-void

    .line 804309
    :cond_3
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, p3, v0}, LX/4lP;->a(LX/4lP;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Z)Z

    goto :goto_2

    .line 804310
    :cond_4
    sget-object v1, LX/4lR;->h:Ljava/lang/String;

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    sget-object v1, LX/4lR;->i:Ljava/lang/String;

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 804311
    :cond_5
    const/4 v0, 0x1

    goto :goto_0

    .line 804312
    :sswitch_0
    const-string p4, "vector"

    invoke-virtual {p2, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p4

    if-eqz p4, :cond_1

    move v2, v1

    goto :goto_1

    :sswitch_1
    const-string p4, "map"

    invoke-virtual {p2, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p4

    if-eqz p4, :cond_1

    move v2, v3

    goto :goto_1

    .line 804313
    :pswitch_0
    instance-of v2, p3, Ljava/util/List;

    if-nez v2, :cond_6

    .line 804314
    invoke-static {p3}, LX/4lP;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 804315
    iget-object v2, p0, LX/4lP;->a:LX/4lO;

    invoke-virtual {p3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, p1, v3, v1, p2}, LX/4lO;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_6
    move v2, v1

    :goto_3
    move-object v1, p3

    .line 804316
    check-cast v1, Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v2, v1, :cond_2

    move-object v1, p3

    .line 804317
    check-cast v1, Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-static {p0, p1, v0, v1, v3}, LX/4lP;->a(LX/4lP;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Z)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 804318
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_3

    .line 804319
    :pswitch_1
    invoke-static {p3}, LX/4lP;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 804320
    instance-of v2, p3, Ljava/util/Map;

    if-nez v2, :cond_7

    .line 804321
    iget-object v2, p0, LX/4lP;->a:LX/4lO;

    invoke-virtual {p3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, p1, v3, v1, p2}, LX/4lO;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 804322
    :cond_7
    check-cast p3, Ljava/util/Map;

    .line 804323
    invoke-interface {p3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_8
    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 804324
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    .line 804325
    if-eqz v1, :cond_8

    invoke-static {p0, p1, p2, v1, v3}, LX/4lP;->a(LX/4lP;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Z)Z

    move-result v1

    if-eqz v1, :cond_2

    goto :goto_4

    nop

    :sswitch_data_0
    .sparse-switch
        -0x30e61ebd -> :sswitch_0
        0x1a55c -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(LX/4lP;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Z)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 804326
    const/4 v2, -0x1

    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v2, :pswitch_data_0

    .line 804327
    iget-object v1, p0, LX/4lP;->a:LX/4lO;

    invoke-virtual {p3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "not found"

    invoke-virtual {v1, p1, v2, v3, p2}, LX/4lO;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 804328
    :cond_1
    :goto_1
    return v0

    .line 804329
    :sswitch_0
    const-string v3, "string"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v2, v0

    goto :goto_0

    :sswitch_1
    const-string v3, "long"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v2, v1

    goto :goto_0

    :sswitch_2
    const-string v3, "int"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x2

    goto :goto_0

    :sswitch_3
    const-string v3, "double"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x3

    goto :goto_0

    :sswitch_4
    const-string v3, "float"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x4

    goto :goto_0

    :sswitch_5
    const-string v3, "bool"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x5

    goto :goto_0

    :sswitch_6
    const-string v3, "boolean"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x6

    goto :goto_0

    .line 804330
    :pswitch_0
    instance-of v0, p3, Ljava/lang/String;

    .line 804331
    :cond_2
    :goto_2
    if-nez v0, :cond_1

    .line 804332
    invoke-static {p3}, LX/4lP;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 804333
    if-eqz p4, :cond_5

    .line 804334
    iget-object v2, p0, LX/4lP;->a:LX/4lO;

    invoke-virtual {p3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 804335
    new-instance p0, Ljava/lang/StringBuilder;

    const-string p3, "Payload field \""

    invoke-direct {p0, p3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p0

    const-string p3, "\" has "

    invoke-virtual {p0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p0

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p0

    const-string p3, " as collection value data type: "

    invoke-virtual {p0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p0

    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p0

    const-string p3, ". Defined type is: "

    invoke-virtual {p0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p0

    invoke-virtual {p0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 804336
    iget-object p3, v2, LX/4lO;->a:Ljava/util/ArrayList;

    invoke-virtual {p3, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 804337
    goto/16 :goto_1

    .line 804338
    :pswitch_1
    instance-of v2, p3, Ljava/lang/Long;

    if-nez v2, :cond_3

    instance-of v2, p3, Ljava/lang/Integer;

    if-eqz v2, :cond_2

    :cond_3
    move v0, v1

    goto :goto_2

    .line 804339
    :pswitch_2
    instance-of v2, p3, Ljava/lang/Float;

    if-nez v2, :cond_4

    instance-of v2, p3, Ljava/lang/Double;

    if-eqz v2, :cond_2

    :cond_4
    move v0, v1

    goto :goto_2

    .line 804340
    :pswitch_3
    instance-of v0, p3, Ljava/lang/Boolean;

    goto :goto_2

    .line 804341
    :cond_5
    iget-object v2, p0, LX/4lP;->a:LX/4lO;

    invoke-virtual {p3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, p1, v3, v1, p2}, LX/4lO;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x4f08842f -> :sswitch_3
        -0x352a9fef -> :sswitch_0
        0x197ef -> :sswitch_2
        0x2e3aea -> :sswitch_5
        0x32c67c -> :sswitch_1
        0x3db6c28 -> :sswitch_6
        0x5d0225c -> :sswitch_4
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method
