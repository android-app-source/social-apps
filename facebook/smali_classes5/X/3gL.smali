.class public LX/3gL;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3gO;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/3gL",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/3gO;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 624288
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 624289
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/3gL;->b:LX/0Zi;

    .line 624290
    iput-object p1, p0, LX/3gL;->a:LX/0Ot;

    .line 624291
    return-void
.end method

.method public static a(LX/0QB;)LX/3gL;
    .locals 4

    .prologue
    .line 624292
    const-class v1, LX/3gL;

    monitor-enter v1

    .line 624293
    :try_start_0
    sget-object v0, LX/3gL;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 624294
    sput-object v2, LX/3gL;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 624295
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 624296
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 624297
    new-instance v3, LX/3gL;

    const/16 p0, 0x719

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/3gL;-><init>(LX/0Ot;)V

    .line 624298
    move-object v0, v3

    .line 624299
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 624300
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/3gL;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 624301
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 624302
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 8

    .prologue
    .line 624303
    check-cast p2, LX/3gM;

    .line 624304
    iget-object v0, p0, LX/3gL;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3gO;

    iget-object v2, p2, LX/3gM;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p2, LX/3gM;->b:LX/1Pq;

    iget v4, p2, LX/3gM;->c:I

    iget v5, p2, LX/3gM;->d:I

    iget v6, p2, LX/3gM;->e:I

    move-object v1, p1

    .line 624305
    iget-object v7, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v7, v7

    .line 624306
    check-cast v7, Lcom/facebook/graphql/model/GraphQLStory;

    .line 624307
    new-instance p1, LX/3gR;

    iget-object p0, v0, LX/3gO;->c:LX/3gP;

    invoke-direct {p1, v2, p0}, LX/3gR;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/3gP;)V

    .line 624308
    iget-object p2, v0, LX/3gO;->d:LX/1xu;

    move-object p0, v3

    check-cast p0, LX/1Pr;

    invoke-virtual {p2, p1, p0}, LX/1xu;->a(LX/1xz;LX/1Pr;)V

    .line 624309
    check-cast v3, LX/1Pr;

    invoke-interface {p1}, LX/1xz;->a()LX/1KL;

    move-result-object p0

    invoke-interface {v3, p0, v7}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/1yT;

    .line 624310
    invoke-static {v1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object p0

    iget-object v7, v7, LX/1yT;->a:Landroid/text/Spannable;

    invoke-virtual {p0, v7}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v7

    invoke-virtual {v7, v4}, LX/1ne;->m(I)LX/1ne;

    move-result-object v7

    invoke-virtual {v7, v5}, LX/1ne;->p(I)LX/1ne;

    move-result-object v7

    invoke-virtual {v7, v6}, LX/1ne;->t(I)LX/1ne;

    move-result-object v7

    const/4 p0, 0x0

    invoke-virtual {v7, p0}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v7

    invoke-virtual {v7}, LX/1X5;->c()LX/1Di;

    move-result-object p0

    invoke-static {v2}, LX/182;->s(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v7

    if-eqz v7, :cond_0

    sget-object v7, LX/3gO;->a:Landroid/util/SparseArray;

    :goto_0
    invoke-interface {p0, v7}, LX/1Di;->a(Landroid/util/SparseArray;)LX/1Di;

    move-result-object v7

    invoke-interface {v7}, LX/1Di;->k()LX/1Dg;

    move-result-object v7

    move-object v0, v7

    .line 624311
    return-object v0

    :cond_0
    sget-object v7, LX/3gO;->b:Landroid/util/SparseArray;

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 624312
    invoke-static {}, LX/1dS;->b()V

    .line 624313
    const/4 v0, 0x0

    return-object v0
.end method
