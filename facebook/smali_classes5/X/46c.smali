.class public LX/46c;
.super Ljava/io/FilterInputStream;
.source ""


# instance fields
.field private a:I

.field private b:I


# direct methods
.method public constructor <init>(Ljava/io/InputStream;I)V
    .locals 2

    .prologue
    .line 671410
    invoke-direct {p0, p1}, Ljava/io/FilterInputStream;-><init>(Ljava/io/InputStream;)V

    .line 671411
    if-nez p1, :cond_0

    .line 671412
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 671413
    :cond_0
    if-gez p2, :cond_1

    .line 671414
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "limit must be >= 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 671415
    :cond_1
    iput p2, p0, LX/46c;->a:I

    .line 671416
    const/4 v0, -0x1

    iput v0, p0, LX/46c;->b:I

    .line 671417
    return-void
.end method


# virtual methods
.method public final available()I
    .locals 2

    .prologue
    .line 671418
    iget-object v0, p0, Ljava/io/FilterInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->available()I

    move-result v0

    iget v1, p0, LX/46c;->a:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0
.end method

.method public final mark(I)V
    .locals 1

    .prologue
    .line 671419
    iget-object v0, p0, Ljava/io/FilterInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->markSupported()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 671420
    iget-object v0, p0, Ljava/io/FilterInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v0, p1}, Ljava/io/InputStream;->mark(I)V

    .line 671421
    iget v0, p0, LX/46c;->a:I

    iput v0, p0, LX/46c;->b:I

    .line 671422
    :cond_0
    return-void
.end method

.method public final read()I
    .locals 2

    .prologue
    const/4 v0, -0x1

    .line 671423
    iget v1, p0, LX/46c;->a:I

    if-nez v1, :cond_0

    .line 671424
    :goto_0
    return v0

    .line 671425
    :cond_0
    iget-object v1, p0, Ljava/io/FilterInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    move-result v1

    .line 671426
    if-eq v1, v0, :cond_1

    .line 671427
    iget v0, p0, LX/46c;->a:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/46c;->a:I

    :cond_1
    move v0, v1

    .line 671428
    goto :goto_0
.end method

.method public final read([BII)I
    .locals 2

    .prologue
    .line 671429
    iget v0, p0, LX/46c;->a:I

    if-nez v0, :cond_1

    .line 671430
    const/4 v0, -0x1

    .line 671431
    :cond_0
    :goto_0
    return v0

    .line 671432
    :cond_1
    iget v0, p0, LX/46c;->a:I

    invoke-static {p3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 671433
    iget-object v1, p0, Ljava/io/FilterInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v1, p1, p2, v0}, Ljava/io/InputStream;->read([BII)I

    move-result v0

    .line 671434
    if-lez v0, :cond_0

    .line 671435
    iget v1, p0, LX/46c;->a:I

    sub-int/2addr v1, v0

    iput v1, p0, LX/46c;->a:I

    goto :goto_0
.end method

.method public final reset()V
    .locals 2

    .prologue
    .line 671436
    iget-object v0, p0, Ljava/io/FilterInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->markSupported()Z

    move-result v0

    if-nez v0, :cond_0

    .line 671437
    new-instance v0, Ljava/io/IOException;

    const-string v1, "mark is not supported"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 671438
    :cond_0
    iget v0, p0, LX/46c;->b:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 671439
    new-instance v0, Ljava/io/IOException;

    const-string v1, "mark not set"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 671440
    :cond_1
    iget-object v0, p0, Ljava/io/FilterInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->reset()V

    .line 671441
    iget v0, p0, LX/46c;->b:I

    iput v0, p0, LX/46c;->a:I

    .line 671442
    return-void
.end method

.method public final skip(J)J
    .locals 5

    .prologue
    .line 671443
    iget v0, p0, LX/46c;->a:I

    int-to-long v0, v0

    invoke-static {p1, p2, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    .line 671444
    iget-object v2, p0, Ljava/io/FilterInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v2, v0, v1}, Ljava/io/InputStream;->skip(J)J

    move-result-wide v0

    .line 671445
    iget v2, p0, LX/46c;->a:I

    int-to-long v2, v2

    sub-long/2addr v2, v0

    long-to-int v2, v2

    iput v2, p0, LX/46c;->a:I

    .line 671446
    return-wide v0
.end method
