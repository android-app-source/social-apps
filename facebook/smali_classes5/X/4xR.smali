.class public LX/4xR;
.super LX/0dW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "LX/0dW",
        "<TE;>;"
    }
.end annotation


# instance fields
.field private final forward:LX/0dW;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0dW",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0dW;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0dW",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 821031
    invoke-virtual {p1}, LX/0dW;->comparator()Ljava/util/Comparator;

    move-result-object v0

    invoke-static {v0}, LX/1sm;->a(Ljava/util/Comparator;)LX/1sm;

    move-result-object v0

    invoke-virtual {v0}, LX/1sm;->a()LX/1sm;

    move-result-object v0

    invoke-direct {p0, v0}, LX/0dW;-><init>(Ljava/util/Comparator;)V

    .line 821032
    iput-object p1, p0, LX/4xR;->forward:LX/0dW;

    .line 821033
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)I
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 821028
    iget-object v0, p0, LX/4xR;->forward:LX/0dW;

    invoke-virtual {v0, p1}, LX/0dW;->a(Ljava/lang/Object;)I

    move-result v0

    .line 821029
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 821030
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, LX/4xR;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    sub-int v0, v1, v0

    goto :goto_0
.end method

.method public final a()LX/0dW;
    .locals 1
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "NavigableSet"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0dW",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 821027
    iget-object v0, p0, LX/4xR;->forward:LX/0dW;

    return-object v0
.end method

.method public final a(Ljava/lang/Object;Z)LX/0dW;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;Z)",
            "LX/0dW",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 821026
    iget-object v0, p0, LX/4xR;->forward:LX/0dW;

    invoke-virtual {v0, p1, p2}, LX/0dW;->d(Ljava/lang/Object;Z)LX/0dW;

    move-result-object v0

    invoke-virtual {v0}, LX/0dW;->a()LX/0dW;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;ZLjava/lang/Object;Z)LX/0dW;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;ZTE;Z)",
            "LX/0dW",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 821025
    iget-object v0, p0, LX/4xR;->forward:LX/0dW;

    invoke-virtual {v0, p3, p4, p1, p2}, LX/0dW;->b(Ljava/lang/Object;ZLjava/lang/Object;Z)LX/0dW;

    move-result-object v0

    invoke-virtual {v0}, LX/0dW;->a()LX/0dW;

    move-result-object v0

    return-object v0
.end method

.method public final b()LX/0Rc;
    .locals 1
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "NavigableSet"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rc",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 821024
    iget-object v0, p0, LX/4xR;->forward:LX/0dW;

    invoke-virtual {v0}, LX/0Py;->iterator()LX/0Rc;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/Object;Z)LX/0dW;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;Z)",
            "LX/0dW",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 821023
    iget-object v0, p0, LX/4xR;->forward:LX/0dW;

    invoke-virtual {v0, p1, p2}, LX/0dW;->c(Ljava/lang/Object;Z)LX/0dW;

    move-result-object v0

    invoke-virtual {v0}, LX/0dW;->a()LX/0dW;

    move-result-object v0

    return-object v0
.end method

.method public final c()LX/0dW;
    .locals 2
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "NavigableSet"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0dW",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 821022
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "should never be called"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public final ceiling(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)TE;"
        }
    .end annotation

    .prologue
    .line 821011
    iget-object v0, p0, LX/4xR;->forward:LX/0dW;

    invoke-virtual {v0, p1}, LX/0dW;->floor(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final contains(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 821021
    iget-object v0, p0, LX/4xR;->forward:LX/0dW;

    invoke-virtual {v0, p1}, LX/0dW;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final synthetic descendingIterator()Ljava/util/Iterator;
    .locals 1
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "NavigableSet"
    .end annotation

    .prologue
    .line 821020
    invoke-virtual {p0}, LX/4xR;->b()LX/0Rc;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic descendingSet()Ljava/util/NavigableSet;
    .locals 1
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "NavigableSet"
    .end annotation

    .prologue
    .line 821019
    invoke-virtual {p0}, LX/4xR;->a()LX/0dW;

    move-result-object v0

    return-object v0
.end method

.method public final floor(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)TE;"
        }
    .end annotation

    .prologue
    .line 821018
    iget-object v0, p0, LX/4xR;->forward:LX/0dW;

    invoke-virtual {v0, p1}, LX/0dW;->ceiling(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final higher(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)TE;"
        }
    .end annotation

    .prologue
    .line 821017
    iget-object v0, p0, LX/4xR;->forward:LX/0dW;

    invoke-virtual {v0, p1}, LX/0dW;->lower(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final isPartialView()Z
    .locals 1

    .prologue
    .line 821016
    iget-object v0, p0, LX/4xR;->forward:LX/0dW;

    invoke-virtual {v0}, LX/0Py;->isPartialView()Z

    move-result v0

    return v0
.end method

.method public final iterator()LX/0Rc;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rc",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 821015
    iget-object v0, p0, LX/4xR;->forward:LX/0dW;

    invoke-virtual {v0}, LX/0dW;->b()LX/0Rc;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 821014
    invoke-virtual {p0}, LX/4xR;->iterator()LX/0Rc;

    move-result-object v0

    return-object v0
.end method

.method public final lower(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)TE;"
        }
    .end annotation

    .prologue
    .line 821013
    iget-object v0, p0, LX/4xR;->forward:LX/0dW;

    invoke-virtual {v0, p1}, LX/0dW;->higher(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 821012
    iget-object v0, p0, LX/4xR;->forward:LX/0dW;

    invoke-virtual {v0}, LX/0dW;->size()I

    move-result v0

    return v0
.end method
