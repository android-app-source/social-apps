.class public LX/45u;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:I

.field public final b:I

.field public final c:Z

.field public final d:J

.field public final e:J

.field public final f:J

.field public final g:J

.field public final h:LX/46S;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/45t;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 670650
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 670651
    iget v0, p1, LX/45t;->a:I

    iput v0, p0, LX/45u;->a:I

    .line 670652
    iget v0, p1, LX/45t;->b:I

    iput v0, p0, LX/45u;->b:I

    .line 670653
    iget-boolean v0, p1, LX/45t;->c:Z

    iput-boolean v0, p0, LX/45u;->c:Z

    .line 670654
    iget-wide v0, p1, LX/45t;->d:J

    iput-wide v0, p0, LX/45u;->d:J

    .line 670655
    iget-wide v0, p1, LX/45t;->e:J

    iput-wide v0, p0, LX/45u;->e:J

    .line 670656
    iget-wide v0, p1, LX/45t;->f:J

    iput-wide v0, p0, LX/45u;->f:J

    .line 670657
    iget-wide v0, p1, LX/45t;->h:J

    iput-wide v0, p0, LX/45u;->g:J

    .line 670658
    iget-object v0, p1, LX/45t;->g:LX/46S;

    iput-object v0, p0, LX/45u;->h:LX/46S;

    .line 670659
    iget-wide v0, p0, LX/45u;->e:J

    cmp-long v0, v0, v4

    if-lez v0, :cond_0

    iget-wide v0, p0, LX/45u;->f:J

    cmp-long v0, v0, v4

    if-lez v0, :cond_0

    .line 670660
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "setSoftMaximumLatencyMs(long) and setHardMaximumLatencyMs(long) were both called. You must use one or the other"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 670661
    :cond_0
    iget-wide v0, p0, LX/45u;->d:J

    cmp-long v0, v0, v4

    if-lez v0, :cond_3

    .line 670662
    iget-wide v0, p0, LX/45u;->e:J

    cmp-long v0, v0, v4

    if-gez v0, :cond_1

    iget-wide v0, p0, LX/45u;->f:J

    cmp-long v0, v0, v4

    if-gez v0, :cond_1

    .line 670663
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "You must call setSoftMaximumLatencyMs(long) or setHardMaximumLatencyMs(long)"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 670664
    :cond_1
    iget-wide v0, p0, LX/45u;->e:J

    cmp-long v0, v0, v4

    if-lez v0, :cond_2

    iget-wide v0, p0, LX/45u;->e:J

    iget-wide v2, p0, LX/45u;->d:J

    cmp-long v0, v0, v2

    if-gtz v0, :cond_2

    .line 670665
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "setSoftMaximumLatencyMs(long) <= setMinimumLatencyMs(long)"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 670666
    :cond_2
    iget-wide v0, p0, LX/45u;->f:J

    cmp-long v0, v0, v4

    if-lez v0, :cond_3

    iget-wide v0, p0, LX/45u;->f:J

    iget-wide v2, p0, LX/45u;->d:J

    cmp-long v0, v0, v2

    if-gtz v0, :cond_3

    .line 670667
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "setHardMaximumLatencyMs(long) <= setMinimumLatencyMs(long)"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 670668
    :cond_3
    iget-wide v0, p0, LX/45u;->e:J

    cmp-long v0, v0, v4

    if-lez v0, :cond_4

    iget-wide v0, p0, LX/45u;->d:J

    cmp-long v0, v0, v4

    if-gez v0, :cond_4

    .line 670669
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "setSoftMaximumLatencyMs(long) <= setMinimumLatencyMs(long)"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 670670
    :cond_4
    iget-wide v0, p0, LX/45u;->f:J

    cmp-long v0, v0, v4

    if-lez v0, :cond_5

    iget-wide v0, p0, LX/45u;->d:J

    cmp-long v0, v0, v4

    if-gez v0, :cond_5

    .line 670671
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "setHardMaximumLatencyMs(long) <= setMinimumLatencyMs(long)"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 670672
    :cond_5
    iget-wide v0, p0, LX/45u;->f:J

    cmp-long v0, v0, v4

    if-lez v0, :cond_7

    .line 670673
    iget-boolean v0, p0, LX/45u;->c:Z

    if-eqz v0, :cond_6

    .line 670674
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "setRequiresCharging(boolean) cannot be called if you are using setHardMaximumLatencyMs(long)"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 670675
    :cond_6
    iget v0, p0, LX/45u;->b:I

    if-eqz v0, :cond_7

    .line 670676
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "setRequiredNetwork(int) cannot be called with anything other than NETWORK_CONNECTION_ANY if you are call setHardMaximumLatencyMs(long)"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 670677
    :cond_7
    iget-wide v0, p0, LX/45u;->g:J

    cmp-long v0, v0, v4

    if-ltz v0, :cond_a

    .line 670678
    iget-wide v0, p0, LX/45u;->d:J

    cmp-long v0, v0, v4

    if-ltz v0, :cond_8

    .line 670679
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "setMinimumLatencyMs(long) and setPeriod(long) cant both be called"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 670680
    :cond_8
    iget-wide v0, p0, LX/45u;->e:J

    cmp-long v0, v0, v4

    if-lez v0, :cond_9

    .line 670681
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "setSoftMaximumLatencyMs(long) and setPeriod(long) cant both be called"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 670682
    :cond_9
    iget-wide v0, p0, LX/45u;->f:J

    cmp-long v0, v0, v4

    if-lez v0, :cond_a

    .line 670683
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "setHardMaximumLatencyMs(long) and setPeriod(long) cant both be called"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 670684
    :cond_a
    return-void
.end method
