.class public LX/44j;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "LX/0Tf;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/0Tf;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 669909
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/0Tf;
    .locals 4

    .prologue
    .line 669896
    sget-object v0, LX/44j;->a:LX/0Tf;

    if-nez v0, :cond_1

    .line 669897
    const-class v1, LX/44j;

    monitor-enter v1

    .line 669898
    :try_start_0
    sget-object v0, LX/44j;->a:LX/0Tf;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 669899
    if-eqz v2, :cond_0

    .line 669900
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 669901
    const/16 v3, 0x1a1

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v3

    check-cast v3, LX/0So;

    invoke-static {p0, v3}, LX/0Su;->a(LX/0Or;LX/0So;)LX/0Tf;

    move-result-object v3

    move-object v0, v3

    .line 669902
    sput-object v0, LX/44j;->a:LX/0Tf;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 669903
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 669904
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 669905
    :cond_1
    sget-object v0, LX/44j;->a:LX/0Tf;

    return-object v0

    .line 669906
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 669907
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 669908
    const/16 v0, 0x1a1

    invoke-static {p0, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-static {p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v0

    check-cast v0, LX/0So;

    invoke-static {v1, v0}, LX/0Su;->a(LX/0Or;LX/0So;)LX/0Tf;

    move-result-object v0

    return-object v0
.end method
