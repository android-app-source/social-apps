.class public LX/4cU;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Lorg/apache/http/util/ByteArrayBuffer;

.field public static final b:Lorg/apache/http/util/ByteArrayBuffer;

.field private static final c:Lorg/apache/http/util/ByteArrayBuffer;


# instance fields
.field private final d:Ljava/lang/String;

.field private final e:Ljava/nio/charset/Charset;

.field public final f:Ljava/lang/String;

.field public final g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/4cQ;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/4cV;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 795102
    sget-object v0, LX/4cW;->a:Ljava/nio/charset/Charset;

    const-string v1, ": "

    invoke-static {v0, v1}, LX/4cU;->a(Ljava/nio/charset/Charset;Ljava/lang/String;)Lorg/apache/http/util/ByteArrayBuffer;

    move-result-object v0

    sput-object v0, LX/4cU;->a:Lorg/apache/http/util/ByteArrayBuffer;

    .line 795103
    sget-object v0, LX/4cW;->a:Ljava/nio/charset/Charset;

    const-string v1, "\r\n"

    invoke-static {v0, v1}, LX/4cU;->a(Ljava/nio/charset/Charset;Ljava/lang/String;)Lorg/apache/http/util/ByteArrayBuffer;

    move-result-object v0

    sput-object v0, LX/4cU;->b:Lorg/apache/http/util/ByteArrayBuffer;

    .line 795104
    sget-object v0, LX/4cW;->a:Ljava/nio/charset/Charset;

    const-string v1, "--"

    invoke-static {v0, v1}, LX/4cU;->a(Ljava/nio/charset/Charset;Ljava/lang/String;)Lorg/apache/http/util/ByteArrayBuffer;

    move-result-object v0

    sput-object v0, LX/4cU;->c:Lorg/apache/http/util/ByteArrayBuffer;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/nio/charset/Charset;Ljava/lang/String;LX/4cV;)V
    .locals 2
    .param p2    # Ljava/nio/charset/Charset;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 795090
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 795091
    if-nez p1, :cond_0

    .line 795092
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Multipart subtype may not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 795093
    :cond_0
    if-nez p3, :cond_1

    .line 795094
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Multipart boundary may not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 795095
    :cond_1
    iput-object p1, p0, LX/4cU;->d:Ljava/lang/String;

    .line 795096
    if-eqz p2, :cond_2

    :goto_0
    iput-object p2, p0, LX/4cU;->e:Ljava/nio/charset/Charset;

    .line 795097
    iput-object p3, p0, LX/4cU;->f:Ljava/lang/String;

    .line 795098
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/4cU;->g:Ljava/util/List;

    .line 795099
    iput-object p4, p0, LX/4cU;->h:LX/4cV;

    .line 795100
    return-void

    .line 795101
    :cond_2
    sget-object p2, LX/4cW;->a:Ljava/nio/charset/Charset;

    goto :goto_0
.end method

.method private static a(Ljava/nio/charset/Charset;Ljava/lang/String;)Lorg/apache/http/util/ByteArrayBuffer;
    .locals 4

    .prologue
    .line 795086
    invoke-static {p1}, Ljava/nio/CharBuffer;->wrap(Ljava/lang/CharSequence;)Ljava/nio/CharBuffer;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/nio/charset/Charset;->encode(Ljava/nio/CharBuffer;)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 795087
    new-instance v1, Lorg/apache/http/util/ByteArrayBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v2

    invoke-direct {v1, v2}, Lorg/apache/http/util/ByteArrayBuffer;-><init>(I)V

    .line 795088
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v2

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v3

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    invoke-virtual {v1, v2, v3, v0}, Lorg/apache/http/util/ByteArrayBuffer;->append([BII)V

    .line 795089
    return-object v1
.end method

.method public static a(LX/4cU;LX/4cV;Ljava/io/OutputStream;Z)V
    .locals 6

    .prologue
    .line 795035
    iget-object v0, p0, LX/4cU;->e:Ljava/nio/charset/Charset;

    .line 795036
    iget-object v1, p0, LX/4cU;->f:Ljava/lang/String;

    move-object v1, v1

    .line 795037
    invoke-static {v0, v1}, LX/4cU;->a(Ljava/nio/charset/Charset;Ljava/lang/String;)Lorg/apache/http/util/ByteArrayBuffer;

    move-result-object v2

    .line 795038
    iget-object v0, p0, LX/4cU;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4cQ;

    .line 795039
    sget-object v1, LX/4cU;->c:Lorg/apache/http/util/ByteArrayBuffer;

    invoke-static {v1, p2}, LX/4cU;->a(Lorg/apache/http/util/ByteArrayBuffer;Ljava/io/OutputStream;)V

    .line 795040
    invoke-static {v2, p2}, LX/4cU;->a(Lorg/apache/http/util/ByteArrayBuffer;Ljava/io/OutputStream;)V

    .line 795041
    sget-object v1, LX/4cU;->b:Lorg/apache/http/util/ByteArrayBuffer;

    invoke-static {v1, p2}, LX/4cU;->a(Lorg/apache/http/util/ByteArrayBuffer;Ljava/io/OutputStream;)V

    .line 795042
    iget-object v1, v0, LX/4cQ;->b:LX/4cR;

    move-object v1, v1

    .line 795043
    sget-object v4, LX/4cS;->a:[I

    invoke-virtual {p1}, LX/4cV;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 795044
    :cond_0
    :goto_1
    sget-object v1, LX/4cU;->b:Lorg/apache/http/util/ByteArrayBuffer;

    invoke-static {v1, p2}, LX/4cU;->a(Lorg/apache/http/util/ByteArrayBuffer;Ljava/io/OutputStream;)V

    .line 795045
    if-eqz p3, :cond_1

    .line 795046
    iget-object v1, v0, LX/4cQ;->c:LX/4cO;

    move-object v0, v1

    .line 795047
    invoke-virtual {v0, p2}, LX/4cO;->a(Ljava/io/OutputStream;)V

    .line 795048
    :cond_1
    sget-object v0, LX/4cU;->b:Lorg/apache/http/util/ByteArrayBuffer;

    invoke-static {v0, p2}, LX/4cU;->a(Lorg/apache/http/util/ByteArrayBuffer;Ljava/io/OutputStream;)V

    goto :goto_0

    .line 795049
    :pswitch_0
    invoke-virtual {v1}, LX/4cR;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/4cX;

    .line 795050
    iget-object v5, v1, LX/4cX;->a:Ljava/lang/String;

    move-object v5, v5

    .line 795051
    invoke-static {v5, p2}, LX/4cU;->a(Ljava/lang/String;Ljava/io/OutputStream;)V

    .line 795052
    sget-object v5, LX/4cU;->a:Lorg/apache/http/util/ByteArrayBuffer;

    invoke-static {v5, p2}, LX/4cU;->a(Lorg/apache/http/util/ByteArrayBuffer;Ljava/io/OutputStream;)V

    .line 795053
    iget-object v5, v1, LX/4cX;->b:Ljava/lang/String;

    move-object v5, v5

    .line 795054
    invoke-static {v5, p2}, LX/4cU;->a(Ljava/lang/String;Ljava/io/OutputStream;)V

    .line 795055
    sget-object v5, LX/4cU;->b:Lorg/apache/http/util/ByteArrayBuffer;

    invoke-static {v5, p2}, LX/4cU;->a(Lorg/apache/http/util/ByteArrayBuffer;Ljava/io/OutputStream;)V

    .line 795056
    goto :goto_2

    .line 795057
    :pswitch_1
    iget-object v1, v0, LX/4cQ;->b:LX/4cR;

    move-object v1, v1

    .line 795058
    const-string v4, "Content-Disposition"

    invoke-virtual {v1, v4}, LX/4cR;->a(Ljava/lang/String;)LX/4cX;

    move-result-object v1

    .line 795059
    iget-object v4, p0, LX/4cU;->e:Ljava/nio/charset/Charset;

    invoke-static {v1, v4, p2}, LX/4cU;->a(LX/4cX;Ljava/nio/charset/Charset;Ljava/io/OutputStream;)V

    .line 795060
    iget-object v1, v0, LX/4cQ;->c:LX/4cO;

    move-object v1, v1

    .line 795061
    invoke-virtual {v1}, LX/4cO;->a()Ljava/lang/String;

    move-result-object v1

    .line 795062
    if-eqz v1, :cond_0

    .line 795063
    iget-object v1, v0, LX/4cQ;->b:LX/4cR;

    move-object v1, v1

    .line 795064
    const-string v4, "Content-Type"

    invoke-virtual {v1, v4}, LX/4cR;->a(Ljava/lang/String;)LX/4cX;

    move-result-object v1

    .line 795065
    iget-object v4, p0, LX/4cU;->e:Ljava/nio/charset/Charset;

    invoke-static {v1, v4, p2}, LX/4cU;->a(LX/4cX;Ljava/nio/charset/Charset;Ljava/io/OutputStream;)V

    goto :goto_1

    .line 795066
    :cond_2
    sget-object v0, LX/4cU;->c:Lorg/apache/http/util/ByteArrayBuffer;

    invoke-static {v0, p2}, LX/4cU;->a(Lorg/apache/http/util/ByteArrayBuffer;Ljava/io/OutputStream;)V

    .line 795067
    invoke-static {v2, p2}, LX/4cU;->a(Lorg/apache/http/util/ByteArrayBuffer;Ljava/io/OutputStream;)V

    .line 795068
    sget-object v0, LX/4cU;->c:Lorg/apache/http/util/ByteArrayBuffer;

    invoke-static {v0, p2}, LX/4cU;->a(Lorg/apache/http/util/ByteArrayBuffer;Ljava/io/OutputStream;)V

    .line 795069
    sget-object v0, LX/4cU;->b:Lorg/apache/http/util/ByteArrayBuffer;

    invoke-static {v0, p2}, LX/4cU;->a(Lorg/apache/http/util/ByteArrayBuffer;Ljava/io/OutputStream;)V

    .line 795070
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static a(LX/4cX;Ljava/nio/charset/Charset;Ljava/io/OutputStream;)V
    .locals 1

    .prologue
    .line 795079
    iget-object v0, p0, LX/4cX;->a:Ljava/lang/String;

    move-object v0, v0

    .line 795080
    invoke-static {v0, p1, p2}, LX/4cU;->a(Ljava/lang/String;Ljava/nio/charset/Charset;Ljava/io/OutputStream;)V

    .line 795081
    sget-object v0, LX/4cU;->a:Lorg/apache/http/util/ByteArrayBuffer;

    invoke-static {v0, p2}, LX/4cU;->a(Lorg/apache/http/util/ByteArrayBuffer;Ljava/io/OutputStream;)V

    .line 795082
    iget-object v0, p0, LX/4cX;->b:Ljava/lang/String;

    move-object v0, v0

    .line 795083
    invoke-static {v0, p1, p2}, LX/4cU;->a(Ljava/lang/String;Ljava/nio/charset/Charset;Ljava/io/OutputStream;)V

    .line 795084
    sget-object v0, LX/4cU;->b:Lorg/apache/http/util/ByteArrayBuffer;

    invoke-static {v0, p2}, LX/4cU;->a(Lorg/apache/http/util/ByteArrayBuffer;Ljava/io/OutputStream;)V

    .line 795085
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/io/OutputStream;)V
    .locals 1

    .prologue
    .line 795076
    sget-object v0, LX/4cW;->a:Ljava/nio/charset/Charset;

    invoke-static {v0, p0}, LX/4cU;->a(Ljava/nio/charset/Charset;Ljava/lang/String;)Lorg/apache/http/util/ByteArrayBuffer;

    move-result-object v0

    .line 795077
    invoke-static {v0, p1}, LX/4cU;->a(Lorg/apache/http/util/ByteArrayBuffer;Ljava/io/OutputStream;)V

    .line 795078
    return-void
.end method

.method private static a(Ljava/lang/String;Ljava/nio/charset/Charset;Ljava/io/OutputStream;)V
    .locals 1

    .prologue
    .line 795073
    invoke-static {p1, p0}, LX/4cU;->a(Ljava/nio/charset/Charset;Ljava/lang/String;)Lorg/apache/http/util/ByteArrayBuffer;

    move-result-object v0

    .line 795074
    invoke-static {v0, p2}, LX/4cU;->a(Lorg/apache/http/util/ByteArrayBuffer;Ljava/io/OutputStream;)V

    .line 795075
    return-void
.end method

.method public static a(Lorg/apache/http/util/ByteArrayBuffer;Ljava/io/OutputStream;)V
    .locals 3

    .prologue
    .line 795071
    invoke-virtual {p0}, Lorg/apache/http/util/ByteArrayBuffer;->buffer()[B

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0}, Lorg/apache/http/util/ByteArrayBuffer;->length()I

    move-result v2

    invoke-virtual {p1, v0, v1, v2}, Ljava/io/OutputStream;->write([BII)V

    .line 795072
    return-void
.end method
