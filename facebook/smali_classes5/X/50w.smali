.class public final LX/50w;
.super LX/50v;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0kB",
        "<TR;TC;TV;>.TableSet<",
        "Ljava/util/Map$Entry",
        "<TR;",
        "Ljava/util/Map",
        "<TC;TV;>;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/50x;


# direct methods
.method public constructor <init>(LX/50x;)V
    .locals 2

    .prologue
    .line 824195
    iput-object p1, p0, LX/50w;->a:LX/50x;

    iget-object v0, p1, LX/50x;->a:LX/0kB;

    invoke-direct {p0, v0}, LX/50v;-><init>(LX/0kB;)V

    return-void
.end method


# virtual methods
.method public final contains(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 824191
    instance-of v1, p1, Ljava/util/Map$Entry;

    if-eqz v1, :cond_0

    .line 824192
    check-cast p1, Ljava/util/Map$Entry;

    .line 824193
    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Ljava/util/Map;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/50w;->a:LX/50x;

    iget-object v1, v1, LX/50x;->a:LX/0kB;

    iget-object v1, v1, LX/0kB;->backingMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-static {v1, p1}, LX/0PN;->a(Ljava/util/Collection;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 824194
    :cond_0
    return v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/util/Map$Entry",
            "<TR;",
            "Ljava/util/Map",
            "<TC;TV;>;>;>;"
        }
    .end annotation

    .prologue
    .line 824185
    iget-object v0, p0, LX/50w;->a:LX/50x;

    iget-object v0, v0, LX/50x;->a:LX/0kB;

    iget-object v0, v0, LX/0kB;->backingMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    new-instance v1, LX/50u;

    invoke-direct {v1, p0}, LX/50u;-><init>(LX/50w;)V

    invoke-static {v0, v1}, LX/0PM;->a(Ljava/util/Set;LX/0QK;)Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 824187
    instance-of v1, p1, Ljava/util/Map$Entry;

    if-eqz v1, :cond_0

    .line 824188
    check-cast p1, Ljava/util/Map$Entry;

    .line 824189
    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Ljava/util/Map;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/50w;->a:LX/50x;

    iget-object v1, v1, LX/50x;->a:LX/0kB;

    iget-object v1, v1, LX/0kB;->backingMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 824190
    :cond_0
    return v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 824186
    iget-object v0, p0, LX/50w;->a:LX/50x;

    iget-object v0, v0, LX/50x;->a:LX/0kB;

    iget-object v0, v0, LX/0kB;->backingMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    return v0
.end method
