.class public abstract LX/3Vv;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements LX/35o;
.implements LX/3Vw;
.implements LX/3Vx;


# instance fields
.field public b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public c:Landroid/widget/TextView;

.field public d:Landroid/widget/TextView;

.field public e:Landroid/widget/TextView;

.field public f:Landroid/widget/RatingBar;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 588879
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/3Vv;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 588880
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 588876
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 588877
    sget-object v0, LX/1vY;->ATTACHMENT:LX/1vY;

    invoke-static {p0, v0}, LX/1vZ;->a(Landroid/view/View;LX/1vY;)V

    .line 588878
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 588881
    invoke-virtual {p0, v1}, LX/3Vv;->setTitle(Ljava/lang/CharSequence;)V

    .line 588882
    invoke-virtual {p0, v1}, LX/3Vv;->setContextText(Ljava/lang/CharSequence;)V

    .line 588883
    invoke-virtual {p0, v1}, LX/3Vv;->setSubcontextText(Ljava/lang/CharSequence;)V

    .line 588884
    invoke-virtual {p0, v2}, LX/3Vv;->setShowRating(Z)V

    .line 588885
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/3Vv;->setRating(F)V

    .line 588886
    invoke-virtual {p0, v2}, LX/3Vv;->setNumberOfStars(I)V

    .line 588887
    invoke-virtual {p0, v1}, LX/3Vv;->setSideImageController(LX/1aZ;)V

    .line 588888
    return-void
.end method

.method public setContextText(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 588870
    iget-object v0, p0, LX/3Vv;->d:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 588871
    iget-object v1, p0, LX/3Vv;->d:Landroid/widget/TextView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 588872
    return-void

    .line 588873
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setNumberOfStars(I)V
    .locals 1

    .prologue
    .line 588874
    iget-object v0, p0, LX/3Vv;->f:Landroid/widget/RatingBar;

    invoke-virtual {v0, p1}, Landroid/widget/RatingBar;->setNumStars(I)V

    .line 588875
    return-void
.end method

.method public setRating(F)V
    .locals 1

    .prologue
    .line 588868
    iget-object v0, p0, LX/3Vv;->f:Landroid/widget/RatingBar;

    invoke-virtual {v0, p1}, Landroid/widget/RatingBar;->setRating(F)V

    .line 588869
    return-void
.end method

.method public setShowRating(Z)V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 588860
    if-nez p1, :cond_0

    .line 588861
    iget-object v0, p0, LX/3Vv;->f:Landroid/widget/RatingBar;

    move v1, v2

    .line 588862
    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/RatingBar;->setVisibility(I)V

    .line 588863
    return-void

    .line 588864
    :cond_0
    iget-object v3, p0, LX/3Vv;->c:Landroid/widget/TextView;

    if-eqz v3, :cond_1

    iget-object v3, p0, LX/3Vv;->c:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getLineCount()I

    move-result v3

    if-le v3, v0, :cond_1

    .line 588865
    :goto_1
    iget-object v3, p0, LX/3Vv;->f:Landroid/widget/RatingBar;

    if-eqz v0, :cond_2

    move v1, v2

    move-object v0, v3

    goto :goto_0

    :cond_1
    move v0, v1

    .line 588866
    goto :goto_1

    :cond_2
    move-object v0, v3

    .line 588867
    goto :goto_0
.end method

.method public setSideImageController(LX/1aZ;)V
    .locals 2
    .param p1    # LX/1aZ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 588848
    iget-object v1, p0, LX/3Vv;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 588849
    iget-object v0, p0, LX/3Vv;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 588850
    return-void

    .line 588851
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setSubcontextText(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 588856
    iget-object v0, p0, LX/3Vv;->e:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 588857
    iget-object v1, p0, LX/3Vv;->e:Landroid/widget/TextView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 588858
    return-void

    .line 588859
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 588852
    iget-object v0, p0, LX/3Vv;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 588853
    iget-object v1, p0, LX/3Vv;->c:Landroid/widget/TextView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 588854
    return-void

    .line 588855
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method
