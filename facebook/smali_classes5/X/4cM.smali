.class public LX/4cM;
.super LX/4cL;
.source ""

# interfaces
.implements LX/14e;


# instance fields
.field public a:LX/4cJ;

.field private final b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/0nA;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 794913
    invoke-direct {p0}, LX/4cL;-><init>()V

    .line 794914
    const/4 v0, 0x0

    iput-object v0, p0, LX/4cM;->a:LX/4cJ;

    .line 794915
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/4cM;->b:Ljava/util/ArrayList;

    .line 794916
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 2

    .prologue
    .line 794908
    monitor-enter p0

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    :try_start_0
    iget-object v0, p0, LX/4cM;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 794909
    iget-object v0, p0, LX/4cM;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0nA;

    invoke-virtual {v0}, LX/0nA;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 794910
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 794911
    :cond_0
    monitor-exit p0

    return-void

    .line 794912
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(LX/0n9;)V
    .locals 7

    .prologue
    .line 794917
    iget v0, p1, LX/0n9;->c:I

    move v2, v0

    .line 794918
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_4

    .line 794919
    invoke-virtual {p1, v1}, LX/0n9;->b(I)Ljava/lang/String;

    move-result-object v3

    .line 794920
    invoke-virtual {p1, v1}, LX/0n9;->c(I)Ljava/lang/Object;

    move-result-object v0

    .line 794921
    if-nez v0, :cond_0

    .line 794922
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "null values are not allowed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 794923
    :cond_0
    instance-of v4, v0, Ljava/lang/String;

    if-nez v4, :cond_1

    instance-of v4, v0, Ljava/lang/Number;

    if-nez v4, :cond_1

    instance-of v4, v0, Ljava/lang/Boolean;

    if-eqz v4, :cond_2

    .line 794924
    :cond_1
    new-instance v4, LX/4cY;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v5, LX/2aP;->UTF_8:Ljava/nio/charset/Charset;

    invoke-direct {v4, v0, v5}, LX/4cY;-><init>(Ljava/lang/String;Ljava/nio/charset/Charset;)V

    invoke-virtual {p0, v3, v4}, LX/4cL;->a(Ljava/lang/String;LX/4cO;)V

    .line 794925
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 794926
    :cond_2
    instance-of v4, v0, LX/0nA;

    if-eqz v4, :cond_3

    .line 794927
    new-instance v4, LX/4cP;

    check-cast v0, LX/0nA;

    const-string v5, "text/plain"

    sget-object v6, LX/2aP;->UTF_8:Ljava/nio/charset/Charset;

    invoke-direct {v4, v0, v5, v6}, LX/4cP;-><init>(LX/0nA;Ljava/lang/String;Ljava/nio/charset/Charset;)V

    .line 794928
    invoke-virtual {p0, v3, v4}, LX/4cL;->a(Ljava/lang/String;LX/4cO;)V

    goto :goto_1

    .line 794929
    :cond_3
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Unsupported params type "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " at key "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 794930
    :cond_4
    monitor-enter p0

    .line 794931
    :try_start_0
    iget-object v0, p0, LX/4cM;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 794932
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final writeTo(Ljava/io/OutputStream;)V
    .locals 2

    .prologue
    .line 794904
    iget-object v0, p0, LX/4cM;->a:LX/4cJ;

    if-eqz v0, :cond_0

    .line 794905
    new-instance v0, LX/4cN;

    iget-object v1, p0, LX/4cM;->a:LX/4cJ;

    invoke-direct {v0, p1, v1}, LX/4cN;-><init>(Ljava/io/OutputStream;LX/4cJ;)V

    move-object p1, v0

    .line 794906
    :cond_0
    invoke-super {p0, p1}, LX/4cL;->writeTo(Ljava/io/OutputStream;)V

    .line 794907
    return-void
.end method
