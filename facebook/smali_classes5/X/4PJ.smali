.class public LX/4PJ;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 700069
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 11

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 700070
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v3, :cond_9

    .line 700071
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 700072
    :goto_0
    return v0

    .line 700073
    :cond_0
    const-string v9, "viewer_has_voted"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 700074
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v2, v0

    move v0, v1

    .line 700075
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_7

    .line 700076
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 700077
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 700078
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_1

    if-eqz v8, :cond_1

    .line 700079
    const-string v9, "id"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 700080
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 700081
    :cond_2
    const-string v9, "media_question_option_order"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 700082
    invoke-static {p0, p1}, LX/4PL;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 700083
    :cond_3
    const-string v9, "media_question_photos"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 700084
    invoke-static {p0, p1}, LX/2sY;->b(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 700085
    :cond_4
    const-string v9, "media_question_type"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 700086
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 700087
    :cond_5
    const-string v9, "url"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 700088
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 700089
    :cond_6
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 700090
    :cond_7
    const/4 v8, 0x7

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 700091
    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 700092
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 700093
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 700094
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 700095
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 700096
    if-eqz v0, :cond_8

    .line 700097
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v2}, LX/186;->a(IZ)V

    .line 700098
    :cond_8
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :cond_9
    move v2, v0

    move v3, v0

    move v4, v0

    move v5, v0

    move v6, v0

    move v7, v0

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 700099
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 700100
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 700101
    if-eqz v0, :cond_0

    .line 700102
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 700103
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 700104
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 700105
    if-eqz v0, :cond_1

    .line 700106
    const-string v1, "media_question_option_order"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 700107
    invoke-static {p0, v0, p2, p3}, LX/4PL;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 700108
    :cond_1
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 700109
    if-eqz v0, :cond_2

    .line 700110
    const-string v1, "media_question_photos"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 700111
    invoke-static {p0, v0, p2, p3}, LX/2sY;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 700112
    :cond_2
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 700113
    if-eqz v0, :cond_3

    .line 700114
    const-string v1, "media_question_type"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 700115
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 700116
    :cond_3
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 700117
    if-eqz v0, :cond_4

    .line 700118
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 700119
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 700120
    :cond_4
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 700121
    if-eqz v0, :cond_5

    .line 700122
    const-string v1, "viewer_has_voted"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 700123
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 700124
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 700125
    return-void
.end method
