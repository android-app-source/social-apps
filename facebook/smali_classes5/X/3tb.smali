.class public LX/3tb;
.super LX/3tG;
.source ""


# instance fields
.field private final a:Landroid/widget/ListView;


# direct methods
.method public constructor <init>(Landroid/widget/ListView;)V
    .locals 0

    .prologue
    .line 645576
    invoke-direct {p0, p1}, LX/3tG;-><init>(Landroid/view/View;)V

    .line 645577
    iput-object p1, p0, LX/3tb;->a:Landroid/widget/ListView;

    .line 645578
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 3

    .prologue
    .line 645596
    iget-object v0, p0, LX/3tb;->a:Landroid/widget/ListView;

    .line 645597
    invoke-virtual {v0}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v1

    .line 645598
    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    .line 645599
    :cond_0
    :goto_0
    return-void

    .line 645600
    :cond_1
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 645601
    if-eqz v2, :cond_0

    .line 645602
    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v2

    sub-int/2addr v2, p1

    .line 645603
    invoke-virtual {v0, v1, v2}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 645595
    const/4 v0, 0x0

    return v0
.end method

.method public final b(I)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 645579
    iget-object v1, p0, LX/3tb;->a:Landroid/widget/ListView;

    .line 645580
    invoke-virtual {v1}, Landroid/widget/ListView;->getCount()I

    move-result v2

    .line 645581
    if-nez v2, :cond_1

    .line 645582
    :cond_0
    :goto_0
    return v0

    .line 645583
    :cond_1
    invoke-virtual {v1}, Landroid/widget/ListView;->getChildCount()I

    move-result v3

    .line 645584
    invoke-virtual {v1}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v4

    .line 645585
    add-int v5, v4, v3

    .line 645586
    if-lez p1, :cond_3

    .line 645587
    if-lt v5, v2, :cond_2

    .line 645588
    add-int/lit8 v2, v3, -0x1

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 645589
    invoke-virtual {v2}, Landroid/view/View;->getBottom()I

    move-result v2

    invoke-virtual {v1}, Landroid/widget/ListView;->getHeight()I

    move-result v1

    if-le v2, v1, :cond_0

    .line 645590
    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    .line 645591
    :cond_3
    if-gez p1, :cond_0

    .line 645592
    if-gtz v4, :cond_2

    .line 645593
    invoke-virtual {v1, v0}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 645594
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    if-ltz v1, :cond_2

    goto :goto_0
.end method
