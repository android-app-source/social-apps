.class public LX/45M;
.super LX/1SG;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1SG",
        "<",
        "LX/0Uq;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/45M;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0Uq;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0Uq;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 670286
    invoke-direct {p0}, LX/1SG;-><init>()V

    .line 670287
    new-instance v0, LX/45L;

    invoke-direct {v0, p0, p2, p1}, LX/45L;-><init>(LX/45M;LX/0Ot;LX/0Ot;)V

    iput-object v0, p0, LX/45M;->a:LX/0Ot;

    .line 670288
    return-void
.end method

.method public static a(LX/0QB;)LX/45M;
    .locals 5

    .prologue
    .line 670289
    sget-object v0, LX/45M;->b:LX/45M;

    if-nez v0, :cond_1

    .line 670290
    const-class v1, LX/45M;

    monitor-enter v1

    .line 670291
    :try_start_0
    sget-object v0, LX/45M;->b:LX/45M;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 670292
    if-eqz v2, :cond_0

    .line 670293
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 670294
    new-instance v3, LX/45M;

    const/16 v4, 0x29f

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 p0, 0x271

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, p0}, LX/45M;-><init>(LX/0Ot;LX/0Ot;)V

    .line 670295
    move-object v0, v3

    .line 670296
    sput-object v0, LX/45M;->b:LX/45M;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 670297
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 670298
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 670299
    :cond_1
    sget-object v0, LX/45M;->b:LX/45M;

    return-object v0

    .line 670300
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 670301
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final synthetic a()Ljava/util/concurrent/Future;
    .locals 1

    .prologue
    .line 670302
    invoke-virtual {p0}, LX/45M;->b()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final b()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0Uq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 670303
    iget-object v0, p0, LX/45M;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/util/concurrent/ListenableFuture;

    return-object v0
.end method

.method public final synthetic e()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 670304
    invoke-virtual {p0}, LX/45M;->b()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
