.class public LX/3fQ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field public final a:LX/CIw;

.field public final b:LX/Btm;

.field public final c:LX/3BI;

.field public final d:LX/3BL;

.field public final e:LX/17V;


# direct methods
.method public constructor <init>(LX/CIw;LX/Btm;LX/3BI;LX/3BL;LX/17V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 622112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 622113
    iput-object p1, p0, LX/3fQ;->a:LX/CIw;

    .line 622114
    iput-object p2, p0, LX/3fQ;->b:LX/Btm;

    .line 622115
    iput-object p3, p0, LX/3fQ;->c:LX/3BI;

    .line 622116
    iput-object p4, p0, LX/3fQ;->d:LX/3BL;

    .line 622117
    iput-object p5, p0, LX/3fQ;->e:LX/17V;

    .line 622118
    return-void
.end method

.method public static a(LX/0QB;)LX/3fQ;
    .locals 9

    .prologue
    .line 622119
    const-class v1, LX/3fQ;

    monitor-enter v1

    .line 622120
    :try_start_0
    sget-object v0, LX/3fQ;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 622121
    sput-object v2, LX/3fQ;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 622122
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 622123
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 622124
    new-instance v3, LX/3fQ;

    invoke-static {v0}, LX/CIw;->a(LX/0QB;)LX/CIw;

    move-result-object v4

    check-cast v4, LX/CIw;

    invoke-static {v0}, LX/Btm;->a(LX/0QB;)LX/Btm;

    move-result-object v5

    check-cast v5, LX/Btm;

    invoke-static {v0}, LX/3BI;->a(LX/0QB;)LX/3BI;

    move-result-object v6

    check-cast v6, LX/3BI;

    invoke-static {v0}, LX/3BL;->a(LX/0QB;)LX/3BL;

    move-result-object v7

    check-cast v7, LX/3BL;

    invoke-static {v0}, LX/17V;->b(LX/0QB;)LX/17V;

    move-result-object v8

    check-cast v8, LX/17V;

    invoke-direct/range {v3 .. v8}, LX/3fQ;-><init>(LX/CIw;LX/Btm;LX/3BI;LX/3BL;LX/17V;)V

    .line 622125
    move-object v0, v3

    .line 622126
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 622127
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/3fQ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 622128
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 622129
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/3BI;Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLPlace;
    .locals 2

    .prologue
    .line 622130
    invoke-static {p1}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 622131
    if-eqz v0, :cond_0

    invoke-static {p1}, LX/3BI;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 622132
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-static {v0}, LX/3BI;->a(Lcom/facebook/graphql/model/GraphQLNode;)Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v0

    .line 622133
    :goto_0
    return-object v0

    .line 622134
    :cond_0
    invoke-static {p1}, LX/16z;->l(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 622135
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->X()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v0

    goto :goto_0

    .line 622136
    :cond_1
    invoke-static {p1}, LX/16z;->m(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 622137
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-static {v0}, LX/3BI;->a(Lcom/facebook/graphql/model/GraphQLNode;)Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v0

    goto :goto_0

    .line 622138
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
