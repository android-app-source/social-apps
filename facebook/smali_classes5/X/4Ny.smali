.class public LX/4Ny;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 695107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 19

    .prologue
    .line 695108
    const/4 v15, 0x0

    .line 695109
    const/4 v14, 0x0

    .line 695110
    const/4 v13, 0x0

    .line 695111
    const/4 v12, 0x0

    .line 695112
    const/4 v11, 0x0

    .line 695113
    const/4 v10, 0x0

    .line 695114
    const/4 v9, 0x0

    .line 695115
    const/4 v8, 0x0

    .line 695116
    const/4 v7, 0x0

    .line 695117
    const/4 v6, 0x0

    .line 695118
    const/4 v5, 0x0

    .line 695119
    const/4 v4, 0x0

    .line 695120
    const/4 v3, 0x0

    .line 695121
    const/4 v2, 0x0

    .line 695122
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v16

    sget-object v17, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    if-eq v0, v1, :cond_1

    .line 695123
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 695124
    const/4 v2, 0x0

    .line 695125
    :goto_0
    return v2

    .line 695126
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 695127
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v16

    sget-object v17, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    if-eq v0, v1, :cond_e

    .line 695128
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v16

    .line 695129
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 695130
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v17

    sget-object v18, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    if-eq v0, v1, :cond_1

    if-eqz v16, :cond_1

    .line 695131
    const-string v17, "comments"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_2

    .line 695132
    invoke-static/range {p0 .. p1}, LX/2sx;->b(LX/15w;LX/186;)I

    move-result v15

    goto :goto_1

    .line 695133
    :cond_2
    const-string v17, "connected_friends"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_3

    .line 695134
    invoke-static/range {p0 .. p1}, LX/4Nl;->a(LX/15w;LX/186;)I

    move-result v14

    goto :goto_1

    .line 695135
    :cond_3
    const-string v17, "info_snippets"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_4

    .line 695136
    invoke-static/range {p0 .. p1}, LX/4O1;->b(LX/15w;LX/186;)I

    move-result v13

    goto :goto_1

    .line 695137
    :cond_4
    const-string v17, "lineage_snippets"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_5

    .line 695138
    invoke-static/range {p0 .. p1}, LX/4O1;->b(LX/15w;LX/186;)I

    move-result v12

    goto :goto_1

    .line 695139
    :cond_5
    const-string v17, "match_words"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_6

    .line 695140
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v11

    goto :goto_1

    .line 695141
    :cond_6
    const-string v17, "ordered_snippets"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_7

    .line 695142
    invoke-static/range {p0 .. p1}, LX/4O1;->b(LX/15w;LX/186;)I

    move-result v10

    goto :goto_1

    .line 695143
    :cond_7
    const-string v17, "snippet_source"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_8

    .line 695144
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto/16 :goto_1

    .line 695145
    :cond_8
    const-string v17, "social_snippet"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_9

    .line 695146
    invoke-static/range {p0 .. p1}, LX/4O1;->a(LX/15w;LX/186;)I

    move-result v8

    goto/16 :goto_1

    .line 695147
    :cond_9
    const-string v17, "summary_snippet"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_a

    .line 695148
    invoke-static/range {p0 .. p1}, LX/4O1;->a(LX/15w;LX/186;)I

    move-result v7

    goto/16 :goto_1

    .line 695149
    :cond_a
    const-string v17, "show_author_annotation"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_b

    .line 695150
    const/4 v2, 0x1

    .line 695151
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    goto/16 :goto_1

    .line 695152
    :cond_b
    const-string v17, "annotation_text"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_c

    .line 695153
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto/16 :goto_1

    .line 695154
    :cond_c
    const-string v17, "annotation_type"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_d

    .line 695155
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto/16 :goto_1

    .line 695156
    :cond_d
    const-string v17, "highlight_snippets"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_0

    .line 695157
    invoke-static/range {p0 .. p1}, LX/4Nn;->a(LX/15w;LX/186;)I

    move-result v3

    goto/16 :goto_1

    .line 695158
    :cond_e
    const/16 v16, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 695159
    const/16 v16, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1, v15}, LX/186;->b(II)V

    .line 695160
    const/4 v15, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v14}, LX/186;->b(II)V

    .line 695161
    const/4 v14, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v13}, LX/186;->b(II)V

    .line 695162
    const/4 v13, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v12}, LX/186;->b(II)V

    .line 695163
    const/4 v12, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v11}, LX/186;->b(II)V

    .line 695164
    const/4 v11, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v10}, LX/186;->b(II)V

    .line 695165
    const/4 v10, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v9}, LX/186;->b(II)V

    .line 695166
    const/4 v9, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v8}, LX/186;->b(II)V

    .line 695167
    const/16 v8, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v7}, LX/186;->b(II)V

    .line 695168
    if-eqz v2, :cond_f

    .line 695169
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->a(IZ)V

    .line 695170
    :cond_f
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 695171
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 695172
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 695173
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x4

    .line 695174
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 695175
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 695176
    if-eqz v0, :cond_0

    .line 695177
    const-string v1, "comments"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 695178
    invoke-static {p0, v0, p2, p3}, LX/2sx;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 695179
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 695180
    if-eqz v0, :cond_1

    .line 695181
    const-string v1, "connected_friends"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 695182
    invoke-static {p0, v0, p2, p3}, LX/4Nl;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 695183
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 695184
    if-eqz v0, :cond_2

    .line 695185
    const-string v1, "info_snippets"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 695186
    invoke-static {p0, v0, p2, p3}, LX/4O1;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 695187
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 695188
    if-eqz v0, :cond_3

    .line 695189
    const-string v1, "lineage_snippets"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 695190
    invoke-static {p0, v0, p2, p3}, LX/4O1;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 695191
    :cond_3
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 695192
    if-eqz v0, :cond_4

    .line 695193
    const-string v0, "match_words"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 695194
    invoke-virtual {p0, p1, v2}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 695195
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 695196
    if-eqz v0, :cond_5

    .line 695197
    const-string v1, "ordered_snippets"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 695198
    invoke-static {p0, v0, p2, p3}, LX/4O1;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 695199
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 695200
    if-eqz v0, :cond_6

    .line 695201
    const-string v1, "snippet_source"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 695202
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 695203
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 695204
    if-eqz v0, :cond_7

    .line 695205
    const-string v1, "social_snippet"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 695206
    invoke-static {p0, v0, p2, p3}, LX/4O1;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 695207
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 695208
    if-eqz v0, :cond_8

    .line 695209
    const-string v1, "summary_snippet"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 695210
    invoke-static {p0, v0, p2, p3}, LX/4O1;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 695211
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 695212
    if-eqz v0, :cond_9

    .line 695213
    const-string v1, "show_author_annotation"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 695214
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 695215
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 695216
    if-eqz v0, :cond_a

    .line 695217
    const-string v1, "annotation_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 695218
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 695219
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 695220
    if-eqz v0, :cond_b

    .line 695221
    const-string v1, "annotation_type"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 695222
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 695223
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 695224
    if-eqz v0, :cond_d

    .line 695225
    const-string v1, "highlight_snippets"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 695226
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 695227
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_c

    .line 695228
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/4Nn;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 695229
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 695230
    :cond_c
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 695231
    :cond_d
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 695232
    return-void
.end method
