.class public LX/3wG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3vk;


# instance fields
.field public a:Landroid/support/v7/widget/Toolbar;

.field private b:I

.field private c:Landroid/view/View;

.field public d:LX/3w4;

.field private e:Landroid/view/View;

.field private f:Landroid/graphics/drawable/Drawable;

.field private g:Landroid/graphics/drawable/Drawable;

.field private h:Landroid/graphics/drawable/Drawable;

.field private i:Z

.field public j:Ljava/lang/CharSequence;

.field private k:Ljava/lang/CharSequence;

.field public l:Ljava/lang/CharSequence;

.field public m:Landroid/view/Window$Callback;

.field public n:Z

.field private o:LX/3wb;

.field private p:I

.field private final q:LX/3wA;

.field public r:I

.field public s:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/support/v7/widget/Toolbar;Z)V
    .locals 2

    .prologue
    .line 654833
    const v0, 0x7f080003

    const v1, 0x7f020014

    invoke-direct {p0, p1, p2, v0, v1}, LX/3wG;-><init>(Landroid/support/v7/widget/Toolbar;ZII)V

    .line 654834
    return-void
.end method

.method private constructor <init>(Landroid/support/v7/widget/Toolbar;ZII)V
    .locals 6

    .prologue
    const/4 v5, -0x1

    const/4 v1, 0x0

    .line 654732
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 654733
    iput v1, p0, LX/3wG;->p:I

    .line 654734
    iput v1, p0, LX/3wG;->r:I

    .line 654735
    iput-object p1, p0, LX/3wG;->a:Landroid/support/v7/widget/Toolbar;

    .line 654736
    iget-object v0, p1, Landroid/support/v7/widget/Toolbar;->v:Ljava/lang/CharSequence;

    move-object v0, v0

    .line 654737
    iput-object v0, p0, LX/3wG;->j:Ljava/lang/CharSequence;

    .line 654738
    iget-object v0, p1, Landroid/support/v7/widget/Toolbar;->w:Ljava/lang/CharSequence;

    move-object v0, v0

    .line 654739
    iput-object v0, p0, LX/3wG;->k:Ljava/lang/CharSequence;

    .line 654740
    iget-object v0, p0, LX/3wG;->j:Ljava/lang/CharSequence;

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/3wG;->i:Z

    .line 654741
    invoke-virtual {p1}, Landroid/support/v7/widget/Toolbar;->getNavigationIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, LX/3wG;->h:Landroid/graphics/drawable/Drawable;

    .line 654742
    if-eqz p2, :cond_f

    .line 654743
    invoke-virtual {p1}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v2, 0x0

    sget-object v3, LX/03r;->ActionBar:[I

    const v4, 0x7f010011

    invoke-static {v0, v2, v3, v4, v1}, LX/3wC;->a(Landroid/content/Context;Landroid/util/AttributeSet;[III)LX/3wC;

    move-result-object v0

    .line 654744
    const/16 v2, 0x18

    invoke-virtual {v0, v2}, LX/3wC;->b(I)Ljava/lang/CharSequence;

    move-result-object v2

    .line 654745
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 654746
    invoke-virtual {p0, v2}, LX/3wG;->b(Ljava/lang/CharSequence;)V

    .line 654747
    :cond_0
    const/16 v2, 0x19

    invoke-virtual {v0, v2}, LX/3wC;->b(I)Ljava/lang/CharSequence;

    move-result-object v2

    .line 654748
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 654749
    invoke-virtual {p0, v2}, LX/3wG;->c(Ljava/lang/CharSequence;)V

    .line 654750
    :cond_1
    const/16 v2, 0x7

    invoke-virtual {v0, v2}, LX/3wC;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 654751
    if-eqz v2, :cond_2

    .line 654752
    invoke-direct {p0, v2}, LX/3wG;->d(Landroid/graphics/drawable/Drawable;)V

    .line 654753
    :cond_2
    const/16 v2, 0x6

    invoke-virtual {v0, v2}, LX/3wC;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 654754
    iget-object v3, p0, LX/3wG;->h:Landroid/graphics/drawable/Drawable;

    if-nez v3, :cond_3

    if-eqz v2, :cond_3

    .line 654755
    invoke-virtual {p0, v2}, LX/3wG;->a(Landroid/graphics/drawable/Drawable;)V

    .line 654756
    :cond_3
    const/16 v2, 0x1

    invoke-virtual {v0, v2}, LX/3wC;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 654757
    if-eqz v2, :cond_4

    .line 654758
    invoke-virtual {p0, v2}, LX/3wG;->b(Landroid/graphics/drawable/Drawable;)V

    .line 654759
    :cond_4
    const/16 v2, 0x3

    invoke-virtual {v0, v2, v1}, LX/3wC;->a(II)I

    move-result v2

    invoke-virtual {p0, v2}, LX/3wG;->c(I)V

    .line 654760
    const/16 v2, 0xb

    invoke-virtual {v0, v2, v1}, LX/3wC;->f(II)I

    move-result v2

    .line 654761
    if-eqz v2, :cond_5

    .line 654762
    iget-object v3, p0, LX/3wG;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v3}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    iget-object v4, p0, LX/3wG;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v3, v2, v4, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/3wG;->a(Landroid/view/View;)V

    .line 654763
    iget v2, p0, LX/3wG;->b:I

    or-int/lit8 v2, v2, 0x10

    invoke-virtual {p0, v2}, LX/3wG;->c(I)V

    .line 654764
    :cond_5
    const/16 v2, 0x0

    invoke-virtual {v0, v2, v1}, LX/3wC;->e(II)I

    move-result v2

    .line 654765
    if-lez v2, :cond_6

    .line 654766
    iget-object v3, p0, LX/3wG;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v3}, Landroid/support/v7/widget/Toolbar;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    .line 654767
    iput v2, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 654768
    iget-object v2, p0, LX/3wG;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/Toolbar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 654769
    :cond_6
    const/16 v2, 0x12

    invoke-virtual {v0, v2, v5}, LX/3wC;->c(II)I

    move-result v2

    .line 654770
    const/16 v3, 0x13

    invoke-virtual {v0, v3, v5}, LX/3wC;->c(II)I

    move-result v3

    .line 654771
    if-gez v2, :cond_7

    if-ltz v3, :cond_8

    .line 654772
    :cond_7
    iget-object v4, p0, LX/3wG;->a:Landroid/support/v7/widget/Toolbar;

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-static {v3, v1}, Ljava/lang/Math;->max(II)I

    move-result v3

    invoke-virtual {v4, v2, v3}, Landroid/support/v7/widget/Toolbar;->a(II)V

    .line 654773
    :cond_8
    const/16 v2, 0x4

    invoke-virtual {v0, v2, v1}, LX/3wC;->f(II)I

    move-result v2

    .line 654774
    if-eqz v2, :cond_9

    .line 654775
    iget-object v3, p0, LX/3wG;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v4, p0, LX/3wG;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v4}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v4, v2}, Landroid/support/v7/widget/Toolbar;->a(Landroid/content/Context;I)V

    .line 654776
    :cond_9
    const/16 v2, 0x5

    invoke-virtual {v0, v2, v1}, LX/3wC;->f(II)I

    move-result v2

    .line 654777
    if-eqz v2, :cond_a

    .line 654778
    iget-object v3, p0, LX/3wG;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v4, p0, LX/3wG;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v4}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v4, v2}, Landroid/support/v7/widget/Toolbar;->b(Landroid/content/Context;I)V

    .line 654779
    :cond_a
    const/16 v2, 0x17

    invoke-virtual {v0, v2, v1}, LX/3wC;->f(II)I

    move-result v1

    .line 654780
    if-eqz v1, :cond_b

    .line 654781
    iget-object v2, p0, LX/3wG;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v2, v1}, Landroid/support/v7/widget/Toolbar;->setPopupTheme(I)V

    .line 654782
    :cond_b
    invoke-virtual {v0}, LX/3wC;->b()V

    .line 654783
    invoke-virtual {v0}, LX/3wC;->c()LX/3wA;

    move-result-object v0

    iput-object v0, p0, LX/3wG;->q:LX/3wA;

    .line 654784
    :goto_1
    iget v0, p0, LX/3wG;->r:I

    if-ne p3, v0, :cond_11

    .line 654785
    :cond_c
    :goto_2
    iget-object v0, p0, LX/3wG;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->getNavigationContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, LX/3wG;->l:Ljava/lang/CharSequence;

    .line 654786
    iget-object v0, p0, LX/3wG;->q:LX/3wA;

    invoke-virtual {v0, p4}, LX/3wA;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 654787
    iget-object v1, p0, LX/3wG;->s:Landroid/graphics/drawable/Drawable;

    if-eq v1, v0, :cond_d

    .line 654788
    iput-object v0, p0, LX/3wG;->s:Landroid/graphics/drawable/Drawable;

    .line 654789
    invoke-static {p0}, LX/3wG;->v(LX/3wG;)V

    .line 654790
    :cond_d
    iget-object v0, p0, LX/3wG;->a:Landroid/support/v7/widget/Toolbar;

    new-instance v1, LX/3wD;

    invoke-direct {v1, p0}, LX/3wD;-><init>(LX/3wG;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setNavigationOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 654791
    return-void

    :cond_e
    move v0, v1

    .line 654792
    goto/16 :goto_0

    .line 654793
    :cond_f
    const/16 v0, 0xb

    .line 654794
    iget-object v1, p0, LX/3wG;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v1}, Landroid/support/v7/widget/Toolbar;->getNavigationIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_10

    .line 654795
    const/16 v0, 0xf

    .line 654796
    :cond_10
    move v0, v0

    .line 654797
    iput v0, p0, LX/3wG;->b:I

    .line 654798
    new-instance v0, LX/3wA;

    invoke-virtual {p1}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/3wA;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/3wG;->q:LX/3wA;

    goto :goto_1

    .line 654799
    :cond_11
    iput p3, p0, LX/3wG;->r:I

    .line 654800
    iget-object v0, p0, LX/3wG;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->getNavigationContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 654801
    iget v0, p0, LX/3wG;->r:I

    .line 654802
    if-nez v0, :cond_12

    const/4 p3, 0x0

    .line 654803
    :goto_3
    iput-object p3, p0, LX/3wG;->l:Ljava/lang/CharSequence;

    .line 654804
    invoke-static {p0}, LX/3wG;->u(LX/3wG;)V

    .line 654805
    goto :goto_2

    .line 654806
    :cond_12
    invoke-virtual {p0}, LX/3wG;->b()Landroid/content/Context;

    move-result-object p3

    invoke-virtual {p3, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p3

    goto :goto_3
.end method

.method private d(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 654835
    iput-object p1, p0, LX/3wG;->g:Landroid/graphics/drawable/Drawable;

    .line 654836
    invoke-direct {p0}, LX/3wG;->s()V

    .line 654837
    return-void
.end method

.method private d(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 654838
    iput-object p1, p0, LX/3wG;->j:Ljava/lang/CharSequence;

    .line 654839
    iget v0, p0, LX/3wG;->b:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    .line 654840
    iget-object v0, p0, LX/3wG;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/Toolbar;->setTitle(Ljava/lang/CharSequence;)V

    .line 654841
    :cond_0
    return-void
.end method

.method private s()V
    .locals 2

    .prologue
    .line 654842
    const/4 v0, 0x0

    .line 654843
    iget v1, p0, LX/3wG;->b:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_0

    .line 654844
    iget v0, p0, LX/3wG;->b:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 654845
    iget-object v0, p0, LX/3wG;->g:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/3wG;->g:Landroid/graphics/drawable/Drawable;

    .line 654846
    :cond_0
    :goto_0
    iget-object v1, p0, LX/3wG;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/Toolbar;->setLogo(Landroid/graphics/drawable/Drawable;)V

    .line 654847
    return-void

    .line 654848
    :cond_1
    iget-object v0, p0, LX/3wG;->f:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 654849
    :cond_2
    iget-object v0, p0, LX/3wG;->f:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method public static u(LX/3wG;)V
    .locals 2

    .prologue
    .line 654850
    iget v0, p0, LX/3wG;->b:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    .line 654851
    iget-object v0, p0, LX/3wG;->l:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 654852
    iget-object v0, p0, LX/3wG;->a:Landroid/support/v7/widget/Toolbar;

    iget v1, p0, LX/3wG;->r:I

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setNavigationContentDescription(I)V

    .line 654853
    :cond_0
    :goto_0
    return-void

    .line 654854
    :cond_1
    iget-object v0, p0, LX/3wG;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, LX/3wG;->l:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setNavigationContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public static v(LX/3wG;)V
    .locals 2

    .prologue
    .line 654855
    iget v0, p0, LX/3wG;->b:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    .line 654856
    iget-object v1, p0, LX/3wG;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v0, p0, LX/3wG;->h:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/3wG;->h:Landroid/graphics/drawable/Drawable;

    :goto_0
    invoke-virtual {v1, v0}, Landroid/support/v7/widget/Toolbar;->setNavigationIcon(Landroid/graphics/drawable/Drawable;)V

    .line 654857
    :cond_0
    return-void

    .line 654858
    :cond_1
    iget-object v0, p0, LX/3wG;->s:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method


# virtual methods
.method public final a()Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 654859
    iget-object v0, p0, LX/3wG;->a:Landroid/support/v7/widget/Toolbar;

    return-object v0
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 654860
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/3wG;->q:LX/3wA;

    invoke-virtual {v0, p1}, LX/3wA;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_0
    invoke-virtual {p0, v0}, LX/3wG;->a(Landroid/graphics/drawable/Drawable;)V

    .line 654861
    return-void

    .line 654862
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/3vu;)V
    .locals 4

    .prologue
    const/4 v3, -0x2

    .line 654863
    iget-object v0, p0, LX/3wG;->c:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3wG;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    iget-object v1, p0, LX/3wG;->a:Landroid/support/v7/widget/Toolbar;

    if-ne v0, v1, :cond_0

    .line 654864
    iget-object v0, p0, LX/3wG;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, LX/3wG;->c:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->removeView(Landroid/view/View;)V

    .line 654865
    :cond_0
    iput-object p1, p0, LX/3wG;->c:Landroid/view/View;

    .line 654866
    if-eqz p1, :cond_1

    iget v0, p0, LX/3wG;->p:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 654867
    iget-object v0, p0, LX/3wG;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, LX/3wG;->c:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/widget/Toolbar;->addView(Landroid/view/View;I)V

    .line 654868
    iget-object v0, p0, LX/3wG;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/3xa;

    .line 654869
    iput v3, v0, LX/3xa;->width:I

    .line 654870
    iput v3, v0, LX/3xa;->height:I

    .line 654871
    const v1, 0x800053

    iput v1, v0, LX/3xa;->a:I

    .line 654872
    const/4 v0, 0x1

    .line 654873
    iput-boolean v0, p1, LX/3vu;->i:Z

    .line 654874
    :cond_1
    return-void
.end method

.method public final a(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 654903
    iput-object p1, p0, LX/3wG;->f:Landroid/graphics/drawable/Drawable;

    .line 654904
    invoke-direct {p0}, LX/3wG;->s()V

    .line 654905
    return-void
.end method

.method public final a(Landroid/view/Menu;LX/3uE;)V
    .locals 2

    .prologue
    .line 654875
    iget-object v0, p0, LX/3wG;->o:LX/3wb;

    if-nez v0, :cond_0

    .line 654876
    new-instance v0, LX/3wb;

    iget-object v1, p0, LX/3wG;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v1}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/3wb;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/3wG;->o:LX/3wb;

    .line 654877
    iget-object v0, p0, LX/3wG;->o:LX/3wb;

    const v1, 0x7f0d0006

    .line 654878
    iput v1, v0, LX/3ut;->j:I

    .line 654879
    :cond_0
    iget-object v0, p0, LX/3wG;->o:LX/3wb;

    .line 654880
    iput-object p2, v0, LX/3ut;->g:LX/3uE;

    .line 654881
    iget-object v0, p0, LX/3wG;->a:Landroid/support/v7/widget/Toolbar;

    check-cast p1, LX/3v0;

    iget-object v1, p0, LX/3wG;->o:LX/3wb;

    invoke-virtual {v0, p1, v1}, Landroid/support/v7/widget/Toolbar;->a(LX/3v0;LX/3wb;)V

    .line 654882
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 654883
    iget-object v0, p0, LX/3wG;->e:Landroid/view/View;

    if-eqz v0, :cond_0

    iget v0, p0, LX/3wG;->b:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    .line 654884
    iget-object v0, p0, LX/3wG;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, LX/3wG;->e:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->removeView(Landroid/view/View;)V

    .line 654885
    :cond_0
    iput-object p1, p0, LX/3wG;->e:Landroid/view/View;

    .line 654886
    if-eqz p1, :cond_1

    iget v0, p0, LX/3wG;->b:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_1

    .line 654887
    iget-object v0, p0, LX/3wG;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, LX/3wG;->e:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->addView(Landroid/view/View;)V

    .line 654888
    :cond_1
    return-void
.end method

.method public final a(Landroid/view/Window$Callback;)V
    .locals 0

    .prologue
    .line 654889
    iput-object p1, p0, LX/3wG;->m:Landroid/view/Window$Callback;

    .line 654890
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 654891
    iget-boolean v0, p0, LX/3wG;->i:Z

    if-nez v0, :cond_0

    .line 654892
    invoke-direct {p0, p1}, LX/3wG;->d(Ljava/lang/CharSequence;)V

    .line 654893
    :cond_0
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 654894
    iget-object v0, p0, LX/3wG;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/Toolbar;->setCollapsible(Z)V

    .line 654895
    return-void
.end method

.method public final b()Landroid/content/Context;
    .locals 1

    .prologue
    .line 654896
    iget-object v0, p0, LX/3wG;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 654897
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/3wG;->q:LX/3wA;

    invoke-virtual {v0, p1}, LX/3wA;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_0
    invoke-direct {p0, v0}, LX/3wG;->d(Landroid/graphics/drawable/Drawable;)V

    .line 654898
    return-void

    .line 654899
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 654900
    iput-object p1, p0, LX/3wG;->h:Landroid/graphics/drawable/Drawable;

    .line 654901
    invoke-static {p0}, LX/3wG;->v(LX/3wG;)V

    .line 654902
    return-void
.end method

.method public final b(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 654808
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/3wG;->i:Z

    .line 654809
    invoke-direct {p0, p1}, LX/3wG;->d(Ljava/lang/CharSequence;)V

    .line 654810
    return-void
.end method

.method public final c(I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 654811
    iget v0, p0, LX/3wG;->b:I

    .line 654812
    xor-int/2addr v0, p1

    .line 654813
    iput p1, p0, LX/3wG;->b:I

    .line 654814
    if-eqz v0, :cond_3

    .line 654815
    and-int/lit8 v1, v0, 0x4

    if-eqz v1, :cond_0

    .line 654816
    and-int/lit8 v1, p1, 0x4

    if-eqz v1, :cond_4

    .line 654817
    invoke-static {p0}, LX/3wG;->v(LX/3wG;)V

    .line 654818
    invoke-static {p0}, LX/3wG;->u(LX/3wG;)V

    .line 654819
    :cond_0
    :goto_0
    and-int/lit8 v1, v0, 0x3

    if-eqz v1, :cond_1

    .line 654820
    invoke-direct {p0}, LX/3wG;->s()V

    .line 654821
    :cond_1
    and-int/lit8 v1, v0, 0x8

    if-eqz v1, :cond_2

    .line 654822
    and-int/lit8 v1, p1, 0x8

    if-eqz v1, :cond_5

    .line 654823
    iget-object v1, p0, LX/3wG;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v2, p0, LX/3wG;->j:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/Toolbar;->setTitle(Ljava/lang/CharSequence;)V

    .line 654824
    iget-object v1, p0, LX/3wG;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v2, p0, LX/3wG;->k:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/Toolbar;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 654825
    :cond_2
    :goto_1
    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/3wG;->e:Landroid/view/View;

    if-eqz v0, :cond_3

    .line 654826
    and-int/lit8 v0, p1, 0x10

    if-eqz v0, :cond_6

    .line 654827
    iget-object v0, p0, LX/3wG;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, LX/3wG;->e:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->addView(Landroid/view/View;)V

    .line 654828
    :cond_3
    :goto_2
    return-void

    .line 654829
    :cond_4
    iget-object v1, p0, LX/3wG;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/Toolbar;->setNavigationIcon(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 654830
    :cond_5
    iget-object v1, p0, LX/3wG;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/Toolbar;->setTitle(Ljava/lang/CharSequence;)V

    .line 654831
    iget-object v1, p0, LX/3wG;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/Toolbar;->setSubtitle(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 654832
    :cond_6
    iget-object v0, p0, LX/3wG;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, LX/3wG;->e:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->removeView(Landroid/view/View;)V

    goto :goto_2
.end method

.method public final c(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 654672
    iput-object p1, p0, LX/3wG;->k:Ljava/lang/CharSequence;

    .line 654673
    iget v0, p0, LX/3wG;->b:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    .line 654674
    iget-object v0, p0, LX/3wG;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/Toolbar;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 654675
    :cond_0
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 654676
    const/4 v0, 0x0

    return v0
.end method

.method public final d(I)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x2

    .line 654677
    iget v0, p0, LX/3wG;->p:I

    .line 654678
    if-eq p1, v0, :cond_2

    .line 654679
    packed-switch v0, :pswitch_data_0

    .line 654680
    :cond_0
    :goto_0
    iput p1, p0, LX/3wG;->p:I

    .line 654681
    packed-switch p1, :pswitch_data_1

    .line 654682
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid navigation mode "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 654683
    :pswitch_0
    iget-object v0, p0, LX/3wG;->d:LX/3w4;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3wG;->d:LX/3w4;

    invoke-virtual {v0}, LX/3w4;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    iget-object v1, p0, LX/3wG;->a:Landroid/support/v7/widget/Toolbar;

    if-ne v0, v1, :cond_0

    .line 654684
    iget-object v0, p0, LX/3wG;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, LX/3wG;->d:LX/3w4;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->removeView(Landroid/view/View;)V

    goto :goto_0

    .line 654685
    :pswitch_1
    iget-object v0, p0, LX/3wG;->c:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3wG;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    iget-object v1, p0, LX/3wG;->a:Landroid/support/v7/widget/Toolbar;

    if-ne v0, v1, :cond_0

    .line 654686
    iget-object v0, p0, LX/3wG;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, LX/3wG;->c:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->removeView(Landroid/view/View;)V

    goto :goto_0

    .line 654687
    :pswitch_2
    const/4 p1, -0x2

    .line 654688
    iget-object v0, p0, LX/3wG;->d:LX/3w4;

    if-nez v0, :cond_1

    .line 654689
    new-instance v0, LX/3w4;

    invoke-virtual {p0}, LX/3wG;->b()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    const v4, 0x7f01002b

    invoke-direct {v0, v1, v2, v4}, LX/3w4;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, LX/3wG;->d:LX/3w4;

    .line 654690
    new-instance v0, LX/3xa;

    const v1, 0x800013

    invoke-direct {v0, p1, p1, v1}, LX/3xa;-><init>(III)V

    .line 654691
    iget-object v1, p0, LX/3wG;->d:LX/3w4;

    invoke-virtual {v1, v0}, LX/3w4;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 654692
    :cond_1
    iget-object v0, p0, LX/3wG;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, LX/3wG;->d:LX/3w4;

    invoke-virtual {v0, v1, v3}, Landroid/support/v7/widget/Toolbar;->addView(Landroid/view/View;I)V

    .line 654693
    :cond_2
    :goto_1
    :pswitch_3
    return-void

    .line 654694
    :pswitch_4
    iget-object v0, p0, LX/3wG;->c:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 654695
    iget-object v0, p0, LX/3wG;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, LX/3wG;->c:Landroid/view/View;

    invoke-virtual {v0, v1, v3}, Landroid/support/v7/widget/Toolbar;->addView(Landroid/view/View;I)V

    .line 654696
    iget-object v0, p0, LX/3wG;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/3xa;

    .line 654697
    iput v2, v0, LX/3xa;->width:I

    .line 654698
    iput v2, v0, LX/3xa;->height:I

    .line 654699
    const v1, 0x800053

    iput v1, v0, LX/3xa;->a:I

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 654700
    iget-object v0, p0, LX/3wG;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->g()Z

    move-result v0

    return v0
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 654701
    iget-object v0, p0, LX/3wG;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->h()V

    .line 654702
    return-void
.end method

.method public final e(I)V
    .locals 2

    .prologue
    .line 654703
    iget-object v0, p0, LX/3wG;->d:LX/3w4;

    if-nez v0, :cond_0

    .line 654704
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t set dropdown selected position without an adapter"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 654705
    :cond_0
    iget-object v0, p0, LX/3wG;->d:LX/3w4;

    invoke-virtual {v0, p1}, LX/3vM;->setSelection(I)V

    .line 654706
    return-void
.end method

.method public final f()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 654707
    iget-object v0, p0, LX/3wG;->a:Landroid/support/v7/widget/Toolbar;

    .line 654708
    iget-object p0, v0, Landroid/support/v7/widget/Toolbar;->v:Ljava/lang/CharSequence;

    move-object v0, p0

    .line 654709
    return-object v0
.end method

.method public final f(I)V
    .locals 2

    .prologue
    .line 654710
    const/16 v0, 0x8

    if-ne p1, v0, :cond_1

    .line 654711
    iget-object v0, p0, LX/3wG;->a:Landroid/support/v7/widget/Toolbar;

    invoke-static {v0}, LX/0vv;->v(Landroid/view/View;)LX/3sU;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/3sU;->a(F)LX/3sU;

    move-result-object v0

    new-instance v1, LX/3wE;

    invoke-direct {v1, p0}, LX/3wE;-><init>(LX/3wG;)V

    invoke-virtual {v0, v1}, LX/3sU;->a(LX/3oQ;)LX/3sU;

    .line 654712
    :cond_0
    :goto_0
    return-void

    .line 654713
    :cond_1
    if-nez p1, :cond_0

    .line 654714
    iget-object v0, p0, LX/3wG;->a:Landroid/support/v7/widget/Toolbar;

    invoke-static {v0}, LX/0vv;->v(Landroid/view/View;)LX/3sU;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, LX/3sU;->a(F)LX/3sU;

    move-result-object v0

    new-instance v1, LX/3wF;

    invoke-direct {v1, p0}, LX/3wF;-><init>(LX/3wG;)V

    invoke-virtual {v0, v1}, LX/3sU;->a(LX/3oQ;)LX/3sU;

    goto :goto_0
.end method

.method public final g(I)V
    .locals 1

    .prologue
    .line 654715
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/3wG;->q:LX/3wA;

    invoke-virtual {v0, p1}, LX/3wA;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_0
    invoke-virtual {p0, v0}, LX/3wG;->b(Landroid/graphics/drawable/Drawable;)V

    .line 654716
    return-void

    .line 654717
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 654718
    iget-object v0, p0, LX/3wG;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->a()Z

    move-result v0

    return v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 654719
    iget-object v0, p0, LX/3wG;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->b()Z

    move-result v0

    return v0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 654720
    iget-object v0, p0, LX/3wG;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->c()Z

    move-result v0

    return v0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 654721
    iget-object v0, p0, LX/3wG;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->d()Z

    move-result v0

    return v0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 654722
    iget-object v0, p0, LX/3wG;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->e()Z

    move-result v0

    return v0
.end method

.method public final l()V
    .locals 1

    .prologue
    .line 654723
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/3wG;->n:Z

    .line 654724
    return-void
.end method

.method public final m()V
    .locals 1

    .prologue
    .line 654725
    iget-object v0, p0, LX/3wG;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->f()V

    .line 654726
    return-void
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 654727
    iget v0, p0, LX/3wG;->b:I

    return v0
.end method

.method public final o()I
    .locals 1

    .prologue
    .line 654728
    iget v0, p0, LX/3wG;->p:I

    return v0
.end method

.method public final p()I
    .locals 1

    .prologue
    .line 654729
    iget-object v0, p0, LX/3wG;->d:LX/3w4;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3wG;->d:LX/3w4;

    .line 654730
    iget p0, v0, LX/3vM;->v:I

    move v0, p0

    .line 654731
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final q()Landroid/view/View;
    .locals 1

    .prologue
    .line 654807
    iget-object v0, p0, LX/3wG;->e:Landroid/view/View;

    return-object v0
.end method
