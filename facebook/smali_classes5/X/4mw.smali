.class public final LX/4mw;
.super LX/1Mu;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "LX/1Ck",
        "<TKey;>.CallbackWithCleanup<TT;>;"
    }
.end annotation


# instance fields
.field public final synthetic b:LX/1Ck;


# direct methods
.method public constructor <init>(LX/1Ck;Ljava/lang/Object;LX/0Ve;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TKey;",
            "LX/0Ve",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 806373
    iput-object p1, p0, LX/4mw;->b:LX/1Ck;

    .line 806374
    invoke-direct {p0, p1, p2, p3}, LX/1Mu;-><init>(LX/1Ck;Ljava/lang/Object;LX/0Ve;)V

    .line 806375
    return-void
.end method


# virtual methods
.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 806376
    iget-object v2, p0, LX/4mw;->b:LX/1Ck;

    monitor-enter v2

    .line 806377
    :try_start_0
    iget-object v0, p0, LX/4mw;->b:LX/1Ck;

    iget-object v0, v0, LX/1Ck;->a:LX/0Xq;

    .line 806378
    iget-object v3, p0, LX/1Mu;->c:Ljava/lang/Object;

    move-object v3, v3

    .line 806379
    invoke-virtual {v0, v3}, LX/0Xr;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 806380
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 806381
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 806382
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Mv;

    .line 806383
    iget-object v4, v0, LX/1Mv;->b:LX/0Ve;

    move-object v4, v4

    .line 806384
    if-ne v4, p0, :cond_1

    .line 806385
    const/4 v0, 0x1

    .line 806386
    :goto_1
    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/1Mu;->isDisposed()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 806387
    :cond_0
    invoke-super {p0, p1}, LX/1Mu;->onSuccess(Ljava/lang/Object;)V

    .line 806388
    :goto_2
    return-void

    .line 806389
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 806390
    :cond_1
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 806391
    :cond_2
    iget-object v1, p0, LX/4mw;->b:LX/1Ck;

    monitor-enter v1

    .line 806392
    :try_start_2
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Mv;

    .line 806393
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, LX/1Mv;->a(Z)V

    .line 806394
    iget-object v3, p0, LX/4mw;->b:LX/1Ck;

    iget-object v3, v3, LX/1Ck;->a:LX/0Xq;

    .line 806395
    iget-object v4, p0, LX/1Mu;->c:Ljava/lang/Object;

    move-object v4, v4

    .line 806396
    invoke-virtual {v3, v4, v0}, LX/0Xt;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    goto :goto_3

    .line 806397
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    :cond_3
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 806398
    invoke-super {p0, p1}, LX/1Mu;->onSuccess(Ljava/lang/Object;)V

    goto :goto_2

    :cond_4
    move v0, v1

    goto :goto_1
.end method
