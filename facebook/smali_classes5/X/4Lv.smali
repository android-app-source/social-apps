.class public LX/4Lv;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 685542
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    .line 685543
    const/4 v9, 0x0

    .line 685544
    const/4 v8, 0x0

    .line 685545
    const/4 v7, 0x0

    .line 685546
    const/4 v6, 0x0

    .line 685547
    const/4 v5, 0x0

    .line 685548
    const/4 v4, 0x0

    .line 685549
    const/4 v3, 0x0

    .line 685550
    const/4 v2, 0x0

    .line 685551
    const/4 v1, 0x0

    .line 685552
    const/4 v0, 0x0

    .line 685553
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->START_OBJECT:LX/15z;

    if-eq v10, v11, :cond_1

    .line 685554
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 685555
    const/4 v0, 0x0

    .line 685556
    :goto_0
    return v0

    .line 685557
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 685558
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_b

    .line 685559
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 685560
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 685561
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_1

    if-eqz v10, :cond_1

    .line 685562
    const-string v11, "fallback_url"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 685563
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 685564
    :cond_2
    const-string v11, "icon"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 685565
    invoke-static {p0, p1}, LX/4Lw;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 685566
    :cond_3
    const-string v11, "item_links"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 685567
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 685568
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->START_ARRAY:LX/15z;

    if-ne v10, v11, :cond_4

    .line 685569
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_ARRAY:LX/15z;

    if-eq v10, v11, :cond_4

    .line 685570
    invoke-static {p0, p1}, LX/4Lx;->b(LX/15w;LX/186;)I

    move-result v10

    .line 685571
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 685572
    :cond_4
    invoke-static {v7, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v7

    move v7, v7

    .line 685573
    goto :goto_1

    .line 685574
    :cond_5
    const-string v11, "item_type"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 685575
    const/4 v0, 0x1

    .line 685576
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;

    move-result-object v6

    goto :goto_1

    .line 685577
    :cond_6
    const-string v11, "logging_param"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 685578
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto/16 :goto_1

    .line 685579
    :cond_7
    const-string v11, "reaction_surface"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 685580
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto/16 :goto_1

    .line 685581
    :cond_8
    const-string v11, "subtitle"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_9

    .line 685582
    invoke-static {p0, p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v3

    goto/16 :goto_1

    .line 685583
    :cond_9
    const-string v11, "title"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_a

    .line 685584
    invoke-static {p0, p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v2

    goto/16 :goto_1

    .line 685585
    :cond_a
    const-string v11, "title_color"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 685586
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    goto/16 :goto_1

    .line 685587
    :cond_b
    const/16 v10, 0x9

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 685588
    const/4 v10, 0x0

    invoke-virtual {p1, v10, v9}, LX/186;->b(II)V

    .line 685589
    const/4 v9, 0x1

    invoke-virtual {p1, v9, v8}, LX/186;->b(II)V

    .line 685590
    const/4 v8, 0x2

    invoke-virtual {p1, v8, v7}, LX/186;->b(II)V

    .line 685591
    if-eqz v0, :cond_c

    .line 685592
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v6}, LX/186;->a(ILjava/lang/Enum;)V

    .line 685593
    :cond_c
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 685594
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 685595
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 685596
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 685597
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 685598
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 685599
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 685600
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 685601
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2, p3}, LX/4Lv;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 685602
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 685603
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 685604
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 685605
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 685606
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 685607
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 685608
    invoke-static {p0, p1}, LX/4Lv;->a(LX/15w;LX/186;)I

    move-result v1

    .line 685609
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 685610
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x0

    .line 685611
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 685612
    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 685613
    if-eqz v0, :cond_0

    .line 685614
    const-string v1, "fallback_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 685615
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 685616
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 685617
    if-eqz v0, :cond_1

    .line 685618
    const-string v1, "icon"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 685619
    invoke-static {p0, v0, p2, p3}, LX/4Lw;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 685620
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 685621
    if-eqz v0, :cond_3

    .line 685622
    const-string v1, "item_links"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 685623
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 685624
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v4

    if-ge v1, v4, :cond_2

    .line 685625
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v4

    invoke-static {p0, v4, p2}, LX/4Lx;->a(LX/15i;ILX/0nX;)V

    .line 685626
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 685627
    :cond_2
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 685628
    :cond_3
    invoke-virtual {p0, p1, v3, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 685629
    if-eqz v0, :cond_4

    .line 685630
    const-string v0, "item_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 685631
    const-class v0, Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;

    invoke-virtual {p0, p1, v3, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 685632
    :cond_4
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 685633
    if-eqz v0, :cond_5

    .line 685634
    const-string v1, "logging_param"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 685635
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 685636
    :cond_5
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 685637
    if-eqz v0, :cond_6

    .line 685638
    const-string v1, "reaction_surface"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 685639
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 685640
    :cond_6
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 685641
    if-eqz v0, :cond_7

    .line 685642
    const-string v1, "subtitle"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 685643
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 685644
    :cond_7
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 685645
    if-eqz v0, :cond_8

    .line 685646
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 685647
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 685648
    :cond_8
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 685649
    if-eqz v0, :cond_9

    .line 685650
    const-string v1, "title_color"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 685651
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 685652
    :cond_9
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 685653
    return-void
.end method
