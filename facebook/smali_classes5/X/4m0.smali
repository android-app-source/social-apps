.class public LX/4m0;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Landroid/net/Uri;

.field public d:Landroid/net/Uri;

.field public e:Landroid/net/Uri;

.field public f:Landroid/net/Uri;

.field public g:Landroid/net/Uri;

.field public h:Landroid/net/Uri;

.field public i:Lcom/facebook/stickers/model/StickerCapabilities;

.field private j:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 805136
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 805137
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4m0;->j:Z

    .line 805138
    return-void
.end method

.method public static newBuilder()LX/4m0;
    .locals 1

    .prologue
    .line 805179
    new-instance v0, LX/4m0;

    invoke-direct {v0}, LX/4m0;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)LX/4m0;
    .locals 0

    .prologue
    .line 805177
    iput-object p1, p0, LX/4m0;->c:Landroid/net/Uri;

    .line 805178
    return-object p0
.end method

.method public final a(Lcom/facebook/stickers/model/Sticker;)LX/4m0;
    .locals 1

    .prologue
    .line 805167
    iget-object v0, p1, Lcom/facebook/stickers/model/Sticker;->a:Ljava/lang/String;

    iput-object v0, p0, LX/4m0;->a:Ljava/lang/String;

    .line 805168
    iget-object v0, p1, Lcom/facebook/stickers/model/Sticker;->b:Ljava/lang/String;

    iput-object v0, p0, LX/4m0;->b:Ljava/lang/String;

    .line 805169
    iget-object v0, p1, Lcom/facebook/stickers/model/Sticker;->c:Landroid/net/Uri;

    iput-object v0, p0, LX/4m0;->c:Landroid/net/Uri;

    .line 805170
    iget-object v0, p1, Lcom/facebook/stickers/model/Sticker;->d:Landroid/net/Uri;

    iput-object v0, p0, LX/4m0;->d:Landroid/net/Uri;

    .line 805171
    iget-object v0, p1, Lcom/facebook/stickers/model/Sticker;->e:Landroid/net/Uri;

    iput-object v0, p0, LX/4m0;->e:Landroid/net/Uri;

    .line 805172
    iget-object v0, p1, Lcom/facebook/stickers/model/Sticker;->f:Landroid/net/Uri;

    iput-object v0, p0, LX/4m0;->f:Landroid/net/Uri;

    .line 805173
    iget-object v0, p1, Lcom/facebook/stickers/model/Sticker;->g:Landroid/net/Uri;

    iput-object v0, p0, LX/4m0;->g:Landroid/net/Uri;

    .line 805174
    iget-object v0, p1, Lcom/facebook/stickers/model/Sticker;->h:Landroid/net/Uri;

    iput-object v0, p0, LX/4m0;->h:Landroid/net/Uri;

    .line 805175
    iget-object v0, p1, Lcom/facebook/stickers/model/Sticker;->i:Lcom/facebook/stickers/model/StickerCapabilities;

    iput-object v0, p0, LX/4m0;->i:Lcom/facebook/stickers/model/StickerCapabilities;

    .line 805176
    return-object p0
.end method

.method public final a(Lcom/facebook/stickers/model/StickerCapabilities;)LX/4m0;
    .locals 0

    .prologue
    .line 805165
    iput-object p1, p0, LX/4m0;->i:Lcom/facebook/stickers/model/StickerCapabilities;

    .line 805166
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/4m0;
    .locals 0

    .prologue
    .line 805163
    iput-object p1, p0, LX/4m0;->a:Ljava/lang/String;

    .line 805164
    return-object p0
.end method

.method public final a()Lcom/facebook/stickers/model/Sticker;
    .locals 10

    .prologue
    .line 805160
    iget-boolean v0, p0, LX/4m0;->j:Z

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 805161
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/4m0;->j:Z

    .line 805162
    new-instance v0, Lcom/facebook/stickers/model/Sticker;

    iget-object v1, p0, LX/4m0;->a:Ljava/lang/String;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v2, p0, LX/4m0;->b:Ljava/lang/String;

    iget-object v3, p0, LX/4m0;->c:Landroid/net/Uri;

    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/Uri;

    iget-object v4, p0, LX/4m0;->d:Landroid/net/Uri;

    iget-object v5, p0, LX/4m0;->e:Landroid/net/Uri;

    iget-object v6, p0, LX/4m0;->f:Landroid/net/Uri;

    iget-object v7, p0, LX/4m0;->g:Landroid/net/Uri;

    iget-object v8, p0, LX/4m0;->h:Landroid/net/Uri;

    iget-object v9, p0, LX/4m0;->i:Lcom/facebook/stickers/model/StickerCapabilities;

    invoke-direct/range {v0 .. v9}, Lcom/facebook/stickers/model/Sticker;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Lcom/facebook/stickers/model/StickerCapabilities;)V

    return-object v0
.end method

.method public final b(Landroid/net/Uri;)LX/4m0;
    .locals 0

    .prologue
    .line 805180
    iput-object p1, p0, LX/4m0;->d:Landroid/net/Uri;

    .line 805181
    return-object p0
.end method

.method public final b(Ljava/lang/String;)LX/4m0;
    .locals 0

    .prologue
    .line 805158
    iput-object p1, p0, LX/4m0;->b:Ljava/lang/String;

    .line 805159
    return-object p0
.end method

.method public final b()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 805147
    iput-object v0, p0, LX/4m0;->a:Ljava/lang/String;

    .line 805148
    iput-object v0, p0, LX/4m0;->b:Ljava/lang/String;

    .line 805149
    iput-object v0, p0, LX/4m0;->c:Landroid/net/Uri;

    .line 805150
    iput-object v0, p0, LX/4m0;->d:Landroid/net/Uri;

    .line 805151
    iput-object v0, p0, LX/4m0;->e:Landroid/net/Uri;

    .line 805152
    iput-object v0, p0, LX/4m0;->f:Landroid/net/Uri;

    .line 805153
    iput-object v0, p0, LX/4m0;->g:Landroid/net/Uri;

    .line 805154
    iput-object v0, p0, LX/4m0;->h:Landroid/net/Uri;

    .line 805155
    iput-object v0, p0, LX/4m0;->i:Lcom/facebook/stickers/model/StickerCapabilities;

    .line 805156
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4m0;->j:Z

    .line 805157
    return-void
.end method

.method public final c(Landroid/net/Uri;)LX/4m0;
    .locals 0
    .param p1    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 805145
    iput-object p1, p0, LX/4m0;->e:Landroid/net/Uri;

    .line 805146
    return-object p0
.end method

.method public final d(Landroid/net/Uri;)LX/4m0;
    .locals 0

    .prologue
    .line 805143
    iput-object p1, p0, LX/4m0;->f:Landroid/net/Uri;

    .line 805144
    return-object p0
.end method

.method public final e(Landroid/net/Uri;)LX/4m0;
    .locals 0
    .param p1    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 805141
    iput-object p1, p0, LX/4m0;->g:Landroid/net/Uri;

    .line 805142
    return-object p0
.end method

.method public final f(Landroid/net/Uri;)LX/4m0;
    .locals 0

    .prologue
    .line 805139
    iput-object p1, p0, LX/4m0;->h:Landroid/net/Uri;

    .line 805140
    return-object p0
.end method
