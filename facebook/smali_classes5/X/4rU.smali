.class public final LX/4rU;
.super LX/2B6;
.source ""


# instance fields
.field private final a:[LX/4rV;


# direct methods
.method public constructor <init>([LX/4rV;)V
    .locals 0

    .prologue
    .line 815882
    invoke-direct {p0}, LX/2B6;-><init>()V

    .line 815883
    iput-object p1, p0, LX/4rU;->a:[LX/4rV;

    .line 815884
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)LX/2B6;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;)",
            "LX/2B6;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 815885
    iget-object v0, p0, LX/4rU;->a:[LX/4rV;

    array-length v0, v0

    .line 815886
    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    .line 815887
    :goto_0
    return-object p0

    .line 815888
    :cond_0
    add-int/lit8 v1, v0, 0x1

    new-array v1, v1, [LX/4rV;

    .line 815889
    iget-object v2, p0, LX/4rU;->a:[LX/4rV;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 815890
    new-instance v2, LX/4rV;

    invoke-direct {v2, p1, p2}, LX/4rV;-><init>(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    aput-object v2, v1, v0

    .line 815891
    new-instance p0, LX/4rU;

    invoke-direct {p0, v1}, LX/4rU;-><init>([LX/4rV;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Class;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 815892
    const/4 v0, 0x0

    iget-object v1, p0, LX/4rU;->a:[LX/4rV;

    array-length v1, v1

    :goto_0
    if-ge v0, v1, :cond_1

    .line 815893
    iget-object v2, p0, LX/4rU;->a:[LX/4rV;

    aget-object v2, v2, v0

    .line 815894
    iget-object v3, v2, LX/4rV;->a:Ljava/lang/Class;

    if-ne v3, p1, :cond_0

    .line 815895
    iget-object v0, v2, LX/4rV;->b:Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 815896
    :goto_1
    return-object v0

    .line 815897
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 815898
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
