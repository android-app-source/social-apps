.class public LX/4eE;
.super LX/4eD;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x17
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field private static d:Ljava/lang/reflect/Method;


# instance fields
.field private final b:LX/4eM;

.field private final c:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "Ljava/nio/ByteBuffer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/4eM;ILX/0Zi;)V
    .locals 0

    .prologue
    .line 797100
    invoke-direct {p0, p1, p2, p3}, LX/4eD;-><init>(LX/4eM;ILX/0Zi;)V

    .line 797101
    iput-object p1, p0, LX/4eE;->b:LX/4eM;

    .line 797102
    iput-object p3, p0, LX/4eE;->c:LX/0Zi;

    .line 797103
    return-void
.end method

.method private static a(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 797104
    :try_start_0
    invoke-static {}, LX/4eE;->a()Ljava/lang/reflect/Method;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 797105
    check-cast v0, Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 797106
    :catch_0
    move-exception v0

    .line 797107
    invoke-static {v0}, LX/0V9;->b(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method private static declared-synchronized a()Ljava/lang/reflect/Method;
    .locals 4

    .prologue
    .line 797093
    const-class v1, LX/4eE;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/4eE;->d:Ljava/lang/reflect/Method;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 797094
    :try_start_1
    const-class v0, Landroid/graphics/Bitmap;

    const-string v2, "createAshmemBitmap"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Class;

    invoke-virtual {v0, v2, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, LX/4eE;->d:Ljava/lang/reflect/Method;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 797095
    :goto_0
    monitor-exit v1

    return-object v0

    .line 797096
    :catch_0
    move-exception v0

    .line 797097
    :try_start_2
    invoke-static {v0}, LX/0V9;->b(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 797098
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 797099
    :cond_0
    :try_start_3
    sget-object v0, LX/4eE;->d:Ljava/lang/reflect/Method;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/io/InputStream;Landroid/graphics/BitmapFactory$Options;)LX/1FJ;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/InputStream;",
            "Landroid/graphics/BitmapFactory$Options;",
            ")",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 797070
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 797071
    iget v0, p2, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v1, p2, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    iget-object v2, p2, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, LX/1le;->a(IILandroid/graphics/Bitmap$Config;)I

    move-result v0

    .line 797072
    iget-object v1, p0, LX/4eE;->b:LX/4eM;

    invoke-virtual {v1, v0}, LX/1FR;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 797073
    if-nez v0, :cond_0

    .line 797074
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "BitmapPool.get returned null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 797075
    :cond_0
    iput-object v0, p2, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    .line 797076
    iget-object v1, p0, LX/4eE;->c:LX/0Zi;

    invoke-virtual {v1}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/nio/ByteBuffer;

    .line 797077
    if-nez v1, :cond_2

    .line 797078
    const/16 v1, 0x4000

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    move-object v2, v1

    .line 797079
    :goto_0
    :try_start_0
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    iput-object v1, p2, Landroid/graphics/BitmapFactory$Options;->inTempStorage:[B

    .line 797080
    const/4 v1, 0x0

    invoke-static {p1, v1, p2}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 797081
    iget-object v3, p0, LX/4eE;->c:LX/0Zi;

    invoke-virtual {v3, v2}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 797082
    if-eq v0, v1, :cond_1

    .line 797083
    iget-object v2, p0, LX/4eE;->b:LX/4eM;

    invoke-virtual {v2, v0}, LX/1FR;->a(Ljava/lang/Object;)V

    .line 797084
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 797085
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 797086
    :catch_0
    move-exception v1

    .line 797087
    :try_start_1
    iget-object v3, p0, LX/4eE;->b:LX/4eM;

    invoke-virtual {v3, v0}, LX/1FR;->a(Ljava/lang/Object;)V

    .line 797088
    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 797089
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/4eE;->c:LX/0Zi;

    invoke-virtual {v1, v2}, LX/0Zj;->a(Ljava/lang/Object;)Z

    throw v0

    .line 797090
    :cond_1
    invoke-static {v1}, LX/4eE;->a(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 797091
    iget-object v2, p0, LX/4eE;->b:LX/4eM;

    invoke-virtual {v2, v1}, LX/1FR;->a(Ljava/lang/Object;)V

    .line 797092
    iget-object v1, p0, LX/4eE;->b:LX/4eM;

    invoke-static {v0, v1}, LX/1FJ;->a(Ljava/lang/Object;LX/1FN;)LX/1FJ;

    move-result-object v0

    return-object v0

    :cond_2
    move-object v2, v1

    goto :goto_0
.end method
