.class public LX/4RY;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 709253
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 27

    .prologue
    .line 709118
    const/16 v23, 0x0

    .line 709119
    const/16 v22, 0x0

    .line 709120
    const/16 v21, 0x0

    .line 709121
    const/16 v20, 0x0

    .line 709122
    const/16 v19, 0x0

    .line 709123
    const/16 v18, 0x0

    .line 709124
    const/16 v17, 0x0

    .line 709125
    const/16 v16, 0x0

    .line 709126
    const/4 v15, 0x0

    .line 709127
    const/4 v14, 0x0

    .line 709128
    const/4 v13, 0x0

    .line 709129
    const/4 v12, 0x0

    .line 709130
    const/4 v11, 0x0

    .line 709131
    const/4 v10, 0x0

    .line 709132
    const/4 v9, 0x0

    .line 709133
    const/4 v8, 0x0

    .line 709134
    const/4 v7, 0x0

    .line 709135
    const/4 v6, 0x0

    .line 709136
    const/4 v5, 0x0

    .line 709137
    const/4 v4, 0x0

    .line 709138
    const/4 v3, 0x0

    .line 709139
    const/4 v2, 0x0

    .line 709140
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v24

    sget-object v25, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    if-eq v0, v1, :cond_1

    .line 709141
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 709142
    const/4 v2, 0x0

    .line 709143
    :goto_0
    return v2

    .line 709144
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 709145
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v24

    sget-object v25, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    if-eq v0, v1, :cond_c

    .line 709146
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v24

    .line 709147
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 709148
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v25

    sget-object v26, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    if-eq v0, v1, :cond_1

    if-eqz v24, :cond_1

    .line 709149
    const-string v25, "is_add_to_home_screen_enabled"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_2

    .line 709150
    const/4 v12, 0x1

    .line 709151
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v23

    goto :goto_1

    .line 709152
    :cond_2
    const-string v25, "is_webview_chrome_enabled"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_3

    .line 709153
    const/4 v11, 0x1

    .line 709154
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v22

    goto :goto_1

    .line 709155
    :cond_3
    const-string v25, "is_copy_link_enabled"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_4

    .line 709156
    const/4 v10, 0x1

    .line 709157
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v21

    goto :goto_1

    .line 709158
    :cond_4
    const-string v25, "is_autofill_settings_enabled"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_5

    .line 709159
    const/4 v9, 0x1

    .line 709160
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v20

    goto :goto_1

    .line 709161
    :cond_5
    const-string v25, "is_log_out_enabled"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_6

    .line 709162
    const/4 v8, 0x1

    .line 709163
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v19

    goto :goto_1

    .line 709164
    :cond_6
    const-string v25, "is_manage_permissions_enabled"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_7

    .line 709165
    const/4 v7, 0x1

    .line 709166
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v18

    goto :goto_1

    .line 709167
    :cond_7
    const-string v25, "is_overflow_menu_enabled"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_8

    .line 709168
    const/4 v6, 0x1

    .line 709169
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v17

    goto/16 :goto_1

    .line 709170
    :cond_8
    const-string v25, "is_scrollable_autofill_bar_enabled"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_9

    .line 709171
    const/4 v5, 0x1

    .line 709172
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v16

    goto/16 :goto_1

    .line 709173
    :cond_9
    const-string v25, "is_save_autofill_data_banner_enabled"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_a

    .line 709174
    const/4 v4, 0x1

    .line 709175
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v15

    goto/16 :goto_1

    .line 709176
    :cond_a
    const-string v25, "is_payment_enabled"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_b

    .line 709177
    const/4 v3, 0x1

    .line 709178
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v14

    goto/16 :goto_1

    .line 709179
    :cond_b
    const-string v25, "is_product_history_enabled"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_0

    .line 709180
    const/4 v2, 0x1

    .line 709181
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v13

    goto/16 :goto_1

    .line 709182
    :cond_c
    const/16 v24, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 709183
    if-eqz v12, :cond_d

    .line 709184
    const/4 v12, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v12, v1}, LX/186;->a(IZ)V

    .line 709185
    :cond_d
    if-eqz v11, :cond_e

    .line 709186
    const/4 v11, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v11, v1}, LX/186;->a(IZ)V

    .line 709187
    :cond_e
    if-eqz v10, :cond_f

    .line 709188
    const/4 v10, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v10, v1}, LX/186;->a(IZ)V

    .line 709189
    :cond_f
    if-eqz v9, :cond_10

    .line 709190
    const/4 v9, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v9, v1}, LX/186;->a(IZ)V

    .line 709191
    :cond_10
    if-eqz v8, :cond_11

    .line 709192
    const/4 v8, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v8, v1}, LX/186;->a(IZ)V

    .line 709193
    :cond_11
    if-eqz v7, :cond_12

    .line 709194
    const/4 v7, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v7, v1}, LX/186;->a(IZ)V

    .line 709195
    :cond_12
    if-eqz v6, :cond_13

    .line 709196
    const/4 v6, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v6, v1}, LX/186;->a(IZ)V

    .line 709197
    :cond_13
    if-eqz v5, :cond_14

    .line 709198
    const/4 v5, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v5, v1}, LX/186;->a(IZ)V

    .line 709199
    :cond_14
    if-eqz v4, :cond_15

    .line 709200
    const/16 v4, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v15}, LX/186;->a(IZ)V

    .line 709201
    :cond_15
    if-eqz v3, :cond_16

    .line 709202
    const/16 v3, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v14}, LX/186;->a(IZ)V

    .line 709203
    :cond_16
    if-eqz v2, :cond_17

    .line 709204
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->a(IZ)V

    .line 709205
    :cond_17
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 2

    .prologue
    .line 709206
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 709207
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 709208
    if-eqz v0, :cond_0

    .line 709209
    const-string v1, "is_add_to_home_screen_enabled"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 709210
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 709211
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 709212
    if-eqz v0, :cond_1

    .line 709213
    const-string v1, "is_webview_chrome_enabled"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 709214
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 709215
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 709216
    if-eqz v0, :cond_2

    .line 709217
    const-string v1, "is_copy_link_enabled"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 709218
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 709219
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 709220
    if-eqz v0, :cond_3

    .line 709221
    const-string v1, "is_autofill_settings_enabled"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 709222
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 709223
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 709224
    if-eqz v0, :cond_4

    .line 709225
    const-string v1, "is_log_out_enabled"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 709226
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 709227
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 709228
    if-eqz v0, :cond_5

    .line 709229
    const-string v1, "is_manage_permissions_enabled"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 709230
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 709231
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 709232
    if-eqz v0, :cond_6

    .line 709233
    const-string v1, "is_overflow_menu_enabled"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 709234
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 709235
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 709236
    if-eqz v0, :cond_7

    .line 709237
    const-string v1, "is_scrollable_autofill_bar_enabled"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 709238
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 709239
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 709240
    if-eqz v0, :cond_8

    .line 709241
    const-string v1, "is_save_autofill_data_banner_enabled"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 709242
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 709243
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 709244
    if-eqz v0, :cond_9

    .line 709245
    const-string v1, "is_payment_enabled"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 709246
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 709247
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 709248
    if-eqz v0, :cond_a

    .line 709249
    const-string v1, "is_product_history_enabled"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 709250
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 709251
    :cond_a
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 709252
    return-void
.end method
