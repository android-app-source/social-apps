.class public LX/4LP;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 683008
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const-wide/16 v4, 0x0

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 683009
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_8

    .line 683010
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 683011
    :goto_0
    return v1

    .line 683012
    :cond_0
    const-string v12, "eligible_for_guardrail"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 683013
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v7

    move v9, v7

    move v7, v6

    .line 683014
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_5

    .line 683015
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 683016
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 683017
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_1

    if-eqz v11, :cond_1

    .line 683018
    const-string v12, "current_privacy_option"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 683019
    invoke-static {p0, p1}, LX/39L;->a(LX/15w;LX/186;)I

    move-result v10

    goto :goto_1

    .line 683020
    :cond_2
    const-string v12, "suggested_option_timestamp"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 683021
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    move v0, v6

    goto :goto_1

    .line 683022
    :cond_3
    const-string v12, "suggested_privacy_option"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 683023
    invoke-static {p0, p1}, LX/39L;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 683024
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 683025
    :cond_5
    const/4 v11, 0x4

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 683026
    invoke-virtual {p1, v1, v10}, LX/186;->b(II)V

    .line 683027
    if-eqz v7, :cond_6

    .line 683028
    invoke-virtual {p1, v6, v9}, LX/186;->a(IZ)V

    .line 683029
    :cond_6
    if-eqz v0, :cond_7

    .line 683030
    const/4 v1, 0x2

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 683031
    :cond_7
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 683032
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_8
    move v0, v1

    move v7, v1

    move v8, v1

    move-wide v2, v4

    move v9, v1

    move v10, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 683033
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 683034
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 683035
    if-eqz v0, :cond_0

    .line 683036
    const-string v1, "current_privacy_option"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 683037
    invoke-static {p0, v0, p2, p3}, LX/39L;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 683038
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 683039
    if-eqz v0, :cond_1

    .line 683040
    const-string v1, "eligible_for_guardrail"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 683041
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 683042
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 683043
    cmp-long v2, v0, v2

    if-eqz v2, :cond_2

    .line 683044
    const-string v2, "suggested_option_timestamp"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 683045
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 683046
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 683047
    if-eqz v0, :cond_3

    .line 683048
    const-string v1, "suggested_privacy_option"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 683049
    invoke-static {p0, v0, p2, p3}, LX/39L;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 683050
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 683051
    return-void
.end method
