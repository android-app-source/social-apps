.class public LX/4Bw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4Bv;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/4Bv",
        "<",
        "Landroid/net/Uri;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/4Bw;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 678081
    new-instance v0, LX/4Bw;

    invoke-direct {v0}, LX/4Bw;-><init>()V

    sput-object v0, LX/4Bw;->a:LX/4Bw;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 678082
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/186;)I
    .locals 2

    .prologue
    .line 678083
    check-cast p1, Landroid/net/Uri;

    .line 678084
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 678085
    const/4 v1, 0x1

    invoke-virtual {p2, v1}, LX/186;->c(I)V

    .line 678086
    const/4 v1, 0x0

    invoke-virtual {p2, v1, v0}, LX/186;->b(II)V

    .line 678087
    invoke-virtual {p2}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(Ljava/nio/ByteBuffer;I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 678088
    const/4 v0, 0x0

    invoke-static {p1, p2, v0}, LX/0ah;->b(Ljava/nio/ByteBuffer;II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method
