.class public LX/3qm;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 642469
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/graphics/drawable/Drawable;I)V
    .locals 1

    .prologue
    .line 642470
    instance-of v0, p0, LX/3qp;

    if-eqz v0, :cond_0

    .line 642471
    check-cast p0, LX/3qp;

    invoke-interface {p0, p1}, LX/3qp;->setTint(I)V

    .line 642472
    :cond_0
    return-void
.end method

.method public static a(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V
    .locals 1

    .prologue
    .line 642473
    instance-of v0, p0, LX/3qp;

    if-eqz v0, :cond_0

    .line 642474
    check-cast p0, LX/3qp;

    invoke-interface {p0, p1}, LX/3qp;->setTintList(Landroid/content/res/ColorStateList;)V

    .line 642475
    :cond_0
    return-void
.end method

.method public static a(Landroid/graphics/drawable/Drawable;Landroid/graphics/PorterDuff$Mode;)V
    .locals 1

    .prologue
    .line 642476
    instance-of v0, p0, LX/3qp;

    if-eqz v0, :cond_0

    .line 642477
    check-cast p0, LX/3qp;

    invoke-interface {p0, p1}, LX/3qp;->setTintMode(Landroid/graphics/PorterDuff$Mode;)V

    .line 642478
    :cond_0
    return-void
.end method
