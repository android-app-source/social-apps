.class public final LX/3tc;
.super LX/0vn;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 645604
    invoke-direct {p0}, LX/0vn;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;LX/3sp;)V
    .locals 2

    .prologue
    .line 645605
    invoke-super {p0, p1, p2}, LX/0vn;->a(Landroid/view/View;LX/3sp;)V

    .line 645606
    check-cast p1, Landroid/support/v4/widget/NestedScrollView;

    .line 645607
    const-class v0, Landroid/widget/ScrollView;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/3sp;->b(Ljava/lang/CharSequence;)V

    .line 645608
    invoke-virtual {p1}, Landroid/support/v4/widget/NestedScrollView;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 645609
    invoke-static {p1}, Landroid/support/v4/widget/NestedScrollView;->getScrollRange(Landroid/support/v4/widget/NestedScrollView;)I

    move-result v0

    .line 645610
    if-lez v0, :cond_1

    .line 645611
    const/4 v1, 0x1

    invoke-virtual {p2, v1}, LX/3sp;->i(Z)V

    .line 645612
    invoke-virtual {p1}, Landroid/support/v4/widget/NestedScrollView;->getScrollY()I

    move-result v1

    if-lez v1, :cond_0

    .line 645613
    const/16 v1, 0x2000

    invoke-virtual {p2, v1}, LX/3sp;->a(I)V

    .line 645614
    :cond_0
    invoke-virtual {p1}, Landroid/support/v4/widget/NestedScrollView;->getScrollY()I

    move-result v1

    if-ge v1, v0, :cond_1

    .line 645615
    const/16 v0, 0x1000

    invoke-virtual {p2, v0}, LX/3sp;->a(I)V

    .line 645616
    :cond_1
    return-void
.end method

.method public final a(Landroid/view/View;ILandroid/os/Bundle;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 645617
    invoke-super {p0, p1, p2, p3}, LX/0vn;->a(Landroid/view/View;ILandroid/os/Bundle;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 645618
    :goto_0
    return v0

    .line 645619
    :cond_0
    check-cast p1, Landroid/support/v4/widget/NestedScrollView;

    .line 645620
    invoke-virtual {p1}, Landroid/support/v4/widget/NestedScrollView;->isEnabled()Z

    move-result v2

    if-nez v2, :cond_1

    move v0, v1

    .line 645621
    goto :goto_0

    .line 645622
    :cond_1
    sparse-switch p2, :sswitch_data_0

    move v0, v1

    .line 645623
    goto :goto_0

    .line 645624
    :sswitch_0
    invoke-virtual {p1}, Landroid/support/v4/widget/NestedScrollView;->getHeight()I

    move-result v2

    invoke-virtual {p1}, Landroid/support/v4/widget/NestedScrollView;->getPaddingBottom()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p1}, Landroid/support/v4/widget/NestedScrollView;->getPaddingTop()I

    move-result v3

    sub-int/2addr v2, v3

    .line 645625
    invoke-virtual {p1}, Landroid/support/v4/widget/NestedScrollView;->getScrollY()I

    move-result v3

    add-int/2addr v2, v3

    invoke-static {p1}, Landroid/support/v4/widget/NestedScrollView;->getScrollRange(Landroid/support/v4/widget/NestedScrollView;)I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 645626
    invoke-virtual {p1}, Landroid/support/v4/widget/NestedScrollView;->getScrollY()I

    move-result v3

    if-eq v2, v3, :cond_2

    .line 645627
    invoke-virtual {p1, v1, v2}, Landroid/support/v4/widget/NestedScrollView;->a(II)V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 645628
    goto :goto_0

    .line 645629
    :sswitch_1
    invoke-virtual {p1}, Landroid/support/v4/widget/NestedScrollView;->getHeight()I

    move-result v2

    invoke-virtual {p1}, Landroid/support/v4/widget/NestedScrollView;->getPaddingBottom()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p1}, Landroid/support/v4/widget/NestedScrollView;->getPaddingTop()I

    move-result v3

    sub-int/2addr v2, v3

    .line 645630
    invoke-virtual {p1}, Landroid/support/v4/widget/NestedScrollView;->getScrollY()I

    move-result v3

    sub-int v2, v3, v2

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 645631
    invoke-virtual {p1}, Landroid/support/v4/widget/NestedScrollView;->getScrollY()I

    move-result v3

    if-eq v2, v3, :cond_3

    .line 645632
    invoke-virtual {p1, v1, v2}, Landroid/support/v4/widget/NestedScrollView;->a(II)V

    goto :goto_0

    :cond_3
    move v0, v1

    .line 645633
    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1000 -> :sswitch_0
        0x2000 -> :sswitch_1
    .end sparse-switch
.end method

.method public final d(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 2

    .prologue
    .line 645634
    invoke-super {p0, p1, p2}, LX/0vn;->d(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    .line 645635
    check-cast p1, Landroid/support/v4/widget/NestedScrollView;

    .line 645636
    const-class v0, Landroid/widget/ScrollView;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 645637
    invoke-static {p2}, LX/2fV;->a(Landroid/view/accessibility/AccessibilityEvent;)LX/2fO;

    move-result-object v1

    .line 645638
    invoke-static {p1}, Landroid/support/v4/widget/NestedScrollView;->getScrollRange(Landroid/support/v4/widget/NestedScrollView;)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    .line 645639
    :goto_0
    invoke-virtual {v1, v0}, LX/2fO;->a(Z)V

    .line 645640
    invoke-virtual {p1}, Landroid/support/v4/widget/NestedScrollView;->getScrollX()I

    move-result v0

    invoke-virtual {v1, v0}, LX/2fO;->d(I)V

    .line 645641
    invoke-virtual {p1}, Landroid/support/v4/widget/NestedScrollView;->getScrollY()I

    move-result v0

    invoke-virtual {v1, v0}, LX/2fO;->e(I)V

    .line 645642
    invoke-virtual {p1}, Landroid/support/v4/widget/NestedScrollView;->getScrollX()I

    move-result v0

    invoke-virtual {v1, v0}, LX/2fO;->f(I)V

    .line 645643
    invoke-static {p1}, Landroid/support/v4/widget/NestedScrollView;->getScrollRange(Landroid/support/v4/widget/NestedScrollView;)I

    move-result v0

    invoke-virtual {v1, v0}, LX/2fO;->g(I)V

    .line 645644
    return-void

    .line 645645
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
