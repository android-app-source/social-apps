.class public LX/44D;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/2Bv;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/2Bv;LX/0Or;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/common/diagnostics/IsDebugLogsEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2Bv;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 669533
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 669534
    iput-object p1, p0, LX/44D;->a:LX/2Bv;

    .line 669535
    iput-object p2, p0, LX/44D;->b:LX/0Or;

    .line 669536
    return-void
.end method

.method public static a(LX/0QB;)LX/44D;
    .locals 1

    .prologue
    .line 669537
    invoke-static {p0}, LX/44D;->b(LX/0QB;)LX/44D;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/44D;
    .locals 3

    .prologue
    .line 669538
    new-instance v1, LX/44D;

    invoke-static {p0}, LX/2Bv;->a(LX/0QB;)LX/2Bv;

    move-result-object v0

    check-cast v0, LX/2Bv;

    const/16 v2, 0x1469

    invoke-static {p0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-direct {v1, v0, v2}, LX/44D;-><init>(LX/2Bv;LX/0Or;)V

    .line 669539
    return-object v1
.end method


# virtual methods
.method public final a(I)LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/0Px",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .prologue
    .line 669540
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 669541
    iget-object v0, p0, LX/44D;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 669542
    iget-object v0, p0, LX/44D;->a:LX/2Bv;

    invoke-virtual {v0}, LX/2Bv;->a()Ljava/util/List;

    move-result-object v0

    .line 669543
    const/4 v2, 0x0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {p1, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    invoke-interface {v0, v2, v3}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 669544
    :cond_0
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method
