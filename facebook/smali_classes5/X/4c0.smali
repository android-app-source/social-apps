.class public LX/4c0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lorg/apache/http/client/HttpRequestRetryHandler;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/4c0;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 794429
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 794430
    return-void
.end method

.method public static a(LX/0QB;)LX/4c0;
    .locals 3

    .prologue
    .line 794431
    sget-object v0, LX/4c0;->a:LX/4c0;

    if-nez v0, :cond_1

    .line 794432
    const-class v1, LX/4c0;

    monitor-enter v1

    .line 794433
    :try_start_0
    sget-object v0, LX/4c0;->a:LX/4c0;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 794434
    if-eqz v2, :cond_0

    .line 794435
    :try_start_1
    new-instance v0, LX/4c0;

    invoke-direct {v0}, LX/4c0;-><init>()V

    .line 794436
    move-object v0, v0

    .line 794437
    sput-object v0, LX/4c0;->a:LX/4c0;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 794438
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 794439
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 794440
    :cond_1
    sget-object v0, LX/4c0;->a:LX/4c0;

    return-object v0

    .line 794441
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 794442
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final retryRequest(Ljava/io/IOException;ILorg/apache/http/protocol/HttpContext;)Z
    .locals 1

    .prologue
    .line 794443
    const/4 v0, 0x0

    return v0
.end method
