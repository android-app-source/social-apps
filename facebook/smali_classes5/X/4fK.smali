.class public LX/4fK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1cF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/1cF",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final a:LX/1cF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1cF",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final b:I

.field public c:I
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public final d:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "Landroid/util/Pair",
            "<",
            "Lcom/facebook/imagepipeline/producers/Consumer",
            "<TT;>;",
            "Lcom/facebook/imagepipeline/producers/ProducerContext;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public final e:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(ILjava/util/concurrent/Executor;LX/1cF;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/concurrent/Executor;",
            "LX/1cF",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 798289
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 798290
    iput p1, p0, LX/4fK;->b:I

    .line 798291
    invoke-static {p2}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, LX/4fK;->e:Ljava/util/concurrent/Executor;

    .line 798292
    invoke-static {p3}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1cF;

    iput-object v0, p0, LX/4fK;->a:LX/1cF;

    .line 798293
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, LX/4fK;->d:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 798294
    const/4 v0, 0x0

    iput v0, p0, LX/4fK;->c:I

    .line 798295
    return-void
.end method


# virtual methods
.method public final a(LX/1cd;LX/1cW;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/imagepipeline/producers/Consumer",
            "<TT;>;",
            "Lcom/facebook/imagepipeline/producers/ProducerContext;",
            ")V"
        }
    .end annotation

    .prologue
    .line 798296
    iget-object v0, p2, LX/1cW;->c:LX/1BV;

    move-object v0, v0

    .line 798297
    iget-object v1, p2, LX/1cW;->b:Ljava/lang/String;

    move-object v1, v1

    .line 798298
    const-string v2, "ThrottlingProducer"

    invoke-interface {v0, v1, v2}, LX/1BV;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 798299
    monitor-enter p0

    .line 798300
    :try_start_0
    iget v0, p0, LX/4fK;->c:I

    iget v1, p0, LX/4fK;->b:I

    if-lt v0, v1, :cond_1

    .line 798301
    iget-object v0, p0, LX/4fK;->d:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-static {p1, p2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    .line 798302
    const/4 v0, 0x1

    .line 798303
    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 798304
    if-nez v0, :cond_0

    .line 798305
    invoke-virtual {p0, p1, p2}, LX/4fK;->b(LX/1cd;LX/1cW;)V

    .line 798306
    :cond_0
    return-void

    .line 798307
    :cond_1
    :try_start_1
    iget v0, p0, LX/4fK;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/4fK;->c:I

    .line 798308
    const/4 v0, 0x0

    goto :goto_0

    .line 798309
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final b(LX/1cd;LX/1cW;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/imagepipeline/producers/Consumer",
            "<TT;>;",
            "Lcom/facebook/imagepipeline/producers/ProducerContext;",
            ")V"
        }
    .end annotation

    .prologue
    .line 798310
    iget-object v0, p2, LX/1cW;->c:LX/1BV;

    move-object v0, v0

    .line 798311
    iget-object v1, p2, LX/1cW;->b:Ljava/lang/String;

    move-object v1, v1

    .line 798312
    const-string v2, "ThrottlingProducer"

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, LX/1BV;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 798313
    iget-object v0, p0, LX/4fK;->a:LX/1cF;

    new-instance v1, LX/4fJ;

    invoke-direct {v1, p0, p1}, LX/4fJ;-><init>(LX/4fK;LX/1cd;)V

    invoke-interface {v0, v1, p2}, LX/1cF;->a(LX/1cd;LX/1cW;)V

    .line 798314
    return-void
.end method
