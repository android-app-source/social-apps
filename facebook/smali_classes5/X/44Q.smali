.class public LX/44Q;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Te;


# instance fields
.field private final a:LX/0So;

.field private final b:LX/0Td;


# direct methods
.method public constructor <init>(LX/0Td;LX/0So;)V
    .locals 0

    .prologue
    .line 669725
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 669726
    iput-object p1, p0, LX/44Q;->b:LX/0Td;

    .line 669727
    iput-object p2, p0, LX/44Q;->a:LX/0So;

    .line 669728
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)LX/0YG;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Runnable;",
            "JJ",
            "Ljava/util/concurrent/TimeUnit;",
            ")",
            "LX/0YG",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 669724
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)LX/0YG;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Runnable;",
            "J",
            "Ljava/util/concurrent/TimeUnit;",
            ")",
            "LX/0YG",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 669722
    iget-object v0, p0, LX/44Q;->b:LX/0Td;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/0Td;->a(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)LX/0YG;

    move-result-object v3

    .line 669723
    new-instance v1, LX/44P;

    iget-object v2, p0, LX/44Q;->a:LX/0So;

    iget-object v0, p0, LX/44Q;->a:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v4

    move-wide v6, p2

    move-object v8, p4

    invoke-direct/range {v1 .. v8}, LX/44P;-><init>(LX/0So;LX/0YG;JJLjava/util/concurrent/TimeUnit;)V

    return-object v1
.end method

.method public final a(Ljava/util/concurrent/Callable;JLjava/util/concurrent/TimeUnit;)LX/0YG;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable",
            "<TV;>;J",
            "Ljava/util/concurrent/TimeUnit;",
            ")",
            "LX/0YG",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 669720
    iget-object v0, p0, LX/44Q;->b:LX/0Td;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/0Td;->a(Ljava/util/concurrent/Callable;JLjava/util/concurrent/TimeUnit;)LX/0YG;

    move-result-object v3

    .line 669721
    new-instance v1, LX/44P;

    iget-object v2, p0, LX/44Q;->a:LX/0So;

    iget-object v0, p0, LX/44Q;->a:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v4

    move-wide v6, p2

    move-object v8, p4

    invoke-direct/range {v1 .. v8}, LX/44P;-><init>(LX/0So;LX/0YG;JJLjava/util/concurrent/TimeUnit;)V

    return-object v1
.end method

.method public final a(Ljava/lang/Runnable;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Runnable;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 669719
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a(Ljava/lang/Runnable;Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Runnable;",
            "TT;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 669718
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable",
            "<TT;>;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 669717
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 669715
    iget-object v0, p0, LX/44Q;->b:LX/0Td;

    invoke-virtual {v0}, LX/0Td;->a()V

    .line 669716
    return-void
.end method

.method public final awaitTermination(JLjava/util/concurrent/TimeUnit;)Z
    .locals 1

    .prologue
    .line 669714
    iget-object v0, p0, LX/44Q;->b:LX/0Td;

    invoke-virtual {v0, p1, p2, p3}, LX/0Td;->awaitTermination(JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)LX/0YG;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Runnable;",
            "JJ",
            "Ljava/util/concurrent/TimeUnit;",
            ")",
            "LX/0YG",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 669713
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 669712
    iget-object v0, p0, LX/44Q;->b:LX/0Td;

    invoke-virtual {v0}, LX/0Td;->b()Z

    move-result v0

    return v0
.end method

.method public final execute(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 669711
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final invokeAll(Ljava/util/Collection;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Collection",
            "<+",
            "Ljava/util/concurrent/Callable",
            "<TT;>;>;)",
            "Ljava/util/List",
            "<",
            "Ljava/util/concurrent/Future",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 669710
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final invokeAll(Ljava/util/Collection;JLjava/util/concurrent/TimeUnit;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Collection",
            "<+",
            "Ljava/util/concurrent/Callable",
            "<TT;>;>;J",
            "Ljava/util/concurrent/TimeUnit;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/util/concurrent/Future",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 669729
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final invokeAny(Ljava/util/Collection;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Collection",
            "<+",
            "Ljava/util/concurrent/Callable",
            "<TT;>;>;)TT;"
        }
    .end annotation

    .prologue
    .line 669709
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final invokeAny(Ljava/util/Collection;JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Collection",
            "<+",
            "Ljava/util/concurrent/Callable",
            "<TT;>;>;J",
            "Ljava/util/concurrent/TimeUnit;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 669708
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final isShutdown()Z
    .locals 1

    .prologue
    .line 669707
    iget-object v0, p0, LX/44Q;->b:LX/0Td;

    invoke-virtual {v0}, LX/0Td;->isShutdown()Z

    move-result v0

    return v0
.end method

.method public final isTerminated()Z
    .locals 1

    .prologue
    .line 669706
    iget-object v0, p0, LX/44Q;->b:LX/0Td;

    invoke-virtual {v0}, LX/0Td;->isTerminated()Z

    move-result v0

    return v0
.end method

.method public final synthetic schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;
    .locals 2

    .prologue
    .line 669705
    invoke-virtual {p0, p1, p2, p3, p4}, LX/44Q;->a(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)LX/0YG;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic schedule(Ljava/util/concurrent/Callable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;
    .locals 2

    .prologue
    .line 669704
    invoke-virtual {p0, p1, p2, p3, p4}, LX/44Q;->a(Ljava/util/concurrent/Callable;JLjava/util/concurrent/TimeUnit;)LX/0YG;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic scheduleAtFixedRate(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;
    .locals 2

    .prologue
    .line 669696
    invoke-virtual/range {p0 .. p6}, LX/44Q;->a(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)LX/0YG;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic scheduleWithFixedDelay(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;
    .locals 2

    .prologue
    .line 669703
    invoke-virtual/range {p0 .. p6}, LX/44Q;->b(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)LX/0YG;

    move-result-object v0

    return-object v0
.end method

.method public final shutdown()V
    .locals 1

    .prologue
    .line 669701
    iget-object v0, p0, LX/44Q;->b:LX/0Td;

    invoke-virtual {v0}, LX/0Td;->shutdown()V

    .line 669702
    return-void
.end method

.method public final shutdownNow()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 669700
    iget-object v0, p0, LX/44Q;->b:LX/0Td;

    invoke-virtual {v0}, LX/0Td;->shutdownNow()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;
    .locals 1

    .prologue
    .line 669699
    invoke-virtual {p0, p1}, LX/44Q;->a(Ljava/lang/Runnable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic submit(Ljava/lang/Runnable;Ljava/lang/Object;)Ljava/util/concurrent/Future;
    .locals 1

    .prologue
    .line 669698
    invoke-virtual {p0, p1, p2}, LX/44Q;->a(Ljava/lang/Runnable;Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;
    .locals 1

    .prologue
    .line 669697
    invoke-virtual {p0, p1}, LX/44Q;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
