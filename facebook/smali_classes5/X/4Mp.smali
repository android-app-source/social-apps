.class public LX/4Mp;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 689424
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 17

    .prologue
    .line 689327
    const/4 v13, 0x0

    .line 689328
    const/4 v12, 0x0

    .line 689329
    const/4 v11, 0x0

    .line 689330
    const/4 v10, 0x0

    .line 689331
    const/4 v9, 0x0

    .line 689332
    const/4 v8, 0x0

    .line 689333
    const/4 v7, 0x0

    .line 689334
    const/4 v6, 0x0

    .line 689335
    const-wide/16 v4, 0x0

    .line 689336
    const/4 v3, 0x0

    .line 689337
    const/4 v2, 0x0

    .line 689338
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->START_OBJECT:LX/15z;

    if-eq v14, v15, :cond_d

    .line 689339
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 689340
    const/4 v2, 0x0

    .line 689341
    :goto_0
    return v2

    .line 689342
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v15, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v15, :cond_b

    .line 689343
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 689344
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 689345
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v15

    sget-object v16, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v16

    if-eq v15, v0, :cond_0

    if-eqz v3, :cond_0

    .line 689346
    const-string v15, "id"

    invoke-virtual {v3, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_1

    .line 689347
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v14, v3

    goto :goto_1

    .line 689348
    :cond_1
    const-string v15, "image"

    invoke-virtual {v3, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_2

    .line 689349
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v3

    move v13, v3

    goto :goto_1

    .line 689350
    :cond_2
    const-string v15, "image_landscape_size"

    invoke-virtual {v3, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 689351
    invoke-static/range {p0 .. p1}, LX/4Mq;->a(LX/15w;LX/186;)I

    move-result v3

    move v12, v3

    goto :goto_1

    .line 689352
    :cond_3
    const-string v15, "image_portrait_size"

    invoke-virtual {v3, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_4

    .line 689353
    invoke-static/range {p0 .. p1}, LX/4Mq;->a(LX/15w;LX/186;)I

    move-result v3

    move v11, v3

    goto :goto_1

    .line 689354
    :cond_4
    const-string v15, "landscape_anchoring"

    invoke-virtual {v3, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_5

    .line 689355
    invoke-static/range {p0 .. p1}, LX/4Mm;->a(LX/15w;LX/186;)I

    move-result v3

    move v10, v3

    goto :goto_1

    .line 689356
    :cond_5
    const-string v15, "portrait_anchoring"

    invoke-virtual {v3, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_6

    .line 689357
    invoke-static/range {p0 .. p1}, LX/4Mm;->a(LX/15w;LX/186;)I

    move-result v3

    move v9, v3

    goto :goto_1

    .line 689358
    :cond_6
    const-string v15, "profileImageLarge"

    invoke-virtual {v3, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_7

    .line 689359
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v3

    move v7, v3

    goto/16 :goto_1

    .line 689360
    :cond_7
    const-string v15, "profileImageSmall"

    invoke-virtual {v3, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_8

    .line 689361
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v3

    move v6, v3

    goto/16 :goto_1

    .line 689362
    :cond_8
    const-string v15, "rotation_degree"

    invoke-virtual {v3, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_9

    .line 689363
    const/4 v2, 0x1

    .line 689364
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v4

    goto/16 :goto_1

    .line 689365
    :cond_9
    const-string v15, "url"

    invoke-virtual {v3, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 689366
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v8, v3

    goto/16 :goto_1

    .line 689367
    :cond_a
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 689368
    :cond_b
    const/16 v3, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 689369
    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v14}, LX/186;->b(II)V

    .line 689370
    const/4 v3, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v13}, LX/186;->b(II)V

    .line 689371
    const/4 v3, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v12}, LX/186;->b(II)V

    .line 689372
    const/4 v3, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, LX/186;->b(II)V

    .line 689373
    const/4 v3, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10}, LX/186;->b(II)V

    .line 689374
    const/4 v3, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 689375
    const/4 v3, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 689376
    const/4 v3, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 689377
    if-eqz v2, :cond_c

    .line 689378
    const/16 v3, 0x8

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 689379
    :cond_c
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 689380
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_d
    move v14, v13

    move v13, v12

    move v12, v11

    move v11, v10

    move v10, v9

    move v9, v8

    move v8, v3

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 689381
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 689382
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 689383
    if-eqz v0, :cond_0

    .line 689384
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 689385
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 689386
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 689387
    if-eqz v0, :cond_1

    .line 689388
    const-string v1, "image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 689389
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 689390
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 689391
    if-eqz v0, :cond_2

    .line 689392
    const-string v1, "image_landscape_size"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 689393
    invoke-static {p0, v0, p2}, LX/4Mq;->a(LX/15i;ILX/0nX;)V

    .line 689394
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 689395
    if-eqz v0, :cond_3

    .line 689396
    const-string v1, "image_portrait_size"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 689397
    invoke-static {p0, v0, p2}, LX/4Mq;->a(LX/15i;ILX/0nX;)V

    .line 689398
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 689399
    if-eqz v0, :cond_4

    .line 689400
    const-string v1, "landscape_anchoring"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 689401
    invoke-static {p0, v0, p2}, LX/4Mm;->a(LX/15i;ILX/0nX;)V

    .line 689402
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 689403
    if-eqz v0, :cond_5

    .line 689404
    const-string v1, "portrait_anchoring"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 689405
    invoke-static {p0, v0, p2}, LX/4Mm;->a(LX/15i;ILX/0nX;)V

    .line 689406
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 689407
    if-eqz v0, :cond_6

    .line 689408
    const-string v1, "profileImageLarge"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 689409
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 689410
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 689411
    if-eqz v0, :cond_7

    .line 689412
    const-string v1, "profileImageSmall"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 689413
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 689414
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 689415
    cmpl-double v2, v0, v2

    if-eqz v2, :cond_8

    .line 689416
    const-string v2, "rotation_degree"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 689417
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 689418
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 689419
    if-eqz v0, :cond_9

    .line 689420
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 689421
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 689422
    :cond_9
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 689423
    return-void
.end method
