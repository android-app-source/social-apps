.class public LX/3WJ;
.super Lcom/facebook/fbui/widget/layout/ImageBlockLayout;
.source ""


# static fields
.field public static final j:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "LX/3WJ;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public k:LX/23P;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Lcom/facebook/drawee/fbpipeline/FbDraweeView;",
            ">;"
        }
    .end annotation
.end field

.field public m:Landroid/widget/TextView;

.field public n:Z

.field public o:Landroid/graphics/Paint;

.field public p:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 590102
    new-instance v0, LX/3WK;

    invoke-direct {v0}, LX/3WK;-><init>()V

    sput-object v0, LX/3WJ;->j:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 590103
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;)V

    .line 590104
    const v0, 0x7f030028

    .line 590105
    const-class v1, LX/3WJ;

    invoke-static {v1, p0}, LX/3WJ;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 590106
    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 590107
    const v1, 0x7f0d038e

    invoke-virtual {p0, v1}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewStub;

    .line 590108
    new-instance v2, LX/0zw;

    invoke-direct {v2, v1}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object v2, p0, LX/3WJ;->l:LX/0zw;

    .line 590109
    const v1, 0x7f0d0390

    invoke-virtual {p0, v1}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, LX/3WJ;->m:Landroid/widget/TextView;

    .line 590110
    iget-object v1, p0, LX/3WJ;->m:Landroid/widget/TextView;

    iget-object v2, p0, LX/3WJ;->k:LX/23P;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 590111
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, LX/3WJ;->o:Landroid/graphics/Paint;

    .line 590112
    iget-object v1, p0, LX/3WJ;->o:Landroid/graphics/Paint;

    invoke-virtual {p0}, LX/3WJ;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const p1, 0x7f0a0442

    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 590113
    iget-object v1, p0, LX/3WJ;->o:Landroid/graphics/Paint;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 590114
    invoke-virtual {p0}, LX/3WJ;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0034

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, LX/3WJ;->p:I

    .line 590115
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/3WJ;

    invoke-static {p0}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object p0

    check-cast p0, LX/23P;

    iput-object p0, p1, LX/3WJ;->k:LX/23P;

    return-void
.end method


# virtual methods
.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 590116
    invoke-super {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 590117
    iget-boolean v0, p0, LX/3WJ;->n:Z

    if-eqz v0, :cond_0

    .line 590118
    const/4 v1, 0x0

    invoke-virtual {p0}, LX/3WJ;->getHeight()I

    move-result v0

    iget v2, p0, LX/3WJ;->p:I

    sub-int/2addr v0, v2

    int-to-float v2, v0

    invoke-virtual {p0}, LX/3WJ;->getWidth()I

    move-result v0

    int-to-float v3, v0

    invoke-virtual {p0}, LX/3WJ;->getHeight()I

    move-result v0

    iget v4, p0, LX/3WJ;->p:I

    sub-int/2addr v0, v4

    int-to-float v4, v0

    iget-object v5, p0, LX/3WJ;->o:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 590119
    :cond_0
    return-void
.end method

.method public getButtonView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 590120
    iget-object v0, p0, LX/3WJ;->m:Landroid/widget/TextView;

    return-object v0
.end method
