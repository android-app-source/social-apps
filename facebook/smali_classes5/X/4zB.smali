.class public final LX/4zB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/ListIterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/ListIterator",
        "<",
        "Ljava/util/Map$Entry",
        "<TK;TV;>;>;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:LX/4zA;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4zA",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public c:LX/4zA;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4zA",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public d:LX/4zA;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4zA",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public e:I

.field public final synthetic f:LX/4zD;


# direct methods
.method public constructor <init>(LX/4zD;I)V
    .locals 2

    .prologue
    .line 822414
    iput-object p1, p0, LX/4zB;->f:LX/4zD;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 822415
    iget-object v0, p0, LX/4zB;->f:LX/4zD;

    iget v0, v0, LX/4zD;->e:I

    iput v0, p0, LX/4zB;->e:I

    .line 822416
    invoke-virtual {p1}, LX/4zD;->f()I

    move-result v1

    .line 822417
    invoke-static {p2, v1}, LX/0PB;->checkPositionIndex(II)I

    .line 822418
    div-int/lit8 v0, v1, 0x2

    if-lt p2, v0, :cond_0

    .line 822419
    iget-object v0, p1, LX/4zD;->b:LX/4zA;

    iput-object v0, p0, LX/4zB;->d:LX/4zA;

    .line 822420
    iput v1, p0, LX/4zB;->a:I

    .line 822421
    :goto_0
    add-int/lit8 v0, p2, 0x1

    if-ge p2, v1, :cond_1

    .line 822422
    invoke-direct {p0}, LX/4zB;->c()LX/4zA;

    move p2, v0

    goto :goto_0

    .line 822423
    :cond_0
    iget-object v0, p1, LX/4zD;->a:LX/4zA;

    iput-object v0, p0, LX/4zB;->b:LX/4zA;

    .line 822424
    :goto_1
    add-int/lit8 v0, p2, -0x1

    if-lez p2, :cond_1

    .line 822425
    invoke-direct {p0}, LX/4zB;->b()LX/4zA;

    move p2, v0

    goto :goto_1

    .line 822426
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, LX/4zB;->c:LX/4zA;

    .line 822427
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 822411
    iget-object v0, p0, LX/4zB;->f:LX/4zD;

    iget v0, v0, LX/4zD;->e:I

    iget v1, p0, LX/4zB;->e:I

    if-eq v0, v1, :cond_0

    .line 822412
    new-instance v0, Ljava/util/ConcurrentModificationException;

    invoke-direct {v0}, Ljava/util/ConcurrentModificationException;-><init>()V

    throw v0

    .line 822413
    :cond_0
    return-void
.end method

.method private b()LX/4zA;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/4zA",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 822405
    invoke-direct {p0}, LX/4zB;->a()V

    .line 822406
    iget-object v0, p0, LX/4zB;->b:LX/4zA;

    invoke-static {v0}, LX/4zD;->i(Ljava/lang/Object;)V

    .line 822407
    iget-object v0, p0, LX/4zB;->b:LX/4zA;

    iput-object v0, p0, LX/4zB;->c:LX/4zA;

    iput-object v0, p0, LX/4zB;->d:LX/4zA;

    .line 822408
    iget-object v0, p0, LX/4zB;->b:LX/4zA;

    iget-object v0, v0, LX/4zA;->c:LX/4zA;

    iput-object v0, p0, LX/4zB;->b:LX/4zA;

    .line 822409
    iget v0, p0, LX/4zB;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/4zB;->a:I

    .line 822410
    iget-object v0, p0, LX/4zB;->c:LX/4zA;

    return-object v0
.end method

.method private c()LX/4zA;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/4zA",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 822399
    invoke-direct {p0}, LX/4zB;->a()V

    .line 822400
    iget-object v0, p0, LX/4zB;->d:LX/4zA;

    invoke-static {v0}, LX/4zD;->i(Ljava/lang/Object;)V

    .line 822401
    iget-object v0, p0, LX/4zB;->d:LX/4zA;

    iput-object v0, p0, LX/4zB;->c:LX/4zA;

    iput-object v0, p0, LX/4zB;->b:LX/4zA;

    .line 822402
    iget-object v0, p0, LX/4zB;->d:LX/4zA;

    iget-object v0, v0, LX/4zA;->d:LX/4zA;

    iput-object v0, p0, LX/4zB;->d:LX/4zA;

    .line 822403
    iget v0, p0, LX/4zB;->a:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/4zB;->a:I

    .line 822404
    iget-object v0, p0, LX/4zB;->c:LX/4zA;

    return-object v0
.end method


# virtual methods
.method public final add(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 822378
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final hasNext()Z
    .locals 1

    .prologue
    .line 822397
    invoke-direct {p0}, LX/4zB;->a()V

    .line 822398
    iget-object v0, p0, LX/4zB;->b:LX/4zA;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasPrevious()Z
    .locals 1

    .prologue
    .line 822395
    invoke-direct {p0}, LX/4zB;->a()V

    .line 822396
    iget-object v0, p0, LX/4zB;->d:LX/4zA;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 822394
    invoke-direct {p0}, LX/4zB;->b()LX/4zA;

    move-result-object v0

    return-object v0
.end method

.method public final nextIndex()I
    .locals 1

    .prologue
    .line 822393
    iget v0, p0, LX/4zB;->a:I

    return v0
.end method

.method public final synthetic previous()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 822392
    invoke-direct {p0}, LX/4zB;->c()LX/4zA;

    move-result-object v0

    return-object v0
.end method

.method public final previousIndex()I
    .locals 1

    .prologue
    .line 822391
    iget v0, p0, LX/4zB;->a:I

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public final remove()V
    .locals 2

    .prologue
    .line 822380
    invoke-direct {p0}, LX/4zB;->a()V

    .line 822381
    iget-object v0, p0, LX/4zB;->c:LX/4zA;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0P6;->a(Z)V

    .line 822382
    iget-object v0, p0, LX/4zB;->c:LX/4zA;

    iget-object v1, p0, LX/4zB;->b:LX/4zA;

    if-eq v0, v1, :cond_1

    .line 822383
    iget-object v0, p0, LX/4zB;->c:LX/4zA;

    iget-object v0, v0, LX/4zA;->d:LX/4zA;

    iput-object v0, p0, LX/4zB;->d:LX/4zA;

    .line 822384
    iget v0, p0, LX/4zB;->a:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/4zB;->a:I

    .line 822385
    :goto_1
    iget-object v0, p0, LX/4zB;->f:LX/4zD;

    iget-object v1, p0, LX/4zB;->c:LX/4zA;

    invoke-static {v0, v1}, LX/4zD;->a$redex0(LX/4zD;LX/4zA;)V

    .line 822386
    const/4 v0, 0x0

    iput-object v0, p0, LX/4zB;->c:LX/4zA;

    .line 822387
    iget-object v0, p0, LX/4zB;->f:LX/4zD;

    iget v0, v0, LX/4zD;->e:I

    iput v0, p0, LX/4zB;->e:I

    .line 822388
    return-void

    .line 822389
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 822390
    :cond_1
    iget-object v0, p0, LX/4zB;->c:LX/4zA;

    iget-object v0, v0, LX/4zA;->c:LX/4zA;

    iput-object v0, p0, LX/4zB;->b:LX/4zA;

    goto :goto_1
.end method

.method public final set(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 822379
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
