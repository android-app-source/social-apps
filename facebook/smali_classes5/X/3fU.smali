.class public LX/3fU;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;

.field public static final c:LX/0Tn;

.field public static final d:LX/0Tn;

.field public static final e:LX/0Tn;

.field public static final f:LX/0Tn;

.field public static final g:LX/0Tn;

.field public static final h:LX/0Tn;

.field public static final i:LX/0Tn;

.field public static final j:LX/0Tn;

.field public static final k:LX/0Tn;

.field public static final l:LX/0Tn;

.field private static final m:LX/0Tn;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 622288
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "new_selfupdate/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 622289
    sput-object v0, LX/3fU;->a:LX/0Tn;

    const-string v1, "fetch_interval"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3fU;->b:LX/0Tn;

    .line 622290
    sget-object v0, LX/3fU;->a:LX/0Tn;

    const-string v1, "last_fetched_ts"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3fU;->c:LX/0Tn;

    .line 622291
    sget-object v0, LX/3fU;->a:LX/0Tn;

    const-string v1, "release_info"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3fU;->d:LX/0Tn;

    .line 622292
    sget-object v0, LX/3fU;->a:LX/0Tn;

    const-string v1, "flow_start_ts"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3fU;->e:LX/0Tn;

    .line 622293
    sget-object v0, LX/3fU;->a:LX/0Tn;

    const-string v1, "flow_timeout_ts"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3fU;->f:LX/0Tn;

    .line 622294
    sget-object v0, LX/3fU;->a:LX/0Tn;

    const-string v1, "download_prompt_count"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3fU;->g:LX/0Tn;

    .line 622295
    sget-object v0, LX/3fU;->a:LX/0Tn;

    const-string v1, "download_prompt_ts"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3fU;->h:LX/0Tn;

    .line 622296
    sget-object v0, LX/3fU;->a:LX/0Tn;

    const-string v1, "install_prompt_count"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3fU;->i:LX/0Tn;

    .line 622297
    sget-object v0, LX/3fU;->a:LX/0Tn;

    const-string v1, "install_prompt_ts"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3fU;->j:LX/0Tn;

    .line 622298
    sget-object v0, LX/3fU;->a:LX/0Tn;

    const-string v1, "install_pending_state/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 622299
    sput-object v0, LX/3fU;->m:LX/0Tn;

    const-string v1, "release_info"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3fU;->k:LX/0Tn;

    .line 622300
    sget-object v0, LX/3fU;->m:LX/0Tn;

    const-string v1, "diff_algorithm"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3fU;->l:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 622287
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
