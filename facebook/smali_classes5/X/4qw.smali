.class public abstract LX/4qw;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 814864
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 814865
    return-void
.end method

.method public static a(LX/15w;LX/0n3;LX/0lJ;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 814880
    iget-object v0, p2, LX/0lJ;->_class:Ljava/lang/Class;

    move-object v0, v0

    .line 814881
    invoke-static {p0, v0}, LX/4qw;->a(LX/15w;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/15w;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/15w;",
            "Ljava/lang/Class",
            "<*>;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 814866
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    .line 814867
    if-nez v1, :cond_1

    .line 814868
    :cond_0
    :goto_0
    return-object v0

    .line 814869
    :cond_1
    sget-object v2, LX/4qv;->a:[I

    invoke-virtual {v1}, LX/15z;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 814870
    :pswitch_0
    const-class v1, Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 814871
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 814872
    :pswitch_1
    const-class v1, Ljava/lang/Integer;

    invoke-virtual {p1, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 814873
    invoke-virtual {p0}, LX/15w;->x()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 814874
    :pswitch_2
    const-class v1, Ljava/lang/Double;

    invoke-virtual {p1, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 814875
    invoke-virtual {p0}, LX/15w;->B()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    goto :goto_0

    .line 814876
    :pswitch_3
    const-class v1, Ljava/lang/Boolean;

    invoke-virtual {p1, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 814877
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    goto :goto_0

    .line 814878
    :pswitch_4
    const-class v1, Ljava/lang/Boolean;

    invoke-virtual {p1, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 814879
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public abstract a()LX/4pO;
.end method

.method public abstract a(LX/2Ay;)LX/4qw;
.end method

.method public abstract a(LX/15w;LX/0n3;)Ljava/lang/Object;
.end method

.method public abstract b(LX/15w;LX/0n3;)Ljava/lang/Object;
.end method

.method public abstract b()Ljava/lang/String;
.end method

.method public abstract c()LX/4qx;
.end method

.method public abstract c(LX/15w;LX/0n3;)Ljava/lang/Object;
.end method

.method public abstract d()Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end method

.method public abstract d(LX/15w;LX/0n3;)Ljava/lang/Object;
.end method
