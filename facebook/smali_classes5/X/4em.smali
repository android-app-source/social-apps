.class public LX/4em;
.super LX/3hi;
.source ""


# instance fields
.field private final b:I

.field private final c:I

.field private d:LX/1bh;


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 797480
    const/4 v0, 0x3

    invoke-direct {p0, v0, p1}, LX/4em;-><init>(II)V

    .line 797481
    return-void
.end method

.method private constructor <init>(II)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 797482
    invoke-direct {p0}, LX/3hi;-><init>()V

    .line 797483
    if-lez p1, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/03g;->a(Z)V

    .line 797484
    if-lez p2, :cond_1

    :goto_1
    invoke-static {v1}, LX/03g;->a(Z)V

    .line 797485
    iput p1, p0, LX/4em;->b:I

    .line 797486
    iput p2, p0, LX/4em;->c:I

    .line 797487
    return-void

    :cond_0
    move v0, v2

    .line 797488
    goto :goto_0

    :cond_1
    move v1, v2

    .line 797489
    goto :goto_1
.end method


# virtual methods
.method public final a()LX/1bh;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 797490
    iget-object v0, p0, LX/4em;->d:LX/1bh;

    if-nez v0, :cond_0

    .line 797491
    const/4 v0, 0x0

    const-string v1, "i%dr%d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, LX/4em;->b:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget v4, p0, LX/4em;->c:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 797492
    new-instance v1, LX/1ed;

    invoke-direct {v1, v0}, LX/1ed;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, LX/4em;->d:LX/1bh;

    .line 797493
    :cond_0
    iget-object v0, p0, LX/4em;->d:LX/1bh;

    return-object v0
.end method

.method public final a(Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 797494
    iget v0, p0, LX/4em;->b:I

    iget v1, p0, LX/4em;->c:I

    invoke-static {p1, v0, v1}, Lcom/facebook/imagepipeline/nativecode/NativeBlurFilter;->a(Landroid/graphics/Bitmap;II)V

    .line 797495
    return-void
.end method
