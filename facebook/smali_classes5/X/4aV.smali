.class public final LX/4aV;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 21

    .prologue
    .line 791870
    const/16 v17, 0x0

    .line 791871
    const/16 v16, 0x0

    .line 791872
    const/4 v15, 0x0

    .line 791873
    const/4 v14, 0x0

    .line 791874
    const/4 v13, 0x0

    .line 791875
    const/4 v12, 0x0

    .line 791876
    const/4 v11, 0x0

    .line 791877
    const/4 v10, 0x0

    .line 791878
    const/4 v9, 0x0

    .line 791879
    const/4 v8, 0x0

    .line 791880
    const/4 v7, 0x0

    .line 791881
    const/4 v6, 0x0

    .line 791882
    const/4 v5, 0x0

    .line 791883
    const/4 v4, 0x0

    .line 791884
    const/4 v3, 0x0

    .line 791885
    const/4 v2, 0x0

    .line 791886
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v18

    sget-object v19, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-eq v0, v1, :cond_1

    .line 791887
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 791888
    const/4 v2, 0x0

    .line 791889
    :goto_0
    return v2

    .line 791890
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 791891
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v18

    sget-object v19, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-eq v0, v1, :cond_b

    .line 791892
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v18

    .line 791893
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 791894
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v19

    sget-object v20, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    if-eq v0, v1, :cond_1

    if-eqz v18, :cond_1

    .line 791895
    const-string v19, "can_see_voice_switcher"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_2

    .line 791896
    const/4 v7, 0x1

    .line 791897
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v17

    goto :goto_1

    .line 791898
    :cond_2
    const-string v19, "can_viewer_comment"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_3

    .line 791899
    const/4 v6, 0x1

    .line 791900
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v16

    goto :goto_1

    .line 791901
    :cond_3
    const-string v19, "can_viewer_comment_with_photo"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_4

    .line 791902
    const/4 v5, 0x1

    .line 791903
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v15

    goto :goto_1

    .line 791904
    :cond_4
    const-string v19, "can_viewer_comment_with_video"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_5

    .line 791905
    const/4 v4, 0x1

    .line 791906
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v14

    goto :goto_1

    .line 791907
    :cond_5
    const-string v19, "can_viewer_like"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_6

    .line 791908
    const/4 v3, 0x1

    .line 791909
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v13

    goto :goto_1

    .line 791910
    :cond_6
    const-string v19, "does_viewer_like"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_7

    .line 791911
    const/4 v2, 0x1

    .line 791912
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v12

    goto :goto_1

    .line 791913
    :cond_7
    const-string v19, "id"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_8

    .line 791914
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    goto/16 :goto_1

    .line 791915
    :cond_8
    const-string v19, "legacy_api_post_id"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_9

    .line 791916
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto/16 :goto_1

    .line 791917
    :cond_9
    const-string v19, "likers"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_a

    .line 791918
    invoke-static/range {p0 .. p1}, LX/4aT;->a(LX/15w;LX/186;)I

    move-result v9

    goto/16 :goto_1

    .line 791919
    :cond_a
    const-string v19, "top_level_comments"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_0

    .line 791920
    invoke-static/range {p0 .. p1}, LX/4aU;->a(LX/15w;LX/186;)I

    move-result v8

    goto/16 :goto_1

    .line 791921
    :cond_b
    const/16 v18, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 791922
    if-eqz v7, :cond_c

    .line 791923
    const/4 v7, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v7, v1}, LX/186;->a(IZ)V

    .line 791924
    :cond_c
    if-eqz v6, :cond_d

    .line 791925
    const/4 v6, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v6, v1}, LX/186;->a(IZ)V

    .line 791926
    :cond_d
    if-eqz v5, :cond_e

    .line 791927
    const/4 v5, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v15}, LX/186;->a(IZ)V

    .line 791928
    :cond_e
    if-eqz v4, :cond_f

    .line 791929
    const/4 v4, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v14}, LX/186;->a(IZ)V

    .line 791930
    :cond_f
    if-eqz v3, :cond_10

    .line 791931
    const/4 v3, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v13}, LX/186;->a(IZ)V

    .line 791932
    :cond_10
    if-eqz v2, :cond_11

    .line 791933
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->a(IZ)V

    .line 791934
    :cond_11
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 791935
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 791936
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 791937
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 791938
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 791939
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 791940
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 791941
    if-eqz v0, :cond_0

    .line 791942
    const-string v1, "can_see_voice_switcher"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 791943
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 791944
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 791945
    if-eqz v0, :cond_1

    .line 791946
    const-string v1, "can_viewer_comment"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 791947
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 791948
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 791949
    if-eqz v0, :cond_2

    .line 791950
    const-string v1, "can_viewer_comment_with_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 791951
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 791952
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 791953
    if-eqz v0, :cond_3

    .line 791954
    const-string v1, "can_viewer_comment_with_video"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 791955
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 791956
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 791957
    if-eqz v0, :cond_4

    .line 791958
    const-string v1, "can_viewer_like"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 791959
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 791960
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 791961
    if-eqz v0, :cond_5

    .line 791962
    const-string v1, "does_viewer_like"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 791963
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 791964
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 791965
    if-eqz v0, :cond_6

    .line 791966
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 791967
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 791968
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 791969
    if-eqz v0, :cond_7

    .line 791970
    const-string v1, "legacy_api_post_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 791971
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 791972
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 791973
    if-eqz v0, :cond_8

    .line 791974
    const-string v1, "likers"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 791975
    invoke-static {p0, v0, p2}, LX/4aT;->a(LX/15i;ILX/0nX;)V

    .line 791976
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 791977
    if-eqz v0, :cond_9

    .line 791978
    const-string v1, "top_level_comments"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 791979
    invoke-static {p0, v0, p2}, LX/4aU;->a(LX/15i;ILX/0nX;)V

    .line 791980
    :cond_9
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 791981
    return-void
.end method
