.class public final enum LX/4g0;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/4g0;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/4g0;

.field public static final enum DATA_CONTROL_FAILURE:LX/4g0;

.field public static final enum UNKNOWN:LX/4g0;

.field public static final enum UPSELL_FAILURE:LX/4g0;


# instance fields
.field public final failureReason:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 798945
    new-instance v0, LX/4g0;

    const-string v1, "UNKNOWN"

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v1, v3, v2}, LX/4g0;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/4g0;->UNKNOWN:LX/4g0;

    .line 798946
    new-instance v0, LX/4g0;

    const-string v1, "DATA_CONTROL_FAILURE"

    const-string v2, "DATA_CONTROL_WITHOUT_UPSELL"

    invoke-direct {v0, v1, v4, v2}, LX/4g0;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/4g0;->DATA_CONTROL_FAILURE:LX/4g0;

    .line 798947
    new-instance v0, LX/4g0;

    const-string v1, "UPSELL_FAILURE"

    const-string v2, "NO_DATA_CONTROL_NO_UPSELL"

    invoke-direct {v0, v1, v5, v2}, LX/4g0;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/4g0;->UPSELL_FAILURE:LX/4g0;

    .line 798948
    const/4 v0, 0x3

    new-array v0, v0, [LX/4g0;

    sget-object v1, LX/4g0;->UNKNOWN:LX/4g0;

    aput-object v1, v0, v3

    sget-object v1, LX/4g0;->DATA_CONTROL_FAILURE:LX/4g0;

    aput-object v1, v0, v4

    sget-object v1, LX/4g0;->UPSELL_FAILURE:LX/4g0;

    aput-object v1, v0, v5

    sput-object v0, LX/4g0;->$VALUES:[LX/4g0;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 798949
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 798950
    iput-object p3, p0, LX/4g0;->failureReason:Ljava/lang/String;

    .line 798951
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/4g0;
    .locals 1

    .prologue
    .line 798952
    const-class v0, LX/4g0;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/4g0;

    return-object v0
.end method

.method public static values()[LX/4g0;
    .locals 1

    .prologue
    .line 798953
    sget-object v0, LX/4g0;->$VALUES:[LX/4g0;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/4g0;

    return-object v0
.end method
