.class public final LX/508;
.super Ljava/util/AbstractQueue;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/Beta;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/AbstractQueue",
        "<TE;>;"
    }
.end annotation


# instance fields
.field public final a:I
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field private final b:LX/505;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/508",
            "<TE;>.Heap;"
        }
    .end annotation
.end field

.field private final c:LX/505;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/508",
            "<TE;>.Heap;"
        }
    .end annotation
.end field

.field public d:[Ljava/lang/Object;

.field public e:I

.field public f:I


# direct methods
.method public constructor <init>(LX/504;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/504",
            "<-TE;>;I)V"
        }
    .end annotation

    .prologue
    .line 823466
    invoke-direct {p0}, Ljava/util/AbstractQueue;-><init>()V

    .line 823467
    iget-object v0, p1, LX/504;->a:Ljava/util/Comparator;

    invoke-static {v0}, LX/1sm;->a(Ljava/util/Comparator;)LX/1sm;

    move-result-object v0

    move-object v0, v0

    .line 823468
    new-instance v1, LX/505;

    invoke-direct {v1, p0, v0}, LX/505;-><init>(LX/508;LX/1sm;)V

    iput-object v1, p0, LX/508;->b:LX/505;

    .line 823469
    new-instance v1, LX/505;

    invoke-virtual {v0}, LX/1sm;->a()LX/1sm;

    move-result-object v0

    invoke-direct {v1, p0, v0}, LX/505;-><init>(LX/508;LX/1sm;)V

    iput-object v1, p0, LX/508;->c:LX/505;

    .line 823470
    iget-object v0, p0, LX/508;->b:LX/505;

    iget-object v1, p0, LX/508;->c:LX/505;

    iput-object v1, v0, LX/505;->b:LX/505;

    .line 823471
    iget-object v0, p0, LX/508;->c:LX/505;

    iget-object v1, p0, LX/508;->b:LX/505;

    iput-object v1, v0, LX/505;->b:LX/505;

    .line 823472
    iget v0, p1, LX/504;->c:I

    iput v0, p0, LX/508;->a:I

    .line 823473
    new-array v0, p2, [Ljava/lang/Object;

    iput-object v0, p0, LX/508;->d:[Ljava/lang/Object;

    .line 823474
    return-void
.end method

.method public static a(II)I
    .locals 1

    .prologue
    .line 823494
    add-int/lit8 v0, p0, -0x1

    invoke-static {v0, p1}, Ljava/lang/Math;->min(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public static a(IILjava/lang/Iterable;)I
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/lang/Iterable",
            "<*>;)I"
        }
    .end annotation

    .prologue
    .line 823489
    const/4 v0, -0x1

    if-ne p0, v0, :cond_0

    const/16 p0, 0xb

    .line 823490
    :cond_0
    instance-of v0, p2, Ljava/util/Collection;

    if-eqz v0, :cond_1

    .line 823491
    check-cast p2, Ljava/util/Collection;

    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v0

    .line 823492
    invoke-static {p0, v0}, Ljava/lang/Math;->max(II)I

    move-result p0

    .line 823493
    :cond_1
    invoke-static {p0, p1}, LX/508;->a(II)I

    move-result v0

    return v0
.end method

.method public static a(Ljava/util/Comparator;)LX/504;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<B:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Comparator",
            "<TB;>;)",
            "LX/504",
            "<TB;>;"
        }
    .end annotation

    .prologue
    .line 823488
    new-instance v0, LX/504;

    invoke-direct {v0, p0}, LX/504;-><init>(Ljava/util/Comparator;)V

    return-object v0
.end method

.method public static c(LX/508;I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TE;"
        }
    .end annotation

    .prologue
    .line 823485
    invoke-virtual {p0, p1}, LX/508;->a(I)Ljava/lang/Object;

    move-result-object v0

    .line 823486
    invoke-virtual {p0, p1}, LX/508;->b(I)LX/506;

    .line 823487
    return-object v0
.end method

.method public static c$redex0(LX/508;)I
    .locals 3

    .prologue
    const/4 v1, 0x2

    const/4 v0, 0x1

    .line 823481
    iget v2, p0, LX/508;->e:I

    packed-switch v2, :pswitch_data_0

    .line 823482
    iget-object v2, p0, LX/508;->c:LX/505;

    invoke-virtual {v2, v0, v1}, LX/505;->a(II)I

    move-result v2

    if-gtz v2, :cond_0

    :goto_0
    :pswitch_0
    return v0

    .line 823483
    :pswitch_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    move v0, v1

    .line 823484
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static d(LX/508;I)LX/505;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/508",
            "<TE;>.Heap;"
        }
    .end annotation

    .prologue
    .line 823480
    invoke-static {p1}, LX/508;->e(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/508;->b:LX/505;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/508;->c:LX/505;

    goto :goto_0
.end method

.method private static e(I)Z
    .locals 5
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 823475
    add-int/lit8 v3, p0, 0x1

    .line 823476
    if-lez v3, :cond_0

    move v0, v1

    :goto_0
    const-string v4, "negative index"

    invoke-static {v0, v4}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 823477
    const v0, 0x55555555

    and-int/2addr v0, v3

    const v4, -0x55555556

    and-int/2addr v3, v4

    if-le v0, v3, :cond_1

    :goto_1
    return v1

    :cond_0
    move v0, v2

    .line 823478
    goto :goto_0

    :cond_1
    move v1, v2

    .line 823479
    goto :goto_1
.end method


# virtual methods
.method public final a(I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TE;"
        }
    .end annotation

    .prologue
    .line 823465
    iget-object v0, p0, LX/508;->d:[Ljava/lang/Object;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public final add(Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)Z"
        }
    .end annotation

    .prologue
    .line 823495
    invoke-virtual {p0, p1}, LX/508;->offer(Ljava/lang/Object;)Z

    .line 823496
    const/4 v0, 0x1

    return v0
.end method

.method public final addAll(Ljava/util/Collection;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+TE;>;)Z"
        }
    .end annotation

    .prologue
    .line 823459
    const/4 v0, 0x0

    .line 823460
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 823461
    invoke-virtual {p0, v0}, LX/508;->offer(Ljava/lang/Object;)Z

    .line 823462
    const/4 v0, 0x1

    .line 823463
    goto :goto_0

    .line 823464
    :cond_0
    return v0
.end method

.method public final b(I)LX/506;
    .locals 8
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/506",
            "<TE;>;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 823415
    iget v1, p0, LX/508;->e:I

    invoke-static {p1, v1}, LX/0PB;->checkPositionIndex(II)I

    .line 823416
    iget v1, p0, LX/508;->f:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/508;->f:I

    .line 823417
    iget v1, p0, LX/508;->e:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, LX/508;->e:I

    .line 823418
    iget v1, p0, LX/508;->e:I

    if-ne v1, p1, :cond_0

    .line 823419
    iget-object v1, p0, LX/508;->d:[Ljava/lang/Object;

    iget v2, p0, LX/508;->e:I

    aput-object v0, v1, v2

    .line 823420
    :goto_0
    return-object v0

    .line 823421
    :cond_0
    iget v1, p0, LX/508;->e:I

    invoke-virtual {p0, v1}, LX/508;->a(I)Ljava/lang/Object;

    move-result-object v2

    .line 823422
    iget v1, p0, LX/508;->e:I

    invoke-static {p0, v1}, LX/508;->d(LX/508;I)LX/505;

    move-result-object v1

    .line 823423
    iget-object v3, v1, LX/505;->c:LX/508;

    iget v3, v3, LX/508;->e:I

    invoke-static {v3}, LX/505;->f(I)I

    move-result v4

    .line 823424
    if-eqz v4, :cond_3

    .line 823425
    invoke-static {v4}, LX/505;->f(I)I

    move-result v3

    .line 823426
    invoke-static {v3}, LX/505;->e(I)I

    move-result v3

    .line 823427
    if-eq v3, v4, :cond_3

    invoke-static {v3}, LX/505;->d(I)I

    move-result v4

    iget-object v5, v1, LX/505;->c:LX/508;

    iget v5, v5, LX/508;->e:I

    if-lt v4, v5, :cond_3

    .line 823428
    iget-object v4, v1, LX/505;->c:LX/508;

    invoke-virtual {v4, v3}, LX/508;->a(I)Ljava/lang/Object;

    move-result-object v4

    .line 823429
    iget-object v5, v1, LX/505;->a:LX/1sm;

    invoke-virtual {v5, v4, v2}, LX/1sm;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v5

    if-gez v5, :cond_3

    .line 823430
    iget-object v5, v1, LX/505;->c:LX/508;

    iget-object v5, v5, LX/508;->d:[Ljava/lang/Object;

    aput-object v2, v5, v3

    .line 823431
    iget-object v5, v1, LX/505;->c:LX/508;

    iget-object v5, v5, LX/508;->d:[Ljava/lang/Object;

    iget-object v6, v1, LX/505;->c:LX/508;

    iget v6, v6, LX/508;->e:I

    aput-object v4, v5, v6

    .line 823432
    :goto_1
    move v3, v3

    .line 823433
    iget v1, p0, LX/508;->e:I

    invoke-virtual {p0, v1}, LX/508;->a(I)Ljava/lang/Object;

    move-result-object v4

    .line 823434
    iget-object v1, p0, LX/508;->d:[Ljava/lang/Object;

    iget v5, p0, LX/508;->e:I

    aput-object v0, v1, v5

    .line 823435
    invoke-static {p0, p1}, LX/508;->d(LX/508;I)LX/505;

    move-result-object v0

    .line 823436
    invoke-virtual {v0, p1}, LX/505;->a(I)I

    move-result v1

    .line 823437
    invoke-virtual {v0, v1, v4}, LX/505;->b(ILjava/lang/Object;)I

    move-result v5

    .line 823438
    if-ne v5, v1, :cond_4

    .line 823439
    const/4 v6, 0x0

    .line 823440
    invoke-static {v1}, LX/505;->d(I)I

    move-result v5

    const/4 v7, 0x2

    invoke-static {v0, v5, v7}, LX/505;->b(LX/505;II)I

    move-result v5

    move v5, v5

    .line 823441
    if-lez v5, :cond_9

    iget-object v7, v0, LX/505;->a:LX/1sm;

    iget-object p0, v0, LX/505;->c:LX/508;

    invoke-virtual {p0, v5}, LX/508;->a(I)Ljava/lang/Object;

    move-result-object p0

    invoke-virtual {v7, p0, v4}, LX/1sm;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v7

    if-gez v7, :cond_9

    .line 823442
    iget-object v7, v0, LX/505;->c:LX/508;

    iget-object v7, v7, LX/508;->d:[Ljava/lang/Object;

    iget-object p0, v0, LX/505;->c:LX/508;

    invoke-virtual {p0, v5}, LX/508;->a(I)Ljava/lang/Object;

    move-result-object p0

    aput-object p0, v7, v1

    .line 823443
    iget-object v7, v0, LX/505;->c:LX/508;

    iget-object v7, v7, LX/508;->d:[Ljava/lang/Object;

    aput-object v4, v7, v5

    .line 823444
    :goto_2
    move v7, v5

    .line 823445
    if-ne v7, v1, :cond_6

    move-object v5, v6

    .line 823446
    :goto_3
    move-object v0, v5

    .line 823447
    :goto_4
    move-object v1, v0

    .line 823448
    if-ge v3, p1, :cond_2

    .line 823449
    if-nez v1, :cond_1

    .line 823450
    new-instance v0, LX/506;

    invoke-direct {v0, v2, v4}, LX/506;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 823451
    :cond_1
    new-instance v0, LX/506;

    iget-object v1, v1, LX/506;->b:Ljava/lang/Object;

    invoke-direct {v0, v2, v1}, LX/506;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_2
    move-object v0, v1

    .line 823452
    goto/16 :goto_0

    :cond_3
    iget-object v3, v1, LX/505;->c:LX/508;

    iget v3, v3, LX/508;->e:I

    goto :goto_1

    :cond_4
    if-ge v5, p1, :cond_5

    new-instance v0, LX/506;

    invoke-virtual {p0, p1}, LX/508;->a(I)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v4, v1}, LX/506;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_4

    :cond_5
    const/4 v0, 0x0

    goto :goto_4

    .line 823453
    :cond_6
    if-ge v7, p1, :cond_7

    .line 823454
    iget-object v5, v0, LX/505;->c:LX/508;

    invoke-virtual {v5, p1}, LX/508;->a(I)Ljava/lang/Object;

    move-result-object v5

    .line 823455
    :goto_5
    iget-object p0, v0, LX/505;->b:LX/505;

    invoke-virtual {p0, v7, v4}, LX/505;->b(ILjava/lang/Object;)I

    move-result v7

    if-ge v7, p1, :cond_8

    .line 823456
    new-instance v6, LX/506;

    invoke-direct {v6, v4, v5}, LX/506;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v5, v6

    goto :goto_3

    .line 823457
    :cond_7
    iget-object v5, v0, LX/505;->c:LX/508;

    invoke-static {p1}, LX/505;->f(I)I

    move-result p0

    invoke-virtual {v5, p0}, LX/508;->a(I)Ljava/lang/Object;

    move-result-object v5

    goto :goto_5

    :cond_8
    move-object v5, v6

    .line 823458
    goto :goto_3

    :cond_9
    invoke-static {v0, v1, v4}, LX/505;->c(LX/505;ILjava/lang/Object;)I

    move-result v5

    goto :goto_2
.end method

.method public final b()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 823377
    invoke-virtual {p0}, LX/508;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, LX/508;->c$redex0(LX/508;)I

    move-result v0

    invoke-virtual {p0, v0}, LX/508;->a(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final clear()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 823410
    move v0, v1

    :goto_0
    iget v2, p0, LX/508;->e:I

    if-ge v0, v2, :cond_0

    .line 823411
    iget-object v2, p0, LX/508;->d:[Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v3, v2, v0

    .line 823412
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 823413
    :cond_0
    iput v1, p0, LX/508;->e:I

    .line 823414
    return-void
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 823409
    new-instance v0, LX/507;

    invoke-direct {v0, p0}, LX/507;-><init>(LX/508;)V

    return-object v0
.end method

.method public final offer(Ljava/lang/Object;)Z
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)Z"
        }
    .end annotation

    .prologue
    .line 823384
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 823385
    iget v0, p0, LX/508;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/508;->f:I

    .line 823386
    iget v0, p0, LX/508;->e:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, LX/508;->e:I

    .line 823387
    const/4 v5, 0x0

    .line 823388
    iget v2, p0, LX/508;->e:I

    iget-object v3, p0, LX/508;->d:[Ljava/lang/Object;

    array-length v3, v3

    if-le v2, v3, :cond_0

    .line 823389
    iget-object v6, p0, LX/508;->d:[Ljava/lang/Object;

    array-length v6, v6

    .line 823390
    const/16 v7, 0x40

    if-ge v6, v7, :cond_3

    add-int/lit8 v6, v6, 0x1

    mul-int/lit8 v6, v6, 0x2

    .line 823391
    :goto_0
    iget v7, p0, LX/508;->a:I

    invoke-static {v6, v7}, LX/508;->a(II)I

    move-result v6

    move v2, v6

    .line 823392
    new-array v2, v2, [Ljava/lang/Object;

    .line 823393
    iget-object v3, p0, LX/508;->d:[Ljava/lang/Object;

    iget-object v4, p0, LX/508;->d:[Ljava/lang/Object;

    array-length v4, v4

    invoke-static {v3, v5, v2, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 823394
    iput-object v2, p0, LX/508;->d:[Ljava/lang/Object;

    .line 823395
    :cond_0
    invoke-static {p0, v0}, LX/508;->d(LX/508;I)LX/505;

    move-result-object v1

    .line 823396
    invoke-static {v1, v0, p1}, LX/505;->c(LX/505;ILjava/lang/Object;)I

    move-result v2

    .line 823397
    if-ne v2, v0, :cond_5

    .line 823398
    :goto_1
    invoke-virtual {v1, v0, p1}, LX/505;->b(ILjava/lang/Object;)I

    .line 823399
    iget v0, p0, LX/508;->e:I

    iget v1, p0, LX/508;->a:I

    if-le v0, v1, :cond_1

    .line 823400
    invoke-virtual {p0}, LX/508;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x0

    :goto_2
    move-object v0, v0

    .line 823401
    if-eq v0, p1, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_3
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_3

    .line 823402
    :cond_3
    div-int/lit8 v6, v6, 0x2

    const/4 v7, 0x3

    .line 823403
    int-to-long v8, v6

    int-to-long v10, v7

    mul-long/2addr v10, v8

    .line 823404
    long-to-int v8, v10

    int-to-long v8, v8

    cmp-long v8, v10, v8

    if-nez v8, :cond_4

    const/4 v8, 0x1

    :goto_4
    invoke-static {v8}, LX/0z9;->b(Z)V

    .line 823405
    long-to-int v8, v10

    move v6, v8

    .line 823406
    goto :goto_0

    .line 823407
    :cond_4
    const/4 v8, 0x0

    goto :goto_4

    .line 823408
    :cond_5
    iget-object v1, v1, LX/505;->b:LX/505;

    move v0, v2

    goto :goto_1

    :cond_6
    invoke-static {p0}, LX/508;->c$redex0(LX/508;)I

    move-result v0

    invoke-static {p0, v0}, LX/508;->c(LX/508;I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_2
.end method

.method public final peek()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 823383
    invoke-virtual {p0}, LX/508;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/508;->a(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final poll()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 823382
    invoke-virtual {p0}, LX/508;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/508;->c(LX/508;I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 823381
    iget v0, p0, LX/508;->e:I

    return v0
.end method

.method public final toArray()[Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 823378
    iget v0, p0, LX/508;->e:I

    new-array v0, v0, [Ljava/lang/Object;

    .line 823379
    iget-object v1, p0, LX/508;->d:[Ljava/lang/Object;

    iget v2, p0, LX/508;->e:I

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 823380
    return-object v0
.end method
