.class public abstract LX/3ut;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3us;


# instance fields
.field public a:Landroid/content/Context;

.field public b:Landroid/content/Context;

.field public c:LX/3v0;

.field public d:Landroid/view/LayoutInflater;

.field public e:Landroid/view/LayoutInflater;

.field public f:LX/3ux;

.field public g:LX/3uE;

.field private h:I

.field private i:I

.field public j:I


# direct methods
.method public constructor <init>(Landroid/content/Context;II)V
    .locals 1

    .prologue
    .line 649710
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 649711
    iput-object p1, p0, LX/3ut;->a:Landroid/content/Context;

    .line 649712
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, LX/3ut;->d:Landroid/view/LayoutInflater;

    .line 649713
    iput p2, p0, LX/3ut;->h:I

    .line 649714
    iput p3, p0, LX/3ut;->i:I

    .line 649715
    return-void
.end method


# virtual methods
.method public a(Landroid/view/ViewGroup;)LX/3ux;
    .locals 3

    .prologue
    .line 649724
    iget-object v0, p0, LX/3ut;->f:LX/3ux;

    if-nez v0, :cond_0

    .line 649725
    iget-object v0, p0, LX/3ut;->d:Landroid/view/LayoutInflater;

    iget v1, p0, LX/3ut;->h:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/3ux;

    iput-object v0, p0, LX/3ut;->f:LX/3ux;

    .line 649726
    iget-object v0, p0, LX/3ut;->f:LX/3ux;

    iget-object v1, p0, LX/3ut;->c:LX/3v0;

    invoke-interface {v0, v1}, LX/3ux;->a(LX/3v0;)V

    .line 649727
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/3ut;->b(Z)V

    .line 649728
    :cond_0
    iget-object v0, p0, LX/3ut;->f:LX/3ux;

    return-object v0
.end method

.method public a(LX/3v3;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 649719
    instance-of v0, p2, LX/3uq;

    if-eqz v0, :cond_0

    .line 649720
    check-cast p2, LX/3uq;

    move-object v0, p2

    .line 649721
    :goto_0
    invoke-virtual {p0, p1, v0}, LX/3ut;->a(LX/3v3;LX/3uq;)V

    .line 649722
    check-cast v0, Landroid/view/View;

    return-object v0

    .line 649723
    :cond_0
    invoke-virtual {p0, p3}, LX/3ut;->b(Landroid/view/ViewGroup;)LX/3uq;

    move-result-object v0

    goto :goto_0
.end method

.method public a(LX/3v0;Z)V
    .locals 1

    .prologue
    .line 649716
    iget-object v0, p0, LX/3ut;->g:LX/3uE;

    if-eqz v0, :cond_0

    .line 649717
    iget-object v0, p0, LX/3ut;->g:LX/3uE;

    invoke-interface {v0, p1, p2}, LX/3uE;->a(LX/3v0;Z)V

    .line 649718
    :cond_0
    return-void
.end method

.method public abstract a(LX/3v3;LX/3uq;)V
.end method

.method public a(Landroid/content/Context;LX/3v0;)V
    .locals 1

    .prologue
    .line 649706
    iput-object p1, p0, LX/3ut;->b:Landroid/content/Context;

    .line 649707
    iget-object v0, p0, LX/3ut;->b:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, LX/3ut;->e:Landroid/view/LayoutInflater;

    .line 649708
    iput-object p2, p0, LX/3ut;->c:LX/3v0;

    .line 649709
    return-void
.end method

.method public a(LX/3v3;)Z
    .locals 1

    .prologue
    .line 649729
    const/4 v0, 0x1

    return v0
.end method

.method public a(LX/3vG;)Z
    .locals 1

    .prologue
    .line 649703
    iget-object v0, p0, LX/3ut;->g:LX/3uE;

    if-eqz v0, :cond_0

    .line 649704
    iget-object v0, p0, LX/3ut;->g:LX/3uE;

    invoke-interface {v0, p1}, LX/3uE;->a_(LX/3v0;)Z

    move-result v0

    .line 649705
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/view/ViewGroup;I)Z
    .locals 1

    .prologue
    .line 649701
    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->removeViewAt(I)V

    .line 649702
    const/4 v0, 0x1

    return v0
.end method

.method public b(Landroid/view/ViewGroup;)LX/3uq;
    .locals 3

    .prologue
    .line 649670
    iget-object v0, p0, LX/3ut;->d:Landroid/view/LayoutInflater;

    iget v1, p0, LX/3ut;->i:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/3uq;

    return-object v0
.end method

.method public b(Z)V
    .locals 10

    .prologue
    const/4 v5, 0x0

    .line 649674
    iget-object v0, p0, LX/3ut;->f:LX/3ux;

    check-cast v0, Landroid/view/ViewGroup;

    .line 649675
    if-nez v0, :cond_1

    .line 649676
    :cond_0
    return-void

    .line 649677
    :cond_1
    iget-object v1, p0, LX/3ut;->c:LX/3v0;

    if-eqz v1, :cond_8

    .line 649678
    iget-object v1, p0, LX/3ut;->c:LX/3v0;

    invoke-virtual {v1}, LX/3v0;->k()V

    .line 649679
    iget-object v1, p0, LX/3ut;->c:LX/3v0;

    invoke-virtual {v1}, LX/3v0;->j()Ljava/util/ArrayList;

    move-result-object v7

    .line 649680
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v8

    move v6, v5

    move v4, v5

    .line 649681
    :goto_0
    if-ge v6, v8, :cond_6

    .line 649682
    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3v3;

    .line 649683
    invoke-virtual {p0, v1}, LX/3ut;->a(LX/3v3;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 649684
    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 649685
    instance-of v2, v3, LX/3uq;

    if-eqz v2, :cond_5

    move-object v2, v3

    check-cast v2, LX/3uq;

    invoke-interface {v2}, LX/3uq;->getItemData()LX/3v3;

    move-result-object v2

    .line 649686
    :goto_1
    invoke-virtual {p0, v1, v3, v0}, LX/3ut;->a(LX/3v3;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v9

    .line 649687
    if-eq v1, v2, :cond_2

    .line 649688
    invoke-virtual {v9, v5}, Landroid/view/View;->setPressed(Z)V

    .line 649689
    sget-object v1, LX/0vv;->a:LX/0w2;

    invoke-interface {v1, v9}, LX/0w2;->C(Landroid/view/View;)V

    .line 649690
    :cond_2
    if-eq v9, v3, :cond_4

    .line 649691
    invoke-virtual {v9}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 649692
    if-eqz v1, :cond_3

    .line 649693
    invoke-virtual {v1, v9}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 649694
    :cond_3
    iget-object v1, p0, LX/3ut;->f:LX/3ux;

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1, v9, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 649695
    :cond_4
    add-int/lit8 v1, v4, 0x1

    .line 649696
    :goto_2
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    move v4, v1

    goto :goto_0

    .line 649697
    :cond_5
    const/4 v2, 0x0

    goto :goto_1

    .line 649698
    :cond_6
    :goto_3
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-ge v4, v1, :cond_0

    .line 649699
    invoke-virtual {p0, v0, v4}, LX/3ut;->a(Landroid/view/ViewGroup;I)Z

    move-result v1

    if-nez v1, :cond_6

    .line 649700
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    :cond_7
    move v1, v4

    goto :goto_2

    :cond_8
    move v4, v5

    goto :goto_3
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 649673
    const/4 v0, 0x0

    return v0
.end method

.method public final b(LX/3v3;)Z
    .locals 1

    .prologue
    .line 649672
    const/4 v0, 0x0

    return v0
.end method

.method public final c(LX/3v3;)Z
    .locals 1

    .prologue
    .line 649671
    const/4 v0, 0x0

    return v0
.end method
