.class public LX/44J;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1MT;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 669604
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 669605
    return-void
.end method


# virtual methods
.method public final getFilesFromWorkerThread(Ljava/io/File;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v6, 0x0

    .line 669606
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 669607
    new-instance v3, Ljava/io/File;

    const-string v1, "acra_log.txt"

    invoke-direct {v3, p1, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 669608
    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 669609
    :try_start_1
    invoke-static {}, LX/009;->getInstance()LX/009;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v2, v4, v1}, LX/009;->writeReportToStream(Ljava/lang/Throwable;Ljava/io/OutputStream;)V

    .line 669610
    new-instance v2, Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;

    const-string v4, "acra_log.txt"

    invoke-static {v3}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v5, "text/plain"

    invoke-direct {v2, v4, v3, v5}, Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 669611
    invoke-static {v1, v6}, LX/1md;->a(Ljava/io/Closeable;Z)V

    .line 669612
    return-object v0

    .line 669613
    :catch_0
    move-exception v0

    move-object v1, v2

    .line 669614
    :goto_0
    :try_start_2
    new-instance v2, Ljava/io/IOException;

    const-string v3, "Failed to write ACRA log for bug report"

    invoke-direct {v2, v3, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 669615
    :catchall_0
    move-exception v0

    :goto_1
    if-eqz v1, :cond_0

    .line 669616
    invoke-static {v1, v6}, LX/1md;->a(Ljava/io/Closeable;Z)V

    :cond_0
    throw v0

    .line 669617
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_1

    .line 669618
    :catch_1
    move-exception v0

    goto :goto_0
.end method
