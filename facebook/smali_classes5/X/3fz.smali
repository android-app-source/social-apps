.class public LX/3fz;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/3fz;


# instance fields
.field public final a:LX/0Xl;


# direct methods
.method public constructor <init>(LX/0Xl;)V
    .locals 0
    .param p1    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 623929
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 623930
    iput-object p1, p0, LX/3fz;->a:LX/0Xl;

    .line 623931
    return-void
.end method

.method public static a(LX/0QB;)LX/3fz;
    .locals 4

    .prologue
    .line 623932
    sget-object v0, LX/3fz;->b:LX/3fz;

    if-nez v0, :cond_1

    .line 623933
    const-class v1, LX/3fz;

    monitor-enter v1

    .line 623934
    :try_start_0
    sget-object v0, LX/3fz;->b:LX/3fz;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 623935
    if-eqz v2, :cond_0

    .line 623936
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 623937
    new-instance p0, LX/3fz;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v3

    check-cast v3, LX/0Xl;

    invoke-direct {p0, v3}, LX/3fz;-><init>(LX/0Xl;)V

    .line 623938
    move-object v0, p0

    .line 623939
    sput-object v0, LX/3fz;->b:LX/3fz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 623940
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 623941
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 623942
    :cond_1
    sget-object v0, LX/3fz;->b:LX/3fz;

    return-object v0

    .line 623943
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 623944
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
