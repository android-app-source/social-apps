.class public LX/3R2;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile g:LX/3R2;


# instance fields
.field private final a:LX/2Pk;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/2cF;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0WJ;

.field private final d:LX/0Xl;

.field public e:LX/0Yb;

.field public final f:Ljava/util/concurrent/ExecutorService;


# direct methods
.method public constructor <init>(LX/2Pk;LX/0WJ;LX/0Xl;Ljava/util/concurrent/ExecutorService;)V
    .locals 1
    .param p3    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p4    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 577726
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 577727
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/3R2;->b:Ljava/util/List;

    .line 577728
    iput-object p1, p0, LX/3R2;->a:LX/2Pk;

    .line 577729
    iput-object p2, p0, LX/3R2;->c:LX/0WJ;

    .line 577730
    iput-object p3, p0, LX/3R2;->d:LX/0Xl;

    .line 577731
    iput-object p4, p0, LX/3R2;->f:Ljava/util/concurrent/ExecutorService;

    .line 577732
    return-void
.end method

.method public static a(LX/0QB;)LX/3R2;
    .locals 7

    .prologue
    .line 577744
    sget-object v0, LX/3R2;->g:LX/3R2;

    if-nez v0, :cond_1

    .line 577745
    const-class v1, LX/3R2;

    monitor-enter v1

    .line 577746
    :try_start_0
    sget-object v0, LX/3R2;->g:LX/3R2;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 577747
    if-eqz v2, :cond_0

    .line 577748
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 577749
    new-instance p0, LX/3R2;

    invoke-static {v0}, LX/2Pk;->getInstance__com_facebook_omnistore_module_OmnistoreComponentManager__INJECTED_BY_TemplateInjector(LX/0QB;)LX/2Pk;

    move-result-object v3

    check-cast v3, LX/2Pk;

    invoke-static {v0}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v4

    check-cast v4, LX/0WJ;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v5

    check-cast v5, LX/0Xl;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/ExecutorService;

    invoke-direct {p0, v3, v4, v5, v6}, LX/3R2;-><init>(LX/2Pk;LX/0WJ;LX/0Xl;Ljava/util/concurrent/ExecutorService;)V

    .line 577750
    move-object v0, p0

    .line 577751
    sput-object v0, LX/3R2;->g:LX/3R2;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 577752
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 577753
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 577754
    :cond_1
    sget-object v0, LX/3R2;->g:LX/3R2;

    return-object v0

    .line 577755
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 577756
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static b$redex0(LX/3R2;)V
    .locals 3

    .prologue
    .line 577757
    iget-object v0, p0, LX/3R2;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2cF;

    .line 577758
    iget-object v2, p0, LX/3R2;->a:LX/2Pk;

    invoke-virtual {v2, v0}, LX/2Pk;->checkComponentSubscription(LX/2cF;)V

    goto :goto_0

    .line 577759
    :cond_0
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 3

    .prologue
    .line 577737
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/3R2;->c:LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 577738
    invoke-static {p0}, LX/3R2;->b$redex0(LX/3R2;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 577739
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 577740
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/3R2;->e:LX/0Yb;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/3R2;->e:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 577741
    :cond_2
    iget-object v0, p0, LX/3R2;->d:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.orca.login.AuthStateMachineMonitor.LOGIN_COMPLETE"

    new-instance v2, LX/Dp8;

    invoke-direct {v2, p0}, LX/Dp8;-><init>(LX/3R2;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, LX/3R2;->e:LX/0Yb;

    .line 577742
    iget-object v0, p0, LX/3R2;->e:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 577743
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/2cF;)V
    .locals 1

    .prologue
    .line 577733
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/3R2;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 577734
    iget-object v0, p0, LX/3R2;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 577735
    monitor-exit p0

    return-void

    .line 577736
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
