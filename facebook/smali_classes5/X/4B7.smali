.class public LX/4B7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1qH;


# instance fields
.field private final nextCallback:LX/4B6;

.field private final previousCallback:LX/1qH;


# direct methods
.method public constructor <init>(LX/1qH;LX/4B6;)V
    .locals 0

    .prologue
    .line 677450
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 677451
    iput-object p1, p0, LX/4B7;->previousCallback:LX/1qH;

    .line 677452
    iput-object p2, p0, LX/4B7;->nextCallback:LX/4B6;

    .line 677453
    return-void
.end method


# virtual methods
.method public onOperationProgress(Lcom/facebook/fbservice/service/OperationResult;)V
    .locals 2

    .prologue
    .line 677454
    iget-object v0, p0, LX/4B7;->previousCallback:LX/1qH;

    iget-object v1, p0, LX/4B7;->nextCallback:LX/4B6;

    invoke-virtual {v1, p1}, LX/4B6;->onOperationProgressChain(Lcom/facebook/fbservice/service/OperationResult;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1qH;->onOperationProgress(Lcom/facebook/fbservice/service/OperationResult;)V

    .line 677455
    return-void
.end method
