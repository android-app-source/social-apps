.class public final LX/4AH;
.super Ljava/util/AbstractSet;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/AbstractSet",
        "<",
        "Lcom/facebook/dracula/runtime/jdk/DraculaMap$1$Dracula$Entry$1$Dracula",
        "<TK;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/4AJ;


# direct methods
.method public constructor <init>(LX/4AJ;)V
    .locals 0

    .prologue
    .line 675787
    iput-object p1, p0, LX/4AH;->a:LX/4AJ;

    invoke-direct {p0}, Ljava/util/AbstractSet;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Object;)LX/4AI;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Lcom/facebook/dracula/runtime/jdk/DraculaMap$1$Dracula$Entry$1$Dracula",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 675784
    :try_start_0
    check-cast p0, LX/4AI;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 675785
    :goto_0
    return-object p0

    :catch_0
    const/4 p0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final clear()V
    .locals 1

    .prologue
    .line 675782
    iget-object v0, p0, LX/4AH;->a:LX/4AJ;

    invoke-virtual {v0}, LX/4AB;->a()V

    .line 675783
    return-void
.end method

.method public final contains(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    .line 675756
    invoke-static {p1}, LX/4AH;->a(Ljava/lang/Object;)LX/4AI;

    move-result-object v0

    .line 675757
    if-nez v0, :cond_0

    .line 675758
    const/4 v0, 0x0

    .line 675759
    :goto_0
    return v0

    .line 675760
    :cond_0
    invoke-virtual {v0}, LX/4AI;->b()LX/1vs;

    move-result-object v1

    .line 675761
    iget-object v2, v1, LX/1vs;->a:LX/15i;

    .line 675762
    iget v3, v1, LX/1vs;->b:I

    .line 675763
    iget v1, v1, LX/1vs;->c:I

    .line 675764
    iget-object v4, p0, LX/4AH;->a:LX/4AJ;

    .line 675765
    iget-object p0, v0, LX/4AI;->a:Ljava/lang/Object;

    move-object v0, p0

    .line 675766
    invoke-static {v4, v0, v2, v3, v1}, LX/4AJ;->c(LX/4AJ;Ljava/lang/Object;LX/15i;II)Z

    move-result v0

    goto :goto_0
.end method

.method public final isEmpty()Z
    .locals 1

    .prologue
    .line 675786
    iget-object v0, p0, LX/4AH;->a:LX/4AJ;

    iget v0, v0, LX/4AJ;->e:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lcom/facebook/dracula/runtime/jdk/DraculaMap$1$Dracula$Entry$1$Dracula",
            "<TK;>;>;"
        }
    .end annotation

    .prologue
    .line 675779
    iget-object v0, p0, LX/4AH;->a:LX/4AJ;

    .line 675780
    new-instance v1, LX/4AG;

    invoke-direct {v1, v0}, LX/4AG;-><init>(LX/4AJ;)V

    move-object v0, v1

    .line 675781
    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    .line 675768
    invoke-static {p1}, LX/4AH;->a(Ljava/lang/Object;)LX/4AI;

    move-result-object v0

    .line 675769
    if-nez v0, :cond_0

    .line 675770
    const/4 v0, 0x0

    .line 675771
    :goto_0
    return v0

    .line 675772
    :cond_0
    invoke-virtual {v0}, LX/4AI;->b()LX/1vs;

    move-result-object v1

    .line 675773
    iget-object v2, v1, LX/1vs;->a:LX/15i;

    .line 675774
    iget v3, v1, LX/1vs;->b:I

    .line 675775
    iget v1, v1, LX/1vs;->c:I

    .line 675776
    iget-object v4, p0, LX/4AH;->a:LX/4AJ;

    .line 675777
    iget-object p0, v0, LX/4AI;->a:Ljava/lang/Object;

    move-object v0, p0

    .line 675778
    invoke-static {v4, v0, v2, v3, v1}, LX/4AJ;->d(LX/4AJ;Ljava/lang/Object;LX/15i;II)Z

    move-result v0

    goto :goto_0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 675767
    iget-object v0, p0, LX/4AH;->a:LX/4AJ;

    iget v0, v0, LX/4AJ;->e:I

    return v0
.end method
