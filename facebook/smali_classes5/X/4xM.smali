.class public abstract LX/4xM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Comparable;


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<C::",
        "Ljava/lang/Comparable;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/io/Serializable;",
        "Ljava/lang/Comparable",
        "<",
        "LX/4xM",
        "<TC;>;>;"
    }
.end annotation


# instance fields
.field public final endpoint:Ljava/lang/Comparable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TC;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Comparable;)V
    .locals 0
    .param p1    # Ljava/lang/Comparable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TC;)V"
        }
    .end annotation

    .prologue
    .line 820956
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 820957
    iput-object p1, p0, LX/4xM;->endpoint:Ljava/lang/Comparable;

    .line 820958
    return-void
.end method

.method public static b(Ljava/lang/Comparable;)LX/4xM;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<C::",
            "Ljava/lang/Comparable;",
            ">(TC;)",
            "LX/4xM",
            "<TC;>;"
        }
    .end annotation

    .prologue
    .line 820955
    new-instance v0, LX/4xQ;

    invoke-direct {v0, p0}, LX/4xQ;-><init>(Ljava/lang/Comparable;)V

    return-object v0
.end method

.method public static c(Ljava/lang/Comparable;)LX/4xM;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<C::",
            "Ljava/lang/Comparable;",
            ">(TC;)",
            "LX/4xM",
            "<TC;>;"
        }
    .end annotation

    .prologue
    .line 820954
    new-instance v0, LX/4xO;

    invoke-direct {v0, p0}, LX/4xO;-><init>(Ljava/lang/Comparable;)V

    return-object v0
.end method


# virtual methods
.method public a(LX/4xM;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4xM",
            "<TC;>;)I"
        }
    .end annotation

    .prologue
    .line 820944
    sget-object v0, LX/4xP;->a:LX/4xP;

    if-ne p1, v0, :cond_1

    .line 820945
    const/4 v0, 0x1

    .line 820946
    :cond_0
    :goto_0
    return v0

    .line 820947
    :cond_1
    sget-object v0, LX/4xN;->a:LX/4xN;

    if-ne p1, v0, :cond_2

    .line 820948
    const/4 v0, -0x1

    goto :goto_0

    .line 820949
    :cond_2
    iget-object v0, p0, LX/4xM;->endpoint:Ljava/lang/Comparable;

    iget-object v1, p1, LX/4xM;->endpoint:Ljava/lang/Comparable;

    invoke-static {v0, v1}, LX/50M;->c(Ljava/lang/Comparable;Ljava/lang/Comparable;)I

    move-result v0

    .line 820950
    if-nez v0, :cond_0

    .line 820951
    instance-of v0, p0, LX/4xO;

    instance-of v1, p1, LX/4xO;

    .line 820952
    if-ne v0, v1, :cond_3

    const/4 p0, 0x0

    :goto_1
    move v0, p0

    .line 820953
    goto :goto_0

    :cond_3
    if-eqz v0, :cond_4

    const/4 p0, 0x1

    goto :goto_1

    :cond_4
    const/4 p0, -0x1

    goto :goto_1
.end method

.method public abstract a()LX/4xG;
.end method

.method public abstract a(Ljava/lang/StringBuilder;)V
.end method

.method public abstract a(Ljava/lang/Comparable;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TC;)Z"
        }
    .end annotation
.end method

.method public abstract b()LX/4xG;
.end method

.method public abstract b(Ljava/lang/StringBuilder;)V
.end method

.method public c()Ljava/lang/Comparable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TC;"
        }
    .end annotation

    .prologue
    .line 820943
    iget-object v0, p0, LX/4xM;->endpoint:Ljava/lang/Comparable;

    return-object v0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 820942
    check-cast p1, LX/4xM;

    invoke-virtual {p0, p1}, LX/4xM;->a(LX/4xM;)I

    move-result v0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 820937
    instance-of v1, p1, LX/4xM;

    if-eqz v1, :cond_0

    .line 820938
    check-cast p1, LX/4xM;

    .line 820939
    :try_start_0
    invoke-virtual {p0, p1}, LX/4xM;->a(LX/4xM;)I
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 820940
    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 820941
    :cond_0
    :goto_0
    return v0

    :catch_0
    goto :goto_0
.end method
