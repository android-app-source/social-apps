.class public final LX/3fn;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/3fc;


# direct methods
.method public constructor <init>(LX/3fc;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 623502
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 623503
    iput-object p1, p0, LX/3fn;->a:LX/3fc;

    .line 623504
    return-void
.end method

.method public static b(LX/0QB;)LX/3fn;
    .locals 2

    .prologue
    .line 623469
    new-instance v1, LX/3fn;

    invoke-static {p0}, LX/3fc;->b(LX/0QB;)LX/3fc;

    move-result-object v0

    check-cast v0, LX/3fc;

    invoke-direct {v1, v0}, LX/3fn;-><init>(LX/3fc;)V

    .line 623470
    return-object v1
.end method


# virtual methods
.method public final a(ILjava/lang/String;LX/3gq;)LX/0gW;
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 623471
    sget-object v0, LX/3gq;->DELTA:LX/3gq;

    if-ne p3, v0, :cond_2

    .line 623472
    if-eqz p2, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 623473
    new-instance v0, LX/6ME;

    invoke-direct {v0}, LX/6ME;-><init>()V

    move-object v0, v0

    .line 623474
    const-string v2, "limit"

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/6ME;

    .line 623475
    if-eqz p2, :cond_0

    .line 623476
    const-string v2, "after"

    invoke-virtual {v0, v2, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 623477
    :cond_0
    iget-object v2, p0, LX/3fn;->a:LX/3fc;

    invoke-virtual {v2, v0}, LX/3fc;->b(LX/0gW;)V

    .line 623478
    iget-object v2, p0, LX/3fn;->a:LX/3fc;

    invoke-virtual {v2, v0}, LX/3fc;->a(LX/0gW;)V

    .line 623479
    iget-object v2, p0, LX/3fn;->a:LX/3fc;

    invoke-virtual {v2, v0}, LX/3fc;->c(LX/0gW;)V

    .line 623480
    iput-boolean v1, v0, LX/0gW;->l:Z

    .line 623481
    move-object v0, v0

    .line 623482
    :goto_1
    return-object v0

    .line 623483
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 623484
    :cond_2
    if-eqz p2, :cond_3

    .line 623485
    new-instance v0, LX/6MF;

    invoke-direct {v0}, LX/6MF;-><init>()V

    move-object v0, v0

    .line 623486
    const-string v2, "limit"

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/6MF;

    .line 623487
    const-string v2, "after"

    invoke-virtual {v0, v2, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 623488
    iget-object v2, p0, LX/3fn;->a:LX/3fc;

    invoke-virtual {v2, v0}, LX/3fc;->b(LX/0gW;)V

    .line 623489
    iget-object v2, p0, LX/3fn;->a:LX/3fc;

    invoke-virtual {v2, v0}, LX/3fc;->a(LX/0gW;)V

    .line 623490
    iget-object v2, p0, LX/3fn;->a:LX/3fc;

    invoke-virtual {v2, v0}, LX/3fc;->c(LX/0gW;)V

    .line 623491
    iput-boolean v1, v0, LX/0gW;->l:Z

    .line 623492
    move-object v0, v0

    .line 623493
    goto :goto_1

    .line 623494
    :cond_3
    new-instance v0, LX/3gs;

    invoke-direct {v0}, LX/3gs;-><init>()V

    move-object v0, v0

    .line 623495
    const-string v2, "limit"

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/3gs;

    .line 623496
    iget-object v2, p0, LX/3fn;->a:LX/3fc;

    invoke-virtual {v2, v0}, LX/3fc;->b(LX/0gW;)V

    .line 623497
    iget-object v2, p0, LX/3fn;->a:LX/3fc;

    invoke-virtual {v2, v0}, LX/3fc;->a(LX/0gW;)V

    .line 623498
    iget-object v2, p0, LX/3fn;->a:LX/3fc;

    invoke-virtual {v2, v0}, LX/3fc;->c(LX/0gW;)V

    .line 623499
    iput-boolean v1, v0, LX/0gW;->l:Z

    .line 623500
    move-object v0, v0

    .line 623501
    goto :goto_1
.end method
