.class public LX/4g9;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/4g9;


# instance fields
.field public final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 799151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 799152
    iput-object p1, p0, LX/4g9;->a:LX/0Zb;

    .line 799153
    return-void
.end method

.method public static a(LX/0QB;)LX/4g9;
    .locals 4

    .prologue
    .line 799154
    sget-object v0, LX/4g9;->b:LX/4g9;

    if-nez v0, :cond_1

    .line 799155
    const-class v1, LX/4g9;

    monitor-enter v1

    .line 799156
    :try_start_0
    sget-object v0, LX/4g9;->b:LX/4g9;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 799157
    if-eqz v2, :cond_0

    .line 799158
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 799159
    new-instance p0, LX/4g9;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-direct {p0, v3}, LX/4g9;-><init>(LX/0Zb;)V

    .line 799160
    move-object v0, p0

    .line 799161
    sput-object v0, LX/4g9;->b:LX/4g9;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 799162
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 799163
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 799164
    :cond_1
    sget-object v0, LX/4g9;->b:LX/4g9;

    return-object v0

    .line 799165
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 799166
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/4g5;)V
    .locals 1

    .prologue
    .line 799167
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {p0, p1, v0}, LX/4g9;->a(LX/4g5;Ljava/util/Map;)V

    .line 799168
    return-void
.end method

.method public final a(LX/4g5;Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4g5;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 799169
    iget-object v0, p0, LX/4g9;->a:LX/0Zb;

    iget-object v1, p1, LX/4g5;->n:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 799170
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 799171
    invoke-virtual {v0, p2}, LX/0oG;->a(Ljava/util/Map;)LX/0oG;

    .line 799172
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 799173
    :cond_0
    return-void
.end method
