.class public LX/4wI;
.super LX/0QG;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LX/0QG",
        "<TK;TV;>;",
        "Ljava/io/Serializable;"
    }
.end annotation


# instance fields
.field public transient b:LX/0QI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QI",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public final concurrencyLevel:I

.field public final expireAfterAccessNanos:J

.field public final expireAfterWriteNanos:J

.field public final keyEquivalence:LX/0Qj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Qj",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final keyStrength:LX/0QX;

.field public final loader:LX/0QM;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QM",
            "<-TK;TV;>;"
        }
    .end annotation
.end field

.field public final maxWeight:J

.field public final removalListener:LX/0Qn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Qn",
            "<-TK;-TV;>;"
        }
    .end annotation
.end field

.field public final ticker:LX/0QV;

.field public final valueEquivalence:LX/0Qj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Qj",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final valueStrength:LX/0QX;

.field public final weigher:LX/0Ql;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ql",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(LX/0QX;LX/0QX;LX/0Qj;LX/0Qj;JJJLX/0Ql;ILX/0Qn;LX/0QV;LX/0QM;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QX;",
            "LX/0QX;",
            "LX/0Qj",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "LX/0Qj",
            "<",
            "Ljava/lang/Object;",
            ">;JJJ",
            "LX/0Ql",
            "<TK;TV;>;I",
            "LX/0Qn",
            "<-TK;-TV;>;",
            "LX/0QV;",
            "LX/0QM",
            "<-TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 819991
    invoke-direct {p0}, LX/0QG;-><init>()V

    .line 819992
    iput-object p1, p0, LX/4wI;->keyStrength:LX/0QX;

    .line 819993
    iput-object p2, p0, LX/4wI;->valueStrength:LX/0QX;

    .line 819994
    iput-object p3, p0, LX/4wI;->keyEquivalence:LX/0Qj;

    .line 819995
    iput-object p4, p0, LX/4wI;->valueEquivalence:LX/0Qj;

    .line 819996
    iput-wide p5, p0, LX/4wI;->expireAfterWriteNanos:J

    .line 819997
    iput-wide p7, p0, LX/4wI;->expireAfterAccessNanos:J

    .line 819998
    iput-wide p9, p0, LX/4wI;->maxWeight:J

    .line 819999
    iput-object p11, p0, LX/4wI;->weigher:LX/0Ql;

    .line 820000
    iput p12, p0, LX/4wI;->concurrencyLevel:I

    .line 820001
    move-object/from16 v0, p13

    iput-object v0, p0, LX/4wI;->removalListener:LX/0Qn;

    .line 820002
    invoke-static {}, LX/0QV;->systemTicker()LX/0QV;

    move-result-object v1

    move-object/from16 v0, p14

    if-eq v0, v1, :cond_0

    sget-object v1, LX/0QN;->d:LX/0QV;

    move-object/from16 v0, p14

    if-ne v0, v1, :cond_1

    :cond_0
    const/16 p14, 0x0

    :cond_1
    move-object/from16 v0, p14

    iput-object v0, p0, LX/4wI;->ticker:LX/0QV;

    .line 820003
    move-object/from16 v0, p15

    iput-object v0, p0, LX/4wI;->loader:LX/0QM;

    .line 820004
    return-void
.end method

.method public constructor <init>(LX/0Qd;)V
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Qd",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 820005
    move-object/from16 v0, p1

    iget-object v4, v0, LX/0Qd;->h:LX/0QX;

    move-object/from16 v0, p1

    iget-object v5, v0, LX/0Qd;->i:LX/0QX;

    move-object/from16 v0, p1

    iget-object v6, v0, LX/0Qd;->f:LX/0Qj;

    move-object/from16 v0, p1

    iget-object v7, v0, LX/0Qd;->g:LX/0Qj;

    move-object/from16 v0, p1

    iget-wide v8, v0, LX/0Qd;->m:J

    move-object/from16 v0, p1

    iget-wide v10, v0, LX/0Qd;->l:J

    move-object/from16 v0, p1

    iget-wide v12, v0, LX/0Qd;->j:J

    move-object/from16 v0, p1

    iget-object v14, v0, LX/0Qd;->k:LX/0Ql;

    move-object/from16 v0, p1

    iget v15, v0, LX/0Qd;->e:I

    move-object/from16 v0, p1

    iget-object v0, v0, LX/0Qd;->p:LX/0Qn;

    move-object/from16 v16, v0

    move-object/from16 v0, p1

    iget-object v0, v0, LX/0Qd;->q:LX/0QV;

    move-object/from16 v17, v0

    move-object/from16 v0, p1

    iget-object v0, v0, LX/0Qd;->t:LX/0QM;

    move-object/from16 v18, v0

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v18}, LX/4wI;-><init>(LX/0QX;LX/0QX;LX/0Qj;LX/0Qj;JJJLX/0Ql;ILX/0Qn;LX/0QV;LX/0QM;)V

    .line 820006
    return-void
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 1

    .prologue
    .line 820007
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 820008
    invoke-virtual {p0}, LX/4wI;->f()LX/0QN;

    move-result-object v0

    .line 820009
    invoke-virtual {v0}, LX/0QN;->q()LX/0QI;

    move-result-object v0

    iput-object v0, p0, LX/4wI;->b:LX/0QI;

    .line 820010
    return-void
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 820011
    iget-object v0, p0, LX/4wI;->b:LX/0QI;

    return-object v0
.end method


# virtual methods
.method public final d()LX/0QI;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0QI",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 820012
    iget-object v0, p0, LX/4wI;->b:LX/0QI;

    return-object v0
.end method

.method public final synthetic e()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 820013
    invoke-virtual {p0}, LX/4wI;->d()LX/0QI;

    move-result-object v0

    return-object v0
.end method

.method public final f()LX/0QN;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0QN",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    const-wide/16 v6, 0x0

    const-wide/16 v4, -0x1

    .line 820014
    invoke-static {}, LX/0QN;->newBuilder()LX/0QN;

    move-result-object v0

    iget-object v1, p0, LX/4wI;->keyStrength:LX/0QX;

    invoke-virtual {v0, v1}, LX/0QN;->a(LX/0QX;)LX/0QN;

    move-result-object v0

    iget-object v1, p0, LX/4wI;->valueStrength:LX/0QX;

    invoke-virtual {v0, v1}, LX/0QN;->b(LX/0QX;)LX/0QN;

    move-result-object v0

    iget-object v1, p0, LX/4wI;->keyEquivalence:LX/0Qj;

    const/4 v3, 0x1

    const/4 v8, 0x0

    .line 820015
    iget-object v2, v0, LX/0QN;->p:LX/0Qj;

    if-nez v2, :cond_5

    move v2, v3

    :goto_0
    const-string v9, "key equivalence was already set to %s"

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v10, v0, LX/0QN;->p:LX/0Qj;

    aput-object v10, v3, v8

    invoke-static {v2, v9, v3}, LX/0PB;->checkState(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 820016
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Qj;

    iput-object v2, v0, LX/0QN;->p:LX/0Qj;

    .line 820017
    move-object v0, v0

    .line 820018
    iget-object v1, p0, LX/4wI;->valueEquivalence:LX/0Qj;

    const/4 v3, 0x1

    const/4 v8, 0x0

    .line 820019
    iget-object v2, v0, LX/0QN;->q:LX/0Qj;

    if-nez v2, :cond_6

    move v2, v3

    :goto_1
    const-string v9, "value equivalence was already set to %s"

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v10, v0, LX/0QN;->q:LX/0Qj;

    aput-object v10, v3, v8

    invoke-static {v2, v9, v3}, LX/0PB;->checkState(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 820020
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Qj;

    iput-object v2, v0, LX/0QN;->q:LX/0Qj;

    .line 820021
    move-object v0, v0

    .line 820022
    iget v1, p0, LX/4wI;->concurrencyLevel:I

    invoke-virtual {v0, v1}, LX/0QN;->b(I)LX/0QN;

    move-result-object v0

    iget-object v1, p0, LX/4wI;->removalListener:LX/0Qn;

    invoke-virtual {v0, v1}, LX/0QN;->a(LX/0Qn;)LX/0QN;

    move-result-object v0

    .line 820023
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/0QN;->e:Z

    .line 820024
    iget-wide v2, p0, LX/4wI;->expireAfterWriteNanos:J

    cmp-long v1, v2, v6

    if-lez v1, :cond_0

    .line 820025
    iget-wide v2, p0, LX/4wI;->expireAfterWriteNanos:J

    sget-object v1, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, LX/0QN;->a(JLjava/util/concurrent/TimeUnit;)LX/0QN;

    .line 820026
    :cond_0
    iget-wide v2, p0, LX/4wI;->expireAfterAccessNanos:J

    cmp-long v1, v2, v6

    if-lez v1, :cond_1

    .line 820027
    iget-wide v2, p0, LX/4wI;->expireAfterAccessNanos:J

    sget-object v1, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, LX/0QN;->b(JLjava/util/concurrent/TimeUnit;)LX/0QN;

    .line 820028
    :cond_1
    iget-object v1, p0, LX/4wI;->weigher:LX/0Ql;

    sget-object v2, LX/0Qk;->INSTANCE:LX/0Qk;

    if-eq v1, v2, :cond_4

    .line 820029
    iget-object v1, p0, LX/4wI;->weigher:LX/0Ql;

    invoke-virtual {v0, v1}, LX/0QN;->a(LX/0Ql;)LX/0QN;

    .line 820030
    iget-wide v2, p0, LX/4wI;->maxWeight:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_2

    .line 820031
    iget-wide v2, p0, LX/4wI;->maxWeight:J

    invoke-virtual {v0, v2, v3}, LX/0QN;->b(J)LX/0QN;

    .line 820032
    :cond_2
    :goto_2
    iget-object v1, p0, LX/4wI;->ticker:LX/0QV;

    if-eqz v1, :cond_3

    .line 820033
    iget-object v1, p0, LX/4wI;->ticker:LX/0QV;

    .line 820034
    iget-object v2, v0, LX/0QN;->s:LX/0QV;

    if-nez v2, :cond_7

    const/4 v2, 0x1

    :goto_3
    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 820035
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0QV;

    iput-object v2, v0, LX/0QN;->s:LX/0QV;

    .line 820036
    :cond_3
    return-object v0

    .line 820037
    :cond_4
    iget-wide v2, p0, LX/4wI;->maxWeight:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_2

    .line 820038
    iget-wide v2, p0, LX/4wI;->maxWeight:J

    invoke-virtual {v0, v2, v3}, LX/0QN;->a(J)LX/0QN;

    goto :goto_2

    :cond_5
    move v2, v8

    .line 820039
    goto/16 :goto_0

    :cond_6
    move v2, v8

    .line 820040
    goto :goto_1

    .line 820041
    :cond_7
    const/4 v2, 0x0

    goto :goto_3
.end method
