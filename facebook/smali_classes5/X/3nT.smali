.class public LX/3nT;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;

.field private static b:Z

.field public static c:Ljava/lang/Class;

.field public static d:Ljava/lang/reflect/Field;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 638719
    const-class v0, LX/3nT;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/3nT;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 638720
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 638721
    return-void
.end method

.method public static a()V
    .locals 4

    .prologue
    .line 638722
    sget-boolean v0, LX/3nT;->b:Z

    if-nez v0, :cond_1

    .line 638723
    const-class v1, LX/3nT;

    monitor-enter v1

    .line 638724
    :try_start_0
    sget-boolean v0, LX/3nT;->b:Z

    if-nez v0, :cond_0

    .line 638725
    const/4 v0, 0x1

    sput-boolean v0, LX/3nT;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 638726
    :try_start_1
    const-string v0, "libcore.io.ErrnoException"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 638727
    const-string v2, "errno"

    invoke-virtual {v0, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    .line 638728
    sput-object v0, LX/3nT;->c:Ljava/lang/Class;

    .line 638729
    sput-object v2, LX/3nT;->d:Ljava/lang/reflect/Field;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 638730
    :cond_0
    :goto_0
    :try_start_2
    monitor-exit v1

    .line 638731
    :cond_1
    return-void

    .line 638732
    :catch_0
    move-exception v0

    .line 638733
    sget-object v2, LX/3nT;->a:Ljava/lang/String;

    const-string v3, "Error loading errno exception class"

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 638734
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method
