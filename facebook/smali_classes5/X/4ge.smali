.class public final enum LX/4ge;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/4ge;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/4ge;

.field public static final enum ACCOUNT_LINK:LX/4ge;

.field public static final enum FACEBOOK_REPORT_A_PROBLEM:LX/4ge;

.field public static final enum MANAGE_MESSAGES:LX/4ge;

.field public static final enum OPEN_CANCEL_RIDE_MUTATION:LX/4ge;

.field public static final enum OPEN_NATIVE:LX/4ge;

.field public static final enum OPEN_URL:LX/4ge;

.field public static final enum PAYMENT:LX/4ge;

.field public static final enum POSTBACK:LX/4ge;

.field public static final enum SHARE:LX/4ge;

.field public static final enum SUBSCRIPTION_PRESELECT:LX/4ge;


# instance fields
.field public final dbValue:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 799825
    new-instance v0, LX/4ge;

    const-string v1, "OPEN_NATIVE"

    const-string v2, "OPEN_NATIVE"

    invoke-direct {v0, v1, v4, v2}, LX/4ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/4ge;->OPEN_NATIVE:LX/4ge;

    .line 799826
    new-instance v0, LX/4ge;

    const-string v1, "OPEN_URL"

    const-string v2, "OPEN_URL"

    invoke-direct {v0, v1, v5, v2}, LX/4ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/4ge;->OPEN_URL:LX/4ge;

    .line 799827
    new-instance v0, LX/4ge;

    const-string v1, "OPEN_CANCEL_RIDE_MUTATION"

    const-string v2, "OPEN_CANCEL_RIDE_MUTATION"

    invoke-direct {v0, v1, v6, v2}, LX/4ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/4ge;->OPEN_CANCEL_RIDE_MUTATION:LX/4ge;

    .line 799828
    new-instance v0, LX/4ge;

    const-string v1, "POSTBACK"

    const-string v2, "POSTBACK"

    invoke-direct {v0, v1, v7, v2}, LX/4ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/4ge;->POSTBACK:LX/4ge;

    .line 799829
    new-instance v0, LX/4ge;

    const-string v1, "ACCOUNT_LINK"

    const-string v2, "ACCOUNT_LINK"

    invoke-direct {v0, v1, v8, v2}, LX/4ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/4ge;->ACCOUNT_LINK:LX/4ge;

    .line 799830
    new-instance v0, LX/4ge;

    const-string v1, "MANAGE_MESSAGES"

    const/4 v2, 0x5

    const-string v3, "MANAGE_MESSAGES"

    invoke-direct {v0, v1, v2, v3}, LX/4ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/4ge;->MANAGE_MESSAGES:LX/4ge;

    .line 799831
    new-instance v0, LX/4ge;

    const-string v1, "PAYMENT"

    const/4 v2, 0x6

    const-string v3, "PAYMENT"

    invoke-direct {v0, v1, v2, v3}, LX/4ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/4ge;->PAYMENT:LX/4ge;

    .line 799832
    new-instance v0, LX/4ge;

    const-string v1, "SHARE"

    const/4 v2, 0x7

    const-string v3, "SHARE"

    invoke-direct {v0, v1, v2, v3}, LX/4ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/4ge;->SHARE:LX/4ge;

    .line 799833
    new-instance v0, LX/4ge;

    const-string v1, "SUBSCRIPTION_PRESELECT"

    const/16 v2, 0x8

    const-string v3, "SUBSCRIPTION_PRESELECT"

    invoke-direct {v0, v1, v2, v3}, LX/4ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/4ge;->SUBSCRIPTION_PRESELECT:LX/4ge;

    .line 799834
    new-instance v0, LX/4ge;

    const-string v1, "FACEBOOK_REPORT_A_PROBLEM"

    const/16 v2, 0x9

    const-string v3, "FACEBOOK_REPORT_A_PROBLEM"

    invoke-direct {v0, v1, v2, v3}, LX/4ge;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/4ge;->FACEBOOK_REPORT_A_PROBLEM:LX/4ge;

    .line 799835
    const/16 v0, 0xa

    new-array v0, v0, [LX/4ge;

    sget-object v1, LX/4ge;->OPEN_NATIVE:LX/4ge;

    aput-object v1, v0, v4

    sget-object v1, LX/4ge;->OPEN_URL:LX/4ge;

    aput-object v1, v0, v5

    sget-object v1, LX/4ge;->OPEN_CANCEL_RIDE_MUTATION:LX/4ge;

    aput-object v1, v0, v6

    sget-object v1, LX/4ge;->POSTBACK:LX/4ge;

    aput-object v1, v0, v7

    sget-object v1, LX/4ge;->ACCOUNT_LINK:LX/4ge;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/4ge;->MANAGE_MESSAGES:LX/4ge;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/4ge;->PAYMENT:LX/4ge;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/4ge;->SHARE:LX/4ge;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/4ge;->SUBSCRIPTION_PRESELECT:LX/4ge;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/4ge;->FACEBOOK_REPORT_A_PROBLEM:LX/4ge;

    aput-object v2, v0, v1

    sput-object v0, LX/4ge;->$VALUES:[LX/4ge;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 799838
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 799839
    iput-object p3, p0, LX/4ge;->dbValue:Ljava/lang/String;

    .line 799840
    return-void
.end method

.method public static fromDbValue(Ljava/lang/String;)LX/4ge;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 799841
    invoke-static {}, LX/4ge;->values()[LX/4ge;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 799842
    iget-object v4, v0, LX/4ge;->dbValue:Ljava/lang/String;

    invoke-static {v4, p0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 799843
    :goto_1
    return-object v0

    .line 799844
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 799845
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LX/4ge;
    .locals 1

    .prologue
    .line 799837
    const-class v0, LX/4ge;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/4ge;

    return-object v0
.end method

.method public static values()[LX/4ge;
    .locals 1

    .prologue
    .line 799836
    sget-object v0, LX/4ge;->$VALUES:[LX/4ge;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/4ge;

    return-object v0
.end method
