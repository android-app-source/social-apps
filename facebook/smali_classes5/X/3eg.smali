.class public final enum LX/3eg;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/3eg;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/3eg;

.field public static final enum DO_NOT_UPDATE_IF_CACHED:LX/3eg;

.field public static final enum REPLACE_FROM_NETWORK:LX/3eg;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 620497
    new-instance v0, LX/3eg;

    const-string v1, "REPLACE_FROM_NETWORK"

    invoke-direct {v0, v1, v2}, LX/3eg;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3eg;->REPLACE_FROM_NETWORK:LX/3eg;

    .line 620498
    new-instance v0, LX/3eg;

    const-string v1, "DO_NOT_UPDATE_IF_CACHED"

    invoke-direct {v0, v1, v3}, LX/3eg;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3eg;->DO_NOT_UPDATE_IF_CACHED:LX/3eg;

    .line 620499
    const/4 v0, 0x2

    new-array v0, v0, [LX/3eg;

    sget-object v1, LX/3eg;->REPLACE_FROM_NETWORK:LX/3eg;

    aput-object v1, v0, v2

    sget-object v1, LX/3eg;->DO_NOT_UPDATE_IF_CACHED:LX/3eg;

    aput-object v1, v0, v3

    sput-object v0, LX/3eg;->$VALUES:[LX/3eg;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 620500
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/3eg;
    .locals 1

    .prologue
    .line 620501
    const-class v0, LX/3eg;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/3eg;

    return-object v0
.end method

.method public static values()[LX/3eg;
    .locals 1

    .prologue
    .line 620502
    sget-object v0, LX/3eg;->$VALUES:[LX/3eg;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/3eg;

    return-object v0
.end method
