.class public final LX/4rG;
.super LX/0rP;
.source ""


# static fields
.field private static final b:Ljava/math/BigInteger;

.field private static final c:Ljava/math/BigInteger;

.field private static final d:Ljava/math/BigInteger;

.field private static final e:Ljava/math/BigInteger;


# instance fields
.field public final a:Ljava/math/BigInteger;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 815388
    const-wide/32 v0, -0x80000000

    invoke-static {v0, v1}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v0

    sput-object v0, LX/4rG;->b:Ljava/math/BigInteger;

    .line 815389
    const-wide/32 v0, 0x7fffffff

    invoke-static {v0, v1}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v0

    sput-object v0, LX/4rG;->c:Ljava/math/BigInteger;

    .line 815390
    const-wide/high16 v0, -0x8000000000000000L

    invoke-static {v0, v1}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v0

    sput-object v0, LX/4rG;->d:Ljava/math/BigInteger;

    .line 815391
    const-wide v0, 0x7fffffffffffffffL

    invoke-static {v0, v1}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v0

    sput-object v0, LX/4rG;->e:Ljava/math/BigInteger;

    return-void
.end method

.method private constructor <init>(Ljava/math/BigInteger;)V
    .locals 0

    .prologue
    .line 815392
    invoke-direct {p0}, LX/0rP;-><init>()V

    iput-object p1, p0, LX/4rG;->a:Ljava/math/BigInteger;

    return-void
.end method

.method public static a(Ljava/math/BigInteger;)LX/4rG;
    .locals 1

    .prologue
    .line 815393
    new-instance v0, LX/4rG;

    invoke-direct {v0, p0}, LX/4rG;-><init>(Ljava/math/BigInteger;)V

    return-object v0
.end method


# virtual methods
.method public final A()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 815396
    iget-object v0, p0, LX/4rG;->a:Ljava/math/BigInteger;

    return-object v0
.end method

.method public final B()Ljava/lang/String;
    .locals 1

    .prologue
    .line 815394
    iget-object v0, p0, LX/4rG;->a:Ljava/math/BigInteger;

    invoke-virtual {v0}, Ljava/math/BigInteger;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a()LX/15z;
    .locals 1

    .prologue
    .line 815395
    sget-object v0, LX/15z;->VALUE_NUMBER_INT:LX/15z;

    return-object v0
.end method

.method public final a(Z)Z
    .locals 2

    .prologue
    .line 815386
    sget-object v0, Ljava/math/BigInteger;->ZERO:Ljava/math/BigInteger;

    iget-object v1, p0, LX/4rG;->a:Ljava/math/BigInteger;

    invoke-virtual {v0, v1}, Ljava/math/BigInteger;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()LX/16L;
    .locals 1

    .prologue
    .line 815387
    sget-object v0, LX/16L;->BIG_INTEGER:LX/16L;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 815381
    if-ne p1, p0, :cond_1

    const/4 v0, 0x1

    .line 815382
    :cond_0
    :goto_0
    return v0

    .line 815383
    :cond_1
    if-eqz p1, :cond_0

    .line 815384
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-ne v1, v2, :cond_0

    .line 815385
    check-cast p1, LX/4rG;

    iget-object v0, p1, LX/4rG;->a:Ljava/math/BigInteger;

    iget-object v1, p0, LX/4rG;->a:Ljava/math/BigInteger;

    invoke-virtual {v0, v1}, Ljava/math/BigInteger;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 815380
    iget-object v0, p0, LX/4rG;->a:Ljava/math/BigInteger;

    invoke-virtual {v0}, Ljava/math/BigInteger;->hashCode()I

    move-result v0

    return v0
.end method

.method public final serialize(LX/0nX;LX/0my;)V
    .locals 1

    .prologue
    .line 815378
    iget-object v0, p0, LX/4rG;->a:Ljava/math/BigInteger;

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/math/BigInteger;)V

    .line 815379
    return-void
.end method

.method public final v()Ljava/lang/Number;
    .locals 1

    .prologue
    .line 815377
    iget-object v0, p0, LX/4rG;->a:Ljava/math/BigInteger;

    return-object v0
.end method

.method public final w()I
    .locals 1

    .prologue
    .line 815376
    iget-object v0, p0, LX/4rG;->a:Ljava/math/BigInteger;

    invoke-virtual {v0}, Ljava/math/BigInteger;->intValue()I

    move-result v0

    return v0
.end method

.method public final x()J
    .locals 2

    .prologue
    .line 815375
    iget-object v0, p0, LX/4rG;->a:Ljava/math/BigInteger;

    invoke-virtual {v0}, Ljava/math/BigInteger;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public final y()D
    .locals 2

    .prologue
    .line 815374
    iget-object v0, p0, LX/4rG;->a:Ljava/math/BigInteger;

    invoke-virtual {v0}, Ljava/math/BigInteger;->doubleValue()D

    move-result-wide v0

    return-wide v0
.end method

.method public final z()Ljava/math/BigDecimal;
    .locals 2

    .prologue
    .line 815373
    new-instance v0, Ljava/math/BigDecimal;

    iget-object v1, p0, LX/4rG;->a:Ljava/math/BigInteger;

    invoke-direct {v0, v1}, Ljava/math/BigDecimal;-><init>(Ljava/math/BigInteger;)V

    return-object v0
.end method
