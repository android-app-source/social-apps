.class public LX/471;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 671850
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;III)I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 671859
    invoke-virtual {p0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [I

    aput p1, v1, v2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 671860
    :try_start_0
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 671861
    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    .line 671862
    iget v2, v0, Landroid/util/TypedValue;->type:I

    sparse-switch v2, :sswitch_data_0

    .line 671863
    const/4 v0, 0x0

    invoke-virtual {v1, v0, p3}, Landroid/content/res/TypedArray;->getLayoutDimension(II)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result p3

    .line 671864
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    :goto_0
    return p3

    :sswitch_0
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    goto :goto_0

    .line 671865
    :sswitch_1
    int-to-float v2, p2

    const/high16 v3, 0x3f800000    # 1.0f

    :try_start_1
    invoke-virtual {v0, v2, v3}, Landroid/util/TypedValue;->getFraction(FF)F
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    float-to-int p3, v0

    .line 671866
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    throw v0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x6 -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(Landroid/content/res/Resources;FI)I
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v0, -0x1

    .line 671851
    float-to-int v1, p1

    .line 671852
    if-gez v1, :cond_1

    .line 671853
    if-ne v1, v0, :cond_0

    .line 671854
    :goto_0
    return v0

    .line 671855
    :cond_0
    const/4 v0, -0x2

    goto :goto_0

    .line 671856
    :cond_1
    if-le v1, v2, :cond_2

    .line 671857
    invoke-virtual {p0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    invoke-static {v2, p1, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    goto :goto_0

    .line 671858
    :cond_2
    int-to-float v0, p2

    mul-float/2addr v0, p1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    goto :goto_0
.end method
