.class public final LX/3mk;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/3mL;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/3mX;

.field public b:Z

.field public c:LX/3x6;

.field public d:LX/1OX;

.field public e:Z

.field public f:LX/8yz;

.field public final synthetic g:LX/3mL;


# direct methods
.method public constructor <init>(LX/3mL;)V
    .locals 1

    .prologue
    .line 637216
    iput-object p1, p0, LX/3mk;->g:LX/3mL;

    .line 637217
    move-object v0, p1

    .line 637218
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 637219
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/3mk;->e:Z

    .line 637220
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 637191
    const-string v0, "FeedHScroll"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 637192
    if-ne p0, p1, :cond_1

    .line 637193
    :cond_0
    :goto_0
    return v0

    .line 637194
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 637195
    goto :goto_0

    .line 637196
    :cond_3
    check-cast p1, LX/3mk;

    .line 637197
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 637198
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 637199
    if-eq v2, v3, :cond_0

    .line 637200
    iget-object v2, p0, LX/3mk;->a:LX/3mX;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/3mk;->a:LX/3mX;

    iget-object v3, p1, LX/3mk;->a:LX/3mX;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 637201
    goto :goto_0

    .line 637202
    :cond_5
    iget-object v2, p1, LX/3mk;->a:LX/3mX;

    if-nez v2, :cond_4

    .line 637203
    :cond_6
    iget-boolean v2, p0, LX/3mk;->b:Z

    iget-boolean v3, p1, LX/3mk;->b:Z

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 637204
    goto :goto_0

    .line 637205
    :cond_7
    iget-object v2, p0, LX/3mk;->c:LX/3x6;

    if-eqz v2, :cond_9

    iget-object v2, p0, LX/3mk;->c:LX/3x6;

    iget-object v3, p1, LX/3mk;->c:LX/3x6;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    :cond_8
    move v0, v1

    .line 637206
    goto :goto_0

    .line 637207
    :cond_9
    iget-object v2, p1, LX/3mk;->c:LX/3x6;

    if-nez v2, :cond_8

    .line 637208
    :cond_a
    iget-object v2, p0, LX/3mk;->d:LX/1OX;

    if-eqz v2, :cond_c

    iget-object v2, p0, LX/3mk;->d:LX/1OX;

    iget-object v3, p1, LX/3mk;->d:LX/1OX;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    :cond_b
    move v0, v1

    .line 637209
    goto :goto_0

    .line 637210
    :cond_c
    iget-object v2, p1, LX/3mk;->d:LX/1OX;

    if-nez v2, :cond_b

    .line 637211
    :cond_d
    iget-boolean v2, p0, LX/3mk;->e:Z

    iget-boolean v3, p1, LX/3mk;->e:Z

    if-eq v2, v3, :cond_e

    move v0, v1

    .line 637212
    goto :goto_0

    .line 637213
    :cond_e
    iget-object v2, p0, LX/3mk;->f:LX/8yz;

    if-eqz v2, :cond_f

    iget-object v2, p0, LX/3mk;->f:LX/8yz;

    iget-object v3, p1, LX/3mk;->f:LX/8yz;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 637214
    goto :goto_0

    .line 637215
    :cond_f
    iget-object v2, p1, LX/3mk;->f:LX/8yz;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
