.class public final LX/4rP;
.super LX/2Ax;
.source ""


# instance fields
.field public final t:LX/2Ax;

.field public final u:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/2Ax;Ljava/lang/Class;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2Ax;",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 815586
    invoke-direct {p0, p1}, LX/2Ax;-><init>(LX/2Ax;)V

    .line 815587
    iput-object p1, p0, LX/4rP;->t:LX/2Ax;

    .line 815588
    iput-object p2, p0, LX/4rP;->u:Ljava/lang/Class;

    .line 815589
    return-void
.end method

.method private c(LX/4ro;)LX/4rP;
    .locals 3

    .prologue
    .line 815604
    new-instance v0, LX/4rP;

    iget-object v1, p0, LX/4rP;->t:LX/2Ax;

    invoke-virtual {v1, p1}, LX/2Ax;->a(LX/4ro;)LX/2Ax;

    move-result-object v1

    iget-object v2, p0, LX/4rP;->u:Ljava/lang/Class;

    invoke-direct {v0, v1, v2}, LX/4rP;-><init>(LX/2Ax;Ljava/lang/Class;)V

    return-object v0
.end method


# virtual methods
.method public final synthetic a(LX/4ro;)LX/2Ax;
    .locals 1

    .prologue
    .line 815603
    invoke-direct {p0, p1}, LX/4rP;->c(LX/4ro;)LX/4rP;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/fasterxml/jackson/databind/JsonSerializer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 815601
    iget-object v0, p0, LX/4rP;->t:LX/2Ax;

    invoke-virtual {v0, p1}, LX/2Ax;->a(Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 815602
    return-void
.end method

.method public final a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 815597
    iget-object v0, p3, LX/0my;->_serializationView:Ljava/lang/Class;

    move-object v0, v0

    .line 815598
    if-eqz v0, :cond_0

    iget-object v1, p0, LX/4rP;->u:Ljava/lang/Class;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 815599
    :cond_0
    iget-object v0, p0, LX/4rP;->t:LX/2Ax;

    invoke-virtual {v0, p1, p2, p3}, LX/2Ax;->a(Ljava/lang/Object;LX/0nX;LX/0my;)V

    .line 815600
    :cond_1
    return-void
.end method

.method public final b(Lcom/fasterxml/jackson/databind/JsonSerializer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 815595
    iget-object v0, p0, LX/4rP;->t:LX/2Ax;

    invoke-virtual {v0, p1}, LX/2Ax;->b(Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 815596
    return-void
.end method

.method public final b(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 815590
    iget-object v0, p3, LX/0my;->_serializationView:Ljava/lang/Class;

    move-object v0, v0

    .line 815591
    if-eqz v0, :cond_0

    iget-object v1, p0, LX/4rP;->u:Ljava/lang/Class;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 815592
    :cond_0
    iget-object v0, p0, LX/4rP;->t:LX/2Ax;

    invoke-virtual {v0, p1, p2, p3}, LX/2Ax;->b(Ljava/lang/Object;LX/0nX;LX/0my;)V

    .line 815593
    :goto_0
    return-void

    .line 815594
    :cond_1
    iget-object v0, p0, LX/4rP;->t:LX/2Ax;

    invoke-virtual {v0, p2, p3}, LX/2Ax;->a(LX/0nX;LX/0my;)V

    goto :goto_0
.end method
