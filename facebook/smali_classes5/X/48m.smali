.class public LX/48m;
.super Ljava/io/FilterInputStream;
.source ""


# instance fields
.field public final a:[B

.field public final b:I

.field public c:I

.field public d:Z


# direct methods
.method public constructor <init>(Ljava/io/InputStream;I)V
    .locals 1

    .prologue
    .line 673197
    invoke-direct {p0, p1}, Ljava/io/FilterInputStream;-><init>(Ljava/io/InputStream;)V

    .line 673198
    new-array v0, p2, [B

    iput-object v0, p0, LX/48m;->a:[B

    .line 673199
    iput p2, p0, LX/48m;->b:I

    .line 673200
    return-void
.end method

.method public static a(LX/48m;[BIII)I
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 673188
    iget v0, p0, LX/48m;->b:I

    sub-int/2addr v0, p3

    .line 673189
    sub-int v1, p2, v0

    invoke-static {v4, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    add-int/2addr v1, p4

    .line 673190
    invoke-static {v0, p2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 673191
    if-lez v0, :cond_1

    .line 673192
    if-lez p3, :cond_0

    .line 673193
    iget-object v2, p0, LX/48m;->a:[B

    iget-object v3, p0, LX/48m;->a:[B

    invoke-static {v2, v4, v3, v0, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 673194
    :cond_0
    iget-object v2, p0, LX/48m;->a:[B

    invoke-static {p1, v1, v2, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 673195
    :cond_1
    add-int/2addr v0, p3

    iput v0, p0, LX/48m;->c:I

    .line 673196
    sub-int v0, v1, p4

    return v0
.end method


# virtual methods
.method public final a()[B
    .locals 2

    .prologue
    .line 673185
    iget v0, p0, LX/48m;->c:I

    iget v1, p0, LX/48m;->b:I

    if-eq v0, v1, :cond_0

    .line 673186
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Not enough tail data"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 673187
    :cond_0
    iget-object v0, p0, LX/48m;->a:[B

    return-object v0
.end method

.method public final markSupported()Z
    .locals 1

    .prologue
    .line 673148
    const/4 v0, 0x0

    return v0
.end method

.method public final read()I
    .locals 5

    .prologue
    const/4 v0, -0x1

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 673179
    new-array v2, v4, [B

    .line 673180
    invoke-virtual {p0, v2, v3, v4}, LX/48m;->read([BII)I

    move-result v1

    .line 673181
    :goto_0
    if-nez v1, :cond_0

    .line 673182
    invoke-virtual {p0, v2, v3, v4}, LX/48m;->read([BII)I

    move-result v1

    goto :goto_0

    .line 673183
    :cond_0
    if-ne v1, v0, :cond_1

    .line 673184
    :goto_1
    return v0

    :cond_1
    aget-byte v0, v2, v3

    and-int/lit16 v0, v0, 0xff

    goto :goto_1
.end method

.method public final read([BII)I
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 673149
    iget-boolean v1, p0, LX/48m;->d:Z

    if-eqz v1, :cond_1

    .line 673150
    const/4 v0, -0x1

    .line 673151
    :cond_0
    return v0

    .line 673152
    :cond_1
    if-eqz p3, :cond_0

    .line 673153
    :goto_0
    if-nez v0, :cond_0

    .line 673154
    const/4 v6, 0x1

    const/4 v2, -0x1

    const/4 v0, 0x0

    .line 673155
    iget v1, p0, LX/48m;->c:I

    if-lt p3, v1, :cond_4

    .line 673156
    iget v1, p0, LX/48m;->c:I

    sub-int v1, p3, v1

    .line 673157
    iget-object v3, p0, Ljava/io/FilterInputStream;->in:Ljava/io/InputStream;

    iget v4, p0, LX/48m;->c:I

    add-int/2addr v4, p2

    invoke-virtual {v3, p1, v4, v1}, Ljava/io/InputStream;->read([BII)I

    move-result v1

    .line 673158
    if-ne v1, v2, :cond_2

    .line 673159
    iput-boolean v6, p0, LX/48m;->d:Z

    move v0, v2

    .line 673160
    :goto_1
    move v0, v0

    .line 673161
    goto :goto_0

    .line 673162
    :cond_2
    iget v3, p0, LX/48m;->c:I

    if-lez v3, :cond_3

    .line 673163
    iget-object v3, p0, LX/48m;->a:[B

    iget v4, p0, LX/48m;->c:I

    invoke-static {v3, v0, p1, p2, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 673164
    :cond_3
    iget v3, p0, LX/48m;->c:I

    add-int/2addr v3, v1

    .line 673165
    iget-object v1, p0, Ljava/io/FilterInputStream;->in:Ljava/io/InputStream;

    iget-object v4, p0, LX/48m;->a:[B

    iget v5, p0, LX/48m;->b:I

    invoke-virtual {v1, v4, v0, v5}, Ljava/io/InputStream;->read([BII)I

    move-result v1

    .line 673166
    if-ne v1, v2, :cond_6

    .line 673167
    iput-boolean v6, p0, LX/48m;->d:Z

    .line 673168
    :goto_2
    invoke-static {p0, p1, v3, v0, p2}, LX/48m;->a(LX/48m;[BIII)I

    move-result v0

    goto :goto_1

    .line 673169
    :cond_4
    iget v1, p0, LX/48m;->c:I

    sub-int/2addr v1, p3

    .line 673170
    iget-object v3, p0, LX/48m;->a:[B

    invoke-static {v3, v0, p1, p2, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 673171
    iget-object v3, p0, LX/48m;->a:[B

    iget-object v4, p0, LX/48m;->a:[B

    invoke-static {v3, p3, v4, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 673172
    iget-object v3, p0, Ljava/io/FilterInputStream;->in:Ljava/io/InputStream;

    iget-object v4, p0, LX/48m;->a:[B

    iget v5, p0, LX/48m;->b:I

    sub-int/2addr v5, v1

    invoke-virtual {v3, v4, v1, v5}, Ljava/io/InputStream;->read([BII)I

    move-result v3

    .line 673173
    if-ne v3, v2, :cond_5

    .line 673174
    iget-object v3, p0, LX/48m;->a:[B

    iget-object v4, p0, LX/48m;->a:[B

    invoke-static {v3, v0, v4, p3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 673175
    iget-object v1, p0, LX/48m;->a:[B

    invoke-static {p1, p2, v1, v0, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 673176
    iput-boolean v6, p0, LX/48m;->d:Z

    move v0, v2

    .line 673177
    goto :goto_1

    .line 673178
    :cond_5
    add-int v0, v3, v1

    invoke-static {p0, p1, p3, v0, p2}, LX/48m;->a(LX/48m;[BIII)I

    move-result v0

    goto :goto_1

    :cond_6
    move v0, v1

    goto :goto_2
.end method
