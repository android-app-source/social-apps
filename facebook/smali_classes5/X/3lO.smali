.class public LX/3lO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/16E;
.implements LX/0i1;


# instance fields
.field public a:Landroid/view/View;

.field private b:LX/0wM;


# direct methods
.method public constructor <init>(LX/0wM;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 633808
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 633809
    iput-object p1, p0, LX/3lO;->b:LX/0wM;

    .line 633810
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 633807
    const-wide/32 v0, 0x5265c00

    return-wide v0
.end method

.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/10S;
    .locals 1

    .prologue
    .line 633793
    sget-object v0, LX/10S;->ELIGIBLE:LX/10S;

    return-object v0
.end method

.method public final a(J)V
    .locals 0

    .prologue
    .line 633806
    return-void
.end method

.method public final a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 633797
    new-instance v0, LX/0hs;

    const/4 v1, 0x2

    invoke-direct {v0, p1, v1}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 633798
    const v1, 0x7f081b8e

    invoke-virtual {v0, v1}, LX/0hs;->a(I)V

    .line 633799
    const v1, 0x7f081b8f

    invoke-virtual {v0, v1}, LX/0hs;->b(I)V

    .line 633800
    iget-object v1, p0, LX/3lO;->b:LX/0wM;

    const v2, 0x7f020765

    invoke-virtual {v1, v2, v3}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0hs;->b(Landroid/graphics/drawable/Drawable;)V

    .line 633801
    sget-object v1, LX/3AV;->BELOW:LX/3AV;

    invoke-virtual {v0, v1}, LX/0ht;->a(LX/3AV;)V

    .line 633802
    iput v3, v0, LX/0hs;->t:I

    .line 633803
    const v1, 0x3e99999a    # 0.3f

    invoke-virtual {v0, v1}, LX/0ht;->b(F)V

    .line 633804
    iget-object v1, p0, LX/3lO;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, LX/0ht;->f(Landroid/view/View;)V

    .line 633805
    return-void
.end method

.method public final a(Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 633796
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 633795
    const-string v0, "4474"

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 633794
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->GROUP_MALL_VISIT_WITH_INTEREST_COMMUNITY_TYPE_VIEW:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
