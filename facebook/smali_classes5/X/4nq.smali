.class public final LX/4nq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/ViewSwitcher$ViewFactory;


# instance fields
.field public final synthetic a:LX/4nr;


# direct methods
.method public constructor <init>(LX/4nr;)V
    .locals 0

    .prologue
    .line 807918
    iput-object p1, p0, LX/4nq;->a:LX/4nr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final makeView()Landroid/view/View;
    .locals 3

    .prologue
    .line 807919
    new-instance v0, Landroid/widget/TextView;

    iget-object v1, p0, LX/4nq;->a:LX/4nr;

    invoke-virtual {v1}, LX/4nr;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 807920
    iget-object v1, p0, LX/4nq;->a:LX/4nr;

    iget-boolean v1, v1, LX/4nr;->c:Z

    if-eqz v1, :cond_0

    .line 807921
    iget-object v1, p0, LX/4nq;->a:LX/4nr;

    iget v1, v1, LX/4nr;->d:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 807922
    :cond_0
    iget-object v1, p0, LX/4nq;->a:LX/4nr;

    iget-boolean v1, v1, LX/4nr;->e:Z

    if-eqz v1, :cond_1

    .line 807923
    const/4 v1, 0x0

    iget-object v2, p0, LX/4nq;->a:LX/4nr;

    iget v2, v2, LX/4nr;->f:F

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 807924
    :cond_1
    iget-object v1, p0, LX/4nq;->a:LX/4nr;

    iget-boolean v1, v1, LX/4nr;->a:Z

    if-eqz v1, :cond_2

    .line 807925
    sget-object v1, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    iget-object v2, p0, LX/4nq;->a:LX/4nr;

    iget v2, v2, LX/4nr;->b:I

    invoke-static {v1, v2}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v1

    .line 807926
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 807927
    :cond_2
    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 807928
    return-object v0
.end method
