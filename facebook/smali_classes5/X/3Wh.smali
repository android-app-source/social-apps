.class public LX/3Wh;
.super Lcom/facebook/events/widget/eventcard/EventCardHeaderView;
.source ""

# interfaces
.implements LX/35q;
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final b:LX/1Cz;


# instance fields
.field public a:Lcom/facebook/common/callercontexttagger/AnalyticsTagger;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Landroid/content/Context;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 590894
    new-instance v0, LX/3Wi;

    invoke-direct {v0}, LX/3Wi;-><init>()V

    sput-object v0, LX/3Wh;->b:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 590888
    invoke-direct {p0, p1}, Lcom/facebook/events/widget/eventcard/EventCardHeaderView;-><init>(Landroid/content/Context;)V

    .line 590889
    const-class v0, LX/3Wh;

    invoke-static {v0, p0}, LX/3Wh;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 590890
    iput-object p1, p0, LX/3Wh;->c:Landroid/content/Context;

    .line 590891
    const-string v1, "newsfeed_angora_attachment_view"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {p0, v1, v2}, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;->a(Landroid/view/View;Ljava/lang/String;Ljava/lang/Class;)V

    .line 590892
    sget-object v0, LX/1vY;->ATTACHMENT:LX/1vY;

    invoke-static {p0, v0}, LX/1vZ;->a(Landroid/view/View;LX/1vY;)V

    .line 590893
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/3Wh;

    invoke-static {p0}, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;->a(LX/0QB;)Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    move-result-object p0

    check-cast p0, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    iput-object p0, p1, LX/3Wh;->a:Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    return-void
.end method


# virtual methods
.method public setLargeImageAspectRatio(F)V
    .locals 0

    .prologue
    .line 590884
    invoke-super {p0, p1}, Lcom/facebook/events/widget/eventcard/EventCardHeaderView;->setCoverPhotoAspectRatio(F)V

    .line 590885
    return-void
.end method

.method public setLargeImageController(LX/1aZ;)V
    .locals 0
    .param p1    # LX/1aZ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 590886
    invoke-super {p0, p1}, Lcom/facebook/events/widget/eventcard/EventCardHeaderView;->setCoverPhotoController(LX/1aZ;)V

    .line 590887
    return-void
.end method
