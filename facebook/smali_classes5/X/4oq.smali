.class public LX/4oq;
.super Landroid/preference/EditTextPreference;
.source ""


# instance fields
.field public a:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/2qx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final c:LX/2qy;

.field public d:LX/0Tn;

.field private e:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 810403
    invoke-direct {p0, p1}, Landroid/preference/EditTextPreference;-><init>(Landroid/content/Context;)V

    .line 810404
    const-class v0, LX/4oq;

    invoke-static {v0, p0}, LX/4oq;->a(Ljava/lang/Class;Landroid/preference/Preference;)V

    .line 810405
    iget-object v0, p0, LX/4oq;->b:LX/2qx;

    invoke-virtual {v0, p0}, LX/2qx;->a(Landroid/preference/Preference;)LX/2qy;

    move-result-object v0

    iput-object v0, p0, LX/4oq;->c:LX/2qy;

    .line 810406
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 810407
    const v1, 0x7f0b02a0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/4oq;->e:I

    .line 810408
    const v0, 0x7f030fff

    invoke-virtual {p0, v0}, LX/4oq;->setDialogLayoutResource(I)V

    .line 810409
    return-void
.end method

.method private a()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 810449
    iget-object v0, p0, LX/4oq;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v1, p0, LX/4oq;->d:LX/0Tn;

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "[,]"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/Class;Landroid/preference/Preference;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/preference/Preference;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p1, LX/4oq;

    invoke-static {v2}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const-class p0, LX/2qx;

    invoke-interface {v2, p0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/2qx;

    iput-object v1, p1, LX/4oq;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object v2, p1, LX/4oq;->b:LX/2qx;

    return-void
.end method


# virtual methods
.method public final a(LX/0Tn;)V
    .locals 1

    .prologue
    .line 810446
    iget-object v0, p0, LX/4oq;->c:LX/2qy;

    invoke-virtual {v0, p1}, LX/2qy;->a(LX/0Tn;)V

    .line 810447
    const-string v0, "history"

    invoke-virtual {p1, v0}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iput-object v0, p0, LX/4oq;->d:LX/0Tn;

    .line 810448
    return-void
.end method

.method public a(Landroid/view/ViewGroup;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 810438
    new-instance v0, Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {p0}, LX/4oq;->getEditText()Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    const v3, 0x7f0101f0

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/resources/ui/FbButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 810439
    invoke-virtual {v0, p2}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 810440
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setGravity(I)V

    .line 810441
    new-instance v1, LX/4op;

    invoke-direct {v1, p0, p2}, LX/4op;-><init>(LX/4oq;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 810442
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 810443
    iget v2, p0, LX/4oq;->e:I

    iget v3, p0, LX/4oq;->e:I

    invoke-virtual {v1, v4, v2, v4, v3}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 810444
    invoke-virtual {p1, v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 810445
    return-void
.end method

.method public getPersistedString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 810450
    iget-object v0, p0, LX/4oq;->c:LX/2qy;

    invoke-virtual {v0, p1}, LX/2qy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getSharedPreferences()Landroid/content/SharedPreferences;
    .locals 1

    .prologue
    .line 810435
    iget-object v0, p0, LX/4oq;->c:LX/2qy;

    .line 810436
    iget-object p0, v0, LX/2qy;->b:Landroid/content/SharedPreferences;

    move-object v0, p0

    .line 810437
    return-object v0
.end method

.method public final onAddEditTextToDialogView(Landroid/view/View;Landroid/widget/EditText;)V
    .locals 3

    .prologue
    .line 810430
    const v0, 0x7f0d2679

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 810431
    if-eqz v0, :cond_0

    .line 810432
    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-virtual {v0, p2, v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;II)V

    .line 810433
    :goto_0
    return-void

    .line 810434
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/preference/EditTextPreference;->onAddEditTextToDialogView(Landroid/view/View;Landroid/widget/EditText;)V

    goto :goto_0
.end method

.method public final onBindDialogView(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 810422
    invoke-super {p0, p1}, Landroid/preference/EditTextPreference;->onBindDialogView(Landroid/view/View;)V

    .line 810423
    const v0, 0x7f0d0578

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 810424
    const v1, 0x7f0805e7

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 810425
    const v0, 0x7f0d267a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 810426
    invoke-direct {p0}, LX/4oq;->a()Ljava/util/List;

    move-result-object v1

    .line 810427
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 810428
    invoke-virtual {p0, v0, v1}, LX/4oq;->a(Landroid/view/ViewGroup;Ljava/lang/String;)V

    goto :goto_0

    .line 810429
    :cond_0
    return-void
.end method

.method public persistString(Ljava/lang/String;)Z
    .locals 6

    .prologue
    .line 810410
    invoke-direct {p0}, LX/4oq;->a()Ljava/util/List;

    move-result-object v0

    .line 810411
    const/4 v1, 0x0

    invoke-interface {v0, v1, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 810412
    const/4 v3, 0x5

    .line 810413
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 810414
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 810415
    invoke-interface {v2, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 810416
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 810417
    :cond_1
    move-object v1, v2

    .line 810418
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-le v2, v3, :cond_2

    .line 810419
    const/4 v2, 0x0

    invoke-interface {v1, v2, v3}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v1

    .line 810420
    :cond_2
    iget-object v2, p0, LX/4oq;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    iget-object v3, p0, LX/4oq;->d:LX/0Tn;

    const-string v4, ","

    invoke-static {v4, v1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v3, v1}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 810421
    iget-object v0, p0, LX/4oq;->c:LX/2qy;

    invoke-virtual {v0, p1}, LX/2qy;->b(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
