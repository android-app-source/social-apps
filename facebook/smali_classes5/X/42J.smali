.class public final LX/42J;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 667008
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_7

    .line 667009
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 667010
    :goto_0
    return v1

    .line 667011
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 667012
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_6

    .line 667013
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 667014
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 667015
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_1

    if-eqz v6, :cond_1

    .line 667016
    const-string v7, "id"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 667017
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 667018
    :cond_2
    const-string v7, "name"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 667019
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 667020
    :cond_3
    const-string v7, "squareProfilePicBig"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 667021
    invoke-static {p0, p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 667022
    :cond_4
    const-string v7, "squareProfilePicHuge"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 667023
    invoke-static {p0, p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 667024
    :cond_5
    const-string v7, "squareProfilePicSmall"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 667025
    invoke-static {p0, p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 667026
    :cond_6
    const/4 v6, 0x5

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 667027
    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 667028
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 667029
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 667030
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 667031
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 667032
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_7
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 667033
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 667034
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 667035
    if-eqz v0, :cond_0

    .line 667036
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 667037
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 667038
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 667039
    if-eqz v0, :cond_1

    .line 667040
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 667041
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 667042
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 667043
    if-eqz v0, :cond_2

    .line 667044
    const-string v1, "squareProfilePicBig"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 667045
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 667046
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 667047
    if-eqz v0, :cond_3

    .line 667048
    const-string v1, "squareProfilePicHuge"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 667049
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 667050
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 667051
    if-eqz v0, :cond_4

    .line 667052
    const-string v1, "squareProfilePicSmall"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 667053
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 667054
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 667055
    return-void
.end method
