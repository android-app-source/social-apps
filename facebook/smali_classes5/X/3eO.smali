.class public LX/3eO;
.super LX/0ro;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0ro",
        "<",
        "Lcom/facebook/stickers/service/StickerSearchParams;",
        "Lcom/facebook/stickers/service/StickerSearchResult;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile d:LX/3eO;


# instance fields
.field private final c:LX/3eL;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 619768
    const-class v0, LX/3eO;

    sput-object v0, LX/3eO;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0sO;LX/3eL;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 619728
    invoke-direct {p0, p1}, LX/0ro;-><init>(LX/0sO;)V

    .line 619729
    iput-object p2, p0, LX/3eO;->c:LX/3eL;

    .line 619730
    return-void
.end method

.method public static a(LX/0QB;)LX/3eO;
    .locals 5

    .prologue
    .line 619731
    sget-object v0, LX/3eO;->d:LX/3eO;

    if-nez v0, :cond_1

    .line 619732
    const-class v1, LX/3eO;

    monitor-enter v1

    .line 619733
    :try_start_0
    sget-object v0, LX/3eO;->d:LX/3eO;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 619734
    if-eqz v2, :cond_0

    .line 619735
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 619736
    new-instance p0, LX/3eO;

    invoke-static {v0}, LX/0sO;->a(LX/0QB;)LX/0sO;

    move-result-object v3

    check-cast v3, LX/0sO;

    invoke-static {v0}, LX/3eL;->a(LX/0QB;)LX/3eL;

    move-result-object v4

    check-cast v4, LX/3eL;

    invoke-direct {p0, v3, v4}, LX/3eO;-><init>(LX/0sO;LX/3eL;)V

    .line 619737
    move-object v0, p0

    .line 619738
    sput-object v0, LX/3eO;->d:LX/3eO;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 619739
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 619740
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 619741
    :cond_1
    sget-object v0, LX/3eO;->d:LX/3eO;

    return-object v0

    .line 619742
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 619743
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/1pN;LX/15w;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 619744
    const/4 v2, 0x0

    .line 619745
    const-class v0, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$SearchTaggedStickersQueryModel;

    invoke-virtual {p3, v0}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$SearchTaggedStickersQueryModel;

    .line 619746
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 619747
    invoke-virtual {v0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$SearchTaggedStickersQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const-class v4, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerFieldsModel;

    invoke-virtual {v1, v0, v2, v4}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    move-object v1, v0

    :goto_0
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v4

    :goto_1
    if-ge v2, v4, :cond_1

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerFieldsModel;

    .line 619748
    :try_start_0
    invoke-static {v0}, LX/3eL;->a(Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerFieldsModel;)Lcom/facebook/stickers/model/Sticker;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 619749
    :goto_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 619750
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 619751
    move-object v1, v0

    goto :goto_0

    .line 619752
    :catch_0
    move-exception v0

    .line 619753
    sget-object v5, LX/3eO;->b:Ljava/lang/Class;

    const-string v6, "Error parsing sticker node"

    invoke-static {v5, v6, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 619754
    :cond_1
    new-instance v0, Lcom/facebook/stickers/service/StickerSearchResult;

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/stickers/service/StickerSearchResult;-><init>(Ljava/util/List;)V

    return-object v0
.end method

.method public final b(Ljava/lang/Object;LX/1pN;)I
    .locals 1

    .prologue
    .line 619755
    const/4 v0, 0x1

    return v0
.end method

.method public final f(Ljava/lang/Object;)LX/0gW;
    .locals 3

    .prologue
    .line 619756
    check-cast p1, Lcom/facebook/stickers/service/StickerSearchParams;

    .line 619757
    iget-object v0, p0, LX/3eO;->c:LX/3eL;

    .line 619758
    new-instance v1, LX/8jr;

    invoke-direct {v1}, LX/8jr;-><init>()V

    move-object v1, v1

    .line 619759
    invoke-static {v0, v1}, LX/3eL;->a(LX/3eL;LX/0gW;)V

    .line 619760
    move-object v0, v1

    .line 619761
    const-string v1, "search_query"

    .line 619762
    iget-object v2, p1, Lcom/facebook/stickers/service/StickerSearchParams;->a:Ljava/lang/String;

    move-object v2, v2

    .line 619763
    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 619764
    const-string v1, "interface"

    .line 619765
    iget-object v2, p1, Lcom/facebook/stickers/service/StickerSearchParams;->b:Ljava/lang/String;

    move-object v2, v2

    .line 619766
    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 619767
    return-object v0
.end method
