.class public LX/4uH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4u1;


# static fields
.field private static a:LX/4uH;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Z


# direct methods
.method private constructor <init>(Landroid/content/Context;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, LX/4uH;->b:Landroid/content/Context;

    iput-boolean p2, p0, LX/4uH;->c:Z

    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;Z)LX/4uH;
    .locals 3

    const-class v1, LX/4uH;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sget-object v2, LX/4uH;->a:LX/4uH;

    if-eqz v2, :cond_0

    sget-object v2, LX/4uH;->a:LX/4uH;

    iget-object v2, v2, LX/4uH;->b:Landroid/content/Context;

    if-ne v2, v0, :cond_0

    sget-object v2, LX/4uH;->a:LX/4uH;

    iget-boolean v2, v2, LX/4uH;->c:Z

    if-eq v2, p1, :cond_1

    :cond_0
    new-instance v2, LX/4uH;

    invoke-direct {v2, v0, p1}, LX/4uH;-><init>(Landroid/content/Context;Z)V

    sput-object v2, LX/4uH;->a:LX/4uH;

    :cond_1
    sget-object v0, LX/4uH;->a:LX/4uH;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/app/Activity;)Landroid/content/ComponentName;
    .locals 4

    invoke-virtual {p1}, Landroid/app/Activity;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const-string v2, "com.google.android.instantapps.supervisor"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    move v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/4uH;->b:Landroid/content/Context;

    invoke-static {v0}, LX/4uF;->a(Landroid/content/Context;)LX/4uF;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-virtual {v1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    const-string p0, "shadowActivity"

    invoke-virtual {v3, p0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string p0, "getCallingActivity"

    invoke-static {v0, p0, v3}, LX/4uF;->a(LX/4uF;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v3

    const-string p0, "result"

    invoke-virtual {v3, p0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Landroid/content/ComponentName;

    move-object v0, v3
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v2, "InstantAppsPMW"

    const-string v3, "Error getting calling activity"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    .locals 3

    iget-boolean v0, p0, LX/4uH;->c:Z

    if-eqz v0, :cond_1

    :try_start_0
    iget-object v0, p0, LX/4uH;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :cond_0
    return-object v0

    :catch_0
    :cond_1
    iget-object v0, p0, LX/4uH;->b:Landroid/content/Context;

    invoke-static {v0}, LX/4uF;->a(Landroid/content/Context;)LX/4uF;

    move-result-object v0

    if-eqz v0, :cond_2

    :try_start_1
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "packageName"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "flags"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "getWHPackageInfo"

    invoke-static {v0, v2, v1}, LX/4uF;->a(LX/4uF;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "result"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/content/pm/PackageInfo;

    move-object v0, v1
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    if-nez v0, :cond_0

    :cond_2
    :goto_0
    new-instance v0, Landroid/content/pm/PackageManager$NameNotFoundException;

    invoke-direct {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;-><init>()V

    throw v0

    :catch_1
    move-exception v0

    const-string v1, "InstantAppsPMW"

    const-string v2, "Error getting package info"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
