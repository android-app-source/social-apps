.class public LX/3YM;
.super Lcom/facebook/feedplugins/video/RichVideoAttachmentView;
.source ""

# interfaces
.implements LX/3VQ;
.implements LX/3VR;


# static fields
.field public static final g:LX/1Cz;


# instance fields
.field public h:Lcom/facebook/attachments/angora/InstantArticleIconView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 595799
    new-instance v0, LX/3YN;

    invoke-direct {v0}, LX/3YN;-><init>()V

    sput-object v0, LX/3YM;->g:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 595775
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/3YM;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 595776
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 595797
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/3YM;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 595798
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 595780
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 595781
    const/4 p3, 0x0

    .line 595782
    new-instance v0, Lcom/facebook/attachments/angora/InstantArticleIconView;

    invoke-virtual {p0}, LX/3YM;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/attachments/angora/InstantArticleIconView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/3YM;->h:Lcom/facebook/attachments/angora/InstantArticleIconView;

    .line 595783
    iget-object v0, p0, LX/3YM;->h:Lcom/facebook/attachments/angora/InstantArticleIconView;

    invoke-virtual {v0, p3}, Lcom/facebook/attachments/angora/InstantArticleIconView;->setBackgroundColor(I)V

    .line 595784
    iget-object v0, p0, LX/3YM;->h:Lcom/facebook/attachments/angora/InstantArticleIconView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Lcom/facebook/attachments/angora/InstantArticleIconView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 595785
    iget-object v0, p0, LX/3YM;->h:Lcom/facebook/attachments/angora/InstantArticleIconView;

    const v1, 0x7f0d0042

    invoke-virtual {v0, v1}, Lcom/facebook/attachments/angora/InstantArticleIconView;->setId(I)V

    .line 595786
    invoke-virtual {p0}, LX/3YM;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b062a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 595787
    invoke-virtual {p0}, LX/3YM;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const p1, 0x7f0b0108

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 595788
    invoke-virtual {p0}, LX/3YM;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p2, 0x7f0b0107

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    .line 595789
    new-instance p2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {p2, v0, v0}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 595790
    invoke-virtual {p2, p3, v1, p1, p3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 595791
    const/16 v0, 0xb

    invoke-virtual {p2, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 595792
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_0

    .line 595793
    const/16 v0, 0x15

    invoke-virtual {p2, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 595794
    :cond_0
    const/16 v0, 0xa

    invoke-virtual {p2, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 595795
    iget-object v0, p0, LX/3YM;->h:Lcom/facebook/attachments/angora/InstantArticleIconView;

    invoke-virtual {p0, v0, p2}, LX/3YM;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 595796
    return-void
.end method


# virtual methods
.method public getTooltipAnchor()Landroid/view/View;
    .locals 1

    .prologue
    .line 595779
    iget-object v0, p0, LX/3YM;->h:Lcom/facebook/attachments/angora/InstantArticleIconView;

    return-object v0
.end method

.method public setCoverPhotoArticleIconVisibility(I)V
    .locals 1

    .prologue
    .line 595777
    iget-object v0, p0, LX/3YM;->h:Lcom/facebook/attachments/angora/InstantArticleIconView;

    invoke-virtual {v0, p1}, Lcom/facebook/attachments/angora/InstantArticleIconView;->setVisibility(I)V

    .line 595778
    return-void
.end method
