.class public LX/4rx;
.super LX/2Aq;
.source ""


# instance fields
.field public final a:LX/0lU;

.field public final b:LX/2An;

.field public final c:Ljava/lang/String;


# direct methods
.method private constructor <init>(LX/2An;Ljava/lang/String;LX/0lU;)V
    .locals 0

    .prologue
    .line 817104
    invoke-direct {p0}, LX/2Aq;-><init>()V

    .line 817105
    iput-object p3, p0, LX/4rx;->a:LX/0lU;

    .line 817106
    iput-object p1, p0, LX/4rx;->b:LX/2An;

    .line 817107
    iput-object p2, p0, LX/4rx;->c:Ljava/lang/String;

    .line 817108
    return-void
.end method

.method public static a(LX/0m4;LX/2An;)LX/4rx;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0m4",
            "<*>;",
            "LX/2An;",
            ")",
            "LX/4rx;"
        }
    .end annotation

    .prologue
    .line 817103
    new-instance v1, LX/4rx;

    invoke-virtual {p1}, LX/0lO;->b()Ljava/lang/String;

    move-result-object v2

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-direct {v1, p1, v2, v0}, LX/4rx;-><init>(LX/2An;Ljava/lang/String;LX/0lU;)V

    return-object v1

    :cond_0
    invoke-virtual {p0}, LX/0m4;->a()LX/0lU;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/0m4;LX/2An;Ljava/lang/String;)LX/4rx;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0m4",
            "<*>;",
            "LX/2An;",
            "Ljava/lang/String;",
            ")",
            "LX/4rx;"
        }
    .end annotation

    .prologue
    .line 817102
    new-instance v1, LX/4rx;

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-direct {v1, p1, p2, v0}, LX/4rx;-><init>(LX/2An;Ljava/lang/String;LX/0lU;)V

    return-object v1

    :cond_0
    invoke-virtual {p0}, LX/0m4;->a()LX/0lU;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 817101
    iget-object v0, p0, LX/4rx;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final b()LX/2Vb;
    .locals 1

    .prologue
    .line 817098
    iget-object v0, p0, LX/4rx;->a:LX/0lU;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 817099
    :cond_0
    const/4 v0, 0x0

    move-object v0, v0

    .line 817100
    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 817097
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 817096
    invoke-virtual {p0}, LX/2Aq;->i()LX/2At;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 817074
    invoke-virtual {p0}, LX/2Aq;->j()LX/2At;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 817095
    iget-object v0, p0, LX/4rx;->b:LX/2An;

    instance-of v0, v0, LX/2Am;

    return v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 817094
    iget-object v0, p0, LX/4rx;->b:LX/2An;

    instance-of v0, v0, LX/2Vd;

    return v0
.end method

.method public final i()LX/2At;
    .locals 1

    .prologue
    .line 817091
    iget-object v0, p0, LX/4rx;->b:LX/2An;

    instance-of v0, v0, LX/2At;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/4rx;->b:LX/2An;

    check-cast v0, LX/2At;

    invoke-virtual {v0}, LX/2At;->l()I

    move-result v0

    if-nez v0, :cond_0

    .line 817092
    iget-object v0, p0, LX/4rx;->b:LX/2An;

    check-cast v0, LX/2At;

    .line 817093
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()LX/2At;
    .locals 2

    .prologue
    .line 817088
    iget-object v0, p0, LX/4rx;->b:LX/2An;

    instance-of v0, v0, LX/2At;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/4rx;->b:LX/2An;

    check-cast v0, LX/2At;

    invoke-virtual {v0}, LX/2At;->l()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 817089
    iget-object v0, p0, LX/4rx;->b:LX/2An;

    check-cast v0, LX/2At;

    .line 817090
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()LX/2Am;
    .locals 1

    .prologue
    .line 817087
    iget-object v0, p0, LX/4rx;->b:LX/2An;

    instance-of v0, v0, LX/2Am;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/4rx;->b:LX/2An;

    check-cast v0, LX/2Am;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()LX/2Vd;
    .locals 1

    .prologue
    .line 817086
    iget-object v0, p0, LX/4rx;->b:LX/2An;

    instance-of v0, v0, LX/2Vd;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/4rx;->b:LX/2An;

    check-cast v0, LX/2Vd;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final m()LX/2An;
    .locals 1

    .prologue
    .line 817082
    invoke-virtual {p0}, LX/2Aq;->i()LX/2At;

    move-result-object v0

    .line 817083
    if-nez v0, :cond_0

    .line 817084
    invoke-virtual {p0}, LX/2Aq;->k()LX/2Am;

    move-result-object v0

    .line 817085
    :cond_0
    return-object v0
.end method

.method public final n()LX/2An;
    .locals 1

    .prologue
    .line 817076
    invoke-virtual {p0}, LX/2Aq;->l()LX/2Vd;

    move-result-object v0

    .line 817077
    if-nez v0, :cond_0

    .line 817078
    invoke-virtual {p0}, LX/2Aq;->j()LX/2At;

    move-result-object v0

    .line 817079
    if-nez v0, :cond_0

    .line 817080
    invoke-virtual {p0}, LX/2Aq;->k()LX/2Am;

    move-result-object v0

    .line 817081
    :cond_0
    return-object v0
.end method

.method public final o()LX/2An;
    .locals 1

    .prologue
    .line 817075
    iget-object v0, p0, LX/4rx;->b:LX/2An;

    return-object v0
.end method
