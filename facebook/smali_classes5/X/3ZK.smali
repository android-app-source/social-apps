.class public LX/3ZK;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# static fields
.field public static final a:LX/1Cz;


# instance fields
.field public b:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:Landroid/view/View;

.field public d:Lcom/facebook/fbui/widget/text/ImageWithTextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 599368
    new-instance v0, LX/3ZL;

    invoke-direct {v0}, LX/3ZL;-><init>()V

    sput-object v0, LX/3ZK;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 599369
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 599370
    const-class v0, LX/3ZK;

    invoke-static {v0, p0}, LX/3ZK;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 599371
    const v0, 0x7f0314b8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 599372
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/3ZK;->setOrientation(I)V

    .line 599373
    const v0, 0x7f0d2f07

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/3ZK;->c:Landroid/view/View;

    .line 599374
    const v0, 0x7f0d2f06

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/text/ImageWithTextView;

    iput-object v0, p0, LX/3ZK;->d:Lcom/facebook/fbui/widget/text/ImageWithTextView;

    .line 599375
    iget-object v0, p0, LX/3ZK;->b:LX/0wM;

    const v1, 0x7f0219c9

    const v2, -0x6e685d

    invoke-virtual {v0, v1, v2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 599376
    iget-object v1, p0, LX/3ZK;->d:Lcom/facebook/fbui/widget/text/ImageWithTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 599377
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/3ZK;

    invoke-static {p0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object p0

    check-cast p0, LX/0wM;

    iput-object p0, p1, LX/3ZK;->b:LX/0wM;

    return-void
.end method


# virtual methods
.method public setHasBottomDivider(Z)V
    .locals 2

    .prologue
    .line 599378
    iget-object v1, p0, LX/3ZK;->c:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 599379
    return-void

    .line 599380
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setHasButton(Z)V
    .locals 2

    .prologue
    .line 599381
    iget-object v1, p0, LX/3ZK;->d:Lcom/facebook/fbui/widget/text/ImageWithTextView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setVisibility(I)V

    .line 599382
    return-void

    .line 599383
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method
