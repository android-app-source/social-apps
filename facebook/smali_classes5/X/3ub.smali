.class public LX/3ub;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 648967
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 648968
    iput-object p1, p0, LX/3ub;->a:Landroid/content/Context;

    .line 648969
    return-void
.end method

.method public static a(Landroid/content/Context;)LX/3ub;
    .locals 1

    .prologue
    .line 648970
    new-instance v0, LX/3ub;

    invoke-direct {v0, p0}, LX/3ub;-><init>(Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method public final d()Z
    .locals 2

    .prologue
    .line 648971
    iget-object v0, p0, LX/3ub;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    .line 648972
    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 648973
    iget-object v0, p0, LX/3ub;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f090000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    .line 648974
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/3ub;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090001

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    goto :goto_0
.end method

.method public final e()I
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 648975
    iget-object v0, p0, LX/3ub;->a:Landroid/content/Context;

    const/4 v1, 0x0

    sget-object v2, LX/03r;->ActionBar:[I

    const v3, 0x7f010011

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 648976
    const/16 v0, 0x0

    invoke-virtual {v1, v0, v4}, Landroid/content/res/TypedArray;->getLayoutDimension(II)I

    move-result v0

    .line 648977
    iget-object v2, p0, LX/3ub;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 648978
    invoke-virtual {p0}, LX/3ub;->d()Z

    move-result v3

    if-nez v3, :cond_0

    .line 648979
    const v3, 0x7f0b0004

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 648980
    :cond_0
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 648981
    return v0
.end method

.method public final g()I
    .locals 2

    .prologue
    .line 648982
    iget-object v0, p0, LX/3ub;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0003

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method
