.class public LX/48O;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0s1;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# instance fields
.field public final a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final b:LX/0Xl;

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Uo;

.field private final f:LX/0s2;

.field public final g:LX/0s9;

.field private final h:LX/0dN;

.field public final i:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public j:LX/0s9;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public k:LX/0s9;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private l:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Xl;LX/0Or;LX/0Or;LX/0Uo;LX/0s2;LX/0Or;)V
    .locals 5
    .param p2    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/config/server/ShouldUsePreferredConfig;
        .end annotation
    .end param
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/config/server/IsBootstrapEnabled;
        .end annotation
    .end param
    .param p7    # LX/0Or;
        .annotation runtime Lcom/facebook/config/application/ApiConnectionType;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Xl;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Uo;",
            "LX/0s2;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 672826
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 672827
    iput-object p1, p0, LX/48O;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 672828
    iput-object p2, p0, LX/48O;->b:LX/0Xl;

    .line 672829
    iput-object p3, p0, LX/48O;->c:LX/0Or;

    .line 672830
    iput-object p4, p0, LX/48O;->d:LX/0Or;

    .line 672831
    iput-object p5, p0, LX/48O;->e:LX/0Uo;

    .line 672832
    iput-object p6, p0, LX/48O;->f:LX/0s2;

    .line 672833
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/48O;->l:Z

    .line 672834
    iput-object p7, p0, LX/48O;->i:LX/0Or;

    .line 672835
    new-instance v0, LX/0sB;

    new-instance v1, LX/4cE;

    const-string v2, "facebook.com"

    const-string v3, ""

    iget-object v4, p0, LX/48O;->i:LX/0Or;

    invoke-direct {v1, v2, v3, v4}, LX/4cE;-><init>(Ljava/lang/String;Ljava/lang/String;LX/0Or;)V

    iget-object v2, p0, LX/48O;->f:LX/0s2;

    invoke-virtual {v2}, LX/0s2;->a()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, p5, v2}, LX/0sB;-><init>(LX/0s9;LX/0Uo;Ljava/lang/String;)V

    iput-object v0, p0, LX/48O;->g:LX/0s9;

    .line 672836
    new-instance v0, LX/3CJ;

    invoke-direct {v0, p0}, LX/3CJ;-><init>(LX/48O;)V

    iput-object v0, p0, LX/48O;->h:LX/0dN;

    .line 672837
    return-void
.end method

.method public static f(LX/48O;)V
    .locals 3
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation

    .prologue
    .line 672777
    iget-boolean v0, p0, LX/48O;->l:Z

    if-eqz v0, :cond_0

    .line 672778
    :goto_0
    return-void

    .line 672779
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/48O;->l:Z

    .line 672780
    iget-object v0, p0, LX/48O;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0dU;->p:LX/0Tn;

    iget-object v2, p0, LX/48O;->h:LX/0dN;

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->c(LX/0Tn;LX/0dN;)V

    goto :goto_0
.end method

.method public static declared-synchronized g(LX/48O;)V
    .locals 2

    .prologue
    .line 672821
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/48O;->j:LX/0s9;

    if-eqz v0, :cond_0

    .line 672822
    const/4 v0, 0x0

    iput-object v0, p0, LX/48O;->j:LX/0s9;

    .line 672823
    iget-object v0, p0, LX/48O;->b:LX/0Xl;

    const-string v1, "com.facebook.config.server.ACTION_SERVER_CONFIG_CHANGED"

    invoke-interface {v0, v1}, LX/0Xl;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 672824
    :cond_0
    monitor-exit p0

    return-void

    .line 672825
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()LX/0s9;
    .locals 6

    .prologue
    .line 672795
    monitor-enter p0

    :try_start_0
    const/4 v1, 0x0

    .line 672796
    iget-object v0, p0, LX/48O;->j:LX/0s9;

    if-eqz v0, :cond_1

    .line 672797
    :cond_0
    :goto_0
    iget-object v0, p0, LX/48O;->j:LX/0s9;

    invoke-interface {v0}, LX/0s9;->b()Landroid/net/Uri$Builder;

    .line 672798
    iget-object v0, p0, LX/48O;->j:LX/0s9;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 672799
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 672800
    :cond_1
    :try_start_1
    invoke-static {p0}, LX/48O;->f(LX/48O;)V

    .line 672801
    iget-object v0, p0, LX/48O;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 672802
    iget-object v0, p0, LX/48O;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/0dU;->q:LX/0Tn;

    const-string v3, "default"

    invoke-interface {v0, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 672803
    iget-object v2, p0, LX/48O;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/0dU;->t:LX/0Tn;

    const-string v4, ""

    invoke-interface {v2, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 672804
    const-string v3, "intern"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 672805
    const-string v0, "intern.facebook.com"

    .line 672806
    :goto_1
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 672807
    :cond_2
    new-instance v1, LX/0sB;

    new-instance v3, LX/4cE;

    iget-object v4, p0, LX/48O;->i:LX/0Or;

    invoke-direct {v3, v0, v2, v4}, LX/4cE;-><init>(Ljava/lang/String;Ljava/lang/String;LX/0Or;)V

    iget-object v4, p0, LX/48O;->e:LX/0Uo;

    invoke-virtual {p0}, LX/48O;->d()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v3, v4, v5}, LX/0sB;-><init>(LX/0s9;LX/0Uo;Ljava/lang/String;)V

    move-object v0, v1

    .line 672808
    iput-object v0, p0, LX/48O;->j:LX/0s9;

    .line 672809
    :cond_3
    iget-object v0, p0, LX/48O;->j:LX/0s9;

    if-nez v0, :cond_0

    .line 672810
    iget-object v0, p0, LX/48O;->g:LX/0s9;

    iput-object v0, p0, LX/48O;->j:LX/0s9;

    goto :goto_0

    .line 672811
    :cond_4
    const-string v3, "dev"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 672812
    const-string v0, "dev.facebook.com"

    goto :goto_1

    .line 672813
    :cond_5
    const-string v3, "production"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 672814
    iget-object v0, p0, LX/48O;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/0dU;->r:LX/0Tn;

    invoke-interface {v0, v3, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 672815
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 672816
    :try_start_2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 672817
    :catch_0
    move-exception v0

    .line 672818
    const-string v3, "DefaultServerConfig"

    const-string v4, "Failed to parse web sandbox URL"

    invoke-static {v3, v4, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    .line 672819
    goto :goto_1

    .line 672820
    :cond_6
    const-string v0, "facebook.com"

    goto :goto_1

    :cond_7
    move-object v0, v1

    goto :goto_1
.end method

.method public final b()LX/0s9;
    .locals 1

    .prologue
    .line 672838
    iget-object v0, p0, LX/48O;->g:LX/0s9;

    return-object v0
.end method

.method public final declared-synchronized c()LX/0s9;
    .locals 4

    .prologue
    .line 672787
    monitor-enter p0

    .line 672788
    :try_start_0
    iget-object v0, p0, LX/48O;->k:LX/0s9;

    if-eqz v0, :cond_0

    .line 672789
    :goto_0
    iget-object v0, p0, LX/48O;->k:LX/0s9;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 672790
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 672791
    :cond_0
    invoke-static {p0}, LX/48O;->f(LX/48O;)V

    .line 672792
    iget-object v0, p0, LX/48O;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 672793
    new-instance v0, LX/0sB;

    new-instance v1, LX/0sD;

    iget-object v2, p0, LX/48O;->i:LX/0Or;

    invoke-direct {v1, v2}, LX/0sD;-><init>(LX/0Or;)V

    iget-object v2, p0, LX/48O;->e:LX/0Uo;

    invoke-virtual {p0}, LX/48O;->d()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LX/0sB;-><init>(LX/0s9;LX/0Uo;Ljava/lang/String;)V

    iput-object v0, p0, LX/48O;->k:LX/0s9;

    goto :goto_0

    .line 672794
    :cond_1
    invoke-virtual {p0}, LX/48O;->a()LX/0s9;

    move-result-object v0

    iput-object v0, p0, LX/48O;->k:LX/0s9;

    goto :goto_0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 672786
    iget-object v0, p0, LX/48O;->f:LX/0s2;

    invoke-virtual {v0}, LX/0s2;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 6

    .prologue
    .line 672781
    iget-object v0, p0, LX/48O;->f:LX/0s2;

    .line 672782
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 672783
    const-string v2, "[%s/%s;%s/%s;]"

    const-string v3, "FBAN"

    iget-object v4, v0, LX/0s2;->d:Ljava/lang/String;

    const-string v5, "FBAV"

    iget-object p0, v0, LX/0s2;->b:LX/0WV;

    invoke-virtual {p0}, LX/0WV;->a()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, LX/0s2;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-static {v2, v3, v4, v5, p0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 672784
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 672785
    return-object v0
.end method
