.class public abstract LX/3vM;
.super Landroid/view/ViewGroup;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Landroid/widget/Adapter;",
        ">",
        "Landroid/view/ViewGroup;"
    }
.end annotation


# instance fields
.field public A:I

.field public B:I

.field public C:J

.field public D:Z

.field public a:I

.field private b:Landroid/view/View;

.field private c:Z

.field private d:Z

.field public e:Landroid/support/v7/internal/widget/AdapterViewCompat$SelectionNotifier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/3vM",
            "<TT;>.SelectionNotifier;"
        }
    .end annotation
.end field

.field public j:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "scrolling"
    .end annotation
.end field

.field public k:I

.field public l:I

.field public m:J

.field public n:J

.field public o:Z

.field public p:I

.field public q:Z

.field public r:LX/3vh;

.field public s:LX/3vf;

.field public t:LX/3vg;

.field public u:Z

.field public v:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "list"
    .end annotation
.end field

.field public w:J

.field public x:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "list"
    .end annotation
.end field

.field public y:J

.field public z:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "list"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v1, -0x1

    const-wide/high16 v2, -0x8000000000000000L

    const/4 v0, 0x0

    .line 651297
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 651298
    iput v0, p0, LX/3vM;->j:I

    .line 651299
    iput-wide v2, p0, LX/3vM;->m:J

    .line 651300
    iput-boolean v0, p0, LX/3vM;->o:Z

    .line 651301
    iput-boolean v0, p0, LX/3vM;->q:Z

    .line 651302
    iput v1, p0, LX/3vM;->v:I

    .line 651303
    iput-wide v2, p0, LX/3vM;->w:J

    .line 651304
    iput v1, p0, LX/3vM;->x:I

    .line 651305
    iput-wide v2, p0, LX/3vM;->y:J

    .line 651306
    iput v1, p0, LX/3vM;->B:I

    .line 651307
    iput-wide v2, p0, LX/3vM;->C:J

    .line 651308
    iput-boolean v0, p0, LX/3vM;->D:Z

    .line 651309
    return-void
.end method

.method private a(I)J
    .locals 2

    .prologue
    .line 651235
    invoke-virtual {p0}, LX/3vM;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    .line 651236
    if-eqz v0, :cond_0

    if-gez p1, :cond_1

    :cond_0
    const-wide/high16 v0, -0x8000000000000000L

    :goto_0
    return-wide v0

    :cond_1
    invoke-interface {v0, p1}, Landroid/widget/Adapter;->getItemId(I)J

    move-result-wide v0

    goto :goto_0
.end method

.method public static synthetic a(LX/3vM;)Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 651237
    invoke-virtual {p0}, LX/3vM;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic a(LX/3vM;Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 651238
    invoke-virtual {p0, p1}, LX/3vM;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    return-void
.end method

.method private a(Z)V
    .locals 6

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 651239
    goto :goto_0

    .line 651240
    :goto_0
    if-eqz p1, :cond_2

    .line 651241
    iget-object v0, p0, LX/3vM;->b:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 651242
    iget-object v0, p0, LX/3vM;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 651243
    invoke-virtual {p0, v2}, LX/3vM;->setVisibility(I)V

    .line 651244
    :goto_1
    iget-boolean v0, p0, LX/3vM;->u:Z

    if-eqz v0, :cond_0

    .line 651245
    invoke-virtual {p0}, LX/3vM;->getLeft()I

    move-result v2

    invoke-virtual {p0}, LX/3vM;->getTop()I

    move-result v3

    invoke-virtual {p0}, LX/3vM;->getRight()I

    move-result v4

    invoke-virtual {p0}, LX/3vM;->getBottom()I

    move-result v5

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, LX/3vM;->onLayout(ZIIII)V

    .line 651246
    :cond_0
    :goto_2
    return-void

    .line 651247
    :cond_1
    invoke-virtual {p0, v1}, LX/3vM;->setVisibility(I)V

    goto :goto_1

    .line 651248
    :cond_2
    iget-object v0, p0, LX/3vM;->b:Landroid/view/View;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/3vM;->b:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 651249
    :cond_3
    invoke-virtual {p0, v1}, LX/3vM;->setVisibility(I)V

    goto :goto_2
.end method

.method public static c(LX/3vM;)V
    .locals 2

    .prologue
    .line 651250
    iget-object v0, p0, LX/3vM;->r:LX/3vh;

    if-nez v0, :cond_1

    .line 651251
    :cond_0
    :goto_0
    return-void

    .line 651252
    :cond_1
    iget v0, p0, LX/3vM;->v:I

    move v0, v0

    .line 651253
    if-ltz v0, :cond_0

    .line 651254
    invoke-virtual {p0}, LX/3vM;->getSelectedView()Landroid/view/View;

    .line 651255
    invoke-virtual {p0}, LX/3vM;->getAdapter()Landroid/widget/Adapter;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/widget/Adapter;->getItemId(I)J

    goto :goto_0
.end method

.method private d()I
    .locals 12

    .prologue
    .line 651256
    iget v6, p0, LX/3vM;->z:I

    .line 651257
    if-nez v6, :cond_0

    .line 651258
    const/4 v3, -0x1

    .line 651259
    :goto_0
    return v3

    .line 651260
    :cond_0
    iget-wide v8, p0, LX/3vM;->m:J

    .line 651261
    iget v0, p0, LX/3vM;->l:I

    .line 651262
    const-wide/high16 v2, -0x8000000000000000L

    cmp-long v1, v8, v2

    if-nez v1, :cond_1

    .line 651263
    const/4 v3, -0x1

    goto :goto_0

    .line 651264
    :cond_1
    const/4 v1, 0x0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 651265
    add-int/lit8 v1, v6, -0x1

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 651266
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x64

    add-long v10, v2, v4

    .line 651267
    const/4 v0, 0x0

    .line 651268
    invoke-virtual {p0}, LX/3vM;->getAdapter()Landroid/widget/Adapter;

    move-result-object v7

    .line 651269
    if-nez v7, :cond_b

    .line 651270
    const/4 v3, -0x1

    goto :goto_0

    .line 651271
    :cond_2
    add-int/lit8 v4, v6, -0x1

    if-ne v1, v4, :cond_6

    const/4 v4, 0x1

    move v5, v4

    .line 651272
    :goto_1
    if-nez v2, :cond_7

    const/4 v4, 0x1

    .line 651273
    :goto_2
    if-eqz v5, :cond_3

    if-nez v4, :cond_a

    .line 651274
    :cond_3
    if-nez v4, :cond_4

    if-eqz v0, :cond_8

    if-nez v5, :cond_8

    .line 651275
    :cond_4
    add-int/lit8 v1, v1, 0x1

    .line 651276
    const/4 v0, 0x0

    move v3, v1

    .line 651277
    :cond_5
    :goto_3
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    cmp-long v4, v4, v10

    if-gtz v4, :cond_a

    .line 651278
    invoke-interface {v7, v3}, Landroid/widget/Adapter;->getItemId(I)J

    move-result-wide v4

    .line 651279
    cmp-long v4, v4, v8

    if-nez v4, :cond_2

    goto :goto_0

    .line 651280
    :cond_6
    const/4 v4, 0x0

    move v5, v4

    goto :goto_1

    .line 651281
    :cond_7
    const/4 v4, 0x0

    goto :goto_2

    .line 651282
    :cond_8
    if-nez v5, :cond_9

    if-nez v0, :cond_5

    if-nez v4, :cond_5

    .line 651283
    :cond_9
    add-int/lit8 v2, v2, -0x1

    .line 651284
    const/4 v0, 0x1

    move v3, v2

    goto :goto_3

    .line 651285
    :cond_a
    const/4 v3, -0x1

    goto :goto_0

    :cond_b
    move v2, v1

    move v3, v1

    goto :goto_3
.end method


# virtual methods
.method public final a(Landroid/view/View;IJ)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 651286
    iget-object v2, p0, LX/3vM;->s:LX/3vf;

    if-eqz v2, :cond_1

    .line 651287
    invoke-virtual {p0, v1}, LX/3vM;->playSoundEffect(I)V

    .line 651288
    if-eqz p1, :cond_0

    .line 651289
    invoke-virtual {p1, v0}, Landroid/view/View;->sendAccessibilityEvent(I)V

    .line 651290
    :cond_0
    iget-object v1, p0, LX/3vM;->s:LX/3vf;

    invoke-interface {v1, p1}, LX/3vf;->a(Landroid/view/View;)V

    .line 651291
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final addView(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 651292
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "addView(View) is not supported in AdapterView"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final addView(Landroid/view/View;I)V
    .locals 2

    .prologue
    .line 651293
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "addView(View, int) is not supported in AdapterView"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .locals 2

    .prologue
    .line 651294
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "addView(View, int, LayoutParams) is not supported in AdapterView"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 2

    .prologue
    .line 651371
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "addView(View, LayoutParams) is not supported in AdapterView"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final canAnimate()Z
    .locals 1

    .prologue
    .line 651295
    invoke-super {p0}, Landroid/view/ViewGroup;->canAnimate()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, LX/3vM;->z:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 2

    .prologue
    .line 651367
    invoke-virtual {p0}, LX/3vM;->getSelectedView()Landroid/view/View;

    move-result-object v0

    .line 651368
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0, p1}, Landroid/view/View;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 651369
    const/4 v0, 0x1

    .line 651370
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final dispatchRestoreInstanceState(Landroid/util/SparseArray;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 651365
    invoke-virtual {p0, p1}, LX/3vM;->dispatchThawSelfOnly(Landroid/util/SparseArray;)V

    .line 651366
    return-void
.end method

.method public final dispatchSaveInstanceState(Landroid/util/SparseArray;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 651363
    invoke-virtual {p0, p1}, LX/3vM;->dispatchFreezeSelfOnly(Landroid/util/SparseArray;)V

    .line 651364
    return-void
.end method

.method public final e()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 651350
    invoke-virtual {p0}, LX/3vM;->getAdapter()Landroid/widget/Adapter;

    move-result-object v4

    .line 651351
    if-eqz v4, :cond_0

    invoke-interface {v4}, Landroid/widget/Adapter;->getCount()I

    move-result v0

    if-nez v0, :cond_5

    :cond_0
    move v0, v1

    .line 651352
    :goto_0
    if-eqz v0, :cond_1

    .line 651353
    goto :goto_4

    :cond_1
    move v3, v1

    .line 651354
    :goto_1
    if-eqz v3, :cond_6

    iget-boolean v0, p0, LX/3vM;->d:Z

    if-eqz v0, :cond_6

    move v0, v1

    :goto_2
    invoke-super {p0, v0}, Landroid/view/ViewGroup;->setFocusableInTouchMode(Z)V

    .line 651355
    if-eqz v3, :cond_7

    iget-boolean v0, p0, LX/3vM;->c:Z

    if-eqz v0, :cond_7

    move v0, v1

    :goto_3
    invoke-super {p0, v0}, Landroid/view/ViewGroup;->setFocusable(Z)V

    .line 651356
    iget-object v0, p0, LX/3vM;->b:Landroid/view/View;

    if-eqz v0, :cond_4

    .line 651357
    if-eqz v4, :cond_2

    invoke-interface {v4}, Landroid/widget/Adapter;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    move v2, v1

    :cond_3
    invoke-direct {p0, v2}, LX/3vM;->a(Z)V

    .line 651358
    :cond_4
    return-void

    :cond_5
    move v0, v2

    .line 651359
    goto :goto_0

    :goto_4
    move v3, v2

    .line 651360
    goto :goto_1

    :cond_6
    move v0, v2

    .line 651361
    goto :goto_2

    :cond_7
    move v0, v2

    .line 651362
    goto :goto_3
.end method

.method public final f()V
    .locals 8

    .prologue
    const-wide/high16 v6, -0x8000000000000000L

    const/4 v2, 0x1

    const/4 v5, -0x1

    const/4 v1, 0x0

    .line 651322
    iget v4, p0, LX/3vM;->z:I

    .line 651323
    if-lez v4, :cond_6

    .line 651324
    iget-boolean v0, p0, LX/3vM;->o:Z

    if-eqz v0, :cond_5

    .line 651325
    iput-boolean v1, p0, LX/3vM;->o:Z

    .line 651326
    invoke-direct {p0}, LX/3vM;->d()I

    move-result v0

    .line 651327
    if-ltz v0, :cond_5

    .line 651328
    move v3, v0

    .line 651329
    if-ne v3, v0, :cond_5

    .line 651330
    invoke-virtual {p0, v0}, LX/3vM;->setNextSelectedPositionInt(I)V

    move v3, v2

    .line 651331
    :goto_0
    if-nez v3, :cond_3

    .line 651332
    iget v0, p0, LX/3vM;->v:I

    move v0, v0

    .line 651333
    if-lt v0, v4, :cond_0

    .line 651334
    add-int/lit8 v0, v4, -0x1

    .line 651335
    :cond_0
    if-gez v0, :cond_1

    move v0, v1

    .line 651336
    :cond_1
    move v4, v0

    .line 651337
    if-gez v4, :cond_4

    .line 651338
    move v0, v0

    .line 651339
    :goto_1
    if-ltz v0, :cond_3

    .line 651340
    invoke-virtual {p0, v0}, LX/3vM;->setNextSelectedPositionInt(I)V

    .line 651341
    invoke-virtual {p0}, LX/3vM;->g()V

    move v0, v2

    .line 651342
    :goto_2
    if-nez v0, :cond_2

    .line 651343
    iput v5, p0, LX/3vM;->x:I

    .line 651344
    iput-wide v6, p0, LX/3vM;->y:J

    .line 651345
    iput v5, p0, LX/3vM;->v:I

    .line 651346
    iput-wide v6, p0, LX/3vM;->w:J

    .line 651347
    iput-boolean v1, p0, LX/3vM;->o:Z

    .line 651348
    invoke-virtual {p0}, LX/3vM;->g()V

    .line 651349
    :cond_2
    return-void

    :cond_3
    move v0, v3

    goto :goto_2

    :cond_4
    move v0, v4

    goto :goto_1

    :cond_5
    move v3, v1

    goto :goto_0

    :cond_6
    move v0, v1

    goto :goto_2
.end method

.method public final g()V
    .locals 4

    .prologue
    .line 651310
    iget v0, p0, LX/3vM;->x:I

    iget v1, p0, LX/3vM;->B:I

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, LX/3vM;->y:J

    iget-wide v2, p0, LX/3vM;->C:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_5

    .line 651311
    :cond_0
    iget-object v0, p0, LX/3vM;->r:LX/3vh;

    if-eqz v0, :cond_3

    .line 651312
    iget-boolean v0, p0, LX/3vM;->q:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, LX/3vM;->D:Z

    if-eqz v0, :cond_6

    .line 651313
    :cond_1
    iget-object v0, p0, LX/3vM;->e:Landroid/support/v7/internal/widget/AdapterViewCompat$SelectionNotifier;

    if-nez v0, :cond_2

    .line 651314
    new-instance v0, Landroid/support/v7/internal/widget/AdapterViewCompat$SelectionNotifier;

    invoke-direct {v0, p0}, Landroid/support/v7/internal/widget/AdapterViewCompat$SelectionNotifier;-><init>(LX/3vM;)V

    iput-object v0, p0, LX/3vM;->e:Landroid/support/v7/internal/widget/AdapterViewCompat$SelectionNotifier;

    .line 651315
    :cond_2
    iget-object v0, p0, LX/3vM;->e:Landroid/support/v7/internal/widget/AdapterViewCompat$SelectionNotifier;

    invoke-virtual {p0, v0}, LX/3vM;->post(Ljava/lang/Runnable;)Z

    .line 651316
    :cond_3
    :goto_0
    iget v0, p0, LX/3vM;->x:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_4

    invoke-virtual {p0}, LX/3vM;->isShown()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, LX/3vM;->isInTouchMode()Z

    move-result v0

    if-nez v0, :cond_4

    .line 651317
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, LX/3vM;->sendAccessibilityEvent(I)V

    .line 651318
    :cond_4
    iget v0, p0, LX/3vM;->x:I

    iput v0, p0, LX/3vM;->B:I

    .line 651319
    iget-wide v0, p0, LX/3vM;->y:J

    iput-wide v0, p0, LX/3vM;->C:J

    .line 651320
    :cond_5
    return-void

    .line 651321
    :cond_6
    invoke-static {p0}, LX/3vM;->c(LX/3vM;)V

    goto :goto_0
.end method

.method public abstract getAdapter()Landroid/widget/Adapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method public getCount()I
    .locals 1
    .annotation runtime Landroid/view/ViewDebug$CapturedViewProperty;
    .end annotation

    .prologue
    .line 651296
    iget v0, p0, LX/3vM;->z:I

    return v0
.end method

.method public getEmptyView()Landroid/view/View;
    .locals 1

    .prologue
    .line 651233
    iget-object v0, p0, LX/3vM;->b:Landroid/view/View;

    return-object v0
.end method

.method public getFirstVisiblePosition()I
    .locals 1

    .prologue
    .line 651234
    iget v0, p0, LX/3vM;->j:I

    return v0
.end method

.method public getLastVisiblePosition()I
    .locals 2

    .prologue
    .line 651173
    iget v0, p0, LX/3vM;->j:I

    invoke-virtual {p0}, LX/3vM;->getChildCount()I

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public final getOnItemClickListener()LX/3vf;
    .locals 1

    .prologue
    .line 651188
    iget-object v0, p0, LX/3vM;->s:LX/3vf;

    return-object v0
.end method

.method public final getOnItemLongClickListener()LX/3vg;
    .locals 1

    .prologue
    .line 651187
    iget-object v0, p0, LX/3vM;->t:LX/3vg;

    return-object v0
.end method

.method public final getOnItemSelectedListener()LX/3vh;
    .locals 1

    .prologue
    .line 651186
    iget-object v0, p0, LX/3vM;->r:LX/3vh;

    return-object v0
.end method

.method public getSelectedItem()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 651181
    invoke-virtual {p0}, LX/3vM;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    .line 651182
    iget v1, p0, LX/3vM;->v:I

    move v1, v1

    .line 651183
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/widget/Adapter;->getCount()I

    move-result v2

    if-lez v2, :cond_0

    if-ltz v1, :cond_0

    .line 651184
    invoke-interface {v0, v1}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 651185
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract getSelectedView()Landroid/view/View;
.end method

.method public onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x14d15d89    # 2.11405E-26f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 651178
    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    .line 651179
    iget-object v1, p0, LX/3vM;->e:Landroid/support/v7/internal/widget/AdapterViewCompat$SelectionNotifier;

    invoke-virtual {p0, v1}, LX/3vM;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 651180
    const/16 v1, 0x2d

    const v2, 0x6b5fb8fc

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onLayout(ZIIII)V
    .locals 1

    .prologue
    .line 651176
    invoke-virtual {p0}, LX/3vM;->getHeight()I

    move-result v0

    iput v0, p0, LX/3vM;->a:I

    .line 651177
    return-void
.end method

.method public final removeAllViews()V
    .locals 2

    .prologue
    .line 651175
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "removeAllViews() is not supported in AdapterView"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final removeView(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 651174
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "removeView(View) is not supported in AdapterView"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final removeViewAt(I)V
    .locals 2

    .prologue
    .line 651172
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "removeViewAt(int) is not supported in AdapterView"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public abstract setAdapter(Landroid/widget/Adapter;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation
.end method

.method public setEmptyView(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 651189
    iput-object p1, p0, LX/3vM;->b:Landroid/view/View;

    .line 651190
    invoke-virtual {p0}, LX/3vM;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    .line 651191
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/widget/Adapter;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 651192
    :goto_0
    invoke-direct {p0, v0}, LX/3vM;->a(Z)V

    .line 651193
    return-void

    .line 651194
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setFocusable(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 651195
    invoke-virtual {p0}, LX/3vM;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    .line 651196
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/widget/Adapter;->getCount()I

    move-result v0

    if-nez v0, :cond_3

    :cond_0
    move v0, v2

    .line 651197
    :goto_0
    iput-boolean p1, p0, LX/3vM;->c:Z

    .line 651198
    if-nez p1, :cond_1

    .line 651199
    iput-boolean v1, p0, LX/3vM;->d:Z

    .line 651200
    :cond_1
    if-eqz p1, :cond_4

    if-eqz v0, :cond_2

    .line 651201
    goto :goto_2

    :cond_2
    :goto_1
    invoke-super {p0, v2}, Landroid/view/ViewGroup;->setFocusable(Z)V

    .line 651202
    return-void

    :cond_3
    move v0, v1

    .line 651203
    goto :goto_0

    :cond_4
    :goto_2
    move v2, v1

    .line 651204
    goto :goto_1
.end method

.method public setFocusableInTouchMode(Z)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 651205
    invoke-virtual {p0}, LX/3vM;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    .line 651206
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/widget/Adapter;->getCount()I

    move-result v0

    if-nez v0, :cond_3

    :cond_0
    move v0, v2

    .line 651207
    :goto_0
    iput-boolean p1, p0, LX/3vM;->d:Z

    .line 651208
    if-eqz p1, :cond_1

    .line 651209
    iput-boolean v2, p0, LX/3vM;->c:Z

    .line 651210
    :cond_1
    if-eqz p1, :cond_4

    if-eqz v0, :cond_2

    .line 651211
    goto :goto_2

    :cond_2
    :goto_1
    invoke-super {p0, v2}, Landroid/view/ViewGroup;->setFocusableInTouchMode(Z)V

    .line 651212
    return-void

    :cond_3
    move v0, v1

    .line 651213
    goto :goto_0

    :cond_4
    :goto_2
    move v2, v1

    .line 651214
    goto :goto_1
.end method

.method public setNextSelectedPositionInt(I)V
    .locals 2

    .prologue
    .line 651215
    iput p1, p0, LX/3vM;->v:I

    .line 651216
    invoke-direct {p0, p1}, LX/3vM;->a(I)J

    move-result-wide v0

    iput-wide v0, p0, LX/3vM;->w:J

    .line 651217
    iget-boolean v0, p0, LX/3vM;->o:Z

    if-eqz v0, :cond_0

    iget v0, p0, LX/3vM;->p:I

    if-nez v0, :cond_0

    if-ltz p1, :cond_0

    .line 651218
    iput p1, p0, LX/3vM;->l:I

    .line 651219
    iget-wide v0, p0, LX/3vM;->w:J

    iput-wide v0, p0, LX/3vM;->m:J

    .line 651220
    :cond_0
    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 2

    .prologue
    .line 651221
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Don\'t call setOnClickListener for an AdapterView. You probably want setOnItemClickListener instead"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setOnItemClickListener(LX/3vf;)V
    .locals 0

    .prologue
    .line 651222
    iput-object p1, p0, LX/3vM;->s:LX/3vf;

    .line 651223
    return-void
.end method

.method public setOnItemLongClickListener(LX/3vg;)V
    .locals 1

    .prologue
    .line 651224
    invoke-virtual {p0}, LX/3vM;->isLongClickable()Z

    move-result v0

    if-nez v0, :cond_0

    .line 651225
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/3vM;->setLongClickable(Z)V

    .line 651226
    :cond_0
    iput-object p1, p0, LX/3vM;->t:LX/3vg;

    .line 651227
    return-void
.end method

.method public setOnItemSelectedListener(LX/3vh;)V
    .locals 0

    .prologue
    .line 651228
    iput-object p1, p0, LX/3vM;->r:LX/3vh;

    .line 651229
    return-void
.end method

.method public setSelectedPositionInt(I)V
    .locals 2

    .prologue
    .line 651230
    iput p1, p0, LX/3vM;->x:I

    .line 651231
    invoke-direct {p0, p1}, LX/3vM;->a(I)J

    move-result-wide v0

    iput-wide v0, p0, LX/3vM;->y:J

    .line 651232
    return-void
.end method

.method public abstract setSelection(I)V
.end method
