.class public final LX/3iy;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    .line 630767
    const/4 v8, 0x0

    .line 630768
    const/4 v7, 0x0

    .line 630769
    const/4 v6, 0x0

    .line 630770
    const-wide/16 v4, 0x0

    .line 630771
    const/4 v3, 0x0

    .line 630772
    const/4 v2, 0x0

    .line 630773
    const/4 v1, 0x0

    .line 630774
    const/4 v0, 0x0

    .line 630775
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->START_OBJECT:LX/15z;

    if-eq v9, v10, :cond_c

    .line 630776
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 630777
    const/4 v0, 0x0

    .line 630778
    :goto_0
    return v0

    .line 630779
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v1, v10, :cond_a

    .line 630780
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v1

    .line 630781
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 630782
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_0

    if-eqz v1, :cond_0

    .line 630783
    const-string v10, "__type__"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_1

    const-string v10, "__typename"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 630784
    :cond_1
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v1

    move v9, v1

    goto :goto_1

    .line 630785
    :cond_2
    const-string v10, "action_links"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 630786
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 630787
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v10, LX/15z;->START_ARRAY:LX/15z;

    if-ne v5, v10, :cond_3

    .line 630788
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v10, LX/15z;->END_ARRAY:LX/15z;

    if-eq v5, v10, :cond_3

    .line 630789
    invoke-static {p0, p1}, LX/ER0;->b(LX/15w;LX/186;)I

    move-result v5

    .line 630790
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 630791
    :cond_3
    invoke-static {v1, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v1

    move v1, v1

    .line 630792
    move v5, v1

    goto :goto_1

    .line 630793
    :cond_4
    const-string v10, "cache_id"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 630794
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    move v4, v1

    goto :goto_1

    .line 630795
    :cond_5
    const-string v10, "creation_time"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 630796
    const/4 v0, 0x1

    .line 630797
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    goto/16 :goto_1

    .line 630798
    :cond_6
    const-string v10, "native_template_view"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 630799
    invoke-static {p0, p1}, LX/5ez;->a(LX/15w;LX/186;)I

    move-result v1

    move v8, v1

    goto/16 :goto_1

    .line 630800
    :cond_7
    const-string v10, "story_header"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 630801
    invoke-static {p0, p1}, LX/3iz;->a(LX/15w;LX/186;)I

    move-result v1

    move v7, v1

    goto/16 :goto_1

    .line 630802
    :cond_8
    const-string v10, "tracking"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 630803
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    move v6, v1

    goto/16 :goto_1

    .line 630804
    :cond_9
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 630805
    :cond_a
    const/4 v1, 0x7

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 630806
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v9}, LX/186;->b(II)V

    .line 630807
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 630808
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 630809
    if-eqz v0, :cond_b

    .line 630810
    const/4 v1, 0x3

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 630811
    :cond_b
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 630812
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 630813
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 630814
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :cond_c
    move v9, v8

    move v8, v3

    move v12, v2

    move-wide v2, v4

    move v4, v6

    move v5, v7

    move v7, v12

    move v6, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 630815
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 630816
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 630817
    if-eqz v0, :cond_0

    .line 630818
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 630819
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 630820
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 630821
    if-eqz v0, :cond_2

    .line 630822
    const-string v1, "action_links"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 630823
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 630824
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v4

    if-ge v1, v4, :cond_1

    .line 630825
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v4

    invoke-static {p0, v4, p2}, LX/DIZ;->a(LX/15i;ILX/0nX;)V

    .line 630826
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 630827
    :cond_1
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 630828
    :cond_2
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 630829
    if-eqz v0, :cond_3

    .line 630830
    const-string v1, "cache_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 630831
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 630832
    :cond_3
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 630833
    cmp-long v2, v0, v2

    if-eqz v2, :cond_4

    .line 630834
    const-string v2, "creation_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 630835
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 630836
    :cond_4
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 630837
    if-eqz v0, :cond_5

    .line 630838
    const-string v1, "native_template_view"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 630839
    invoke-static {p0, v0, p2, p3}, LX/5ez;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 630840
    :cond_5
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 630841
    if-eqz v0, :cond_6

    .line 630842
    const-string v1, "story_header"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 630843
    invoke-static {p0, v0, p2, p3}, LX/3iz;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 630844
    :cond_6
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 630845
    if-eqz v0, :cond_7

    .line 630846
    const-string v1, "tracking"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 630847
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 630848
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 630849
    return-void
.end method
