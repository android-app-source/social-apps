.class public final LX/4xo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/io/Serializable;"
    }
.end annotation


# instance fields
.field public final comparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<-TT;>;"
        }
    .end annotation
.end field

.field public final hasLowerBound:Z

.field public final hasUpperBound:Z

.field public final lowerBoundType:LX/4xG;

.field public final lowerEndpoint:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final upperBoundType:LX/4xG;

.field public final upperEndpoint:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Comparator;ZLjava/lang/Object;LX/4xG;ZLjava/lang/Object;LX/4xG;)V
    .locals 6
    .param p3    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Comparator",
            "<-TT;>;ZTT;",
            "LX/4xG;",
            "ZTT;",
            "LX/4xG;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 821359
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 821360
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Comparator;

    iput-object v0, p0, LX/4xo;->comparator:Ljava/util/Comparator;

    .line 821361
    iput-boolean p2, p0, LX/4xo;->hasLowerBound:Z

    .line 821362
    iput-boolean p5, p0, LX/4xo;->hasUpperBound:Z

    .line 821363
    iput-object p3, p0, LX/4xo;->lowerEndpoint:Ljava/lang/Object;

    .line 821364
    invoke-static {p4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4xG;

    iput-object v0, p0, LX/4xo;->lowerBoundType:LX/4xG;

    .line 821365
    iput-object p6, p0, LX/4xo;->upperEndpoint:Ljava/lang/Object;

    .line 821366
    invoke-static {p7}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4xG;

    iput-object v0, p0, LX/4xo;->upperBoundType:LX/4xG;

    .line 821367
    if-eqz p2, :cond_0

    .line 821368
    invoke-interface {p1, p3, p3}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    .line 821369
    :cond_0
    if-eqz p5, :cond_1

    .line 821370
    invoke-interface {p1, p6, p6}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    .line 821371
    :cond_1
    if-eqz p2, :cond_2

    if-eqz p5, :cond_2

    .line 821372
    invoke-interface {p1, p3, p6}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v3

    .line 821373
    if-gtz v3, :cond_3

    move v0, v1

    :goto_0
    const-string v4, "lowerEndpoint (%s) > upperEndpoint (%s)"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aput-object p3, v5, v2

    aput-object p6, v5, v1

    invoke-static {v0, v4, v5}, LX/0PB;->checkArgument(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 821374
    if-nez v3, :cond_2

    .line 821375
    sget-object v0, LX/4xG;->OPEN:LX/4xG;

    if-eq p4, v0, :cond_4

    move v0, v1

    :goto_1
    sget-object v3, LX/4xG;->OPEN:LX/4xG;

    if-eq p7, v3, :cond_5

    :goto_2
    or-int/2addr v0, v1

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 821376
    :cond_2
    return-void

    :cond_3
    move v0, v2

    .line 821377
    goto :goto_0

    :cond_4
    move v0, v2

    .line 821378
    goto :goto_1

    :cond_5
    move v1, v2

    goto :goto_2
.end method

.method public static a(Ljava/util/Comparator;)LX/4xo;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Comparator",
            "<-TT;>;)",
            "LX/4xo",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 821423
    new-instance v0, LX/4xo;

    sget-object v4, LX/4xG;->OPEN:LX/4xG;

    sget-object v7, LX/4xG;->OPEN:LX/4xG;

    move-object v1, p0

    move v5, v2

    move-object v6, v3

    invoke-direct/range {v0 .. v7}, LX/4xo;-><init>(Ljava/util/Comparator;ZLjava/lang/Object;LX/4xG;ZLjava/lang/Object;LX/4xG;)V

    return-object v0
.end method


# virtual methods
.method public final a(LX/4xo;)LX/4xo;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4xo",
            "<TT;>;)",
            "LX/4xo",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 821379
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 821380
    iget-object v0, p0, LX/4xo;->comparator:Ljava/util/Comparator;

    iget-object v1, p1, LX/4xo;->comparator:Ljava/util/Comparator;

    invoke-interface {v0, v1}, Ljava/util/Comparator;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 821381
    iget-boolean v0, p0, LX/4xo;->hasLowerBound:Z

    .line 821382
    iget-object v1, p0, LX/4xo;->lowerEndpoint:Ljava/lang/Object;

    move-object v2, v1

    .line 821383
    iget-object v1, p0, LX/4xo;->lowerBoundType:LX/4xG;

    move-object v1, v1

    .line 821384
    iget-boolean v3, p0, LX/4xo;->hasLowerBound:Z

    move v3, v3

    .line 821385
    if-nez v3, :cond_3

    .line 821386
    iget-boolean v0, p1, LX/4xo;->hasLowerBound:Z

    .line 821387
    :cond_0
    iget-object v1, p1, LX/4xo;->lowerEndpoint:Ljava/lang/Object;

    move-object v2, v1

    .line 821388
    iget-object v1, p1, LX/4xo;->lowerBoundType:LX/4xG;

    move-object v1, v1

    .line 821389
    move-object v9, v1

    move-object v1, v2

    move v2, v0

    move-object v0, v9

    .line 821390
    :goto_0
    iget-boolean v3, p0, LX/4xo;->hasUpperBound:Z

    .line 821391
    iget-object v4, p0, LX/4xo;->upperEndpoint:Ljava/lang/Object;

    move-object v6, v4

    .line 821392
    iget-object v4, p0, LX/4xo;->upperBoundType:LX/4xG;

    move-object v7, v4

    .line 821393
    iget-boolean v4, p0, LX/4xo;->hasUpperBound:Z

    move v4, v4

    .line 821394
    if-nez v4, :cond_5

    .line 821395
    iget-boolean v3, p1, LX/4xo;->hasUpperBound:Z

    .line 821396
    :cond_1
    iget-object v4, p1, LX/4xo;->upperEndpoint:Ljava/lang/Object;

    move-object v6, v4

    .line 821397
    iget-object v4, p1, LX/4xo;->upperBoundType:LX/4xG;

    move-object v7, v4

    .line 821398
    move v5, v3

    .line 821399
    :goto_1
    if-eqz v2, :cond_7

    if-eqz v5, :cond_7

    .line 821400
    iget-object v3, p0, LX/4xo;->comparator:Ljava/util/Comparator;

    invoke-interface {v3, v1, v6}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v3

    .line 821401
    if-gtz v3, :cond_2

    if-nez v3, :cond_7

    sget-object v3, LX/4xG;->OPEN:LX/4xG;

    if-ne v0, v3, :cond_7

    sget-object v3, LX/4xG;->OPEN:LX/4xG;

    if-ne v7, v3, :cond_7

    .line 821402
    :cond_2
    sget-object v0, LX/4xG;->OPEN:LX/4xG;

    .line 821403
    sget-object v7, LX/4xG;->CLOSED:LX/4xG;

    move-object v4, v0

    move-object v3, v6

    .line 821404
    :goto_2
    new-instance v0, LX/4xo;

    iget-object v1, p0, LX/4xo;->comparator:Ljava/util/Comparator;

    invoke-direct/range {v0 .. v7}, LX/4xo;-><init>(Ljava/util/Comparator;ZLjava/lang/Object;LX/4xG;ZLjava/lang/Object;LX/4xG;)V

    return-object v0

    .line 821405
    :cond_3
    iget-boolean v3, p1, LX/4xo;->hasLowerBound:Z

    move v3, v3

    .line 821406
    if-eqz v3, :cond_4

    .line 821407
    iget-object v3, p0, LX/4xo;->comparator:Ljava/util/Comparator;

    .line 821408
    iget-object v4, p0, LX/4xo;->lowerEndpoint:Ljava/lang/Object;

    move-object v4, v4

    .line 821409
    iget-object v5, p1, LX/4xo;->lowerEndpoint:Ljava/lang/Object;

    move-object v5, v5

    .line 821410
    invoke-interface {v3, v4, v5}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v3

    .line 821411
    if-ltz v3, :cond_0

    if-nez v3, :cond_4

    .line 821412
    iget-object v3, p1, LX/4xo;->lowerBoundType:LX/4xG;

    move-object v3, v3

    .line 821413
    sget-object v4, LX/4xG;->OPEN:LX/4xG;

    if-eq v3, v4, :cond_0

    :cond_4
    move-object v9, v1

    move-object v1, v2

    move v2, v0

    move-object v0, v9

    goto :goto_0

    .line 821414
    :cond_5
    iget-boolean v4, p1, LX/4xo;->hasUpperBound:Z

    move v4, v4

    .line 821415
    if-eqz v4, :cond_6

    .line 821416
    iget-object v4, p0, LX/4xo;->comparator:Ljava/util/Comparator;

    .line 821417
    iget-object v5, p0, LX/4xo;->upperEndpoint:Ljava/lang/Object;

    move-object v5, v5

    .line 821418
    iget-object v8, p1, LX/4xo;->upperEndpoint:Ljava/lang/Object;

    move-object v8, v8

    .line 821419
    invoke-interface {v4, v5, v8}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v4

    .line 821420
    if-gtz v4, :cond_1

    if-nez v4, :cond_6

    .line 821421
    iget-object v4, p1, LX/4xo;->upperBoundType:LX/4xG;

    move-object v4, v4

    .line 821422
    sget-object v5, LX/4xG;->OPEN:LX/4xG;

    if-eq v4, v5, :cond_1

    :cond_6
    move v5, v3

    goto :goto_1

    :cond_7
    move-object v4, v0

    move-object v3, v1

    goto :goto_2
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 6
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 821343
    iget-boolean v0, p0, LX/4xo;->hasLowerBound:Z

    move v0, v0

    .line 821344
    if-nez v0, :cond_0

    .line 821345
    :goto_0
    return v2

    .line 821346
    :cond_0
    iget-object v0, p0, LX/4xo;->lowerEndpoint:Ljava/lang/Object;

    move-object v0, v0

    .line 821347
    iget-object v3, p0, LX/4xo;->comparator:Ljava/util/Comparator;

    invoke-interface {v3, p1, v0}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    .line 821348
    if-gez v0, :cond_1

    move v3, v1

    :goto_1
    if-nez v0, :cond_2

    move v0, v1

    .line 821349
    :goto_2
    iget-object v4, p0, LX/4xo;->lowerBoundType:LX/4xG;

    move-object v4, v4

    .line 821350
    sget-object v5, LX/4xG;->OPEN:LX/4xG;

    if-ne v4, v5, :cond_3

    :goto_3
    and-int/2addr v0, v1

    or-int v2, v3, v0

    goto :goto_0

    :cond_1
    move v3, v2

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move v1, v2

    goto :goto_3
.end method

.method public final b(Ljava/lang/Object;)Z
    .locals 6
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 821351
    iget-boolean v0, p0, LX/4xo;->hasUpperBound:Z

    move v0, v0

    .line 821352
    if-nez v0, :cond_0

    .line 821353
    :goto_0
    return v2

    .line 821354
    :cond_0
    iget-object v0, p0, LX/4xo;->upperEndpoint:Ljava/lang/Object;

    move-object v0, v0

    .line 821355
    iget-object v3, p0, LX/4xo;->comparator:Ljava/util/Comparator;

    invoke-interface {v3, p1, v0}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    .line 821356
    if-lez v0, :cond_1

    move v3, v1

    :goto_1
    if-nez v0, :cond_2

    move v0, v1

    .line 821357
    :goto_2
    iget-object v4, p0, LX/4xo;->upperBoundType:LX/4xG;

    move-object v4, v4

    .line 821358
    sget-object v5, LX/4xG;->OPEN:LX/4xG;

    if-ne v4, v5, :cond_3

    :goto_3
    and-int/2addr v0, v1

    or-int v2, v3, v0

    goto :goto_0

    :cond_1
    move v3, v2

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move v1, v2

    goto :goto_3
.end method

.method public final c(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    .prologue
    .line 821342
    invoke-virtual {p0, p1}, LX/4xo;->a(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, LX/4xo;->b(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 821326
    instance-of v1, p1, LX/4xo;

    if-eqz v1, :cond_0

    .line 821327
    check-cast p1, LX/4xo;

    .line 821328
    iget-object v1, p0, LX/4xo;->comparator:Ljava/util/Comparator;

    iget-object v2, p1, LX/4xo;->comparator:Ljava/util/Comparator;

    invoke-interface {v1, v2}, Ljava/util/Comparator;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, LX/4xo;->hasLowerBound:Z

    iget-boolean v2, p1, LX/4xo;->hasLowerBound:Z

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, LX/4xo;->hasUpperBound:Z

    iget-boolean v2, p1, LX/4xo;->hasUpperBound:Z

    if-ne v1, v2, :cond_0

    .line 821329
    iget-object v1, p0, LX/4xo;->lowerBoundType:LX/4xG;

    move-object v1, v1

    .line 821330
    iget-object v2, p1, LX/4xo;->lowerBoundType:LX/4xG;

    move-object v2, v2

    .line 821331
    invoke-virtual {v1, v2}, LX/4xG;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 821332
    iget-object v1, p0, LX/4xo;->upperBoundType:LX/4xG;

    move-object v1, v1

    .line 821333
    iget-object v2, p1, LX/4xo;->upperBoundType:LX/4xG;

    move-object v2, v2

    .line 821334
    invoke-virtual {v1, v2}, LX/4xG;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 821335
    iget-object v1, p0, LX/4xo;->lowerEndpoint:Ljava/lang/Object;

    move-object v1, v1

    .line 821336
    iget-object v2, p1, LX/4xo;->lowerEndpoint:Ljava/lang/Object;

    move-object v2, v2

    .line 821337
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 821338
    iget-object v1, p0, LX/4xo;->upperEndpoint:Ljava/lang/Object;

    move-object v1, v1

    .line 821339
    iget-object v2, p1, LX/4xo;->upperEndpoint:Ljava/lang/Object;

    move-object v2, v2

    .line 821340
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 821341
    :cond_0
    return v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 821317
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LX/4xo;->comparator:Ljava/util/Comparator;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 821318
    iget-object v2, p0, LX/4xo;->lowerEndpoint:Ljava/lang/Object;

    move-object v2, v2

    .line 821319
    aput-object v2, v0, v1

    const/4 v1, 0x2

    .line 821320
    iget-object v2, p0, LX/4xo;->lowerBoundType:LX/4xG;

    move-object v2, v2

    .line 821321
    aput-object v2, v0, v1

    const/4 v1, 0x3

    .line 821322
    iget-object v2, p0, LX/4xo;->upperEndpoint:Ljava/lang/Object;

    move-object v2, v2

    .line 821323
    aput-object v2, v0, v1

    const/4 v1, 0x4

    .line 821324
    iget-object v2, p0, LX/4xo;->upperBoundType:LX/4xG;

    move-object v2, v2

    .line 821325
    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 821316
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, LX/4xo;->comparator:Ljava/util/Comparator;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, LX/4xo;->lowerBoundType:LX/4xG;

    sget-object v2, LX/4xG;->CLOSED:LX/4xG;

    if-ne v0, v2, :cond_0

    const/16 v0, 0x5b

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v0, p0, LX/4xo;->hasLowerBound:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/4xo;->lowerEndpoint:Ljava/lang/Object;

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x2c

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v0, p0, LX/4xo;->hasUpperBound:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/4xo;->upperEndpoint:Ljava/lang/Object;

    :goto_2
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, LX/4xo;->upperBoundType:LX/4xG;

    sget-object v2, LX/4xG;->CLOSED:LX/4xG;

    if-ne v0, v2, :cond_3

    const/16 v0, 0x5d

    :goto_3
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/16 v0, 0x28

    goto :goto_0

    :cond_1
    const-string v0, "-\u221e"

    goto :goto_1

    :cond_2
    const-string v0, "\u221e"

    goto :goto_2

    :cond_3
    const/16 v0, 0x29

    goto :goto_3
.end method
