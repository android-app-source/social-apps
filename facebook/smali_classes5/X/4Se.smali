.class public LX/4Se;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 715039
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 25

    .prologue
    .line 715040
    const/16 v18, 0x0

    .line 715041
    const/16 v17, 0x0

    .line 715042
    const/16 v16, 0x0

    .line 715043
    const-wide/16 v14, 0x0

    .line 715044
    const/4 v13, 0x0

    .line 715045
    const/4 v12, 0x0

    .line 715046
    const/4 v11, 0x0

    .line 715047
    const/4 v10, 0x0

    .line 715048
    const/4 v9, 0x0

    .line 715049
    const/4 v8, 0x0

    .line 715050
    const/4 v7, 0x0

    .line 715051
    const/4 v6, 0x0

    .line 715052
    const/4 v5, 0x0

    .line 715053
    const/4 v4, 0x0

    .line 715054
    const/4 v3, 0x0

    .line 715055
    const/4 v2, 0x0

    .line 715056
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v19

    sget-object v20, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    if-eq v0, v1, :cond_12

    .line 715057
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 715058
    const/4 v2, 0x0

    .line 715059
    :goto_0
    return v2

    .line 715060
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_f

    .line 715061
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 715062
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 715063
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v21, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v21

    if-eq v6, v0, :cond_0

    if-eqz v2, :cond_0

    .line 715064
    const-string v6, "action_links"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 715065
    invoke-static/range {p0 .. p1}, LX/2bb;->b(LX/15w;LX/186;)I

    move-result v2

    move/from16 v20, v2

    goto :goto_1

    .line 715066
    :cond_1
    const-string v6, "cache_id"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 715067
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v19, v2

    goto :goto_1

    .line 715068
    :cond_2
    const-string v6, "debug_info"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 715069
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v7, v2

    goto :goto_1

    .line 715070
    :cond_3
    const-string v6, "fetchTimeMs"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 715071
    const/4 v2, 0x1

    .line 715072
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 715073
    :cond_4
    const-string v6, "local_last_negative_feedback_action_type"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 715074
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v18, v2

    goto :goto_1

    .line 715075
    :cond_5
    const-string v6, "local_story_visibility"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 715076
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v17, v2

    goto/16 :goto_1

    .line 715077
    :cond_6
    const-string v6, "local_story_visible_height"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 715078
    const/4 v2, 0x1

    .line 715079
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move v8, v2

    move/from16 v16, v6

    goto/16 :goto_1

    .line 715080
    :cond_7
    const-string v6, "savedActionLinks"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 715081
    invoke-static/range {p0 .. p1}, LX/2bb;->b(LX/15w;LX/186;)I

    move-result v2

    move v15, v2

    goto/16 :goto_1

    .line 715082
    :cond_8
    const-string v6, "savedCollection"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 715083
    invoke-static/range {p0 .. p1}, LX/39K;->a(LX/15w;LX/186;)I

    move-result v2

    move v14, v2

    goto/16 :goto_1

    .line 715084
    :cond_9
    const-string v6, "savedItems"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 715085
    invoke-static/range {p0 .. p1}, LX/4Sf;->a(LX/15w;LX/186;)I

    move-result v2

    move v13, v2

    goto/16 :goto_1

    .line 715086
    :cond_a
    const-string v6, "savedTitle"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 715087
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v2

    move v12, v2

    goto/16 :goto_1

    .line 715088
    :cond_b
    const-string v6, "short_term_cache_key"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 715089
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v11, v2

    goto/16 :goto_1

    .line 715090
    :cond_c
    const-string v6, "title"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_d

    .line 715091
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v2

    move v10, v2

    goto/16 :goto_1

    .line 715092
    :cond_d
    const-string v6, "tracking"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 715093
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v9, v2

    goto/16 :goto_1

    .line 715094
    :cond_e
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 715095
    :cond_f
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 715096
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 715097
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 715098
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 715099
    if-eqz v3, :cond_10

    .line 715100
    const/4 v3, 0x3

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 715101
    :cond_10
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 715102
    const/4 v2, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 715103
    if-eqz v8, :cond_11

    .line 715104
    const/4 v2, 0x6

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 715105
    :cond_11
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 715106
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 715107
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 715108
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 715109
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 715110
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 715111
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 715112
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_12
    move/from16 v19, v17

    move/from16 v20, v18

    move/from16 v17, v12

    move/from16 v18, v13

    move v12, v7

    move v13, v8

    move/from16 v7, v16

    move v8, v2

    move/from16 v16, v11

    move v11, v6

    move/from16 v22, v5

    move-wide/from16 v23, v14

    move v14, v9

    move v15, v10

    move/from16 v10, v22

    move v9, v4

    move-wide/from16 v4, v23

    goto/16 :goto_1
.end method

.method public static a(LX/15w;S)LX/15i;
    .locals 5

    .prologue
    .line 715113
    const/4 v0, 0x1

    const/4 v4, 0x0

    .line 715114
    new-instance v2, LX/186;

    const/16 v1, 0x80

    invoke-direct {v2, v1}, LX/186;-><init>(I)V

    .line 715115
    invoke-static {p0, v2}, LX/4Se;->a(LX/15w;LX/186;)I

    move-result v1

    .line 715116
    if-eqz v0, :cond_0

    .line 715117
    const/4 v3, 0x2

    invoke-virtual {v2, v3}, LX/186;->c(I)V

    .line 715118
    invoke-virtual {v2, v4, p1, v4}, LX/186;->a(ISI)V

    .line 715119
    const/4 v3, 0x1

    invoke-virtual {v2, v3, v1}, LX/186;->b(II)V

    .line 715120
    invoke-virtual {v2}, LX/186;->d()I

    move-result v1

    .line 715121
    :cond_0
    invoke-virtual {v2, v1}, LX/186;->d(I)V

    .line 715122
    invoke-static {v2}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v1

    move-object v0, v1

    .line 715123
    return-object v0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    .line 715124
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 715125
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 715126
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 715127
    const-string v0, "name"

    const-string v1, "SavedCollectionFeedUnit"

    invoke-virtual {p2, v0, v1}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 715128
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 715129
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 715130
    if-eqz v0, :cond_0

    .line 715131
    const-string v1, "action_links"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 715132
    invoke-static {p0, v0, p2, p3}, LX/2bb;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 715133
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 715134
    if-eqz v0, :cond_1

    .line 715135
    const-string v1, "cache_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 715136
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 715137
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 715138
    if-eqz v0, :cond_2

    .line 715139
    const-string v1, "debug_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 715140
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 715141
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 715142
    cmp-long v2, v0, v4

    if-eqz v2, :cond_3

    .line 715143
    const-string v2, "fetchTimeMs"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 715144
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 715145
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 715146
    if-eqz v0, :cond_4

    .line 715147
    const-string v1, "local_last_negative_feedback_action_type"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 715148
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 715149
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 715150
    if-eqz v0, :cond_5

    .line 715151
    const-string v1, "local_story_visibility"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 715152
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 715153
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 715154
    if-eqz v0, :cond_6

    .line 715155
    const-string v1, "local_story_visible_height"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 715156
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 715157
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 715158
    if-eqz v0, :cond_7

    .line 715159
    const-string v1, "savedActionLinks"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 715160
    invoke-static {p0, v0, p2, p3}, LX/2bb;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 715161
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 715162
    if-eqz v0, :cond_8

    .line 715163
    const-string v1, "savedCollection"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 715164
    invoke-static {p0, v0, p2, p3}, LX/39K;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 715165
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 715166
    if-eqz v0, :cond_a

    .line 715167
    const-string v1, "savedItems"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 715168
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 715169
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_9

    .line 715170
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/4Sf;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 715171
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 715172
    :cond_9
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 715173
    :cond_a
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 715174
    if-eqz v0, :cond_b

    .line 715175
    const-string v1, "savedTitle"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 715176
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 715177
    :cond_b
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 715178
    if-eqz v0, :cond_c

    .line 715179
    const-string v1, "short_term_cache_key"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 715180
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 715181
    :cond_c
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 715182
    if-eqz v0, :cond_d

    .line 715183
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 715184
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 715185
    :cond_d
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 715186
    if-eqz v0, :cond_e

    .line 715187
    const-string v1, "tracking"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 715188
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 715189
    :cond_e
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 715190
    return-void
.end method
