.class public final LX/4rH;
.super LX/0mE;
.source ""


# static fields
.field public static final a:LX/4rH;


# instance fields
.field public final b:[B


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 815417
    new-instance v0, LX/4rH;

    const/4 v1, 0x0

    new-array v1, v1, [B

    invoke-direct {v0, v1}, LX/4rH;-><init>([B)V

    sput-object v0, LX/4rH;->a:LX/4rH;

    return-void
.end method

.method private constructor <init>([B)V
    .locals 0

    .prologue
    .line 815414
    invoke-direct {p0}, LX/0mE;-><init>()V

    .line 815415
    iput-object p1, p0, LX/4rH;->b:[B

    .line 815416
    return-void
.end method

.method public static a([B)LX/4rH;
    .locals 1

    .prologue
    .line 815408
    if-nez p0, :cond_0

    .line 815409
    const/4 v0, 0x0

    .line 815410
    :goto_0
    return-object v0

    .line 815411
    :cond_0
    array-length v0, p0

    if-nez v0, :cond_1

    .line 815412
    sget-object v0, LX/4rH;->a:LX/4rH;

    goto :goto_0

    .line 815413
    :cond_1
    new-instance v0, LX/4rH;

    invoke-direct {v0, p0}, LX/4rH;-><init>([B)V

    goto :goto_0
.end method


# virtual methods
.method public final B()Ljava/lang/String;
    .locals 3

    .prologue
    .line 815406
    sget-object v0, LX/0lm;->b:LX/0ln;

    move-object v0, v0

    .line 815407
    iget-object v1, p0, LX/4rH;->b:[B

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0ln;->a([BZ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a()LX/15z;
    .locals 1

    .prologue
    .line 815405
    sget-object v0, LX/15z;->VALUE_EMBEDDED_OBJECT:LX/15z;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 815418
    if-ne p1, p0, :cond_1

    const/4 v0, 0x1

    .line 815419
    :cond_0
    :goto_0
    return v0

    .line 815420
    :cond_1
    if-eqz p1, :cond_0

    .line 815421
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-ne v1, v2, :cond_0

    .line 815422
    check-cast p1, LX/4rH;

    iget-object v0, p1, LX/4rH;->b:[B

    iget-object v1, p0, LX/4rH;->b:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 815404
    iget-object v0, p0, LX/4rH;->b:[B

    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/4rH;->b:[B

    array-length v0, v0

    goto :goto_0
.end method

.method public final k()LX/0nH;
    .locals 1

    .prologue
    .line 815403
    sget-object v0, LX/0nH;->BINARY:LX/0nH;

    return-object v0
.end method

.method public final serialize(LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 815400
    iget-object v0, p2, LX/0my;->_config:LX/0m2;

    move-object v0, v0

    .line 815401
    invoke-virtual {v0}, LX/0m4;->r()LX/0ln;

    move-result-object v0

    iget-object v1, p0, LX/4rH;->b:[B

    const/4 v2, 0x0

    iget-object v3, p0, LX/4rH;->b:[B

    array-length v3, v3

    invoke-virtual {p1, v0, v1, v2, v3}, LX/0nX;->a(LX/0ln;[BII)V

    .line 815402
    return-void
.end method

.method public final t()[B
    .locals 1

    .prologue
    .line 815399
    iget-object v0, p0, LX/4rH;->b:[B

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 815397
    sget-object v0, LX/0lm;->b:LX/0ln;

    move-object v0, v0

    .line 815398
    iget-object v1, p0, LX/4rH;->b:[B

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LX/0ln;->a([BZ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
