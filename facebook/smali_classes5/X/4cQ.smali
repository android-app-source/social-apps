.class public LX/4cQ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:LX/4cR;

.field public final c:LX/4cO;


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/4cO;)V
    .locals 2

    .prologue
    .line 794973
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 794974
    if-nez p1, :cond_0

    .line 794975
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Name may not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 794976
    :cond_0
    if-nez p2, :cond_1

    .line 794977
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Body may not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 794978
    :cond_1
    iput-object p1, p0, LX/4cQ;->a:Ljava/lang/String;

    .line 794979
    iput-object p2, p0, LX/4cQ;->c:LX/4cO;

    .line 794980
    new-instance v0, LX/4cR;

    invoke-direct {v0}, LX/4cR;-><init>()V

    iput-object v0, p0, LX/4cQ;->b:LX/4cR;

    .line 794981
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 794982
    const-string v1, "form-data; name=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 794983
    iget-object v1, p0, LX/4cQ;->a:Ljava/lang/String;

    move-object v1, v1

    .line 794984
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 794985
    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 794986
    invoke-virtual {p2}, LX/4cO;->a()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 794987
    const-string v1, "; filename=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 794988
    invoke-virtual {p2}, LX/4cO;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 794989
    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 794990
    :cond_2
    const-string v1, "Content-Disposition"

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v1, v0}, LX/4cQ;->a(LX/4cQ;Ljava/lang/String;Ljava/lang/String;)V

    .line 794991
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 794992
    iget-object v1, p2, LX/4cO;->a:Ljava/lang/String;

    move-object v1, v1

    .line 794993
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 794994
    invoke-virtual {p2}, LX/4cO;->b()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 794995
    const-string v1, "; charset="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 794996
    invoke-virtual {p2}, LX/4cO;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 794997
    :cond_3
    const-string v1, "Content-Type"

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v1, v0}, LX/4cQ;->a(LX/4cQ;Ljava/lang/String;Ljava/lang/String;)V

    .line 794998
    const-string v0, "Content-Transfer-Encoding"

    invoke-virtual {p2}, LX/4cO;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1}, LX/4cQ;->a(LX/4cQ;Ljava/lang/String;Ljava/lang/String;)V

    .line 794999
    return-void
.end method

.method public static a(LX/4cQ;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 795000
    if-nez p1, :cond_0

    .line 795001
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Field name may not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 795002
    :cond_0
    iget-object v0, p0, LX/4cQ;->b:LX/4cR;

    new-instance v1, LX/4cX;

    invoke-direct {v1, p1, p2}, LX/4cX;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 795003
    if-nez v1, :cond_1

    .line 795004
    :goto_0
    return-void

    .line 795005
    :cond_1
    iget-object p0, v1, LX/4cX;->a:Ljava/lang/String;

    move-object p0, p0

    .line 795006
    sget-object p1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, p1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    .line 795007
    iget-object p0, v0, LX/4cR;->b:Ljava/util/Map;

    invoke-interface {p0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/util/List;

    .line 795008
    if-nez p0, :cond_2

    .line 795009
    new-instance p0, Ljava/util/LinkedList;

    invoke-direct {p0}, Ljava/util/LinkedList;-><init>()V

    .line 795010
    iget-object p2, v0, LX/4cR;->b:Ljava/util/Map;

    invoke-interface {p2, p1, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 795011
    :cond_2
    invoke-interface {p0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 795012
    iget-object p0, v0, LX/4cR;->a:Ljava/util/List;

    invoke-interface {p0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
