.class public LX/4bF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lorg/apache/http/HttpEntity;


# instance fields
.field private final a:LX/4bJ;

.field private final b:Lorg/apache/http/HttpEntity;

.field private c:LX/4bI;


# direct methods
.method public constructor <init>(LX/4bJ;Lorg/apache/http/HttpEntity;)V
    .locals 1
    .param p2    # Lorg/apache/http/HttpEntity;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 793493
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 793494
    invoke-interface {p2}, Lorg/apache/http/HttpEntity;->isRepeatable()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 793495
    iput-object p1, p0, LX/4bF;->a:LX/4bJ;

    .line 793496
    iput-object p2, p0, LX/4bF;->b:Lorg/apache/http/HttpEntity;

    .line 793497
    return-void

    .line 793498
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a()J
    .locals 4

    .prologue
    .line 793499
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/4bF;->c:LX/4bI;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const-wide/16 v0, -0x1

    :goto_0
    monitor-exit p0

    return-wide v0

    :cond_0
    :try_start_1
    iget-object v0, p0, LX/4bF;->c:LX/4bI;

    .line 793500
    iget-wide v2, v0, LX/4bI;->c:J

    move-wide v0, v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 793501
    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final consumeContent()V
    .locals 1

    .prologue
    .line 793502
    iget-object v0, p0, LX/4bF;->b:Lorg/apache/http/HttpEntity;

    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->consumeContent()V

    .line 793503
    return-void
.end method

.method public final declared-synchronized getContent()Ljava/io/InputStream;
    .locals 4

    .prologue
    .line 793505
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/4bF;->c:LX/4bI;

    if-nez v0, :cond_0

    .line 793506
    iget-object v0, p0, LX/4bF;->a:LX/4bJ;

    iget-object v1, p0, LX/4bF;->b:Lorg/apache/http/HttpEntity;

    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v1

    .line 793507
    new-instance v3, LX/4bI;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v2

    check-cast v2, LX/0So;

    invoke-direct {v3, v2, v1}, LX/4bI;-><init>(LX/0So;Ljava/io/InputStream;)V

    .line 793508
    move-object v0, v3

    .line 793509
    iput-object v0, p0, LX/4bF;->c:LX/4bI;

    .line 793510
    :cond_0
    iget-object v0, p0, LX/4bF;->c:LX/4bI;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 793511
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final getContentEncoding()Lorg/apache/http/Header;
    .locals 1

    .prologue
    .line 793504
    iget-object v0, p0, LX/4bF;->b:Lorg/apache/http/HttpEntity;

    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContentEncoding()Lorg/apache/http/Header;

    move-result-object v0

    return-object v0
.end method

.method public final getContentLength()J
    .locals 2

    .prologue
    .line 793492
    iget-object v0, p0, LX/4bF;->b:Lorg/apache/http/HttpEntity;

    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v0

    return-wide v0
.end method

.method public final getContentType()Lorg/apache/http/Header;
    .locals 1

    .prologue
    .line 793486
    iget-object v0, p0, LX/4bF;->b:Lorg/apache/http/HttpEntity;

    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContentType()Lorg/apache/http/Header;

    move-result-object v0

    return-object v0
.end method

.method public final isChunked()Z
    .locals 1

    .prologue
    .line 793491
    iget-object v0, p0, LX/4bF;->b:Lorg/apache/http/HttpEntity;

    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->isChunked()Z

    move-result v0

    return v0
.end method

.method public final isRepeatable()Z
    .locals 1

    .prologue
    .line 793490
    iget-object v0, p0, LX/4bF;->b:Lorg/apache/http/HttpEntity;

    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->isRepeatable()Z

    move-result v0

    return v0
.end method

.method public final isStreaming()Z
    .locals 1

    .prologue
    .line 793489
    iget-object v0, p0, LX/4bF;->b:Lorg/apache/http/HttpEntity;

    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->isStreaming()Z

    move-result v0

    return v0
.end method

.method public final writeTo(Ljava/io/OutputStream;)V
    .locals 1

    .prologue
    .line 793487
    iget-object v0, p0, LX/4bF;->b:Lorg/apache/http/HttpEntity;

    invoke-interface {v0, p1}, Lorg/apache/http/HttpEntity;->writeTo(Ljava/io/OutputStream;)V

    .line 793488
    return-void
.end method
