.class public LX/4f1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4ey;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/4ey",
        "<",
        "LX/1FL;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/concurrent/Executor;

.field public final b:LX/1Fj;

.field public final c:Landroid/content/ContentResolver;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;LX/1Fj;Landroid/content/ContentResolver;)V
    .locals 0

    .prologue
    .line 797858
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 797859
    iput-object p1, p0, LX/4f1;->a:Ljava/util/concurrent/Executor;

    .line 797860
    iput-object p2, p0, LX/4f1;->b:LX/1Fj;

    .line 797861
    iput-object p3, p0, LX/4f1;->c:Landroid/content/ContentResolver;

    .line 797862
    return-void
.end method

.method public static a(LX/1FK;Landroid/media/ExifInterface;)LX/1FL;
    .locals 5

    .prologue
    const/4 v1, -0x1

    .line 797864
    new-instance v0, LX/1lZ;

    invoke-direct {v0, p0}, LX/1lZ;-><init>(LX/1FK;)V

    invoke-static {v0}, LX/1le;->a(Ljava/io/InputStream;)Landroid/util/Pair;

    move-result-object v3

    .line 797865
    const-string v0, "Orientation"

    invoke-virtual {p1, v0}, Landroid/media/ExifInterface;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, LX/1li;->a(I)I

    move-result v0

    move v4, v0

    .line 797866
    if-eqz v3, :cond_0

    iget-object v0, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    move v2, v0

    .line 797867
    :goto_0
    if-eqz v3, :cond_1

    iget-object v0, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 797868
    :goto_1
    invoke-static {p0}, LX/1FJ;->a(Ljava/io/Closeable;)LX/1FJ;

    move-result-object v1

    .line 797869
    :try_start_0
    new-instance v3, LX/1FL;

    invoke-direct {v3, v1}, LX/1FL;-><init>(LX/1FJ;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 797870
    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    .line 797871
    sget-object v1, LX/1ld;->a:LX/1lW;

    .line 797872
    iput-object v1, v3, LX/1FL;->c:LX/1lW;

    .line 797873
    iput v4, v3, LX/1FL;->d:I

    .line 797874
    iput v2, v3, LX/1FL;->e:I

    .line 797875
    iput v0, v3, LX/1FL;->f:I

    .line 797876
    return-object v3

    :cond_0
    move v2, v1

    .line 797877
    goto :goto_0

    :cond_1
    move v0, v1

    .line 797878
    goto :goto_1

    .line 797879
    :catchall_0
    move-exception v0

    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    throw v0
.end method


# virtual methods
.method public final a(LX/1cd;LX/1cW;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/imagepipeline/producers/Consumer",
            "<",
            "LX/1FL;",
            ">;",
            "Lcom/facebook/imagepipeline/producers/ProducerContext;",
            ")V"
        }
    .end annotation

    .prologue
    .line 797880
    iget-object v0, p2, LX/1cW;->c:LX/1BV;

    move-object v3, v0

    .line 797881
    iget-object v0, p2, LX/1cW;->b:Ljava/lang/String;

    move-object v5, v0

    .line 797882
    iget-object v0, p2, LX/1cW;->a:LX/1bf;

    move-object v6, v0

    .line 797883
    new-instance v0, Lcom/facebook/imagepipeline/producers/LocalExifThumbnailProducer$1;

    const-string v4, "LocalExifThumbnailProducer"

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v6}, Lcom/facebook/imagepipeline/producers/LocalExifThumbnailProducer$1;-><init>(LX/4f1;LX/1cd;LX/1BV;Ljava/lang/String;Ljava/lang/String;LX/1bf;)V

    .line 797884
    new-instance v1, LX/4f0;

    invoke-direct {v1, p0, v0}, LX/4f0;-><init>(LX/4f1;Lcom/facebook/imagepipeline/producers/StatefulProducerRunnable;)V

    invoke-virtual {p2, v1}, LX/1cW;->a(LX/1cg;)V

    .line 797885
    iget-object v1, p0, LX/4f1;->a:Ljava/util/concurrent/Executor;

    const v2, -0xb00279a

    invoke-static {v1, v0, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 797886
    return-void
.end method

.method public final a(LX/1o9;)Z
    .locals 1

    .prologue
    const/16 v0, 0x200

    .line 797863
    invoke-static {v0, v0, p1}, LX/4fN;->a(IILX/1o9;)Z

    move-result v0

    return v0
.end method
