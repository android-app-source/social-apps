.class public LX/4CQ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:LX/0Zh;


# instance fields
.field private final c:Ljava/io/File;

.field private final d:LX/0SG;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 678842
    const-class v0, LX/4CQ;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/4CQ;->a:Ljava/lang/String;

    .line 678843
    invoke-static {}, LX/0Zh;->a()LX/0Zh;

    move-result-object v0

    sput-object v0, LX/4CQ;->b:LX/0Zh;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/00G;LX/0SG;)V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 678832
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 678833
    iget-object v0, p2, LX/00G;->b:Ljava/lang/String;

    move-object v0, v0

    .line 678834
    if-eqz v0, :cond_0

    .line 678835
    :goto_0
    new-instance v1, Ljava/io/File;

    const-string v2, "funnel_analytics"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 678836
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    .line 678837
    move-object v0, v1

    .line 678838
    iput-object v0, p0, LX/4CQ;->c:Ljava/io/File;

    .line 678839
    iput-object p3, p0, LX/4CQ;->d:LX/0SG;

    .line 678840
    return-void

    .line 678841
    :cond_0
    const-string v0, "default"

    goto :goto_0
.end method
