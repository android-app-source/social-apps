.class public LX/47e;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0Rh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rh",
            "<",
            "LX/47d;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:LX/0Rh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rh",
            "<",
            "Ljava/lang/Integer;",
            "LX/47d;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 672382
    invoke-static {}, LX/0Rh;->d()LX/4y1;

    move-result-object v0

    sget-object v1, LX/47d;->NORMAL:LX/47d;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4y1;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/4y1;

    move-result-object v0

    sget-object v1, LX/47d;->ROTATE_90:LX/47d;

    const/16 v2, 0x5a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4y1;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/4y1;

    move-result-object v0

    sget-object v1, LX/47d;->ROTATE_180:LX/47d;

    const/16 v2, 0xb4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4y1;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/4y1;

    move-result-object v0

    sget-object v1, LX/47d;->ROTATE_270:LX/47d;

    const/16 v2, 0x10e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4y1;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/4y1;

    move-result-object v0

    invoke-virtual {v0}, LX/4y1;->a()LX/0Rh;

    move-result-object v0

    .line 672383
    sput-object v0, LX/47e;->a:LX/0Rh;

    invoke-virtual {v0}, LX/0Rh;->e()LX/0Rh;

    move-result-object v0

    sput-object v0, LX/47e;->b:LX/0Rh;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 672384
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/47d;)I
    .locals 2

    .prologue
    .line 672385
    const/4 v0, 0x0

    .line 672386
    sget-object v1, LX/47e;->a:LX/0Rh;

    invoke-virtual {v1, p0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 672387
    if-nez v1, :cond_0

    :goto_0
    move v0, v0

    .line 672388
    return v0

    :cond_0
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public static a(I)LX/47d;
    .locals 3

    .prologue
    .line 672389
    sget-object v0, LX/47d;->NORMAL:LX/47d;

    .line 672390
    sget-object v1, LX/47e;->b:LX/0Rh;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/47d;

    .line 672391
    if-nez v1, :cond_0

    :goto_0
    move-object v0, v0

    .line 672392
    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method
