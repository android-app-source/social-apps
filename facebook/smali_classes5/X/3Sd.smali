.class public LX/3Sd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# static fields
.field public static final a:LX/3Sd;


# instance fields
.field public final b:I

.field private final c:[LX/15i;

.field private final d:[I

.field private final e:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 582895
    new-instance v0, LX/3Sd;

    new-array v1, v3, [LX/15i;

    new-array v2, v3, [I

    new-array v3, v3, [I

    invoke-direct {v0, v1, v2, v3}, LX/3Sd;-><init>([LX/15i;[I[I)V

    sput-object v0, LX/3Sd;->a:LX/3Sd;

    return-void
.end method

.method private constructor <init>([LX/15i;[I[I)V
    .locals 1

    .prologue
    .line 582889
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 582890
    array-length v0, p2

    iput v0, p0, LX/3Sd;->b:I

    .line 582891
    iput-object p1, p0, LX/3Sd;->c:[LX/15i;

    .line 582892
    iput-object p2, p0, LX/3Sd;->d:[I

    .line 582893
    iput-object p3, p0, LX/3Sd;->e:[I

    .line 582894
    return-void
.end method

.method public static a(I)LX/3Sd;
    .locals 4

    .prologue
    .line 582886
    if-lez p0, :cond_0

    .line 582887
    new-instance v0, LX/3Sd;

    new-array v1, p0, [LX/15i;

    new-array v2, p0, [I

    new-array v3, p0, [I

    invoke-direct {v0, v1, v2, v3}, LX/3Sd;-><init>([LX/15i;[I[I)V

    .line 582888
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/3Sd;->a:LX/3Sd;

    goto :goto_0
.end method

.method public static a(LX/3Sd;ILX/3Sd;II)V
    .locals 2

    .prologue
    .line 582882
    iget-object v0, p0, LX/3Sd;->c:[LX/15i;

    iget-object v1, p2, LX/3Sd;->c:[LX/15i;

    invoke-static {v0, p1, v1, p3, p4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 582883
    iget-object v0, p0, LX/3Sd;->d:[I

    iget-object v1, p2, LX/3Sd;->d:[I

    invoke-static {v0, p1, v1, p3, p4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 582884
    iget-object v0, p0, LX/3Sd;->e:[I

    iget-object v1, p2, LX/3Sd;->e:[I

    invoke-static {v0, p1, v1, p3, p4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 582885
    return-void
.end method


# virtual methods
.method public final a(II)I
    .locals 1

    .prologue
    .line 582881
    iget-object v0, p0, LX/3Sd;->d:[I

    aput p2, v0, p1

    return p2
.end method

.method public final a(ILX/15i;)LX/15i;
    .locals 1

    .prologue
    .line 582880
    iget-object v0, p0, LX/3Sd;->c:[LX/15i;

    aput-object p2, v0, p1

    return-object p2
.end method

.method public final a()LX/3Sd;
    .locals 2

    .prologue
    .line 582877
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Sd;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 582878
    :catch_0
    move-exception v0

    .line 582879
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final b(II)I
    .locals 1

    .prologue
    .line 582872
    iget-object v0, p0, LX/3Sd;->e:[I

    aput p2, v0, p1

    return p2
.end method

.method public final b(I)LX/15i;
    .locals 1

    .prologue
    .line 582876
    iget-object v0, p0, LX/3Sd;->c:[LX/15i;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public final c(I)I
    .locals 1

    .prologue
    .line 582875
    iget-object v0, p0, LX/3Sd;->d:[I

    aget v0, v0, p1

    return v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 582874
    invoke-virtual {p0}, LX/3Sd;->a()LX/3Sd;

    move-result-object v0

    return-object v0
.end method

.method public final d(I)I
    .locals 1

    .prologue
    .line 582873
    iget-object v0, p0, LX/3Sd;->e:[I

    aget v0, v0, p1

    return v0
.end method
