.class public LX/4es;
.super Ljava/lang/Object;
.source ""


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 797589
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/1bf;LX/1FL;)I
    .locals 14

    .prologue
    .line 797590
    invoke-static {p1}, LX/1FL;->c(LX/1FL;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 797591
    const/4 v0, 0x1

    .line 797592
    :cond_0
    return v0

    .line 797593
    :cond_1
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 797594
    invoke-static {p1}, LX/1FL;->c(LX/1FL;)Z

    move-result v0

    invoke-static {v0}, LX/03g;->a(Z)V

    .line 797595
    iget-object v0, p0, LX/1bf;->h:LX/1o9;

    move-object v5, v0

    .line 797596
    if-eqz v5, :cond_2

    iget v0, v5, LX/1o9;->b:I

    if-lez v0, :cond_2

    iget v0, v5, LX/1o9;->a:I

    if-lez v0, :cond_2

    .line 797597
    iget v0, p1, LX/1FL;->e:I

    move v0, v0

    .line 797598
    if-eqz v0, :cond_2

    .line 797599
    iget v0, p1, LX/1FL;->f:I

    move v0, v0

    .line 797600
    if-nez v0, :cond_6

    .line 797601
    :cond_2
    const/high16 v0, 0x3f800000    # 1.0f

    .line 797602
    :goto_0
    move v0, v0

    .line 797603
    iget-object v1, p1, LX/1FL;->c:LX/1lW;

    move-object v1, v1

    .line 797604
    sget-object v2, LX/1ld;->a:LX/1lW;

    if-ne v1, v2, :cond_4

    .line 797605
    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    .line 797606
    const v4, 0x3f2aaaab

    cmpl-float v4, v0, v4

    if-lez v4, :cond_e

    .line 797607
    const/4 v4, 0x1

    .line 797608
    :cond_3
    move v0, v4

    .line 797609
    :goto_1
    iget v1, p1, LX/1FL;->f:I

    move v1, v1

    .line 797610
    iget v2, p1, LX/1FL;->e:I

    move v2, v2

    .line 797611
    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 797612
    :goto_2
    div-int v2, v1, v0

    int-to-float v2, v2

    const/high16 v3, 0x45000000    # 2048.0f

    cmpl-float v2, v2, v3

    if-lez v2, :cond_0

    .line 797613
    iget-object v2, p1, LX/1FL;->c:LX/1lW;

    move-object v2, v2

    .line 797614
    sget-object v3, LX/1ld;->a:LX/1lW;

    if-ne v2, v3, :cond_5

    .line 797615
    mul-int/lit8 v0, v0, 0x2

    goto :goto_2

    .line 797616
    :cond_4
    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    .line 797617
    const v4, 0x3f2aaaab

    cmpl-float v4, v0, v4

    if-lez v4, :cond_f

    .line 797618
    const/4 v4, 0x1

    .line 797619
    :goto_3
    move v0, v4

    .line 797620
    goto :goto_1

    .line 797621
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 797622
    :cond_6
    const/4 v0, 0x0

    .line 797623
    iget-object v1, p0, LX/1bf;->i:LX/1bd;

    move-object v1, v1

    .line 797624
    invoke-virtual {v1}, LX/1bd;->d()Z

    move-result v1

    if-nez v1, :cond_b

    .line 797625
    :goto_4
    move v0, v0

    .line 797626
    const/16 v1, 0x5a

    if-eq v0, v1, :cond_7

    const/16 v1, 0x10e

    if-ne v0, v1, :cond_8

    :cond_7
    move v1, v4

    .line 797627
    :goto_5
    if-eqz v1, :cond_9

    .line 797628
    iget v0, p1, LX/1FL;->f:I

    move v0, v0

    .line 797629
    move v2, v0

    .line 797630
    :goto_6
    if-eqz v1, :cond_a

    .line 797631
    iget v0, p1, LX/1FL;->e:I

    move v0, v0

    .line 797632
    :goto_7
    iget v1, v5, LX/1o9;->a:I

    int-to-float v1, v1

    int-to-float v6, v2

    div-float v6, v1, v6

    .line 797633
    iget v1, v5, LX/1o9;->b:I

    int-to-float v1, v1

    int-to-float v7, v0

    div-float v7, v1, v7

    .line 797634
    invoke-static {v6, v7}, Ljava/lang/Math;->max(FF)F

    move-result v1

    .line 797635
    const/16 v8, 0x8

    new-array v8, v8, [Ljava/lang/Object;

    iget v9, v5, LX/1o9;->a:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v3

    iget v3, v5, LX/1o9;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v8, v4

    const/4 v3, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v8, v3

    const/4 v2, 0x3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v8, v2

    const/4 v0, 0x4

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v8, v0

    const/4 v0, 0x5

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v8, v0

    const/4 v0, 0x6

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v8, v0

    const/4 v0, 0x7

    .line 797636
    iget-object v2, p0, LX/1bf;->b:Landroid/net/Uri;

    move-object v2, v2

    .line 797637
    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v8, v0

    move v0, v1

    .line 797638
    goto/16 :goto_0

    :cond_8
    move v1, v3

    .line 797639
    goto :goto_5

    .line 797640
    :cond_9
    iget v0, p1, LX/1FL;->e:I

    move v0, v0

    .line 797641
    move v2, v0

    goto :goto_6

    .line 797642
    :cond_a
    iget v0, p1, LX/1FL;->f:I

    move v0, v0

    .line 797643
    goto :goto_7

    .line 797644
    :cond_b
    iget v1, p1, LX/1FL;->d:I

    move v1, v1

    .line 797645
    if-eqz v1, :cond_c

    const/16 v2, 0x5a

    if-eq v1, v2, :cond_c

    const/16 v2, 0xb4

    if-eq v1, v2, :cond_c

    const/16 v2, 0x10e

    if-ne v1, v2, :cond_d

    :cond_c
    const/4 v0, 0x1

    :cond_d
    invoke-static {v0}, LX/03g;->a(Z)V

    move v0, v1

    .line 797646
    goto/16 :goto_4

    .line 797647
    :cond_e
    const/4 v4, 0x2

    .line 797648
    :goto_8
    mul-int/lit8 v5, v4, 0x2

    int-to-double v6, v5

    div-double v6, v12, v6

    .line 797649
    mul-int/lit8 v5, v4, 0x2

    int-to-double v8, v5

    div-double v8, v12, v8

    const-wide v10, 0x3fd5555560000000L    # 0.3333333432674408

    mul-double/2addr v6, v10

    add-double/2addr v6, v8

    .line 797650
    float-to-double v8, v0

    cmpg-double v5, v6, v8

    if-lez v5, :cond_3

    .line 797651
    mul-int/lit8 v4, v4, 0x2

    .line 797652
    goto :goto_8

    .line 797653
    :cond_f
    const/4 v4, 0x2

    .line 797654
    :goto_9
    int-to-double v6, v4

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v6

    int-to-double v8, v4

    sub-double/2addr v6, v8

    div-double v6, v12, v6

    .line 797655
    int-to-double v8, v4

    div-double v8, v12, v8

    const-wide v10, 0x3fd5555560000000L    # 0.3333333432674408

    mul-double/2addr v6, v10

    add-double/2addr v6, v8

    .line 797656
    float-to-double v8, v0

    cmpg-double v5, v6, v8

    if-gtz v5, :cond_10

    .line 797657
    add-int/lit8 v4, v4, -0x1

    goto/16 :goto_3

    .line 797658
    :cond_10
    add-int/lit8 v4, v4, 0x1

    .line 797659
    goto :goto_9
.end method
