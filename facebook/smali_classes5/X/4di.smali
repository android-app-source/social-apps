.class public LX/4di;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:[S


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 796404
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 796405
    const/16 v0, 0x3c

    new-array v0, v0, [S

    iput-object v0, p0, LX/4di;->a:[S

    .line 796406
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x3c

    .line 796392
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 796393
    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    .line 796394
    rem-long v2, v0, v4

    long-to-int v2, v2

    .line 796395
    div-long/2addr v0, v4

    const-wide/16 v4, 0xff

    and-long/2addr v0, v4

    long-to-int v0, v0

    .line 796396
    iget-object v1, p0, LX/4di;->a:[S

    aget-short v1, v1, v2

    .line 796397
    and-int/lit16 v3, v1, 0xff

    .line 796398
    shr-int/lit8 v1, v1, 0x8

    and-int/lit16 v1, v1, 0xff

    .line 796399
    if-eq v0, v1, :cond_0

    .line 796400
    :goto_0
    shl-int/lit8 v0, v0, 0x8

    or-int/2addr v0, p1

    .line 796401
    iget-object v1, p0, LX/4di;->a:[S

    int-to-short v0, v0

    aput-short v0, v1, v2

    .line 796402
    return-void

    .line 796403
    :cond_0
    add-int/2addr p1, v3

    goto :goto_0
.end method

.method public final b(I)I
    .locals 8

    .prologue
    const-wide/16 v6, 0x3c

    const/4 v0, 0x0

    .line 796380
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 796381
    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    .line 796382
    int-to-long v4, p1

    sub-long v4, v2, v4

    rem-long/2addr v4, v6

    long-to-int v4, v4

    .line 796383
    div-long/2addr v2, v6

    const-wide/16 v6, 0xff

    and-long/2addr v2, v6

    long-to-int v2, v2

    move v1, v0

    .line 796384
    :goto_0
    if-ge v1, p1, :cond_1

    .line 796385
    iget-object v3, p0, LX/4di;->a:[S

    add-int v5, v4, v1

    rem-int/lit8 v5, v5, 0x3c

    aget-short v3, v3, v5

    .line 796386
    and-int/lit16 v5, v3, 0xff

    .line 796387
    shr-int/lit8 v3, v3, 0x8

    and-int/lit16 v3, v3, 0xff

    .line 796388
    if-ne v3, v2, :cond_0

    .line 796389
    add-int/2addr v0, v5

    .line 796390
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 796391
    :cond_1
    return v0
.end method
