.class public final enum LX/4g1;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/4g1;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/4g1;

.field public static final enum DATA_CONTROL_WITHOUT_UPSELL:LX/4g1;

.field public static final enum NO_DATA_CONTROL_NO_UPSELL:LX/4g1;

.field public static final enum UNKOWN:LX/4g1;

.field public static final enum UPSELL_WITHOUT_DATA_CONTROL:LX/4g1;

.field public static final enum UPSELL_WITH_DATA_CONTROL:LX/4g1;

.field public static final enum UPSELL_WITH_SMS:LX/4g1;


# instance fields
.field public final dialogState:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 798959
    new-instance v0, LX/4g1;

    const-string v1, "UNKOWN"

    const-string v2, "UNKOWN"

    invoke-direct {v0, v1, v4, v2}, LX/4g1;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/4g1;->UNKOWN:LX/4g1;

    .line 798960
    new-instance v0, LX/4g1;

    const-string v1, "DATA_CONTROL_WITHOUT_UPSELL"

    const-string v2, "DATA_CONTROL_WITHOUT_UPSELL"

    invoke-direct {v0, v1, v5, v2}, LX/4g1;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/4g1;->DATA_CONTROL_WITHOUT_UPSELL:LX/4g1;

    .line 798961
    new-instance v0, LX/4g1;

    const-string v1, "NO_DATA_CONTROL_NO_UPSELL"

    const-string v2, "NO_DATA_CONTROL_NO_UPSELL"

    invoke-direct {v0, v1, v6, v2}, LX/4g1;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/4g1;->NO_DATA_CONTROL_NO_UPSELL:LX/4g1;

    .line 798962
    new-instance v0, LX/4g1;

    const-string v1, "UPSELL_WITH_DATA_CONTROL"

    const-string v2, "UPSELL_WITH_DATA_CONTROL"

    invoke-direct {v0, v1, v7, v2}, LX/4g1;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/4g1;->UPSELL_WITH_DATA_CONTROL:LX/4g1;

    .line 798963
    new-instance v0, LX/4g1;

    const-string v1, "UPSELL_WITHOUT_DATA_CONTROL"

    const-string v2, "UPSELL_WITHOUT_DATA_CONTROL"

    invoke-direct {v0, v1, v8, v2}, LX/4g1;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/4g1;->UPSELL_WITHOUT_DATA_CONTROL:LX/4g1;

    .line 798964
    new-instance v0, LX/4g1;

    const-string v1, "UPSELL_WITH_SMS"

    const/4 v2, 0x5

    const-string v3, "UPSELL_WITH_SMS"

    invoke-direct {v0, v1, v2, v3}, LX/4g1;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/4g1;->UPSELL_WITH_SMS:LX/4g1;

    .line 798965
    const/4 v0, 0x6

    new-array v0, v0, [LX/4g1;

    sget-object v1, LX/4g1;->UNKOWN:LX/4g1;

    aput-object v1, v0, v4

    sget-object v1, LX/4g1;->DATA_CONTROL_WITHOUT_UPSELL:LX/4g1;

    aput-object v1, v0, v5

    sget-object v1, LX/4g1;->NO_DATA_CONTROL_NO_UPSELL:LX/4g1;

    aput-object v1, v0, v6

    sget-object v1, LX/4g1;->UPSELL_WITH_DATA_CONTROL:LX/4g1;

    aput-object v1, v0, v7

    sget-object v1, LX/4g1;->UPSELL_WITHOUT_DATA_CONTROL:LX/4g1;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/4g1;->UPSELL_WITH_SMS:LX/4g1;

    aput-object v2, v0, v1

    sput-object v0, LX/4g1;->$VALUES:[LX/4g1;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 798956
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 798957
    iput-object p3, p0, LX/4g1;->dialogState:Ljava/lang/String;

    .line 798958
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/4g1;
    .locals 1

    .prologue
    .line 798955
    const-class v0, LX/4g1;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/4g1;

    return-object v0
.end method

.method public static values()[LX/4g1;
    .locals 1

    .prologue
    .line 798954
    sget-object v0, LX/4g1;->$VALUES:[LX/4g1;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/4g1;

    return-object v0
.end method
