.class public LX/4Ru;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 711024
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    .line 710982
    const/4 v8, 0x0

    .line 710983
    const-wide/16 v6, 0x0

    .line 710984
    const/4 v5, 0x0

    .line 710985
    const/4 v4, 0x0

    .line 710986
    const/4 v3, 0x0

    .line 710987
    const/4 v2, 0x0

    .line 710988
    const/4 v1, 0x0

    .line 710989
    const/4 v0, 0x0

    .line 710990
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->START_OBJECT:LX/15z;

    if-eq v9, v10, :cond_a

    .line 710991
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 710992
    const/4 v0, 0x0

    .line 710993
    :goto_0
    return v0

    .line 710994
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v1, v5, :cond_8

    .line 710995
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v1

    .line 710996
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 710997
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v11, :cond_0

    if-eqz v1, :cond_0

    .line 710998
    const-string v5, "debugInfo"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 710999
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    move v4, v1

    goto :goto_1

    .line 711000
    :cond_1
    const-string v5, "fetchTimeMs"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 711001
    const/4 v0, 0x1

    .line 711002
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    goto :goto_1

    .line 711003
    :cond_2
    const-string v5, "hideableToken"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 711004
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    move v10, v1

    goto :goto_1

    .line 711005
    :cond_3
    const-string v5, "id"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 711006
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    move v9, v1

    goto :goto_1

    .line 711007
    :cond_4
    const-string v5, "localStoryVisibility"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 711008
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    move v8, v1

    goto :goto_1

    .line 711009
    :cond_5
    const-string v5, "localStoryVisibleHeight"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 711010
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    move v7, v1

    goto :goto_1

    .line 711011
    :cond_6
    const-string v5, "tracking"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 711012
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    move v6, v1

    goto/16 :goto_1

    .line 711013
    :cond_7
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 711014
    :cond_8
    const/4 v1, 0x7

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 711015
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 711016
    if-eqz v0, :cond_9

    .line 711017
    const/4 v1, 0x1

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 711018
    :cond_9
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 711019
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 711020
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 711021
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 711022
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 711023
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :cond_a
    move v9, v4

    move v10, v5

    move v4, v8

    move v8, v3

    move v12, v2

    move-wide v2, v6

    move v7, v12

    move v6, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15w;S)LX/15i;
    .locals 5

    .prologue
    .line 710971
    const/4 v0, 0x1

    const/4 v4, 0x0

    .line 710972
    new-instance v2, LX/186;

    const/16 v1, 0x80

    invoke-direct {v2, v1}, LX/186;-><init>(I)V

    .line 710973
    invoke-static {p0, v2}, LX/4Ru;->a(LX/15w;LX/186;)I

    move-result v1

    .line 710974
    if-eqz v0, :cond_0

    .line 710975
    const/4 v3, 0x2

    invoke-virtual {v2, v3}, LX/186;->c(I)V

    .line 710976
    invoke-virtual {v2, v4, p1, v4}, LX/186;->a(ISI)V

    .line 710977
    const/4 v3, 0x1

    invoke-virtual {v2, v3, v1}, LX/186;->b(II)V

    .line 710978
    invoke-virtual {v2}, LX/186;->d()I

    move-result v1

    .line 710979
    :cond_0
    invoke-virtual {v2, v1}, LX/186;->d(I)V

    .line 710980
    invoke-static {v2}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v1

    move-object v0, v1

    .line 710981
    return-object v0
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 710936
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 710937
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 710938
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 710939
    const-string v0, "name"

    const-string v1, "PymgfFeedUnit"

    invoke-virtual {p2, v0, v1}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 710940
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 710941
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 710942
    if-eqz v0, :cond_0

    .line 710943
    const-string v1, "debugInfo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 710944
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 710945
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 710946
    cmp-long v2, v0, v2

    if-eqz v2, :cond_1

    .line 710947
    const-string v2, "fetchTimeMs"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 710948
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 710949
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 710950
    if-eqz v0, :cond_2

    .line 710951
    const-string v1, "hideableToken"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 710952
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 710953
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 710954
    if-eqz v0, :cond_3

    .line 710955
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 710956
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 710957
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 710958
    if-eqz v0, :cond_4

    .line 710959
    const-string v1, "localStoryVisibility"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 710960
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 710961
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 710962
    if-eqz v0, :cond_5

    .line 710963
    const-string v1, "localStoryVisibleHeight"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 710964
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 710965
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 710966
    if-eqz v0, :cond_6

    .line 710967
    const-string v1, "tracking"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 710968
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 710969
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 710970
    return-void
.end method
