.class public final LX/4XN;
.super LX/0ur;
.source ""


# instance fields
.field public b:Lcom/facebook/graphql/model/GraphQLFeedback;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterTypeSet;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Lcom/facebook/graphql/model/GraphQLNode;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLVideo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 767047
    invoke-direct {p0}, LX/0ur;-><init>()V

    .line 767048
    const/4 v0, 0x0

    iput-object v0, p0, LX/4XN;->i:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 767049
    instance-of v0, p0, LX/4XN;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 767050
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;
    .locals 2

    .prologue
    .line 767051
    new-instance v0, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;

    invoke-direct {v0, p0}, Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;-><init>(LX/4XN;)V

    .line 767052
    return-object v0
.end method
