.class public final LX/4Va;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Vj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Vj",
        "<",
        "LX/4Ve",
        "<TT;>;",
        "LX/4Ve",
        "<TT;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0zO;

.field public final synthetic b:LX/1Mz;


# direct methods
.method public constructor <init>(LX/1Mz;LX/0zO;)V
    .locals 0

    .prologue
    .line 742627
    iput-object p1, p0, LX/4Va;->b:LX/1Mz;

    iput-object p2, p0, LX/4Va;->a:LX/0zO;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 5

    .prologue
    .line 742628
    check-cast p1, LX/4Ve;

    .line 742629
    iget-object v0, p0, LX/4Va;->b:LX/1Mz;

    iget-object v1, p0, LX/4Va;->a:LX/0zO;

    .line 742630
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 742631
    iget-object v2, p1, LX/4Ve;->a:Lcom/facebook/graphql/executor/GraphQLResult;

    move-object v2, v2

    .line 742632
    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 742633
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v2

    .line 742634
    iget-object v3, v0, LX/1Mz;->e:LX/0TD;

    new-instance v4, Lcom/facebook/graphql/executor/live/GraphQLLiveQueryExecutor$5;

    invoke-direct {v4, v0, v1, p1, v2}, Lcom/facebook/graphql/executor/live/GraphQLLiveQueryExecutor$5;-><init>(LX/1Mz;LX/0zO;LX/4Ve;Lcom/google/common/util/concurrent/SettableFuture;)V

    const p0, 0x2bf22d59

    invoke-static {v3, v4, p0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 742635
    move-object v0, v2

    .line 742636
    return-object v0
.end method
