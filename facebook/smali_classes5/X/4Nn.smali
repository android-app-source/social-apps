.class public LX/4Nn;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 694484
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 694485
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 694486
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 694487
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 694488
    invoke-static {p0, p1}, LX/4Nn;->b(LX/15w;LX/186;)I

    move-result v1

    .line 694489
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 694490
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 694491
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v2, :cond_1

    .line 694492
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 694493
    :goto_0
    return v0

    .line 694494
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 694495
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_OBJECT:LX/15z;

    if-eq v1, v2, :cond_2

    .line 694496
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v1

    .line 694497
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 694498
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v2, v3, :cond_1

    if-eqz v1, :cond_1

    .line 694499
    const-string v2, "snippet_sentences"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 694500
    invoke-static {p0, p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 694501
    :cond_2
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 694502
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 694503
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto :goto_0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 694504
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 694505
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 694506
    if-eqz v0, :cond_0

    .line 694507
    const-string v1, "snippet_sentences"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 694508
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 694509
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 694510
    return-void
.end method
