.class public LX/4C9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4C5;
.implements LX/4C7;


# instance fields
.field private final a:LX/1FZ;

.field private final b:LX/4CA;

.field private final c:LX/4C4;

.field private final d:LX/4CH;

.field private final e:Landroid/graphics/Paint;

.field private f:Landroid/graphics/Rect;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I

.field private h:I

.field private i:Landroid/graphics/Bitmap$Config;


# direct methods
.method public constructor <init>(LX/1FZ;LX/4CA;LX/4C4;LX/4CH;)V
    .locals 2

    .prologue
    .line 678469
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 678470
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v0, p0, LX/4C9;->i:Landroid/graphics/Bitmap$Config;

    .line 678471
    iput-object p1, p0, LX/4C9;->a:LX/1FZ;

    .line 678472
    iput-object p2, p0, LX/4C9;->b:LX/4CA;

    .line 678473
    iput-object p3, p0, LX/4C9;->c:LX/4C4;

    .line 678474
    iput-object p4, p0, LX/4C9;->d:LX/4CH;

    .line 678475
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x6

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, LX/4C9;->e:Landroid/graphics/Paint;

    .line 678476
    invoke-direct {p0}, LX/4C9;->g()V

    .line 678477
    return-void
.end method

.method private a(ILX/1FJ;)Z
    .locals 2
    .param p2    # LX/1FJ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 678478
    invoke-static {p2}, LX/1FJ;->a(LX/1FJ;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 678479
    const/4 v0, 0x0

    .line 678480
    :goto_0
    return v0

    .line 678481
    :cond_0
    iget-object v1, p0, LX/4C9;->d:LX/4CH;

    invoke-virtual {p2}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 678482
    iget-object p0, v1, LX/4CH;->c:LX/4dh;

    invoke-virtual {p0, p1, v0}, LX/4dh;->a(ILandroid/graphics/Bitmap;)V

    .line 678483
    const/4 p0, 0x1

    move v0, p0

    .line 678484
    goto :goto_0
    .line 678485
.end method

.method private a(ILX/1FJ;Landroid/graphics/Canvas;I)Z
    .locals 4
    .param p2    # LX/1FJ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Landroid/graphics/Canvas;",
            "I)Z"
        }
    .end annotation

    .prologue
    .line 678486
    invoke-static {p2}, LX/1FJ;->a(LX/1FJ;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 678487
    const/4 v0, 0x0

    .line 678488
    :goto_0
    return v0

    .line 678489
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/4C9;->f:Landroid/graphics/Rect;

    if-nez v0, :cond_1

    .line 678490
    invoke-virtual {p2}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget-object v3, p0, LX/4C9;->e:Landroid/graphics/Paint;

    invoke-virtual {p3, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 678491
    :goto_1
    iget-object v0, p0, LX/4C9;->b:LX/4CA;

    invoke-interface {v0, p1, p2}, LX/4CA;->a(ILX/1FJ;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 678492
    invoke-static {p2}, LX/1FJ;->c(LX/1FJ;)V

    .line 678493
    const/4 v0, 0x1

    goto :goto_0

    .line 678494
    :cond_1
    :try_start_1
    invoke-virtual {p2}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    iget-object v2, p0, LX/4C9;->f:Landroid/graphics/Rect;

    iget-object v3, p0, LX/4C9;->e:Landroid/graphics/Paint;

    invoke-virtual {p3, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 678495
    :catchall_0
    move-exception v0

    invoke-static {p2}, LX/1FJ;->c(LX/1FJ;)V

    throw v0
.end method

.method private g()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, -0x1

    .line 678496
    iget-object v0, p0, LX/4C9;->d:LX/4CH;

    invoke-virtual {v0}, LX/4CH;->a()I

    move-result v0

    iput v0, p0, LX/4C9;->g:I

    .line 678497
    iget v0, p0, LX/4C9;->g:I

    if-ne v0, v2, :cond_0

    .line 678498
    iget-object v0, p0, LX/4C9;->f:Landroid/graphics/Rect;

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    iput v0, p0, LX/4C9;->g:I

    .line 678499
    :cond_0
    iget-object v0, p0, LX/4C9;->d:LX/4CH;

    invoke-virtual {v0}, LX/4CH;->a()I

    move-result v0

    iput v0, p0, LX/4C9;->h:I

    .line 678500
    iget v0, p0, LX/4C9;->h:I

    if-ne v0, v2, :cond_1

    .line 678501
    iget-object v0, p0, LX/4C9;->f:Landroid/graphics/Rect;

    if-nez v0, :cond_3

    :goto_1
    iput v1, p0, LX/4C9;->h:I

    .line 678502
    :cond_1
    return-void

    .line 678503
    :cond_2
    iget-object v0, p0, LX/4C9;->f:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    goto :goto_0

    .line 678504
    :cond_3
    iget-object v0, p0, LX/4C9;->f:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v1

    goto :goto_1
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 678505
    iget v0, p0, LX/4C9;->g:I

    return v0
.end method

.method public final a(I)V
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/IntRange;
        .end annotation
    .end param

    .prologue
    .line 678506
    iget-object v0, p0, LX/4C9;->e:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 678507
    return-void
.end method

.method public final a(Landroid/graphics/ColorFilter;)V
    .locals 1
    .param p1    # Landroid/graphics/ColorFilter;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 678508
    iget-object v0, p0, LX/4C9;->e:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 678509
    return-void
.end method

.method public final a(Landroid/graphics/Rect;)V
    .locals 4
    .param p1    # Landroid/graphics/Rect;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 678461
    iput-object p1, p0, LX/4C9;->f:Landroid/graphics/Rect;

    .line 678462
    iget-object v0, p0, LX/4C9;->d:LX/4CH;

    .line 678463
    iget-object v1, v0, LX/4CH;->b:LX/4dG;

    invoke-interface {v1, p1}, LX/4dG;->a(Landroid/graphics/Rect;)LX/4dG;

    move-result-object v1

    .line 678464
    iget-object v2, v0, LX/4CH;->b:LX/4dG;

    if-eq v1, v2, :cond_0

    .line 678465
    iput-object v1, v0, LX/4CH;->b:LX/4dG;

    .line 678466
    new-instance v1, LX/4dh;

    iget-object v2, v0, LX/4CH;->b:LX/4dG;

    iget-object v3, v0, LX/4CH;->d:LX/4CF;

    invoke-direct {v1, v2, v3}, LX/4dh;-><init>(LX/4dG;LX/4CF;)V

    iput-object v1, v0, LX/4CH;->c:LX/4dh;

    .line 678467
    :cond_0
    invoke-direct {p0}, LX/4C9;->g()V

    .line 678468
    return-void
.end method

.method public final a(Landroid/graphics/drawable/Drawable;Landroid/graphics/Canvas;I)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 678443
    iget-object v2, p0, LX/4C9;->b:LX/4CA;

    invoke-interface {v2, p3}, LX/4CA;->a(I)LX/1FJ;

    move-result-object v2

    .line 678444
    invoke-direct {p0, p3, v2, p2, v1}, LX/4C9;->a(ILX/1FJ;Landroid/graphics/Canvas;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 678445
    :cond_0
    :goto_0
    return v0

    .line 678446
    :cond_1
    iget-object v2, p0, LX/4C9;->b:LX/4CA;

    invoke-interface {v2}, LX/4CA;->b()LX/1FJ;

    move-result-object v2

    .line 678447
    invoke-direct {p0, p3, v2}, LX/4C9;->a(ILX/1FJ;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-direct {p0, p3, v2, p2, v0}, LX/4C9;->a(ILX/1FJ;Landroid/graphics/Canvas;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 678448
    :cond_2
    iget-object v2, p0, LX/4C9;->a:LX/1FZ;

    iget v3, p0, LX/4C9;->g:I

    iget v4, p0, LX/4C9;->h:I

    iget-object v5, p0, LX/4C9;->i:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v2, v3, v4, v5}, LX/1FZ;->a(IILandroid/graphics/Bitmap$Config;)LX/1FJ;

    move-result-object v2

    .line 678449
    invoke-direct {p0, p3, v2}, LX/4C9;->a(ILX/1FJ;)Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x2

    invoke-direct {p0, p3, v2, p2, v3}, LX/4C9;->a(ILX/1FJ;Landroid/graphics/Canvas;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 678450
    :cond_3
    iget-object v2, p0, LX/4C9;->b:LX/4CA;

    invoke-interface {v2}, LX/4CA;->a()LX/1FJ;

    move-result-object v2

    .line 678451
    const/4 v3, 0x3

    invoke-direct {p0, p3, v2, p2, v3}, LX/4C9;->a(ILX/1FJ;Landroid/graphics/Canvas;I)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 678452
    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 678460
    iget v0, p0, LX/4C9;->h:I

    return v0
.end method

.method public final b(I)I
    .locals 1

    .prologue
    .line 678459
    iget-object v0, p0, LX/4C9;->c:LX/4C4;

    invoke-interface {v0, p1}, LX/4C4;->b(I)I

    move-result v0

    return v0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 678457
    iget-object v0, p0, LX/4C9;->b:LX/4CA;

    invoke-interface {v0}, LX/4CA;->c()V

    .line 678458
    return-void
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 678456
    iget-object v0, p0, LX/4C9;->c:LX/4C4;

    invoke-interface {v0}, LX/4C4;->d()I

    move-result v0

    return v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 678455
    iget-object v0, p0, LX/4C9;->c:LX/4C4;

    invoke-interface {v0}, LX/4C4;->e()I

    move-result v0

    return v0
.end method

.method public final f()V
    .locals 0

    .prologue
    .line 678453
    invoke-virtual {p0}, LX/4C9;->c()V

    .line 678454
    return-void
.end method
