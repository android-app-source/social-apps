.class public LX/3f4;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:[Ljava/lang/String;

.field private static volatile e:LX/3f4;


# instance fields
.field private b:LX/3f6;

.field private c:LX/0Sh;

.field private d:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 621381
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    sget-object v2, LX/3f5;->a:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, LX/3f5;->b:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, LX/3f5;->c:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, LX/3f5;->e:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, LX/3f5;->f:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, LX/3f5;->g:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    sput-object v0, LX/3f4;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/3f6;LX/0Sh;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 621376
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 621377
    iput-object p1, p0, LX/3f4;->b:LX/3f6;

    .line 621378
    iput-object p2, p0, LX/3f4;->c:LX/0Sh;

    .line 621379
    iput-object p3, p0, LX/3f4;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 621380
    return-void
.end method

.method public static a(LX/0QB;)LX/3f4;
    .locals 6

    .prologue
    .line 621281
    sget-object v0, LX/3f4;->e:LX/3f4;

    if-nez v0, :cond_1

    .line 621282
    const-class v1, LX/3f4;

    monitor-enter v1

    .line 621283
    :try_start_0
    sget-object v0, LX/3f4;->e:LX/3f4;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 621284
    if-eqz v2, :cond_0

    .line 621285
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 621286
    new-instance p0, LX/3f4;

    invoke-static {v0}, LX/3f6;->a(LX/0QB;)LX/3f6;

    move-result-object v3

    check-cast v3, LX/3f6;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v4

    check-cast v4, LX/0Sh;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v5

    check-cast v5, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {p0, v3, v4, v5}, LX/3f4;-><init>(LX/3f6;LX/0Sh;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 621287
    move-object v0, p0

    .line 621288
    sput-object v0, LX/3f4;->e:LX/3f4;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 621289
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 621290
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 621291
    :cond_1
    sget-object v0, LX/3f4;->e:LX/3f4;

    return-object v0

    .line 621292
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 621293
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Lcom/facebook/fbservice/service/OperationResult;
    .locals 23

    .prologue
    .line 621327
    move-object/from16 v0, p0

    iget-object v4, v0, LX/3f4;->c:LX/0Sh;

    invoke-virtual {v4}, LX/0Sh;->b()V

    .line 621328
    new-instance v4, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v4}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 621329
    const-string v5, "admined_pages_table"

    invoke-virtual {v4, v5}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 621330
    const/4 v12, 0x0

    .line 621331
    :try_start_0
    move-object/from16 v0, p0

    iget-object v5, v0, LX/3f4;->b:LX/3f6;

    invoke-virtual {v5}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    sget-object v6, LX/3f4;->a:[Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v4 .. v11}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    .line 621332
    :try_start_1
    invoke-interface {v5}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-nez v4, :cond_1

    .line 621333
    invoke-static {}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess()Lcom/facebook/fbservice/service/OperationResult;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v4

    .line 621334
    if-eqz v5, :cond_0

    .line 621335
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    :cond_0
    :goto_0
    return-object v4

    .line 621336
    :cond_1
    :try_start_2
    new-instance v7, LX/0Pz;

    invoke-direct {v7}, LX/0Pz;-><init>()V

    .line 621337
    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    .line 621338
    const/4 v4, 0x0

    .line 621339
    sget-object v6, LX/3f5;->a:LX/0U1;

    invoke-virtual {v6}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    .line 621340
    sget-object v6, LX/3f5;->b:LX/0U1;

    invoke-virtual {v6}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    .line 621341
    sget-object v6, LX/3f5;->c:LX/0U1;

    invoke-virtual {v6}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    .line 621342
    sget-object v6, LX/3f5;->e:LX/0U1;

    invoke-virtual {v6}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v12

    .line 621343
    sget-object v6, LX/3f5;->f:LX/0U1;

    invoke-virtual {v6}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    .line 621344
    sget-object v6, LX/3f5;->g:LX/0U1;

    invoke-virtual {v6}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    .line 621345
    const/4 v6, -0x1

    if-eq v9, v6, :cond_2

    const/4 v6, -0x1

    if-eq v10, v6, :cond_2

    const/4 v6, -0x1

    if-eq v11, v6, :cond_2

    const/4 v6, -0x1

    if-eq v13, v6, :cond_2

    const/4 v6, -0x1

    if-eq v12, v6, :cond_2

    const/4 v6, -0x1

    if-ne v14, v6, :cond_3

    .line 621346
    :cond_2
    sget-object v4, LX/1nY;->CACHE_DISK_ERROR:LX/1nY;

    invoke-static {v4}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v4

    .line 621347
    if-eqz v5, :cond_0

    .line 621348
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 621349
    :cond_3
    const/4 v6, 0x0

    .line 621350
    :try_start_3
    invoke-interface {v5, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 621351
    invoke-static {v15}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v16

    if-nez v16, :cond_4

    .line 621352
    new-instance v6, LX/0Pz;

    invoke-direct {v6}, LX/0Pz;-><init>()V

    const/16 v16, 0x3a

    invoke-static/range {v15 .. v16}, LX/0YN;->a(Ljava/lang/String;C)Ljava/util/List;

    move-result-object v15

    invoke-virtual {v6, v15}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v6

    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    .line 621353
    :cond_4
    invoke-interface {v5, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 621354
    invoke-interface {v5, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 621355
    invoke-interface {v5, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    new-instance v18, LX/186;

    const/16 v19, 0x400

    invoke-direct/range {v18 .. v19}, LX/186;-><init>(I)V

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v17

    const/16 v19, 0x1

    invoke-virtual/range {v18 .. v19}, LX/186;->c(I)V

    const/16 v19, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    const v17, -0x4e30aecf

    move-object/from16 v0, v18

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/1vs;->a(LX/186;I)LX/1vs;

    move-result-object v17

    move-object/from16 v0, v17

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v18, v0

    move-object/from16 v0, v17

    iget v0, v0, LX/1vs;->b:I

    move/from16 v17, v0

    .line 621356
    invoke-interface {v5, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v19

    new-instance v20, LX/186;

    const/16 v21, 0x400

    invoke-direct/range {v20 .. v21}, LX/186;-><init>(I)V

    const/16 v21, 0x1

    invoke-virtual/range {v20 .. v21}, LX/186;->c(I)V

    const/16 v21, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    const v19, -0x171aa0d7

    move-object/from16 v0, v20

    move/from16 v1, v19

    invoke-static {v0, v1}, LX/1vs;->a(LX/186;I)LX/1vs;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v20, v0

    move-object/from16 v0, v19

    iget v0, v0, LX/1vs;->b:I

    move/from16 v19, v0

    .line 621357
    new-instance v21, LX/8De;

    invoke-direct/range {v21 .. v21}, LX/8De;-><init>()V

    move-object/from16 v0, v21

    invoke-virtual {v0, v15}, LX/8De;->a(Ljava/lang/String;)LX/8De;

    move-result-object v21

    invoke-interface {v5, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, LX/8De;->b(Ljava/lang/String;)LX/8De;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, v18

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, LX/8De;->b(LX/15i;I)LX/8De;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v6}, LX/8De;->a(LX/0Px;)LX/8De;

    move-result-object v6

    move-object/from16 v0, v20

    move/from16 v1, v19

    invoke-virtual {v6, v0, v1}, LX/8De;->a(LX/15i;I)LX/8De;
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v6

    .line 621358
    :try_start_4
    invoke-virtual {v6}, LX/8De;->a()Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;

    move-result-object v6

    invoke-virtual {v7, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 621359
    move-object/from16 v0, v16

    invoke-interface {v8, v15, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 621360
    add-int/lit8 v4, v4, 0x1

    .line 621361
    :goto_1
    invoke-interface {v5}, Landroid/database/Cursor;->moveToNext()Z

    move-result v6

    if-nez v6, :cond_3

    .line 621362
    new-instance v6, LX/8Dg;

    invoke-direct {v6}, LX/8Dg;-><init>()V

    new-instance v9, LX/8Dd;

    invoke-direct {v9}, LX/8Dd;-><init>()V

    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v7

    invoke-virtual {v9, v7}, LX/8Dd;->a(LX/0Px;)LX/8Dd;

    move-result-object v7

    invoke-virtual {v7, v4}, LX/8Dd;->a(I)LX/8Dd;

    move-result-object v4

    invoke-virtual {v4}, LX/8Dd;->a()Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel;

    move-result-object v4

    invoke-virtual {v6, v4}, LX/8Dg;->a(Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel;)LX/8Dg;

    move-result-object v4

    .line 621363
    move-object/from16 v0, p0

    iget-object v6, v0, LX/3f4;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v7, LX/3ex;->b:LX/0Tn;

    const-wide/16 v10, 0x0

    invoke-interface {v6, v7, v10, v11}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v6

    .line 621364
    new-instance v9, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchMethod$Result;

    sget-object v10, LX/0ta;->FROM_CACHE_STALE:LX/0ta;

    invoke-virtual {v4}, LX/8Dg;->a()Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel;

    move-result-object v4

    invoke-direct {v9, v10, v6, v7, v4}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchMethod$Result;-><init>(LX/0ta;JLcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel;)V

    .line 621365
    new-instance v4, Lcom/facebook/pages/adminedpages/protocol/PagesAccessTokenPrefetchMethod$Result;

    sget-object v10, LX/0ta;->FROM_CACHE_STALE:LX/0ta;

    invoke-direct {v4, v10, v6, v7, v8}, Lcom/facebook/pages/adminedpages/protocol/PagesAccessTokenPrefetchMethod$Result;-><init>(LX/0ta;JLjava/util/Map;)V

    .line 621366
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 621367
    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 621368
    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 621369
    invoke-static {v6}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/util/ArrayList;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result-object v4

    .line 621370
    if-eqz v5, :cond_0

    .line 621371
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 621372
    :catchall_0
    move-exception v4

    move-object v5, v12

    :goto_2
    if-eqz v5, :cond_5

    .line 621373
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v4

    .line 621374
    :catchall_1
    move-exception v4

    goto :goto_2

    .line 621375
    :catch_0
    goto :goto_1
.end method

.method public final a(Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchMethod$Result;Lcom/facebook/pages/adminedpages/protocol/PagesAccessTokenPrefetchMethod$Result;)V
    .locals 9

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 621294
    iget-object v0, p0, LX/3f4;->c:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 621295
    invoke-virtual {p1}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchMethod$Result;->a()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_1

    .line 621296
    :cond_0
    return-void

    .line 621297
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchMethod$Result;->a()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel;

    invoke-virtual {v0}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel;->a()LX/0Px;

    move-result-object v0

    .line 621298
    iget-object v1, p0, LX/3f4;->b:LX/3f6;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const-string v4, "admined_pages_table"

    invoke-virtual {v1, v4, v5, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 621299
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 621300
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;

    .line 621301
    invoke-virtual {v0}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->d()Ljava/lang/String;

    move-result-object v5

    .line 621302
    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 621303
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 621304
    sget-object v8, LX/3f8;->a:LX/0Px;

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result p1

    const/4 v1, 0x0

    move v7, v1

    :goto_1
    if-ge v7, p1, :cond_3

    invoke-virtual {v8, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0U1;

    .line 621305
    invoke-virtual {v1}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 621306
    add-int/lit8 v1, v7, 0x1

    move v7, v1

    goto :goto_1

    .line 621307
    :cond_3
    sget-object v1, LX/3f5;->a:LX/0U1;

    invoke-virtual {v1}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->d()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v1, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 621308
    invoke-virtual {v0}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->e()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 621309
    sget-object v1, LX/3f5;->b:LX/0U1;

    invoke-virtual {v1}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->e()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v1, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 621310
    :cond_4
    invoke-virtual {v0}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->b()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-eqz v1, :cond_a

    .line 621311
    invoke-virtual {v0}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->c()LX/1vs;

    move-result-object v1

    iget-object v7, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 621312
    invoke-virtual {v7, v1, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_9

    move v1, v2

    :goto_2
    if-eqz v1, :cond_5

    .line 621313
    invoke-virtual {v0}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->c()LX/1vs;

    move-result-object v1

    iget-object v7, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 621314
    sget-object v8, LX/3f5;->c:LX/0U1;

    invoke-virtual {v8}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v1, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v8, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 621315
    :cond_5
    invoke-virtual {v0}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->eD_()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_6

    invoke-virtual {v0}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->eD_()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6

    .line 621316
    const-string v1, ":"

    new-array v7, v2, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->eD_()LX/0Px;

    move-result-object v8

    aput-object v8, v7, v3

    invoke-static {v1, v7}, LX/0YN;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 621317
    sget-object v7, LX/3f5;->e:LX/0U1;

    invoke-virtual {v7}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 621318
    :cond_6
    invoke-virtual {v0}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->b()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-eqz v1, :cond_7

    .line 621319
    invoke-virtual {v0}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->b()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 621320
    sget-object v7, LX/3f5;->f:LX/0U1;

    invoke-virtual {v7}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v0, v3}, LX/15i;->h(II)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v7, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 621321
    :cond_7
    iget-object v0, p2, Lcom/facebook/pages/adminedpages/protocol/PagesAccessTokenPrefetchMethod$Result;->a:Ljava/util/Map;

    move-object v0, v0

    .line 621322
    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 621323
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 621324
    sget-object v1, LX/3f5;->g:LX/0U1;

    invoke-virtual {v1}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 621325
    :cond_8
    iget-object v0, p0, LX/3f4;->b:LX/3f6;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "admined_pages_table"

    const-string v5, ""

    const v7, 0x3e9fc1ab

    invoke-static {v7}, LX/03h;->a(I)V

    invoke-virtual {v0, v1, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v0, 0x787bc23

    invoke-static {v0}, LX/03h;->a(I)V

    goto/16 :goto_0

    :cond_9
    move v1, v3

    .line 621326
    goto/16 :goto_2

    :cond_a
    move v1, v3

    goto/16 :goto_2
.end method
