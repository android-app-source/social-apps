.class public final LX/4fY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Ot;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/0Ot",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<TT;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private b:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 798634
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 798635
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Or;

    iput-object v0, p0, LX/4fY;->a:LX/0Or;

    .line 798636
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 798637
    iget-object v0, p0, LX/4fY;->a:LX/0Or;

    if-eqz v0, :cond_1

    .line 798638
    monitor-enter p0

    .line 798639
    :try_start_0
    iget-object v0, p0, LX/4fY;->a:LX/0Or;

    if-eqz v0, :cond_0

    .line 798640
    iget-object v0, p0, LX/4fY;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, LX/4fY;->b:Ljava/lang/Object;

    .line 798641
    const/4 v0, 0x0

    iput-object v0, p0, LX/4fY;->a:LX/0Or;

    .line 798642
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 798643
    :cond_1
    iget-object v0, p0, LX/4fY;->b:Ljava/lang/Object;

    return-object v0

    .line 798644
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
