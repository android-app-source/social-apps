.class public final LX/4vZ;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/4vQ;

.field public static final b:LX/4vQ;

.field public static final c:LX/4vQ;

.field public static final d:LX/4vQ;

.field public static final e:LX/4vQ;

.field private static f:LX/4va;

.field private static final g:LX/4vO;


# instance fields
.field private final h:Landroid/content/Context;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, LX/4vP;

    invoke-direct {v0}, LX/4vP;-><init>()V

    sput-object v0, LX/4vZ;->g:LX/4vO;

    new-instance v0, LX/4vR;

    invoke-direct {v0}, LX/4vR;-><init>()V

    sput-object v0, LX/4vZ;->a:LX/4vQ;

    new-instance v0, LX/4vS;

    invoke-direct {v0}, LX/4vS;-><init>()V

    sput-object v0, LX/4vZ;->b:LX/4vQ;

    new-instance v0, LX/4vT;

    invoke-direct {v0}, LX/4vT;-><init>()V

    sput-object v0, LX/4vZ;->c:LX/4vQ;

    new-instance v0, LX/4vU;

    invoke-direct {v0}, LX/4vU;-><init>()V

    sput-object v0, LX/4vZ;->d:LX/4vQ;

    new-instance v0, LX/4vV;

    invoke-direct {v0}, LX/4vV;-><init>()V

    sput-object v0, LX/4vZ;->e:LX/4vQ;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, LX/1ol;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, LX/4vZ;->h:Landroid/content/Context;

    return-void
.end method

.method public static a(Landroid/content/Context;LX/4vQ;Ljava/lang/String;)LX/4vZ;
    .locals 9

    const/4 v8, 0x1

    const/4 v6, -0x1

    sget-object v0, LX/4vZ;->g:LX/4vO;

    invoke-interface {p1, p0, p2, v0}, LX/4vQ;->a(Landroid/content/Context;Ljava/lang/String;LX/4vO;)LX/4vY;

    move-result-object v2

    iget v0, v2, LX/4vY;->a:I

    iget v1, v2, LX/4vY;->b:I

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x44

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Considering local module "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " and remote module "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ":"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    iget v0, v2, LX/4vY;->c:I

    if-eqz v0, :cond_1

    iget v0, v2, LX/4vY;->c:I

    if-ne v0, v6, :cond_0

    iget v0, v2, LX/4vY;->a:I

    if-eqz v0, :cond_1

    :cond_0
    iget v0, v2, LX/4vY;->c:I

    if-ne v0, v8, :cond_2

    iget v0, v2, LX/4vY;->b:I

    if-nez v0, :cond_2

    :cond_1
    new-instance v0, LX/4vX;

    iget v1, v2, LX/4vY;->a:I

    iget v2, v2, LX/4vY;->b:I

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x5b

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "No acceptable module found. Local version is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " and remote version is "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/4vX;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget v0, v2, LX/4vY;->c:I

    if-ne v0, v6, :cond_3

    invoke-static {p0, p2}, LX/4vZ;->b(Landroid/content/Context;Ljava/lang/String;)LX/4vZ;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_3
    iget v0, v2, LX/4vY;->c:I

    if-ne v0, v8, :cond_6

    :try_start_0
    iget v0, v2, LX/4vY;->b:I

    invoke-static {p0, p2, v0}, LX/4vZ;->a(Landroid/content/Context;Ljava/lang/String;I)LX/4vZ;
    :try_end_0
    .catch LX/4vX; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v1, v0

    const-string v3, "DynamiteModule"

    const-string v4, "Failed to load remote module: "

    invoke-virtual {v1}, LX/4vX;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_4

    invoke-virtual {v4, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, v2, LX/4vY;->a:I

    if-eqz v0, :cond_5

    iget v0, v2, LX/4vY;->a:I

    new-instance v2, LX/4vW;

    invoke-direct {v2, v0}, LX/4vW;-><init>(I)V

    invoke-interface {p1, p0, p2, v2}, LX/4vQ;->a(Landroid/content/Context;Ljava/lang/String;LX/4vO;)LX/4vY;

    move-result-object v0

    iget v0, v0, LX/4vY;->c:I

    if-ne v0, v6, :cond_5

    invoke-static {p0, p2}, LX/4vZ;->b(Landroid/content/Context;Ljava/lang/String;)LX/4vZ;

    move-result-object v0

    goto :goto_0

    :cond_4
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    :cond_5
    new-instance v0, LX/4vX;

    const-string v2, "Remote load failed. No local fallback found."

    invoke-direct {v0, v2, v1}, LX/4vX;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    :cond_6
    new-instance v0, LX/4vX;

    iget v1, v2, LX/4vY;->c:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x2f

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "VersionPolicy returned invalid code:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/4vX;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;I)LX/4vZ;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x33

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Selected remote version of "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", version >= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-static {p0}, LX/4vZ;->a(Landroid/content/Context;)LX/4va;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, LX/4vX;

    const-string v1, "Failed to create IDynamiteLoader."

    invoke-direct {v0, v1}, LX/4vX;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    :try_start_0
    invoke-static {p0}, LX/1or;->a(Ljava/lang/Object;)LX/1ot;

    move-result-object v1

    invoke-interface {v0, v1, p1, p2}, LX/4va;->a(LX/1ot;Ljava/lang/String;I)LX/1ot;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    invoke-static {v0}, LX/1or;->a(LX/1ot;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    new-instance v0, LX/4vX;

    const-string v1, "Failed to load remote module."

    invoke-direct {v0, v1}, LX/4vX;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_0
    move-exception v0

    new-instance v1, LX/4vX;

    const-string v2, "Failed to load remote module."

    invoke-direct {v1, v2, v0}, LX/4vX;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :cond_1
    new-instance v1, LX/4vZ;

    invoke-static {v0}, LX/1or;->a(LX/1ot;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v1, v0}, LX/4vZ;-><init>(Landroid/content/Context;)V

    return-object v1
.end method

.method public static a(Landroid/content/Context;)LX/4va;
    .locals 6

    const/4 v1, 0x0

    const-class v2, LX/4vZ;

    monitor-enter v2

    :try_start_0
    sget-object v0, LX/4vZ;->f:LX/4va;

    if-eqz v0, :cond_0

    sget-object v0, LX/4vZ;->f:LX/4va;

    monitor-exit v2

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/1od;->b:LX/1od;

    move-object v0, v0

    invoke-virtual {v0, p0}, LX/1od;->a(Landroid/content/Context;)I

    move-result v0

    if-eqz v0, :cond_1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v0, v1

    goto :goto_0

    :cond_1
    :try_start_1
    const-string v0, "com.google.android.gms"

    const/4 v3, 0x3

    invoke-virtual {p0, v0, v3}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    const-string v3, "com.google.android.gms.chimera.container.DynamiteLoaderImpl"

    invoke-virtual {v0, v3}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/IBinder;

    if-nez v0, :cond_4

    const/4 v3, 0x0

    :goto_1
    move-object v0, v3

    if-eqz v0, :cond_2

    sput-object v0, LX/4vZ;->f:LX/4va;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :catch_0
    move-exception v0

    :try_start_3
    const-string v3, "DynamiteModule"

    const-string v4, "Failed to load IDynamiteLoader from GmsCore: "

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {v4, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    monitor-exit v2

    move-object v0, v1

    goto :goto_0

    :cond_3
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    :cond_4
    const-string v3, "com.google.android.gms.dynamite.IDynamiteLoader"

    invoke-interface {v0, v3}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v3

    if-eqz v3, :cond_5

    instance-of v4, v3, LX/4va;

    if-eqz v4, :cond_5

    check-cast v3, LX/4va;

    goto :goto_1

    :cond_5
    new-instance v3, LX/4vb;

    invoke-direct {v3, v0}, LX/4vb;-><init>(Landroid/os/IBinder;)V

    goto :goto_1
.end method

.method private static b(Landroid/content/Context;Ljava/lang/String;)LX/4vZ;
    .locals 2

    const-string v0, "Selected local version of "

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    new-instance v0, LX/4vZ;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/4vZ;-><init>(Landroid/content/Context;)V

    return-object v0

    :cond_0
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Landroid/os/IBinder;
    .locals 5

    :try_start_0
    iget-object v0, p0, LX/4vZ;->h:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/IBinder;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2

    return-object v0

    :catch_0
    move-exception v0

    :goto_0
    new-instance v2, LX/4vX;

    const-string v3, "Failed to instantiate module class: "

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_1
    invoke-direct {v2, v1, v0}, LX/4vX;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    :cond_0
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    :catch_1
    move-exception v0

    goto :goto_0

    :catch_2
    move-exception v0

    goto :goto_0
.end method
