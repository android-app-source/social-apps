.class public final LX/3dM;
.super LX/0ur;
.source ""


# instance fields
.field public A:Lcom/facebook/graphql/model/GraphQLInteractorsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public B:Lcom/facebook/graphql/model/GraphQLInteractorsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public C:Z

.field public D:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public E:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public F:Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public G:Lcom/facebook/graphql/model/GraphQLProfileDiscoveryBucket;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public H:Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public I:Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public J:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public K:Lcom/facebook/graphql/model/GraphQLResharesOfContentConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public L:Lcom/facebook/graphql/model/GraphQLSeenByConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public M:Z

.field public N:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedbackReaction;",
            ">;"
        }
    .end annotation
.end field

.field public O:Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public P:Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public R:Lcom/facebook/graphql/model/GraphQLVideoTimestampedCommentsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public S:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public T:Lcom/facebook/graphql/model/GraphQLUser;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public U:Lcom/facebook/graphql/model/GraphQLUser;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public V:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public W:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public X:Lcom/facebook/graphql/model/GraphQLFeedbackReaction;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Y:I

.field public Z:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aa:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ab:Lcom/facebook/graphql/model/GraphQLVoiceSwitcherActorsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ac:Lcom/facebook/graphql/model/GraphQLVoiceSwitcherPagesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ad:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:LX/15i;

.field public c:I

.field public d:Z

.field public e:Z

.field public f:Z

.field public g:Z

.field public h:Z

.field public i:Z

.field public j:Z

.field public k:Z

.field public l:Z

.field public m:Lcom/facebook/graphql/model/GraphQLCommentersConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/graphql/model/GraphQLCommentsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:I

.field public r:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Z

.field public t:Z

.field public u:Z

.field public v:J

.field public w:Lcom/facebook/graphql/model/GraphQLCommentersConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Z

.field public y:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:Lcom/facebook/graphql/model/GraphQLImportantReactorsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 617043
    invoke-direct {p0}, LX/0ur;-><init>()V

    .line 617044
    const/4 v0, 0x0

    iput-object v0, p0, LX/3dM;->ad:LX/0x2;

    .line 617045
    instance-of v0, p0, LX/3dM;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 617046
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/3dM;
    .locals 4

    .prologue
    .line 617047
    new-instance v1, LX/3dM;

    invoke-direct {v1}, LX/3dM;-><init>()V

    .line 617048
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 617049
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->aa()Z

    move-result v0

    iput-boolean v0, v1, LX/3dM;->d:Z

    .line 617050
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->b()Z

    move-result v0

    iput-boolean v0, v1, LX/3dM;->e:Z

    .line 617051
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->c()Z

    move-result v0

    iput-boolean v0, v1, LX/3dM;->f:Z

    .line 617052
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->d()Z

    move-result v0

    iput-boolean v0, v1, LX/3dM;->g:Z

    .line 617053
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->n()Z

    move-result v0

    iput-boolean v0, v1, LX/3dM;->h:Z

    .line 617054
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->e()Z

    move-result v0

    iput-boolean v0, v1, LX/3dM;->i:Z

    .line 617055
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->r_()Z

    move-result v0

    iput-boolean v0, v1, LX/3dM;->j:Z

    .line 617056
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->o()Z

    move-result v0

    iput-boolean v0, v1, LX/3dM;->k:Z

    .line 617057
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->p()Z

    move-result v0

    iput-boolean v0, v1, LX/3dM;->l:Z

    .line 617058
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->q()Lcom/facebook/graphql/model/GraphQLCommentersConnection;

    move-result-object v0

    iput-object v0, v1, LX/3dM;->m:Lcom/facebook/graphql/model/GraphQLCommentersConnection;

    .line 617059
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->r()Lcom/facebook/graphql/model/GraphQLCommentsConnection;

    move-result-object v0

    iput-object v0, v1, LX/3dM;->n:Lcom/facebook/graphql/model/GraphQLCommentsConnection;

    .line 617060
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    iput-object v0, v1, LX/3dM;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 617061
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->t()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/3dM;->p:Ljava/lang/String;

    .line 617062
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->ad()I

    move-result v0

    iput v0, v1, LX/3dM;->q:I

    .line 617063
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->u()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/3dM;->r:Ljava/lang/String;

    .line 617064
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->v()Z

    move-result v0

    iput-boolean v0, v1, LX/3dM;->s:Z

    .line 617065
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->s_()Z

    move-result v0

    iput-boolean v0, v1, LX/3dM;->t:Z

    .line 617066
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->w()Z

    move-result v0

    iput-boolean v0, v1, LX/3dM;->u:Z

    .line 617067
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->x()J

    move-result-wide v2

    iput-wide v2, v1, LX/3dM;->v:J

    .line 617068
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->y()Lcom/facebook/graphql/model/GraphQLCommentersConnection;

    move-result-object v0

    iput-object v0, v1, LX/3dM;->w:Lcom/facebook/graphql/model/GraphQLCommentersConnection;

    .line 617069
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->z()Z

    move-result v0

    iput-boolean v0, v1, LX/3dM;->x:Z

    .line 617070
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/3dM;->y:Ljava/lang/String;

    .line 617071
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->A()Lcom/facebook/graphql/model/GraphQLImportantReactorsConnection;

    move-result-object v0

    iput-object v0, v1, LX/3dM;->z:Lcom/facebook/graphql/model/GraphQLImportantReactorsConnection;

    .line 617072
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->B()Lcom/facebook/graphql/model/GraphQLInteractorsConnection;

    move-result-object v0

    iput-object v0, v1, LX/3dM;->A:Lcom/facebook/graphql/model/GraphQLInteractorsConnection;

    .line 617073
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->C()Lcom/facebook/graphql/model/GraphQLInteractorsConnection;

    move-result-object v0

    iput-object v0, v1, LX/3dM;->B:Lcom/facebook/graphql/model/GraphQLInteractorsConnection;

    .line 617074
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->D()Z

    move-result v0

    iput-boolean v0, v1, LX/3dM;->C:Z

    .line 617075
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/3dM;->D:Ljava/lang/String;

    .line 617076
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    iput-object v0, v1, LX/3dM;->E:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 617077
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->F()Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v0

    iput-object v0, v1, LX/3dM;->F:Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    .line 617078
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->ae()Lcom/facebook/graphql/model/GraphQLProfileDiscoveryBucket;

    move-result-object v0

    iput-object v0, v1, LX/3dM;->G:Lcom/facebook/graphql/model/GraphQLProfileDiscoveryBucket;

    .line 617079
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->G()Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    move-result-object v0

    iput-object v0, v1, LX/3dM;->H:Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    .line 617080
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->H()Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;

    move-result-object v0

    iput-object v0, v1, LX/3dM;->I:Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;

    .line 617081
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->I()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/3dM;->J:Ljava/lang/String;

    .line 617082
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->J()Lcom/facebook/graphql/model/GraphQLResharesOfContentConnection;

    move-result-object v0

    iput-object v0, v1, LX/3dM;->K:Lcom/facebook/graphql/model/GraphQLResharesOfContentConnection;

    .line 617083
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->K()Lcom/facebook/graphql/model/GraphQLSeenByConnection;

    move-result-object v0

    iput-object v0, v1, LX/3dM;->L:Lcom/facebook/graphql/model/GraphQLSeenByConnection;

    .line 617084
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->L()Z

    move-result v0

    iput-boolean v0, v1, LX/3dM;->M:Z

    .line 617085
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->M()LX/0Px;

    move-result-object v0

    iput-object v0, v1, LX/3dM;->N:LX/0Px;

    .line 617086
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->N()Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    move-result-object v0

    iput-object v0, v1, LX/3dM;->O:Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    .line 617087
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->O()Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;

    move-result-object v0

    iput-object v0, v1, LX/3dM;->P:Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;

    .line 617088
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->P()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/3dM;->Q:Ljava/lang/String;

    .line 617089
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->Q()Lcom/facebook/graphql/model/GraphQLVideoTimestampedCommentsConnection;

    move-result-object v0

    iput-object v0, v1, LX/3dM;->R:Lcom/facebook/graphql/model/GraphQLVideoTimestampedCommentsConnection;

    .line 617090
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->R()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    iput-object v0, v1, LX/3dM;->S:Lcom/facebook/graphql/model/GraphQLPage;

    .line 617091
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->S()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v0

    iput-object v0, v1, LX/3dM;->T:Lcom/facebook/graphql/model/GraphQLUser;

    .line 617092
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->ab()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v0

    iput-object v0, v1, LX/3dM;->U:Lcom/facebook/graphql/model/GraphQLUser;

    .line 617093
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->T()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/3dM;->V:Ljava/lang/String;

    .line 617094
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->U()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    iput-object v0, v1, LX/3dM;->W:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 617095
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->V()Lcom/facebook/graphql/model/GraphQLFeedbackReaction;

    move-result-object v0

    iput-object v0, v1, LX/3dM;->X:Lcom/facebook/graphql/model/GraphQLFeedbackReaction;

    .line 617096
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->W()I

    move-result v0

    iput v0, v1, LX/3dM;->Y:I

    .line 617097
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->X()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/3dM;->Z:Ljava/lang/String;

    .line 617098
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->Y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    iput-object v0, v1, LX/3dM;->aa:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 617099
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->ac()Lcom/facebook/graphql/model/GraphQLVoiceSwitcherActorsConnection;

    move-result-object v0

    iput-object v0, v1, LX/3dM;->ab:Lcom/facebook/graphql/model/GraphQLVoiceSwitcherActorsConnection;

    .line 617100
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->Z()Lcom/facebook/graphql/model/GraphQLVoiceSwitcherPagesConnection;

    move-result-object v0

    iput-object v0, v1, LX/3dM;->ac:Lcom/facebook/graphql/model/GraphQLVoiceSwitcherPagesConnection;

    .line 617101
    invoke-static {v1, p0}, LX/0ur;->a(LX/0ur;Lcom/facebook/graphql/modelutil/BaseModel;)V

    .line 617102
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->L_()LX/0x2;

    move-result-object v0

    invoke-virtual {v0}, LX/0x2;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0x2;

    iput-object v0, v1, LX/3dM;->ad:LX/0x2;

    .line 617103
    return-object v1
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;)LX/3dM;
    .locals 3
    .param p1    # Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 617104
    iput-object p1, p0, LX/3dM;->F:Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    .line 617105
    iget-object v0, p0, LX/3dM;->b:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3dM;->b:LX/15i;

    .line 617106
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 617107
    if-eqz v0, :cond_0

    .line 617108
    iget-object v0, p0, LX/3dM;->b:LX/15i;

    iget v1, p0, LX/3dM;->c:I

    const/16 v2, 0x1b

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 617109
    :cond_0
    return-object p0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLPage;)LX/3dM;
    .locals 0
    .param p1    # Lcom/facebook/graphql/model/GraphQLPage;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 617110
    iput-object p1, p0, LX/3dM;->S:Lcom/facebook/graphql/model/GraphQLPage;

    .line 617111
    return-object p0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;)LX/3dM;
    .locals 3
    .param p1    # Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 617112
    iput-object p1, p0, LX/3dM;->H:Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    .line 617113
    iget-object v0, p0, LX/3dM;->b:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3dM;->b:LX/15i;

    .line 617114
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 617115
    if-eqz v0, :cond_0

    .line 617116
    iget-object v0, p0, LX/3dM;->b:LX/15i;

    iget v1, p0, LX/3dM;->c:I

    const/16 v2, 0x1d

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 617117
    :cond_0
    return-object p0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;)LX/3dM;
    .locals 3
    .param p1    # Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 617035
    iput-object p1, p0, LX/3dM;->O:Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    .line 617036
    iget-object v0, p0, LX/3dM;->b:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3dM;->b:LX/15i;

    .line 617037
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 617038
    if-eqz v0, :cond_0

    .line 617039
    iget-object v0, p0, LX/3dM;->b:LX/15i;

    iget v1, p0, LX/3dM;->c:I

    const/16 v2, 0x24

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 617040
    :cond_0
    return-object p0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;)LX/3dM;
    .locals 3
    .param p1    # Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 616993
    iput-object p1, p0, LX/3dM;->P:Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;

    .line 616994
    iget-object v0, p0, LX/3dM;->b:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3dM;->b:LX/15i;

    .line 616995
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 616996
    if-eqz v0, :cond_0

    .line 616997
    iget-object v0, p0, LX/3dM;->b:LX/15i;

    iget v1, p0, LX/3dM;->c:I

    const/16 v2, 0x25

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 616998
    :cond_0
    return-object p0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 2

    .prologue
    .line 617041
    new-instance v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-direct {v0, p0}, Lcom/facebook/graphql/model/GraphQLFeedback;-><init>(LX/3dM;)V

    .line 617042
    return-object v0
.end method

.method public final b(I)LX/3dM;
    .locals 3

    .prologue
    .line 617029
    iput p1, p0, LX/3dM;->Y:I

    .line 617030
    iget-object v0, p0, LX/3dM;->b:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3dM;->b:LX/15i;

    .line 617031
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 617032
    if-eqz v0, :cond_0

    .line 617033
    iget-object v0, p0, LX/3dM;->b:LX/15i;

    iget v1, p0, LX/3dM;->c:I

    const/16 v2, 0x2d

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->b(III)V

    .line 617034
    :cond_0
    return-object p0
.end method

.method public final c(Z)LX/3dM;
    .locals 3

    .prologue
    .line 617023
    iput-boolean p1, p0, LX/3dM;->f:Z

    .line 617024
    iget-object v0, p0, LX/3dM;->b:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3dM;->b:LX/15i;

    .line 617025
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 617026
    if-eqz v0, :cond_0

    .line 617027
    iget-object v0, p0, LX/3dM;->b:LX/15i;

    iget v1, p0, LX/3dM;->c:I

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 617028
    :cond_0
    return-object p0
.end method

.method public final g(Z)LX/3dM;
    .locals 3

    .prologue
    .line 617017
    iput-boolean p1, p0, LX/3dM;->j:Z

    .line 617018
    iget-object v0, p0, LX/3dM;->b:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3dM;->b:LX/15i;

    .line 617019
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 617020
    if-eqz v0, :cond_0

    .line 617021
    iget-object v0, p0, LX/3dM;->b:LX/15i;

    iget v1, p0, LX/3dM;->c:I

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 617022
    :cond_0
    return-object p0
.end method

.method public final j(Z)LX/3dM;
    .locals 3

    .prologue
    .line 617011
    iput-boolean p1, p0, LX/3dM;->t:Z

    .line 617012
    iget-object v0, p0, LX/3dM;->b:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3dM;->b:LX/15i;

    .line 617013
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 617014
    if-eqz v0, :cond_0

    .line 617015
    iget-object v0, p0, LX/3dM;->b:LX/15i;

    iget v1, p0, LX/3dM;->c:I

    const/16 v2, 0xf

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 617016
    :cond_0
    return-object p0
.end method

.method public final k(Z)LX/3dM;
    .locals 3

    .prologue
    .line 617005
    iput-boolean p1, p0, LX/3dM;->x:Z

    .line 617006
    iget-object v0, p0, LX/3dM;->b:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3dM;->b:LX/15i;

    .line 617007
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 617008
    if-eqz v0, :cond_0

    .line 617009
    iget-object v0, p0, LX/3dM;->b:LX/15i;

    iget v1, p0, LX/3dM;->c:I

    const/16 v2, 0x13

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 617010
    :cond_0
    return-object p0
.end method

.method public final l(Z)LX/3dM;
    .locals 3

    .prologue
    .line 616999
    iput-boolean p1, p0, LX/3dM;->C:Z

    .line 617000
    iget-object v0, p0, LX/3dM;->b:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3dM;->b:LX/15i;

    .line 617001
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 617002
    if-eqz v0, :cond_0

    .line 617003
    iget-object v0, p0, LX/3dM;->b:LX/15i;

    iget v1, p0, LX/3dM;->c:I

    const/16 v2, 0x18

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 617004
    :cond_0
    return-object p0
.end method
