.class public LX/4ex;
.super LX/4ep;
.source ""


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field private final b:Landroid/content/ContentResolver;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 797734
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "_data"

    aput-object v2, v0, v1

    sput-object v0, LX/4ex;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/Executor;LX/1Fj;Landroid/content/ContentResolver;Z)V
    .locals 0

    .prologue
    .line 797735
    invoke-direct {p0, p1, p2, p4}, LX/4ep;-><init>(Ljava/util/concurrent/Executor;LX/1Fj;Z)V

    .line 797736
    iput-object p3, p0, LX/4ex;->b:Landroid/content/ContentResolver;

    .line 797737
    return-void
.end method

.method private a(Landroid/net/Uri;)LX/1FL;
    .locals 8
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 797738
    iget-object v0, p0, LX/4ex;->b:Landroid/content/ContentResolver;

    sget-object v2, LX/4ex;->a:[Ljava/lang/String;

    move-object v1, p1

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 797739
    if-nez v1, :cond_0

    .line 797740
    :goto_0
    return-object v3

    .line 797741
    :cond_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 797742
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 797743
    :cond_1
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 797744
    const-string v0, "_data"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 797745
    if-eqz v0, :cond_2

    .line 797746
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, v0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 797747
    if-nez v0, :cond_3

    const/4 v6, -0x1

    :goto_1
    move v0, v6

    .line 797748
    invoke-virtual {p0, v2, v0}, LX/4ep;->b(Ljava/io/InputStream;I)LX/1FL;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v3

    .line 797749
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_3
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/File;->length()J

    move-result-wide v6

    long-to-int v6, v6

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/1bf;)LX/1FL;
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 797750
    iget-object v0, p1, LX/1bf;->b:Landroid/net/Uri;

    move-object v1, v0

    .line 797751
    invoke-static {v1}, LX/1be;->c(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "com.android.contacts"

    invoke-virtual {v1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    sget-object v2, LX/1be;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 797752
    if-eqz v0, :cond_3

    .line 797753
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "/photo"

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 797754
    iget-object v0, p0, LX/4ex;->b:Landroid/content/ContentResolver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v0

    .line 797755
    :cond_0
    invoke-virtual {p0, v0, v3}, LX/4ep;->b(Ljava/io/InputStream;I)LX/1FL;

    move-result-object v0

    .line 797756
    :cond_1
    :goto_1
    return-object v0

    .line 797757
    :cond_2
    iget-object v0, p0, LX/4ex;->b:Landroid/content/ContentResolver;

    invoke-static {v0, v1}, Landroid/provider/ContactsContract$Contacts;->openContactPhotoInputStream(Landroid/content/ContentResolver;Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v0

    .line 797758
    if-nez v0, :cond_0

    .line 797759
    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Contact photo does not exist: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 797760
    :cond_3
    invoke-static {v1}, LX/1be;->e(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 797761
    invoke-direct {p0, v1}, LX/4ex;->a(Landroid/net/Uri;)LX/1FL;

    move-result-object v0

    .line 797762
    if-nez v0, :cond_1

    .line 797763
    :cond_4
    iget-object v0, p0, LX/4ex;->b:Landroid/content/ContentResolver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v0

    invoke-virtual {p0, v0, v3}, LX/4ep;->b(Ljava/io/InputStream;I)LX/1FL;

    move-result-object v0

    goto :goto_1

    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 797764
    const-string v0, "LocalContentUriFetchProducer"

    return-object v0
.end method
