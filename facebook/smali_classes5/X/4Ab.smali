.class public LX/4Ab;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/4Aa;

.field public b:Z

.field public c:[F

.field public d:I

.field public e:F

.field public f:I

.field public g:F


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 676616
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 676617
    sget-object v0, LX/4Aa;->BITMAP_ONLY:LX/4Aa;

    iput-object v0, p0, LX/4Ab;->a:LX/4Aa;

    .line 676618
    iput-boolean v1, p0, LX/4Ab;->b:Z

    .line 676619
    const/4 v0, 0x0

    iput-object v0, p0, LX/4Ab;->c:[F

    .line 676620
    iput v1, p0, LX/4Ab;->d:I

    .line 676621
    iput v2, p0, LX/4Ab;->e:F

    .line 676622
    iput v1, p0, LX/4Ab;->f:I

    .line 676623
    iput v2, p0, LX/4Ab;->g:F

    return-void
.end method

.method public static b(F)LX/4Ab;
    .locals 1

    .prologue
    .line 676629
    new-instance v0, LX/4Ab;

    invoke-direct {v0}, LX/4Ab;-><init>()V

    invoke-virtual {v0, p0}, LX/4Ab;->a(F)LX/4Ab;

    move-result-object v0

    return-object v0
.end method

.method public static b(FFFF)LX/4Ab;
    .locals 1

    .prologue
    .line 676628
    new-instance v0, LX/4Ab;

    invoke-direct {v0}, LX/4Ab;-><init>()V

    invoke-virtual {v0, p0, p1, p2, p3}, LX/4Ab;->a(FFFF)LX/4Ab;

    move-result-object v0

    return-object v0
.end method

.method public static e()LX/4Ab;
    .locals 2

    .prologue
    .line 676624
    new-instance v0, LX/4Ab;

    invoke-direct {v0}, LX/4Ab;-><init>()V

    const/4 v1, 0x1

    .line 676625
    iput-boolean v1, v0, LX/4Ab;->b:Z

    .line 676626
    move-object v0, v0

    .line 676627
    return-object v0
.end method

.method private i()[F
    .locals 1

    .prologue
    .line 676613
    iget-object v0, p0, LX/4Ab;->c:[F

    if-nez v0, :cond_0

    .line 676614
    const/16 v0, 0x8

    new-array v0, v0, [F

    iput-object v0, p0, LX/4Ab;->c:[F

    .line 676615
    :cond_0
    iget-object v0, p0, LX/4Ab;->c:[F

    return-object v0
.end method


# virtual methods
.method public final a(F)LX/4Ab;
    .locals 1

    .prologue
    .line 676611
    invoke-direct {p0}, LX/4Ab;->i()[F

    move-result-object v0

    invoke-static {v0, p1}, Ljava/util/Arrays;->fill([FF)V

    .line 676612
    return-object p0
.end method

.method public final a(FFFF)LX/4Ab;
    .locals 3

    .prologue
    .line 676630
    invoke-direct {p0}, LX/4Ab;->i()[F

    move-result-object v0

    .line 676631
    const/4 v1, 0x0

    const/4 v2, 0x1

    aput p1, v0, v2

    aput p1, v0, v1

    .line 676632
    const/4 v1, 0x2

    const/4 v2, 0x3

    aput p2, v0, v2

    aput p2, v0, v1

    .line 676633
    const/4 v1, 0x4

    const/4 v2, 0x5

    aput p3, v0, v2

    aput p3, v0, v1

    .line 676634
    const/4 v1, 0x6

    const/4 v2, 0x7

    aput p4, v0, v2

    aput p4, v0, v1

    .line 676635
    return-object p0
.end method

.method public final a(I)LX/4Ab;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 676608
    iput p1, p0, LX/4Ab;->d:I

    .line 676609
    sget-object v0, LX/4Aa;->OVERLAY_COLOR:LX/4Aa;

    iput-object v0, p0, LX/4Ab;->a:LX/4Aa;

    .line 676610
    return-object p0
.end method

.method public final a(IF)LX/4Ab;
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 676603
    const/4 v0, 0x0

    cmpl-float v0, p2, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "the border width cannot be < 0"

    invoke-static {v0, v1}, LX/03g;->a(ZLjava/lang/Object;)V

    .line 676604
    iput p2, p0, LX/4Ab;->e:F

    .line 676605
    iput p1, p0, LX/4Ab;->f:I

    .line 676606
    return-object p0

    .line 676607
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(F)LX/4Ab;
    .locals 2

    .prologue
    .line 676571
    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "the border width cannot be < 0"

    invoke-static {v0, v1}, LX/03g;->a(ZLjava/lang/Object;)V

    .line 676572
    iput p1, p0, LX/4Ab;->e:F

    .line 676573
    return-object p0

    .line 676574
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(F)LX/4Ab;
    .locals 2

    .prologue
    .line 676599
    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "the padding cannot be < 0"

    invoke-static {v0, v1}, LX/03g;->a(ZLjava/lang/Object;)V

    .line 676600
    iput p1, p0, LX/4Ab;->g:F

    .line 676601
    return-object p0

    .line 676602
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 676587
    if-ne p0, p1, :cond_1

    .line 676588
    const/4 v0, 0x1

    .line 676589
    :cond_0
    :goto_0
    return v0

    .line 676590
    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-ne v1, v2, :cond_0

    .line 676591
    check-cast p1, LX/4Ab;

    .line 676592
    iget-boolean v1, p0, LX/4Ab;->b:Z

    iget-boolean v2, p1, LX/4Ab;->b:Z

    if-ne v1, v2, :cond_0

    .line 676593
    iget v1, p0, LX/4Ab;->d:I

    iget v2, p1, LX/4Ab;->d:I

    if-ne v1, v2, :cond_0

    .line 676594
    iget v1, p1, LX/4Ab;->e:F

    iget v2, p0, LX/4Ab;->e:F

    invoke-static {v1, v2}, Ljava/lang/Float;->compare(FF)I

    move-result v1

    if-nez v1, :cond_0

    .line 676595
    iget v1, p0, LX/4Ab;->f:I

    iget v2, p1, LX/4Ab;->f:I

    if-ne v1, v2, :cond_0

    .line 676596
    iget v1, p1, LX/4Ab;->g:F

    iget v2, p0, LX/4Ab;->g:F

    invoke-static {v1, v2}, Ljava/lang/Float;->compare(FF)I

    move-result v1

    if-nez v1, :cond_0

    .line 676597
    iget-object v1, p0, LX/4Ab;->a:LX/4Aa;

    iget-object v2, p1, LX/4Ab;->a:LX/4Aa;

    if-ne v1, v2, :cond_0

    .line 676598
    iget-object v0, p0, LX/4Ab;->c:[F

    iget-object v1, p1, LX/4Ab;->c:[F

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([F[F)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 676575
    iget-object v0, p0, LX/4Ab;->a:LX/4Aa;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/4Ab;->a:LX/4Aa;

    invoke-virtual {v0}, LX/4Aa;->hashCode()I

    move-result v0

    .line 676576
    :goto_0
    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v0, p0, LX/4Ab;->b:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    add-int/2addr v0, v2

    .line 676577
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, LX/4Ab;->c:[F

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/4Ab;->c:[F

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([F)I

    move-result v0

    :goto_2
    add-int/2addr v0, v2

    .line 676578
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, LX/4Ab;->d:I

    add-int/2addr v0, v2

    .line 676579
    mul-int/lit8 v2, v0, 0x1f

    iget v0, p0, LX/4Ab;->e:F

    cmpl-float v0, v0, v3

    if-eqz v0, :cond_4

    iget v0, p0, LX/4Ab;->e:F

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    :goto_3
    add-int/2addr v0, v2

    .line 676580
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, LX/4Ab;->f:I

    add-int/2addr v0, v2

    .line 676581
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, LX/4Ab;->g:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_0

    iget v1, p0, LX/4Ab;->g:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    .line 676582
    return v0

    :cond_1
    move v0, v1

    .line 676583
    goto :goto_0

    :cond_2
    move v0, v1

    .line 676584
    goto :goto_1

    :cond_3
    move v0, v1

    .line 676585
    goto :goto_2

    :cond_4
    move v0, v1

    .line 676586
    goto :goto_3
.end method
