.class public LX/3xT;
.super LX/1OR;
.source ""


# instance fields
.field private A:Z

.field private final B:Ljava/lang/Runnable;

.field public a:LX/1P5;

.field public b:LX/1P5;

.field public c:Z

.field public d:I

.field public e:I

.field public f:LX/3xQ;

.field public g:I

.field public h:[LX/3xS;

.field public i:I

.field public j:I

.field private k:LX/3wv;

.field public l:Z

.field public m:Ljava/util/BitSet;

.field private n:I

.field public o:Z

.field public p:Z

.field public t:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

.field public u:I

.field public v:I

.field public w:I

.field private final x:Landroid/graphics/Rect;

.field private final y:LX/3xN;

.field private z:Z


# direct methods
.method public constructor <init>(II)V
    .locals 3

    .prologue
    const/4 v0, -0x1

    const/4 v1, 0x0

    .line 658301
    invoke-direct {p0}, LX/1OR;-><init>()V

    .line 658302
    iput v0, p0, LX/3xT;->g:I

    .line 658303
    iput-boolean v1, p0, LX/3xT;->l:Z

    .line 658304
    iput-boolean v1, p0, LX/3xT;->c:Z

    .line 658305
    iput v0, p0, LX/3xT;->d:I

    .line 658306
    const/high16 v0, -0x80000000

    iput v0, p0, LX/3xT;->e:I

    .line 658307
    new-instance v0, LX/3xQ;

    invoke-direct {v0}, LX/3xQ;-><init>()V

    iput-object v0, p0, LX/3xT;->f:LX/3xQ;

    .line 658308
    const/4 v0, 0x2

    iput v0, p0, LX/3xT;->n:I

    .line 658309
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/3xT;->x:Landroid/graphics/Rect;

    .line 658310
    new-instance v0, LX/3xN;

    invoke-direct {v0, p0}, LX/3xN;-><init>(LX/3xT;)V

    iput-object v0, p0, LX/3xT;->y:LX/3xN;

    .line 658311
    iput-boolean v1, p0, LX/3xT;->z:Z

    .line 658312
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/3xT;->A:Z

    .line 658313
    new-instance v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$1;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager$1;-><init>(LX/3xT;)V

    iput-object v0, p0, LX/3xT;->B:Ljava/lang/Runnable;

    .line 658314
    iput p2, p0, LX/3xT;->i:I

    .line 658315
    const/4 v1, 0x0

    .line 658316
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/1OR;->a(Ljava/lang/String;)V

    .line 658317
    iget v0, p0, LX/3xT;->g:I

    if-eq p1, v0, :cond_1

    .line 658318
    iget-object v0, p0, LX/3xT;->f:LX/3xQ;

    invoke-virtual {v0}, LX/3xQ;->a()V

    .line 658319
    invoke-virtual {p0}, LX/1OR;->p()V

    .line 658320
    iput p1, p0, LX/3xT;->g:I

    .line 658321
    new-instance v0, Ljava/util/BitSet;

    iget v2, p0, LX/3xT;->g:I

    invoke-direct {v0, v2}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/3xT;->m:Ljava/util/BitSet;

    .line 658322
    iget v0, p0, LX/3xT;->g:I

    new-array v0, v0, [LX/3xS;

    iput-object v0, p0, LX/3xT;->h:[LX/3xS;

    move v0, v1

    .line 658323
    :goto_0
    iget v2, p0, LX/3xT;->g:I

    if-ge v0, v2, :cond_0

    .line 658324
    iget-object v2, p0, LX/3xT;->h:[LX/3xS;

    new-instance p2, LX/3xS;

    invoke-direct {p2, p0, v0}, LX/3xS;-><init>(LX/3xT;I)V

    aput-object p2, v2, v0

    .line 658325
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 658326
    :cond_0
    invoke-virtual {p0}, LX/1OR;->p()V

    .line 658327
    :cond_1
    return-void
.end method

.method private I()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/high16 v5, -0x80000000

    .line 658203
    iget-object v0, p0, LX/3xT;->h:[LX/3xS;

    aget-object v0, v0, v2

    invoke-virtual {v0, v5}, LX/3xS;->b(I)I

    move-result v3

    move v0, v1

    .line 658204
    :goto_0
    iget v4, p0, LX/3xT;->g:I

    if-ge v0, v4, :cond_0

    .line 658205
    iget-object v4, p0, LX/3xT;->h:[LX/3xS;

    aget-object v4, v4, v0

    invoke-virtual {v4, v5}, LX/3xS;->b(I)I

    move-result v4

    if-eq v4, v3, :cond_1

    move v1, v2

    .line 658206
    :cond_0
    return v1

    .line 658207
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private J()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/high16 v5, -0x80000000

    .line 658296
    iget-object v0, p0, LX/3xT;->h:[LX/3xS;

    aget-object v0, v0, v2

    invoke-virtual {v0, v5}, LX/3xS;->a(I)I

    move-result v3

    move v0, v1

    .line 658297
    :goto_0
    iget v4, p0, LX/3xT;->g:I

    if-ge v0, v4, :cond_0

    .line 658298
    iget-object v4, p0, LX/3xT;->h:[LX/3xS;

    aget-object v4, v4, v0

    invoke-virtual {v4, v5}, LX/3xS;->a(I)I

    move-result v4

    if-eq v4, v3, :cond_1

    move v1, v2

    .line 658299
    :cond_0
    return v1

    .line 658300
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static K(LX/3xT;)I
    .locals 1

    .prologue
    .line 658294
    invoke-virtual {p0}, LX/1OR;->v()I

    move-result v0

    .line 658295
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, LX/1OR;->f(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, LX/1OR;->d(Landroid/view/View;)I

    move-result v0

    goto :goto_0
.end method

.method public static L(LX/3xT;)I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 658292
    invoke-virtual {p0}, LX/1OR;->v()I

    move-result v1

    .line 658293
    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, v0}, LX/1OR;->f(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, LX/1OR;->d(Landroid/view/View;)I

    move-result v0

    goto :goto_0
.end method

.method private static a(III)I
    .locals 2

    .prologue
    .line 658287
    if-nez p1, :cond_1

    if-nez p2, :cond_1

    .line 658288
    :cond_0
    :goto_0
    return p0

    .line 658289
    :cond_1
    invoke-static {p0}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    .line 658290
    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_2

    const/high16 v1, 0x40000000    # 2.0f

    if-ne v0, v1, :cond_0

    .line 658291
    :cond_2
    invoke-static {p0}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    sub-int/2addr v1, p1

    sub-int/2addr v1, p2

    invoke-static {v1, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p0

    goto :goto_0
.end method

.method private a(LX/1Od;LX/3wv;LX/1Ok;)I
    .locals 18

    .prologue
    .line 658212
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3xT;->m:Ljava/util/BitSet;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget v4, v0, LX/3xT;->g:I

    const/4 v5, 0x1

    invoke-virtual {v2, v3, v4, v5}, Ljava/util/BitSet;->set(IIZ)V

    .line 658213
    move-object/from16 v0, p2

    iget v2, v0, LX/3wv;->d:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    .line 658214
    move-object/from16 v0, p2

    iget v2, v0, LX/3wv;->f:I

    move-object/from16 v0, p2

    iget v3, v0, LX/3wv;->a:I

    add-int/2addr v2, v3

    move v15, v2

    .line 658215
    :goto_0
    move-object/from16 v0, p2

    iget v2, v0, LX/3wv;->d:I

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v15}, LX/3xT;->f(II)V

    .line 658216
    move-object/from16 v0, p0

    iget-boolean v2, v0, LX/3xT;->c:Z

    if-eqz v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, LX/3xT;->a:LX/1P5;

    invoke-virtual {v2}, LX/1P5;->d()I

    move-result v2

    move/from16 v16, v2

    .line 658217
    :goto_1
    const/4 v2, 0x0

    .line 658218
    :goto_2
    invoke-virtual/range {p2 .. p3}, LX/3wv;->a(LX/1Ok;)Z

    move-result v3

    if-eqz v3, :cond_12

    move-object/from16 v0, p0

    iget-object v3, v0, LX/3xT;->m:Ljava/util/BitSet;

    invoke-virtual {v3}, Ljava/util/BitSet;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_12

    .line 658219
    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, LX/3wv;->a(LX/1Od;)Landroid/view/View;

    move-result-object v3

    .line 658220
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    move-object v14, v2

    check-cast v14, LX/3xO;

    .line 658221
    invoke-virtual {v14}, LX/1a3;->e()I

    move-result v6

    .line 658222
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3xT;->f:LX/3xQ;

    invoke-virtual {v2, v6}, LX/3xQ;->c(I)I

    move-result v5

    .line 658223
    const/4 v2, -0x1

    if-ne v5, v2, :cond_4

    const/4 v2, 0x1

    move v4, v2

    .line 658224
    :goto_3
    if-eqz v4, :cond_6

    .line 658225
    iget-boolean v2, v14, LX/3xO;->f:Z

    if-eqz v2, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, LX/3xT;->h:[LX/3xS;

    const/4 v5, 0x0

    aget-object v2, v2, v5

    .line 658226
    :goto_4
    move-object/from16 v0, p0

    iget-object v5, v0, LX/3xT;->f:LX/3xQ;

    invoke-virtual {v5, v6, v2}, LX/3xQ;->a(ILX/3xS;)V

    move-object/from16 v17, v2

    .line 658227
    :goto_5
    move-object/from16 v0, v17

    iput-object v0, v14, LX/3xO;->e:LX/3xS;

    .line 658228
    move-object/from16 v0, p2

    iget v2, v0, LX/3wv;->d:I

    const/4 v5, 0x1

    if-ne v2, v5, :cond_7

    .line 658229
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, LX/1OR;->b(Landroid/view/View;)V

    .line 658230
    :goto_6
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v14}, LX/3xT;->a(Landroid/view/View;LX/3xO;)V

    .line 658231
    move-object/from16 v0, p2

    iget v2, v0, LX/3wv;->d:I

    const/4 v5, 0x1

    if-ne v2, v5, :cond_9

    .line 658232
    iget-boolean v2, v14, LX/3xO;->f:Z

    if-eqz v2, :cond_8

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v1}, LX/3xT;->m(I)I

    move-result v2

    .line 658233
    :goto_7
    move-object/from16 v0, p0

    iget-object v5, v0, LX/3xT;->a:LX/1P5;

    invoke-virtual {v5, v3}, LX/1P5;->c(Landroid/view/View;)I

    move-result v5

    add-int v7, v2, v5

    .line 658234
    if-eqz v4, :cond_16

    iget-boolean v5, v14, LX/3xO;->f:Z

    if-eqz v5, :cond_16

    .line 658235
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, LX/3xT;->d(I)Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;

    move-result-object v5

    .line 658236
    const/4 v8, -0x1

    iput v8, v5, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;->b:I

    .line 658237
    iput v6, v5, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;->a:I

    .line 658238
    move-object/from16 v0, p0

    iget-object v8, v0, LX/3xT;->f:LX/3xQ;

    invoke-virtual {v8, v5}, LX/3xQ;->a(Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;)V

    move v5, v2

    .line 658239
    :goto_8
    iget-boolean v2, v14, LX/3xO;->f:Z

    if-eqz v2, :cond_1

    move-object/from16 v0, p2

    iget v2, v0, LX/3wv;->c:I

    const/4 v8, -0x1

    if-ne v2, v8, :cond_1

    .line 658240
    if-nez v4, :cond_0

    .line 658241
    move-object/from16 v0, p2

    iget v2, v0, LX/3wv;->d:I

    const/4 v4, 0x1

    if-ne v2, v4, :cond_d

    .line 658242
    invoke-direct/range {p0 .. p0}, LX/3xT;->I()Z

    move-result v2

    if-nez v2, :cond_c

    const/4 v2, 0x1

    .line 658243
    :goto_9
    if-eqz v2, :cond_1

    .line 658244
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3xT;->f:LX/3xQ;

    invoke-virtual {v2, v6}, LX/3xQ;->d(I)Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;

    move-result-object v2

    .line 658245
    if-eqz v2, :cond_0

    .line 658246
    const/4 v4, 0x1

    iput-boolean v4, v2, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;->d:Z

    .line 658247
    :cond_0
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, LX/3xT;->z:Z

    .line 658248
    :cond_1
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v3, v14, v1}, LX/3xT;->a(Landroid/view/View;LX/3xO;LX/3wv;)V

    .line 658249
    iget-boolean v2, v14, LX/3xO;->f:Z

    if-eqz v2, :cond_f

    move-object/from16 v0, p0

    iget-object v2, v0, LX/3xT;->b:LX/1P5;

    invoke-virtual {v2}, LX/1P5;->c()I

    move-result v4

    .line 658250
    :goto_a
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3xT;->b:LX/1P5;

    invoke-virtual {v2, v3}, LX/1P5;->c(Landroid/view/View;)I

    move-result v2

    add-int v6, v4, v2

    .line 658251
    move-object/from16 v0, p0

    iget v2, v0, LX/3xT;->i:I

    const/4 v8, 0x1

    if-ne v2, v8, :cond_10

    move-object/from16 v2, p0

    .line 658252
    invoke-direct/range {v2 .. v7}, LX/3xT;->b(Landroid/view/View;IIII)V

    .line 658253
    :goto_b
    iget-boolean v2, v14, LX/3xO;->f:Z

    if-eqz v2, :cond_11

    .line 658254
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3xT;->k:LX/3wv;

    iget v2, v2, LX/3wv;->d:I

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v15}, LX/3xT;->f(II)V

    .line 658255
    :goto_c
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3xT;->k:LX/3wv;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v2}, LX/3xT;->a(LX/1Od;LX/3wv;)V

    .line 658256
    const/4 v2, 0x1

    .line 658257
    goto/16 :goto_2

    .line 658258
    :cond_2
    move-object/from16 v0, p2

    iget v2, v0, LX/3wv;->e:I

    move-object/from16 v0, p2

    iget v3, v0, LX/3wv;->a:I

    sub-int/2addr v2, v3

    move v15, v2

    goto/16 :goto_0

    .line 658259
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3xT;->a:LX/1P5;

    invoke-virtual {v2}, LX/1P5;->c()I

    move-result v2

    move/from16 v16, v2

    goto/16 :goto_1

    .line 658260
    :cond_4
    const/4 v2, 0x0

    move v4, v2

    goto/16 :goto_3

    .line 658261
    :cond_5
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, LX/3xT;->a(LX/3wv;)LX/3xS;

    move-result-object v2

    goto/16 :goto_4

    .line 658262
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3xT;->h:[LX/3xS;

    aget-object v2, v2, v5

    move-object/from16 v17, v2

    goto/16 :goto_5

    .line 658263
    :cond_7
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v2}, LX/1OR;->b(Landroid/view/View;I)V

    goto/16 :goto_6

    .line 658264
    :cond_8
    move-object/from16 v0, v17

    move/from16 v1, v16

    invoke-virtual {v0, v1}, LX/3xS;->b(I)I

    move-result v2

    goto/16 :goto_7

    .line 658265
    :cond_9
    iget-boolean v2, v14, LX/3xO;->f:Z

    if-eqz v2, :cond_b

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v1}, LX/3xT;->l(I)I

    move-result v2

    .line 658266
    :goto_d
    move-object/from16 v0, p0

    iget-object v5, v0, LX/3xT;->a:LX/1P5;

    invoke-virtual {v5, v3}, LX/1P5;->c(Landroid/view/View;)I

    move-result v5

    sub-int v5, v2, v5

    .line 658267
    if-eqz v4, :cond_a

    iget-boolean v7, v14, LX/3xO;->f:Z

    if-eqz v7, :cond_a

    .line 658268
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, LX/3xT;->j(I)Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;

    move-result-object v7

    .line 658269
    const/4 v8, 0x1

    iput v8, v7, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;->b:I

    .line 658270
    iput v6, v7, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;->a:I

    .line 658271
    move-object/from16 v0, p0

    iget-object v8, v0, LX/3xT;->f:LX/3xQ;

    invoke-virtual {v8, v7}, LX/3xQ;->a(Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;)V

    :cond_a
    move v7, v2

    goto/16 :goto_8

    .line 658272
    :cond_b
    move-object/from16 v0, v17

    move/from16 v1, v16

    invoke-virtual {v0, v1}, LX/3xS;->a(I)I

    move-result v2

    goto :goto_d

    .line 658273
    :cond_c
    const/4 v2, 0x0

    goto/16 :goto_9

    .line 658274
    :cond_d
    invoke-direct/range {p0 .. p0}, LX/3xT;->J()Z

    move-result v2

    if-nez v2, :cond_e

    const/4 v2, 0x1

    goto/16 :goto_9

    :cond_e
    const/4 v2, 0x0

    goto/16 :goto_9

    .line 658275
    :cond_f
    move-object/from16 v0, v17

    iget v2, v0, LX/3xS;->d:I

    move-object/from16 v0, p0

    iget v4, v0, LX/3xT;->j:I

    mul-int/2addr v2, v4

    move-object/from16 v0, p0

    iget-object v4, v0, LX/3xT;->b:LX/1P5;

    invoke-virtual {v4}, LX/1P5;->c()I

    move-result v4

    add-int/2addr v4, v2

    goto/16 :goto_a

    :cond_10
    move-object/from16 v8, p0

    move-object v9, v3

    move v10, v5

    move v11, v4

    move v12, v7

    move v13, v6

    .line 658276
    invoke-direct/range {v8 .. v13}, LX/3xT;->b(Landroid/view/View;IIII)V

    goto/16 :goto_b

    .line 658277
    :cond_11
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3xT;->k:LX/3wv;

    iget v2, v2, LX/3wv;->d:I

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1, v2, v15}, LX/3xT;->a(LX/3xS;II)V

    goto/16 :goto_c

    .line 658278
    :cond_12
    if-nez v2, :cond_13

    .line 658279
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3xT;->k:LX/3wv;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v2}, LX/3xT;->a(LX/1Od;LX/3wv;)V

    .line 658280
    :cond_13
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3xT;->k:LX/3wv;

    iget v2, v2, LX/3wv;->d:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_14

    .line 658281
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3xT;->a:LX/1P5;

    invoke-virtual {v2}, LX/1P5;->c()I

    move-result v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, LX/3xT;->l(I)I

    move-result v2

    .line 658282
    move-object/from16 v0, p0

    iget-object v3, v0, LX/3xT;->a:LX/1P5;

    invoke-virtual {v3}, LX/1P5;->c()I

    move-result v3

    sub-int v2, v3, v2

    .line 658283
    :goto_e
    if-lez v2, :cond_15

    move-object/from16 v0, p2

    iget v3, v0, LX/3wv;->a:I

    invoke-static {v3, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    :goto_f
    return v2

    .line 658284
    :cond_14
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3xT;->a:LX/1P5;

    invoke-virtual {v2}, LX/1P5;->d()I

    move-result v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, LX/3xT;->m(I)I

    move-result v2

    .line 658285
    move-object/from16 v0, p0

    iget-object v3, v0, LX/3xT;->a:LX/1P5;

    invoke-virtual {v3}, LX/1P5;->d()I

    move-result v3

    sub-int/2addr v2, v3

    goto :goto_e

    .line 658286
    :cond_15
    const/4 v2, 0x0

    goto :goto_f

    :cond_16
    move v5, v2

    goto/16 :goto_8
.end method

.method private a(LX/1Ok;)I
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 658208
    invoke-virtual {p0}, LX/1OR;->v()I

    move-result v0

    if-nez v0, :cond_0

    .line 658209
    :goto_0
    return v4

    .line 658210
    :cond_0
    invoke-direct {p0}, LX/3xT;->k()V

    .line 658211
    iget-object v1, p0, LX/3xT;->a:LX/1P5;

    iget-boolean v0, p0, LX/3xT;->A:Z

    if-nez v0, :cond_2

    move v0, v3

    :goto_1
    invoke-static {p0, v0, v3}, LX/3xT;->a(LX/3xT;ZZ)Landroid/view/View;

    move-result-object v2

    iget-boolean v0, p0, LX/3xT;->A:Z

    if-nez v0, :cond_1

    move v4, v3

    :cond_1
    invoke-static {p0, v4, v3}, LX/3xT;->b(LX/3xT;ZZ)Landroid/view/View;

    move-result-object v3

    iget-boolean v5, p0, LX/3xT;->A:Z

    iget-boolean v6, p0, LX/3xT;->c:Z

    move-object v0, p1

    move-object v4, p0

    invoke-static/range {v0 .. v6}, LX/2fZ;->a(LX/1Ok;LX/1P5;Landroid/view/View;Landroid/view/View;LX/1OR;ZZ)I

    move-result v4

    goto :goto_0

    :cond_2
    move v0, v4

    goto :goto_1
.end method

.method private a(LX/3wv;)LX/3xS;
    .locals 8

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 657997
    iget v2, p1, LX/3wv;->d:I

    const/4 v7, -0x1

    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 657998
    iget v3, p0, LX/3xT;->i:I

    if-nez v3, :cond_8

    .line 657999
    if-ne v2, v7, :cond_6

    move v3, v4

    :goto_0
    iget-boolean v7, p0, LX/3xT;->c:Z

    if-eq v3, v7, :cond_7

    .line 658000
    :cond_0
    :goto_1
    move v2, v4

    .line 658001
    if-eqz v2, :cond_1

    .line 658002
    iget v2, p0, LX/3xT;->g:I

    add-int/lit8 v2, v2, -0x1

    move v3, v2

    move v2, v0

    .line 658003
    :goto_2
    iget v4, p1, LX/3wv;->d:I

    if-ne v4, v1, :cond_2

    .line 658004
    const v1, 0x7fffffff

    .line 658005
    iget-object v4, p0, LX/3xT;->a:LX/1P5;

    invoke-virtual {v4}, LX/1P5;->c()I

    move-result v7

    move v6, v3

    move v3, v1

    .line 658006
    :goto_3
    if-eq v6, v2, :cond_3

    .line 658007
    iget-object v1, p0, LX/3xT;->h:[LX/3xS;

    aget-object v4, v1, v6

    .line 658008
    invoke-virtual {v4, v7}, LX/3xS;->b(I)I

    move-result v1

    .line 658009
    if-ge v1, v3, :cond_5

    move-object v3, v4

    .line 658010
    :goto_4
    add-int v4, v6, v0

    move v6, v4

    move-object v5, v3

    move v3, v1

    goto :goto_3

    .line 658011
    :cond_1
    const/4 v2, 0x0

    .line 658012
    iget v0, p0, LX/3xT;->g:I

    move v3, v2

    move v2, v0

    move v0, v1

    .line 658013
    goto :goto_2

    .line 658014
    :cond_2
    const/high16 v1, -0x80000000

    .line 658015
    iget-object v4, p0, LX/3xT;->a:LX/1P5;

    invoke-virtual {v4}, LX/1P5;->d()I

    move-result v7

    move v6, v3

    move v3, v1

    .line 658016
    :goto_5
    if-eq v6, v2, :cond_3

    .line 658017
    iget-object v1, p0, LX/3xT;->h:[LX/3xS;

    aget-object v4, v1, v6

    .line 658018
    invoke-virtual {v4, v7}, LX/3xS;->a(I)I

    move-result v1

    .line 658019
    if-le v1, v3, :cond_4

    move-object v3, v4

    .line 658020
    :goto_6
    add-int v4, v6, v0

    move v6, v4

    move-object v5, v3

    move v3, v1

    goto :goto_5

    .line 658021
    :cond_3
    return-object v5

    :cond_4
    move v1, v3

    move-object v3, v5

    goto :goto_6

    :cond_5
    move v1, v3

    move-object v3, v5

    goto :goto_4

    :cond_6
    move v3, v6

    .line 658022
    goto :goto_0

    :cond_7
    move v4, v6

    goto :goto_1

    .line 658023
    :cond_8
    if-ne v2, v7, :cond_9

    move v3, v4

    :goto_7
    iget-boolean v7, p0, LX/3xT;->c:Z

    if-ne v3, v7, :cond_a

    move v3, v4

    :goto_8
    invoke-static {p0}, LX/3xT;->m(LX/3xT;)Z

    move-result v7

    if-eq v3, v7, :cond_0

    move v4, v6

    goto :goto_1

    :cond_9
    move v3, v6

    goto :goto_7

    :cond_a
    move v3, v6

    goto :goto_8
.end method

.method public static a(LX/3xT;ZZ)Landroid/view/View;
    .locals 8

    .prologue
    .line 658188
    invoke-direct {p0}, LX/3xT;->k()V

    .line 658189
    iget-object v0, p0, LX/3xT;->a:LX/1P5;

    invoke-virtual {v0}, LX/1P5;->c()I

    move-result v3

    .line 658190
    iget-object v0, p0, LX/3xT;->a:LX/1P5;

    invoke-virtual {v0}, LX/1P5;->d()I

    move-result v4

    .line 658191
    invoke-virtual {p0}, LX/1OR;->v()I

    move-result v5

    .line 658192
    const/4 v1, 0x0

    .line 658193
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v5, :cond_2

    .line 658194
    invoke-virtual {p0, v2}, LX/1OR;->f(I)Landroid/view/View;

    move-result-object v0

    .line 658195
    iget-object v6, p0, LX/3xT;->a:LX/1P5;

    invoke-virtual {v6, v0}, LX/1P5;->a(Landroid/view/View;)I

    move-result v6

    .line 658196
    iget-object v7, p0, LX/3xT;->a:LX/1P5;

    invoke-virtual {v7, v0}, LX/1P5;->b(Landroid/view/View;)I

    move-result v7

    .line 658197
    if-le v7, v3, :cond_3

    if-ge v6, v4, :cond_3

    .line 658198
    if-ge v6, v3, :cond_0

    if-nez p1, :cond_1

    .line 658199
    :cond_0
    :goto_1
    return-object v0

    .line 658200
    :cond_1
    if-eqz p2, :cond_3

    if-nez v1, :cond_3

    .line 658201
    :goto_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 658202
    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2
.end method

.method private a(ILX/1Ok;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 658172
    iget-object v0, p0, LX/3xT;->k:LX/3wv;

    iput v1, v0, LX/3wv;->a:I

    .line 658173
    iget-object v0, p0, LX/3xT;->k:LX/3wv;

    iput p1, v0, LX/3wv;->b:I

    .line 658174
    invoke-virtual {p0}, LX/1OR;->s()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 658175
    iget v0, p2, LX/1Ok;->f:I

    move v0, v0

    .line 658176
    const/4 v2, -0x1

    if-eq v0, v2, :cond_3

    .line 658177
    iget-boolean v2, p0, LX/3xT;->c:Z

    if-ge v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-ne v2, v0, :cond_1

    .line 658178
    iget-object v0, p0, LX/3xT;->a:LX/1P5;

    invoke-virtual {v0}, LX/1P5;->f()I

    move-result v0

    .line 658179
    :goto_1
    invoke-virtual {p0}, LX/1OR;->r()Z

    move-result v2

    .line 658180
    if-eqz v2, :cond_2

    .line 658181
    iget-object v2, p0, LX/3xT;->k:LX/3wv;

    iget-object v3, p0, LX/3xT;->a:LX/1P5;

    invoke-virtual {v3}, LX/1P5;->c()I

    move-result v3

    sub-int v1, v3, v1

    iput v1, v2, LX/3wv;->e:I

    .line 658182
    iget-object v1, p0, LX/3xT;->k:LX/3wv;

    iget-object v2, p0, LX/3xT;->a:LX/1P5;

    invoke-virtual {v2}, LX/1P5;->d()I

    move-result v2

    add-int/2addr v0, v2

    iput v0, v1, LX/3wv;->f:I

    .line 658183
    :goto_2
    return-void

    :cond_0
    move v0, v1

    .line 658184
    goto :goto_0

    .line 658185
    :cond_1
    iget-object v0, p0, LX/3xT;->a:LX/1P5;

    invoke-virtual {v0}, LX/1P5;->f()I

    move-result v0

    move v4, v1

    move v1, v0

    move v0, v4

    goto :goto_1

    .line 658186
    :cond_2
    iget-object v2, p0, LX/3xT;->k:LX/3wv;

    iget-object v3, p0, LX/3xT;->a:LX/1P5;

    invoke-virtual {v3}, LX/1P5;->e()I

    move-result v3

    add-int/2addr v0, v3

    iput v0, v2, LX/3wv;->f:I

    .line 658187
    iget-object v0, p0, LX/3xT;->k:LX/3wv;

    neg-int v1, v1

    iput v1, v0, LX/3wv;->e:I

    goto :goto_2

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method private a(LX/1Od;I)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 658157
    :goto_0
    invoke-virtual {p0}, LX/1OR;->v()I

    move-result v0

    if-lez v0, :cond_0

    .line 658158
    invoke-virtual {p0, v1}, LX/1OR;->f(I)Landroid/view/View;

    move-result-object v2

    .line 658159
    iget-object v0, p0, LX/3xT;->a:LX/1P5;

    invoke-virtual {v0, v2}, LX/1P5;->b(Landroid/view/View;)I

    move-result v0

    if-gt v0, p2, :cond_0

    .line 658160
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/3xO;

    .line 658161
    iget-boolean v3, v0, LX/3xO;->f:Z

    if-eqz v3, :cond_3

    move v0, v1

    .line 658162
    :goto_1
    iget v3, p0, LX/3xT;->g:I

    if-ge v0, v3, :cond_2

    .line 658163
    iget-object v3, p0, LX/3xT;->h:[LX/3xS;

    aget-object v3, v3, v0

    iget-object v3, v3, LX/3xS;->f:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ne v3, v4, :cond_1

    .line 658164
    :cond_0
    return-void

    .line 658165
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move v0, v1

    .line 658166
    :goto_2
    iget v3, p0, LX/3xT;->g:I

    if-ge v0, v3, :cond_4

    .line 658167
    iget-object v3, p0, LX/3xT;->h:[LX/3xS;

    aget-object v3, v3, v0

    invoke-virtual {v3}, LX/3xS;->e()V

    .line 658168
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 658169
    :cond_3
    iget-object v3, v0, LX/3xO;->e:LX/3xS;

    iget-object v3, v3, LX/3xS;->f:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-eq v3, v4, :cond_0

    .line 658170
    iget-object v0, v0, LX/3xO;->e:LX/3xS;

    invoke-virtual {v0}, LX/3xS;->e()V

    .line 658171
    :cond_4
    invoke-virtual {p0, v2, p1}, LX/1OR;->a(Landroid/view/View;LX/1Od;)V

    goto :goto_0
.end method

.method private a(LX/1Od;LX/1Ok;Z)V
    .locals 2

    .prologue
    .line 658149
    iget-object v0, p0, LX/3xT;->a:LX/1P5;

    invoke-virtual {v0}, LX/1P5;->d()I

    move-result v0

    invoke-direct {p0, v0}, LX/3xT;->m(I)I

    move-result v0

    .line 658150
    iget-object v1, p0, LX/3xT;->a:LX/1P5;

    invoke-virtual {v1}, LX/1P5;->d()I

    move-result v1

    sub-int v0, v1, v0

    .line 658151
    if-lez v0, :cond_0

    .line 658152
    neg-int v1, v0

    invoke-direct {p0, v1, p1, p2}, LX/3xT;->d(ILX/1Od;LX/1Ok;)I

    move-result v1

    neg-int v1, v1

    .line 658153
    sub-int/2addr v0, v1

    .line 658154
    if-eqz p3, :cond_0

    if-lez v0, :cond_0

    .line 658155
    iget-object v1, p0, LX/3xT;->a:LX/1P5;

    invoke-virtual {v1, v0}, LX/1P5;->a(I)V

    .line 658156
    :cond_0
    return-void
.end method

.method private a(LX/1Od;LX/3wv;)V
    .locals 5

    .prologue
    const/4 v1, -0x1

    .line 658119
    iget v0, p2, LX/3wv;->a:I

    if-nez v0, :cond_1

    .line 658120
    iget v0, p2, LX/3wv;->d:I

    if-ne v0, v1, :cond_0

    .line 658121
    iget v0, p2, LX/3wv;->f:I

    invoke-direct {p0, p1, v0}, LX/3xT;->b(LX/1Od;I)V

    .line 658122
    :goto_0
    return-void

    .line 658123
    :cond_0
    iget v0, p2, LX/3wv;->e:I

    invoke-direct {p0, p1, v0}, LX/3xT;->a(LX/1Od;I)V

    goto :goto_0

    .line 658124
    :cond_1
    iget v0, p2, LX/3wv;->d:I

    if-ne v0, v1, :cond_5

    .line 658125
    iget v0, p2, LX/3wv;->e:I

    iget v1, p2, LX/3wv;->e:I

    .line 658126
    iget-object v2, p0, LX/3xT;->h:[LX/3xS;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    invoke-virtual {v2, v1}, LX/3xS;->a(I)I

    move-result v3

    .line 658127
    const/4 v2, 0x1

    :goto_1
    iget v4, p0, LX/3xT;->g:I

    if-ge v2, v4, :cond_3

    .line 658128
    iget-object v4, p0, LX/3xT;->h:[LX/3xS;

    aget-object v4, v4, v2

    invoke-virtual {v4, v1}, LX/3xS;->a(I)I

    move-result v4

    .line 658129
    if-le v4, v3, :cond_2

    move v3, v4

    .line 658130
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 658131
    :cond_3
    move v1, v3

    .line 658132
    sub-int/2addr v0, v1

    .line 658133
    if-gez v0, :cond_4

    .line 658134
    iget v0, p2, LX/3wv;->f:I

    .line 658135
    :goto_2
    invoke-direct {p0, p1, v0}, LX/3xT;->b(LX/1Od;I)V

    goto :goto_0

    .line 658136
    :cond_4
    iget v1, p2, LX/3wv;->f:I

    iget v2, p2, LX/3wv;->a:I

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    sub-int v0, v1, v0

    goto :goto_2

    .line 658137
    :cond_5
    iget v0, p2, LX/3wv;->f:I

    .line 658138
    iget-object v1, p0, LX/3xT;->h:[LX/3xS;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v1, v0}, LX/3xS;->b(I)I

    move-result v2

    .line 658139
    const/4 v1, 0x1

    :goto_3
    iget v3, p0, LX/3xT;->g:I

    if-ge v1, v3, :cond_7

    .line 658140
    iget-object v3, p0, LX/3xT;->h:[LX/3xS;

    aget-object v3, v3, v1

    invoke-virtual {v3, v0}, LX/3xS;->b(I)I

    move-result v3

    .line 658141
    if-ge v3, v2, :cond_6

    move v2, v3

    .line 658142
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 658143
    :cond_7
    move v0, v2

    .line 658144
    iget v1, p2, LX/3wv;->f:I

    sub-int/2addr v0, v1

    .line 658145
    if-gez v0, :cond_8

    .line 658146
    iget v0, p2, LX/3wv;->e:I

    .line 658147
    :goto_4
    invoke-direct {p0, p1, v0}, LX/3xT;->a(LX/1Od;I)V

    goto :goto_0

    .line 658148
    :cond_8
    iget v1, p2, LX/3wv;->e:I

    iget v2, p2, LX/3wv;->a:I

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    add-int/2addr v0, v1

    goto :goto_4
.end method

.method private a(LX/1Ok;LX/3xN;)V
    .locals 6

    .prologue
    .line 658054
    const/4 v0, 0x0

    const/4 v5, -0x1

    const/high16 v4, -0x80000000

    const/4 v1, 0x1

    .line 658055
    iget-boolean v2, p1, LX/1Ok;->k:Z

    move v2, v2

    .line 658056
    if-nez v2, :cond_0

    iget v2, p0, LX/3xT;->d:I

    if-ne v2, v5, :cond_2

    :cond_0
    move v1, v0

    .line 658057
    :goto_0
    move v0, v1

    .line 658058
    if-eqz v0, :cond_1

    .line 658059
    :goto_1
    return-void

    .line 658060
    :cond_1
    iget-boolean v0, p0, LX/3xT;->o:Z

    if-eqz v0, :cond_12

    invoke-virtual {p1}, LX/1Ok;->e()I

    move-result v0

    .line 658061
    invoke-virtual {p0}, LX/1OR;->v()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    move p1, v1

    :goto_2
    if-ltz p1, :cond_14

    .line 658062
    invoke-virtual {p0, p1}, LX/1OR;->f(I)Landroid/view/View;

    move-result-object v1

    .line 658063
    invoke-static {v1}, LX/1OR;->d(Landroid/view/View;)I

    move-result v1

    .line 658064
    if-ltz v1, :cond_13

    if-ge v1, v0, :cond_13

    .line 658065
    :goto_3
    move v0, v1

    .line 658066
    :goto_4
    iput v0, p2, LX/3xN;->a:I

    .line 658067
    const/high16 v0, -0x80000000

    iput v0, p2, LX/3xN;->b:I

    .line 658068
    goto :goto_1

    .line 658069
    :cond_2
    iget v2, p0, LX/3xT;->d:I

    if-ltz v2, :cond_3

    iget v2, p0, LX/3xT;->d:I

    invoke-virtual {p1}, LX/1Ok;->e()I

    move-result v3

    if-lt v2, v3, :cond_4

    .line 658070
    :cond_3
    iput v5, p0, LX/3xT;->d:I

    .line 658071
    iput v4, p0, LX/3xT;->e:I

    move v1, v0

    .line 658072
    goto :goto_0

    .line 658073
    :cond_4
    iget-object v2, p0, LX/3xT;->t:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/3xT;->t:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    iget v2, v2, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->a:I

    if-eq v2, v5, :cond_5

    iget-object v2, p0, LX/3xT;->t:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    iget v2, v2, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->c:I

    if-gtz v2, :cond_10

    .line 658074
    :cond_5
    iget v2, p0, LX/3xT;->d:I

    invoke-virtual {p0, v2}, LX/1OR;->c(I)Landroid/view/View;

    move-result-object v2

    .line 658075
    if-eqz v2, :cond_d

    .line 658076
    iget-boolean v0, p0, LX/3xT;->c:Z

    if-eqz v0, :cond_6

    invoke-static {p0}, LX/3xT;->K(LX/3xT;)I

    move-result v0

    :goto_5
    iput v0, p2, LX/3xN;->a:I

    .line 658077
    iget v0, p0, LX/3xT;->e:I

    if-eq v0, v4, :cond_8

    .line 658078
    iget-boolean v0, p2, LX/3xN;->c:Z

    if-eqz v0, :cond_7

    .line 658079
    iget-object v0, p0, LX/3xT;->a:LX/1P5;

    invoke-virtual {v0}, LX/1P5;->d()I

    move-result v0

    iget v3, p0, LX/3xT;->e:I

    sub-int/2addr v0, v3

    .line 658080
    iget-object v3, p0, LX/3xT;->a:LX/1P5;

    invoke-virtual {v3, v2}, LX/1P5;->b(Landroid/view/View;)I

    move-result v2

    sub-int/2addr v0, v2

    iput v0, p2, LX/3xN;->b:I

    goto :goto_0

    .line 658081
    :cond_6
    invoke-static {p0}, LX/3xT;->L(LX/3xT;)I

    move-result v0

    goto :goto_5

    .line 658082
    :cond_7
    iget-object v0, p0, LX/3xT;->a:LX/1P5;

    invoke-virtual {v0}, LX/1P5;->c()I

    move-result v0

    iget v3, p0, LX/3xT;->e:I

    add-int/2addr v0, v3

    .line 658083
    iget-object v3, p0, LX/3xT;->a:LX/1P5;

    invoke-virtual {v3, v2}, LX/1P5;->a(Landroid/view/View;)I

    move-result v2

    sub-int/2addr v0, v2

    iput v0, p2, LX/3xN;->b:I

    goto/16 :goto_0

    .line 658084
    :cond_8
    iget-object v0, p0, LX/3xT;->a:LX/1P5;

    invoke-virtual {v0, v2}, LX/1P5;->c(Landroid/view/View;)I

    move-result v0

    .line 658085
    iget-object v3, p0, LX/3xT;->a:LX/1P5;

    invoke-virtual {v3}, LX/1P5;->f()I

    move-result v3

    if-le v0, v3, :cond_a

    .line 658086
    iget-boolean v0, p2, LX/3xN;->c:Z

    if-eqz v0, :cond_9

    iget-object v0, p0, LX/3xT;->a:LX/1P5;

    invoke-virtual {v0}, LX/1P5;->d()I

    move-result v0

    :goto_6
    iput v0, p2, LX/3xN;->b:I

    goto/16 :goto_0

    :cond_9
    iget-object v0, p0, LX/3xT;->a:LX/1P5;

    invoke-virtual {v0}, LX/1P5;->c()I

    move-result v0

    goto :goto_6

    .line 658087
    :cond_a
    iget-object v0, p0, LX/3xT;->a:LX/1P5;

    invoke-virtual {v0, v2}, LX/1P5;->a(Landroid/view/View;)I

    move-result v0

    iget-object v3, p0, LX/3xT;->a:LX/1P5;

    invoke-virtual {v3}, LX/1P5;->c()I

    move-result v3

    sub-int/2addr v0, v3

    .line 658088
    if-gez v0, :cond_b

    .line 658089
    neg-int v0, v0

    iput v0, p2, LX/3xN;->b:I

    goto/16 :goto_0

    .line 658090
    :cond_b
    iget-object v0, p0, LX/3xT;->a:LX/1P5;

    invoke-virtual {v0}, LX/1P5;->d()I

    move-result v0

    iget-object v3, p0, LX/3xT;->a:LX/1P5;

    invoke-virtual {v3, v2}, LX/1P5;->b(Landroid/view/View;)I

    move-result v2

    sub-int/2addr v0, v2

    .line 658091
    if-gez v0, :cond_c

    .line 658092
    iput v0, p2, LX/3xN;->b:I

    goto/16 :goto_0

    .line 658093
    :cond_c
    iput v4, p2, LX/3xN;->b:I

    goto/16 :goto_0

    .line 658094
    :cond_d
    iget v2, p0, LX/3xT;->d:I

    iput v2, p2, LX/3xN;->a:I

    .line 658095
    iget v2, p0, LX/3xT;->e:I

    if-ne v2, v4, :cond_f

    .line 658096
    iget v2, p2, LX/3xN;->a:I

    invoke-static {p0, v2}, LX/3xT;->p(LX/3xT;I)I

    move-result v2

    .line 658097
    if-ne v2, v1, :cond_e

    move v0, v1

    :cond_e
    iput-boolean v0, p2, LX/3xN;->c:Z

    .line 658098
    invoke-virtual {p2}, LX/3xN;->b()V

    .line 658099
    :goto_7
    iput-boolean v1, p2, LX/3xN;->d:Z

    goto/16 :goto_0

    .line 658100
    :cond_f
    iget v0, p0, LX/3xT;->e:I

    .line 658101
    iget-boolean v2, p2, LX/3xN;->c:Z

    if-eqz v2, :cond_11

    .line 658102
    iget-object v2, p2, LX/3xN;->e:LX/3xT;

    iget-object v2, v2, LX/3xT;->a:LX/1P5;

    invoke-virtual {v2}, LX/1P5;->d()I

    move-result v2

    sub-int/2addr v2, v0

    iput v2, p2, LX/3xN;->b:I

    .line 658103
    :goto_8
    goto :goto_7

    .line 658104
    :cond_10
    iput v4, p2, LX/3xN;->b:I

    .line 658105
    iget v0, p0, LX/3xT;->d:I

    iput v0, p2, LX/3xN;->a:I

    goto/16 :goto_0

    .line 658106
    :cond_11
    iget-object v2, p2, LX/3xN;->e:LX/3xT;

    iget-object v2, v2, LX/3xT;->a:LX/1P5;

    invoke-virtual {v2}, LX/1P5;->c()I

    move-result v2

    add-int/2addr v2, v0

    iput v2, p2, LX/3xN;->b:I

    goto :goto_8

    .line 658107
    :cond_12
    invoke-virtual {p1}, LX/1Ok;->e()I

    move-result v0

    const/4 v2, 0x0

    .line 658108
    invoke-virtual {p0}, LX/1OR;->v()I

    move-result p1

    move v3, v2

    .line 658109
    :goto_9
    if-ge v3, p1, :cond_16

    .line 658110
    invoke-virtual {p0, v3}, LX/1OR;->f(I)Landroid/view/View;

    move-result-object v1

    .line 658111
    invoke-static {v1}, LX/1OR;->d(Landroid/view/View;)I

    move-result v1

    .line 658112
    if-ltz v1, :cond_15

    if-ge v1, v0, :cond_15

    .line 658113
    :goto_a
    move v0, v1

    .line 658114
    goto/16 :goto_4

    .line 658115
    :cond_13
    add-int/lit8 v1, p1, -0x1

    move p1, v1

    goto/16 :goto_2

    .line 658116
    :cond_14
    const/4 v1, 0x0

    goto/16 :goto_3

    .line 658117
    :cond_15
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_9

    :cond_16
    move v1, v2

    .line 658118
    goto :goto_a
.end method

.method private a(LX/3xS;II)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 658045
    iget v0, p1, LX/3xS;->c:I

    move v0, v0

    .line 658046
    const/4 v1, -0x1

    if-ne p2, v1, :cond_1

    .line 658047
    invoke-virtual {p1}, LX/3xS;->a()I

    move-result v1

    .line 658048
    add-int/2addr v0, v1

    if-gt v0, p3, :cond_0

    .line 658049
    iget-object v0, p0, LX/3xT;->m:Ljava/util/BitSet;

    iget v1, p1, LX/3xS;->d:I

    invoke-virtual {v0, v1, v2}, Ljava/util/BitSet;->set(IZ)V

    .line 658050
    :cond_0
    :goto_0
    return-void

    .line 658051
    :cond_1
    invoke-virtual {p1}, LX/3xS;->b()I

    move-result v1

    .line 658052
    sub-int v0, v1, v0

    if-lt v0, p3, :cond_0

    .line 658053
    iget-object v0, p0, LX/3xT;->m:Ljava/util/BitSet;

    iget v1, p1, LX/3xS;->d:I

    invoke-virtual {v0, v1, v2}, Ljava/util/BitSet;->set(IZ)V

    goto :goto_0
.end method

.method private a(Landroid/view/View;LX/3xO;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 658037
    iget-boolean v0, p2, LX/3xO;->f:Z

    if-eqz v0, :cond_1

    .line 658038
    iget v0, p0, LX/3xT;->i:I

    if-ne v0, v1, :cond_0

    .line 658039
    iget v0, p0, LX/3xT;->u:I

    iget v1, p2, Landroid/view/ViewGroup$LayoutParams;->height:I

    iget v2, p0, LX/3xT;->w:I

    invoke-static {v1, v2}, LX/3xT;->d(II)I

    move-result v1

    invoke-static {p0, p1, v0, v1}, LX/3xT;->b(LX/3xT;Landroid/view/View;II)V

    .line 658040
    :goto_0
    return-void

    .line 658041
    :cond_0
    iget v0, p2, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget v1, p0, LX/3xT;->v:I

    invoke-static {v0, v1}, LX/3xT;->d(II)I

    move-result v0

    iget v1, p0, LX/3xT;->u:I

    invoke-static {p0, p1, v0, v1}, LX/3xT;->b(LX/3xT;Landroid/view/View;II)V

    goto :goto_0

    .line 658042
    :cond_1
    iget v0, p0, LX/3xT;->i:I

    if-ne v0, v1, :cond_2

    .line 658043
    iget v0, p0, LX/3xT;->v:I

    iget v1, p2, Landroid/view/ViewGroup$LayoutParams;->height:I

    iget v2, p0, LX/3xT;->w:I

    invoke-static {v1, v2}, LX/3xT;->d(II)I

    move-result v1

    invoke-static {p0, p1, v0, v1}, LX/3xT;->b(LX/3xT;Landroid/view/View;II)V

    goto :goto_0

    .line 658044
    :cond_2
    iget v0, p2, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget v1, p0, LX/3xT;->v:I

    invoke-static {v0, v1}, LX/3xT;->d(II)I

    move-result v0

    iget v1, p0, LX/3xT;->w:I

    invoke-static {p0, p1, v0, v1}, LX/3xT;->b(LX/3xT;Landroid/view/View;II)V

    goto :goto_0
.end method

.method private a(Landroid/view/View;LX/3xO;LX/3wv;)V
    .locals 2

    .prologue
    .line 658024
    iget v0, p3, LX/3wv;->d:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 658025
    iget-boolean v0, p2, LX/3xO;->f:Z

    if-eqz v0, :cond_1

    .line 658026
    iget v0, p0, LX/3xT;->g:I

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_0

    .line 658027
    iget-object v1, p0, LX/3xT;->h:[LX/3xS;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, LX/3xS;->b(Landroid/view/View;)V

    .line 658028
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 658029
    :cond_0
    :goto_1
    return-void

    .line 658030
    :cond_1
    iget-object v0, p2, LX/3xO;->e:LX/3xS;

    invoke-virtual {v0, p1}, LX/3xS;->b(Landroid/view/View;)V

    goto :goto_1

    .line 658031
    :cond_2
    iget-boolean v0, p2, LX/3xO;->f:Z

    if-eqz v0, :cond_4

    .line 658032
    iget v0, p0, LX/3xT;->g:I

    add-int/lit8 v0, v0, -0x1

    :goto_2
    if-ltz v0, :cond_3

    .line 658033
    iget-object v1, p0, LX/3xT;->h:[LX/3xS;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, LX/3xS;->a(Landroid/view/View;)V

    .line 658034
    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    .line 658035
    :cond_3
    goto :goto_1

    .line 658036
    :cond_4
    iget-object v0, p2, LX/3xO;->e:LX/3xS;

    invoke-virtual {v0, p1}, LX/3xS;->a(Landroid/view/View;)V

    goto :goto_1
.end method

.method public static b(LX/3xT;ZZ)Landroid/view/View;
    .locals 7

    .prologue
    .line 658460
    invoke-direct {p0}, LX/3xT;->k()V

    .line 658461
    iget-object v0, p0, LX/3xT;->a:LX/1P5;

    invoke-virtual {v0}, LX/1P5;->c()I

    move-result v3

    .line 658462
    iget-object v0, p0, LX/3xT;->a:LX/1P5;

    invoke-virtual {v0}, LX/1P5;->d()I

    move-result v4

    .line 658463
    const/4 v1, 0x0

    .line 658464
    invoke-virtual {p0}, LX/1OR;->v()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_0
    if-ltz v2, :cond_2

    .line 658465
    invoke-virtual {p0, v2}, LX/1OR;->f(I)Landroid/view/View;

    move-result-object v0

    .line 658466
    iget-object v5, p0, LX/3xT;->a:LX/1P5;

    invoke-virtual {v5, v0}, LX/1P5;->a(Landroid/view/View;)I

    move-result v5

    .line 658467
    iget-object v6, p0, LX/3xT;->a:LX/1P5;

    invoke-virtual {v6, v0}, LX/1P5;->b(Landroid/view/View;)I

    move-result v6

    .line 658468
    if-le v6, v3, :cond_3

    if-ge v5, v4, :cond_3

    .line 658469
    if-le v6, v4, :cond_0

    if-nez p1, :cond_1

    .line 658470
    :cond_0
    :goto_1
    return-object v0

    .line 658471
    :cond_1
    if-eqz p2, :cond_3

    if-nez v1, :cond_3

    .line 658472
    :goto_2
    add-int/lit8 v1, v2, -0x1

    move v2, v1

    move-object v1, v0

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 658473
    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2
.end method

.method private b(I)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, -0x1

    .line 658333
    iget-object v2, p0, LX/3xT;->k:LX/3wv;

    iput p1, v2, LX/3wv;->d:I

    .line 658334
    iget-object v3, p0, LX/3xT;->k:LX/3wv;

    iget-boolean v4, p0, LX/3xT;->c:Z

    if-ne p1, v1, :cond_0

    move v2, v0

    :goto_0
    if-ne v4, v2, :cond_1

    :goto_1
    iput v0, v3, LX/3wv;->c:I

    .line 658335
    return-void

    .line 658336
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method private b(III)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 658474
    iget-boolean v0, p0, LX/3xT;->c:Z

    if-eqz v0, :cond_1

    invoke-static {p0}, LX/3xT;->K(LX/3xT;)I

    move-result v0

    move v2, v0

    .line 658475
    :goto_0
    const/4 v0, 0x3

    if-ne p3, v0, :cond_3

    .line 658476
    if-ge p1, p2, :cond_2

    .line 658477
    add-int/lit8 v0, p2, 0x1

    move v1, v0

    move v0, p1

    .line 658478
    :goto_1
    iget-object v3, p0, LX/3xT;->f:LX/3xQ;

    invoke-virtual {v3, v0}, LX/3xQ;->b(I)I

    .line 658479
    packed-switch p3, :pswitch_data_0

    .line 658480
    :goto_2
    :pswitch_0
    if-gt v1, v2, :cond_4

    .line 658481
    :cond_0
    :goto_3
    return-void

    .line 658482
    :cond_1
    invoke-static {p0}, LX/3xT;->L(LX/3xT;)I

    move-result v0

    move v2, v0

    goto :goto_0

    .line 658483
    :cond_2
    add-int/lit8 v0, p1, 0x1

    move v1, v0

    move v0, p2

    .line 658484
    goto :goto_1

    .line 658485
    :cond_3
    add-int v0, p1, p2

    move v1, v0

    move v0, p1

    goto :goto_1

    .line 658486
    :pswitch_1
    iget-object v3, p0, LX/3xT;->f:LX/3xQ;

    invoke-virtual {v3, p1, p2}, LX/3xQ;->b(II)V

    goto :goto_2

    .line 658487
    :pswitch_2
    iget-object v3, p0, LX/3xT;->f:LX/3xQ;

    invoke-virtual {v3, p1, p2}, LX/3xQ;->a(II)V

    goto :goto_2

    .line 658488
    :pswitch_3
    iget-object v3, p0, LX/3xT;->f:LX/3xQ;

    invoke-virtual {v3, p1, v4}, LX/3xQ;->a(II)V

    .line 658489
    iget-object v3, p0, LX/3xT;->f:LX/3xQ;

    invoke-virtual {v3, p2, v4}, LX/3xQ;->b(II)V

    goto :goto_2

    .line 658490
    :cond_4
    iget-boolean v1, p0, LX/3xT;->c:Z

    if-eqz v1, :cond_5

    invoke-static {p0}, LX/3xT;->L(LX/3xT;)I

    move-result v1

    .line 658491
    :goto_4
    if-gt v0, v1, :cond_0

    .line 658492
    invoke-virtual {p0}, LX/1OR;->p()V

    goto :goto_3

    .line 658493
    :cond_5
    invoke-static {p0}, LX/3xT;->K(LX/3xT;)I

    move-result v1

    goto :goto_4

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private b(LX/1Od;I)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 658494
    invoke-virtual {p0}, LX/1OR;->v()I

    move-result v0

    .line 658495
    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_0
    if-ltz v2, :cond_0

    .line 658496
    invoke-virtual {p0, v2}, LX/1OR;->f(I)Landroid/view/View;

    move-result-object v3

    .line 658497
    iget-object v0, p0, LX/3xT;->a:LX/1P5;

    invoke-virtual {v0, v3}, LX/1P5;->a(Landroid/view/View;)I

    move-result v0

    if-lt v0, p2, :cond_0

    .line 658498
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/3xO;

    .line 658499
    iget-boolean v4, v0, LX/3xO;->f:Z

    if-eqz v4, :cond_3

    move v0, v1

    .line 658500
    :goto_1
    iget v4, p0, LX/3xT;->g:I

    if-ge v0, v4, :cond_2

    .line 658501
    iget-object v4, p0, LX/3xT;->h:[LX/3xS;

    aget-object v4, v4, v0

    iget-object v4, v4, LX/3xS;->f:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ne v4, v5, :cond_1

    .line 658502
    :cond_0
    return-void

    .line 658503
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move v0, v1

    .line 658504
    :goto_2
    iget v4, p0, LX/3xT;->g:I

    if-ge v0, v4, :cond_4

    .line 658505
    iget-object v4, p0, LX/3xT;->h:[LX/3xS;

    aget-object v4, v4, v0

    invoke-virtual {v4}, LX/3xS;->d()V

    .line 658506
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 658507
    :cond_3
    iget-object v4, v0, LX/3xO;->e:LX/3xS;

    iget-object v4, v4, LX/3xS;->f:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-eq v4, v5, :cond_0

    .line 658508
    iget-object v0, v0, LX/3xO;->e:LX/3xS;

    invoke-virtual {v0}, LX/3xS;->d()V

    .line 658509
    :cond_4
    invoke-virtual {p0, v3, p1}, LX/1OR;->a(Landroid/view/View;LX/1Od;)V

    .line 658510
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_0
.end method

.method private b(LX/1Od;LX/1Ok;Z)V
    .locals 2

    .prologue
    .line 658511
    iget-object v0, p0, LX/3xT;->a:LX/1P5;

    invoke-virtual {v0}, LX/1P5;->c()I

    move-result v0

    invoke-direct {p0, v0}, LX/3xT;->l(I)I

    move-result v0

    .line 658512
    iget-object v1, p0, LX/3xT;->a:LX/1P5;

    invoke-virtual {v1}, LX/1P5;->c()I

    move-result v1

    sub-int/2addr v0, v1

    .line 658513
    if-lez v0, :cond_0

    .line 658514
    invoke-direct {p0, v0, p1, p2}, LX/3xT;->d(ILX/1Od;LX/1Ok;)I

    move-result v1

    .line 658515
    sub-int/2addr v0, v1

    .line 658516
    if-eqz p3, :cond_0

    if-lez v0, :cond_0

    .line 658517
    iget-object v1, p0, LX/3xT;->a:LX/1P5;

    neg-int v0, v0

    invoke-virtual {v1, v0}, LX/1P5;->a(I)V

    .line 658518
    :cond_0
    return-void
.end method

.method private static b(LX/3xT;Landroid/view/View;II)V
    .locals 4

    .prologue
    .line 658528
    iget-object v0, p0, LX/3xT;->x:Landroid/graphics/Rect;

    invoke-virtual {p0, p1, v0}, LX/1OR;->a(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 658529
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/3xO;

    .line 658530
    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget-object v2, p0, LX/3xT;->x:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, v2

    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    iget-object v3, p0, LX/3xT;->x:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    add-int/2addr v2, v3

    invoke-static {p2, v1, v2}, LX/3xT;->a(III)I

    move-result v1

    .line 658531
    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget-object v3, p0, LX/3xT;->x:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    add-int/2addr v2, v3

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    iget-object v3, p0, LX/3xT;->x:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v3

    invoke-static {p3, v2, v0}, LX/3xT;->a(III)I

    move-result v0

    .line 658532
    invoke-virtual {p1, v1, v0}, Landroid/view/View;->measure(II)V

    .line 658533
    return-void
.end method

.method private b(Landroid/view/View;IIII)V
    .locals 6

    .prologue
    .line 658519
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/3xO;

    .line 658520
    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int v2, p2, v1

    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int v3, p3, v1

    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    sub-int v4, p4, v1

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    sub-int v5, p5, v0

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/1OR;->a(Landroid/view/View;IIII)V

    .line 658521
    return-void
.end method

.method private static d(II)I
    .locals 1

    .prologue
    .line 658534
    if-gez p0, :cond_0

    .line 658535
    :goto_0
    return p1

    :cond_0
    const/high16 v0, 0x40000000    # 2.0f

    invoke-static {p0, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    goto :goto_0
.end method

.method private d(ILX/1Od;LX/1Ok;)I
    .locals 3

    .prologue
    .line 658441
    invoke-direct {p0}, LX/3xT;->k()V

    .line 658442
    if-lez p1, :cond_0

    .line 658443
    const/4 v1, 0x1

    .line 658444
    invoke-static {p0}, LX/3xT;->K(LX/3xT;)I

    move-result v0

    .line 658445
    :goto_0
    invoke-direct {p0, v0, p3}, LX/3xT;->a(ILX/1Ok;)V

    .line 658446
    invoke-direct {p0, v1}, LX/3xT;->b(I)V

    .line 658447
    iget-object v1, p0, LX/3xT;->k:LX/3wv;

    iget-object v2, p0, LX/3xT;->k:LX/3wv;

    iget v2, v2, LX/3wv;->c:I

    add-int/2addr v0, v2

    iput v0, v1, LX/3wv;->b:I

    .line 658448
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 658449
    iget-object v0, p0, LX/3xT;->k:LX/3wv;

    iput v1, v0, LX/3wv;->a:I

    .line 658450
    iget-object v0, p0, LX/3xT;->k:LX/3wv;

    invoke-direct {p0, p2, v0, p3}, LX/3xT;->a(LX/1Od;LX/3wv;LX/1Ok;)I

    move-result v0

    .line 658451
    if-ge v1, v0, :cond_1

    .line 658452
    :goto_1
    iget-object v0, p0, LX/3xT;->a:LX/1P5;

    neg-int v1, p1

    invoke-virtual {v0, v1}, LX/1P5;->a(I)V

    .line 658453
    iget-boolean v0, p0, LX/3xT;->c:Z

    iput-boolean v0, p0, LX/3xT;->o:Z

    .line 658454
    return p1

    .line 658455
    :cond_0
    const/4 v1, -0x1

    .line 658456
    invoke-static {p0}, LX/3xT;->L(LX/3xT;)I

    move-result v0

    goto :goto_0

    .line 658457
    :cond_1
    if-gez p1, :cond_2

    .line 658458
    neg-int p1, v0

    goto :goto_1

    :cond_2
    move p1, v0

    .line 658459
    goto :goto_1
.end method

.method private d(I)Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;
    .locals 4

    .prologue
    .line 658522
    new-instance v1, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;

    invoke-direct {v1}, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;-><init>()V

    .line 658523
    iget v0, p0, LX/3xT;->g:I

    new-array v0, v0, [I

    iput-object v0, v1, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;->c:[I

    .line 658524
    const/4 v0, 0x0

    :goto_0
    iget v2, p0, LX/3xT;->g:I

    if-ge v0, v2, :cond_0

    .line 658525
    iget-object v2, v1, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;->c:[I

    iget-object v3, p0, LX/3xT;->h:[LX/3xS;

    aget-object v3, v3, v0

    invoke-virtual {v3, p1}, LX/3xS;->b(I)I

    move-result v3

    sub-int v3, p1, v3

    aput v3, v2, v0

    .line 658526
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 658527
    :cond_0
    return-object v1
.end method

.method public static d(LX/3xT;)Z
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 658411
    invoke-virtual {p0}, LX/1OR;->v()I

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, LX/3xT;->n:I

    if-eqz v0, :cond_0

    .line 658412
    iget-boolean v0, p0, LX/1OR;->b:Z

    move v0, v0

    .line 658413
    if-nez v0, :cond_1

    :cond_0
    move v1, v2

    .line 658414
    :goto_0
    return v1

    .line 658415
    :cond_1
    iget-boolean v0, p0, LX/3xT;->c:Z

    if-eqz v0, :cond_2

    .line 658416
    invoke-static {p0}, LX/3xT;->K(LX/3xT;)I

    move-result v3

    .line 658417
    invoke-static {p0}, LX/3xT;->L(LX/3xT;)I

    move-result v0

    move v4, v3

    move v3, v0

    .line 658418
    :goto_1
    if-nez v4, :cond_3

    .line 658419
    invoke-direct {p0}, LX/3xT;->i()Landroid/view/View;

    move-result-object v0

    .line 658420
    if-eqz v0, :cond_3

    .line 658421
    iget-object v0, p0, LX/3xT;->f:LX/3xQ;

    invoke-virtual {v0}, LX/3xQ;->a()V

    .line 658422
    invoke-virtual {p0}, LX/1OR;->H()V

    .line 658423
    invoke-virtual {p0}, LX/1OR;->p()V

    goto :goto_0

    .line 658424
    :cond_2
    invoke-static {p0}, LX/3xT;->L(LX/3xT;)I

    move-result v3

    .line 658425
    invoke-static {p0}, LX/3xT;->K(LX/3xT;)I

    move-result v0

    move v4, v3

    move v3, v0

    goto :goto_1

    .line 658426
    :cond_3
    iget-boolean v0, p0, LX/3xT;->z:Z

    if-nez v0, :cond_4

    move v1, v2

    .line 658427
    goto :goto_0

    .line 658428
    :cond_4
    iget-boolean v0, p0, LX/3xT;->c:Z

    if-eqz v0, :cond_5

    const/4 v0, -0x1

    .line 658429
    :goto_2
    iget-object v5, p0, LX/3xT;->f:LX/3xQ;

    add-int/lit8 v6, v3, 0x1

    invoke-virtual {v5, v4, v6, v0, v1}, LX/3xQ;->a(IIIZ)Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;

    move-result-object v5

    .line 658430
    if-nez v5, :cond_6

    .line 658431
    iput-boolean v2, p0, LX/3xT;->z:Z

    .line 658432
    iget-object v0, p0, LX/3xT;->f:LX/3xQ;

    add-int/lit8 v1, v3, 0x1

    invoke-virtual {v0, v1}, LX/3xQ;->a(I)I

    move v1, v2

    .line 658433
    goto :goto_0

    :cond_5
    move v0, v1

    .line 658434
    goto :goto_2

    .line 658435
    :cond_6
    iget-object v2, p0, LX/3xT;->f:LX/3xQ;

    iget v3, v5, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;->a:I

    mul-int/lit8 v0, v0, -0x1

    invoke-virtual {v2, v4, v3, v0, v1}, LX/3xQ;->a(IIIZ)Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;

    move-result-object v0

    .line 658436
    if-nez v0, :cond_7

    .line 658437
    iget-object v0, p0, LX/3xT;->f:LX/3xQ;

    iget v2, v5, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;->a:I

    invoke-virtual {v0, v2}, LX/3xQ;->a(I)I

    .line 658438
    :goto_3
    invoke-virtual {p0}, LX/1OR;->H()V

    .line 658439
    invoke-virtual {p0}, LX/1OR;->p()V

    goto :goto_0

    .line 658440
    :cond_7
    iget-object v2, p0, LX/3xT;->f:LX/3xQ;

    iget v0, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;->a:I

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v2, v0}, LX/3xQ;->a(I)I

    goto :goto_3
.end method

.method private f(II)V
    .locals 2

    .prologue
    .line 658406
    const/4 v0, 0x0

    :goto_0
    iget v1, p0, LX/3xT;->g:I

    if-ge v0, v1, :cond_1

    .line 658407
    iget-object v1, p0, LX/3xT;->h:[LX/3xS;

    aget-object v1, v1, v0

    iget-object v1, v1, LX/3xS;->f:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 658408
    iget-object v1, p0, LX/3xT;->h:[LX/3xS;

    aget-object v1, v1, v0

    invoke-direct {p0, v1, p1, p2}, LX/3xT;->a(LX/3xS;II)V

    .line 658409
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 658410
    :cond_1
    return-void
.end method

.method private h(LX/1Ok;)I
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 658402
    invoke-virtual {p0}, LX/1OR;->v()I

    move-result v0

    if-nez v0, :cond_0

    .line 658403
    :goto_0
    return v4

    .line 658404
    :cond_0
    invoke-direct {p0}, LX/3xT;->k()V

    .line 658405
    iget-object v1, p0, LX/3xT;->a:LX/1P5;

    iget-boolean v0, p0, LX/3xT;->A:Z

    if-nez v0, :cond_2

    move v0, v3

    :goto_1
    invoke-static {p0, v0, v3}, LX/3xT;->a(LX/3xT;ZZ)Landroid/view/View;

    move-result-object v2

    iget-boolean v0, p0, LX/3xT;->A:Z

    if-nez v0, :cond_1

    move v4, v3

    :cond_1
    invoke-static {p0, v4, v3}, LX/3xT;->b(LX/3xT;ZZ)Landroid/view/View;

    move-result-object v3

    iget-boolean v5, p0, LX/3xT;->A:Z

    move-object v0, p1

    move-object v4, p0

    invoke-static/range {v0 .. v5}, LX/2fZ;->a(LX/1Ok;LX/1P5;Landroid/view/View;Landroid/view/View;LX/1OR;Z)I

    move-result v4

    goto :goto_0

    :cond_2
    move v0, v4

    goto :goto_1
.end method

.method private i(LX/1Ok;)I
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 658398
    invoke-virtual {p0}, LX/1OR;->v()I

    move-result v0

    if-nez v0, :cond_0

    .line 658399
    :goto_0
    return v4

    .line 658400
    :cond_0
    invoke-direct {p0}, LX/3xT;->k()V

    .line 658401
    iget-object v1, p0, LX/3xT;->a:LX/1P5;

    iget-boolean v0, p0, LX/3xT;->A:Z

    if-nez v0, :cond_2

    move v0, v3

    :goto_1
    invoke-static {p0, v0, v3}, LX/3xT;->a(LX/3xT;ZZ)Landroid/view/View;

    move-result-object v2

    iget-boolean v0, p0, LX/3xT;->A:Z

    if-nez v0, :cond_1

    move v4, v3

    :cond_1
    invoke-static {p0, v4, v3}, LX/3xT;->b(LX/3xT;ZZ)Landroid/view/View;

    move-result-object v3

    iget-boolean v5, p0, LX/3xT;->A:Z

    move-object v0, p1

    move-object v4, p0

    invoke-static/range {v0 .. v5}, LX/2fZ;->b(LX/1Ok;LX/1P5;Landroid/view/View;Landroid/view/View;LX/1OR;Z)I

    move-result v4

    goto :goto_0

    :cond_2
    move v0, v4

    goto :goto_1
.end method

.method private i()Landroid/view/View;
    .locals 13

    .prologue
    const/4 v0, -0x1

    const/4 v5, 0x0

    const/4 v3, 0x1

    .line 658354
    invoke-virtual {p0}, LX/1OR;->v()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    .line 658355
    new-instance v9, Ljava/util/BitSet;

    iget v2, p0, LX/3xT;->g:I

    invoke-direct {v9, v2}, Ljava/util/BitSet;-><init>(I)V

    .line 658356
    iget v2, p0, LX/3xT;->g:I

    invoke-virtual {v9, v5, v2, v3}, Ljava/util/BitSet;->set(IIZ)V

    .line 658357
    iget v2, p0, LX/3xT;->i:I

    if-ne v2, v3, :cond_1

    invoke-static {p0}, LX/3xT;->m(LX/3xT;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v2, v3

    .line 658358
    :goto_0
    iget-boolean v4, p0, LX/3xT;->c:Z

    if-eqz v4, :cond_2

    move v8, v0

    .line 658359
    :goto_1
    if-ge v1, v8, :cond_3

    move v4, v3

    :goto_2
    move v7, v1

    .line 658360
    :goto_3
    if-eq v7, v8, :cond_c

    .line 658361
    invoke-virtual {p0, v7}, LX/1OR;->f(I)Landroid/view/View;

    move-result-object v6

    .line 658362
    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/3xO;

    .line 658363
    iget-object v1, v0, LX/3xO;->e:LX/3xS;

    iget v1, v1, LX/3xS;->d:I

    invoke-virtual {v9, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 658364
    iget-object v1, v0, LX/3xO;->e:LX/3xS;

    const/4 v10, 0x1

    .line 658365
    iget-boolean v11, p0, LX/3xT;->c:Z

    if-eqz v11, :cond_e

    .line 658366
    invoke-virtual {v1}, LX/3xS;->b()I

    move-result v11

    iget-object v12, p0, LX/3xT;->a:LX/1P5;

    invoke-virtual {v12}, LX/1P5;->d()I

    move-result v12

    if-ge v11, v12, :cond_f

    .line 658367
    :cond_0
    :goto_4
    move v1, v10

    .line 658368
    if-eqz v1, :cond_4

    move-object v0, v6

    .line 658369
    :goto_5
    return-object v0

    :cond_1
    move v2, v0

    .line 658370
    goto :goto_0

    .line 658371
    :cond_2
    add-int/lit8 v1, v1, 0x1

    move v8, v1

    move v1, v5

    goto :goto_1

    :cond_3
    move v4, v0

    .line 658372
    goto :goto_2

    .line 658373
    :cond_4
    iget-object v1, v0, LX/3xO;->e:LX/3xS;

    iget v1, v1, LX/3xS;->d:I

    invoke-virtual {v9, v1}, Ljava/util/BitSet;->clear(I)V

    .line 658374
    :cond_5
    iget-boolean v1, v0, LX/3xO;->f:Z

    if-nez v1, :cond_b

    .line 658375
    add-int v1, v7, v4

    if-eq v1, v8, :cond_b

    .line 658376
    add-int v1, v7, v4

    invoke-virtual {p0, v1}, LX/1OR;->f(I)Landroid/view/View;

    move-result-object v10

    .line 658377
    iget-boolean v1, p0, LX/3xT;->c:Z

    if-eqz v1, :cond_7

    .line 658378
    iget-object v1, p0, LX/3xT;->a:LX/1P5;

    invoke-virtual {v1, v6}, LX/1P5;->b(Landroid/view/View;)I

    move-result v1

    .line 658379
    iget-object v11, p0, LX/3xT;->a:LX/1P5;

    invoke-virtual {v11, v10}, LX/1P5;->b(Landroid/view/View;)I

    move-result v11

    .line 658380
    if-ge v1, v11, :cond_6

    move-object v0, v6

    .line 658381
    goto :goto_5

    .line 658382
    :cond_6
    if-ne v1, v11, :cond_d

    move v1, v3

    .line 658383
    :goto_6
    if-eqz v1, :cond_b

    .line 658384
    invoke-virtual {v10}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, LX/3xO;

    .line 658385
    iget-object v0, v0, LX/3xO;->e:LX/3xS;

    iget v0, v0, LX/3xS;->d:I

    iget-object v1, v1, LX/3xO;->e:LX/3xS;

    iget v1, v1, LX/3xS;->d:I

    sub-int/2addr v0, v1

    if-gez v0, :cond_9

    move v1, v3

    :goto_7
    if-gez v2, :cond_a

    move v0, v3

    :goto_8
    if-eq v1, v0, :cond_b

    move-object v0, v6

    .line 658386
    goto :goto_5

    .line 658387
    :cond_7
    iget-object v1, p0, LX/3xT;->a:LX/1P5;

    invoke-virtual {v1, v6}, LX/1P5;->a(Landroid/view/View;)I

    move-result v1

    .line 658388
    iget-object v11, p0, LX/3xT;->a:LX/1P5;

    invoke-virtual {v11, v10}, LX/1P5;->a(Landroid/view/View;)I

    move-result v11

    .line 658389
    if-le v1, v11, :cond_8

    move-object v0, v6

    .line 658390
    goto :goto_5

    .line 658391
    :cond_8
    if-ne v1, v11, :cond_d

    move v1, v3

    .line 658392
    goto :goto_6

    :cond_9
    move v1, v5

    .line 658393
    goto :goto_7

    :cond_a
    move v0, v5

    goto :goto_8

    .line 658394
    :cond_b
    add-int v0, v7, v4

    move v7, v0

    goto/16 :goto_3

    .line 658395
    :cond_c
    const/4 v0, 0x0

    goto :goto_5

    :cond_d
    move v1, v5

    goto :goto_6

    .line 658396
    :cond_e
    invoke-virtual {v1}, LX/3xS;->a()I

    move-result v11

    iget-object v12, p0, LX/3xT;->a:LX/1P5;

    invoke-virtual {v12}, LX/1P5;->c()I

    move-result v12

    if-gt v11, v12, :cond_0

    .line 658397
    :cond_f
    const/4 v10, 0x0

    goto/16 :goto_4
.end method

.method private j(I)Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;
    .locals 4

    .prologue
    .line 658348
    new-instance v1, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;

    invoke-direct {v1}, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;-><init>()V

    .line 658349
    iget v0, p0, LX/3xT;->g:I

    new-array v0, v0, [I

    iput-object v0, v1, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;->c:[I

    .line 658350
    const/4 v0, 0x0

    :goto_0
    iget v2, p0, LX/3xT;->g:I

    if-ge v0, v2, :cond_0

    .line 658351
    iget-object v2, v1, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;->c:[I

    iget-object v3, p0, LX/3xT;->h:[LX/3xS;

    aget-object v3, v3, v0

    invoke-virtual {v3, p1}, LX/3xS;->a(I)I

    move-result v3

    sub-int/2addr v3, p1

    aput v3, v2, v0

    .line 658352
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 658353
    :cond_0
    return-object v1
.end method

.method private k()V
    .locals 1

    .prologue
    .line 658343
    iget-object v0, p0, LX/3xT;->a:LX/1P5;

    if-nez v0, :cond_0

    .line 658344
    iget v0, p0, LX/3xT;->i:I

    invoke-static {p0, v0}, LX/1P5;->a(LX/1OR;I)LX/1P5;

    move-result-object v0

    iput-object v0, p0, LX/3xT;->a:LX/1P5;

    .line 658345
    iget v0, p0, LX/3xT;->i:I

    rsub-int/lit8 v0, v0, 0x1

    invoke-static {p0, v0}, LX/1P5;->a(LX/1OR;I)LX/1P5;

    move-result-object v0

    iput-object v0, p0, LX/3xT;->b:LX/1P5;

    .line 658346
    new-instance v0, LX/3wv;

    invoke-direct {v0}, LX/3wv;-><init>()V

    iput-object v0, p0, LX/3xT;->k:LX/3wv;

    .line 658347
    :cond_0
    return-void
.end method

.method private l(I)I
    .locals 3

    .prologue
    .line 658337
    iget-object v0, p0, LX/3xT;->h:[LX/3xS;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0, p1}, LX/3xS;->a(I)I

    move-result v1

    .line 658338
    const/4 v0, 0x1

    :goto_0
    iget v2, p0, LX/3xT;->g:I

    if-ge v0, v2, :cond_1

    .line 658339
    iget-object v2, p0, LX/3xT;->h:[LX/3xS;

    aget-object v2, v2, v0

    invoke-virtual {v2, p1}, LX/3xS;->a(I)I

    move-result v2

    .line 658340
    if-ge v2, v1, :cond_0

    move v1, v2

    .line 658341
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 658342
    :cond_1
    return v1
.end method

.method public static l(LX/3xT;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 658328
    iget v1, p0, LX/3xT;->i:I

    if-eq v1, v0, :cond_0

    invoke-static {p0}, LX/3xT;->m(LX/3xT;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 658329
    :cond_0
    iget-boolean v0, p0, LX/3xT;->l:Z

    .line 658330
    :cond_1
    :goto_0
    iput-boolean v0, p0, LX/3xT;->c:Z

    .line 658331
    return-void

    .line 658332
    :cond_2
    iget-boolean v1, p0, LX/3xT;->l:Z

    if-eqz v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private m(I)I
    .locals 3

    .prologue
    .line 657803
    iget-object v0, p0, LX/3xT;->h:[LX/3xS;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0, p1}, LX/3xS;->b(I)I

    move-result v1

    .line 657804
    const/4 v0, 0x1

    :goto_0
    iget v2, p0, LX/3xT;->g:I

    if-ge v0, v2, :cond_1

    .line 657805
    iget-object v2, p0, LX/3xT;->h:[LX/3xS;

    aget-object v2, v2, v0

    invoke-virtual {v2, p1}, LX/3xS;->b(I)I

    move-result v2

    .line 657806
    if-le v2, v1, :cond_0

    move v1, v2

    .line 657807
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 657808
    :cond_1
    return v1
.end method

.method public static m(LX/3xT;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 657802
    invoke-virtual {p0}, LX/1OR;->t()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static p(LX/3xT;I)I
    .locals 4

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x1

    .line 657796
    invoke-virtual {p0}, LX/1OR;->v()I

    move-result v0

    if-nez v0, :cond_2

    .line 657797
    iget-boolean v0, p0, LX/3xT;->c:Z

    if-eqz v0, :cond_1

    .line 657798
    :cond_0
    :goto_0
    return v1

    :cond_1
    move v1, v2

    .line 657799
    goto :goto_0

    .line 657800
    :cond_2
    invoke-static {p0}, LX/3xT;->L(LX/3xT;)I

    move-result v0

    .line 657801
    if-ge p1, v0, :cond_3

    move v0, v1

    :goto_1
    iget-boolean v3, p0, LX/3xT;->c:Z

    if-eq v0, v3, :cond_0

    move v1, v2

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final a(ILX/1Od;LX/1Ok;)I
    .locals 1

    .prologue
    .line 657795
    invoke-direct {p0, p1, p2, p3}, LX/3xT;->d(ILX/1Od;LX/1Ok;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1Od;LX/1Ok;)I
    .locals 1

    .prologue
    .line 657792
    iget v0, p0, LX/3xT;->i:I

    if-nez v0, :cond_0

    .line 657793
    iget v0, p0, LX/3xT;->g:I

    .line 657794
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, LX/1OR;->a(LX/1Od;LX/1Ok;)I

    move-result v0

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Landroid/util/AttributeSet;)LX/1a3;
    .locals 1

    .prologue
    .line 657791
    new-instance v0, LX/3xO;

    invoke-direct {v0, p1, p2}, LX/3xO;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method public final a(Landroid/view/ViewGroup$LayoutParams;)LX/1a3;
    .locals 1

    .prologue
    .line 657788
    instance-of v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v0, :cond_0

    .line 657789
    new-instance v0, LX/3xO;

    check-cast p1, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-direct {v0, p1}, LX/3xO;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    .line 657790
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/3xO;

    invoke-direct {v0, p1}, LX/3xO;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 657785
    iget-object v0, p0, LX/3xT;->f:LX/3xQ;

    invoke-virtual {v0}, LX/3xQ;->a()V

    .line 657786
    invoke-virtual {p0}, LX/1OR;->p()V

    .line 657787
    return-void
.end method

.method public final a(II)V
    .locals 1

    .prologue
    .line 657783
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/3xT;->b(III)V

    .line 657784
    return-void
.end method

.method public final a(LX/1Od;LX/1Ok;Landroid/view/View;LX/3sp;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v3, -0x1

    .line 657773
    invoke-virtual {p3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 657774
    instance-of v2, v0, LX/3xO;

    if-nez v2, :cond_0

    .line 657775
    invoke-super {p0, p3, p4}, LX/1OR;->a(Landroid/view/View;LX/3sp;)V

    .line 657776
    :goto_0
    return-void

    :cond_0
    move-object v4, v0

    .line 657777
    check-cast v4, LX/3xO;

    .line 657778
    iget v0, p0, LX/3xT;->i:I

    if-nez v0, :cond_2

    .line 657779
    invoke-virtual {v4}, LX/3xO;->a()I

    move-result v2

    iget-boolean v0, v4, LX/3xO;->f:Z

    if-eqz v0, :cond_1

    iget v0, p0, LX/3xT;->g:I

    :goto_1
    move v1, v0

    move v0, v2

    move v2, v3

    .line 657780
    :goto_2
    iget-boolean v4, v4, LX/3xO;->f:Z

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, LX/3so;->a(IIIIZZ)LX/3so;

    move-result-object v0

    invoke-virtual {p4, v0}, LX/3sp;->b(Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 657781
    goto :goto_1

    .line 657782
    :cond_2
    invoke-virtual {v4}, LX/3xO;->a()I

    move-result v2

    iget-boolean v0, v4, LX/3xO;->f:Z

    if-eqz v0, :cond_3

    iget v1, p0, LX/3xT;->g:I

    move v0, v3

    move v6, v3

    move v3, v1

    move v1, v6

    goto :goto_2

    :cond_3
    move v0, v3

    move v6, v3

    move v3, v1

    move v1, v6

    goto :goto_2
.end method

.method public final a(Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 657769
    instance-of v0, p1, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    if-eqz v0, :cond_0

    .line 657770
    check-cast p1, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    iput-object p1, p0, LX/3xT;->t:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    .line 657771
    invoke-virtual {p0}, LX/1OR;->p()V

    .line 657772
    :cond_0
    return-void
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 1

    .prologue
    .line 657767
    const/4 v0, 0x2

    invoke-direct {p0, p2, p3, v0}, LX/3xT;->b(III)V

    .line 657768
    return-void
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView;LX/1Od;)V
    .locals 2

    .prologue
    .line 657762
    iget-object v0, p0, LX/3xT;->B:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, LX/1OR;->b(Ljava/lang/Runnable;)Z

    .line 657763
    const/4 v0, 0x0

    :goto_0
    iget v1, p0, LX/3xT;->g:I

    if-ge v0, v1, :cond_0

    .line 657764
    iget-object v1, p0, LX/3xT;->h:[LX/3xS;

    aget-object v1, v1, v0

    invoke-virtual {v1}, LX/3xS;->c()V

    .line 657765
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 657766
    :cond_0
    return-void
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView;LX/1Ok;I)V
    .locals 2

    .prologue
    .line 657758
    new-instance v0, LX/3xM;

    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LX/3xM;-><init>(LX/3xT;Landroid/content/Context;)V

    .line 657759
    iput p3, v0, LX/25Y;->a:I

    .line 657760
    invoke-virtual {p0, v0}, LX/1OR;->a(LX/25Y;)V

    .line 657761
    return-void
.end method

.method public final a(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 657744
    invoke-super {p0, p1}, LX/1OR;->a(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 657745
    invoke-virtual {p0}, LX/1OR;->v()I

    move-result v0

    if-lez v0, :cond_0

    .line 657746
    invoke-static {p1}, LX/2fV;->a(Landroid/view/accessibility/AccessibilityEvent;)LX/2fO;

    move-result-object v0

    .line 657747
    invoke-static {p0, v2, v3}, LX/3xT;->a(LX/3xT;ZZ)Landroid/view/View;

    move-result-object v1

    .line 657748
    invoke-static {p0, v2, v3}, LX/3xT;->b(LX/3xT;ZZ)Landroid/view/View;

    move-result-object v2

    .line 657749
    if-eqz v1, :cond_0

    if-nez v2, :cond_1

    .line 657750
    :cond_0
    :goto_0
    return-void

    .line 657751
    :cond_1
    invoke-static {v1}, LX/1OR;->d(Landroid/view/View;)I

    move-result v1

    .line 657752
    invoke-static {v2}, LX/1OR;->d(Landroid/view/View;)I

    move-result v2

    .line 657753
    if-ge v1, v2, :cond_2

    .line 657754
    invoke-virtual {v0, v1}, LX/2fO;->b(I)V

    .line 657755
    invoke-virtual {v0, v2}, LX/2fO;->c(I)V

    goto :goto_0

    .line 657756
    :cond_2
    invoke-virtual {v0, v2}, LX/2fO;->b(I)V

    .line 657757
    invoke-virtual {v0, v1}, LX/2fO;->c(I)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 657741
    iget-object v0, p0, LX/3xT;->t:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    if-nez v0, :cond_0

    .line 657742
    invoke-super {p0, p1}, LX/1OR;->a(Ljava/lang/String;)V

    .line 657743
    :cond_0
    return-void
.end method

.method public final a(LX/1a3;)Z
    .locals 1

    .prologue
    .line 657740
    instance-of v0, p1, LX/3xO;

    return v0
.end method

.method public final b(ILX/1Od;LX/1Ok;)I
    .locals 1

    .prologue
    .line 657739
    invoke-direct {p0, p1, p2, p3}, LX/3xT;->d(ILX/1Od;LX/1Ok;)I

    move-result v0

    return v0
.end method

.method public final b(LX/1Od;LX/1Ok;)I
    .locals 2

    .prologue
    .line 657875
    iget v0, p0, LX/3xT;->i:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 657876
    iget v0, p0, LX/3xT;->g:I

    .line 657877
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, LX/1OR;->b(LX/1Od;LX/1Ok;)I

    move-result v0

    goto :goto_0
.end method

.method public final b(LX/1Ok;)I
    .locals 1

    .prologue
    .line 657996
    invoke-direct {p0, p1}, LX/3xT;->a(LX/1Ok;)I

    move-result v0

    return v0
.end method

.method public final b()LX/1a3;
    .locals 2

    .prologue
    const/4 v1, -0x2

    .line 657995
    new-instance v0, LX/3xO;

    invoke-direct {v0, v1, v1}, LX/3xO;-><init>(II)V

    return-object v0
.end method

.method public final b(II)V
    .locals 1

    .prologue
    .line 657993
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, LX/3xT;->b(III)V

    .line 657994
    return-void
.end method

.method public final c(LX/1Ok;)I
    .locals 1

    .prologue
    .line 657992
    invoke-direct {p0, p1}, LX/3xT;->a(LX/1Ok;)I

    move-result v0

    return v0
.end method

.method public final c(II)V
    .locals 1

    .prologue
    .line 657990
    const/4 v0, 0x3

    invoke-direct {p0, p1, p2, v0}, LX/3xT;->b(III)V

    .line 657991
    return-void
.end method

.method public final c(LX/1Od;LX/1Ok;)V
    .locals 12

    .prologue
    const/high16 v8, -0x80000000

    const/4 v7, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 657880
    invoke-direct {p0}, LX/3xT;->k()V

    .line 657881
    iget-object v3, p0, LX/3xT;->y:LX/3xN;

    .line 657882
    const/4 v4, 0x0

    .line 657883
    const/4 v0, -0x1

    iput v0, v3, LX/3xN;->a:I

    .line 657884
    const/high16 v0, -0x80000000

    iput v0, v3, LX/3xN;->b:I

    .line 657885
    iput-boolean v4, v3, LX/3xN;->c:Z

    .line 657886
    iput-boolean v4, v3, LX/3xN;->d:Z

    .line 657887
    iget-object v0, p0, LX/3xT;->t:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    if-nez v0, :cond_0

    iget v0, p0, LX/3xT;->d:I

    if-eq v0, v7, :cond_1

    .line 657888
    :cond_0
    invoke-virtual {p2}, LX/1Ok;->e()I

    move-result v0

    if-nez v0, :cond_1

    .line 657889
    invoke-virtual {p0, p1}, LX/1OR;->c(LX/1Od;)V

    .line 657890
    :goto_0
    return-void

    .line 657891
    :cond_1
    iget-object v0, p0, LX/3xT;->t:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    if-eqz v0, :cond_c

    .line 657892
    iget-object v0, p0, LX/3xT;->t:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    iget v0, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->c:I

    if-lez v0, :cond_5

    .line 657893
    iget-object v0, p0, LX/3xT;->t:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    iget v0, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->c:I

    iget v4, p0, LX/3xT;->g:I

    if-ne v0, v4, :cond_4

    .line 657894
    const/4 v0, 0x0

    :goto_1
    iget v4, p0, LX/3xT;->g:I

    if-ge v0, v4, :cond_5

    .line 657895
    iget-object v4, p0, LX/3xT;->h:[LX/3xS;

    aget-object v4, v4, v0

    invoke-virtual {v4}, LX/3xS;->c()V

    .line 657896
    iget-object v4, p0, LX/3xT;->t:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    iget-object v4, v4, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->d:[I

    aget v4, v4, v0

    .line 657897
    const/high16 v5, -0x80000000

    if-eq v4, v5, :cond_2

    .line 657898
    iget-object v5, p0, LX/3xT;->t:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    iget-boolean v5, v5, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->i:Z

    if-eqz v5, :cond_3

    .line 657899
    iget-object v5, p0, LX/3xT;->a:LX/1P5;

    invoke-virtual {v5}, LX/1P5;->d()I

    move-result v5

    add-int/2addr v4, v5

    .line 657900
    :cond_2
    :goto_2
    iget-object v5, p0, LX/3xT;->h:[LX/3xS;

    aget-object v5, v5, v0

    invoke-virtual {v5, v4}, LX/3xS;->c(I)V

    .line 657901
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 657902
    :cond_3
    iget-object v5, p0, LX/3xT;->a:LX/1P5;

    invoke-virtual {v5}, LX/1P5;->c()I

    move-result v5

    add-int/2addr v4, v5

    goto :goto_2

    .line 657903
    :cond_4
    iget-object v0, p0, LX/3xT;->t:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 657904
    iput-object v4, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->d:[I

    .line 657905
    iput v5, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->c:I

    .line 657906
    iput v5, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->e:I

    .line 657907
    iput-object v4, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->f:[I

    .line 657908
    iput-object v4, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->g:Ljava/util/List;

    .line 657909
    iget-object v0, p0, LX/3xT;->t:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    iget-object v4, p0, LX/3xT;->t:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    iget v4, v4, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->b:I

    iput v4, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->a:I

    .line 657910
    :cond_5
    iget-object v0, p0, LX/3xT;->t:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    iget-boolean v0, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->j:Z

    iput-boolean v0, p0, LX/3xT;->p:Z

    .line 657911
    iget-object v0, p0, LX/3xT;->t:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    iget-boolean v0, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->h:Z

    .line 657912
    const/4 v4, 0x0

    invoke-virtual {p0, v4}, LX/1OR;->a(Ljava/lang/String;)V

    .line 657913
    iget-object v4, p0, LX/3xT;->t:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    if-eqz v4, :cond_6

    iget-object v4, p0, LX/3xT;->t:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    iget-boolean v4, v4, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->h:Z

    if-eq v4, v0, :cond_6

    .line 657914
    iget-object v4, p0, LX/3xT;->t:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    iput-boolean v0, v4, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->h:Z

    .line 657915
    :cond_6
    iput-boolean v0, p0, LX/3xT;->l:Z

    .line 657916
    invoke-virtual {p0}, LX/1OR;->p()V

    .line 657917
    invoke-static {p0}, LX/3xT;->l(LX/3xT;)V

    .line 657918
    iget-object v0, p0, LX/3xT;->t:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    iget v0, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->a:I

    const/4 v4, -0x1

    if-eq v0, v4, :cond_17

    .line 657919
    iget-object v0, p0, LX/3xT;->t:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    iget v0, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->a:I

    iput v0, p0, LX/3xT;->d:I

    .line 657920
    iget-object v0, p0, LX/3xT;->t:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    iget-boolean v0, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->i:Z

    iput-boolean v0, v3, LX/3xN;->c:Z

    .line 657921
    :goto_3
    iget-object v0, p0, LX/3xT;->t:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    iget v0, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->e:I

    const/4 v4, 0x1

    if-le v0, v4, :cond_7

    .line 657922
    iget-object v0, p0, LX/3xT;->f:LX/3xQ;

    iget-object v4, p0, LX/3xT;->t:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    iget-object v4, v4, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->f:[I

    iput-object v4, v0, LX/3xQ;->a:[I

    .line 657923
    iget-object v0, p0, LX/3xT;->f:LX/3xQ;

    iget-object v4, p0, LX/3xT;->t:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    iget-object v4, v4, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->g:Ljava/util/List;

    iput-object v4, v0, LX/3xQ;->b:Ljava/util/List;

    .line 657924
    :cond_7
    :goto_4
    invoke-direct {p0, p2, v3}, LX/3xT;->a(LX/1Ok;LX/3xN;)V

    .line 657925
    iget-object v0, p0, LX/3xT;->t:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    if-nez v0, :cond_9

    .line 657926
    iget-boolean v0, v3, LX/3xN;->c:Z

    iget-boolean v4, p0, LX/3xT;->o:Z

    if-ne v0, v4, :cond_8

    invoke-static {p0}, LX/3xT;->m(LX/3xT;)Z

    move-result v0

    iget-boolean v4, p0, LX/3xT;->p:Z

    if-eq v0, v4, :cond_9

    .line 657927
    :cond_8
    iget-object v0, p0, LX/3xT;->f:LX/3xQ;

    invoke-virtual {v0}, LX/3xQ;->a()V

    .line 657928
    iput-boolean v2, v3, LX/3xN;->d:Z

    .line 657929
    :cond_9
    invoke-virtual {p0}, LX/1OR;->v()I

    move-result v0

    if-lez v0, :cond_f

    iget-object v0, p0, LX/3xT;->t:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    if-eqz v0, :cond_a

    iget-object v0, p0, LX/3xT;->t:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    iget v0, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->c:I

    if-gtz v0, :cond_f

    .line 657930
    :cond_a
    iget-boolean v0, v3, LX/3xN;->d:Z

    if-eqz v0, :cond_d

    move v0, v1

    .line 657931
    :goto_5
    iget v4, p0, LX/3xT;->g:I

    if-ge v0, v4, :cond_f

    .line 657932
    iget-object v4, p0, LX/3xT;->h:[LX/3xS;

    aget-object v4, v4, v0

    invoke-virtual {v4}, LX/3xS;->c()V

    .line 657933
    iget v4, v3, LX/3xN;->b:I

    if-eq v4, v8, :cond_b

    .line 657934
    iget-object v4, p0, LX/3xT;->h:[LX/3xS;

    aget-object v4, v4, v0

    iget v5, v3, LX/3xN;->b:I

    invoke-virtual {v4, v5}, LX/3xS;->c(I)V

    .line 657935
    :cond_b
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 657936
    :cond_c
    invoke-static {p0}, LX/3xT;->l(LX/3xT;)V

    .line 657937
    iget-boolean v0, p0, LX/3xT;->c:Z

    iput-boolean v0, v3, LX/3xN;->c:Z

    goto :goto_4

    :cond_d
    move v0, v1

    .line 657938
    :goto_6
    iget v4, p0, LX/3xT;->g:I

    if-ge v0, v4, :cond_f

    .line 657939
    iget-object v4, p0, LX/3xT;->h:[LX/3xS;

    aget-object v4, v4, v0

    iget-boolean v5, p0, LX/3xT;->c:Z

    iget v6, v3, LX/3xN;->b:I

    const/high16 v11, -0x80000000

    .line 657940
    if-eqz v5, :cond_18

    .line 657941
    invoke-virtual {v4, v11}, LX/3xS;->b(I)I

    move-result v9

    .line 657942
    :goto_7
    invoke-virtual {v4}, LX/3xS;->c()V

    .line 657943
    if-ne v9, v11, :cond_19

    .line 657944
    :cond_e
    :goto_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 657945
    :cond_f
    invoke-virtual {p0, p1}, LX/1OR;->a(LX/1Od;)V

    .line 657946
    iput-boolean v1, p0, LX/3xT;->z:Z

    .line 657947
    const/high16 v6, 0x40000000    # 2.0f

    const/4 v5, 0x0

    .line 657948
    iget-object v0, p0, LX/3xT;->b:LX/1P5;

    invoke-virtual {v0}, LX/1P5;->f()I

    move-result v0

    iget v4, p0, LX/3xT;->g:I

    div-int/2addr v0, v4

    iput v0, p0, LX/3xT;->j:I

    .line 657949
    iget-object v0, p0, LX/3xT;->b:LX/1P5;

    invoke-virtual {v0}, LX/1P5;->f()I

    move-result v0

    invoke-static {v0, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    iput v0, p0, LX/3xT;->u:I

    .line 657950
    iget v0, p0, LX/3xT;->i:I

    const/4 v4, 0x1

    if-ne v0, v4, :cond_1d

    .line 657951
    iget v0, p0, LX/3xT;->j:I

    invoke-static {v0, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    iput v0, p0, LX/3xT;->v:I

    .line 657952
    invoke-static {v5, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    iput v0, p0, LX/3xT;->w:I

    .line 657953
    :goto_9
    iget v0, v3, LX/3xN;->a:I

    invoke-direct {p0, v0, p2}, LX/3xT;->a(ILX/1Ok;)V

    .line 657954
    iget-boolean v0, v3, LX/3xN;->c:Z

    if-eqz v0, :cond_15

    .line 657955
    invoke-direct {p0, v7}, LX/3xT;->b(I)V

    .line 657956
    iget-object v0, p0, LX/3xT;->k:LX/3wv;

    invoke-direct {p0, p1, v0, p2}, LX/3xT;->a(LX/1Od;LX/3wv;LX/1Ok;)I

    .line 657957
    invoke-direct {p0, v2}, LX/3xT;->b(I)V

    .line 657958
    iget-object v0, p0, LX/3xT;->k:LX/3wv;

    iget v4, v3, LX/3xN;->a:I

    iget-object v5, p0, LX/3xT;->k:LX/3wv;

    iget v5, v5, LX/3wv;->c:I

    add-int/2addr v4, v5

    iput v4, v0, LX/3wv;->b:I

    .line 657959
    iget-object v0, p0, LX/3xT;->k:LX/3wv;

    invoke-direct {p0, p1, v0, p2}, LX/3xT;->a(LX/1Od;LX/3wv;LX/1Ok;)I

    .line 657960
    :goto_a
    invoke-virtual {p0}, LX/1OR;->v()I

    move-result v0

    if-lez v0, :cond_10

    .line 657961
    iget-boolean v0, p0, LX/3xT;->c:Z

    if-eqz v0, :cond_16

    .line 657962
    invoke-direct {p0, p1, p2, v2}, LX/3xT;->a(LX/1Od;LX/1Ok;Z)V

    .line 657963
    invoke-direct {p0, p1, p2, v1}, LX/3xT;->b(LX/1Od;LX/1Ok;Z)V

    .line 657964
    :cond_10
    :goto_b
    iget-boolean v0, p2, LX/1Ok;->k:Z

    move v0, v0

    .line 657965
    if-nez v0, :cond_14

    .line 657966
    iget v0, p0, LX/3xT;->n:I

    if-eqz v0, :cond_12

    invoke-virtual {p0}, LX/1OR;->v()I

    move-result v0

    if-lez v0, :cond_12

    iget-boolean v0, p0, LX/3xT;->z:Z

    if-nez v0, :cond_11

    invoke-direct {p0}, LX/3xT;->i()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_12

    :cond_11
    move v1, v2

    .line 657967
    :cond_12
    if-eqz v1, :cond_13

    .line 657968
    iget-object v0, p0, LX/3xT;->B:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, LX/1OR;->b(Ljava/lang/Runnable;)Z

    .line 657969
    iget-object v0, p0, LX/3xT;->B:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, LX/1OR;->a(Ljava/lang/Runnable;)V

    .line 657970
    :cond_13
    iput v7, p0, LX/3xT;->d:I

    .line 657971
    iput v8, p0, LX/3xT;->e:I

    .line 657972
    :cond_14
    iget-boolean v0, v3, LX/3xN;->c:Z

    iput-boolean v0, p0, LX/3xT;->o:Z

    .line 657973
    invoke-static {p0}, LX/3xT;->m(LX/3xT;)Z

    move-result v0

    iput-boolean v0, p0, LX/3xT;->p:Z

    .line 657974
    const/4 v0, 0x0

    iput-object v0, p0, LX/3xT;->t:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    goto/16 :goto_0

    .line 657975
    :cond_15
    invoke-direct {p0, v2}, LX/3xT;->b(I)V

    .line 657976
    iget-object v0, p0, LX/3xT;->k:LX/3wv;

    invoke-direct {p0, p1, v0, p2}, LX/3xT;->a(LX/1Od;LX/3wv;LX/1Ok;)I

    .line 657977
    invoke-direct {p0, v7}, LX/3xT;->b(I)V

    .line 657978
    iget-object v0, p0, LX/3xT;->k:LX/3wv;

    iget v4, v3, LX/3xN;->a:I

    iget-object v5, p0, LX/3xT;->k:LX/3wv;

    iget v5, v5, LX/3wv;->c:I

    add-int/2addr v4, v5

    iput v4, v0, LX/3wv;->b:I

    .line 657979
    iget-object v0, p0, LX/3xT;->k:LX/3wv;

    invoke-direct {p0, p1, v0, p2}, LX/3xT;->a(LX/1Od;LX/3wv;LX/1Ok;)I

    goto :goto_a

    .line 657980
    :cond_16
    invoke-direct {p0, p1, p2, v2}, LX/3xT;->b(LX/1Od;LX/1Ok;Z)V

    .line 657981
    invoke-direct {p0, p1, p2, v1}, LX/3xT;->a(LX/1Od;LX/1Ok;Z)V

    goto :goto_b

    .line 657982
    :cond_17
    iget-boolean v0, p0, LX/3xT;->c:Z

    iput-boolean v0, v3, LX/3xN;->c:Z

    goto/16 :goto_3

    .line 657983
    :cond_18
    invoke-virtual {v4, v11}, LX/3xS;->a(I)I

    move-result v9

    goto/16 :goto_7

    .line 657984
    :cond_19
    if-eqz v5, :cond_1a

    iget-object v10, v4, LX/3xS;->e:LX/3xT;

    iget-object v10, v10, LX/3xT;->a:LX/1P5;

    invoke-virtual {v10}, LX/1P5;->d()I

    move-result v10

    if-lt v9, v10, :cond_e

    :cond_1a
    if-nez v5, :cond_1b

    iget-object v10, v4, LX/3xS;->e:LX/3xT;

    iget-object v10, v10, LX/3xT;->a:LX/1P5;

    invoke-virtual {v10}, LX/1P5;->c()I

    move-result v10

    if-gt v9, v10, :cond_e

    .line 657985
    :cond_1b
    if-eq v6, v11, :cond_1c

    .line 657986
    add-int/2addr v9, v6

    .line 657987
    :cond_1c
    iput v9, v4, LX/3xS;->b:I

    iput v9, v4, LX/3xS;->a:I

    goto/16 :goto_8

    .line 657988
    :cond_1d
    iget v0, p0, LX/3xT;->j:I

    invoke-static {v0, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    iput v0, p0, LX/3xT;->w:I

    .line 657989
    invoke-static {v5, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    iput v0, p0, LX/3xT;->v:I

    goto/16 :goto_9
.end method

.method public final d(LX/1Ok;)I
    .locals 1

    .prologue
    .line 657879
    invoke-direct {p0, p1}, LX/3xT;->h(LX/1Ok;)I

    move-result v0

    return v0
.end method

.method public final e(LX/1Ok;)I
    .locals 1

    .prologue
    .line 657878
    invoke-direct {p0, p1}, LX/3xT;->h(LX/1Ok;)I

    move-result v0

    return v0
.end method

.method public final e(I)V
    .locals 3

    .prologue
    .line 657809
    iget-object v0, p0, LX/3xT;->t:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3xT;->t:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    iget v0, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->a:I

    if-eq v0, p1, :cond_0

    .line 657810
    iget-object v0, p0, LX/3xT;->t:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    const/4 v2, -0x1

    .line 657811
    const/4 v1, 0x0

    iput-object v1, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->d:[I

    .line 657812
    const/4 v1, 0x0

    iput v1, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->c:I

    .line 657813
    iput v2, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->a:I

    .line 657814
    iput v2, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->b:I

    .line 657815
    :cond_0
    iput p1, p0, LX/3xT;->d:I

    .line 657816
    const/high16 v0, -0x80000000

    iput v0, p0, LX/3xT;->e:I

    .line 657817
    invoke-virtual {p0}, LX/1OR;->p()V

    .line 657818
    return-void
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 657874
    iget-object v0, p0, LX/3xT;->t:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f(LX/1Ok;)I
    .locals 1

    .prologue
    .line 657873
    invoke-direct {p0, p1}, LX/3xT;->i(LX/1Ok;)I

    move-result v0

    return v0
.end method

.method public final f()Landroid/os/Parcelable;
    .locals 5

    .prologue
    const/4 v3, -0x1

    const/4 v1, 0x0

    const/high16 v4, -0x80000000

    .line 657835
    iget-object v0, p0, LX/3xT;->t:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    if-eqz v0, :cond_0

    .line 657836
    new-instance v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    iget-object v1, p0, LX/3xT;->t:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    invoke-direct {v0, v1}, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;-><init>(Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;)V

    .line 657837
    :goto_0
    return-object v0

    .line 657838
    :cond_0
    new-instance v2, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    invoke-direct {v2}, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;-><init>()V

    .line 657839
    iget-boolean v0, p0, LX/3xT;->l:Z

    iput-boolean v0, v2, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->h:Z

    .line 657840
    iget-boolean v0, p0, LX/3xT;->o:Z

    iput-boolean v0, v2, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->i:Z

    .line 657841
    iget-boolean v0, p0, LX/3xT;->p:Z

    iput-boolean v0, v2, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->j:Z

    .line 657842
    iget-object v0, p0, LX/3xT;->f:LX/3xQ;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/3xT;->f:LX/3xQ;

    iget-object v0, v0, LX/3xQ;->a:[I

    if-eqz v0, :cond_2

    .line 657843
    iget-object v0, p0, LX/3xT;->f:LX/3xQ;

    iget-object v0, v0, LX/3xQ;->a:[I

    iput-object v0, v2, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->f:[I

    .line 657844
    iget-object v0, v2, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->f:[I

    array-length v0, v0

    iput v0, v2, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->e:I

    .line 657845
    iget-object v0, p0, LX/3xT;->f:LX/3xQ;

    iget-object v0, v0, LX/3xQ;->b:Ljava/util/List;

    iput-object v0, v2, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->g:Ljava/util/List;

    .line 657846
    :goto_1
    invoke-virtual {p0}, LX/1OR;->v()I

    move-result v0

    if-lez v0, :cond_5

    .line 657847
    invoke-direct {p0}, LX/3xT;->k()V

    .line 657848
    iget-boolean v0, p0, LX/3xT;->o:Z

    if-eqz v0, :cond_3

    invoke-static {p0}, LX/3xT;->K(LX/3xT;)I

    move-result v0

    :goto_2
    iput v0, v2, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->a:I

    .line 657849
    const/4 v3, 0x1

    .line 657850
    iget-boolean v0, p0, LX/3xT;->c:Z

    if-eqz v0, :cond_7

    invoke-static {p0, v3, v3}, LX/3xT;->b(LX/3xT;ZZ)Landroid/view/View;

    move-result-object v0

    .line 657851
    :goto_3
    if-nez v0, :cond_8

    const/4 v0, -0x1

    :goto_4
    move v0, v0

    .line 657852
    iput v0, v2, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->b:I

    .line 657853
    iget v0, p0, LX/3xT;->g:I

    iput v0, v2, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->c:I

    .line 657854
    iget v0, p0, LX/3xT;->g:I

    new-array v0, v0, [I

    iput-object v0, v2, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->d:[I

    move v0, v1

    .line 657855
    :goto_5
    iget v1, p0, LX/3xT;->g:I

    if-ge v0, v1, :cond_6

    .line 657856
    iget-boolean v1, p0, LX/3xT;->o:Z

    if-eqz v1, :cond_4

    .line 657857
    iget-object v1, p0, LX/3xT;->h:[LX/3xS;

    aget-object v1, v1, v0

    invoke-virtual {v1, v4}, LX/3xS;->b(I)I

    move-result v1

    .line 657858
    if-eq v1, v4, :cond_1

    .line 657859
    iget-object v3, p0, LX/3xT;->a:LX/1P5;

    invoke-virtual {v3}, LX/1P5;->d()I

    move-result v3

    sub-int/2addr v1, v3

    .line 657860
    :cond_1
    :goto_6
    iget-object v3, v2, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->d:[I

    aput v1, v3, v0

    .line 657861
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 657862
    :cond_2
    iput v1, v2, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->e:I

    goto :goto_1

    .line 657863
    :cond_3
    invoke-static {p0}, LX/3xT;->L(LX/3xT;)I

    move-result v0

    goto :goto_2

    .line 657864
    :cond_4
    iget-object v1, p0, LX/3xT;->h:[LX/3xS;

    aget-object v1, v1, v0

    invoke-virtual {v1, v4}, LX/3xS;->a(I)I

    move-result v1

    .line 657865
    if-eq v1, v4, :cond_1

    .line 657866
    iget-object v3, p0, LX/3xT;->a:LX/1P5;

    invoke-virtual {v3}, LX/1P5;->c()I

    move-result v3

    sub-int/2addr v1, v3

    goto :goto_6

    .line 657867
    :cond_5
    iput v3, v2, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->a:I

    .line 657868
    iput v3, v2, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->b:I

    .line 657869
    iput v1, v2, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->c:I

    :cond_6
    move-object v0, v2

    .line 657870
    goto/16 :goto_0

    .line 657871
    :cond_7
    invoke-static {p0, v3, v3}, LX/3xT;->a(LX/3xT;ZZ)Landroid/view/View;

    move-result-object v0

    goto :goto_3

    .line 657872
    :cond_8
    invoke-static {v0}, LX/1OR;->d(Landroid/view/View;)I

    move-result v0

    goto :goto_4
.end method

.method public final g(LX/1Ok;)I
    .locals 1

    .prologue
    .line 657834
    invoke-direct {p0, p1}, LX/3xT;->i(LX/1Ok;)I

    move-result v0

    return v0
.end method

.method public final g(I)V
    .locals 2

    .prologue
    .line 657829
    invoke-super {p0, p1}, LX/1OR;->g(I)V

    .line 657830
    const/4 v0, 0x0

    :goto_0
    iget v1, p0, LX/3xT;->g:I

    if-ge v0, v1, :cond_0

    .line 657831
    iget-object v1, p0, LX/3xT;->h:[LX/3xS;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, LX/3xS;->d(I)V

    .line 657832
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 657833
    :cond_0
    return-void
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 657828
    iget v0, p0, LX/3xT;->i:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h(I)V
    .locals 2

    .prologue
    .line 657823
    invoke-super {p0, p1}, LX/1OR;->h(I)V

    .line 657824
    const/4 v0, 0x0

    :goto_0
    iget v1, p0, LX/3xT;->g:I

    if-ge v0, v1, :cond_0

    .line 657825
    iget-object v1, p0, LX/3xT;->h:[LX/3xS;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, LX/3xS;->d(I)V

    .line 657826
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 657827
    :cond_0
    return-void
.end method

.method public final h()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 657819
    iget v1, p0, LX/3xT;->i:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i(I)V
    .locals 0

    .prologue
    .line 657820
    if-nez p1, :cond_0

    .line 657821
    invoke-static {p0}, LX/3xT;->d(LX/3xT;)Z

    .line 657822
    :cond_0
    return-void
.end method
