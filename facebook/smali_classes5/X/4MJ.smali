.class public LX/4MJ;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 687460
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 687461
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 687462
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 687463
    :goto_0
    return v1

    .line 687464
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 687465
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_4

    .line 687466
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 687467
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 687468
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_1

    if-eqz v4, :cond_1

    .line 687469
    const-string v5, "end"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 687470
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 687471
    :cond_2
    const-string v5, "start"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 687472
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 687473
    :cond_3
    const-string v5, "timezone"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 687474
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 687475
    :cond_4
    const/4 v4, 0x3

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 687476
    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 687477
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 687478
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 687479
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    move v3, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 2

    .prologue
    .line 687480
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 687481
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 687482
    if-eqz v0, :cond_0

    .line 687483
    const-string v1, "end"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 687484
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 687485
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 687486
    if-eqz v0, :cond_1

    .line 687487
    const-string v1, "start"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 687488
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 687489
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 687490
    if-eqz v0, :cond_2

    .line 687491
    const-string v1, "timezone"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 687492
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 687493
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 687494
    return-void
.end method
