.class public LX/4SM;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 714010
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 16

    .prologue
    .line 714011
    const/4 v12, 0x0

    .line 714012
    const/4 v11, 0x0

    .line 714013
    const/4 v10, 0x0

    .line 714014
    const/4 v9, 0x0

    .line 714015
    const/4 v8, 0x0

    .line 714016
    const/4 v7, 0x0

    .line 714017
    const/4 v6, 0x0

    .line 714018
    const/4 v5, 0x0

    .line 714019
    const/4 v4, 0x0

    .line 714020
    const/4 v3, 0x0

    .line 714021
    const/4 v2, 0x0

    .line 714022
    const/4 v1, 0x0

    .line 714023
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->START_OBJECT:LX/15z;

    if-eq v13, v14, :cond_1

    .line 714024
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 714025
    const/4 v1, 0x0

    .line 714026
    :goto_0
    return v1

    .line 714027
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 714028
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->END_OBJECT:LX/15z;

    if-eq v13, v14, :cond_9

    .line 714029
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v13

    .line 714030
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 714031
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v14, v15, :cond_1

    if-eqz v13, :cond_1

    .line 714032
    const-string v14, "component_logical_path"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 714033
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    goto :goto_1

    .line 714034
    :cond_2
    const-string v14, "component_style"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 714035
    const/4 v4, 0x1

    .line 714036
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v11

    goto :goto_1

    .line 714037
    :cond_3
    const-string v14, "component_tracking_data"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_4

    .line 714038
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto :goto_1

    .line 714039
    :cond_4
    const-string v14, "has_bottom_border"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 714040
    const/4 v3, 0x1

    .line 714041
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v9

    goto :goto_1

    .line 714042
    :cond_5
    const-string v14, "has_inner_borders"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 714043
    const/4 v2, 0x1

    .line 714044
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v8

    goto :goto_1

    .line 714045
    :cond_6
    const-string v14, "has_top_border"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_7

    .line 714046
    const/4 v1, 0x1

    .line 714047
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v7

    goto :goto_1

    .line 714048
    :cond_7
    const-string v14, "sub_components"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_8

    .line 714049
    invoke-static/range {p0 .. p1}, LX/4SK;->a(LX/15w;LX/186;)I

    move-result v6

    goto/16 :goto_1

    .line 714050
    :cond_8
    const-string v14, "empty_state_action"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 714051
    invoke-static/range {p0 .. p1}, LX/4SH;->a(LX/15w;LX/186;)I

    move-result v5

    goto/16 :goto_1

    .line 714052
    :cond_9
    const/16 v13, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->c(I)V

    .line 714053
    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v12}, LX/186;->b(II)V

    .line 714054
    if-eqz v4, :cond_a

    .line 714055
    const/4 v4, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v11}, LX/186;->a(ILjava/lang/Enum;)V

    .line 714056
    :cond_a
    const/4 v4, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v10}, LX/186;->b(II)V

    .line 714057
    if-eqz v3, :cond_b

    .line 714058
    const/4 v3, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->a(IZ)V

    .line 714059
    :cond_b
    if-eqz v2, :cond_c

    .line 714060
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->a(IZ)V

    .line 714061
    :cond_c
    if-eqz v1, :cond_d

    .line 714062
    const/4 v1, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v7}, LX/186;->a(IZ)V

    .line 714063
    :cond_d
    const/4 v1, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 714064
    const/4 v1, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 714065
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 714066
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 714067
    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 714068
    if-eqz v0, :cond_0

    .line 714069
    const-string v1, "component_logical_path"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 714070
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 714071
    :cond_0
    invoke-virtual {p0, p1, v3, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 714072
    if-eqz v0, :cond_1

    .line 714073
    const-string v0, "component_style"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 714074
    const-class v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    invoke-virtual {p0, p1, v3, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 714075
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 714076
    if-eqz v0, :cond_2

    .line 714077
    const-string v1, "component_tracking_data"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 714078
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 714079
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 714080
    if-eqz v0, :cond_3

    .line 714081
    const-string v1, "has_bottom_border"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 714082
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 714083
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 714084
    if-eqz v0, :cond_4

    .line 714085
    const-string v1, "has_inner_borders"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 714086
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 714087
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 714088
    if-eqz v0, :cond_5

    .line 714089
    const-string v1, "has_top_border"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 714090
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 714091
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 714092
    if-eqz v0, :cond_6

    .line 714093
    const-string v1, "sub_components"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 714094
    invoke-static {p0, v0, p2, p3}, LX/4SK;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 714095
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 714096
    if-eqz v0, :cond_7

    .line 714097
    const-string v1, "empty_state_action"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 714098
    invoke-static {p0, v0, p2, p3}, LX/4SH;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 714099
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 714100
    return-void
.end method
