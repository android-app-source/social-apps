.class public LX/3St;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "LX/1qM;",
        ">;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 583084
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/1qM;
    .locals 4

    .prologue
    .line 583085
    const-class v1, LX/3St;

    monitor-enter v1

    .line 583086
    :try_start_0
    sget-object v0, LX/3St;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 583087
    sput-object v2, LX/3St;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 583088
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 583089
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 583090
    invoke-static {v0}, LX/3Su;->b(LX/0QB;)LX/3Su;

    move-result-object v3

    check-cast v3, LX/3Su;

    invoke-static {v0}, Lcom/facebook/notifications/service/NotificationsServiceHandler;->b(LX/0QB;)Lcom/facebook/notifications/service/NotificationsServiceHandler;

    move-result-object p0

    check-cast p0, Lcom/facebook/notifications/service/NotificationsServiceHandler;

    invoke-static {v3, p0}, LX/3Sv;->a(LX/3Su;Lcom/facebook/notifications/service/NotificationsServiceHandler;)LX/1qM;

    move-result-object v3

    move-object v0, v3

    .line 583091
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 583092
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1qM;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 583093
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 583094
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 583095
    invoke-static {p0}, LX/3Su;->b(LX/0QB;)LX/3Su;

    move-result-object v0

    check-cast v0, LX/3Su;

    invoke-static {p0}, Lcom/facebook/notifications/service/NotificationsServiceHandler;->b(LX/0QB;)Lcom/facebook/notifications/service/NotificationsServiceHandler;

    move-result-object v1

    check-cast v1, Lcom/facebook/notifications/service/NotificationsServiceHandler;

    invoke-static {v0, v1}, LX/3Sv;->a(LX/3Su;Lcom/facebook/notifications/service/NotificationsServiceHandler;)LX/1qM;

    move-result-object v0

    return-object v0
.end method
