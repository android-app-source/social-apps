.class public LX/4bP;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/4bP;


# instance fields
.field public final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/15D",
            "<*>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 793691
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 793692
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/4bP;->a:Ljava/util/ArrayList;

    .line 793693
    return-void
.end method

.method public static a(LX/0QB;)LX/4bP;
    .locals 3

    .prologue
    .line 793694
    sget-object v0, LX/4bP;->b:LX/4bP;

    if-nez v0, :cond_1

    .line 793695
    const-class v1, LX/4bP;

    monitor-enter v1

    .line 793696
    :try_start_0
    sget-object v0, LX/4bP;->b:LX/4bP;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 793697
    if-eqz v2, :cond_0

    .line 793698
    :try_start_1
    new-instance v0, LX/4bP;

    invoke-direct {v0}, LX/4bP;-><init>()V

    .line 793699
    move-object v0, v0

    .line 793700
    sput-object v0, LX/4bP;->b:LX/4bP;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 793701
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 793702
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 793703
    :cond_1
    sget-object v0, LX/4bP;->b:LX/4bP;

    return-object v0

    .line 793704
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 793705
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final b()I
    .locals 1

    .prologue
    .line 793706
    iget-object v0, p0, LX/4bP;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final c()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "LX/15D",
            "<*>;>;"
        }
    .end annotation

    .prologue
    .line 793707
    iget-object v0, p0, LX/4bP;->a:Ljava/util/ArrayList;

    invoke-static {v0}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method
