.class public LX/3WL;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# static fields
.field public static final b:LX/1Cz;


# instance fields
.field public a:LX/20J;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final c:Lcom/facebook/resources/ui/FbButton;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 590133
    new-instance v0, LX/3WM;

    invoke-direct {v0}, LX/3WM;-><init>()V

    sput-object v0, LX/3WL;->b:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 590131
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/3WL;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 590132
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 590129
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/3WL;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 590130
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 590134
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 590135
    const v0, 0x7f030682

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 590136
    const v0, 0x7f0d11df

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, LX/3WL;->c:Lcom/facebook/resources/ui/FbButton;

    .line 590137
    const-class v0, LX/3WL;

    invoke-static {v0, p0}, LX/3WL;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 590138
    const v0, 0x7f030682

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 590139
    iget-object v0, p0, LX/3WL;->a:LX/20J;

    sget-object v1, LX/1Wl;->VISIBLE:LX/1Wl;

    .line 590140
    iput-object v1, v0, LX/20J;->d:LX/1Wl;

    .line 590141
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/3WL;

    invoke-static {p0}, LX/20J;->b(LX/0QB;)LX/20J;

    move-result-object p0

    check-cast p0, LX/20J;

    iput-object p0, p1, LX/3WL;->a:LX/20J;

    return-void
.end method


# virtual methods
.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 590126
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 590127
    iget-object v0, p0, LX/3WL;->a:LX/20J;

    invoke-virtual {v0, p0, p1}, LX/20J;->a(Landroid/view/View;Landroid/graphics/Canvas;)V

    .line 590128
    return-void
.end method

.method public getButton()Lcom/facebook/resources/ui/FbButton;
    .locals 1

    .prologue
    .line 590125
    iget-object v0, p0, LX/3WL;->c:Lcom/facebook/resources/ui/FbButton;

    return-object v0
.end method

.method public setButtonState(LX/C0a;)V
    .locals 1

    .prologue
    .line 590123
    iget-object v0, p0, LX/3WL;->c:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {p1, v0}, LX/C0a;->setState(Lcom/facebook/resources/ui/FbButton;)Lcom/facebook/resources/ui/FbButton;

    .line 590124
    return-void
.end method
