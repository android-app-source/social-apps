.class public LX/3hh;
.super LX/3hi;
.source ""


# instance fields
.field public final b:I

.field public final c:I

.field public final d:I


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 627469
    invoke-direct {p0}, LX/3hi;-><init>()V

    .line 627470
    const/high16 v0, 0x40400000    # 3.0f

    invoke-static {p1, v0}, LX/0tP;->a(Landroid/content/res/Resources;F)I

    move-result v0

    iput v0, p0, LX/3hh;->b:I

    .line 627471
    const/high16 v0, 0x42200000    # 40.0f

    invoke-static {p1, v0}, LX/0tP;->a(Landroid/content/res/Resources;F)I

    move-result v0

    iput v0, p0, LX/3hh;->c:I

    .line 627472
    const/high16 v0, 0x40e00000    # 7.0f

    invoke-static {p1, v0}, LX/0tP;->a(Landroid/content/res/Resources;F)I

    move-result v0

    iput v0, p0, LX/3hh;->d:I

    .line 627473
    return-void
.end method

.method private static a(Landroid/graphics/Canvas;ILandroid/graphics/RectF;)V
    .locals 1

    .prologue
    .line 627465
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 627466
    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 627467
    invoke-virtual {p0, p2, v0}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 627468
    return-void
.end method

.method public static b(LX/0QB;)LX/3hh;
    .locals 2

    .prologue
    .line 627438
    new-instance v1, LX/3hh;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    invoke-direct {v1, v0}, LX/3hh;-><init>(Landroid/content/res/Resources;)V

    .line 627439
    return-object v1
.end method


# virtual methods
.method public final a(Landroid/graphics/Bitmap;LX/1FZ;)LX/1FJ;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Bitmap;",
            "LX/1FZ;",
            ")",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 627440
    iget v0, p0, LX/3hh;->c:I

    iget v1, p0, LX/3hh;->b:I

    mul-int/lit8 v1, v1, 0x4

    add-int/2addr v1, v0

    .line 627441
    iget v0, p0, LX/3hh;->d:I

    add-int/2addr v0, v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {p2, v1, v0, v2}, LX/1FZ;->a(IILandroid/graphics/Bitmap$Config;)LX/1FJ;

    move-result-object v2

    .line 627442
    new-instance v3, Landroid/graphics/Canvas;

    invoke-virtual {v2}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-direct {v3, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 627443
    const-string v0, "#fff5156f"

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    .line 627444
    new-instance v4, Landroid/graphics/RectF;

    int-to-float v5, v1

    int-to-float v6, v1

    invoke-direct {v4, v7, v7, v5, v6}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-static {v3, v0, v4}, LX/3hh;->a(Landroid/graphics/Canvas;ILandroid/graphics/RectF;)V

    .line 627445
    const/4 v4, -0x1

    new-instance v5, Landroid/graphics/RectF;

    iget v6, p0, LX/3hh;->b:I

    int-to-float v6, v6

    iget v7, p0, LX/3hh;->b:I

    int-to-float v7, v7

    iget v8, p0, LX/3hh;->b:I

    mul-int/lit8 v8, v8, 0x3

    iget v9, p0, LX/3hh;->c:I

    add-int/2addr v8, v9

    int-to-float v8, v8

    iget v9, p0, LX/3hh;->b:I

    mul-int/lit8 v9, v9, 0x3

    iget v10, p0, LX/3hh;->c:I

    add-int/2addr v9, v10

    int-to-float v9, v9

    invoke-direct {v5, v6, v7, v8, v9}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-static {v3, v4, v5}, LX/3hh;->a(Landroid/graphics/Canvas;ILandroid/graphics/RectF;)V

    .line 627446
    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    .line 627447
    invoke-virtual {v4, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 627448
    const/high16 v5, 0x3f800000    # 1.0f

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 627449
    sget-object v5, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 627450
    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 627451
    new-instance v5, Landroid/graphics/Point;

    div-int/lit8 v6, v1, 0x2

    iget v7, p0, LX/3hh;->d:I

    sub-int/2addr v6, v7

    invoke-direct {v5, v6, v1}, Landroid/graphics/Point;-><init>(II)V

    .line 627452
    new-instance v6, Landroid/graphics/Point;

    div-int/lit8 v7, v1, 0x2

    iget v8, p0, LX/3hh;->d:I

    add-int/2addr v8, v1

    invoke-direct {v6, v7, v8}, Landroid/graphics/Point;-><init>(II)V

    .line 627453
    new-instance v7, Landroid/graphics/Point;

    div-int/lit8 v8, v1, 0x2

    iget v9, p0, LX/3hh;->d:I

    add-int/2addr v8, v9

    invoke-direct {v7, v8, v1}, Landroid/graphics/Point;-><init>(II)V

    .line 627454
    new-instance v8, Landroid/graphics/Path;

    invoke-direct {v8}, Landroid/graphics/Path;-><init>()V

    .line 627455
    iget v9, v5, Landroid/graphics/Point;->x:I

    int-to-float v9, v9

    iget v10, v5, Landroid/graphics/Point;->y:I

    int-to-float v10, v10

    invoke-virtual {v8, v9, v10}, Landroid/graphics/Path;->moveTo(FF)V

    .line 627456
    iget v9, v6, Landroid/graphics/Point;->x:I

    int-to-float v9, v9

    iget v6, v6, Landroid/graphics/Point;->y:I

    int-to-float v6, v6

    invoke-virtual {v8, v9, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 627457
    iget v6, v7, Landroid/graphics/Point;->x:I

    int-to-float v6, v6

    iget v7, v7, Landroid/graphics/Point;->y:I

    int-to-float v7, v7

    invoke-virtual {v8, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 627458
    iget v6, v5, Landroid/graphics/Point;->x:I

    int-to-float v6, v6

    iget v5, v5, Landroid/graphics/Point;->y:I

    int-to-float v5, v5

    invoke-virtual {v8, v6, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 627459
    invoke-virtual {v8}, Landroid/graphics/Path;->close()V

    .line 627460
    invoke-virtual {v3, v8, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 627461
    const/4 v8, 0x0

    .line 627462
    invoke-virtual {v2}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Bitmap;->setHasAlpha(Z)V

    .line 627463
    new-instance v0, Landroid/graphics/RectF;

    iget v1, p0, LX/3hh;->b:I

    mul-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    iget v4, p0, LX/3hh;->b:I

    mul-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    iget v5, p0, LX/3hh;->b:I

    mul-int/lit8 v5, v5, 0x2

    iget v6, p0, LX/3hh;->c:I

    add-int/2addr v5, v6

    int-to-float v5, v5

    iget v6, p0, LX/3hh;->b:I

    mul-int/lit8 v6, v6, 0x2

    iget v7, p0, LX/3hh;->c:I

    add-int/2addr v6, v7

    int-to-float v6, v6

    invoke-direct {v0, v1, v4, v5, v6}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual {v3, p1, v8, v0, v8}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 627464
    return-object v2
.end method
