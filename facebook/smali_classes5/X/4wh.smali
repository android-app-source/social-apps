.class public final LX/4wh;
.super LX/4wg;
.source ""

# interfaces
.implements LX/4wb;
.implements Ljava/io/Serializable;


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 820359
    invoke-direct {p0}, LX/4wg;-><init>()V

    .line 820360
    return-void
.end method

.method private c()J
    .locals 8

    .prologue
    .line 820342
    iget-wide v0, p0, LX/4wg;->e:J

    .line 820343
    iget-object v3, p0, LX/4wg;->d:[LX/4wk;

    .line 820344
    if-eqz v3, :cond_1

    .line 820345
    array-length v4, v3

    .line 820346
    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_1

    .line 820347
    aget-object v5, v3, v2

    .line 820348
    if-eqz v5, :cond_0

    .line 820349
    iget-wide v6, v5, LX/4wk;->a:J

    add-long/2addr v0, v6

    .line 820350
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 820351
    :cond_1
    return-wide v0
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 2

    .prologue
    .line 820361
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 820362
    const/4 v0, 0x0

    iput v0, p0, LX/4wh;->f:I

    .line 820363
    const/4 v0, 0x0

    iput-object v0, p0, LX/4wh;->d:[LX/4wk;

    .line 820364
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, LX/4wh;->e:J

    .line 820365
    return-void
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 2

    .prologue
    .line 820367
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V

    .line 820368
    invoke-direct {p0}, LX/4wh;->c()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Ljava/io/ObjectOutputStream;->writeLong(J)V

    .line 820369
    return-void
.end method


# virtual methods
.method public final a(JJ)J
    .locals 3

    .prologue
    .line 820366
    add-long v0, p1, p3

    return-wide v0
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 820352
    const-wide/16 v0, 0x1

    invoke-virtual {p0, v0, v1}, LX/4wh;->a(J)V

    .line 820353
    return-void
.end method

.method public final a(J)V
    .locals 9

    .prologue
    .line 820354
    iget-object v2, p0, LX/4wg;->d:[LX/4wk;

    if-nez v2, :cond_0

    iget-wide v0, p0, LX/4wg;->e:J

    add-long v4, v0, p1

    invoke-virtual {p0, v0, v1, v4, v5}, LX/4wg;->b(JJ)Z

    move-result v0

    if-nez v0, :cond_2

    .line 820355
    :cond_0
    const/4 v1, 0x1

    .line 820356
    sget-object v0, LX/4wg;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    if-eqz v0, :cond_1

    if-eqz v2, :cond_1

    array-length v3, v2

    if-lez v3, :cond_1

    add-int/lit8 v3, v3, -0x1

    const/4 v4, 0x0

    aget v4, v0, v4

    and-int/2addr v3, v4

    aget-object v2, v2, v3

    if-eqz v2, :cond_1

    iget-wide v4, v2, LX/4wk;->a:J

    add-long v6, v4, p1

    invoke-virtual {v2, v4, v5, v6, v7}, LX/4wk;->a(JJ)Z

    move-result v1

    if-nez v1, :cond_2

    .line 820357
    :cond_1
    invoke-virtual {p0, p1, p2, v0, v1}, LX/4wg;->a(J[IZ)V

    .line 820358
    :cond_2
    return-void
.end method

.method public final doubleValue()D
    .locals 2

    .prologue
    .line 820338
    invoke-direct {p0}, LX/4wh;->c()J

    move-result-wide v0

    long-to-double v0, v0

    return-wide v0
.end method

.method public final floatValue()F
    .locals 2

    .prologue
    .line 820337
    invoke-direct {p0}, LX/4wh;->c()J

    move-result-wide v0

    long-to-float v0, v0

    return v0
.end method

.method public final intValue()I
    .locals 2

    .prologue
    .line 820339
    invoke-direct {p0}, LX/4wh;->c()J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public final longValue()J
    .locals 2

    .prologue
    .line 820340
    invoke-direct {p0}, LX/4wh;->c()J

    move-result-wide v0

    return-wide v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 820341
    invoke-direct {p0}, LX/4wh;->c()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
