.class public final LX/40N;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 27

    .prologue
    .line 663270
    const/16 v23, 0x0

    .line 663271
    const/16 v22, 0x0

    .line 663272
    const/16 v21, 0x0

    .line 663273
    const/16 v20, 0x0

    .line 663274
    const/16 v19, 0x0

    .line 663275
    const/16 v18, 0x0

    .line 663276
    const/16 v17, 0x0

    .line 663277
    const/16 v16, 0x0

    .line 663278
    const/4 v15, 0x0

    .line 663279
    const/4 v14, 0x0

    .line 663280
    const/4 v13, 0x0

    .line 663281
    const/4 v12, 0x0

    .line 663282
    const/4 v11, 0x0

    .line 663283
    const/4 v10, 0x0

    .line 663284
    const/4 v9, 0x0

    .line 663285
    const/4 v8, 0x0

    .line 663286
    const/4 v7, 0x0

    .line 663287
    const/4 v6, 0x0

    .line 663288
    const/4 v5, 0x0

    .line 663289
    const/4 v4, 0x0

    .line 663290
    const/4 v3, 0x0

    .line 663291
    const/4 v2, 0x0

    .line 663292
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v24

    sget-object v25, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    if-eq v0, v1, :cond_1

    .line 663293
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 663294
    const/4 v2, 0x0

    .line 663295
    :goto_0
    return v2

    .line 663296
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 663297
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v24

    sget-object v25, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    if-eq v0, v1, :cond_c

    .line 663298
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v24

    .line 663299
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 663300
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v25

    sget-object v26, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    if-eq v0, v1, :cond_1

    if-eqz v24, :cond_1

    .line 663301
    const-string v25, "is_add_to_home_screen_enabled"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_2

    .line 663302
    const/4 v12, 0x1

    .line 663303
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v23

    goto :goto_1

    .line 663304
    :cond_2
    const-string v25, "is_autofill_settings_enabled"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_3

    .line 663305
    const/4 v11, 0x1

    .line 663306
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v22

    goto :goto_1

    .line 663307
    :cond_3
    const-string v25, "is_copy_link_enabled"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_4

    .line 663308
    const/4 v10, 0x1

    .line 663309
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v21

    goto :goto_1

    .line 663310
    :cond_4
    const-string v25, "is_log_out_enabled"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_5

    .line 663311
    const/4 v9, 0x1

    .line 663312
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v20

    goto :goto_1

    .line 663313
    :cond_5
    const-string v25, "is_manage_permissions_enabled"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_6

    .line 663314
    const/4 v8, 0x1

    .line 663315
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v19

    goto :goto_1

    .line 663316
    :cond_6
    const-string v25, "is_overflow_menu_enabled"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_7

    .line 663317
    const/4 v7, 0x1

    .line 663318
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v18

    goto :goto_1

    .line 663319
    :cond_7
    const-string v25, "is_payment_enabled"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_8

    .line 663320
    const/4 v6, 0x1

    .line 663321
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v17

    goto/16 :goto_1

    .line 663322
    :cond_8
    const-string v25, "is_product_history_enabled"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_9

    .line 663323
    const/4 v5, 0x1

    .line 663324
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v16

    goto/16 :goto_1

    .line 663325
    :cond_9
    const-string v25, "is_save_autofill_data_banner_enabled"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_a

    .line 663326
    const/4 v4, 0x1

    .line 663327
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v15

    goto/16 :goto_1

    .line 663328
    :cond_a
    const-string v25, "is_scrollable_autofill_bar_enabled"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_b

    .line 663329
    const/4 v3, 0x1

    .line 663330
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v14

    goto/16 :goto_1

    .line 663331
    :cond_b
    const-string v25, "is_webview_chrome_enabled"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_0

    .line 663332
    const/4 v2, 0x1

    .line 663333
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v13

    goto/16 :goto_1

    .line 663334
    :cond_c
    const/16 v24, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 663335
    if-eqz v12, :cond_d

    .line 663336
    const/4 v12, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v12, v1}, LX/186;->a(IZ)V

    .line 663337
    :cond_d
    if-eqz v11, :cond_e

    .line 663338
    const/4 v11, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v11, v1}, LX/186;->a(IZ)V

    .line 663339
    :cond_e
    if-eqz v10, :cond_f

    .line 663340
    const/4 v10, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v10, v1}, LX/186;->a(IZ)V

    .line 663341
    :cond_f
    if-eqz v9, :cond_10

    .line 663342
    const/4 v9, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v9, v1}, LX/186;->a(IZ)V

    .line 663343
    :cond_10
    if-eqz v8, :cond_11

    .line 663344
    const/4 v8, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v8, v1}, LX/186;->a(IZ)V

    .line 663345
    :cond_11
    if-eqz v7, :cond_12

    .line 663346
    const/4 v7, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v7, v1}, LX/186;->a(IZ)V

    .line 663347
    :cond_12
    if-eqz v6, :cond_13

    .line 663348
    const/4 v6, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v6, v1}, LX/186;->a(IZ)V

    .line 663349
    :cond_13
    if-eqz v5, :cond_14

    .line 663350
    const/4 v5, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v5, v1}, LX/186;->a(IZ)V

    .line 663351
    :cond_14
    if-eqz v4, :cond_15

    .line 663352
    const/16 v4, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v15}, LX/186;->a(IZ)V

    .line 663353
    :cond_15
    if-eqz v3, :cond_16

    .line 663354
    const/16 v3, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v14}, LX/186;->a(IZ)V

    .line 663355
    :cond_16
    if-eqz v2, :cond_17

    .line 663356
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->a(IZ)V

    .line 663357
    :cond_17
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0
.end method
