.class public abstract LX/4ro;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/4ro;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 816968
    new-instance v0, LX/4rp;

    invoke-direct {v0}, LX/4rp;-><init>()V

    sput-object v0, LX/4ro;->a:LX/4ro;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 816969
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/4ro;LX/4ro;)LX/4ro;
    .locals 1

    .prologue
    .line 816970
    new-instance v0, LX/4rt;

    invoke-direct {v0, p0, p1}, LX/4rt;-><init>(LX/4ro;LX/4ro;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)LX/4ro;
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 816971
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    move v2, v0

    .line 816972
    :goto_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_1

    .line 816973
    :goto_1
    if-eqz v2, :cond_3

    .line 816974
    if-eqz v0, :cond_2

    .line 816975
    new-instance v0, LX/4rq;

    invoke-direct {v0, p0, p1}, LX/4rq;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 816976
    :goto_2
    return-object v0

    :cond_0
    move v2, v1

    .line 816977
    goto :goto_0

    :cond_1
    move v0, v1

    .line 816978
    goto :goto_1

    .line 816979
    :cond_2
    new-instance v0, LX/4rr;

    invoke-direct {v0, p0}, LX/4rr;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 816980
    :cond_3
    if-eqz v0, :cond_4

    .line 816981
    new-instance v0, LX/4rs;

    invoke-direct {v0, p1}, LX/4rs;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 816982
    :cond_4
    sget-object v0, LX/4ro;->a:LX/4ro;

    goto :goto_2
.end method


# virtual methods
.method public abstract a(Ljava/lang/String;)Ljava/lang/String;
.end method
