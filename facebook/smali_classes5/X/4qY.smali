.class public final LX/4qY;
.super LX/3CB;
.source ""


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JacksonStdImpl;
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1L


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 814512
    const-class v0, Ljava/lang/Character;

    invoke-direct {p0, v0}, LX/3CB;-><init>(Ljava/lang/Class;)V

    return-void
.end method

.method private c(Ljava/lang/String;LX/0n3;)Ljava/lang/Character;
    .locals 2

    .prologue
    .line 814514
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 814515
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    return-object v0

    .line 814516
    :cond_0
    iget-object v0, p0, LX/3CB;->_keyClass:Ljava/lang/Class;

    const-string v1, "can only convert 1-character Strings"

    invoke-virtual {p2, v0, p1, v1}, LX/0n3;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)LX/28E;

    move-result-object v0

    throw v0
.end method


# virtual methods
.method public final synthetic b(Ljava/lang/String;LX/0n3;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 814513
    invoke-direct {p0, p1, p2}, LX/4qY;->c(Ljava/lang/String;LX/0n3;)Ljava/lang/Character;

    move-result-object v0

    return-object v0
.end method
