.class public LX/46l;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:J


# direct methods
.method public constructor <init>(JLjava/util/concurrent/TimeUnit;)V
    .locals 3

    .prologue
    .line 671564
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 671565
    invoke-virtual {p3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v0

    iput-wide v0, p0, LX/46l;->a:J

    .line 671566
    return-void
.end method

.method private static final a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V
    .locals 2

    .prologue
    .line 671510
    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 671511
    const-wide/16 v0, 0x1

    cmp-long v0, p2, v0

    if-lez v0, :cond_0

    .line 671512
    const-string v0, "s"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 671513
    :cond_0
    return-void
.end method

.method private b()J
    .locals 2

    .prologue
    .line 671563
    iget-wide v0, p0, LX/46l;->a:J

    return-wide v0
.end method

.method private c()J
    .locals 4

    .prologue
    .line 671562
    sget-object v0, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    iget-wide v2, p0, LX/46l;->a:J

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMicros(J)J

    move-result-wide v0

    return-wide v0
.end method

.method private d()J
    .locals 4

    .prologue
    .line 671561
    sget-object v0, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    iget-wide v2, p0, LX/46l;->a:J

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v0

    return-wide v0
.end method

.method private e()J
    .locals 4

    .prologue
    .line 671560
    sget-object v0, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    iget-wide v2, p0, LX/46l;->a:J

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide v0

    return-wide v0
.end method

.method private f()J
    .locals 4

    .prologue
    .line 671567
    sget-object v0, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    iget-wide v2, p0, LX/46l;->a:J

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toHours(J)J

    move-result-wide v0

    return-wide v0
.end method

.method private g()J
    .locals 4

    .prologue
    .line 671559
    sget-object v0, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    iget-wide v2, p0, LX/46l;->a:J

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toDays(J)J

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method public final a()J
    .locals 4

    .prologue
    .line 671558
    sget-object v0, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    iget-wide v2, p0, LX/46l;->a:J

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 671553
    if-ne p0, p1, :cond_1

    .line 671554
    :cond_0
    :goto_0
    return v0

    .line 671555
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    .line 671556
    :cond_3
    check-cast p1, LX/46l;

    .line 671557
    iget-wide v2, p0, LX/46l;->a:J

    iget-wide v4, p1, LX/46l;->a:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 5

    .prologue
    .line 671552
    iget-wide v0, p0, LX/46l;->a:J

    iget-wide v2, p0, LX/46l;->a:J

    const/16 v4, 0x20

    ushr-long/2addr v2, v4

    xor-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 20

    .prologue
    .line 671514
    invoke-direct/range {p0 .. p0}, LX/46l;->g()J

    move-result-wide v4

    .line 671515
    invoke-direct/range {p0 .. p0}, LX/46l;->f()J

    move-result-wide v2

    const-wide/16 v6, 0x18

    rem-long v6, v2, v6

    .line 671516
    invoke-direct/range {p0 .. p0}, LX/46l;->e()J

    move-result-wide v2

    const-wide/16 v8, 0x3c

    rem-long v8, v2, v8

    .line 671517
    invoke-direct/range {p0 .. p0}, LX/46l;->d()J

    move-result-wide v2

    const-wide/16 v10, 0x3c

    rem-long v10, v2, v10

    .line 671518
    invoke-virtual/range {p0 .. p0}, LX/46l;->a()J

    move-result-wide v2

    const-wide/16 v12, 0x3e8

    rem-long v12, v2, v12

    .line 671519
    invoke-direct/range {p0 .. p0}, LX/46l;->c()J

    move-result-wide v2

    const-wide/16 v14, 0x3e8

    rem-long v14, v2, v14

    .line 671520
    invoke-direct/range {p0 .. p0}, LX/46l;->b()J

    move-result-wide v2

    const-wide/16 v16, 0x3e8

    rem-long v16, v2, v16

    .line 671521
    const-string v2, ""

    .line 671522
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v18, "TimeSpan{"

    move-object/from16 v0, v18

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 671523
    const-wide/16 v18, 0x0

    cmp-long v18, v4, v18

    if-lez v18, :cond_0

    .line 671524
    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v18, " "

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 671525
    const-string v2, "Day"

    invoke-static {v3, v2, v4, v5}, LX/46l;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    .line 671526
    const-string v2, ", "

    .line 671527
    :cond_0
    const-wide/16 v4, 0x0

    cmp-long v4, v6, v4

    if-lez v4, :cond_1

    .line 671528
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 671529
    const-string v2, "Hour"

    invoke-static {v3, v2, v6, v7}, LX/46l;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    .line 671530
    const-string v2, ", "

    .line 671531
    :cond_1
    const-wide/16 v4, 0x0

    cmp-long v4, v8, v4

    if-lez v4, :cond_2

    .line 671532
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 671533
    const-string v2, "Minute"

    invoke-static {v3, v2, v8, v9}, LX/46l;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    .line 671534
    const-string v2, ", "

    .line 671535
    :cond_2
    const-wide/16 v4, 0x0

    cmp-long v4, v10, v4

    if-lez v4, :cond_3

    .line 671536
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 671537
    const-string v2, "Second"

    invoke-static {v3, v2, v10, v11}, LX/46l;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    .line 671538
    const-string v2, ", "

    .line 671539
    :cond_3
    const-wide/16 v4, 0x0

    cmp-long v4, v12, v4

    if-lez v4, :cond_4

    .line 671540
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 671541
    const-string v2, "Milli"

    invoke-static {v3, v2, v12, v13}, LX/46l;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    .line 671542
    const-string v2, ", "

    .line 671543
    :cond_4
    const-wide/16 v4, 0x0

    cmp-long v4, v14, v4

    if-lez v4, :cond_5

    .line 671544
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 671545
    const-string v2, "Micro"

    invoke-static {v3, v2, v14, v15}, LX/46l;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    .line 671546
    const-string v2, ", "

    .line 671547
    :cond_5
    const-wide/16 v4, 0x0

    cmp-long v4, v16, v4

    if-lez v4, :cond_6

    .line 671548
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-wide/from16 v0, v16

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 671549
    const-string v2, "Nano"

    move-wide/from16 v0, v16

    invoke-static {v3, v2, v0, v1}, LX/46l;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    .line 671550
    :cond_6
    const-string v2, "}"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 671551
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method
