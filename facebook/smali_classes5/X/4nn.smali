.class public LX/4nn;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 807785
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 807786
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Landroid/widget/FrameLayout$LayoutParams;)V
    .locals 2

    .prologue
    .line 807775
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 807776
    instance-of v1, v0, LX/4nn;

    if-eqz v1, :cond_0

    .line 807777
    check-cast v0, LX/4nn;

    .line 807778
    invoke-virtual {v0, p1}, LX/4nn;->detachViewFromParent(Landroid/view/View;)V

    .line 807779
    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1, p2}, LX/4nn;->attachViewToParent(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 807780
    invoke-virtual {v0}, LX/4nn;->requestLayout()V

    .line 807781
    invoke-virtual {v0}, LX/4nn;->invalidate()V

    .line 807782
    invoke-virtual {p0}, LX/4nn;->requestLayout()V

    .line 807783
    invoke-virtual {p0}, LX/4nn;->invalidate()V

    .line 807784
    :cond_0
    return-void
.end method
