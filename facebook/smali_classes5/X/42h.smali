.class public final LX/42h;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/42g;


# instance fields
.field public final synthetic a:Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;)V
    .locals 0

    .prologue
    .line 667689
    iput-object p1, p0, LX/42h;->a:Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Class;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<+TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 667690
    iget-object v0, p0, LX/42h;->a:Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;

    invoke-static {v0, p1}, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->a(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 667691
    iget-object v0, p0, LX/42h;->a:Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;

    invoke-static {v0, p1}, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->a(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 667692
    iget-object v0, p0, LX/42h;->a:Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;

    invoke-static {v0}, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->a(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;)V

    .line 667693
    return-void
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 667694
    iget-object v0, p0, LX/42h;->a:Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;

    invoke-static {v0, p1}, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->a(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;I)V

    .line 667695
    return-void
.end method

.method public final a(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 667696
    iget-object v0, p0, LX/42h;->a:Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;

    invoke-static {v0, p1, p2, p3}, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->a(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;IILandroid/content/Intent;)V

    .line 667697
    return-void
.end method

.method public final a(ILandroid/app/Dialog;)V
    .locals 1

    .prologue
    .line 667698
    iget-object v0, p0, LX/42h;->a:Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;

    invoke-static {v0, p1, p2}, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->a(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;ILandroid/app/Dialog;)V

    .line 667699
    return-void
.end method

.method public final a(LX/0T2;)V
    .locals 1

    .prologue
    .line 667700
    iget-object v0, p0, LX/42h;->a:Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;

    invoke-static {v0, p1}, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->a(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;LX/0T2;)V

    .line 667701
    return-void
.end method

.method public final a(LX/0je;)V
    .locals 1

    .prologue
    .line 667702
    iget-object v0, p0, LX/42h;->a:Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;

    invoke-static {v0, p1}, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->a(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;LX/0je;)V

    .line 667703
    return-void
.end method

.method public final a(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 667704
    iget-object v0, p0, LX/42h;->a:Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;

    invoke-static {v0, p1}, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->a(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;Landroid/app/Activity;)V

    .line 667705
    return-void
.end method

.method public final a(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 667706
    iget-object v0, p0, LX/42h;->a:Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;

    invoke-static {v0, p1}, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->a(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;Landroid/content/Intent;)V

    .line 667707
    return-void
.end method

.method public final a(Landroid/content/Intent;I)V
    .locals 1

    .prologue
    .line 667708
    iget-object v0, p0, LX/42h;->a:Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;

    invoke-static {v0, p1, p2}, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->a(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;Landroid/content/Intent;I)V

    .line 667709
    return-void
.end method

.method public final a(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 667710
    iget-object v0, p0, LX/42h;->a:Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;

    invoke-static {v0, p1}, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->a(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;Landroid/content/res/Configuration;)V

    .line 667711
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 667712
    iget-object v0, p0, LX/42h;->a:Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;

    invoke-static {v0, p1}, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->a(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;Landroid/os/Bundle;)V

    .line 667713
    return-void
.end method

.method public final a(Landroid/support/v4/app/Fragment;)V
    .locals 1

    .prologue
    .line 667714
    iget-object v0, p0, LX/42h;->a:Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;

    invoke-static {v0, p1}, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->a(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;Landroid/support/v4/app/Fragment;)V

    .line 667715
    return-void
.end method

.method public final a(Landroid/support/v4/app/Fragment;Landroid/content/Intent;I)V
    .locals 1

    .prologue
    .line 667718
    iget-object v0, p0, LX/42h;->a:Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;

    invoke-static {v0, p1, p2, p3}, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->a(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;Landroid/support/v4/app/Fragment;Landroid/content/Intent;I)V

    .line 667719
    return-void
.end method

.method public final a(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 1

    .prologue
    .line 667716
    iget-object v0, p0, LX/42h;->a:Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;

    invoke-static {v0, p1, p2, p3}, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->a(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    .line 667717
    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 667736
    iget-object v0, p0, LX/42h;->a:Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;

    invoke-static {v0, p1, p2}, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->a(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 667737
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 667734
    iget-object v0, p0, LX/42h;->a:Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;

    invoke-static {v0, p1}, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->a(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;Z)V

    .line 667735
    return-void
.end method

.method public final a(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 667733
    iget-object v0, p0, LX/42h;->a:Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;

    invoke-static {v0, p1, p2}, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->a(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public final a(Landroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 667732
    iget-object v0, p0, LX/42h;->a:Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;

    invoke-static {v0, p1}, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->a(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;Landroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public final a(Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 667731
    iget-object v0, p0, LX/42h;->a:Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;

    invoke-static {v0, p1}, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->a(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public final a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 667729
    iget-object v0, p0, LX/42h;->a:Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;

    invoke-static {v0, p1}, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->a(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 667728
    iget-object v0, p0, LX/42h;->a:Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;

    invoke-static {v0, p1}, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->a(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/Throwable;)Z
    .locals 1

    .prologue
    .line 667727
    iget-object v0, p0, LX/42h;->a:Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;

    invoke-static {v0, p1}, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->a(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;Ljava/lang/Throwable;)Z

    move-result v0

    return v0
.end method

.method public final b(I)Landroid/app/Dialog;
    .locals 1

    .prologue
    .line 667726
    iget-object v0, p0, LX/42h;->a:Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;

    invoke-static {v0, p1}, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->b(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;I)Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 667724
    iget-object v0, p0, LX/42h;->a:Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;

    invoke-static {v0}, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->b(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;)V

    .line 667725
    return-void
.end method

.method public final b(LX/0T2;)V
    .locals 1

    .prologue
    .line 667722
    iget-object v0, p0, LX/42h;->a:Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;

    invoke-static {v0, p1}, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->b(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;LX/0T2;)V

    .line 667723
    return-void
.end method

.method public final b(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 667720
    iget-object v0, p0, LX/42h;->a:Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;

    invoke-static {v0, p1}, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->b(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;Landroid/content/Intent;)V

    .line 667721
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 667687
    iget-object v0, p0, LX/42h;->a:Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;

    invoke-static {v0, p1}, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->b(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;Landroid/os/Bundle;)V

    .line 667688
    return-void
.end method

.method public final b(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 667730
    iget-object v0, p0, LX/42h;->a:Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;

    invoke-static {v0, p1, p2}, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->b(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public final b(Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 667665
    iget-object v0, p0, LX/42h;->a:Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;

    invoke-static {v0, p1}, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->b(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public final b(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 667662
    iget-object v0, p0, LX/42h;->a:Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;

    invoke-static {v0, p1}, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->b(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public final c(I)Landroid/view/View;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(I)TT;"
        }
    .end annotation

    .prologue
    .line 667661
    iget-object v0, p0, LX/42h;->a:Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;

    invoke-static {v0, p1}, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->c(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 667659
    iget-object v0, p0, LX/42h;->a:Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;

    invoke-static {v0}, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->c(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;)V

    .line 667660
    return-void
.end method

.method public final c(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 667657
    iget-object v0, p0, LX/42h;->a:Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;

    invoke-static {v0, p1}, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->c(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;Landroid/content/Intent;)V

    .line 667658
    return-void
.end method

.method public final c(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 667655
    iget-object v0, p0, LX/42h;->a:Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;

    invoke-static {v0, p1}, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->c(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;Landroid/os/Bundle;)V

    .line 667656
    return-void
.end method

.method public final d(I)Landroid/view/View;
    .locals 1

    .prologue
    .line 667654
    iget-object v0, p0, LX/42h;->a:Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;

    invoke-static {v0, p1}, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->d(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 667652
    iget-object v0, p0, LX/42h;->a:Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;

    invoke-static {v0}, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->d(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;)V

    .line 667653
    return-void
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 667663
    iget-object v0, p0, LX/42h;->a:Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;

    invoke-static {v0, p1}, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->d(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;Landroid/os/Bundle;)V

    .line 667664
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 667650
    iget-object v0, p0, LX/42h;->a:Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;

    invoke-static {v0}, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->e(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;)V

    .line 667651
    return-void
.end method

.method public final e(I)V
    .locals 1

    .prologue
    .line 667648
    iget-object v0, p0, LX/42h;->a:Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;

    invoke-static {v0, p1}, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->e(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;I)V

    .line 667649
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 667646
    iget-object v0, p0, LX/42h;->a:Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;

    invoke-static {v0}, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->f(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;)V

    .line 667647
    return-void
.end method

.method public final f(I)V
    .locals 1

    .prologue
    .line 667644
    iget-object v0, p0, LX/42h;->a:Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;

    invoke-static {v0, p1}, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->f(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;I)V

    .line 667645
    return-void
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 667642
    iget-object v0, p0, LX/42h;->a:Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;

    invoke-static {v0}, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->g(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;)V

    .line 667643
    return-void
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 667640
    iget-object v0, p0, LX/42h;->a:Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;

    invoke-static {v0}, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->h(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;)V

    .line 667641
    return-void
.end method

.method public final i()V
    .locals 1

    .prologue
    .line 667666
    iget-object v0, p0, LX/42h;->a:Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;

    invoke-static {v0}, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->i(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;)V

    .line 667667
    return-void
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 667668
    iget-object v0, p0, LX/42h;->a:Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;

    invoke-static {v0}, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->j(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;)Z

    move-result v0

    return v0
.end method

.method public final k()V
    .locals 1

    .prologue
    .line 667669
    iget-object v0, p0, LX/42h;->a:Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;

    invoke-static {v0}, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->k(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;)V

    .line 667670
    return-void
.end method

.method public final l()V
    .locals 1

    .prologue
    .line 667671
    iget-object v0, p0, LX/42h;->a:Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;

    invoke-static {v0}, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->l(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;)V

    .line 667672
    return-void
.end method

.method public final m()V
    .locals 1

    .prologue
    .line 667673
    iget-object v0, p0, LX/42h;->a:Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;

    invoke-static {v0}, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->m(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;)V

    .line 667674
    return-void
.end method

.method public final n()LX/0QA;
    .locals 1

    .prologue
    .line 667675
    iget-object v0, p0, LX/42h;->a:Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;

    invoke-static {v0}, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->n(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;)LX/0QA;

    move-result-object v0

    return-object v0
.end method

.method public final o()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "LX/0T2;",
            ">;"
        }
    .end annotation

    .prologue
    .line 667676
    iget-object v0, p0, LX/42h;->a:Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;

    invoke-static {v0}, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->o(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final p()V
    .locals 1

    .prologue
    .line 667677
    iget-object v0, p0, LX/42h;->a:Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;

    invoke-static {v0}, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->p(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;)V

    .line 667678
    return-void
.end method

.method public final q()V
    .locals 1

    .prologue
    .line 667679
    iget-object v0, p0, LX/42h;->a:Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;

    invoke-static {v0}, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->q(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;)V

    .line 667680
    return-void
.end method

.method public final r()LX/0gc;
    .locals 1

    .prologue
    .line 667681
    iget-object v0, p0, LX/42h;->a:Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;

    invoke-static {v0}, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->r(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;)LX/0gc;

    move-result-object v0

    return-object v0
.end method

.method public final s()Landroid/view/Window;
    .locals 1

    .prologue
    .line 667682
    iget-object v0, p0, LX/42h;->a:Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;

    invoke-static {v0}, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->s(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;)Landroid/view/Window;

    move-result-object v0

    return-object v0
.end method

.method public final t()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 667683
    iget-object v0, p0, LX/42h;->a:Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;

    invoke-static {v0}, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->t(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final u()Landroid/content/res/Resources;
    .locals 1

    .prologue
    .line 667684
    iget-object v0, p0, LX/42h;->a:Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;

    invoke-static {v0}, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->u(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;)Landroid/content/res/Resources;

    move-result-object v0

    return-object v0
.end method

.method public final v()Landroid/view/MenuInflater;
    .locals 1

    .prologue
    .line 667685
    iget-object v0, p0, LX/42h;->a:Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;

    invoke-static {v0}, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->v(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;)Landroid/view/MenuInflater;

    move-result-object v0

    return-object v0
.end method

.method public final w()Z
    .locals 1

    .prologue
    .line 667686
    iget-object v0, p0, LX/42h;->a:Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;

    invoke-static {v0}, Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;->w(Lcom/facebook/base/activity/DelegatingFbFragmentFrameworkActivity;)Z

    move-result v0

    return v0
.end method
