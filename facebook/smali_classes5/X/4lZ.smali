.class public final LX/4lZ;
.super LX/0wa;
.source ""


# instance fields
.field public final synthetic a:LX/4la;


# direct methods
.method public constructor <init>(LX/4la;)V
    .locals 0

    .prologue
    .line 804551
    iput-object p1, p0, LX/4lZ;->a:LX/4la;

    invoke-direct {p0}, LX/0wa;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(J)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 804552
    iget-object v0, p0, LX/4lZ;->a:LX/4la;

    iget v0, v0, LX/4la;->f:I

    iget-object v1, p0, LX/4lZ;->a:LX/4la;

    iget v1, v1, LX/4la;->g:I

    if-ge v0, v1, :cond_0

    .line 804553
    iget-object v0, p0, LX/4lZ;->a:LX/4la;

    .line 804554
    iget v1, v0, LX/4la;->f:I

    add-int/lit8 v2, v1, 0x1

    iput v2, v0, LX/4la;->f:I

    .line 804555
    :cond_0
    iget-object v0, p0, LX/4lZ;->a:LX/4la;

    iget v0, v0, LX/4la;->h:I

    iget-object v1, p0, LX/4lZ;->a:LX/4la;

    iget-object v1, v1, LX/4la;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    sub-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    move v1, v0

    .line 804556
    :goto_0
    iget-object v0, p0, LX/4lZ;->a:LX/4la;

    iget v0, v0, LX/4la;->f:I

    if-ge v1, v0, :cond_2

    .line 804557
    iget-object v0, p0, LX/4lZ;->a:LX/4la;

    iget-object v0, v0, LX/4la;->c:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 804558
    if-eqz v0, :cond_1

    move v2, v3

    .line 804559
    :goto_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    if-ge v2, v4, :cond_1

    .line 804560
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    iget-object v4, p0, LX/4lZ;->a:LX/4la;

    iget-object v4, v4, LX/4la;->b:Ljava/util/List;

    iget-object v5, p0, LX/4lZ;->a:LX/4la;

    iget v5, v5, LX/4la;->h:I

    sub-int/2addr v5, v1

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 804561
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 804562
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 804563
    :cond_2
    iget-object v0, p0, LX/4lZ;->a:LX/4la;

    iget v0, v0, LX/4la;->h:I

    iget-object v1, p0, LX/4lZ;->a:LX/4la;

    iget v1, v1, LX/4la;->g:I

    iget-object v2, p0, LX/4lZ;->a:LX/4la;

    iget-object v2, v2, LX/4la;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_4

    .line 804564
    iget-object v0, p0, LX/4lZ;->a:LX/4la;

    .line 804565
    iget v1, v0, LX/4la;->h:I

    add-int/lit8 v2, v1, 0x1

    iput v2, v0, LX/4la;->h:I

    .line 804566
    iget-object v0, p0, LX/4lZ;->a:LX/4la;

    iget-object v0, v0, LX/4la;->a:LX/0wY;

    iget-object v1, p0, LX/4lZ;->a:LX/4la;

    iget-object v1, v1, LX/4la;->i:LX/0wa;

    invoke-interface {v0, v1}, LX/0wY;->a(LX/0wa;)V

    .line 804567
    :cond_3
    :goto_2
    return-void

    .line 804568
    :cond_4
    iget-object v0, p0, LX/4lZ;->a:LX/4la;

    const/4 v1, 0x1

    .line 804569
    iput-boolean v1, v0, LX/4la;->e:Z

    .line 804570
    iget-object v0, p0, LX/4lZ;->a:LX/4la;

    iget-object v0, v0, LX/4la;->d:LX/4mL;

    if-eqz v0, :cond_3

    .line 804571
    iget-object v0, p0, LX/4lZ;->a:LX/4la;

    iget-object v0, v0, LX/4la;->d:LX/4mL;

    .line 804572
    iget-object v1, v0, LX/4mL;->e:LX/4mK;

    sget-object v2, LX/4mK;->COMPLETE:LX/4mK;

    if-eq v1, v2, :cond_5

    const/4 v1, 0x1

    :goto_3
    const-string v2, "Entry animation cannot complete twice."

    invoke-static {v1, v2}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 804573
    sget-object v1, LX/4mK;->COMPLETE:LX/4mK;

    iput-object v1, v0, LX/4mL;->e:LX/4mK;

    .line 804574
    goto :goto_2

    .line 804575
    :cond_5
    const/4 v1, 0x0

    goto :goto_3
.end method
