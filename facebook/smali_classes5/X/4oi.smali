.class public LX/4oi;
.super Landroid/preference/CheckBoxPreference;
.source ""


# instance fields
.field private final a:LX/2qy;

.field public b:LX/2qx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 810310
    invoke-direct {p0, p1}, Landroid/preference/CheckBoxPreference;-><init>(Landroid/content/Context;)V

    .line 810311
    const-class v0, LX/4oi;

    invoke-static {v0, p0}, LX/4oi;->a(Ljava/lang/Class;Landroid/preference/Preference;)V

    .line 810312
    iget-object v0, p0, LX/4oi;->b:LX/2qx;

    invoke-virtual {v0, p0}, LX/2qx;->a(Landroid/preference/Preference;)LX/2qy;

    move-result-object v0

    iput-object v0, p0, LX/4oi;->a:LX/2qy;

    .line 810313
    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 810300
    instance-of v1, p1, Landroid/view/ViewGroup;

    if-eqz v1, :cond_0

    .line 810301
    check-cast p1, Landroid/view/ViewGroup;

    .line 810302
    :goto_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 810303
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-direct {p0, v1}, LX/4oi;->a(Landroid/view/View;)V

    .line 810304
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 810305
    :cond_0
    instance-of v1, p1, Landroid/widget/TextView;

    if-eqz v1, :cond_1

    .line 810306
    check-cast p1, Landroid/widget/TextView;

    .line 810307
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 810308
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 810309
    :cond_1
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/preference/Preference;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/preference/Preference;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, LX/4oi;

    const-class p0, LX/2qx;

    invoke-interface {v1, p0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/2qx;

    iput-object v1, p1, LX/4oi;->b:LX/2qx;

    return-void
.end method


# virtual methods
.method public final a(LX/0Tn;)V
    .locals 1

    .prologue
    .line 810298
    iget-object v0, p0, LX/4oi;->a:LX/2qy;

    invoke-virtual {v0, p1}, LX/2qy;->a(LX/0Tn;)V

    .line 810299
    return-void
.end method

.method public final getPersistedBoolean(Z)Z
    .locals 1

    .prologue
    .line 810314
    iget-object v0, p0, LX/4oi;->a:LX/2qy;

    invoke-virtual {v0, p1}, LX/2qy;->a(Z)Z

    move-result v0

    return v0
.end method

.method public final getSharedPreferences()Landroid/content/SharedPreferences;
    .locals 1

    .prologue
    .line 810295
    iget-object v0, p0, LX/4oi;->a:LX/2qy;

    .line 810296
    iget-object p0, v0, LX/2qy;->b:Landroid/content/SharedPreferences;

    move-object v0, p0

    .line 810297
    return-object v0
.end method

.method public onBindView(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 810292
    invoke-super {p0, p1}, Landroid/preference/CheckBoxPreference;->onBindView(Landroid/view/View;)V

    .line 810293
    invoke-direct {p0, p1}, LX/4oi;->a(Landroid/view/View;)V

    .line 810294
    return-void
.end method

.method public final persistBoolean(Z)Z
    .locals 4

    .prologue
    .line 810283
    invoke-virtual {p0}, LX/4oi;->shouldPersist()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 810284
    iget-object v0, p0, LX/4oi;->a:LX/2qy;

    const/4 v2, 0x1

    .line 810285
    if-nez p1, :cond_2

    move v1, v2

    :goto_0
    invoke-virtual {v0, v1}, LX/2qy;->a(Z)Z

    move-result v1

    if-eq p1, v1, :cond_0

    .line 810286
    iget-object v1, v0, LX/2qy;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    .line 810287
    new-instance v3, LX/0Tn;

    iget-object p0, v0, LX/2qy;->a:Landroid/preference/Preference;

    invoke-virtual {p0}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v3, p0}, LX/0Tn;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v3, p1}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    .line 810288
    invoke-interface {v1}, LX/0hN;->commit()V

    .line 810289
    :cond_0
    move v0, v2

    .line 810290
    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 810291
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method
