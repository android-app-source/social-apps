.class public LX/4r5;
.super LX/4r2;
.source ""


# instance fields
.field public final a:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/4qx;LX/2Ay;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 815060
    invoke-direct {p0, p1, p2}, LX/4r2;-><init>(LX/4qx;LX/2Ay;)V

    .line 815061
    iput-object p3, p0, LX/4r5;->a:Ljava/lang/String;

    .line 815062
    return-void
.end method

.method private a(LX/0nX;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 815058
    iget-object v0, p0, LX/4r5;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, p2}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 815059
    return-void
.end method

.method private b(LX/2Ay;)LX/4r5;
    .locals 3

    .prologue
    .line 815038
    iget-object v0, p0, LX/4r2;->c:LX/2Ay;

    if-ne v0, p1, :cond_0

    .line 815039
    :goto_0
    return-object p0

    :cond_0
    new-instance v0, LX/4r5;

    iget-object v1, p0, LX/4r2;->b:LX/4qx;

    iget-object v2, p0, LX/4r5;->a:Ljava/lang/String;

    invoke-direct {v0, v1, p1, v2}, LX/4r5;-><init>(LX/4qx;LX/2Ay;Ljava/lang/String;)V

    move-object p0, v0

    goto :goto_0
.end method

.method private b(LX/0nX;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 815055
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 815056
    iget-object v0, p0, LX/4r5;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, p2}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 815057
    return-void
.end method

.method private c(LX/0nX;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 815052
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 815053
    iget-object v0, p0, LX/4r5;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, p2}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 815054
    return-void
.end method


# virtual methods
.method public final synthetic a(LX/2Ay;)LX/4qz;
    .locals 1

    .prologue
    .line 815051
    invoke-direct {p0, p1}, LX/4r5;->b(LX/2Ay;)LX/4r5;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/0nX;)V
    .locals 0

    .prologue
    .line 815050
    return-void
.end method

.method public final a(Ljava/lang/Object;LX/0nX;Ljava/lang/Class;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "LX/0nX;",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 815063
    return-void
.end method

.method public final a(Ljava/lang/Object;LX/0nX;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 815048
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 815049
    return-void
.end method

.method public final b(Ljava/lang/Object;LX/0nX;)V
    .locals 0

    .prologue
    .line 815046
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 815047
    return-void
.end method

.method public final b(Ljava/lang/Object;LX/0nX;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 815044
    invoke-direct {p0, p2, p3}, LX/4r5;->b(LX/0nX;Ljava/lang/String;)V

    .line 815045
    return-void
.end method

.method public final c(Ljava/lang/Object;LX/0nX;)V
    .locals 0

    .prologue
    .line 815042
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 815043
    return-void
.end method

.method public final d(Ljava/lang/Object;LX/0nX;)V
    .locals 1

    .prologue
    .line 815040
    invoke-virtual {p0, p1}, LX/4r2;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p2, v0}, LX/4r5;->a(LX/0nX;Ljava/lang/String;)V

    .line 815041
    return-void
.end method

.method public final e(Ljava/lang/Object;LX/0nX;)V
    .locals 1

    .prologue
    .line 815036
    invoke-virtual {p0, p1}, LX/4r2;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p2, v0}, LX/4r5;->b(LX/0nX;Ljava/lang/String;)V

    .line 815037
    return-void
.end method

.method public final f(Ljava/lang/Object;LX/0nX;)V
    .locals 1

    .prologue
    .line 815034
    invoke-virtual {p0, p1}, LX/4r2;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p2, v0}, LX/4r5;->c(LX/0nX;Ljava/lang/String;)V

    .line 815035
    return-void
.end method
