.class public LX/4QF;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 703806
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 39

    .prologue
    .line 703807
    const/16 v33, 0x0

    .line 703808
    const/16 v32, 0x0

    .line 703809
    const/16 v31, 0x0

    .line 703810
    const/16 v30, 0x0

    .line 703811
    const/16 v29, 0x0

    .line 703812
    const/16 v28, 0x0

    .line 703813
    const/16 v27, 0x0

    .line 703814
    const/16 v26, 0x0

    .line 703815
    const/16 v25, 0x0

    .line 703816
    const/16 v24, 0x0

    .line 703817
    const/16 v23, 0x0

    .line 703818
    const/16 v22, 0x0

    .line 703819
    const/16 v21, 0x0

    .line 703820
    const/16 v20, 0x0

    .line 703821
    const-wide/16 v18, 0x0

    .line 703822
    const/16 v17, 0x0

    .line 703823
    const/16 v16, 0x0

    .line 703824
    const/4 v15, 0x0

    .line 703825
    const/4 v14, 0x0

    .line 703826
    const/4 v13, 0x0

    .line 703827
    const/4 v12, 0x0

    .line 703828
    const/4 v11, 0x0

    .line 703829
    const/4 v10, 0x0

    .line 703830
    const/4 v9, 0x0

    .line 703831
    const/4 v8, 0x0

    .line 703832
    const/4 v7, 0x0

    .line 703833
    const/4 v6, 0x0

    .line 703834
    const/4 v5, 0x0

    .line 703835
    const/4 v4, 0x0

    .line 703836
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v34

    sget-object v35, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v34

    move-object/from16 v1, v35

    if-eq v0, v1, :cond_1f

    .line 703837
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 703838
    const/4 v4, 0x0

    .line 703839
    :goto_0
    return v4

    .line 703840
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 703841
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v34

    sget-object v35, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v34

    move-object/from16 v1, v35

    if-eq v0, v1, :cond_11

    .line 703842
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v34

    .line 703843
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 703844
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v35

    sget-object v36, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    if-eq v0, v1, :cond_1

    if-eqz v34, :cond_1

    .line 703845
    const-string v35, "boosted_local_awareness_promotion_status_description"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_2

    .line 703846
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, p1

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v33

    goto :goto_1

    .line 703847
    :cond_2
    const-string v35, "boosted_page_like_promotion_status_description"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_3

    .line 703848
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, p1

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v32

    goto :goto_1

    .line 703849
    :cond_3
    const-string v35, "boosted_post_default_audience"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_4

    .line 703850
    const/16 v18, 0x1

    .line 703851
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v31 .. v31}, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-result-object v31

    goto :goto_1

    .line 703852
    :cond_4
    const-string v35, "can_viewer_promote"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_5

    .line 703853
    const/16 v17, 0x1

    .line 703854
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v30

    goto :goto_1

    .line 703855
    :cond_5
    const-string v35, "can_viewer_promote_cta"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_6

    .line 703856
    const/16 v16, 0x1

    .line 703857
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v29

    goto :goto_1

    .line 703858
    :cond_6
    const-string v35, "can_viewer_promote_for_page_likes"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_7

    .line 703859
    const/4 v15, 0x1

    .line 703860
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v28

    goto/16 :goto_1

    .line 703861
    :cond_7
    const-string v35, "can_viewer_promote_local_awareness"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_8

    .line 703862
    const/4 v14, 0x1

    .line 703863
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v27

    goto/16 :goto_1

    .line 703864
    :cond_8
    const-string v35, "can_viewer_promote_website"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_9

    .line 703865
    const/4 v13, 0x1

    .line 703866
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v26

    goto/16 :goto_1

    .line 703867
    :cond_9
    const-string v35, "default_duration_for_boosted_post"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_a

    .line 703868
    const/4 v12, 0x1

    .line 703869
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v25

    goto/16 :goto_1

    .line 703870
    :cond_a
    const-string v35, "does_viewer_pin"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_b

    .line 703871
    const/4 v11, 0x1

    .line 703872
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v24

    goto/16 :goto_1

    .line 703873
    :cond_b
    const-string v35, "has_boosted_posts"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_c

    .line 703874
    const/4 v10, 0x1

    .line 703875
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v23

    goto/16 :goto_1

    .line 703876
    :cond_c
    const-string v35, "is_likely_to_advertise"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_d

    .line 703877
    const/4 v9, 0x1

    .line 703878
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v22

    goto/16 :goto_1

    .line 703879
    :cond_d
    const-string v35, "is_viewer_business_manager_admin"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_e

    .line 703880
    const/4 v8, 0x1

    .line 703881
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v21

    goto/16 :goto_1

    .line 703882
    :cond_e
    const-string v35, "messaging_enabled"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_f

    .line 703883
    const/4 v5, 0x1

    .line 703884
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v20

    goto/16 :goto_1

    .line 703885
    :cond_f
    const-string v35, "page_scheduled_deletion_time"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_10

    .line 703886
    const/4 v4, 0x1

    .line 703887
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v6

    goto/16 :goto_1

    .line 703888
    :cond_10
    const-string v35, "viewer"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_0

    .line 703889
    invoke-static/range {p0 .. p1}, LX/262;->a(LX/15w;LX/186;)I

    move-result v19

    goto/16 :goto_1

    .line 703890
    :cond_11
    const/16 v34, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 703891
    const/16 v34, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v34

    move/from16 v2, v33

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 703892
    const/16 v33, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v33

    move/from16 v2, v32

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 703893
    if-eqz v18, :cond_12

    .line 703894
    const/16 v18, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v18

    move-object/from16 v2, v31

    invoke-virtual {v0, v1, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 703895
    :cond_12
    if-eqz v17, :cond_13

    .line 703896
    const/16 v17, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v30

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 703897
    :cond_13
    if-eqz v16, :cond_14

    .line 703898
    const/16 v16, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v16

    move/from16 v2, v29

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 703899
    :cond_14
    if-eqz v15, :cond_15

    .line 703900
    const/4 v15, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v15, v1}, LX/186;->a(IZ)V

    .line 703901
    :cond_15
    if-eqz v14, :cond_16

    .line 703902
    const/4 v14, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v14, v1}, LX/186;->a(IZ)V

    .line 703903
    :cond_16
    if-eqz v13, :cond_17

    .line 703904
    const/4 v13, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v13, v1}, LX/186;->a(IZ)V

    .line 703905
    :cond_17
    if-eqz v12, :cond_18

    .line 703906
    const/16 v12, 0x8

    const/4 v13, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v12, v1, v13}, LX/186;->a(III)V

    .line 703907
    :cond_18
    if-eqz v11, :cond_19

    .line 703908
    const/16 v11, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v11, v1}, LX/186;->a(IZ)V

    .line 703909
    :cond_19
    if-eqz v10, :cond_1a

    .line 703910
    const/16 v10, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v10, v1}, LX/186;->a(IZ)V

    .line 703911
    :cond_1a
    if-eqz v9, :cond_1b

    .line 703912
    const/16 v9, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v9, v1}, LX/186;->a(IZ)V

    .line 703913
    :cond_1b
    if-eqz v8, :cond_1c

    .line 703914
    const/16 v8, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v8, v1}, LX/186;->a(IZ)V

    .line 703915
    :cond_1c
    if-eqz v5, :cond_1d

    .line 703916
    const/16 v5, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v5, v1}, LX/186;->a(IZ)V

    .line 703917
    :cond_1d
    if-eqz v4, :cond_1e

    .line 703918
    const/16 v5, 0xe

    const-wide/16 v8, 0x0

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, LX/186;->a(IJJ)V

    .line 703919
    :cond_1e
    const/16 v4, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 703920
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v4

    goto/16 :goto_0

    :cond_1f
    move/from16 v37, v6

    move/from16 v38, v7

    move-wide/from16 v6, v18

    move/from16 v18, v16

    move/from16 v19, v17

    move/from16 v16, v14

    move/from16 v17, v15

    move v14, v12

    move v15, v13

    move v13, v11

    move v12, v10

    move v11, v9

    move v10, v8

    move/from16 v8, v37

    move/from16 v9, v38

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 703921
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 703922
    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 703923
    if-eqz v0, :cond_0

    .line 703924
    const-string v1, "boosted_local_awareness_promotion_status_description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 703925
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 703926
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 703927
    if-eqz v0, :cond_1

    .line 703928
    const-string v1, "boosted_page_like_promotion_status_description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 703929
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 703930
    :cond_1
    invoke-virtual {p0, p1, v3, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 703931
    if-eqz v0, :cond_2

    .line 703932
    const-string v0, "boosted_post_default_audience"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 703933
    const-class v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    invoke-virtual {p0, p1, v3, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 703934
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 703935
    if-eqz v0, :cond_3

    .line 703936
    const-string v1, "can_viewer_promote"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 703937
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 703938
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 703939
    if-eqz v0, :cond_4

    .line 703940
    const-string v1, "can_viewer_promote_cta"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 703941
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 703942
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 703943
    if-eqz v0, :cond_5

    .line 703944
    const-string v1, "can_viewer_promote_for_page_likes"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 703945
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 703946
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 703947
    if-eqz v0, :cond_6

    .line 703948
    const-string v1, "can_viewer_promote_local_awareness"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 703949
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 703950
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 703951
    if-eqz v0, :cond_7

    .line 703952
    const-string v1, "can_viewer_promote_website"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 703953
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 703954
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 703955
    if-eqz v0, :cond_8

    .line 703956
    const-string v1, "default_duration_for_boosted_post"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 703957
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 703958
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 703959
    if-eqz v0, :cond_9

    .line 703960
    const-string v1, "does_viewer_pin"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 703961
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 703962
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 703963
    if-eqz v0, :cond_a

    .line 703964
    const-string v1, "has_boosted_posts"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 703965
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 703966
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 703967
    if-eqz v0, :cond_b

    .line 703968
    const-string v1, "is_likely_to_advertise"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 703969
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 703970
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 703971
    if-eqz v0, :cond_c

    .line 703972
    const-string v1, "is_viewer_business_manager_admin"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 703973
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 703974
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 703975
    if-eqz v0, :cond_d

    .line 703976
    const-string v1, "messaging_enabled"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 703977
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 703978
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 703979
    cmp-long v2, v0, v4

    if-eqz v2, :cond_e

    .line 703980
    const-string v2, "page_scheduled_deletion_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 703981
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 703982
    :cond_e
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 703983
    if-eqz v0, :cond_f

    .line 703984
    const-string v1, "viewer"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 703985
    invoke-static {p0, v0, p2, p3}, LX/262;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 703986
    :cond_f
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 703987
    return-void
.end method
