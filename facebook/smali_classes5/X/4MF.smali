.class public LX/4MF;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 687236
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const/4 v0, 0x0

    .line 687237
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v2, :cond_d

    .line 687238
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 687239
    :goto_0
    return v0

    .line 687240
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 687241
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_c

    .line 687242
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 687243
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 687244
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_1

    if-eqz v11, :cond_1

    .line 687245
    const-string v12, "coverPhotoImage"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 687246
    invoke-static {p0, p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v10

    goto :goto_1

    .line 687247
    :cond_2
    const-string v12, "hi_res_theme_image"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 687248
    invoke-static {p0, p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 687249
    :cond_3
    const-string v12, "id"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 687250
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 687251
    :cond_4
    const-string v12, "image"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 687252
    invoke-static {p0, p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 687253
    :cond_5
    const-string v12, "low_res_theme_image"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 687254
    invoke-static {p0, p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 687255
    :cond_6
    const-string v12, "med_res_theme_image"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 687256
    invoke-static {p0, p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 687257
    :cond_7
    const-string v12, "profileImageLarge"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_8

    .line 687258
    invoke-static {p0, p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 687259
    :cond_8
    const-string v12, "profileImageSmall"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_9

    .line 687260
    invoke-static {p0, p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v3

    goto/16 :goto_1

    .line 687261
    :cond_9
    const-string v12, "themeListImage"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_a

    .line 687262
    invoke-static {p0, p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v2

    goto/16 :goto_1

    .line 687263
    :cond_a
    const-string v12, "theme_image"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_b

    .line 687264
    invoke-static {p0, p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v1

    goto/16 :goto_1

    .line 687265
    :cond_b
    const-string v12, "url"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 687266
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_1

    .line 687267
    :cond_c
    const/16 v11, 0xc

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 687268
    const/4 v11, 0x1

    invoke-virtual {p1, v11, v10}, LX/186;->b(II)V

    .line 687269
    const/4 v10, 0x2

    invoke-virtual {p1, v10, v9}, LX/186;->b(II)V

    .line 687270
    const/4 v9, 0x3

    invoke-virtual {p1, v9, v8}, LX/186;->b(II)V

    .line 687271
    const/4 v8, 0x4

    invoke-virtual {p1, v8, v7}, LX/186;->b(II)V

    .line 687272
    const/4 v7, 0x5

    invoke-virtual {p1, v7, v6}, LX/186;->b(II)V

    .line 687273
    const/4 v6, 0x6

    invoke-virtual {p1, v6, v5}, LX/186;->b(II)V

    .line 687274
    const/4 v5, 0x7

    invoke-virtual {p1, v5, v4}, LX/186;->b(II)V

    .line 687275
    const/16 v4, 0x8

    invoke-virtual {p1, v4, v3}, LX/186;->b(II)V

    .line 687276
    const/16 v3, 0x9

    invoke-virtual {p1, v3, v2}, LX/186;->b(II)V

    .line 687277
    const/16 v2, 0xa

    invoke-virtual {p1, v2, v1}, LX/186;->b(II)V

    .line 687278
    const/16 v1, 0xb

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 687279
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :cond_d
    move v1, v0

    move v2, v0

    move v3, v0

    move v4, v0

    move v5, v0

    move v6, v0

    move v7, v0

    move v8, v0

    move v9, v0

    move v10, v0

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 687280
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 687281
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 687282
    if-eqz v0, :cond_0

    .line 687283
    const-string v1, "coverPhotoImage"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 687284
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 687285
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 687286
    if-eqz v0, :cond_1

    .line 687287
    const-string v1, "hi_res_theme_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 687288
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 687289
    :cond_1
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 687290
    if-eqz v0, :cond_2

    .line 687291
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 687292
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 687293
    :cond_2
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 687294
    if-eqz v0, :cond_3

    .line 687295
    const-string v1, "image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 687296
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 687297
    :cond_3
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 687298
    if-eqz v0, :cond_4

    .line 687299
    const-string v1, "low_res_theme_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 687300
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 687301
    :cond_4
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 687302
    if-eqz v0, :cond_5

    .line 687303
    const-string v1, "med_res_theme_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 687304
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 687305
    :cond_5
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 687306
    if-eqz v0, :cond_6

    .line 687307
    const-string v1, "profileImageLarge"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 687308
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 687309
    :cond_6
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 687310
    if-eqz v0, :cond_7

    .line 687311
    const-string v1, "profileImageSmall"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 687312
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 687313
    :cond_7
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 687314
    if-eqz v0, :cond_8

    .line 687315
    const-string v1, "themeListImage"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 687316
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 687317
    :cond_8
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 687318
    if-eqz v0, :cond_9

    .line 687319
    const-string v1, "theme_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 687320
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 687321
    :cond_9
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 687322
    if-eqz v0, :cond_a

    .line 687323
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 687324
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 687325
    :cond_a
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 687326
    return-void
.end method
