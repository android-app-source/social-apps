.class public LX/440;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field public final a:LX/18b;

.field public b:Z


# direct methods
.method public constructor <init>(LX/18b;)V
    .locals 1

    .prologue
    .line 669362
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 669363
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/440;->b:Z

    .line 669364
    iput-object p1, p0, LX/440;->a:LX/18b;

    .line 669365
    iget-object v0, p0, LX/440;->a:LX/18b;

    invoke-virtual {v0, p0}, LX/18b;->a(LX/440;)V

    .line 669366
    return-void
.end method


# virtual methods
.method public final a(I)Landroid/util/Pair;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 669354
    iget-boolean v0, p0, LX/440;->b:Z

    if-nez v0, :cond_0

    .line 669355
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "called getLastObservedDbm after close"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 669356
    :cond_0
    iget-object v0, p0, LX/440;->a:LX/18b;

    invoke-virtual {v0, p1}, LX/18b;->a(I)Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/telephony/SignalStrength;)V
    .locals 0

    .prologue
    .line 669367
    return-void
.end method

.method public final declared-synchronized close()V
    .locals 1

    .prologue
    .line 669357
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/440;->b:Z

    if-eqz v0, :cond_0

    .line 669358
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/440;->b:Z

    .line 669359
    iget-object v0, p0, LX/440;->a:LX/18b;

    invoke-virtual {v0, p0}, LX/18b;->b(LX/440;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 669360
    :cond_0
    monitor-exit p0

    return-void

    .line 669361
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final finalize()V
    .locals 2

    .prologue
    .line 669349
    iget-boolean v0, p0, LX/440;->b:Z

    if-eqz v0, :cond_0

    .line 669350
    invoke-virtual {p0}, LX/440;->close()V

    .line 669351
    const-string v0, "CarrierMonitor"

    const-string v1, "finalized without close"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 669352
    :cond_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 669353
    return-void
.end method
