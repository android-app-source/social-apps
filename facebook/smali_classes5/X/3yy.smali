.class public final enum LX/3yy;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/3yy;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/3yy;

.field public static final enum EXPERIMENTS_IM_IN:LX/3yy;

.field public static final enum EXPERIMENTS_I_OVERRODE:LX/3yy;

.field public static final enum SHOW_ALL_EXPERIMENTS:LX/3yy;


# instance fields
.field private final key:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 661801
    new-instance v0, LX/3yy;

    const-string v1, "SHOW_ALL_EXPERIMENTS"

    const-string v2, "Show all experiments"

    invoke-direct {v0, v1, v3, v2}, LX/3yy;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3yy;->SHOW_ALL_EXPERIMENTS:LX/3yy;

    .line 661802
    new-instance v0, LX/3yy;

    const-string v1, "EXPERIMENTS_IM_IN"

    const-string v2, "Show experiments I\'m in"

    invoke-direct {v0, v1, v4, v2}, LX/3yy;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3yy;->EXPERIMENTS_IM_IN:LX/3yy;

    .line 661803
    new-instance v0, LX/3yy;

    const-string v1, "EXPERIMENTS_I_OVERRODE"

    const-string v2, "Show experiments I overrode"

    invoke-direct {v0, v1, v5, v2}, LX/3yy;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3yy;->EXPERIMENTS_I_OVERRODE:LX/3yy;

    .line 661804
    const/4 v0, 0x3

    new-array v0, v0, [LX/3yy;

    sget-object v1, LX/3yy;->SHOW_ALL_EXPERIMENTS:LX/3yy;

    aput-object v1, v0, v3

    sget-object v1, LX/3yy;->EXPERIMENTS_IM_IN:LX/3yy;

    aput-object v1, v0, v4

    sget-object v1, LX/3yy;->EXPERIMENTS_I_OVERRODE:LX/3yy;

    aput-object v1, v0, v5

    sput-object v0, LX/3yy;->$VALUES:[LX/3yy;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 661798
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 661799
    iput-object p3, p0, LX/3yy;->key:Ljava/lang/String;

    .line 661800
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/3yy;
    .locals 1

    .prologue
    .line 661805
    const-class v0, LX/3yy;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/3yy;

    return-object v0
.end method

.method public static values()[LX/3yy;
    .locals 1

    .prologue
    .line 661797
    sget-object v0, LX/3yy;->$VALUES:[LX/3yy;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/3yy;

    return-object v0
.end method


# virtual methods
.method public final getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 661796
    iget-object v0, p0, LX/3yy;->key:Ljava/lang/String;

    return-object v0
.end method

.method public final getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 661795
    invoke-virtual {p0}, LX/3yy;->name()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
