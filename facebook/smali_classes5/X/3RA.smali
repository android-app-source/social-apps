.class public LX/3RA;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile h:LX/3RA;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Sg;

.field public final e:LX/01T;

.field private final f:Ljava/lang/Class;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 579195
    sget-object v0, LX/3RB;->d:Ljava/lang/String;

    sget-object v1, LX/3RB;->b:Ljava/lang/String;

    invoke-static {v0, v1}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/3RA;->a:LX/0Rf;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0Or;LX/0Sg;LX/01T;Ljava/lang/Class;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .param p5    # Ljava/lang/Class;
        .annotation runtime Lcom/facebook/messaging/chatheads/annotations/ForChatHeads;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/common/appchoreographer/AppChoreographer;",
            "LX/01T;",
            "Ljava/lang/Class;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 579188
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 579189
    iput-object p1, p0, LX/3RA;->b:Landroid/content/Context;

    .line 579190
    iput-object p2, p0, LX/3RA;->c:LX/0Or;

    .line 579191
    iput-object p3, p0, LX/3RA;->d:LX/0Sg;

    .line 579192
    iput-object p4, p0, LX/3RA;->e:LX/01T;

    .line 579193
    iput-object p5, p0, LX/3RA;->f:Ljava/lang/Class;

    .line 579194
    return-void
.end method

.method public static a(LX/0QB;)LX/3RA;
    .locals 9

    .prologue
    .line 579172
    sget-object v0, LX/3RA;->h:LX/3RA;

    if-nez v0, :cond_1

    .line 579173
    const-class v1, LX/3RA;

    monitor-enter v1

    .line 579174
    :try_start_0
    sget-object v0, LX/3RA;->h:LX/3RA;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 579175
    if-eqz v2, :cond_0

    .line 579176
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 579177
    new-instance v3, LX/3RA;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    const/16 v5, 0x15e7

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {v0}, LX/0Sg;->a(LX/0QB;)LX/0Sg;

    move-result-object v6

    check-cast v6, LX/0Sg;

    invoke-static {v0}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v7

    check-cast v7, LX/01T;

    .line 579178
    const/4 v8, 0x0

    move-object v8, v8

    .line 579179
    move-object v8, v8

    .line 579180
    check-cast v8, Ljava/lang/Class;

    invoke-direct/range {v3 .. v8}, LX/3RA;-><init>(Landroid/content/Context;LX/0Or;LX/0Sg;LX/01T;Ljava/lang/Class;)V

    .line 579181
    move-object v0, v3

    .line 579182
    sput-object v0, LX/3RA;->h:LX/3RA;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 579183
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 579184
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 579185
    :cond_1
    sget-object v0, LX/3RA;->h:LX/3RA;

    return-object v0

    .line 579186
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 579187
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a$redex0(LX/3RA;Landroid/content/Intent;)V
    .locals 5

    .prologue
    .line 579162
    iget-object v0, p0, LX/3RA;->e:LX/01T;

    sget-object v1, LX/01T;->MESSENGER:LX/01T;

    if-ne v0, v1, :cond_2

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 579163
    if-nez v0, :cond_0

    .line 579164
    :goto_1
    return-void

    .line 579165
    :cond_0
    sget-object v1, LX/3RB;->l:Ljava/lang/String;

    iget-object v0, p0, LX/3RA;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 579166
    sget-object v0, LX/3RA;->a:LX/0Rf;

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 579167
    iget-boolean v0, p0, LX/3RA;->g:Z

    if-eqz v0, :cond_3

    .line 579168
    invoke-static {p0, p1}, LX/3RA;->c(LX/3RA;Landroid/content/Intent;)V

    .line 579169
    :goto_2
    goto :goto_1

    .line 579170
    :cond_1
    invoke-static {p0, p1}, LX/3RA;->c(LX/3RA;Landroid/content/Intent;)V

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 579171
    :cond_3
    iget-object v0, p0, LX/3RA;->d:LX/0Sg;

    const-string v1, "ChatHeadsInitializer initAfterUiIdle"

    new-instance v2, Lcom/facebook/messaging/chatheads/intents/ChatHeadsIntentDispatcher$1;

    invoke-direct {v2, p0, p1}, Lcom/facebook/messaging/chatheads/intents/ChatHeadsIntentDispatcher$1;-><init>(LX/3RA;Landroid/content/Intent;)V

    sget-object v3, LX/0VZ;->APPLICATION_LOADED_UI_IDLE:LX/0VZ;

    sget-object v4, LX/0Vm;->BACKGROUND:LX/0Vm;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/0Sg;->a(Ljava/lang/String;Ljava/lang/Runnable;LX/0VZ;LX/0Vm;)LX/0Va;

    goto :goto_2
.end method

.method public static b(LX/3RA;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 579159
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LX/3RA;->b:Landroid/content/Context;

    iget-object v2, p0, LX/3RA;->f:Ljava/lang/Class;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 579160
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 579161
    return-object v0
.end method

.method public static c(LX/3RA;Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 579152
    iget-object v0, p0, LX/3RA;->b:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 579153
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 579154
    sget-object v0, LX/3RB;->g:Ljava/lang/String;

    invoke-static {p0, v0}, LX/3RA;->b(LX/3RA;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 579155
    sget-object v1, LX/FDW;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 579156
    sget-object v1, LX/3RB;->n:Ljava/lang/String;

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 579157
    invoke-static {p0, v0}, LX/3RA;->a$redex0(LX/3RA;Landroid/content/Intent;)V

    .line 579158
    return-void
.end method
