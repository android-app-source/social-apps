.class public LX/4wM;
.super Ljava/lang/ref/SoftReference;
.source ""

# interfaces
.implements LX/0Qf;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/ref/SoftReference",
        "<TV;>;",
        "LX/0Qf",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field public final a:LX/0R1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0R1",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;LX/0R1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/ReferenceQueue",
            "<TV;>;TV;",
            "LX/0R1",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 820069
    invoke-direct {p0, p2, p1}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;Ljava/lang/ref/ReferenceQueue;)V

    .line 820070
    iput-object p3, p0, LX/4wM;->a:LX/0R1;

    .line 820071
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 820063
    const/4 v0, 0x1

    return v0
.end method

.method public a(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;LX/0R1;)LX/0Qf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/ReferenceQueue",
            "<TV;>;TV;",
            "LX/0R1",
            "<TK;TV;>;)",
            "LX/0Qf",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 820068
    new-instance v0, LX/4wM;

    invoke-direct {v0, p1, p2, p3}, LX/4wM;-><init>(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;LX/0R1;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 820067
    return-void
.end method

.method public final b()LX/0R1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0R1",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 820072
    iget-object v0, p0, LX/4wM;->a:LX/0R1;

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 820066
    const/4 v0, 0x0

    return v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 820065
    const/4 v0, 0x1

    return v0
.end method

.method public final e()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .prologue
    .line 820064
    invoke-virtual {p0}, LX/4wM;->get()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
