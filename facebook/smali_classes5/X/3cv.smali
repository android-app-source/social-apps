.class public final LX/3cv;
.super LX/3Eh;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;Ljava/lang/Long;)V
    .locals 0

    .prologue
    .line 615844
    iput-object p1, p0, LX/3cv;->a:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    invoke-direct {p0, p2}, LX/3Eh;-><init>(Ljava/lang/Long;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/fbservice/service/OperationResult;)V
    .locals 8

    .prologue
    .line 615849
    invoke-super {p0, p1}, LX/3Eh;->a(Lcom/facebook/fbservice/service/OperationResult;)V

    .line 615850
    iget-boolean v0, p1, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    move v0, v0

    .line 615851
    if-eqz v0, :cond_1

    .line 615852
    iget-object v0, p0, LX/3cv;->a:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    .line 615853
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;

    .line 615854
    iget-object v2, v0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->H:LX/1rn;

    invoke-virtual {v2}, LX/1rn;->c()V

    .line 615855
    if-eqz v1, :cond_0

    iget-object v2, v1, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;->a:LX/3T7;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;->a:LX/3T7;

    invoke-virtual {v2}, LX/3T7;->c()LX/0Px;

    move-result-object v2

    if-nez v2, :cond_0

    iget-object v1, v1, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;->a:LX/3T7;

    invoke-virtual {v1}, LX/3T7;->b()LX/0Px;

    move-result-object v1

    if-nez v1, :cond_0

    .line 615856
    iget-object v1, v0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->af:LX/3Te;

    .line 615857
    iget-object v2, v1, LX/3Te;->e:LX/3Tk;

    .line 615858
    sget-object v1, LX/3dV;->SUCCESS:LX/3dV;

    invoke-static {v1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    iput-object v1, v2, LX/3Tk;->o:LX/0am;

    .line 615859
    iget-object v1, v2, LX/3Tk;->i:LX/3Tg;

    invoke-interface {v1}, LX/3Tg;->e()V

    .line 615860
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->a(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;Z)V

    .line 615861
    :cond_0
    invoke-static {v0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->v$redex0(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)V

    .line 615862
    iget-object v1, v0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->R:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x910005

    const/4 p0, 0x2

    invoke-interface {v1, v2, p0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 615863
    :goto_0
    return-void

    .line 615864
    :cond_1
    iget-object v0, p0, LX/3cv;->a:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    .line 615865
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f08006f

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 615866
    iget-object v2, p1, Lcom/facebook/fbservice/service/OperationResult;->errorCode:LX/1nY;

    move-object v2, v2

    .line 615867
    invoke-virtual {v2}, LX/1nY;->ordinal()I

    move-result v2

    .line 615868
    iget-object v3, p1, Lcom/facebook/fbservice/service/OperationResult;->errorDescription:Ljava/lang/String;

    move-object v3, v3

    .line 615869
    if-nez v2, :cond_2

    .line 615870
    :goto_1
    move-object v1, v1

    .line 615871
    sget-object v2, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->Z:Ljava/lang/Class;

    invoke-static {v2, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 615872
    iget-object v2, v0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->o:LX/03V;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->Z:Ljava/lang/Class;

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_sync_failed"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 615873
    iget-object v0, p0, LX/3cv;->a:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    invoke-static {v0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->u$redex0(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)V

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f081154

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v1, v6, v7

    const/4 v7, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v6, v7

    const/4 v7, 0x2

    aput-object v3, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 615846
    invoke-super {p0, p1}, LX/3Eh;->onFailure(Ljava/lang/Throwable;)V

    .line 615847
    iget-object v0, p0, LX/3cv;->a:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    invoke-static {v0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->u$redex0(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)V

    .line 615848
    return-void
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 615845
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    invoke-virtual {p0, p1}, LX/3cv;->a(Lcom/facebook/fbservice/service/OperationResult;)V

    return-void
.end method
