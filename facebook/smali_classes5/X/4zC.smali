.class public final LX/4zC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/ListIterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/ListIterator",
        "<TV;>;"
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/Object;

.field public b:I

.field public c:LX/4zA;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4zA",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public d:LX/4zA;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4zA",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public e:LX/4zA;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4zA",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public final synthetic f:LX/4zD;


# direct methods
.method public constructor <init>(LX/4zD;Ljava/lang/Object;)V
    .locals 1
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 822448
    iput-object p1, p0, LX/4zC;->f:LX/4zD;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 822449
    iput-object p2, p0, LX/4zC;->a:Ljava/lang/Object;

    .line 822450
    iget-object v0, p1, LX/4zD;->c:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4z9;

    .line 822451
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, LX/4zC;->c:LX/4zA;

    .line 822452
    return-void

    .line 822453
    :cond_0
    iget-object v0, v0, LX/4z9;->a:LX/4zA;

    goto :goto_0
.end method

.method public constructor <init>(LX/4zD;Ljava/lang/Object;I)V
    .locals 4
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 822460
    iput-object p1, p0, LX/4zC;->f:LX/4zD;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 822461
    iget-object v0, p1, LX/4zD;->c:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4z9;

    .line 822462
    if-nez v0, :cond_0

    const/4 v1, 0x0

    .line 822463
    :goto_0
    invoke-static {p3, v1}, LX/0PB;->checkPositionIndex(II)I

    .line 822464
    div-int/lit8 v3, v1, 0x2

    if-lt p3, v3, :cond_2

    .line 822465
    if-nez v0, :cond_1

    move-object v0, v2

    :goto_1
    iput-object v0, p0, LX/4zC;->e:LX/4zA;

    .line 822466
    iput v1, p0, LX/4zC;->b:I

    .line 822467
    :goto_2
    add-int/lit8 v0, p3, 0x1

    if-ge p3, v1, :cond_4

    .line 822468
    invoke-virtual {p0}, LX/4zC;->previous()Ljava/lang/Object;

    move p3, v0

    goto :goto_2

    .line 822469
    :cond_0
    iget v1, v0, LX/4z9;->c:I

    goto :goto_0

    .line 822470
    :cond_1
    iget-object v0, v0, LX/4z9;->b:LX/4zA;

    goto :goto_1

    .line 822471
    :cond_2
    if-nez v0, :cond_3

    move-object v0, v2

    :goto_3
    iput-object v0, p0, LX/4zC;->c:LX/4zA;

    .line 822472
    :goto_4
    add-int/lit8 v0, p3, -0x1

    if-lez p3, :cond_4

    .line 822473
    invoke-virtual {p0}, LX/4zC;->next()Ljava/lang/Object;

    move p3, v0

    goto :goto_4

    .line 822474
    :cond_3
    iget-object v0, v0, LX/4z9;->a:LX/4zA;

    goto :goto_3

    .line 822475
    :cond_4
    iput-object p2, p0, LX/4zC;->a:Ljava/lang/Object;

    .line 822476
    iput-object v2, p0, LX/4zC;->d:LX/4zA;

    .line 822477
    return-void
.end method


# virtual methods
.method public final add(Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 822456
    iget-object v0, p0, LX/4zC;->f:LX/4zD;

    iget-object v1, p0, LX/4zC;->a:Ljava/lang/Object;

    iget-object v2, p0, LX/4zC;->c:LX/4zA;

    invoke-static {v0, v1, p1, v2}, LX/4zD;->a$redex0(LX/4zD;Ljava/lang/Object;Ljava/lang/Object;LX/4zA;)LX/4zA;

    move-result-object v0

    iput-object v0, p0, LX/4zC;->e:LX/4zA;

    .line 822457
    iget v0, p0, LX/4zC;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/4zC;->b:I

    .line 822458
    const/4 v0, 0x0

    iput-object v0, p0, LX/4zC;->d:LX/4zA;

    .line 822459
    return-void
.end method

.method public final hasNext()Z
    .locals 1

    .prologue
    .line 822455
    iget-object v0, p0, LX/4zC;->c:LX/4zA;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasPrevious()Z
    .locals 1

    .prologue
    .line 822454
    iget-object v0, p0, LX/4zC;->e:LX/4zA;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final next()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .prologue
    .line 822478
    iget-object v0, p0, LX/4zC;->c:LX/4zA;

    invoke-static {v0}, LX/4zD;->i(Ljava/lang/Object;)V

    .line 822479
    iget-object v0, p0, LX/4zC;->c:LX/4zA;

    iput-object v0, p0, LX/4zC;->d:LX/4zA;

    iput-object v0, p0, LX/4zC;->e:LX/4zA;

    .line 822480
    iget-object v0, p0, LX/4zC;->c:LX/4zA;

    iget-object v0, v0, LX/4zA;->e:LX/4zA;

    iput-object v0, p0, LX/4zC;->c:LX/4zA;

    .line 822481
    iget v0, p0, LX/4zC;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/4zC;->b:I

    .line 822482
    iget-object v0, p0, LX/4zC;->d:LX/4zA;

    iget-object v0, v0, LX/4zA;->b:Ljava/lang/Object;

    return-object v0
.end method

.method public final nextIndex()I
    .locals 1

    .prologue
    .line 822428
    iget v0, p0, LX/4zC;->b:I

    return v0
.end method

.method public final previous()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .prologue
    .line 822429
    iget-object v0, p0, LX/4zC;->e:LX/4zA;

    invoke-static {v0}, LX/4zD;->i(Ljava/lang/Object;)V

    .line 822430
    iget-object v0, p0, LX/4zC;->e:LX/4zA;

    iput-object v0, p0, LX/4zC;->d:LX/4zA;

    iput-object v0, p0, LX/4zC;->c:LX/4zA;

    .line 822431
    iget-object v0, p0, LX/4zC;->e:LX/4zA;

    iget-object v0, v0, LX/4zA;->f:LX/4zA;

    iput-object v0, p0, LX/4zC;->e:LX/4zA;

    .line 822432
    iget v0, p0, LX/4zC;->b:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/4zC;->b:I

    .line 822433
    iget-object v0, p0, LX/4zC;->d:LX/4zA;

    iget-object v0, v0, LX/4zA;->b:Ljava/lang/Object;

    return-object v0
.end method

.method public final previousIndex()I
    .locals 1

    .prologue
    .line 822434
    iget v0, p0, LX/4zC;->b:I

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public final remove()V
    .locals 2

    .prologue
    .line 822435
    iget-object v0, p0, LX/4zC;->d:LX/4zA;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0P6;->a(Z)V

    .line 822436
    iget-object v0, p0, LX/4zC;->d:LX/4zA;

    iget-object v1, p0, LX/4zC;->c:LX/4zA;

    if-eq v0, v1, :cond_1

    .line 822437
    iget-object v0, p0, LX/4zC;->d:LX/4zA;

    iget-object v0, v0, LX/4zA;->f:LX/4zA;

    iput-object v0, p0, LX/4zC;->e:LX/4zA;

    .line 822438
    iget v0, p0, LX/4zC;->b:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/4zC;->b:I

    .line 822439
    :goto_1
    iget-object v0, p0, LX/4zC;->f:LX/4zD;

    iget-object v1, p0, LX/4zC;->d:LX/4zA;

    invoke-static {v0, v1}, LX/4zD;->a$redex0(LX/4zD;LX/4zA;)V

    .line 822440
    const/4 v0, 0x0

    iput-object v0, p0, LX/4zC;->d:LX/4zA;

    .line 822441
    return-void

    .line 822442
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 822443
    :cond_1
    iget-object v0, p0, LX/4zC;->d:LX/4zA;

    iget-object v0, v0, LX/4zA;->e:LX/4zA;

    iput-object v0, p0, LX/4zC;->c:LX/4zA;

    goto :goto_1
.end method

.method public final set(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 822444
    iget-object v0, p0, LX/4zC;->d:LX/4zA;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 822445
    iget-object v0, p0, LX/4zC;->d:LX/4zA;

    iput-object p1, v0, LX/4zA;->b:Ljava/lang/Object;

    .line 822446
    return-void

    .line 822447
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
