.class public LX/458;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 670167
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/telephony/TelephonyManager;)LX/456;
    .locals 3

    .prologue
    .line 670168
    invoke-virtual {p0}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v0

    .line 670169
    const/4 p0, 0x3

    const/4 v1, 0x0

    .line 670170
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v2, p0, :cond_2

    .line 670171
    :cond_0
    :goto_0
    move v1, v1

    .line 670172
    const/4 p0, 0x3

    .line 670173
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-gt v2, p0, :cond_3

    .line 670174
    :cond_1
    const/4 v2, 0x0

    .line 670175
    :goto_1
    move v0, v2

    .line 670176
    invoke-static {v1, v0}, LX/456;->fromMncMcc(II)LX/456;

    move-result-object v0

    return-object v0

    :cond_2
    invoke-virtual {v0, v1, p0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/458;->c(Ljava/lang/String;)I

    move-result v1

    goto :goto_0

    :cond_3
    invoke-virtual {v0, p0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/458;->c(Ljava/lang/String;)I

    move-result v2

    goto :goto_1
.end method

.method public static c(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 670177
    :try_start_0
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 670178
    :goto_0
    return v0

    :catch_0
    const/4 v0, 0x0

    goto :goto_0
.end method
