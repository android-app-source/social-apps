.class public final LX/3ct;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Vj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Vj",
        "<",
        "Lcom/facebook/notifications/server/FetchNotificationSeenStatesParams;",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1rk;


# direct methods
.method public constructor <init>(LX/1rk;)V
    .locals 0

    .prologue
    .line 615829
    iput-object p1, p0, LX/3ct;->a:LX/1rk;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4

    .prologue
    .line 615825
    check-cast p1, Lcom/facebook/notifications/server/FetchNotificationSeenStatesParams;

    .line 615826
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 615827
    const-string v1, "notificationsFetchSeenStatesParams"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 615828
    iget-object v1, p0, LX/3ct;->a:LX/1rk;

    iget-object v1, v1, LX/1rk;->e:LX/0aG;

    const-string v2, "fecthNotificationSeenStates"

    const v3, 0x7589bb60

    invoke-static {v1, v2, v0, v3}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    return-object v0
.end method
