.class public LX/3tz;
.super Landroid/view/ViewGroup$MarginLayoutParams;
.source ""


# instance fields
.field public a:I


# direct methods
.method public constructor <init>(II)V
    .locals 1

    .prologue
    .line 647328
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    .line 647329
    const/4 v0, 0x0

    iput v0, p0, LX/3tz;->a:I

    .line 647330
    const v0, 0x800013

    iput v0, p0, LX/3tz;->a:I

    .line 647331
    return-void
.end method

.method public constructor <init>(LX/3tz;)V
    .locals 1

    .prologue
    .line 647332
    invoke-direct {p0, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    .line 647333
    const/4 v0, 0x0

    iput v0, p0, LX/3tz;->a:I

    .line 647334
    iget v0, p1, LX/3tz;->a:I

    iput v0, p0, LX/3tz;->a:I

    .line 647335
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 647336
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 647337
    iput v2, p0, LX/3tz;->a:I

    .line 647338
    sget-object v0, LX/03r;->ActionBarLayout:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 647339
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, LX/3tz;->a:I

    .line 647340
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 647341
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    .prologue
    .line 647342
    invoke-direct {p0, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 647343
    const/4 v0, 0x0

    iput v0, p0, LX/3tz;->a:I

    .line 647344
    return-void
.end method
