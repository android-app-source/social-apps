.class public LX/4bu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lorg/apache/http/client/CookieStore;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/4bu;


# instance fields
.field private final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/http/cookie/Cookie;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/http/cookie/Cookie;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 794274
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 794275
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/4bu;->a:Ljava/util/ArrayList;

    .line 794276
    new-instance v0, Lorg/apache/http/cookie/CookieIdentityComparator;

    invoke-direct {v0}, Lorg/apache/http/cookie/CookieIdentityComparator;-><init>()V

    iput-object v0, p0, LX/4bu;->b:Ljava/util/Comparator;

    .line 794277
    return-void
.end method

.method public static a(LX/0QB;)LX/4bu;
    .locals 3

    .prologue
    .line 794262
    sget-object v0, LX/4bu;->c:LX/4bu;

    if-nez v0, :cond_1

    .line 794263
    const-class v1, LX/4bu;

    monitor-enter v1

    .line 794264
    :try_start_0
    sget-object v0, LX/4bu;->c:LX/4bu;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 794265
    if-eqz v2, :cond_0

    .line 794266
    :try_start_1
    new-instance v0, LX/4bu;

    invoke-direct {v0}, LX/4bu;-><init>()V

    .line 794267
    move-object v0, v0

    .line 794268
    sput-object v0, LX/4bu;->c:LX/4bu;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 794269
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 794270
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 794271
    :cond_1
    sget-object v0, LX/4bu;->c:LX/4bu;

    return-object v0

    .line 794272
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 794273
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final declared-synchronized addCookie(Lorg/apache/http/cookie/Cookie;)V
    .locals 3

    .prologue
    .line 794246
    monitor-enter p0

    if-eqz p1, :cond_2

    .line 794247
    :try_start_0
    iget-object v0, p0, LX/4bu;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 794248
    iget-object v1, p0, LX/4bu;->b:Ljava/util/Comparator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, p1, v2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v1

    if-nez v1, :cond_0

    .line 794249
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 794250
    :cond_1
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    invoke-interface {p1, v0}, Lorg/apache/http/cookie/Cookie;->isExpired(Ljava/util/Date;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 794251
    iget-object v0, p0, LX/4bu;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 794252
    :cond_2
    monitor-exit p0

    return-void

    .line 794253
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized clear()V
    .locals 1

    .prologue
    .line 794278
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/4bu;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 794279
    monitor-exit p0

    return-void

    .line 794280
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized clearExpired(Ljava/util/Date;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 794254
    monitor-enter p0

    if-nez p1, :cond_0

    .line 794255
    :goto_0
    monitor-exit p0

    return v0

    .line 794256
    :cond_0
    :try_start_0
    iget-object v1, p0, LX/4bu;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 794257
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/cookie/Cookie;

    invoke-interface {v0, p1}, Lorg/apache/http/cookie/Cookie;->isExpired(Ljava/util/Date;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 794258
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 794259
    const/4 v1, 0x1

    goto :goto_1

    :cond_2
    move v0, v1

    .line 794260
    goto :goto_0

    .line 794261
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized getCookies()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/cookie/Cookie;",
            ">;"
        }
    .end annotation

    .prologue
    .line 794245
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/4bu;->a:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 794244
    iget-object v0, p0, LX/4bu;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
