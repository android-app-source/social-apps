.class public LX/4On;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 698440
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 32

    .prologue
    .line 698336
    const/16 v26, 0x0

    .line 698337
    const/16 v25, 0x0

    .line 698338
    const/16 v24, 0x0

    .line 698339
    const/16 v23, 0x0

    .line 698340
    const/16 v22, 0x0

    .line 698341
    const/16 v19, 0x0

    .line 698342
    const-wide/16 v20, 0x0

    .line 698343
    const/16 v18, 0x0

    .line 698344
    const/16 v17, 0x0

    .line 698345
    const/16 v16, 0x0

    .line 698346
    const/4 v15, 0x0

    .line 698347
    const/4 v14, 0x0

    .line 698348
    const/4 v13, 0x0

    .line 698349
    const/4 v12, 0x0

    .line 698350
    const/4 v11, 0x0

    .line 698351
    const/4 v10, 0x0

    .line 698352
    const/4 v9, 0x0

    .line 698353
    const/4 v8, 0x0

    .line 698354
    const/4 v7, 0x0

    .line 698355
    const/4 v6, 0x0

    .line 698356
    const/4 v5, 0x0

    .line 698357
    const/4 v4, 0x0

    .line 698358
    const/4 v3, 0x0

    .line 698359
    const/4 v2, 0x0

    .line 698360
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v27

    sget-object v28, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    if-eq v0, v1, :cond_1a

    .line 698361
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 698362
    const/4 v2, 0x0

    .line 698363
    :goto_0
    return v2

    .line 698364
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_16

    .line 698365
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 698366
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 698367
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v29, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v29

    if-eq v6, v0, :cond_0

    if-eqz v2, :cond_0

    .line 698368
    const-string v6, "action_links"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 698369
    invoke-static/range {p0 .. p1}, LX/2bb;->b(LX/15w;LX/186;)I

    move-result v2

    move/from16 v28, v2

    goto :goto_1

    .line 698370
    :cond_1
    const-string v6, "actors"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 698371
    invoke-static/range {p0 .. p1}, LX/2bM;->b(LX/15w;LX/186;)I

    move-result v2

    move/from16 v27, v2

    goto :goto_1

    .line 698372
    :cond_2
    const-string v6, "cache_id"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 698373
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v26, v2

    goto :goto_1

    .line 698374
    :cond_3
    const-string v6, "debug_info"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 698375
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v25, v2

    goto :goto_1

    .line 698376
    :cond_4
    const-string v6, "feedback"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 698377
    invoke-static/range {p0 .. p1}, LX/2bG;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v24, v2

    goto :goto_1

    .line 698378
    :cond_5
    const-string v6, "feedback_context"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 698379
    invoke-static/range {p0 .. p1}, LX/2aq;->a(LX/15w;LX/186;)I

    move-result v2

    move v7, v2

    goto :goto_1

    .line 698380
    :cond_6
    const-string v6, "fetchTimeMs"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 698381
    const/4 v2, 0x1

    .line 698382
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto/16 :goto_1

    .line 698383
    :cond_7
    const-string v6, "hideable_token"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 698384
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v23, v2

    goto/16 :goto_1

    .line 698385
    :cond_8
    const-string v6, "items"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 698386
    invoke-static/range {p0 .. p1}, LX/4Om;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v22, v2

    goto/16 :goto_1

    .line 698387
    :cond_9
    const-string v6, "local_last_negative_feedback_action_type"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 698388
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v21, v2

    goto/16 :goto_1

    .line 698389
    :cond_a
    const-string v6, "local_story_visibility"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 698390
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v20, v2

    goto/16 :goto_1

    .line 698391
    :cond_b
    const-string v6, "local_story_visible_height"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 698392
    const/4 v2, 0x1

    .line 698393
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move v9, v2

    move/from16 v19, v6

    goto/16 :goto_1

    .line 698394
    :cond_c
    const-string v6, "negative_feedback_actions"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_d

    .line 698395
    invoke-static/range {p0 .. p1}, LX/2bY;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v18, v2

    goto/16 :goto_1

    .line 698396
    :cond_d
    const-string v6, "privacy_scope"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_e

    .line 698397
    invoke-static/range {p0 .. p1}, LX/2bo;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v17, v2

    goto/16 :goto_1

    .line 698398
    :cond_e
    const-string v6, "seen_state"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_f

    .line 698399
    const/4 v2, 0x1

    .line 698400
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v6

    move v8, v2

    move-object/from16 v16, v6

    goto/16 :goto_1

    .line 698401
    :cond_f
    const-string v6, "short_term_cache_key"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_10

    .line 698402
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v15, v2

    goto/16 :goto_1

    .line 698403
    :cond_10
    const-string v6, "story_header"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_11

    .line 698404
    invoke-static/range {p0 .. p1}, LX/32O;->a(LX/15w;LX/186;)I

    move-result v2

    move v14, v2

    goto/16 :goto_1

    .line 698405
    :cond_11
    const-string v6, "substories_grouping_reasons"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_12

    .line 698406
    const-class v2, Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v2}, LX/2gu;->a(LX/15w;LX/186;Ljava/lang/Class;)I

    move-result v2

    move v13, v2

    goto/16 :goto_1

    .line 698407
    :cond_12
    const-string v6, "title"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_13

    .line 698408
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v2

    move v12, v2

    goto/16 :goto_1

    .line 698409
    :cond_13
    const-string v6, "tracking"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_14

    .line 698410
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v11, v2

    goto/16 :goto_1

    .line 698411
    :cond_14
    const-string v6, "jobSearchUnitTitle"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_15

    .line 698412
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v2

    move v10, v2

    goto/16 :goto_1

    .line 698413
    :cond_15
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 698414
    :cond_16
    const/16 v2, 0x15

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 698415
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 698416
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 698417
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 698418
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 698419
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 698420
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 698421
    if-eqz v3, :cond_17

    .line 698422
    const/4 v3, 0x6

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 698423
    :cond_17
    const/4 v2, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 698424
    const/16 v2, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 698425
    const/16 v2, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 698426
    const/16 v2, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 698427
    if-eqz v9, :cond_18

    .line 698428
    const/16 v2, 0xb

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 698429
    :cond_18
    const/16 v2, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 698430
    const/16 v2, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 698431
    if-eqz v8, :cond_19

    .line 698432
    const/16 v2, 0xe

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 698433
    :cond_19
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 698434
    const/16 v2, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 698435
    const/16 v2, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 698436
    const/16 v2, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 698437
    const/16 v2, 0x13

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 698438
    const/16 v2, 0x14

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 698439
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_1a
    move/from16 v27, v25

    move/from16 v28, v26

    move/from16 v25, v23

    move/from16 v26, v24

    move/from16 v23, v18

    move/from16 v24, v22

    move/from16 v18, v13

    move/from16 v22, v17

    move/from16 v17, v12

    move v13, v8

    move v12, v7

    move v8, v2

    move/from16 v7, v19

    move/from16 v19, v14

    move v14, v9

    move v9, v3

    move v3, v4

    move-wide/from16 v30, v20

    move/from16 v20, v15

    move/from16 v21, v16

    move v15, v10

    move-object/from16 v16, v11

    move v10, v5

    move v11, v6

    move-wide/from16 v4, v30

    goto/16 :goto_1
.end method

.method public static a(LX/15w;S)LX/15i;
    .locals 5

    .prologue
    .line 698325
    const/4 v0, 0x1

    const/4 v4, 0x0

    .line 698326
    new-instance v2, LX/186;

    const/16 v1, 0x80

    invoke-direct {v2, v1}, LX/186;-><init>(I)V

    .line 698327
    invoke-static {p0, v2}, LX/4On;->a(LX/15w;LX/186;)I

    move-result v1

    .line 698328
    if-eqz v0, :cond_0

    .line 698329
    const/4 v3, 0x2

    invoke-virtual {v2, v3}, LX/186;->c(I)V

    .line 698330
    invoke-virtual {v2, v4, p1, v4}, LX/186;->a(ISI)V

    .line 698331
    const/4 v3, 0x1

    invoke-virtual {v2, v3, v1}, LX/186;->b(II)V

    .line 698332
    invoke-virtual {v2}, LX/186;->d()I

    move-result v1

    .line 698333
    :cond_0
    invoke-virtual {v2, v1}, LX/186;->d(I)V

    .line 698334
    invoke-static {v2}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v1

    move-object v0, v1

    .line 698335
    return-object v0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/16 v5, 0x11

    const/16 v4, 0xe

    const/4 v3, 0x0

    .line 698230
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 698231
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 698232
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 698233
    const-string v0, "name"

    const-string v1, "JobCollectionFeedUnit"

    invoke-virtual {p2, v0, v1}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 698234
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 698235
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 698236
    if-eqz v0, :cond_0

    .line 698237
    const-string v1, "action_links"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 698238
    invoke-static {p0, v0, p2, p3}, LX/2bb;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 698239
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 698240
    if-eqz v0, :cond_1

    .line 698241
    const-string v1, "actors"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 698242
    invoke-static {p0, v0, p2, p3}, LX/2bM;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 698243
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 698244
    if-eqz v0, :cond_2

    .line 698245
    const-string v1, "cache_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 698246
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 698247
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 698248
    if-eqz v0, :cond_3

    .line 698249
    const-string v1, "debug_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 698250
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 698251
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 698252
    if-eqz v0, :cond_4

    .line 698253
    const-string v1, "feedback"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 698254
    invoke-static {p0, v0, p2, p3}, LX/2bG;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 698255
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 698256
    if-eqz v0, :cond_5

    .line 698257
    const-string v1, "feedback_context"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 698258
    invoke-static {p0, v0, p2, p3}, LX/2aq;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 698259
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0, v6, v7}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 698260
    cmp-long v2, v0, v6

    if-eqz v2, :cond_6

    .line 698261
    const-string v2, "fetchTimeMs"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 698262
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 698263
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 698264
    if-eqz v0, :cond_7

    .line 698265
    const-string v1, "hideable_token"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 698266
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 698267
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 698268
    if-eqz v0, :cond_9

    .line 698269
    const-string v1, "items"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 698270
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 698271
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_8

    .line 698272
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/4Om;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 698273
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 698274
    :cond_8
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 698275
    :cond_9
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 698276
    if-eqz v0, :cond_a

    .line 698277
    const-string v1, "local_last_negative_feedback_action_type"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 698278
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 698279
    :cond_a
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 698280
    if-eqz v0, :cond_b

    .line 698281
    const-string v1, "local_story_visibility"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 698282
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 698283
    :cond_b
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 698284
    if-eqz v0, :cond_c

    .line 698285
    const-string v1, "local_story_visible_height"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 698286
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 698287
    :cond_c
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 698288
    if-eqz v0, :cond_d

    .line 698289
    const-string v1, "negative_feedback_actions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 698290
    invoke-static {p0, v0, p2, p3}, LX/2bY;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 698291
    :cond_d
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 698292
    if-eqz v0, :cond_e

    .line 698293
    const-string v1, "privacy_scope"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 698294
    invoke-static {p0, v0, p2, p3}, LX/2bo;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 698295
    :cond_e
    invoke-virtual {p0, p1, v4, v3}, LX/15i;->a(IIS)S

    move-result v0

    .line 698296
    if-eqz v0, :cond_f

    .line 698297
    const-string v0, "seen_state"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 698298
    const-class v0, Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-virtual {p0, p1, v4, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 698299
    :cond_f
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 698300
    if-eqz v0, :cond_10

    .line 698301
    const-string v1, "short_term_cache_key"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 698302
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 698303
    :cond_10
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 698304
    if-eqz v0, :cond_11

    .line 698305
    const-string v1, "story_header"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 698306
    invoke-static {p0, v0, p2, p3}, LX/32O;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 698307
    :cond_11
    invoke-virtual {p0, p1, v5}, LX/15i;->g(II)I

    move-result v0

    .line 698308
    if-eqz v0, :cond_12

    .line 698309
    const-string v0, "substories_grouping_reasons"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 698310
    const-class v0, Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;

    invoke-virtual {p0, p1, v5, v0}, LX/15i;->b(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->b(Ljava/util/Iterator;LX/0nX;)V

    .line 698311
    :cond_12
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 698312
    if-eqz v0, :cond_13

    .line 698313
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 698314
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 698315
    :cond_13
    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 698316
    if-eqz v0, :cond_14

    .line 698317
    const-string v1, "tracking"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 698318
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 698319
    :cond_14
    const/16 v0, 0x14

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 698320
    if-eqz v0, :cond_15

    .line 698321
    const-string v1, "jobSearchUnitTitle"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 698322
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 698323
    :cond_15
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 698324
    return-void
.end method
