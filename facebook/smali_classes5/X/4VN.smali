.class public LX/4VN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2lk;


# static fields
.field private static final h:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0si;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field private final d:LX/0t8;

.field private final e:LX/0sT;

.field private final f:LX/4Ud;

.field public final g:I

.field private final i:LX/3Bp;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 742319
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, LX/4VN;->h:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method public constructor <init>(LX/4VM;LX/0t8;LX/0Ot;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0sT;)V
    .locals 2
    .param p1    # LX/4VM;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4VM;",
            "Lcom/facebook/graphql/executor/cache/ConsistencyConfig;",
            "LX/0Ot",
            "<",
            "LX/0si;",
            ">;",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            "LX/0sT;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 742322
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 742323
    new-instance v0, LX/3Bp;

    invoke-direct {v0, p0}, LX/3Bp;-><init>(LX/2lk;)V

    iput-object v0, p0, LX/4VN;->i:LX/3Bp;

    .line 742324
    iput-object p2, p0, LX/4VN;->d:LX/0t8;

    .line 742325
    iput-object p3, p0, LX/4VN;->b:LX/0Ot;

    .line 742326
    iput-object p4, p0, LX/4VN;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 742327
    iput-object p5, p0, LX/4VN;->e:LX/0sT;

    .line 742328
    new-instance v0, LX/4Ue;

    new-instance v1, LX/JaO;

    invoke-direct {v1}, LX/JaO;-><init>()V

    invoke-direct {v0, v1}, LX/4Ue;-><init>(LX/4Ud;)V

    iput-object v0, p0, LX/4VN;->f:LX/4Ud;

    .line 742329
    if-eqz p1, :cond_0

    .line 742330
    iget-object v0, p1, LX/4VM;->a:Ljava/util/Map;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, LX/4VN;->a:Ljava/util/Map;

    .line 742331
    iget v0, p1, LX/4VM;->b:I

    iput v0, p0, LX/4VN;->g:I

    .line 742332
    :goto_0
    return-void

    .line 742333
    :cond_0
    sget-object v0, Ljava/util/Collections;->EMPTY_MAP:Ljava/util/Map;

    iput-object v0, p0, LX/4VN;->a:Ljava/util/Map;

    .line 742334
    const/4 v0, 0x0

    iput v0, p0, LX/4VN;->g:I

    goto :goto_0
.end method

.method private e(LX/0jT;)[[I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;)[[I"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 742335
    invoke-static {p1}, LX/4VN;->f(LX/0jT;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/4VN;->e:LX/0sT;

    invoke-virtual {v0}, LX/0sT;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 742336
    iget-object v0, p0, LX/4VN;->f:LX/4Ud;

    check-cast p1, Lcom/facebook/graphql/modelutil/FragmentModel;

    invoke-interface {p1}, Lcom/facebook/graphql/modelutil/FragmentModel;->d_()I

    move-result v1

    invoke-interface {v0, v1}, LX/4Ud;->a(I)[[I

    move-result-object v0

    .line 742337
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static f(LX/0jT;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 742340
    instance-of v2, p0, Lcom/facebook/graphql/modelutil/FragmentModel;

    if-eqz v2, :cond_2

    .line 742341
    check-cast p0, Lcom/facebook/graphql/modelutil/FragmentModel;

    invoke-interface {p0}, Lcom/facebook/flatbuffers/MutableFlattenable;->q_()LX/15i;

    move-result-object v2

    .line 742342
    if-nez v2, :cond_1

    .line 742343
    :cond_0
    :goto_0
    return v0

    .line 742344
    :cond_1
    invoke-virtual {v2}, LX/15i;->e()Ljava/nio/ByteBuffer;

    move-result-object v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 742345
    goto :goto_0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 742338
    iget-object v0, p0, LX/4VN;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final a(LX/0jT;)Z
    .locals 1

    .prologue
    .line 742339
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 742320
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a(Ljava/util/List;)Z
    .locals 1

    .prologue
    .line 742321
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/0jT;)LX/0jT;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;)TT;"
        }
    .end annotation

    .prologue
    const v8, 0x31001f    # 4.499983E-39f

    .line 742290
    sget-object v1, LX/4VN;->h:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v7

    .line 742291
    invoke-direct {p0, p1}, LX/4VN;->e(LX/0jT;)[[I

    move-result-object v3

    .line 742292
    iget-object v1, p0, LX/4VN;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v1, v8, v7}, Lcom/facebook/quicklog/QuickPerformanceLogger;->e(II)V

    .line 742293
    if-eqz v3, :cond_0

    .line 742294
    :try_start_0
    iget-object v1, p0, LX/4VN;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x31001f    # 4.499983E-39f

    const-string v4, "buffer_write"

    invoke-interface {v1, v2, v7, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;)V

    .line 742295
    move-object v0, p1

    check-cast v0, Lcom/facebook/flatbuffers/MutableFlattenable;

    move-object v1, v0

    iget-object v2, p0, LX/4VN;->f:LX/4Ud;

    iget-object v4, p0, LX/4VN;->d:LX/0t8;

    iget-object v5, p0, LX/4VN;->a:Ljava/util/Map;

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, LX/4VC;->a(Lcom/facebook/flatbuffers/MutableFlattenable;LX/4Ud;[[ILX/0t8;Ljava/util/Map;Z)Lcom/facebook/flatbuffers/MutableFlattenable;

    move-result-object v1

    check-cast v1, LX/0jT;

    .line 742296
    :goto_0
    iget-object v2, p0, LX/4VN;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x31001f    # 4.499983E-39f

    const/4 v4, 0x2

    invoke-interface {v2, v3, v7, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 742297
    return-object v1

    .line 742298
    :cond_0
    new-instance v1, LX/3d2;

    const-class v2, LX/16i;

    new-instance v3, LX/3d3;

    iget-object v4, p0, LX/4VN;->a:Ljava/util/Map;

    invoke-direct {v3, v4}, LX/3d3;-><init>(Ljava/util/Map;)V

    invoke-direct {v1, v2, v3}, LX/3d2;-><init>(Ljava/lang/Class;LX/3d4;)V

    .line 742299
    invoke-virtual {v1, p1}, LX/3d2;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jT;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 742300
    :catch_0
    move-exception v1

    .line 742301
    iget-object v2, p0, LX/4VN;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v3, 0x3

    invoke-interface {v2, v8, v7, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 742302
    invoke-static {v1}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v1

    throw v1
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 742303
    invoke-virtual {p0}, LX/4VN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 742304
    :goto_0
    return-void

    .line 742305
    :cond_0
    iget-object v0, p0, LX/4VN;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0si;

    iget-object v1, p0, LX/4VN;->a:Ljava/util/Map;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0si;->a(Ljava/util/Map;)V

    goto :goto_0
.end method

.method public final c(LX/0jT;)LX/0jT;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;)TT;"
        }
    .end annotation

    .prologue
    .line 742306
    instance-of v0, p1, LX/16i;

    if-eqz v0, :cond_0

    .line 742307
    new-instance v0, LX/3d3;

    iget-object v1, p0, LX/4VN;->a:Ljava/util/Map;

    invoke-direct {v0, v1}, LX/3d3;-><init>(Ljava/util/Map;)V

    .line 742308
    check-cast p1, LX/16i;

    invoke-virtual {v0, p1}, LX/3d3;->a(LX/16i;)LX/16i;

    move-result-object p1

    .line 742309
    :cond_0
    return-object p1
.end method

.method public final c()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 742310
    iget-object v0, p0, LX/4VN;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/3Bq;
    .locals 1

    .prologue
    .line 742311
    iget-object v0, p0, LX/4VN;->i:LX/3Bp;

    return-object v0
.end method

.method public final d(LX/0jT;)Z
    .locals 3

    .prologue
    .line 742312
    instance-of v0, p1, Lcom/facebook/graphql/modelutil/FragmentModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/4VN;->e:LX/0sT;

    invoke-virtual {v0}, LX/0sT;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 742313
    check-cast p1, Lcom/facebook/graphql/modelutil/FragmentModel;

    iget-object v0, p0, LX/4VN;->f:LX/4Ud;

    iget-object v1, p0, LX/4VN;->d:LX/0t8;

    iget-object v2, p0, LX/4VN;->a:Ljava/util/Map;

    invoke-static {p1, v0, v1, v2}, LX/4VC;->a(Lcom/facebook/graphql/modelutil/FragmentModel;LX/4Ud;LX/0t8;Ljava/util/Map;)Z

    move-result v0

    .line 742314
    :goto_0
    return v0

    .line 742315
    :cond_0
    new-instance v0, LX/2lm;

    iget-object v1, p0, LX/4VN;->a:Ljava/util/Map;

    invoke-direct {v0, v1}, LX/2lm;-><init>(Ljava/util/Map;)V

    .line 742316
    invoke-virtual {v0, p1}, LX/1jx;->b(LX/0jT;)LX/0jT;

    .line 742317
    iget-boolean v1, v0, LX/2lm;->c:Z

    move v0, v1

    .line 742318
    goto :goto_0
.end method
