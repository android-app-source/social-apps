.class public final LX/4XS;
.super LX/40T;
.source ""

# interfaces
.implements LX/40U;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/40T;",
        "LX/40U",
        "<",
        "Lcom/facebook/graphql/model/GraphQLNode;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(LX/4VK;)V
    .locals 0

    .prologue
    .line 769316
    invoke-direct {p0, p1}, LX/40T;-><init>(LX/4VK;)V

    .line 769317
    return-void
.end method


# virtual methods
.method public final a(LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPage;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 769314
    iget-object v0, p0, LX/40T;->a:LX/4VK;

    const-string v1, "confirmed_places_for_attachment"

    invoke-virtual {v0, v1, p1}, LX/4VK;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 769315
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLPlaceListItemsFromPlaceListConnection;)V
    .locals 2

    .prologue
    .line 769312
    iget-object v0, p0, LX/40T;->a:LX/4VK;

    const-string v1, "list_items"

    invoke-virtual {v0, v1, p1}, LX/4VK;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 769313
    return-void
.end method

.method public final c(LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 769318
    iget-object v0, p0, LX/40T;->a:LX/4VK;

    const-string v1, "lightweight_recs"

    invoke-virtual {v0, v1, p1}, LX/4VK;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 769319
    return-void
.end method

.method public final d(LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 769310
    iget-object v0, p0, LX/40T;->a:LX/4VK;

    const-string v1, "pending_place_slots"

    invoke-virtual {v0, v1, p1}, LX/4VK;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 769311
    return-void
.end method

.method public final e(LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPage;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 769308
    iget-object v0, p0, LX/40T;->a:LX/4VK;

    const-string v1, "pending_places_for_attachment"

    invoke-virtual {v0, v1, p1}, LX/4VK;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 769309
    return-void
.end method
