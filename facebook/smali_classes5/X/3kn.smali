.class public LX/3kn;
.super LX/3ko;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field public final b:LX/3kp;

.field public c:LX/0hs;

.field public d:Landroid/view/View;

.field public e:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/3kp;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 633294
    invoke-direct {p0}, LX/3ko;-><init>()V

    .line 633295
    iput-object p1, p0, LX/3kn;->a:Landroid/content/Context;

    .line 633296
    iput-object p2, p0, LX/3kn;->b:LX/3kp;

    .line 633297
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/10S;
    .locals 1

    .prologue
    .line 633291
    iget-boolean v0, p0, LX/3kn;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3kn;->b:LX/3kp;

    invoke-virtual {v0}, LX/3kp;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 633292
    :cond_0
    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;

    .line 633293
    :goto_0
    return-object v0

    :cond_1
    sget-object v0, LX/10S;->ELIGIBLE:LX/10S;

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 633290
    const-string v0, "3883"

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 633289
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PHOTO_PICKER_DETECTED_RECENT_VIDEO:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 633276
    const/4 v0, 0x0

    iput-object v0, p0, LX/3kn;->d:Landroid/view/View;

    .line 633277
    return-void
.end method

.method public final e()V
    .locals 3

    .prologue
    .line 633278
    new-instance v0, LX/BIA;

    iget-object v1, p0, LX/3kn;->a:Landroid/content/Context;

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, LX/BIA;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, LX/3kn;->c:LX/0hs;

    .line 633279
    iget-object v0, p0, LX/3kn;->c:LX/0hs;

    const/4 v1, -0x1

    .line 633280
    iput v1, v0, LX/0hs;->t:I

    .line 633281
    iget-object v0, p0, LX/3kn;->c:LX/0hs;

    new-instance v1, LX/BI4;

    invoke-direct {v1, p0}, LX/BI4;-><init>(LX/3kn;)V

    invoke-virtual {v0, v1}, LX/0hs;->a(LX/5Od;)V

    .line 633282
    iget-object v0, p0, LX/3kn;->c:LX/0hs;

    new-instance v1, LX/BI5;

    invoke-direct {v1, p0}, LX/BI5;-><init>(LX/3kn;)V

    .line 633283
    iput-object v1, v0, LX/0ht;->H:LX/2dD;

    .line 633284
    iget-object v0, p0, LX/3kn;->c:LX/0hs;

    sget-object v1, LX/3AV;->ABOVE:LX/3AV;

    invoke-virtual {v0, v1}, LX/0ht;->a(LX/3AV;)V

    .line 633285
    iget-object v0, p0, LX/3kn;->c:LX/0hs;

    iget-object v1, p0, LX/3kn;->a:Landroid/content/Context;

    const v2, 0x7f081374

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0hs;->a(Ljava/lang/CharSequence;)V

    .line 633286
    iget-object v0, p0, LX/3kn;->c:LX/0hs;

    iget-object v1, p0, LX/3kn;->a:Landroid/content/Context;

    const v2, 0x7f081376

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0hs;->b(Ljava/lang/CharSequence;)V

    .line 633287
    iget-object v0, p0, LX/3kn;->c:LX/0hs;

    iget-object v1, p0, LX/3kn;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, LX/0ht;->f(Landroid/view/View;)V

    .line 633288
    return-void
.end method
