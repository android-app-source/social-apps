.class public LX/3fM;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/0lF;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0lF;)V
    .locals 4

    .prologue
    .line 621987
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 621988
    invoke-virtual {p1}, LX/0lF;->h()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "data"

    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 621989
    const-string v0, "data"

    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object p1

    .line 621990
    :cond_0
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/3fM;->a:Ljava/util/Map;

    .line 621991
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, LX/0lF;->e()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 621992
    invoke-virtual {p1, v0}, LX/0lF;->a(I)LX/0lF;

    move-result-object v1

    .line 621993
    const-string v2, "name"

    invoke-virtual {v1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v2

    .line 621994
    const-string v3, "fql_result_set"

    invoke-virtual {v1, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    .line 621995
    iget-object v3, p0, LX/3fM;->a:Ljava/util/Map;

    invoke-interface {v3, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 621996
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 621997
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/0lF;
    .locals 1

    .prologue
    .line 621998
    iget-object v0, p0, LX/3fM;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    return-object v0
.end method
