.class public LX/4rL;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/2Ay;

.field public final b:LX/2An;

.field public c:Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;


# direct methods
.method public constructor <init>(LX/2Ay;LX/2An;Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;)V
    .locals 0

    .prologue
    .line 815500
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 815501
    iput-object p2, p0, LX/4rL;->b:LX/2An;

    .line 815502
    iput-object p1, p0, LX/4rL;->a:LX/2Ay;

    .line 815503
    iput-object p3, p0, LX/4rL;->c:Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;

    .line 815504
    return-void
.end method


# virtual methods
.method public final a(LX/0my;)V
    .locals 2

    .prologue
    .line 815492
    iget-object v0, p0, LX/4rL;->c:Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;

    iget-object v1, p0, LX/4rL;->a:LX/2Ay;

    invoke-virtual {v0, p1, v1}, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->a(LX/0my;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    check-cast v0, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;

    iput-object v0, p0, LX/4rL;->c:Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;

    .line 815493
    return-void
.end method

.method public final a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 815494
    iget-object v0, p0, LX/4rL;->b:LX/2An;

    invoke-virtual {v0, p1}, LX/2An;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 815495
    if-nez v0, :cond_0

    .line 815496
    :goto_0
    return-void

    .line 815497
    :cond_0
    instance-of v1, v0, Ljava/util/Map;

    if-nez v1, :cond_1

    .line 815498
    new-instance v1, LX/28E;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Value returned by \'any-getter\' ("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/4rL;->b:LX/2An;

    invoke-virtual {v3}, LX/0lO;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "()) not java.util.Map but "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LX/28E;-><init>(Ljava/lang/String;)V

    throw v1

    .line 815499
    :cond_1
    iget-object v1, p0, LX/4rL;->c:Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;

    check-cast v0, Ljava/util/Map;

    invoke-virtual {v1, v0, p2, p3}, Lcom/fasterxml/jackson/databind/ser/std/MapSerializer;->a(Ljava/util/Map;LX/0nX;LX/0my;)V

    goto :goto_0
.end method
