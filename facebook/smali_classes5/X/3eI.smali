.class public LX/3eI;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/3eI;


# instance fields
.field public final a:LX/0lC;


# direct methods
.method public constructor <init>(LX/0lC;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 619369
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 619370
    iput-object p1, p0, LX/3eI;->a:LX/0lC;

    .line 619371
    return-void
.end method

.method public static a(LX/0QB;)LX/3eI;
    .locals 4

    .prologue
    .line 619356
    sget-object v0, LX/3eI;->b:LX/3eI;

    if-nez v0, :cond_1

    .line 619357
    const-class v1, LX/3eI;

    monitor-enter v1

    .line 619358
    :try_start_0
    sget-object v0, LX/3eI;->b:LX/3eI;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 619359
    if-eqz v2, :cond_0

    .line 619360
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 619361
    new-instance p0, LX/3eI;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v3

    check-cast v3, LX/0lC;

    invoke-direct {p0, v3}, LX/3eI;-><init>(LX/0lC;)V

    .line 619362
    move-object v0, p0

    .line 619363
    sput-object v0, LX/3eI;->b:LX/3eI;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 619364
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 619365
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 619366
    :cond_1
    sget-object v0, LX/3eI;->b:LX/3eI;

    return-object v0

    .line 619367
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 619368
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/0lF;)Lcom/facebook/stickers/model/StickerCapabilities;
    .locals 7

    .prologue
    .line 619336
    const-string v0, "is_comments_capable"

    invoke-virtual {p0, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    sget-object v1, LX/03R;->UNSET:LX/03R;

    invoke-virtual {v1}, LX/03R;->getDbValue()I

    move-result v1

    invoke-static {v0, v1}, LX/16N;->a(LX/0lF;I)I

    move-result v0

    invoke-static {v0}, LX/03R;->fromDbValue(I)LX/03R;

    move-result-object v0

    .line 619337
    const-string v1, "is_composer_capable"

    invoke-virtual {p0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    sget-object v2, LX/03R;->UNSET:LX/03R;

    invoke-virtual {v2}, LX/03R;->getDbValue()I

    move-result v2

    invoke-static {v1, v2}, LX/16N;->a(LX/0lF;I)I

    move-result v1

    invoke-static {v1}, LX/03R;->fromDbValue(I)LX/03R;

    move-result-object v1

    .line 619338
    const-string v2, "is_messenger_capable"

    invoke-virtual {p0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    sget-object v3, LX/03R;->UNSET:LX/03R;

    invoke-virtual {v3}, LX/03R;->getDbValue()I

    move-result v3

    invoke-static {v2, v3}, LX/16N;->a(LX/0lF;I)I

    move-result v2

    invoke-static {v2}, LX/03R;->fromDbValue(I)LX/03R;

    move-result-object v2

    .line 619339
    const-string v3, "is_sms_capable"

    invoke-virtual {p0, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    sget-object v4, LX/03R;->UNSET:LX/03R;

    invoke-virtual {v4}, LX/03R;->getDbValue()I

    move-result v4

    invoke-static {v3, v4}, LX/16N;->a(LX/0lF;I)I

    move-result v3

    invoke-static {v3}, LX/03R;->fromDbValue(I)LX/03R;

    move-result-object v3

    .line 619340
    const-string v4, "is_posts_capable"

    invoke-virtual {p0, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    sget-object v5, LX/03R;->UNSET:LX/03R;

    invoke-virtual {v5}, LX/03R;->getDbValue()I

    move-result v5

    invoke-static {v4, v5}, LX/16N;->a(LX/0lF;I)I

    move-result v4

    invoke-static {v4}, LX/03R;->fromDbValue(I)LX/03R;

    move-result-object v4

    .line 619341
    const-string v5, "is_montage_capable"

    invoke-virtual {p0, v5}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v5

    sget-object v6, LX/03R;->UNSET:LX/03R;

    invoke-virtual {v6}, LX/03R;->getDbValue()I

    move-result v6

    invoke-static {v5, v6}, LX/16N;->a(LX/0lF;I)I

    move-result v5

    invoke-static {v5}, LX/03R;->fromDbValue(I)LX/03R;

    move-result-object v5

    .line 619342
    invoke-static {}, Lcom/facebook/stickers/model/StickerCapabilities;->newBuilder()LX/4m3;

    move-result-object v6

    .line 619343
    iput-object v0, v6, LX/4m3;->a:LX/03R;

    .line 619344
    move-object v0, v6

    .line 619345
    iput-object v1, v0, LX/4m3;->b:LX/03R;

    .line 619346
    move-object v0, v0

    .line 619347
    iput-object v2, v0, LX/4m3;->c:LX/03R;

    .line 619348
    move-object v0, v0

    .line 619349
    iput-object v3, v0, LX/4m3;->d:LX/03R;

    .line 619350
    move-object v0, v0

    .line 619351
    iput-object v4, v0, LX/4m3;->e:LX/03R;

    .line 619352
    move-object v0, v0

    .line 619353
    iput-object v5, v0, LX/4m3;->f:LX/03R;

    .line 619354
    move-object v0, v0

    .line 619355
    invoke-virtual {v0}, LX/4m3;->a()Lcom/facebook/stickers/model/StickerCapabilities;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 619372
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(LX/0lF;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 619334
    invoke-static {p0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    .line 619335
    if-eqz v0, :cond_0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 619325
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 619326
    const/4 v0, 0x0

    .line 619327
    :goto_0
    return-object v0

    .line 619328
    :cond_0
    iget-object v0, p0, LX/3eI;->a:LX/0lC;

    invoke-virtual {v0, p1}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    .line 619329
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 619330
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1}, LX/0lF;->e()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 619331
    invoke-virtual {v1, v0}, LX/0lF;->a(I)LX/0lF;

    move-result-object v3

    invoke-virtual {v3}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 619332
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 619333
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public final c(Ljava/lang/String;)LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 619313
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 619314
    const/4 v0, 0x0

    .line 619315
    :goto_0
    return-object v0

    .line 619316
    :cond_0
    iget-object v0, p0, LX/3eI;->a:LX/0lC;

    invoke-virtual {v0, p1}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 619317
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 619318
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v0}, LX/0lF;->e()I

    move-result p0

    if-ge v1, p0, :cond_1

    .line 619319
    invoke-virtual {v0, v1}, LX/0lF;->a(I)LX/0lF;

    move-result-object p0

    .line 619320
    const-string p1, "id"

    invoke-virtual {p0, p1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object p0

    invoke-static {p0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object p0

    .line 619321
    invoke-virtual {v2, p0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 619322
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 619323
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    move-object v0, v1

    .line 619324
    goto :goto_0
.end method
