.class public LX/4Nb;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 693815
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const-wide/16 v4, 0x0

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 693816
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_8

    .line 693817
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 693818
    :goto_0
    return v1

    .line 693819
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_6

    .line 693820
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 693821
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 693822
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_0

    if-eqz v11, :cond_0

    .line 693823
    const-string v12, "fetchTimeMs"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 693824
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    move v0, v6

    goto :goto_1

    .line 693825
    :cond_1
    const-string v12, "photo_attachments"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 693826
    invoke-static {p0, p1}, LX/2as;->b(LX/15w;LX/186;)I

    move-result v10

    goto :goto_1

    .line 693827
    :cond_2
    const-string v12, "photo_stories"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 693828
    invoke-static {p0, p1}, LX/2aD;->b(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 693829
    :cond_3
    const-string v12, "render_style"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 693830
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 693831
    :cond_4
    const-string v12, "section_header"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 693832
    invoke-static {p0, p1}, LX/4Ni;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 693833
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 693834
    :cond_6
    const/4 v11, 0x5

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 693835
    if-eqz v0, :cond_7

    move-object v0, p1

    .line 693836
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 693837
    :cond_7
    invoke-virtual {p1, v6, v10}, LX/186;->b(II)V

    .line 693838
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 693839
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 693840
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 693841
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_8
    move v0, v1

    move v7, v1

    move v8, v1

    move v9, v1

    move v10, v1

    move-wide v2, v4

    goto/16 :goto_1
.end method

.method public static a(LX/15w;S)LX/15i;
    .locals 5

    .prologue
    .line 693842
    const/4 v0, 0x1

    const/4 v4, 0x0

    .line 693843
    new-instance v2, LX/186;

    const/16 v1, 0x80

    invoke-direct {v2, v1}, LX/186;-><init>(I)V

    .line 693844
    invoke-static {p0, v2}, LX/4Nb;->a(LX/15w;LX/186;)I

    move-result v1

    .line 693845
    if-eqz v0, :cond_0

    .line 693846
    const/4 v3, 0x2

    invoke-virtual {v2, v3}, LX/186;->c(I)V

    .line 693847
    invoke-virtual {v2, v4, p1, v4}, LX/186;->a(ISI)V

    .line 693848
    const/4 v3, 0x1

    invoke-virtual {v2, v3, v1}, LX/186;->b(II)V

    .line 693849
    invoke-virtual {v2}, LX/186;->d()I

    move-result v1

    .line 693850
    :cond_0
    invoke-virtual {v2, v1}, LX/186;->d(I)V

    .line 693851
    invoke-static {v2}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v1

    move-object v0, v1

    .line 693852
    return-object v0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 693853
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 693854
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 693855
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 693856
    const-string v0, "name"

    const-string v1, "GoodwillThrowbackMissedMemoriesStory"

    invoke-virtual {p2, v0, v1}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 693857
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 693858
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 693859
    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    .line 693860
    const-string v2, "fetchTimeMs"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 693861
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 693862
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 693863
    if-eqz v0, :cond_1

    .line 693864
    const-string v1, "photo_attachments"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 693865
    invoke-static {p0, v0, p2, p3}, LX/2as;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 693866
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 693867
    if-eqz v0, :cond_2

    .line 693868
    const-string v1, "photo_stories"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 693869
    invoke-static {p0, v0, p2, p3}, LX/2aD;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 693870
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 693871
    if-eqz v0, :cond_3

    .line 693872
    const-string v1, "render_style"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 693873
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 693874
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 693875
    if-eqz v0, :cond_4

    .line 693876
    const-string v1, "section_header"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 693877
    invoke-static {p0, v0, p2, p3}, LX/4Ni;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 693878
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 693879
    return-void
.end method
