.class public LX/3cF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1TE;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/3cF;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 613383
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 613384
    return-void
.end method

.method public static a(LX/0QB;)LX/3cF;
    .locals 3

    .prologue
    .line 613385
    sget-object v0, LX/3cF;->a:LX/3cF;

    if-nez v0, :cond_1

    .line 613386
    const-class v1, LX/3cF;

    monitor-enter v1

    .line 613387
    :try_start_0
    sget-object v0, LX/3cF;->a:LX/3cF;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 613388
    if-eqz v2, :cond_0

    .line 613389
    :try_start_1
    new-instance v0, LX/3cF;

    invoke-direct {v0}, LX/3cF;-><init>()V

    .line 613390
    move-object v0, v0

    .line 613391
    sput-object v0, LX/3cF;->a:LX/3cF;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 613392
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 613393
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 613394
    :cond_1
    sget-object v0, LX/3cF;->a:LX/3cF;

    return-object v0

    .line 613395
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 613396
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1KB;)V
    .locals 1

    .prologue
    .line 613397
    sget-object v0, Lcom/facebook/topics/sections/cover/TopicCoverPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 613398
    sget-object v0, Lcom/facebook/topics/sections/sources/TopicTopSourcesPartDefinition;->d:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 613399
    return-void
.end method
