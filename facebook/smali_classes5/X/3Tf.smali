.class public abstract LX/3Tf;
.super Landroid/widget/BaseAdapter;
.source ""

# interfaces
.implements Landroid/widget/Filterable;
.implements Landroid/widget/SectionIndexer;
.implements LX/3LJ;


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/3dW;",
            ">;"
        }
    .end annotation
.end field

.field public b:Z

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/database/DataSetObserver;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 584660
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 584661
    return-void
.end method

.method private d()V
    .locals 6

    .prologue
    .line 584631
    iget-boolean v0, p0, LX/3Tf;->b:Z

    if-nez v0, :cond_7

    .line 584632
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 584633
    iget-boolean v0, p0, LX/3Tf;->b:Z

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 584634
    iget-object v0, p0, LX/3Tf;->a:Ljava/util/List;

    if-nez v0, :cond_0

    .line 584635
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/3Tf;->a:Ljava/util/List;

    .line 584636
    :cond_0
    iget-object v0, p0, LX/3Tf;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 584637
    invoke-virtual {p0}, LX/3Tf;->c()I

    move-result v3

    .line 584638
    :goto_1
    if-ge v0, v3, :cond_2

    .line 584639
    iget-object v4, p0, LX/3Tf;->a:Ljava/util/List;

    new-instance v5, LX/3dW;

    invoke-direct {v5, v0}, LX/3dW;-><init>(I)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 584640
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    move v0, v2

    .line 584641
    goto :goto_0

    .line 584642
    :cond_2
    iget-object v0, p0, LX/3Tf;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {p0}, LX/3Tf;->c()I

    move-result v3

    if-lt v0, v3, :cond_3

    move v0, v1

    :goto_2
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 584643
    iget-object v0, p0, LX/3Tf;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 584644
    invoke-virtual {p0}, LX/3Tf;->c()I

    move-result v3

    .line 584645
    :goto_3
    if-le v0, v3, :cond_4

    .line 584646
    iget-object v4, p0, LX/3Tf;->a:Ljava/util/List;

    add-int/lit8 v5, v0, -0x1

    invoke-interface {v4, v5}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 584647
    add-int/lit8 v0, v0, -0x1

    goto :goto_3

    :cond_3
    move v0, v2

    .line 584648
    goto :goto_2

    .line 584649
    :cond_4
    invoke-virtual {p0}, LX/3Tf;->c()I

    move-result v0

    iget-object v3, p0, LX/3Tf;->a:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ne v0, v3, :cond_5

    move v0, v1

    :goto_4
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    move v3, v2

    .line 584650
    :goto_5
    invoke-virtual {p0}, LX/3Tf;->c()I

    move-result v0

    if-ge v3, v0, :cond_6

    .line 584651
    invoke-virtual {p0, v3}, LX/3Tf;->c(I)I

    move-result v4

    .line 584652
    iget-object v0, p0, LX/3Tf;->a:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3dW;

    .line 584653
    iput v2, v0, LX/3dW;->c:I

    .line 584654
    iput v4, v0, LX/3dW;->d:I

    .line 584655
    add-int/lit8 v0, v4, 0x1

    add-int/2addr v2, v0

    .line 584656
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_5

    :cond_5
    move v0, v2

    .line 584657
    goto :goto_4

    .line 584658
    :cond_6
    iput-boolean v1, p0, LX/3Tf;->b:Z

    .line 584659
    :cond_7
    return-void
.end method


# virtual methods
.method public abstract a(I)I
.end method

.method public a()LX/333;
    .locals 1

    .prologue
    .line 584630
    const/4 v0, 0x0

    return-object v0
.end method

.method public abstract a(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end method

.method public abstract a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end method

.method public abstract a(II)Ljava/lang/Object;
.end method

.method public abstract b(I)Ljava/lang/Object;
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 584629
    const/4 v0, 0x0

    return v0
.end method

.method public abstract b(II)Z
.end method

.method public abstract c()I
.end method

.method public abstract c(I)I
.end method

.method public abstract c(II)I
.end method

.method public final d(I)[I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 584620
    invoke-direct {p0}, LX/3Tf;->d()V

    .line 584621
    const/4 v0, 0x2

    new-array v1, v0, [I

    .line 584622
    iget-object v0, p0, LX/3Tf;->a:Ljava/util/List;

    .line 584623
    new-instance v2, LX/3dW;

    const/4 v4, 0x0

    invoke-direct {v2, v4}, LX/3dW;-><init>(I)V

    .line 584624
    iput p1, v2, LX/3dW;->c:I

    .line 584625
    sget-object v4, LX/3dW;->a:Ljava/util/Comparator;

    invoke-static {v0, v2, v4}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;)I

    move-result v2

    move v0, v2

    .line 584626
    aput v0, v1, v3

    .line 584627
    const/4 v2, 0x1

    iget-object v0, p0, LX/3Tf;->a:Ljava/util/List;

    aget v3, v1, v3

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3dW;

    iget v0, v0, LX/3dW;->c:I

    sub-int v0, p1, v0

    add-int/lit8 v0, v0, -0x1

    aput v0, v1, v2

    .line 584628
    return-object v1
.end method

.method public final getCount()I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 584614
    invoke-virtual {p0}, LX/3Tf;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 584615
    :cond_0
    :goto_0
    return v0

    .line 584616
    :cond_1
    invoke-direct {p0}, LX/3Tf;->d()V

    .line 584617
    iget-object v1, p0, LX/3Tf;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 584618
    iget-object v0, p0, LX/3Tf;->a:Ljava/util/List;

    iget-object v1, p0, LX/3Tf;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3dW;

    .line 584619
    iget v1, v0, LX/3dW;->c:I

    iget v0, v0, LX/3dW;->d:I

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getFilter()Landroid/widget/Filter;
    .locals 1

    .prologue
    .line 584559
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 584560
    invoke-virtual {p0, p1}, LX/3Tf;->d(I)[I

    move-result-object v0

    .line 584561
    aget v1, v0, v3

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 584562
    const/4 v0, 0x0

    .line 584563
    :goto_0
    return-object v0

    :cond_0
    const/4 v1, 0x0

    aget v1, v0, v1

    aget v0, v0, v3

    invoke-virtual {p0, v1, v0}, LX/3Tf;->a(II)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 584564
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 584565
    invoke-virtual {p0, p1}, LX/3Tf;->d(I)[I

    move-result-object v0

    .line 584566
    aget v1, v0, v4

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 584567
    aget v0, v0, v3

    invoke-virtual {p0, v0}, LX/3Tf;->a(I)I

    move-result v0

    .line 584568
    :goto_0
    return v0

    :cond_0
    aget v1, v0, v3

    aget v0, v0, v4

    invoke-virtual {p0, v1, v0}, LX/3Tf;->c(II)I

    move-result v0

    goto :goto_0
.end method

.method public final getPositionForSection(I)I
    .locals 1

    .prologue
    .line 584569
    if-gez p1, :cond_1

    .line 584570
    const/4 p1, 0x0

    .line 584571
    :cond_0
    :goto_0
    invoke-virtual {p0, p1}, LX/3Tf;->p_(I)I

    move-result v0

    return v0

    .line 584572
    :cond_1
    invoke-virtual {p0}, LX/3Tf;->c()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 584573
    invoke-virtual {p0}, LX/3Tf;->c()I

    move-result v0

    add-int/lit8 p1, v0, -0x1

    goto :goto_0
.end method

.method public final getSectionForPosition(I)I
    .locals 2

    .prologue
    .line 584574
    if-gez p1, :cond_0

    .line 584575
    const/4 v0, 0x0

    .line 584576
    :goto_0
    return v0

    .line 584577
    :cond_0
    invoke-virtual {p0}, LX/3Tf;->getCount()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 584578
    invoke-virtual {p0}, LX/3Tf;->c()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 584579
    :cond_1
    invoke-virtual {p0, p1}, LX/3Tf;->d(I)[I

    move-result-object v0

    .line 584580
    const/4 v1, 0x1

    aget v0, v0, v1

    goto :goto_0
.end method

.method public final getSections()[Ljava/lang/Object;
    .locals 4

    .prologue
    .line 584581
    invoke-virtual {p0}, LX/3Tf;->c()I

    move-result v1

    .line 584582
    new-array v2, v1, [Ljava/lang/Object;

    .line 584583
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 584584
    invoke-virtual {p0, v0}, LX/3Tf;->b(I)Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v2, v0

    .line 584585
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 584586
    :cond_0
    return-object v2
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p2    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 584587
    invoke-virtual {p0, p1}, LX/3Tf;->d(I)[I

    move-result-object v4

    .line 584588
    aget v0, v4, v2

    const/4 v3, -0x1

    if-ne v0, v3, :cond_0

    .line 584589
    aget v0, v4, v1

    invoke-virtual {p0, v0, p2, p3}, LX/3Tf;->a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 584590
    :goto_0
    return-object v0

    .line 584591
    :cond_0
    iget-object v0, p0, LX/3Tf;->a:Ljava/util/List;

    aget v3, v4, v1

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3dW;

    iget v0, v0, LX/3dW;->d:I

    add-int/lit8 v0, v0, -0x1

    aget v3, v4, v2

    if-ne v0, v3, :cond_1

    move v3, v2

    .line 584592
    :goto_1
    aget v1, v4, v1

    aget v2, v4, v2

    move-object v0, p0

    move-object v4, p2

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, LX/3Tf;->a(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :cond_1
    move v3, v1

    .line 584593
    goto :goto_1
.end method

.method public final isEmpty()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 584594
    invoke-virtual {p0}, LX/3Tf;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 584595
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, LX/3Tf;->getCount()I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isEnabled(I)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 584596
    invoke-virtual {p0, p1}, LX/3Tf;->d(I)[I

    move-result-object v1

    .line 584597
    aget v2, v1, v4

    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    .line 584598
    :goto_0
    return v0

    :cond_0
    aget v0, v1, v0

    aget v1, v1, v4

    invoke-virtual {p0, v0, v1}, LX/3Tf;->b(II)Z

    move-result v0

    goto :goto_0
.end method

.method public notifyDataSetChanged()V
    .locals 2

    .prologue
    .line 584599
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/3Tf;->b:Z

    .line 584600
    invoke-super {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 584601
    iget-object v0, p0, LX/3Tf;->c:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 584602
    iget-object v0, p0, LX/3Tf;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/DataSetObserver;

    .line 584603
    invoke-virtual {v0}, Landroid/database/DataSetObserver;->onChanged()V

    goto :goto_0

    .line 584604
    :cond_0
    return-void
.end method

.method public final p_(I)I
    .locals 1

    .prologue
    .line 584605
    invoke-direct {p0}, LX/3Tf;->d()V

    .line 584606
    iget-object v0, p0, LX/3Tf;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3dW;

    iget v0, v0, LX/3dW;->c:I

    return v0
.end method

.method public final registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1

    .prologue
    .line 584607
    iget-object v0, p0, LX/3Tf;->c:Ljava/util/List;

    if-nez v0, :cond_0

    .line 584608
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, LX/3Tf;->c:Ljava/util/List;

    .line 584609
    :cond_0
    iget-object v0, p0, LX/3Tf;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 584610
    return-void
.end method

.method public final unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1

    .prologue
    .line 584611
    iget-object v0, p0, LX/3Tf;->c:Ljava/util/List;

    if-nez v0, :cond_0

    .line 584612
    :goto_0
    return-void

    .line 584613
    :cond_0
    iget-object v0, p0, LX/3Tf;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method
