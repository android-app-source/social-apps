.class public LX/4At;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final context:Landroid/content/Context;

.field private progressDialog:LX/4BY;

.field private final progressText:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 677104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 677105
    iput-object p1, p0, LX/4At;->context:Landroid/content/Context;

    .line 677106
    invoke-virtual {p1, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/4At;->progressText:Ljava/lang/String;

    .line 677107
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 677108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 677109
    iput-object p1, p0, LX/4At;->context:Landroid/content/Context;

    .line 677110
    iput-object p2, p0, LX/4At;->progressText:Ljava/lang/String;

    .line 677111
    return-void
.end method


# virtual methods
.method public beginShowingProgress()V
    .locals 2

    .prologue
    .line 677112
    iget-object v0, p0, LX/4At;->progressDialog:LX/4BY;

    if-nez v0, :cond_0

    .line 677113
    new-instance v0, LX/4BY;

    iget-object v1, p0, LX/4At;->context:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/4BY;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/4At;->progressDialog:LX/4BY;

    .line 677114
    iget-object v0, p0, LX/4At;->progressDialog:LX/4BY;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/4BY;->setCancelable(Z)V

    .line 677115
    iget-object v0, p0, LX/4At;->progressDialog:LX/4BY;

    iget-object v1, p0, LX/4At;->progressText:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/2EJ;->a(Ljava/lang/CharSequence;)V

    .line 677116
    iget-object v0, p0, LX/4At;->progressDialog:LX/4BY;

    invoke-static {v0}, LX/4md;->a(Landroid/app/Dialog;)V

    .line 677117
    iget-object v0, p0, LX/4At;->progressDialog:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->show()V

    .line 677118
    :cond_0
    return-void
.end method

.method public stopShowingProgress()V
    .locals 1

    .prologue
    .line 677119
    iget-object v0, p0, LX/4At;->progressDialog:LX/4BY;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/4At;->progressDialog:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 677120
    iget-object v0, p0, LX/4At;->progressDialog:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->dismiss()V

    .line 677121
    const/4 v0, 0x0

    iput-object v0, p0, LX/4At;->progressDialog:LX/4BY;

    .line 677122
    :cond_0
    return-void
.end method
