.class public LX/4UT;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 722950
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    .line 722951
    const/4 v8, 0x0

    .line 722952
    const/4 v5, 0x0

    .line 722953
    const-wide/16 v6, 0x0

    .line 722954
    const/4 v4, 0x0

    .line 722955
    const/4 v3, 0x0

    .line 722956
    const/4 v2, 0x0

    .line 722957
    const/4 v1, 0x0

    .line 722958
    const/4 v0, 0x0

    .line 722959
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->START_OBJECT:LX/15z;

    if-eq v9, v10, :cond_b

    .line 722960
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 722961
    const/4 v0, 0x0

    .line 722962
    :goto_0
    return v0

    .line 722963
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v1, v10, :cond_9

    .line 722964
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v1

    .line 722965
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 722966
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_0

    if-eqz v1, :cond_0

    .line 722967
    const-string v10, "cache_id"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 722968
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    move v5, v1

    goto :goto_1

    .line 722969
    :cond_1
    const-string v10, "debug_info"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 722970
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    move v4, v1

    goto :goto_1

    .line 722971
    :cond_2
    const-string v10, "fetchTimeMs"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 722972
    const/4 v0, 0x1

    .line 722973
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    goto :goto_1

    .line 722974
    :cond_3
    const-string v10, "short_term_cache_key"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 722975
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    move v9, v1

    goto :goto_1

    .line 722976
    :cond_4
    const-string v10, "title"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 722977
    invoke-static {p0, p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v1

    move v8, v1

    goto :goto_1

    .line 722978
    :cond_5
    const-string v10, "tracking"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 722979
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    move v7, v1

    goto :goto_1

    .line 722980
    :cond_6
    const-string v10, "trending_stories"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 722981
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 722982
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v10, LX/15z;->START_ARRAY:LX/15z;

    if-ne v6, v10, :cond_7

    .line 722983
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v10, LX/15z;->END_ARRAY:LX/15z;

    if-eq v6, v10, :cond_7

    .line 722984
    invoke-static {p0, p1}, LX/4UU;->b(LX/15w;LX/186;)I

    move-result v6

    .line 722985
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 722986
    :cond_7
    invoke-static {v1, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v1

    move v1, v1

    .line 722987
    move v6, v1

    goto/16 :goto_1

    .line 722988
    :cond_8
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 722989
    :cond_9
    const/4 v1, 0x7

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 722990
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 722991
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 722992
    if-eqz v0, :cond_a

    .line 722993
    const/4 v1, 0x2

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 722994
    :cond_a
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 722995
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 722996
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 722997
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 722998
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :cond_b
    move v9, v4

    move v4, v5

    move v5, v8

    move v8, v3

    move-wide v12, v6

    move v7, v2

    move v6, v1

    move-wide v2, v12

    goto/16 :goto_1
.end method

.method public static a(LX/15w;S)LX/15i;
    .locals 5

    .prologue
    .line 722999
    const/4 v0, 0x1

    const/4 v4, 0x0

    .line 723000
    new-instance v2, LX/186;

    const/16 v1, 0x80

    invoke-direct {v2, v1}, LX/186;-><init>(I)V

    .line 723001
    invoke-static {p0, v2}, LX/4UT;->a(LX/15w;LX/186;)I

    move-result v1

    .line 723002
    if-eqz v0, :cond_0

    .line 723003
    const/4 v3, 0x2

    invoke-virtual {v2, v3}, LX/186;->c(I)V

    .line 723004
    invoke-virtual {v2, v4, p1, v4}, LX/186;->a(ISI)V

    .line 723005
    const/4 v3, 0x1

    invoke-virtual {v2, v3, v1}, LX/186;->b(II)V

    .line 723006
    invoke-virtual {v2}, LX/186;->d()I

    move-result v1

    .line 723007
    :cond_0
    invoke-virtual {v2, v1}, LX/186;->d(I)V

    .line 723008
    invoke-static {v2}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v1

    move-object v0, v1

    .line 723009
    return-object v0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 723010
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 723011
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 723012
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 723013
    const-string v0, "name"

    const-string v1, "WorkCommunityTrendingFeedUnit"

    invoke-virtual {p2, v0, v1}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 723014
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 723015
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 723016
    if-eqz v0, :cond_0

    .line 723017
    const-string v1, "cache_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 723018
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 723019
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 723020
    if-eqz v0, :cond_1

    .line 723021
    const-string v1, "debug_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 723022
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 723023
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 723024
    cmp-long v2, v0, v2

    if-eqz v2, :cond_2

    .line 723025
    const-string v2, "fetchTimeMs"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 723026
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 723027
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 723028
    if-eqz v0, :cond_3

    .line 723029
    const-string v1, "short_term_cache_key"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 723030
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 723031
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 723032
    if-eqz v0, :cond_4

    .line 723033
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 723034
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 723035
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 723036
    if-eqz v0, :cond_5

    .line 723037
    const-string v1, "tracking"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 723038
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 723039
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 723040
    if-eqz v0, :cond_7

    .line 723041
    const-string v1, "trending_stories"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 723042
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 723043
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_6

    .line 723044
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/4UU;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 723045
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 723046
    :cond_6
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 723047
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 723048
    return-void
.end method
