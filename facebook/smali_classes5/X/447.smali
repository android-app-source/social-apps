.class public final enum LX/447;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/447;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/447;

.field public static final enum FRAGMENT_CHROME_ACTIVITY:LX/447;

.field public static final enum NONE:LX/447;

.field public static final enum REACT_FRAGMENT_ACTIVITY:LX/447;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 669451
    new-instance v0, LX/447;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, LX/447;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/447;->NONE:LX/447;

    .line 669452
    new-instance v0, LX/447;

    const-string v1, "FRAGMENT_CHROME_ACTIVITY"

    invoke-direct {v0, v1, v3}, LX/447;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/447;->FRAGMENT_CHROME_ACTIVITY:LX/447;

    .line 669453
    new-instance v0, LX/447;

    const-string v1, "REACT_FRAGMENT_ACTIVITY"

    invoke-direct {v0, v1, v4}, LX/447;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/447;->REACT_FRAGMENT_ACTIVITY:LX/447;

    .line 669454
    const/4 v0, 0x3

    new-array v0, v0, [LX/447;

    sget-object v1, LX/447;->NONE:LX/447;

    aput-object v1, v0, v2

    sget-object v1, LX/447;->FRAGMENT_CHROME_ACTIVITY:LX/447;

    aput-object v1, v0, v3

    sget-object v1, LX/447;->REACT_FRAGMENT_ACTIVITY:LX/447;

    aput-object v1, v0, v4

    sput-object v0, LX/447;->$VALUES:[LX/447;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 669456
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/447;
    .locals 1

    .prologue
    .line 669457
    const-class v0, LX/447;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/447;

    return-object v0
.end method

.method public static values()[LX/447;
    .locals 1

    .prologue
    .line 669455
    sget-object v0, LX/447;->$VALUES:[LX/447;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/447;

    return-object v0
.end method
