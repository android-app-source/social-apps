.class public LX/4RI;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 708457
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 17

    .prologue
    .line 708458
    const/4 v13, 0x0

    .line 708459
    const/4 v12, 0x0

    .line 708460
    const/4 v11, 0x0

    .line 708461
    const/4 v10, 0x0

    .line 708462
    const/4 v9, 0x0

    .line 708463
    const/4 v8, 0x0

    .line 708464
    const/4 v7, 0x0

    .line 708465
    const/4 v6, 0x0

    .line 708466
    const/4 v5, 0x0

    .line 708467
    const/4 v4, 0x0

    .line 708468
    const/4 v3, 0x0

    .line 708469
    const/4 v2, 0x0

    .line 708470
    const/4 v1, 0x0

    .line 708471
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->START_OBJECT:LX/15z;

    if-eq v14, v15, :cond_1

    .line 708472
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 708473
    const/4 v1, 0x0

    .line 708474
    :goto_0
    return v1

    .line 708475
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 708476
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->END_OBJECT:LX/15z;

    if-eq v14, v15, :cond_b

    .line 708477
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v14

    .line 708478
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 708479
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v15

    sget-object v16, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v16

    if-eq v15, v0, :cond_1

    if-eqz v14, :cond_1

    .line 708480
    const-string v15, "count"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_2

    .line 708481
    const/4 v3, 0x1

    .line 708482
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v13

    goto :goto_1

    .line 708483
    :cond_2
    const-string v15, "id"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 708484
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    goto :goto_1

    .line 708485
    :cond_3
    const-string v15, "more_posts_query"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_4

    .line 708486
    invoke-static/range {p0 .. p1}, LX/4Np;->a(LX/15w;LX/186;)I

    move-result v11

    goto :goto_1

    .line 708487
    :cond_4
    const-string v15, "phrase"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_5

    .line 708488
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto :goto_1

    .line 708489
    :cond_5
    const-string v15, "phrase_length"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_6

    .line 708490
    const/4 v2, 0x1

    .line 708491
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v9

    goto :goto_1

    .line 708492
    :cond_6
    const-string v15, "phrase_offset"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_7

    .line 708493
    const/4 v1, 0x1

    .line 708494
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v8

    goto :goto_1

    .line 708495
    :cond_7
    const-string v15, "phrase_owner"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_8

    .line 708496
    invoke-static/range {p0 .. p1}, LX/2aw;->a(LX/15w;LX/186;)I

    move-result v7

    goto/16 :goto_1

    .line 708497
    :cond_8
    const-string v15, "sample_text"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_9

    .line 708498
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_1

    .line 708499
    :cond_9
    const-string v15, "sentence"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_a

    .line 708500
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto/16 :goto_1

    .line 708501
    :cond_a
    const-string v15, "url"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_0

    .line 708502
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto/16 :goto_1

    .line 708503
    :cond_b
    const/16 v14, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->c(I)V

    .line 708504
    if-eqz v3, :cond_c

    .line 708505
    const/4 v3, 0x1

    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v13, v14}, LX/186;->a(III)V

    .line 708506
    :cond_c
    const/4 v3, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v12}, LX/186;->b(II)V

    .line 708507
    const/4 v3, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, LX/186;->b(II)V

    .line 708508
    const/4 v3, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10}, LX/186;->b(II)V

    .line 708509
    if-eqz v2, :cond_d

    .line 708510
    const/4 v2, 0x5

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9, v3}, LX/186;->a(III)V

    .line 708511
    :cond_d
    if-eqz v1, :cond_e

    .line 708512
    const/4 v1, 0x6

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v8, v2}, LX/186;->a(III)V

    .line 708513
    :cond_e
    const/4 v1, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 708514
    const/16 v1, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 708515
    const/16 v1, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 708516
    const/16 v1, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 708517
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 708414
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 708415
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 708416
    if-eqz v0, :cond_0

    .line 708417
    const-string v1, "count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 708418
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 708419
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 708420
    if-eqz v0, :cond_1

    .line 708421
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 708422
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 708423
    :cond_1
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 708424
    if-eqz v0, :cond_2

    .line 708425
    const-string v1, "more_posts_query"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 708426
    invoke-static {p0, v0, p2, p3}, LX/4Np;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 708427
    :cond_2
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 708428
    if-eqz v0, :cond_3

    .line 708429
    const-string v1, "phrase"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 708430
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 708431
    :cond_3
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 708432
    if-eqz v0, :cond_4

    .line 708433
    const-string v1, "phrase_length"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 708434
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 708435
    :cond_4
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 708436
    if-eqz v0, :cond_5

    .line 708437
    const-string v1, "phrase_offset"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 708438
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 708439
    :cond_5
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 708440
    if-eqz v0, :cond_6

    .line 708441
    const-string v1, "phrase_owner"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 708442
    invoke-static {p0, v0, p2, p3}, LX/2aw;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 708443
    :cond_6
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 708444
    if-eqz v0, :cond_7

    .line 708445
    const-string v1, "sample_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 708446
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 708447
    :cond_7
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 708448
    if-eqz v0, :cond_8

    .line 708449
    const-string v1, "sentence"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 708450
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 708451
    :cond_8
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 708452
    if-eqz v0, :cond_9

    .line 708453
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 708454
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 708455
    :cond_9
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 708456
    return-void
.end method
