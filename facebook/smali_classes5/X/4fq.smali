.class public LX/4fq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/0Or",
        "<TT;>;"
    }
.end annotation


# static fields
.field private static final a:LX/4fW;


# instance fields
.field private final b:LX/0RI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0RI",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 798731
    sget-object v0, LX/4fW;->a:LX/4fW;

    sput-object v0, LX/4fq;->a:LX/4fW;

    return-void
.end method

.method public constructor <init>(LX/0RI;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0RI",
            "<TT;>;",
            "LX/0Or",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 798732
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 798733
    iput-object p1, p0, LX/4fq;->b:LX/0RI;

    .line 798734
    iput-object p2, p0, LX/4fq;->c:LX/0Or;

    .line 798735
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 798736
    sget-object v0, LX/4fo;->INSTANCE_GET:LX/4fo;

    iget-object v1, p0, LX/4fq;->b:LX/0RI;

    invoke-static {v0, v1}, LX/4fp;->a(LX/4fo;LX/0RI;)V

    .line 798737
    const/4 v0, 0x2

    :try_start_0
    invoke-static {v0}, LX/01m;->b(I)Z

    move-result v1

    .line 798738
    if-eqz v1, :cond_0

    .line 798739
    const-string v0, "Provider.get %s"

    iget-object v2, p0, LX/4fq;->b:LX/0RI;

    const v3, 0x4401b4ec

    invoke-static {v0, v2, v3}, LX/02m;->a(Ljava/lang/String;Ljava/lang/Object;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 798740
    :cond_0
    :try_start_1
    sget-object v0, LX/4fq;->a:LX/4fW;

    iget-object v2, p0, LX/4fq;->b:LX/0RI;

    .line 798741
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v7

    .line 798742
    invoke-static {v0}, LX/4fW;->e(LX/4fW;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 798743
    const/4 v5, 0x0

    .line 798744
    :goto_0
    move-object v2, v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 798745
    const/4 v3, 0x0

    .line 798746
    :try_start_2
    iget-object v0, p0, LX/4fq;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    .line 798747
    :try_start_3
    sget-object v3, LX/4fq;->a:LX/4fW;

    invoke-virtual {v3, v2, v0}, LX/4fW;->a(LX/4fU;Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 798748
    if-eqz v1, :cond_1

    .line 798749
    const-wide/16 v2, 0xa

    const v1, -0x1a9541b7

    :try_start_4
    invoke-static {v2, v3, v1}, LX/02m;->a(JI)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 798750
    :cond_1
    invoke-static {}, LX/4fp;->a()V

    return-object v0

    .line 798751
    :catchall_0
    move-exception v0

    :try_start_5
    sget-object v4, LX/4fq;->a:LX/4fW;

    invoke-virtual {v4, v2, v3}, LX/4fW;->a(LX/4fU;Ljava/lang/Object;)V

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 798752
    :catchall_1
    move-exception v0

    if-eqz v1, :cond_2

    .line 798753
    const-wide/16 v2, 0xa

    const v1, 0x37e84230

    :try_start_6
    invoke-static {v2, v3, v1}, LX/02m;->a(JI)V

    :cond_2
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 798754
    :catchall_2
    move-exception v0

    invoke-static {}, LX/4fp;->a()V

    throw v0

    .line 798755
    :cond_3
    iget-object v5, v0, LX/4fW;->h:LX/45T;

    if-nez v5, :cond_4

    .line 798756
    new-instance v5, LX/45T;

    invoke-direct {v5}, LX/45T;-><init>()V

    iput-object v5, v0, LX/4fW;->h:LX/45T;

    .line 798757
    :cond_4
    new-instance v5, LX/4fU;

    iget-object v6, v0, LX/4fW;->h:LX/45T;

    invoke-direct {v5, v2, v6}, LX/4fU;-><init>(LX/0RI;LX/45T;)V

    .line 798758
    invoke-static {v0, v5}, LX/4fW;->a(LX/4fW;LX/4fU;)V

    .line 798759
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v9

    sub-long v7, v9, v7

    .line 798760
    iget-object v11, v5, LX/4fU;->e:Ljava/lang/Long;

    if-eqz v11, :cond_5

    .line 798761
    sget-object v11, LX/4fW;->c:Ljava/lang/Class;

    const-string v12, "Already started provider call."

    invoke-static {v11, v12}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 798762
    :goto_1
    goto :goto_0

    .line 798763
    :cond_5
    iget-wide v11, v5, LX/4fU;->g:J

    add-long/2addr v11, v7

    iput-wide v11, v5, LX/4fU;->g:J

    .line 798764
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v11

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    iput-object v11, v5, LX/4fU;->e:Ljava/lang/Long;

    goto :goto_1
.end method
