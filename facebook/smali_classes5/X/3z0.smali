.class public final LX/3z0;
.super LX/1Mt;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/abtest/qe/settings/QuickExperimentSyncDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/abtest/qe/settings/QuickExperimentSyncDialogFragment;)V
    .locals 0

    .prologue
    .line 661970
    iput-object p1, p0, LX/3z0;->a:Lcom/facebook/abtest/qe/settings/QuickExperimentSyncDialogFragment;

    invoke-direct {p0}, LX/1Mt;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 661964
    sget-object v0, Lcom/facebook/abtest/qe/settings/QuickExperimentSyncDialogFragment;->p:Ljava/lang/Class;

    const-string v1, "Failed to fetch sessionless QEs"

    invoke-static {v0, v1, p1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 661965
    iget-object v0, p0, LX/3z0;->a:Lcom/facebook/abtest/qe/settings/QuickExperimentSyncDialogFragment;

    iget-object v0, v0, Lcom/facebook/abtest/qe/settings/QuickExperimentSyncDialogFragment;->o:LX/0kL;

    new-instance v1, LX/27k;

    const-string v2, "Failed to fetch Sessionless QEs"

    invoke-direct {v1, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 661966
    iget-object v0, p0, LX/3z0;->a:Lcom/facebook/abtest/qe/settings/QuickExperimentSyncDialogFragment;

    const/4 v1, 0x1

    .line 661967
    iput-boolean v1, v0, Lcom/facebook/abtest/qe/settings/QuickExperimentSyncDialogFragment;->s:Z

    .line 661968
    iget-object v0, p0, LX/3z0;->a:Lcom/facebook/abtest/qe/settings/QuickExperimentSyncDialogFragment;

    invoke-static {v0}, Lcom/facebook/abtest/qe/settings/QuickExperimentSyncDialogFragment;->l(Lcom/facebook/abtest/qe/settings/QuickExperimentSyncDialogFragment;)V

    .line 661969
    return-void
.end method

.method public final onSuccessfulResult(Lcom/facebook/fbservice/service/OperationResult;)V
    .locals 2

    .prologue
    .line 661959
    iget-object v0, p0, LX/3z0;->a:Lcom/facebook/abtest/qe/settings/QuickExperimentSyncDialogFragment;

    const/4 v1, 0x1

    .line 661960
    iput-boolean v1, v0, Lcom/facebook/abtest/qe/settings/QuickExperimentSyncDialogFragment;->s:Z

    .line 661961
    iget-object v0, p0, LX/3z0;->a:Lcom/facebook/abtest/qe/settings/QuickExperimentSyncDialogFragment;

    invoke-static {v0}, Lcom/facebook/abtest/qe/settings/QuickExperimentSyncDialogFragment;->l(Lcom/facebook/abtest/qe/settings/QuickExperimentSyncDialogFragment;)V

    .line 661962
    return-void
.end method

.method public final bridge synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 661963
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    invoke-virtual {p0, p1}, LX/3z0;->onSuccessfulResult(Lcom/facebook/fbservice/service/OperationResult;)V

    return-void
.end method
