.class public LX/4AS;
.super Landroid/graphics/drawable/Drawable;
.source ""


# instance fields
.field private a:Ljava/lang/String;

.field public b:I

.field public c:I

.field public d:I

.field private e:Ljava/lang/String;

.field private f:I

.field private g:I

.field public h:I

.field public final i:Landroid/graphics/Paint;

.field public j:I

.field public k:I

.field public l:I

.field private m:I

.field private n:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 676246
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 676247
    const/16 v0, 0x50

    iput v0, p0, LX/4AS;->h:I

    .line 676248
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, LX/4AS;->i:Landroid/graphics/Paint;

    .line 676249
    invoke-virtual {p0}, LX/4AS;->a()V

    .line 676250
    return-void
.end method

.method private varargs a(Landroid/graphics/Canvas;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 4
    .param p3    # [Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 676241
    if-nez p3, :cond_0

    .line 676242
    iget v0, p0, LX/4AS;->m:I

    int-to-float v0, v0

    iget v1, p0, LX/4AS;->n:I

    int-to-float v1, v1

    iget-object v2, p0, LX/4AS;->i:Landroid/graphics/Paint;

    invoke-virtual {p1, p2, v0, v1, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 676243
    :goto_0
    iget v0, p0, LX/4AS;->n:I

    iget v1, p0, LX/4AS;->l:I

    add-int/2addr v0, v1

    iput v0, p0, LX/4AS;->n:I

    .line 676244
    return-void

    .line 676245
    :cond_0
    invoke-static {p2, p3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget v1, p0, LX/4AS;->m:I

    int-to-float v1, v1

    iget v2, p0, LX/4AS;->n:I

    int-to-float v2, v2

    iget-object v3, p0, LX/4AS;->i:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method private b(II)I
    .locals 8
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .prologue
    const v0, 0x66f44336

    const/high16 v7, 0x3f000000    # 0.5f

    const v6, 0x3dcccccd    # 0.1f

    .line 676227
    invoke-virtual {p0}, LX/4AS;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    .line 676228
    invoke-virtual {p0}, LX/4AS;->getBounds()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    .line 676229
    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 676230
    :cond_0
    :goto_0
    return v0

    .line 676231
    :cond_1
    int-to-float v3, v1

    mul-float/2addr v3, v6

    .line 676232
    int-to-float v4, v1

    mul-float/2addr v4, v7

    .line 676233
    int-to-float v5, v2

    mul-float/2addr v5, v6

    .line 676234
    int-to-float v6, v2

    mul-float/2addr v6, v7

    .line 676235
    sub-int v1, p1, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 676236
    sub-int v2, p2, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    .line 676237
    int-to-float v7, v1

    cmpg-float v3, v7, v3

    if-gez v3, :cond_2

    int-to-float v3, v2

    cmpg-float v3, v3, v5

    if-gez v3, :cond_2

    .line 676238
    const v0, 0x664caf50

    goto :goto_0

    .line 676239
    :cond_2
    int-to-float v1, v1

    cmpg-float v1, v1, v4

    if-gez v1, :cond_0

    int-to-float v1, v2

    cmpg-float v1, v1, v6

    if-gez v1, :cond_0

    .line 676240
    const v0, 0x66ff9800

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 676218
    iput v0, p0, LX/4AS;->b:I

    .line 676219
    iput v0, p0, LX/4AS;->c:I

    .line 676220
    iput v0, p0, LX/4AS;->d:I

    .line 676221
    iput v0, p0, LX/4AS;->f:I

    .line 676222
    iput v0, p0, LX/4AS;->g:I

    .line 676223
    iput-object v1, p0, LX/4AS;->e:Ljava/lang/String;

    .line 676224
    invoke-virtual {p0, v1}, LX/4AS;->a(Ljava/lang/String;)V

    .line 676225
    invoke-virtual {p0}, LX/4AS;->invalidateSelf()V

    .line 676226
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 676214
    if-eqz p1, :cond_0

    :goto_0
    iput-object p1, p0, LX/4AS;->a:Ljava/lang/String;

    .line 676215
    invoke-virtual {p0}, LX/4AS;->invalidateSelf()V

    .line 676216
    return-void

    .line 676217
    :cond_0
    const-string p1, "none"

    goto :goto_0
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 676192
    invoke-virtual {p0}, LX/4AS;->getBounds()Landroid/graphics/Rect;

    move-result-object v6

    .line 676193
    iget-object v0, p0, LX/4AS;->i:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 676194
    iget-object v0, p0, LX/4AS;->i:Landroid/graphics/Paint;

    const/high16 v1, 0x40000000    # 2.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 676195
    iget-object v0, p0, LX/4AS;->i:Landroid/graphics/Paint;

    const/16 v1, -0x6800

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 676196
    iget v0, v6, Landroid/graphics/Rect;->left:I

    int-to-float v1, v0

    iget v0, v6, Landroid/graphics/Rect;->top:I

    int-to-float v2, v0

    iget v0, v6, Landroid/graphics/Rect;->right:I

    int-to-float v3, v0

    iget v0, v6, Landroid/graphics/Rect;->bottom:I

    int-to-float v4, v0

    iget-object v5, p0, LX/4AS;->i:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 676197
    iget-object v0, p0, LX/4AS;->i:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 676198
    iget-object v0, p0, LX/4AS;->i:Landroid/graphics/Paint;

    iget v1, p0, LX/4AS;->b:I

    iget v2, p0, LX/4AS;->c:I

    invoke-direct {p0, v1, v2}, LX/4AS;->b(II)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 676199
    iget v0, v6, Landroid/graphics/Rect;->left:I

    int-to-float v1, v0

    iget v0, v6, Landroid/graphics/Rect;->top:I

    int-to-float v2, v0

    iget v0, v6, Landroid/graphics/Rect;->right:I

    int-to-float v3, v0

    iget v0, v6, Landroid/graphics/Rect;->bottom:I

    int-to-float v4, v0

    iget-object v5, p0, LX/4AS;->i:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 676200
    iget-object v0, p0, LX/4AS;->i:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 676201
    iget-object v0, p0, LX/4AS;->i:Landroid/graphics/Paint;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 676202
    iget-object v0, p0, LX/4AS;->i:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 676203
    iget v0, p0, LX/4AS;->j:I

    iput v0, p0, LX/4AS;->m:I

    .line 676204
    iget v0, p0, LX/4AS;->k:I

    iput v0, p0, LX/4AS;->n:I

    .line 676205
    const-string v0, "ID: %s"

    new-array v1, v8, [Ljava/lang/Object;

    iget-object v2, p0, LX/4AS;->a:Ljava/lang/String;

    aput-object v2, v1, v7

    invoke-direct {p0, p1, v0, v1}, LX/4AS;->a(Landroid/graphics/Canvas;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 676206
    const-string v0, "D: %dx%d"

    new-array v1, v9, [Ljava/lang/Object;

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v7

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v8

    invoke-direct {p0, p1, v0, v1}, LX/4AS;->a(Landroid/graphics/Canvas;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 676207
    const-string v0, "I: %dx%d"

    new-array v1, v9, [Ljava/lang/Object;

    iget v2, p0, LX/4AS;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v7

    iget v2, p0, LX/4AS;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v8

    invoke-direct {p0, p1, v0, v1}, LX/4AS;->a(Landroid/graphics/Canvas;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 676208
    const-string v0, "I: %d KiB"

    new-array v1, v8, [Ljava/lang/Object;

    iget v2, p0, LX/4AS;->d:I

    div-int/lit16 v2, v2, 0x400

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v7

    invoke-direct {p0, p1, v0, v1}, LX/4AS;->a(Landroid/graphics/Canvas;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 676209
    iget-object v0, p0, LX/4AS;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 676210
    const-string v0, "i format: %s"

    new-array v1, v8, [Ljava/lang/Object;

    iget-object v2, p0, LX/4AS;->e:Ljava/lang/String;

    aput-object v2, v1, v7

    invoke-direct {p0, p1, v0, v1}, LX/4AS;->a(Landroid/graphics/Canvas;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 676211
    :cond_0
    iget v0, p0, LX/4AS;->f:I

    if-lez v0, :cond_1

    .line 676212
    const-string v0, "anim: f %d, l %d"

    new-array v1, v9, [Ljava/lang/Object;

    iget v2, p0, LX/4AS;->f:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v7

    iget v2, p0, LX/4AS;->g:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v8

    invoke-direct {p0, p1, v0, v1}, LX/4AS;->a(Landroid/graphics/Canvas;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 676213
    :cond_1
    return-void
.end method

.method public final getOpacity()I
    .locals 1

    .prologue
    .line 676177
    const/4 v0, -0x3

    return v0
.end method

.method public final onBoundsChange(Landroid/graphics/Rect;)V
    .locals 6

    .prologue
    .line 676180
    invoke-super {p0, p1}, Landroid/graphics/drawable/Drawable;->onBoundsChange(Landroid/graphics/Rect;)V

    .line 676181
    const/4 v0, 0x6

    const/16 v1, 0x8

    const/16 v5, 0x50

    .line 676182
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v2

    div-int/2addr v2, v1

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v3

    div-int/2addr v3, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 676183
    const/16 v3, 0x28

    const/16 v4, 0xc

    invoke-static {v4, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-static {v3, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 676184
    iget-object v3, p0, LX/4AS;->i:Landroid/graphics/Paint;

    int-to-float v4, v2

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 676185
    add-int/lit8 v2, v2, 0x8

    iput v2, p0, LX/4AS;->l:I

    .line 676186
    iget v2, p0, LX/4AS;->h:I

    if-ne v2, v5, :cond_0

    .line 676187
    iget v2, p0, LX/4AS;->l:I

    mul-int/lit8 v2, v2, -0x1

    iput v2, p0, LX/4AS;->l:I

    .line 676188
    :cond_0
    iget v2, p1, Landroid/graphics/Rect;->left:I

    add-int/lit8 v2, v2, 0xa

    iput v2, p0, LX/4AS;->j:I

    .line 676189
    iget v2, p0, LX/4AS;->h:I

    if-ne v2, v5, :cond_1

    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v2, v2, -0xa

    :goto_0
    iput v2, p0, LX/4AS;->k:I

    .line 676190
    return-void

    .line 676191
    :cond_1
    iget v2, p1, Landroid/graphics/Rect;->top:I

    add-int/lit8 v2, v2, 0xa

    add-int/lit8 v2, v2, 0xc

    goto :goto_0
.end method

.method public final setAlpha(I)V
    .locals 0

    .prologue
    .line 676179
    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 0

    .prologue
    .line 676178
    return-void
.end method
