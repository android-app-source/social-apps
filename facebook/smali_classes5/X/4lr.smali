.class public LX/4lr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4lm;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile h:LX/4lr;


# instance fields
.field private final a:LX/4lw;

.field private final b:Ljava/net/Socket;

.field private final c:[B

.field private final d:Ljava/lang/String;

.field private final e:I

.field private f:Z

.field private g:Z


# direct methods
.method public constructor <init>(LX/4lw;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 804846
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 804847
    new-instance v0, Ljava/net/Socket;

    invoke-direct {v0}, Ljava/net/Socket;-><init>()V

    iput-object v0, p0, LX/4lr;->b:Ljava/net/Socket;

    .line 804848
    const/4 v0, 0x4

    new-array v0, v0, [B

    iput-object v0, p0, LX/4lr;->c:[B

    .line 804849
    const-string v0, "dummy_host"

    iput-object v0, p0, LX/4lr;->d:Ljava/lang/String;

    .line 804850
    const/16 v0, 0x1bb

    iput v0, p0, LX/4lr;->e:I

    .line 804851
    iput-boolean v1, p0, LX/4lr;->f:Z

    .line 804852
    iput-boolean v1, p0, LX/4lr;->g:Z

    .line 804853
    iput-object p1, p0, LX/4lr;->a:LX/4lw;

    .line 804854
    return-void
.end method

.method public static a(LX/0QB;)LX/4lr;
    .locals 4

    .prologue
    .line 804866
    sget-object v0, LX/4lr;->h:LX/4lr;

    if-nez v0, :cond_1

    .line 804867
    const-class v1, LX/4lr;

    monitor-enter v1

    .line 804868
    :try_start_0
    sget-object v0, LX/4lr;->h:LX/4lr;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 804869
    if-eqz v2, :cond_0

    .line 804870
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 804871
    new-instance p0, LX/4lr;

    invoke-static {v0}, LX/4lw;->a(LX/0QB;)LX/4lw;

    move-result-object v3

    check-cast v3, LX/4lw;

    invoke-direct {p0, v3}, LX/4lr;-><init>(LX/4lw;)V

    .line 804872
    move-object v0, p0

    .line 804873
    sput-object v0, LX/4lr;->h:LX/4lr;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 804874
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 804875
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 804876
    :cond_1
    sget-object v0, LX/4lr;->h:LX/4lr;

    return-object v0

    .line 804877
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 804878
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 804855
    monitor-enter p0

    .line 804856
    :try_start_0
    sget-boolean v0, LX/4lw;->b:Z

    move v0, v0

    .line 804857
    if-eqz v0, :cond_0

    iget-boolean v3, p0, LX/4lr;->f:Z

    if-eqz v3, :cond_2

    .line 804858
    :cond_0
    if-eqz v0, :cond_1

    iget-boolean v0, p0, LX/4lr;->g:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    move v0, v2

    .line 804859
    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    move v0, v1

    .line 804860
    goto :goto_0

    .line 804861
    :cond_2
    :try_start_1
    iget-object v2, p0, LX/4lr;->b:Ljava/net/Socket;

    iget-object v3, p0, LX/4lr;->c:[B

    const-string v4, "dummy_host"

    const/16 v5, 0x1bb

    invoke-static {v2, v3, v4, v5}, LX/4lw;->a(Ljava/net/Socket;[BLjava/lang/String;I)V

    .line 804862
    const/4 v2, 0x1

    iput-boolean v2, p0, LX/4lr;->g:Z
    :try_end_1
    .catch LX/4ll; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 804863
    const/4 v1, 0x1

    :try_start_2
    iput-boolean v1, p0, LX/4lr;->f:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 804864
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 804865
    :catch_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4lr;->f:Z

    move v0, v1

    goto :goto_0

    :catchall_1
    move-exception v0

    const/4 v1, 0x1

    iput-boolean v1, p0, LX/4lr;->f:Z

    throw v0
.end method
