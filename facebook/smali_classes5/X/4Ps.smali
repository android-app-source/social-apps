.class public LX/4Ps;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 701970
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 21

    .prologue
    .line 702010
    const/4 v15, 0x0

    .line 702011
    const/4 v14, 0x0

    .line 702012
    const/4 v13, 0x0

    .line 702013
    const/4 v12, 0x0

    .line 702014
    const/4 v11, 0x0

    .line 702015
    const/4 v10, 0x0

    .line 702016
    const/4 v7, 0x0

    .line 702017
    const-wide/16 v8, 0x0

    .line 702018
    const/4 v6, 0x0

    .line 702019
    const/4 v5, 0x0

    .line 702020
    const/4 v4, 0x0

    .line 702021
    const/4 v3, 0x0

    .line 702022
    const/4 v2, 0x0

    .line 702023
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v16

    sget-object v17, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    if-eq v0, v1, :cond_f

    .line 702024
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 702025
    const/4 v2, 0x0

    .line 702026
    :goto_0
    return v2

    .line 702027
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 702028
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v16

    sget-object v17, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    if-eq v0, v1, :cond_a

    .line 702029
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v16

    .line 702030
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 702031
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v17

    sget-object v18, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    if-eq v0, v1, :cond_1

    if-eqz v16, :cond_1

    .line 702032
    const-string v17, "bump_reason"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_2

    .line 702033
    const/4 v7, 0x1

    .line 702034
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Lcom/facebook/graphql/enums/GraphQLBumpReason;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLBumpReason;

    move-result-object v15

    goto :goto_1

    .line 702035
    :cond_2
    const-string v17, "cursor"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_3

    .line 702036
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    goto :goto_1

    .line 702037
    :cond_3
    const-string v17, "deduplication_key"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_4

    .line 702038
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    goto :goto_1

    .line 702039
    :cond_4
    const-string v17, "disallow_first_position"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_5

    .line 702040
    const/4 v6, 0x1

    .line 702041
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v12

    goto :goto_1

    .line 702042
    :cond_5
    const-string v17, "features_meta"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_6

    .line 702043
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    goto :goto_1

    .line 702044
    :cond_6
    const-string v17, "is_in_low_engagement_block"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_7

    .line 702045
    const/4 v3, 0x1

    .line 702046
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v10

    goto/16 :goto_1

    .line 702047
    :cond_7
    const-string v17, "node"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_8

    .line 702048
    invoke-static/range {p0 .. p1}, LX/2ao;->a(LX/15w;LX/186;)I

    move-result v9

    goto/16 :goto_1

    .line 702049
    :cond_8
    const-string v17, "ranking_weight"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_9

    .line 702050
    const/4 v2, 0x1

    .line 702051
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v4

    goto/16 :goto_1

    .line 702052
    :cond_9
    const-string v17, "sort_key"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_0

    .line 702053
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto/16 :goto_1

    .line 702054
    :cond_a
    const/16 v16, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 702055
    if-eqz v7, :cond_b

    .line 702056
    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v15}, LX/186;->a(ILjava/lang/Enum;)V

    .line 702057
    :cond_b
    const/4 v7, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v14}, LX/186;->b(II)V

    .line 702058
    const/4 v7, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v13}, LX/186;->b(II)V

    .line 702059
    if-eqz v6, :cond_c

    .line 702060
    const/4 v6, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v12}, LX/186;->a(IZ)V

    .line 702061
    :cond_c
    const/4 v6, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v11}, LX/186;->b(II)V

    .line 702062
    if-eqz v3, :cond_d

    .line 702063
    const/4 v3, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10}, LX/186;->a(IZ)V

    .line 702064
    :cond_d
    const/4 v3, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 702065
    if-eqz v2, :cond_e

    .line 702066
    const/4 v3, 0x7

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 702067
    :cond_e
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 702068
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_f
    move/from16 v19, v4

    move/from16 v20, v5

    move-wide v4, v8

    move v8, v6

    move v9, v7

    move/from16 v7, v20

    move/from16 v6, v19

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 701971
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 701972
    invoke-virtual {p0, p1, v1, v1}, LX/15i;->a(IIS)S

    move-result v0

    .line 701973
    if-eqz v0, :cond_0

    .line 701974
    const-string v0, "bump_reason"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 701975
    const-class v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLBumpReason;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLBumpReason;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 701976
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 701977
    if-eqz v0, :cond_1

    .line 701978
    const-string v1, "cursor"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 701979
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 701980
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 701981
    if-eqz v0, :cond_2

    .line 701982
    const-string v1, "deduplication_key"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 701983
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 701984
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 701985
    if-eqz v0, :cond_3

    .line 701986
    const-string v1, "disallow_first_position"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 701987
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 701988
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 701989
    if-eqz v0, :cond_4

    .line 701990
    const-string v1, "features_meta"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 701991
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 701992
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 701993
    if-eqz v0, :cond_5

    .line 701994
    const-string v1, "is_in_low_engagement_block"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 701995
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 701996
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 701997
    if-eqz v0, :cond_6

    .line 701998
    const-string v1, "node"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 701999
    invoke-static {p0, v0, p2, p3}, LX/2ao;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 702000
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 702001
    cmpl-double v2, v0, v2

    if-eqz v2, :cond_7

    .line 702002
    const-string v2, "ranking_weight"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 702003
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 702004
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 702005
    if-eqz v0, :cond_8

    .line 702006
    const-string v1, "sort_key"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 702007
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 702008
    :cond_8
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 702009
    return-void
.end method
