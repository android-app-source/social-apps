.class public LX/4MW;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 688653
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 688628
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v2, :cond_7

    .line 688629
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 688630
    :goto_0
    return v0

    .line 688631
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 688632
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_6

    .line 688633
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 688634
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 688635
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_1

    if-eqz v5, :cond_1

    .line 688636
    const-string v6, "facebox_center"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 688637
    invoke-static {p0, p1}, LX/2ay;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 688638
    :cond_2
    const-string v6, "facebox_size"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 688639
    invoke-static {p0, p1}, LX/2ay;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 688640
    :cond_3
    const-string v6, "id"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 688641
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 688642
    :cond_4
    const-string v6, "tag_suggestions"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 688643
    invoke-static {p0, p1}, LX/4MX;->a(LX/15w;LX/186;)I

    move-result v1

    goto :goto_1

    .line 688644
    :cond_5
    const-string v6, "url"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 688645
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 688646
    :cond_6
    const/4 v5, 0x6

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 688647
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, LX/186;->b(II)V

    .line 688648
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, LX/186;->b(II)V

    .line 688649
    const/4 v3, 0x3

    invoke-virtual {p1, v3, v2}, LX/186;->b(II)V

    .line 688650
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v1}, LX/186;->b(II)V

    .line 688651
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 688652
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :cond_7
    move v1, v0

    move v2, v0

    move v3, v0

    move v4, v0

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 688605
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 688606
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 688607
    if-eqz v0, :cond_0

    .line 688608
    const-string v1, "facebox_center"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688609
    invoke-static {p0, v0, p2}, LX/2ay;->a(LX/15i;ILX/0nX;)V

    .line 688610
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 688611
    if-eqz v0, :cond_1

    .line 688612
    const-string v1, "facebox_size"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688613
    invoke-static {p0, v0, p2}, LX/2ay;->a(LX/15i;ILX/0nX;)V

    .line 688614
    :cond_1
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 688615
    if-eqz v0, :cond_2

    .line 688616
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688617
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 688618
    :cond_2
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 688619
    if-eqz v0, :cond_3

    .line 688620
    const-string v1, "tag_suggestions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688621
    invoke-static {p0, v0, p2, p3}, LX/4MX;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 688622
    :cond_3
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 688623
    if-eqz v0, :cond_4

    .line 688624
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 688625
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 688626
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 688627
    return-void
.end method
