.class public LX/495;
.super LX/0ux;
.source ""


# instance fields
.field private a:LX/0ux;

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/494;)V
    .locals 1

    .prologue
    .line 674138
    invoke-direct {p0}, LX/0ux;-><init>()V

    .line 674139
    iget-object v0, p1, LX/494;->a:LX/0ux;

    iput-object v0, p0, LX/495;->a:LX/0ux;

    .line 674140
    iget-object v0, p1, LX/494;->b:Ljava/lang/String;

    iput-object v0, p0, LX/495;->b:Ljava/lang/String;

    .line 674141
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 674143
    const-string v0, "WHEN %1$s THEN ?"

    iget-object v1, p0, LX/495;->a:LX/0ux;

    invoke-virtual {v1}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()[Ljava/lang/String;
    .locals 2

    .prologue
    .line 674144
    invoke-virtual {p0}, LX/495;->c()Ljava/lang/Iterable;

    move-result-object v0

    .line 674145
    const-class v1, Ljava/lang/String;

    invoke-static {v0, v1}, LX/0Ph;->a(Ljava/lang/Iterable;Ljava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/Iterable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 674142
    iget-object v0, p0, LX/495;->a:LX/0ux;

    invoke-virtual {v0}, LX/0ux;->c()Ljava/lang/Iterable;

    move-result-object v0

    iget-object v1, p0, LX/495;->b:Ljava/lang/String;

    invoke-static {v1}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Ph;->b(Ljava/lang/Iterable;Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object v0

    return-object v0
.end method
