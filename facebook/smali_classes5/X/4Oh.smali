.class public LX/4Oh;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 697978
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 40

    .prologue
    .line 697855
    const/16 v34, 0x0

    .line 697856
    const/16 v33, 0x0

    .line 697857
    const/16 v32, 0x0

    .line 697858
    const/16 v29, 0x0

    .line 697859
    const-wide/16 v30, 0x0

    .line 697860
    const/16 v28, 0x0

    .line 697861
    const/16 v27, 0x0

    .line 697862
    const/16 v26, 0x0

    .line 697863
    const/16 v25, 0x0

    .line 697864
    const/16 v24, 0x0

    .line 697865
    const/16 v21, 0x0

    .line 697866
    const-wide/16 v22, 0x0

    .line 697867
    const/16 v20, 0x0

    .line 697868
    const-wide/16 v18, 0x0

    .line 697869
    const/16 v17, 0x0

    .line 697870
    const/16 v16, 0x0

    .line 697871
    const/4 v15, 0x0

    .line 697872
    const/4 v14, 0x0

    .line 697873
    const/4 v13, 0x0

    .line 697874
    const/4 v12, 0x0

    .line 697875
    const/4 v11, 0x0

    .line 697876
    const/4 v10, 0x0

    .line 697877
    const/4 v9, 0x0

    .line 697878
    const/4 v8, 0x0

    .line 697879
    const/4 v7, 0x0

    .line 697880
    const/4 v6, 0x0

    .line 697881
    const/4 v5, 0x0

    .line 697882
    const/4 v4, 0x0

    .line 697883
    const/4 v3, 0x0

    .line 697884
    const/4 v2, 0x0

    .line 697885
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v35

    sget-object v36, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    if-eq v0, v1, :cond_20

    .line 697886
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 697887
    const/4 v2, 0x0

    .line 697888
    :goto_0
    return v2

    .line 697889
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v7, :cond_17

    .line 697890
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 697891
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 697892
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v37, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v37

    if-eq v7, v0, :cond_0

    if-eqz v2, :cond_0

    .line 697893
    const-string v7, "article_canonical_url"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 697894
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v36, v2

    goto :goto_1

    .line 697895
    :cond_1
    const-string v7, "article_version_number"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 697896
    const/4 v2, 0x1

    .line 697897
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move/from16 v35, v6

    move v6, v2

    goto :goto_1

    .line 697898
    :cond_2
    const-string v7, "byline"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 697899
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v34, v2

    goto :goto_1

    .line 697900
    :cond_3
    const-string v7, "cover_media"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 697901
    invoke-static/range {p0 .. p1}, LX/4Lk;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v33, v2

    goto :goto_1

    .line 697902
    :cond_4
    const-string v7, "creation_time"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 697903
    const/4 v2, 0x1

    .line 697904
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 697905
    :cond_5
    const-string v7, "document_owner"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 697906
    invoke-static/range {p0 .. p1}, LX/2aw;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v32, v2

    goto :goto_1

    .line 697907
    :cond_6
    const-string v7, "feed_attachment"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 697908
    invoke-static/range {p0 .. p1}, LX/2as;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v31, v2

    goto/16 :goto_1

    .line 697909
    :cond_7
    const-string v7, "feedback"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 697910
    invoke-static/range {p0 .. p1}, LX/2bG;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v30, v2

    goto/16 :goto_1

    .line 697911
    :cond_8
    const-string v7, "feedback_options"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 697912
    const/4 v2, 0x1

    .line 697913
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    move-result-object v7

    move v13, v2

    move-object/from16 v29, v7

    goto/16 :goto_1

    .line 697914
    :cond_9
    const-string v7, "format_version"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 697915
    const/4 v2, 0x1

    .line 697916
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/facebook/graphql/enums/GraphQLDocumentFormatVersion;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentFormatVersion;

    move-result-object v7

    move v12, v2

    move-object/from16 v28, v7

    goto/16 :goto_1

    .line 697917
    :cond_a
    const-string v7, "id"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_b

    .line 697918
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v25, v2

    goto/16 :goto_1

    .line 697919
    :cond_b
    const-string v7, "modified_timestamp"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_c

    .line 697920
    const/4 v2, 0x1

    .line 697921
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v26

    move v11, v2

    goto/16 :goto_1

    .line 697922
    :cond_c
    const-string v7, "publish_status"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_d

    .line 697923
    const/4 v2, 0x1

    .line 697924
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/facebook/graphql/enums/GraphQLInstantArticlePublishStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLInstantArticlePublishStatus;

    move-result-object v7

    move v10, v2

    move-object/from16 v24, v7

    goto/16 :goto_1

    .line 697925
    :cond_d
    const-string v7, "publish_timestamp"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_e

    .line 697926
    const/4 v2, 0x1

    .line 697927
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v22

    move v9, v2

    goto/16 :goto_1

    .line 697928
    :cond_e
    const-string v7, "text_direction"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_f

    .line 697929
    const/4 v2, 0x1

    .line 697930
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/facebook/graphql/enums/GraphQLDocumentTextDirectionEnum;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentTextDirectionEnum;

    move-result-object v7

    move v8, v2

    move-object/from16 v21, v7

    goto/16 :goto_1

    .line 697931
    :cond_f
    const-string v7, "url"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_10

    .line 697932
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v20, v2

    goto/16 :goto_1

    .line 697933
    :cond_10
    const-string v7, "copyright"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_11

    .line 697934
    invoke-static/range {p0 .. p1}, LX/4LK;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v19, v2

    goto/16 :goto_1

    .line 697935
    :cond_11
    const-string v7, "credits"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_12

    .line 697936
    invoke-static/range {p0 .. p1}, LX/4LK;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v18, v2

    goto/16 :goto_1

    .line 697937
    :cond_12
    const-string v7, "document_description"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_13

    .line 697938
    invoke-static/range {p0 .. p1}, LX/4LK;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v17, v2

    goto/16 :goto_1

    .line 697939
    :cond_13
    const-string v7, "document_kicker"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_14

    .line 697940
    invoke-static/range {p0 .. p1}, LX/4LK;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v16, v2

    goto/16 :goto_1

    .line 697941
    :cond_14
    const-string v7, "document_subtitle"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_15

    .line 697942
    invoke-static/range {p0 .. p1}, LX/4LK;->a(LX/15w;LX/186;)I

    move-result v2

    move v15, v2

    goto/16 :goto_1

    .line 697943
    :cond_15
    const-string v7, "document_title"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_16

    .line 697944
    invoke-static/range {p0 .. p1}, LX/4LK;->a(LX/15w;LX/186;)I

    move-result v2

    move v14, v2

    goto/16 :goto_1

    .line 697945
    :cond_16
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 697946
    :cond_17
    const/16 v2, 0x17

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 697947
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 697948
    if-eqz v6, :cond_18

    .line 697949
    const/4 v2, 0x2

    const/4 v6, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v2, v1, v6}, LX/186;->a(III)V

    .line 697950
    :cond_18
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 697951
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 697952
    if-eqz v3, :cond_19

    .line 697953
    const/4 v3, 0x5

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 697954
    :cond_19
    const/4 v2, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 697955
    const/4 v2, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 697956
    const/16 v2, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 697957
    if-eqz v13, :cond_1a

    .line 697958
    const/16 v2, 0x9

    move-object/from16 v0, p1

    move-object/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 697959
    :cond_1a
    if-eqz v12, :cond_1b

    .line 697960
    const/16 v2, 0xa

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 697961
    :cond_1b
    const/16 v2, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 697962
    if-eqz v11, :cond_1c

    .line 697963
    const/16 v3, 0xc

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v26

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 697964
    :cond_1c
    if-eqz v10, :cond_1d

    .line 697965
    const/16 v2, 0xd

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 697966
    :cond_1d
    if-eqz v9, :cond_1e

    .line 697967
    const/16 v3, 0xe

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v22

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 697968
    :cond_1e
    if-eqz v8, :cond_1f

    .line 697969
    const/16 v2, 0xf

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 697970
    :cond_1f
    const/16 v2, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 697971
    const/16 v2, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 697972
    const/16 v2, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 697973
    const/16 v2, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 697974
    const/16 v2, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 697975
    const/16 v2, 0x15

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 697976
    const/16 v2, 0x16

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 697977
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_20
    move/from16 v35, v33

    move/from16 v36, v34

    move/from16 v33, v29

    move/from16 v34, v32

    move-object/from16 v29, v25

    move/from16 v32, v28

    move/from16 v25, v21

    move-object/from16 v28, v24

    move-object/from16 v21, v17

    move-object/from16 v24, v20

    move/from16 v17, v13

    move/from16 v20, v16

    move/from16 v16, v12

    move v13, v7

    move v12, v6

    move v6, v9

    move v9, v3

    move v3, v8

    move v8, v2

    move/from16 v38, v11

    move v11, v5

    move/from16 v39, v10

    move v10, v4

    move-wide/from16 v4, v30

    move/from16 v30, v26

    move/from16 v31, v27

    move-wide/from16 v26, v22

    move-wide/from16 v22, v18

    move/from16 v19, v15

    move/from16 v18, v14

    move/from16 v14, v39

    move/from16 v15, v38

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 9

    .prologue
    const/16 v8, 0xd

    const/16 v7, 0xa

    const/16 v6, 0x9

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    .line 697764
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 697765
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 697766
    if-eqz v0, :cond_0

    .line 697767
    const-string v1, "article_canonical_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 697768
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 697769
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 697770
    if-eqz v0, :cond_1

    .line 697771
    const-string v1, "article_version_number"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 697772
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 697773
    :cond_1
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 697774
    if-eqz v0, :cond_2

    .line 697775
    const-string v1, "byline"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 697776
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 697777
    :cond_2
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 697778
    if-eqz v0, :cond_3

    .line 697779
    const-string v1, "cover_media"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 697780
    invoke-static {p0, v0, p2, p3}, LX/4Lk;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 697781
    :cond_3
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 697782
    cmp-long v2, v0, v4

    if-eqz v2, :cond_4

    .line 697783
    const-string v2, "creation_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 697784
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 697785
    :cond_4
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 697786
    if-eqz v0, :cond_5

    .line 697787
    const-string v1, "document_owner"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 697788
    invoke-static {p0, v0, p2, p3}, LX/2aw;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 697789
    :cond_5
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 697790
    if-eqz v0, :cond_6

    .line 697791
    const-string v1, "feed_attachment"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 697792
    invoke-static {p0, v0, p2, p3}, LX/2as;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 697793
    :cond_6
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 697794
    if-eqz v0, :cond_7

    .line 697795
    const-string v1, "feedback"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 697796
    invoke-static {p0, v0, p2, p3}, LX/2bG;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 697797
    :cond_7
    invoke-virtual {p0, p1, v6, v3}, LX/15i;->a(IIS)S

    move-result v0

    .line 697798
    if-eqz v0, :cond_8

    .line 697799
    const-string v0, "feedback_options"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 697800
    const-class v0, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    invoke-virtual {p0, p1, v6, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 697801
    :cond_8
    invoke-virtual {p0, p1, v7, v3}, LX/15i;->a(IIS)S

    move-result v0

    .line 697802
    if-eqz v0, :cond_9

    .line 697803
    const-string v0, "format_version"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 697804
    const-class v0, Lcom/facebook/graphql/enums/GraphQLDocumentFormatVersion;

    invoke-virtual {p0, p1, v7, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLDocumentFormatVersion;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLDocumentFormatVersion;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 697805
    :cond_9
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 697806
    if-eqz v0, :cond_a

    .line 697807
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 697808
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 697809
    :cond_a
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 697810
    cmp-long v2, v0, v4

    if-eqz v2, :cond_b

    .line 697811
    const-string v2, "modified_timestamp"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 697812
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 697813
    :cond_b
    invoke-virtual {p0, p1, v8, v3}, LX/15i;->a(IIS)S

    move-result v0

    .line 697814
    if-eqz v0, :cond_c

    .line 697815
    const-string v0, "publish_status"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 697816
    const-class v0, Lcom/facebook/graphql/enums/GraphQLInstantArticlePublishStatus;

    invoke-virtual {p0, p1, v8, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLInstantArticlePublishStatus;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLInstantArticlePublishStatus;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 697817
    :cond_c
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 697818
    cmp-long v2, v0, v4

    if-eqz v2, :cond_d

    .line 697819
    const-string v2, "publish_timestamp"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 697820
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 697821
    :cond_d
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(IIS)S

    move-result v0

    .line 697822
    if-eqz v0, :cond_e

    .line 697823
    const-string v0, "text_direction"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 697824
    const/16 v0, 0xf

    const-class v1, Lcom/facebook/graphql/enums/GraphQLDocumentTextDirectionEnum;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLDocumentTextDirectionEnum;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLDocumentTextDirectionEnum;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 697825
    :cond_e
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 697826
    if-eqz v0, :cond_f

    .line 697827
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 697828
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 697829
    :cond_f
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 697830
    if-eqz v0, :cond_10

    .line 697831
    const-string v1, "copyright"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 697832
    invoke-static {p0, v0, p2, p3}, LX/4LK;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 697833
    :cond_10
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 697834
    if-eqz v0, :cond_11

    .line 697835
    const-string v1, "credits"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 697836
    invoke-static {p0, v0, p2, p3}, LX/4LK;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 697837
    :cond_11
    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 697838
    if-eqz v0, :cond_12

    .line 697839
    const-string v1, "document_description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 697840
    invoke-static {p0, v0, p2, p3}, LX/4LK;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 697841
    :cond_12
    const/16 v0, 0x14

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 697842
    if-eqz v0, :cond_13

    .line 697843
    const-string v1, "document_kicker"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 697844
    invoke-static {p0, v0, p2, p3}, LX/4LK;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 697845
    :cond_13
    const/16 v0, 0x15

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 697846
    if-eqz v0, :cond_14

    .line 697847
    const-string v1, "document_subtitle"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 697848
    invoke-static {p0, v0, p2, p3}, LX/4LK;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 697849
    :cond_14
    const/16 v0, 0x16

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 697850
    if-eqz v0, :cond_15

    .line 697851
    const-string v1, "document_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 697852
    invoke-static {p0, v0, p2, p3}, LX/4LK;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 697853
    :cond_15
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 697854
    return-void
.end method
