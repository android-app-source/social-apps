.class public LX/3vH;
.super LX/3vF;
.source ""

# interfaces
.implements Landroid/view/SubMenu;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/3qw;)V
    .locals 0

    .prologue
    .line 651070
    invoke-direct {p0, p1, p2}, LX/3vF;-><init>(Landroid/content/Context;LX/3qu;)V

    .line 651071
    return-void
.end method

.method private b()LX/3qw;
    .locals 1

    .prologue
    .line 651072
    iget-object v0, p0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v0, LX/3qw;

    return-object v0
.end method


# virtual methods
.method public final clearHeader()V
    .locals 1

    .prologue
    .line 651068
    invoke-direct {p0}, LX/3vH;->b()LX/3qw;

    move-result-object v0

    invoke-interface {v0}, LX/3qw;->clearHeader()V

    .line 651069
    return-void
.end method

.method public final getItem()Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 651067
    invoke-direct {p0}, LX/3vH;->b()LX/3qw;

    move-result-object v0

    invoke-interface {v0}, LX/3qw;->getItem()Landroid/view/MenuItem;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/3uv;->a(Landroid/view/MenuItem;)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public final setHeaderIcon(I)Landroid/view/SubMenu;
    .locals 1

    .prologue
    .line 651065
    invoke-direct {p0}, LX/3vH;->b()LX/3qw;

    move-result-object v0

    invoke-interface {v0, p1}, LX/3qw;->setHeaderIcon(I)Landroid/view/SubMenu;

    .line 651066
    return-object p0
.end method

.method public final setHeaderIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/SubMenu;
    .locals 1

    .prologue
    .line 651073
    invoke-direct {p0}, LX/3vH;->b()LX/3qw;

    move-result-object v0

    invoke-interface {v0, p1}, LX/3qw;->setHeaderIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/SubMenu;

    .line 651074
    return-object p0
.end method

.method public final setHeaderTitle(I)Landroid/view/SubMenu;
    .locals 1

    .prologue
    .line 651063
    invoke-direct {p0}, LX/3vH;->b()LX/3qw;

    move-result-object v0

    invoke-interface {v0, p1}, LX/3qw;->setHeaderTitle(I)Landroid/view/SubMenu;

    .line 651064
    return-object p0
.end method

.method public final setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/SubMenu;
    .locals 1

    .prologue
    .line 651061
    invoke-direct {p0}, LX/3vH;->b()LX/3qw;

    move-result-object v0

    invoke-interface {v0, p1}, LX/3qw;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/SubMenu;

    .line 651062
    return-object p0
.end method

.method public final setHeaderView(Landroid/view/View;)Landroid/view/SubMenu;
    .locals 1

    .prologue
    .line 651059
    invoke-direct {p0}, LX/3vH;->b()LX/3qw;

    move-result-object v0

    invoke-interface {v0, p1}, LX/3qw;->setHeaderView(Landroid/view/View;)Landroid/view/SubMenu;

    .line 651060
    return-object p0
.end method

.method public final setIcon(I)Landroid/view/SubMenu;
    .locals 1

    .prologue
    .line 651055
    invoke-direct {p0}, LX/3vH;->b()LX/3qw;

    move-result-object v0

    invoke-interface {v0, p1}, LX/3qw;->setIcon(I)Landroid/view/SubMenu;

    .line 651056
    return-object p0
.end method

.method public final setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/SubMenu;
    .locals 1

    .prologue
    .line 651057
    invoke-direct {p0}, LX/3vH;->b()LX/3qw;

    move-result-object v0

    invoke-interface {v0, p1}, LX/3qw;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/SubMenu;

    .line 651058
    return-object p0
.end method
