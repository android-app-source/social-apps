.class public final enum LX/47W;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/47W;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/47W;

.field public static final enum BOOLEAN:LX/47W;

.field public static final enum LONG:LX/47W;

.field public static final enum STRING:LX/47W;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 672308
    new-instance v0, LX/47W;

    const-string v1, "STRING"

    invoke-direct {v0, v1, v2}, LX/47W;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/47W;->STRING:LX/47W;

    .line 672309
    new-instance v0, LX/47W;

    const-string v1, "LONG"

    invoke-direct {v0, v1, v3}, LX/47W;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/47W;->LONG:LX/47W;

    .line 672310
    new-instance v0, LX/47W;

    const-string v1, "BOOLEAN"

    invoke-direct {v0, v1, v4}, LX/47W;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/47W;->BOOLEAN:LX/47W;

    .line 672311
    const/4 v0, 0x3

    new-array v0, v0, [LX/47W;

    sget-object v1, LX/47W;->STRING:LX/47W;

    aput-object v1, v0, v2

    sget-object v1, LX/47W;->LONG:LX/47W;

    aput-object v1, v0, v3

    sget-object v1, LX/47W;->BOOLEAN:LX/47W;

    aput-object v1, v0, v4

    sput-object v0, LX/47W;->$VALUES:[LX/47W;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 672312
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/47W;
    .locals 1

    .prologue
    .line 672313
    const-class v0, LX/47W;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/47W;

    return-object v0
.end method

.method public static values()[LX/47W;
    .locals 1

    .prologue
    .line 672314
    sget-object v0, LX/47W;->$VALUES:[LX/47W;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/47W;

    return-object v0
.end method
