.class public LX/3tL;
.super Landroid/widget/Filter;
.source ""


# instance fields
.field public a:LX/3tJ;


# direct methods
.method public constructor <init>(LX/3tJ;)V
    .locals 0

    .prologue
    .line 644762
    invoke-direct {p0}, Landroid/widget/Filter;-><init>()V

    .line 644763
    iput-object p1, p0, LX/3tL;->a:LX/3tJ;

    .line 644764
    return-void
.end method


# virtual methods
.method public final convertResultToString(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 644765
    iget-object v0, p0, LX/3tL;->a:LX/3tJ;

    check-cast p1, Landroid/database/Cursor;

    invoke-interface {v0, p1}, LX/3tJ;->c(Landroid/database/Cursor;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final performFiltering(Ljava/lang/CharSequence;)Landroid/widget/Filter$FilterResults;
    .locals 3

    .prologue
    .line 644766
    iget-object v0, p0, LX/3tL;->a:LX/3tJ;

    invoke-interface {v0, p1}, LX/3tJ;->a(Ljava/lang/CharSequence;)Landroid/database/Cursor;

    move-result-object v0

    .line 644767
    new-instance v1, Landroid/widget/Filter$FilterResults;

    invoke-direct {v1}, Landroid/widget/Filter$FilterResults;-><init>()V

    .line 644768
    if-eqz v0, :cond_0

    .line 644769
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v2

    iput v2, v1, Landroid/widget/Filter$FilterResults;->count:I

    .line 644770
    iput-object v0, v1, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 644771
    :goto_0
    return-object v1

    .line 644772
    :cond_0
    const/4 v0, 0x0

    iput v0, v1, Landroid/widget/Filter$FilterResults;->count:I

    .line 644773
    const/4 v0, 0x0

    iput-object v0, v1, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    goto :goto_0
.end method

.method public final publishResults(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V
    .locals 2

    .prologue
    .line 644774
    iget-object v0, p0, LX/3tL;->a:LX/3tJ;

    invoke-interface {v0}, LX/3tJ;->a()Landroid/database/Cursor;

    move-result-object v0

    .line 644775
    iget-object v1, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    if-eqz v1, :cond_0

    iget-object v1, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    if-eq v1, v0, :cond_0

    .line 644776
    iget-object v1, p0, LX/3tL;->a:LX/3tJ;

    iget-object v0, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    check-cast v0, Landroid/database/Cursor;

    invoke-interface {v1, v0}, LX/3tJ;->a(Landroid/database/Cursor;)V

    .line 644777
    :cond_0
    return-void
.end method
