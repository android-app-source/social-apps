.class public LX/3mT;
.super LX/3mU;
.source ""


# instance fields
.field private final a:Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;

.field private final b:LX/3mN;


# direct methods
.method public constructor <init>(Lcom/facebook/graphql/model/GroupsYouShouldJoinFeedUnit;LX/3mN;)V
    .locals 1

    .prologue
    .line 636373
    invoke-direct {p0}, LX/3mU;-><init>()V

    .line 636374
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;

    :goto_0
    iput-object p1, p0, LX/3mT;->a:Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;

    .line 636375
    iput-object p2, p0, LX/3mT;->b:LX/3mN;

    .line 636376
    return-void

    .line 636377
    :cond_0
    const/4 p1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 636378
    iget-object v0, p0, LX/3mT;->b:LX/3mN;

    iget-object v1, p0, LX/3mT;->a:Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;

    invoke-virtual {v0, v1}, LX/3mN;->b(Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;)V

    .line 636379
    return-void
.end method

.method public final a(I)Z
    .locals 2

    .prologue
    .line 636380
    iget-object v0, p0, LX/3mT;->a:Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3mT;->b:LX/3mN;

    iget-object v1, p0, LX/3mT;->a:Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;

    invoke-virtual {v0, v1, p1}, LX/3mN;->a(Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3mT;->a:Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;

    invoke-static {v0}, LX/3mN;->a(Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
