.class public final LX/50F;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<TE;>;"
    }
.end annotation


# instance fields
.field private final a:LX/1M1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1M1",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<",
            "Lcom/google/common/collect/Multiset$Entry",
            "<TE;>;>;"
        }
    .end annotation
.end field

.field private c:LX/4wx;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/Multiset$Entry",
            "<TE;>;"
        }
    .end annotation
.end field

.field private d:I

.field private e:I

.field private f:Z


# direct methods
.method public constructor <init>(LX/1M1;Ljava/util/Iterator;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1M1",
            "<TE;>;",
            "Ljava/util/Iterator",
            "<",
            "Lcom/google/common/collect/Multiset$Entry",
            "<TE;>;>;)V"
        }
    .end annotation

    .prologue
    .line 823535
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 823536
    iput-object p1, p0, LX/50F;->a:LX/1M1;

    .line 823537
    iput-object p2, p0, LX/50F;->b:Ljava/util/Iterator;

    .line 823538
    return-void
.end method


# virtual methods
.method public final hasNext()Z
    .locals 1

    .prologue
    .line 823539
    iget v0, p0, LX/50F;->d:I

    if-gtz v0, :cond_0

    iget-object v0, p0, LX/50F;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final next()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 823540
    invoke-virtual {p0}, LX/50F;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 823541
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 823542
    :cond_0
    iget v0, p0, LX/50F;->d:I

    if-nez v0, :cond_1

    .line 823543
    iget-object v0, p0, LX/50F;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4wx;

    iput-object v0, p0, LX/50F;->c:LX/4wx;

    .line 823544
    iget-object v0, p0, LX/50F;->c:LX/4wx;

    invoke-virtual {v0}, LX/4wx;->b()I

    move-result v0

    iput v0, p0, LX/50F;->d:I

    iput v0, p0, LX/50F;->e:I

    .line 823545
    :cond_1
    iget v0, p0, LX/50F;->d:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/50F;->d:I

    .line 823546
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/50F;->f:Z

    .line 823547
    iget-object v0, p0, LX/50F;->c:LX/4wx;

    invoke-virtual {v0}, LX/4wx;->a()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final remove()V
    .locals 2

    .prologue
    .line 823548
    iget-boolean v0, p0, LX/50F;->f:Z

    invoke-static {v0}, LX/0P6;->a(Z)V

    .line 823549
    iget v0, p0, LX/50F;->e:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 823550
    iget-object v0, p0, LX/50F;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 823551
    :goto_0
    iget v0, p0, LX/50F;->e:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/50F;->e:I

    .line 823552
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/50F;->f:Z

    .line 823553
    return-void

    .line 823554
    :cond_0
    iget-object v0, p0, LX/50F;->a:LX/1M1;

    iget-object v1, p0, LX/50F;->c:LX/4wx;

    invoke-virtual {v1}, LX/4wx;->a()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1M1;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method
