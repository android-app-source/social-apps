.class public LX/4cK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lorg/apache/http/HttpEntity;


# instance fields
.field private final a:Ljava/io/File;

.field private final b:I

.field private final c:I

.field public d:LX/4cJ;


# direct methods
.method public constructor <init>(Ljava/io/File;II)V
    .locals 0

    .prologue
    .line 794833
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 794834
    iput-object p1, p0, LX/4cK;->a:Ljava/io/File;

    .line 794835
    iput p2, p0, LX/4cK;->b:I

    .line 794836
    iput p3, p0, LX/4cK;->c:I

    .line 794837
    return-void
.end method


# virtual methods
.method public final consumeContent()V
    .locals 0

    .prologue
    .line 794832
    return-void
.end method

.method public final getContent()Ljava/io/InputStream;
    .locals 2

    .prologue
    .line 794831
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Unsupported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final getContentEncoding()Lorg/apache/http/Header;
    .locals 1

    .prologue
    .line 794828
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getContentLength()J
    .locals 2

    .prologue
    .line 794830
    iget v0, p0, LX/4cK;->c:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public final getContentType()Lorg/apache/http/Header;
    .locals 3

    .prologue
    .line 794838
    new-instance v0, Lorg/apache/http/message/BasicHeader;

    const-string v1, "Content-Type"

    const-string v2, "application/octet-stream"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public final isChunked()Z
    .locals 1

    .prologue
    .line 794829
    const/4 v0, 0x0

    return v0
.end method

.method public final isRepeatable()Z
    .locals 1

    .prologue
    .line 794827
    const/4 v0, 0x1

    return v0
.end method

.method public final isStreaming()Z
    .locals 1

    .prologue
    .line 794826
    const/4 v0, 0x1

    return v0
.end method

.method public final writeTo(Ljava/io/OutputStream;)V
    .locals 6

    .prologue
    .line 794811
    new-instance v1, Ljava/io/RandomAccessFile;

    iget-object v0, p0, LX/4cK;->a:Ljava/io/File;

    const-string v2, "r"

    invoke-direct {v1, v0, v2}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 794812
    :try_start_0
    iget v0, p0, LX/4cK;->b:I

    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 794813
    const/16 v0, 0x1000

    new-array v2, v0, [B

    .line 794814
    iget v0, p0, LX/4cK;->c:I

    .line 794815
    :cond_0
    :goto_0
    const/4 v3, 0x0

    const/16 v4, 0x1000

    invoke-static {v0, v4}, Ljava/lang/Math;->min(II)I

    move-result v4

    invoke-virtual {v1, v2, v3, v4}, Ljava/io/RandomAccessFile;->read([BII)I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    if-lez v0, :cond_1

    .line 794816
    const/4 v4, 0x0

    invoke-virtual {p1, v2, v4, v3}, Ljava/io/OutputStream;->write([BII)V

    .line 794817
    sub-int/2addr v0, v3

    .line 794818
    iget-object v3, p0, LX/4cK;->d:LX/4cJ;

    if-eqz v3, :cond_0

    .line 794819
    iget-object v3, p0, LX/4cK;->d:LX/4cJ;

    iget v4, p0, LX/4cK;->c:I

    sub-int/2addr v4, v0

    int-to-long v4, v4

    invoke-interface {v3, v4, v5}, LX/4cJ;->a(J)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 794820
    :catch_0
    move-exception v0

    .line 794821
    :try_start_1
    new-instance v2, Ljava/io/IOException;

    const-string v3, "Cannot find source file"

    invoke-direct {v2, v3, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 794822
    :catchall_0
    move-exception v0

    .line 794823
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V

    throw v0

    .line 794824
    :cond_1
    :try_start_2
    invoke-virtual {p1}, Ljava/io/OutputStream;->flush()V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 794825
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V

    return-void
.end method
