.class public final LX/4wJ;
.super LX/4wI;
.source ""

# interfaces
.implements LX/0QJ;
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LX/4wI",
        "<TK;TV;>;",
        "LX/0QJ",
        "<TK;TV;>;",
        "Ljava/io/Serializable;"
    }
.end annotation


# instance fields
.field public transient a:LX/0QJ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QJ",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Qd;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Qd",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 820050
    invoke-direct {p0, p1}, LX/4wI;-><init>(LX/0Qd;)V

    .line 820051
    return-void
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 2

    .prologue
    .line 820046
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 820047
    invoke-virtual {p0}, LX/4wI;->f()LX/0QN;

    move-result-object v0

    .line 820048
    iget-object v1, p0, LX/4wI;->loader:LX/0QM;

    invoke-virtual {v0, v1}, LX/0QN;->a(LX/0QM;)LX/0QJ;

    move-result-object v0

    iput-object v0, p0, LX/4wJ;->a:LX/0QJ;

    .line 820049
    return-void
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 820045
    iget-object v0, p0, LX/4wJ;->a:LX/0QJ;

    return-object v0
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)TV;"
        }
    .end annotation

    .prologue
    .line 820042
    iget-object v0, p0, LX/4wJ;->a:LX/0QJ;

    invoke-interface {v0, p1}, LX/0QJ;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)TV;"
        }
    .end annotation

    .prologue
    .line 820044
    iget-object v0, p0, LX/4wJ;->a:LX/0QJ;

    invoke-interface {v0, p1}, LX/0QJ;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final d(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)TV;"
        }
    .end annotation

    .prologue
    .line 820043
    iget-object v0, p0, LX/4wJ;->a:LX/0QJ;

    invoke-interface {v0, p1}, LX/0QJ;->d(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
