.class public LX/3lZ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile k:LX/3lZ;


# instance fields
.field public final a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final b:LX/0lC;

.field public final c:LX/03V;

.field public final d:LX/0Xl;

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/privacy/PrivacyOperationsClient;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/concurrent/Executor;

.field public final g:LX/0SG;

.field public final h:LX/0YZ;

.field public i:LX/0Yb;

.field public j:Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0lC;LX/03V;LX/0Xl;LX/0Ot;Ljava/util/concurrent/ExecutorService;LX/0SG;)V
    .locals 1
    .param p4    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p6    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0lC;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Xl;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/privacy/PrivacyOperationsClient;",
            ">;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0SG;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 634365
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 634366
    new-instance v0, LX/3la;

    invoke-direct {v0, p0}, LX/3la;-><init>(LX/3lZ;)V

    iput-object v0, p0, LX/3lZ;->h:LX/0YZ;

    .line 634367
    iput-object p1, p0, LX/3lZ;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 634368
    iput-object p2, p0, LX/3lZ;->b:LX/0lC;

    .line 634369
    iput-object p3, p0, LX/3lZ;->c:LX/03V;

    .line 634370
    iput-object p4, p0, LX/3lZ;->d:LX/0Xl;

    .line 634371
    iput-object p5, p0, LX/3lZ;->e:LX/0Ot;

    .line 634372
    iput-object p6, p0, LX/3lZ;->f:Ljava/util/concurrent/Executor;

    .line 634373
    iput-object p7, p0, LX/3lZ;->g:LX/0SG;

    .line 634374
    iget-object v0, p0, LX/3lZ;->d:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string p1, "com.facebook.STREAM_PUBLISH_COMPLETE"

    iget-object p2, p0, LX/3lZ;->h:LX/0YZ;

    invoke-interface {v0, p1, p2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, LX/3lZ;->i:LX/0Yb;

    .line 634375
    iget-object v0, p0, LX/3lZ;->i:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 634376
    return-void
.end method

.method public static a(LX/0QB;)LX/3lZ;
    .locals 11

    .prologue
    .line 634377
    sget-object v0, LX/3lZ;->k:LX/3lZ;

    if-nez v0, :cond_1

    .line 634378
    const-class v1, LX/3lZ;

    monitor-enter v1

    .line 634379
    :try_start_0
    sget-object v0, LX/3lZ;->k:LX/3lZ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 634380
    if-eqz v2, :cond_0

    .line 634381
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 634382
    new-instance v3, LX/3lZ;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v5

    check-cast v5, LX/0lC;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v7

    check-cast v7, LX/0Xl;

    const/16 v8, 0x2fc7

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v0}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v9

    check-cast v9, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v10

    check-cast v10, LX/0SG;

    invoke-direct/range {v3 .. v10}, LX/3lZ;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0lC;LX/03V;LX/0Xl;LX/0Ot;Ljava/util/concurrent/ExecutorService;LX/0SG;)V

    .line 634383
    move-object v0, v3

    .line 634384
    sput-object v0, LX/3lZ;->k:LX/3lZ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 634385
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 634386
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 634387
    :cond_1
    sget-object v0, LX/3lZ;->k:LX/3lZ;

    return-object v0

    .line 634388
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 634389
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;)V
    .locals 4

    .prologue
    .line 634390
    iput-object p1, p0, LX/3lZ;->j:Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;

    .line 634391
    const/4 v1, 0x0

    .line 634392
    :try_start_0
    iget-object v0, p0, LX/3lZ;->b:LX/0lC;

    iget-object v2, p0, LX/3lZ;->j:Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;

    invoke-virtual {v0, v2}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 634393
    :goto_0
    if-eqz v0, :cond_0

    .line 634394
    iget-object v1, p0, LX/3lZ;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/2bs;->l:LX/0Tn;

    invoke-interface {v1, v2, v0}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 634395
    :cond_0
    return-void

    .line 634396
    :catch_0
    move-exception v0

    .line 634397
    iget-object v2, p0, LX/3lZ;->c:LX/03V;

    const-string v3, "sticky_guardrail_manager_serialize_error"

    invoke-virtual {v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;
    .locals 7

    .prologue
    .line 634398
    iget-object v0, p0, LX/3lZ;->j:Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;

    if-eqz v0, :cond_1

    .line 634399
    iget-object v0, p0, LX/3lZ;->j:Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;

    .line 634400
    iget-object v1, p0, LX/3lZ;->g:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v1

    iget-wide v3, v0, Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;->mUpdatedTime:J

    const-wide/32 v5, 0x48190800

    add-long/2addr v3, v5

    cmp-long v1, v1, v3

    if-lez v1, :cond_2

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 634401
    if-eqz v0, :cond_0

    .line 634402
    invoke-virtual {p0}, LX/3lZ;->b()V

    .line 634403
    :cond_0
    iget-object v0, p0, LX/3lZ;->j:Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;

    .line 634404
    :goto_1
    return-object v0

    .line 634405
    :cond_1
    iget-object v0, p0, LX/3lZ;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2bs;->l:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 634406
    if-nez v0, :cond_3

    .line 634407
    invoke-virtual {p0}, LX/3lZ;->b()V

    .line 634408
    :goto_2
    iget-object v0, p0, LX/3lZ;->j:Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    .line 634409
    :cond_3
    :try_start_0
    iget-object v1, p0, LX/3lZ;->b:LX/0lC;

    const-class v2, Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;

    invoke-virtual {v1, v0, v2}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;

    iput-object v0, p0, LX/3lZ;->j:Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 634410
    :catch_0
    move-exception v0

    .line 634411
    iget-object v1, p0, LX/3lZ;->c:LX/03V;

    const-string v2, "sticky_guardrail_manager_deserialize_error"

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 634412
    invoke-virtual {p0}, LX/3lZ;->b()V

    goto :goto_2
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLViewer;)V
    .locals 5

    .prologue
    .line 634413
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLViewer;->a()Lcom/facebook/graphql/model/GraphQLAudienceInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAudienceInfo;->a()Lcom/facebook/graphql/model/GraphQLComposerPrivacyGuardrailInfo;

    move-result-object v0

    .line 634414
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComposerPrivacyGuardrailInfo;->l()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v1

    if-nez v1, :cond_1

    .line 634415
    :cond_0
    new-instance v0, LX/3le;

    invoke-direct {v0}, LX/3le;-><init>()V

    const/4 v1, 0x0

    .line 634416
    iput-boolean v1, v0, LX/3le;->a:Z

    .line 634417
    move-object v0, v0

    .line 634418
    iget-object v1, p0, LX/3lZ;->g:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    .line 634419
    iput-wide v2, v0, LX/3le;->e:J

    .line 634420
    move-object v0, v0

    .line 634421
    invoke-virtual {v0}, LX/3le;->a()Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;

    move-result-object v0

    .line 634422
    :goto_0
    invoke-direct {p0, v0}, LX/3lZ;->a(Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;)V

    .line 634423
    return-void

    .line 634424
    :cond_1
    new-instance v1, LX/3le;

    invoke-direct {v1}, LX/3le;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComposerPrivacyGuardrailInfo;->j()Z

    move-result v2

    .line 634425
    iput-boolean v2, v1, LX/3le;->a:Z

    .line 634426
    move-object v1, v1

    .line 634427
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComposerPrivacyGuardrailInfo;->a()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v2

    .line 634428
    iput-object v2, v1, LX/3le;->b:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 634429
    move-object v1, v1

    .line 634430
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComposerPrivacyGuardrailInfo;->l()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v2

    .line 634431
    iput-object v2, v1, LX/3le;->c:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 634432
    move-object v1, v1

    .line 634433
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComposerPrivacyGuardrailInfo;->k()J

    move-result-wide v2

    .line 634434
    iput-wide v2, v1, LX/3le;->d:J

    .line 634435
    move-object v0, v1

    .line 634436
    iget-object v1, p0, LX/3lZ;->g:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    .line 634437
    iput-wide v2, v0, LX/3le;->e:J

    .line 634438
    move-object v0, v0

    .line 634439
    invoke-virtual {v0}, LX/3le;->a()Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;

    move-result-object v0

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 634440
    new-instance v0, LX/3le;

    invoke-direct {v0}, LX/3le;-><init>()V

    const/4 v1, 0x0

    .line 634441
    iput-boolean v1, v0, LX/3le;->a:Z

    .line 634442
    move-object v0, v0

    .line 634443
    invoke-virtual {v0}, LX/3le;->a()Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;

    move-result-object v0

    invoke-direct {p0, v0}, LX/3lZ;->a(Lcom/facebook/privacy/audience/ComposerStickyGuardrailConfig;)V

    .line 634444
    return-void
.end method

.method public final c()V
    .locals 9

    .prologue
    .line 634445
    iget-object v0, p0, LX/3lZ;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/PrivacyOperationsClient;

    .line 634446
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 634447
    iget-object v3, v0, Lcom/facebook/privacy/PrivacyOperationsClient;->c:LX/0aG;

    const-string v4, "fetch_sticky_guardrail"

    sget-object v6, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    sget-object v7, Lcom/facebook/privacy/PrivacyOperationsClient;->a:Lcom/facebook/common/callercontext/CallerContext;

    const v8, -0x77f5ff5c

    invoke-static/range {v3 .. v8}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v3

    .line 634448
    invoke-static {v0, v3}, Lcom/facebook/privacy/PrivacyOperationsClient;->a(Lcom/facebook/privacy/PrivacyOperationsClient;LX/1MF;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    move-object v0, v3

    .line 634449
    new-instance v1, LX/8Q2;

    invoke-direct {v1, p0}, LX/8Q2;-><init>(LX/3lZ;)V

    iget-object v2, p0, LX/3lZ;->f:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 634450
    return-void
.end method
