.class public LX/3uc;
.super Landroid/content/ContextWrapper;
.source ""


# instance fields
.field public a:I

.field private b:Landroid/content/res/Resources$Theme;

.field private c:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 0

    .prologue
    .line 649008
    invoke-direct {p0, p1}, Landroid/content/ContextWrapper;-><init>(Landroid/content/Context;)V

    .line 649009
    iput p2, p0, LX/3uc;->a:I

    .line 649010
    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 648998
    iget-object v0, p0, LX/3uc;->b:Landroid/content/res/Resources$Theme;

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 648999
    :goto_0
    if-eqz v0, :cond_0

    .line 649000
    invoke-virtual {p0}, LX/3uc;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->newTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    iput-object v0, p0, LX/3uc;->b:Landroid/content/res/Resources$Theme;

    .line 649001
    invoke-virtual {p0}, LX/3uc;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    .line 649002
    if-eqz v0, :cond_0

    .line 649003
    iget-object v1, p0, LX/3uc;->b:Landroid/content/res/Resources$Theme;

    invoke-virtual {v1, v0}, Landroid/content/res/Resources$Theme;->setTo(Landroid/content/res/Resources$Theme;)V

    .line 649004
    :cond_0
    iget-object v0, p0, LX/3uc;->b:Landroid/content/res/Resources$Theme;

    iget v1, p0, LX/3uc;->a:I

    .line 649005
    const/4 p0, 0x1

    invoke-virtual {v0, v1, p0}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    .line 649006
    return-void

    .line 649007
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final getSystemService(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 648993
    const-string v0, "layout_inflater"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 648994
    iget-object v0, p0, LX/3uc;->c:Landroid/view/LayoutInflater;

    if-nez v0, :cond_0

    .line 648995
    invoke-virtual {p0}, LX/3uc;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, LX/3uc;->c:Landroid/view/LayoutInflater;

    .line 648996
    :cond_0
    iget-object v0, p0, LX/3uc;->c:Landroid/view/LayoutInflater;

    .line 648997
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, LX/3uc;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final getTheme()Landroid/content/res/Resources$Theme;
    .locals 1

    .prologue
    .line 648983
    iget-object v0, p0, LX/3uc;->b:Landroid/content/res/Resources$Theme;

    if-eqz v0, :cond_0

    .line 648984
    iget-object v0, p0, LX/3uc;->b:Landroid/content/res/Resources$Theme;

    .line 648985
    :goto_0
    return-object v0

    .line 648986
    :cond_0
    iget v0, p0, LX/3uc;->a:I

    if-nez v0, :cond_1

    .line 648987
    const v0, 0x7f0e00ca

    iput v0, p0, LX/3uc;->a:I

    .line 648988
    :cond_1
    invoke-direct {p0}, LX/3uc;->b()V

    .line 648989
    iget-object v0, p0, LX/3uc;->b:Landroid/content/res/Resources$Theme;

    goto :goto_0
.end method

.method public final setTheme(I)V
    .locals 0

    .prologue
    .line 648990
    iput p1, p0, LX/3uc;->a:I

    .line 648991
    invoke-direct {p0}, LX/3uc;->b()V

    .line 648992
    return-void
.end method
