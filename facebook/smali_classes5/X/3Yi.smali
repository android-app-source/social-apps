.class public LX/3Yi;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements LX/2yW;
.implements LX/2yX;
.implements LX/35q;


# static fields
.field public static final a:LX/1Cz;


# instance fields
.field private final b:Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;

.field private final c:Landroid/widget/TextView;

.field private final d:Landroid/widget/TextView;

.field public final e:Lcom/facebook/resources/ui/FbButton;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 596788
    new-instance v0, LX/3Yj;

    invoke-direct {v0}, LX/3Yj;-><init>()V

    sput-object v0, LX/3Yi;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 596786
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/3Yi;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 596787
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 596784
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/3Yi;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 596785
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .prologue
    .line 596771
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 596772
    const v0, 0x7f030958

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 596773
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/3Yi;->setOrientation(I)V

    .line 596774
    const v0, 0x7f0d052f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;

    iput-object v0, p0, LX/3Yi;->b:Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;

    .line 596775
    const v0, 0x7f0d052c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/3Yi;->c:Landroid/widget/TextView;

    .line 596776
    const v0, 0x7f0d052d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/3Yi;->d:Landroid/widget/TextView;

    .line 596777
    const v0, 0x7f0d052e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, LX/3Yi;->e:Lcom/facebook/resources/ui/FbButton;

    .line 596778
    iget-object v0, p0, LX/3Yi;->e:Lcom/facebook/resources/ui/FbButton;

    new-instance v1, LX/JVi;

    invoke-direct {v1, p0}, LX/JVi;-><init>(LX/3Yi;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 596779
    sget-object v0, LX/1vY;->ATTACHMENT:LX/1vY;

    invoke-static {p0, v0}, LX/1vZ;->a(Landroid/view/View;LX/1vY;)V

    .line 596780
    iget-object v0, p0, LX/3Yi;->c:Landroid/widget/TextView;

    sget-object v1, LX/1vY;->TITLE:LX/1vY;

    invoke-static {v0, v1}, LX/1vZ;->a(Landroid/view/View;LX/1vY;)V

    .line 596781
    iget-object v0, p0, LX/3Yi;->d:Landroid/widget/TextView;

    sget-object v1, LX/1vY;->SOCIAL_CONTEXT:LX/1vY;

    invoke-static {v0, v1}, LX/1vZ;->a(Landroid/view/View;LX/1vY;)V

    .line 596782
    iget-object v0, p0, LX/3Yi;->e:Lcom/facebook/resources/ui/FbButton;

    sget-object v1, LX/1vY;->GENERIC_CALL_TO_ACTION_BUTTON:LX/1vY;

    invoke-static {v0, v1}, LX/1vZ;->a(Landroid/view/View;LX/1vY;)V

    .line 596783
    return-void
.end method

.method private static a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 596751
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 596752
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 596753
    return-void

    .line 596754
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 596768
    invoke-virtual {p0, v0}, LX/3Yi;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 596769
    invoke-virtual {p0, v0}, LX/3Yi;->setLargeImageController(LX/1aZ;)V

    .line 596770
    return-void
.end method

.method public setCallToActionText(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 596766
    iget-object v0, p0, LX/3Yi;->e:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 596767
    return-void
.end method

.method public setContextText(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 596764
    iget-object v0, p0, LX/3Yi;->d:Landroid/widget/TextView;

    invoke-static {v0, p1}, LX/3Yi;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 596765
    return-void
.end method

.method public setLargeImageAspectRatio(F)V
    .locals 1

    .prologue
    .line 596762
    iget-object v0, p0, LX/3Yi;->b:Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 596763
    return-void
.end method

.method public setLargeImageController(LX/1aZ;)V
    .locals 1
    .param p1    # LX/1aZ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 596757
    iget-object v0, p0, LX/3Yi;->b:Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;

    .line 596758
    if-eqz p1, :cond_0

    const/4 p0, 0x0

    :goto_0
    invoke-virtual {v0, p0}, Lcom/facebook/drawee/view/DraweeView;->setVisibility(I)V

    .line 596759
    invoke-virtual {v0, p1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 596760
    return-void

    .line 596761
    :cond_0
    const/16 p0, 0x8

    goto :goto_0
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 596755
    iget-object v0, p0, LX/3Yi;->c:Landroid/widget/TextView;

    invoke-static {v0, p1}, LX/3Yi;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 596756
    return-void
.end method
