.class public final LX/4pp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public _fieldName:Ljava/lang/String;

.field public _from:Ljava/lang/Object;

.field public _index:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 811940
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 811941
    const/4 v0, -0x1

    iput v0, p0, LX/4pp;->_index:I

    .line 811942
    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;I)V
    .locals 1

    .prologue
    .line 811943
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 811944
    const/4 v0, -0x1

    iput v0, p0, LX/4pp;->_index:I

    .line 811945
    iput-object p1, p0, LX/4pp;->_from:Ljava/lang/Object;

    .line 811946
    iput p2, p0, LX/4pp;->_index:I

    .line 811947
    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 811948
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 811949
    const/4 v0, -0x1

    iput v0, p0, LX/4pp;->_index:I

    .line 811950
    iput-object p1, p0, LX/4pp;->_from:Ljava/lang/Object;

    .line 811951
    if-nez p2, :cond_0

    .line 811952
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Can not pass null fieldName"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 811953
    :cond_0
    iput-object p2, p0, LX/4pp;->_fieldName:Ljava/lang/String;

    .line 811954
    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    const/16 v3, 0x22

    .line 811955
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 811956
    iget-object v0, p0, LX/4pp;->_from:Ljava/lang/Object;

    instance-of v0, v0, Ljava/lang/Class;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/4pp;->_from:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Class;

    .line 811957
    :goto_0
    invoke-virtual {v0}, Ljava/lang/Class;->getPackage()Ljava/lang/Package;

    move-result-object v2

    .line 811958
    if-eqz v2, :cond_0

    .line 811959
    invoke-virtual {v2}, Ljava/lang/Package;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 811960
    const/16 v2, 0x2e

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 811961
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 811962
    const/16 v0, 0x5b

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 811963
    iget-object v0, p0, LX/4pp;->_fieldName:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 811964
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 811965
    iget-object v0, p0, LX/4pp;->_fieldName:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 811966
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 811967
    :goto_1
    const/16 v0, 0x5d

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 811968
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 811969
    :cond_1
    iget-object v0, p0, LX/4pp;->_from:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    goto :goto_0

    .line 811970
    :cond_2
    iget v0, p0, LX/4pp;->_index:I

    if-ltz v0, :cond_3

    .line 811971
    iget v0, p0, LX/4pp;->_index:I

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 811972
    :cond_3
    const/16 v0, 0x3f

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1
.end method
