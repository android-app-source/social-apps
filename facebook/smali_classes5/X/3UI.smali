.class public LX/3UI;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/23P;

.field public final b:LX/2hs;

.field public final c:LX/3UJ;

.field public final d:LX/2nY;

.field public final e:Landroid/content/res/Resources;

.field public f:LX/2h7;

.field public g:LX/3UH;


# direct methods
.method public constructor <init>(LX/23P;LX/2hS;LX/3UJ;LX/2nY;Landroid/content/res/Resources;LX/1Ck;LX/2h7;LX/3UH;)V
    .locals 1
    .param p7    # LX/2h7;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # LX/3UH;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 585722
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 585723
    iput-object p1, p0, LX/3UI;->a:LX/23P;

    .line 585724
    invoke-virtual {p2, p6}, LX/2hS;->a(LX/1Ck;)LX/2hs;

    move-result-object v0

    iput-object v0, p0, LX/3UI;->b:LX/2hs;

    .line 585725
    iput-object p3, p0, LX/3UI;->c:LX/3UJ;

    .line 585726
    iput-object p4, p0, LX/3UI;->d:LX/2nY;

    .line 585727
    iput-object p5, p0, LX/3UI;->e:Landroid/content/res/Resources;

    .line 585728
    iput-object p7, p0, LX/3UI;->f:LX/2h7;

    .line 585729
    iput-object p8, p0, LX/3UI;->g:LX/3UH;

    .line 585730
    return-void
.end method

.method public static a(LX/3UI;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 2
    .param p0    # LX/3UI;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 585731
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, LX/3UI;->a:LX/23P;

    invoke-virtual {v1, p1, v0}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method
