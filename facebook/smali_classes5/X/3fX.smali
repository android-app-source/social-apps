.class public LX/3fX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field private static final b:Ljava/lang/Object;


# instance fields
.field private final a:LX/0QI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QI",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            "Lcom/facebook/contacts/graphql/Contact;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 622551
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/3fX;->b:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/3LO;)V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 622548
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 622549
    invoke-static {}, LX/0QN;->newBuilder()LX/0QN;

    move-result-object v0

    const-wide/16 v2, 0x258

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, LX/0QN;->a(JLjava/util/concurrent/TimeUnit;)LX/0QN;

    move-result-object v0

    const/16 v1, 0x3c

    const/16 v2, 0x64

    invoke-virtual {p1, v1, v2}, LX/3LO;->a(II)I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, LX/0QN;->a(J)LX/0QN;

    move-result-object v0

    invoke-virtual {v0}, LX/0QN;->q()LX/0QI;

    move-result-object v0

    iput-object v0, p0, LX/3fX;->a:LX/0QI;

    .line 622550
    return-void
.end method

.method public static a(LX/0QB;)LX/3fX;
    .locals 7

    .prologue
    .line 622519
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 622520
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 622521
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 622522
    if-nez v1, :cond_0

    .line 622523
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 622524
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 622525
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 622526
    sget-object v1, LX/3fX;->b:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 622527
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 622528
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 622529
    :cond_1
    if-nez v1, :cond_4

    .line 622530
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 622531
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 622532
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 622533
    new-instance p0, LX/3fX;

    invoke-static {v0}, LX/3LO;->a(LX/0QB;)LX/3LO;

    move-result-object v1

    check-cast v1, LX/3LO;

    invoke-direct {p0, v1}, LX/3fX;-><init>(LX/3LO;)V

    .line 622534
    move-object v1, p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 622535
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 622536
    if-nez v1, :cond_2

    .line 622537
    sget-object v0, LX/3fX;->b:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3fX;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 622538
    :goto_1
    if-eqz v0, :cond_3

    .line 622539
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 622540
    :goto_3
    check-cast v0, LX/3fX;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 622541
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 622542
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 622543
    :catchall_1
    move-exception v0

    .line 622544
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 622545
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 622546
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 622547
    :cond_2
    :try_start_8
    sget-object v0, LX/3fX;->b:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3fX;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final a(Lcom/facebook/user/model/UserKey;)Lcom/facebook/contacts/graphql/Contact;
    .locals 1

    .prologue
    .line 622516
    if-nez p1, :cond_0

    .line 622517
    const/4 v0, 0x0

    .line 622518
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/3fX;->a:LX/0QI;

    invoke-interface {v0, p1}, LX/0QI;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/graphql/Contact;

    goto :goto_0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 622514
    iget-object v0, p0, LX/3fX;->a:LX/0QI;

    invoke-interface {v0}, LX/0QI;->a()V

    .line 622515
    return-void
.end method

.method public final a(Lcom/facebook/contacts/graphql/Contact;)V
    .locals 3

    .prologue
    .line 622511
    invoke-static {p1}, LX/6Ms;->a(Lcom/facebook/contacts/graphql/Contact;)LX/0Py;

    move-result-object v0

    invoke-virtual {v0}, LX/0Py;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserKey;

    .line 622512
    iget-object v2, p0, LX/3fX;->a:LX/0QI;

    invoke-interface {v2, v0, p1}, LX/0QI;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 622513
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/Iterable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 622507
    iget-object v0, p0, LX/3fX;->a:LX/0QI;

    invoke-interface {v0, p1}, LX/0QI;->a(Ljava/lang/Iterable;)V

    .line 622508
    return-void
.end method

.method public final clearUserData()V
    .locals 0

    .prologue
    .line 622509
    invoke-virtual {p0}, LX/3fX;->a()V

    .line 622510
    return-void
.end method
