.class public final enum LX/43W;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/43W;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/43W;

.field public static final enum DELETE_FILE:LX/43W;

.field public static final enum EVICTION:LX/43W;

.field public static final enum GENERIC_IO:LX/43W;

.field public static final enum OTHER:LX/43W;

.field public static final enum READ_DECODE:LX/43W;

.field public static final enum READ_FILE:LX/43W;

.field public static final enum READ_FILE_NOT_FOUND:LX/43W;

.field public static final enum READ_INVALID_ENTRY:LX/43W;

.field public static final enum WRITE_CALLBACK_ERROR:LX/43W;

.field public static final enum WRITE_CREATE_DIR:LX/43W;

.field public static final enum WRITE_CREATE_TEMPFILE:LX/43W;

.field public static final enum WRITE_ENCODE:LX/43W;

.field public static final enum WRITE_INVALID_ENTRY:LX/43W;

.field public static final enum WRITE_RENAME_FILE_OTHER:LX/43W;

.field public static final enum WRITE_RENAME_FILE_TEMPFILE_NOT_FOUND:LX/43W;

.field public static final enum WRITE_RENAME_FILE_TEMPFILE_PARENT_NOT_FOUND:LX/43W;

.field public static final enum WRITE_UPDATE_FILE_NOT_FOUND:LX/43W;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 669116
    new-instance v0, LX/43W;

    const-string v1, "READ_DECODE"

    invoke-direct {v0, v1, v3}, LX/43W;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/43W;->READ_DECODE:LX/43W;

    .line 669117
    new-instance v0, LX/43W;

    const-string v1, "READ_FILE"

    invoke-direct {v0, v1, v4}, LX/43W;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/43W;->READ_FILE:LX/43W;

    .line 669118
    new-instance v0, LX/43W;

    const-string v1, "READ_FILE_NOT_FOUND"

    invoke-direct {v0, v1, v5}, LX/43W;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/43W;->READ_FILE_NOT_FOUND:LX/43W;

    .line 669119
    new-instance v0, LX/43W;

    const-string v1, "READ_INVALID_ENTRY"

    invoke-direct {v0, v1, v6}, LX/43W;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/43W;->READ_INVALID_ENTRY:LX/43W;

    .line 669120
    new-instance v0, LX/43W;

    const-string v1, "WRITE_ENCODE"

    invoke-direct {v0, v1, v7}, LX/43W;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/43W;->WRITE_ENCODE:LX/43W;

    .line 669121
    new-instance v0, LX/43W;

    const-string v1, "WRITE_CREATE_TEMPFILE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/43W;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/43W;->WRITE_CREATE_TEMPFILE:LX/43W;

    .line 669122
    new-instance v0, LX/43W;

    const-string v1, "WRITE_UPDATE_FILE_NOT_FOUND"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/43W;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/43W;->WRITE_UPDATE_FILE_NOT_FOUND:LX/43W;

    .line 669123
    new-instance v0, LX/43W;

    const-string v1, "WRITE_RENAME_FILE_TEMPFILE_NOT_FOUND"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/43W;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/43W;->WRITE_RENAME_FILE_TEMPFILE_NOT_FOUND:LX/43W;

    .line 669124
    new-instance v0, LX/43W;

    const-string v1, "WRITE_RENAME_FILE_TEMPFILE_PARENT_NOT_FOUND"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/43W;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/43W;->WRITE_RENAME_FILE_TEMPFILE_PARENT_NOT_FOUND:LX/43W;

    .line 669125
    new-instance v0, LX/43W;

    const-string v1, "WRITE_RENAME_FILE_OTHER"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/43W;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/43W;->WRITE_RENAME_FILE_OTHER:LX/43W;

    .line 669126
    new-instance v0, LX/43W;

    const-string v1, "WRITE_CREATE_DIR"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/43W;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/43W;->WRITE_CREATE_DIR:LX/43W;

    .line 669127
    new-instance v0, LX/43W;

    const-string v1, "WRITE_CALLBACK_ERROR"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/43W;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/43W;->WRITE_CALLBACK_ERROR:LX/43W;

    .line 669128
    new-instance v0, LX/43W;

    const-string v1, "WRITE_INVALID_ENTRY"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, LX/43W;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/43W;->WRITE_INVALID_ENTRY:LX/43W;

    .line 669129
    new-instance v0, LX/43W;

    const-string v1, "DELETE_FILE"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, LX/43W;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/43W;->DELETE_FILE:LX/43W;

    .line 669130
    new-instance v0, LX/43W;

    const-string v1, "EVICTION"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, LX/43W;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/43W;->EVICTION:LX/43W;

    .line 669131
    new-instance v0, LX/43W;

    const-string v1, "GENERIC_IO"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, LX/43W;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/43W;->GENERIC_IO:LX/43W;

    .line 669132
    new-instance v0, LX/43W;

    const-string v1, "OTHER"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, LX/43W;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/43W;->OTHER:LX/43W;

    .line 669133
    const/16 v0, 0x11

    new-array v0, v0, [LX/43W;

    sget-object v1, LX/43W;->READ_DECODE:LX/43W;

    aput-object v1, v0, v3

    sget-object v1, LX/43W;->READ_FILE:LX/43W;

    aput-object v1, v0, v4

    sget-object v1, LX/43W;->READ_FILE_NOT_FOUND:LX/43W;

    aput-object v1, v0, v5

    sget-object v1, LX/43W;->READ_INVALID_ENTRY:LX/43W;

    aput-object v1, v0, v6

    sget-object v1, LX/43W;->WRITE_ENCODE:LX/43W;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/43W;->WRITE_CREATE_TEMPFILE:LX/43W;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/43W;->WRITE_UPDATE_FILE_NOT_FOUND:LX/43W;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/43W;->WRITE_RENAME_FILE_TEMPFILE_NOT_FOUND:LX/43W;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/43W;->WRITE_RENAME_FILE_TEMPFILE_PARENT_NOT_FOUND:LX/43W;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/43W;->WRITE_RENAME_FILE_OTHER:LX/43W;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/43W;->WRITE_CREATE_DIR:LX/43W;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/43W;->WRITE_CALLBACK_ERROR:LX/43W;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/43W;->WRITE_INVALID_ENTRY:LX/43W;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/43W;->DELETE_FILE:LX/43W;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/43W;->EVICTION:LX/43W;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/43W;->GENERIC_IO:LX/43W;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/43W;->OTHER:LX/43W;

    aput-object v2, v0, v1

    sput-object v0, LX/43W;->$VALUES:[LX/43W;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 669134
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/43W;
    .locals 1

    .prologue
    .line 669135
    const-class v0, LX/43W;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/43W;

    return-object v0
.end method

.method public static values()[LX/43W;
    .locals 1

    .prologue
    .line 669136
    sget-object v0, LX/43W;->$VALUES:[LX/43W;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/43W;

    return-object v0
.end method
