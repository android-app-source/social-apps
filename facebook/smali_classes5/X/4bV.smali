.class public LX/4bV;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:Lcom/google/common/util/concurrent/SettableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<TT;>;"
        }
    .end annotation
.end field

.field public final b:I

.field public final c:LX/15D;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/15D",
            "<*>;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/http/interfaces/RequestPriority;

.field private final e:Lcom/facebook/http/interfaces/RequestPriority;


# direct methods
.method public constructor <init>(LX/15D;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/15D",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 793885
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 793886
    iput-object p1, p0, LX/4bV;->c:LX/15D;

    .line 793887
    invoke-static {}, Landroid/os/Process;->myTid()I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->getThreadPriority(I)I

    move-result v0

    iput v0, p0, LX/4bV;->b:I

    .line 793888
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v0

    iput-object v0, p0, LX/4bV;->a:Lcom/google/common/util/concurrent/SettableFuture;

    .line 793889
    invoke-virtual {p1}, LX/15D;->h()Lcom/facebook/http/interfaces/RequestPriority;

    move-result-object v0

    iput-object v0, p0, LX/4bV;->d:Lcom/facebook/http/interfaces/RequestPriority;

    .line 793890
    const/4 v0, 0x0

    iput-object v0, p0, LX/4bV;->e:Lcom/facebook/http/interfaces/RequestPriority;

    .line 793891
    return-void
.end method

.method public constructor <init>(LX/4bV;Lcom/facebook/http/interfaces/RequestPriority;)V
    .locals 1

    .prologue
    .line 793892
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 793893
    iget-object v0, p1, LX/4bV;->c:LX/15D;

    iput-object v0, p0, LX/4bV;->c:LX/15D;

    .line 793894
    iget v0, p1, LX/4bV;->b:I

    iput v0, p0, LX/4bV;->b:I

    .line 793895
    iget-object v0, p1, LX/4bV;->a:Lcom/google/common/util/concurrent/SettableFuture;

    iput-object v0, p0, LX/4bV;->a:Lcom/google/common/util/concurrent/SettableFuture;

    .line 793896
    iput-object p2, p0, LX/4bV;->d:Lcom/facebook/http/interfaces/RequestPriority;

    .line 793897
    const/4 v0, 0x0

    iput-object v0, p0, LX/4bV;->e:Lcom/facebook/http/interfaces/RequestPriority;

    .line 793898
    return-void
.end method

.method private constructor <init>(LX/4bV;Lcom/facebook/http/interfaces/RequestPriority;Lcom/facebook/http/interfaces/RequestPriority;)V
    .locals 1

    .prologue
    .line 793899
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 793900
    iget-object v0, p1, LX/4bV;->c:LX/15D;

    iput-object v0, p0, LX/4bV;->c:LX/15D;

    .line 793901
    iget v0, p1, LX/4bV;->b:I

    iput v0, p0, LX/4bV;->b:I

    .line 793902
    iget-object v0, p1, LX/4bV;->a:Lcom/google/common/util/concurrent/SettableFuture;

    iput-object v0, p0, LX/4bV;->a:Lcom/google/common/util/concurrent/SettableFuture;

    .line 793903
    iput-object p2, p0, LX/4bV;->d:Lcom/facebook/http/interfaces/RequestPriority;

    .line 793904
    iput-object p3, p0, LX/4bV;->e:Lcom/facebook/http/interfaces/RequestPriority;

    .line 793905
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/http/interfaces/RequestPriority;
    .locals 1

    .prologue
    .line 793906
    iget-object v0, p0, LX/4bV;->e:Lcom/facebook/http/interfaces/RequestPriority;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/4bV;->e:Lcom/facebook/http/interfaces/RequestPriority;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/4bV;->d:Lcom/facebook/http/interfaces/RequestPriority;

    goto :goto_0
.end method

.method public final b(Lcom/facebook/http/interfaces/RequestPriority;)LX/4bV;
    .locals 2

    .prologue
    .line 793907
    new-instance v0, LX/4bV;

    iget-object v1, p0, LX/4bV;->d:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-direct {v0, p0, v1, p1}, LX/4bV;-><init>(LX/4bV;Lcom/facebook/http/interfaces/RequestPriority;Lcom/facebook/http/interfaces/RequestPriority;)V

    return-object v0
.end method
