.class public final enum LX/4hG;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/4hG;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/4hG;

.field public static final enum E164:LX/4hG;

.field public static final enum INTERNATIONAL:LX/4hG;

.field public static final enum NATIONAL:LX/4hG;

.field public static final enum RFC3966:LX/4hG;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 800822
    new-instance v0, LX/4hG;

    const-string v1, "E164"

    invoke-direct {v0, v1, v2}, LX/4hG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4hG;->E164:LX/4hG;

    .line 800823
    new-instance v0, LX/4hG;

    const-string v1, "INTERNATIONAL"

    invoke-direct {v0, v1, v3}, LX/4hG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4hG;->INTERNATIONAL:LX/4hG;

    .line 800824
    new-instance v0, LX/4hG;

    const-string v1, "NATIONAL"

    invoke-direct {v0, v1, v4}, LX/4hG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4hG;->NATIONAL:LX/4hG;

    .line 800825
    new-instance v0, LX/4hG;

    const-string v1, "RFC3966"

    invoke-direct {v0, v1, v5}, LX/4hG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4hG;->RFC3966:LX/4hG;

    .line 800826
    const/4 v0, 0x4

    new-array v0, v0, [LX/4hG;

    sget-object v1, LX/4hG;->E164:LX/4hG;

    aput-object v1, v0, v2

    sget-object v1, LX/4hG;->INTERNATIONAL:LX/4hG;

    aput-object v1, v0, v3

    sget-object v1, LX/4hG;->NATIONAL:LX/4hG;

    aput-object v1, v0, v4

    sget-object v1, LX/4hG;->RFC3966:LX/4hG;

    aput-object v1, v0, v5

    sput-object v0, LX/4hG;->$VALUES:[LX/4hG;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 800827
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/4hG;
    .locals 1

    .prologue
    .line 800828
    const-class v0, LX/4hG;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/4hG;

    return-object v0
.end method

.method public static values()[LX/4hG;
    .locals 1

    .prologue
    .line 800829
    sget-object v0, LX/4hG;->$VALUES:[LX/4hG;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/4hG;

    return-object v0
.end method
