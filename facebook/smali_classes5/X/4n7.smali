.class public LX/4n7;
.super LX/2WG;
.source ""


# static fields
.field public static final a:LX/4n5;

.field public static final b:LX/4n5;

.field public static final c:LX/4n5;


# instance fields
.field private final d:LX/4n3;

.field private final e:Landroid/net/Uri;

.field private final f:Landroid/net/Uri;

.field private final g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final h:LX/4n5;

.field private volatile i:LX/1bh;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/16 v2, 0x40

    .line 806533
    invoke-static {}, LX/4n5;->newBuilder()LX/4n6;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/4n6;->a(Z)LX/4n6;

    move-result-object v0

    invoke-virtual {v0}, LX/4n6;->f()LX/4n5;

    move-result-object v0

    sput-object v0, LX/4n7;->a:LX/4n5;

    .line 806534
    invoke-static {}, LX/4n5;->newBuilder()LX/4n6;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, LX/4n6;->a(II)LX/4n6;

    move-result-object v0

    invoke-virtual {v0}, LX/4n6;->f()LX/4n5;

    move-result-object v0

    sput-object v0, LX/4n7;->b:LX/4n5;

    .line 806535
    invoke-static {}, LX/4n5;->newBuilder()LX/4n6;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/4n6;->a(Z)LX/4n6;

    move-result-object v0

    invoke-virtual {v0}, LX/4n6;->f()LX/4n5;

    move-result-object v0

    sput-object v0, LX/4n7;->c:LX/4n5;

    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;LX/4n3;LX/4n5;Ljava/lang/String;)V
    .locals 1
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 806544
    invoke-direct {p0}, LX/2WG;-><init>()V

    .line 806545
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 806546
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 806547
    iput-object p1, p0, LX/4n7;->e:Landroid/net/Uri;

    .line 806548
    invoke-static {p1}, LX/1H1;->g(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, LX/4n7;->f:Landroid/net/Uri;

    .line 806549
    iput-object p2, p0, LX/4n7;->d:LX/4n3;

    .line 806550
    iput-object p3, p0, LX/4n7;->h:LX/4n5;

    .line 806551
    iput-object p4, p0, LX/4n7;->g:Ljava/lang/String;

    .line 806552
    return-void
.end method


# virtual methods
.method public final a()LX/1bh;
    .locals 4

    .prologue
    .line 806536
    iget-object v0, p0, LX/4n7;->i:LX/1bh;

    if-nez v0, :cond_0

    .line 806537
    new-instance v1, LX/1ed;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, LX/4n7;->f:Landroid/net/Uri;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, LX/4n7;->g:Ljava/lang/String;

    if-nez v0, :cond_1

    const-string v0, ""

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LX/1ed;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, LX/4n7;->i:LX/1bh;

    .line 806538
    :cond_0
    iget-object v0, p0, LX/4n7;->i:LX/1bh;

    return-object v0

    .line 806539
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "____"

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/4n7;->g:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 806540
    instance-of v1, p1, LX/4n7;

    if-eqz v1, :cond_0

    .line 806541
    check-cast p1, LX/4n7;

    .line 806542
    iget-object v1, p0, LX/4n7;->f:Landroid/net/Uri;

    iget-object v2, p1, LX/4n7;->f:Landroid/net/Uri;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/4n7;->d:LX/4n3;

    iget-object v2, p1, LX/4n7;->d:LX/4n3;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/4n7;->g:Ljava/lang/String;

    iget-object v2, p1, LX/4n7;->g:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/4n7;->h:LX/4n5;

    iget-object v2, p1, LX/4n7;->h:LX/4n5;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 806543
    :cond_0
    return v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 806531
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LX/4n7;->f:Landroid/net/Uri;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LX/4n7;->h:LX/4n5;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, LX/4n7;->g:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, LX/4n7;->d:LX/4n3;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 806532
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "k"

    iget-object v2, p0, LX/4n7;->f:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "o"

    iget-object v2, p0, LX/4n7;->h:LX/4n5;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "e"

    iget-object v2, p0, LX/4n7;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "t"

    iget-object v2, p0, LX/4n7;->d:LX/4n3;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
