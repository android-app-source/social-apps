.class public final LX/3TH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public final synthetic this$0:LX/2zl;

.field public final synthetic val$callback:Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;


# direct methods
.method public constructor <init>(LX/2zl;Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;)V
    .locals 0

    .prologue
    .line 584053
    iput-object p1, p0, LX/3TH;->this$0:LX/2zl;

    iput-object p2, p0, LX/3TH;->val$callback:Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x26

    const v1, 0x18840a61

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 584054
    iget-object v1, p0, LX/3TH;->this$0:LX/2zl;

    iget-object v2, p0, LX/3TH;->val$callback:Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;

    .line 584055
    sget-object p0, LX/2EU;->CHANNEL_CONNECTED:LX/2EU;

    const-string p1, "event"

    sget-object p3, LX/2EU;->UNKNOWN:LX/2EU;

    invoke-virtual {p3}, LX/2EU;->toValue()I

    move-result p3

    invoke-virtual {p2, p1, p3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result p1

    invoke-static {p1}, LX/2EU;->fromValue(I)LX/2EU;

    move-result-object p1

    invoke-virtual {p0, p1}, LX/2EU;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    .line 584056
    iget-boolean p0, v1, LX/2zl;->mIsEnabledInBackground:Z

    if-nez p0, :cond_0

    iget-boolean p0, v1, LX/2zl;->mIsAppActive:Z

    if-eqz p0, :cond_2

    .line 584057
    :cond_0
    const/4 p0, 0x0

    iput-boolean p0, v1, LX/2zl;->mPendingConnect:Z

    .line 584058
    invoke-virtual {v2}, Lcom/facebook/omnistore/mqtt/OmnistoreMqttJniHandler;->connectionEstablished()V

    .line 584059
    :cond_1
    :goto_0
    const/16 v1, 0x27

    const v2, 0x6ae26582

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 584060
    :cond_2
    const/4 p0, 0x1

    iput-boolean p0, v1, LX/2zl;->mPendingConnect:Z

    goto :goto_0
.end method
