.class public final LX/50B;
.super LX/1q0;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LX/1q0",
        "<TK;",
        "Ljava/util/Collection",
        "<TV;>;>;"
    }
.end annotation


# instance fields
.field public final a:LX/0Xu;
    .annotation build Lcom/google/j2objc/annotations/Weak;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Xu",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Xu;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Xu",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 823509
    invoke-direct {p0}, LX/1q0;-><init>()V

    .line 823510
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Xu;

    iput-object v0, p0, LX/50B;->a:LX/0Xu;

    .line 823511
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;",
            "Ljava/util/Collection",
            "<TV;>;>;>;"
        }
    .end annotation

    .prologue
    .line 823512
    new-instance v0, LX/50A;

    invoke-direct {v0, p0}, LX/50A;-><init>(LX/50B;)V

    return-object v0
.end method

.method public final clear()V
    .locals 1

    .prologue
    .line 823513
    iget-object v0, p0, LX/50B;->a:LX/0Xu;

    invoke-interface {v0}, LX/0Xu;->g()V

    .line 823514
    return-void
.end method

.method public final containsKey(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 823515
    iget-object v0, p0, LX/50B;->a:LX/0Xu;

    invoke-interface {v0, p1}, LX/0Xu;->f(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 823516
    invoke-virtual {p0, p1}, LX/50B;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/50B;->a:LX/0Xu;

    invoke-interface {v0, p1}, LX/0Xu;->c(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isEmpty()Z
    .locals 1

    .prologue
    .line 823517
    iget-object v0, p0, LX/50B;->a:LX/0Xu;

    invoke-interface {v0}, LX/0Xu;->n()Z

    move-result v0

    return v0
.end method

.method public final keySet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 823518
    iget-object v0, p0, LX/50B;->a:LX/0Xu;

    invoke-interface {v0}, LX/0Xu;->p()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 823519
    invoke-virtual {p0, p1}, LX/50B;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/50B;->a:LX/0Xu;

    invoke-interface {v0, p1}, LX/0Xu;->d(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 823520
    iget-object v0, p0, LX/50B;->a:LX/0Xu;

    invoke-interface {v0}, LX/0Xu;->p()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    return v0
.end method
