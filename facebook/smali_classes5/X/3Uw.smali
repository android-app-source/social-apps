.class public LX/3Uw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1TE;
.implements LX/1TG;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/3Uw;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 587376
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 587377
    return-void
.end method

.method public static a(LX/0QB;)LX/3Uw;
    .locals 3

    .prologue
    .line 587378
    sget-object v0, LX/3Uw;->a:LX/3Uw;

    if-nez v0, :cond_1

    .line 587379
    const-class v1, LX/3Uw;

    monitor-enter v1

    .line 587380
    :try_start_0
    sget-object v0, LX/3Uw;->a:LX/3Uw;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 587381
    if-eqz v2, :cond_0

    .line 587382
    :try_start_1
    new-instance v0, LX/3Uw;

    invoke-direct {v0}, LX/3Uw;-><init>()V

    .line 587383
    move-object v0, v0

    .line 587384
    sput-object v0, LX/3Uw;->a:LX/3Uw;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 587385
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 587386
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 587387
    :cond_1
    sget-object v0, LX/3Uw;->a:LX/3Uw;

    return-object v0

    .line 587388
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 587389
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0ja;)V
    .locals 1

    .prologue
    .line 587390
    sget-object v0, Lcom/facebook/feed/rows/sections/SubStoriesPagePartDefinition;->a:LX/1Cz;

    invoke-static {p1, v0}, LX/99p;->a(LX/0ja;LX/1Cz;)V

    .line 587391
    return-void
.end method

.method public final a(LX/1KB;)V
    .locals 16

    .prologue
    .line 587392
    sget-object v1, Lcom/facebook/feed/rows/sections/offline/OfflineFooterPartDefinition;->a:LX/1Cz;

    sget-object v2, LX/3Uy;->a:LX/1Cz;

    sget-object v3, Lcom/facebook/feed/rows/sections/header/ThrowbackSharedStoryExplanationPartDefinition;->a:LX/1Cz;

    sget-object v4, Lcom/facebook/feed/rows/sections/header/ThrowbackSharedStoryHeaderExplanationPartDefinition;->a:LX/1Cz;

    sget-object v5, Lcom/facebook/feed/rows/sections/header/ThrowbackSharedStoryHeaderExplanationV2PartDefinition;->a:LX/1Cz;

    sget-object v6, LX/3V0;->j:LX/1Cz;

    sget-object v7, LX/3V4;->m:LX/1Cz;

    sget-object v8, LX/3V8;->l:LX/1Cz;

    sget-object v9, LX/2ee;->b:LX/1Cz;

    sget-object v10, LX/3VB;->a:LX/1Cz;

    sget-object v11, Lcom/facebook/feed/rows/sections/text/BaseTextPartDefinition;->a:LX/1Cz;

    sget-object v12, Lcom/facebook/feed/rows/sections/text/ContentTextLayoutBasePartDefinition;->a:LX/1Cz;

    const/16 v13, 0x76

    new-array v13, v13, [LX/1Cz;

    const/4 v14, 0x0

    sget-object v15, Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/4 v14, 0x1

    sget-object v15, Lcom/facebook/feedplugins/graphqlstory/location/LocationPartDefinitionWithUpsell;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/4 v14, 0x2

    sget-object v15, LX/3VF;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/4 v14, 0x3

    sget-object v15, Lcom/facebook/feedplugins/graphqlstory/footer/BaseVideoPlaysBlingBarPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/4 v14, 0x4

    sget-object v15, Lcom/facebook/feedplugins/graphqlstory/footer/DefaultStoryEmptyFooterPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/4 v14, 0x5

    sget-object v15, Lcom/facebook/feedplugins/graphqlstory/footer/components/StoryEmptyFooterComponentPartDefinition;->d:LX/1Cz;

    aput-object v15, v13, v14

    const/4 v14, 0x6

    sget-object v15, LX/3VK;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/4 v14, 0x7

    sget-object v15, Lcom/facebook/feedplugins/base/footer/EmptyFooterPartDefinition;->b:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x8

    sget-object v15, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x9

    sget-object v15, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->h:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0xa

    sget-object v15, Lcom/facebook/feed/rows/sections/HoldoutUnitPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0xb

    sget-object v15, Lcom/facebook/feed/rows/sections/common/scissor/ScissorPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0xc

    sget-object v15, Lcom/facebook/feedplugins/attachments/photo/LivePhotoAttachmentPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0xd

    sget-object v15, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0xe

    sget-object v15, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentPartDefinition;->d:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0xf

    sget-object v15, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x10

    sget-object v15, LX/3VN;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x11

    sget-object v15, Lcom/facebook/attachments/angora/AngoraAttachmentView;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x12

    sget-object v15, Lcom/facebook/attachments/angora/AngoraAttachmentView;->b:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x13

    sget-object v15, LX/3VP;->c:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x14

    sget-object v15, LX/3VT;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x15

    sget-object v15, LX/3VV;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x16

    sget-object v15, LX/3VX;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x17

    sget-object v15, LX/3VZ;->j:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x18

    sget-object v15, LX/3Vc;->j:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x19

    sget-object v15, LX/3Ve;->j:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x1a

    sget-object v15, Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x1b

    sget-object v15, Lcom/facebook/permalink/rows/ReactorsDescriptionPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x1c

    sget-object v15, Lcom/facebook/permalink/rows/SeenByPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x1d

    sget-object v15, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x1e

    sget-object v15, Lcom/facebook/feedplugins/hidden/HiddenUnitPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x1f

    sget-object v15, Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x20

    sget-object v15, LX/3Vm;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x21

    sget-object v15, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/RowViewPagerIndicator;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x22

    sget-object v15, LX/2eA;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x23

    sget-object v15, LX/2eA;->b:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x24

    sget-object v15, LX/2eA;->c:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x25

    sget-object v15, LX/2eA;->d:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x26

    sget-object v15, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentHeaderIconPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x27

    sget-object v15, Lcom/facebook/feedplugins/graphqlstory/location/ZeroLocationPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x28

    sget-object v15, Lcom/facebook/feed/rows/sections/StoryPromotionComponentPartDefinition;->d:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x29

    sget-object v15, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerComponentPartDefinition;->d:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x2a

    sget-object v15, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x2b

    sget-object v15, LX/3Vq;->b:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x2c

    sget-object v15, Lcom/facebook/feedplugins/attachments/ImageShareAttachmentPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x2d

    sget-object v15, Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentView;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x2e

    sget-object v15, Lcom/facebook/feed/rows/permalink/VideoViewCountPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x2f

    sget-object v15, LX/3W0;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x30

    sget-object v15, Lcom/facebook/feed/rows/links/ActionLinkFooterPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x31

    sget-object v15, LX/3W2;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x32

    sget-object v15, Lcom/facebook/feed/rows/sections/common/endoffeed/EndOfFeedPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x33

    sget-object v15, Lcom/facebook/feed/rows/sections/common/loadingmore/LoadingMorePartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x34

    sget-object v15, LX/3W6;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x35

    sget-object v15, Lcom/facebook/feed/rows/permalink/PermalinkProfilePhotoPromptPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x36

    sget-object v15, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x37

    sget-object v15, Lcom/facebook/feedback/reactions/ui/GFReactionsBlingBarView;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x38

    sget-object v15, Lcom/facebook/feed/rows/sections/common/unknown/UnknownFeedUnitPartDefinition;->d:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x39

    sget-object v15, Lcom/facebook/feed/rows/sections/common/EmptyPartDefinition;->d:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x3a

    sget-object v15, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImagePartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x3b

    sget-object v15, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentVideoPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x3c

    sget-object v15, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentSphericalImagePartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x3d

    sget-object v15, Lcom/facebook/feed/rows/photosfeed/PhotosFeedObjectionableAttachmentMessagePartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x3e

    sget-object v15, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryUFIFeedbackSummaryComponentPartDefinition;->d:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x3f

    sget-object v15, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentUFIFeedbackSummaryComponentPartDefinition;->d:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x40

    sget-object v15, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryReactionsFooterComponentPartDefinition;->d:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x41

    sget-object v15, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextComponentPartDefinition;->d:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x42

    sget-object v15, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageComponentPartDefinition;->d:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x43

    sget-object v15, Lcom/facebook/feedplugins/calltoaction/LinkShareActionPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x44

    sget-object v15, Lcom/facebook/components/feed/ComponentPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x45

    sget-object v15, Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultFooterComponentPartDefinition;->d:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x46

    sget-object v15, Lcom/facebook/feedplugins/graphqlstory/footer/components/DefaultReactionsFooterComponentPartDefinition;->d:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x47

    sget-object v15, Lcom/facebook/feed/rows/sections/header/components/ExplanationComponentPartDefinition;->d:LX/32K;

    aput-object v15, v13, v14

    const/16 v14, 0x48

    sget-object v15, Lcom/facebook/feed/rows/sections/header/FeedTextHeaderComponentPartDefinition;->d:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x49

    sget-object v15, Lcom/facebook/feed/rows/sections/header/GroupStoryTitleComponentPartDefinition;->d:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x4a

    sget-object v15, Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;->d:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x4b

    sget-object v15, Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;->d:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x4c

    sget-object v15, Lcom/facebook/feedplugins/attachments/SportsMatchPhotoPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x4d

    sget-object v15, Lcom/facebook/feedplugins/attachments/SportsMatchScorePartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x4e

    sget-object v15, Lcom/facebook/feedplugins/attachments/SportsMatchFooterTextPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x4f

    sget-object v15, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkCoverPartDefinition;->b:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x50

    sget-object v15, LX/3WJ;->j:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x51

    sget-object v15, Lcom/facebook/feedplugins/prompts/storycta/PromptCallToActionPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x52

    sget-object v15, LX/3WL;->b:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x53

    sget-object v15, Lcom/facebook/feed/rows/sections/text/CopyTextPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x54

    sget-object v15, Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x55

    sget-object v15, Lcom/facebook/feedplugins/attachments/poll/PollOptionItemPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x56

    sget-object v15, Lcom/facebook/feedplugins/attachments/poll/RadioButtonPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x57

    sget-object v15, Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareWithVerticalBarPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x58

    sget-object v15, LX/3WP;->c:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x59

    sget-object v15, LX/3WR;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x5a

    sget-object v15, Lcom/facebook/feed/rows/permalink/ShareCountPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x5b

    sget-object v15, Lcom/facebook/feedplugins/attachments/linkshare/ShareAttachmentComponentPartDefinition;->d:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x5c

    sget-object v15, Lcom/facebook/feedplugins/prompts/SocialPromptFeedEntryPointPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x5d

    sget-object v15, Lcom/facebook/feed/rows/sections/StoryAYMTPagePostFooterNativeChannelPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x5e

    sget-object v15, LX/3WV;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x5f

    sget-object v15, LX/3WX;->c:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x60

    sget-object v15, Lcom/facebook/feedplugins/attachments/MinutiaeAttachmentIconPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x61

    sget-object v15, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyAcknowledgementPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x62

    sget-object v15, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyQuestionPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x63

    sget-object v15, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyFooterPartDefinition;->b:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x64

    sget-object v15, Lcom/facebook/feedplugins/graphqlstory/footer/LiveVideoCommercialBreakFooterPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x65

    sget-object v15, Lcom/facebook/feedplugins/loadingindicator/GlowingStoryPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x66

    sget-object v15, Lcom/facebook/feed/rows/sections/MultiShareFixedTextPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x67

    sget-object v15, Lcom/facebook/feed/rows/sections/MultiShareFixedTextOnImagePartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x68

    sget-object v15, Lcom/facebook/feedplugins/attachments/EventAttachmentFooterView;->k:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x69

    sget-object v15, LX/3Wh;->b:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x6a

    sget-object v15, LX/3Wj;->c:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x6b

    sget-object v15, Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x6c

    sget-object v15, Lcom/facebook/feed/rows/sections/header/StoryHeaderPartDefinition;->d:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x6d

    sget-object v15, Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewComponentPartDefinition;->d:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x6e

    sget-object v15, Lcom/facebook/feedplugins/attachments/EventAttachmentPhotoComponentPartDefinition;->d:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x6f

    sget-object v15, Lcom/facebook/feedplugins/attachments/EventTicketAttachmentFooterViewComponentPartDefinition;->d:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x70

    sget-object v15, Lcom/facebook/feedplugins/pillsblingbar/ui/UFIFeedbackSummaryComponentPartDefinition;->d:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x71

    sget-object v15, Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;->d:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x72

    sget-object v15, Lcom/facebook/feed/rows/sections/header/components/FundraiserUpsellHeaderComponentPartDefinition;->d:LX/32I;

    aput-object v15, v13, v14

    const/16 v14, 0x73

    sget-object v15, Lcom/facebook/feedplugins/attachments/linkshare/externalgallery/ExternalGalleryAttachmentComponentPartDefinition;->d:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x74

    sget-object v15, Lcom/facebook/feedplugins/attachments/linkshare/InstantArticleEdgeToEdgeShareAttachmentPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x75

    sget-object v15, LX/3Wn;->a:LX/1Cz;

    aput-object v15, v13, v14

    invoke-static/range {v1 .. v13}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    .line 587393
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_0

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Cz;

    .line 587394
    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, LX/1KB;->a(LX/1Cz;)V

    .line 587395
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 587396
    :cond_0
    return-void
.end method
