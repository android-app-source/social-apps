.class public LX/490;
.super LX/0ux;
.source ""


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/0ux;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 674101
    invoke-direct {p0}, LX/0ux;-><init>()V

    .line 674102
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/490;->a:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 4

    .prologue
    .line 674103
    iget-object v0, p0, LX/490;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 674104
    const-string v0, ""

    .line 674105
    :goto_0
    return-object v0

    .line 674106
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 674107
    const/4 v0, 0x1

    .line 674108
    iget-object v1, p0, LX/490;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ux;

    .line 674109
    if-nez v1, :cond_1

    .line 674110
    const-string v1, ","

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 674111
    :cond_1
    invoke-virtual {v0}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 674112
    const/4 v0, 0x0

    move v1, v0

    .line 674113
    goto :goto_1

    .line 674114
    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/0ux;)V
    .locals 1

    .prologue
    .line 674115
    iget-object v0, p0, LX/490;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 674116
    return-void
.end method

.method public final b()[Ljava/lang/String;
    .locals 2

    .prologue
    .line 674117
    invoke-virtual {p0}, LX/490;->c()Ljava/lang/Iterable;

    move-result-object v0

    .line 674118
    const-class v1, Ljava/lang/String;

    invoke-static {v0, v1}, LX/0Ph;->a(Ljava/lang/Iterable;Ljava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/Iterable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 674119
    iget-object v0, p0, LX/490;->a:Ljava/util/List;

    new-instance v1, LX/48z;

    invoke-direct {v1, p0}, LX/48z;-><init>(LX/490;)V

    invoke-static {v0, v1}, LX/0Ph;->a(Ljava/lang/Iterable;LX/0QK;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, LX/0Ph;->f(Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object v0

    return-object v0
.end method
