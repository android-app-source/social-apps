.class public LX/4QO;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 704530
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 704531
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_a

    .line 704532
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 704533
    :goto_0
    return v1

    .line 704534
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_5

    .line 704535
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 704536
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 704537
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_0

    if-eqz v10, :cond_0

    .line 704538
    const-string v11, "can_edit_menu"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 704539
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v5

    move v9, v5

    move v5, v2

    goto :goto_1

    .line 704540
    :cond_1
    const-string v11, "has_link_menus"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 704541
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v4

    move v8, v4

    move v4, v2

    goto :goto_1

    .line 704542
    :cond_2
    const-string v11, "has_photo_menus"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 704543
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v3

    move v7, v3

    move v3, v2

    goto :goto_1

    .line 704544
    :cond_3
    const-string v11, "has_structured_menu"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 704545
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v6, v0

    move v0, v2

    goto :goto_1

    .line 704546
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 704547
    :cond_5
    const/4 v10, 0x5

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 704548
    if-eqz v5, :cond_6

    .line 704549
    invoke-virtual {p1, v1, v9}, LX/186;->a(IZ)V

    .line 704550
    :cond_6
    if-eqz v4, :cond_7

    .line 704551
    invoke-virtual {p1, v2, v8}, LX/186;->a(IZ)V

    .line 704552
    :cond_7
    if-eqz v3, :cond_8

    .line 704553
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v7}, LX/186;->a(IZ)V

    .line 704554
    :cond_8
    if-eqz v0, :cond_9

    .line 704555
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v6}, LX/186;->a(IZ)V

    .line 704556
    :cond_9
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_a
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    move v9, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 2

    .prologue
    .line 704557
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 704558
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 704559
    if-eqz v0, :cond_0

    .line 704560
    const-string v1, "can_edit_menu"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 704561
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 704562
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 704563
    if-eqz v0, :cond_1

    .line 704564
    const-string v1, "has_link_menus"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 704565
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 704566
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 704567
    if-eqz v0, :cond_2

    .line 704568
    const-string v1, "has_photo_menus"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 704569
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 704570
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 704571
    if-eqz v0, :cond_3

    .line 704572
    const-string v1, "has_structured_menu"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 704573
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 704574
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 704575
    return-void
.end method
