.class public LX/3sF;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/3s9;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 643766
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 643767
    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 643768
    new-instance v0, LX/3sE;

    invoke-direct {v0}, LX/3sE;-><init>()V

    sput-object v0, LX/3sF;->a:LX/3s9;

    .line 643769
    :goto_0
    return-void

    .line 643770
    :cond_0
    const/16 v1, 0x12

    if-lt v0, v1, :cond_1

    .line 643771
    new-instance v0, LX/3sD;

    invoke-direct {v0}, LX/3sD;-><init>()V

    sput-object v0, LX/3sF;->a:LX/3s9;

    goto :goto_0

    .line 643772
    :cond_1
    const/16 v1, 0xe

    if-lt v0, v1, :cond_2

    .line 643773
    new-instance v0, LX/3sC;

    invoke-direct {v0}, LX/3sC;-><init>()V

    sput-object v0, LX/3sF;->a:LX/3s9;

    goto :goto_0

    .line 643774
    :cond_2
    const/16 v1, 0xb

    if-lt v0, v1, :cond_3

    .line 643775
    new-instance v0, LX/3sB;

    invoke-direct {v0}, LX/3sB;-><init>()V

    sput-object v0, LX/3sF;->a:LX/3s9;

    goto :goto_0

    .line 643776
    :cond_3
    new-instance v0, LX/3sA;

    invoke-direct {v0}, LX/3sA;-><init>()V

    sput-object v0, LX/3sF;->a:LX/3s9;

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 643777
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 643778
    return-void
.end method

.method public static a(Landroid/view/ViewGroup;)I
    .locals 1

    .prologue
    .line 643779
    sget-object v0, LX/3sF;->a:LX/3s9;

    invoke-interface {v0, p0}, LX/3s9;->a(Landroid/view/ViewGroup;)I

    move-result v0

    return v0
.end method
