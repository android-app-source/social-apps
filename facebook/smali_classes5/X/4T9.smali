.class public LX/4T9;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 716897
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 22

    .prologue
    .line 716828
    const/16 v17, 0x0

    .line 716829
    const/16 v16, 0x0

    .line 716830
    const/4 v15, 0x0

    .line 716831
    const/4 v14, 0x0

    .line 716832
    const-wide/16 v12, 0x0

    .line 716833
    const/4 v11, 0x0

    .line 716834
    const/4 v10, 0x0

    .line 716835
    const/4 v9, 0x0

    .line 716836
    const/4 v8, 0x0

    .line 716837
    const/4 v7, 0x0

    .line 716838
    const/4 v6, 0x0

    .line 716839
    const/4 v5, 0x0

    .line 716840
    const/4 v4, 0x0

    .line 716841
    const/4 v3, 0x0

    .line 716842
    const/4 v2, 0x0

    .line 716843
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v18

    sget-object v19, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-eq v0, v1, :cond_11

    .line 716844
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 716845
    const/4 v2, 0x0

    .line 716846
    :goto_0
    return v2

    .line 716847
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_e

    .line 716848
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 716849
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 716850
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v20, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v20

    if-eq v6, v0, :cond_0

    if-eqz v2, :cond_0

    .line 716851
    const-string v6, "acting_team"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 716852
    invoke-static/range {p0 .. p1}, LX/2bc;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v19, v2

    goto :goto_1

    .line 716853
    :cond_1
    const-string v6, "acting_team_short_name"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 716854
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v18, v2

    goto :goto_1

    .line 716855
    :cond_2
    const-string v6, "fact_clock"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 716856
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v17, v2

    goto :goto_1

    .line 716857
    :cond_3
    const-string v6, "fact_message"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 716858
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v7, v2

    goto :goto_1

    .line 716859
    :cond_4
    const-string v6, "fact_time"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 716860
    const/4 v2, 0x1

    .line 716861
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 716862
    :cond_5
    const-string v6, "feedback"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 716863
    invoke-static/range {p0 .. p1}, LX/2bG;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v16, v2

    goto/16 :goto_1

    .line 716864
    :cond_6
    const-string v6, "header_text"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 716865
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v15, v2

    goto/16 :goto_1

    .line 716866
    :cond_7
    const-string v6, "id"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 716867
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v14, v2

    goto/16 :goto_1

    .line 716868
    :cond_8
    const-string v6, "preview_comment"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 716869
    invoke-static/range {p0 .. p1}, LX/2sx;->a(LX/15w;LX/186;)I

    move-result v2

    move v13, v2

    goto/16 :goto_1

    .line 716870
    :cond_9
    const-string v6, "score_text"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 716871
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v12, v2

    goto/16 :goto_1

    .line 716872
    :cond_a
    const-string v6, "sequence_number"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 716873
    const/4 v2, 0x1

    .line 716874
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move v8, v2

    move v11, v6

    goto/16 :goto_1

    .line 716875
    :cond_b
    const-string v6, "story"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 716876
    invoke-static/range {p0 .. p1}, LX/2aD;->a(LX/15w;LX/186;)I

    move-result v2

    move v10, v2

    goto/16 :goto_1

    .line 716877
    :cond_c
    const-string v6, "url"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 716878
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v9, v2

    goto/16 :goto_1

    .line 716879
    :cond_d
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 716880
    :cond_e
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 716881
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 716882
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 716883
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 716884
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 716885
    if-eqz v3, :cond_f

    .line 716886
    const/4 v3, 0x5

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 716887
    :cond_f
    const/4 v2, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 716888
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 716889
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 716890
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 716891
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 716892
    if-eqz v8, :cond_10

    .line 716893
    const/16 v2, 0xb

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11, v3}, LX/186;->a(III)V

    .line 716894
    :cond_10
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 716895
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 716896
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_11
    move/from16 v18, v16

    move/from16 v19, v17

    move/from16 v16, v11

    move/from16 v17, v15

    move v15, v10

    move v11, v6

    move v10, v5

    move/from16 v21, v7

    move v7, v14

    move v14, v9

    move v9, v4

    move-wide v4, v12

    move v13, v8

    move/from16 v12, v21

    move v8, v2

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 716822
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 716823
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 716824
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2, p3}, LX/4T9;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 716825
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 716826
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 716827
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 716816
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 716817
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 716818
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 716819
    invoke-static {p0, p1}, LX/4T9;->a(LX/15w;LX/186;)I

    move-result v1

    .line 716820
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 716821
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 716761
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 716762
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 716763
    if-eqz v0, :cond_0

    .line 716764
    const-string v1, "acting_team"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 716765
    invoke-static {p0, v0, p2, p3}, LX/2bc;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 716766
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 716767
    if-eqz v0, :cond_1

    .line 716768
    const-string v1, "acting_team_short_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 716769
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 716770
    :cond_1
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 716771
    if-eqz v0, :cond_2

    .line 716772
    const-string v1, "fact_clock"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 716773
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 716774
    :cond_2
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 716775
    if-eqz v0, :cond_3

    .line 716776
    const-string v1, "fact_message"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 716777
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 716778
    :cond_3
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 716779
    cmp-long v2, v0, v2

    if-eqz v2, :cond_4

    .line 716780
    const-string v2, "fact_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 716781
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 716782
    :cond_4
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 716783
    if-eqz v0, :cond_5

    .line 716784
    const-string v1, "feedback"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 716785
    invoke-static {p0, v0, p2, p3}, LX/2bG;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 716786
    :cond_5
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 716787
    if-eqz v0, :cond_6

    .line 716788
    const-string v1, "header_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 716789
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 716790
    :cond_6
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 716791
    if-eqz v0, :cond_7

    .line 716792
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 716793
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 716794
    :cond_7
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 716795
    if-eqz v0, :cond_8

    .line 716796
    const-string v1, "preview_comment"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 716797
    invoke-static {p0, v0, p2, p3}, LX/2sx;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 716798
    :cond_8
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 716799
    if-eqz v0, :cond_9

    .line 716800
    const-string v1, "score_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 716801
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 716802
    :cond_9
    const/16 v0, 0xb

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->a(III)I

    move-result v0

    .line 716803
    if-eqz v0, :cond_a

    .line 716804
    const-string v1, "sequence_number"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 716805
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 716806
    :cond_a
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 716807
    if-eqz v0, :cond_b

    .line 716808
    const-string v1, "story"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 716809
    invoke-static {p0, v0, p2, p3}, LX/2aD;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 716810
    :cond_b
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 716811
    if-eqz v0, :cond_c

    .line 716812
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 716813
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 716814
    :cond_c
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 716815
    return-void
.end method
