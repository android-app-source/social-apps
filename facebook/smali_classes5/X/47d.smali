.class public final enum LX/47d;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/47d;",
        ">;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/47d;

.field public static final enum FLIP_HORIZONTAL:LX/47d;

.field public static final enum FLIP_VERTICAL:LX/47d;

.field public static final enum NORMAL:LX/47d;

.field public static final enum ROTATE_180:LX/47d;

.field public static final enum ROTATE_270:LX/47d;

.field public static final enum ROTATE_90:LX/47d;

.field public static final enum TRANSPOSE:LX/47d;

.field public static final enum TRANSVERSE:LX/47d;

.field public static final enum UNDEFINED:LX/47d;


# instance fields
.field public final exifValue:I


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 672367
    new-instance v0, LX/47d;

    const-string v1, "UNDEFINED"

    invoke-direct {v0, v1, v4, v4}, LX/47d;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/47d;->UNDEFINED:LX/47d;

    .line 672368
    new-instance v0, LX/47d;

    const-string v1, "NORMAL"

    invoke-direct {v0, v1, v5, v5}, LX/47d;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/47d;->NORMAL:LX/47d;

    .line 672369
    new-instance v0, LX/47d;

    const-string v1, "FLIP_HORIZONTAL"

    invoke-direct {v0, v1, v6, v6}, LX/47d;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/47d;->FLIP_HORIZONTAL:LX/47d;

    .line 672370
    new-instance v0, LX/47d;

    const-string v1, "ROTATE_180"

    invoke-direct {v0, v1, v7, v7}, LX/47d;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/47d;->ROTATE_180:LX/47d;

    .line 672371
    new-instance v0, LX/47d;

    const-string v1, "FLIP_VERTICAL"

    invoke-direct {v0, v1, v8, v8}, LX/47d;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/47d;->FLIP_VERTICAL:LX/47d;

    .line 672372
    new-instance v0, LX/47d;

    const-string v1, "TRANSPOSE"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, LX/47d;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/47d;->TRANSPOSE:LX/47d;

    .line 672373
    new-instance v0, LX/47d;

    const-string v1, "ROTATE_90"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, LX/47d;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/47d;->ROTATE_90:LX/47d;

    .line 672374
    new-instance v0, LX/47d;

    const-string v1, "TRANSVERSE"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, LX/47d;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/47d;->TRANSVERSE:LX/47d;

    .line 672375
    new-instance v0, LX/47d;

    const-string v1, "ROTATE_270"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, LX/47d;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/47d;->ROTATE_270:LX/47d;

    .line 672376
    const/16 v0, 0x9

    new-array v0, v0, [LX/47d;

    sget-object v1, LX/47d;->UNDEFINED:LX/47d;

    aput-object v1, v0, v4

    sget-object v1, LX/47d;->NORMAL:LX/47d;

    aput-object v1, v0, v5

    sget-object v1, LX/47d;->FLIP_HORIZONTAL:LX/47d;

    aput-object v1, v0, v6

    sget-object v1, LX/47d;->ROTATE_180:LX/47d;

    aput-object v1, v0, v7

    sget-object v1, LX/47d;->FLIP_VERTICAL:LX/47d;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/47d;->TRANSPOSE:LX/47d;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/47d;->ROTATE_90:LX/47d;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/47d;->TRANSVERSE:LX/47d;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/47d;->ROTATE_270:LX/47d;

    aput-object v2, v0, v1

    sput-object v0, LX/47d;->$VALUES:[LX/47d;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 672364
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 672365
    iput p3, p0, LX/47d;->exifValue:I

    .line 672366
    return-void
.end method

.method public static fromExifInterfaceOrientation(I)LX/47d;
    .locals 5

    .prologue
    .line 672377
    invoke-static {}, LX/47d;->values()[LX/47d;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 672378
    iget v4, v3, LX/47d;->exifValue:I

    if-ne p0, v4, :cond_0

    .line 672379
    return-object v3

    .line 672380
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 672381
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid ExifInterface Orientation: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/47d;
    .locals 1

    .prologue
    .line 672363
    const-class v0, LX/47d;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/47d;

    return-object v0
.end method

.method public static values()[LX/47d;
    .locals 1

    .prologue
    .line 672362
    sget-object v0, LX/47d;->$VALUES:[LX/47d;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/47d;

    return-object v0
.end method
