.class public final LX/4y1;
.super LX/0P2;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LX/0P2",
        "<TK;TV;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 821549
    invoke-direct {p0}, LX/0P2;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Iterable;)LX/0P2;
    .locals 1
    .annotation build Lcom/google/common/annotations/Beta;
    .end annotation

    .prologue
    .line 821547
    invoke-super {p0, p1}, LX/0P2;->a(Ljava/lang/Iterable;)LX/0P2;

    .line 821548
    return-object p0
.end method

.method public final a(Ljava/util/Map$Entry;)LX/0P2;
    .locals 1

    .prologue
    .line 821545
    invoke-super {p0, p1}, LX/0P2;->a(Ljava/util/Map$Entry;)LX/0P2;

    .line 821546
    return-object p0
.end method

.method public final a(Ljava/util/Map;)LX/0P2;
    .locals 1

    .prologue
    .line 821550
    invoke-super {p0, p1}, LX/0P2;->a(Ljava/util/Map;)LX/0P2;

    .line 821551
    return-object p0
.end method

.method public final a()LX/0Rh;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rh",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 821532
    iget v0, p0, LX/0P2;->c:I

    packed-switch v0, :pswitch_data_0

    .line 821533
    iget-object v0, p0, LX/0P2;->a:Ljava/util/Comparator;

    if-eqz v0, :cond_1

    .line 821534
    iget-boolean v0, p0, LX/0P2;->d:Z

    if-eqz v0, :cond_0

    .line 821535
    iget-object v0, p0, LX/0P2;->b:[LX/0P3;

    iget v2, p0, LX/0P2;->c:I

    invoke-static {v0, v2}, LX/0P8;->b([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0P3;

    iput-object v0, p0, LX/4y1;->b:[LX/0P3;

    .line 821536
    :cond_0
    iget-object v0, p0, LX/0P2;->b:[LX/0P3;

    iget v2, p0, LX/0P2;->c:I

    iget-object v3, p0, LX/0P2;->a:Ljava/util/Comparator;

    invoke-static {v3}, LX/1sm;->a(Ljava/util/Comparator;)LX/1sm;

    move-result-object v3

    .line 821537
    sget-object v4, LX/2zy;->VALUE:LX/2zy;

    move-object v4, v4

    .line 821538
    invoke-virtual {v3, v4}, LX/1sm;->a(LX/0QK;)LX/1sm;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Ljava/util/Arrays;->sort([Ljava/lang/Object;IILjava/util/Comparator;)V

    .line 821539
    :cond_1
    iget v0, p0, LX/0P2;->c:I

    iget-object v2, p0, LX/0P2;->b:[LX/0P3;

    array-length v2, v2

    if-ne v0, v2, :cond_2

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/4y1;->d:Z

    .line 821540
    iget v0, p0, LX/0P2;->c:I

    iget-object v1, p0, LX/0P2;->b:[LX/0P3;

    invoke-static {v0, v1}, LX/0Rg;->a(I[Ljava/util/Map$Entry;)LX/0Rg;

    move-result-object v0

    :goto_1
    return-object v0

    .line 821541
    :pswitch_0
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 821542
    goto :goto_1

    .line 821543
    :pswitch_1
    iget-object v0, p0, LX/0P2;->b:[LX/0P3;

    aget-object v0, v0, v1

    invoke-virtual {v0}, LX/0P4;->getKey()Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, LX/0P2;->b:[LX/0P3;

    aget-object v1, v2, v1

    invoke-virtual {v1}, LX/0P4;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v0

    goto :goto_1

    :cond_2
    move v0, v1

    .line 821544
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)LX/4y1;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)",
            "LX/4y1",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 821530
    invoke-super {p0, p1, p2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 821531
    return-object p0
.end method

.method public final synthetic b()LX/0P1;
    .locals 1

    .prologue
    .line 821529
    invoke-virtual {p0}, LX/4y1;->a()LX/0Rh;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;
    .locals 1

    .prologue
    .line 821526
    invoke-super {p0, p1, p2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 821527
    move-object v0, p0

    .line 821528
    return-object v0
.end method
