.class public final enum LX/49B;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/49B;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/49B;

.field public static final enum MEDIUM:LX/49B;

.field public static final enum NO_CHANGE:LX/49B;

.field public static final enum SMALL:LX/49B;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 674184
    new-instance v0, LX/49B;

    const-string v1, "SMALL"

    invoke-direct {v0, v1, v2}, LX/49B;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/49B;->SMALL:LX/49B;

    .line 674185
    new-instance v0, LX/49B;

    const-string v1, "MEDIUM"

    invoke-direct {v0, v1, v3}, LX/49B;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/49B;->MEDIUM:LX/49B;

    .line 674186
    new-instance v0, LX/49B;

    const-string v1, "NO_CHANGE"

    invoke-direct {v0, v1, v4}, LX/49B;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/49B;->NO_CHANGE:LX/49B;

    .line 674187
    const/4 v0, 0x3

    new-array v0, v0, [LX/49B;

    sget-object v1, LX/49B;->SMALL:LX/49B;

    aput-object v1, v0, v2

    sget-object v1, LX/49B;->MEDIUM:LX/49B;

    aput-object v1, v0, v3

    sget-object v1, LX/49B;->NO_CHANGE:LX/49B;

    aput-object v1, v0, v4

    sput-object v0, LX/49B;->$VALUES:[LX/49B;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 674181
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/49B;
    .locals 1

    .prologue
    .line 674183
    const-class v0, LX/49B;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/49B;

    return-object v0
.end method

.method public static values()[LX/49B;
    .locals 1

    .prologue
    .line 674182
    sget-object v0, LX/49B;->$VALUES:[LX/49B;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/49B;

    return-object v0
.end method
