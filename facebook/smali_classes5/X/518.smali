.class public final LX/518;
.super LX/512;
.source ""

# interfaces
.implements LX/1M1;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "LX/512",
        "<TE;>;",
        "LX/1M1",
        "<TE;>;"
    }
.end annotation


# instance fields
.field public transient a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<TE;>;"
        }
    .end annotation
.end field

.field public transient b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/common/collect/Multiset$Entry",
            "<TE;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1M1;Ljava/lang/Object;)V
    .locals 1
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1M1",
            "<TE;>;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 824443
    invoke-direct {p0, p1, p2}, LX/512;-><init>(Ljava/util/Collection;Ljava/lang/Object;)V

    .line 824444
    return-void
.end method

.method private e()LX/1M1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1M1",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 824445
    invoke-super {p0}, LX/512;->b()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, LX/1M1;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 824454
    iget-object v1, p0, LX/18e;->mutex:Ljava/lang/Object;

    monitor-enter v1

    .line 824455
    :try_start_0
    invoke-direct {p0}, LX/518;->e()LX/1M1;

    move-result-object v0

    invoke-interface {v0, p1}, LX/1M1;->a(Ljava/lang/Object;)I

    move-result v0

    monitor-exit v1

    return v0

    .line 824456
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Ljava/lang/Object;I)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;I)I"
        }
    .end annotation

    .prologue
    .line 824446
    iget-object v1, p0, LX/18e;->mutex:Ljava/lang/Object;

    monitor-enter v1

    .line 824447
    :try_start_0
    invoke-direct {p0}, LX/518;->e()LX/1M1;

    move-result-object v0

    invoke-interface {v0, p1, p2}, LX/1M1;->a(Ljava/lang/Object;I)I

    move-result v0

    monitor-exit v1

    return v0

    .line 824448
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a()Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/google/common/collect/Multiset$Entry",
            "<TE;>;>;"
        }
    .end annotation

    .prologue
    .line 824449
    iget-object v1, p0, LX/18e;->mutex:Ljava/lang/Object;

    monitor-enter v1

    .line 824450
    :try_start_0
    iget-object v0, p0, LX/518;->b:Ljava/util/Set;

    if-nez v0, :cond_0

    .line 824451
    invoke-direct {p0}, LX/518;->e()LX/1M1;

    move-result-object v0

    invoke-interface {v0}, LX/1M1;->a()Ljava/util/Set;

    move-result-object v0

    iget-object v2, p0, LX/18e;->mutex:Ljava/lang/Object;

    invoke-static {v0, v2}, LX/2sR;->c(Ljava/util/Set;Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, LX/518;->b:Ljava/util/Set;

    .line 824452
    :cond_0
    iget-object v0, p0, LX/518;->b:Ljava/util/Set;

    monitor-exit v1

    return-object v0

    .line 824453
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Ljava/lang/Object;II)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;II)Z"
        }
    .end annotation

    .prologue
    .line 824437
    iget-object v1, p0, LX/18e;->mutex:Ljava/lang/Object;

    monitor-enter v1

    .line 824438
    :try_start_0
    invoke-direct {p0}, LX/518;->e()LX/1M1;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, LX/1M1;->a(Ljava/lang/Object;II)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 824439
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final b(Ljava/lang/Object;I)I
    .locals 2

    .prologue
    .line 824440
    iget-object v1, p0, LX/18e;->mutex:Ljava/lang/Object;

    monitor-enter v1

    .line 824441
    :try_start_0
    invoke-direct {p0}, LX/518;->e()LX/1M1;

    move-result-object v0

    invoke-interface {v0, p1, p2}, LX/1M1;->b(Ljava/lang/Object;I)I

    move-result v0

    monitor-exit v1

    return v0

    .line 824442
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final synthetic b()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 824436
    invoke-direct {p0}, LX/518;->e()LX/1M1;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/lang/Object;I)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;I)I"
        }
    .end annotation

    .prologue
    .line 824433
    iget-object v1, p0, LX/18e;->mutex:Ljava/lang/Object;

    monitor-enter v1

    .line 824434
    :try_start_0
    invoke-direct {p0}, LX/518;->e()LX/1M1;

    move-result-object v0

    invoke-interface {v0, p1, p2}, LX/1M1;->c(Ljava/lang/Object;I)I

    move-result v0

    monitor-exit v1

    return v0

    .line 824435
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final synthetic c()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 824432
    invoke-direct {p0}, LX/518;->e()LX/1M1;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 824427
    iget-object v1, p0, LX/18e;->mutex:Ljava/lang/Object;

    monitor-enter v1

    .line 824428
    :try_start_0
    iget-object v0, p0, LX/518;->a:Ljava/util/Set;

    if-nez v0, :cond_0

    .line 824429
    invoke-direct {p0}, LX/518;->e()LX/1M1;

    move-result-object v0

    invoke-interface {v0}, LX/1M1;->d()Ljava/util/Set;

    move-result-object v0

    iget-object v2, p0, LX/18e;->mutex:Ljava/lang/Object;

    invoke-static {v0, v2}, LX/2sR;->c(Ljava/util/Set;Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, LX/518;->a:Ljava/util/Set;

    .line 824430
    :cond_0
    iget-object v0, p0, LX/518;->a:Ljava/util/Set;

    monitor-exit v1

    return-object v0

    .line 824431
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 824418
    if-ne p1, p0, :cond_0

    .line 824419
    const/4 v0, 0x1

    .line 824420
    :goto_0
    return v0

    .line 824421
    :cond_0
    iget-object v1, p0, LX/18e;->mutex:Ljava/lang/Object;

    monitor-enter v1

    .line 824422
    :try_start_0
    invoke-direct {p0}, LX/518;->e()LX/1M1;

    move-result-object v0

    invoke-interface {v0, p1}, LX/1M1;->equals(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    goto :goto_0

    .line 824423
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 824424
    iget-object v1, p0, LX/18e;->mutex:Ljava/lang/Object;

    monitor-enter v1

    .line 824425
    :try_start_0
    invoke-direct {p0}, LX/518;->e()LX/1M1;

    move-result-object v0

    invoke-interface {v0}, LX/1M1;->hashCode()I

    move-result v0

    monitor-exit v1

    return v0

    .line 824426
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
