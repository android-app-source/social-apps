.class public final LX/47m;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Map$Entry;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Map$Entry",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/reflect/Field;

.field public final synthetic b:LX/47n;


# direct methods
.method public constructor <init>(LX/47n;Ljava/lang/reflect/Field;)V
    .locals 0

    .prologue
    .line 672467
    iput-object p1, p0, LX/47m;->b:LX/47n;

    iput-object p2, p0, LX/47m;->a:Ljava/lang/reflect/Field;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getKey()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 672468
    iget-object v0, p0, LX/47m;->a:Ljava/lang/reflect/Field;

    invoke-virtual {v0}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getValue()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 672469
    iget-object v0, p0, LX/47m;->b:LX/47n;

    iget-object v0, v0, LX/47n;->a:Ljava/lang/Object;

    iget-object v1, p0, LX/47m;->a:Ljava/lang/reflect/Field;

    invoke-static {v0, v1}, LX/47p;->a(Ljava/lang/Object;Ljava/lang/reflect/Field;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final setValue(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 672470
    invoke-virtual {p0}, LX/47m;->getValue()Ljava/lang/Object;

    move-result-object v0

    .line 672471
    iget-object v1, p0, LX/47m;->b:LX/47n;

    iget-object v1, v1, LX/47n;->a:Ljava/lang/Object;

    iget-object v2, p0, LX/47m;->a:Ljava/lang/reflect/Field;

    invoke-static {v1, v2, p1}, LX/47p;->a(Ljava/lang/Object;Ljava/lang/reflect/Field;Ljava/lang/Object;)V

    .line 672472
    return-object v0
.end method
