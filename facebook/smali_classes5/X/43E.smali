.class public final enum LX/43E;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/43E;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/43E;

.field public static final enum JAVA_RESIZER:LX/43E;

.field public static final enum NATIVE_JT_13:LX/43E;


# instance fields
.field private final name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 668505
    new-instance v0, LX/43E;

    const-string v1, "JAVA_RESIZER"

    const-string v2, "false"

    invoke-direct {v0, v1, v3, v2}, LX/43E;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/43E;->JAVA_RESIZER:LX/43E;

    .line 668506
    new-instance v0, LX/43E;

    const-string v1, "NATIVE_JT_13"

    const-string v2, "jt13"

    invoke-direct {v0, v1, v4, v2}, LX/43E;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/43E;->NATIVE_JT_13:LX/43E;

    .line 668507
    const/4 v0, 0x2

    new-array v0, v0, [LX/43E;

    sget-object v1, LX/43E;->JAVA_RESIZER:LX/43E;

    aput-object v1, v0, v3

    sget-object v1, LX/43E;->NATIVE_JT_13:LX/43E;

    aput-object v1, v0, v4

    sput-object v0, LX/43E;->$VALUES:[LX/43E;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 668508
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 668509
    iput-object p3, p0, LX/43E;->name:Ljava/lang/String;

    .line 668510
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/43E;
    .locals 1

    .prologue
    .line 668511
    const-class v0, LX/43E;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/43E;

    return-object v0
.end method

.method public static values()[LX/43E;
    .locals 1

    .prologue
    .line 668512
    sget-object v0, LX/43E;->$VALUES:[LX/43E;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/43E;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 668513
    iget-object v0, p0, LX/43E;->name:Ljava/lang/String;

    return-object v0
.end method
