.class public LX/4R9;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 707929
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 20

    .prologue
    .line 707809
    const/4 v14, 0x0

    .line 707810
    const/4 v11, 0x0

    .line 707811
    const-wide/16 v12, 0x0

    .line 707812
    const/4 v10, 0x0

    .line 707813
    const/4 v9, 0x0

    .line 707814
    const/4 v8, 0x0

    .line 707815
    const/4 v7, 0x0

    .line 707816
    const/4 v6, 0x0

    .line 707817
    const/4 v5, 0x0

    .line 707818
    const/4 v4, 0x0

    .line 707819
    const/4 v3, 0x0

    .line 707820
    const/4 v2, 0x0

    .line 707821
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v15

    sget-object v16, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v16

    if-eq v15, v0, :cond_e

    .line 707822
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 707823
    const/4 v2, 0x0

    .line 707824
    :goto_0
    return v2

    .line 707825
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v16, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v16

    if-eq v3, v0, :cond_c

    .line 707826
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 707827
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 707828
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v16

    sget-object v17, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    if-eq v0, v1, :cond_0

    if-eqz v3, :cond_0

    .line 707829
    const-string v16, "cache_id"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_1

    .line 707830
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v7, v3

    goto :goto_1

    .line 707831
    :cond_1
    const-string v16, "debug_info"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_2

    .line 707832
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v6, v3

    goto :goto_1

    .line 707833
    :cond_2
    const-string v16, "fetchTimeMs"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_3

    .line 707834
    const/4 v2, 0x1

    .line 707835
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    goto :goto_1

    .line 707836
    :cond_3
    const-string v16, "hideable_token"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_4

    .line 707837
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v15, v3

    goto :goto_1

    .line 707838
    :cond_4
    const-string v16, "id"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_5

    .line 707839
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v14, v3

    goto/16 :goto_1

    .line 707840
    :cond_5
    const-string v16, "pysfItems"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_6

    .line 707841
    invoke-static/range {p0 .. p1}, LX/4R0;->a(LX/15w;LX/186;)I

    move-result v3

    move v13, v3

    goto/16 :goto_1

    .line 707842
    :cond_6
    const-string v16, "short_term_cache_key"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_7

    .line 707843
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v12, v3

    goto/16 :goto_1

    .line 707844
    :cond_7
    const-string v16, "title"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_8

    .line 707845
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v3

    move v11, v3

    goto/16 :goto_1

    .line 707846
    :cond_8
    const-string v16, "titleForSummary"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_9

    .line 707847
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v3

    move v10, v3

    goto/16 :goto_1

    .line 707848
    :cond_9
    const-string v16, "tracking"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_a

    .line 707849
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v9, v3

    goto/16 :goto_1

    .line 707850
    :cond_a
    const-string v16, "url"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 707851
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v8, v3

    goto/16 :goto_1

    .line 707852
    :cond_b
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 707853
    :cond_c
    const/16 v3, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 707854
    const/4 v3, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 707855
    const/4 v3, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 707856
    if-eqz v2, :cond_d

    .line 707857
    const/4 v3, 0x3

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 707858
    :cond_d
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 707859
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 707860
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 707861
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 707862
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 707863
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 707864
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 707865
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 707866
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_e
    move v15, v10

    move v10, v5

    move/from16 v18, v4

    move-wide v4, v12

    move v12, v7

    move v13, v8

    move v8, v3

    move v7, v14

    move v14, v9

    move/from16 v9, v18

    move/from16 v19, v11

    move v11, v6

    move/from16 v6, v19

    goto/16 :goto_1
.end method

.method public static a(LX/15w;S)LX/15i;
    .locals 5

    .prologue
    .line 707918
    const/4 v0, 0x1

    const/4 v4, 0x0

    .line 707919
    new-instance v2, LX/186;

    const/16 v1, 0x80

    invoke-direct {v2, v1}, LX/186;-><init>(I)V

    .line 707920
    invoke-static {p0, v2}, LX/4R9;->a(LX/15w;LX/186;)I

    move-result v1

    .line 707921
    if-eqz v0, :cond_0

    .line 707922
    const/4 v3, 0x2

    invoke-virtual {v2, v3}, LX/186;->c(I)V

    .line 707923
    invoke-virtual {v2, v4, p1, v4}, LX/186;->a(ISI)V

    .line 707924
    const/4 v3, 0x1

    invoke-virtual {v2, v3, v1}, LX/186;->b(II)V

    .line 707925
    invoke-virtual {v2}, LX/186;->d()I

    move-result v1

    .line 707926
    :cond_0
    invoke-virtual {v2, v1}, LX/186;->d(I)V

    .line 707927
    invoke-static {v2}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v1

    move-object v0, v1

    .line 707928
    return-object v0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 707867
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 707868
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 707869
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 707870
    const-string v0, "name"

    const-string v1, "PeopleYouShouldFollowFeedUnit"

    invoke-virtual {p2, v0, v1}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 707871
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 707872
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 707873
    if-eqz v0, :cond_0

    .line 707874
    const-string v1, "cache_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 707875
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 707876
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 707877
    if-eqz v0, :cond_1

    .line 707878
    const-string v1, "debug_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 707879
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 707880
    :cond_1
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 707881
    cmp-long v2, v0, v2

    if-eqz v2, :cond_2

    .line 707882
    const-string v2, "fetchTimeMs"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 707883
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 707884
    :cond_2
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 707885
    if-eqz v0, :cond_3

    .line 707886
    const-string v1, "hideable_token"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 707887
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 707888
    :cond_3
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 707889
    if-eqz v0, :cond_4

    .line 707890
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 707891
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 707892
    :cond_4
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 707893
    if-eqz v0, :cond_5

    .line 707894
    const-string v1, "pysfItems"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 707895
    invoke-static {p0, v0, p2, p3}, LX/4R0;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 707896
    :cond_5
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 707897
    if-eqz v0, :cond_6

    .line 707898
    const-string v1, "short_term_cache_key"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 707899
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 707900
    :cond_6
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 707901
    if-eqz v0, :cond_7

    .line 707902
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 707903
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 707904
    :cond_7
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 707905
    if-eqz v0, :cond_8

    .line 707906
    const-string v1, "titleForSummary"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 707907
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 707908
    :cond_8
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 707909
    if-eqz v0, :cond_9

    .line 707910
    const-string v1, "tracking"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 707911
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 707912
    :cond_9
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 707913
    if-eqz v0, :cond_a

    .line 707914
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 707915
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 707916
    :cond_a
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 707917
    return-void
.end method
