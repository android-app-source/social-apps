.class public LX/3a5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1TE;
.implements LX/1TG;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/3a5;


# instance fields
.field private a:Ljava/util/List;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/1Cz;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 602852
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 602853
    return-void
.end method

.method public static a(LX/0QB;)LX/3a5;
    .locals 3

    .prologue
    .line 602830
    sget-object v0, LX/3a5;->b:LX/3a5;

    if-nez v0, :cond_1

    .line 602831
    const-class v1, LX/3a5;

    monitor-enter v1

    .line 602832
    :try_start_0
    sget-object v0, LX/3a5;->b:LX/3a5;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 602833
    if-eqz v2, :cond_0

    .line 602834
    :try_start_1
    new-instance v0, LX/3a5;

    invoke-direct {v0}, LX/3a5;-><init>()V

    .line 602835
    move-object v0, v0

    .line 602836
    sput-object v0, LX/3a5;->b:LX/3a5;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 602837
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 602838
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 602839
    :cond_1
    sget-object v0, LX/3a5;->b:LX/3a5;

    return-object v0

    .line 602840
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 602841
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0ja;)V
    .locals 1

    .prologue
    .line 602847
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollUnitComponentPartDefinition;->a:LX/1Cz;

    invoke-static {p1, v0}, LX/99p;->a(LX/0ja;LX/1Cz;)V

    .line 602848
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollUnitComponentPartDefinition;->b:LX/1Cz;

    invoke-static {p1, v0}, LX/99p;->a(LX/0ja;LX/1Cz;)V

    .line 602849
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPaginatedHScrollUnitComponentPartDefinition;->a:LX/1Cz;

    invoke-static {p1, v0}, LX/99p;->a(LX/0ja;LX/1Cz;)V

    .line 602850
    sget-object v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoFullWidthPartDefinition;->a:LX/1Cz;

    invoke-static {p1, v0}, LX/99p;->a(LX/0ja;LX/1Cz;)V

    .line 602851
    return-void
.end method

.method public final a(LX/1KB;)V
    .locals 16

    .prologue
    .line 602842
    move-object/from16 v0, p0

    iget-object v1, v0, LX/3a5;->a:Ljava/util/List;

    if-nez v1, :cond_0

    .line 602843
    sget-object v1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionArticleUnitComponentPartDefinition;->a:LX/1Cz;

    sget-object v2, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionActionListUnitComponentPartDefinition;->a:LX/1Cz;

    sget-object v3, Lcom/facebook/reaction/feed/rows/attachments/ReactionAttachmentsFallbackPartDefinition;->a:LX/1Cz;

    sget-object v4, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionBannerHighlightableUnitComponentPartDefinition;->b:LX/1Cz;

    sget-object v5, Lcom/facebook/reaction/feed/rows/ReactionCenterAlignedHeaderPartDefinition;->a:LX/1Cz;

    sget-object v6, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsableIconMessageUnitComponentPartDefinition;->a:LX/1Cz;

    sget-object v7, Lcom/facebook/reaction/feed/rows/ReactionCollapsableStoryIconHeaderPartDefinition;->a:LX/1Cz;

    sget-object v8, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCollapsedIconMessageUnitComponentPartDefinition;->a:LX/1Cz;

    sget-object v9, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCountsHorizontalUnitComponentPartDefinition;->a:LX/1Cz;

    sget-object v10, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCrisisActionUnitComponentPartDefinition;->a:LX/1Cz;

    sget-object v11, Lcom/facebook/reaction/feed/rows/ReactionDescriptiveHeaderPartDefinition;->a:LX/1Cz;

    sget-object v12, Lcom/facebook/reaction/feed/common/ReactionDividerUnitComponentPartDefinition;->a:LX/1Cz;

    const/16 v13, 0x3f

    new-array v13, v13, [LX/1Cz;

    const/4 v14, 0x0

    sget-object v15, Lcom/facebook/reaction/feed/rows/attachments/ReactionEventBlocksAttachmentPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/4 v14, 0x1

    sget-object v15, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionEventRowUnitComponentPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/4 v14, 0x2

    sget-object v15, Lcom/facebook/reaction/feed/rows/ReactionEventSubscribeHeaderPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/4 v14, 0x3

    sget-object v15, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionExpandableUnitComponentPromptPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/4 v14, 0x4

    sget-object v15, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionFacepileHScrollUnitComponentPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/4 v14, 0x5

    sget-object v15, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionFigActionFooterPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/4 v14, 0x6

    sget-object v15, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionFigFooterPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/4 v14, 0x7

    sget-object v15, Lcom/facebook/components/feed/ComponentPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x8

    sget-object v15, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionFormattedParagraphUnitComponentPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x9

    sget-object v15, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionGroupDescriptionPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0xa

    sget-object v15, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHeaderWithVerifiedBadgeComponentPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0xb

    sget-object v15, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollGenericComponentsListPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0xc

    sget-object v15, Lcom/facebook/reaction/feed/rows/ReactionIconHeaderPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0xd

    sget-object v15, Lcom/facebook/reaction/feed/rows/ReactionIconHeaderWithActionPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0xe

    sget-object v15, Lcom/facebook/reaction/feed/rows/ReactionIconInlineActionHeaderPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0xf

    sget-object v15, Lcom/facebook/reaction/feed/rows/ReactionIconMenuHeaderPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x10

    sget-object v15, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionIconMessageAutoActionUnitComponentPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x11

    sget-object v15, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionIconMessageUnitComponentPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x12

    sget-object v15, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionIconMessageUnitComponentWithMenuPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x13

    sget-object v15, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionIconOverMessageUnitComponentPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x14

    sget-object v15, Lcom/facebook/reaction/feed/rows/ReactionIconPivotHeaderPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x15

    sget-object v15, Lcom/facebook/reaction/feed/rows/ui/ReactionImageStoryBlockUnitComponentView;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x16

    sget-object v15, Lcom/facebook/reaction/feed/rows/attachments/ReactionImageTextBlockAttachmentPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x17

    sget-object v15, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageWithOverlayGridUnitComponentPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x18

    sget-object v15, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionInfoRowDividerPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x19

    sget-object v15, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionInfoRowUnitComponentPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x1a

    sget-object v15, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionInfoRowWithRightIconUnitComponentPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x1b

    sget-object v15, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionMapWithBreadcrumbsHeaderUnitComponentPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x1c

    sget-object v15, Lcom/facebook/components/feed/ComponentPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x1d

    sget-object v15, Lcom/facebook/reaction/feed/rows/ReactionMultiActionFooterPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x1e

    sget-object v15, Lcom/facebook/reaction/feed/rows/ReactionMultiFacepileHeaderPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x1f

    sget-object v15, Lcom/facebook/reaction/feed/rows/attachments/ReactionPageLikesAndVisitsAttachmentPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x20

    sget-object v15, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPageMapWithNavigationUnitComponentPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x21

    sget-object v15, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPaginatedHScrollUnitComponentPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x22

    sget-object v15, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoFullWidthCounterUnitComponentPartDefinition;->b:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x23

    sget-object v15, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoGridPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x24

    sget-object v15, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotosUnitComponentPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x25

    sget-object v15, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoUnitComponentPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x26

    sget-object v15, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPlaceholderPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x27

    sget-object v15, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPostPivotUnitComponentDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x28

    sget-object v15, Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryAttachmentPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x29

    sget-object v15, Lcom/facebook/reaction/feed/rows/attachments/ReactionProfileStoryWithIconAttachmentPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x2a

    sget-object v15, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionProgressBarUnitComponentPartDefinition;->b:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x2b

    sget-object v15, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionReviewComposerUnitComponentPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x2c

    sget-object v15, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionReviewUnitComponentPartDefinition;->c:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x2d

    sget-object v15, Lcom/facebook/reaction/feed/rows/attachments/ReactionSimpleLeftRightTextAttachmentPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x2e

    sget-object v15, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionSimpleTextUnitComponentPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x2f

    sget-object v15, Lcom/facebook/reaction/feed/rows/ReactionSingleActionFooterPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x30

    sget-object v15, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionSingleButtonUnitComponentPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x31

    sget-object v15, Lcom/facebook/reaction/feed/rows/ReactionSingleFacepileHeaderPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x32

    sget-object v15, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionSingleImageUnitComponentPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x33

    sget-object v15, Lcom/facebook/reaction/feed/rows/attachments/ReactionSinglePhotoAttachmentPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x34

    sget-object v15, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionStaticMapUnitComponentPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x35

    sget-object v15, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionStoryBlockUnitComponentPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x36

    sget-object v15, Lcom/facebook/reaction/feed/rows/ReactionStoryBottomWithMarginPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x37

    sget-object v15, Lcom/facebook/reaction/feed/rows/ReactionStoryTopWithMarginPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x38

    sget-object v15, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionTextWithInlineFacepileUnitComponentPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x39

    sget-object v15, Lcom/facebook/reaction/feed/rows/attachments/ReactionTopicBlocksAttachmentPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x3a

    sget-object v15, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionTwoMessageUnitComponentPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x3b

    sget-object v15, Lcom/facebook/reaction/feed/rows/ReactionUnitStackPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x3c

    sget-object v15, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalComponentsGapPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x3d

    sget-object v15, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVideoUnitComponentPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    const/16 v14, 0x3e

    sget-object v15, Lcom/facebook/components/feed/ComponentPartDefinition;->a:LX/1Cz;

    aput-object v15, v13, v14

    invoke-static/range {v1 .. v13}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, LX/3a5;->a:Ljava/util/List;

    .line 602844
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, LX/3a5;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Cz;

    .line 602845
    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, LX/1KB;->a(LX/1Cz;)V

    goto :goto_0

    .line 602846
    :cond_1
    return-void
.end method
