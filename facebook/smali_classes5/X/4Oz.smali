.class public LX/4Oz;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 699282
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 699283
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 699284
    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 699285
    if-eqz v0, :cond_0

    .line 699286
    const-string v1, "privacy_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 699287
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 699288
    :cond_0
    invoke-virtual {p0, p1, v3, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 699289
    if-eqz v0, :cond_1

    .line 699290
    const-string v0, "privacy_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 699291
    const-class v0, Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;

    invoke-virtual {p0, p1, v3, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 699292
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 699293
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 699294
    const/4 v0, 0x0

    .line 699295
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v4, :cond_5

    .line 699296
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 699297
    :goto_0
    return v1

    .line 699298
    :cond_0
    const-string v6, "privacy_type"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 699299
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;

    move-result-object v0

    move-object v3, v0

    move v0, v2

    .line 699300
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_3

    .line 699301
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 699302
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 699303
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_1

    if-eqz v5, :cond_1

    .line 699304
    const-string v6, "privacy_text"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 699305
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 699306
    :cond_2
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 699307
    :cond_3
    const/4 v5, 0x2

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 699308
    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 699309
    if-eqz v0, :cond_4

    .line 699310
    invoke-virtual {p1, v2, v3}, LX/186;->a(ILjava/lang/Enum;)V

    .line 699311
    :cond_4
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move-object v3, v0

    move v4, v1

    move v0, v1

    goto :goto_1
.end method
