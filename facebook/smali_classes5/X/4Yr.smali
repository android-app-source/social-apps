.class public final LX/4Yr;
.super LX/40T;
.source ""

# interfaces
.implements LX/40U;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/40T;",
        "LX/40U",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(LX/4VK;)V
    .locals 0

    .prologue
    .line 784373
    invoke-direct {p0, p1}, LX/40T;-><init>(LX/4VK;)V

    .line 784374
    return-void
.end method


# virtual methods
.method public final a(LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 784375
    iget-object v0, p0, LX/40T;->a:LX/4VK;

    const-string v1, "attachments"

    invoke-virtual {v0, v1, p1}, LX/4VK;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 784376
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 784377
    iget-object v0, p0, LX/40T;->a:LX/4VK;

    const-string v1, "local_story_visibility"

    invoke-virtual {v0, v1, p1}, LX/4VK;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 784378
    return-void
.end method
