.class public LX/4ot;
.super LX/4or;
.source ""


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0dN;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 810466
    invoke-direct {p0, p1}, LX/4or;-><init>(Landroid/content/Context;)V

    .line 810467
    new-instance v0, LX/4os;

    invoke-direct {v0, p0}, LX/4os;-><init>(LX/4ot;)V

    iput-object v0, p0, LX/4ot;->b:LX/0dN;

    .line 810468
    invoke-virtual {p0}, Landroid/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    check-cast p0, LX/4ot;

    const/16 v0, 0xf9a

    invoke-static {p1, v0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p1

    iput-object p1, p0, LX/4ot;->a:LX/0Or;

    .line 810469
    return-void
.end method

.method public static b(LX/4ot;)V
    .locals 2

    .prologue
    .line 810470
    invoke-virtual {p0}, LX/4ot;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/4ot;->findIndexOfValue(Ljava/lang/String;)I

    move-result v0

    .line 810471
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 810472
    const/4 v0, 0x0

    .line 810473
    :cond_0
    invoke-virtual {p0}, LX/4ot;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v1

    aget-object v0, v1, v0

    invoke-virtual {p0, v0}, LX/4ot;->setSummary(Ljava/lang/CharSequence;)V

    .line 810474
    return-void
.end method


# virtual methods
.method public final onBindView(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 810475
    invoke-static {p0}, LX/4ot;->b(LX/4ot;)V

    .line 810476
    invoke-super {p0, p1}, LX/4or;->onBindView(Landroid/view/View;)V

    .line 810477
    return-void
.end method
