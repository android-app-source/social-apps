.class public LX/4e7;
.super LX/1ln;
.source ""


# instance fields
.field private a:LX/4dO;


# direct methods
.method public constructor <init>(LX/4dO;)V
    .locals 0

    .prologue
    .line 796644
    invoke-direct {p0}, LX/1ln;-><init>()V

    .line 796645
    iput-object p1, p0, LX/4e7;->a:LX/4dO;

    .line 796646
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()LX/4dO;
    .locals 1

    .prologue
    .line 796643
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/4e7;->a:LX/4dO;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()I
    .locals 2

    .prologue
    .line 796640
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LX/1ln;->c()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    iget-object v0, p0, LX/4e7;->a:LX/4dO;

    .line 796641
    iget-object v1, v0, LX/4dO;->a:LX/1GB;

    move-object v0, v1

    .line 796642
    invoke-interface {v0}, LX/1GB;->g()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()Z
    .locals 1

    .prologue
    .line 796639
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/4e7;->a:LX/4dO;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final close()V
    .locals 2

    .prologue
    .line 796630
    monitor-enter p0

    .line 796631
    :try_start_0
    iget-object v0, p0, LX/4e7;->a:LX/4dO;

    if-nez v0, :cond_0

    .line 796632
    monitor-exit p0

    .line 796633
    :goto_0
    return-void

    .line 796634
    :cond_0
    iget-object v0, p0, LX/4e7;->a:LX/4dO;

    .line 796635
    const/4 v1, 0x0

    iput-object v1, p0, LX/4e7;->a:LX/4dO;

    .line 796636
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 796637
    invoke-virtual {v0}, LX/4dO;->d()V

    goto :goto_0

    .line 796638
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 796623
    const/4 v0, 0x1

    return v0
.end method

.method public final declared-synchronized g()I
    .locals 2

    .prologue
    .line 796627
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LX/1ln;->c()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    iget-object v0, p0, LX/4e7;->a:LX/4dO;

    .line 796628
    iget-object v1, v0, LX/4dO;->a:LX/1GB;

    move-object v0, v1

    .line 796629
    invoke-interface {v0}, LX/1GB;->a()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized h()I
    .locals 2

    .prologue
    .line 796624
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LX/1ln;->c()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    iget-object v0, p0, LX/4e7;->a:LX/4dO;

    .line 796625
    iget-object v1, v0, LX/4dO;->a:LX/1GB;

    move-object v0, v1

    .line 796626
    invoke-interface {v0}, LX/1GB;->b()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
