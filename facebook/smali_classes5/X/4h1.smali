.class public LX/4h1;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/4h1;


# instance fields
.field public final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 800351
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 800352
    iput-object p1, p0, LX/4h1;->a:Landroid/content/Context;

    .line 800353
    return-void
.end method

.method public static a(LX/0QB;)LX/4h1;
    .locals 4

    .prologue
    .line 800354
    sget-object v0, LX/4h1;->b:LX/4h1;

    if-nez v0, :cond_1

    .line 800355
    const-class v1, LX/4h1;

    monitor-enter v1

    .line 800356
    :try_start_0
    sget-object v0, LX/4h1;->b:LX/4h1;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 800357
    if-eqz v2, :cond_0

    .line 800358
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 800359
    new-instance p0, LX/4h1;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {p0, v3}, LX/4h1;-><init>(Landroid/content/Context;)V

    .line 800360
    move-object v0, p0

    .line 800361
    sput-object v0, LX/4h1;->b:LX/4h1;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 800362
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 800363
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 800364
    :cond_1
    sget-object v0, LX/4h1;->b:LX/4h1;

    return-object v0

    .line 800365
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 800366
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
