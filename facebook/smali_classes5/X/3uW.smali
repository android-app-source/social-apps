.class public final LX/3uW;
.super LX/3uV;
.source ""

# interfaces
.implements LX/3u7;


# instance fields
.field public final synthetic a:LX/3uZ;

.field private final b:Landroid/content/Context;

.field private final c:LX/3v0;

.field private d:LX/3uG;

.field private e:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/3uZ;Landroid/content/Context;LX/3uG;)V
    .locals 2

    .prologue
    .line 648556
    iput-object p1, p0, LX/3uW;->a:LX/3uZ;

    invoke-direct {p0}, LX/3uV;-><init>()V

    .line 648557
    iput-object p2, p0, LX/3uW;->b:Landroid/content/Context;

    .line 648558
    iput-object p3, p0, LX/3uW;->d:LX/3uG;

    .line 648559
    new-instance v0, LX/3v0;

    invoke-direct {v0, p2}, LX/3v0;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x1

    .line 648560
    iput v1, v0, LX/3v0;->p:I

    .line 648561
    move-object v0, v0

    .line 648562
    iput-object v0, p0, LX/3uW;->c:LX/3v0;

    .line 648563
    iget-object v0, p0, LX/3uW;->c:LX/3v0;

    invoke-virtual {v0, p0}, LX/3v0;->a(LX/3u7;)V

    .line 648564
    return-void
.end method


# virtual methods
.method public final a()Landroid/view/MenuInflater;
    .locals 2

    .prologue
    .line 648555
    new-instance v0, LX/3ui;

    iget-object v1, p0, LX/3uW;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/3ui;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 648553
    iget-object v0, p0, LX/3uW;->a:LX/3uZ;

    iget-object v0, v0, LX/3uZ;->j:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/3uV;->b(Ljava/lang/CharSequence;)V

    .line 648554
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 648550
    iget-object v0, p0, LX/3uW;->a:LX/3uZ;

    iget-object v0, v0, LX/3uZ;->q:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v0, p1}, Landroid/support/v7/internal/widget/ActionBarContextView;->setCustomView(Landroid/view/View;)V

    .line 648551
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/3uW;->e:Ljava/lang/ref/WeakReference;

    .line 648552
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 648548
    iget-object v0, p0, LX/3uW;->a:LX/3uZ;

    iget-object v0, v0, LX/3uZ;->q:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v0, p1}, Landroid/support/v7/internal/widget/ActionBarContextView;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 648549
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 648518
    invoke-super {p0, p1}, LX/3uV;->a(Z)V

    .line 648519
    iget-object v0, p0, LX/3uW;->a:LX/3uZ;

    iget-object v0, v0, LX/3uZ;->q:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v0, p1}, Landroid/support/v7/internal/widget/ActionBarContextView;->setTitleOptional(Z)V

    .line 648520
    return-void
.end method

.method public final a(LX/3v0;Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 648545
    iget-object v0, p0, LX/3uW;->d:LX/3uG;

    if-eqz v0, :cond_0

    .line 648546
    iget-object v0, p0, LX/3uW;->d:LX/3uG;

    invoke-interface {v0, p0, p2}, LX/3uG;->a(LX/3uV;Landroid/view/MenuItem;)Z

    move-result v0

    .line 648547
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Landroid/view/Menu;
    .locals 1

    .prologue
    .line 648544
    iget-object v0, p0, LX/3uW;->c:LX/3v0;

    return-object v0
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 648542
    iget-object v0, p0, LX/3uW;->a:LX/3uZ;

    iget-object v0, v0, LX/3uZ;->j:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/3uV;->a(Ljava/lang/CharSequence;)V

    .line 648543
    return-void
.end method

.method public final b(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 648565
    iget-object v0, p0, LX/3uW;->a:LX/3uZ;

    iget-object v0, v0, LX/3uZ;->q:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v0, p1}, Landroid/support/v7/internal/widget/ActionBarContextView;->setTitle(Ljava/lang/CharSequence;)V

    .line 648566
    return-void
.end method

.method public final b_(LX/3v0;)V
    .locals 1

    .prologue
    .line 648538
    iget-object v0, p0, LX/3uW;->d:LX/3uG;

    if-nez v0, :cond_0

    .line 648539
    :goto_0
    return-void

    .line 648540
    :cond_0
    invoke-virtual {p0}, LX/3uV;->d()V

    .line 648541
    iget-object v0, p0, LX/3uW;->a:LX/3uZ;

    iget-object v0, v0, LX/3uZ;->q:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->a()Z

    goto :goto_0
.end method

.method public final c()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 648526
    iget-object v0, p0, LX/3uW;->a:LX/3uZ;

    iget-object v0, v0, LX/3uZ;->a:LX/3uW;

    if-eq v0, p0, :cond_0

    .line 648527
    :goto_0
    return-void

    .line 648528
    :cond_0
    iget-object v0, p0, LX/3uW;->a:LX/3uZ;

    iget-boolean v0, v0, LX/3uZ;->E:Z

    iget-object v1, p0, LX/3uW;->a:LX/3uZ;

    iget-boolean v1, v1, LX/3uZ;->F:Z

    invoke-static {v0, v1, v2}, LX/3uZ;->b(ZZZ)Z

    move-result v0

    if-nez v0, :cond_1

    .line 648529
    iget-object v0, p0, LX/3uW;->a:LX/3uZ;

    iput-object p0, v0, LX/3uZ;->b:LX/3uV;

    .line 648530
    iget-object v0, p0, LX/3uW;->a:LX/3uZ;

    iget-object v1, p0, LX/3uW;->d:LX/3uG;

    iput-object v1, v0, LX/3uZ;->c:LX/3uG;

    .line 648531
    :goto_1
    iput-object v3, p0, LX/3uW;->d:LX/3uG;

    .line 648532
    iget-object v0, p0, LX/3uW;->a:LX/3uZ;

    invoke-virtual {v0, v2}, LX/3uZ;->g(Z)V

    .line 648533
    iget-object v0, p0, LX/3uW;->a:LX/3uZ;

    iget-object v0, v0, LX/3uZ;->q:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->b()V

    .line 648534
    iget-object v0, p0, LX/3uW;->a:LX/3uZ;

    iget-object v0, v0, LX/3uZ;->p:LX/3vk;

    invoke-interface {v0}, LX/3vk;->a()Landroid/view/ViewGroup;

    move-result-object v0

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->sendAccessibilityEvent(I)V

    .line 648535
    iget-object v0, p0, LX/3uW;->a:LX/3uZ;

    iget-object v0, v0, LX/3uZ;->n:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    iget-object v1, p0, LX/3uW;->a:LX/3uZ;

    iget-boolean v1, v1, LX/3uZ;->d:Z

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->setHideOnContentScrollEnabled(Z)V

    .line 648536
    iget-object v0, p0, LX/3uW;->a:LX/3uZ;

    iput-object v3, v0, LX/3uZ;->a:LX/3uW;

    goto :goto_0

    .line 648537
    :cond_1
    iget-object v0, p0, LX/3uW;->d:LX/3uG;

    invoke-interface {v0, p0}, LX/3uG;->a(LX/3uV;)V

    goto :goto_1
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 648521
    iget-object v0, p0, LX/3uW;->a:LX/3uZ;

    iget-object v0, v0, LX/3uZ;->a:LX/3uW;

    if-eq v0, p0, :cond_0

    .line 648522
    :goto_0
    return-void

    .line 648523
    :cond_0
    iget-object v0, p0, LX/3uW;->c:LX/3v0;

    invoke-virtual {v0}, LX/3v0;->f()V

    .line 648524
    :try_start_0
    iget-object v0, p0, LX/3uW;->d:LX/3uG;

    iget-object v1, p0, LX/3uW;->c:LX/3v0;

    invoke-interface {v0, p0, v1}, LX/3uG;->b(LX/3uV;Landroid/view/Menu;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 648525
    iget-object v0, p0, LX/3uW;->c:LX/3v0;

    invoke-virtual {v0}, LX/3v0;->g()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/3uW;->c:LX/3v0;

    invoke-virtual {v1}, LX/3v0;->g()V

    throw v0
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 648515
    iget-object v0, p0, LX/3uW;->c:LX/3v0;

    invoke-virtual {v0}, LX/3v0;->f()V

    .line 648516
    :try_start_0
    iget-object v0, p0, LX/3uW;->d:LX/3uG;

    iget-object v1, p0, LX/3uW;->c:LX/3v0;

    invoke-interface {v0, p0, v1}, LX/3uG;->a(LX/3uV;Landroid/view/Menu;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 648517
    iget-object v1, p0, LX/3uW;->c:LX/3v0;

    invoke-virtual {v1}, LX/3v0;->g()V

    return v0

    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/3uW;->c:LX/3v0;

    invoke-virtual {v1}, LX/3v0;->g()V

    throw v0
.end method

.method public final f()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 648512
    iget-object v0, p0, LX/3uW;->a:LX/3uZ;

    iget-object v0, v0, LX/3uZ;->q:Landroid/support/v7/internal/widget/ActionBarContextView;

    .line 648513
    iget-object p0, v0, Landroid/support/v7/internal/widget/ActionBarContextView;->j:Ljava/lang/CharSequence;

    move-object v0, p0

    .line 648514
    return-object v0
.end method

.method public final g()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 648509
    iget-object v0, p0, LX/3uW;->a:LX/3uZ;

    iget-object v0, v0, LX/3uZ;->q:Landroid/support/v7/internal/widget/ActionBarContextView;

    .line 648510
    iget-object p0, v0, Landroid/support/v7/internal/widget/ActionBarContextView;->k:Ljava/lang/CharSequence;

    move-object v0, p0

    .line 648511
    return-object v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 648506
    iget-object v0, p0, LX/3uW;->a:LX/3uZ;

    iget-object v0, v0, LX/3uZ;->q:Landroid/support/v7/internal/widget/ActionBarContextView;

    .line 648507
    iget-boolean p0, v0, Landroid/support/v7/internal/widget/ActionBarContextView;->t:Z

    move v0, p0

    .line 648508
    return v0
.end method

.method public final i()Landroid/view/View;
    .locals 1

    .prologue
    .line 648505
    iget-object v0, p0, LX/3uW;->e:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3uW;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
