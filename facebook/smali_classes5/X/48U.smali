.class public LX/48U;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final b:LX/2QP;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2QP",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Landroid/net/Uri;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 672913
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "api.instagram.com"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "www.tumblr.com"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "pinterest.com"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "accounts.spotify.com"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "www.pinterest.com"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "www.flickr.com"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "api.flickr.com"

    aput-object v2, v0, v1

    invoke-static {v0}, LX/2QP;->a([Ljava/lang/Object;)LX/2QP;

    move-result-object v0

    sput-object v0, LX/48U;->b:LX/2QP;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 672902
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 672903
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 672904
    const/4 v1, 0x0

    .line 672905
    sget-object v2, LX/48U;->b:LX/2QP;

    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/2QP;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 672906
    :cond_0
    :goto_0
    move v1, v1

    .line 672907
    if-eqz v1, :cond_1

    .line 672908
    iput-object v0, p0, LX/48U;->a:Landroid/net/Uri;

    return-void

    .line 672909
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid auth url accessed "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 672910
    :cond_2
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "oauth"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "authorize"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 672911
    :cond_3
    const/4 v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 672912
    iget-object v0, p0, LX/48U;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
