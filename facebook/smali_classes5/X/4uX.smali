.class public LX/4uX;
.super LX/4uO;
.source ""


# virtual methods
.method public final a(Lcom/google/android/gms/common/ConnectionResult;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {v0, p1, p2}, LX/4um;->b(Lcom/google/android/gms/common/ConnectionResult;I)V

    return-void
.end method

.method public final c()V
    .locals 9

    const/4 v2, 0x0

    invoke-super {p0}, LX/4uO;->c()V

    invoke-virtual {v2}, LX/4tT;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4sV;

    const/4 v3, 0x1

    iget-object v4, v0, LX/4sV;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v4, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v4

    if-eqz v4, :cond_1

    :goto_1
    goto :goto_0

    :cond_0
    invoke-virtual {v2}, LX/4tT;->clear()V

    invoke-virtual {v2, p0}, LX/4um;->a(LX/4uX;)V

    return-void

    :cond_1
    iget-object v4, v0, LX/4sV;->b:LX/2wZ;

    invoke-virtual {v4}, LX/2wZ;->a()V

    iget-object v4, v0, LX/4sV;->g:LX/4um;

    iget v5, v0, LX/4sV;->f:I

    iget-object v6, v0, LX/4sV;->i:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v6}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v6

    if-lez v6, :cond_2

    :goto_2
    iget-object v7, v4, LX/4um;->m:Landroid/os/Handler;

    iget-object v8, v4, LX/4um;->m:Landroid/os/Handler;

    const/4 v0, 0x7

    if-eqz v3, :cond_3

    const/4 v6, 0x1

    :goto_3
    invoke-virtual {v8, v0, v5, v6}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v6

    invoke-virtual {v7, v6}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_1

    :cond_2
    const/4 v3, 0x0

    goto :goto_2

    :cond_3
    const/4 v6, 0x2

    goto :goto_3
.end method

.method public final d()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {v0}, LX/4um;->b()V

    return-void
.end method
