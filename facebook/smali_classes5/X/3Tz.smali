.class public LX/3Tz;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/23R;


# direct methods
.method public constructor <init>(LX/23R;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 585293
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 585294
    iput-object p1, p0, LX/3Tz;->a:LX/23R;

    .line 585295
    return-void
.end method

.method public static b(LX/0QB;)LX/3Tz;
    .locals 2

    .prologue
    .line 585296
    new-instance v1, LX/3Tz;

    invoke-static {p0}, Lcom/facebook/photos/mediagallery/ui/DefaultMediaGalleryLauncher;->a(LX/0QB;)Lcom/facebook/photos/mediagallery/ui/DefaultMediaGalleryLauncher;

    move-result-object v0

    check-cast v0, LX/23R;

    invoke-direct {v1, v0}, LX/3Tz;-><init>(LX/23R;)V

    .line 585297
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;Ljava/lang/String;Lcom/facebook/drawee/view/DraweeView;LX/1bf;Landroid/content/Context;)V
    .locals 8

    .prologue
    .line 585298
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 585299
    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;->b()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel$EdgesModel;

    .line 585300
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel$EdgesModel;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel$EdgesModel;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->E()LX/1U8;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 585301
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel$EdgesModel;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->E()LX/1U8;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 585302
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 585303
    :cond_1
    move-object v2, v2

    .line 585304
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v7, p6

    invoke-virtual/range {v0 .. v7}, LX/3Tz;->a(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Lcom/facebook/drawee/view/DraweeView;LX/1bf;LX/9hM;Landroid/content/Context;)V

    .line 585305
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Lcom/facebook/drawee/view/DraweeView;LX/1bf;LX/9hM;Landroid/content/Context;)V
    .locals 3
    .param p6    # LX/9hM;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<+",
            "LX/1U8;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/facebook/drawee/view/DraweeView;",
            "LX/1bf;",
            "LX/9hM;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .prologue
    .line 585306
    invoke-static {p1, p2}, LX/9hF;->a(Ljava/lang/String;Ljava/util/List;)LX/9hE;

    move-result-object v0

    sget-object v1, LX/74S;->REACTION_PHOTO_ITEM:LX/74S;

    invoke-virtual {v0, v1}, LX/9hD;->a(LX/74S;)LX/9hD;

    move-result-object v0

    const/4 v1, 0x1

    .line 585307
    iput-boolean v1, v0, LX/9hD;->m:Z

    .line 585308
    move-object v0, v0

    .line 585309
    iput-object p6, v0, LX/9hD;->u:LX/9hM;

    .line 585310
    move-object v0, v0

    .line 585311
    invoke-virtual {v0, p3}, LX/9hD;->a(Ljava/lang/String;)LX/9hD;

    move-result-object v0

    invoke-virtual {v0, p5}, LX/9hD;->a(LX/1bf;)LX/9hD;

    move-result-object v0

    .line 585312
    new-instance v1, LX/CfQ;

    invoke-direct {v1, p0, p3, p4, p5}, LX/CfQ;-><init>(LX/3Tz;Ljava/lang/String;Lcom/facebook/drawee/view/DraweeView;LX/1bf;)V

    .line 585313
    iget-object v2, p0, LX/3Tz;->a:LX/23R;

    invoke-virtual {v0}, LX/9hD;->b()Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    move-result-object v0

    invoke-interface {v2, p7, v0, v1}, LX/23R;->a(Landroid/content/Context;Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;LX/9hN;)V

    .line 585314
    return-void
.end method
