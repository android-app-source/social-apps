.class public abstract LX/51j;
.super LX/51i;
.source ""


# instance fields
.field private final a:Ljava/nio/ByteBuffer;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 825378
    invoke-direct {p0}, LX/51i;-><init>()V

    .line 825379
    const/16 v0, 0x8

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    sget-object v1, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, LX/51j;->a:Ljava/nio/ByteBuffer;

    return-void
.end method

.method private b(I)LX/51h;
    .locals 2

    .prologue
    .line 825359
    :try_start_0
    iget-object v0, p0, LX/51j;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1, p1}, LX/51j;->a([BII)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 825360
    iget-object v0, p0, LX/51j;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 825361
    return-object p0

    .line 825362
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/51j;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    throw v0
.end method


# virtual methods
.method public final a(C)LX/51h;
    .locals 1

    .prologue
    .line 825380
    iget-object v0, p0, LX/51j;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->putChar(C)Ljava/nio/ByteBuffer;

    .line 825381
    const/4 v0, 0x2

    invoke-direct {p0, v0}, LX/51j;->b(I)LX/51h;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)LX/51h;
    .locals 1

    .prologue
    .line 825384
    iget-object v0, p0, LX/51j;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 825385
    const/4 v0, 0x4

    invoke-direct {p0, v0}, LX/51j;->b(I)LX/51h;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)LX/51h;
    .locals 1

    .prologue
    .line 825382
    iget-object v0, p0, LX/51j;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1, p2}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    .line 825383
    const/16 v0, 0x8

    invoke-direct {p0, v0}, LX/51j;->b(I)LX/51h;

    move-result-object v0

    return-object v0
.end method

.method public abstract a(B)V
.end method

.method public a([B)V
    .locals 2

    .prologue
    .line 825376
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, LX/51j;->a([BII)V

    .line 825377
    return-void
.end method

.method public a([BII)V
    .locals 2

    .prologue
    .line 825372
    move v0, p2

    :goto_0
    add-int v1, p2, p3

    if-ge v0, v1, :cond_0

    .line 825373
    aget-byte v1, p1, v0

    invoke-virtual {p0, v1}, LX/51j;->a(B)V

    .line 825374
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 825375
    :cond_0
    return-void
.end method

.method public final b([BII)LX/51g;
    .locals 2

    .prologue
    .line 825369
    add-int v0, p2, p3

    array-length v1, p1

    invoke-static {p2, v0, v1}, LX/0PB;->checkPositionIndexes(III)V

    .line 825370
    invoke-virtual {p0, p1, p2, p3}, LX/51j;->a([BII)V

    .line 825371
    return-object p0
.end method

.method public final b(B)LX/51h;
    .locals 0

    .prologue
    .line 825367
    invoke-virtual {p0, p1}, LX/51j;->a(B)V

    .line 825368
    return-object p0
.end method

.method public final b([B)LX/51h;
    .locals 0

    .prologue
    .line 825364
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 825365
    invoke-virtual {p0, p1}, LX/51j;->a([B)V

    .line 825366
    return-object p0
.end method

.method public final synthetic c(B)LX/51g;
    .locals 1

    .prologue
    .line 825363
    invoke-virtual {p0, p1}, LX/51j;->b(B)LX/51h;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c([B)LX/51g;
    .locals 1

    .prologue
    .line 825358
    invoke-virtual {p0, p1}, LX/51j;->b([B)LX/51h;

    move-result-object v0

    return-object v0
.end method
