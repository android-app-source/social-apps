.class public LX/4lv;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static a:Ljava/lang/reflect/Field;

.field public static b:Z

.field private static volatile c:LX/4lv;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 804990
    const/4 v0, 0x0

    sput-boolean v0, LX/4lv;->b:Z

    .line 804991
    :try_start_0
    const-string v0, "org.apache.harmony.xnet.provider.jsse.SSLParametersImpl"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    .line 804992
    const-string v0, "org.apache.harmony.xnet.provider.jsse.ClientSessionContext"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    .line 804993
    const-class v0, Lorg/apache/harmony/xnet/provider/jsse/SSLParametersImpl;

    const-string v1, "clientSessionContext"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 804994
    sput-object v0, LX/4lv;->a:Ljava/lang/reflect/Field;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 804995
    const/4 v0, 0x1

    sput-boolean v0, LX/4lv;->b:Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 804996
    :goto_0
    return-void

    :catch_0
    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 804997
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 804998
    return-void
.end method

.method public static a(LX/0QB;)LX/4lv;
    .locals 3

    .prologue
    .line 804999
    sget-object v0, LX/4lv;->c:LX/4lv;

    if-nez v0, :cond_1

    .line 805000
    const-class v1, LX/4lv;

    monitor-enter v1

    .line 805001
    :try_start_0
    sget-object v0, LX/4lv;->c:LX/4lv;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 805002
    if-eqz v2, :cond_0

    .line 805003
    :try_start_1
    new-instance v0, LX/4lv;

    invoke-direct {v0}, LX/4lv;-><init>()V

    .line 805004
    move-object v0, v0

    .line 805005
    sput-object v0, LX/4lv;->c:LX/4lv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 805006
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 805007
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 805008
    :cond_1
    sget-object v0, LX/4lv;->c:LX/4lv;

    return-object v0

    .line 805009
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 805010
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Lorg/apache/harmony/xnet/provider/jsse/SSLParametersImpl;I)V
    .locals 2

    .prologue
    .line 805011
    :try_start_0
    sget-object v0, LX/4lv;->a:Ljava/lang/reflect/Field;

    invoke-virtual {v0, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/harmony/xnet/provider/jsse/ClientSessionContext;

    .line 805012
    invoke-virtual {v0, p1}, Lorg/apache/harmony/xnet/provider/jsse/ClientSessionContext;->setSessionTimeout(I)V
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    .line 805013
    return-void

    .line 805014
    :catch_0
    move-exception v0

    .line 805015
    new-instance v1, LX/4ll;

    invoke-direct {v1, v0}, LX/4ll;-><init>(Ljava/lang/Exception;)V

    throw v1
.end method
