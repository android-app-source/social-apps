.class public LX/4Re;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 709534
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 709535
    const/4 v0, 0x0

    .line 709536
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v4, :cond_b

    .line 709537
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 709538
    :goto_0
    return v1

    .line 709539
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_6

    .line 709540
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 709541
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 709542
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_0

    if-eqz v11, :cond_0

    .line 709543
    const-string v12, "is_currently_selected"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 709544
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v5

    move v10, v5

    move v5, v2

    goto :goto_1

    .line 709545
    :cond_1
    const-string v12, "is_most_recent"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 709546
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v4

    move v9, v4

    move v4, v2

    goto :goto_1

    .line 709547
    :cond_2
    const-string v12, "is_primary"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 709548
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v3

    move v8, v3

    move v3, v2

    goto :goto_1

    .line 709549
    :cond_3
    const-string v12, "node"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 709550
    invoke-static {p0, p1}, LX/39L;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 709551
    :cond_4
    const-string v12, "option_type"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 709552
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    move-result-object v0

    move-object v6, v0

    move v0, v2

    goto :goto_1

    .line 709553
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 709554
    :cond_6
    const/4 v11, 0x5

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 709555
    if-eqz v5, :cond_7

    .line 709556
    invoke-virtual {p1, v1, v10}, LX/186;->a(IZ)V

    .line 709557
    :cond_7
    if-eqz v4, :cond_8

    .line 709558
    invoke-virtual {p1, v2, v9}, LX/186;->a(IZ)V

    .line 709559
    :cond_8
    if-eqz v3, :cond_9

    .line 709560
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v8}, LX/186;->a(IZ)V

    .line 709561
    :cond_9
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 709562
    if-eqz v0, :cond_a

    .line 709563
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v6}, LX/186;->a(ILjava/lang/Enum;)V

    .line 709564
    :cond_a
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_b
    move v3, v1

    move v4, v1

    move v5, v1

    move-object v6, v0

    move v7, v1

    move v8, v1

    move v9, v1

    move v10, v1

    move v0, v1

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x0

    .line 709565
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 709566
    invoke-virtual {p0, p1, v2}, LX/15i;->b(II)Z

    move-result v0

    .line 709567
    if-eqz v0, :cond_0

    .line 709568
    const-string v1, "is_currently_selected"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 709569
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 709570
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 709571
    if-eqz v0, :cond_1

    .line 709572
    const-string v1, "is_most_recent"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 709573
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 709574
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 709575
    if-eqz v0, :cond_2

    .line 709576
    const-string v1, "is_primary"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 709577
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 709578
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 709579
    if-eqz v0, :cond_3

    .line 709580
    const-string v1, "node"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 709581
    invoke-static {p0, v0, p2, p3}, LX/39L;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 709582
    :cond_3
    invoke-virtual {p0, p1, v3, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 709583
    if-eqz v0, :cond_4

    .line 709584
    const-string v0, "option_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 709585
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    invoke-virtual {p0, p1, v3, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 709586
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 709587
    return-void
.end method
