.class public LX/4MH;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 687328
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 12

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 687329
    const/4 v0, 0x0

    .line 687330
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v4, :cond_9

    .line 687331
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 687332
    :goto_0
    return v1

    .line 687333
    :cond_0
    const-string v10, "order"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 687334
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v3

    move v5, v3

    move v3, v2

    .line 687335
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_6

    .line 687336
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 687337
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 687338
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_1

    if-eqz v9, :cond_1

    .line 687339
    const-string v10, "amount"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 687340
    invoke-static {p0, p1}, LX/4Lf;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 687341
    :cond_2
    const-string v10, "charge_type"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 687342
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 687343
    :cond_3
    const-string v10, "label"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 687344
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 687345
    :cond_4
    const-string v10, "fee_type"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 687346
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLEventTicketFeeType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEventTicketFeeType;

    move-result-object v0

    move-object v4, v0

    move v0, v2

    goto :goto_1

    .line 687347
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 687348
    :cond_6
    const/4 v9, 0x5

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 687349
    invoke-virtual {p1, v1, v8}, LX/186;->b(II)V

    .line 687350
    invoke-virtual {p1, v2, v7}, LX/186;->b(II)V

    .line 687351
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v6}, LX/186;->b(II)V

    .line 687352
    if-eqz v3, :cond_7

    .line 687353
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v5, v1}, LX/186;->a(III)V

    .line 687354
    :cond_7
    if-eqz v0, :cond_8

    .line 687355
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->a(ILjava/lang/Enum;)V

    .line 687356
    :cond_8
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_9
    move v3, v1

    move-object v4, v0

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    move v0, v1

    goto/16 :goto_1
.end method
