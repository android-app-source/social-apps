.class public LX/42p;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Intent;


# direct methods
.method public constructor <init>(Ljava/lang/Class;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/base/fragment/NavigableFragment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 668147
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 668148
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.facebook.fragment.FRAGMENT_ACTION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Class;->getPackage()Ljava/lang/Package;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Package;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, LX/42p;->a:Landroid/content/Intent;

    .line 668149
    return-void
.end method


# virtual methods
.method public final a()LX/42p;
    .locals 3

    .prologue
    .line 668150
    iget-object v0, p0, LX/42p;->a:Landroid/content/Intent;

    const-string v1, "com.facebook.fragment.PUSH_BACK_STACK"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 668151
    return-object p0
.end method

.method public final a(IIII)LX/42p;
    .locals 2

    .prologue
    .line 668152
    iget-object v0, p0, LX/42p;->a:Landroid/content/Intent;

    const-string v1, "com.facebook.fragment.ENTER_ANIM"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 668153
    iget-object v0, p0, LX/42p;->a:Landroid/content/Intent;

    const-string v1, "com.facebook.fragment.EXIT_ANIM"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 668154
    iget-object v0, p0, LX/42p;->a:Landroid/content/Intent;

    const-string v1, "com.facebook.fragment.POP_ENTER_ANIM"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 668155
    iget-object v0, p0, LX/42p;->a:Landroid/content/Intent;

    const-string v1, "com.facebook.fragment.POP_EXIT_ANIM"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 668156
    return-object p0
.end method

.method public final b()LX/42p;
    .locals 3

    .prologue
    .line 668157
    iget-object v0, p0, LX/42p;->a:Landroid/content/Intent;

    const-string v1, "com.facebook.fragment.CLEAR_BACK_STACK"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 668158
    return-object p0
.end method
