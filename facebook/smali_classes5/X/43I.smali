.class public final enum LX/43I;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/43I;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/43I;

.field public static final enum NO_ROTATION:LX/43I;

.field public static final enum ROTATE_TO_0:LX/43I;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 668616
    new-instance v0, LX/43I;

    const-string v1, "ROTATE_TO_0"

    invoke-direct {v0, v1, v2}, LX/43I;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/43I;->ROTATE_TO_0:LX/43I;

    .line 668617
    new-instance v0, LX/43I;

    const-string v1, "NO_ROTATION"

    invoke-direct {v0, v1, v3}, LX/43I;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/43I;->NO_ROTATION:LX/43I;

    .line 668618
    const/4 v0, 0x2

    new-array v0, v0, [LX/43I;

    sget-object v1, LX/43I;->ROTATE_TO_0:LX/43I;

    aput-object v1, v0, v2

    sget-object v1, LX/43I;->NO_ROTATION:LX/43I;

    aput-object v1, v0, v3

    sput-object v0, LX/43I;->$VALUES:[LX/43I;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 668619
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/43I;
    .locals 1

    .prologue
    .line 668620
    const-class v0, LX/43I;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/43I;

    return-object v0
.end method

.method public static values()[LX/43I;
    .locals 1

    .prologue
    .line 668621
    sget-object v0, LX/43I;->$VALUES:[LX/43I;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/43I;

    return-object v0
.end method
