.class public LX/3vG;
.super LX/3v0;
.source ""

# interfaces
.implements Landroid/view/SubMenu;


# instance fields
.field public d:LX/3v0;

.field private e:LX/3v3;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/3v0;LX/3v3;)V
    .locals 0

    .prologue
    .line 651051
    invoke-direct {p0, p1}, LX/3v0;-><init>(Landroid/content/Context;)V

    .line 651052
    iput-object p2, p0, LX/3vG;->d:LX/3v0;

    .line 651053
    iput-object p3, p0, LX/3vG;->e:LX/3v3;

    .line 651054
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 3

    .prologue
    .line 651045
    iget-object v0, p0, LX/3vG;->e:LX/3v3;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3vG;->e:LX/3v3;

    invoke-virtual {v0}, LX/3v3;->getItemId()I

    move-result v0

    .line 651046
    :goto_0
    if-nez v0, :cond_1

    .line 651047
    const/4 v0, 0x0

    .line 651048
    :goto_1
    return-object v0

    .line 651049
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 651050
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, LX/3v0;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public final a(LX/3u7;)V
    .locals 1

    .prologue
    .line 651043
    iget-object v0, p0, LX/3vG;->d:LX/3v0;

    invoke-virtual {v0, p1}, LX/3v0;->a(LX/3u7;)V

    .line 651044
    return-void
.end method

.method public final a(LX/3v0;Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 651042
    invoke-super {p0, p1, p2}, LX/3v0;->a(LX/3v0;Landroid/view/MenuItem;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/3vG;->d:LX/3v0;

    invoke-virtual {v0, p1, p2}, LX/3v0;->a(LX/3v0;Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/3v3;)Z
    .locals 1

    .prologue
    .line 651041
    iget-object v0, p0, LX/3vG;->d:LX/3v0;

    invoke-virtual {v0, p1}, LX/3v0;->a(LX/3v3;)Z

    move-result v0

    return v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 651028
    iget-object v0, p0, LX/3vG;->d:LX/3v0;

    invoke-virtual {v0}, LX/3v0;->b()Z

    move-result v0

    return v0
.end method

.method public final b(LX/3v3;)Z
    .locals 1

    .prologue
    .line 651040
    iget-object v0, p0, LX/3vG;->d:LX/3v0;

    invoke-virtual {v0, p1}, LX/3v0;->b(LX/3v3;)Z

    move-result v0

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 651039
    iget-object v0, p0, LX/3vG;->d:LX/3v0;

    invoke-virtual {v0}, LX/3v0;->c()Z

    move-result v0

    return v0
.end method

.method public final getItem()Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 651038
    iget-object v0, p0, LX/3vG;->e:LX/3v3;

    return-object v0
.end method

.method public final q()LX/3v0;
    .locals 1

    .prologue
    .line 651037
    iget-object v0, p0, LX/3vG;->d:LX/3v0;

    return-object v0
.end method

.method public final setHeaderIcon(I)Landroid/view/SubMenu;
    .locals 1

    .prologue
    .line 651034
    iget-object v0, p0, LX/3v0;->e:Landroid/content/Context;

    move-object v0, v0

    .line 651035
    invoke-static {v0, p1}, LX/1ED;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-super {p0, v0}, LX/3v0;->a(Landroid/graphics/drawable/Drawable;)LX/3v0;

    .line 651036
    return-object p0
.end method

.method public final setHeaderIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/SubMenu;
    .locals 0

    .prologue
    .line 651032
    invoke-super {p0, p1}, LX/3v0;->a(Landroid/graphics/drawable/Drawable;)LX/3v0;

    .line 651033
    return-object p0
.end method

.method public final setHeaderTitle(I)Landroid/view/SubMenu;
    .locals 1

    .prologue
    .line 651029
    iget-object v0, p0, LX/3v0;->e:Landroid/content/Context;

    move-object v0, v0

    .line 651030
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-super {p0, v0}, LX/3v0;->a(Ljava/lang/CharSequence;)LX/3v0;

    .line 651031
    return-object p0
.end method

.method public final setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/SubMenu;
    .locals 0

    .prologue
    .line 651026
    invoke-super {p0, p1}, LX/3v0;->a(Ljava/lang/CharSequence;)LX/3v0;

    .line 651027
    return-object p0
.end method

.method public final setHeaderView(Landroid/view/View;)Landroid/view/SubMenu;
    .locals 6

    .prologue
    .line 651023
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 651024
    move-object v0, p0

    move v3, v1

    move-object v4, v2

    move-object v5, p1

    invoke-static/range {v0 .. v5}, LX/3v0;->a(LX/3v0;ILjava/lang/CharSequence;ILandroid/graphics/drawable/Drawable;Landroid/view/View;)V

    .line 651025
    return-object p0
.end method

.method public final setIcon(I)Landroid/view/SubMenu;
    .locals 1

    .prologue
    .line 651021
    iget-object v0, p0, LX/3vG;->e:LX/3v3;

    invoke-virtual {v0, p1}, LX/3v3;->setIcon(I)Landroid/view/MenuItem;

    .line 651022
    return-object p0
.end method

.method public final setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/SubMenu;
    .locals 1

    .prologue
    .line 651019
    iget-object v0, p0, LX/3vG;->e:LX/3v3;

    invoke-virtual {v0, p1}, LX/3v3;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 651020
    return-object p0
.end method

.method public final setQwertyMode(Z)V
    .locals 1

    .prologue
    .line 651017
    iget-object v0, p0, LX/3vG;->d:LX/3v0;

    invoke-virtual {v0, p1}, LX/3v0;->setQwertyMode(Z)V

    .line 651018
    return-void
.end method
