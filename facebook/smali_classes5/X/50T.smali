.class public LX/50T;
.super LX/4yO;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "LX/4yO",
        "<TE;>;"
    }
.end annotation


# static fields
.field public static final a:LX/50T;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/50T",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final transient b:[LX/50E;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "LX/50E",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final transient c:[LX/50E;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "LX/50E",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final transient d:I

.field private final transient e:I

.field private transient f:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 823726
    new-instance v0, LX/50T;

    .line 823727
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 823728
    invoke-direct {v0, v1}, LX/50T;-><init>(Ljava/util/Collection;)V

    sput-object v0, LX/50T;->a:LX/50T;

    return-void
.end method

.method public constructor <init>(Ljava/util/Collection;)V
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+",
            "Lcom/google/common/collect/Multiset$Entry",
            "<+TE;>;>;)V"
        }
    .end annotation

    .prologue
    .line 823729
    invoke-direct {p0}, LX/4yO;-><init>()V

    .line 823730
    invoke-interface/range {p1 .. p1}, Ljava/util/Collection;->size()I

    move-result v0

    .line 823731
    new-array v7, v0, [LX/50E;

    .line 823732
    if-nez v0, :cond_0

    .line 823733
    iput-object v7, p0, LX/50T;->b:[LX/50E;

    .line 823734
    const/4 v0, 0x0

    iput-object v0, p0, LX/50T;->c:[LX/50E;

    .line 823735
    const/4 v0, 0x0

    iput v0, p0, LX/50T;->d:I

    .line 823736
    const/4 v0, 0x0

    iput v0, p0, LX/50T;->e:I

    .line 823737
    invoke-static {}, LX/0Rf;->of()LX/0Rf;

    move-result-object v0

    iput-object v0, p0, LX/50T;->f:LX/0Rf;

    .line 823738
    :goto_0
    return-void

    .line 823739
    :cond_0
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-static {v0, v2, v3}, LX/0PC;->a(ID)I

    move-result v0

    .line 823740
    add-int/lit8 v8, v0, -0x1

    .line 823741
    new-array v9, v0, [LX/50E;

    .line 823742
    const/4 v3, 0x0

    .line 823743
    const/4 v2, 0x0

    .line 823744
    const-wide/16 v0, 0x0

    .line 823745
    invoke-interface/range {p1 .. p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v10

    move v5, v3

    move v14, v2

    move-wide v2, v0

    move v1, v14

    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4wx;

    .line 823746
    invoke-virtual {v0}, LX/4wx;->a()Ljava/lang/Object;

    move-result-object v4

    invoke-static {v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    .line 823747
    invoke-virtual {v0}, LX/4wx;->b()I

    move-result v11

    .line 823748
    invoke-virtual {v6}, Ljava/lang/Object;->hashCode()I

    move-result v12

    .line 823749
    invoke-static {v12}, LX/0PC;->a(I)I

    move-result v4

    and-int v13, v4, v8

    .line 823750
    aget-object v4, v9, v13

    .line 823751
    if-nez v4, :cond_3

    .line 823752
    instance-of v4, v0, LX/50E;

    if-eqz v4, :cond_1

    instance-of v4, v0, LX/50S;

    if-nez v4, :cond_1

    const/4 v4, 0x1

    .line 823753
    :goto_2
    if-eqz v4, :cond_2

    check-cast v0, LX/50E;

    .line 823754
    :goto_3
    xor-int v4, v12, v11

    add-int/2addr v4, v1

    .line 823755
    add-int/lit8 v6, v5, 0x1

    aput-object v0, v7, v5

    .line 823756
    aput-object v0, v9, v13

    .line 823757
    int-to-long v0, v11

    add-long/2addr v0, v2

    move-wide v2, v0

    move v5, v6

    move v1, v4

    .line 823758
    goto :goto_1

    .line 823759
    :cond_1
    const/4 v4, 0x0

    goto :goto_2

    .line 823760
    :cond_2
    new-instance v0, LX/50E;

    invoke-direct {v0, v6, v11}, LX/50E;-><init>(Ljava/lang/Object;I)V

    goto :goto_3

    .line 823761
    :cond_3
    new-instance v0, LX/50S;

    invoke-direct {v0, v6, v11, v4}, LX/50S;-><init>(Ljava/lang/Object;ILX/50E;)V

    goto :goto_3

    .line 823762
    :cond_4
    iput-object v7, p0, LX/50T;->b:[LX/50E;

    .line 823763
    iput-object v9, p0, LX/50T;->c:[LX/50E;

    .line 823764
    invoke-static {v2, v3}, LX/0a4;->b(J)I

    move-result v0

    iput v0, p0, LX/50T;->d:I

    .line 823765
    iput v1, p0, LX/50T;->e:I

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)I
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 823766
    iget-object v1, p0, LX/50T;->c:[LX/50E;

    .line 823767
    if-eqz p1, :cond_0

    if-nez v1, :cond_1

    .line 823768
    :cond_0
    :goto_0
    return v0

    .line 823769
    :cond_1
    invoke-static {p1}, LX/0PC;->a(Ljava/lang/Object;)I

    move-result v2

    .line 823770
    array-length v3, v1

    add-int/lit8 v3, v3, -0x1

    .line 823771
    and-int/2addr v2, v3

    aget-object v1, v1, v2

    .line 823772
    :goto_1
    if-eqz v1, :cond_0

    .line 823773
    invoke-virtual {v1}, LX/50E;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-static {p1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 823774
    invoke-virtual {v1}, LX/50E;->b()I

    move-result v0

    goto :goto_0

    .line 823775
    :cond_2
    invoke-virtual {v1}, LX/50E;->c()LX/50E;

    move-result-object v1

    goto :goto_1
.end method

.method public final a(I)LX/4wx;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/google/common/collect/Multiset$Entry",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 823725
    iget-object v0, p0, LX/50T;->b:[LX/50E;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public final d()Ljava/util/Set;
    .locals 2

    .prologue
    .line 823723
    iget-object v0, p0, LX/50T;->f:LX/0Rf;

    .line 823724
    if-nez v0, :cond_0

    new-instance v0, LX/50R;

    invoke-direct {v0, p0}, LX/50R;-><init>(LX/50T;)V

    iput-object v0, p0, LX/50T;->f:LX/0Rf;

    :cond_0
    return-object v0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 823720
    iget v0, p0, LX/50T;->e:I

    return v0
.end method

.method public final isPartialView()Z
    .locals 1

    .prologue
    .line 823722
    const/4 v0, 0x0

    return v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 823721
    iget v0, p0, LX/50T;->d:I

    return v0
.end method
