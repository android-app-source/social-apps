.class public LX/4Ac;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<DH::",
        "LX/1aY;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public a:Z
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field

.field public b:Ljava/util/ArrayList;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/1aX",
            "<TDH;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 676684
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 676685
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/4Ac;->a:Z

    .line 676686
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/4Ac;->b:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 676678
    iget-boolean v0, p0, LX/4Ac;->a:Z

    if-eqz v0, :cond_1

    .line 676679
    :cond_0
    return-void

    .line 676680
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4Ac;->a:Z

    .line 676681
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/4Ac;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 676682
    iget-object v0, p0, LX/4Ac;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1aX;

    invoke-virtual {v0}, LX/1aX;->d()V

    .line 676683
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 676673
    iget-object v0, p0, LX/4Ac;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1aX;

    .line 676674
    iget-boolean v1, p0, LX/4Ac;->a:Z

    if-eqz v1, :cond_0

    .line 676675
    invoke-virtual {v0}, LX/1aX;->f()V

    .line 676676
    :cond_0
    iget-object v0, p0, LX/4Ac;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 676677
    return-void
.end method

.method public final a(LX/1aX;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1aX",
            "<TDH;>;)V"
        }
    .end annotation

    .prologue
    .line 676666
    iget-object v0, p0, LX/4Ac;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 676667
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 676668
    iget-object v1, p0, LX/4Ac;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, LX/03g;->a(II)I

    .line 676669
    iget-object v1, p0, LX/4Ac;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v0, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 676670
    iget-boolean v1, p0, LX/4Ac;->a:Z

    if-eqz v1, :cond_0

    .line 676671
    invoke-virtual {p1}, LX/1aX;->d()V

    .line 676672
    :cond_0
    return-void
.end method

.method public final a(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 676636
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, LX/4Ac;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 676637
    invoke-virtual {p0, v0}, LX/4Ac;->b(I)LX/1aX;

    move-result-object v1

    invoke-virtual {v1}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 676638
    if-eqz v1, :cond_0

    .line 676639
    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 676640
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 676641
    :cond_1
    return-void
.end method

.method public final a(Landroid/graphics/drawable/Drawable;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 676661
    move v0, v1

    :goto_0
    iget-object v2, p0, LX/4Ac;->b:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 676662
    invoke-virtual {p0, v0}, LX/4Ac;->b(I)LX/1aX;

    move-result-object v2

    invoke-virtual {v2}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    if-ne p1, v2, :cond_1

    .line 676663
    const/4 v1, 0x1

    .line 676664
    :cond_0
    return v1

    .line 676665
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 676656
    move v1, v2

    :goto_0
    iget-object v0, p0, LX/4Ac;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 676657
    iget-object v0, p0, LX/4Ac;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1aX;

    invoke-virtual {v0, p1}, LX/1aX;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 676658
    const/4 v2, 0x1

    .line 676659
    :cond_0
    return v2

    .line 676660
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public final b(I)LX/1aX;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/1aX",
            "<TDH;>;"
        }
    .end annotation

    .prologue
    .line 676655
    iget-object v0, p0, LX/4Ac;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1aX;

    return-object v0
.end method

.method public final b()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 676649
    iget-boolean v1, p0, LX/4Ac;->a:Z

    if-nez v1, :cond_1

    .line 676650
    :cond_0
    return-void

    .line 676651
    :cond_1
    iput-boolean v0, p0, LX/4Ac;->a:Z

    move v1, v0

    .line 676652
    :goto_0
    iget-object v0, p0, LX/4Ac;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 676653
    iget-object v0, p0, LX/4Ac;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1aX;

    invoke-virtual {v0}, LX/1aX;->f()V

    .line 676654
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 676643
    iget-boolean v0, p0, LX/4Ac;->a:Z

    if-eqz v0, :cond_0

    .line 676644
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/4Ac;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 676645
    iget-object v0, p0, LX/4Ac;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1aX;

    invoke-virtual {v0}, LX/1aX;->f()V

    .line 676646
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 676647
    :cond_0
    iget-object v0, p0, LX/4Ac;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 676648
    return-void
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 676642
    iget-object v0, p0, LX/4Ac;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method
