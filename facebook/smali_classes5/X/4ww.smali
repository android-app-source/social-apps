.class public final LX/4ww;
.super LX/11e;
.source ""

# interfaces
.implements Ljava/util/SortedSet;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Xs",
        "<TK;TV;>.WrappedCollection;",
        "Ljava/util/SortedSet",
        "<TV;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0Xs;


# direct methods
.method public constructor <init>(LX/0Xs;Ljava/lang/Object;Ljava/util/SortedSet;LX/11e;)V
    .locals 0
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/11e;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "Ljava/util/SortedSet",
            "<TV;>;",
            "LX/0Xs",
            "<TK;TV;>.WrappedCollection;)V"
        }
    .end annotation

    .prologue
    .line 820633
    iput-object p1, p0, LX/4ww;->a:LX/0Xs;

    .line 820634
    invoke-direct {p0, p1, p2, p3, p4}, LX/11e;-><init>(LX/0Xs;Ljava/lang/Object;Ljava/util/Collection;LX/11e;)V

    .line 820635
    return-void
.end method

.method private g()Ljava/util/SortedSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/SortedSet",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 820636
    iget-object v0, p0, LX/11e;->c:Ljava/util/Collection;

    move-object v0, v0

    .line 820637
    check-cast v0, Ljava/util/SortedSet;

    return-object v0
.end method


# virtual methods
.method public final comparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<-TV;>;"
        }
    .end annotation

    .prologue
    .line 820630
    invoke-direct {p0}, LX/4ww;->g()Ljava/util/SortedSet;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedSet;->comparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public final first()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .prologue
    .line 820631
    invoke-virtual {p0}, LX/11e;->a()V

    .line 820632
    invoke-direct {p0}, LX/4ww;->g()Ljava/util/SortedSet;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedSet;->first()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final headSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)",
            "Ljava/util/SortedSet",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 820622
    invoke-virtual {p0}, LX/11e;->a()V

    .line 820623
    new-instance v0, LX/4ww;

    iget-object v1, p0, LX/4ww;->a:LX/0Xs;

    .line 820624
    iget-object v2, p0, LX/11e;->b:Ljava/lang/Object;

    move-object v2, v2

    .line 820625
    invoke-direct {p0}, LX/4ww;->g()Ljava/util/SortedSet;

    move-result-object v3

    invoke-interface {v3, p1}, Ljava/util/SortedSet;->headSet(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v3

    .line 820626
    iget-object v4, p0, LX/11e;->d:LX/11e;

    move-object v4, v4

    .line 820627
    if-nez v4, :cond_0

    :goto_0
    invoke-direct {v0, v1, v2, v3, p0}, LX/4ww;-><init>(LX/0Xs;Ljava/lang/Object;Ljava/util/SortedSet;LX/11e;)V

    return-object v0

    .line 820628
    :cond_0
    iget-object v4, p0, LX/11e;->d:LX/11e;

    move-object p0, v4

    .line 820629
    goto :goto_0
.end method

.method public final last()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .prologue
    .line 820620
    invoke-virtual {p0}, LX/11e;->a()V

    .line 820621
    invoke-direct {p0}, LX/4ww;->g()Ljava/util/SortedSet;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedSet;->last()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;TV;)",
            "Ljava/util/SortedSet",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 820612
    invoke-virtual {p0}, LX/11e;->a()V

    .line 820613
    new-instance v0, LX/4ww;

    iget-object v1, p0, LX/4ww;->a:LX/0Xs;

    .line 820614
    iget-object v2, p0, LX/11e;->b:Ljava/lang/Object;

    move-object v2, v2

    .line 820615
    invoke-direct {p0}, LX/4ww;->g()Ljava/util/SortedSet;

    move-result-object v3

    invoke-interface {v3, p1, p2}, Ljava/util/SortedSet;->subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v3

    .line 820616
    iget-object v4, p0, LX/11e;->d:LX/11e;

    move-object v4, v4

    .line 820617
    if-nez v4, :cond_0

    :goto_0
    invoke-direct {v0, v1, v2, v3, p0}, LX/4ww;-><init>(LX/0Xs;Ljava/lang/Object;Ljava/util/SortedSet;LX/11e;)V

    return-object v0

    .line 820618
    :cond_0
    iget-object v4, p0, LX/11e;->d:LX/11e;

    move-object p0, v4

    .line 820619
    goto :goto_0
.end method

.method public final tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)",
            "Ljava/util/SortedSet",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 820604
    invoke-virtual {p0}, LX/11e;->a()V

    .line 820605
    new-instance v0, LX/4ww;

    iget-object v1, p0, LX/4ww;->a:LX/0Xs;

    .line 820606
    iget-object v2, p0, LX/11e;->b:Ljava/lang/Object;

    move-object v2, v2

    .line 820607
    invoke-direct {p0}, LX/4ww;->g()Ljava/util/SortedSet;

    move-result-object v3

    invoke-interface {v3, p1}, Ljava/util/SortedSet;->tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v3

    .line 820608
    iget-object v4, p0, LX/11e;->d:LX/11e;

    move-object v4, v4

    .line 820609
    if-nez v4, :cond_0

    :goto_0
    invoke-direct {v0, v1, v2, v3, p0}, LX/4ww;-><init>(LX/0Xs;Ljava/lang/Object;Ljava/util/SortedSet;LX/11e;)V

    return-object v0

    .line 820610
    :cond_0
    iget-object v4, p0, LX/11e;->d:LX/11e;

    move-object p0, v4

    .line 820611
    goto :goto_0
.end method
