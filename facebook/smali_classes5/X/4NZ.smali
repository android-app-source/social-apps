.class public LX/4NZ;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 693669
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const-wide/16 v4, 0x0

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 693670
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_8

    .line 693671
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 693672
    :goto_0
    return v1

    .line 693673
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_6

    .line 693674
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 693675
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 693676
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_0

    if-eqz v11, :cond_0

    .line 693677
    const-string v12, "fetchTimeMs"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 693678
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    move v0, v6

    goto :goto_1

    .line 693679
    :cond_1
    const-string v12, "friend_description"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 693680
    invoke-static {p0, p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v10

    goto :goto_1

    .line 693681
    :cond_2
    const-string v12, "friendversary_campaign"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 693682
    invoke-static {p0, p1}, LX/4NR;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 693683
    :cond_3
    const-string v12, "render_style"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 693684
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 693685
    :cond_4
    const-string v12, "section_header"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 693686
    invoke-static {p0, p1}, LX/4Ni;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 693687
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 693688
    :cond_6
    const/4 v11, 0x5

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 693689
    if-eqz v0, :cond_7

    move-object v0, p1

    .line 693690
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 693691
    :cond_7
    invoke-virtual {p1, v6, v10}, LX/186;->b(II)V

    .line 693692
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 693693
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 693694
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 693695
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_8
    move v0, v1

    move v7, v1

    move v8, v1

    move v9, v1

    move v10, v1

    move-wide v2, v4

    goto/16 :goto_1
.end method

.method public static a(LX/15w;S)LX/15i;
    .locals 5

    .prologue
    .line 693696
    const/4 v0, 0x1

    const/4 v4, 0x0

    .line 693697
    new-instance v2, LX/186;

    const/16 v1, 0x80

    invoke-direct {v2, v1}, LX/186;-><init>(I)V

    .line 693698
    invoke-static {p0, v2}, LX/4NZ;->a(LX/15w;LX/186;)I

    move-result v1

    .line 693699
    if-eqz v0, :cond_0

    .line 693700
    const/4 v3, 0x2

    invoke-virtual {v2, v3}, LX/186;->c(I)V

    .line 693701
    invoke-virtual {v2, v4, p1, v4}, LX/186;->a(ISI)V

    .line 693702
    const/4 v3, 0x1

    invoke-virtual {v2, v3, v1}, LX/186;->b(II)V

    .line 693703
    invoke-virtual {v2}, LX/186;->d()I

    move-result v1

    .line 693704
    :cond_0
    invoke-virtual {v2, v1}, LX/186;->d(I)V

    .line 693705
    invoke-static {v2}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v1

    move-object v0, v1

    .line 693706
    return-object v0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 693707
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 693708
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 693709
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 693710
    const-string v0, "name"

    const-string v1, "GoodwillThrowbackFriendversaryPromotionStory"

    invoke-virtual {p2, v0, v1}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 693711
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 693712
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 693713
    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    .line 693714
    const-string v2, "fetchTimeMs"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 693715
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 693716
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 693717
    if-eqz v0, :cond_1

    .line 693718
    const-string v1, "friend_description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 693719
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 693720
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 693721
    if-eqz v0, :cond_2

    .line 693722
    const-string v1, "friendversary_campaign"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 693723
    invoke-static {p0, v0, p2, p3}, LX/4NR;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 693724
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 693725
    if-eqz v0, :cond_3

    .line 693726
    const-string v1, "render_style"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 693727
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 693728
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 693729
    if-eqz v0, :cond_4

    .line 693730
    const-string v1, "section_header"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 693731
    invoke-static {p0, v0, p2, p3}, LX/4Ni;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 693732
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 693733
    return-void
.end method
