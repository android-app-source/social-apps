.class public final LX/4z8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<TK;>;"
    }
.end annotation


# instance fields
.field public final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<TK;>;"
        }
    .end annotation
.end field

.field public b:LX/4zA;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4zA",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public c:LX/4zA;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4zA",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public d:I

.field public final synthetic e:LX/4zD;


# direct methods
.method public constructor <init>(LX/4zD;)V
    .locals 1

    .prologue
    .line 822358
    iput-object p1, p0, LX/4z8;->e:LX/4zD;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 822359
    iget-object v0, p0, LX/4z8;->e:LX/4zD;

    invoke-virtual {v0}, LX/0Xt;->p()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    invoke-static {v0}, LX/0RA;->a(I)Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/4z8;->a:Ljava/util/Set;

    .line 822360
    iget-object v0, p0, LX/4z8;->e:LX/4zD;

    iget-object v0, v0, LX/4zD;->a:LX/4zA;

    iput-object v0, p0, LX/4z8;->b:LX/4zA;

    .line 822361
    iget-object v0, p0, LX/4z8;->e:LX/4zD;

    iget v0, v0, LX/4zD;->e:I

    iput v0, p0, LX/4z8;->d:I

    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 822339
    iget-object v0, p0, LX/4z8;->e:LX/4zD;

    iget v0, v0, LX/4zD;->e:I

    iget v1, p0, LX/4z8;->d:I

    if-eq v0, v1, :cond_0

    .line 822340
    new-instance v0, Ljava/util/ConcurrentModificationException;

    invoke-direct {v0}, Ljava/util/ConcurrentModificationException;-><init>()V

    throw v0

    .line 822341
    :cond_0
    return-void
.end method


# virtual methods
.method public final hasNext()Z
    .locals 1

    .prologue
    .line 822356
    invoke-direct {p0}, LX/4z8;->a()V

    .line 822357
    iget-object v0, p0, LX/4z8;->b:LX/4zA;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final next()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TK;"
        }
    .end annotation

    .prologue
    .line 822349
    invoke-direct {p0}, LX/4z8;->a()V

    .line 822350
    iget-object v0, p0, LX/4z8;->b:LX/4zA;

    invoke-static {v0}, LX/4zD;->i(Ljava/lang/Object;)V

    .line 822351
    iget-object v0, p0, LX/4z8;->b:LX/4zA;

    iput-object v0, p0, LX/4z8;->c:LX/4zA;

    .line 822352
    iget-object v0, p0, LX/4z8;->a:Ljava/util/Set;

    iget-object v1, p0, LX/4z8;->c:LX/4zA;

    iget-object v1, v1, LX/4zA;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 822353
    :cond_0
    iget-object v0, p0, LX/4z8;->b:LX/4zA;

    iget-object v0, v0, LX/4zA;->c:LX/4zA;

    iput-object v0, p0, LX/4z8;->b:LX/4zA;

    .line 822354
    iget-object v0, p0, LX/4z8;->b:LX/4zA;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/4z8;->a:Ljava/util/Set;

    iget-object v1, p0, LX/4z8;->b:LX/4zA;

    iget-object v1, v1, LX/4zA;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 822355
    :cond_1
    iget-object v0, p0, LX/4z8;->c:LX/4zA;

    iget-object v0, v0, LX/4zA;->a:Ljava/lang/Object;

    return-object v0
.end method

.method public final remove()V
    .locals 2

    .prologue
    .line 822342
    invoke-direct {p0}, LX/4z8;->a()V

    .line 822343
    iget-object v0, p0, LX/4z8;->c:LX/4zA;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0P6;->a(Z)V

    .line 822344
    iget-object v0, p0, LX/4z8;->e:LX/4zD;

    iget-object v1, p0, LX/4z8;->c:LX/4zA;

    iget-object v1, v1, LX/4zA;->a:Ljava/lang/Object;

    invoke-static {v0, v1}, LX/4zD;->h(LX/4zD;Ljava/lang/Object;)V

    .line 822345
    const/4 v0, 0x0

    iput-object v0, p0, LX/4z8;->c:LX/4zA;

    .line 822346
    iget-object v0, p0, LX/4z8;->e:LX/4zD;

    iget v0, v0, LX/4zD;->e:I

    iput v0, p0, LX/4z8;->d:I

    .line 822347
    return-void

    .line 822348
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
