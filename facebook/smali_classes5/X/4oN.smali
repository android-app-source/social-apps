.class public LX/4oN;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Z

.field public final b:Z

.field public final c:Z

.field public final d:Z

.field public final e:Z

.field public final f:F

.field public final g:I

.field public final h:F

.field public i:I


# direct methods
.method private constructor <init>(ZZZZZFIFI)V
    .locals 2

    .prologue
    .line 809582
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 809583
    iput-boolean p1, p0, LX/4oN;->a:Z

    .line 809584
    iput-boolean p2, p0, LX/4oN;->b:Z

    .line 809585
    iput-boolean p3, p0, LX/4oN;->c:Z

    .line 809586
    iput-boolean p4, p0, LX/4oN;->d:Z

    .line 809587
    iput-boolean p5, p0, LX/4oN;->e:Z

    .line 809588
    iput p6, p0, LX/4oN;->f:F

    .line 809589
    iput p7, p0, LX/4oN;->g:I

    .line 809590
    iput p8, p0, LX/4oN;->h:F

    .line 809591
    iput p9, p0, LX/4oN;->i:I

    .line 809592
    iget-boolean v0, p0, LX/4oN;->a:Z

    if-eqz v0, :cond_1

    .line 809593
    const/4 v0, 0x0

    cmpl-float v0, p6, v0

    if-lez v0, :cond_0

    .line 809594
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "RoundedView cannot have cornerRadius set if it\'s a circle"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 809595
    :cond_0
    iget-boolean v0, p0, LX/4oN;->b:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, LX/4oN;->c:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, LX/4oN;->d:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, LX/4oN;->e:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 809596
    if-nez v0, :cond_1

    .line 809597
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "RoundedView doesn\'t support disabling individual rounded corners when it\'s a circle"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 809598
    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic constructor <init>(ZZZZZFIFIB)V
    .locals 0

    .prologue
    .line 809604
    invoke-direct/range {p0 .. p9}, LX/4oN;-><init>(ZZZZZFIFI)V

    return-void
.end method

.method public static a()LX/4oN;
    .locals 10

    .prologue
    const/4 v7, 0x0

    const/4 v1, 0x1

    .line 809605
    new-instance v0, LX/4oN;

    const/4 v6, 0x0

    const/high16 v8, 0x40000000    # 2.0f

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v9, v7

    invoke-direct/range {v0 .. v9}, LX/4oN;-><init>(ZZZZZFIFI)V

    return-object v0
.end method

.method public static a(Landroid/content/Context;Landroid/util/AttributeSet;I)LX/4oN;
    .locals 12
    .param p1    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v6, 0x1

    const/4 v11, 0x0

    .line 809599
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 809600
    sget-object v0, LX/03r;->RoundedView:[I

    invoke-virtual {p0, p1, v0, p2, v11}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v10

    .line 809601
    new-instance v0, LX/4oN;

    const/16 v1, 0x0

    invoke-virtual {v10, v1, v11}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    const/16 v2, 0x1

    invoke-virtual {v10, v2, v6}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    const/16 v3, 0x2

    invoke-virtual {v10, v3, v6}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v3

    const/16 v4, 0x4

    invoke-virtual {v10, v4, v6}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v4

    const/16 v5, 0x3

    invoke-virtual {v10, v5, v6}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v5

    const/16 v6, 0x6

    const/4 v7, 0x0

    invoke-virtual {v10, v6, v7}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v6

    const/16 v7, 0xb

    invoke-virtual {v10, v7, v11}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v7

    const/16 v8, 0xc

    const/high16 v9, 0x40000000    # 2.0f

    invoke-virtual {v10, v8, v9}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v8

    const/16 v9, 0xd

    invoke-virtual {v10, v9, v11}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v9

    invoke-direct/range {v0 .. v9}, LX/4oN;-><init>(ZZZZZFIFI)V

    .line 809602
    invoke-virtual {v10}, Landroid/content/res/TypedArray;->recycle()V

    .line 809603
    return-object v0
.end method
