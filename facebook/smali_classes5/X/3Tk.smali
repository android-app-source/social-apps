.class public LX/3Tk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Tl;
.implements LX/0o8;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/3Tl",
        "<",
        "LX/2nq;",
        ">;",
        "LX/0o8;"
    }
.end annotation


# static fields
.field public static final b:Ljava/lang/String;


# instance fields
.field public final a:Ljava/util/List;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/2nq;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/03V;

.field public final d:LX/17W;

.field private final e:Landroid/support/v4/app/Fragment;

.field private final f:LX/3Tu;

.field private final g:LX/0xW;

.field public final h:LX/3TK;

.field public final i:LX/3Tg;

.field private final j:LX/2jY;

.field public final k:Landroid/content/res/Resources;

.field public l:LX/1Qq;

.field public m:Z

.field public final n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/2nq;",
            ">;"
        }
    .end annotation
.end field

.field public o:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/3dV;",
            ">;"
        }
    .end annotation
.end field

.field public p:LX/3Ts;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 584694
    const-class v0, LX/3Tl;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/3Tk;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/3Tg;Landroid/support/v4/app/Fragment;LX/03V;LX/17W;LX/0xW;LX/3TK;LX/1DS;LX/0Ot;LX/3Tm;LX/3Tn;LX/3To;LX/3Tp;LX/3Fx;LX/2iz;LX/1QH;LX/1DL;)V
    .locals 15
    .param p1    # LX/3Tg;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Landroid/support/v4/app/Fragment;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3Tg;",
            "Landroid/support/v4/app/Fragment;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/17W;",
            "LX/0xW;",
            "LX/3TK;",
            "LX/1DS;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/notifications/multirow/partdefinition/NotificationsFeedRootPartDefinition;",
            ">;",
            "LX/3Tm;",
            "LX/3Tn;",
            "LX/3To;",
            "LX/3Tp;",
            "LX/3Fx;",
            "LX/2iz;",
            "LX/1QH;",
            "LX/1DL;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 584695
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 584696
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v2

    iput-object v2, p0, LX/3Tk;->o:LX/0am;

    .line 584697
    move-object/from16 v0, p2

    iput-object v0, p0, LX/3Tk;->e:Landroid/support/v4/app/Fragment;

    .line 584698
    move-object/from16 v0, p3

    iput-object v0, p0, LX/3Tk;->c:LX/03V;

    .line 584699
    move-object/from16 v0, p4

    iput-object v0, p0, LX/3Tk;->d:LX/17W;

    .line 584700
    move-object/from16 v0, p5

    iput-object v0, p0, LX/3Tk;->g:LX/0xW;

    .line 584701
    invoke-virtual/range {p2 .. p2}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iput-object v2, p0, LX/3Tk;->k:Landroid/content/res/Resources;

    .line 584702
    move-object/from16 v0, p1

    iput-object v0, p0, LX/3Tk;->i:LX/3Tg;

    .line 584703
    move-object/from16 v0, p6

    iput-object v0, p0, LX/3Tk;->h:LX/3TK;

    .line 584704
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, LX/3Tk;->a:Ljava/util/List;

    .line 584705
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, LX/3Tk;->n:Ljava/util/List;

    .line 584706
    move-object/from16 v0, p14

    invoke-direct {p0, v0}, LX/3Tk;->a(LX/2iz;)LX/2jY;

    move-result-object v2

    iput-object v2, p0, LX/3Tk;->j:LX/2jY;

    .line 584707
    invoke-virtual/range {p2 .. p2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p0}, LX/3Tk;->o()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/3Fx;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Context;

    move-result-object v3

    .line 584708
    iget-object v2, p0, LX/3Tk;->a:Ljava/util/List;

    move-object/from16 v0, p9

    invoke-virtual {v0, v2}, LX/3Tm;->a(Ljava/util/List;)LX/3Ts;

    move-result-object v2

    iput-object v2, p0, LX/3Tk;->p:LX/3Ts;

    .line 584709
    iget-object v2, p0, LX/3Tk;->j:LX/2jY;

    const/4 v4, 0x0

    iget-object v5, p0, LX/3Tk;->p:LX/3Ts;

    move-object/from16 v0, p11

    invoke-virtual {v0, v2, v4, v5}, LX/3To;->a(LX/2jY;LX/1P1;LX/3Tt;)LX/3Tu;

    move-result-object v2

    iput-object v2, p0, LX/3Tk;->f:LX/3Tu;

    .line 584710
    iget-object v2, p0, LX/3Tk;->p:LX/3Ts;

    move-object/from16 v0, p7

    move-object/from16 v1, p8

    invoke-virtual {v0, v1, v2}, LX/1DS;->a(LX/0Ot;LX/0g1;)LX/1Ql;

    move-result-object v14

    invoke-static {}, LX/2kh;->b()LX/2kh;

    move-result-object v4

    const/4 v5, 0x0

    new-instance v6, Lcom/facebook/notifications/notificationsfriending/adapter/NotificationsAdapterSection$1;

    invoke-direct {v6, p0}, Lcom/facebook/notifications/notificationsfriending/adapter/NotificationsAdapterSection$1;-><init>(LX/3Tk;)V

    new-instance v7, Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    const/4 v2, 0x0

    const-string v8, "ANDROID_NOTIFICATIONS_FRIENDING"

    const-string v9, "unknown"

    const/4 v10, 0x0

    invoke-direct {v7, v2, v8, v9, v10}, Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;-><init>(Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    move-object/from16 v0, p12

    invoke-virtual {v0, v3, p0}, LX/3Tp;->a(Landroid/content/Context;LX/0o8;)LX/3Tv;

    move-result-object v8

    iget-object v9, p0, LX/3Tk;->f:LX/3Tu;

    iget-object v10, p0, LX/3Tk;->j:LX/2jY;

    sget-object v11, LX/1PU;->b:LX/1PY;

    iget-object v12, p0, LX/3Tk;->p:LX/3Ts;

    new-instance v2, LX/3U4;

    invoke-direct {v2}, LX/3U4;-><init>()V

    move-object/from16 v0, p15

    invoke-virtual {v0, v2}, LX/1QH;->a(LX/1Jg;)LX/1QW;

    move-result-object v13

    move-object/from16 v2, p10

    invoke-virtual/range {v2 .. v13}, LX/3Tn;->a(Landroid/content/Context;LX/1PT;LX/1SX;Ljava/lang/Runnable;Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;LX/3Tv;LX/3Tu;LX/2jY;LX/1PY;LX/3Tt;LX/1QW;)LX/3U5;

    move-result-object v2

    invoke-virtual {v14, v2}, LX/1Ql;->a(LX/1PW;)LX/1Ql;

    move-result-object v2

    invoke-virtual {v2}, LX/1Ql;->e()LX/1Qq;

    move-result-object v2

    iput-object v2, p0, LX/3Tk;->l:LX/1Qq;

    .line 584711
    return-void
.end method

.method private a(LX/2iz;)LX/2jY;
    .locals 2

    .prologue
    .line 584712
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    .line 584713
    invoke-virtual {p0}, LX/3Tk;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/2iz;->b(Ljava/lang/String;Ljava/lang/String;)LX/2jY;

    move-result-object v1

    .line 584714
    invoke-virtual {p1, v0}, LX/2iz;->c(Ljava/lang/String;)V

    .line 584715
    return-object v1
.end method

.method private static a(Ljava/util/List;Ljava/util/List;II)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/2nq;",
            ">;",
            "Ljava/util/List",
            "<",
            "LX/2nq;",
            ">;II)V"
        }
    .end annotation

    .prologue
    .line 584716
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0, p3}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 584717
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    .line 584718
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2nq;

    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 584719
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->UNSEEN_AND_UNREAD:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aF()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    add-int/lit8 v0, p2, -0x1

    if-gt v1, v0, :cond_2

    .line 584720
    :cond_0
    const/4 v0, 0x0

    add-int/lit8 v1, v1, 0x1

    invoke-interface {p0, v0, v1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 584721
    :cond_1
    return-void

    .line 584722
    :cond_2
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0
.end method

.method private static a(Ljava/util/List;Ljava/util/List;Ljava/util/List;JI)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/2nq;",
            ">;",
            "Ljava/util/List",
            "<",
            "LX/2nq;",
            ">;",
            "Ljava/util/List",
            "<",
            "LX/2nq;",
            ">;JI)V"
        }
    .end annotation

    .prologue
    .line 584723
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 584724
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2nq;

    .line 584725
    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->U()J

    move-result-wide v4

    cmp-long v3, v4, p3

    if-lez v3, :cond_3

    .line 584726
    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 584727
    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 584728
    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 584729
    :cond_1
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-lt v0, p5, :cond_0

    .line 584730
    :cond_2
    return-void

    .line 584731
    :cond_3
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 584732
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_4
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2nq;

    .line 584733
    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 584734
    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 584735
    :cond_5
    move-object v2, v2

    .line 584736
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_6
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2nq;

    .line 584737
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v4

    if-ge v4, p5, :cond_2

    .line 584738
    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v4

    .line 584739
    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    invoke-interface {v2, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    const/4 v0, 0x1

    .line 584740
    :goto_2
    invoke-interface {v1, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    .line 584741
    if-eqz v0, :cond_6

    if-nez v5, :cond_6

    .line 584742
    invoke-interface {v2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 584743
    :cond_7
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public static d(LX/3Tk;I)LX/2nq;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 584744
    iget-object v0, p0, LX/3Tk;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/3Tk;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 584745
    :cond_0
    const/4 v0, 0x0

    .line 584746
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LX/3Tk;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2nq;

    goto :goto_0
.end method

.method private t()Z
    .locals 1

    .prologue
    .line 584747
    iget-boolean v0, p0, LX/3Tk;->m:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/3Tk;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(I)I
    .locals 1

    .prologue
    .line 584748
    iget-object v0, p0, LX/3Tk;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/3Tk;->l:LX/1Qq;

    invoke-interface {v0, p1}, LX/1Qq;->getItemViewType(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public final a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 584749
    iget-object v0, p0, LX/3Tk;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 584750
    if-eqz p2, :cond_1

    check-cast p2, Lcom/facebook/fbui/widget/text/ImageWithTextView;

    .line 584751
    :goto_0
    iget-object v0, p0, LX/3Tk;->o:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/3Tk;->o:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, LX/3dV;->SUCCESS:LX/3dV;

    if-ne v0, v1, :cond_2

    iget-object v0, p0, LX/3Tk;->k:Landroid/content/res/Resources;

    const v1, 0x7f081152

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {p2, v0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setText(Ljava/lang/CharSequence;)V

    .line 584752
    move-object v0, p2

    .line 584753
    :goto_2
    return-object v0

    :cond_0
    iget-object v0, p0, LX/3Tk;->l:LX/1Qq;

    invoke-interface {v0, p1, p2, p3}, LX/1Qq;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_2

    .line 584754
    :cond_1
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030c38

    const/4 p1, 0x0

    invoke-virtual {v0, v1, p3, p1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/text/ImageWithTextView;

    move-object p2, v0

    goto :goto_0

    .line 584755
    :cond_2
    iget-object v0, p0, LX/3Tk;->k:Landroid/content/res/Resources;

    const v1, 0x7f083936

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public final a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 584756
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 584757
    const/4 v0, 0x0

    return v0
.end method

.method public final b(Ljava/lang/String;)Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 584758
    const/4 v0, 0x0

    return-object v0
.end method

.method public final synthetic b(I)Ljava/lang/Object;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 584759
    invoke-static {p0, p1}, LX/3Tk;->d(LX/3Tk;I)LX/2nq;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/util/List;Z)V
    .locals 7
    .param p1    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/2nq;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 584760
    sget-object v0, LX/3dV;->SUCCESS:LX/3dV;

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/3Tk;->o:LX/0am;

    .line 584761
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 584762
    :cond_0
    iget-object v0, p0, LX/3Tk;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 584763
    iget-object v0, p0, LX/3Tk;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 584764
    :goto_0
    return-void

    .line 584765
    :cond_1
    iget-object v0, p0, LX/3Tk;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 584766
    if-eqz p2, :cond_4

    iget-object v0, p0, LX/3Tk;->g:LX/0xW;

    .line 584767
    iget-object v1, v0, LX/0xW;->a:LX/0ad;

    sget v2, LX/0xc;->l:I

    const/4 v3, 0x3

    invoke-interface {v1, v2, v3}, LX/0ad;->a(II)I

    move-result v1

    move v0, v1

    .line 584768
    :goto_1
    iget-object v1, p0, LX/3Tk;->a:Ljava/util/List;

    iget-object v2, p0, LX/3Tk;->g:LX/0xW;

    invoke-virtual {v2}, LX/0xW;->k()I

    move-result v2

    invoke-static {p1, v1, v0, v2}, LX/3Tk;->a(Ljava/util/List;Ljava/util/List;II)V

    .line 584769
    :cond_2
    :goto_2
    iget-object v0, p0, LX/3Tk;->g:LX/0xW;

    .line 584770
    iget-object v1, v0, LX/0xW;->a:LX/0ad;

    sget-short v2, LX/0xc;->g:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    move v0, v1

    .line 584771
    if-eqz v0, :cond_3

    iget-boolean v0, p0, LX/3Tk;->m:Z

    if-nez v0, :cond_3

    .line 584772
    iget-object v2, p0, LX/3Tk;->a:Ljava/util/List;

    iget-object v3, p0, LX/3Tk;->n:Ljava/util/List;

    iget-object v0, p0, LX/3Tk;->g:LX/0xW;

    .line 584773
    iget-object v1, v0, LX/0xW;->a:LX/0ad;

    sget v4, LX/0xc;->i:I

    const/4 v5, 0x5

    invoke-interface {v1, v4, v5}, LX/0ad;->a(II)I

    move-result v1

    move v4, v1

    .line 584774
    iget-object v0, p0, LX/3Tk;->g:LX/0xW;

    .line 584775
    iget-object v1, v0, LX/0xW;->a:LX/0ad;

    sget v5, LX/0xc;->h:I

    const/16 v6, 0xa

    invoke-interface {v1, v5, v6}, LX/0ad;->a(II)I

    move-result v1

    move v5, v1

    .line 584776
    move-object v0, p0

    move-object v1, p1

    .line 584777
    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 584778
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_6

    .line 584779
    :cond_3
    :goto_3
    iget-object v0, p0, LX/3Tk;->l:LX/1Qq;

    invoke-interface {v0}, LX/1Cw;->notifyDataSetChanged()V

    goto :goto_0

    .line 584780
    :cond_4
    iget-object v0, p0, LX/3Tk;->g:LX/0xW;

    invoke-virtual {v0}, LX/0xW;->j()I

    move-result v0

    goto :goto_1

    .line 584781
    :cond_5
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, LX/3Tk;->a:Ljava/util/List;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 584782
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2nq;

    .line 584783
    iget-object v2, p0, LX/3Tk;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 584784
    iget-object v2, p0, LX/3Tk;->g:LX/0xW;

    invoke-virtual {v2}, LX/0xW;->k()I

    move-result v6

    .line 584785
    iget-object v3, p0, LX/3Tk;->a:Ljava/util/List;

    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->U()J

    move-result-wide v4

    move-object v2, p1

    invoke-static/range {v1 .. v6}, LX/3Tk;->a(Ljava/util/List;Ljava/util/List;Ljava/util/List;JI)V

    .line 584786
    iget-object v0, p0, LX/3Tk;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 584787
    iget-object v0, p0, LX/3Tk;->a:Ljava/util/List;

    iget-object v1, p0, LX/3Tk;->g:LX/0xW;

    invoke-virtual {v1}, LX/0xW;->j()I

    move-result v1

    invoke-static {p1, v0, v1, v6}, LX/3Tk;->a(Ljava/util/List;Ljava/util/List;II)V

    goto/16 :goto_2

    .line 584788
    :cond_6
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-interface {v2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-interface {v1, v6}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v6

    .line 584789
    const/4 p1, -0x1

    if-ne v6, p1, :cond_7

    .line 584790
    iget-object v6, v0, LX/3Tk;->c:LX/03V;

    sget-object p1, LX/3Tk;->b:Ljava/lang/String;

    const-string p2, "Notification in truncated list not present in full list"

    invoke-virtual {v6, p1, p2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 584791
    :cond_7
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result p1

    sub-int/2addr p1, v6

    add-int/lit8 p1, p1, -0x1

    .line 584792
    if-lt p1, v4, :cond_3

    .line 584793
    invoke-static {v5, p1}, Ljava/lang/Math;->min(II)I

    move-result p1

    .line 584794
    add-int/lit8 p2, v6, 0x1

    add-int/2addr v6, p1

    add-int/lit8 v6, v6, 0x1

    invoke-interface {v1, p2, v6}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v6

    invoke-interface {v3, v6}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_3
.end method

.method public final c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 584693
    iget-object v0, p0, LX/3Tk;->k:Landroid/content/res/Resources;

    const v1, 0x7f081153

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 584795
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 584672
    const/4 v0, 0x0

    return-object v0
.end method

.method public final f()Landroid/view/View$OnClickListener;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 584673
    const/4 v0, 0x0

    return-object v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 584674
    iget-object v0, p0, LX/3Tk;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/3Tk;->l:LX/1Qq;

    invoke-interface {v0}, LX/1Qq;->getCount()I

    move-result v0

    goto :goto_0
.end method

.method public final getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 584675
    iget-object v0, p0, LX/3Tk;->e:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 584676
    iget-object v0, p0, LX/3Tk;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()I
    .locals 1

    .prologue
    .line 584677
    iget-object v0, p0, LX/3Tk;->l:LX/1Qq;

    invoke-interface {v0}, LX/1Qq;->getViewTypeCount()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 584678
    iget-object v0, p0, LX/3Tk;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()LX/3UN;
    .locals 1

    .prologue
    .line 584679
    invoke-direct {p0}, LX/3Tk;->t()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/3UN;->SEE_MORE_FOOTER:LX/3UN;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/3UN;->SEE_ALL_FOOTER:LX/3UN;

    goto :goto_0
.end method

.method public final kI_()Landroid/view/ViewGroup;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 584680
    const/4 v0, 0x0

    return-object v0
.end method

.method public final l()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 584681
    invoke-direct {p0}, LX/3Tk;->t()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 584682
    new-instance v0, LX/Iw2;

    invoke-direct {v0, p0}, LX/Iw2;-><init>(LX/3Tk;)V

    move-object v0, v0

    .line 584683
    :goto_0
    return-object v0

    .line 584684
    :cond_0
    new-instance v0, LX/Iw3;

    invoke-direct {v0, p0}, LX/Iw3;-><init>(LX/3Tk;)V

    move-object v0, v0

    .line 584685
    goto :goto_0
.end method

.method public final m()LX/2ja;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 584686
    iget-object v0, p0, LX/3Tk;->f:LX/3Tu;

    return-object v0
.end method

.method public final mZ_()Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 584687
    iget-object v0, p0, LX/3Tk;->e:Landroid/support/v4/app/Fragment;

    return-object v0
.end method

.method public final n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 584688
    iget-object v0, p0, LX/3Tk;->j:LX/2jY;

    .line 584689
    iget-object p0, v0, LX/2jY;->a:Ljava/lang/String;

    move-object v0, p0

    .line 584690
    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 1
    .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
    .end annotation

    .prologue
    .line 584691
    const-string v0, "ANDROID_NOTIFICATIONS_FRIENDING"

    return-object v0
.end method

.method public final p()Z
    .locals 1

    .prologue
    .line 584692
    iget-object v0, p0, LX/3Tk;->o:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    return v0
.end method
