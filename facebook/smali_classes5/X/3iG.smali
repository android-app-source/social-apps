.class public LX/3iG;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/3iK;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 629033
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 629034
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/feedback/ui/FeedbackControllerParams;)LX/3iK;
    .locals 18

    .prologue
    .line 629035
    new-instance v1, LX/3iK;

    invoke-static/range {p0 .. p0}, LX/1K9;->a(LX/0QB;)LX/1K9;

    move-result-object v2

    check-cast v2, LX/1K9;

    invoke-static/range {p0 .. p0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v3

    check-cast v3, LX/0bH;

    invoke-static/range {p0 .. p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/Executor;

    invoke-static/range {p0 .. p0}, LX/3iL;->a(LX/0QB;)LX/3iL;

    move-result-object v5

    check-cast v5, LX/3iL;

    invoke-static/range {p0 .. p0}, LX/3H7;->a(LX/0QB;)LX/3H7;

    move-result-object v6

    check-cast v6, LX/3H7;

    const/16 v7, 0x3567

    move-object/from16 v0, p0

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static/range {p0 .. p0}, LX/3iM;->a(LX/0QB;)LX/3iM;

    move-result-object v8

    check-cast v8, LX/3iM;

    invoke-static/range {p0 .. p0}, LX/1Ck;->a(LX/0QB;)LX/1Ck;

    move-result-object v9

    check-cast v9, LX/1Ck;

    invoke-static/range {p0 .. p0}, LX/3iP;->a(LX/0QB;)LX/3iP;

    move-result-object v10

    check-cast v10, LX/3iP;

    invoke-static/range {p0 .. p0}, LX/0WG;->a(LX/0QB;)LX/0SI;

    move-result-object v11

    check-cast v11, LX/0SI;

    invoke-static/range {p0 .. p0}, LX/3iQ;->a(LX/0QB;)LX/3iQ;

    move-result-object v12

    check-cast v12, LX/3iQ;

    invoke-static/range {p0 .. p0}, LX/3id;->a(LX/0QB;)LX/3id;

    move-result-object v13

    check-cast v13, LX/3id;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v14

    check-cast v14, LX/0ad;

    invoke-static/range {p0 .. p0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v15

    check-cast v15, LX/0if;

    invoke-static/range {p0 .. p0}, LX/0tn;->a(LX/0QB;)LX/0tn;

    move-result-object v16

    check-cast v16, LX/0tn;

    move-object/from16 v17, p1

    invoke-direct/range {v1 .. v17}, LX/3iK;-><init>(LX/1K9;LX/0bH;Ljava/util/concurrent/Executor;LX/3iL;LX/3H7;LX/0Ot;LX/3iM;LX/1Ck;LX/3iP;LX/0SI;LX/3iQ;LX/3id;LX/0ad;LX/0if;LX/0tn;Lcom/facebook/feedback/ui/FeedbackControllerParams;)V

    .line 629036
    return-object v1
.end method
