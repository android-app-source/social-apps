.class public final LX/3Rs;
.super LX/0Tz;
.source ""


# static fields
.field private static final a:LX/0sv;

.field private static final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/0U1;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Ljava/lang/String;

.field private static final d:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 15

    .prologue
    .line 581669
    new-instance v0, LX/0su;

    sget-object v1, LX/2P1;->a:LX/0U1;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0su;-><init>(LX/0Px;)V

    sput-object v0, LX/3Rs;->a:LX/0sv;

    .line 581670
    sget-object v0, LX/2P1;->a:LX/0U1;

    sget-object v1, LX/2P1;->b:LX/0U1;

    sget-object v2, LX/2P1;->c:LX/0U1;

    sget-object v3, LX/2P1;->d:LX/0U1;

    sget-object v4, LX/2P1;->e:LX/0U1;

    sget-object v5, LX/2P1;->f:LX/0U1;

    sget-object v6, LX/2P1;->g:LX/0U1;

    sget-object v7, LX/2P1;->h:LX/0U1;

    sget-object v8, LX/2P1;->i:LX/0U1;

    sget-object v9, LX/2P1;->j:LX/0U1;

    sget-object v10, LX/2P1;->l:LX/0U1;

    sget-object v11, LX/2P1;->k:LX/0U1;

    const/4 v12, 0x4

    new-array v12, v12, [LX/0U1;

    const/4 v13, 0x0

    sget-object v14, LX/2P1;->m:LX/0U1;

    aput-object v14, v12, v13

    const/4 v13, 0x1

    sget-object v14, LX/2P1;->n:LX/0U1;

    aput-object v14, v12, v13

    const/4 v13, 0x2

    sget-object v14, LX/2P1;->o:LX/0U1;

    aput-object v14, v12, v13

    const/4 v13, 0x3

    sget-object v14, LX/2P1;->p:LX/0U1;

    aput-object v14, v12, v13

    invoke-static/range {v0 .. v12}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/3Rs;->b:LX/0Px;

    .line 581671
    const-string v0, "messages"

    const-string v1, "messages_timestamp_index"

    sget-object v2, LX/2P1;->b:LX/0U1;

    .line 581672
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 581673
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, LX/2P1;->f:LX/0U1;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " DESC"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Tz;->b(Ljava/lang/String;Ljava/lang/String;LX/0Px;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/3Rs;->c:Ljava/lang/String;

    .line 581674
    const-string v0, "messages"

    const-string v1, "messages_offline_threading_id_index"

    sget-object v2, LX/2P1;->j:LX/0U1;

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Tz;->a(Ljava/lang/String;Ljava/lang/String;LX/0Px;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/3Rs;->d:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 581675
    const-string v0, "messages"

    sget-object v1, LX/3Rs;->b:LX/0Px;

    sget-object v2, LX/3Rs;->a:LX/0sv;

    invoke-direct {p0, v0, v1, v2}, LX/0Tz;-><init>(Ljava/lang/String;LX/0Px;LX/0sv;)V

    .line 581676
    return-void
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2

    .prologue
    .line 581677
    invoke-super {p0, p1}, LX/0Tz;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 581678
    sget-object v0, LX/3Rs;->c:Ljava/lang/String;

    const v1, 0x6f146d60

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x15732255

    invoke-static {v0}, LX/03h;->a(I)V

    .line 581679
    sget-object v0, LX/3Rs;->d:Ljava/lang/String;

    const v1, -0xb7636

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0xa3a9c40

    invoke-static {v0}, LX/03h;->a(I)V

    .line 581680
    return-void
.end method

.method public final a(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0

    .prologue
    .line 581681
    return-void
.end method
