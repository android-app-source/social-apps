.class public final LX/4gN;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/ipc/media/data/MediaData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:J

.field public c:Ljava/lang/String;

.field public d:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 799394
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 799395
    const/4 v0, 0x0

    iput-object v0, p0, LX/4gN;->a:Lcom/facebook/ipc/media/data/MediaData;

    .line 799396
    const-string v0, ""

    iput-object v0, p0, LX/4gN;->c:Ljava/lang/String;

    .line 799397
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/4gN;->d:J

    .line 799398
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/ipc/media/data/MediaData;)LX/4gN;
    .locals 1

    .prologue
    .line 799402
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/data/MediaData;

    iput-object v0, p0, LX/4gN;->a:Lcom/facebook/ipc/media/data/MediaData;

    .line 799403
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/4gN;
    .locals 1

    .prologue
    .line 799400
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/4gN;->c:Ljava/lang/String;

    .line 799401
    return-object p0
.end method

.method public final a()Lcom/facebook/ipc/media/data/LocalMediaData;
    .locals 2

    .prologue
    .line 799399
    new-instance v0, Lcom/facebook/ipc/media/data/LocalMediaData;

    invoke-direct {v0, p0}, Lcom/facebook/ipc/media/data/LocalMediaData;-><init>(LX/4gN;)V

    return-object v0
.end method
