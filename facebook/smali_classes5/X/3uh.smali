.class public final LX/3uh;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/3ui;

.field public b:Landroid/view/Menu;

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field public g:Z

.field public h:Z

.field public i:Z

.field public j:I

.field public k:I

.field public l:Ljava/lang/CharSequence;

.field public m:Ljava/lang/CharSequence;

.field public n:I

.field public o:C

.field public p:C

.field public q:I

.field public r:Z

.field public s:Z

.field public t:Z

.field public u:I

.field public v:I

.field public w:Ljava/lang/String;

.field public x:Ljava/lang/String;

.field public y:Ljava/lang/String;

.field public z:LX/3rR;


# direct methods
.method public constructor <init>(LX/3ui;Landroid/view/Menu;)V
    .locals 0

    .prologue
    .line 649132
    iput-object p1, p0, LX/3uh;->a:LX/3ui;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 649133
    iput-object p2, p0, LX/3uh;->b:Landroid/view/Menu;

    .line 649134
    invoke-virtual {p0}, LX/3uh;->a()V

    .line 649135
    return-void
.end method

.method public static a(Ljava/lang/String;)C
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 649190
    if-nez p0, :cond_0

    .line 649191
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/3uh;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/Class",
            "<*>;[",
            "Ljava/lang/Object;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 649183
    :try_start_0
    iget-object v0, p0, LX/3uh;->a:LX/3ui;

    iget-object v0, v0, LX/3ui;->e:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 649184
    invoke-virtual {v0, p2}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    .line 649185
    invoke-virtual {v0, p3}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 649186
    :goto_0
    return-object v0

    .line 649187
    :catch_0
    move-exception v0

    .line 649188
    const-string v1, "SupportMenuInflater"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Cannot instantiate class: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 649189
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(LX/3uh;Landroid/view/MenuItem;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 649147
    iget-boolean v0, p0, LX/3uh;->r:Z

    invoke-interface {p1, v0}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    move-result-object v0

    iget-boolean v3, p0, LX/3uh;->s:Z

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    move-result-object v0

    iget-boolean v3, p0, LX/3uh;->t:Z

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    move-result-object v3

    iget v0, p0, LX/3uh;->q:I

    if-lez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setCheckable(Z)Landroid/view/MenuItem;

    move-result-object v0

    iget-object v3, p0, LX/3uh;->m:Ljava/lang/CharSequence;

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setTitleCondensed(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    iget v3, p0, LX/3uh;->n:I

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v0

    iget-char v3, p0, LX/3uh;->o:C

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setAlphabeticShortcut(C)Landroid/view/MenuItem;

    move-result-object v0

    iget-char v3, p0, LX/3uh;->p:C

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setNumericShortcut(C)Landroid/view/MenuItem;

    .line 649148
    iget v0, p0, LX/3uh;->u:I

    if-ltz v0, :cond_0

    .line 649149
    iget v0, p0, LX/3uh;->u:I

    invoke-static {p1, v0}, LX/3rl;->a(Landroid/view/MenuItem;I)Z

    .line 649150
    :cond_0
    iget-object v0, p0, LX/3uh;->y:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 649151
    iget-object v0, p0, LX/3uh;->a:LX/3ui;

    iget-object v0, v0, LX/3ui;->e:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->isRestricted()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 649152
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The android:onClick attribute cannot be used within a restricted context"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    move v0, v2

    .line 649153
    goto :goto_0

    .line 649154
    :cond_2
    new-instance v0, LX/3ug;

    iget-object v3, p0, LX/3uh;->a:LX/3ui;

    .line 649155
    iget-object v4, v3, LX/3ui;->f:Ljava/lang/Object;

    if-nez v4, :cond_3

    .line 649156
    iget-object v4, v3, LX/3ui;->e:Landroid/content/Context;

    invoke-static {v3, v4}, LX/3ui;->a(LX/3ui;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    iput-object v4, v3, LX/3ui;->f:Ljava/lang/Object;

    .line 649157
    :cond_3
    iget-object v4, v3, LX/3ui;->f:Ljava/lang/Object;

    move-object v3, v4

    .line 649158
    iget-object v4, p0, LX/3uh;->y:Ljava/lang/String;

    invoke-direct {v0, v3, v4}, LX/3ug;-><init>(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 649159
    :cond_4
    iget v0, p0, LX/3uh;->q:I

    const/4 v3, 0x2

    if-lt v0, v3, :cond_5

    .line 649160
    instance-of v0, p1, LX/3v3;

    if-eqz v0, :cond_8

    move-object v0, p1

    .line 649161
    check-cast v0, LX/3v3;

    invoke-virtual {v0, v1}, LX/3v3;->a(Z)V

    .line 649162
    :cond_5
    :goto_1
    iget-object v0, p0, LX/3uh;->w:Ljava/lang/String;

    if-eqz v0, :cond_b

    .line 649163
    iget-object v0, p0, LX/3uh;->w:Ljava/lang/String;

    sget-object v2, LX/3ui;->a:[Ljava/lang/Class;

    iget-object v3, p0, LX/3uh;->a:LX/3ui;

    iget-object v3, v3, LX/3ui;->c:[Ljava/lang/Object;

    invoke-static {p0, v0, v2, v3}, LX/3uh;->a(LX/3uh;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 649164
    invoke-static {p1, v0}, LX/3rl;->a(Landroid/view/MenuItem;Landroid/view/View;)Landroid/view/MenuItem;

    .line 649165
    :goto_2
    iget v0, p0, LX/3uh;->v:I

    if-lez v0, :cond_6

    .line 649166
    if-nez v1, :cond_a

    .line 649167
    iget v0, p0, LX/3uh;->v:I

    invoke-static {p1, v0}, LX/3rl;->b(Landroid/view/MenuItem;I)Landroid/view/MenuItem;

    .line 649168
    :cond_6
    :goto_3
    iget-object v0, p0, LX/3uh;->z:LX/3rR;

    if-eqz v0, :cond_7

    .line 649169
    iget-object v0, p0, LX/3uh;->z:LX/3rR;

    .line 649170
    instance-of v1, p1, LX/3qv;

    if-eqz v1, :cond_c

    .line 649171
    check-cast p1, LX/3qv;

    invoke-interface {p1, v0}, LX/3qv;->a(LX/3rR;)LX/3qv;

    .line 649172
    :cond_7
    :goto_4
    return-void

    .line 649173
    :cond_8
    instance-of v0, p1, LX/3v9;

    if-eqz v0, :cond_5

    move-object v0, p1

    .line 649174
    check-cast v0, LX/3v9;

    .line 649175
    :try_start_0
    iget-object v3, v0, LX/3v9;->c:Ljava/lang/reflect/Method;

    if-nez v3, :cond_9

    .line 649176
    iget-object v3, v0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v3, LX/3qv;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string v4, "setExclusiveCheckable"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    sget-object v7, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    iput-object v3, v0, LX/3v9;->c:Ljava/lang/reflect/Method;

    .line 649177
    :cond_9
    iget-object v3, v0, LX/3v9;->c:Ljava/lang/reflect/Method;

    iget-object v4, v0, LX/3uu;->b:Ljava/lang/Object;

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 649178
    :goto_5
    goto :goto_1

    .line 649179
    :cond_a
    const-string v0, "SupportMenuInflater"

    const-string v1, "Ignoring attribute \'itemActionViewLayout\'. Action view already specified."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :cond_b
    move v1, v2

    goto :goto_2

    .line 649180
    :cond_c
    const-string v1, "MenuItemCompat"

    const-string v2, "setActionProvider: item does not implement SupportMenuItem; ignoring"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 649181
    :catch_0
    move-exception v3

    .line 649182
    const-string v4, "MenuItemWrapper"

    const-string v5, "Error while calling setExclusiveCheckable"

    invoke-static {v4, v5, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_5
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 649140
    iput v0, p0, LX/3uh;->c:I

    .line 649141
    iput v0, p0, LX/3uh;->d:I

    .line 649142
    iput v0, p0, LX/3uh;->e:I

    .line 649143
    iput v0, p0, LX/3uh;->f:I

    .line 649144
    iput-boolean v1, p0, LX/3uh;->g:Z

    .line 649145
    iput-boolean v1, p0, LX/3uh;->h:Z

    .line 649146
    return-void
.end method

.method public final c()Landroid/view/SubMenu;
    .locals 5

    .prologue
    .line 649136
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/3uh;->i:Z

    .line 649137
    iget-object v0, p0, LX/3uh;->b:Landroid/view/Menu;

    iget v1, p0, LX/3uh;->c:I

    iget v2, p0, LX/3uh;->j:I

    iget v3, p0, LX/3uh;->k:I

    iget-object v4, p0, LX/3uh;->l:Ljava/lang/CharSequence;

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/view/Menu;->addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;

    move-result-object v0

    .line 649138
    invoke-interface {v0}, Landroid/view/SubMenu;->getItem()Landroid/view/MenuItem;

    move-result-object v1

    invoke-static {p0, v1}, LX/3uh;->a(LX/3uh;Landroid/view/MenuItem;)V

    .line 649139
    return-object v0
.end method
