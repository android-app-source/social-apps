.class public LX/4cY;
.super LX/4cO;
.source ""


# instance fields
.field private final b:I

.field private final c:[B

.field private final d:Ljava/nio/charset/Charset;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 795122
    const-string v0, "text/plain"

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, LX/4cY;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/nio/charset/Charset;)V

    .line 795123
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/nio/charset/Charset;)V
    .locals 2

    .prologue
    .line 795130
    invoke-direct {p0, p2}, LX/4cO;-><init>(Ljava/lang/String;)V

    .line 795131
    const/16 v0, 0x1000

    iput v0, p0, LX/4cY;->b:I

    .line 795132
    if-nez p1, :cond_0

    .line 795133
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Text may not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 795134
    :cond_0
    if-nez p3, :cond_1

    .line 795135
    const-string v0, "US-ASCII"

    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object p3

    .line 795136
    :cond_1
    invoke-virtual {p3}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    iput-object v0, p0, LX/4cY;->c:[B

    .line 795137
    iput-object p3, p0, LX/4cY;->d:Ljava/nio/charset/Charset;

    .line 795138
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/nio/charset/Charset;)V
    .locals 1

    .prologue
    .line 795128
    const-string v0, "text/plain"

    invoke-direct {p0, p1, v0, p2}, LX/4cY;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/nio/charset/Charset;)V

    .line 795129
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 795127
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/io/OutputStream;)V
    .locals 5

    .prologue
    .line 795139
    if-nez p1, :cond_0

    .line 795140
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Output stream may not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 795141
    :cond_0
    iget-object v0, p0, LX/4cY;->c:[B

    array-length v1, v0

    .line 795142
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 795143
    iget-object v2, p0, LX/4cY;->c:[B

    sub-int v3, v1, v0

    const/16 v4, 0x1000

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    invoke-virtual {p1, v2, v0, v3}, Ljava/io/OutputStream;->write([BII)V

    .line 795144
    add-int/lit16 v0, v0, 0x1000

    goto :goto_0

    .line 795145
    :cond_1
    invoke-virtual {p1}, Ljava/io/OutputStream;->flush()V

    .line 795146
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 795126
    iget-object v0, p0, LX/4cY;->d:Ljava/nio/charset/Charset;

    invoke-virtual {v0}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 795125
    const-string v0, "8bit"

    return-object v0
.end method

.method public final d()J
    .locals 2

    .prologue
    .line 795124
    iget-object v0, p0, LX/4cY;->c:[B

    array-length v0, v0

    int-to-long v0, v0

    return-wide v0
.end method
