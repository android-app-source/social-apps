.class public abstract LX/3mY;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/ViewGroup;",
        "R:",
        "LX/3me;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "Ljava/util/List;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/1De;

.field private final c:LX/3mf;

.field private final d:Landroid/os/Looper;

.field private e:I

.field private f:I

.field private g:Landroid/view/ViewGroup;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TV;"
        }
    .end annotation
.end field

.field public h:LX/3mh;

.field public i:LX/3me;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TR;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 636649
    new-instance v0, LX/0Zi;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/3mY;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/3me;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "TR;)V"
        }
    .end annotation

    .prologue
    .line 636650
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, LX/3mY;-><init>(Landroid/content/Context;Landroid/os/Looper;LX/3me;)V

    .line 636651
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;LX/3me;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/os/Looper;",
            "TR;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 636652
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 636653
    invoke-static {v1, v1}, LX/1mh;->a(II)I

    move-result v0

    iput v0, p0, LX/3mY;->e:I

    .line 636654
    invoke-static {v1, v1}, LX/1mh;->a(II)I

    move-result v0

    iput v0, p0, LX/3mY;->f:I

    .line 636655
    new-instance v0, LX/1De;

    invoke-direct {v0, p1}, LX/1De;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/3mY;->b:LX/1De;

    .line 636656
    new-instance v0, LX/3mf;

    invoke-direct {v0}, LX/3mf;-><init>()V

    iput-object v0, p0, LX/3mY;->c:LX/3mf;

    .line 636657
    invoke-virtual {p0, p3}, LX/3mY;->a(LX/3me;)V

    .line 636658
    iput-object p2, p0, LX/3mY;->d:Landroid/os/Looper;

    .line 636659
    iget-object v0, p0, LX/3mY;->d:Landroid/os/Looper;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3mY;->d:Landroid/os/Looper;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 636660
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "If you want to compute the layout of the Binder\'s elements in the Main Thread you shouldn\'t set the MainLooper here butoverride isAsyncLayoutEnabled() and return false."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 636661
    :cond_0
    return-void
.end method

.method private static a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 636662
    invoke-interface {p0}, Ljava/util/List;->clear()V

    .line 636663
    sget-object v0, LX/3mY;->a:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 636664
    return-void
.end method

.method private declared-synchronized c(II)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 636637
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, LX/3mY;->c:LX/3mf;

    invoke-virtual {v1}, LX/3mf;->a()I

    move-result v1

    .line 636638
    const/4 v2, 0x0

    iget-object v3, p0, LX/3mY;->c:LX/3mf;

    invoke-virtual {v3}, LX/3mf;->c()I

    move-result v3

    add-int/2addr v3, v1

    add-int/lit8 v3, v3, -0x1

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 636639
    const/4 v3, 0x0

    add-int v4, p1, p2

    add-int/lit8 v4, v4, -0x1

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    .line 636640
    if-lt v3, v1, :cond_0

    if-gt p1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    monitor-exit p0

    return v0

    .line 636641
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private g(I)V
    .locals 8

    .prologue
    .line 636665
    invoke-static {p0}, LX/1dS;->b(Ljava/lang/Object;)V

    .line 636666
    invoke-virtual {p0}, LX/3mY;->e()I

    move-result v0

    sub-int/2addr v0, p1

    invoke-static {v0}, LX/3mY;->i(I)Ljava/util/List;

    move-result-object v3

    .line 636667
    iget-object v0, p0, LX/3mY;->c:LX/3mf;

    invoke-virtual {v0}, LX/3mf;->c()I

    move-result v0

    invoke-static {v0}, LX/3mY;->i(I)Ljava/util/List;

    move-result-object v4

    .line 636668
    invoke-virtual {p0}, LX/3mY;->e()I

    move-result v1

    move v0, p1

    :goto_0
    if-ge v0, v1, :cond_0

    .line 636669
    iget-object v2, p0, LX/3mY;->b:LX/1De;

    invoke-virtual {p0, v2, v0}, LX/3mY;->a(LX/1De;I)LX/1X1;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 636670
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 636671
    :cond_0
    monitor-enter p0

    .line 636672
    :try_start_0
    iget-object v0, p0, LX/3mY;->c:LX/3mf;

    .line 636673
    const/4 v1, 0x0

    iget-object v2, v0, LX/3mf;->a:LX/0YU;

    invoke-virtual {v2}, LX/0YU;->a()I

    move-result v2

    :goto_1
    if-ge v1, v2, :cond_1

    .line 636674
    iget-object v5, v0, LX/3mf;->a:LX/0YU;

    invoke-virtual {v5, v1}, LX/0YU;->f(I)Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 636675
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 636676
    :cond_1
    iget-object v0, p0, LX/3mY;->c:LX/3mf;

    .line 636677
    iget-object v1, v0, LX/3mf;->a:LX/0YU;

    invoke-virtual {v1}, LX/0YU;->b()V

    .line 636678
    new-instance v5, LX/1no;

    invoke-direct {v5}, LX/1no;-><init>()V

    .line 636679
    const/4 v0, 0x1

    .line 636680
    invoke-virtual {p0}, LX/3mY;->e()I

    move-result v6

    move v1, p1

    .line 636681
    :goto_2
    if-ge v1, v6, :cond_2

    if-eqz v0, :cond_2

    .line 636682
    sub-int v0, v1, p1

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1X1;

    .line 636683
    iget-object v2, p0, LX/3mY;->b:LX/1De;

    invoke-static {v2, v0}, LX/1cy;->a(LX/1De;LX/1X1;)LX/1me;

    move-result-object v0

    .line 636684
    const/4 v2, 0x0

    move v2, v2

    .line 636685
    iput-boolean v2, v0, LX/1me;->c:Z

    .line 636686
    move-object v0, v0

    .line 636687
    iget-object v2, p0, LX/3mY;->d:Landroid/os/Looper;

    invoke-virtual {v0, v2}, LX/1me;->a(Landroid/os/Looper;)LX/1me;

    move-result-object v0

    invoke-virtual {v0}, LX/1me;->b()LX/1dV;

    move-result-object v0

    .line 636688
    invoke-virtual {p0, v1}, LX/3mY;->e(I)I

    move-result v2

    invoke-virtual {p0, v1}, LX/3mY;->f(I)I

    move-result v7

    invoke-virtual {v0, v2, v7, v5}, LX/1dV;->a(IILX/1no;)V

    .line 636689
    iget-object v2, p0, LX/3mY;->c:LX/3mf;

    invoke-virtual {v2, v1, v0}, LX/3mf;->a(ILX/1dV;)V

    .line 636690
    iget v0, v5, LX/1no;->a:I

    iget v2, v5, LX/1no;->b:I

    invoke-virtual {p0, v1, v0, v2}, LX/3mY;->b(III)Z

    move-result v2

    .line 636691
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v2

    goto :goto_2

    .line 636692
    :cond_2
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 636693
    const/4 v0, 0x0

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v2

    move v1, v0

    :goto_3
    if-ge v1, v2, :cond_3

    .line 636694
    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1dV;

    invoke-virtual {v0}, LX/1dV;->j()V

    .line 636695
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 636696
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 636697
    :cond_3
    invoke-static {v4}, LX/3mY;->a(Ljava/util/List;)V

    .line 636698
    invoke-static {v3}, LX/3mY;->a(Ljava/util/List;)V

    .line 636699
    return-void
.end method

.method private declared-synchronized h(I)Z
    .locals 1

    .prologue
    .line 636700
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    invoke-direct {p0, p1, v0}, LX/3mY;->c(II)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static i(I)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(I)",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 636701
    sget-object v0, LX/3mY;->a:LX/0Zi;

    invoke-virtual {v0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 636702
    if-nez v0, :cond_0

    .line 636703
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p0}, Ljava/util/ArrayList;-><init>(I)V

    .line 636704
    :cond_0
    return-object v0
.end method

.method private j()Z
    .locals 1

    .prologue
    .line 636705
    monitor-enter p0

    .line 636706
    :try_start_0
    iget v0, p0, LX/3mY;->e:I

    invoke-static {v0}, LX/1mh;->a(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, LX/3mY;->f:I

    invoke-static {v0}, LX/1mh;->a(I)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 636707
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a(LX/1dV;)I
    .locals 4

    .prologue
    .line 636816
    monitor-enter p0

    .line 636817
    :try_start_0
    iget-object v0, p0, LX/3mY;->c:LX/3mf;

    .line 636818
    iget-object v1, v0, LX/3mf;->a:LX/0YU;

    .line 636819
    iget-boolean v2, v1, LX/0YU;->b:Z

    if-eqz v2, :cond_0

    .line 636820
    invoke-static {v1}, LX/0YU;->d(LX/0YU;)V

    .line 636821
    :cond_0
    const/4 v2, 0x0

    :goto_0
    iget v3, v1, LX/0YU;->e:I

    if-ge v2, v3, :cond_3

    .line 636822
    iget-object v3, v1, LX/0YU;->d:[Ljava/lang/Object;

    aget-object v3, v3, v2

    if-ne v3, p1, :cond_2

    .line 636823
    :goto_1
    move v1, v2

    .line 636824
    if-gez v1, :cond_1

    .line 636825
    :goto_2
    move v0, v1

    .line 636826
    monitor-exit p0

    return v0

    .line 636827
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    iget-object v2, v0, LX/3mf;->a:LX/0YU;

    invoke-virtual {v2, v1}, LX/0YU;->e(I)I

    move-result v1

    goto :goto_2

    .line 636828
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 636829
    :cond_3
    const/4 v2, -0x1

    goto :goto_1
.end method

.method public abstract a(LX/1De;I)LX/1X1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "I)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation
.end method

.method public final a(II)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 636708
    invoke-static {}, LX/1dS;->b()V

    .line 636709
    invoke-direct {p0}, LX/3mY;->j()Z

    move-result v1

    if-nez v1, :cond_1

    .line 636710
    :cond_0
    :goto_0
    return-void

    .line 636711
    :cond_1
    const/4 v1, 0x1

    .line 636712
    monitor-enter p0

    .line 636713
    :try_start_0
    iget-object v2, p0, LX/3mY;->c:LX/3mf;

    invoke-virtual {v2}, LX/3mf;->a()I

    move-result v2

    if-ge p1, v2, :cond_2

    .line 636714
    iget-object v1, p0, LX/3mY;->c:LX/3mf;

    invoke-virtual {v1, p2}, LX/3mf;->d(I)V

    move v1, v0

    .line 636715
    :cond_2
    iget-object v2, p0, LX/3mY;->c:LX/3mf;

    invoke-virtual {v2}, LX/3mf;->a()I

    move-result v2

    iget-object v3, p0, LX/3mY;->c:LX/3mf;

    invoke-virtual {v3}, LX/3mf;->c()I

    move-result v3

    add-int/2addr v2, v3

    if-le p1, v2, :cond_3

    move v1, v0

    .line 636716
    :cond_3
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 636717
    if-eqz v1, :cond_7

    .line 636718
    invoke-static {p2}, LX/3mY;->i(I)Ljava/util/List;

    move-result-object v2

    .line 636719
    add-int v3, p1, p2

    move v1, p1

    :goto_1
    if-ge v1, v3, :cond_4

    .line 636720
    iget-object v4, p0, LX/3mY;->b:LX/1De;

    invoke-virtual {p0, v4, v1}, LX/3mY;->a(LX/1De;I)LX/1X1;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 636721
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 636722
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 636723
    :cond_4
    monitor-enter p0

    .line 636724
    :try_start_2
    iget-object v1, p0, LX/3mY;->c:LX/3mf;

    invoke-virtual {v1}, LX/3mf;->a()I

    move-result v1

    if-lt p1, v1, :cond_6

    iget-object v1, p0, LX/3mY;->c:LX/3mf;

    invoke-virtual {v1}, LX/3mf;->a()I

    move-result v1

    iget-object v3, p0, LX/3mY;->c:LX/3mf;

    invoke-virtual {v3}, LX/3mf;->c()I

    move-result v3

    add-int/2addr v1, v3

    if-gt p1, v1, :cond_6

    move v1, v0

    .line 636725
    :goto_2
    if-ge v1, p2, :cond_6

    .line 636726
    iget-object v3, p0, LX/3mY;->b:LX/1De;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1X1;

    invoke-static {v3, v0}, LX/1cy;->a(LX/1De;LX/1X1;)LX/1me;

    move-result-object v0

    .line 636727
    const/4 v3, 0x0

    move v3, v3

    .line 636728
    iput-boolean v3, v0, LX/1me;->c:Z

    .line 636729
    move-object v0, v0

    .line 636730
    iget-object v3, p0, LX/3mY;->d:Landroid/os/Looper;

    invoke-virtual {v0, v3}, LX/1me;->a(Landroid/os/Looper;)LX/1me;

    move-result-object v0

    invoke-virtual {v0}, LX/1me;->b()LX/1dV;

    move-result-object v0

    .line 636731
    invoke-virtual {p0}, LX/3mY;->g()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 636732
    invoke-virtual {p0, v1}, LX/3mY;->e(I)I

    move-result v3

    invoke-virtual {p0, v1}, LX/3mY;->f(I)I

    move-result v4

    invoke-virtual {v0, v3, v4}, LX/1dV;->b(II)V

    .line 636733
    :goto_3
    iget-object v3, p0, LX/3mY;->c:LX/3mf;

    add-int v4, v1, p1

    invoke-virtual {v3, v4, v0}, LX/3mf;->b(ILX/1dV;)V

    .line 636734
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 636735
    :cond_5
    invoke-virtual {p0, v1}, LX/3mY;->e(I)I

    move-result v3

    invoke-virtual {p0, v1}, LX/3mY;->f(I)I

    move-result v4

    invoke-virtual {v0, v3, v4}, LX/1dV;->a(II)V

    goto :goto_3

    .line 636736
    :catchall_1
    move-exception v0

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    :cond_6
    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 636737
    if-eqz v2, :cond_7

    .line 636738
    invoke-static {v2}, LX/3mY;->a(Ljava/util/List;)V

    .line 636739
    :cond_7
    iget-object v0, p0, LX/3mY;->h:LX/3mh;

    if-eqz v0, :cond_0

    .line 636740
    iget-object v0, p0, LX/3mY;->h:LX/3mh;

    invoke-interface {v0, p1, p2}, LX/3mh;->a_(II)V

    goto/16 :goto_0
.end method

.method public final a(III)V
    .locals 11

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 636741
    invoke-static {p0}, LX/1dS;->b(Ljava/lang/Object;)V

    .line 636742
    and-int/lit8 v1, p3, 0x1

    if-eqz v1, :cond_0

    move v4, v0

    .line 636743
    :goto_0
    and-int/lit8 v1, p3, 0x2

    if-eqz v1, :cond_1

    move v1, v0

    .line 636744
    :goto_1
    invoke-virtual {p0}, LX/3mY;->e()I

    move-result v0

    invoke-static {v0}, LX/3mY;->i(I)Ljava/util/List;

    move-result-object v5

    .line 636745
    invoke-virtual {p0}, LX/3mY;->e()I

    move-result v0

    invoke-static {v0}, LX/3mY;->i(I)Ljava/util/List;

    move-result-object v6

    .line 636746
    add-int v3, p1, p2

    move v0, p1

    :goto_2
    if-ge v0, v3, :cond_2

    .line 636747
    iget-object v7, p0, LX/3mY;->b:LX/1De;

    invoke-virtual {p0, v7, v0}, LX/3mY;->a(LX/1De;I)LX/1X1;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 636748
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_0
    move v4, v2

    .line 636749
    goto :goto_0

    :cond_1
    move v1, v2

    .line 636750
    goto :goto_1

    .line 636751
    :cond_2
    monitor-enter p0

    .line 636752
    :try_start_0
    iget-object v0, p0, LX/3mY;->c:LX/3mf;

    invoke-virtual {v0}, LX/3mf;->a()I

    move-result v3

    .line 636753
    iget-object v0, p0, LX/3mY;->c:LX/3mf;

    invoke-virtual {v0}, LX/3mf;->c()I

    move-result v7

    .line 636754
    const/4 v0, 0x0

    invoke-static {p1, v3}, Ljava/lang/Math;->min(II)I

    move-result v8

    invoke-static {v0, v8}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 636755
    add-int v8, p1, p2

    add-int/2addr v3, v7

    invoke-static {v8, v3}, Ljava/lang/Math;->max(II)I

    move-result v7

    move v3, v0

    .line 636756
    :goto_3
    if-ge v3, v7, :cond_9

    .line 636757
    iget-object v0, p0, LX/3mY;->c:LX/3mf;

    invoke-virtual {v0, v3}, LX/3mf;->c(I)LX/1dV;

    move-result-object v8

    .line 636758
    if-eqz v8, :cond_5

    if-eqz v1, :cond_5

    if-lt v3, p1, :cond_3

    add-int v0, p1, p2

    if-lt v3, v0, :cond_5

    .line 636759
    :cond_3
    invoke-interface {v5, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 636760
    iget-object v0, p0, LX/3mY;->c:LX/3mf;

    .line 636761
    iget-object v8, v0, LX/3mf;->a:LX/0YU;

    .line 636762
    invoke-virtual {v8, v3}, LX/0YU;->b(I)V

    .line 636763
    :cond_4
    :goto_4
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3

    .line 636764
    :cond_5
    if-lt v3, p1, :cond_4

    add-int v0, p1, p2

    if-ge v3, v0, :cond_4

    .line 636765
    sub-int v0, v3, p1

    invoke-interface {v6, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1X1;

    .line 636766
    if-nez v8, :cond_7

    .line 636767
    iget-object v8, p0, LX/3mY;->b:LX/1De;

    invoke-static {v8, v0}, LX/1cy;->a(LX/1De;LX/1X1;)LX/1me;

    move-result-object v0

    .line 636768
    const/4 v8, 0x0

    move v8, v8

    .line 636769
    iput-boolean v8, v0, LX/1me;->c:Z

    .line 636770
    move-object v0, v0

    .line 636771
    iget-object v8, p0, LX/3mY;->d:Landroid/os/Looper;

    invoke-virtual {v0, v8}, LX/1me;->a(Landroid/os/Looper;)LX/1me;

    move-result-object v0

    invoke-virtual {v0}, LX/1me;->b()LX/1dV;

    move-result-object v0

    .line 636772
    invoke-virtual {p0}, LX/3mY;->g()Z

    move-result v8

    if-eqz v8, :cond_6

    .line 636773
    invoke-virtual {p0, v3}, LX/3mY;->e(I)I

    move-result v8

    invoke-virtual {p0, v3}, LX/3mY;->f(I)I

    move-result v9

    invoke-virtual {v0, v8, v9}, LX/1dV;->b(II)V

    .line 636774
    :goto_5
    iget-object v8, p0, LX/3mY;->c:LX/3mf;

    invoke-virtual {v8, v3, v0}, LX/3mf;->a(ILX/1dV;)V

    goto :goto_4

    .line 636775
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 636776
    :cond_6
    :try_start_1
    invoke-virtual {p0, v3}, LX/3mY;->e(I)I

    move-result v8

    invoke-virtual {p0, v3}, LX/3mY;->f(I)I

    move-result v9

    invoke-virtual {v0, v8, v9}, LX/1dV;->a(II)V

    goto :goto_5

    .line 636777
    :cond_7
    if-eqz v4, :cond_4

    .line 636778
    invoke-virtual {p0}, LX/3mY;->g()Z

    move-result v9

    if-eqz v9, :cond_8

    .line 636779
    invoke-virtual {p0, v3}, LX/3mY;->e(I)I

    move-result v9

    invoke-virtual {p0, v3}, LX/3mY;->f(I)I

    move-result v10

    invoke-virtual {v8, v0, v9, v10}, LX/1dV;->a(LX/1X1;II)V

    goto :goto_4

    .line 636780
    :cond_8
    invoke-virtual {p0, v3}, LX/3mY;->e(I)I

    move-result v9

    invoke-virtual {p0, v3}, LX/3mY;->f(I)I

    move-result v10

    invoke-virtual {v8, v0, v9, v10}, LX/1dV;->b(LX/1X1;II)V

    goto :goto_4

    .line 636781
    :cond_9
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 636782
    invoke-static {v6}, LX/3mY;->a(Ljava/util/List;)V

    .line 636783
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v1

    :goto_6
    if-ge v2, v1, :cond_a

    .line 636784
    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1dV;

    invoke-virtual {v0}, LX/1dV;->j()V

    .line 636785
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 636786
    :cond_a
    invoke-static {v5}, LX/3mY;->a(Ljava/util/List;)V

    .line 636787
    return-void
.end method

.method public final a(LX/3me;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;)V"
        }
    .end annotation

    .prologue
    .line 636788
    if-nez p1, :cond_0

    .line 636789
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The range controller should not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 636790
    :cond_0
    iget-object v0, p0, LX/3mY;->i:LX/3me;

    if-eqz v0, :cond_1

    .line 636791
    iget-object v0, p0, LX/3mY;->i:LX/3me;

    const/4 v1, 0x0

    .line 636792
    iput-object v1, v0, LX/3me;->a:LX/3mY;

    .line 636793
    :cond_1
    iput-object p1, p0, LX/3mY;->i:LX/3me;

    .line 636794
    iget-object v0, p0, LX/3mY;->i:LX/3me;

    .line 636795
    iput-object p0, v0, LX/3me;->a:LX/3mY;

    .line 636796
    return-void
.end method

.method public final a(Landroid/view/ViewGroup;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 636797
    invoke-static {}, LX/1dS;->b()V

    .line 636798
    iget-object v0, p0, LX/3mY;->g:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 636799
    iget-object v0, p0, LX/3mY;->g:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0}, LX/3mY;->f(Landroid/view/ViewGroup;)V

    .line 636800
    :cond_0
    iput-object p1, p0, LX/3mY;->g:Landroid/view/ViewGroup;

    .line 636801
    iget-object v0, p0, LX/3mY;->g:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0}, LX/3mY;->e(Landroid/view/ViewGroup;)V

    .line 636802
    return-void
.end method

.method public final b(II)V
    .locals 3

    .prologue
    const/high16 v1, 0x40000000    # 2.0f

    .line 636803
    invoke-static {p1, v1}, LX/1mh;->a(II)I

    move-result v0

    .line 636804
    invoke-static {p2, v1}, LX/1mh;->a(II)I

    move-result v1

    .line 636805
    monitor-enter p0

    .line 636806
    :try_start_0
    iget v2, p0, LX/3mY;->f:I

    if-ne v2, v1, :cond_0

    iget v2, p0, LX/3mY;->e:I

    if-ne v2, v0, :cond_0

    .line 636807
    monitor-exit p0

    .line 636808
    :goto_0
    return-void

    .line 636809
    :cond_0
    iput v0, p0, LX/3mY;->e:I

    .line 636810
    iput v1, p0, LX/3mY;->f:I

    .line 636811
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 636812
    sget-boolean v0, LX/1V5;->h:Z

    if-eqz v0, :cond_1

    .line 636813
    invoke-virtual {p0}, LX/3mY;->bF_()I

    move-result v0

    invoke-direct {p0, v0}, LX/3mY;->g(I)V

    goto :goto_0

    .line 636814
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 636815
    :cond_1
    invoke-virtual {p0}, LX/3mY;->f()V

    goto :goto_0
.end method

.method public final b(Landroid/view/ViewGroup;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 636642
    invoke-static {}, LX/1dS;->b()V

    .line 636643
    iget-object v0, p0, LX/3mY;->g:Landroid/view/ViewGroup;

    if-eq v0, p1, :cond_0

    .line 636644
    iget-object v0, p0, LX/3mY;->g:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0}, LX/3mY;->c(Landroid/view/ViewGroup;)V

    .line 636645
    iget-object v0, p0, LX/3mY;->g:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0}, LX/3mY;->d(Landroid/view/ViewGroup;)V

    .line 636646
    const/4 v0, 0x0

    iput-object v0, p0, LX/3mY;->g:Landroid/view/ViewGroup;

    .line 636647
    invoke-virtual {p0, p1}, LX/3mY;->a(Landroid/view/ViewGroup;)V

    .line 636648
    :cond_0
    return-void
.end method

.method public abstract b(III)Z
.end method

.method public final bE_()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 636560
    invoke-static {}, LX/1dS;->b()V

    .line 636561
    invoke-direct {p0}, LX/3mY;->j()Z

    move-result v0

    if-nez v0, :cond_1

    .line 636562
    :cond_0
    :goto_0
    return-void

    .line 636563
    :cond_1
    sget-boolean v0, LX/1V5;->h:Z

    if-eqz v0, :cond_2

    .line 636564
    invoke-direct {p0, v2}, LX/3mY;->g(I)V

    .line 636565
    :goto_1
    iget-object v0, p0, LX/3mY;->h:LX/3mh;

    if-eqz v0, :cond_0

    .line 636566
    iget-object v0, p0, LX/3mY;->h:LX/3mh;

    invoke-interface {v0}, LX/3mh;->bG_()V

    goto :goto_0

    .line 636567
    :cond_2
    invoke-virtual {p0}, LX/3mY;->e()I

    move-result v0

    const/4 v1, 0x3

    invoke-virtual {p0, v2, v0, v1}, LX/3mY;->a(III)V

    goto :goto_1
.end method

.method public bF_()I
    .locals 1

    .prologue
    .line 636568
    const/4 v0, 0x0

    return v0
.end method

.method public final b_(I)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 636569
    invoke-static {}, LX/1dS;->b()V

    .line 636570
    invoke-direct {p0}, LX/3mY;->j()Z

    move-result v0

    if-nez v0, :cond_1

    .line 636571
    :cond_0
    :goto_0
    return-void

    .line 636572
    :cond_1
    invoke-direct {p0, p1}, LX/3mY;->h(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 636573
    invoke-virtual {p0, p1, v1, v1}, LX/3mY;->a(III)V

    .line 636574
    :cond_2
    iget-object v0, p0, LX/3mY;->h:LX/3mh;

    if-eqz v0, :cond_0

    .line 636575
    iget-object v0, p0, LX/3mY;->h:LX/3mh;

    invoke-interface {v0, p1}, LX/3mh;->e(I)V

    goto :goto_0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 636576
    iget v0, p0, LX/3mY;->f:I

    invoke-static {v0}, LX/1mh;->b(I)I

    move-result v0

    return v0
.end method

.method public final c(I)V
    .locals 5

    .prologue
    .line 636577
    invoke-static {}, LX/1dS;->b()V

    .line 636578
    invoke-direct {p0}, LX/3mY;->j()Z

    move-result v0

    if-nez v0, :cond_1

    .line 636579
    :cond_0
    :goto_0
    return-void

    .line 636580
    :cond_1
    const/4 v0, 0x0

    .line 636581
    monitor-enter p0

    .line 636582
    :try_start_0
    iget-object v1, p0, LX/3mY;->c:LX/3mf;

    invoke-virtual {v1}, LX/3mf;->a()I

    move-result v1

    if-ge p1, v1, :cond_4

    .line 636583
    iget-object v1, p0, LX/3mY;->c:LX/3mf;

    const/4 v2, 0x1

    .line 636584
    invoke-virtual {v1}, LX/3mf;->a()I

    move-result v3

    invoke-virtual {v1}, LX/3mf;->c()I

    move-result v4

    invoke-static {v1, v3, v4, v2}, LX/3mf;->b(LX/3mf;III)V

    .line 636585
    :cond_2
    :goto_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 636586
    if-eqz v0, :cond_3

    .line 636587
    invoke-virtual {v0}, LX/1dV;->j()V

    .line 636588
    :cond_3
    iget-object v0, p0, LX/3mY;->h:LX/3mh;

    if-eqz v0, :cond_0

    .line 636589
    iget-object v0, p0, LX/3mY;->h:LX/3mh;

    invoke-interface {v0, p1}, LX/3mh;->f(I)V

    goto :goto_0

    .line 636590
    :cond_4
    :try_start_1
    invoke-direct {p0, p1}, LX/3mY;->h(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 636591
    iget-object v0, p0, LX/3mY;->c:LX/3mf;

    invoke-virtual {v0, p1}, LX/3mf;->c(I)LX/1dV;

    move-result-object v0

    .line 636592
    iget-object v1, p0, LX/3mY;->c:LX/3mf;

    invoke-virtual {v1, p1}, LX/3mf;->b(I)V

    goto :goto_1

    .line 636593
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final c(Landroid/view/ViewGroup;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 636594
    invoke-static {}, LX/1dS;->b()V

    .line 636595
    iget-object v0, p0, LX/3mY;->g:Landroid/view/ViewGroup;

    if-eq p1, v0, :cond_0

    .line 636596
    :cond_0
    return-void
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 636597
    iget v0, p0, LX/3mY;->e:I

    invoke-static {v0}, LX/1mh;->b(I)I

    move-result v0

    return v0
.end method

.method public final d(I)LX/1dV;
    .locals 1

    .prologue
    .line 636598
    monitor-enter p0

    .line 636599
    :try_start_0
    iget-object v0, p0, LX/3mY;->c:LX/3mf;

    invoke-virtual {v0, p1}, LX/3mf;->c(I)LX/1dV;

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 636600
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final d(Landroid/view/ViewGroup;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 636601
    invoke-static {}, LX/1dS;->b()V

    .line 636602
    iget-object v0, p0, LX/3mY;->g:Landroid/view/ViewGroup;

    if-eq p1, v0, :cond_0

    .line 636603
    :goto_0
    return-void

    .line 636604
    :cond_0
    iget-object v0, p0, LX/3mY;->g:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0}, LX/3mY;->f(Landroid/view/ViewGroup;)V

    .line 636605
    const/4 v0, 0x0

    iput-object v0, p0, LX/3mY;->g:Landroid/view/ViewGroup;

    goto :goto_0
.end method

.method public abstract e()I
.end method

.method public e(I)I
    .locals 1

    .prologue
    .line 636606
    iget v0, p0, LX/3mY;->e:I

    return v0
.end method

.method public abstract e(Landroid/view/ViewGroup;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation
.end method

.method public f(I)I
    .locals 1

    .prologue
    .line 636607
    iget v0, p0, LX/3mY;->f:I

    return v0
.end method

.method public f()V
    .locals 3

    .prologue
    .line 636608
    const/4 v0, 0x0

    invoke-virtual {p0}, LX/3mY;->e()I

    move-result v1

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, LX/3mY;->a(III)V

    .line 636609
    return-void
.end method

.method public abstract f(Landroid/view/ViewGroup;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation
.end method

.method public abstract g()Z
.end method

.method public final u_(I)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 636610
    invoke-static {}, LX/1dS;->b()V

    .line 636611
    invoke-direct {p0}, LX/3mY;->j()Z

    move-result v1

    if-nez v1, :cond_1

    .line 636612
    :cond_0
    :goto_0
    return-void

    .line 636613
    :cond_1
    monitor-enter p0

    .line 636614
    :try_start_0
    invoke-direct {p0, p1}, LX/3mY;->h(I)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, LX/3mY;->c:LX/3mf;

    invoke-virtual {v1}, LX/3mf;->a()I

    move-result v1

    iget-object v2, p0, LX/3mY;->c:LX/3mf;

    invoke-virtual {v2}, LX/3mf;->c()I

    move-result v2

    add-int/2addr v1, v2

    if-eq p1, v1, :cond_3

    .line 636615
    iget-object v0, p0, LX/3mY;->c:LX/3mf;

    invoke-virtual {v0}, LX/3mf;->a()I

    move-result v0

    if-ge p1, v0, :cond_2

    .line 636616
    iget-object v0, p0, LX/3mY;->c:LX/3mf;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/3mf;->d(I)V

    .line 636617
    :cond_2
    const/4 v0, 0x0

    .line 636618
    :cond_3
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 636619
    if-eqz v0, :cond_6

    .line 636620
    iget-object v0, p0, LX/3mY;->b:LX/1De;

    invoke-virtual {p0, v0, p1}, LX/3mY;->a(LX/1De;I)LX/1X1;

    move-result-object v0

    .line 636621
    monitor-enter p0

    .line 636622
    :try_start_1
    invoke-direct {p0, p1}, LX/3mY;->h(I)Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, LX/3mY;->c:LX/3mf;

    invoke-virtual {v1}, LX/3mf;->a()I

    move-result v1

    iget-object v2, p0, LX/3mY;->c:LX/3mf;

    invoke-virtual {v2}, LX/3mf;->c()I

    move-result v2

    add-int/2addr v1, v2

    if-ne p1, v1, :cond_5

    .line 636623
    :cond_4
    iget-object v1, p0, LX/3mY;->b:LX/1De;

    invoke-static {v1, v0}, LX/1cy;->a(LX/1De;LX/1X1;)LX/1me;

    move-result-object v0

    .line 636624
    const/4 v1, 0x0

    move v1, v1

    .line 636625
    iput-boolean v1, v0, LX/1me;->c:Z

    .line 636626
    move-object v0, v0

    .line 636627
    iget-object v1, p0, LX/3mY;->d:Landroid/os/Looper;

    invoke-virtual {v0, v1}, LX/1me;->a(Landroid/os/Looper;)LX/1me;

    move-result-object v0

    invoke-virtual {v0}, LX/1me;->b()LX/1dV;

    move-result-object v0

    .line 636628
    invoke-virtual {p0}, LX/3mY;->g()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 636629
    invoke-virtual {p0, p1}, LX/3mY;->e(I)I

    move-result v1

    invoke-virtual {p0, p1}, LX/3mY;->f(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/1dV;->b(II)V

    .line 636630
    :goto_1
    iget-object v1, p0, LX/3mY;->c:LX/3mf;

    invoke-virtual {v1, p1, v0}, LX/3mf;->b(ILX/1dV;)V

    .line 636631
    :cond_5
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 636632
    :cond_6
    iget-object v0, p0, LX/3mY;->h:LX/3mh;

    if-eqz v0, :cond_0

    .line 636633
    iget-object v0, p0, LX/3mY;->h:LX/3mh;

    invoke-interface {v0, p1}, LX/3mh;->c_(I)V

    goto :goto_0

    .line 636634
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 636635
    :cond_7
    :try_start_3
    invoke-virtual {p0, p1}, LX/3mY;->e(I)I

    move-result v1

    invoke-virtual {p0, p1}, LX/3mY;->f(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/1dV;->a(II)V

    goto :goto_1

    .line 636636
    :catchall_1
    move-exception v0

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
