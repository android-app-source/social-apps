.class public final LX/4VK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3d4;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/3d4",
        "<",
        "LX/16f;",
        ">;",
        "Lcom/facebook/graphql/visitor/MutationReceiver;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/4VL;

.field private final b:Z

.field public c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class;",
            "LX/40U;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:LX/16f;

.field private e:LX/16f;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:LX/16f;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Z

.field public h:Z


# direct methods
.method public constructor <init>(LX/4VL;Z)V
    .locals 0

    .prologue
    .line 742214
    iput-object p1, p0, LX/4VK;->a:LX/4VL;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 742215
    iput-boolean p2, p0, LX/4VK;->b:Z

    .line 742216
    return-void
.end method

.method public static d(LX/4VK;)LX/16f;
    .locals 1

    .prologue
    .line 742252
    iget-boolean v0, p0, LX/4VK;->b:Z

    if-eqz v0, :cond_0

    .line 742253
    iget-object v0, p0, LX/4VK;->d:LX/16f;

    .line 742254
    :goto_0
    return-object v0

    .line 742255
    :cond_0
    iget-object v0, p0, LX/4VK;->e:LX/16f;

    if-nez v0, :cond_1

    .line 742256
    iget-object v0, p0, LX/4VK;->d:LX/16f;

    invoke-interface {v0}, LX/0jT;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/16f;

    iput-object v0, p0, LX/4VK;->e:LX/16f;

    .line 742257
    :cond_1
    iget-object v0, p0, LX/4VK;->e:LX/16f;

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 742230
    check-cast p1, LX/16f;

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 742231
    iput-object p1, p0, LX/4VK;->d:LX/16f;

    .line 742232
    iput-object v0, p0, LX/4VK;->e:LX/16f;

    .line 742233
    iput-object v0, p0, LX/4VK;->f:LX/16f;

    .line 742234
    iput-boolean v1, p0, LX/4VK;->g:Z

    .line 742235
    iput-boolean v1, p0, LX/4VK;->h:Z

    .line 742236
    iget-object v2, p0, LX/4VK;->a:LX/4VL;

    iget-object v2, v2, LX/4VL;->b:[LX/4VT;

    array-length v3, v2

    :goto_0
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 742237
    invoke-interface {v4}, LX/4VT;->b()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 742238
    iget-object v5, p0, LX/4VK;->c:Ljava/util/Map;

    if-nez v5, :cond_0

    .line 742239
    new-instance v5, LX/026;

    invoke-direct {v5}, LX/026;-><init>()V

    iput-object v5, p0, LX/4VK;->c:Ljava/util/Map;

    .line 742240
    :cond_0
    iget-object v5, p0, LX/4VK;->c:Ljava/util/Map;

    invoke-interface {v4}, LX/4VT;->b()Ljava/lang/Class;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/40U;

    .line 742241
    if-nez v5, :cond_1

    .line 742242
    invoke-interface {p1, p0}, LX/16f;->a(LX/4VK;)LX/40U;

    move-result-object v5

    .line 742243
    iget-object v6, p0, LX/4VK;->c:Ljava/util/Map;

    invoke-interface {v4}, LX/4VT;->b()Ljava/lang/Class;

    move-result-object v7

    invoke-interface {v6, v7, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 742244
    :cond_1
    move-object v5, v5

    .line 742245
    invoke-interface {v4, p1, v5}, LX/4VT;->a(LX/16f;LX/40U;)V

    .line 742246
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 742247
    :cond_3
    iget-boolean v1, p0, LX/4VK;->g:Z

    if-eqz v1, :cond_4

    .line 742248
    :goto_1
    return-object v0

    .line 742249
    :cond_4
    iget-object v0, p0, LX/4VK;->f:LX/16f;

    if-eqz v0, :cond_5

    .line 742250
    iget-object v0, p0, LX/4VK;->f:LX/16f;

    goto :goto_1

    .line 742251
    :cond_5
    iget-object v0, p0, LX/4VK;->e:LX/16f;

    if-eqz v0, :cond_6

    iget-object v0, p0, LX/4VK;->e:LX/16f;

    goto :goto_1

    :cond_6
    iget-object v0, p0, LX/4VK;->d:LX/16f;

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 742223
    iget-boolean v0, p0, LX/4VK;->g:Z

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 742224
    iget-object v0, p0, LX/4VK;->f:LX/16f;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 742225
    invoke-static {p0}, LX/4VK;->d(LX/4VK;)LX/16f;

    move-result-object v0

    iget-boolean v3, p0, LX/4VK;->b:Z

    if-nez v3, :cond_0

    move v2, v1

    :cond_0
    invoke-interface {v0, p1, p2, v2}, LX/16f;->a(Ljava/lang/String;Ljava/lang/Object;Z)V

    .line 742226
    iput-boolean v1, p0, LX/4VK;->h:Z

    .line 742227
    return-void

    :cond_1
    move v0, v2

    .line 742228
    goto :goto_0

    :cond_2
    move v0, v2

    .line 742229
    goto :goto_1
.end method

.method public final b(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 742217
    iget-boolean v0, p0, LX/4VK;->g:Z

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 742218
    iget-object v0, p0, LX/4VK;->f:LX/16f;

    if-nez v0, :cond_0

    move v2, v1

    :cond_0
    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 742219
    invoke-static {p0}, LX/4VK;->d(LX/4VK;)LX/16f;

    move-result-object v0

    invoke-interface {v0, p1, p2}, LX/16f;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 742220
    iput-boolean v1, p0, LX/4VK;->h:Z

    .line 742221
    return-void

    :cond_1
    move v0, v2

    .line 742222
    goto :goto_0
.end method
