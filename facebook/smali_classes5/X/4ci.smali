.class public LX/4ci;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 795301
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0W3;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/http/onion/OnionRewriteRule;",
            ">;"
        }
    .end annotation

    .prologue
    .line 795287
    sget-wide v0, LX/0X5;->et:J

    const-string v2, "[]"

    invoke-interface {p0, v0, v1, v2}, LX/0W4;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 795288
    :try_start_0
    invoke-static {}, LX/0lB;->i()LX/0lB;

    move-result-object v1

    new-instance v2, LX/4cg;

    invoke-direct {v2}, LX/4cg;-><init>()V

    invoke-virtual {v1, v0, v2}, LX/0lC;->a(Ljava/lang/String;LX/266;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 795289
    :goto_0
    move-object v0, v1

    .line 795290
    return-object v0

    .line 795291
    :catch_0
    move-exception v1

    .line 795292
    const-class v2, LX/4ci;

    const-string p0, "Failed to decode onion rules"

    invoke-static {v2, p0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 795293
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method public static b(LX/0W3;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 795294
    sget-wide v0, LX/0X5;->eu:J

    const-string v2, "[]"

    invoke-interface {p0, v0, v1, v2}, LX/0W4;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 795295
    :try_start_0
    invoke-static {}, LX/0lB;->i()LX/0lB;

    move-result-object v1

    new-instance v2, LX/4ch;

    invoke-direct {v2}, LX/4ch;-><init>()V

    invoke-virtual {v1, v0, v2}, LX/0lC;->a(Ljava/lang/String;LX/266;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 795296
    :goto_0
    move-object v0, v1

    .line 795297
    return-object v0

    .line 795298
    :catch_0
    move-exception v1

    .line 795299
    const-class v2, LX/4ci;

    const-string p0, "Failed to decode onion string array"

    invoke-static {v2, p0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 795300
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method
