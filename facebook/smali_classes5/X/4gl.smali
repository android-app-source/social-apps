.class public LX/4gl;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 800107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/facebook/messaging/business/messengerextensions/model/MessengerExtensionProperties;
    .locals 3
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 800108
    invoke-static {p0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 800109
    :goto_0
    return-object v0

    .line 800110
    :cond_0
    :try_start_0
    invoke-static {}, LX/0lB;->i()LX/0lB;

    move-result-object v1

    invoke-virtual {v1, p0}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 800111
    new-instance v1, LX/4gk;

    invoke-direct {v1}, LX/4gk;-><init>()V

    .line 800112
    const-string v2, "is_structured_menu_type_message_hidden"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->g(LX/0lF;)Z

    move-result v2

    .line 800113
    iput-boolean v2, v1, LX/4gk;->a:Z

    .line 800114
    const-string v2, "top_level_ctas"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/4gg;->a(Ljava/lang/String;)LX/0Px;

    move-result-object v2

    .line 800115
    iput-object v2, v1, LX/4gk;->b:LX/0Px;

    .line 800116
    const-string v2, "cart"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 800117
    if-nez v0, :cond_1

    .line 800118
    const/4 v2, 0x0

    .line 800119
    :goto_1
    move-object v0, v2

    .line 800120
    iput-object v0, v1, LX/4gk;->c:Lcom/facebook/messaging/business/messengerextensions/model/MessengerCart;

    .line 800121
    invoke-virtual {v1}, LX/4gk;->a()Lcom/facebook/messaging/business/messengerextensions/model/MessengerExtensionProperties;

    move-result-object v0

    goto :goto_0

    .line 800122
    :catch_0
    goto :goto_0

    :cond_1
    new-instance v2, LX/4gi;

    invoke-direct {v2}, LX/4gi;-><init>()V

    const-string p0, "cart_url"

    invoke-virtual {v0, p0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object p0

    invoke-static {p0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object p0

    .line 800123
    iput-object p0, v2, LX/4gi;->b:Ljava/lang/String;

    .line 800124
    move-object v2, v2

    .line 800125
    const-string p0, "cart_native_url"

    invoke-virtual {v0, p0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object p0

    invoke-static {p0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object p0

    .line 800126
    iput-object p0, v2, LX/4gi;->c:Ljava/lang/String;

    .line 800127
    move-object v2, v2

    .line 800128
    const-string p0, "cart_item_count"

    invoke-virtual {v0, p0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object p0

    invoke-static {p0}, LX/16N;->d(LX/0lF;)I

    move-result p0

    .line 800129
    iput p0, v2, LX/4gi;->a:I

    .line 800130
    move-object v2, v2

    .line 800131
    invoke-virtual {v2}, LX/4gi;->a()Lcom/facebook/messaging/business/messengerextensions/model/MessengerCart;

    move-result-object v2

    goto :goto_1
.end method
