.class public LX/3i8;
.super LX/3i9;
.source ""

# interfaces
.implements LX/35p;
.implements LX/3iA;
.implements LX/3iB;
.implements LX/3FR;


# instance fields
.field public a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public b:Lcom/facebook/feedplugins/multishare/MultiShareInlineVideoView;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public c:LX/3hW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Z

.field private final e:Lcom/facebook/attachments/angora/InstantArticleIconView;

.field public final f:Landroid/widget/TextView;

.field public final g:Landroid/widget/TextView;

.field private final h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private final i:Landroid/view/View;

.field public final j:Landroid/view/View;

.field public k:LX/4Ac;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4Ac",
            "<",
            "LX/1af;",
            ">;"
        }
    .end annotation
.end field

.field public l:I

.field public m:I

.field public n:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;",
            ">;"
        }
    .end annotation
.end field

.field public o:LX/1ap;

.field private p:Ljava/util/Timer;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 628777
    invoke-direct {p0, p1}, LX/3i9;-><init>(Landroid/content/Context;)V

    .line 628778
    const-class v0, LX/3i8;

    invoke-static {v0, p0}, LX/3i8;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 628779
    const v0, 0x7f030ba3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 628780
    const v0, 0x7f0d0537

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/attachments/angora/InstantArticleIconView;

    iput-object v0, p0, LX/3i8;->e:Lcom/facebook/attachments/angora/InstantArticleIconView;

    .line 628781
    const v0, 0x7f0d1cd6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/3i8;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 628782
    const v0, 0x7f0d1cda

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/3i8;->f:Landroid/widget/TextView;

    .line 628783
    const v0, 0x7f0d1cdb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/3i8;->g:Landroid/widget/TextView;

    .line 628784
    const v0, 0x7f0d1cd5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/multishare/MultiShareInlineVideoView;

    iput-object v0, p0, LX/3i8;->b:Lcom/facebook/feedplugins/multishare/MultiShareInlineVideoView;

    .line 628785
    iget-object v0, p0, LX/3i8;->b:Lcom/facebook/feedplugins/multishare/MultiShareInlineVideoView;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->getCoverImage()Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-result-object v0

    iput-object v0, p0, LX/3i8;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 628786
    const v0, 0x7f0d1cd9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/3i8;->i:Landroid/view/View;

    .line 628787
    const v0, 0x7f0d1cd7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/3i8;->j:Landroid/view/View;

    .line 628788
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/3i8;

    invoke-static {p0}, LX/3hW;->a(LX/0QB;)LX/3hW;

    move-result-object p0

    check-cast p0, LX/3hW;

    iput-object p0, p1, LX/3i8;->c:LX/3hW;

    return-void
.end method

.method private getImageDurationMs()I
    .locals 3

    .prologue
    .line 628789
    iget-object v0, p0, LX/3i8;->c:LX/3hW;

    .line 628790
    iget-object v1, v0, LX/3hW;->b:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 628791
    iget-object v1, v0, LX/3hW;->a:LX/0ad;

    sget v2, LX/3hX;->i:I

    const/16 p0, 0x5dc

    invoke-interface {v1, v2, p0}, LX/0ad;->a(II)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, LX/3hW;->b:Ljava/lang/Integer;

    .line 628792
    :cond_0
    iget-object v1, v0, LX/3hW;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    move v0, v1

    .line 628793
    return v0
.end method

.method private getTransitionDurationMs()I
    .locals 3

    .prologue
    .line 628794
    iget-object v0, p0, LX/3i8;->c:LX/3hW;

    .line 628795
    iget-object v1, v0, LX/3hW;->c:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 628796
    iget-object v1, v0, LX/3hW;->a:LX/0ad;

    sget v2, LX/3hX;->j:I

    const/16 p0, 0x12c

    invoke-interface {v1, v2, p0}, LX/0ad;->a(II)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, LX/3hW;->c:Ljava/lang/Integer;

    .line 628797
    :cond_0
    iget-object v1, v0, LX/3hW;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    move v0, v1

    .line 628798
    return v0
.end method


# virtual methods
.method public final a(II)V
    .locals 2

    .prologue
    .line 628799
    iget-object v0, p0, LX/3i8;->b:Lcom/facebook/feedplugins/multishare/MultiShareInlineVideoView;

    invoke-virtual {v0}, Lcom/facebook/feedplugins/multishare/MultiShareInlineVideoView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 628800
    if-nez v0, :cond_0

    .line 628801
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v0, p1, p2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 628802
    :cond_0
    iput p2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 628803
    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 628804
    iget-object v1, p0, LX/3i8;->b:Lcom/facebook/feedplugins/multishare/MultiShareInlineVideoView;

    invoke-virtual {v1, v0}, Lcom/facebook/feedplugins/multishare/MultiShareInlineVideoView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 628805
    iget-object v0, p0, LX/3i8;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-eqz v0, :cond_1

    .line 628806
    iget-object v0, p0, LX/3i8;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 628807
    iput p2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 628808
    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 628809
    iget-object v1, p0, LX/3i8;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 628810
    :cond_1
    return-void
.end method

.method public final a(LX/04g;I)V
    .locals 1

    .prologue
    .line 628811
    iget-object v0, p0, LX/3i8;->b:Lcom/facebook/feedplugins/multishare/MultiShareInlineVideoView;

    invoke-virtual {v0, p2, p1}, Lcom/facebook/video/player/RichVideoPlayer;->a(ILX/04g;)V

    .line 628812
    iget-object v0, p0, LX/3i8;->b:Lcom/facebook/feedplugins/multishare/MultiShareInlineVideoView;

    invoke-virtual {v0, p1}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;)V

    .line 628813
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 628833
    iget-boolean v0, p0, LX/3i8;->d:Z

    return v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 628814
    iget-object v0, p0, LX/3i8;->p:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 628815
    iget-object v0, p0, LX/3i8;->p:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 628816
    iget-object v0, p0, LX/3i8;->p:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->purge()I

    .line 628817
    const/4 v0, 0x0

    iput-object v0, p0, LX/3i8;->p:Ljava/util/Timer;

    .line 628818
    :cond_0
    return-void
.end method

.method public final b(LX/04g;)V
    .locals 1

    .prologue
    .line 628830
    iget-object v0, p0, LX/3i8;->b:Lcom/facebook/feedplugins/multishare/MultiShareInlineVideoView;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 628831
    iget-object v0, p0, LX/3i8;->b:Lcom/facebook/feedplugins/multishare/MultiShareInlineVideoView;

    invoke-virtual {v0, p1}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/04g;)V

    .line 628832
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 6

    .prologue
    .line 628824
    iget-object v0, p0, LX/3i8;->p:Ljava/util/Timer;

    if-nez v0, :cond_0

    .line 628825
    new-instance v2, Lcom/facebook/feedplugins/multishare/ui/MultiShareProductItemView$1;

    invoke-direct {v2, p0}, Lcom/facebook/feedplugins/multishare/ui/MultiShareProductItemView$1;-><init>(LX/3i8;)V

    .line 628826
    new-instance v3, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v3, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 628827
    new-instance v0, Ljava/util/Timer;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/Timer;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LX/3i8;->p:Ljava/util/Timer;

    .line 628828
    iget-object v0, p0, LX/3i8;->p:Ljava/util/Timer;

    new-instance v1, Lcom/facebook/feedplugins/multishare/ui/MultiShareProductItemView$2;

    invoke-direct {v1, p0, v3, v2}, Lcom/facebook/feedplugins/multishare/ui/MultiShareProductItemView$2;-><init>(LX/3i8;Landroid/os/Handler;Ljava/lang/Runnable;)V

    invoke-direct {p0}, LX/3i8;->getImageDurationMs()I

    move-result v2

    int-to-long v2, v2

    invoke-direct {p0}, LX/3i8;->getImageDurationMs()I

    move-result v4

    invoke-direct {p0}, LX/3i8;->getTransitionDurationMs()I

    move-result v5

    add-int/2addr v4, v5

    int-to-long v4, v4

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->scheduleAtFixedRate(Ljava/util/TimerTask;JJ)V

    .line 628829
    :cond_0
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 628821
    iget-object v0, p0, LX/3i8;->b:Lcom/facebook/feedplugins/multishare/MultiShareInlineVideoView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/feedplugins/multishare/MultiShareInlineVideoView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 628822
    iget-object v0, p0, LX/3i8;->b:Lcom/facebook/feedplugins/multishare/MultiShareInlineVideoView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/feedplugins/multishare/MultiShareInlineVideoView;->setClickable(Z)V

    .line 628823
    return-void
.end method

.method public getCoverController()LX/1aZ;
    .locals 1

    .prologue
    .line 628820
    iget-object v0, p0, LX/3i8;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3i8;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;
    .locals 1

    .prologue
    .line 628819
    iget-object v0, p0, LX/3i8;->b:Lcom/facebook/feedplugins/multishare/MultiShareInlineVideoView;

    return-object v0
.end method

.method public getSeekPosition()I
    .locals 1

    .prologue
    .line 628776
    iget-object v0, p0, LX/3i8;->b:Lcom/facebook/feedplugins/multishare/MultiShareInlineVideoView;

    invoke-virtual {v0}, Lcom/facebook/feedplugins/multishare/MultiShareInlineVideoView;->getSeekPosition()I

    move-result v0

    return v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x5d02964c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 628717
    invoke-super {p0}, LX/3i9;->onAttachedToWindow()V

    .line 628718
    const/4 v1, 0x1

    .line 628719
    iput-boolean v1, p0, LX/3i8;->d:Z

    .line 628720
    const/16 v1, 0x2d

    const v2, 0x151efaa1

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x4212b314

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 628721
    invoke-super {p0}, LX/3i9;->onDetachedFromWindow()V

    .line 628722
    const/4 v1, 0x0

    .line 628723
    iput-boolean v1, p0, LX/3i8;->d:Z

    .line 628724
    const/16 v1, 0x2d

    const v2, 0xc58487f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setCenterCrop(Z)V
    .locals 2

    .prologue
    .line 628725
    new-instance v0, LX/1Uo;

    invoke-virtual {p0}, LX/3i8;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    .line 628726
    if-eqz p1, :cond_0

    .line 628727
    sget-object v1, LX/1Up;->g:LX/1Up;

    invoke-virtual {v0, v1}, LX/1Uo;->e(LX/1Up;)LX/1Uo;

    .line 628728
    :goto_0
    iget-object v1, p0, LX/3i8;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 628729
    return-void

    .line 628730
    :cond_0
    sget-object v1, LX/1Up;->c:LX/1Up;

    invoke-virtual {v0, v1}, LX/1Uo;->e(LX/1Up;)LX/1Uo;

    goto :goto_0
.end method

.method public setCoverController(LX/1aZ;)V
    .locals 1

    .prologue
    .line 628731
    iget-object v0, p0, LX/3i8;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-eqz v0, :cond_0

    .line 628732
    iget-object v0, p0, LX/3i8;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 628733
    :cond_0
    return-void
.end method

.method public setDescriptionTextVisibility(I)V
    .locals 1

    .prologue
    .line 628734
    iget-object v0, p0, LX/3i8;->g:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 628735
    return-void
.end method

.method public setFlashIconVisibility(Z)V
    .locals 2

    .prologue
    .line 628736
    iget-object v1, p0, LX/3i8;->e:Lcom/facebook/attachments/angora/InstantArticleIconView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/attachments/angora/InstantArticleIconView;->setVisibility(I)V

    .line 628737
    return-void

    .line 628738
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setInlineVideoViewVisibility(Z)V
    .locals 2

    .prologue
    .line 628739
    iget-object v1, p0, LX/3i8;->b:Lcom/facebook/feedplugins/multishare/MultiShareInlineVideoView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/feedplugins/multishare/MultiShareInlineVideoView;->setVisibility(I)V

    .line 628740
    return-void

    .line 628741
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setItemFooterViewVisibility(Z)V
    .locals 2

    .prologue
    .line 628742
    iget-object v1, p0, LX/3i8;->i:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 628743
    return-void

    .line 628744
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setItemImageViewVisibility(Z)V
    .locals 2

    .prologue
    .line 628745
    iget-object v1, p0, LX/3i8;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 628746
    return-void

    .line 628747
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setOriginalPlayReason(LX/04g;)V
    .locals 1

    .prologue
    .line 628748
    iget-object v0, p0, LX/3i8;->b:Lcom/facebook/feedplugins/multishare/MultiShareInlineVideoView;

    invoke-virtual {v0, p1}, Lcom/facebook/video/player/RichVideoPlayer;->setOriginalPlayReason(LX/04g;)V

    .line 628749
    return-void
.end method

.method public setupSlideshow(LX/0Px;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 628750
    iput-object p1, p0, LX/3i8;->n:LX/0Px;

    .line 628751
    new-instance v0, LX/1Uo;

    invoke-virtual {p0}, LX/3i8;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    .line 628752
    new-instance v1, LX/4Ac;

    invoke-direct {v1}, LX/4Ac;-><init>()V

    iput-object v1, p0, LX/3i8;->k:LX/4Ac;

    .line 628753
    iget-object v1, p0, LX/3i8;->k:LX/4Ac;

    .line 628754
    invoke-static {v0}, LX/1Uo;->v(LX/1Uo;)V

    .line 628755
    move-object v2, v0

    .line 628756
    sget-object v3, LX/1Up;->c:LX/1Up;

    invoke-virtual {v2, v3}, LX/1Uo;->e(LX/1Up;)LX/1Uo;

    move-result-object v2

    invoke-virtual {v2}, LX/1Uo;->u()LX/1af;

    move-result-object v2

    invoke-virtual {p0}, LX/3i8;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v2, v3}, LX/1aX;->a(LX/1aY;Landroid/content/Context;)LX/1aX;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/4Ac;->a(LX/1aX;)V

    .line 628757
    iget-object v1, p0, LX/3i8;->k:LX/4Ac;

    .line 628758
    invoke-static {v0}, LX/1Uo;->v(LX/1Uo;)V

    .line 628759
    move-object v0, v0

    .line 628760
    sget-object v2, LX/1Up;->c:LX/1Up;

    invoke-virtual {v0, v2}, LX/1Uo;->e(LX/1Up;)LX/1Uo;

    move-result-object v0

    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    invoke-virtual {p0}, LX/3i8;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v0, v2}, LX/1aX;->a(LX/1aY;Landroid/content/Context;)LX/1aX;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/4Ac;->a(LX/1aX;)V

    .line 628761
    new-instance v0, LX/1ap;

    const/4 v1, 0x2

    new-array v1, v1, [Landroid/graphics/drawable/Drawable;

    iget-object v2, p0, LX/3i8;->k:LX/4Ac;

    invoke-virtual {v2, v4}, LX/4Ac;->b(I)LX/1aX;

    move-result-object v2

    invoke-virtual {v2}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    aput-object v2, v1, v4

    iget-object v2, p0, LX/3i8;->k:LX/4Ac;

    invoke-virtual {v2, v5}, LX/4Ac;->b(I)LX/1aX;

    move-result-object v2

    invoke-virtual {v2}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-direct {v0, v1}, LX/1ap;-><init>([Landroid/graphics/drawable/Drawable;)V

    iput-object v0, p0, LX/3i8;->o:LX/1ap;

    .line 628762
    iget-object v0, p0, LX/3i8;->o:LX/1ap;

    invoke-direct {p0}, LX/3i8;->getTransitionDurationMs()I

    move-result v1

    invoke-virtual {v0, v1}, LX/1ap;->c(I)V

    .line 628763
    iget-object v0, p0, LX/3i8;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v1, p0, LX/3i8;->o:LX/1ap;

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 628764
    invoke-virtual {p0}, LX/3i8;->b()V

    .line 628765
    const/4 v0, 0x0

    .line 628766
    iput v0, p0, LX/3i8;->m:I

    .line 628767
    iput v0, p0, LX/3i8;->l:I

    .line 628768
    iget-object v0, p0, LX/3i8;->k:LX/4Ac;

    iget v1, p0, LX/3i8;->m:I

    invoke-virtual {v0, v1}, LX/4Ac;->b(I)LX/1aX;

    move-result-object v1

    iget-object v0, p0, LX/3i8;->n:LX/0Px;

    iget v2, p0, LX/3i8;->l:I

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1aZ;

    invoke-virtual {v1, v0}, LX/1aX;->a(LX/1aZ;)V

    .line 628769
    iget-object v0, p0, LX/3i8;->o:LX/1ap;

    iget v1, p0, LX/3i8;->m:I

    invoke-virtual {v0, v1}, LX/1ap;->f(I)V

    .line 628770
    iget-object v0, p0, LX/3i8;->o:LX/1ap;

    invoke-virtual {v0}, LX/1ap;->f()V

    .line 628771
    return-void
.end method

.method public final x()V
    .locals 1

    .prologue
    .line 628772
    iget-object v0, p0, LX/3i8;->b:Lcom/facebook/feedplugins/multishare/MultiShareInlineVideoView;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->x()V

    .line 628773
    return-void
.end method

.method public final y()V
    .locals 1

    .prologue
    .line 628774
    iget-object v0, p0, LX/3i8;->b:Lcom/facebook/feedplugins/multishare/MultiShareInlineVideoView;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->y()V

    .line 628775
    return-void
.end method
