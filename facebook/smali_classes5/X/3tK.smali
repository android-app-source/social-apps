.class public abstract LX/3tK;
.super Landroid/widget/BaseAdapter;
.source ""

# interfaces
.implements LX/3tJ;
.implements Landroid/widget/Filterable;


# instance fields
.field public a:Z

.field public b:Z

.field public c:Landroid/database/Cursor;

.field public d:Landroid/content/Context;

.field public e:I

.field public f:LX/3tH;

.field public g:Landroid/database/DataSetObserver;

.field public h:LX/3tL;

.field public i:Landroid/widget/FilterQueryProvider;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;I)V
    .locals 0

    .prologue
    .line 644727
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 644728
    invoke-direct {p0, p1, p2, p3}, LX/3tK;->a(Landroid/content/Context;Landroid/database/Cursor;I)V

    .line 644729
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;Z)V
    .locals 1

    .prologue
    .line 644730
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 644731
    if-eqz p3, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, p1, p2, v0}, LX/3tK;->a(Landroid/content/Context;Landroid/database/Cursor;I)V

    .line 644732
    return-void

    .line 644733
    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method private a(Landroid/content/Context;Landroid/database/Cursor;I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 644734
    and-int/lit8 v2, p3, 0x1

    if-ne v2, v0, :cond_2

    .line 644735
    or-int/lit8 p3, p3, 0x2

    .line 644736
    iput-boolean v0, p0, LX/3tK;->b:Z

    .line 644737
    :goto_0
    if-eqz p2, :cond_3

    .line 644738
    :goto_1
    iput-object p2, p0, LX/3tK;->c:Landroid/database/Cursor;

    .line 644739
    iput-boolean v0, p0, LX/3tK;->a:Z

    .line 644740
    iput-object p1, p0, LX/3tK;->d:Landroid/content/Context;

    .line 644741
    if-eqz v0, :cond_4

    const-string v2, "_id"

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    :goto_2
    iput v2, p0, LX/3tK;->e:I

    .line 644742
    and-int/lit8 v2, p3, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_5

    .line 644743
    new-instance v2, LX/3tH;

    invoke-direct {v2, p0}, LX/3tH;-><init>(LX/3tK;)V

    iput-object v2, p0, LX/3tK;->f:LX/3tH;

    .line 644744
    new-instance v2, LX/3tI;

    invoke-direct {v2, p0}, LX/3tI;-><init>(LX/3tK;)V

    iput-object v2, p0, LX/3tK;->g:Landroid/database/DataSetObserver;

    .line 644745
    :goto_3
    if-eqz v0, :cond_1

    .line 644746
    iget-object v0, p0, LX/3tK;->f:LX/3tH;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3tK;->f:LX/3tH;

    invoke-interface {p2, v0}, Landroid/database/Cursor;->registerContentObserver(Landroid/database/ContentObserver;)V

    .line 644747
    :cond_0
    iget-object v0, p0, LX/3tK;->g:Landroid/database/DataSetObserver;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/3tK;->g:Landroid/database/DataSetObserver;

    invoke-interface {p2, v0}, Landroid/database/Cursor;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 644748
    :cond_1
    return-void

    .line 644749
    :cond_2
    iput-boolean v1, p0, LX/3tK;->b:Z

    goto :goto_0

    :cond_3
    move v0, v1

    .line 644750
    goto :goto_1

    .line 644751
    :cond_4
    const/4 v2, -0x1

    goto :goto_2

    .line 644752
    :cond_5
    iput-object v4, p0, LX/3tK;->f:LX/3tH;

    .line 644753
    iput-object v4, p0, LX/3tK;->g:Landroid/database/DataSetObserver;

    goto :goto_3
.end method


# virtual methods
.method public final a()Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 644761
    iget-object v0, p0, LX/3tK;->c:Landroid/database/Cursor;

    return-object v0
.end method

.method public a(Ljava/lang/CharSequence;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 644754
    iget-object v0, p0, LX/3tK;->i:Landroid/widget/FilterQueryProvider;

    if-eqz v0, :cond_0

    .line 644755
    iget-object v0, p0, LX/3tK;->i:Landroid/widget/FilterQueryProvider;

    invoke-interface {v0, p1}, Landroid/widget/FilterQueryProvider;->runQuery(Ljava/lang/CharSequence;)Landroid/database/Cursor;

    move-result-object v0

    .line 644756
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/3tK;->c:Landroid/database/Cursor;

    goto :goto_0
.end method

.method public abstract a(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
.end method

.method public a(Landroid/database/Cursor;)V
    .locals 1

    .prologue
    .line 644757
    invoke-virtual {p0, p1}, LX/3tK;->b(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    .line 644758
    if-eqz v0, :cond_0

    .line 644759
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 644760
    :cond_0
    return-void
.end method

.method public abstract a(Landroid/view/View;Landroid/database/Cursor;)V
.end method

.method public b(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 2

    .prologue
    .line 644710
    iget-object v0, p0, LX/3tK;->c:Landroid/database/Cursor;

    if-ne p1, v0, :cond_0

    .line 644711
    const/4 v0, 0x0

    .line 644712
    :goto_0
    return-object v0

    .line 644713
    :cond_0
    iget-object v0, p0, LX/3tK;->c:Landroid/database/Cursor;

    .line 644714
    if-eqz v0, :cond_2

    .line 644715
    iget-object v1, p0, LX/3tK;->f:LX/3tH;

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/3tK;->f:LX/3tH;

    invoke-interface {v0, v1}, Landroid/database/Cursor;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 644716
    :cond_1
    iget-object v1, p0, LX/3tK;->g:Landroid/database/DataSetObserver;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/3tK;->g:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/database/Cursor;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 644717
    :cond_2
    iput-object p1, p0, LX/3tK;->c:Landroid/database/Cursor;

    .line 644718
    if-eqz p1, :cond_5

    .line 644719
    iget-object v1, p0, LX/3tK;->f:LX/3tH;

    if-eqz v1, :cond_3

    iget-object v1, p0, LX/3tK;->f:LX/3tH;

    invoke-interface {p1, v1}, Landroid/database/Cursor;->registerContentObserver(Landroid/database/ContentObserver;)V

    .line 644720
    :cond_3
    iget-object v1, p0, LX/3tK;->g:Landroid/database/DataSetObserver;

    if-eqz v1, :cond_4

    iget-object v1, p0, LX/3tK;->g:Landroid/database/DataSetObserver;

    invoke-interface {p1, v1}, Landroid/database/Cursor;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 644721
    :cond_4
    const-string v1, "_id"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, LX/3tK;->e:I

    .line 644722
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/3tK;->a:Z

    .line 644723
    const v1, -0x7a34e4d3    # -1.9100011E-35f

    invoke-static {p0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    goto :goto_0

    .line 644724
    :cond_5
    const/4 v1, -0x1

    iput v1, p0, LX/3tK;->e:I

    .line 644725
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/3tK;->a:Z

    .line 644726
    const v1, -0x559b77f5

    invoke-static {p0, v1}, LX/08p;->b(Landroid/widget/BaseAdapter;I)V

    goto :goto_0
.end method

.method public b(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 644679
    invoke-virtual {p0, p1, p2, p3}, LX/3tK;->a(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public c(Landroid/database/Cursor;)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 644680
    if-nez p1, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 644681
    iget-boolean v0, p0, LX/3tK;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3tK;->c:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 644682
    iget-object v0, p0, LX/3tK;->c:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 644683
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 644684
    iget-boolean v0, p0, LX/3tK;->a:Z

    if-eqz v0, :cond_1

    .line 644685
    iget-object v0, p0, LX/3tK;->c:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 644686
    if-nez p2, :cond_0

    .line 644687
    iget-object v0, p0, LX/3tK;->d:Landroid/content/Context;

    iget-object v1, p0, LX/3tK;->c:Landroid/database/Cursor;

    invoke-virtual {p0, v0, v1, p3}, LX/3tK;->b(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 644688
    :cond_0
    iget-object v0, p0, LX/3tK;->c:Landroid/database/Cursor;

    invoke-virtual {p0, p2, v0}, LX/3tK;->a(Landroid/view/View;Landroid/database/Cursor;)V

    .line 644689
    :goto_0
    return-object p2

    :cond_1
    const/4 p2, 0x0

    goto :goto_0
.end method

.method public final getFilter()Landroid/widget/Filter;
    .locals 1

    .prologue
    .line 644690
    iget-object v0, p0, LX/3tK;->h:LX/3tL;

    if-nez v0, :cond_0

    .line 644691
    new-instance v0, LX/3tL;

    invoke-direct {v0, p0}, LX/3tL;-><init>(LX/3tJ;)V

    iput-object v0, p0, LX/3tK;->h:LX/3tL;

    .line 644692
    :cond_0
    iget-object v0, p0, LX/3tK;->h:LX/3tL;

    return-object v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 644693
    iget-boolean v0, p0, LX/3tK;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3tK;->c:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 644694
    iget-object v0, p0, LX/3tK;->c:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 644695
    iget-object v0, p0, LX/3tK;->c:Landroid/database/Cursor;

    .line 644696
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 3

    .prologue
    const-wide/16 v0, 0x0

    .line 644697
    iget-boolean v2, p0, LX/3tK;->a:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/3tK;->c:Landroid/database/Cursor;

    if-eqz v2, :cond_0

    .line 644698
    iget-object v2, p0, LX/3tK;->c:Landroid/database/Cursor;

    invoke-interface {v2, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 644699
    iget-object v0, p0, LX/3tK;->c:Landroid/database/Cursor;

    iget v1, p0, LX/3tK;->e:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 644700
    :cond_0
    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 644701
    iget-boolean v0, p0, LX/3tK;->a:Z

    if-nez v0, :cond_0

    .line 644702
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "this should only be called when the cursor is valid"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 644703
    :cond_0
    iget-object v0, p0, LX/3tK;->c:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 644704
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "couldn\'t move cursor to position "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 644705
    :cond_1
    if-nez p2, :cond_2

    .line 644706
    iget-object v0, p0, LX/3tK;->d:Landroid/content/Context;

    iget-object v1, p0, LX/3tK;->c:Landroid/database/Cursor;

    invoke-virtual {p0, v0, v1, p3}, LX/3tK;->a(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 644707
    :cond_2
    iget-object v0, p0, LX/3tK;->c:Landroid/database/Cursor;

    invoke-virtual {p0, p2, v0}, LX/3tK;->a(Landroid/view/View;Landroid/database/Cursor;)V

    .line 644708
    return-object p2
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 644709
    const/4 v0, 0x1

    return v0
.end method
