.class public LX/4N5;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 690644
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    .line 690645
    const/4 v9, 0x0

    .line 690646
    const/4 v8, 0x0

    .line 690647
    const/4 v7, 0x0

    .line 690648
    const/4 v6, 0x0

    .line 690649
    const/4 v5, 0x0

    .line 690650
    const/4 v4, 0x0

    .line 690651
    const/4 v3, 0x0

    .line 690652
    const/4 v2, 0x0

    .line 690653
    const/4 v1, 0x0

    .line 690654
    const/4 v0, 0x0

    .line 690655
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->START_OBJECT:LX/15z;

    if-eq v10, v11, :cond_1

    .line 690656
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 690657
    const/4 v0, 0x0

    .line 690658
    :goto_0
    return v0

    .line 690659
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 690660
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_b

    .line 690661
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 690662
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 690663
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_1

    if-eqz v10, :cond_1

    .line 690664
    const-string v11, "__type__"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_2

    const-string v11, "__typename"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 690665
    :cond_2
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->a(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 690666
    :cond_3
    const-string v11, "campaign_title"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 690667
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 690668
    :cond_4
    const-string v11, "fundraiser_for_charity_text"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 690669
    invoke-static {p0, p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 690670
    :cond_5
    const-string v11, "id"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 690671
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 690672
    :cond_6
    const-string v11, "owner"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 690673
    invoke-static {p0, p1}, LX/2bM;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 690674
    :cond_7
    const-string v11, "posted_item_privacy_scope"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 690675
    invoke-static {p0, p1}, LX/2bo;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 690676
    :cond_8
    const-string v11, "can_invite_to_campaign"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_9

    .line 690677
    const/4 v0, 0x1

    .line 690678
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v3

    goto/16 :goto_1

    .line 690679
    :cond_9
    const-string v11, "fundraiser_page_subtitle"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_a

    .line 690680
    invoke-static {p0, p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v2

    goto/16 :goto_1

    .line 690681
    :cond_a
    const-string v11, "logo_image"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 690682
    invoke-static {p0, p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v1

    goto/16 :goto_1

    .line 690683
    :cond_b
    const/16 v10, 0x9

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 690684
    const/4 v10, 0x0

    invoke-virtual {p1, v10, v9}, LX/186;->b(II)V

    .line 690685
    const/4 v9, 0x1

    invoke-virtual {p1, v9, v8}, LX/186;->b(II)V

    .line 690686
    const/4 v8, 0x2

    invoke-virtual {p1, v8, v7}, LX/186;->b(II)V

    .line 690687
    const/4 v7, 0x3

    invoke-virtual {p1, v7, v6}, LX/186;->b(II)V

    .line 690688
    const/4 v6, 0x4

    invoke-virtual {p1, v6, v5}, LX/186;->b(II)V

    .line 690689
    const/4 v5, 0x5

    invoke-virtual {p1, v5, v4}, LX/186;->b(II)V

    .line 690690
    if-eqz v0, :cond_c

    .line 690691
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v3}, LX/186;->a(IZ)V

    .line 690692
    :cond_c
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 690693
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 690694
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 690695
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 690696
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 690697
    if-eqz v0, :cond_0

    .line 690698
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 690699
    invoke-static {p0, p1, v1, p2}, LX/2bt;->a(LX/15i;IILX/0nX;)V

    .line 690700
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 690701
    if-eqz v0, :cond_1

    .line 690702
    const-string v1, "campaign_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 690703
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 690704
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 690705
    if-eqz v0, :cond_2

    .line 690706
    const-string v1, "fundraiser_for_charity_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 690707
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 690708
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 690709
    if-eqz v0, :cond_3

    .line 690710
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 690711
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 690712
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 690713
    if-eqz v0, :cond_4

    .line 690714
    const-string v1, "owner"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 690715
    invoke-static {p0, v0, p2, p3}, LX/2bM;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 690716
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 690717
    if-eqz v0, :cond_5

    .line 690718
    const-string v1, "posted_item_privacy_scope"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 690719
    invoke-static {p0, v0, p2, p3}, LX/2bo;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 690720
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 690721
    if-eqz v0, :cond_6

    .line 690722
    const-string v1, "can_invite_to_campaign"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 690723
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 690724
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 690725
    if-eqz v0, :cond_7

    .line 690726
    const-string v1, "fundraiser_page_subtitle"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 690727
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 690728
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 690729
    if-eqz v0, :cond_8

    .line 690730
    const-string v1, "logo_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 690731
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 690732
    :cond_8
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 690733
    return-void
.end method
