.class public LX/4rW;
.super LX/2Ax;
.source ""


# instance fields
.field public final t:LX/4ro;


# direct methods
.method public constructor <init>(LX/2Ax;LX/4ro;)V
    .locals 0

    .prologue
    .line 816089
    invoke-direct {p0, p1}, LX/2Ax;-><init>(LX/2Ax;)V

    .line 816090
    iput-object p2, p0, LX/4rW;->t:LX/4ro;

    .line 816091
    return-void
.end method

.method private constructor <init>(LX/4rW;LX/4ro;LX/0lb;)V
    .locals 0

    .prologue
    .line 816086
    invoke-direct {p0, p1, p3}, LX/2Ax;-><init>(LX/2Ax;LX/0lb;)V

    .line 816087
    iput-object p2, p0, LX/4rW;->t:LX/4ro;

    .line 816088
    return-void
.end method

.method private c(LX/4ro;)LX/4rW;
    .locals 4

    .prologue
    .line 816082
    iget-object v0, p0, LX/2Ax;->h:LX/0lb;

    invoke-virtual {v0}, LX/0lb;->a()Ljava/lang/String;

    move-result-object v0

    .line 816083
    invoke-virtual {p1, v0}, LX/4ro;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 816084
    iget-object v1, p0, LX/4rW;->t:LX/4ro;

    invoke-static {p1, v1}, LX/4ro;->a(LX/4ro;LX/4ro;)LX/4ro;

    move-result-object v1

    .line 816085
    new-instance v2, LX/4rW;

    new-instance v3, LX/0lb;

    invoke-direct {v3, v0}, LX/0lb;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, p0, v1, v3}, LX/4rW;-><init>(LX/4rW;LX/4ro;LX/0lb;)V

    return-object v2
.end method


# virtual methods
.method public final synthetic a(LX/4ro;)LX/2Ax;
    .locals 1

    .prologue
    .line 816081
    invoke-direct {p0, p1}, LX/4rW;->c(LX/4ro;)LX/4rW;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/2B6;Ljava/lang/Class;LX/0my;)Lcom/fasterxml/jackson/databind/JsonSerializer;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2B6;",
            "Ljava/lang/Class",
            "<*>;",
            "LX/0my;",
            ")",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 816043
    iget-object v0, p0, LX/2Ax;->r:LX/0lJ;

    if-eqz v0, :cond_0

    .line 816044
    iget-object v0, p0, LX/2Ax;->r:LX/0lJ;

    invoke-virtual {p3, v0, p2}, LX/0mz;->a(LX/0lJ;Ljava/lang/Class;)LX/0lJ;

    move-result-object v0

    .line 816045
    invoke-virtual {p3, v0, p0}, LX/0my;->a(LX/0lJ;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v1

    .line 816046
    :goto_0
    iget-object v2, p0, LX/4rW;->t:LX/4ro;

    .line 816047
    invoke-virtual {v1}, Lcom/fasterxml/jackson/databind/JsonSerializer;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v0, v1

    .line 816048
    check-cast v0, Lcom/fasterxml/jackson/databind/ser/impl/UnwrappingBeanSerializer;

    iget-object v0, v0, Lcom/fasterxml/jackson/databind/ser/impl/UnwrappingBeanSerializer;->a:LX/4ro;

    invoke-static {v2, v0}, LX/4ro;->a(LX/4ro;LX/4ro;)LX/4ro;

    move-result-object v0

    .line 816049
    :goto_1
    invoke-virtual {v1, v0}, Lcom/fasterxml/jackson/databind/JsonSerializer;->a(LX/4ro;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    .line 816050
    iget-object v1, p0, LX/2Ax;->m:LX/2B6;

    invoke-virtual {v1, p2, v0}, LX/2B6;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)LX/2B6;

    move-result-object v1

    iput-object v1, p0, LX/4rW;->m:LX/2B6;

    .line 816051
    return-object v0

    .line 816052
    :cond_0
    invoke-virtual {p3, p2, p0}, LX/0my;->a(Ljava/lang/Class;LX/2Ay;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v1

    goto :goto_0

    :cond_1
    move-object v0, v2

    goto :goto_1
.end method

.method public final a(Lcom/fasterxml/jackson/databind/JsonSerializer;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/databind/JsonSerializer",
            "<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 816053
    invoke-super {p0, p1}, LX/2Ax;->a(Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 816054
    iget-object v0, p0, LX/2Ax;->k:Lcom/fasterxml/jackson/databind/JsonSerializer;

    if-eqz v0, :cond_0

    .line 816055
    iget-object v1, p0, LX/4rW;->t:LX/4ro;

    .line 816056
    iget-object v0, p0, LX/2Ax;->k:Lcom/fasterxml/jackson/databind/JsonSerializer;

    invoke-virtual {v0}, Lcom/fasterxml/jackson/databind/JsonSerializer;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 816057
    iget-object v0, p0, LX/2Ax;->k:Lcom/fasterxml/jackson/databind/JsonSerializer;

    check-cast v0, Lcom/fasterxml/jackson/databind/ser/impl/UnwrappingBeanSerializer;

    iget-object v0, v0, Lcom/fasterxml/jackson/databind/ser/impl/UnwrappingBeanSerializer;->a:LX/4ro;

    invoke-static {v1, v0}, LX/4ro;->a(LX/4ro;LX/4ro;)LX/4ro;

    move-result-object v0

    .line 816058
    :goto_0
    iget-object v1, p0, LX/2Ax;->k:Lcom/fasterxml/jackson/databind/JsonSerializer;

    invoke-virtual {v1, v0}, Lcom/fasterxml/jackson/databind/JsonSerializer;->a(LX/4ro;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    iput-object v0, p0, LX/4rW;->k:Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 816059
    :cond_0
    return-void

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 816060
    invoke-virtual {p0, p1}, LX/2Ax;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 816061
    if-nez v1, :cond_1

    .line 816062
    :cond_0
    :goto_0
    return-void

    .line 816063
    :cond_1
    iget-object v0, p0, LX/2Ax;->k:Lcom/fasterxml/jackson/databind/JsonSerializer;

    .line 816064
    if-nez v0, :cond_2

    .line 816065
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    .line 816066
    iget-object v3, p0, LX/2Ax;->m:LX/2B6;

    .line 816067
    invoke-virtual {v3, v2}, LX/2B6;->a(Ljava/lang/Class;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    .line 816068
    if-nez v0, :cond_2

    .line 816069
    invoke-virtual {p0, v3, v2, p3}, LX/4rW;->a(LX/2B6;Ljava/lang/Class;LX/0my;)Lcom/fasterxml/jackson/databind/JsonSerializer;

    move-result-object v0

    .line 816070
    :cond_2
    iget-object v2, p0, LX/2Ax;->o:Ljava/lang/Object;

    if-eqz v2, :cond_3

    .line 816071
    sget-object v2, LX/2Ax;->a:Ljava/lang/Object;

    iget-object v3, p0, LX/2Ax;->o:Ljava/lang/Object;

    if-ne v2, v3, :cond_6

    .line 816072
    invoke-virtual {v0, v1}, Lcom/fasterxml/jackson/databind/JsonSerializer;->a(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 816073
    :cond_3
    if-ne v1, p1, :cond_4

    .line 816074
    invoke-static {v0}, LX/2Ax;->c(Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 816075
    :cond_4
    invoke-virtual {v0}, Lcom/fasterxml/jackson/databind/JsonSerializer;->c()Z

    move-result v2

    if-nez v2, :cond_5

    .line 816076
    iget-object v2, p0, LX/2Ax;->h:LX/0lb;

    invoke-virtual {p2, v2}, LX/0nX;->b(LX/0lc;)V

    .line 816077
    :cond_5
    iget-object v2, p0, LX/2Ax;->q:LX/4qz;

    if-nez v2, :cond_7

    .line 816078
    invoke-virtual {v0, v1, p2, p3}, Lcom/fasterxml/jackson/databind/JsonSerializer;->a(Ljava/lang/Object;LX/0nX;LX/0my;)V

    goto :goto_0

    .line 816079
    :cond_6
    iget-object v2, p0, LX/2Ax;->o:Ljava/lang/Object;

    invoke-virtual {v2, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0

    .line 816080
    :cond_7
    iget-object v2, p0, LX/2Ax;->q:LX/4qz;

    invoke-virtual {v0, v1, p2, p3, v2}, Lcom/fasterxml/jackson/databind/JsonSerializer;->a(Ljava/lang/Object;LX/0nX;LX/0my;LX/4qz;)V

    goto :goto_0
.end method
