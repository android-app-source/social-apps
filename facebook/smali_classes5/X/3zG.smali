.class public LX/3zG;
.super LX/3zF;
.source ""


# instance fields
.field private final e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 0

    .prologue
    .line 662332
    invoke-direct {p0, p1, p3, p4, p5}, LX/3zF;-><init>(Ljava/lang/String;Ljava/lang/String;J)V

    .line 662333
    iput-object p2, p0, LX/3zG;->e:Ljava/lang/String;

    .line 662334
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 6

    .prologue
    .line 662335
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v4

    iput-wide v4, p0, LX/3zF;->d:J

    .line 662336
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "composer_written"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/3zF;->a:Ljava/lang/String;

    .line 662337
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 662338
    move-object v0, v0

    .line 662339
    iget-object v1, p0, LX/3zG;->e:Ljava/lang/String;

    .line 662340
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->f:Ljava/lang/String;

    .line 662341
    move-object v0, v0

    .line 662342
    const-string v1, "written_time"

    iget-wide v2, p0, LX/3zF;->d:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "loaded_time"

    iget-wide v2, p0, LX/3zF;->c:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "publish_target"

    iget-object v2, p0, LX/3zF;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    return-object v0
.end method
