.class public final LX/3xq;
.super LX/3xp;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 660404
    invoke-direct {p0}, LX/3xp;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;Landroid/view/View;FFIZ)V
    .locals 7

    .prologue
    .line 660385
    if-eqz p7, :cond_1

    .line 660386
    const v0, 0x7f0d000e

    invoke-virtual {p3, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    .line 660387
    if-nez v0, :cond_1

    .line 660388
    invoke-static {p3}, LX/0vv;->x(Landroid/view/View;)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    .line 660389
    const/high16 v1, 0x3f800000    # 1.0f

    .line 660390
    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView;->getChildCount()I

    move-result v5

    .line 660391
    const/4 v3, 0x0

    .line 660392
    const/4 v2, 0x0

    move v4, v2

    :goto_0
    if-ge v4, v5, :cond_0

    .line 660393
    invoke-virtual {p2, v4}, Landroid/support/v7/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 660394
    if-eq v2, p3, :cond_2

    .line 660395
    invoke-static {v2}, LX/0vv;->x(Landroid/view/View;)F

    move-result v2

    .line 660396
    cmpl-float v6, v2, v3

    if-lez v6, :cond_2

    .line 660397
    :goto_1
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move v3, v2

    goto :goto_0

    .line 660398
    :cond_0
    move v2, v3

    .line 660399
    add-float/2addr v1, v2

    .line 660400
    invoke-static {p3, v1}, LX/0vv;->f(Landroid/view/View;F)V

    .line 660401
    const v1, 0x7f0d000e

    invoke-virtual {p3, v1, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 660402
    :cond_1
    invoke-super/range {p0 .. p7}, LX/3xp;->a(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;Landroid/view/View;FFIZ)V

    .line 660403
    return-void

    :cond_2
    move v2, v3

    goto :goto_1
.end method

.method public final a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 660379
    const v0, 0x7f0d000e

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    .line 660380
    if-eqz v0, :cond_0

    instance-of v1, v0, Ljava/lang/Float;

    if-eqz v1, :cond_0

    .line 660381
    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-static {p1, v0}, LX/0vv;->f(Landroid/view/View;F)V

    .line 660382
    :cond_0
    const v0, 0x7f0d000e

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 660383
    invoke-super {p0, p1}, LX/3xp;->a(Landroid/view/View;)V

    .line 660384
    return-void
.end method
