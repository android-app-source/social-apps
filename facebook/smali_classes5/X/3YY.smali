.class public LX/3YY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1TE;
.implements LX/1TG;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 596256
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/3YY;
    .locals 3

    .prologue
    .line 596257
    const-class v1, LX/3YY;

    monitor-enter v1

    .line 596258
    :try_start_0
    sget-object v0, LX/3YY;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 596259
    sput-object v2, LX/3YY;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 596260
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 596261
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 596262
    new-instance v0, LX/3YY;

    invoke-direct {v0}, LX/3YY;-><init>()V

    .line 596263
    move-object v0, v0

    .line 596264
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 596265
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/3YY;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 596266
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 596267
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0ja;)V
    .locals 0

    .prologue
    .line 596268
    return-void
.end method

.method public final a(LX/1KB;)V
    .locals 1

    .prologue
    .line 596269
    sget-object v0, Lcom/facebook/feedplugins/offline/rows/OfflineSuccessPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 596270
    sget-object v0, Lcom/facebook/feedplugins/offline/rows/OfflineFailedPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 596271
    sget-object v0, Lcom/facebook/feedplugins/offline/rows/OfflineFailedV2PartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 596272
    sget-object v0, Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 596273
    sget-object v0, Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition;->b:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 596274
    sget-object v0, Lcom/facebook/feedplugins/offline/rows/OfflineProgressV2PartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 596275
    sget-object v0, Lcom/facebook/feedplugins/offline/rows/OfflineRetryPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 596276
    sget-object v0, Lcom/facebook/feedplugins/offline/rows/MediaUploadProgressPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 596277
    sget-object v0, Lcom/facebook/components/feed/ComponentPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 596278
    return-void
.end method
