.class public LX/4Uf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Bq;


# instance fields
.field private final b:[LX/3Bq;

.field private final c:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public varargs constructor <init>([LX/3Bq;)V
    .locals 4

    .prologue
    .line 741632
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 741633
    iput-object p1, p0, LX/4Uf;->b:[LX/3Bq;

    .line 741634
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v1

    .line 741635
    array-length v2, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, p1, v0

    .line 741636
    invoke-interface {v3}, LX/3Bq;->a()Ljava/util/Set;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/0cA;->b(Ljava/lang/Iterable;)LX/0cA;

    .line 741637
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 741638
    :cond_0
    invoke-virtual {v1}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    iput-object v0, p0, LX/4Uf;->c:LX/0Rf;

    .line 741639
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Z)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;Z)TT;"
        }
    .end annotation

    .prologue
    .line 741640
    iget-object v1, p0, LX/4Uf;->b:[LX/3Bq;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 741641
    invoke-interface {v3, p1, p2}, LX/3Bq;->a(Ljava/lang/Object;Z)Ljava/lang/Object;

    move-result-object p1

    .line 741642
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 741643
    :cond_0
    return-object p1
.end method

.method public final a()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 741644
    iget-object v0, p0, LX/4Uf;->c:LX/0Rf;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 3

    .prologue
    .line 741645
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 741646
    const-string v0, "Composite["

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 741647
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, LX/4Uf;->b:[LX/3Bq;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 741648
    if-eqz v0, :cond_0

    .line 741649
    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 741650
    :cond_0
    iget-object v2, p0, LX/4Uf;->b:[LX/3Bq;

    aget-object v2, v2, v0

    invoke-interface {v2}, LX/3Bq;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 741651
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 741652
    :cond_1
    const-string v0, "]"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 741653
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
