.class public LX/480;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static a:LX/47a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/47a",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field public static b:LX/47a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/47a",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 672631
    new-instance v0, LX/47y;

    invoke-direct {v0}, LX/47y;-><init>()V

    sput-object v0, LX/480;->a:LX/47a;

    .line 672632
    new-instance v0, LX/47z;

    invoke-direct {v0}, LX/47z;-><init>()V

    sput-object v0, LX/480;->b:LX/47a;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 672622
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/util/List;LX/47a;LX/03R;)LX/03R;
    .locals 3
    .param p2    # LX/03R;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/03R;",
            ">;",
            "LX/47a",
            "<",
            "LX/03R;",
            ">;",
            "LX/03R;",
            ")",
            "LX/03R;"
        }
    .end annotation

    .prologue
    .line 672623
    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 672624
    sget-object v0, LX/03R;->UNSET:LX/03R;

    .line 672625
    :cond_0
    :goto_0
    return-object v0

    .line 672626
    :cond_1
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 672627
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    .line 672628
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 672629
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {p1, v0, v2}, LX/47a;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    .line 672630
    if-ne v0, p2, :cond_2

    goto :goto_0
.end method
