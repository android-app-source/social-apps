.class public final LX/3ea;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:LX/3do;

.field private b:LX/0rS;

.field public c:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/StickerInterfaces;
    .end annotation
.end field

.field private d:Z

.field private e:Z

.field private f:Z

.field private g:Z

.field private h:LX/3eb;


# direct methods
.method public constructor <init>(LX/3do;LX/0rS;)V
    .locals 1

    .prologue
    .line 620331
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 620332
    iput-object p1, p0, LX/3ea;->a:LX/3do;

    .line 620333
    iput-object p2, p0, LX/3ea;->b:LX/0rS;

    .line 620334
    sget-object v0, LX/3eb;->DO_NOT_UPDATE:LX/3eb;

    iput-object v0, p0, LX/3ea;->h:LX/3eb;

    .line 620335
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/stickers/service/FetchStickerPacksParams;
    .locals 10

    .prologue
    .line 620336
    new-instance v0, Lcom/facebook/stickers/service/FetchStickerPacksParams;

    iget-object v1, p0, LX/3ea;->a:LX/3do;

    iget-object v2, p0, LX/3ea;->b:LX/0rS;

    iget-object v3, p0, LX/3ea;->c:Ljava/lang/String;

    iget-boolean v4, p0, LX/3ea;->d:Z

    iget-boolean v5, p0, LX/3ea;->e:Z

    iget-boolean v6, p0, LX/3ea;->f:Z

    iget-boolean v7, p0, LX/3ea;->g:Z

    iget-object v8, p0, LX/3ea;->h:LX/3eb;

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/facebook/stickers/service/FetchStickerPacksParams;-><init>(LX/3do;LX/0rS;Ljava/lang/String;ZZZZLX/3eb;B)V

    return-object v0
.end method
