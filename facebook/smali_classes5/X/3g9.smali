.class public final LX/3g9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2ZE;


# instance fields
.field public final synthetic a:LX/3g7;


# direct methods
.method public constructor <init>(LX/3g7;)V
    .locals 0

    .prologue
    .line 624093
    iput-object p1, p0, LX/3g9;->a:LX/3g7;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Iterable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "LX/2Vj;",
            ">;"
        }
    .end annotation

    .prologue
    .line 624094
    iget-object v0, p0, LX/3g9;->a:LX/3g7;

    iget-object v0, v0, LX/3g7;->c:LX/3g8;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v0

    const-string v1, "isr_ping_operation"

    .line 624095
    iput-object v1, v0, LX/2Vk;->c:Ljava/lang/String;

    .line 624096
    move-object v0, v0

    .line 624097
    invoke-virtual {v0}, LX/2Vk;->a()LX/2Vj;

    move-result-object v0

    .line 624098
    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 624099
    const-string v0, "isr_ping_operation"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/appirater/api/FetchISRConfigResult;

    .line 624100
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/appirater/api/FetchISRConfigResult;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 624101
    iget-object v1, p0, LX/3g9;->a:LX/3g7;

    iget-object v1, v1, LX/3g7;->b:LX/29r;

    .line 624102
    iput-object v0, v1, LX/29r;->q:Lcom/facebook/appirater/api/FetchISRConfigResult;

    .line 624103
    iget-object p0, v1, LX/29r;->c:LX/29s;

    .line 624104
    sget-object v1, LX/BZn;->b:LX/0Tn;

    invoke-static {p0, v1, v0}, LX/29s;->a(LX/29s;LX/0Tn;Ljava/lang/Object;)V

    .line 624105
    :cond_0
    return-void
.end method
