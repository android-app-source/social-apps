.class public final enum LX/3zk;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/3zk;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/3zk;

.field public static final enum PAGES:LX/3zk;


# instance fields
.field private objectType:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 662613
    new-instance v0, LX/3zk;

    const-string v1, "PAGES"

    const-string v2, "pages"

    invoke-direct {v0, v1, v3, v2}, LX/3zk;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3zk;->PAGES:LX/3zk;

    .line 662614
    const/4 v0, 0x1

    new-array v0, v0, [LX/3zk;

    sget-object v1, LX/3zk;->PAGES:LX/3zk;

    aput-object v1, v0, v3

    sput-object v0, LX/3zk;->$VALUES:[LX/3zk;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 662610
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 662611
    iput-object p3, p0, LX/3zk;->objectType:Ljava/lang/String;

    .line 662612
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/3zk;
    .locals 1

    .prologue
    .line 662609
    const-class v0, LX/3zk;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/3zk;

    return-object v0
.end method

.method public static values()[LX/3zk;
    .locals 1

    .prologue
    .line 662608
    sget-object v0, LX/3zk;->$VALUES:[LX/3zk;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/3zk;

    return-object v0
.end method


# virtual methods
.method public final getTypeName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 662607
    iget-object v0, p0, LX/3zk;->objectType:Ljava/lang/String;

    return-object v0
.end method
