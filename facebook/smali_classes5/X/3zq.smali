.class public LX/3zq;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/3zq;


# instance fields
.field private final a:LX/0Zb;

.field public final b:LX/0So;

.field public final c:Ljava/security/SecureRandom;

.field public d:J


# direct methods
.method public constructor <init>(LX/0Zb;LX/0So;Ljava/security/SecureRandom;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 662682
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 662683
    iput-object p1, p0, LX/3zq;->a:LX/0Zb;

    .line 662684
    iput-object p2, p0, LX/3zq;->b:LX/0So;

    .line 662685
    iput-object p3, p0, LX/3zq;->c:Ljava/security/SecureRandom;

    .line 662686
    return-void
.end method

.method public static a(LX/0QB;)LX/3zq;
    .locals 6

    .prologue
    .line 662687
    sget-object v0, LX/3zq;->e:LX/3zq;

    if-nez v0, :cond_1

    .line 662688
    const-class v1, LX/3zq;

    monitor-enter v1

    .line 662689
    :try_start_0
    sget-object v0, LX/3zq;->e:LX/3zq;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 662690
    if-eqz v2, :cond_0

    .line 662691
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 662692
    new-instance p0, LX/3zq;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v4

    check-cast v4, LX/0So;

    invoke-static {v0}, LX/46W;->a(LX/0QB;)LX/46W;

    move-result-object v5

    check-cast v5, Ljava/security/SecureRandom;

    invoke-direct {p0, v3, v4, v5}, LX/3zq;-><init>(LX/0Zb;LX/0So;Ljava/security/SecureRandom;)V

    .line 662693
    move-object v0, p0

    .line 662694
    sput-object v0, LX/3zq;->e:LX/3zq;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 662695
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 662696
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 662697
    :cond_1
    sget-object v0, LX/3zq;->e:LX/3zq;

    return-object v0

    .line 662698
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 662699
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/3zq;Lcom/facebook/analytics/useractions/utils/UserActionEvent;)V
    .locals 4

    .prologue
    .line 662700
    const-string v0, "user_actions_session_id"

    iget-wide v2, p0, LX/3zq;->d:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 662701
    iget-object v0, p0, LX/3zq;->a:LX/0Zb;

    invoke-interface {v0, p1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 662702
    return-void
.end method
