.class public LX/3YV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1TE;
.implements LX/1TG;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 596165
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 596166
    return-void
.end method

.method public static a(LX/0QB;)LX/3YV;
    .locals 3

    .prologue
    .line 596167
    const-class v1, LX/3YV;

    monitor-enter v1

    .line 596168
    :try_start_0
    sget-object v0, LX/3YV;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 596169
    sput-object v2, LX/3YV;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 596170
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 596171
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 596172
    new-instance v0, LX/3YV;

    invoke-direct {v0}, LX/3YV;-><init>()V

    .line 596173
    move-object v0, v0

    .line 596174
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 596175
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/3YV;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 596176
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 596177
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0ja;)V
    .locals 1

    .prologue
    .line 596178
    sget-object v0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsNearbyItemPartDefinition;->a:LX/1Cz;

    invoke-static {p1, v0}, LX/99p;->a(LX/0ja;LX/1Cz;)V

    .line 596179
    return-void
.end method

.method public final a(LX/1KB;)V
    .locals 1

    .prologue
    .line 596180
    sget-object v0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsHeaderPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 596181
    sget-object v0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsFooterPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 596182
    return-void
.end method
