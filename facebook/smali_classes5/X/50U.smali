.class public final LX/50U;
.super LX/0dW;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "LX/0dW",
        "<TE;>;"
    }
.end annotation


# instance fields
.field public final transient c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Px;Ljava/util/Comparator;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<TE;>;",
            "Ljava/util/Comparator",
            "<-TE;>;)V"
        }
    .end annotation

    .prologue
    .line 823776
    invoke-direct {p0, p2}, LX/0dW;-><init>(Ljava/util/Comparator;)V

    .line 823777
    iput-object p1, p0, LX/50U;->c:LX/0Px;

    .line 823778
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)I
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, -0x1

    .line 823818
    if-nez p1, :cond_1

    .line 823819
    :cond_0
    :goto_0
    return v0

    .line 823820
    :cond_1
    :try_start_0
    iget-object v1, p0, LX/50U;->c:LX/0Px;

    .line 823821
    iget-object v2, p0, LX/0dW;->a:Ljava/util/Comparator;

    move-object v2, v2

    .line 823822
    sget-object v3, LX/50g;->ANY_PRESENT:LX/50g;

    sget-object v4, LX/50c;->INVERTED_INSERTION_INDEX:LX/50c;

    invoke-static {v1, p1, v2, v3, v4}, LX/50m;->a(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;LX/50g;LX/50c;)I
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 823823
    if-ltz v1, :cond_0

    move v0, v1

    goto :goto_0

    .line 823824
    :catch_0
    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;Z)LX/0dW;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;Z)",
            "LX/0dW",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 823825
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2}, LX/50U;->e(Ljava/lang/Object;Z)I

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/50U;->a(II)LX/50U;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;ZLjava/lang/Object;Z)LX/0dW;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;ZTE;Z)",
            "LX/0dW",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 823826
    invoke-virtual {p0, p1, p2}, LX/50U;->b(Ljava/lang/Object;Z)LX/0dW;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, LX/0dW;->a(Ljava/lang/Object;Z)LX/0dW;

    move-result-object v0

    return-object v0
.end method

.method public final a(II)LX/50U;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "LX/50U",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 823827
    if-nez p1, :cond_0

    invoke-virtual {p0}, LX/50U;->size()I

    move-result v0

    if-ne p2, v0, :cond_0

    .line 823828
    :goto_0
    return-object p0

    .line 823829
    :cond_0
    if-ge p1, p2, :cond_1

    .line 823830
    new-instance v0, LX/50U;

    iget-object v1, p0, LX/50U;->c:LX/0Px;

    invoke-virtual {v1, p1, p2}, LX/0Px;->subList(II)LX/0Px;

    move-result-object v1

    iget-object v2, p0, LX/0dW;->a:Ljava/util/Comparator;

    invoke-direct {v0, v1, v2}, LX/50U;-><init>(LX/0Px;Ljava/util/Comparator;)V

    move-object p0, v0

    goto :goto_0

    .line 823831
    :cond_1
    iget-object v0, p0, LX/0dW;->a:Ljava/util/Comparator;

    invoke-static {v0}, LX/0dW;->a(Ljava/util/Comparator;)LX/50U;

    move-result-object p0

    goto :goto_0
.end method

.method public final b()LX/0Rc;
    .locals 1
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "NavigableSet"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rc",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 823832
    iget-object v0, p0, LX/50U;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->reverse()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Py;->iterator()LX/0Rc;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/Object;Z)LX/0dW;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;Z)",
            "LX/0dW",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 823833
    invoke-virtual {p0, p1, p2}, LX/50U;->f(Ljava/lang/Object;Z)I

    move-result v0

    invoke-virtual {p0}, LX/50U;->size()I

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/50U;->a(II)LX/50U;

    move-result-object v0

    return-object v0
.end method

.method public final c()LX/0dW;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0dW",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 823834
    iget-object v0, p0, LX/0dW;->a:Ljava/util/Comparator;

    invoke-static {v0}, LX/1sm;->a(Ljava/util/Comparator;)LX/1sm;

    move-result-object v0

    invoke-virtual {v0}, LX/1sm;->a()LX/1sm;

    move-result-object v1

    .line 823835
    invoke-virtual {p0}, LX/50U;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {v1}, LX/0dW;->a(Ljava/util/Comparator;)LX/50U;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/50U;

    iget-object v2, p0, LX/50U;->c:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->reverse()LX/0Px;

    move-result-object v2

    invoke-direct {v0, v2, v1}, LX/50U;-><init>(LX/0Px;Ljava/util/Comparator;)V

    goto :goto_0
.end method

.method public final ceiling(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)TE;"
        }
    .end annotation

    .prologue
    .line 823836
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/50U;->f(Ljava/lang/Object;Z)I

    move-result v0

    .line 823837
    invoke-virtual {p0}, LX/50U;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, LX/50U;->c:LX/0Px;

    invoke-virtual {v1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final contains(Ljava/lang/Object;)Z
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 823838
    if-eqz p1, :cond_0

    .line 823839
    :try_start_0
    iget-object v1, p0, LX/50U;->c:LX/0Px;

    .line 823840
    iget-object v2, p0, LX/0dW;->a:Ljava/util/Comparator;

    move-object v2, v2

    .line 823841
    invoke-static {v1, p1, v2}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;)I

    move-result v1

    move v1, v1
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 823842
    if-ltz v1, :cond_0

    const/4 v0, 0x1

    .line 823843
    :cond_0
    :goto_0
    return v0

    :catch_0
    goto :goto_0
.end method

.method public final containsAll(Ljava/util/Collection;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 823844
    instance-of v2, p1, LX/1M1;

    if-eqz v2, :cond_0

    .line 823845
    check-cast p1, LX/1M1;

    invoke-interface {p1}, LX/1M1;->d()Ljava/util/Set;

    move-result-object p1

    .line 823846
    :cond_0
    invoke-virtual {p0}, LX/0dW;->comparator()Ljava/util/Comparator;

    move-result-object v2

    invoke-static {v2, p1}, LX/50b;->a(Ljava/util/Comparator;Ljava/lang/Iterable;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v2

    if-gt v2, v0, :cond_3

    .line 823847
    :cond_1
    invoke-super {p0, p1}, LX/0dW;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    .line 823848
    :cond_2
    :goto_0
    return v0

    .line 823849
    :cond_3
    invoke-virtual {p0}, LX/50U;->iterator()LX/0Rc;

    move-result-object v2

    invoke-static {v2}, LX/0RZ;->i(Ljava/util/Iterator;)LX/4yu;

    move-result-object v3

    .line 823850
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 823851
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 823852
    :cond_4
    :goto_1
    :try_start_0
    invoke-interface {v3}, LX/4yu;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 823853
    invoke-interface {v3}, LX/4yu;->a()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {p0, v5, v2}, LX/0dW;->a(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v5

    .line 823854
    if-gez v5, :cond_5

    .line 823855
    invoke-interface {v3}, LX/4yu;->next()Ljava/lang/Object;

    goto :goto_1

    .line 823856
    :catch_0
    move v0, v1

    goto :goto_0

    .line 823857
    :cond_5
    if-nez v5, :cond_6

    .line 823858
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 823859
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    goto :goto_1

    .line 823860
    :cond_6
    if-lez v5, :cond_4

    move v0, v1

    .line 823861
    goto :goto_0

    .line 823862
    :catch_1
    move v0, v1

    goto :goto_0

    :cond_7
    move v0, v1

    .line 823863
    goto :goto_0
.end method

.method public final copyIntoArray([Ljava/lang/Object;I)I
    .locals 1

    .prologue
    .line 823864
    iget-object v0, p0, LX/50U;->c:LX/0Px;

    invoke-virtual {v0, p1, p2}, LX/0Px;->copyIntoArray([Ljava/lang/Object;I)I

    move-result v0

    return v0
.end method

.method public final createAsList()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 823816
    invoke-virtual {p0}, LX/50U;->size()I

    move-result v0

    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    iget-object v0, p0, LX/50U;->c:LX/0Px;

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/4yd;

    iget-object v1, p0, LX/50U;->c:LX/0Px;

    invoke-direct {v0, p0, v1}, LX/4yd;-><init>(LX/0dW;LX/0Px;)V

    goto :goto_0
.end method

.method public final synthetic descendingIterator()Ljava/util/Iterator;
    .locals 1
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "NavigableSet"
    .end annotation

    .prologue
    .line 823817
    invoke-virtual {p0}, LX/50U;->b()LX/0Rc;

    move-result-object v0

    return-object v0
.end method

.method public final e(Ljava/lang/Object;Z)I
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;Z)I"
        }
    .end annotation

    .prologue
    .line 823815
    iget-object v1, p0, LX/50U;->c:LX/0Px;

    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0}, LX/0dW;->comparator()Ljava/util/Comparator;

    move-result-object v3

    if-eqz p2, :cond_0

    sget-object v0, LX/50g;->FIRST_AFTER:LX/50g;

    :goto_0
    sget-object v4, LX/50c;->NEXT_HIGHER:LX/50c;

    invoke-static {v1, v2, v3, v0, v4}, LX/50m;->a(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;LX/50g;LX/50c;)I

    move-result v0

    return v0

    :cond_0
    sget-object v0, LX/50g;->FIRST_PRESENT:LX/50g;

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 823796
    if-ne p1, p0, :cond_1

    .line 823797
    :cond_0
    :goto_0
    return v0

    .line 823798
    :cond_1
    instance-of v2, p1, Ljava/util/Set;

    if-nez v2, :cond_2

    move v0, v1

    .line 823799
    goto :goto_0

    .line 823800
    :cond_2
    check-cast p1, Ljava/util/Set;

    .line 823801
    invoke-virtual {p0}, LX/50U;->size()I

    move-result v2

    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v3

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 823802
    goto :goto_0

    .line 823803
    :cond_3
    invoke-virtual {p0}, LX/50U;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 823804
    iget-object v2, p0, LX/0dW;->a:Ljava/util/Comparator;

    invoke-static {v2, p1}, LX/50b;->a(Ljava/util/Comparator;Ljava/lang/Iterable;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 823805
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 823806
    :try_start_0
    invoke-virtual {p0}, LX/50U;->iterator()LX/0Rc;

    move-result-object v3

    .line 823807
    :cond_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 823808
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 823809
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    .line 823810
    if-eqz v5, :cond_5

    invoke-virtual {p0, v4, v5}, LX/0dW;->a(Ljava/lang/Object;Ljava/lang/Object;)I
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v4

    if-eqz v4, :cond_4

    :cond_5
    move v0, v1

    .line 823811
    goto :goto_0

    .line 823812
    :catch_0
    move v0, v1

    goto :goto_0

    .line 823813
    :catch_1
    move v0, v1

    goto :goto_0

    .line 823814
    :cond_6
    invoke-virtual {p0, p1}, LX/50U;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    goto :goto_0
.end method

.method public final f(Ljava/lang/Object;Z)I
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;Z)I"
        }
    .end annotation

    .prologue
    .line 823795
    iget-object v1, p0, LX/50U;->c:LX/0Px;

    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0}, LX/0dW;->comparator()Ljava/util/Comparator;

    move-result-object v3

    if-eqz p2, :cond_0

    sget-object v0, LX/50g;->FIRST_PRESENT:LX/50g;

    :goto_0
    sget-object v4, LX/50c;->NEXT_HIGHER:LX/50c;

    invoke-static {v1, v2, v3, v0, v4}, LX/50m;->a(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;LX/50g;LX/50c;)I

    move-result v0

    return v0

    :cond_0
    sget-object v0, LX/50g;->FIRST_AFTER:LX/50g;

    goto :goto_0
.end method

.method public final first()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 823792
    invoke-virtual {p0}, LX/50U;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 823793
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 823794
    :cond_0
    iget-object v0, p0, LX/50U;->c:LX/0Px;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final floor(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)TE;"
        }
    .end annotation

    .prologue
    .line 823790
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/50U;->e(Ljava/lang/Object;Z)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 823791
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, LX/50U;->c:LX/0Px;

    invoke-virtual {v1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final higher(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)TE;"
        }
    .end annotation

    .prologue
    .line 823788
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/50U;->f(Ljava/lang/Object;Z)I

    move-result v0

    .line 823789
    invoke-virtual {p0}, LX/50U;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, LX/50U;->c:LX/0Px;

    invoke-virtual {v1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final isPartialView()Z
    .locals 1

    .prologue
    .line 823787
    iget-object v0, p0, LX/50U;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Py;->isPartialView()Z

    move-result v0

    return v0
.end method

.method public final iterator()LX/0Rc;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rc",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 823786
    iget-object v0, p0, LX/50U;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Py;->iterator()LX/0Rc;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 823785
    invoke-virtual {p0}, LX/50U;->iterator()LX/0Rc;

    move-result-object v0

    return-object v0
.end method

.method public final last()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 823782
    invoke-virtual {p0}, LX/50U;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 823783
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 823784
    :cond_0
    iget-object v0, p0, LX/50U;->c:LX/0Px;

    invoke-virtual {p0}, LX/50U;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final lower(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)TE;"
        }
    .end annotation

    .prologue
    .line 823780
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/50U;->e(Ljava/lang/Object;Z)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 823781
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, LX/50U;->c:LX/0Px;

    invoke-virtual {v1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 823779
    iget-object v0, p0, LX/50U;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method
