.class public LX/3ku;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0i1;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 633388
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 633389
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 633387
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/10S;
    .locals 1

    .prologue
    .line 633382
    sget-object v0, LX/10S;->ELIGIBLE:LX/10S;

    return-object v0
.end method

.method public final a(J)V
    .locals 0

    .prologue
    .line 633386
    return-void
.end method

.method public final a(Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 633385
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 633384
    const-string v0, "4058"

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 633383
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->TIMELINE_INTRO_CARD_SUGGESTED_BIO:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
