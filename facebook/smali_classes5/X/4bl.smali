.class public LX/4bl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lorg/apache/http/HttpRequestInterceptor;
.implements Lorg/apache/http/HttpResponseInterceptor;


# instance fields
.field private a:LX/1Gl;


# direct methods
.method public constructor <init>(LX/1Gl;)V
    .locals 1

    .prologue
    .line 794101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 794102
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Gl;

    iput-object v0, p0, LX/4bl;->a:LX/1Gl;

    .line 794103
    return-void
.end method


# virtual methods
.method public final process(Lorg/apache/http/HttpRequest;Lorg/apache/http/protocol/HttpContext;)V
    .locals 8

    .prologue
    .line 794075
    invoke-static {p2}, LX/4bz;->a(Lorg/apache/http/protocol/HttpContext;)LX/1iW;

    move-result-object v0

    .line 794076
    invoke-static {p1}, LX/1Gl;->a(Lorg/apache/http/HttpMessage;)J

    move-result-wide v4

    .line 794077
    invoke-interface {p1}, Lorg/apache/http/HttpRequest;->getRequestLine()Lorg/apache/http/RequestLine;

    move-result-object v6

    .line 794078
    if-eqz v6, :cond_0

    .line 794079
    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    int-to-long v6, v6

    add-long/2addr v4, v6

    .line 794080
    :cond_0
    move-wide v2, v4

    .line 794081
    iget-object v1, v0, LX/1iW;->requestHeaderBytes:LX/1iX;

    .line 794082
    iput-wide v2, v1, LX/1iX;->a:J

    .line 794083
    instance-of v1, p1, Lorg/apache/http/HttpEntityEnclosingRequest;

    if-eqz v1, :cond_1

    .line 794084
    check-cast p1, Lorg/apache/http/HttpEntityEnclosingRequest;

    .line 794085
    invoke-interface {p1}, Lorg/apache/http/HttpEntityEnclosingRequest;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    .line 794086
    if-eqz v1, :cond_1

    .line 794087
    new-instance v2, LX/4bj;

    invoke-direct {v2, v1, v0}, LX/4bj;-><init>(Lorg/apache/http/HttpEntity;LX/1iW;)V

    invoke-interface {p1, v2}, Lorg/apache/http/HttpEntityEnclosingRequest;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 794088
    :cond_1
    return-void
.end method

.method public final process(Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;)V
    .locals 8

    .prologue
    .line 794089
    invoke-static {p2}, LX/4bz;->a(Lorg/apache/http/protocol/HttpContext;)LX/1iW;

    move-result-object v0

    .line 794090
    invoke-static {p1}, LX/1Gl;->a(Lorg/apache/http/HttpMessage;)J

    move-result-wide v4

    .line 794091
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v6

    .line 794092
    if-eqz v6, :cond_0

    .line 794093
    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    int-to-long v6, v6

    add-long/2addr v4, v6

    .line 794094
    :cond_0
    move-wide v2, v4

    .line 794095
    iget-object v1, v0, LX/1iW;->responseHeaderBytes:LX/1iX;

    .line 794096
    iput-wide v2, v1, LX/1iX;->a:J

    .line 794097
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    .line 794098
    if-eqz v1, :cond_1

    .line 794099
    new-instance v2, LX/4bk;

    invoke-direct {v2, v1, v0}, LX/4bk;-><init>(Lorg/apache/http/HttpEntity;LX/1iW;)V

    invoke-interface {p1, v2}, Lorg/apache/http/HttpResponse;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 794100
    :cond_1
    return-void
.end method
