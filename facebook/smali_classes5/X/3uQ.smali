.class public LX/3uQ;
.super Landroid/graphics/drawable/Drawable;
.source ""

# interfaces
.implements Landroid/graphics/drawable/Drawable$Callback;


# instance fields
.field public a:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .prologue
    .line 648458
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 648459
    iget-object v0, p0, LX/3uQ;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 648460
    iget-object v0, p0, LX/3uQ;->a:Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 648461
    :cond_0
    iput-object p1, p0, LX/3uQ;->a:Landroid/graphics/drawable/Drawable;

    .line 648462
    if-eqz p1, :cond_1

    .line 648463
    invoke-virtual {p1, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 648464
    :cond_1
    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 648447
    iget-object v0, p0, LX/3uQ;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 648448
    return-void
.end method

.method public final getChangingConfigurations()I
    .locals 1

    .prologue
    .line 648449
    iget-object v0, p0, LX/3uQ;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getChangingConfigurations()I

    move-result v0

    return v0
.end method

.method public final getCurrent()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 648450
    iget-object v0, p0, LX/3uQ;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getCurrent()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public final getIntrinsicHeight()I
    .locals 1

    .prologue
    .line 648451
    iget-object v0, p0, LX/3uQ;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    return v0
.end method

.method public final getIntrinsicWidth()I
    .locals 1

    .prologue
    .line 648452
    iget-object v0, p0, LX/3uQ;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    return v0
.end method

.method public final getMinimumHeight()I
    .locals 1

    .prologue
    .line 648453
    iget-object v0, p0, LX/3uQ;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getMinimumHeight()I

    move-result v0

    return v0
.end method

.method public final getMinimumWidth()I
    .locals 1

    .prologue
    .line 648454
    iget-object v0, p0, LX/3uQ;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getMinimumWidth()I

    move-result v0

    return v0
.end method

.method public final getOpacity()I
    .locals 1

    .prologue
    .line 648444
    iget-object v0, p0, LX/3uQ;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getOpacity()I

    move-result v0

    return v0
.end method

.method public final getPadding(Landroid/graphics/Rect;)Z
    .locals 1

    .prologue
    .line 648455
    iget-object v0, p0, LX/3uQ;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    move-result v0

    return v0
.end method

.method public final getState()[I
    .locals 1

    .prologue
    .line 648456
    iget-object v0, p0, LX/3uQ;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getState()[I

    move-result-object v0

    return-object v0
.end method

.method public final getTransparentRegion()Landroid/graphics/Region;
    .locals 1

    .prologue
    .line 648457
    iget-object v0, p0, LX/3uQ;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getTransparentRegion()Landroid/graphics/Region;

    move-result-object v0

    return-object v0
.end method

.method public final invalidateDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 648442
    invoke-virtual {p0}, LX/3uQ;->invalidateSelf()V

    .line 648443
    return-void
.end method

.method public final isAutoMirrored()Z
    .locals 1

    .prologue
    .line 648465
    iget-object v0, p0, LX/3uQ;->a:Landroid/graphics/drawable/Drawable;

    .line 648466
    sget-object p0, LX/1d5;->a:LX/2yv;

    invoke-interface {p0, v0}, LX/2yv;->b(Landroid/graphics/drawable/Drawable;)Z

    move-result p0

    move v0, p0

    .line 648467
    return v0
.end method

.method public final isStateful()Z
    .locals 1

    .prologue
    .line 648468
    iget-object v0, p0, LX/3uQ;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v0

    return v0
.end method

.method public final jumpToCurrentState()V
    .locals 1

    .prologue
    .line 648469
    iget-object v0, p0, LX/3uQ;->a:Landroid/graphics/drawable/Drawable;

    invoke-static {v0}, LX/1d5;->a(Landroid/graphics/drawable/Drawable;)V

    .line 648470
    return-void
.end method

.method public final onBoundsChange(Landroid/graphics/Rect;)V
    .locals 1

    .prologue
    .line 648445
    iget-object v0, p0, LX/3uQ;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 648446
    return-void
.end method

.method public final onLevelChange(I)Z
    .locals 1

    .prologue
    .line 648413
    iget-object v0, p0, LX/3uQ;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setLevel(I)Z

    move-result v0

    return v0
.end method

.method public final scheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;J)V
    .locals 1

    .prologue
    .line 648414
    invoke-virtual {p0, p2, p3, p4}, LX/3uQ;->scheduleSelf(Ljava/lang/Runnable;J)V

    .line 648415
    return-void
.end method

.method public final setAlpha(I)V
    .locals 1

    .prologue
    .line 648416
    iget-object v0, p0, LX/3uQ;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 648417
    return-void
.end method

.method public final setAutoMirrored(Z)V
    .locals 1

    .prologue
    .line 648418
    iget-object v0, p0, LX/3uQ;->a:Landroid/graphics/drawable/Drawable;

    invoke-static {v0, p1}, LX/1d5;->a(Landroid/graphics/drawable/Drawable;Z)V

    .line 648419
    return-void
.end method

.method public final setChangingConfigurations(I)V
    .locals 1

    .prologue
    .line 648420
    iget-object v0, p0, LX/3uQ;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setChangingConfigurations(I)V

    .line 648421
    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1

    .prologue
    .line 648422
    iget-object v0, p0, LX/3uQ;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 648423
    return-void
.end method

.method public final setDither(Z)V
    .locals 1

    .prologue
    .line 648424
    iget-object v0, p0, LX/3uQ;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setDither(Z)V

    .line 648425
    return-void
.end method

.method public final setFilterBitmap(Z)V
    .locals 1

    .prologue
    .line 648426
    iget-object v0, p0, LX/3uQ;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setFilterBitmap(Z)V

    .line 648427
    return-void
.end method

.method public setHotspot(FF)V
    .locals 1

    .prologue
    .line 648428
    iget-object v0, p0, LX/3uQ;->a:Landroid/graphics/drawable/Drawable;

    invoke-static {v0, p1, p2}, LX/1d5;->a(Landroid/graphics/drawable/Drawable;FF)V

    .line 648429
    return-void
.end method

.method public setHotspotBounds(IIII)V
    .locals 1

    .prologue
    .line 648430
    iget-object v0, p0, LX/3uQ;->a:Landroid/graphics/drawable/Drawable;

    invoke-static {v0, p1, p2, p3, p4}, LX/1d5;->a(Landroid/graphics/drawable/Drawable;IIII)V

    .line 648431
    return-void
.end method

.method public setState([I)Z
    .locals 1

    .prologue
    .line 648432
    iget-object v0, p0, LX/3uQ;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    move-result v0

    return v0
.end method

.method public final setTint(I)V
    .locals 1

    .prologue
    .line 648433
    iget-object v0, p0, LX/3uQ;->a:Landroid/graphics/drawable/Drawable;

    invoke-static {v0, p1}, LX/1d5;->a(Landroid/graphics/drawable/Drawable;I)V

    .line 648434
    return-void
.end method

.method public final setTintList(Landroid/content/res/ColorStateList;)V
    .locals 1

    .prologue
    .line 648435
    iget-object v0, p0, LX/3uQ;->a:Landroid/graphics/drawable/Drawable;

    invoke-static {v0, p1}, LX/1d5;->a(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V

    .line 648436
    return-void
.end method

.method public final setTintMode(Landroid/graphics/PorterDuff$Mode;)V
    .locals 1

    .prologue
    .line 648437
    iget-object v0, p0, LX/3uQ;->a:Landroid/graphics/drawable/Drawable;

    invoke-static {v0, p1}, LX/1d5;->a(Landroid/graphics/drawable/Drawable;Landroid/graphics/PorterDuff$Mode;)V

    .line 648438
    return-void
.end method

.method public setVisible(ZZ)Z
    .locals 1

    .prologue
    .line 648439
    invoke-super {p0, p1, p2}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/3uQ;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final unscheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 648440
    invoke-virtual {p0, p2}, LX/3uQ;->unscheduleSelf(Ljava/lang/Runnable;)V

    .line 648441
    return-void
.end method
