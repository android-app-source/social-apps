.class public LX/3qa;
.super LX/25A;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/25A",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field public final f:LX/2s6;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0k9",
            "<",
            "Landroid/database/Cursor;",
            ">.Force",
            "LoadContentObserver;"
        }
    .end annotation
.end field

.field public g:Landroid/net/Uri;

.field public h:[Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:[Ljava/lang/String;

.field public k:Ljava/lang/String;

.field public l:Landroid/database/Cursor;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 642324
    invoke-direct {p0, p1}, LX/25A;-><init>(Landroid/content/Context;)V

    .line 642325
    new-instance v0, LX/2s6;

    invoke-direct {v0, p0}, LX/2s6;-><init>(LX/0k9;)V

    iput-object v0, p0, LX/3qa;->f:LX/2s6;

    .line 642326
    iput-object p2, p0, LX/3qa;->g:Landroid/net/Uri;

    .line 642327
    iput-object p3, p0, LX/3qa;->h:[Ljava/lang/String;

    .line 642328
    iput-object p4, p0, LX/3qa;->i:Ljava/lang/String;

    .line 642329
    iput-object p5, p0, LX/3qa;->j:[Ljava/lang/String;

    .line 642330
    iput-object p6, p0, LX/3qa;->k:Ljava/lang/String;

    .line 642331
    return-void
.end method

.method private a(Landroid/database/Cursor;)V
    .locals 2

    .prologue
    .line 642283
    iget-boolean v0, p0, LX/0k9;->r:Z

    move v0, v0

    .line 642284
    if-eqz v0, :cond_1

    .line 642285
    if-eqz p1, :cond_0

    .line 642286
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 642287
    :cond_0
    :goto_0
    return-void

    .line 642288
    :cond_1
    iget-object v0, p0, LX/3qa;->l:Landroid/database/Cursor;

    .line 642289
    iput-object p1, p0, LX/3qa;->l:Landroid/database/Cursor;

    .line 642290
    iget-boolean v1, p0, LX/0k9;->p:Z

    move v1, v1

    .line 642291
    if-eqz v1, :cond_2

    .line 642292
    invoke-super {p0, p1}, LX/25A;->b(Ljava/lang/Object;)V

    .line 642293
    :cond_2
    if-eqz v0, :cond_0

    if-eq v0, p1, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 642294
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 642320
    check-cast p1, Landroid/database/Cursor;

    .line 642321
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 642322
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 642323
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 642309
    invoke-super {p0, p1, p2, p3, p4}, LX/25A;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 642310
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mUri="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, LX/3qa;->g:Landroid/net/Uri;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 642311
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mProjection="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 642312
    iget-object v0, p0, LX/3qa;->h:[Ljava/lang/String;

    invoke-static {v0}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 642313
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mSelection="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, LX/3qa;->i:Ljava/lang/String;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 642314
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mSelectionArgs="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 642315
    iget-object v0, p0, LX/3qa;->j:[Ljava/lang/String;

    invoke-static {v0}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 642316
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mSortOrder="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, LX/3qa;->k:Ljava/lang/String;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 642317
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mCursor="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, LX/3qa;->l:Landroid/database/Cursor;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 642318
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mContentChanged="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, LX/0k9;->s:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 642319
    return-void
.end method

.method public final synthetic b(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 642308
    check-cast p1, Landroid/database/Cursor;

    invoke-direct {p0, p1}, LX/3qa;->a(Landroid/database/Cursor;)V

    return-void
.end method

.method public synthetic d()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 642332
    invoke-virtual {p0}, LX/3qa;->f()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public f()Landroid/database/Cursor;
    .locals 6

    .prologue
    .line 642302
    iget-object v0, p0, LX/0k9;->o:Landroid/content/Context;

    move-object v0, v0

    .line 642303
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, LX/3qa;->g:Landroid/net/Uri;

    iget-object v2, p0, LX/3qa;->h:[Ljava/lang/String;

    iget-object v3, p0, LX/3qa;->i:Ljava/lang/String;

    iget-object v4, p0, LX/3qa;->j:[Ljava/lang/String;

    iget-object v5, p0, LX/3qa;->k:Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 642304
    if-eqz v0, :cond_0

    .line 642305
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    .line 642306
    iget-object v1, p0, LX/3qa;->f:LX/2s6;

    invoke-interface {v0, v1}, Landroid/database/Cursor;->registerContentObserver(Landroid/database/ContentObserver;)V

    .line 642307
    :cond_0
    return-object v0
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 642297
    iget-object v0, p0, LX/3qa;->l:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 642298
    iget-object v0, p0, LX/3qa;->l:Landroid/database/Cursor;

    invoke-direct {p0, v0}, LX/3qa;->a(Landroid/database/Cursor;)V

    .line 642299
    :cond_0
    invoke-virtual {p0}, LX/0k9;->t()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LX/3qa;->l:Landroid/database/Cursor;

    if-nez v0, :cond_2

    .line 642300
    :cond_1
    invoke-virtual {p0}, LX/0k9;->a()V

    .line 642301
    :cond_2
    return-void
.end method

.method public final h()V
    .locals 0

    .prologue
    .line 642295
    invoke-virtual {p0}, LX/25A;->b()Z

    .line 642296
    return-void
.end method

.method public final i()V
    .locals 1

    .prologue
    .line 642277
    invoke-super {p0}, LX/25A;->i()V

    .line 642278
    invoke-virtual {p0}, LX/3qa;->h()V

    .line 642279
    iget-object v0, p0, LX/3qa;->l:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3qa;->l:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 642280
    iget-object v0, p0, LX/3qa;->l:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 642281
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/3qa;->l:Landroid/database/Cursor;

    .line 642282
    return-void
.end method
