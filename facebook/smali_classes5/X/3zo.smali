.class public LX/3zo;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static f:Z

.field private static volatile g:LX/3zo;


# instance fields
.field private a:Landroid/app/Activity;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/3zn;",
            ">;"
        }
    .end annotation
.end field

.field private c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/3zq;

.field private final e:LX/3zs;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 662622
    const/4 v0, 0x1

    sput-boolean v0, LX/3zo;->f:Z

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/3zq;LX/2qw;LX/3zs;)V
    .locals 5
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/analytics/useractions/annotations/UserActionsRecorderGK;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/3zq;",
            "LX/2qw;",
            "LX/3zs;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 662623
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 662624
    iput-object p1, p0, LX/3zo;->c:LX/0Or;

    .line 662625
    const/4 v0, 0x0

    iput-object v0, p0, LX/3zo;->a:Landroid/app/Activity;

    .line 662626
    iput-object p4, p0, LX/3zo;->e:LX/3zs;

    .line 662627
    iput-object p2, p0, LX/3zo;->d:LX/3zq;

    .line 662628
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/3zo;->b:Ljava/util/List;

    .line 662629
    invoke-static {p0}, LX/3zo;->b(LX/3zo;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 662630
    iget-object v0, p0, LX/3zo;->b:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 662631
    iget-object v0, p0, LX/3zo;->d:LX/3zq;

    .line 662632
    iget-object v3, v0, LX/3zq;->b:LX/0So;

    invoke-interface {v3}, LX/0So;->now()J

    move-result-wide v3

    long-to-float v3, v3

    iget-object v4, v0, LX/3zq;->c:Ljava/security/SecureRandom;

    invoke-virtual {v4}, Ljava/security/SecureRandom;->nextFloat()F

    move-result v4

    mul-float/2addr v3, v4

    float-to-long v3, v3

    move-wide v1, v3

    .line 662633
    iput-wide v1, v0, LX/3zq;->d:J

    .line 662634
    new-instance v1, Lcom/facebook/analytics/useractions/utils/UserActionEvent;

    sget-object v2, LX/3zr;->SESSION_STARTED:LX/3zr;

    invoke-direct {v1, v2}, Lcom/facebook/analytics/useractions/utils/UserActionEvent;-><init>(LX/3zr;)V

    invoke-static {v0, v1}, LX/3zq;->a(LX/3zq;Lcom/facebook/analytics/useractions/utils/UserActionEvent;)V

    .line 662635
    :cond_0
    return-void
.end method

.method public static a(LX/0QB;)LX/3zo;
    .locals 7

    .prologue
    .line 662636
    sget-object v0, LX/3zo;->g:LX/3zo;

    if-nez v0, :cond_1

    .line 662637
    const-class v1, LX/3zo;

    monitor-enter v1

    .line 662638
    :try_start_0
    sget-object v0, LX/3zo;->g:LX/3zo;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 662639
    if-eqz v2, :cond_0

    .line 662640
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 662641
    new-instance v6, LX/3zo;

    const/16 v3, 0x2f8

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/3zq;->a(LX/0QB;)LX/3zq;

    move-result-object v3

    check-cast v3, LX/3zq;

    invoke-static {v0}, LX/2qw;->a(LX/0QB;)LX/2qw;

    move-result-object v4

    check-cast v4, LX/2qw;

    invoke-static {v0}, LX/3zs;->a(LX/0QB;)LX/3zs;

    move-result-object v5

    check-cast v5, LX/3zs;

    invoke-direct {v6, p0, v3, v4, v5}, LX/3zo;-><init>(LX/0Or;LX/3zq;LX/2qw;LX/3zs;)V

    .line 662642
    move-object v0, v6

    .line 662643
    sput-object v0, LX/3zo;->g:LX/3zo;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 662644
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 662645
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 662646
    :cond_1
    sget-object v0, LX/3zo;->g:LX/3zo;

    return-object v0

    .line 662647
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 662648
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static b(LX/3zo;)Z
    .locals 2

    .prologue
    .line 662649
    sget-boolean v0, LX/3zo;->f:Z

    if-eqz v0, :cond_0

    .line 662650
    sget-boolean v0, LX/007;->i:Z

    move v0, v0

    .line 662651
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3zo;->c:LX/0Or;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3zo;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, LX/03R;->YES:LX/03R;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
