.class public LX/3nq;
.super Landroid/widget/FrameLayout;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Lcom/facebook/components/ComponentView;",
        ">",
        "Landroid/widget/FrameLayout;"
    }
.end annotation


# instance fields
.field public a:LX/7zk;

.field public final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/components/ComponentView;Ljava/lang/Class;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "TV;",
            "Ljava/lang/Class",
            "<TV;>;)V"
        }
    .end annotation

    .prologue
    .line 639194
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 639195
    invoke-virtual {p0, p2}, LX/3nq;->addView(Landroid/view/View;)V

    .line 639196
    iput-object p3, p0, LX/3nq;->b:Ljava/lang/Class;

    .line 639197
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 639202
    iget-object v0, p0, LX/3nq;->a:LX/7zk;

    if-nez v0, :cond_0

    .line 639203
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Trying to detach a FloatingComponentWrapper with no assigned controller"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 639204
    :cond_0
    invoke-virtual {p0}, LX/3nq;->getComponentView()Lcom/facebook/components/ComponentView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/components/ComponentView;->h()V

    .line 639205
    const/4 v0, 0x0

    iput-object v0, p0, LX/3nq;->a:LX/7zk;

    .line 639206
    return-void
.end method

.method public getComponentView()Lcom/facebook/components/ComponentView;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .prologue
    .line 639201
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/3nq;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/components/ComponentView;

    return-object v0
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 639198
    iget-object v0, p0, LX/3nq;->a:LX/7zk;

    if-eqz v0, :cond_0

    .line 639199
    iget-object v0, p0, LX/3nq;->a:LX/7zk;

    invoke-virtual {v0, p1}, LX/7zk;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 639200
    :cond_0
    const/4 v0, 0x1

    return v0
.end method
