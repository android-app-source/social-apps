.class public LX/3wu;
.super LX/1P1;
.source ""


# static fields
.field public static final a:I


# instance fields
.field public b:Z

.field public c:I

.field public d:[I

.field public e:[Landroid/view/View;

.field public final f:Landroid/util/SparseIntArray;

.field public final g:Landroid/util/SparseIntArray;

.field public h:LX/3wr;

.field public final i:Landroid/graphics/Rect;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 656413
    invoke-static {v0, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    sput v0, LX/3wu;->a:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 656419
    invoke-direct {p0, p1}, LX/1P1;-><init>(Landroid/content/Context;)V

    .line 656420
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/3wu;->b:Z

    .line 656421
    const/4 v0, -0x1

    iput v0, p0, LX/3wu;->c:I

    .line 656422
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, LX/3wu;->f:Landroid/util/SparseIntArray;

    .line 656423
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, LX/3wu;->g:Landroid/util/SparseIntArray;

    .line 656424
    new-instance v0, LX/3ws;

    invoke-direct {v0}, LX/3ws;-><init>()V

    iput-object v0, p0, LX/3wu;->h:LX/3wr;

    .line 656425
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/3wu;->i:Landroid/graphics/Rect;

    .line 656426
    invoke-virtual {p0, p2}, LX/3wu;->a(I)V

    .line 656427
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;IIZ)V
    .locals 1

    .prologue
    .line 656428
    invoke-direct {p0, p3, p4}, LX/1P1;-><init>(IZ)V

    .line 656429
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/3wu;->b:Z

    .line 656430
    const/4 v0, -0x1

    iput v0, p0, LX/3wu;->c:I

    .line 656431
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, LX/3wu;->f:Landroid/util/SparseIntArray;

    .line 656432
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, LX/3wu;->g:Landroid/util/SparseIntArray;

    .line 656433
    new-instance v0, LX/3ws;

    invoke-direct {v0}, LX/3ws;-><init>()V

    iput-object v0, p0, LX/3wu;->h:LX/3wr;

    .line 656434
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/3wu;->i:Landroid/graphics/Rect;

    .line 656435
    invoke-virtual {p0, p2}, LX/3wu;->a(I)V

    .line 656436
    return-void
.end method

.method private I()V
    .locals 1

    .prologue
    .line 656437
    iget-object v0, p0, LX/3wu;->f:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->clear()V

    .line 656438
    iget-object v0, p0, LX/3wu;->g:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->clear()V

    .line 656439
    return-void
.end method

.method private J()V
    .locals 6

    .prologue
    .line 656440
    invoke-virtual {p0}, LX/1OR;->v()I

    move-result v2

    .line 656441
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 656442
    invoke-virtual {p0, v1}, LX/1OR;->f(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/3wt;

    .line 656443
    invoke-virtual {v0}, LX/1a3;->e()I

    move-result v3

    .line 656444
    iget-object v4, p0, LX/3wu;->f:Landroid/util/SparseIntArray;

    .line 656445
    iget v5, v0, LX/3wt;->f:I

    move v5, v5

    .line 656446
    invoke-virtual {v4, v3, v5}, Landroid/util/SparseIntArray;->put(II)V

    .line 656447
    iget-object v4, p0, LX/3wu;->g:Landroid/util/SparseIntArray;

    .line 656448
    iget v5, v0, LX/3wt;->e:I

    move v0, v5

    .line 656449
    invoke-virtual {v4, v3, v0}, Landroid/util/SparseIntArray;->put(II)V

    .line 656450
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 656451
    :cond_0
    return-void
.end method

.method private K()V
    .locals 2

    .prologue
    .line 656452
    iget v0, p0, LX/1P1;->j:I

    move v0, v0

    .line 656453
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 656454
    invoke-virtual {p0}, LX/1OR;->w()I

    move-result v0

    invoke-virtual {p0}, LX/1OR;->A()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, LX/1OR;->y()I

    move-result v1

    sub-int/2addr v0, v1

    .line 656455
    :goto_0
    invoke-direct {p0, v0}, LX/3wu;->j(I)V

    .line 656456
    return-void

    .line 656457
    :cond_0
    invoke-virtual {p0}, LX/1OR;->x()I

    move-result v0

    invoke-virtual {p0}, LX/1OR;->B()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, LX/1OR;->z()I

    move-result v1

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method private static a(III)I
    .locals 2

    .prologue
    .line 656414
    if-nez p1, :cond_1

    if-nez p2, :cond_1

    .line 656415
    :cond_0
    :goto_0
    return p0

    .line 656416
    :cond_1
    invoke-static {p0}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    .line 656417
    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_2

    const/high16 v1, 0x40000000    # 2.0f

    if-ne v0, v1, :cond_0

    .line 656418
    :cond_2
    invoke-static {p0}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    sub-int/2addr v1, p1

    sub-int/2addr v1, p2

    invoke-static {v1, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p0

    goto :goto_0
.end method

.method private a(LX/1Od;LX/1Ok;I)I
    .locals 3

    .prologue
    .line 656461
    iget-boolean v0, p2, LX/1Ok;->k:Z

    move v0, v0

    .line 656462
    if-nez v0, :cond_0

    .line 656463
    iget-object v0, p0, LX/3wu;->h:LX/3wr;

    iget v1, p0, LX/3wu;->c:I

    invoke-virtual {v0, p3, v1}, LX/3wr;->c(II)I

    move-result v0

    .line 656464
    :goto_0
    return v0

    .line 656465
    :cond_0
    invoke-virtual {p1, p3}, LX/1Od;->b(I)I

    move-result v0

    .line 656466
    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 656467
    const-string v0, "GridLayoutManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot find span size for pre layout position. "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 656468
    const/4 v0, 0x0

    goto :goto_0

    .line 656469
    :cond_1
    iget-object v1, p0, LX/3wu;->h:LX/3wr;

    iget v2, p0, LX/3wu;->c:I

    invoke-virtual {v1, v0, v2}, LX/3wr;->c(II)I

    move-result v0

    goto :goto_0
.end method

.method private a(LX/1Od;LX/1Ok;IZ)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x1

    const/4 v3, -0x1

    .line 656470
    if-eqz p4, :cond_0

    move v1, v2

    move v0, v4

    .line 656471
    :goto_0
    iget v5, p0, LX/1P1;->j:I

    if-ne v5, v2, :cond_1

    invoke-virtual {p0}, LX/1P1;->j()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 656472
    iget v4, p0, LX/3wu;->c:I

    add-int/lit8 v4, v4, -0x1

    move v5, v4

    move v4, v3

    :goto_1
    move v6, v5

    move v5, v0

    .line 656473
    :goto_2
    if-eq v5, p3, :cond_3

    .line 656474
    iget-object v0, p0, LX/3wu;->e:[Landroid/view/View;

    aget-object v7, v0, v5

    .line 656475
    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/3wt;

    .line 656476
    invoke-static {v7}, LX/1OR;->d(Landroid/view/View;)I

    move-result v7

    invoke-direct {p0, p1, p2, v7}, LX/3wu;->c(LX/1Od;LX/1Ok;I)I

    move-result v7

    .line 656477
    iput v7, v0, LX/3wt;->f:I

    .line 656478
    if-ne v4, v3, :cond_2

    iget v7, v0, LX/3wt;->f:I

    if-le v7, v2, :cond_2

    .line 656479
    iget v7, v0, LX/3wt;->f:I

    add-int/lit8 v7, v7, -0x1

    sub-int v7, v6, v7

    .line 656480
    iput v7, v0, LX/3wt;->e:I

    .line 656481
    :goto_3
    iget v0, v0, LX/3wt;->f:I

    mul-int/2addr v0, v4

    add-int/2addr v6, v0

    .line 656482
    add-int v0, v5, v1

    move v5, v0

    goto :goto_2

    .line 656483
    :cond_0
    add-int/lit8 v0, p3, -0x1

    move v1, v3

    move p3, v3

    .line 656484
    goto :goto_0

    :cond_1
    move v5, v4

    move v4, v2

    .line 656485
    goto :goto_1

    .line 656486
    :cond_2
    iput v6, v0, LX/3wt;->e:I

    .line 656487
    goto :goto_3

    .line 656488
    :cond_3
    return-void
.end method

.method private a(Landroid/view/View;IIZ)V
    .locals 4

    .prologue
    .line 656489
    iget-object v0, p0, LX/3wu;->i:Landroid/graphics/Rect;

    invoke-virtual {p0, p1, v0}, LX/1OR;->a(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 656490
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/1a3;

    .line 656491
    if-nez p4, :cond_0

    iget v1, p0, LX/1P1;->j:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 656492
    :cond_0
    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget-object v2, p0, LX/3wu;->i:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, v2

    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    iget-object v3, p0, LX/3wu;->i:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    add-int/2addr v2, v3

    invoke-static {p2, v1, v2}, LX/3wu;->a(III)I

    move-result p2

    .line 656493
    :cond_1
    if-nez p4, :cond_2

    iget v1, p0, LX/1P1;->j:I

    if-nez v1, :cond_3

    .line 656494
    :cond_2
    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget-object v2, p0, LX/3wu;->i:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    add-int/2addr v1, v2

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    iget-object v2, p0, LX/3wu;->i:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v2

    invoke-static {p3, v1, v0}, LX/3wu;->a(III)I

    move-result p3

    .line 656495
    :cond_3
    invoke-virtual {p1, p2, p3}, Landroid/view/View;->measure(II)V

    .line 656496
    return-void
.end method

.method private b(LX/1Od;LX/1Ok;I)I
    .locals 3

    .prologue
    const/4 v1, -0x1

    .line 656497
    iget-boolean v0, p2, LX/1Ok;->k:Z

    move v0, v0

    .line 656498
    if-nez v0, :cond_1

    .line 656499
    iget-object v0, p0, LX/3wu;->h:LX/3wr;

    iget v1, p0, LX/3wu;->c:I

    invoke-virtual {v0, p3, v1}, LX/3wr;->b(II)I

    move-result v0

    .line 656500
    :cond_0
    :goto_0
    return v0

    .line 656501
    :cond_1
    iget-object v0, p0, LX/3wu;->g:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p3, v1}, Landroid/util/SparseIntArray;->get(II)I

    move-result v0

    .line 656502
    if-ne v0, v1, :cond_0

    .line 656503
    invoke-virtual {p1, p3}, LX/1Od;->b(I)I

    move-result v0

    .line 656504
    if-ne v0, v1, :cond_2

    .line 656505
    const-string v0, "GridLayoutManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot find span size for pre layout position. It is not cached, not in the adapter. Pos:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 656506
    const/4 v0, 0x0

    goto :goto_0

    .line 656507
    :cond_2
    iget-object v1, p0, LX/3wu;->h:LX/3wr;

    iget v2, p0, LX/3wu;->c:I

    invoke-virtual {v1, v0, v2}, LX/3wr;->b(II)I

    move-result v0

    goto :goto_0
.end method

.method private b(LX/1Od;LX/1Ok;LX/1P3;)V
    .locals 1

    .prologue
    .line 656508
    iget v0, p3, LX/1P3;->a:I

    invoke-direct {p0, p1, p2, v0}, LX/3wu;->b(LX/1Od;LX/1Ok;I)I

    move-result v0

    .line 656509
    :goto_0
    if-lez v0, :cond_0

    iget v0, p3, LX/1P3;->a:I

    if-lez v0, :cond_0

    .line 656510
    iget v0, p3, LX/1P3;->a:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p3, LX/1P3;->a:I

    .line 656511
    iget v0, p3, LX/1P3;->a:I

    invoke-direct {p0, p1, p2, v0}, LX/3wu;->b(LX/1Od;LX/1Ok;I)I

    move-result v0

    goto :goto_0

    .line 656512
    :cond_0
    return-void
.end method

.method private c(LX/1Od;LX/1Ok;I)I
    .locals 3

    .prologue
    const/4 v1, -0x1

    .line 656513
    iget-boolean v0, p2, LX/1Ok;->k:Z

    move v0, v0

    .line 656514
    if-nez v0, :cond_1

    .line 656515
    iget-object v0, p0, LX/3wu;->h:LX/3wr;

    invoke-virtual {v0, p3}, LX/3wr;->a(I)I

    move-result v0

    .line 656516
    :cond_0
    :goto_0
    return v0

    .line 656517
    :cond_1
    iget-object v0, p0, LX/3wu;->f:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p3, v1}, Landroid/util/SparseIntArray;->get(II)I

    move-result v0

    .line 656518
    if-ne v0, v1, :cond_0

    .line 656519
    invoke-virtual {p1, p3}, LX/1Od;->b(I)I

    move-result v0

    .line 656520
    if-ne v0, v1, :cond_2

    .line 656521
    const-string v0, "GridLayoutManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot find span size for pre layout position. It is not cached, not in the adapter. Pos:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 656522
    const/4 v0, 0x1

    goto :goto_0

    .line 656523
    :cond_2
    iget-object v1, p0, LX/3wu;->h:LX/3wr;

    invoke-virtual {v1, v0}, LX/3wr;->a(I)I

    move-result v0

    goto :goto_0
.end method

.method private j(I)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 656524
    iget-object v0, p0, LX/3wu;->d:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3wu;->d:[I

    array-length v0, v0

    iget v2, p0, LX/3wu;->c:I

    add-int/lit8 v2, v2, 0x1

    if-ne v0, v2, :cond_0

    iget-object v0, p0, LX/3wu;->d:[I

    iget-object v2, p0, LX/3wu;->d:[I

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    aget v0, v0, v2

    if-eq v0, p1, :cond_1

    .line 656525
    :cond_0
    iget v0, p0, LX/3wu;->c:I

    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, LX/3wu;->d:[I

    .line 656526
    :cond_1
    iget-object v0, p0, LX/3wu;->d:[I

    aput v1, v0, v1

    .line 656527
    iget v0, p0, LX/3wu;->c:I

    div-int v4, p1, v0

    .line 656528
    iget v0, p0, LX/3wu;->c:I

    rem-int v5, p1, v0

    .line 656529
    const/4 v0, 0x1

    move v2, v1

    :goto_0
    iget v3, p0, LX/3wu;->c:I

    if-gt v0, v3, :cond_2

    .line 656530
    add-int v3, v1, v5

    .line 656531
    if-lez v3, :cond_3

    iget v1, p0, LX/3wu;->c:I

    sub-int/2addr v1, v3

    if-ge v1, v5, :cond_3

    .line 656532
    add-int/lit8 v1, v4, 0x1

    .line 656533
    iget v6, p0, LX/3wu;->c:I

    sub-int/2addr v3, v6

    move v7, v1

    move v1, v3

    move v3, v7

    .line 656534
    :goto_1
    add-int/2addr v2, v3

    .line 656535
    iget-object v3, p0, LX/3wu;->d:[I

    aput v2, v3, v0

    .line 656536
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 656537
    :cond_2
    return-void

    :cond_3
    move v1, v3

    move v3, v4

    goto :goto_1
.end method

.method private static k(I)I
    .locals 1

    .prologue
    .line 656538
    if-gez p0, :cond_0

    .line 656539
    sget v0, LX/3wu;->a:I

    .line 656540
    :goto_0
    return v0

    :cond_0
    const/high16 v0, 0x40000000    # 2.0f

    invoke-static {p0, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1Od;LX/1Ok;)I
    .locals 1

    .prologue
    .line 656541
    iget v0, p0, LX/1P1;->j:I

    if-nez v0, :cond_0

    .line 656542
    iget v0, p0, LX/3wu;->c:I

    .line 656543
    :goto_0
    return v0

    .line 656544
    :cond_0
    invoke-virtual {p2}, LX/1Ok;->e()I

    move-result v0

    if-gtz v0, :cond_1

    .line 656545
    const/4 v0, 0x0

    goto :goto_0

    .line 656546
    :cond_1
    invoke-virtual {p2}, LX/1Ok;->e()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, p1, p2, v0}, LX/3wu;->a(LX/1Od;LX/1Ok;I)I

    move-result v0

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Landroid/util/AttributeSet;)LX/1a3;
    .locals 1

    .prologue
    .line 656547
    new-instance v0, LX/3wt;

    invoke-direct {v0, p1, p2}, LX/3wt;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method public final a(Landroid/view/ViewGroup$LayoutParams;)LX/1a3;
    .locals 1

    .prologue
    .line 656458
    instance-of v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v0, :cond_0

    .line 656459
    new-instance v0, LX/3wt;

    check-cast p1, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-direct {v0, p1}, LX/3wt;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    .line 656460
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/3wt;

    invoke-direct {v0, p1}, LX/3wt;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public final a(LX/1Od;LX/1Ok;III)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 656251
    invoke-virtual {p0}, LX/1P1;->k()V

    .line 656252
    iget-object v0, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v0}, LX/1P5;->c()I

    move-result v5

    .line 656253
    iget-object v0, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v0}, LX/1P5;->d()I

    move-result v6

    .line 656254
    if-le p4, p3, :cond_0

    const/4 v0, 0x1

    move v1, v0

    :goto_0
    move-object v4, v2

    .line 656255
    :goto_1
    if-eq p3, p4, :cond_3

    .line 656256
    invoke-virtual {p0, p3}, LX/1OR;->f(I)Landroid/view/View;

    move-result-object v3

    .line 656257
    invoke-static {v3}, LX/1OR;->d(Landroid/view/View;)I

    move-result v0

    .line 656258
    if-ltz v0, :cond_6

    if-ge v0, p5, :cond_6

    .line 656259
    invoke-direct {p0, p1, p2, v0}, LX/3wu;->b(LX/1Od;LX/1Ok;I)I

    move-result v0

    .line 656260
    if-nez v0, :cond_6

    .line 656261
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/1a3;

    invoke-virtual {v0}, LX/1a3;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 656262
    if-nez v4, :cond_6

    move-object v0, v2

    .line 656263
    :goto_2
    add-int/2addr p3, v1

    move-object v2, v0

    move-object v4, v3

    goto :goto_1

    .line 656264
    :cond_0
    const/4 v0, -0x1

    move v1, v0

    goto :goto_0

    .line 656265
    :cond_1
    iget-object v0, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v0, v3}, LX/1P5;->a(Landroid/view/View;)I

    move-result v0

    if-ge v0, v6, :cond_2

    iget-object v0, p0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v0, v3}, LX/1P5;->b(Landroid/view/View;)I

    move-result v0

    if-ge v0, v5, :cond_4

    .line 656266
    :cond_2
    if-nez v2, :cond_6

    move-object v0, v3

    move-object v3, v4

    .line 656267
    goto :goto_2

    .line 656268
    :cond_3
    if-eqz v2, :cond_5

    move-object v3, v2

    :cond_4
    :goto_3
    return-object v3

    :cond_5
    move-object v3, v4

    goto :goto_3

    :cond_6
    move-object v0, v2

    move-object v3, v4

    goto :goto_2
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 656269
    iget-object v0, p0, LX/3wu;->h:LX/3wr;

    invoke-virtual {v0}, LX/3wr;->a()V

    .line 656270
    return-void
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 656385
    iget v0, p0, LX/3wu;->c:I

    if-ne p1, v0, :cond_0

    .line 656386
    :goto_0
    return-void

    .line 656387
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/3wu;->b:Z

    .line 656388
    if-gtz p1, :cond_1

    .line 656389
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Span count should be at least 1. Provided "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 656390
    :cond_1
    iput p1, p0, LX/3wu;->c:I

    .line 656391
    iget-object v0, p0, LX/3wu;->h:LX/3wr;

    invoke-virtual {v0}, LX/3wr;->a()V

    goto :goto_0
.end method

.method public final a(II)V
    .locals 1

    .prologue
    .line 656271
    iget-object v0, p0, LX/3wu;->h:LX/3wr;

    invoke-virtual {v0}, LX/3wr;->a()V

    .line 656272
    return-void
.end method

.method public final a(LX/1Od;LX/1Ok;LX/1P3;)V
    .locals 2

    .prologue
    .line 656273
    invoke-super {p0, p1, p2, p3}, LX/1P1;->a(LX/1Od;LX/1Ok;LX/1P3;)V

    .line 656274
    invoke-direct {p0}, LX/3wu;->K()V

    .line 656275
    invoke-virtual {p2}, LX/1Ok;->e()I

    move-result v0

    if-lez v0, :cond_0

    .line 656276
    iget-boolean v0, p2, LX/1Ok;->k:Z

    move v0, v0

    .line 656277
    if-nez v0, :cond_0

    .line 656278
    invoke-direct {p0, p1, p2, p3}, LX/3wu;->b(LX/1Od;LX/1Ok;LX/1P3;)V

    .line 656279
    :cond_0
    iget-object v0, p0, LX/3wu;->e:[Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/3wu;->e:[Landroid/view/View;

    array-length v0, v0

    iget v1, p0, LX/3wu;->c:I

    if-eq v0, v1, :cond_2

    .line 656280
    :cond_1
    iget v0, p0, LX/3wu;->c:I

    new-array v0, v0, [Landroid/view/View;

    iput-object v0, p0, LX/3wu;->e:[Landroid/view/View;

    .line 656281
    :cond_2
    return-void
.end method

.method public final a(LX/1Od;LX/1Ok;LX/1ZW;LX/1Zz;)V
    .locals 17

    .prologue
    .line 656282
    move-object/from16 v0, p3

    iget v3, v0, LX/1ZW;->e:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    const/4 v3, 0x1

    move v4, v3

    .line 656283
    :goto_0
    const/4 v5, 0x0

    .line 656284
    move-object/from16 v0, p0

    iget v3, v0, LX/3wu;->c:I

    .line 656285
    if-nez v4, :cond_14

    .line 656286
    move-object/from16 v0, p3

    iget v3, v0, LX/1ZW;->d:I

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v2, v3}, LX/3wu;->b(LX/1Od;LX/1Ok;I)I

    move-result v3

    .line 656287
    move-object/from16 v0, p3

    iget v6, v0, LX/1ZW;->d:I

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v2, v6}, LX/3wu;->c(LX/1Od;LX/1Ok;I)I

    move-result v6

    .line 656288
    add-int/2addr v3, v6

    move v10, v5

    .line 656289
    :goto_1
    move-object/from16 v0, p0

    iget v5, v0, LX/3wu;->c:I

    if-ge v10, v5, :cond_2

    move-object/from16 v0, p3

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, LX/1ZW;->a(LX/1Ok;)Z

    move-result v5

    if-eqz v5, :cond_2

    if-lez v3, :cond_2

    .line 656290
    move-object/from16 v0, p3

    iget v5, v0, LX/1ZW;->d:I

    .line 656291
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v2, v5}, LX/3wu;->c(LX/1Od;LX/1Ok;I)I

    move-result v6

    .line 656292
    move-object/from16 v0, p0

    iget v7, v0, LX/3wu;->c:I

    if-le v6, v7, :cond_1

    .line 656293
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v7, "Item at position "

    invoke-direct {v4, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " requires "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " spans but GridLayoutManager has only "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget v5, v0, LX/3wu;->c:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " spans."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 656294
    :cond_0
    const/4 v3, 0x0

    move v4, v3

    goto :goto_0

    .line 656295
    :cond_1
    sub-int/2addr v3, v6

    .line 656296
    if-ltz v3, :cond_2

    .line 656297
    move-object/from16 v0, p3

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, LX/1ZW;->a(LX/1Od;)Landroid/view/View;

    move-result-object v5

    .line 656298
    if-eqz v5, :cond_2

    .line 656299
    move-object/from16 v0, p0

    iget-object v6, v0, LX/3wu;->e:[Landroid/view/View;

    aput-object v5, v6, v10

    .line 656300
    add-int/lit8 v5, v10, 0x1

    move v10, v5

    .line 656301
    goto :goto_1

    .line 656302
    :cond_2
    if-nez v10, :cond_3

    .line 656303
    const/4 v3, 0x1

    move-object/from16 v0, p4

    iput-boolean v3, v0, LX/1Zz;->b:Z

    .line 656304
    :goto_2
    return-void

    .line 656305
    :cond_3
    const/4 v5, 0x0

    .line 656306
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v2, v10, v4}, LX/3wu;->a(LX/1Od;LX/1Ok;IZ)V

    .line 656307
    const/4 v3, 0x0

    move v6, v3

    :goto_3
    if-ge v6, v10, :cond_8

    .line 656308
    move-object/from16 v0, p0

    iget-object v3, v0, LX/3wu;->e:[Landroid/view/View;

    aget-object v7, v3, v6

    .line 656309
    move-object/from16 v0, p3

    iget-object v3, v0, LX/1ZW;->k:Ljava/util/List;

    if-nez v3, :cond_5

    .line 656310
    if-eqz v4, :cond_4

    .line 656311
    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, LX/1OR;->b(Landroid/view/View;)V

    .line 656312
    :goto_4
    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, LX/3wt;

    .line 656313
    move-object/from16 v0, p0

    iget-object v8, v0, LX/3wu;->d:[I

    iget v9, v3, LX/3wt;->e:I

    iget v11, v3, LX/3wt;->f:I

    add-int/2addr v9, v11

    aget v8, v8, v9

    move-object/from16 v0, p0

    iget-object v9, v0, LX/3wu;->d:[I

    iget v11, v3, LX/3wt;->e:I

    aget v9, v9, v11

    sub-int/2addr v8, v9

    const/high16 v9, 0x40000000    # 2.0f

    invoke-static {v8, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    .line 656314
    move-object/from16 v0, p0

    iget v9, v0, LX/1P1;->j:I

    const/4 v11, 0x1

    if-ne v9, v11, :cond_7

    .line 656315
    iget v3, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {v3}, LX/3wu;->k(I)I

    move-result v3

    const/4 v9, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v7, v8, v3, v9}, LX/3wu;->a(Landroid/view/View;IIZ)V

    .line 656316
    :goto_5
    move-object/from16 v0, p0

    iget-object v3, v0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v3, v7}, LX/1P5;->c(Landroid/view/View;)I

    move-result v3

    .line 656317
    if-le v3, v5, :cond_13

    .line 656318
    :goto_6
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    move v5, v3

    goto :goto_3

    .line 656319
    :cond_4
    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v7, v3}, LX/1OR;->b(Landroid/view/View;I)V

    goto :goto_4

    .line 656320
    :cond_5
    if-eqz v4, :cond_6

    .line 656321
    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, LX/1OR;->a(Landroid/view/View;)V

    goto :goto_4

    .line 656322
    :cond_6
    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v7, v3}, LX/1OR;->a(Landroid/view/View;I)V

    goto :goto_4

    .line 656323
    :cond_7
    iget v3, v3, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-static {v3}, LX/3wu;->k(I)I

    move-result v3

    const/4 v9, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v7, v3, v8, v9}, LX/3wu;->a(Landroid/view/View;IIZ)V

    goto :goto_5

    .line 656324
    :cond_8
    invoke-static {v5}, LX/3wu;->k(I)I

    move-result v6

    .line 656325
    const/4 v3, 0x0

    move v4, v3

    :goto_7
    if-ge v4, v10, :cond_b

    .line 656326
    move-object/from16 v0, p0

    iget-object v3, v0, LX/3wu;->e:[Landroid/view/View;

    aget-object v7, v3, v4

    .line 656327
    move-object/from16 v0, p0

    iget-object v3, v0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v3, v7}, LX/1P5;->c(Landroid/view/View;)I

    move-result v3

    if-eq v3, v5, :cond_9

    .line 656328
    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, LX/3wt;

    .line 656329
    move-object/from16 v0, p0

    iget-object v8, v0, LX/3wu;->d:[I

    iget v9, v3, LX/3wt;->e:I

    iget v11, v3, LX/3wt;->f:I

    add-int/2addr v9, v11

    aget v8, v8, v9

    move-object/from16 v0, p0

    iget-object v9, v0, LX/3wu;->d:[I

    iget v3, v3, LX/3wt;->e:I

    aget v3, v9, v3

    sub-int v3, v8, v3

    const/high16 v8, 0x40000000    # 2.0f

    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 656330
    move-object/from16 v0, p0

    iget v8, v0, LX/1P1;->j:I

    const/4 v9, 0x1

    if-ne v8, v9, :cond_a

    .line 656331
    const/4 v8, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v7, v3, v6, v8}, LX/3wu;->a(Landroid/view/View;IIZ)V

    .line 656332
    :cond_9
    :goto_8
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_7

    .line 656333
    :cond_a
    const/4 v8, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v7, v6, v3, v8}, LX/3wu;->a(Landroid/view/View;IIZ)V

    goto :goto_8

    .line 656334
    :cond_b
    move-object/from16 v0, p4

    iput v5, v0, LX/1Zz;->a:I

    .line 656335
    const/4 v7, 0x0

    const/4 v6, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 656336
    move-object/from16 v0, p0

    iget v8, v0, LX/1P1;->j:I

    const/4 v9, 0x1

    if-ne v8, v9, :cond_f

    .line 656337
    move-object/from16 v0, p3

    iget v3, v0, LX/1ZW;->f:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_e

    .line 656338
    move-object/from16 v0, p3

    iget v3, v0, LX/1ZW;->b:I

    .line 656339
    sub-int v4, v3, v5

    move v5, v6

    move v6, v7

    .line 656340
    :goto_9
    const/4 v7, 0x0

    move v15, v7

    move v8, v6

    move v6, v4

    move v7, v5

    move v5, v3

    :goto_a
    if-ge v15, v10, :cond_12

    .line 656341
    move-object/from16 v0, p0

    iget-object v3, v0, LX/3wu;->e:[Landroid/view/View;

    aget-object v4, v3, v15

    .line 656342
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    move-object v9, v3

    check-cast v9, LX/3wt;

    .line 656343
    move-object/from16 v0, p0

    iget v3, v0, LX/1P1;->j:I

    const/4 v11, 0x1

    if-ne v3, v11, :cond_11

    .line 656344
    invoke-virtual/range {p0 .. p0}, LX/1OR;->y()I

    move-result v3

    move-object/from16 v0, p0

    iget-object v7, v0, LX/3wu;->d:[I

    iget v8, v9, LX/3wt;->e:I

    aget v7, v7, v8

    add-int/2addr v7, v3

    .line 656345
    move-object/from16 v0, p0

    iget-object v3, v0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v3, v4}, LX/1P5;->d(Landroid/view/View;)I

    move-result v3

    add-int/2addr v3, v7

    move v11, v5

    move v12, v6

    move v13, v3

    move v14, v7

    .line 656346
    :goto_b
    iget v3, v9, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int v5, v14, v3

    iget v3, v9, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int v6, v12, v3

    iget v3, v9, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    sub-int v7, v13, v3

    iget v3, v9, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    sub-int v8, v11, v3

    move-object/from16 v3, p0

    invoke-virtual/range {v3 .. v8}, LX/1OR;->a(Landroid/view/View;IIII)V

    .line 656347
    invoke-virtual {v9}, LX/1a3;->c()Z

    move-result v3

    if-nez v3, :cond_c

    invoke-virtual {v9}, LX/1a3;->d()Z

    move-result v3

    if-eqz v3, :cond_d

    .line 656348
    :cond_c
    const/4 v3, 0x1

    move-object/from16 v0, p4

    iput-boolean v3, v0, LX/1Zz;->c:Z

    .line 656349
    :cond_d
    move-object/from16 v0, p4

    iget-boolean v3, v0, LX/1Zz;->d:Z

    invoke-virtual {v4}, Landroid/view/View;->isFocusable()Z

    move-result v4

    or-int/2addr v3, v4

    move-object/from16 v0, p4

    iput-boolean v3, v0, LX/1Zz;->d:Z

    .line 656350
    add-int/lit8 v3, v15, 0x1

    move v15, v3

    move v5, v11

    move v6, v12

    move v7, v13

    move v8, v14

    goto :goto_a

    .line 656351
    :cond_e
    move-object/from16 v0, p3

    iget v4, v0, LX/1ZW;->b:I

    .line 656352
    add-int v3, v4, v5

    move v5, v6

    move v6, v7

    goto :goto_9

    .line 656353
    :cond_f
    move-object/from16 v0, p3

    iget v6, v0, LX/1ZW;->f:I

    const/4 v7, -0x1

    if-ne v6, v7, :cond_10

    .line 656354
    move-object/from16 v0, p3

    iget v6, v0, LX/1ZW;->b:I

    .line 656355
    sub-int v5, v6, v5

    move/from16 v16, v6

    move v6, v5

    move/from16 v5, v16

    goto/16 :goto_9

    .line 656356
    :cond_10
    move-object/from16 v0, p3

    iget v6, v0, LX/1ZW;->b:I

    .line 656357
    add-int/2addr v5, v6

    goto/16 :goto_9

    .line 656358
    :cond_11
    invoke-virtual/range {p0 .. p0}, LX/1OR;->z()I

    move-result v3

    move-object/from16 v0, p0

    iget-object v5, v0, LX/3wu;->d:[I

    iget v6, v9, LX/3wt;->e:I

    aget v5, v5, v6

    add-int/2addr v5, v3

    .line 656359
    move-object/from16 v0, p0

    iget-object v3, v0, LX/1P1;->k:LX/1P5;

    invoke-virtual {v3, v4}, LX/1P5;->d(Landroid/view/View;)I

    move-result v3

    add-int/2addr v3, v5

    move v11, v3

    move v12, v5

    move v13, v7

    move v14, v8

    goto :goto_b

    .line 656360
    :cond_12
    move-object/from16 v0, p0

    iget-object v3, v0, LX/3wu;->e:[Landroid/view/View;

    const/4 v4, 0x0

    invoke-static {v3, v4}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_2

    :cond_13
    move v3, v5

    goto/16 :goto_6

    :cond_14
    move v10, v5

    goto/16 :goto_1
.end method

.method public final a(LX/1Od;LX/1Ok;Landroid/view/View;LX/3sp;)V
    .locals 8

    .prologue
    .line 656361
    invoke-virtual {p3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 656362
    instance-of v1, v0, LX/3wt;

    if-nez v1, :cond_0

    .line 656363
    invoke-super {p0, p3, p4}, LX/1P1;->a(Landroid/view/View;LX/3sp;)V

    .line 656364
    :goto_0
    return-void

    :cond_0
    move-object v6, v0

    .line 656365
    check-cast v6, LX/3wt;

    .line 656366
    invoke-virtual {v6}, LX/1a3;->e()I

    move-result v0

    invoke-direct {p0, p1, p2, v0}, LX/3wu;->a(LX/1Od;LX/1Ok;I)I

    move-result v2

    .line 656367
    iget v0, p0, LX/1P1;->j:I

    if-nez v0, :cond_2

    .line 656368
    iget v0, v6, LX/3wt;->e:I

    move v0, v0

    .line 656369
    iget v1, v6, LX/3wt;->f:I

    move v1, v1

    .line 656370
    const/4 v3, 0x1

    iget v4, p0, LX/3wu;->c:I

    const/4 v5, 0x1

    if-le v4, v5, :cond_1

    .line 656371
    iget v4, v6, LX/3wt;->f:I

    move v4, v4

    .line 656372
    iget v5, p0, LX/3wu;->c:I

    if-ne v4, v5, :cond_1

    const/4 v4, 0x1

    :goto_1
    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, LX/3so;->a(IIIIZZ)LX/3so;

    move-result-object v0

    invoke-virtual {p4, v0}, LX/3sp;->b(Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    goto :goto_1

    .line 656373
    :cond_2
    const/4 v3, 0x1

    .line 656374
    iget v0, v6, LX/3wt;->e:I

    move v4, v0

    .line 656375
    iget v0, v6, LX/3wt;->f:I

    move v5, v0

    .line 656376
    iget v0, p0, LX/3wu;->c:I

    const/4 v1, 0x1

    if-le v0, v1, :cond_3

    .line 656377
    iget v0, v6, LX/3wt;->f:I

    move v0, v0

    .line 656378
    iget v1, p0, LX/3wu;->c:I

    if-ne v0, v1, :cond_3

    const/4 v6, 0x1

    :goto_2
    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, LX/3so;->a(IIIIZZ)LX/3so;

    move-result-object v0

    invoke-virtual {p4, v0}, LX/3sp;->b(Ljava/lang/Object;)V

    goto :goto_0

    :cond_3
    const/4 v6, 0x0

    goto :goto_2
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 1

    .prologue
    .line 656379
    iget-object v0, p0, LX/3wu;->h:LX/3wr;

    invoke-virtual {v0}, LX/3wr;->a()V

    .line 656380
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 656381
    if-eqz p1, :cond_0

    .line 656382
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "GridLayoutManager does not support stack from end. Consider using reverse layout"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 656383
    :cond_0
    const/4 v0, 0x0

    invoke-super {p0, v0}, LX/1P1;->a(Z)V

    .line 656384
    return-void
.end method

.method public final a(LX/1a3;)Z
    .locals 1

    .prologue
    .line 656250
    instance-of v0, p1, LX/3wt;

    return v0
.end method

.method public final b(LX/1Od;LX/1Ok;)I
    .locals 2

    .prologue
    .line 656392
    iget v0, p0, LX/1P1;->j:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 656393
    iget v0, p0, LX/3wu;->c:I

    .line 656394
    :goto_0
    return v0

    .line 656395
    :cond_0
    invoke-virtual {p2}, LX/1Ok;->e()I

    move-result v0

    if-gtz v0, :cond_1

    .line 656396
    const/4 v0, 0x0

    goto :goto_0

    .line 656397
    :cond_1
    invoke-virtual {p2}, LX/1Ok;->e()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, p1, p2, v0}, LX/3wu;->a(LX/1Od;LX/1Ok;I)I

    move-result v0

    goto :goto_0
.end method

.method public final b()LX/1a3;
    .locals 2

    .prologue
    const/4 v1, -0x2

    .line 656398
    new-instance v0, LX/3wt;

    invoke-direct {v0, v1, v1}, LX/3wt;-><init>(II)V

    return-object v0
.end method

.method public final b(II)V
    .locals 1

    .prologue
    .line 656399
    iget-object v0, p0, LX/3wu;->h:LX/3wr;

    invoke-virtual {v0}, LX/3wr;->a()V

    .line 656400
    return-void
.end method

.method public final c(II)V
    .locals 1

    .prologue
    .line 656401
    iget-object v0, p0, LX/3wu;->h:LX/3wr;

    invoke-virtual {v0}, LX/3wr;->a()V

    .line 656402
    return-void
.end method

.method public c(LX/1Od;LX/1Ok;)V
    .locals 1

    .prologue
    .line 656403
    iget-boolean v0, p2, LX/1Ok;->k:Z

    move v0, v0

    .line 656404
    if-eqz v0, :cond_0

    .line 656405
    invoke-direct {p0}, LX/3wu;->J()V

    .line 656406
    :cond_0
    invoke-super {p0, p1, p2}, LX/1P1;->c(LX/1Od;LX/1Ok;)V

    .line 656407
    invoke-direct {p0}, LX/3wu;->I()V

    .line 656408
    iget-boolean v0, p2, LX/1Ok;->k:Z

    move v0, v0

    .line 656409
    if-nez v0, :cond_1

    .line 656410
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/3wu;->b:Z

    .line 656411
    :cond_1
    return-void
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 656412
    iget-object v0, p0, LX/1P1;->o:Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/3wu;->b:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
