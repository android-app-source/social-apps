.class public LX/3Ri;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;


# instance fields
.field private b:LX/0Uh;

.field public c:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 581448
    const-class v0, LX/3Ri;

    sput-object v0, LX/3Ri;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Uh;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 581444
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 581445
    iput-object p1, p0, LX/3Ri;->b:LX/0Uh;

    .line 581446
    iput-object p2, p0, LX/3Ri;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 581447
    return-void
.end method

.method public static a(LX/0QB;)LX/3Ri;
    .locals 1

    .prologue
    .line 581443
    invoke-static {p0}, LX/3Ri;->b(LX/0QB;)LX/3Ri;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/3Ri;
    .locals 3

    .prologue
    .line 581449
    new-instance v2, LX/3Ri;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v0

    check-cast v0, LX/0Uh;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {v2, v0, v1}, LX/3Ri;-><init>(LX/0Uh;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 581450
    return-object v2
.end method


# virtual methods
.method public final a(LX/KAh;)V
    .locals 4

    .prologue
    .line 581433
    const-string v0, "messenger_wear"

    invoke-interface {p1}, LX/KAh;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 581434
    :goto_0
    return-void

    .line 581435
    :cond_0
    invoke-interface {p1}, LX/KAh;->b()Ljava/util/Set;

    move-result-object v0

    .line 581436
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/KAv;

    .line 581437
    invoke-interface {v1}, LX/KAv;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 581438
    const/4 v1, 0x1

    .line 581439
    :goto_1
    move v0, v1

    .line 581440
    invoke-virtual {p0}, LX/3Ri;->a()Z

    move-result v1

    .line 581441
    invoke-interface {p1}, LX/KAh;->b()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->size()I

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 581442
    iget-object v2, p0, LX/3Ri;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v3, LX/JtV;->b:LX/0Tn;

    invoke-interface {v2, v3, v0}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v2

    sget-object v3, LX/JtV;->c:LX/0Tn;

    if-nez v1, :cond_2

    if-eqz v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    :goto_2
    invoke-interface {v2, v3, v0}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    :cond_4
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 581432
    invoke-virtual {p0}, LX/3Ri;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/3Ri;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/JtV;->c:LX/0Tn;

    invoke-interface {v1, v2, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final c()Z
    .locals 3

    .prologue
    .line 581431
    iget-object v0, p0, LX/3Ri;->b:LX/0Uh;

    const/16 v1, 0x56d

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method
