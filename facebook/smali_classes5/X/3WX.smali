.class public LX/3WX;
.super Lcom/facebook/attachments/angora/AngoraAttachmentView;
.source ""


# static fields
.field public static final c:LX/1Cz;


# instance fields
.field private final e:Lcom/facebook/resources/ui/EllipsizingTextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 590400
    new-instance v0, LX/3WY;

    invoke-direct {v0}, LX/3WY;-><init>()V

    sput-object v0, LX/3WX;->c:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 590401
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/3WX;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 590402
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 590403
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/3WX;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 590404
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 590405
    const v0, 0x7f0300e0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/facebook/attachments/angora/AngoraAttachmentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 590406
    const v0, 0x7f0d0538

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/EllipsizingTextView;

    iput-object v0, p0, LX/3WX;->e:Lcom/facebook/resources/ui/EllipsizingTextView;

    .line 590407
    return-void
.end method


# virtual methods
.method public setPreviewText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 590408
    iget-object v0, p0, LX/3WX;->e:Lcom/facebook/resources/ui/EllipsizingTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/EllipsizingTextView;->setText(Ljava/lang/CharSequence;)V

    .line 590409
    return-void
.end method
