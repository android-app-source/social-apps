.class public LX/4Le;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 684408
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 684409
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_7

    .line 684410
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 684411
    :goto_0
    return v1

    .line 684412
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_5

    .line 684413
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 684414
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 684415
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_0

    if-eqz v7, :cond_0

    .line 684416
    const-string v8, "amount_in_hundredths"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 684417
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v6, v0

    move v0, v2

    goto :goto_1

    .line 684418
    :cond_1
    const-string v8, "currency"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 684419
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 684420
    :cond_2
    const-string v8, "formatted_amount"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 684421
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 684422
    :cond_3
    const-string v8, "amount"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 684423
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 684424
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 684425
    :cond_5
    const/4 v7, 0x4

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 684426
    if-eqz v0, :cond_6

    .line 684427
    invoke-virtual {p1, v1, v6, v1}, LX/186;->a(III)V

    .line 684428
    :cond_6
    invoke-virtual {p1, v2, v5}, LX/186;->b(II)V

    .line 684429
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 684430
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 684431
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_7
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 684432
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 684433
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v0

    .line 684434
    if-eqz v0, :cond_0

    .line 684435
    const-string v1, "amount_in_hundredths"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 684436
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 684437
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 684438
    if-eqz v0, :cond_1

    .line 684439
    const-string v1, "currency"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 684440
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 684441
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 684442
    if-eqz v0, :cond_2

    .line 684443
    const-string v1, "formatted_amount"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 684444
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 684445
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 684446
    if-eqz v0, :cond_3

    .line 684447
    const-string v1, "amount"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 684448
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 684449
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 684450
    return-void
.end method
