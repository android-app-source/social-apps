.class public LX/42l;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static volatile d:LX/42l;


# instance fields
.field private final b:LX/03V;

.field private final c:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 667939
    const-class v0, LX/42l;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/42l;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/03V;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 667935
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 667936
    iput-object p1, p0, LX/42l;->b:LX/03V;

    .line 667937
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, LX/42l;->c:Landroid/util/SparseArray;

    .line 667938
    return-void
.end method

.method public static a(LX/0QB;)LX/42l;
    .locals 4

    .prologue
    .line 667962
    sget-object v0, LX/42l;->d:LX/42l;

    if-nez v0, :cond_1

    .line 667963
    const-class v1, LX/42l;

    monitor-enter v1

    .line 667964
    :try_start_0
    sget-object v0, LX/42l;->d:LX/42l;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 667965
    if-eqz v2, :cond_0

    .line 667966
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 667967
    new-instance p0, LX/42l;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-direct {p0, v3}, LX/42l;-><init>(LX/03V;)V

    .line 667968
    move-object v0, p0

    .line 667969
    sput-object v0, LX/42l;->d:LX/42l;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 667970
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 667971
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 667972
    :cond_1
    sget-object v0, LX/42l;->d:LX/42l;

    return-object v0

    .line 667973
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 667974
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/42l;Landroid/app/Activity;)V
    .locals 4

    .prologue
    .line 667975
    iget-object v0, p0, LX/42l;->b:LX/03V;

    sget-object v1, LX/42l;->a:Ljava/lang/String;

    const-string v2, "Calling Activity (%s) does not belong to a Task"

    invoke-virtual {p1}, Landroid/app/Activity;->getLocalClassName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0VG;->b(Ljava/lang/String;Ljava/lang/String;)LX/0VG;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    .line 667976
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Landroid/app/Activity;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 667951
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Landroid/app/Activity;->getTaskId()I

    move-result v0

    .line 667952
    if-ne v0, v1, :cond_2

    .line 667953
    :goto_0
    if-ne p2, v1, :cond_1

    .line 667954
    invoke-static {p0, p1}, LX/42l;->a(LX/42l;Landroid/app/Activity;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 667955
    :cond_0
    :goto_1
    monitor-exit p0

    return-void

    .line 667956
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/42l;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 667957
    if-eqz v0, :cond_0

    .line 667958
    invoke-interface {v0, p3}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 667959
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 667960
    iget-object v0, p0, LX/42l;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, p2}, Landroid/util/SparseArray;->remove(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 667961
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    move p2, v0

    goto :goto_0
.end method

.method public final declared-synchronized a(Landroid/app/Activity;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 667940
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Landroid/app/Activity;->getTaskId()I

    move-result v1

    .line 667941
    const/4 v0, -0x1

    if-ne v1, v0, :cond_0

    .line 667942
    invoke-static {p0, p1}, LX/42l;->a(LX/42l;Landroid/app/Activity;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 667943
    :goto_0
    monitor-exit p0

    return-void

    .line 667944
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/42l;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 667945
    if-nez v0, :cond_1

    .line 667946
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 667947
    :cond_1
    invoke-interface {v0, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 667948
    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 667949
    :cond_2
    iget-object v2, p0, LX/42l;->c:Landroid/util/SparseArray;

    invoke-virtual {v2, v1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 667950
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
