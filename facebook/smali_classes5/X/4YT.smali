.class public final LX/4YT;
.super LX/0ur;
.source ""


# instance fields
.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLQuestionOption;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 777453
    invoke-direct {p0}, LX/0ur;-><init>()V

    .line 777454
    instance-of v0, p0, LX/4YT;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 777455
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLQuestionOptionsConnection;)LX/4YT;
    .locals 2

    .prologue
    .line 777456
    new-instance v0, LX/4YT;

    invoke-direct {v0}, LX/4YT;-><init>()V

    .line 777457
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 777458
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLQuestionOptionsConnection;->a()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4YT;->b:LX/0Px;

    .line 777459
    invoke-static {v0, p0}, LX/0ur;->a(LX/0ur;Lcom/facebook/graphql/modelutil/BaseModel;)V

    .line 777460
    return-object v0
.end method


# virtual methods
.method public final a()Lcom/facebook/graphql/model/GraphQLQuestionOptionsConnection;
    .locals 2

    .prologue
    .line 777451
    new-instance v0, Lcom/facebook/graphql/model/GraphQLQuestionOptionsConnection;

    invoke-direct {v0, p0}, Lcom/facebook/graphql/model/GraphQLQuestionOptionsConnection;-><init>(LX/4YT;)V

    .line 777452
    return-object v0
.end method
