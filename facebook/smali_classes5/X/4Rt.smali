.class public LX/4Rt;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 710906
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 710907
    const/4 v0, 0x0

    .line 710908
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v4, :cond_5

    .line 710909
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 710910
    :goto_0
    return v1

    .line 710911
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_3

    .line 710912
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 710913
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 710914
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_0

    if-eqz v5, :cond_0

    .line 710915
    const-string v6, "status"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 710916
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;

    move-result-object v0

    move-object v4, v0

    move v0, v2

    goto :goto_1

    .line 710917
    :cond_1
    const-string v6, "subunit_id"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 710918
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 710919
    :cond_2
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 710920
    :cond_3
    const/4 v5, 0x2

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 710921
    if-eqz v0, :cond_4

    .line 710922
    invoke-virtual {p1, v1, v4}, LX/186;->a(ILjava/lang/Enum;)V

    .line 710923
    :cond_4
    invoke-virtual {p1, v2, v3}, LX/186;->b(II)V

    .line 710924
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v3, v1

    move-object v4, v0

    move v0, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 710925
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 710926
    invoke-virtual {p0, p1, v1, v1}, LX/15i;->a(IIS)S

    move-result v0

    .line 710927
    if-eqz v0, :cond_0

    .line 710928
    const-string v0, "status"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 710929
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPromotionUnitAtTopStatus;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 710930
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 710931
    if-eqz v0, :cond_1

    .line 710932
    const-string v1, "subunit_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 710933
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 710934
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 710935
    return-void
.end method
