.class public LX/3v0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3qu;


# static fields
.field private static final d:[I


# instance fields
.field public a:Ljava/lang/CharSequence;

.field public b:Landroid/graphics/drawable/Drawable;

.field public c:Landroid/view/View;

.field public final e:Landroid/content/Context;

.field public final f:Landroid/content/res/Resources;

.field private g:Z

.field public h:Z

.field private i:LX/3u7;

.field public j:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/3v3;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/3v3;",
            ">;"
        }
    .end annotation
.end field

.field private l:Z

.field private m:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/3v3;",
            ">;"
        }
    .end annotation
.end field

.field private n:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/3v3;",
            ">;"
        }
    .end annotation
.end field

.field private o:Z

.field public p:I

.field private q:Landroid/view/ContextMenu$ContextMenuInfo;

.field private r:Z

.field private s:Z

.field public t:Z

.field private u:Z

.field private v:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/3v3;",
            ">;"
        }
    .end annotation
.end field

.field public w:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/3us;",
            ">;>;"
        }
    .end annotation
.end field

.field public x:LX/3v3;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 650303
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, LX/3v0;->d:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x1
        0x4
        0x5
        0x3
        0x2
        0x0
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 650205
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 650206
    iput v0, p0, LX/3v0;->p:I

    .line 650207
    iput-boolean v0, p0, LX/3v0;->r:Z

    .line 650208
    iput-boolean v0, p0, LX/3v0;->s:Z

    .line 650209
    iput-boolean v0, p0, LX/3v0;->t:Z

    .line 650210
    iput-boolean v0, p0, LX/3v0;->u:Z

    .line 650211
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/3v0;->v:Ljava/util/ArrayList;

    .line 650212
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, LX/3v0;->w:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 650213
    iput-object p1, p0, LX/3v0;->e:Landroid/content/Context;

    .line 650214
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, LX/3v0;->f:Landroid/content/res/Resources;

    .line 650215
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/3v0;->j:Ljava/util/ArrayList;

    .line 650216
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/3v0;->k:Ljava/util/ArrayList;

    .line 650217
    iput-boolean v1, p0, LX/3v0;->l:Z

    .line 650218
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/3v0;->m:Ljava/util/ArrayList;

    .line 650219
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/3v0;->n:Ljava/util/ArrayList;

    .line 650220
    iput-boolean v1, p0, LX/3v0;->o:Z

    .line 650221
    const/4 v0, 0x1

    .line 650222
    if-eqz v1, :cond_0

    iget-object v2, p0, LX/3v0;->f:Landroid/content/res/Resources;

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->keyboard:I

    if-eq v2, v0, :cond_0

    iget-object v2, p0, LX/3v0;->f:Landroid/content/res/Resources;

    const p1, 0x7f090003

    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    iput-boolean v0, p0, LX/3v0;->h:Z

    .line 650223
    return-void

    .line 650224
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(IIIILjava/lang/CharSequence;I)LX/3v3;
    .locals 8

    .prologue
    .line 650225
    new-instance v0, LX/3v3;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move-object v6, p5

    move v7, p6

    invoke-direct/range {v0 .. v7}, LX/3v3;-><init>(LX/3v0;IIIILjava/lang/CharSequence;I)V

    return-object v0
.end method

.method private a(ILandroid/view/KeyEvent;)LX/3v3;
    .locals 12

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 650226
    iget-object v5, p0, LX/3v0;->v:Ljava/util/ArrayList;

    .line 650227
    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    .line 650228
    invoke-direct {p0, v5, p1, p2}, LX/3v0;->a(Ljava/util/List;ILandroid/view/KeyEvent;)V

    .line 650229
    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v0, v2

    .line 650230
    :cond_0
    :goto_0
    return-object v0

    .line 650231
    :cond_1
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getMetaState()I

    move-result v6

    .line 650232
    new-instance v7, Landroid/view/KeyCharacterMap$KeyData;

    invoke-direct {v7}, Landroid/view/KeyCharacterMap$KeyData;-><init>()V

    .line 650233
    invoke-virtual {p2, v7}, Landroid/view/KeyEvent;->getKeyData(Landroid/view/KeyCharacterMap$KeyData;)Z

    .line 650234
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v8

    .line 650235
    const/4 v0, 0x1

    if-ne v8, v0, :cond_2

    .line 650236
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3v3;

    goto :goto_0

    .line 650237
    :cond_2
    invoke-virtual {p0}, LX/3v0;->b()Z

    move-result v9

    move v3, v4

    .line 650238
    :goto_1
    if-ge v3, v8, :cond_7

    .line 650239
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3v3;

    .line 650240
    if-eqz v9, :cond_6

    invoke-virtual {v0}, LX/3v3;->getAlphabeticShortcut()C

    move-result v1

    .line 650241
    :goto_2
    iget-object v10, v7, Landroid/view/KeyCharacterMap$KeyData;->meta:[C

    aget-char v10, v10, v4

    if-ne v1, v10, :cond_3

    and-int/lit8 v10, v6, 0x2

    if-eqz v10, :cond_0

    :cond_3
    iget-object v10, v7, Landroid/view/KeyCharacterMap$KeyData;->meta:[C

    const/4 v11, 0x2

    aget-char v10, v10, v11

    if-ne v1, v10, :cond_4

    and-int/lit8 v10, v6, 0x2

    if-nez v10, :cond_0

    :cond_4
    if-eqz v9, :cond_5

    const/16 v10, 0x8

    if-ne v1, v10, :cond_5

    const/16 v1, 0x43

    if-eq p1, v1, :cond_0

    .line 650242
    :cond_5
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 650243
    :cond_6
    invoke-virtual {v0}, LX/3v3;->getNumericShortcut()C

    move-result v1

    goto :goto_2

    :cond_7
    move-object v0, v2

    .line 650244
    goto :goto_0
.end method

.method private a(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 7

    .prologue
    .line 650245
    invoke-static {p3}, LX/3v0;->d(I)I

    move-result v4

    .line 650246
    iget v6, p0, LX/3v0;->p:I

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v6}, LX/3v0;->a(IIIILjava/lang/CharSequence;I)LX/3v3;

    move-result-object v0

    .line 650247
    iget-object v1, p0, LX/3v0;->q:Landroid/view/ContextMenu$ContextMenuInfo;

    if-eqz v1, :cond_0

    .line 650248
    iget-object v1, p0, LX/3v0;->q:Landroid/view/ContextMenu$ContextMenuInfo;

    .line 650249
    iput-object v1, v0, LX/3v3;->v:Landroid/view/ContextMenu$ContextMenuInfo;

    .line 650250
    :cond_0
    iget-object v1, p0, LX/3v0;->j:Ljava/util/ArrayList;

    iget-object v2, p0, LX/3v0;->j:Ljava/util/ArrayList;

    .line 650251
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    move v5, v3

    :goto_0
    if-ltz v5, :cond_2

    .line 650252
    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/3v3;

    .line 650253
    iget v6, v3, LX/3v3;->d:I

    move v3, v6

    .line 650254
    if-gt v3, v4, :cond_1

    .line 650255
    add-int/lit8 v3, v5, 0x1

    .line 650256
    :goto_1
    move v2, v3

    .line 650257
    invoke-virtual {v1, v2, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 650258
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, LX/3v0;->b(Z)V

    .line 650259
    return-object v0

    .line 650260
    :cond_1
    add-int/lit8 v3, v5, -0x1

    move v5, v3

    goto :goto_0

    .line 650261
    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private a(IZ)V
    .locals 1

    .prologue
    .line 650262
    if-ltz p1, :cond_0

    iget-object v0, p0, LX/3v0;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 650263
    :cond_0
    :goto_0
    return-void

    .line 650264
    :cond_1
    iget-object v0, p0, LX/3v0;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 650265
    if-eqz p2, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/3v0;->b(Z)V

    goto :goto_0
.end method

.method public static a(LX/3v0;ILjava/lang/CharSequence;ILandroid/graphics/drawable/Drawable;Landroid/view/View;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 650266
    iget-object v0, p0, LX/3v0;->f:Landroid/content/res/Resources;

    move-object v0, v0

    .line 650267
    if-eqz p5, :cond_0

    .line 650268
    iput-object p5, p0, LX/3v0;->c:Landroid/view/View;

    .line 650269
    iput-object v1, p0, LX/3v0;->a:Ljava/lang/CharSequence;

    .line 650270
    iput-object v1, p0, LX/3v0;->b:Landroid/graphics/drawable/Drawable;

    .line 650271
    :goto_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/3v0;->b(Z)V

    .line 650272
    return-void

    .line 650273
    :cond_0
    if-lez p1, :cond_3

    .line 650274
    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, LX/3v0;->a:Ljava/lang/CharSequence;

    .line 650275
    :cond_1
    :goto_1
    if-lez p3, :cond_4

    .line 650276
    iget-object v0, p0, LX/3v0;->e:Landroid/content/Context;

    move-object v0, v0

    .line 650277
    invoke-static {v0, p3}, LX/1ED;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, LX/3v0;->b:Landroid/graphics/drawable/Drawable;

    .line 650278
    :cond_2
    :goto_2
    iput-object v1, p0, LX/3v0;->c:Landroid/view/View;

    goto :goto_0

    .line 650279
    :cond_3
    if-eqz p2, :cond_1

    .line 650280
    iput-object p2, p0, LX/3v0;->a:Ljava/lang/CharSequence;

    goto :goto_1

    .line 650281
    :cond_4
    if-eqz p4, :cond_2

    .line 650282
    iput-object p4, p0, LX/3v0;->b:Landroid/graphics/drawable/Drawable;

    goto :goto_2
.end method

.method private a(Ljava/util/List;ILandroid/view/KeyEvent;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/3v3;",
            ">;I",
            "Landroid/view/KeyEvent;",
            ")V"
        }
    .end annotation

    .prologue
    const/16 v10, 0x43

    const/4 v3, 0x0

    .line 650283
    invoke-virtual {p0}, LX/3v0;->b()Z

    move-result v4

    .line 650284
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getMetaState()I

    move-result v5

    .line 650285
    new-instance v6, Landroid/view/KeyCharacterMap$KeyData;

    invoke-direct {v6}, Landroid/view/KeyCharacterMap$KeyData;-><init>()V

    .line 650286
    invoke-virtual {p3, v6}, Landroid/view/KeyEvent;->getKeyData(Landroid/view/KeyCharacterMap$KeyData;)Z

    move-result v0

    .line 650287
    if-nez v0, :cond_1

    if-eq p2, v10, :cond_1

    .line 650288
    :cond_0
    return-void

    .line 650289
    :cond_1
    iget-object v0, p0, LX/3v0;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v7

    move v2, v3

    .line 650290
    :goto_0
    if-ge v2, v7, :cond_0

    .line 650291
    iget-object v0, p0, LX/3v0;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3v3;

    .line 650292
    invoke-virtual {v0}, LX/3v3;->hasSubMenu()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 650293
    invoke-virtual {v0}, LX/3v3;->getSubMenu()Landroid/view/SubMenu;

    move-result-object v1

    check-cast v1, LX/3v0;

    invoke-direct {v1, p1, p2, p3}, LX/3v0;->a(Ljava/util/List;ILandroid/view/KeyEvent;)V

    .line 650294
    :cond_2
    if-eqz v4, :cond_5

    invoke-virtual {v0}, LX/3v3;->getAlphabeticShortcut()C

    move-result v1

    .line 650295
    :goto_1
    and-int/lit8 v8, v5, 0x5

    if-nez v8, :cond_4

    if-eqz v1, :cond_4

    iget-object v8, v6, Landroid/view/KeyCharacterMap$KeyData;->meta:[C

    aget-char v8, v8, v3

    if-eq v1, v8, :cond_3

    iget-object v8, v6, Landroid/view/KeyCharacterMap$KeyData;->meta:[C

    const/4 v9, 0x2

    aget-char v8, v8, v9

    if-eq v1, v8, :cond_3

    if-eqz v4, :cond_4

    const/16 v8, 0x8

    if-ne v1, v8, :cond_4

    if-ne p2, v10, :cond_4

    :cond_3
    invoke-virtual {v0}, LX/3v3;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 650296
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 650297
    :cond_4
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 650298
    :cond_5
    invoke-virtual {v0}, LX/3v3;->getNumericShortcut()C

    move-result v1

    goto :goto_1
.end method

.method private static d(I)I
    .locals 2

    .prologue
    .line 650299
    const/high16 v0, -0x10000

    and-int/2addr v0, p0

    shr-int/lit8 v0, v0, 0x10

    .line 650300
    if-ltz v0, :cond_0

    sget-object v1, LX/3v0;->d:[I

    array-length v1, v1

    if-lt v0, v1, :cond_1

    .line 650301
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "order does not contain a valid category."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 650302
    :cond_1
    sget-object v1, LX/3v0;->d:[I

    aget v0, v1, v0

    shl-int/lit8 v0, v0, 0x10

    const v1, 0xffff

    and-int/2addr v1, p0

    or-int/2addr v0, v1

    return v0
.end method


# virtual methods
.method public final a(Landroid/graphics/drawable/Drawable;)LX/3v0;
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 650180
    move-object v0, p0

    move v3, v1

    move-object v4, p1

    move-object v5, v2

    invoke-static/range {v0 .. v5}, LX/3v0;->a(LX/3v0;ILjava/lang/CharSequence;ILandroid/graphics/drawable/Drawable;Landroid/view/View;)V

    .line 650181
    return-object p0
.end method

.method public final a(Ljava/lang/CharSequence;)LX/3v0;
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 650304
    move-object v0, p0

    move-object v2, p1

    move v3, v1

    move-object v5, v4

    invoke-static/range {v0 .. v5}, LX/3v0;->a(LX/3v0;ILjava/lang/CharSequence;ILandroid/graphics/drawable/Drawable;Landroid/view/View;)V

    .line 650305
    return-object p0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 650306
    const-string v0, "android:menu:actionviewstates"

    return-object v0
.end method

.method public a(LX/3u7;)V
    .locals 0

    .prologue
    .line 650307
    iput-object p1, p0, LX/3v0;->i:LX/3u7;

    .line 650308
    return-void
.end method

.method public final a(LX/3us;)V
    .locals 1

    .prologue
    .line 650309
    iget-object v0, p0, LX/3v0;->e:Landroid/content/Context;

    invoke-virtual {p0, p1, v0}, LX/3v0;->a(LX/3us;Landroid/content/Context;)V

    .line 650310
    return-void
.end method

.method public final a(LX/3us;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 650311
    iget-object v0, p0, LX/3v0;->w:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 650312
    invoke-interface {p1, p2, p0}, LX/3us;->a(Landroid/content/Context;LX/3v0;)V

    .line 650313
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/3v0;->o:Z

    .line 650314
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    .line 650414
    const/4 v1, 0x0

    .line 650415
    invoke-virtual {p0}, LX/3v0;->size()I

    move-result v3

    .line 650416
    const/4 v0, 0x0

    move v2, v0

    move-object v0, v1

    :goto_0
    if-ge v2, v3, :cond_3

    .line 650417
    invoke-virtual {p0, v2}, LX/3v0;->getItem(I)Landroid/view/MenuItem;

    move-result-object v4

    .line 650418
    invoke-static {v4}, LX/3rl;->a(Landroid/view/MenuItem;)Landroid/view/View;

    move-result-object v1

    .line 650419
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v5

    const/4 v6, -0x1

    if-eq v5, v6, :cond_1

    .line 650420
    if-nez v0, :cond_0

    .line 650421
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    .line 650422
    :cond_0
    invoke-virtual {v1, v0}, Landroid/view/View;->saveHierarchyState(Landroid/util/SparseArray;)V

    .line 650423
    invoke-static {v4}, LX/3rl;->c(Landroid/view/MenuItem;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 650424
    const-string v1, "android:menu:expandedactionview"

    invoke-interface {v4}, Landroid/view/MenuItem;->getItemId()I

    move-result v5

    invoke-virtual {p1, v1, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_1
    move-object v1, v0

    .line 650425
    invoke-interface {v4}, Landroid/view/MenuItem;->hasSubMenu()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 650426
    invoke-interface {v4}, Landroid/view/MenuItem;->getSubMenu()Landroid/view/SubMenu;

    move-result-object v0

    check-cast v0, LX/3vG;

    .line 650427
    invoke-virtual {v0, p1}, LX/3v0;->a(Landroid/os/Bundle;)V

    .line 650428
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move-object v0, v1

    goto :goto_0

    .line 650429
    :cond_3
    if-eqz v0, :cond_4

    .line 650430
    invoke-virtual {p0}, LX/3v0;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putSparseParcelableArray(Ljava/lang/String;Landroid/util/SparseArray;)V

    .line 650431
    :cond_4
    return-void
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 650315
    iget-boolean v0, p0, LX/3v0;->u:Z

    if-eqz v0, :cond_0

    .line 650316
    :goto_0
    return-void

    .line 650317
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/3v0;->u:Z

    .line 650318
    iget-object v0, p0, LX/3v0;->w:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 650319
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3us;

    .line 650320
    if-nez v1, :cond_1

    .line 650321
    iget-object v1, p0, LX/3v0;->w:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    .line 650322
    :cond_1
    invoke-interface {v1, p0, p1}, LX/3us;->a(LX/3v0;Z)V

    goto :goto_1

    .line 650323
    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/3v0;->u:Z

    goto :goto_0
.end method

.method public a(LX/3v0;Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 650413
    iget-object v0, p0, LX/3v0;->i:LX/3u7;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3v0;->i:LX/3u7;

    invoke-interface {v0, p1, p2}, LX/3u7;->a(LX/3v0;Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(LX/3v3;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 650401
    iget-object v1, p0, LX/3v0;->w:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 650402
    :cond_0
    :goto_0
    return v0

    .line 650403
    :cond_1
    invoke-virtual {p0}, LX/3v0;->f()V

    .line 650404
    iget-object v1, p0, LX/3v0;->w:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 650405
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3us;

    .line 650406
    if-nez v1, :cond_2

    .line 650407
    iget-object v1, p0, LX/3v0;->w:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    .line 650408
    :cond_2
    invoke-interface {v1, p1}, LX/3us;->b(LX/3v3;)Z

    move-result v0

    if-nez v0, :cond_4

    move v2, v0

    .line 650409
    goto :goto_1

    :cond_3
    move v0, v2

    .line 650410
    :cond_4
    invoke-virtual {p0}, LX/3v0;->g()V

    .line 650411
    if-eqz v0, :cond_0

    .line 650412
    iput-object p1, p0, LX/3v0;->x:LX/3v3;

    goto :goto_0
.end method

.method public final a(Landroid/view/MenuItem;I)Z
    .locals 1

    .prologue
    .line 650400
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, LX/3v0;->a(Landroid/view/MenuItem;LX/3us;I)Z

    move-result v0

    return v0
.end method

.method public final a(Landroid/view/MenuItem;LX/3us;I)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 650351
    check-cast p1, LX/3v3;

    .line 650352
    if-eqz p1, :cond_0

    invoke-virtual {p1}, LX/3v3;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_1

    .line 650353
    :cond_0
    :goto_0
    return v0

    .line 650354
    :cond_1
    const/4 v1, 0x1

    .line 650355
    iget-object v3, p1, LX/3v3;->o:Landroid/view/MenuItem$OnMenuItemClickListener;

    if-eqz v3, :cond_a

    iget-object v3, p1, LX/3v3;->o:Landroid/view/MenuItem$OnMenuItemClickListener;

    invoke-interface {v3, p1}, Landroid/view/MenuItem$OnMenuItemClickListener;->onMenuItemClick(Landroid/view/MenuItem;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 650356
    :cond_2
    :goto_1
    move v3, v1

    .line 650357
    invoke-virtual {p1}, LX/3v3;->a()LX/3rR;

    move-result-object v4

    .line 650358
    if-eqz v4, :cond_3

    invoke-virtual {v4}, LX/3rR;->e()Z

    move-result v1

    if-eqz v1, :cond_3

    move v1, v2

    .line 650359
    :goto_2
    invoke-virtual {p1}, LX/3v3;->n()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 650360
    invoke-virtual {p1}, LX/3v3;->expandActionView()Z

    move-result v0

    or-int/2addr v0, v3

    .line 650361
    if-eqz v0, :cond_0

    invoke-virtual {p0, v2}, LX/3v0;->a(Z)V

    goto :goto_0

    :cond_3
    move v1, v0

    .line 650362
    goto :goto_2

    .line 650363
    :cond_4
    invoke-virtual {p1}, LX/3v3;->hasSubMenu()Z

    move-result v5

    if-nez v5, :cond_5

    if-eqz v1, :cond_8

    .line 650364
    :cond_5
    invoke-virtual {p0, v0}, LX/3v0;->a(Z)V

    .line 650365
    invoke-virtual {p1}, LX/3v3;->hasSubMenu()Z

    move-result v0

    if-nez v0, :cond_6

    .line 650366
    new-instance v0, LX/3vG;

    .line 650367
    iget-object v5, p0, LX/3v0;->e:Landroid/content/Context;

    move-object v5, v5

    .line 650368
    invoke-direct {v0, v5, p0, p1}, LX/3vG;-><init>(Landroid/content/Context;LX/3v0;LX/3v3;)V

    invoke-virtual {p1, v0}, LX/3v3;->a(LX/3vG;)V

    .line 650369
    :cond_6
    invoke-virtual {p1}, LX/3v3;->getSubMenu()Landroid/view/SubMenu;

    move-result-object v0

    check-cast v0, LX/3vG;

    .line 650370
    if-eqz v1, :cond_7

    .line 650371
    invoke-virtual {v4, v0}, LX/3rR;->a(Landroid/view/SubMenu;)V

    .line 650372
    :cond_7
    const/4 v1, 0x0

    .line 650373
    iget-object v4, p0, LX/3v0;->w:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v4}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_e

    .line 650374
    :goto_3
    move v0, v1

    .line 650375
    or-int/2addr v0, v3

    .line 650376
    if-nez v0, :cond_0

    invoke-virtual {p0, v2}, LX/3v0;->a(Z)V

    goto :goto_0

    .line 650377
    :cond_8
    and-int/lit8 v0, p3, 0x1

    if-nez v0, :cond_9

    .line 650378
    invoke-virtual {p0, v2}, LX/3v0;->a(Z)V

    :cond_9
    move v0, v3

    goto :goto_0

    .line 650379
    :cond_a
    iget-object v3, p1, LX/3v3;->l:LX/3v0;

    iget-object v4, p1, LX/3v3;->l:LX/3v0;

    invoke-virtual {v4}, LX/3v0;->q()LX/3v0;

    move-result-object v4

    invoke-virtual {v3, v4, p1}, LX/3v0;->a(LX/3v0;Landroid/view/MenuItem;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 650380
    iget-object v3, p1, LX/3v3;->n:Ljava/lang/Runnable;

    if-eqz v3, :cond_b

    .line 650381
    iget-object v3, p1, LX/3v3;->n:Ljava/lang/Runnable;

    invoke-interface {v3}, Ljava/lang/Runnable;->run()V

    goto :goto_1

    .line 650382
    :cond_b
    iget-object v3, p1, LX/3v3;->g:Landroid/content/Intent;

    if-eqz v3, :cond_c

    .line 650383
    :try_start_0
    iget-object v3, p1, LX/3v3;->l:LX/3v0;

    .line 650384
    iget-object v4, v3, LX/3v0;->e:Landroid/content/Context;

    move-object v3, v4

    .line 650385
    iget-object v4, p1, LX/3v3;->g:Landroid/content/Intent;

    invoke-virtual {v3, v4}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    .line 650386
    :catch_0
    move-exception v3

    .line 650387
    const-string v4, "MenuItemImpl"

    const-string v5, "Can\'t find activity to handle intent; ignoring"

    invoke-static {v4, v5, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 650388
    :cond_c
    iget-object v3, p1, LX/3v3;->s:LX/3rR;

    if-eqz v3, :cond_d

    iget-object v3, p1, LX/3v3;->s:LX/3rR;

    invoke-virtual {v3}, LX/3rR;->d()Z

    move-result v3

    if-nez v3, :cond_2

    .line 650389
    :cond_d
    const/4 v1, 0x0

    goto/16 :goto_1

    .line 650390
    :cond_e
    if-eqz p2, :cond_f

    .line 650391
    invoke-interface {p2, v0}, LX/3us;->a(LX/3vG;)Z

    move-result v1

    .line 650392
    :cond_f
    iget-object v4, p0, LX/3v0;->w:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v4}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    move v5, v1

    :goto_4
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_11

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    .line 650393
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/3us;

    .line 650394
    if-nez v4, :cond_10

    .line 650395
    iget-object v4, p0, LX/3v0;->w:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v4, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_4

    .line 650396
    :cond_10
    if-nez v5, :cond_12

    .line 650397
    invoke-interface {v4, v0}, LX/3us;->a(LX/3vG;)Z

    move-result v1

    :goto_5
    move v5, v1

    .line 650398
    goto :goto_4

    :cond_11
    move v1, v5

    .line 650399
    goto :goto_3

    :cond_12
    move v1, v5

    goto :goto_5
.end method

.method public final add(I)Landroid/view/MenuItem;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 650350
    iget-object v0, p0, LX/3v0;->f:Landroid/content/res/Resources;

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v1, v1, v1, v0}, LX/3v0;->a(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public final add(IIII)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 650349
    iget-object v0, p0, LX/3v0;->f:Landroid/content/res/Resources;

    invoke-virtual {v0, p4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, p2, p3, v0}, LX/3v0;->a(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public final add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 650348
    invoke-direct {p0, p1, p2, p3, p4}, LX/3v0;->a(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public final add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 650347
    invoke-direct {p0, v0, v0, v0, p1}, LX/3v0;->a(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public final addIntentOptions(IIILandroid/content/ComponentName;[Landroid/content/Intent;Landroid/content/Intent;I[Landroid/view/MenuItem;)I
    .locals 9

    .prologue
    .line 650331
    iget-object v0, p0, LX/3v0;->e:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 650332
    const/4 v0, 0x0

    invoke-virtual {v4, p4, p5, p6, v0}, Landroid/content/pm/PackageManager;->queryIntentActivityOptions(Landroid/content/ComponentName;[Landroid/content/Intent;Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v5

    .line 650333
    if-eqz v5, :cond_2

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    move v3, v0

    .line 650334
    :goto_0
    and-int/lit8 v0, p7, 0x1

    if-nez v0, :cond_0

    .line 650335
    invoke-virtual {p0, p1}, LX/3v0;->removeGroup(I)V

    .line 650336
    :cond_0
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v3, :cond_4

    .line 650337
    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 650338
    new-instance v6, Landroid/content/Intent;

    iget v1, v0, Landroid/content/pm/ResolveInfo;->specificIndex:I

    if-gez v1, :cond_3

    move-object v1, p6

    :goto_2
    invoke-direct {v6, v1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 650339
    new-instance v1, Landroid/content/ComponentName;

    iget-object v7, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v7, v7, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v7, v7, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    iget-object v8, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v8, v8, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    invoke-direct {v1, v7, v8}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 650340
    invoke-virtual {v0, v4}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p0, p1, p2, p3, v1}, LX/3v0;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v1

    invoke-virtual {v0, v4}, Landroid/content/pm/ResolveInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-interface {v1, v7}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v6}, Landroid/view/MenuItem;->setIntent(Landroid/content/Intent;)Landroid/view/MenuItem;

    move-result-object v1

    .line 650341
    if-eqz p8, :cond_1

    iget v6, v0, Landroid/content/pm/ResolveInfo;->specificIndex:I

    if-ltz v6, :cond_1

    .line 650342
    iget v0, v0, Landroid/content/pm/ResolveInfo;->specificIndex:I

    aput-object v1, p8, v0

    .line 650343
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 650344
    :cond_2
    const/4 v0, 0x0

    move v3, v0

    goto :goto_0

    .line 650345
    :cond_3
    iget v1, v0, Landroid/content/pm/ResolveInfo;->specificIndex:I

    aget-object v1, p5, v1

    goto :goto_2

    .line 650346
    :cond_4
    return v3
.end method

.method public final addSubMenu(I)Landroid/view/SubMenu;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 650330
    iget-object v0, p0, LX/3v0;->f:Landroid/content/res/Resources;

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v1, v1, v1, v0}, LX/3v0;->addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;

    move-result-object v0

    return-object v0
.end method

.method public final addSubMenu(IIII)Landroid/view/SubMenu;
    .locals 1

    .prologue
    .line 650329
    iget-object v0, p0, LX/3v0;->f:Landroid/content/res/Resources;

    invoke-virtual {v0, p4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, p2, p3, v0}, LX/3v0;->addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;

    move-result-object v0

    return-object v0
.end method

.method public final addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;
    .locals 3

    .prologue
    .line 650325
    invoke-direct {p0, p1, p2, p3, p4}, LX/3v0;->a(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    check-cast v0, LX/3v3;

    .line 650326
    new-instance v1, LX/3vG;

    iget-object v2, p0, LX/3v0;->e:Landroid/content/Context;

    invoke-direct {v1, v2, p0, v0}, LX/3vG;-><init>(Landroid/content/Context;LX/3v0;LX/3v3;)V

    .line 650327
    invoke-virtual {v0, v1}, LX/3v3;->a(LX/3vG;)V

    .line 650328
    return-object v1
.end method

.method public final addSubMenu(Ljava/lang/CharSequence;)Landroid/view/SubMenu;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 650324
    invoke-virtual {p0, v0, v0, v0, p1}, LX/3v0;->addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/3us;)V
    .locals 3

    .prologue
    .line 650182
    iget-object v0, p0, LX/3v0;->w:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 650183
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3us;

    .line 650184
    if-eqz v1, :cond_1

    if-ne v1, p1, :cond_0

    .line 650185
    :cond_1
    iget-object v1, p0, LX/3v0;->w:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 650186
    :cond_2
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    .line 650187
    if-nez p1, :cond_1

    .line 650188
    :cond_0
    :goto_0
    return-void

    .line 650189
    :cond_1
    invoke-virtual {p0}, LX/3v0;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSparseParcelableArray(Ljava/lang/String;)Landroid/util/SparseArray;

    move-result-object v2

    .line 650190
    invoke-virtual {p0}, LX/3v0;->size()I

    move-result v3

    .line 650191
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_4

    .line 650192
    invoke-virtual {p0, v1}, LX/3v0;->getItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 650193
    invoke-static {v0}, LX/3rl;->a(Landroid/view/MenuItem;)Landroid/view/View;

    move-result-object v4

    .line 650194
    if-eqz v4, :cond_2

    invoke-virtual {v4}, Landroid/view/View;->getId()I

    move-result v5

    const/4 v6, -0x1

    if-eq v5, v6, :cond_2

    .line 650195
    invoke-virtual {v4, v2}, Landroid/view/View;->restoreHierarchyState(Landroid/util/SparseArray;)V

    .line 650196
    :cond_2
    invoke-interface {v0}, Landroid/view/MenuItem;->hasSubMenu()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 650197
    invoke-interface {v0}, Landroid/view/MenuItem;->getSubMenu()Landroid/view/SubMenu;

    move-result-object v0

    check-cast v0, LX/3vG;

    .line 650198
    invoke-virtual {v0, p1}, LX/3v0;->b(Landroid/os/Bundle;)V

    .line 650199
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 650200
    :cond_4
    const-string v0, "android:menu:expandedactionview"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 650201
    if-lez v0, :cond_0

    .line 650202
    invoke-virtual {p0, v0}, LX/3v0;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 650203
    if-eqz v0, :cond_0

    .line 650204
    invoke-static {v0}, LX/3rl;->b(Landroid/view/MenuItem;)Z

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 650065
    iget-boolean v0, p0, LX/3v0;->r:Z

    if-nez v0, :cond_1

    .line 650066
    if-eqz p1, :cond_0

    .line 650067
    iput-boolean v1, p0, LX/3v0;->l:Z

    .line 650068
    iput-boolean v1, p0, LX/3v0;->o:Z

    .line 650069
    :cond_0
    iget-object v0, p0, LX/3v0;->w:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 650070
    :goto_0
    return-void

    .line 650071
    :cond_1
    iput-boolean v1, p0, LX/3v0;->s:Z

    goto :goto_0

    .line 650072
    :cond_2
    invoke-virtual {p0}, LX/3v0;->f()V

    .line 650073
    iget-object v0, p0, LX/3v0;->w:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 650074
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3us;

    .line 650075
    if-nez v1, :cond_3

    .line 650076
    iget-object v1, p0, LX/3v0;->w:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    .line 650077
    :cond_3
    invoke-interface {v1, p1}, LX/3us;->b(Z)V

    goto :goto_1

    .line 650078
    :cond_4
    invoke-virtual {p0}, LX/3v0;->g()V

    goto :goto_0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 650064
    iget-boolean v0, p0, LX/3v0;->g:Z

    return v0
.end method

.method public b(LX/3v3;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 650052
    iget-object v1, p0, LX/3v0;->w:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/3v0;->x:LX/3v3;

    if-eq v1, p1, :cond_1

    .line 650053
    :cond_0
    :goto_0
    return v0

    .line 650054
    :cond_1
    invoke-virtual {p0}, LX/3v0;->f()V

    .line 650055
    iget-object v1, p0, LX/3v0;->w:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 650056
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3us;

    .line 650057
    if-nez v1, :cond_2

    .line 650058
    iget-object v1, p0, LX/3v0;->w:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    .line 650059
    :cond_2
    invoke-interface {v1, p1}, LX/3us;->c(LX/3v3;)Z

    move-result v0

    if-nez v0, :cond_4

    move v2, v0

    .line 650060
    goto :goto_1

    :cond_3
    move v0, v2

    .line 650061
    :cond_4
    invoke-virtual {p0}, LX/3v0;->g()V

    .line 650062
    if-eqz v0, :cond_0

    .line 650063
    const/4 v1, 0x0

    iput-object v1, p0, LX/3v0;->x:LX/3v3;

    goto :goto_0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 650051
    iget-boolean v0, p0, LX/3v0;->h:Z

    return v0
.end method

.method public final clear()V
    .locals 1

    .prologue
    .line 650046
    iget-object v0, p0, LX/3v0;->x:LX/3v3;

    if-eqz v0, :cond_0

    .line 650047
    iget-object v0, p0, LX/3v0;->x:LX/3v3;

    invoke-virtual {p0, v0}, LX/3v0;->b(LX/3v3;)Z

    .line 650048
    :cond_0
    iget-object v0, p0, LX/3v0;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 650049
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/3v0;->b(Z)V

    .line 650050
    return-void
.end method

.method public final clearHeader()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 650002
    iput-object v0, p0, LX/3v0;->b:Landroid/graphics/drawable/Drawable;

    .line 650003
    iput-object v0, p0, LX/3v0;->a:Ljava/lang/CharSequence;

    .line 650004
    iput-object v0, p0, LX/3v0;->c:Landroid/view/View;

    .line 650005
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/3v0;->b(Z)V

    .line 650006
    return-void
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 650044
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/3v0;->a(Z)V

    .line 650045
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 650041
    iget-object v0, p0, LX/3v0;->i:LX/3u7;

    if-eqz v0, :cond_0

    .line 650042
    iget-object v0, p0, LX/3v0;->i:LX/3u7;

    invoke-interface {v0, p0}, LX/3u7;->b_(LX/3v0;)V

    .line 650043
    :cond_0
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 650037
    iget-boolean v0, p0, LX/3v0;->r:Z

    if-nez v0, :cond_0

    .line 650038
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/3v0;->r:Z

    .line 650039
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/3v0;->s:Z

    .line 650040
    :cond_0
    return-void
.end method

.method public final findItem(I)Landroid/view/MenuItem;
    .locals 4

    .prologue
    .line 650027
    invoke-virtual {p0}, LX/3v0;->size()I

    move-result v2

    .line 650028
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_3

    .line 650029
    iget-object v0, p0, LX/3v0;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3v3;

    .line 650030
    invoke-virtual {v0}, LX/3v3;->getItemId()I

    move-result v3

    if-ne v3, p1, :cond_1

    .line 650031
    :cond_0
    :goto_1
    return-object v0

    .line 650032
    :cond_1
    invoke-virtual {v0}, LX/3v3;->hasSubMenu()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 650033
    invoke-virtual {v0}, LX/3v3;->getSubMenu()Landroid/view/SubMenu;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/view/SubMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 650034
    if-nez v0, :cond_0

    .line 650035
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 650036
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final g()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 650022
    iput-boolean v1, p0, LX/3v0;->r:Z

    .line 650023
    iget-boolean v0, p0, LX/3v0;->s:Z

    if-eqz v0, :cond_0

    .line 650024
    iput-boolean v1, p0, LX/3v0;->s:Z

    .line 650025
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/3v0;->b(Z)V

    .line 650026
    :cond_0
    return-void
.end method

.method public final getItem(I)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 650021
    iget-object v0, p0, LX/3v0;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/MenuItem;

    return-object v0
.end method

.method public final h()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 650018
    iput-boolean v0, p0, LX/3v0;->l:Z

    .line 650019
    invoke-virtual {p0, v0}, LX/3v0;->b(Z)V

    .line 650020
    return-void
.end method

.method public final hasVisibleItems()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 650010
    invoke-virtual {p0}, LX/3v0;->size()I

    move-result v3

    move v2, v1

    .line 650011
    :goto_0
    if-ge v2, v3, :cond_1

    .line 650012
    iget-object v0, p0, LX/3v0;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3v3;

    .line 650013
    invoke-virtual {v0}, LX/3v3;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 650014
    const/4 v0, 0x1

    .line 650015
    :goto_1
    return v0

    .line 650016
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 650017
    goto :goto_1
.end method

.method public final i()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 650007
    iput-boolean v0, p0, LX/3v0;->o:Z

    .line 650008
    invoke-virtual {p0, v0}, LX/3v0;->b(Z)V

    .line 650009
    return-void
.end method

.method public final isShortcutKey(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 650079
    invoke-direct {p0, p1, p2}, LX/3v0;->a(ILandroid/view/KeyEvent;)LX/3v3;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "LX/3v3;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 650080
    iget-boolean v0, p0, LX/3v0;->l:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/3v0;->k:Ljava/util/ArrayList;

    .line 650081
    :goto_0
    return-object v0

    .line 650082
    :cond_0
    iget-object v0, p0, LX/3v0;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 650083
    iget-object v0, p0, LX/3v0;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v2

    .line 650084
    :goto_1
    if-ge v1, v3, :cond_2

    .line 650085
    iget-object v0, p0, LX/3v0;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3v3;

    .line 650086
    invoke-virtual {v0}, LX/3v3;->isVisible()Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, LX/3v0;->k:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 650087
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 650088
    :cond_2
    iput-boolean v2, p0, LX/3v0;->l:Z

    .line 650089
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/3v0;->o:Z

    .line 650090
    iget-object v0, p0, LX/3v0;->k:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method public final k()V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 650091
    invoke-virtual {p0}, LX/3v0;->j()Ljava/util/ArrayList;

    move-result-object v4

    .line 650092
    iget-boolean v0, p0, LX/3v0;->o:Z

    if-nez v0, :cond_0

    .line 650093
    :goto_0
    return-void

    .line 650094
    :cond_0
    iget-object v0, p0, LX/3v0;->w:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v2, v3

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 650095
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3us;

    .line 650096
    if-nez v1, :cond_1

    .line 650097
    iget-object v1, p0, LX/3v0;->w:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    .line 650098
    :cond_1
    invoke-interface {v1}, LX/3us;->b()Z

    move-result v0

    or-int/2addr v0, v2

    move v2, v0

    .line 650099
    goto :goto_1

    .line 650100
    :cond_2
    if-eqz v2, :cond_4

    .line 650101
    iget-object v0, p0, LX/3v0;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 650102
    iget-object v0, p0, LX/3v0;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 650103
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v1, v3

    .line 650104
    :goto_2
    if-ge v1, v2, :cond_5

    .line 650105
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3v3;

    .line 650106
    invoke-virtual {v0}, LX/3v3;->j()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 650107
    iget-object v5, p0, LX/3v0;->m:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 650108
    :goto_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 650109
    :cond_3
    iget-object v5, p0, LX/3v0;->n:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 650110
    :cond_4
    iget-object v0, p0, LX/3v0;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 650111
    iget-object v0, p0, LX/3v0;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 650112
    iget-object v0, p0, LX/3v0;->n:Ljava/util/ArrayList;

    invoke-virtual {p0}, LX/3v0;->j()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 650113
    :cond_5
    iput-boolean v3, p0, LX/3v0;->o:Z

    goto :goto_0
.end method

.method public final l()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "LX/3v3;",
            ">;"
        }
    .end annotation

    .prologue
    .line 650114
    invoke-virtual {p0}, LX/3v0;->k()V

    .line 650115
    iget-object v0, p0, LX/3v0;->m:Ljava/util/ArrayList;

    return-object v0
.end method

.method public final m()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "LX/3v3;",
            ">;"
        }
    .end annotation

    .prologue
    .line 650116
    invoke-virtual {p0}, LX/3v0;->k()V

    .line 650117
    iget-object v0, p0, LX/3v0;->n:Ljava/util/ArrayList;

    return-object v0
.end method

.method public final performIdentifierAction(II)Z
    .locals 1

    .prologue
    .line 650118
    invoke-virtual {p0, p1}, LX/3v0;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, LX/3v0;->a(Landroid/view/MenuItem;I)Z

    move-result v0

    return v0
.end method

.method public final performShortcut(ILandroid/view/KeyEvent;I)Z
    .locals 2

    .prologue
    .line 650119
    invoke-direct {p0, p1, p2}, LX/3v0;->a(ILandroid/view/KeyEvent;)LX/3v3;

    move-result-object v1

    .line 650120
    const/4 v0, 0x0

    .line 650121
    if-eqz v1, :cond_0

    .line 650122
    invoke-virtual {p0, v1, p3}, LX/3v0;->a(Landroid/view/MenuItem;I)Z

    move-result v0

    .line 650123
    :cond_0
    and-int/lit8 v1, p3, 0x2

    if-eqz v1, :cond_1

    .line 650124
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, LX/3v0;->a(Z)V

    .line 650125
    :cond_1
    return v0
.end method

.method public q()LX/3v0;
    .locals 0

    .prologue
    .line 650126
    return-object p0
.end method

.method public final removeGroup(I)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 650127
    const/4 v0, 0x0

    .line 650128
    invoke-virtual {p0}, LX/3v0;->size()I

    move-result v4

    .line 650129
    if-gez v0, :cond_0

    .line 650130
    const/4 v0, 0x0

    :cond_0
    move v3, v0

    .line 650131
    :goto_0
    if-ge v3, v4, :cond_4

    .line 650132
    iget-object v2, p0, LX/3v0;->j:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/3v3;

    .line 650133
    invoke-virtual {v2}, LX/3v3;->getGroupId()I

    move-result v2

    if-ne v2, p1, :cond_3

    move v2, v3

    .line 650134
    :goto_1
    move v0, v2

    .line 650135
    move v3, v0

    .line 650136
    if-ltz v3, :cond_2

    .line 650137
    iget-object v0, p0, LX/3v0;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    sub-int v4, v0, v3

    move v0, v1

    .line 650138
    :goto_2
    add-int/lit8 v2, v0, 0x1

    if-ge v0, v4, :cond_1

    iget-object v0, p0, LX/3v0;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3v3;

    invoke-virtual {v0}, LX/3v3;->getGroupId()I

    move-result v0

    if-ne v0, p1, :cond_1

    .line 650139
    invoke-direct {p0, v3, v1}, LX/3v0;->a(IZ)V

    move v0, v2

    goto :goto_2

    .line 650140
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/3v0;->b(Z)V

    .line 650141
    :cond_2
    return-void

    .line 650142
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 650143
    :cond_4
    const/4 v2, -0x1

    goto :goto_1
.end method

.method public final removeItem(I)V
    .locals 3

    .prologue
    .line 650144
    invoke-virtual {p0}, LX/3v0;->size()I

    move-result v2

    .line 650145
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 650146
    iget-object v0, p0, LX/3v0;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3v3;

    .line 650147
    invoke-virtual {v0}, LX/3v3;->getItemId()I

    move-result v0

    if-ne v0, p1, :cond_0

    move v0, v1

    .line 650148
    :goto_1
    move v0, v0

    .line 650149
    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, LX/3v0;->a(IZ)V

    .line 650150
    return-void

    .line 650151
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 650152
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public final setGroupCheckable(IZZ)V
    .locals 4

    .prologue
    .line 650153
    iget-object v0, p0, LX/3v0;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 650154
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 650155
    iget-object v0, p0, LX/3v0;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3v3;

    .line 650156
    invoke-virtual {v0}, LX/3v3;->getGroupId()I

    move-result v3

    if-ne v3, p1, :cond_0

    .line 650157
    invoke-virtual {v0, p3}, LX/3v3;->a(Z)V

    .line 650158
    invoke-virtual {v0, p2}, LX/3v3;->setCheckable(Z)Landroid/view/MenuItem;

    .line 650159
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 650160
    :cond_1
    return-void
.end method

.method public final setGroupEnabled(IZ)V
    .locals 4

    .prologue
    .line 650161
    iget-object v0, p0, LX/3v0;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 650162
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 650163
    iget-object v0, p0, LX/3v0;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3v3;

    .line 650164
    invoke-virtual {v0}, LX/3v3;->getGroupId()I

    move-result v3

    if-ne v3, p1, :cond_0

    .line 650165
    invoke-virtual {v0, p2}, LX/3v3;->setEnabled(Z)Landroid/view/MenuItem;

    .line 650166
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 650167
    :cond_1
    return-void
.end method

.method public final setGroupVisible(IZ)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 650168
    iget-object v2, p0, LX/3v0;->j:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v3, v0

    move v2, v0

    .line 650169
    :goto_0
    if-ge v3, v4, :cond_0

    .line 650170
    iget-object v0, p0, LX/3v0;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3v3;

    .line 650171
    invoke-virtual {v0}, LX/3v3;->getGroupId()I

    move-result v5

    if-ne v5, p1, :cond_2

    .line 650172
    invoke-virtual {v0, p2}, LX/3v3;->c(Z)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 650173
    :goto_1
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v0

    goto :goto_0

    .line 650174
    :cond_0
    if-eqz v2, :cond_1

    invoke-virtual {p0, v1}, LX/3v0;->b(Z)V

    .line 650175
    :cond_1
    return-void

    :cond_2
    move v0, v2

    goto :goto_1
.end method

.method public setQwertyMode(Z)V
    .locals 1

    .prologue
    .line 650176
    iput-boolean p1, p0, LX/3v0;->g:Z

    .line 650177
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/3v0;->b(Z)V

    .line 650178
    return-void
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 650179
    iget-object v0, p0, LX/3v0;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method
