.class public abstract LX/4x6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4x5;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<C::",
        "Ljava/lang/Comparable;",
        ">",
        "Ljava/lang/Object;",
        "LX/4x5",
        "<TC;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 820769
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(LX/50M;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/50M",
            "<TC;>;)V"
        }
    .end annotation

    .prologue
    .line 820770
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public b(LX/50M;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/50M",
            "<TC;>;)V"
        }
    .end annotation

    .prologue
    .line 820771
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 820772
    if-ne p1, p0, :cond_0

    .line 820773
    const/4 v0, 0x1

    .line 820774
    :goto_0
    return v0

    .line 820775
    :cond_0
    instance-of v0, p1, LX/4x5;

    if-eqz v0, :cond_1

    .line 820776
    check-cast p1, LX/4x5;

    .line 820777
    invoke-virtual {p0}, LX/4x6;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {p1}, LX/4x5;->a()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 820778
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 820779
    invoke-virtual {p0}, LX/4x6;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->hashCode()I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 820780
    invoke-virtual {p0}, LX/4x6;->a()Ljava/util/Set;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
