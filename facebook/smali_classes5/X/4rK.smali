.class public final LX/4rK;
.super LX/0rP;
.source ""


# instance fields
.field public final a:S


# direct methods
.method private constructor <init>(S)V
    .locals 0

    .prologue
    .line 815485
    invoke-direct {p0}, LX/0rP;-><init>()V

    iput-short p1, p0, LX/4rK;->a:S

    return-void
.end method

.method public static a(S)LX/4rK;
    .locals 1

    .prologue
    .line 815484
    new-instance v0, LX/4rK;

    invoke-direct {v0, p0}, LX/4rK;-><init>(S)V

    return-object v0
.end method


# virtual methods
.method public final A()Ljava/math/BigInteger;
    .locals 2

    .prologue
    .line 815483
    iget-short v0, p0, LX/4rK;->a:S

    int-to-long v0, v0

    invoke-static {v0, v1}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v0

    return-object v0
.end method

.method public final B()Ljava/lang/String;
    .locals 1

    .prologue
    .line 815482
    iget-short v0, p0, LX/4rK;->a:S

    invoke-static {v0}, LX/13I;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a()LX/15z;
    .locals 1

    .prologue
    .line 815481
    sget-object v0, LX/15z;->VALUE_NUMBER_INT:LX/15z;

    return-object v0
.end method

.method public final a(Z)Z
    .locals 1

    .prologue
    .line 815480
    iget-short v0, p0, LX/4rK;->a:S

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()LX/16L;
    .locals 1

    .prologue
    .line 815479
    sget-object v0, LX/16L;->INT:LX/16L;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 815486
    if-ne p1, p0, :cond_1

    .line 815487
    :cond_0
    :goto_0
    return v0

    .line 815488
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    goto :goto_0

    .line 815489
    :cond_2
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 815490
    goto :goto_0

    .line 815491
    :cond_3
    check-cast p1, LX/4rK;

    iget-short v2, p1, LX/4rK;->a:S

    iget-short v3, p0, LX/4rK;->a:S

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 815478
    iget-short v0, p0, LX/4rK;->a:S

    return v0
.end method

.method public final serialize(LX/0nX;LX/0my;)V
    .locals 1

    .prologue
    .line 815476
    iget-short v0, p0, LX/4rK;->a:S

    invoke-virtual {p1, v0}, LX/0nX;->a(S)V

    .line 815477
    return-void
.end method

.method public final v()Ljava/lang/Number;
    .locals 1

    .prologue
    .line 815475
    iget-short v0, p0, LX/4rK;->a:S

    invoke-static {v0}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v0

    return-object v0
.end method

.method public final w()I
    .locals 1

    .prologue
    .line 815474
    iget-short v0, p0, LX/4rK;->a:S

    return v0
.end method

.method public final x()J
    .locals 2

    .prologue
    .line 815473
    iget-short v0, p0, LX/4rK;->a:S

    int-to-long v0, v0

    return-wide v0
.end method

.method public final y()D
    .locals 2

    .prologue
    .line 815472
    iget-short v0, p0, LX/4rK;->a:S

    int-to-double v0, v0

    return-wide v0
.end method

.method public final z()Ljava/math/BigDecimal;
    .locals 2

    .prologue
    .line 815471
    iget-short v0, p0, LX/4rK;->a:S

    int-to-long v0, v0

    invoke-static {v0, v1}, Ljava/math/BigDecimal;->valueOf(J)Ljava/math/BigDecimal;

    move-result-object v0

    return-object v0
.end method
