.class public final LX/4wF;
.super LX/4wA;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Qd",
        "<TK;TV;>.AbstractCacheSet<",
        "Ljava/util/Map$Entry",
        "<TK;TV;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic c:LX/0Qd;


# direct methods
.method public constructor <init>(LX/0Qd;Ljava/util/concurrent/ConcurrentMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ConcurrentMap",
            "<**>;)V"
        }
    .end annotation

    .prologue
    .line 819967
    iput-object p1, p0, LX/4wF;->c:LX/0Qd;

    .line 819968
    invoke-direct {p0, p1, p2}, LX/4wA;-><init>(LX/0Qd;Ljava/util/concurrent/ConcurrentMap;)V

    .line 819969
    return-void
.end method


# virtual methods
.method public final contains(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 819970
    instance-of v1, p1, Ljava/util/Map$Entry;

    if-nez v1, :cond_1

    .line 819971
    :cond_0
    :goto_0
    return v0

    .line 819972
    :cond_1
    check-cast p1, Ljava/util/Map$Entry;

    .line 819973
    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    .line 819974
    if-eqz v1, :cond_0

    .line 819975
    iget-object v2, p0, LX/4wF;->c:LX/0Qd;

    invoke-virtual {v2, v1}, LX/0Qd;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 819976
    if-eqz v1, :cond_0

    iget-object v2, p0, LX/4wF;->c:LX/0Qd;

    iget-object v2, v2, LX/0Qd;->g:LX/0Qj;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, LX/0Qj;->equivalent(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 819977
    new-instance v0, LX/4wE;

    iget-object v1, p0, LX/4wF;->c:LX/0Qd;

    invoke-direct {v0, v1}, LX/4wE;-><init>(LX/0Qd;)V

    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 819978
    instance-of v1, p1, Ljava/util/Map$Entry;

    if-nez v1, :cond_1

    .line 819979
    :cond_0
    :goto_0
    return v0

    .line 819980
    :cond_1
    check-cast p1, Ljava/util/Map$Entry;

    .line 819981
    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    .line 819982
    if-eqz v1, :cond_0

    iget-object v2, p0, LX/4wF;->c:LX/0Qd;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, LX/0Qd;->remove(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method
