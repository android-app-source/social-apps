.class public final LX/4pn;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/4pm;

.field public final b:Ljava/lang/String;


# direct methods
.method private constructor <init>(LX/4pm;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 811934
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 811935
    iput-object p1, p0, LX/4pn;->a:LX/4pm;

    .line 811936
    iput-object p2, p0, LX/4pn;->b:Ljava/lang/String;

    .line 811937
    return-void
.end method

.method public static a(Ljava/lang/String;)LX/4pn;
    .locals 2

    .prologue
    .line 811933
    new-instance v0, LX/4pn;

    sget-object v1, LX/4pm;->MANAGED_REFERENCE:LX/4pm;

    invoke-direct {v0, v1, p0}, LX/4pn;-><init>(LX/4pm;Ljava/lang/String;)V

    return-object v0
.end method

.method public static b(Ljava/lang/String;)LX/4pn;
    .locals 2

    .prologue
    .line 811932
    new-instance v0, LX/4pn;

    sget-object v1, LX/4pm;->BACK_REFERENCE:LX/4pm;

    invoke-direct {v0, v1, p0}, LX/4pn;-><init>(LX/4pm;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final b()Z
    .locals 2

    .prologue
    .line 811931
    iget-object v0, p0, LX/4pn;->a:LX/4pm;

    sget-object v1, LX/4pm;->MANAGED_REFERENCE:LX/4pm;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 811930
    iget-object v0, p0, LX/4pn;->a:LX/4pm;

    sget-object v1, LX/4pm;->BACK_REFERENCE:LX/4pm;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
