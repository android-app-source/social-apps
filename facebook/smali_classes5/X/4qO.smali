.class public LX/4qO;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/32s;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 813390
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 813391
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/4qO;->a:Ljava/util/List;

    .line 813392
    return-void
.end method

.method private constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/32s;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 813402
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 813403
    iput-object p1, p0, LX/4qO;->a:Ljava/util/List;

    .line 813404
    return-void
.end method


# virtual methods
.method public final a(LX/4ro;)LX/4qO;
    .locals 5

    .prologue
    .line 813405
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, LX/4qO;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 813406
    iget-object v0, p0, LX/4qO;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/32s;

    .line 813407
    iget-object v3, v0, LX/32s;->_propName:Ljava/lang/String;

    move-object v3, v3

    .line 813408
    invoke-virtual {p1, v3}, LX/4ro;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 813409
    invoke-virtual {v0, v3}, LX/32s;->a(Ljava/lang/String;)LX/32s;

    move-result-object v0

    .line 813410
    invoke-virtual {v0}, LX/32s;->l()Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v3

    .line 813411
    if-eqz v3, :cond_0

    .line 813412
    invoke-virtual {v3, p1}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->unwrappingDeserializer(LX/4ro;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v4

    .line 813413
    if-eq v4, v3, :cond_0

    .line 813414
    invoke-virtual {v0, v4}, LX/32s;->b(Lcom/fasterxml/jackson/databind/JsonDeserializer;)LX/32s;

    move-result-object v0

    .line 813415
    :cond_0
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 813416
    :cond_1
    new-instance v0, LX/4qO;

    invoke-direct {v0, v1}, LX/4qO;-><init>(Ljava/util/List;)V

    return-object v0
.end method

.method public final a(LX/0n3;Ljava/lang/Object;LX/0nW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 813395
    const/4 v0, 0x0

    iget-object v1, p0, LX/4qO;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 813396
    iget-object v0, p0, LX/4qO;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/32s;

    .line 813397
    invoke-virtual {p3}, LX/0nW;->i()LX/15w;

    move-result-object v3

    .line 813398
    invoke-virtual {v3}, LX/15w;->c()LX/15z;

    .line 813399
    invoke-virtual {v0, v3, p1, p2}, LX/32s;->a(LX/15w;LX/0n3;Ljava/lang/Object;)V

    .line 813400
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 813401
    :cond_0
    return-object p2
.end method

.method public final a(LX/32s;)V
    .locals 1

    .prologue
    .line 813393
    iget-object v0, p0, LX/4qO;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 813394
    return-void
.end method
