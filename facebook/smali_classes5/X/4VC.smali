.class public LX/4VC;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 742169
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/flatbuffers/MutableFlattenable;LX/4Ud;[[ILX/0t8;Ljava/util/Map;Z)Lcom/facebook/flatbuffers/MutableFlattenable;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/flatbuffers/MutableFlattenable;",
            "LX/4Ud;",
            "[[I",
            "Lcom/facebook/graphql/executor/cache/ConsistencyConfig;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;Z)",
            "Lcom/facebook/flatbuffers/MutableFlattenable;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 742170
    instance-of v0, p0, Lcom/facebook/graphql/modelutil/FragmentModel;

    if-nez v0, :cond_0

    .line 742171
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 742172
    :cond_0
    invoke-interface {p0}, Lcom/facebook/flatbuffers/MutableFlattenable;->q_()LX/15i;

    move-result-object v7

    .line 742173
    if-nez v7, :cond_2

    .line 742174
    :cond_1
    :goto_0
    return-object p0

    .line 742175
    :cond_2
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    .line 742176
    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    move-object v3, p3

    invoke-static/range {v0 .. v6}, LX/4V0;->a(Lcom/facebook/flatbuffers/MutableFlattenable;[[ILX/4Ud;LX/0t8;ILjava/util/Collection;Ljava/util/Collection;)V

    .line 742177
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-object v2, v6

    :cond_3
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4Zs;

    .line 742178
    iget-object v1, v0, LX/4Zs;->a:Ljava/lang/String;

    invoke-interface {p4, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    .line 742179
    if-eqz v1, :cond_3

    .line 742180
    iget-object v4, v0, LX/4Zs;->b:Ljava/lang/String;

    invoke-interface {v1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    .line 742181
    if-eqz v4, :cond_3

    iget v1, v0, LX/4Zs;->d:I

    iget v5, v0, LX/4Zs;->e:I

    invoke-static {v7, v1, v5, v4, p5}, LX/4VC;->b(LX/15i;IILjava/lang/Object;Z)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 742182
    if-nez v2, :cond_5

    .line 742183
    const-string v1, "DeltaBufferConsistentFieldMerger.mergeConsistentFieldsIntoNewInstance"

    invoke-static {v7, v1}, LX/1lN;->a(LX/15i;Ljava/lang/String;)LX/15i;

    move-result-object v1

    .line 742184
    :goto_2
    iget v2, v0, LX/4Zs;->d:I

    iget v0, v0, LX/4Zs;->e:I

    invoke-static {v1, v2, v0, v4, p5}, LX/4VC;->a(LX/15i;IILjava/lang/Object;Z)V

    move-object v2, v1

    goto :goto_1

    .line 742185
    :cond_4
    if-eqz v2, :cond_1

    .line 742186
    invoke-interface {p0}, Lcom/facebook/flatbuffers/MutableFlattenable;->o_()I

    move-result v0

    .line 742187
    check-cast p0, Lcom/facebook/graphql/modelutil/FragmentModel;

    invoke-interface {p0, v2, v0}, Lcom/facebook/graphql/modelutil/FragmentModel;->b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;

    move-result-object p0

    goto :goto_0

    :cond_5
    move-object v1, v2

    goto :goto_2
.end method

.method private static a(LX/15i;IILjava/lang/Object;Z)V
    .locals 2

    .prologue
    .line 742129
    instance-of v0, p3, Ljava/lang/Byte;

    if-eqz v0, :cond_0

    .line 742130
    check-cast p3, Ljava/lang/Byte;

    invoke-virtual {p3}, Ljava/lang/Byte;->byteValue()B

    move-result v0

    .line 742131
    const-string v1, "Byte"

    invoke-static {p0, v1, p1, p2}, LX/15i;->a(LX/15i;Ljava/lang/String;II)V

    .line 742132
    invoke-static {p0}, LX/15i;->i(LX/15i;)LX/16b;

    move-result-object v1

    .line 742133
    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object p0

    invoke-static {v1, p1, p2, p0}, LX/16b;->a(LX/16b;IILjava/lang/Object;)V

    .line 742134
    :goto_0
    return-void

    .line 742135
    :cond_0
    instance-of v0, p3, Ljava/lang/Short;

    if-eqz v0, :cond_1

    .line 742136
    check-cast p3, Ljava/lang/Short;

    invoke-virtual {p3}, Ljava/lang/Short;->shortValue()S

    move-result v0

    .line 742137
    const-string v1, "Short"

    invoke-static {p0, v1, p1, p2}, LX/15i;->a(LX/15i;Ljava/lang/String;II)V

    .line 742138
    invoke-static {p0}, LX/15i;->i(LX/15i;)LX/16b;

    move-result-object v1

    .line 742139
    invoke-static {v0}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object p0

    invoke-static {v1, p1, p2, p0}, LX/16b;->a(LX/16b;IILjava/lang/Object;)V

    .line 742140
    goto :goto_0

    .line 742141
    :cond_1
    instance-of v0, p3, Ljava/lang/Float;

    if-eqz v0, :cond_2

    .line 742142
    check-cast p3, Ljava/lang/Float;

    invoke-virtual {p3}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 742143
    const-string v1, "Float"

    invoke-static {p0, v1, p1, p2}, LX/15i;->a(LX/15i;Ljava/lang/String;II)V

    .line 742144
    invoke-static {p0}, LX/15i;->i(LX/15i;)LX/16b;

    move-result-object v1

    .line 742145
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p0

    invoke-static {v1, p1, p2, p0}, LX/16b;->a(LX/16b;IILjava/lang/Object;)V

    .line 742146
    goto :goto_0

    .line 742147
    :cond_2
    instance-of v0, p3, Ljava/lang/Double;

    if-eqz v0, :cond_3

    .line 742148
    check-cast p3, Ljava/lang/Double;

    invoke-virtual {p3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    .line 742149
    const-string p3, "Double"

    invoke-static {p0, p3, p1, p2}, LX/15i;->a(LX/15i;Ljava/lang/String;II)V

    .line 742150
    invoke-static {p0}, LX/15i;->i(LX/15i;)LX/16b;

    move-result-object p3

    .line 742151
    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p0

    invoke-static {p3, p1, p2, p0}, LX/16b;->a(LX/16b;IILjava/lang/Object;)V

    .line 742152
    goto :goto_0

    .line 742153
    :cond_3
    instance-of v0, p3, Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 742154
    check-cast p3, Ljava/lang/Long;

    invoke-virtual {p3}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p0, p1, p2, v0, v1}, LX/15i;->b(IIJ)V

    goto :goto_0

    .line 742155
    :cond_4
    instance-of v0, p3, Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 742156
    check-cast p3, Ljava/lang/Integer;

    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, p1, p2, v0}, LX/15i;->b(III)V

    goto :goto_0

    .line 742157
    :cond_5
    instance-of v0, p3, Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    .line 742158
    check-cast p3, Ljava/lang/Boolean;

    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p0, p1, p2, v0}, LX/15i;->a(IIZ)V

    goto :goto_0

    .line 742159
    :cond_6
    instance-of v0, p3, Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 742160
    check-cast p3, Ljava/lang/String;

    invoke-virtual {p0, p1, p2, p3}, LX/15i;->a(IILjava/lang/String;)V

    goto/16 :goto_0

    .line 742161
    :cond_7
    instance-of v0, p3, Ljava/lang/Enum;

    if-eqz v0, :cond_9

    .line 742162
    check-cast p3, Ljava/lang/Enum;

    .line 742163
    if-eqz p4, :cond_8

    .line 742164
    invoke-virtual {p0, p1, p2, p3}, LX/15i;->a(IILjava/lang/Enum;)V

    goto/16 :goto_0

    .line 742165
    :cond_8
    invoke-virtual {p3}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, LX/15i;->a(IILjava/lang/String;)V

    goto/16 :goto_0

    .line 742166
    :cond_9
    if-nez p3, :cond_a

    .line 742167
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, LX/15i;->a(IILjava/lang/String;)V

    goto/16 :goto_0

    .line 742168
    :cond_a
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static a(LX/15i;IILjava/lang/Object;)Z
    .locals 1

    .prologue
    .line 742188
    invoke-virtual {p0, p1, p2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 742189
    invoke-virtual {p3, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(LX/15i;ILX/4Ud;LX/0t8;Ljava/util/Map;ZZ)Z
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/15i;",
            "I",
            "LX/4Ud;",
            "Lcom/facebook/graphql/executor/cache/ConsistencyConfig;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;ZZ)Z"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 742118
    if-nez p0, :cond_1

    .line 742119
    :cond_0
    return v5

    .line 742120
    :cond_1
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    .line 742121
    const/4 v7, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p5

    move-object v3, p2

    move-object v4, p3

    invoke-static/range {v0 .. v7}, LX/4V0;->a(LX/15i;IZLX/4Ud;LX/0t8;ILjava/util/Collection;Ljava/util/Collection;)V

    .line 742122
    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4Zs;

    .line 742123
    iget-object v1, v0, LX/4Zs;->a:Ljava/lang/String;

    invoke-interface {p4, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    .line 742124
    if-eqz v1, :cond_2

    .line 742125
    iget-object v3, v0, LX/4Zs;->b:Ljava/lang/String;

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 742126
    if-eqz v1, :cond_2

    iget v3, v0, LX/4Zs;->d:I

    iget v4, v0, LX/4Zs;->e:I

    invoke-static {p0, v3, v4, v1, p6}, LX/4VC;->b(LX/15i;IILjava/lang/Object;Z)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 742127
    iget v3, v0, LX/4Zs;->d:I

    iget v0, v0, LX/4Zs;->e:I

    invoke-static {p0, v3, v0, v1, p6}, LX/4VC;->a(LX/15i;IILjava/lang/Object;Z)V

    .line 742128
    const/4 v5, 0x1

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/modelutil/FragmentModel;LX/4Ud;LX/0t8;Ljava/util/Map;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/modelutil/FragmentModel;",
            "LX/4Ud;",
            "Lcom/facebook/graphql/executor/cache/ConsistencyConfig;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;)Z"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 742085
    invoke-interface {p0}, Lcom/facebook/flatbuffers/MutableFlattenable;->q_()LX/15i;

    move-result-object v0

    invoke-interface {p0}, Lcom/facebook/graphql/modelutil/FragmentModel;->d_()I

    move-result v1

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v6, v5

    invoke-static/range {v0 .. v6}, LX/4VC;->a(LX/15i;ILX/4Ud;LX/0t8;Ljava/util/Map;ZZ)Z

    move-result v0

    return v0
.end method

.method private static b(LX/15i;IILjava/lang/Object;Z)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 742086
    instance-of v0, p3, Ljava/lang/Byte;

    if-eqz v0, :cond_1

    .line 742087
    invoke-virtual {p0, p1, p2, v2}, LX/15i;->a(IIB)B

    move-result v0

    .line 742088
    check-cast p3, Ljava/lang/Byte;

    invoke-virtual {p3}, Ljava/lang/Byte;->byteValue()B

    move-result v3

    if-eq v0, v3, :cond_0

    move v0, v1

    .line 742089
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 742090
    goto :goto_0

    .line 742091
    :cond_1
    instance-of v0, p3, Ljava/lang/Short;

    if-eqz v0, :cond_3

    .line 742092
    invoke-virtual {p0, p1, p2, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 742093
    check-cast p3, Ljava/lang/Short;

    invoke-virtual {p3}, Ljava/lang/Short;->shortValue()S

    move-result v3

    if-eq v0, v3, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0

    .line 742094
    :cond_3
    instance-of v0, p3, Ljava/lang/Float;

    if-eqz v0, :cond_5

    .line 742095
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, LX/15i;->a(IIF)F

    move-result v0

    .line 742096
    check-cast p3, Ljava/lang/Float;

    invoke-virtual {p3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    cmpl-float v0, v0, v3

    if-eqz v0, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_0

    .line 742097
    :cond_5
    instance-of v0, p3, Ljava/lang/Double;

    if-eqz v0, :cond_7

    .line 742098
    const-wide/16 v4, 0x0

    invoke-virtual {p0, p1, p2, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v4

    .line 742099
    check-cast p3, Ljava/lang/Double;

    invoke-virtual {p3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    cmpl-double v0, v4, v6

    if-eqz v0, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    move v0, v2

    goto :goto_0

    .line 742100
    :cond_7
    instance-of v0, p3, Ljava/lang/Long;

    if-eqz v0, :cond_9

    .line 742101
    const-wide/16 v4, 0x0

    invoke-virtual {p0, p1, p2, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v4

    .line 742102
    check-cast p3, Ljava/lang/Long;

    invoke-virtual {p3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v0, v4, v6

    if-eqz v0, :cond_8

    move v0, v1

    goto :goto_0

    :cond_8
    move v0, v2

    goto :goto_0

    .line 742103
    :cond_9
    instance-of v0, p3, Ljava/lang/Integer;

    if-eqz v0, :cond_b

    .line 742104
    invoke-virtual {p0, p1, p2, v2}, LX/15i;->a(III)I

    move-result v0

    .line 742105
    check-cast p3, Ljava/lang/Integer;

    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-eq v0, v3, :cond_a

    move v0, v1

    goto :goto_0

    :cond_a
    move v0, v2

    goto :goto_0

    .line 742106
    :cond_b
    instance-of v0, p3, Ljava/lang/Boolean;

    if-eqz v0, :cond_d

    .line 742107
    invoke-virtual {p0, p1, p2}, LX/15i;->b(II)Z

    move-result v0

    .line 742108
    check-cast p3, Ljava/lang/Boolean;

    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eq v0, v3, :cond_c

    move v0, v1

    goto/16 :goto_0

    :cond_c
    move v0, v2

    goto/16 :goto_0

    .line 742109
    :cond_d
    instance-of v0, p3, Ljava/lang/String;

    if-eqz v0, :cond_e

    .line 742110
    invoke-static {p0, p1, p2, p3}, LX/4VC;->a(LX/15i;IILjava/lang/Object;)Z

    move-result v0

    goto/16 :goto_0

    .line 742111
    :cond_e
    instance-of v0, p3, Ljava/lang/Enum;

    if-eqz v0, :cond_11

    move-object v0, p3

    .line 742112
    check-cast v0, Ljava/lang/Enum;

    .line 742113
    if-eqz p4, :cond_10

    .line 742114
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    .line 742115
    invoke-virtual {p3, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_f

    move v0, v1

    goto/16 :goto_0

    :cond_f
    move v0, v2

    goto/16 :goto_0

    .line 742116
    :cond_10
    invoke-virtual {v0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p1, p2, v0}, LX/4VC;->a(LX/15i;IILjava/lang/Object;)Z

    move-result v0

    goto/16 :goto_0

    .line 742117
    :cond_11
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
