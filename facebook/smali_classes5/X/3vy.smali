.class public final LX/3vy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements LX/3vx;


# instance fields
.field public final synthetic a:LX/3w4;

.field private b:Landroid/app/AlertDialog;

.field private c:Landroid/widget/ListAdapter;

.field private d:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(LX/3w4;)V
    .locals 0

    .prologue
    .line 653895
    iput-object p1, p0, LX/3vy;->a:LX/3w4;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 653913
    iget-object v0, p0, LX/3vy;->b:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 653914
    iget-object v0, p0, LX/3vy;->b:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 653915
    const/4 v0, 0x0

    iput-object v0, p0, LX/3vy;->b:Landroid/app/AlertDialog;

    .line 653916
    :cond_0
    return-void
.end method

.method public final a(Landroid/widget/ListAdapter;)V
    .locals 0

    .prologue
    .line 653911
    iput-object p1, p0, LX/3vy;->c:Landroid/widget/ListAdapter;

    .line 653912
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 653917
    iput-object p1, p0, LX/3vy;->d:Ljava/lang/CharSequence;

    .line 653918
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 653910
    iget-object v0, p0, LX/3vy;->b:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3vy;->b:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()V
    .locals 4

    .prologue
    .line 653901
    iget-object v0, p0, LX/3vy;->c:Landroid/widget/ListAdapter;

    if-nez v0, :cond_0

    .line 653902
    :goto_0
    return-void

    .line 653903
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, LX/3vy;->a:LX/3w4;

    invoke-virtual {v1}, LX/3w4;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 653904
    iget-object v1, p0, LX/3vy;->d:Ljava/lang/CharSequence;

    if-eqz v1, :cond_1

    .line 653905
    iget-object v1, p0, LX/3vy;->d:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 653906
    :cond_1
    iget-object v1, p0, LX/3vy;->c:Landroid/widget/ListAdapter;

    iget-object v2, p0, LX/3vy;->a:LX/3w4;

    .line 653907
    iget v3, v2, LX/3vM;->v:I

    move v2, v3

    .line 653908
    invoke-virtual {v0, v1, v2, p0}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, LX/3vy;->b:Landroid/app/AlertDialog;

    .line 653909
    iget-object v0, p0, LX/3vy;->b:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    .line 653896
    iget-object v0, p0, LX/3vy;->a:LX/3w4;

    invoke-virtual {v0, p2}, LX/3vM;->setSelection(I)V

    .line 653897
    iget-object v0, p0, LX/3vy;->a:LX/3w4;

    iget-object v0, v0, LX/3vM;->s:LX/3vf;

    if-eqz v0, :cond_0

    .line 653898
    iget-object v0, p0, LX/3vy;->a:LX/3w4;

    const/4 v1, 0x0

    iget-object v2, p0, LX/3vy;->c:Landroid/widget/ListAdapter;

    invoke-interface {v2, p2}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v2

    invoke-virtual {v0, v1, p2, v2, v3}, LX/3vM;->a(Landroid/view/View;IJ)Z

    .line 653899
    :cond_0
    invoke-virtual {p0}, LX/3vy;->a()V

    .line 653900
    return-void
.end method
