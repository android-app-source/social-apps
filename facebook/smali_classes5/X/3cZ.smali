.class public final LX/3cZ;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/friends/model/FriendRequest;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)V
    .locals 0

    .prologue
    .line 614697
    iput-object p1, p0, LX/3cZ;->a:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 614709
    iget-object v0, p0, LX/3cZ;->a:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    iget-object v0, v0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->af:LX/3Te;

    .line 614710
    iget-object p0, v0, LX/3Te;->f:LX/3UE;

    .line 614711
    sget-object v0, LX/3UG;->FAILURE:LX/3UG;

    iput-object v0, p0, LX/3UE;->l:LX/3UG;

    .line 614712
    iget-object v0, p0, LX/3UE;->i:LX/3Tg;

    invoke-interface {v0}, LX/3Tg;->e()V

    .line 614713
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 614698
    check-cast p1, Ljava/util/List;

    const/4 v1, 0x0

    .line 614699
    iget-object v0, p0, LX/3cZ;->a:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    invoke-static {v0, p1}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->b(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;Ljava/util/List;)V

    .line 614700
    iget-object v2, p0, LX/3cZ;->a:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friends/model/FriendRequest;

    .line 614701
    iget-boolean p1, v0, Lcom/facebook/friends/model/FriendRequest;->h:Z

    move v0, p1

    .line 614702
    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 614703
    :goto_0
    iput-boolean v0, v2, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->aE:Z

    .line 614704
    iget-object v0, p0, LX/3cZ;->a:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    invoke-static {v0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->w(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)V

    .line 614705
    iget-object v0, p0, LX/3cZ;->a:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    iget-object v0, v0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->af:LX/3Te;

    invoke-virtual {v0}, LX/3Te;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 614706
    iget-object v0, p0, LX/3cZ;->a:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    invoke-static {v0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->O(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)V

    .line 614707
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 614708
    goto :goto_0
.end method
