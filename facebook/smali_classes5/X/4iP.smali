.class public LX/4iP;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final mCertCacheHit:Z

.field private final mConnEstablished:Z

.field private final mDnsLatency:J

.field private final mIsPush:Z

.field private final mLocalAddr:Ljava/lang/String;

.field public final mLocalPort:I

.field private final mNewConnection:Z

.field private final mProtocol:Ljava/lang/String;

.field public final mReqBodyBytes:I

.field private final mReqHeaderBytes:I

.field public final mReqHeaderCompBytes:I

.field private final mReqSent:Z

.field private final mRspBodyBytes:I

.field public final mRspBodyBytesTime:J

.field public final mRspBodyCompBytes:I

.field private final mRspHeaderBytes:I

.field public final mRspHeaderCompBytes:I

.field private final mRspReceived:Z

.field public final mRtt:J

.field public mServerAddr:Ljava/net/InetAddress;

.field public final mServerCwnd:J

.field public final mServerMss:J

.field private final mServerQuality:Ljava/lang/String;

.field public final mServerRtt:J

.field public final mServerRtx:J

.field public final mServerTotalBytesWritten:J

.field private final mTcpLatency:J

.field public final mTimeToFirstByte:J

.field public final mTimeToLastByte:J

.field private final mTlsLatency:J

.field public final mTotalConnect:J


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;IZZZZZLjava/lang/String;IIIIIIIJJJJJJJJLjava/lang/String;JJJJJZ)V
    .locals 3

    .prologue
    .line 803077
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 803078
    const/4 v2, 0x0

    iput-object v2, p0, LX/4iP;->mServerAddr:Ljava/net/InetAddress;

    .line 803079
    const-string v2, ""

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 803080
    :try_start_0
    invoke-static {p1}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v2

    iput-object v2, p0, LX/4iP;->mServerAddr:Ljava/net/InetAddress;
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0

    .line 803081
    :cond_0
    :goto_0
    iput-object p2, p0, LX/4iP;->mLocalAddr:Ljava/lang/String;

    .line 803082
    iput p3, p0, LX/4iP;->mLocalPort:I

    .line 803083
    iput-boolean p4, p0, LX/4iP;->mConnEstablished:Z

    .line 803084
    iput-boolean p5, p0, LX/4iP;->mNewConnection:Z

    .line 803085
    iput-boolean p6, p0, LX/4iP;->mReqSent:Z

    .line 803086
    iput-boolean p7, p0, LX/4iP;->mRspReceived:Z

    .line 803087
    iput-boolean p8, p0, LX/4iP;->mCertCacheHit:Z

    .line 803088
    iput-object p9, p0, LX/4iP;->mProtocol:Ljava/lang/String;

    .line 803089
    iput p10, p0, LX/4iP;->mReqHeaderBytes:I

    .line 803090
    iput p11, p0, LX/4iP;->mReqHeaderCompBytes:I

    .line 803091
    iput p12, p0, LX/4iP;->mReqBodyBytes:I

    .line 803092
    move/from16 v0, p13

    iput v0, p0, LX/4iP;->mRspHeaderBytes:I

    .line 803093
    move/from16 v0, p14

    iput v0, p0, LX/4iP;->mRspHeaderCompBytes:I

    .line 803094
    move/from16 v0, p15

    iput v0, p0, LX/4iP;->mRspBodyBytes:I

    .line 803095
    move/from16 v0, p16

    iput v0, p0, LX/4iP;->mRspBodyCompBytes:I

    .line 803096
    move-wide/from16 v0, p17

    iput-wide v0, p0, LX/4iP;->mDnsLatency:J

    .line 803097
    move-wide/from16 v0, p19

    iput-wide v0, p0, LX/4iP;->mTcpLatency:J

    .line 803098
    move-wide/from16 v0, p21

    iput-wide v0, p0, LX/4iP;->mTlsLatency:J

    .line 803099
    move-wide/from16 v0, p23

    iput-wide v0, p0, LX/4iP;->mRtt:J

    .line 803100
    move-wide/from16 v0, p25

    iput-wide v0, p0, LX/4iP;->mTimeToFirstByte:J

    .line 803101
    move-wide/from16 v0, p27

    iput-wide v0, p0, LX/4iP;->mTimeToLastByte:J

    .line 803102
    move-wide/from16 v0, p29

    iput-wide v0, p0, LX/4iP;->mTotalConnect:J

    .line 803103
    move-wide/from16 v0, p31

    iput-wide v0, p0, LX/4iP;->mRspBodyBytesTime:J

    .line 803104
    move-object/from16 v0, p33

    iput-object v0, p0, LX/4iP;->mServerQuality:Ljava/lang/String;

    .line 803105
    move-wide/from16 v0, p34

    iput-wide v0, p0, LX/4iP;->mServerRtt:J

    .line 803106
    move-wide/from16 v0, p36

    iput-wide v0, p0, LX/4iP;->mServerRtx:J

    .line 803107
    move-wide/from16 v0, p38

    iput-wide v0, p0, LX/4iP;->mServerCwnd:J

    .line 803108
    move-wide/from16 v0, p40

    iput-wide v0, p0, LX/4iP;->mServerMss:J

    .line 803109
    move-wide/from16 v0, p42

    iput-wide v0, p0, LX/4iP;->mServerTotalBytesWritten:J

    .line 803110
    move/from16 v0, p44

    iput-boolean v0, p0, LX/4iP;->mIsPush:Z

    .line 803111
    return-void

    :catch_0
    goto :goto_0
.end method


# virtual methods
.method public getCertCacheHit()Z
    .locals 1

    .prologue
    .line 803076
    iget-boolean v0, p0, LX/4iP;->mCertCacheHit:Z

    return v0
.end method

.method public getDnsLatency()J
    .locals 2

    .prologue
    .line 803075
    iget-wide v0, p0, LX/4iP;->mDnsLatency:J

    return-wide v0
.end method

.method public getIsConnectionEstablished()Z
    .locals 1

    .prologue
    .line 803074
    iget-boolean v0, p0, LX/4iP;->mConnEstablished:Z

    return v0
.end method

.method public getIsNewConnection()Z
    .locals 1

    .prologue
    .line 803073
    iget-boolean v0, p0, LX/4iP;->mNewConnection:Z

    return v0
.end method

.method public getLocalAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 803072
    iget-object v0, p0, LX/4iP;->mLocalAddr:Ljava/lang/String;

    return-object v0
.end method

.method public getNegotiatedProtocol()Ljava/lang/String;
    .locals 1

    .prologue
    .line 803071
    iget-object v0, p0, LX/4iP;->mProtocol:Ljava/lang/String;

    return-object v0
.end method

.method public getRequestBodyBytes()I
    .locals 1

    .prologue
    .line 803058
    iget v0, p0, LX/4iP;->mReqBodyBytes:I

    return v0
.end method

.method public getRequestHeaderBytes()I
    .locals 1

    .prologue
    .line 803070
    iget v0, p0, LX/4iP;->mReqHeaderBytes:I

    return v0
.end method

.method public getRequestHeaderCompressedBytes()I
    .locals 1

    .prologue
    .line 803069
    iget v0, p0, LX/4iP;->mReqHeaderCompBytes:I

    return v0
.end method

.method public getRequestSent()Z
    .locals 1

    .prologue
    .line 803068
    iget-boolean v0, p0, LX/4iP;->mReqSent:Z

    return v0
.end method

.method public getResponseBodyBytes()I
    .locals 1

    .prologue
    .line 803067
    iget v0, p0, LX/4iP;->mRspBodyBytes:I

    return v0
.end method

.method public getResponseBodyCompressedBytes()I
    .locals 1

    .prologue
    .line 803066
    iget v0, p0, LX/4iP;->mRspBodyCompBytes:I

    return v0
.end method

.method public getResponseHeaderBytes()I
    .locals 1

    .prologue
    .line 803065
    iget v0, p0, LX/4iP;->mRspHeaderBytes:I

    return v0
.end method

.method public getResponseHeaderCompressedBytes()I
    .locals 1

    .prologue
    .line 803064
    iget v0, p0, LX/4iP;->mRspHeaderCompBytes:I

    return v0
.end method

.method public getResponseReceived()Z
    .locals 1

    .prologue
    .line 803063
    iget-boolean v0, p0, LX/4iP;->mRspReceived:Z

    return v0
.end method

.method public getRspBodyBytesTime()J
    .locals 2

    .prologue
    .line 803062
    iget-wide v0, p0, LX/4iP;->mRspBodyBytesTime:J

    return-wide v0
.end method

.method public getRtt()J
    .locals 2

    .prologue
    .line 803061
    iget-wide v0, p0, LX/4iP;->mRtt:J

    return-wide v0
.end method

.method public getServerAddress()Ljava/net/InetAddress;
    .locals 1

    .prologue
    .line 803060
    iget-object v0, p0, LX/4iP;->mServerAddr:Ljava/net/InetAddress;

    return-object v0
.end method

.method public getServerQuality()Ljava/lang/String;
    .locals 1

    .prologue
    .line 803059
    iget-object v0, p0, LX/4iP;->mServerQuality:Ljava/lang/String;

    return-object v0
.end method

.method public getTcpLatency()J
    .locals 2

    .prologue
    .line 803057
    iget-wide v0, p0, LX/4iP;->mTcpLatency:J

    return-wide v0
.end method

.method public getTimeToFirstByte()J
    .locals 2

    .prologue
    .line 803056
    iget-wide v0, p0, LX/4iP;->mTimeToFirstByte:J

    return-wide v0
.end method

.method public getTimeToLastByte()J
    .locals 2

    .prologue
    .line 803055
    iget-wide v0, p0, LX/4iP;->mTimeToLastByte:J

    return-wide v0
.end method

.method public getTlsLatency()J
    .locals 2

    .prologue
    .line 803054
    iget-wide v0, p0, LX/4iP;->mTlsLatency:J

    return-wide v0
.end method

.method public getTotalRequestTime()J
    .locals 4

    .prologue
    .line 803053
    iget-wide v0, p0, LX/4iP;->mTimeToLastByte:J

    iget-wide v2, p0, LX/4iP;->mTotalConnect:J

    add-long/2addr v0, v2

    return-wide v0
.end method

.method public isPush()Z
    .locals 1

    .prologue
    .line 803052
    iget-boolean v0, p0, LX/4iP;->mIsPush:Z

    return v0
.end method

.method public isSpdy()Z
    .locals 2

    .prologue
    .line 803051
    iget-object v0, p0, LX/4iP;->mProtocol:Ljava/lang/String;

    const-string v1, "SPDY"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
