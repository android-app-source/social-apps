.class public final enum LX/49d;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/49d;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/49d;

.field public static final enum FB_SYNC:LX/49d;

.field public static final enum GLOBAL_SYNC:LX/49d;

.field public static final enum REGENERATE:LX/49d;


# instance fields
.field private mType:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 674596
    new-instance v0, LX/49d;

    const-string v1, "FB_SYNC"

    const-string v2, "fb_sync"

    invoke-direct {v0, v1, v3, v2}, LX/49d;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/49d;->FB_SYNC:LX/49d;

    .line 674597
    new-instance v0, LX/49d;

    const-string v1, "REGENERATE"

    const-string v2, "regenerate"

    invoke-direct {v0, v1, v4, v2}, LX/49d;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/49d;->REGENERATE:LX/49d;

    .line 674598
    new-instance v0, LX/49d;

    const-string v1, "GLOBAL_SYNC"

    const-string v2, "global_sync"

    invoke-direct {v0, v1, v5, v2}, LX/49d;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/49d;->GLOBAL_SYNC:LX/49d;

    .line 674599
    const/4 v0, 0x3

    new-array v0, v0, [LX/49d;

    sget-object v1, LX/49d;->FB_SYNC:LX/49d;

    aput-object v1, v0, v3

    sget-object v1, LX/49d;->REGENERATE:LX/49d;

    aput-object v1, v0, v4

    sget-object v1, LX/49d;->GLOBAL_SYNC:LX/49d;

    aput-object v1, v0, v5

    sput-object v0, LX/49d;->$VALUES:[LX/49d;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 674600
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 674601
    iput-object p3, p0, LX/49d;->mType:Ljava/lang/String;

    .line 674602
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/49d;
    .locals 1

    .prologue
    .line 674603
    const-class v0, LX/49d;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/49d;

    return-object v0
.end method

.method public static values()[LX/49d;
    .locals 1

    .prologue
    .line 674604
    sget-object v0, LX/49d;->$VALUES:[LX/49d;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/49d;

    return-object v0
.end method


# virtual methods
.method public final type()Ljava/lang/String;
    .locals 1

    .prologue
    .line 674605
    iget-object v0, p0, LX/49d;->mType:Ljava/lang/String;

    return-object v0
.end method
