.class public final LX/51S;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4yY;


# annotations
.annotation build Lcom/google/common/annotations/Beta;
.end annotation

.annotation build Lcom/google/common/annotations/GwtIncompatible;
    value = "NavigableMap"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K::",
        "Ljava/lang/Comparable;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/4yY",
        "<TK;TV;>;"
    }
.end annotation


# static fields
.field private static final b:LX/4yY;


# instance fields
.field public final a:Ljava/util/NavigableMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/NavigableMap",
            "<",
            "LX/4xM",
            "<TK;>;",
            "LX/51R",
            "<TK;TV;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 825080
    new-instance v0, LX/51P;

    invoke-direct {v0}, LX/51P;-><init>()V

    sput-object v0, LX/51S;->b:LX/4yY;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 825083
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 825084
    invoke-static {}, LX/0PM;->f()Ljava/util/TreeMap;

    move-result-object v0

    iput-object v0, p0, LX/51S;->a:Ljava/util/NavigableMap;

    .line 825085
    return-void
.end method

.method public static a(LX/51S;LX/4xM;LX/4xM;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4xM",
            "<TK;>;",
            "LX/4xM",
            "<TK;>;TV;)V"
        }
    .end annotation

    .prologue
    .line 825081
    iget-object v0, p0, LX/51S;->a:Ljava/util/NavigableMap;

    new-instance v1, LX/51R;

    invoke-direct {v1, p1, p2, p3}, LX/51R;-><init>(LX/4xM;LX/4xM;Ljava/lang/Object;)V

    invoke-interface {v0, p1, v1}, Ljava/util/NavigableMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 825082
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Comparable;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)TV;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 825073
    iget-object v0, p0, LX/51S;->a:Ljava/util/NavigableMap;

    invoke-static {p1}, LX/4xM;->b(Ljava/lang/Comparable;)LX/4xM;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/NavigableMap;->floorEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v1

    .line 825074
    if-eqz v1, :cond_1

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/51R;

    .line 825075
    iget-object p0, v0, LX/51R;->a:LX/50M;

    invoke-virtual {p0, p1}, LX/50M;->a(Ljava/lang/Comparable;)Z

    move-result p0

    move v0, p0

    .line 825076
    if-eqz v0, :cond_1

    .line 825077
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 825078
    :goto_0
    move-object v0, v0

    .line 825079
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_1
    return-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/50M;Ljava/lang/Object;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/50M",
            "<TK;>;TV;)V"
        }
    .end annotation

    .prologue
    .line 825086
    invoke-virtual {p1}, LX/50M;->i()Z

    move-result v0

    if-nez v0, :cond_0

    .line 825087
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 825088
    invoke-virtual {p1}, LX/50M;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 825089
    :goto_0
    iget-object v0, p0, LX/51S;->a:Ljava/util/NavigableMap;

    iget-object v1, p1, LX/50M;->lowerBound:LX/4xM;

    new-instance v2, LX/51R;

    invoke-direct {v2, p1, p2}, LX/51R;-><init>(LX/50M;Ljava/lang/Object;)V

    invoke-interface {v0, v1, v2}, Ljava/util/NavigableMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 825090
    :cond_0
    return-void

    .line 825091
    :cond_1
    iget-object v0, p0, LX/51S;->a:Ljava/util/NavigableMap;

    iget-object v1, p1, LX/50M;->lowerBound:LX/4xM;

    invoke-interface {v0, v1}, Ljava/util/NavigableMap;->lowerEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v2

    .line 825092
    if-eqz v2, :cond_3

    .line 825093
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/51R;

    .line 825094
    invoke-virtual {v0}, LX/51R;->c()LX/4xM;

    move-result-object v1

    iget-object v3, p1, LX/50M;->lowerBound:LX/4xM;

    invoke-virtual {v1, v3}, LX/4xM;->a(LX/4xM;)I

    move-result v1

    if-lez v1, :cond_3

    .line 825095
    invoke-virtual {v0}, LX/51R;->c()LX/4xM;

    move-result-object v1

    iget-object v3, p1, LX/50M;->upperBound:LX/4xM;

    invoke-virtual {v1, v3}, LX/4xM;->a(LX/4xM;)I

    move-result v1

    if-lez v1, :cond_2

    .line 825096
    iget-object v3, p1, LX/50M;->upperBound:LX/4xM;

    invoke-virtual {v0}, LX/51R;->c()LX/4xM;

    move-result-object v4

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/51R;

    invoke-virtual {v1}, LX/51R;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-static {p0, v3, v4, v1}, LX/51S;->a(LX/51S;LX/4xM;LX/4xM;Ljava/lang/Object;)V

    .line 825097
    :cond_2
    iget-object v1, v0, LX/51R;->a:LX/50M;

    iget-object v1, v1, LX/50M;->lowerBound:LX/4xM;

    move-object v1, v1

    .line 825098
    iget-object v3, p1, LX/50M;->lowerBound:LX/4xM;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/51R;

    invoke-virtual {v0}, LX/51R;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-static {p0, v1, v3, v0}, LX/51S;->a(LX/51S;LX/4xM;LX/4xM;Ljava/lang/Object;)V

    .line 825099
    :cond_3
    iget-object v0, p0, LX/51S;->a:Ljava/util/NavigableMap;

    iget-object v1, p1, LX/50M;->upperBound:LX/4xM;

    invoke-interface {v0, v1}, Ljava/util/NavigableMap;->lowerEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v1

    .line 825100
    if-eqz v1, :cond_4

    .line 825101
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/51R;

    .line 825102
    invoke-virtual {v0}, LX/51R;->c()LX/4xM;

    move-result-object v2

    iget-object v3, p1, LX/50M;->upperBound:LX/4xM;

    invoke-virtual {v2, v3}, LX/4xM;->a(LX/4xM;)I

    move-result v2

    if-lez v2, :cond_4

    .line 825103
    iget-object v2, p1, LX/50M;->upperBound:LX/4xM;

    invoke-virtual {v0}, LX/51R;->c()LX/4xM;

    move-result-object v3

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/51R;

    invoke-virtual {v0}, LX/51R;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-static {p0, v2, v3, v0}, LX/51S;->a(LX/51S;LX/4xM;LX/4xM;Ljava/lang/Object;)V

    .line 825104
    iget-object v0, p0, LX/51S;->a:Ljava/util/NavigableMap;

    iget-object v1, p1, LX/50M;->lowerBound:LX/4xM;

    invoke-interface {v0, v1}, Ljava/util/NavigableMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 825105
    :cond_4
    iget-object v0, p0, LX/51S;->a:Ljava/util/NavigableMap;

    iget-object v1, p1, LX/50M;->lowerBound:LX/4xM;

    iget-object v2, p1, LX/50M;->upperBound:LX/4xM;

    invoke-interface {v0, v1, v2}, Ljava/util/NavigableMap;->subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedMap;->clear()V

    goto/16 :goto_0
.end method

.method public final b()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "LX/50M",
            "<TK;>;TV;>;"
        }
    .end annotation

    .prologue
    .line 825072
    new-instance v0, LX/51Q;

    iget-object v1, p0, LX/51S;->a:Ljava/util/NavigableMap;

    invoke-interface {v1}, Ljava/util/NavigableMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LX/51Q;-><init>(LX/51S;Ljava/lang/Iterable;)V

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 825066
    instance-of v0, p1, LX/4yY;

    if-eqz v0, :cond_0

    .line 825067
    check-cast p1, LX/4yY;

    .line 825068
    invoke-virtual {p0}, LX/51S;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {p1}, LX/4yY;->b()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 825069
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 825071
    invoke-virtual {p0}, LX/51S;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->hashCode()I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 825070
    iget-object v0, p0, LX/51S;->a:Ljava/util/NavigableMap;

    invoke-interface {v0}, Ljava/util/NavigableMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
