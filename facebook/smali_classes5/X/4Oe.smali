.class public LX/4Oe;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 697567
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    .line 697519
    const/4 v8, 0x0

    .line 697520
    const/4 v5, 0x0

    .line 697521
    const-wide/16 v6, 0x0

    .line 697522
    const/4 v4, 0x0

    .line 697523
    const/4 v3, 0x0

    .line 697524
    const/4 v2, 0x0

    .line 697525
    const/4 v1, 0x0

    .line 697526
    const/4 v0, 0x0

    .line 697527
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->START_OBJECT:LX/15z;

    if-eq v9, v10, :cond_b

    .line 697528
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 697529
    const/4 v0, 0x0

    .line 697530
    :goto_0
    return v0

    .line 697531
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v1, v10, :cond_9

    .line 697532
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v1

    .line 697533
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 697534
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_0

    if-eqz v1, :cond_0

    .line 697535
    const-string v10, "cache_id"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 697536
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    move v5, v1

    goto :goto_1

    .line 697537
    :cond_1
    const-string v10, "debug_info"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 697538
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    move v4, v1

    goto :goto_1

    .line 697539
    :cond_2
    const-string v10, "fetchTimeMs"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 697540
    const/4 v0, 0x1

    .line 697541
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    goto :goto_1

    .line 697542
    :cond_3
    const-string v10, "igPffItems"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 697543
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 697544
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->START_ARRAY:LX/15z;

    if-ne v9, v10, :cond_4

    .line 697545
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_ARRAY:LX/15z;

    if-eq v9, v10, :cond_4

    .line 697546
    invoke-static {p0, p1}, LX/4Of;->b(LX/15w;LX/186;)I

    move-result v9

    .line 697547
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v1, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 697548
    :cond_4
    invoke-static {v1, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v1

    move v1, v1

    .line 697549
    move v9, v1

    goto :goto_1

    .line 697550
    :cond_5
    const-string v10, "short_term_cache_key"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 697551
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    move v8, v1

    goto/16 :goto_1

    .line 697552
    :cond_6
    const-string v10, "title"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 697553
    invoke-static {p0, p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v1

    move v7, v1

    goto/16 :goto_1

    .line 697554
    :cond_7
    const-string v10, "tracking"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 697555
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    move v6, v1

    goto/16 :goto_1

    .line 697556
    :cond_8
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 697557
    :cond_9
    const/4 v1, 0x7

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 697558
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 697559
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 697560
    if-eqz v0, :cond_a

    .line 697561
    const/4 v1, 0x2

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 697562
    :cond_a
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 697563
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 697564
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 697565
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 697566
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :cond_b
    move v9, v4

    move v4, v5

    move v5, v8

    move v8, v3

    move-wide v12, v6

    move v7, v2

    move v6, v1

    move-wide v2, v12

    goto/16 :goto_1
.end method

.method public static a(LX/15w;S)LX/15i;
    .locals 5

    .prologue
    .line 697607
    const/4 v0, 0x1

    const/4 v4, 0x0

    .line 697608
    new-instance v2, LX/186;

    const/16 v1, 0x80

    invoke-direct {v2, v1}, LX/186;-><init>(I)V

    .line 697609
    invoke-static {p0, v2}, LX/4Oe;->a(LX/15w;LX/186;)I

    move-result v1

    .line 697610
    if-eqz v0, :cond_0

    .line 697611
    const/4 v3, 0x2

    invoke-virtual {v2, v3}, LX/186;->c(I)V

    .line 697612
    invoke-virtual {v2, v4, p1, v4}, LX/186;->a(ISI)V

    .line 697613
    const/4 v3, 0x1

    invoke-virtual {v2, v3, v1}, LX/186;->b(II)V

    .line 697614
    invoke-virtual {v2}, LX/186;->d()I

    move-result v1

    .line 697615
    :cond_0
    invoke-virtual {v2, v1}, LX/186;->d(I)V

    .line 697616
    invoke-static {v2}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v1

    move-object v0, v1

    .line 697617
    return-object v0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 697568
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 697569
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 697570
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 697571
    const-string v0, "name"

    const-string v1, "InstagramPhotosFromFriendsFeedUnit"

    invoke-virtual {p2, v0, v1}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 697572
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 697573
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 697574
    if-eqz v0, :cond_0

    .line 697575
    const-string v1, "cache_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 697576
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 697577
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 697578
    if-eqz v0, :cond_1

    .line 697579
    const-string v1, "debug_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 697580
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 697581
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 697582
    cmp-long v2, v0, v2

    if-eqz v2, :cond_2

    .line 697583
    const-string v2, "fetchTimeMs"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 697584
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 697585
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 697586
    if-eqz v0, :cond_4

    .line 697587
    const-string v1, "igPffItems"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 697588
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 697589
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_3

    .line 697590
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/4Of;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 697591
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 697592
    :cond_3
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 697593
    :cond_4
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 697594
    if-eqz v0, :cond_5

    .line 697595
    const-string v1, "short_term_cache_key"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 697596
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 697597
    :cond_5
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 697598
    if-eqz v0, :cond_6

    .line 697599
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 697600
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 697601
    :cond_6
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 697602
    if-eqz v0, :cond_7

    .line 697603
    const-string v1, "tracking"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 697604
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 697605
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 697606
    return-void
.end method
