.class public final LX/50M;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Rl;
.implements Ljava/io/Serializable;


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<C::",
        "Ljava/lang/Comparable;",
        ">",
        "Ljava/lang/Object;",
        "LX/0Rl",
        "<TC;>;",
        "Ljava/io/Serializable;"
    }
.end annotation


# static fields
.field public static final a:LX/1sm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1sm",
            "<",
            "LX/50M",
            "<*>;>;"
        }
    .end annotation
.end field

.field public static final b:LX/0QK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QK",
            "<",
            "LX/50M;",
            "LX/4xM;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:LX/0QK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QK",
            "<",
            "LX/50M;",
            "LX/4xM;",
            ">;"
        }
    .end annotation
.end field

.field public static final d:LX/50M;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/50M",
            "<",
            "Ljava/lang/Comparable;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final lowerBound:LX/4xM;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4xM",
            "<TC;>;"
        }
    .end annotation
.end field

.field public final upperBound:LX/4xM;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4xM",
            "<TC;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 823674
    new-instance v0, LX/50I;

    invoke-direct {v0}, LX/50I;-><init>()V

    sput-object v0, LX/50M;->b:LX/0QK;

    .line 823675
    new-instance v0, LX/50J;

    invoke-direct {v0}, LX/50J;-><init>()V

    sput-object v0, LX/50M;->c:LX/0QK;

    .line 823676
    new-instance v0, LX/50L;

    invoke-direct {v0}, LX/50L;-><init>()V

    sput-object v0, LX/50M;->a:LX/1sm;

    .line 823677
    new-instance v0, LX/50M;

    sget-object v1, LX/4xP;->a:LX/4xP;

    sget-object v2, LX/4xN;->a:LX/4xN;

    invoke-direct {v0, v1, v2}, LX/50M;-><init>(LX/4xM;LX/4xM;)V

    sput-object v0, LX/50M;->d:LX/50M;

    return-void
.end method

.method private constructor <init>(LX/4xM;LX/4xM;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4xM",
            "<TC;>;",
            "LX/4xM",
            "<TC;>;)V"
        }
    .end annotation

    .prologue
    .line 823668
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 823669
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4xM;

    iput-object v0, p0, LX/50M;->lowerBound:LX/4xM;

    .line 823670
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4xM;

    iput-object v0, p0, LX/50M;->upperBound:LX/4xM;

    .line 823671
    invoke-virtual {p1, p2}, LX/4xM;->a(LX/4xM;)I

    move-result v0

    if-gtz v0, :cond_0

    sget-object v0, LX/4xN;->a:LX/4xN;

    if-eq p1, v0, :cond_0

    sget-object v0, LX/4xP;->a:LX/4xP;

    if-ne p2, v0, :cond_1

    .line 823672
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid range: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1, p2}, LX/50M;->b(LX/4xM;LX/4xM;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 823673
    :cond_1
    return-void
.end method

.method public static a(LX/4xM;LX/4xM;)LX/50M;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<C::",
            "Ljava/lang/Comparable",
            "<*>;>(",
            "LX/4xM",
            "<TC;>;",
            "LX/4xM",
            "<TC;>;)",
            "LX/50M",
            "<TC;>;"
        }
    .end annotation

    .prologue
    .line 823667
    new-instance v0, LX/50M;

    invoke-direct {v0, p0, p1}, LX/50M;-><init>(LX/4xM;LX/4xM;)V

    return-object v0
.end method

.method public static a(Ljava/lang/Comparable;LX/4xG;)LX/50M;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<C::",
            "Ljava/lang/Comparable",
            "<*>;>(TC;",
            "LX/4xG;",
            ")",
            "LX/50M",
            "<TC;>;"
        }
    .end annotation

    .prologue
    .line 823661
    sget-object v0, LX/50K;->a:[I

    invoke-virtual {p1}, LX/4xG;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 823662
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 823663
    :pswitch_0
    sget-object v0, LX/4xP;->a:LX/4xP;

    invoke-static {p0}, LX/4xM;->b(Ljava/lang/Comparable;)LX/4xM;

    move-result-object v1

    invoke-static {v0, v1}, LX/50M;->a(LX/4xM;LX/4xM;)LX/50M;

    move-result-object v0

    move-object v0, v0

    .line 823664
    :goto_0
    return-object v0

    .line 823665
    :pswitch_1
    sget-object v0, LX/4xP;->a:LX/4xP;

    invoke-static {p0}, LX/4xM;->c(Ljava/lang/Comparable;)LX/4xM;

    move-result-object v1

    invoke-static {v0, v1}, LX/50M;->a(LX/4xM;LX/4xM;)LX/50M;

    move-result-object v0

    move-object v0, v0

    .line 823666
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Ljava/lang/Comparable;LX/4xG;Ljava/lang/Comparable;LX/4xG;)LX/50M;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<C::",
            "Ljava/lang/Comparable",
            "<*>;>(TC;",
            "LX/4xG;",
            "TC;",
            "LX/4xG;",
            ")",
            "LX/50M",
            "<TC;>;"
        }
    .end annotation

    .prologue
    .line 823654
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 823655
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 823656
    sget-object v0, LX/4xG;->OPEN:LX/4xG;

    if-ne p1, v0, :cond_0

    invoke-static {p0}, LX/4xM;->c(Ljava/lang/Comparable;)LX/4xM;

    move-result-object v0

    .line 823657
    :goto_0
    sget-object v1, LX/4xG;->OPEN:LX/4xG;

    if-ne p3, v1, :cond_1

    invoke-static {p2}, LX/4xM;->b(Ljava/lang/Comparable;)LX/4xM;

    move-result-object v1

    .line 823658
    :goto_1
    invoke-static {v0, v1}, LX/50M;->a(LX/4xM;LX/4xM;)LX/50M;

    move-result-object v0

    return-object v0

    .line 823659
    :cond_0
    invoke-static {p0}, LX/4xM;->b(Ljava/lang/Comparable;)LX/4xM;

    move-result-object v0

    goto :goto_0

    .line 823660
    :cond_1
    invoke-static {p2}, LX/4xM;->c(Ljava/lang/Comparable;)LX/4xM;

    move-result-object v1

    goto :goto_1
.end method

.method public static a(Ljava/lang/Comparable;Ljava/lang/Comparable;)LX/50M;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<C::",
            "Ljava/lang/Comparable",
            "<*>;>(TC;TC;)",
            "LX/50M",
            "<TC;>;"
        }
    .end annotation

    .prologue
    .line 823631
    invoke-static {p0}, LX/4xM;->b(Ljava/lang/Comparable;)LX/4xM;

    move-result-object v0

    invoke-static {p1}, LX/4xM;->c(Ljava/lang/Comparable;)LX/4xM;

    move-result-object v1

    invoke-static {v0, v1}, LX/50M;->a(LX/4xM;LX/4xM;)LX/50M;

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/lang/Comparable;LX/4xG;)LX/50M;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<C::",
            "Ljava/lang/Comparable",
            "<*>;>(TC;",
            "LX/4xG;",
            ")",
            "LX/50M",
            "<TC;>;"
        }
    .end annotation

    .prologue
    .line 823648
    sget-object v0, LX/50K;->a:[I

    invoke-virtual {p1}, LX/4xG;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 823649
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 823650
    :pswitch_0
    invoke-static {p0}, LX/4xM;->c(Ljava/lang/Comparable;)LX/4xM;

    move-result-object v0

    sget-object v1, LX/4xN;->a:LX/4xN;

    invoke-static {v0, v1}, LX/50M;->a(LX/4xM;LX/4xM;)LX/50M;

    move-result-object v0

    move-object v0, v0

    .line 823651
    :goto_0
    return-object v0

    .line 823652
    :pswitch_1
    invoke-static {p0}, LX/4xM;->b(Ljava/lang/Comparable;)LX/4xM;

    move-result-object v0

    sget-object v1, LX/4xN;->a:LX/4xN;

    invoke-static {v0, v1}, LX/50M;->a(LX/4xM;LX/4xM;)LX/50M;

    move-result-object v0

    move-object v0, v0

    .line 823653
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static b(Ljava/lang/Comparable;Ljava/lang/Comparable;)LX/50M;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<C::",
            "Ljava/lang/Comparable",
            "<*>;>(TC;TC;)",
            "LX/50M",
            "<TC;>;"
        }
    .end annotation

    .prologue
    .line 823647
    invoke-static {p0}, LX/4xM;->b(Ljava/lang/Comparable;)LX/4xM;

    move-result-object v0

    invoke-static {p1}, LX/4xM;->b(Ljava/lang/Comparable;)LX/4xM;

    move-result-object v1

    invoke-static {v0, v1}, LX/50M;->a(LX/4xM;LX/4xM;)LX/50M;

    move-result-object v0

    return-object v0
.end method

.method private static b(LX/4xM;LX/4xM;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4xM",
            "<*>;",
            "LX/4xM",
            "<*>;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 823642
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x10

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 823643
    invoke-virtual {p0, v0}, LX/4xM;->a(Ljava/lang/StringBuilder;)V

    .line 823644
    const/16 v1, 0x2025

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 823645
    invoke-virtual {p1, v0}, LX/4xM;->b(Ljava/lang/StringBuilder;)V

    .line 823646
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static c(Ljava/lang/Comparable;Ljava/lang/Comparable;)I
    .locals 1

    .prologue
    .line 823641
    invoke-interface {p0, p1}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Ljava/lang/Comparable;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TC;)Z"
        }
    .end annotation

    .prologue
    .line 823639
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 823640
    iget-object v0, p0, LX/50M;->lowerBound:LX/4xM;

    invoke-virtual {v0, p1}, LX/4xM;->a(Ljava/lang/Comparable;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/50M;->upperBound:LX/4xM;

    invoke-virtual {v0, p1}, LX/4xM;->a(Ljava/lang/Comparable;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final apply(Ljava/lang/Object;)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 823678
    check-cast p1, Ljava/lang/Comparable;

    .line 823679
    invoke-virtual {p0, p1}, LX/50M;->a(Ljava/lang/Comparable;)Z

    move-result v0

    return v0
.end method

.method public final b(LX/50M;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/50M",
            "<TC;>;)Z"
        }
    .end annotation

    .prologue
    .line 823612
    iget-object v0, p0, LX/50M;->lowerBound:LX/4xM;

    iget-object v1, p1, LX/50M;->upperBound:LX/4xM;

    invoke-virtual {v0, v1}, LX/4xM;->a(LX/4xM;)I

    move-result v0

    if-gtz v0, :cond_0

    iget-object v0, p1, LX/50M;->lowerBound:LX/4xM;

    iget-object v1, p0, LX/50M;->upperBound:LX/4xM;

    invoke-virtual {v0, v1}, LX/4xM;->a(LX/4xM;)I

    move-result v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(LX/50M;)LX/50M;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/50M",
            "<TC;>;)",
            "LX/50M",
            "<TC;>;"
        }
    .end annotation

    .prologue
    .line 823613
    iget-object v0, p0, LX/50M;->lowerBound:LX/4xM;

    iget-object v1, p1, LX/50M;->lowerBound:LX/4xM;

    invoke-virtual {v0, v1}, LX/4xM;->a(LX/4xM;)I

    move-result v0

    .line 823614
    iget-object v1, p0, LX/50M;->upperBound:LX/4xM;

    iget-object v2, p1, LX/50M;->upperBound:LX/4xM;

    invoke-virtual {v1, v2}, LX/4xM;->a(LX/4xM;)I

    move-result v2

    .line 823615
    if-ltz v0, :cond_0

    if-gtz v2, :cond_0

    .line 823616
    :goto_0
    return-object p0

    .line 823617
    :cond_0
    if-gtz v0, :cond_1

    if-ltz v2, :cond_1

    move-object p0, p1

    .line 823618
    goto :goto_0

    .line 823619
    :cond_1
    if-ltz v0, :cond_2

    iget-object v0, p0, LX/50M;->lowerBound:LX/4xM;

    move-object v1, v0

    .line 823620
    :goto_1
    if-gtz v2, :cond_3

    iget-object v0, p0, LX/50M;->upperBound:LX/4xM;

    .line 823621
    :goto_2
    invoke-static {v1, v0}, LX/50M;->a(LX/4xM;LX/4xM;)LX/50M;

    move-result-object p0

    goto :goto_0

    .line 823622
    :cond_2
    iget-object v0, p1, LX/50M;->lowerBound:LX/4xM;

    move-object v1, v0

    goto :goto_1

    .line 823623
    :cond_3
    iget-object v0, p1, LX/50M;->upperBound:LX/4xM;

    goto :goto_2
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 823624
    iget-object v0, p0, LX/50M;->lowerBound:LX/4xM;

    sget-object v1, LX/4xP;->a:LX/4xP;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Ljava/lang/Comparable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TC;"
        }
    .end annotation

    .prologue
    .line 823625
    iget-object v0, p0, LX/50M;->lowerBound:LX/4xM;

    invoke-virtual {v0}, LX/4xM;->c()Ljava/lang/Comparable;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 823626
    instance-of v1, p1, LX/50M;

    if-eqz v1, :cond_0

    .line 823627
    check-cast p1, LX/50M;

    .line 823628
    iget-object v1, p0, LX/50M;->lowerBound:LX/4xM;

    iget-object v2, p1, LX/50M;->lowerBound:LX/4xM;

    invoke-virtual {v1, v2}, LX/4xM;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/50M;->upperBound:LX/4xM;

    iget-object v2, p1, LX/50M;->upperBound:LX/4xM;

    invoke-virtual {v1, v2}, LX/4xM;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 823629
    :cond_0
    return v0
.end method

.method public final f()Z
    .locals 2

    .prologue
    .line 823630
    iget-object v0, p0, LX/50M;->upperBound:LX/4xM;

    sget-object v1, LX/4xN;->a:LX/4xN;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Ljava/lang/Comparable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TC;"
        }
    .end annotation

    .prologue
    .line 823632
    iget-object v0, p0, LX/50M;->upperBound:LX/4xM;

    invoke-virtual {v0}, LX/4xM;->c()Ljava/lang/Comparable;

    move-result-object v0

    return-object v0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 823633
    iget-object v0, p0, LX/50M;->lowerBound:LX/4xM;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, LX/50M;->upperBound:LX/4xM;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final i()Z
    .locals 2

    .prologue
    .line 823634
    iget-object v0, p0, LX/50M;->lowerBound:LX/4xM;

    iget-object v1, p0, LX/50M;->upperBound:LX/4xM;

    invoke-virtual {v0, v1}, LX/4xM;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final readResolve()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 823635
    sget-object v0, LX/50M;->d:LX/50M;

    invoke-virtual {p0, v0}, LX/50M;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 823636
    sget-object v0, LX/50M;->d:LX/50M;

    move-object p0, v0

    .line 823637
    :cond_0
    return-object p0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 823638
    iget-object v0, p0, LX/50M;->lowerBound:LX/4xM;

    iget-object v1, p0, LX/50M;->upperBound:LX/4xM;

    invoke-static {v0, v1}, LX/50M;->b(LX/4xM;LX/4xM;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
