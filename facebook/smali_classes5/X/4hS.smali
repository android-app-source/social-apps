.class public final enum LX/4hS;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/4hS;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/4hS;

.field public static final enum FROM_DEFAULT_COUNTRY:LX/4hS;

.field public static final enum FROM_NUMBER_WITHOUT_PLUS_SIGN:LX/4hS;

.field public static final enum FROM_NUMBER_WITH_IDD:LX/4hS;

.field public static final enum FROM_NUMBER_WITH_PLUS_SIGN:LX/4hS;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 801297
    new-instance v0, LX/4hS;

    const-string v1, "FROM_NUMBER_WITH_PLUS_SIGN"

    invoke-direct {v0, v1, v2}, LX/4hS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4hS;->FROM_NUMBER_WITH_PLUS_SIGN:LX/4hS;

    .line 801298
    new-instance v0, LX/4hS;

    const-string v1, "FROM_NUMBER_WITH_IDD"

    invoke-direct {v0, v1, v3}, LX/4hS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4hS;->FROM_NUMBER_WITH_IDD:LX/4hS;

    .line 801299
    new-instance v0, LX/4hS;

    const-string v1, "FROM_NUMBER_WITHOUT_PLUS_SIGN"

    invoke-direct {v0, v1, v4}, LX/4hS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4hS;->FROM_NUMBER_WITHOUT_PLUS_SIGN:LX/4hS;

    .line 801300
    new-instance v0, LX/4hS;

    const-string v1, "FROM_DEFAULT_COUNTRY"

    invoke-direct {v0, v1, v5}, LX/4hS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4hS;->FROM_DEFAULT_COUNTRY:LX/4hS;

    .line 801301
    const/4 v0, 0x4

    new-array v0, v0, [LX/4hS;

    sget-object v1, LX/4hS;->FROM_NUMBER_WITH_PLUS_SIGN:LX/4hS;

    aput-object v1, v0, v2

    sget-object v1, LX/4hS;->FROM_NUMBER_WITH_IDD:LX/4hS;

    aput-object v1, v0, v3

    sget-object v1, LX/4hS;->FROM_NUMBER_WITHOUT_PLUS_SIGN:LX/4hS;

    aput-object v1, v0, v4

    sget-object v1, LX/4hS;->FROM_DEFAULT_COUNTRY:LX/4hS;

    aput-object v1, v0, v5

    sput-object v0, LX/4hS;->$VALUES:[LX/4hS;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 801302
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/4hS;
    .locals 1

    .prologue
    .line 801303
    const-class v0, LX/4hS;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/4hS;

    return-object v0
.end method

.method public static values()[LX/4hS;
    .locals 1

    .prologue
    .line 801304
    sget-object v0, LX/4hS;->$VALUES:[LX/4hS;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/4hS;

    return-object v0
.end method
