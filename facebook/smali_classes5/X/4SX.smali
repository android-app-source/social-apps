.class public LX/4SX;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 714708
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    .line 714740
    const/4 v8, 0x0

    .line 714741
    const/4 v7, 0x0

    .line 714742
    const/4 v6, 0x0

    .line 714743
    const/4 v3, 0x0

    .line 714744
    const-wide/16 v4, 0x0

    .line 714745
    const/4 v2, 0x0

    .line 714746
    const/4 v1, 0x0

    .line 714747
    const/4 v0, 0x0

    .line 714748
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->START_OBJECT:LX/15z;

    if-eq v9, v10, :cond_a

    .line 714749
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 714750
    const/4 v0, 0x0

    .line 714751
    :goto_0
    return v0

    .line 714752
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v1, v10, :cond_8

    .line 714753
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v1

    .line 714754
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 714755
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_0

    if-eqz v1, :cond_0

    .line 714756
    const-string v10, "actor"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 714757
    invoke-static {p0, p1}, LX/2bM;->a(LX/15w;LX/186;)I

    move-result v1

    move v9, v1

    goto :goto_1

    .line 714758
    :cond_1
    const-string v10, "first_question"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 714759
    invoke-static {p0, p1}, LX/4SS;->a(LX/15w;LX/186;)I

    move-result v1

    move v8, v1

    goto :goto_1

    .line 714760
    :cond_2
    const-string v10, "id"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 714761
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    move v5, v1

    goto :goto_1

    .line 714762
    :cond_3
    const-string v10, "questions"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 714763
    invoke-static {p0, p1}, LX/4SS;->b(LX/15w;LX/186;)I

    move-result v1

    move v4, v1

    goto :goto_1

    .line 714764
    :cond_4
    const-string v10, "research_poll_complete_time"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 714765
    const/4 v0, 0x1

    .line 714766
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    goto :goto_1

    .line 714767
    :cond_5
    const-string v10, "url"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 714768
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    move v7, v1

    goto :goto_1

    .line 714769
    :cond_6
    const-string v10, "user_question_history"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 714770
    invoke-static {p0, p1}, LX/4SY;->a(LX/15w;LX/186;)I

    move-result v1

    move v6, v1

    goto/16 :goto_1

    .line 714771
    :cond_7
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 714772
    :cond_8
    const/16 v1, 0x8

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 714773
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v9}, LX/186;->b(II)V

    .line 714774
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v8}, LX/186;->b(II)V

    .line 714775
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 714776
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 714777
    if-eqz v0, :cond_9

    .line 714778
    const/4 v1, 0x5

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 714779
    :cond_9
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 714780
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 714781
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :cond_a
    move v9, v8

    move v8, v7

    move v7, v2

    move v12, v3

    move-wide v2, v4

    move v5, v6

    move v4, v12

    move v6, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 714709
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 714710
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 714711
    if-eqz v0, :cond_0

    .line 714712
    const-string v1, "actor"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 714713
    invoke-static {p0, v0, p2, p3}, LX/2bM;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 714714
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 714715
    if-eqz v0, :cond_1

    .line 714716
    const-string v1, "first_question"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 714717
    invoke-static {p0, v0, p2, p3}, LX/4SS;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 714718
    :cond_1
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 714719
    if-eqz v0, :cond_2

    .line 714720
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 714721
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 714722
    :cond_2
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 714723
    if-eqz v0, :cond_3

    .line 714724
    const-string v1, "questions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 714725
    invoke-static {p0, v0, p2, p3}, LX/4SS;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 714726
    :cond_3
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 714727
    cmp-long v2, v0, v2

    if-eqz v2, :cond_4

    .line 714728
    const-string v2, "research_poll_complete_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 714729
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 714730
    :cond_4
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 714731
    if-eqz v0, :cond_5

    .line 714732
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 714733
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 714734
    :cond_5
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 714735
    if-eqz v0, :cond_6

    .line 714736
    const-string v1, "user_question_history"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 714737
    invoke-static {p0, v0, p2, p3}, LX/4SY;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 714738
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 714739
    return-void
.end method
