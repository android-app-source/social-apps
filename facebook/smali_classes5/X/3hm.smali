.class public LX/3hm;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ":",
        "LX/3iA;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:Landroid/os/Handler;

.field private final b:Ljava/lang/Runnable;

.field public final c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<TV;>;"
        }
    .end annotation
.end field

.field public final d:LX/1AY;


# direct methods
.method public constructor <init>(LX/1AY;Landroid/os/Handler;)V
    .locals 1
    .param p2    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 628029
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 628030
    iput-object p1, p0, LX/3hm;->d:LX/1AY;

    .line 628031
    iput-object p2, p0, LX/3hm;->a:Landroid/os/Handler;

    .line 628032
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/3hm;->c:Ljava/util/ArrayList;

    .line 628033
    new-instance v0, Lcom/facebook/feedplugins/multishare/MultiShareProductItemSlideshowAutoplayManager$1;

    invoke-direct {v0, p0}, Lcom/facebook/feedplugins/multishare/MultiShareProductItemSlideshowAutoplayManager$1;-><init>(LX/3hm;)V

    iput-object v0, p0, LX/3hm;->b:Ljava/lang/Runnable;

    .line 628034
    return-void
.end method

.method public static b(LX/3hm;)V
    .locals 5

    .prologue
    .line 628035
    iget-object v0, p0, LX/3hm;->a:Landroid/os/Handler;

    iget-object v1, p0, LX/3hm;->b:Ljava/lang/Runnable;

    const-wide/16 v2, 0xc8

    const v4, -0x9a72f47

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 628036
    return-void
.end method
