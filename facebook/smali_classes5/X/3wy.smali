.class public final LX/3wy;
.super LX/3vn;
.source ""


# instance fields
.field public f:Z

.field private g:Z

.field public h:Z

.field public i:LX/3sU;

.field private j:LX/3tb;


# direct methods
.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 2

    .prologue
    .line 656572
    const/4 v0, 0x0

    const v1, 0x7f010049

    invoke-direct {p0, p1, v0, v1}, LX/3vn;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 656573
    iput-boolean p2, p0, LX/3wy;->g:Z

    .line 656574
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/3wy;->setCacheColorHint(I)V

    .line 656575
    return-void
.end method

.method private static a(LX/3wy;Landroid/view/View;IFF)V
    .locals 9

    .prologue
    const/4 v0, 0x1

    .line 656614
    iput-boolean v0, p0, LX/3wy;->h:Z

    .line 656615
    invoke-virtual {p0, v0}, LX/3wy;->setPressed(Z)V

    .line 656616
    invoke-virtual {p0}, LX/3wy;->layoutChildren()V

    .line 656617
    invoke-virtual {p0, p2}, LX/3wy;->setSelection(I)V

    .line 656618
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 656619
    invoke-virtual {p0}, LX/3vn;->getSelector()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 656620
    if-eqz v3, :cond_4

    const/4 v2, -0x1

    if-eq p2, v2, :cond_4

    move v2, v0

    .line 656621
    :goto_0
    if-eqz v2, :cond_0

    .line 656622
    invoke-virtual {v3, v1, v1}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    .line 656623
    :cond_0
    iget-object v4, p0, LX/3vn;->a:Landroid/graphics/Rect;

    .line 656624
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v5

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v6

    invoke-virtual {p1}, Landroid/view/View;->getRight()I

    move-result v7

    invoke-virtual {p1}, Landroid/view/View;->getBottom()I

    move-result v8

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/graphics/Rect;->set(IIII)V

    .line 656625
    iget v5, v4, Landroid/graphics/Rect;->left:I

    iget v6, p0, LX/3vn;->b:I

    sub-int/2addr v5, v6

    iput v5, v4, Landroid/graphics/Rect;->left:I

    .line 656626
    iget v5, v4, Landroid/graphics/Rect;->top:I

    iget v6, p0, LX/3vn;->c:I

    sub-int/2addr v5, v6

    iput v5, v4, Landroid/graphics/Rect;->top:I

    .line 656627
    iget v5, v4, Landroid/graphics/Rect;->right:I

    iget v6, p0, LX/3vn;->d:I

    add-int/2addr v5, v6

    iput v5, v4, Landroid/graphics/Rect;->right:I

    .line 656628
    iget v5, v4, Landroid/graphics/Rect;->bottom:I

    iget v6, p0, LX/3vn;->e:I

    add-int/2addr v5, v6

    iput v5, v4, Landroid/graphics/Rect;->bottom:I

    .line 656629
    :try_start_0
    iget-object v4, p0, LX/3vn;->g:Ljava/lang/reflect/Field;

    invoke-virtual {v4, p0}, Ljava/lang/reflect/Field;->getBoolean(Ljava/lang/Object;)Z

    move-result v4

    .line 656630
    invoke-virtual {p1}, Landroid/view/View;->isEnabled()Z

    move-result v5

    if-eq v5, v4, :cond_1

    .line 656631
    iget-object v5, p0, LX/3vn;->g:Ljava/lang/reflect/Field;

    if-nez v4, :cond_6

    const/4 v4, 0x1

    :goto_1
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v5, p0, v4}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 656632
    const/4 v4, -0x1

    if-eq p2, v4, :cond_1

    .line 656633
    invoke-virtual {p0}, LX/3vn;->refreshDrawableState()V
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    .line 656634
    :cond_1
    :goto_2
    if-eqz v2, :cond_2

    .line 656635
    iget-object v2, p0, LX/3vn;->a:Landroid/graphics/Rect;

    .line 656636
    invoke-virtual {v2}, Landroid/graphics/Rect;->exactCenterX()F

    move-result v4

    .line 656637
    invoke-virtual {v2}, Landroid/graphics/Rect;->exactCenterY()F

    move-result v2

    .line 656638
    invoke-virtual {p0}, LX/3vn;->getVisibility()I

    move-result v5

    if-nez v5, :cond_5

    :goto_3
    invoke-virtual {v3, v0, v1}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    .line 656639
    invoke-static {v3, v4, v2}, LX/1d5;->a(Landroid/graphics/drawable/Drawable;FF)V

    .line 656640
    :cond_2
    invoke-virtual {p0}, LX/3vn;->getSelector()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 656641
    if-eqz v0, :cond_3

    const/4 v1, -0x1

    if-eq p2, v1, :cond_3

    .line 656642
    invoke-static {v0, p3, p4}, LX/1d5;->a(Landroid/graphics/drawable/Drawable;FF)V

    .line 656643
    :cond_3
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/3vn;->setSelectorEnabled(Z)V

    .line 656644
    invoke-virtual {p0}, LX/3wy;->refreshDrawableState()V

    .line 656645
    return-void

    :cond_4
    move v2, v1

    .line 656646
    goto/16 :goto_0

    :cond_5
    move v0, v1

    .line 656647
    goto :goto_3

    .line 656648
    :cond_6
    const/4 v4, 0x0

    goto :goto_1

    .line 656649
    :catch_0
    move-exception v4

    .line 656650
    invoke-virtual {v4}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_2
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 656613
    iget-boolean v0, p0, LX/3wy;->h:Z

    if-nez v0, :cond_0

    invoke-super {p0}, LX/3vn;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/view/MotionEvent;I)Z
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 656580
    invoke-static {p1}, LX/2xd;->a(Landroid/view/MotionEvent;)I

    move-result v3

    .line 656581
    packed-switch v3, :pswitch_data_0

    :cond_0
    :goto_0
    move v0, v1

    move v3, v2

    .line 656582
    :goto_1
    if-eqz v3, :cond_1

    if-eqz v0, :cond_2

    .line 656583
    :cond_1
    const/4 v0, 0x0

    .line 656584
    iput-boolean v0, p0, LX/3wy;->h:Z

    .line 656585
    invoke-virtual {p0, v0}, LX/3wy;->setPressed(Z)V

    .line 656586
    invoke-virtual {p0}, LX/3vn;->drawableStateChanged()V

    .line 656587
    iget-object v0, p0, LX/3wy;->i:LX/3sU;

    if-eqz v0, :cond_2

    .line 656588
    iget-object v0, p0, LX/3wy;->i:LX/3sU;

    invoke-virtual {v0}, LX/3sU;->a()V

    .line 656589
    const/4 v0, 0x0

    iput-object v0, p0, LX/3wy;->i:LX/3sU;

    .line 656590
    :cond_2
    if-eqz v3, :cond_7

    .line 656591
    iget-object v0, p0, LX/3wy;->j:LX/3tb;

    if-nez v0, :cond_3

    .line 656592
    new-instance v0, LX/3tb;

    invoke-direct {v0, p0}, LX/3tb;-><init>(Landroid/widget/ListView;)V

    iput-object v0, p0, LX/3wy;->j:LX/3tb;

    .line 656593
    :cond_3
    iget-object v0, p0, LX/3wy;->j:LX/3tb;

    invoke-virtual {v0, v2}, LX/3tG;->a(Z)LX/3tG;

    .line 656594
    iget-object v0, p0, LX/3wy;->j:LX/3tb;

    invoke-virtual {v0, p0, p1}, LX/3tG;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    .line 656595
    :cond_4
    :goto_2
    return v3

    :pswitch_0
    move v0, v1

    move v3, v1

    .line 656596
    goto :goto_1

    :pswitch_1
    move v0, v1

    .line 656597
    :goto_3
    invoke-virtual {p1, p2}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v4

    .line 656598
    if-gez v4, :cond_5

    move v0, v1

    move v3, v1

    .line 656599
    goto :goto_1

    .line 656600
    :cond_5
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getX(I)F

    move-result v5

    float-to-int v5, v5

    .line 656601
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getY(I)F

    move-result v4

    float-to-int v4, v4

    .line 656602
    invoke-virtual {p0, v5, v4}, LX/3wy;->pointToPosition(II)I

    move-result v6

    .line 656603
    const/4 v7, -0x1

    if-ne v6, v7, :cond_6

    move v3, v0

    move v0, v2

    .line 656604
    goto :goto_1

    .line 656605
    :cond_6
    invoke-virtual {p0}, LX/3wy;->getFirstVisiblePosition()I

    move-result v0

    sub-int v0, v6, v0

    invoke-virtual {p0, v0}, LX/3wy;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 656606
    int-to-float v5, v5

    int-to-float v4, v4

    invoke-static {p0, v0, v6, v5, v4}, LX/3wy;->a(LX/3wy;Landroid/view/View;IFF)V

    .line 656607
    if-ne v3, v2, :cond_0

    .line 656608
    invoke-virtual {p0, v6}, LX/3wy;->getItemIdAtPosition(I)J

    move-result-wide v8

    .line 656609
    invoke-virtual {p0, v0, v6, v8, v9}, LX/3wy;->performItemClick(Landroid/view/View;IJ)Z

    .line 656610
    goto :goto_0

    .line 656611
    :cond_7
    iget-object v0, p0, LX/3wy;->j:LX/3tb;

    if-eqz v0, :cond_4

    .line 656612
    iget-object v0, p0, LX/3wy;->j:LX/3tb;

    invoke-virtual {v0, v1}, LX/3tG;->a(Z)LX/3tG;

    goto :goto_2

    :pswitch_2
    move v0, v2

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public final hasFocus()Z
    .locals 1

    .prologue
    .line 656579
    iget-boolean v0, p0, LX/3wy;->g:Z

    if-nez v0, :cond_0

    invoke-super {p0}, LX/3vn;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasWindowFocus()Z
    .locals 1

    .prologue
    .line 656578
    iget-boolean v0, p0, LX/3wy;->g:Z

    if-nez v0, :cond_0

    invoke-super {p0}, LX/3vn;->hasWindowFocus()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isFocused()Z
    .locals 1

    .prologue
    .line 656577
    iget-boolean v0, p0, LX/3wy;->g:Z

    if-nez v0, :cond_0

    invoke-super {p0}, LX/3vn;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInTouchMode()Z
    .locals 1

    .prologue
    .line 656576
    iget-boolean v0, p0, LX/3wy;->g:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/3wy;->f:Z

    if-nez v0, :cond_1

    :cond_0
    invoke-super {p0}, LX/3vn;->isInTouchMode()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
