.class public LX/3lt;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Lcom/facebook/common/perftest/DrawFrameLogger;


# direct methods
.method public constructor <init>(Lcom/facebook/common/perftest/DrawFrameLogger;Lcom/facebook/common/perftest/PerfTestConfig;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 635151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 635152
    invoke-static {}, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->a()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 635153
    iput-object p1, p0, LX/3lt;->a:Lcom/facebook/common/perftest/DrawFrameLogger;

    .line 635154
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .prologue
    .line 635155
    packed-switch p1, :pswitch_data_0

    .line 635156
    :goto_0
    return-void

    .line 635157
    :pswitch_0
    iget-object v0, p0, LX/3lt;->a:Lcom/facebook/common/perftest/DrawFrameLogger;

    invoke-virtual {v0}, Lcom/facebook/common/perftest/DrawFrameLogger;->b()V

    goto :goto_0

    .line 635158
    :pswitch_1
    iget-object v0, p0, LX/3lt;->a:Lcom/facebook/common/perftest/DrawFrameLogger;

    invoke-virtual {v0}, Lcom/facebook/common/perftest/DrawFrameLogger;->a()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
