.class public final LX/50o;
.super LX/50n;
.source ""

# interfaces
.implements Ljava/util/NavigableSet;


# annotations
.annotation build Lcom/google/common/annotations/GwtIncompatible;
    value = "Navigable"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "LX/50n",
        "<TE;>;",
        "Ljava/util/NavigableSet",
        "<TE;>;"
    }
.end annotation


# direct methods
.method public constructor <init>(LX/4x9;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4x9",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 824099
    invoke-direct {p0, p1}, LX/50n;-><init>(LX/4x9;)V

    .line 824100
    return-void
.end method


# virtual methods
.method public final ceiling(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)TE;"
        }
    .end annotation

    .prologue
    .line 824097
    iget-object v0, p0, LX/50n;->a:LX/4x9;

    move-object v0, v0

    .line 824098
    sget-object v1, LX/4xG;->CLOSED:LX/4xG;

    invoke-interface {v0, p1, v1}, LX/4x9;->b(Ljava/lang/Object;LX/4xG;)LX/4x9;

    move-result-object v0

    invoke-interface {v0}, LX/4x9;->h()LX/4wx;

    move-result-object v0

    invoke-static {v0}, LX/50p;->d(LX/4wx;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final descendingIterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 824096
    invoke-virtual {p0}, LX/50o;->descendingSet()Ljava/util/NavigableSet;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/NavigableSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public final descendingSet()Ljava/util/NavigableSet;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/NavigableSet",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 824084
    new-instance v0, LX/50o;

    .line 824085
    iget-object v1, p0, LX/50n;->a:LX/4x9;

    move-object v1, v1

    .line 824086
    invoke-interface {v1}, LX/4x9;->n()LX/4x9;

    move-result-object v1

    invoke-direct {v0, v1}, LX/50o;-><init>(LX/4x9;)V

    return-object v0
.end method

.method public final floor(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)TE;"
        }
    .end annotation

    .prologue
    .line 824094
    iget-object v0, p0, LX/50n;->a:LX/4x9;

    move-object v0, v0

    .line 824095
    sget-object v1, LX/4xG;->CLOSED:LX/4xG;

    invoke-interface {v0, p1, v1}, LX/4x9;->a(Ljava/lang/Object;LX/4xG;)LX/4x9;

    move-result-object v0

    invoke-interface {v0}, LX/4x9;->i()LX/4wx;

    move-result-object v0

    invoke-static {v0}, LX/50p;->d(LX/4wx;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final headSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;Z)",
            "Ljava/util/NavigableSet",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 824091
    new-instance v0, LX/50o;

    .line 824092
    iget-object v1, p0, LX/50n;->a:LX/4x9;

    move-object v1, v1

    .line 824093
    invoke-static {p2}, LX/4xG;->forBoolean(Z)LX/4xG;

    move-result-object v2

    invoke-interface {v1, p1, v2}, LX/4x9;->a(Ljava/lang/Object;LX/4xG;)LX/4x9;

    move-result-object v1

    invoke-direct {v0, v1}, LX/50o;-><init>(LX/4x9;)V

    return-object v0
.end method

.method public final higher(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)TE;"
        }
    .end annotation

    .prologue
    .line 824101
    iget-object v0, p0, LX/50n;->a:LX/4x9;

    move-object v0, v0

    .line 824102
    sget-object v1, LX/4xG;->OPEN:LX/4xG;

    invoke-interface {v0, p1, v1}, LX/4x9;->b(Ljava/lang/Object;LX/4xG;)LX/4x9;

    move-result-object v0

    invoke-interface {v0}, LX/4x9;->h()LX/4wx;

    move-result-object v0

    invoke-static {v0}, LX/50p;->d(LX/4wx;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final lower(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)TE;"
        }
    .end annotation

    .prologue
    .line 824089
    iget-object v0, p0, LX/50n;->a:LX/4x9;

    move-object v0, v0

    .line 824090
    sget-object v1, LX/4xG;->OPEN:LX/4xG;

    invoke-interface {v0, p1, v1}, LX/4x9;->a(Ljava/lang/Object;LX/4xG;)LX/4x9;

    move-result-object v0

    invoke-interface {v0}, LX/4x9;->i()LX/4wx;

    move-result-object v0

    invoke-static {v0}, LX/50p;->d(LX/4wx;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final pollFirst()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 824087
    iget-object v0, p0, LX/50n;->a:LX/4x9;

    move-object v0, v0

    .line 824088
    invoke-interface {v0}, LX/4x9;->j()LX/4wx;

    move-result-object v0

    invoke-static {v0}, LX/50p;->d(LX/4wx;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final pollLast()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 824082
    iget-object v0, p0, LX/50n;->a:LX/4x9;

    move-object v0, v0

    .line 824083
    invoke-interface {v0}, LX/4x9;->k()LX/4wx;

    move-result-object v0

    invoke-static {v0}, LX/50p;->d(LX/4wx;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final subSet(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableSet;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;ZTE;Z)",
            "Ljava/util/NavigableSet",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 824079
    new-instance v0, LX/50o;

    .line 824080
    iget-object v1, p0, LX/50n;->a:LX/4x9;

    move-object v1, v1

    .line 824081
    invoke-static {p2}, LX/4xG;->forBoolean(Z)LX/4xG;

    move-result-object v2

    invoke-static {p4}, LX/4xG;->forBoolean(Z)LX/4xG;

    move-result-object v3

    invoke-interface {v1, p1, v2, p3, v3}, LX/4x9;->a(Ljava/lang/Object;LX/4xG;Ljava/lang/Object;LX/4xG;)LX/4x9;

    move-result-object v1

    invoke-direct {v0, v1}, LX/50o;-><init>(LX/4x9;)V

    return-object v0
.end method

.method public final tailSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;Z)",
            "Ljava/util/NavigableSet",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 824076
    new-instance v0, LX/50o;

    .line 824077
    iget-object v1, p0, LX/50n;->a:LX/4x9;

    move-object v1, v1

    .line 824078
    invoke-static {p2}, LX/4xG;->forBoolean(Z)LX/4xG;

    move-result-object v2

    invoke-interface {v1, p1, v2}, LX/4x9;->b(Ljava/lang/Object;LX/4xG;)LX/4x9;

    move-result-object v1

    invoke-direct {v0, v1}, LX/50o;-><init>(LX/4x9;)V

    return-object v0
.end method
