.class public LX/4fh;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static final a:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "LX/0RI;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/0QA;

.field public final c:LX/0RI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0RI",
            "<TT;>;"
        }
    .end annotation
.end field

.field public final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/0RI",
            "<+TT;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 798669
    new-instance v0, LX/4fg;

    invoke-direct {v0}, LX/4fg;-><init>()V

    sput-object v0, LX/4fh;->a:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(LX/0QA;LX/0RI;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QA;",
            "LX/0RI",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 798670
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 798671
    iput-object p1, p0, LX/4fh;->b:LX/0QA;

    .line 798672
    iput-object p2, p0, LX/4fh;->c:LX/0RI;

    .line 798673
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/4fh;->d:Ljava/util/List;

    .line 798674
    return-void
.end method


# virtual methods
.method public final b()LX/0Or;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Or",
            "<",
            "Ljava/util/Set",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 798675
    new-instance v0, LX/4fj;

    iget-object v1, p0, LX/4fh;->b:LX/0QA;

    iget-object v2, p0, LX/4fh;->d:Ljava/util/List;

    iget-object v3, p0, LX/4fh;->c:LX/0RI;

    invoke-direct {v0, v1, v2, v3}, LX/4fj;-><init>(LX/0QA;Ljava/util/List;LX/0RI;)V

    return-object v0
.end method

.method public final c()LX/0RI;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0RI",
            "<+",
            "Ljava/util/Set",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 798676
    new-instance v0, LX/52Y;

    const/4 v1, 0x0

    const-class v2, Ljava/util/Set;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/reflect/Type;

    const/4 v4, 0x0

    iget-object v5, p0, LX/4fh;->c:LX/0RI;

    .line 798677
    iget-object v6, v5, LX/0RI;->b:LX/0RL;

    move-object v5, v6

    .line 798678
    iget-object v6, v5, LX/0RL;->b:Ljava/lang/reflect/Type;

    move-object v5, v6

    .line 798679
    aput-object v5, v3, v4

    invoke-direct {v0, v1, v2, v3}, LX/52Y;-><init>(Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)V

    .line 798680
    iget-object v1, p0, LX/4fh;->c:LX/0RI;

    invoke-virtual {v1}, LX/0RI;->c()Ljava/lang/annotation/Annotation;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 798681
    iget-object v1, p0, LX/4fh;->c:LX/0RI;

    invoke-virtual {v1}, LX/0RI;->c()Ljava/lang/annotation/Annotation;

    move-result-object v1

    .line 798682
    new-instance v2, LX/0RI;

    invoke-static {v1}, LX/0RI;->a(Ljava/lang/annotation/Annotation;)LX/0RK;

    move-result-object v3

    invoke-direct {v2, v0, v3}, LX/0RI;-><init>(Ljava/lang/reflect/Type;LX/0RK;)V

    move-object v0, v2

    .line 798683
    :goto_0
    return-object v0

    .line 798684
    :cond_0
    iget-object v1, p0, LX/4fh;->c:LX/0RI;

    invoke-virtual {v1}, LX/0RI;->b()Ljava/lang/Class;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 798685
    iget-object v1, p0, LX/4fh;->c:LX/0RI;

    invoke-virtual {v1}, LX/0RI;->b()Ljava/lang/Class;

    move-result-object v1

    .line 798686
    new-instance v2, LX/0RI;

    invoke-static {v1}, LX/0RI;->b(Ljava/lang/Class;)LX/0RK;

    move-result-object v3

    invoke-direct {v2, v0, v3}, LX/0RI;-><init>(Ljava/lang/reflect/Type;LX/0RK;)V

    move-object v0, v2

    .line 798687
    goto :goto_0

    .line 798688
    :cond_1
    new-instance v1, LX/0RI;

    sget-object v2, LX/0RJ;->INSTANCE:LX/0RJ;

    invoke-direct {v1, v0, v2}, LX/0RI;-><init>(Ljava/lang/reflect/Type;LX/0RK;)V

    move-object v0, v1

    .line 798689
    goto :goto_0
.end method
