.class public LX/4Za;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 790108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLNode;)Lcom/facebook/graphql/model/GraphQLImage;
    .locals 1

    .prologue
    .line 790109
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cC()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    return-object v0
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLNode;)Z
    .locals 1

    .prologue
    .line 790110
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cC()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cC()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cC()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
