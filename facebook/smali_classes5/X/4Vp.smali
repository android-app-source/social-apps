.class public final LX/4Vp;
.super LX/0ur;
.source ""


# instance fields
.field public b:Lcom/facebook/graphql/model/GraphQLPhoto;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

.field public d:Z

.field public e:Lcom/facebook/graphql/model/GraphQLApplication;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Z

.field public g:Z

.field public h:Z

.field public i:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;"
        }
    .end annotation
.end field

.field public j:J

.field public k:Lcom/facebook/graphql/model/GraphQLPlace;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/graphql/model/GraphQLFeedback;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Lcom/facebook/graphql/model/GraphQLProfile;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:J

.field public s:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Lcom/facebook/graphql/model/GraphQLActor;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 744393
    invoke-direct {p0}, LX/0ur;-><init>()V

    .line 744394
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    iput-object v0, p0, LX/4Vp;->c:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    .line 744395
    instance-of v0, p0, LX/4Vp;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 744396
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLAlbum;)LX/4Vp;
    .locals 4

    .prologue
    .line 744397
    new-instance v0, LX/4Vp;

    invoke-direct {v0}, LX/4Vp;-><init>()V

    .line 744398
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 744399
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v1

    iput-object v1, v0, LX/4Vp;->b:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 744400
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->k()Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    move-result-object v1

    iput-object v1, v0, LX/4Vp;->c:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    .line 744401
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->l()Z

    move-result v1

    iput-boolean v1, v0, LX/4Vp;->d:Z

    .line 744402
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->m()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v1

    iput-object v1, v0, LX/4Vp;->e:Lcom/facebook/graphql/model/GraphQLApplication;

    .line 744403
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->n()Z

    move-result v1

    iput-boolean v1, v0, LX/4Vp;->f:Z

    .line 744404
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->o()Z

    move-result v1

    iput-boolean v1, v0, LX/4Vp;->g:Z

    .line 744405
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->p()Z

    move-result v1

    iput-boolean v1, v0, LX/4Vp;->h:Z

    .line 744406
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->q()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4Vp;->i:LX/0Px;

    .line 744407
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->r()J

    move-result-wide v2

    iput-wide v2, v0, LX/4Vp;->j:J

    .line 744408
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->s()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v1

    iput-object v1, v0, LX/4Vp;->k:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 744409
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->t()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    iput-object v1, v0, LX/4Vp;->l:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 744410
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->u()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Vp;->m:Ljava/lang/String;

    .line 744411
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->v()Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    move-result-object v1

    iput-object v1, v0, LX/4Vp;->n:Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    .line 744412
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->w()Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    move-result-object v1

    iput-object v1, v0, LX/4Vp;->o:Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    .line 744413
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->x()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    iput-object v1, v0, LX/4Vp;->p:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 744414
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/4Vp;->q:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 744415
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->z()J

    move-result-wide v2

    iput-wide v2, v0, LX/4Vp;->r:J

    .line 744416
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->A()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Vp;->s:Ljava/lang/String;

    .line 744417
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->B()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    iput-object v1, v0, LX/4Vp;->t:Lcom/facebook/graphql/model/GraphQLActor;

    .line 744418
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->C()Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    move-result-object v1

    iput-object v1, v0, LX/4Vp;->u:Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    .line 744419
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->D()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v1

    iput-object v1, v0, LX/4Vp;->v:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 744420
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/4Vp;->w:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 744421
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->F()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/4Vp;->x:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 744422
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->G()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Vp;->y:Ljava/lang/String;

    .line 744423
    invoke-static {v0, p0}, LX/0ur;->a(LX/0ur;Lcom/facebook/graphql/modelutil/BaseModel;)V

    .line 744424
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;)LX/4Vp;
    .locals 0

    .prologue
    .line 744425
    iput-object p1, p0, LX/4Vp;->c:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    .line 744426
    return-object p0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;)LX/4Vp;
    .locals 0
    .param p1    # Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 744427
    iput-object p1, p0, LX/4Vp;->n:Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    .line 744428
    return-object p0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLPhoto;)LX/4Vp;
    .locals 0
    .param p1    # Lcom/facebook/graphql/model/GraphQLPhoto;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 744429
    iput-object p1, p0, LX/4Vp;->b:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 744430
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/4Vp;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 744431
    iput-object p1, p0, LX/4Vp;->m:Ljava/lang/String;

    .line 744432
    return-object p0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLAlbum;
    .locals 2

    .prologue
    .line 744433
    new-instance v0, Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-direct {v0, p0}, Lcom/facebook/graphql/model/GraphQLAlbum;-><init>(LX/4Vp;)V

    .line 744434
    return-object v0
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)LX/4Vp;
    .locals 0
    .param p1    # Lcom/facebook/graphql/model/GraphQLTextWithEntities;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 744435
    iput-object p1, p0, LX/4Vp;->w:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 744436
    return-object p0
.end method

.method public final b(Ljava/lang/String;)LX/4Vp;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 744437
    iput-object p1, p0, LX/4Vp;->s:Ljava/lang/String;

    .line 744438
    return-object p0
.end method
