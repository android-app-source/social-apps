.class public LX/4fW;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/4fW;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public static final synthetic b:Z

.field public static final c:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static final d:LX/45Q;


# instance fields
.field private final e:Ljava/lang/Object;

.field private final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "LX/4fV;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mLock"
    .end annotation
.end field

.field private final g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/4fV;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mLock"
    .end annotation
.end field

.field public h:LX/45T;

.field private i:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private j:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private k:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 798625
    const-class v0, LX/4fW;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, LX/4fW;->b:Z

    .line 798626
    const-class v0, LX/4fW;

    sput-object v0, LX/4fW;->c:Ljava/lang/Class;

    .line 798627
    new-instance v0, LX/4fT;

    invoke-direct {v0}, LX/4fT;-><init>()V

    sput-object v0, LX/4fW;->d:LX/45Q;

    .line 798628
    new-instance v0, LX/4fW;

    invoke-direct {v0}, LX/4fW;-><init>()V

    sput-object v0, LX/4fW;->a:LX/4fW;

    return-void

    .line 798629
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 798619
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 798620
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/4fW;->e:Ljava/lang/Object;

    .line 798621
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/4fW;->f:Ljava/util/Map;

    .line 798622
    invoke-static {}, LX/0R9;->b()Ljava/util/LinkedList;

    move-result-object v0

    iput-object v0, p0, LX/4fW;->g:Ljava/util/List;

    .line 798623
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/4fW;->k:Z

    .line 798624
    return-void
.end method

.method public static a(LX/4fW;LX/4fU;)V
    .locals 8

    .prologue
    .line 798524
    iget-object v1, p0, LX/4fW;->e:Ljava/lang/Object;

    monitor-enter v1

    .line 798525
    :try_start_0
    invoke-static {}, LX/4fW;->d()J

    move-result-wide v2

    .line 798526
    iget-object v0, p0, LX/4fW;->f:Ljava/util/Map;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4fV;

    .line 798527
    if-nez v0, :cond_1

    .line 798528
    new-instance v0, LX/4fV;

    .line 798529
    new-instance v4, Ljava/lang/Throwable;

    invoke-direct {v4}, Ljava/lang/Throwable;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Throwable;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v5

    .line 798530
    array-length v4, v5

    add-int/lit8 v4, v4, -0x5

    new-array v6, v4, [Ljava/lang/String;

    .line 798531
    const/4 v4, 0x0

    :goto_0
    array-length v7, v6

    if-ge v4, v7, :cond_0

    .line 798532
    add-int/lit8 v7, v4, 0x5

    aget-object v7, v5, v7

    invoke-virtual {v7}, Ljava/lang/StackTraceElement;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v4

    .line 798533
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 798534
    :cond_0
    move-object v4, v6

    .line 798535
    invoke-direct {v0, p1, v4}, LX/4fV;-><init>(LX/4fU;[Ljava/lang/String;)V

    move-object v0, v0

    .line 798536
    iget-object v4, p0, LX/4fW;->f:Ljava/util/Map;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v4, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 798537
    :cond_1
    iget-object v0, v0, LX/4fV;->a:Ljava/util/Stack;

    invoke-virtual {v0, p1}, Ljava/util/Stack;->add(Ljava/lang/Object;)Z

    .line 798538
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static a(LX/4fW;Z)V
    .locals 2

    .prologue
    .line 798612
    iget-object v1, p0, LX/4fW;->e:Ljava/lang/Object;

    monitor-enter v1

    .line 798613
    if-nez p1, :cond_0

    :try_start_0
    iget-object v0, p0, LX/4fW;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 798614
    :cond_0
    monitor-exit v1

    .line 798615
    :goto_0
    return-void

    .line 798616
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 798617
    invoke-direct {p0}, LX/4fW;->g()V

    goto :goto_0

    .line 798618
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private static a(Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 798606
    :try_start_0
    invoke-static {p0}, LX/011;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 798607
    const-string v1, "true"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 798608
    :goto_0
    return v0

    .line 798609
    :catch_0
    move-exception v0

    .line 798610
    sget-object v1, LX/4fW;->c:Ljava/lang/Class;

    const-string v2, "Cannot call get for key %s from SystemProperties"

    invoke-static {v2, p0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 798611
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(LX/4fU;)LX/4fU;
    .locals 6

    .prologue
    .line 798595
    iget-object v2, p0, LX/4fW;->e:Ljava/lang/Object;

    monitor-enter v2

    .line 798596
    :try_start_0
    invoke-static {}, LX/4fW;->d()J

    move-result-wide v4

    .line 798597
    iget-object v0, p0, LX/4fW;->f:Ljava/util/Map;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4fV;

    .line 798598
    iget-object v1, v0, LX/4fV;->a:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/4fU;

    .line 798599
    sget-boolean v3, LX/4fW;->b:Z

    if-nez v3, :cond_0

    if-eq v1, p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 798600
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 798601
    :cond_0
    :try_start_1
    iget-object v1, v0, LX/4fV;->a:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 798602
    iget-object v1, p0, LX/4fW;->f:Ljava/util/Map;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 798603
    iget-object v1, p0, LX/4fW;->g:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 798604
    const/4 v0, 0x0

    monitor-exit v2

    .line 798605
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, v0, LX/4fV;->a:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4fU;

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method private static d()J
    .locals 2

    .prologue
    .line 798594
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getId()J

    move-result-wide v0

    return-wide v0
.end method

.method public static e(LX/4fW;)Z
    .locals 4

    .prologue
    .line 798578
    invoke-static {p0}, LX/4fW;->f(LX/4fW;)Z

    move-result v0

    .line 798579
    iget-boolean v1, p0, LX/4fW;->k:Z

    if-ne v1, v0, :cond_0

    .line 798580
    iget-boolean v0, p0, LX/4fW;->k:Z

    .line 798581
    :goto_0
    return v0

    .line 798582
    :cond_0
    iput-boolean v0, p0, LX/4fW;->k:Z

    .line 798583
    iget-boolean v1, p0, LX/4fW;->k:Z

    if-eqz v1, :cond_2

    .line 798584
    invoke-static {}, LX/4fW;->i()Ljava/io/File;

    move-result-object v1

    .line 798585
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result p0

    if-nez p0, :cond_4

    .line 798586
    :cond_1
    :goto_1
    goto :goto_0

    .line 798587
    :cond_2
    iget-object v1, p0, LX/4fW;->h:LX/45T;

    if-eqz v1, :cond_3

    .line 798588
    iget-object v1, p0, LX/4fW;->h:LX/45T;

    .line 798589
    iget-object v2, v1, LX/45T;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 798590
    const/4 v1, 0x0

    iput-object v1, p0, LX/4fW;->h:LX/45T;

    .line 798591
    :cond_3
    invoke-static {p0, v0}, LX/4fW;->a(LX/4fW;Z)V

    goto :goto_0

    .line 798592
    :cond_4
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    move-result v1

    if-nez v1, :cond_1

    .line 798593
    sget-object v1, LX/4fW;->c:Ljava/lang/Class;

    const-string p0, "Could not delete DI call graph to the sdcard"

    invoke-static {v1, p0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static f(LX/4fW;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 798569
    iget-object v1, p0, LX/4fW;->j:Ljava/util/concurrent/atomic/AtomicBoolean;

    if-eqz v1, :cond_1

    .line 798570
    iget-object v0, p0, LX/4fW;->j:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    .line 798571
    :cond_0
    :goto_0
    return v0

    .line 798572
    :cond_1
    sget-boolean v1, LX/007;->i:Z

    move v1, v1

    .line 798573
    if-eqz v1, :cond_0

    .line 798574
    iget-object v1, p0, LX/4fW;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    if-nez v1, :cond_2

    .line 798575
    new-instance v1, Ljava/util/concurrent/atomic/AtomicBoolean;

    const-string v2, "fb4a.debug.digraph.enabled"

    invoke-static {v2}, LX/4fW;->a(Ljava/lang/String;)Z

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v1, p0, LX/4fW;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 798576
    :cond_2
    iget-object v1, p0, LX/4fW;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 798577
    const-string v0, "fb4a.debug.digraph.running"

    invoke-static {v0}, LX/4fW;->a(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method private g()V
    .locals 8

    .prologue
    .line 798539
    :try_start_0
    new-instance v1, Lorg/json/JSONStringer;

    invoke-direct {v1}, Lorg/json/JSONStringer;-><init>()V

    .line 798540
    invoke-virtual {v1}, Lorg/json/JSONStringer;->object()Lorg/json/JSONStringer;

    .line 798541
    const-string v0, "callTrees"

    invoke-virtual {v1, v0}, Lorg/json/JSONStringer;->key(Ljava/lang/String;)Lorg/json/JSONStringer;

    .line 798542
    invoke-virtual {v1}, Lorg/json/JSONStringer;->array()Lorg/json/JSONStringer;

    .line 798543
    iget-object v2, p0, LX/4fW;->e:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    .line 798544
    :try_start_1
    iget-object v0, p0, LX/4fW;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4fV;

    .line 798545
    invoke-virtual {v1}, Lorg/json/JSONStringer;->object()Lorg/json/JSONStringer;

    .line 798546
    const-string v4, "stackTrace"

    invoke-virtual {v1, v4}, Lorg/json/JSONStringer;->key(Ljava/lang/String;)Lorg/json/JSONStringer;

    .line 798547
    invoke-virtual {v1}, Lorg/json/JSONStringer;->array()Lorg/json/JSONStringer;

    .line 798548
    iget-object v5, v0, LX/4fV;->c:[Ljava/lang/String;

    array-length v6, v5

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v6, :cond_0

    aget-object v7, v5, v4

    .line 798549
    invoke-virtual {v1, v7}, Lorg/json/JSONStringer;->value(Ljava/lang/Object;)Lorg/json/JSONStringer;

    .line 798550
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 798551
    :cond_0
    invoke-virtual {v1}, Lorg/json/JSONStringer;->endArray()Lorg/json/JSONStringer;

    .line 798552
    const-string v4, "head"

    invoke-virtual {v1, v4}, Lorg/json/JSONStringer;->key(Ljava/lang/String;)Lorg/json/JSONStringer;

    .line 798553
    iget-object v4, v0, LX/4fV;->b:LX/4fU;

    invoke-virtual {v4, v1}, LX/4fU;->a(Lorg/json/JSONStringer;)V

    .line 798554
    invoke-virtual {v1}, Lorg/json/JSONStringer;->endObject()Lorg/json/JSONStringer;

    .line 798555
    goto :goto_0

    .line 798556
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1

    .line 798557
    :catch_0
    move-exception v0

    .line 798558
    :goto_2
    sget-object v1, LX/4fW;->c:Ljava/lang/Class;

    const-string v2, "Could not write DI call graph to the sdcard"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 798559
    :goto_3
    return-void

    .line 798560
    :cond_1
    :try_start_3
    iget-object v0, p0, LX/4fW;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 798561
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 798562
    :try_start_4
    invoke-virtual {v1}, Lorg/json/JSONStringer;->endArray()Lorg/json/JSONStringer;

    .line 798563
    invoke-virtual {v1}, Lorg/json/JSONStringer;->endObject()Lorg/json/JSONStringer;

    .line 798564
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-static {}, LX/4fW;->i()Ljava/io/File;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_1

    .line 798565
    :try_start_5
    invoke-virtual {v1}, Lorg/json/JSONStringer;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, LX/2aP;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 798566
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V

    goto :goto_3

    .line 798567
    :catch_1
    move-exception v0

    goto :goto_2

    .line 798568
    :catchall_1
    move-exception v0

    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V

    throw v0
.end method

.method public static i()Ljava/io/File;
    .locals 3

    .prologue
    .line 798523
    new-instance v0, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    const-string v2, "DIGraph.json"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final a(LX/4fU;Ljava/lang/Object;)V
    .locals 10

    .prologue
    .line 798499
    if-nez p1, :cond_0

    .line 798500
    :goto_0
    return-void

    .line 798501
    :cond_0
    iget-object v4, p1, LX/4fU;->e:Ljava/lang/Long;

    if-nez v4, :cond_2

    .line 798502
    sget-object v4, LX/4fW;->c:Ljava/lang/Class;

    const-string v5, "Haven\'t started provider call."

    invoke-static {v4, v5}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 798503
    :goto_1
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    .line 798504
    invoke-direct {p0, p1}, LX/4fW;->b(LX/4fU;)LX/4fU;

    move-result-object v2

    .line 798505
    if-eqz v2, :cond_1

    .line 798506
    iget-object v3, v2, LX/4fU;->b:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 798507
    :cond_1
    invoke-static {p0}, LX/4fW;->f(LX/4fW;)Z

    move-result v2

    .line 798508
    invoke-static {p0, v2}, LX/4fW;->a(LX/4fW;Z)V

    .line 798509
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    sub-long v0, v2, v0

    .line 798510
    iget-wide v4, p1, LX/4fU;->g:J

    add-long/2addr v4, v0

    iput-wide v4, p1, LX/4fU;->g:J

    .line 798511
    goto :goto_0

    .line 798512
    :cond_2
    iget-object v4, p1, LX/4fU;->f:Ljava/lang/Long;

    if-eqz v4, :cond_3

    .line 798513
    sget-object v4, LX/4fW;->c:Ljava/lang/Class;

    const-string v5, "Have already called stop on this provider calls"

    invoke-static {v4, v5}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_1

    .line 798514
    :cond_3
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v4

    iget-object v6, p1, LX/4fU;->e:Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    sub-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    iput-object v4, p1, LX/4fU;->f:Ljava/lang/Long;

    .line 798515
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v6

    .line 798516
    if-eqz p2, :cond_4

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    :goto_2
    iput-object v4, p1, LX/4fU;->h:Ljava/lang/Class;

    .line 798517
    iget-object v4, p1, LX/4fU;->c:LX/45T;

    sget-object v5, LX/4fW;->d:LX/45Q;

    invoke-static {p2, v4, v5}, LX/45S;->a(Ljava/lang/Object;LX/45T;LX/45Q;)LX/45U;

    move-result-object v4

    iput-object v4, p1, LX/4fU;->d:LX/45U;

    .line 798518
    iget-wide v4, p1, LX/4fU;->g:J

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v8

    sub-long v6, v8, v6

    add-long/2addr v4, v6

    iput-wide v4, p1, LX/4fU;->g:J

    goto :goto_1

    .line 798519
    :cond_4
    iget-object v4, p1, LX/4fU;->a:LX/0RI;

    .line 798520
    iget-object v5, v4, LX/0RI;->b:LX/0RL;

    move-object v4, v5

    .line 798521
    iget-object v5, v4, LX/0RL;->a:Ljava/lang/Class;

    move-object v4, v5

    .line 798522
    goto :goto_2
.end method
