.class public LX/3ma;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/DCu;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DCw;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 636915
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/3ma;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/DCw;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 636916
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 636917
    iput-object p1, p0, LX/3ma;->b:LX/0Ot;

    .line 636918
    return-void
.end method

.method public static a(LX/0QB;)LX/3ma;
    .locals 4

    .prologue
    .line 636919
    const-class v1, LX/3ma;

    monitor-enter v1

    .line 636920
    :try_start_0
    sget-object v0, LX/3ma;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 636921
    sput-object v2, LX/3ma;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 636922
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 636923
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 636924
    new-instance v3, LX/3ma;

    const/16 p0, 0x1ed7

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/3ma;-><init>(LX/0Ot;)V

    .line 636925
    move-object v0, v3

    .line 636926
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 636927
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/3ma;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 636928
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 636929
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 636930
    check-cast p2, LX/DCv;

    .line 636931
    iget-object v0, p0, LX/3ma;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget v0, p2, LX/DCv;->a:I

    const/4 v3, 0x1

    .line 636932
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, v3}, LX/1Dh;->T(I)LX/1Dh;

    move-result-object v2

    if-ne v0, v3, :cond_2

    const v1, 0x7f0203e1

    :goto_0
    invoke-interface {v2, v1}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v1

    const/16 v5, 0x8

    const/4 v4, 0x1

    .line 636933
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v2

    const v3, 0x7f0214c6

    invoke-virtual {v2, v3}, LX/1o5;->h(I)LX/1o5;

    move-result-object v2

    sget-object v3, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v2, v3}, LX/1o5;->a(Landroid/widget/ImageView$ScaleType;)LX/1o5;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    invoke-interface {v2, v5, v4}, LX/1Di;->d(II)LX/1Di;

    move-result-object v2

    .line 636934
    if-nez v0, :cond_0

    .line 636935
    const v3, 0x7f0b0933

    invoke-interface {v2, v3}, LX/1Di;->i(I)LX/1Di;

    move-result-object v3

    invoke-interface {v3, v5, v4}, LX/1Di;->h(II)LX/1Di;

    .line 636936
    :cond_0
    move-object v2, v2

    .line 636937
    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    const/4 p2, 0x0

    const/4 p0, 0x3

    const/4 v5, 0x1

    .line 636938
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v5}, LX/1Dh;->C(I)LX/1Dh;

    move-result-object v3

    if-ne v0, v5, :cond_3

    const v2, 0x7f0b0f02

    :goto_1
    invoke-interface {v3, p2, v2}, LX/1Dh;->x(II)LX/1Dh;

    move-result-object v3

    if-ne v0, v5, :cond_4

    const v2, 0x7f0b0f03

    :goto_2
    invoke-interface {v3, v5, v2}, LX/1Dh;->x(II)LX/1Dh;

    move-result-object v2

    const v3, 0x7f0b0f0e

    invoke-interface {v2, v3}, LX/1Dh;->G(I)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, p2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v3

    const v4, 0x7f081c23

    invoke-virtual {v3, v4}, LX/1ne;->h(I)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, p2}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v3

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v3, v4}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v3

    const v4, 0x7f0b0050

    invoke-virtual {v3, v4}, LX/1ne;->q(I)LX/1ne;

    move-result-object v3

    const v4, 0x7f0a00ab

    invoke-virtual {v3, v4}, LX/1ne;->n(I)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, v5}, LX/1ne;->t(I)LX/1ne;

    move-result-object v3

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, LX/1ne;->j(I)LX/1ne;

    move-result-object v3

    sget-object v4, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v3, v4}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const v4, 0x7f020a8d

    invoke-interface {v3, v4}, LX/1Di;->x(I)LX/1Di;

    move-result-object v3

    invoke-interface {v3, p0, p0}, LX/1Di;->d(II)LX/1Di;

    move-result-object v3

    invoke-interface {v3, v5, p0}, LX/1Di;->h(II)LX/1Di;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v3

    const v4, 0x7f081c24

    invoke-virtual {v3, v4}, LX/1ne;->h(I)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, p2}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v3

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v3, v4}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v3

    const v4, 0x7f0b004e

    invoke-virtual {v3, v4}, LX/1ne;->q(I)LX/1ne;

    move-result-object v3

    const v4, 0x7f0a00a4

    invoke-virtual {v3, v4}, LX/1ne;->n(I)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, p0}, LX/1ne;->j(I)LX/1ne;

    move-result-object v3

    sget-object v4, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v3, v4}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const v4, 0x7f020a8d

    invoke-interface {v3, v4}, LX/1Di;->x(I)LX/1Di;

    move-result-object v3

    invoke-interface {v3, p0, p0}, LX/1Di;->d(II)LX/1Di;

    move-result-object v3

    invoke-interface {v3, v5, p0}, LX/1Di;->h(II)LX/1Di;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    move-object v2, v2

    .line 636939
    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    const/4 v4, 0x2

    const/4 p2, 0x1

    const/4 p0, 0x0

    .line 636940
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-interface {v2, v3}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v4}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v4}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v3

    const v4, 0x7f0a009f

    invoke-virtual {v3, v4}, LX/25Q;->i(I)LX/25Q;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const/4 v4, 0x6

    const v5, 0x7f0b0f0f

    invoke-interface {v3, v4, v5}, LX/1Di;->c(II)LX/1Di;

    move-result-object v3

    const/4 v4, 0x4

    invoke-interface {v3, v4}, LX/1Di;->b(I)LX/1Di;

    move-result-object v3

    const v4, 0x7f0b0033

    invoke-interface {v3, v4}, LX/1Di;->q(I)LX/1Di;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v3

    const v4, 0x7f081c25

    invoke-virtual {v3, v4}, LX/1ne;->h(I)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, p0}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, p2}, LX/1ne;->j(I)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, p2}, LX/1ne;->t(I)LX/1ne;

    move-result-object v3

    sget-object v4, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v3, v4}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v3

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v3, v4}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v3

    sget-object v4, LX/1nd;->CENTER:LX/1nd;

    invoke-virtual {v3, v4}, LX/1ne;->a(LX/1nd;)LX/1ne;

    move-result-object v3

    const v4, 0x7f0b004e

    invoke-virtual {v3, v4}, LX/1ne;->q(I)LX/1ne;

    move-result-object v3

    const v4, 0x7f0a00a4

    invoke-virtual {v3, v4}, LX/1ne;->n(I)LX/1ne;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const v4, 0x7f0b0f0d

    invoke-interface {v3, v4}, LX/1Di;->q(I)LX/1Di;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    move-object v2, v2

    .line 636941
    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    .line 636942
    const v2, 0x3e410db9

    const/4 v3, 0x0

    invoke-static {p1, v2, v3}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v2

    move-object v2, v2

    .line 636943
    invoke-interface {v1, v2}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v1

    .line 636944
    if-nez v0, :cond_1

    .line 636945
    const v2, 0x7f0b0f0a

    invoke-interface {v1, v2}, LX/1Di;->q(I)LX/1Di;

    .line 636946
    :cond_1
    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    move-object v0, v1

    .line 636947
    return-object v0

    .line 636948
    :cond_2
    const v1, 0x7f020a3f

    goto/16 :goto_0

    :cond_3
    const v2, 0x7f0b0f0b

    goto/16 :goto_1

    :cond_4
    const v2, 0x7f0b0f0c

    goto/16 :goto_2
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 636949
    invoke-static {}, LX/1dS;->b()V

    .line 636950
    iget v0, p1, LX/1dQ;->b:I

    .line 636951
    packed-switch v0, :pswitch_data_0

    .line 636952
    :goto_0
    return-object v2

    .line 636953
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 636954
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    .line 636955
    iget-object p1, p0, LX/3ma;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/DCw;

    .line 636956
    new-instance p2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p0, "gysj_discover_unit_click"

    invoke-direct {p2, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 636957
    iget-object p0, p1, LX/DCw;->a:LX/0Zb;

    invoke-interface {p0, p2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 636958
    iget-object p2, p1, LX/DCw;->b:LX/17W;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p0

    sget-object v1, LX/0ax;->V:Ljava/lang/String;

    invoke-virtual {p2, p0, v1}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 636959
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x3e410db9
        :pswitch_0
    .end packed-switch
.end method

.method public final c(LX/1De;)LX/DCu;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 636960
    new-instance v1, LX/DCv;

    invoke-direct {v1, p0}, LX/DCv;-><init>(LX/3ma;)V

    .line 636961
    sget-object v2, LX/3ma;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/DCu;

    .line 636962
    if-nez v2, :cond_0

    .line 636963
    new-instance v2, LX/DCu;

    invoke-direct {v2}, LX/DCu;-><init>()V

    .line 636964
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/DCu;->a$redex0(LX/DCu;LX/1De;IILX/DCv;)V

    .line 636965
    move-object v1, v2

    .line 636966
    move-object v0, v1

    .line 636967
    return-object v0
.end method
