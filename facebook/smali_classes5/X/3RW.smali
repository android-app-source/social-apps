.class public LX/3RW;
.super LX/3RX;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile i:LX/3RW;


# instance fields
.field private final a:LX/01T;

.field public final b:LX/3RU;

.field public final c:Landroid/media/AudioManager;

.field public final d:Landroid/telephony/TelephonyManager;

.field private final e:LX/0Uo;

.field private final f:Z

.field public final g:LX/3RZ;

.field private final h:LX/3Ra;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "ui thread"
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/01T;LX/3RU;Landroid/media/AudioManager;LX/0Or;LX/0Or;Landroid/telephony/TelephonyManager;LX/0Uo;Ljava/lang/Boolean;LX/3RZ;)V
    .locals 1
    .param p8    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/01T;",
            "LX/3RU;",
            "Landroid/media/AudioManager;",
            "LX/0Or",
            "<",
            "LX/7Cb;",
            ">;",
            "LX/0Or",
            "<",
            "LX/7Cc;",
            ">;",
            "Landroid/telephony/TelephonyManager;",
            "LX/0Uo;",
            "Ljava/lang/Boolean;",
            "LX/3RZ;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 580529
    invoke-direct {p0, p4, p5, p9}, LX/3RX;-><init>(LX/0Or;LX/0Or;LX/3RZ;)V

    .line 580530
    iput-object p1, p0, LX/3RW;->a:LX/01T;

    .line 580531
    iput-object p2, p0, LX/3RW;->b:LX/3RU;

    .line 580532
    new-instance v0, LX/3Ra;

    invoke-direct {v0}, LX/3Ra;-><init>()V

    iput-object v0, p0, LX/3RW;->h:LX/3Ra;

    .line 580533
    iput-object p3, p0, LX/3RW;->c:Landroid/media/AudioManager;

    .line 580534
    iput-object p6, p0, LX/3RW;->d:Landroid/telephony/TelephonyManager;

    .line 580535
    iput-object p7, p0, LX/3RW;->e:LX/0Uo;

    .line 580536
    invoke-virtual {p8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/3RW;->f:Z

    .line 580537
    iput-object p9, p0, LX/3RW;->g:LX/3RZ;

    .line 580538
    return-void
.end method

.method public static b(LX/0QB;)LX/3RW;
    .locals 13

    .prologue
    .line 580516
    sget-object v0, LX/3RW;->i:LX/3RW;

    if-nez v0, :cond_1

    .line 580517
    const-class v1, LX/3RW;

    monitor-enter v1

    .line 580518
    :try_start_0
    sget-object v0, LX/3RW;->i:LX/3RW;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 580519
    if-eqz v2, :cond_0

    .line 580520
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 580521
    new-instance v3, LX/3RW;

    invoke-static {v0}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v4

    check-cast v4, LX/01T;

    invoke-static {v0}, LX/3RU;->b(LX/0QB;)LX/3RU;

    move-result-object v5

    check-cast v5, LX/3RU;

    invoke-static {v0}, LX/19T;->b(LX/0QB;)Landroid/media/AudioManager;

    move-result-object v6

    check-cast v6, Landroid/media/AudioManager;

    const/16 v7, 0x3563

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    const/16 v8, 0x3564

    invoke-static {v0, v8}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-static {v0}, LX/0e7;->b(LX/0QB;)Landroid/telephony/TelephonyManager;

    move-result-object v9

    check-cast v9, Landroid/telephony/TelephonyManager;

    invoke-static {v0}, LX/0Uo;->a(LX/0QB;)LX/0Uo;

    move-result-object v10

    check-cast v10, LX/0Uo;

    invoke-static {v0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v11

    check-cast v11, Ljava/lang/Boolean;

    invoke-static {v0}, LX/3RZ;->b(LX/0QB;)LX/3RZ;

    move-result-object v12

    check-cast v12, LX/3RZ;

    invoke-direct/range {v3 .. v12}, LX/3RW;-><init>(LX/01T;LX/3RU;Landroid/media/AudioManager;LX/0Or;LX/0Or;Landroid/telephony/TelephonyManager;LX/0Uo;Ljava/lang/Boolean;LX/3RZ;)V

    .line 580522
    move-object v0, v3

    .line 580523
    sput-object v0, LX/3RW;->i:LX/3RW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 580524
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 580525
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 580526
    :cond_1
    sget-object v0, LX/3RW;->i:LX/3RW;

    return-object v0

    .line 580527
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 580528
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static e(LX/3RW;)Z
    .locals 4

    .prologue
    .line 580510
    iget-object v0, p0, LX/3RW;->a:LX/01T;

    sget-object v1, LX/01T;->MESSENGER:LX/01T;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/3RW;->b:LX/3RU;

    .line 580511
    iget-object v1, v0, LX/3RU;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/0db;->L:LX/0Tn;

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v1

    move v0, v1

    .line 580512
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3RW;->e:LX/0Uo;

    invoke-virtual {v0}, LX/0Uo;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3RW;->c:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isMusicActive()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/3RW;->c:Landroid/media/AudioManager;

    iget-object v1, p0, LX/3RW;->g:LX/3RZ;

    invoke-virtual {v1}, LX/3RZ;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final d(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 580513
    invoke-static {p0}, LX/3RW;->e(LX/3RW;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 580514
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0, p1, v0}, LX/3RX;->a(Ljava/lang/String;F)LX/7Cb;

    .line 580515
    :cond_0
    return-void
.end method
