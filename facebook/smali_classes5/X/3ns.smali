.class public LX/3ns;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Landroid/view/animation/Interpolator;

.field public static final b:Landroid/view/animation/Interpolator;

.field public static final c:Landroid/view/animation/Interpolator;

.field public static final d:Landroid/view/animation/Interpolator;

.field public static final e:Landroid/view/animation/Interpolator;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 639237
    new-instance v0, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v0}, Landroid/view/animation/LinearInterpolator;-><init>()V

    sput-object v0, LX/3ns;->a:Landroid/view/animation/Interpolator;

    .line 639238
    new-instance v0, LX/3tC;

    invoke-direct {v0}, LX/3tC;-><init>()V

    sput-object v0, LX/3ns;->b:Landroid/view/animation/Interpolator;

    .line 639239
    new-instance v0, LX/3tB;

    invoke-direct {v0}, LX/3tB;-><init>()V

    sput-object v0, LX/3ns;->c:Landroid/view/animation/Interpolator;

    .line 639240
    new-instance v0, LX/3tD;

    invoke-direct {v0}, LX/3tD;-><init>()V

    sput-object v0, LX/3ns;->d:Landroid/view/animation/Interpolator;

    .line 639241
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    sput-object v0, LX/3ns;->e:Landroid/view/animation/Interpolator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 639235
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 639236
    return-void
.end method

.method public static a(FFF)F
    .locals 1

    .prologue
    .line 639234
    sub-float v0, p1, p0

    mul-float/2addr v0, p2

    add-float/2addr v0, p0

    return v0
.end method
