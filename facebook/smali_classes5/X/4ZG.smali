.class public final LX/4ZG;
.super LX/0ur;
.source ""


# instance fields
.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lcom/facebook/graphql/model/GraphQLPageInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 787953
    invoke-direct {p0}, LX/0ur;-><init>()V

    .line 787954
    instance-of v0, p0, LX/4ZG;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 787955
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLTimelineStoriesConnection;)LX/4ZG;
    .locals 2

    .prologue
    .line 787947
    new-instance v0, LX/4ZG;

    invoke-direct {v0}, LX/4ZG;-><init>()V

    .line 787948
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 787949
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTimelineStoriesConnection;->a()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4ZG;->b:LX/0Px;

    .line 787950
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTimelineStoriesConnection;->j()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v1

    iput-object v1, v0, LX/4ZG;->c:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 787951
    invoke-static {v0, p0}, LX/0ur;->a(LX/0ur;Lcom/facebook/graphql/modelutil/BaseModel;)V

    .line 787952
    return-object v0
.end method


# virtual methods
.method public final a()Lcom/facebook/graphql/model/GraphQLTimelineStoriesConnection;
    .locals 2

    .prologue
    .line 787945
    new-instance v0, Lcom/facebook/graphql/model/GraphQLTimelineStoriesConnection;

    invoke-direct {v0, p0}, Lcom/facebook/graphql/model/GraphQLTimelineStoriesConnection;-><init>(LX/4ZG;)V

    .line 787946
    return-object v0
.end method
