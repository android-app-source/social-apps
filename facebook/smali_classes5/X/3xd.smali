.class public final LX/3xd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1OH;


# instance fields
.field public final synthetic a:LX/3xm;


# direct methods
.method public constructor <init>(LX/3xm;)V
    .locals 0

    .prologue
    .line 659731
    iput-object p1, p0, LX/3xd;->a:LX/3xm;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 3

    .prologue
    .line 659732
    if-nez p1, :cond_0

    .line 659733
    :goto_0
    return-void

    .line 659734
    :cond_0
    iget-object v0, p0, LX/3xd;->a:LX/3xm;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, LX/3xm;->a$redex0(LX/3xm;LX/1a1;I)V

    goto :goto_0
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .locals 8

    .prologue
    const/4 v4, -0x1

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 659695
    iget-object v2, p0, LX/3xd;->a:LX/3xm;

    iget-object v2, v2, LX/3xm;->x:LX/3rW;

    invoke-virtual {v2, p1}, LX/3rW;->a(Landroid/view/MotionEvent;)Z

    .line 659696
    invoke-static {p1}, LX/2xd;->a(Landroid/view/MotionEvent;)I

    move-result v2

    .line 659697
    if-nez v2, :cond_4

    .line 659698
    iget-object v2, p0, LX/3xd;->a:LX/3xm;

    invoke-static {p1, v1}, LX/2xd;->b(Landroid/view/MotionEvent;I)I

    move-result v3

    iput v3, v2, LX/3xm;->i:I

    .line 659699
    iget-object v2, p0, LX/3xd;->a:LX/3xm;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    iput v3, v2, LX/3xm;->c:F

    .line 659700
    iget-object v2, p0, LX/3xd;->a:LX/3xm;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    iput v3, v2, LX/3xm;->d:F

    .line 659701
    iget-object v2, p0, LX/3xd;->a:LX/3xm;

    invoke-static {v2}, LX/3xm;->f(LX/3xm;)V

    .line 659702
    iget-object v2, p0, LX/3xd;->a:LX/3xm;

    iget-object v2, v2, LX/3xm;->b:LX/1a1;

    if-nez v2, :cond_2

    .line 659703
    iget-object v2, p0, LX/3xd;->a:LX/3xm;

    const/4 v4, 0x0

    .line 659704
    iget-object v3, v2, LX/3xm;->m:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_8

    move-object v3, v4

    .line 659705
    :cond_0
    :goto_0
    move-object v2, v3

    .line 659706
    if-eqz v2, :cond_2

    .line 659707
    iget-object v3, p0, LX/3xd;->a:LX/3xm;

    iget v4, v3, LX/3xm;->c:F

    iget v5, v2, LX/3xe;->k:F

    sub-float/2addr v4, v5

    iput v4, v3, LX/3xm;->c:F

    .line 659708
    iget-object v3, p0, LX/3xd;->a:LX/3xm;

    iget v4, v3, LX/3xm;->d:F

    iget v5, v2, LX/3xe;->l:F

    sub-float/2addr v4, v5

    iput v4, v3, LX/3xm;->d:F

    .line 659709
    iget-object v3, p0, LX/3xd;->a:LX/3xm;

    iget-object v4, v2, LX/3xe;->h:LX/1a1;

    invoke-static {v3, v4, v0}, LX/3xm;->a$redex0(LX/3xm;LX/1a1;Z)I

    .line 659710
    iget-object v3, p0, LX/3xd;->a:LX/3xm;

    iget-object v3, v3, LX/3xm;->a:Ljava/util/List;

    iget-object v4, v2, LX/3xe;->h:LX/1a1;

    iget-object v4, v4, LX/1a1;->a:Landroid/view/View;

    invoke-interface {v3, v4}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 659711
    iget-object v3, p0, LX/3xd;->a:LX/3xm;

    iget-object v3, v3, LX/3xm;->j:LX/3xj;

    iget-object v4, p0, LX/3xd;->a:LX/3xm;

    iget-object v4, v4, LX/3xm;->p:Landroid/support/v7/widget/RecyclerView;

    iget-object v5, v2, LX/3xe;->h:LX/1a1;

    invoke-virtual {v3, v4, v5}, LX/3xj;->b(Landroid/support/v7/widget/RecyclerView;LX/1a1;)V

    .line 659712
    :cond_1
    iget-object v3, p0, LX/3xd;->a:LX/3xm;

    iget-object v4, v2, LX/3xe;->h:LX/1a1;

    iget v2, v2, LX/3xe;->i:I

    invoke-static {v3, v4, v2}, LX/3xm;->a$redex0(LX/3xm;LX/1a1;I)V

    .line 659713
    iget-object v2, p0, LX/3xd;->a:LX/3xm;

    iget-object v3, p0, LX/3xd;->a:LX/3xm;

    iget v3, v3, LX/3xm;->l:I

    invoke-static {v2, p1, v3, v1}, LX/3xm;->a$redex0(LX/3xm;Landroid/view/MotionEvent;II)V

    .line 659714
    :cond_2
    :goto_1
    iget-object v2, p0, LX/3xd;->a:LX/3xm;

    iget-object v2, v2, LX/3xm;->r:Landroid/view/VelocityTracker;

    if-eqz v2, :cond_3

    .line 659715
    iget-object v2, p0, LX/3xd;->a:LX/3xm;

    iget-object v2, v2, LX/3xm;->r:Landroid/view/VelocityTracker;

    invoke-virtual {v2, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 659716
    :cond_3
    iget-object v2, p0, LX/3xd;->a:LX/3xm;

    iget-object v2, v2, LX/3xm;->b:LX/1a1;

    if-eqz v2, :cond_7

    :goto_2
    return v0

    .line 659717
    :cond_4
    const/4 v3, 0x3

    if-eq v2, v3, :cond_5

    if-ne v2, v0, :cond_6

    .line 659718
    :cond_5
    iget-object v2, p0, LX/3xd;->a:LX/3xm;

    iput v4, v2, LX/3xm;->i:I

    .line 659719
    iget-object v2, p0, LX/3xd;->a:LX/3xm;

    const/4 v3, 0x0

    invoke-static {v2, v3, v1}, LX/3xm;->a$redex0(LX/3xm;LX/1a1;I)V

    goto :goto_1

    .line 659720
    :cond_6
    iget-object v3, p0, LX/3xd;->a:LX/3xm;

    iget v3, v3, LX/3xm;->i:I

    if-eq v3, v4, :cond_2

    .line 659721
    iget-object v3, p0, LX/3xd;->a:LX/3xm;

    iget v3, v3, LX/3xm;->i:I

    invoke-static {p1, v3}, LX/2xd;->a(Landroid/view/MotionEvent;I)I

    move-result v3

    .line 659722
    if-ltz v3, :cond_2

    .line 659723
    iget-object v4, p0, LX/3xd;->a:LX/3xm;

    invoke-static {v4, v2, p1, v3}, LX/3xm;->a$redex0(LX/3xm;ILandroid/view/MotionEvent;I)Z

    goto :goto_1

    :cond_7
    move v0, v1

    .line 659724
    goto :goto_2

    .line 659725
    :cond_8
    invoke-static {v2, p1}, LX/3xm;->b$redex0(LX/3xm;Landroid/view/MotionEvent;)Landroid/view/View;

    move-result-object v6

    .line 659726
    iget-object v3, v2, LX/3xm;->m:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    move v5, v3

    :goto_3
    if-ltz v5, :cond_9

    .line 659727
    iget-object v3, v2, LX/3xm;->m:Ljava/util/List;

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/3xe;

    .line 659728
    iget-object v7, v3, LX/3xe;->h:LX/1a1;

    iget-object v7, v7, LX/1a1;->a:Landroid/view/View;

    if-eq v7, v6, :cond_0

    .line 659729
    add-int/lit8 v3, v5, -0x1

    move v5, v3

    goto :goto_3

    :cond_9
    move-object v3, v4

    .line 659730
    goto/16 :goto_0
.end method

.method public final b(Landroid/view/MotionEvent;)V
    .locals 6

    .prologue
    const/16 v5, 0x3e8

    const/4 v0, 0x0

    const/4 v4, -0x1

    .line 659661
    iget-object v1, p0, LX/3xd;->a:LX/3xm;

    iget-object v1, v1, LX/3xm;->x:LX/3rW;

    invoke-virtual {v1, p1}, LX/3rW;->a(Landroid/view/MotionEvent;)Z

    .line 659662
    iget-object v1, p0, LX/3xd;->a:LX/3xm;

    iget-object v1, v1, LX/3xm;->r:Landroid/view/VelocityTracker;

    if-eqz v1, :cond_0

    .line 659663
    iget-object v1, p0, LX/3xd;->a:LX/3xm;

    iget-object v1, v1, LX/3xm;->r:Landroid/view/VelocityTracker;

    invoke-virtual {v1, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 659664
    :cond_0
    iget-object v1, p0, LX/3xd;->a:LX/3xm;

    iget v1, v1, LX/3xm;->i:I

    if-ne v1, v4, :cond_2

    .line 659665
    :cond_1
    :goto_0
    return-void

    .line 659666
    :cond_2
    invoke-static {p1}, LX/2xd;->a(Landroid/view/MotionEvent;)I

    move-result v1

    .line 659667
    iget-object v2, p0, LX/3xd;->a:LX/3xm;

    iget v2, v2, LX/3xm;->i:I

    invoke-static {p1, v2}, LX/2xd;->a(Landroid/view/MotionEvent;I)I

    move-result v2

    .line 659668
    if-ltz v2, :cond_3

    .line 659669
    iget-object v3, p0, LX/3xd;->a:LX/3xm;

    invoke-static {v3, v1, p1, v2}, LX/3xm;->a$redex0(LX/3xm;ILandroid/view/MotionEvent;I)Z

    .line 659670
    :cond_3
    iget-object v3, p0, LX/3xd;->a:LX/3xm;

    iget-object v3, v3, LX/3xm;->b:LX/1a1;

    .line 659671
    if-eqz v3, :cond_1

    .line 659672
    packed-switch v1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 659673
    :pswitch_1
    iget-object v1, p0, LX/3xd;->a:LX/3xm;

    iget-object v1, v1, LX/3xm;->r:Landroid/view/VelocityTracker;

    if-eqz v1, :cond_4

    .line 659674
    iget-object v1, p0, LX/3xd;->a:LX/3xm;

    iget-object v1, v1, LX/3xm;->r:Landroid/view/VelocityTracker;

    iget-object v2, p0, LX/3xd;->a:LX/3xm;

    iget-object v2, v2, LX/3xm;->p:Landroid/support/v7/widget/RecyclerView;

    .line 659675
    iget v3, v2, Landroid/support/v7/widget/RecyclerView;->V:I

    move v2, v3

    .line 659676
    int-to-float v2, v2

    invoke-virtual {v1, v5, v2}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 659677
    :cond_4
    iget-object v1, p0, LX/3xd;->a:LX/3xm;

    const/4 v2, 0x0

    invoke-static {v1, v2, v0}, LX/3xm;->a$redex0(LX/3xm;LX/1a1;I)V

    .line 659678
    iget-object v0, p0, LX/3xd;->a:LX/3xm;

    iput v4, v0, LX/3xm;->i:I

    goto :goto_0

    .line 659679
    :pswitch_2
    if-ltz v2, :cond_1

    .line 659680
    iget-object v0, p0, LX/3xd;->a:LX/3xm;

    iget-object v1, p0, LX/3xd;->a:LX/3xm;

    iget v1, v1, LX/3xm;->l:I

    invoke-static {v0, p1, v1, v2}, LX/3xm;->a$redex0(LX/3xm;Landroid/view/MotionEvent;II)V

    .line 659681
    iget-object v0, p0, LX/3xd;->a:LX/3xm;

    invoke-static {v0, v3}, LX/3xm;->c(LX/3xm;LX/1a1;)V

    .line 659682
    iget-object v0, p0, LX/3xd;->a:LX/3xm;

    iget-object v0, v0, LX/3xm;->p:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, LX/3xd;->a:LX/3xm;

    iget-object v1, v1, LX/3xm;->q:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 659683
    iget-object v0, p0, LX/3xd;->a:LX/3xm;

    iget-object v0, v0, LX/3xm;->q:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 659684
    iget-object v0, p0, LX/3xd;->a:LX/3xm;

    iget-object v0, v0, LX/3xm;->p:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->invalidate()V

    goto :goto_0

    .line 659685
    :pswitch_3
    invoke-static {p1}, LX/2xd;->b(Landroid/view/MotionEvent;)I

    move-result v1

    .line 659686
    invoke-static {p1, v1}, LX/2xd;->b(Landroid/view/MotionEvent;I)I

    move-result v2

    .line 659687
    iget-object v3, p0, LX/3xd;->a:LX/3xm;

    iget v3, v3, LX/3xm;->i:I

    if-ne v2, v3, :cond_1

    .line 659688
    iget-object v2, p0, LX/3xd;->a:LX/3xm;

    iget-object v2, v2, LX/3xm;->r:Landroid/view/VelocityTracker;

    if-eqz v2, :cond_5

    .line 659689
    iget-object v2, p0, LX/3xd;->a:LX/3xm;

    iget-object v2, v2, LX/3xm;->r:Landroid/view/VelocityTracker;

    iget-object v3, p0, LX/3xd;->a:LX/3xm;

    iget-object v3, v3, LX/3xm;->p:Landroid/support/v7/widget/RecyclerView;

    .line 659690
    iget v4, v3, Landroid/support/v7/widget/RecyclerView;->V:I

    move v3, v4

    .line 659691
    int-to-float v3, v3

    invoke-virtual {v2, v5, v3}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 659692
    :cond_5
    if-nez v1, :cond_6

    const/4 v0, 0x1

    .line 659693
    :cond_6
    iget-object v2, p0, LX/3xd;->a:LX/3xm;

    invoke-static {p1, v0}, LX/2xd;->b(Landroid/view/MotionEvent;I)I

    move-result v0

    iput v0, v2, LX/3xm;->i:I

    .line 659694
    iget-object v0, p0, LX/3xd;->a:LX/3xm;

    iget-object v2, p0, LX/3xd;->a:LX/3xm;

    iget v2, v2, LX/3xm;->l:I

    invoke-static {v0, p1, v2, v1}, LX/3xm;->a$redex0(LX/3xm;Landroid/view/MotionEvent;II)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
