.class public LX/4QE;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 703646
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 26

    .prologue
    .line 703647
    const/16 v22, 0x0

    .line 703648
    const/16 v21, 0x0

    .line 703649
    const/16 v20, 0x0

    .line 703650
    const/16 v19, 0x0

    .line 703651
    const/16 v18, 0x0

    .line 703652
    const/16 v17, 0x0

    .line 703653
    const/16 v16, 0x0

    .line 703654
    const/4 v15, 0x0

    .line 703655
    const/4 v14, 0x0

    .line 703656
    const/4 v13, 0x0

    .line 703657
    const/4 v12, 0x0

    .line 703658
    const/4 v11, 0x0

    .line 703659
    const/4 v10, 0x0

    .line 703660
    const/4 v9, 0x0

    .line 703661
    const/4 v8, 0x0

    .line 703662
    const/4 v7, 0x0

    .line 703663
    const/4 v6, 0x0

    .line 703664
    const/4 v5, 0x0

    .line 703665
    const/4 v4, 0x0

    .line 703666
    const/4 v3, 0x0

    .line 703667
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v23

    sget-object v24, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    if-eq v0, v1, :cond_1

    .line 703668
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 703669
    const/4 v3, 0x0

    .line 703670
    :goto_0
    return v3

    .line 703671
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 703672
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v23

    sget-object v24, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    if-eq v0, v1, :cond_13

    .line 703673
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v23

    .line 703674
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 703675
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v24

    sget-object v25, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    if-eq v0, v1, :cond_1

    if-eqz v23, :cond_1

    .line 703676
    const-string v24, "__type__"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-nez v24, :cond_2

    const-string v24, "__typename"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_3

    .line 703677
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/String;)I

    move-result v22

    goto :goto_1

    .line 703678
    :cond_3
    const-string v24, "action_type"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_4

    .line 703679
    const/4 v5, 0x1

    .line 703680
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Lcom/facebook/graphql/enums/GraphQLPageActionType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v21

    goto :goto_1

    .line 703681
    :cond_4
    const-string v24, "page"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_5

    .line 703682
    invoke-static/range {p0 .. p1}, LX/2bc;->a(LX/15w;LX/186;)I

    move-result v20

    goto :goto_1

    .line 703683
    :cond_5
    const-string v24, "content_type"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_6

    .line 703684
    const/4 v4, 0x1

    .line 703685
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Lcom/facebook/graphql/enums/GraphQLPagePresenceTabContentType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPagePresenceTabContentType;

    move-result-object v19

    goto :goto_1

    .line 703686
    :cond_6
    const-string v24, "description"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_7

    .line 703687
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v18

    goto :goto_1

    .line 703688
    :cond_7
    const-string v24, "name_propercase"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_8

    .line 703689
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v17

    goto/16 :goto_1

    .line 703690
    :cond_8
    const-string v24, "name_uppercase"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_9

    .line 703691
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v16

    goto/16 :goto_1

    .line 703692
    :cond_9
    const-string v24, "optional_id"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_a

    .line 703693
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    goto/16 :goto_1

    .line 703694
    :cond_a
    const-string v24, "reaction_surface"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_b

    .line 703695
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    goto/16 :goto_1

    .line 703696
    :cond_b
    const-string v24, "uri"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_c

    .line 703697
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    goto/16 :goto_1

    .line 703698
    :cond_c
    const-string v24, "action"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_d

    .line 703699
    invoke-static/range {p0 .. p1}, LX/4QE;->b(LX/15w;LX/186;)I

    move-result v12

    goto/16 :goto_1

    .line 703700
    :cond_d
    const-string v24, "preview_action_type"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_e

    .line 703701
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    goto/16 :goto_1

    .line 703702
    :cond_e
    const-string v24, "preview_optional_id"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_f

    .line 703703
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto/16 :goto_1

    .line 703704
    :cond_f
    const-string v24, "preview_text"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_10

    .line 703705
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v9

    goto/16 :goto_1

    .line 703706
    :cond_10
    const-string v24, "tab_type"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_11

    .line 703707
    const/4 v3, 0x1

    .line 703708
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/facebook/graphql/enums/GraphQLPageActionType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v8

    goto/16 :goto_1

    .line 703709
    :cond_11
    const-string v24, "call_to_action"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_12

    .line 703710
    invoke-static/range {p0 .. p1}, LX/4QK;->a(LX/15w;LX/186;)I

    move-result v7

    goto/16 :goto_1

    .line 703711
    :cond_12
    const-string v24, "callout_text"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_0

    .line 703712
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v6

    goto/16 :goto_1

    .line 703713
    :cond_13
    const/16 v23, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 703714
    const/16 v23, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v23

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 703715
    if-eqz v5, :cond_14

    .line 703716
    const/4 v5, 0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v5, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 703717
    :cond_14
    const/4 v5, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 703718
    if-eqz v4, :cond_15

    .line 703719
    const/4 v4, 0x3

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v4, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 703720
    :cond_15
    const/4 v4, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 703721
    const/4 v4, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 703722
    const/4 v4, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 703723
    const/4 v4, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v15}, LX/186;->b(II)V

    .line 703724
    const/16 v4, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v14}, LX/186;->b(II)V

    .line 703725
    const/16 v4, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v13}, LX/186;->b(II)V

    .line 703726
    const/16 v4, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v12}, LX/186;->b(II)V

    .line 703727
    const/16 v4, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v11}, LX/186;->b(II)V

    .line 703728
    const/16 v4, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v10}, LX/186;->b(II)V

    .line 703729
    const/16 v4, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v9}, LX/186;->b(II)V

    .line 703730
    if-eqz v3, :cond_16

    .line 703731
    const/16 v3, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, LX/186;->a(ILjava/lang/Enum;)V

    .line 703732
    :cond_16
    const/16 v3, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 703733
    const/16 v3, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 703734
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    const/16 v4, 0xe

    const/4 v3, 0x3

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 703735
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 703736
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 703737
    if-eqz v0, :cond_0

    .line 703738
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 703739
    invoke-static {p0, p1, v2, p2}, LX/2bt;->a(LX/15i;IILX/0nX;)V

    .line 703740
    :cond_0
    invoke-virtual {p0, p1, v1, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 703741
    if-eqz v0, :cond_1

    .line 703742
    const-string v0, "action_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 703743
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPageActionType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 703744
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 703745
    if-eqz v0, :cond_2

    .line 703746
    const-string v1, "page"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 703747
    invoke-static {p0, v0, p2, p3}, LX/2bc;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 703748
    :cond_2
    invoke-virtual {p0, p1, v3, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 703749
    if-eqz v0, :cond_3

    .line 703750
    const-string v0, "content_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 703751
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPagePresenceTabContentType;

    invoke-virtual {p0, p1, v3, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPagePresenceTabContentType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPagePresenceTabContentType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 703752
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 703753
    if-eqz v0, :cond_4

    .line 703754
    const-string v1, "description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 703755
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 703756
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 703757
    if-eqz v0, :cond_5

    .line 703758
    const-string v1, "name_propercase"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 703759
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 703760
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 703761
    if-eqz v0, :cond_6

    .line 703762
    const-string v1, "name_uppercase"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 703763
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 703764
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 703765
    if-eqz v0, :cond_7

    .line 703766
    const-string v1, "optional_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 703767
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 703768
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 703769
    if-eqz v0, :cond_8

    .line 703770
    const-string v1, "reaction_surface"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 703771
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 703772
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 703773
    if-eqz v0, :cond_9

    .line 703774
    const-string v1, "uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 703775
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 703776
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 703777
    if-eqz v0, :cond_a

    .line 703778
    const-string v1, "action"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 703779
    invoke-static {p0, v0, p2, p3}, LX/4QE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 703780
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 703781
    if-eqz v0, :cond_b

    .line 703782
    const-string v1, "preview_action_type"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 703783
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 703784
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 703785
    if-eqz v0, :cond_c

    .line 703786
    const-string v1, "preview_optional_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 703787
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 703788
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 703789
    if-eqz v0, :cond_d

    .line 703790
    const-string v1, "preview_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 703791
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 703792
    :cond_d
    invoke-virtual {p0, p1, v4, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 703793
    if-eqz v0, :cond_e

    .line 703794
    const-string v0, "tab_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 703795
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    invoke-virtual {p0, p1, v4, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPageActionType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 703796
    :cond_e
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 703797
    if-eqz v0, :cond_f

    .line 703798
    const-string v1, "call_to_action"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 703799
    invoke-static {p0, v0, p2, p3}, LX/4QK;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 703800
    :cond_f
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 703801
    if-eqz v0, :cond_10

    .line 703802
    const-string v1, "callout_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 703803
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 703804
    :cond_10
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 703805
    return-void
.end method
