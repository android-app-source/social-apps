.class public LX/3fD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/pages/adminedpages/protocol/PagesAccessTokenPrefetchMethod$Params;",
        "Lcom/facebook/pages/adminedpages/protocol/PagesAccessTokenPrefetchMethod$Result;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/3fA;


# direct methods
.method public constructor <init>(LX/3fA;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 621616
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 621617
    iput-object p1, p0, LX/3fD;->a:LX/3fA;

    .line 621618
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 621619
    check-cast p1, Lcom/facebook/pages/adminedpages/protocol/PagesAccessTokenPrefetchMethod$Params;

    .line 621620
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 621621
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "format"

    const-string v2, "json"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 621622
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "q"

    .line 621623
    new-instance v2, LX/3fG;

    invoke-direct {v2}, LX/3fG;-><init>()V

    .line 621624
    const-string v3, "page_query"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string p0, "SELECT page_id FROM page_admin WHERE uid = me() AND type != \'APPLICATION\' ORDER BY last_used_time DESC LIMIT "

    invoke-direct {v5, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 621625
    iget p0, p1, Lcom/facebook/pages/adminedpages/protocol/PagesAccessTokenPrefetchMethod$Params;->a:I

    move p0, p0

    .line 621626
    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v3, v5}, LX/3fG;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 621627
    const-string v3, "token_query"

    const-string v5, "SELECT page_id, access_token FROM page WHERE page_id IN (SELECT page_id FROM #page_query)"

    invoke-virtual {v2, v3, v5}, LX/3fG;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 621628
    invoke-virtual {v2}, LX/3fG;->a()LX/0lF;

    move-result-object v2

    invoke-virtual {v2}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v2, v2

    .line 621629
    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 621630
    new-instance v0, LX/14N;

    const-string v1, "page_access_token"

    const-string v2, "GET"

    const-string v3, "fql"

    sget-object v5, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 621631
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 621632
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 621633
    new-instance v1, LX/3fM;

    invoke-direct {v1, v0}, LX/3fM;-><init>(LX/0lF;)V

    .line 621634
    new-instance v0, Lcom/facebook/pages/adminedpages/protocol/PagesAccessTokenPrefetchMethod$Result;

    sget-object v2, LX/0ta;->FROM_SERVER:LX/0ta;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-string v3, "token_query"

    invoke-virtual {v1, v3}, LX/3fM;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    .line 621635
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v6

    .line 621636
    invoke-virtual {v1}, LX/0lF;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0lF;

    .line 621637
    const-string p1, "page_id"

    invoke-virtual {v3, p1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object p1

    .line 621638
    const-string p2, "access_token"

    invoke-virtual {v3, p2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    .line 621639
    if-eqz p1, :cond_0

    if-eqz v3, :cond_0

    .line 621640
    invoke-virtual {p1}, LX/0lF;->B()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, p1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 621641
    :cond_1
    move-object v1, v6

    .line 621642
    invoke-direct {v0, v2, v4, v5, v1}, Lcom/facebook/pages/adminedpages/protocol/PagesAccessTokenPrefetchMethod$Result;-><init>(LX/0ta;JLjava/util/Map;)V

    return-object v0
.end method
