.class public LX/51U;
.super LX/4x6;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/Beta;
.end annotation

.annotation build Lcom/google/common/annotations/GwtIncompatible;
    value = "uses NavigableMap"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<C::",
        "Ljava/lang/Comparable",
        "<*>;>",
        "LX/4x6",
        "<TC;>;"
    }
.end annotation


# instance fields
.field public final a:Ljava/util/NavigableMap;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/NavigableMap",
            "<",
            "LX/4xM",
            "<TC;>;",
            "LX/50M",
            "<TC;>;>;"
        }
    .end annotation
.end field

.field private transient b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/50M",
            "<TC;>;>;"
        }
    .end annotation
.end field

.field private transient c:LX/4x5;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4x5",
            "<TC;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/NavigableMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/NavigableMap",
            "<",
            "LX/4xM",
            "<TC;>;",
            "LX/50M",
            "<TC;>;>;)V"
        }
    .end annotation

    .prologue
    .line 825142
    invoke-direct {p0}, LX/4x6;-><init>()V

    .line 825143
    iput-object p1, p0, LX/51U;->a:Ljava/util/NavigableMap;

    .line 825144
    return-void
.end method

.method private d(LX/50M;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/50M",
            "<TC;>;)V"
        }
    .end annotation

    .prologue
    .line 825138
    invoke-virtual {p1}, LX/50M;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 825139
    iget-object v0, p0, LX/51U;->a:Ljava/util/NavigableMap;

    iget-object v1, p1, LX/50M;->lowerBound:LX/4xM;

    invoke-interface {v0, v1}, Ljava/util/NavigableMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 825140
    :goto_0
    return-void

    .line 825141
    :cond_0
    iget-object v0, p0, LX/51U;->a:Ljava/util/NavigableMap;

    iget-object v1, p1, LX/50M;->lowerBound:LX/4xM;

    invoke-interface {v0, v1, p1}, Ljava/util/NavigableMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "LX/50M",
            "<TC;>;>;"
        }
    .end annotation

    .prologue
    .line 825136
    iget-object v0, p0, LX/51U;->b:Ljava/util/Set;

    .line 825137
    if-nez v0, :cond_0

    new-instance v0, LX/51T;

    iget-object v1, p0, LX/51U;->a:Ljava/util/NavigableMap;

    invoke-interface {v1}, Ljava/util/NavigableMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LX/51T;-><init>(LX/51U;Ljava/util/Collection;)V

    iput-object v0, p0, LX/51U;->b:Ljava/util/Set;

    :cond_0
    return-object v0
.end method

.method public a(LX/50M;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/50M",
            "<TC;>;)V"
        }
    .end annotation

    .prologue
    .line 825145
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 825146
    invoke-virtual {p1}, LX/50M;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 825147
    :goto_0
    return-void

    .line 825148
    :cond_0
    iget-object v2, p1, LX/50M;->lowerBound:LX/4xM;

    .line 825149
    iget-object v1, p1, LX/50M;->upperBound:LX/4xM;

    .line 825150
    iget-object v0, p0, LX/51U;->a:Ljava/util/NavigableMap;

    invoke-interface {v0, v2}, Ljava/util/NavigableMap;->lowerEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    .line 825151
    if-eqz v0, :cond_2

    .line 825152
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/50M;

    .line 825153
    iget-object v3, v0, LX/50M;->upperBound:LX/4xM;

    invoke-virtual {v3, v2}, LX/4xM;->a(LX/4xM;)I

    move-result v3

    if-ltz v3, :cond_2

    .line 825154
    iget-object v2, v0, LX/50M;->upperBound:LX/4xM;

    invoke-virtual {v2, v1}, LX/4xM;->a(LX/4xM;)I

    move-result v2

    if-ltz v2, :cond_1

    .line 825155
    iget-object v1, v0, LX/50M;->upperBound:LX/4xM;

    .line 825156
    :cond_1
    iget-object v0, v0, LX/50M;->lowerBound:LX/4xM;

    move-object v2, v0

    .line 825157
    :cond_2
    iget-object v0, p0, LX/51U;->a:Ljava/util/NavigableMap;

    invoke-interface {v0, v1}, Ljava/util/NavigableMap;->floorEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    .line 825158
    if-eqz v0, :cond_3

    .line 825159
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/50M;

    .line 825160
    iget-object v3, v0, LX/50M;->upperBound:LX/4xM;

    invoke-virtual {v3, v1}, LX/4xM;->a(LX/4xM;)I

    move-result v3

    if-ltz v3, :cond_3

    .line 825161
    iget-object v1, v0, LX/50M;->upperBound:LX/4xM;

    .line 825162
    :cond_3
    iget-object v0, p0, LX/51U;->a:Ljava/util/NavigableMap;

    invoke-interface {v0, v2, v1}, Ljava/util/NavigableMap;->subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedMap;->clear()V

    .line 825163
    invoke-static {v2, v1}, LX/50M;->a(LX/4xM;LX/4xM;)LX/50M;

    move-result-object v0

    invoke-direct {p0, v0}, LX/51U;->d(LX/50M;)V

    goto :goto_0
.end method

.method public b()LX/4x5;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/4x5",
            "<TC;>;"
        }
    .end annotation

    .prologue
    .line 825134
    iget-object v0, p0, LX/51U;->c:LX/4x5;

    .line 825135
    if-nez v0, :cond_0

    new-instance v0, LX/51V;

    invoke-direct {v0, p0}, LX/51V;-><init>(LX/51U;)V

    iput-object v0, p0, LX/51U;->c:LX/4x5;

    :cond_0
    return-object v0
.end method

.method public b(LX/50M;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/50M",
            "<TC;>;)V"
        }
    .end annotation

    .prologue
    .line 825118
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 825119
    invoke-virtual {p1}, LX/50M;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 825120
    :goto_0
    return-void

    .line 825121
    :cond_0
    iget-object v0, p0, LX/51U;->a:Ljava/util/NavigableMap;

    iget-object v1, p1, LX/50M;->lowerBound:LX/4xM;

    invoke-interface {v0, v1}, Ljava/util/NavigableMap;->lowerEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    .line 825122
    if-eqz v0, :cond_2

    .line 825123
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/50M;

    .line 825124
    iget-object v1, v0, LX/50M;->upperBound:LX/4xM;

    iget-object v2, p1, LX/50M;->lowerBound:LX/4xM;

    invoke-virtual {v1, v2}, LX/4xM;->a(LX/4xM;)I

    move-result v1

    if-ltz v1, :cond_2

    .line 825125
    invoke-virtual {p1}, LX/50M;->f()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, v0, LX/50M;->upperBound:LX/4xM;

    iget-object v2, p1, LX/50M;->upperBound:LX/4xM;

    invoke-virtual {v1, v2}, LX/4xM;->a(LX/4xM;)I

    move-result v1

    if-ltz v1, :cond_1

    .line 825126
    iget-object v1, p1, LX/50M;->upperBound:LX/4xM;

    iget-object v2, v0, LX/50M;->upperBound:LX/4xM;

    invoke-static {v1, v2}, LX/50M;->a(LX/4xM;LX/4xM;)LX/50M;

    move-result-object v1

    invoke-direct {p0, v1}, LX/51U;->d(LX/50M;)V

    .line 825127
    :cond_1
    iget-object v0, v0, LX/50M;->lowerBound:LX/4xM;

    iget-object v1, p1, LX/50M;->lowerBound:LX/4xM;

    invoke-static {v0, v1}, LX/50M;->a(LX/4xM;LX/4xM;)LX/50M;

    move-result-object v0

    invoke-direct {p0, v0}, LX/51U;->d(LX/50M;)V

    .line 825128
    :cond_2
    iget-object v0, p0, LX/51U;->a:Ljava/util/NavigableMap;

    iget-object v1, p1, LX/50M;->upperBound:LX/4xM;

    invoke-interface {v0, v1}, Ljava/util/NavigableMap;->floorEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    .line 825129
    if-eqz v0, :cond_3

    .line 825130
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/50M;

    .line 825131
    invoke-virtual {p1}, LX/50M;->f()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, v0, LX/50M;->upperBound:LX/4xM;

    iget-object v2, p1, LX/50M;->upperBound:LX/4xM;

    invoke-virtual {v1, v2}, LX/4xM;->a(LX/4xM;)I

    move-result v1

    if-ltz v1, :cond_3

    .line 825132
    iget-object v1, p1, LX/50M;->upperBound:LX/4xM;

    iget-object v0, v0, LX/50M;->upperBound:LX/4xM;

    invoke-static {v1, v0}, LX/50M;->a(LX/4xM;LX/4xM;)LX/50M;

    move-result-object v0

    invoke-direct {p0, v0}, LX/51U;->d(LX/50M;)V

    .line 825133
    :cond_3
    iget-object v0, p0, LX/51U;->a:Ljava/util/NavigableMap;

    iget-object v1, p1, LX/50M;->lowerBound:LX/4xM;

    iget-object v2, p1, LX/50M;->upperBound:LX/4xM;

    invoke-interface {v0, v1, v2}, Ljava/util/NavigableMap;->subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedMap;->clear()V

    goto :goto_0
.end method

.method public final c(LX/50M;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/50M",
            "<TC;>;)Z"
        }
    .end annotation

    .prologue
    .line 825113
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 825114
    iget-object v0, p0, LX/51U;->a:Ljava/util/NavigableMap;

    iget-object v1, p1, LX/50M;->lowerBound:LX/4xM;

    invoke-interface {v0, v1}, Ljava/util/NavigableMap;->floorEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    .line 825115
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/50M;

    .line 825116
    iget-object v1, v0, LX/50M;->lowerBound:LX/4xM;

    iget-object p0, p1, LX/50M;->lowerBound:LX/4xM;

    invoke-virtual {v1, p0}, LX/4xM;->a(LX/4xM;)I

    move-result v1

    if-gtz v1, :cond_1

    iget-object v1, v0, LX/50M;->upperBound:LX/4xM;

    iget-object p0, p1, LX/50M;->upperBound:LX/4xM;

    invoke-virtual {v1, p0}, LX/4xM;->a(LX/4xM;)I

    move-result v1

    if-ltz v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 825117
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
