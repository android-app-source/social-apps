.class public LX/4L0;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 681473
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 12

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x1

    const/4 v6, 0x0

    .line 681474
    const/4 v0, 0x0

    .line 681475
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v3, :cond_6

    .line 681476
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 681477
    :goto_0
    return v6

    .line 681478
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_3

    .line 681479
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 681480
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 681481
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_0

    if-eqz v9, :cond_0

    .line 681482
    const-string v10, "granularity"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 681483
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/facebook/graphql/enums/GraphQLDateGranularity;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDateGranularity;

    move-result-object v7

    move-object v8, v7

    move v7, v1

    goto :goto_1

    .line 681484
    :cond_1
    const-string v10, "time"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 681485
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    move v0, v1

    goto :goto_1

    .line 681486
    :cond_2
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 681487
    :cond_3
    const/4 v9, 0x2

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 681488
    if-eqz v7, :cond_4

    .line 681489
    invoke-virtual {p1, v6, v8}, LX/186;->a(ILjava/lang/Enum;)V

    .line 681490
    :cond_4
    if-eqz v0, :cond_5

    move-object v0, p1

    .line 681491
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 681492
    :cond_5
    invoke-virtual {p1}, LX/186;->d()I

    move-result v6

    goto :goto_0

    :cond_6
    move v7, v6

    move-wide v2, v4

    move-object v8, v0

    move v0, v6

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 681493
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 681494
    invoke-virtual {p0, p1, v1, v1}, LX/15i;->a(IIS)S

    move-result v0

    .line 681495
    if-eqz v0, :cond_0

    .line 681496
    const-string v0, "granularity"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 681497
    const-class v0, Lcom/facebook/graphql/enums/GraphQLDateGranularity;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLDateGranularity;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLDateGranularity;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 681498
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 681499
    cmp-long v2, v0, v2

    if-eqz v2, :cond_1

    .line 681500
    const-string v2, "time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 681501
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 681502
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 681503
    return-void
.end method
