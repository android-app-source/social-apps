.class public final LX/3z5;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryInterfaces$Configuration;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;)V
    .locals 0

    .prologue
    .line 662100
    iput-object p1, p0, LX/3z5;->a:Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationModel;)V
    .locals 1

    .prologue
    .line 662101
    iget-object v0, p0, LX/3z5;->a:Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;

    .line 662102
    iput-object p1, v0, Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;->j:Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationModel;

    .line 662103
    iget-object v0, p0, LX/3z5;->a:Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;

    invoke-static {v0}, Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;->d(Lcom/facebook/abtest/qe/settings/QuickExperimentViewActivity;)V

    .line 662104
    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 662105
    invoke-static {p1}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 662106
    check-cast p1, Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationModel;

    invoke-direct {p0, p1}, LX/3z5;->a(Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationModel;)V

    return-void
.end method
