.class public LX/4sE;
.super LX/15u;
.source ""


# static fields
.field public static final ad:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Ljava/lang/ref/SoftReference",
            "<",
            "LX/4s6",
            "<",
            "Ljava/lang/String;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private static final ae:[I

.field private static final af:[Ljava/lang/String;


# instance fields
.field public M:LX/0lD;

.field public N:Z

.field public final O:LX/4s6;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4s6",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public P:Ljava/io/InputStream;

.field public Q:[B

.field public R:Z

.field public S:Z

.field public T:I

.field public U:Z

.field public final V:LX/0lv;

.field public W:[I

.field public X:I

.field public Y:I

.field public Z:[Ljava/lang/String;

.field public aa:I

.field public ab:[Ljava/lang/String;

.field public ac:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 818945
    new-array v0, v1, [I

    sput-object v0, LX/4sE;->ae:[I

    .line 818946
    new-array v0, v1, [Ljava/lang/String;

    sput-object v0, LX/4sE;->af:[Ljava/lang/String;

    .line 818947
    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    sput-object v0, LX/4sE;->ad:Ljava/lang/ThreadLocal;

    return-void
.end method

.method public constructor <init>(LX/12A;ILX/0lD;LX/0lv;Ljava/io/InputStream;[BIIZ)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 818927
    invoke-direct {p0, p1, p2}, LX/15u;-><init>(LX/12A;I)V

    .line 818928
    iput-boolean v2, p0, LX/4sE;->S:Z

    .line 818929
    sget-object v0, LX/4sE;->ae:[I

    iput-object v0, p0, LX/4sE;->W:[I

    .line 818930
    sget-object v0, LX/4sE;->af:[Ljava/lang/String;

    iput-object v0, p0, LX/4sE;->Z:[Ljava/lang/String;

    .line 818931
    iput v2, p0, LX/4sE;->aa:I

    .line 818932
    const/4 v0, 0x0

    iput-object v0, p0, LX/4sE;->ab:[Ljava/lang/String;

    .line 818933
    iput v1, p0, LX/4sE;->ac:I

    .line 818934
    iput-object p3, p0, LX/4sE;->M:LX/0lD;

    .line 818935
    iput-object p4, p0, LX/4sE;->V:LX/0lv;

    .line 818936
    iput-object p5, p0, LX/4sE;->P:Ljava/io/InputStream;

    .line 818937
    iput-object p6, p0, LX/4sE;->Q:[B

    .line 818938
    iput p7, p0, LX/4sE;->d:I

    .line 818939
    iput p8, p0, LX/4sE;->e:I

    .line 818940
    iput-boolean p9, p0, LX/4sE;->R:Z

    .line 818941
    iput v1, p0, LX/4sE;->j:I

    .line 818942
    iput v1, p0, LX/4sE;->k:I

    .line 818943
    invoke-static {}, LX/4sE;->V()LX/4s6;

    move-result-object v0

    iput-object v0, p0, LX/4sE;->O:LX/4s6;

    .line 818944
    return-void
.end method

.method private static V()LX/4s6;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/4s6",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 818920
    sget-object v0, LX/4sE;->ad:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/SoftReference;

    .line 818921
    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 818922
    :goto_0
    if-nez v0, :cond_0

    .line 818923
    new-instance v0, LX/4s6;

    invoke-direct {v0}, LX/4s6;-><init>()V

    .line 818924
    sget-object v1, LX/4sE;->ad:Ljava/lang/ThreadLocal;

    new-instance v2, Ljava/lang/ref/SoftReference;

    invoke-direct {v2, v0}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 818925
    :cond_0
    return-object v0

    .line 818926
    :cond_1
    invoke-virtual {v0}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4s6;

    goto :goto_0
.end method

.method private final W()V
    .locals 3

    .prologue
    .line 818915
    invoke-direct {p0}, LX/4sE;->aa()V

    .line 818916
    iget v0, p0, LX/4sE;->ac:I

    iget-object v1, p0, LX/4sE;->ab:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 818917
    iget-object v0, p0, LX/4sE;->ab:[Ljava/lang/String;

    iget v1, p0, LX/4sE;->ac:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/4sE;->ac:I

    iget-object v2, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v2}, LX/15x;->g()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 818918
    :goto_0
    return-void

    .line 818919
    :cond_0
    invoke-direct {p0}, LX/4sE;->X()V

    goto :goto_0
.end method

.method private final X()V
    .locals 5

    .prologue
    const/16 v0, 0x400

    const/16 v4, 0x40

    const/4 v3, 0x0

    .line 818901
    iget-object v1, p0, LX/4sE;->ab:[Ljava/lang/String;

    .line 818902
    array-length v2, v1

    .line 818903
    if-nez v2, :cond_1

    .line 818904
    iget-object v0, p0, LX/4sE;->O:LX/4s6;

    invoke-virtual {v0}, LX/4s6;->b()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 818905
    if-nez v0, :cond_0

    .line 818906
    new-array v0, v4, [Ljava/lang/String;

    .line 818907
    :cond_0
    :goto_0
    iput-object v0, p0, LX/4sE;->ab:[Ljava/lang/String;

    .line 818908
    iget-object v0, p0, LX/4sE;->ab:[Ljava/lang/String;

    iget v1, p0, LX/4sE;->ac:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/4sE;->ac:I

    iget-object v2, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v2}, LX/15x;->g()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 818909
    return-void

    .line 818910
    :cond_1
    if-ne v2, v0, :cond_2

    .line 818911
    iput v3, p0, LX/4sE;->ac:I

    move-object v0, v1

    goto :goto_0

    .line 818912
    :cond_2
    if-ne v2, v4, :cond_3

    const/16 v0, 0x100

    .line 818913
    :cond_3
    new-array v0, v0, [Ljava/lang/String;

    .line 818914
    array-length v2, v1

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0
.end method

.method private Y()LX/15z;
    .locals 4

    .prologue
    .line 818837
    iget v0, p0, LX/15u;->d:I

    iget v1, p0, LX/15u;->e:I

    if-lt v0, v1, :cond_0

    .line 818838
    invoke-virtual {p0}, LX/15u;->K()V

    .line 818839
    :cond_0
    iget-object v0, p0, LX/4sE;->Q:[B

    iget v1, p0, LX/15u;->d:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/4sE;->d:I

    aget-byte v0, v0, v1

    .line 818840
    iput v0, p0, LX/4sE;->T:I

    .line 818841
    shr-int/lit8 v1, v0, 0x6

    and-int/lit8 v1, v1, 0x3

    packed-switch v1, :pswitch_data_0

    .line 818842
    :cond_1
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Invalid type marker byte 0x"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, LX/4sE;->T:I

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " for expected field name (or END_OBJECT marker)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/15v;->e(Ljava/lang/String;)V

    .line 818843
    const/4 v0, 0x0

    :goto_1
    return-object v0

    .line 818844
    :pswitch_0
    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 818845
    :sswitch_0
    iget-object v0, p0, LX/15u;->l:LX/15y;

    const-string v1, ""

    .line 818846
    iput-object v1, v0, LX/15y;->f:Ljava/lang/String;

    .line 818847
    sget-object v0, LX/15z;->FIELD_NAME:LX/15z;

    goto :goto_1

    .line 818848
    :sswitch_1
    iget v1, p0, LX/15u;->d:I

    iget v2, p0, LX/15u;->e:I

    if-lt v1, v2, :cond_2

    .line 818849
    invoke-virtual {p0}, LX/15u;->K()V

    .line 818850
    :cond_2
    and-int/lit8 v0, v0, 0x3

    shl-int/lit8 v0, v0, 0x8

    iget-object v1, p0, LX/4sE;->Q:[B

    iget v2, p0, LX/15u;->d:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/4sE;->d:I

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    add-int/2addr v0, v1

    .line 818851
    iget v1, p0, LX/4sE;->aa:I

    if-lt v0, v1, :cond_3

    .line 818852
    invoke-direct {p0, v0}, LX/4sE;->t(I)V

    .line 818853
    :cond_3
    iget-object v1, p0, LX/15u;->l:LX/15y;

    iget-object v2, p0, LX/4sE;->Z:[Ljava/lang/String;

    aget-object v0, v2, v0

    .line 818854
    iput-object v0, v1, LX/15y;->f:Ljava/lang/String;

    .line 818855
    sget-object v0, LX/15z;->FIELD_NAME:LX/15z;

    goto :goto_1

    .line 818856
    :sswitch_2
    invoke-direct {p0}, LX/4sE;->Z()V

    .line 818857
    sget-object v0, LX/15z;->FIELD_NAME:LX/15z;

    goto :goto_1

    .line 818858
    :pswitch_1
    and-int/lit8 v0, v0, 0x3f

    .line 818859
    iget v1, p0, LX/4sE;->aa:I

    if-lt v0, v1, :cond_4

    .line 818860
    invoke-direct {p0, v0}, LX/4sE;->t(I)V

    .line 818861
    :cond_4
    iget-object v1, p0, LX/15u;->l:LX/15y;

    iget-object v2, p0, LX/4sE;->Z:[Ljava/lang/String;

    aget-object v0, v2, v0

    .line 818862
    iput-object v0, v1, LX/15y;->f:Ljava/lang/String;

    .line 818863
    sget-object v0, LX/15z;->FIELD_NAME:LX/15z;

    goto :goto_1

    .line 818864
    :pswitch_2
    and-int/lit8 v0, v0, 0x3f

    add-int/lit8 v1, v0, 0x1

    .line 818865
    invoke-direct {p0, v1}, LX/4sE;->j(I)LX/0lx;

    move-result-object v0

    .line 818866
    if-eqz v0, :cond_7

    .line 818867
    iget-object v2, v0, LX/0lx;->a:Ljava/lang/String;

    move-object v0, v2

    .line 818868
    iget v2, p0, LX/15u;->d:I

    add-int/2addr v1, v2

    iput v1, p0, LX/4sE;->d:I

    .line 818869
    :goto_2
    iget-object v1, p0, LX/4sE;->Z:[Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 818870
    iget v1, p0, LX/4sE;->aa:I

    iget-object v2, p0, LX/4sE;->Z:[Ljava/lang/String;

    array-length v2, v2

    if-lt v1, v2, :cond_5

    .line 818871
    iget-object v1, p0, LX/4sE;->Z:[Ljava/lang/String;

    invoke-direct {p0, v1}, LX/4sE;->a([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/4sE;->Z:[Ljava/lang/String;

    .line 818872
    :cond_5
    iget-object v1, p0, LX/4sE;->Z:[Ljava/lang/String;

    iget v2, p0, LX/4sE;->aa:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/4sE;->aa:I

    aput-object v0, v1, v2

    .line 818873
    :cond_6
    iget-object v1, p0, LX/15u;->l:LX/15y;

    .line 818874
    iput-object v0, v1, LX/15y;->f:Ljava/lang/String;

    .line 818875
    sget-object v0, LX/15z;->FIELD_NAME:LX/15z;

    goto/16 :goto_1

    .line 818876
    :cond_7
    invoke-direct {p0, v1}, LX/4sE;->h(I)Ljava/lang/String;

    move-result-object v0

    .line 818877
    invoke-direct {p0, v1, v0}, LX/4sE;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 818878
    :pswitch_3
    and-int/lit8 v0, v0, 0x3f

    .line 818879
    const/16 v1, 0x37

    if-le v0, v1, :cond_9

    .line 818880
    const/16 v1, 0x3b

    if-ne v0, v1, :cond_1

    .line 818881
    iget-object v0, p0, LX/15u;->l:LX/15y;

    invoke-virtual {v0}, LX/12V;->d()Z

    move-result v0

    if-nez v0, :cond_8

    .line 818882
    const/16 v0, 0x7d

    const/16 v1, 0x5d

    invoke-virtual {p0, v0, v1}, LX/15u;->a(IC)V

    .line 818883
    :cond_8
    iget-object v0, p0, LX/15u;->l:LX/15y;

    .line 818884
    iget-object v1, v0, LX/15y;->c:LX/15y;

    move-object v0, v1

    .line 818885
    iput-object v0, p0, LX/4sE;->l:LX/15y;

    .line 818886
    sget-object v0, LX/15z;->END_OBJECT:LX/15z;

    goto/16 :goto_1

    .line 818887
    :cond_9
    add-int/lit8 v1, v0, 0x2

    .line 818888
    invoke-direct {p0, v1}, LX/4sE;->j(I)LX/0lx;

    move-result-object v0

    .line 818889
    if-eqz v0, :cond_c

    .line 818890
    iget-object v2, v0, LX/0lx;->a:Ljava/lang/String;

    move-object v0, v2

    .line 818891
    iget v2, p0, LX/15u;->d:I

    add-int/2addr v1, v2

    iput v1, p0, LX/4sE;->d:I

    .line 818892
    :goto_3
    iget-object v1, p0, LX/4sE;->Z:[Ljava/lang/String;

    if-eqz v1, :cond_b

    .line 818893
    iget v1, p0, LX/4sE;->aa:I

    iget-object v2, p0, LX/4sE;->Z:[Ljava/lang/String;

    array-length v2, v2

    if-lt v1, v2, :cond_a

    .line 818894
    iget-object v1, p0, LX/4sE;->Z:[Ljava/lang/String;

    invoke-direct {p0, v1}, LX/4sE;->a([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/4sE;->Z:[Ljava/lang/String;

    .line 818895
    :cond_a
    iget-object v1, p0, LX/4sE;->Z:[Ljava/lang/String;

    iget v2, p0, LX/4sE;->aa:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/4sE;->aa:I

    aput-object v0, v1, v2

    .line 818896
    :cond_b
    iget-object v1, p0, LX/15u;->l:LX/15y;

    .line 818897
    iput-object v0, v1, LX/15y;->f:Ljava/lang/String;

    .line 818898
    sget-object v0, LX/15z;->FIELD_NAME:LX/15z;

    goto/16 :goto_1

    .line 818899
    :cond_c
    invoke-direct {p0, v1}, LX/4sE;->i(I)Ljava/lang/String;

    move-result-object v0

    .line 818900
    invoke-direct {p0, v1, v0}, LX/4sE;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0x20 -> :sswitch_0
        0x30 -> :sswitch_1
        0x31 -> :sswitch_1
        0x32 -> :sswitch_1
        0x33 -> :sswitch_1
        0x34 -> :sswitch_2
    .end sparse-switch
.end method

.method private final Z()V
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v6, -0x4

    .line 818787
    iget-object v4, p0, LX/4sE;->Q:[B

    move v0, v1

    move v2, v1

    .line 818788
    :goto_0
    iget v3, p0, LX/15u;->d:I

    iget v5, p0, LX/15u;->e:I

    if-lt v3, v5, :cond_0

    .line 818789
    invoke-virtual {p0}, LX/15u;->K()V

    .line 818790
    :cond_0
    iget v3, p0, LX/15u;->d:I

    add-int/lit8 v5, v3, 0x1

    iput v5, p0, LX/4sE;->d:I

    aget-byte v3, v4, v3

    .line 818791
    if-ne v6, v3, :cond_5

    move v3, v0

    move v4, v1

    .line 818792
    :goto_1
    shl-int/lit8 v0, v2, 0x2

    .line 818793
    if-lez v4, :cond_2

    .line 818794
    iget-object v1, p0, LX/4sE;->W:[I

    array-length v1, v1

    if-lt v2, v1, :cond_1

    .line 818795
    iget-object v1, p0, LX/4sE;->W:[I

    iget-object v5, p0, LX/4sE;->W:[I

    array-length v5, v5

    add-int/lit16 v5, v5, 0x100

    invoke-static {v1, v5}, LX/4sE;->a([II)[I

    move-result-object v1

    iput-object v1, p0, LX/4sE;->W:[I

    .line 818796
    :cond_1
    iget-object v5, p0, LX/4sE;->W:[I

    add-int/lit8 v1, v2, 0x1

    aput v3, v5, v2

    .line 818797
    add-int/2addr v0, v4

    move v2, v1

    .line 818798
    :cond_2
    iget-object v1, p0, LX/4sE;->V:LX/0lv;

    iget-object v3, p0, LX/4sE;->W:[I

    invoke-virtual {v1, v3, v2}, LX/0lv;->a([II)LX/0lx;

    move-result-object v1

    .line 818799
    if-eqz v1, :cond_d

    .line 818800
    iget-object v0, v1, LX/0lx;->a:Ljava/lang/String;

    move-object v0, v0

    .line 818801
    :goto_2
    iget-object v1, p0, LX/4sE;->Z:[Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 818802
    iget v1, p0, LX/4sE;->aa:I

    iget-object v2, p0, LX/4sE;->Z:[Ljava/lang/String;

    array-length v2, v2

    if-lt v1, v2, :cond_3

    .line 818803
    iget-object v1, p0, LX/4sE;->Z:[Ljava/lang/String;

    invoke-direct {p0, v1}, LX/4sE;->a([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/4sE;->Z:[Ljava/lang/String;

    .line 818804
    :cond_3
    iget-object v1, p0, LX/4sE;->Z:[Ljava/lang/String;

    iget v2, p0, LX/4sE;->aa:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/4sE;->aa:I

    aput-object v0, v1, v2

    .line 818805
    :cond_4
    iget-object v1, p0, LX/15u;->l:LX/15y;

    .line 818806
    iput-object v0, v1, LX/15y;->f:Ljava/lang/String;

    .line 818807
    return-void

    .line 818808
    :cond_5
    and-int/lit16 v0, v3, 0xff

    .line 818809
    iget v3, p0, LX/15u;->d:I

    iget v5, p0, LX/15u;->e:I

    if-lt v3, v5, :cond_6

    .line 818810
    invoke-virtual {p0}, LX/15u;->K()V

    .line 818811
    :cond_6
    iget v3, p0, LX/15u;->d:I

    add-int/lit8 v5, v3, 0x1

    iput v5, p0, LX/4sE;->d:I

    aget-byte v3, v4, v3

    .line 818812
    if-ne v6, v3, :cond_7

    .line 818813
    const/4 v1, 0x1

    move v3, v0

    move v4, v1

    .line 818814
    goto :goto_1

    .line 818815
    :cond_7
    shl-int/lit8 v0, v0, 0x8

    and-int/lit16 v3, v3, 0xff

    or-int/2addr v0, v3

    .line 818816
    iget v3, p0, LX/15u;->d:I

    iget v5, p0, LX/15u;->e:I

    if-lt v3, v5, :cond_8

    .line 818817
    invoke-virtual {p0}, LX/15u;->K()V

    .line 818818
    :cond_8
    iget v3, p0, LX/15u;->d:I

    add-int/lit8 v5, v3, 0x1

    iput v5, p0, LX/4sE;->d:I

    aget-byte v3, v4, v3

    .line 818819
    if-ne v6, v3, :cond_9

    .line 818820
    const/4 v1, 0x2

    move v3, v0

    move v4, v1

    .line 818821
    goto/16 :goto_1

    .line 818822
    :cond_9
    shl-int/lit8 v0, v0, 0x8

    and-int/lit16 v3, v3, 0xff

    or-int/2addr v0, v3

    .line 818823
    iget v3, p0, LX/15u;->d:I

    iget v5, p0, LX/15u;->e:I

    if-lt v3, v5, :cond_a

    .line 818824
    invoke-virtual {p0}, LX/15u;->K()V

    .line 818825
    :cond_a
    iget v3, p0, LX/15u;->d:I

    add-int/lit8 v5, v3, 0x1

    iput v5, p0, LX/4sE;->d:I

    aget-byte v3, v4, v3

    .line 818826
    if-ne v6, v3, :cond_b

    .line 818827
    const/4 v1, 0x3

    move v3, v0

    move v4, v1

    .line 818828
    goto/16 :goto_1

    .line 818829
    :cond_b
    shl-int/lit8 v0, v0, 0x8

    and-int/lit16 v3, v3, 0xff

    or-int/2addr v0, v3

    .line 818830
    iget-object v3, p0, LX/4sE;->W:[I

    array-length v3, v3

    if-lt v2, v3, :cond_c

    .line 818831
    iget-object v3, p0, LX/4sE;->W:[I

    iget-object v5, p0, LX/4sE;->W:[I

    array-length v5, v5

    add-int/lit16 v5, v5, 0x100

    invoke-static {v3, v5}, LX/4sE;->a([II)[I

    move-result-object v3

    iput-object v3, p0, LX/4sE;->W:[I

    .line 818832
    :cond_c
    iget-object v5, p0, LX/4sE;->W:[I

    add-int/lit8 v3, v2, 0x1

    aput v0, v5, v2

    move v2, v3

    .line 818833
    goto/16 :goto_0

    .line 818834
    :cond_d
    iget-object v1, p0, LX/4sE;->W:[I

    invoke-direct {p0, v1, v0, v2}, LX/4sE;->a([III)LX/0lx;

    move-result-object v0

    .line 818835
    iget-object v1, v0, LX/0lx;->a:Ljava/lang/String;

    move-object v0, v1

    .line 818836
    goto/16 :goto_2
.end method

.method private final a([III)LX/0lx;
    .locals 11

    .prologue
    .line 818612
    and-int/lit8 v6, p2, 0x3

    .line 818613
    const/4 v0, 0x4

    if-ge v6, v0, :cond_7

    .line 818614
    add-int/lit8 v0, p3, -0x1

    aget v0, p1, v0

    .line 818615
    add-int/lit8 v1, p3, -0x1

    rsub-int/lit8 v2, v6, 0x4

    shl-int/lit8 v2, v2, 0x3

    shl-int v2, v0, v2

    aput v2, p1, v1

    .line 818616
    :goto_0
    iget-object v1, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v1}, LX/15x;->l()[C

    move-result-object v1

    .line 818617
    const/4 v5, 0x0

    .line 818618
    const/4 v2, 0x0

    move v3, v2

    :goto_1
    if-ge v3, p2, :cond_b

    .line 818619
    shr-int/lit8 v2, v3, 0x2

    aget v2, p1, v2

    .line 818620
    and-int/lit8 v4, v3, 0x3

    .line 818621
    rsub-int/lit8 v4, v4, 0x3

    shl-int/lit8 v4, v4, 0x3

    shr-int/2addr v2, v4

    and-int/lit16 v2, v2, 0xff

    .line 818622
    add-int/lit8 v3, v3, 0x1

    .line 818623
    const/16 v4, 0x7f

    if-le v2, v4, :cond_d

    .line 818624
    and-int/lit16 v4, v2, 0xe0

    const/16 v7, 0xc0

    if-ne v4, v7, :cond_8

    .line 818625
    and-int/lit8 v4, v2, 0x1f

    .line 818626
    const/4 v2, 0x1

    move v10, v2

    move v2, v4

    move v4, v10

    .line 818627
    :goto_2
    add-int v7, v3, v4

    if-le v7, p2, :cond_0

    .line 818628
    const-string v7, " in long field name"

    invoke-virtual {p0, v7}, LX/15v;->d(Ljava/lang/String;)V

    .line 818629
    :cond_0
    shr-int/lit8 v7, v3, 0x2

    aget v7, p1, v7

    .line 818630
    and-int/lit8 v8, v3, 0x3

    .line 818631
    rsub-int/lit8 v8, v8, 0x3

    shl-int/lit8 v8, v8, 0x3

    shr-int/2addr v7, v8

    .line 818632
    add-int/lit8 v3, v3, 0x1

    .line 818633
    and-int/lit16 v8, v7, 0xc0

    const/16 v9, 0x80

    if-eq v8, v9, :cond_1

    .line 818634
    invoke-direct {p0, v7}, LX/4sE;->x(I)V

    .line 818635
    :cond_1
    shl-int/lit8 v2, v2, 0x6

    and-int/lit8 v7, v7, 0x3f

    or-int/2addr v2, v7

    .line 818636
    const/4 v7, 0x1

    if-le v4, v7, :cond_4

    .line 818637
    shr-int/lit8 v7, v3, 0x2

    aget v7, p1, v7

    .line 818638
    and-int/lit8 v8, v3, 0x3

    .line 818639
    rsub-int/lit8 v8, v8, 0x3

    shl-int/lit8 v8, v8, 0x3

    shr-int/2addr v7, v8

    .line 818640
    add-int/lit8 v3, v3, 0x1

    .line 818641
    and-int/lit16 v8, v7, 0xc0

    const/16 v9, 0x80

    if-eq v8, v9, :cond_2

    .line 818642
    invoke-direct {p0, v7}, LX/4sE;->x(I)V

    .line 818643
    :cond_2
    shl-int/lit8 v2, v2, 0x6

    and-int/lit8 v7, v7, 0x3f

    or-int/2addr v2, v7

    .line 818644
    const/4 v7, 0x2

    if-le v4, v7, :cond_4

    .line 818645
    shr-int/lit8 v7, v3, 0x2

    aget v7, p1, v7

    .line 818646
    and-int/lit8 v8, v3, 0x3

    .line 818647
    rsub-int/lit8 v8, v8, 0x3

    shl-int/lit8 v8, v8, 0x3

    shr-int/2addr v7, v8

    .line 818648
    add-int/lit8 v3, v3, 0x1

    .line 818649
    and-int/lit16 v8, v7, 0xc0

    const/16 v9, 0x80

    if-eq v8, v9, :cond_3

    .line 818650
    and-int/lit16 v8, v7, 0xff

    invoke-direct {p0, v8}, LX/4sE;->x(I)V

    .line 818651
    :cond_3
    shl-int/lit8 v2, v2, 0x6

    and-int/lit8 v7, v7, 0x3f

    or-int/2addr v2, v7

    .line 818652
    :cond_4
    const/4 v7, 0x2

    if-le v4, v7, :cond_d

    .line 818653
    const/high16 v4, 0x10000

    sub-int/2addr v2, v4

    .line 818654
    array-length v4, v1

    if-lt v5, v4, :cond_5

    .line 818655
    iget-object v1, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v1}, LX/15x;->o()[C

    move-result-object v1

    .line 818656
    :cond_5
    add-int/lit8 v4, v5, 0x1

    const v7, 0xd800

    shr-int/lit8 v8, v2, 0xa

    add-int/2addr v7, v8

    int-to-char v7, v7

    aput-char v7, v1, v5

    .line 818657
    const v5, 0xdc00

    and-int/lit16 v2, v2, 0x3ff

    or-int/2addr v2, v5

    move v10, v2

    move v2, v3

    move v3, v4

    move-object v4, v1

    move v1, v10

    .line 818658
    :goto_3
    array-length v5, v4

    if-lt v3, v5, :cond_6

    .line 818659
    iget-object v4, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v4}, LX/15x;->o()[C

    move-result-object v4

    .line 818660
    :cond_6
    add-int/lit8 v5, v3, 0x1

    int-to-char v1, v1

    aput-char v1, v4, v3

    move v3, v2

    move-object v1, v4

    .line 818661
    goto/16 :goto_1

    .line 818662
    :cond_7
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 818663
    :cond_8
    and-int/lit16 v4, v2, 0xf0

    const/16 v7, 0xe0

    if-ne v4, v7, :cond_9

    .line 818664
    and-int/lit8 v4, v2, 0xf

    .line 818665
    const/4 v2, 0x2

    move v10, v2

    move v2, v4

    move v4, v10

    goto/16 :goto_2

    .line 818666
    :cond_9
    and-int/lit16 v4, v2, 0xf8

    const/16 v7, 0xf0

    if-ne v4, v7, :cond_a

    .line 818667
    and-int/lit8 v4, v2, 0x7

    .line 818668
    const/4 v2, 0x3

    move v10, v2

    move v2, v4

    move v4, v10

    goto/16 :goto_2

    .line 818669
    :cond_a
    invoke-direct {p0, v2}, LX/4sE;->w(I)V

    .line 818670
    const/4 v2, 0x1

    move v4, v2

    goto/16 :goto_2

    .line 818671
    :cond_b
    new-instance v2, Ljava/lang/String;

    const/4 v3, 0x0

    invoke-direct {v2, v1, v3, v5}, Ljava/lang/String;-><init>([CII)V

    .line 818672
    const/4 v1, 0x4

    if-ge v6, v1, :cond_c

    .line 818673
    add-int/lit8 v1, p3, -0x1

    aput v0, p1, v1

    .line 818674
    :cond_c
    iget-object v0, p0, LX/4sE;->V:LX/0lv;

    invoke-virtual {v0, v2, p1, p3}, LX/0lv;->a(Ljava/lang/String;[II)LX/0lx;

    move-result-object v0

    return-object v0

    :cond_d
    move-object v4, v1

    move v1, v2

    move v2, v3

    move v3, v5

    goto :goto_3
.end method

.method private a(II)V
    .locals 0

    .prologue
    .line 818771
    iput p2, p0, LX/4sE;->d:I

    .line 818772
    invoke-direct {p0, p1}, LX/4sE;->x(I)V

    .line 818773
    return-void
.end method

.method private static a([II)[I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 818767
    add-int/lit8 v0, p1, 0x4

    new-array v0, v0, [I

    .line 818768
    if-eqz p0, :cond_0

    .line 818769
    array-length v1, p0

    invoke-static {p0, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 818770
    :cond_0
    return-object v0
.end method

.method private final a([Ljava/lang/String;)[Ljava/lang/String;
    .locals 4

    .prologue
    const/16 v0, 0x400

    const/16 v3, 0x40

    const/4 v2, 0x0

    .line 818756
    array-length v1, p1

    .line 818757
    if-nez v1, :cond_1

    .line 818758
    iget-object v0, p0, LX/4sE;->O:LX/4s6;

    invoke-virtual {v0}, LX/4s6;->a()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 818759
    if-nez v0, :cond_0

    .line 818760
    new-array v0, v3, [Ljava/lang/String;

    .line 818761
    :cond_0
    :goto_0
    return-object v0

    .line 818762
    :cond_1
    if-ne v1, v0, :cond_2

    .line 818763
    iput v2, p0, LX/4sE;->aa:I

    move-object v0, p1

    goto :goto_0

    .line 818764
    :cond_2
    if-ne v1, v3, :cond_3

    const/16 v0, 0x100

    .line 818765
    :cond_3
    new-array v0, v0, [Ljava/lang/String;

    .line 818766
    array-length v1, p1

    invoke-static {p1, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0
.end method

.method private aa()V
    .locals 3

    .prologue
    .line 818738
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/4sE;->S:Z

    .line 818739
    iget v0, p0, LX/4sE;->T:I

    .line 818740
    shr-int/lit8 v1, v0, 0x5

    and-int/lit8 v1, v1, 0x7

    .line 818741
    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 818742
    invoke-direct {p0, v0}, LX/4sE;->l(I)V

    .line 818743
    :goto_0
    return-void

    .line 818744
    :cond_0
    const/4 v2, 0x3

    if-gt v1, v2, :cond_1

    .line 818745
    and-int/lit8 v0, v0, 0x3f

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, LX/4sE;->m(I)V

    goto :goto_0

    .line 818746
    :cond_1
    const/4 v2, 0x5

    if-gt v1, v2, :cond_2

    .line 818747
    and-int/lit8 v0, v0, 0x3f

    add-int/lit8 v0, v0, 0x2

    invoke-direct {p0, v0}, LX/4sE;->n(I)V

    goto :goto_0

    .line 818748
    :cond_2
    const/4 v2, 0x7

    if-ne v1, v2, :cond_3

    .line 818749
    and-int/lit8 v0, v0, 0x1f

    .line 818750
    shr-int/lit8 v0, v0, 0x2

    packed-switch v0, :pswitch_data_0

    .line 818751
    :cond_3
    :pswitch_0
    invoke-static {}, LX/4pl;->b()V

    goto :goto_0

    .line 818752
    :pswitch_1
    invoke-direct {p0}, LX/4sE;->ak()V

    goto :goto_0

    .line 818753
    :pswitch_2
    invoke-direct {p0}, LX/4sE;->al()V

    goto :goto_0

    .line 818754
    :pswitch_3
    invoke-direct {p0}, LX/4sE;->aj()[B

    move-result-object v0

    iput-object v0, p0, LX/4sE;->r:[B

    goto :goto_0

    .line 818755
    :pswitch_4
    invoke-direct {p0}, LX/4sE;->am()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method private final ab()V
    .locals 4

    .prologue
    .line 818709
    iget v0, p0, LX/15u;->d:I

    iget v1, p0, LX/15u;->e:I

    if-lt v0, v1, :cond_0

    .line 818710
    invoke-virtual {p0}, LX/15u;->K()V

    .line 818711
    :cond_0
    iget-object v0, p0, LX/4sE;->Q:[B

    iget v1, p0, LX/15u;->d:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/4sE;->d:I

    aget-byte v1, v0, v1

    .line 818712
    if-gez v1, :cond_1

    .line 818713
    and-int/lit8 v0, v1, 0x3f

    .line 818714
    :goto_0
    invoke-static {v0}, LX/4sG;->b(I)I

    move-result v0

    iput v0, p0, LX/4sE;->B:I

    .line 818715
    const/4 v0, 0x1

    iput v0, p0, LX/4sE;->A:I

    .line 818716
    return-void

    .line 818717
    :cond_1
    iget v0, p0, LX/15u;->d:I

    iget v2, p0, LX/15u;->e:I

    if-lt v0, v2, :cond_2

    .line 818718
    invoke-virtual {p0}, LX/15u;->K()V

    .line 818719
    :cond_2
    iget-object v0, p0, LX/4sE;->Q:[B

    iget v2, p0, LX/15u;->d:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/4sE;->d:I

    aget-byte v0, v0, v2

    .line 818720
    if-ltz v0, :cond_6

    .line 818721
    shl-int/lit8 v1, v1, 0x7

    add-int/2addr v1, v0

    .line 818722
    iget v0, p0, LX/15u;->d:I

    iget v2, p0, LX/15u;->e:I

    if-lt v0, v2, :cond_3

    .line 818723
    invoke-virtual {p0}, LX/15u;->K()V

    .line 818724
    :cond_3
    iget-object v0, p0, LX/4sE;->Q:[B

    iget v2, p0, LX/15u;->d:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/4sE;->d:I

    aget-byte v0, v0, v2

    .line 818725
    if-ltz v0, :cond_6

    .line 818726
    shl-int/lit8 v1, v1, 0x7

    add-int/2addr v1, v0

    .line 818727
    iget v0, p0, LX/15u;->d:I

    iget v2, p0, LX/15u;->e:I

    if-lt v0, v2, :cond_4

    .line 818728
    invoke-virtual {p0}, LX/15u;->K()V

    .line 818729
    :cond_4
    iget-object v0, p0, LX/4sE;->Q:[B

    iget v2, p0, LX/15u;->d:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/4sE;->d:I

    aget-byte v0, v0, v2

    .line 818730
    if-ltz v0, :cond_6

    .line 818731
    shl-int/lit8 v1, v1, 0x7

    add-int/2addr v1, v0

    .line 818732
    iget v0, p0, LX/15u;->d:I

    iget v2, p0, LX/15u;->e:I

    if-lt v0, v2, :cond_5

    .line 818733
    invoke-virtual {p0}, LX/15u;->K()V

    .line 818734
    :cond_5
    iget-object v0, p0, LX/4sE;->Q:[B

    iget v2, p0, LX/15u;->d:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/4sE;->d:I

    aget-byte v0, v0, v2

    .line 818735
    if-ltz v0, :cond_6

    .line 818736
    const-string v2, "Corrupt input; 32-bit VInt extends beyond 5 data bytes"

    invoke-virtual {p0, v2}, LX/15v;->e(Ljava/lang/String;)V

    .line 818737
    :cond_6
    shl-int/lit8 v1, v1, 0x6

    and-int/lit8 v0, v0, 0x3f

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method private final ac()V
    .locals 5

    .prologue
    .line 818698
    invoke-direct {p0}, LX/4sE;->ag()I

    move-result v0

    int-to-long v0, v0

    .line 818699
    :goto_0
    iget v2, p0, LX/15u;->d:I

    iget v3, p0, LX/15u;->e:I

    if-lt v2, v3, :cond_0

    .line 818700
    invoke-virtual {p0}, LX/15u;->K()V

    .line 818701
    :cond_0
    iget-object v2, p0, LX/4sE;->Q:[B

    iget v3, p0, LX/15u;->d:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/4sE;->d:I

    aget-byte v2, v2, v3

    .line 818702
    if-gez v2, :cond_1

    .line 818703
    const/4 v3, 0x6

    shl-long/2addr v0, v3

    and-int/lit8 v2, v2, 0x3f

    int-to-long v2, v2

    add-long/2addr v0, v2

    .line 818704
    invoke-static {v0, v1}, LX/4sG;->b(J)J

    move-result-wide v0

    iput-wide v0, p0, LX/4sE;->C:J

    .line 818705
    const/4 v0, 0x2

    iput v0, p0, LX/4sE;->A:I

    .line 818706
    return-void

    .line 818707
    :cond_1
    const/4 v3, 0x7

    shl-long/2addr v0, v3

    int-to-long v2, v2

    add-long/2addr v0, v2

    .line 818708
    goto :goto_0
.end method

.method private final ad()V
    .locals 2

    .prologue
    .line 818694
    invoke-direct {p0}, LX/4sE;->aj()[B

    move-result-object v0

    .line 818695
    new-instance v1, Ljava/math/BigInteger;

    invoke-direct {v1, v0}, Ljava/math/BigInteger;-><init>([B)V

    iput-object v1, p0, LX/4sE;->E:Ljava/math/BigInteger;

    .line 818696
    const/4 v0, 0x4

    iput v0, p0, LX/4sE;->A:I

    .line 818697
    return-void
.end method

.method private final ae()V
    .locals 4

    .prologue
    .line 818686
    invoke-direct {p0}, LX/4sE;->ag()I

    move-result v0

    .line 818687
    iget v1, p0, LX/15u;->d:I

    iget v2, p0, LX/15u;->e:I

    if-lt v1, v2, :cond_0

    .line 818688
    invoke-virtual {p0}, LX/15u;->K()V

    .line 818689
    :cond_0
    shl-int/lit8 v0, v0, 0x7

    iget-object v1, p0, LX/4sE;->Q:[B

    iget v2, p0, LX/15u;->d:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/4sE;->d:I

    aget-byte v1, v1, v2

    add-int/2addr v0, v1

    .line 818690
    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    .line 818691
    float-to-double v0, v0

    iput-wide v0, p0, LX/4sE;->D:D

    .line 818692
    const/16 v0, 0x8

    iput v0, p0, LX/4sE;->A:I

    .line 818693
    return-void
.end method

.method private final af()V
    .locals 6

    .prologue
    const/4 v5, 0x7

    .line 818675
    invoke-direct {p0}, LX/4sE;->ag()I

    move-result v0

    int-to-long v0, v0

    .line 818676
    const/16 v2, 0x1c

    shl-long/2addr v0, v2

    invoke-direct {p0}, LX/4sE;->ag()I

    move-result v2

    int-to-long v2, v2

    add-long/2addr v0, v2

    .line 818677
    iget v2, p0, LX/15u;->d:I

    iget v3, p0, LX/15u;->e:I

    if-lt v2, v3, :cond_0

    .line 818678
    invoke-virtual {p0}, LX/15u;->K()V

    .line 818679
    :cond_0
    shl-long/2addr v0, v5

    iget-object v2, p0, LX/4sE;->Q:[B

    iget v3, p0, LX/15u;->d:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/4sE;->d:I

    aget-byte v2, v2, v3

    int-to-long v2, v2

    add-long/2addr v0, v2

    .line 818680
    iget v2, p0, LX/15u;->d:I

    iget v3, p0, LX/15u;->e:I

    if-lt v2, v3, :cond_1

    .line 818681
    invoke-virtual {p0}, LX/15u;->K()V

    .line 818682
    :cond_1
    shl-long/2addr v0, v5

    iget-object v2, p0, LX/4sE;->Q:[B

    iget v3, p0, LX/15u;->d:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/4sE;->d:I

    aget-byte v2, v2, v3

    int-to-long v2, v2

    add-long/2addr v0, v2

    .line 818683
    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    iput-wide v0, p0, LX/4sE;->D:D

    .line 818684
    const/16 v0, 0x8

    iput v0, p0, LX/4sE;->A:I

    .line 818685
    return-void
.end method

.method private final ag()I
    .locals 4

    .prologue
    .line 819271
    iget v0, p0, LX/15u;->d:I

    iget v1, p0, LX/15u;->e:I

    if-lt v0, v1, :cond_0

    .line 819272
    invoke-virtual {p0}, LX/15u;->K()V

    .line 819273
    :cond_0
    iget-object v0, p0, LX/4sE;->Q:[B

    iget v1, p0, LX/15u;->d:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/4sE;->d:I

    aget-byte v0, v0, v1

    .line 819274
    iget v1, p0, LX/15u;->d:I

    iget v2, p0, LX/15u;->e:I

    if-lt v1, v2, :cond_1

    .line 819275
    invoke-virtual {p0}, LX/15u;->K()V

    .line 819276
    :cond_1
    shl-int/lit8 v0, v0, 0x7

    iget-object v1, p0, LX/4sE;->Q:[B

    iget v2, p0, LX/15u;->d:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/4sE;->d:I

    aget-byte v1, v1, v2

    add-int/2addr v0, v1

    .line 819277
    iget v1, p0, LX/15u;->d:I

    iget v2, p0, LX/15u;->e:I

    if-lt v1, v2, :cond_2

    .line 819278
    invoke-virtual {p0}, LX/15u;->K()V

    .line 819279
    :cond_2
    shl-int/lit8 v0, v0, 0x7

    iget-object v1, p0, LX/4sE;->Q:[B

    iget v2, p0, LX/15u;->d:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/4sE;->d:I

    aget-byte v1, v1, v2

    add-int/2addr v0, v1

    .line 819280
    iget v1, p0, LX/15u;->d:I

    iget v2, p0, LX/15u;->e:I

    if-lt v1, v2, :cond_3

    .line 819281
    invoke-virtual {p0}, LX/15u;->K()V

    .line 819282
    :cond_3
    shl-int/lit8 v0, v0, 0x7

    iget-object v1, p0, LX/4sE;->Q:[B

    iget v2, p0, LX/15u;->d:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/4sE;->d:I

    aget-byte v1, v1, v2

    add-int/2addr v0, v1

    return v0
.end method

.method private final ah()V
    .locals 4

    .prologue
    .line 818975
    invoke-direct {p0}, LX/4sE;->ai()I

    move-result v0

    invoke-static {v0}, LX/4sG;->b(I)I

    move-result v0

    .line 818976
    invoke-direct {p0}, LX/4sE;->aj()[B

    move-result-object v1

    .line 818977
    new-instance v2, Ljava/math/BigDecimal;

    new-instance v3, Ljava/math/BigInteger;

    invoke-direct {v3, v1}, Ljava/math/BigInteger;-><init>([B)V

    invoke-direct {v2, v3, v0}, Ljava/math/BigDecimal;-><init>(Ljava/math/BigInteger;I)V

    iput-object v2, p0, LX/4sE;->F:Ljava/math/BigDecimal;

    .line 818978
    const/16 v0, 0x10

    iput v0, p0, LX/4sE;->A:I

    .line 818979
    return-void
.end method

.method private final ai()I
    .locals 4

    .prologue
    .line 819262
    const/4 v0, 0x0

    .line 819263
    :goto_0
    iget v1, p0, LX/15u;->d:I

    iget v2, p0, LX/15u;->e:I

    if-lt v1, v2, :cond_0

    .line 819264
    invoke-virtual {p0}, LX/15u;->K()V

    .line 819265
    :cond_0
    iget-object v1, p0, LX/4sE;->Q:[B

    iget v2, p0, LX/15u;->d:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/4sE;->d:I

    aget-byte v1, v1, v2

    .line 819266
    if-gez v1, :cond_1

    .line 819267
    shl-int/lit8 v0, v0, 0x6

    and-int/lit8 v1, v1, 0x3f

    add-int/2addr v0, v1

    .line 819268
    return v0

    .line 819269
    :cond_1
    shl-int/lit8 v0, v0, 0x7

    add-int/2addr v0, v1

    .line 819270
    goto :goto_0
.end method

.method private final aj()[B
    .locals 10

    .prologue
    const/16 v9, 0x8

    .line 819232
    invoke-direct {p0}, LX/4sE;->ai()I

    move-result v0

    .line 819233
    new-array v4, v0, [B

    .line 819234
    const/4 v2, 0x0

    .line 819235
    add-int/lit8 v1, v0, -0x7

    .line 819236
    :goto_0
    if-gt v2, v1, :cond_1

    .line 819237
    iget v3, p0, LX/15u;->e:I

    iget v5, p0, LX/15u;->d:I

    sub-int/2addr v3, v5

    if-ge v3, v9, :cond_0

    .line 819238
    invoke-direct {p0, v9}, LX/4sE;->f(I)Z

    .line 819239
    :cond_0
    iget-object v3, p0, LX/4sE;->Q:[B

    iget v5, p0, LX/15u;->d:I

    add-int/lit8 v6, v5, 0x1

    iput v6, p0, LX/4sE;->d:I

    aget-byte v3, v3, v5

    shl-int/lit8 v3, v3, 0x19

    iget-object v5, p0, LX/4sE;->Q:[B

    iget v6, p0, LX/15u;->d:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, LX/4sE;->d:I

    aget-byte v5, v5, v6

    shl-int/lit8 v5, v5, 0x12

    add-int/2addr v3, v5

    iget-object v5, p0, LX/4sE;->Q:[B

    iget v6, p0, LX/15u;->d:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, LX/4sE;->d:I

    aget-byte v5, v5, v6

    shl-int/lit8 v5, v5, 0xb

    add-int/2addr v3, v5

    iget-object v5, p0, LX/4sE;->Q:[B

    iget v6, p0, LX/15u;->d:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, LX/4sE;->d:I

    aget-byte v5, v5, v6

    shl-int/lit8 v5, v5, 0x4

    add-int/2addr v3, v5

    .line 819240
    iget-object v5, p0, LX/4sE;->Q:[B

    iget v6, p0, LX/15u;->d:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, LX/4sE;->d:I

    aget-byte v5, v5, v6

    .line 819241
    shr-int/lit8 v6, v5, 0x3

    add-int/2addr v3, v6

    .line 819242
    and-int/lit8 v5, v5, 0x7

    shl-int/lit8 v5, v5, 0x15

    iget-object v6, p0, LX/4sE;->Q:[B

    iget v7, p0, LX/15u;->d:I

    add-int/lit8 v8, v7, 0x1

    iput v8, p0, LX/4sE;->d:I

    aget-byte v6, v6, v7

    shl-int/lit8 v6, v6, 0xe

    add-int/2addr v5, v6

    iget-object v6, p0, LX/4sE;->Q:[B

    iget v7, p0, LX/15u;->d:I

    add-int/lit8 v8, v7, 0x1

    iput v8, p0, LX/4sE;->d:I

    aget-byte v6, v6, v7

    shl-int/lit8 v6, v6, 0x7

    add-int/2addr v5, v6

    iget-object v6, p0, LX/4sE;->Q:[B

    iget v7, p0, LX/15u;->d:I

    add-int/lit8 v8, v7, 0x1

    iput v8, p0, LX/4sE;->d:I

    aget-byte v6, v6, v7

    add-int/2addr v5, v6

    .line 819243
    add-int/lit8 v6, v2, 0x1

    shr-int/lit8 v7, v3, 0x18

    int-to-byte v7, v7

    aput-byte v7, v4, v2

    .line 819244
    add-int/lit8 v2, v6, 0x1

    shr-int/lit8 v7, v3, 0x10

    int-to-byte v7, v7

    aput-byte v7, v4, v6

    .line 819245
    add-int/lit8 v6, v2, 0x1

    shr-int/lit8 v7, v3, 0x8

    int-to-byte v7, v7

    aput-byte v7, v4, v2

    .line 819246
    add-int/lit8 v2, v6, 0x1

    int-to-byte v3, v3

    aput-byte v3, v4, v6

    .line 819247
    add-int/lit8 v3, v2, 0x1

    shr-int/lit8 v6, v5, 0x10

    int-to-byte v6, v6

    aput-byte v6, v4, v2

    .line 819248
    add-int/lit8 v6, v3, 0x1

    shr-int/lit8 v2, v5, 0x8

    int-to-byte v2, v2

    aput-byte v2, v4, v3

    .line 819249
    add-int/lit8 v2, v6, 0x1

    int-to-byte v3, v5

    aput-byte v3, v4, v6

    goto/16 :goto_0

    .line 819250
    :cond_1
    sub-int v5, v0, v2

    .line 819251
    if-lez v5, :cond_4

    .line 819252
    iget v0, p0, LX/15u;->e:I

    iget v1, p0, LX/15u;->d:I

    sub-int/2addr v0, v1

    add-int/lit8 v1, v5, 0x1

    if-ge v0, v1, :cond_2

    .line 819253
    add-int/lit8 v0, v5, 0x1

    invoke-direct {p0, v0}, LX/4sE;->f(I)Z

    .line 819254
    :cond_2
    iget-object v0, p0, LX/4sE;->Q:[B

    iget v1, p0, LX/15u;->d:I

    add-int/lit8 v3, v1, 0x1

    iput v3, p0, LX/4sE;->d:I

    aget-byte v1, v0, v1

    .line 819255
    const/4 v0, 0x1

    :goto_1
    if-ge v0, v5, :cond_3

    .line 819256
    shl-int/lit8 v1, v1, 0x7

    iget-object v3, p0, LX/4sE;->Q:[B

    iget v6, p0, LX/15u;->d:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, LX/4sE;->d:I

    aget-byte v3, v3, v6

    add-int/2addr v1, v3

    .line 819257
    add-int/lit8 v3, v2, 0x1

    rsub-int/lit8 v6, v0, 0x7

    shr-int v6, v1, v6

    int-to-byte v6, v6

    aput-byte v6, v4, v2

    .line 819258
    add-int/lit8 v0, v0, 0x1

    move v2, v3

    goto :goto_1

    .line 819259
    :cond_3
    shl-int v0, v1, v5

    .line 819260
    iget-object v1, p0, LX/4sE;->Q:[B

    iget v3, p0, LX/15u;->d:I

    add-int/lit8 v5, v3, 0x1

    iput v5, p0, LX/4sE;->d:I

    aget-byte v1, v1, v3

    add-int/2addr v0, v1

    int-to-byte v0, v0

    aput-byte v0, v4, v2

    .line 819261
    :cond_4
    return-object v4
.end method

.method private final ak()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 819215
    iget-object v0, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v0}, LX/15x;->l()[C

    move-result-object v0

    move v1, v2

    .line 819216
    :goto_0
    iget v3, p0, LX/15u;->d:I

    iget v4, p0, LX/15u;->e:I

    if-lt v3, v4, :cond_0

    .line 819217
    invoke-virtual {p0}, LX/15u;->K()V

    .line 819218
    :cond_0
    iget v4, p0, LX/15u;->d:I

    .line 819219
    iget v3, p0, LX/15u;->e:I

    sub-int/2addr v3, v4

    .line 819220
    array-length v5, v0

    if-lt v1, v5, :cond_1

    .line 819221
    iget-object v0, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v0}, LX/15x;->n()[C

    move-result-object v0

    move v1, v2

    .line 819222
    :cond_1
    array-length v5, v0

    sub-int/2addr v5, v1

    invoke-static {v3, v5}, Ljava/lang/Math;->min(II)I

    move-result v3

    move v5, v1

    move v1, v4

    .line 819223
    :goto_1
    iget-object v6, p0, LX/4sE;->Q:[B

    add-int/lit8 v4, v1, 0x1

    aget-byte v6, v6, v1

    .line 819224
    const/4 v1, -0x4

    if-ne v6, v1, :cond_2

    .line 819225
    iput v4, p0, LX/4sE;->d:I

    .line 819226
    iget-object v0, p0, LX/15u;->n:LX/15x;

    .line 819227
    iput v5, v0, LX/15x;->j:I

    .line 819228
    return-void

    .line 819229
    :cond_2
    add-int/lit8 v1, v5, 0x1

    int-to-char v6, v6

    aput-char v6, v0, v5

    .line 819230
    add-int/lit8 v3, v3, -0x1

    if-gtz v3, :cond_3

    .line 819231
    iput v4, p0, LX/4sE;->d:I

    goto :goto_0

    :cond_3
    move v5, v1

    move v1, v4

    goto :goto_1
.end method

.method private final al()V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 819178
    iget-object v0, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v0}, LX/15x;->l()[C

    move-result-object v0

    .line 819179
    sget-object v7, LX/4s7;->a:[I

    .line 819180
    iget-object v8, p0, LX/4sE;->Q:[B

    move v1, v2

    .line 819181
    :goto_0
    iget v3, p0, LX/15u;->d:I

    .line 819182
    iget v4, p0, LX/15u;->e:I

    if-lt v3, v4, :cond_0

    .line 819183
    invoke-virtual {p0}, LX/15u;->K()V

    .line 819184
    iget v3, p0, LX/15u;->d:I

    .line 819185
    :cond_0
    array-length v4, v0

    if-lt v1, v4, :cond_1

    .line 819186
    iget-object v0, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v0}, LX/15x;->n()[C

    move-result-object v0

    move v1, v2

    .line 819187
    :cond_1
    iget v5, p0, LX/15u;->e:I

    .line 819188
    array-length v4, v0

    sub-int/2addr v4, v1

    add-int/2addr v4, v3

    .line 819189
    if-ge v4, v5, :cond_8

    .line 819190
    :goto_1
    if-ge v3, v4, :cond_3

    .line 819191
    add-int/lit8 v5, v3, 0x1

    aget-byte v3, v8, v3

    and-int/lit16 v3, v3, 0xff

    .line 819192
    aget v6, v7, v3

    if-eqz v6, :cond_2

    .line 819193
    iput v5, p0, LX/4sE;->d:I

    .line 819194
    const/16 v4, 0xfc

    if-eq v3, v4, :cond_5

    .line 819195
    aget v4, v7, v3

    packed-switch v4, :pswitch_data_0

    .line 819196
    invoke-direct {p0, v3}, LX/4sE;->v(I)V

    .line 819197
    :goto_2
    array-length v4, v0

    if-lt v1, v4, :cond_6

    .line 819198
    iget-object v0, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v0}, LX/15x;->n()[C

    move-result-object v0

    move v4, v2

    .line 819199
    :goto_3
    add-int/lit8 v1, v4, 0x1

    int-to-char v3, v3

    aput-char v3, v0, v4

    goto :goto_0

    .line 819200
    :cond_2
    add-int/lit8 v6, v1, 0x1

    int-to-char v3, v3

    aput-char v3, v0, v1

    move v3, v5

    move v1, v6

    goto :goto_1

    .line 819201
    :cond_3
    iput v3, p0, LX/4sE;->d:I

    goto :goto_0

    .line 819202
    :pswitch_0
    invoke-direct {p0, v3}, LX/4sE;->p(I)I

    move-result v3

    goto :goto_2

    .line 819203
    :pswitch_1
    iget v4, p0, LX/15u;->e:I

    iget v5, p0, LX/15u;->d:I

    sub-int/2addr v4, v5

    const/4 v5, 0x2

    if-lt v4, v5, :cond_4

    .line 819204
    invoke-direct {p0, v3}, LX/4sE;->r(I)I

    move-result v3

    goto :goto_2

    .line 819205
    :cond_4
    invoke-direct {p0, v3}, LX/4sE;->q(I)I

    move-result v3

    goto :goto_2

    .line 819206
    :pswitch_2
    invoke-direct {p0, v3}, LX/4sE;->s(I)I

    move-result v4

    .line 819207
    add-int/lit8 v3, v1, 0x1

    const v5, 0xd800

    shr-int/lit8 v6, v4, 0xa

    or-int/2addr v5, v6

    int-to-char v5, v5

    aput-char v5, v0, v1

    .line 819208
    array-length v1, v0

    if-lt v3, v1, :cond_7

    .line 819209
    iget-object v0, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v0}, LX/15x;->n()[C

    move-result-object v0

    move v1, v2

    .line 819210
    :goto_4
    const v3, 0xdc00

    and-int/lit16 v4, v4, 0x3ff

    or-int/2addr v3, v4

    .line 819211
    goto :goto_2

    .line 819212
    :cond_5
    iget-object v0, p0, LX/15u;->n:LX/15x;

    .line 819213
    iput v1, v0, LX/15x;->j:I

    .line 819214
    return-void

    :cond_6
    move v4, v1

    goto :goto_3

    :cond_7
    move v1, v3

    goto :goto_4

    :cond_8
    move v4, v5

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private final am()V
    .locals 6

    .prologue
    .line 819165
    invoke-direct {p0}, LX/4sE;->ai()I

    move-result v1

    .line 819166
    new-array v0, v1, [B

    iput-object v0, p0, LX/4sE;->r:[B

    .line 819167
    iget v0, p0, LX/15u;->d:I

    iget v2, p0, LX/15u;->e:I

    if-lt v0, v2, :cond_0

    .line 819168
    invoke-virtual {p0}, LX/15u;->K()V

    .line 819169
    :cond_0
    const/4 v0, 0x0

    .line 819170
    :goto_0
    iget v2, p0, LX/15u;->e:I

    iget v3, p0, LX/15u;->d:I

    sub-int/2addr v2, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 819171
    iget-object v3, p0, LX/4sE;->Q:[B

    iget v4, p0, LX/15u;->d:I

    iget-object v5, p0, LX/15u;->r:[B

    invoke-static {v3, v4, v5, v0, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 819172
    iget v3, p0, LX/15u;->d:I

    add-int/2addr v3, v2

    iput v3, p0, LX/4sE;->d:I

    .line 819173
    add-int/2addr v0, v2

    .line 819174
    sub-int/2addr v1, v2

    .line 819175
    if-gtz v1, :cond_1

    .line 819176
    return-void

    .line 819177
    :cond_1
    invoke-virtual {p0}, LX/15u;->K()V

    goto :goto_0
.end method

.method private an()V
    .locals 4

    .prologue
    .line 819134
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/4sE;->S:Z

    .line 819135
    iget v0, p0, LX/4sE;->T:I

    .line 819136
    shr-int/lit8 v1, v0, 0x5

    and-int/lit8 v1, v1, 0x7

    packed-switch v1, :pswitch_data_0

    .line 819137
    :goto_0
    :pswitch_0
    invoke-static {}, LX/4pl;->b()V

    .line 819138
    :goto_1
    return-void

    .line 819139
    :pswitch_1
    and-int/lit8 v0, v0, 0x1f

    .line 819140
    shr-int/lit8 v1, v0, 0x2

    packed-switch v1, :pswitch_data_1

    goto :goto_0

    .line 819141
    :pswitch_2
    and-int/lit8 v0, v0, 0x3

    packed-switch v0, :pswitch_data_2

    goto :goto_0

    .line 819142
    :goto_2
    :pswitch_3
    iget v0, p0, LX/15u;->e:I

    .line 819143
    iget-object v1, p0, LX/4sE;->Q:[B

    .line 819144
    :cond_0
    iget v2, p0, LX/15u;->d:I

    if-ge v2, v0, :cond_1

    .line 819145
    iget v2, p0, LX/15u;->d:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/4sE;->d:I

    aget-byte v2, v1, v2

    if-gez v2, :cond_0

    goto :goto_1

    .line 819146
    :pswitch_4
    const/4 v0, 0x4

    invoke-direct {p0, v0}, LX/4sE;->o(I)V

    goto :goto_2

    .line 819147
    :cond_1
    invoke-virtual {p0}, LX/15u;->K()V

    goto :goto_2

    .line 819148
    :pswitch_5
    invoke-direct {p0}, LX/4sE;->ao()V

    goto :goto_1

    .line 819149
    :pswitch_6
    and-int/lit8 v0, v0, 0x3

    packed-switch v0, :pswitch_data_3

    goto :goto_0

    .line 819150
    :pswitch_7
    const/4 v0, 0x5

    invoke-direct {p0, v0}, LX/4sE;->o(I)V

    goto :goto_1

    .line 819151
    :pswitch_8
    const/16 v0, 0xa

    invoke-direct {p0, v0}, LX/4sE;->o(I)V

    goto :goto_1

    .line 819152
    :pswitch_9
    invoke-direct {p0}, LX/4sE;->ai()I

    .line 819153
    invoke-direct {p0}, LX/4sE;->ao()V

    goto :goto_1

    .line 819154
    :pswitch_a
    and-int/lit8 v0, v0, 0x3f

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, LX/4sE;->o(I)V

    goto :goto_1

    .line 819155
    :pswitch_b
    and-int/lit8 v0, v0, 0x3f

    add-int/lit8 v0, v0, 0x2

    invoke-direct {p0, v0}, LX/4sE;->o(I)V

    goto :goto_1

    .line 819156
    :pswitch_c
    and-int/lit8 v0, v0, 0x1f

    .line 819157
    shr-int/lit8 v0, v0, 0x2

    packed-switch v0, :pswitch_data_4

    :pswitch_d
    goto :goto_0

    .line 819158
    :goto_3
    :pswitch_e
    iget v0, p0, LX/15u;->e:I

    .line 819159
    iget-object v1, p0, LX/4sE;->Q:[B

    .line 819160
    :cond_2
    iget v2, p0, LX/15u;->d:I

    if-ge v2, v0, :cond_3

    .line 819161
    iget v2, p0, LX/15u;->d:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/4sE;->d:I

    aget-byte v2, v1, v2

    const/4 v3, -0x4

    if-ne v2, v3, :cond_2

    goto :goto_1

    .line 819162
    :cond_3
    invoke-virtual {p0}, LX/15u;->K()V

    goto :goto_3

    .line 819163
    :pswitch_f
    invoke-direct {p0}, LX/4sE;->ao()V

    goto :goto_1

    .line 819164
    :pswitch_10
    invoke-direct {p0}, LX/4sE;->ai()I

    move-result v0

    invoke-direct {p0, v0}, LX/4sE;->o(I)V

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_a
        :pswitch_a
        :pswitch_b
        :pswitch_b
        :pswitch_0
        :pswitch_c
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_6
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_e
        :pswitch_e
        :pswitch_f
        :pswitch_d
        :pswitch_d
        :pswitch_d
        :pswitch_d
        :pswitch_10
    .end packed-switch
.end method

.method private ao()V
    .locals 3

    .prologue
    .line 819283
    invoke-direct {p0}, LX/4sE;->ai()I

    move-result v1

    .line 819284
    div-int/lit8 v2, v1, 0x7

    .line 819285
    mul-int/lit8 v0, v2, 0x8

    .line 819286
    mul-int/lit8 v2, v2, 0x7

    sub-int/2addr v1, v2

    .line 819287
    if-lez v1, :cond_0

    .line 819288
    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 819289
    :cond_0
    invoke-direct {p0, v0}, LX/4sE;->o(I)V

    .line 819290
    return-void
.end method

.method private final d(ILjava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 819122
    const/4 v0, 0x5

    if-ge p1, v0, :cond_0

    .line 819123
    iget-object v0, p0, LX/4sE;->V:LX/0lv;

    iget v1, p0, LX/4sE;->X:I

    const/4 v2, 0x0

    invoke-virtual {v0, p2, v1, v2}, LX/0lv;->a(Ljava/lang/String;II)LX/0lx;

    move-result-object v0

    .line 819124
    iget-object v1, v0, LX/0lx;->a:Ljava/lang/String;

    move-object v0, v1

    .line 819125
    :goto_0
    return-object v0

    .line 819126
    :cond_0
    const/16 v0, 0x9

    if-ge p1, v0, :cond_1

    .line 819127
    iget-object v0, p0, LX/4sE;->V:LX/0lv;

    iget v1, p0, LX/4sE;->X:I

    iget v2, p0, LX/4sE;->Y:I

    invoke-virtual {v0, p2, v1, v2}, LX/0lv;->a(Ljava/lang/String;II)LX/0lx;

    move-result-object v0

    .line 819128
    iget-object v1, v0, LX/0lx;->a:Ljava/lang/String;

    move-object v0, v1

    .line 819129
    goto :goto_0

    .line 819130
    :cond_1
    add-int/lit8 v0, p1, 0x3

    shr-int/lit8 v0, v0, 0x2

    .line 819131
    iget-object v1, p0, LX/4sE;->V:LX/0lv;

    iget-object v2, p0, LX/4sE;->W:[I

    invoke-virtual {v1, p2, v2, v0}, LX/0lv;->a(Ljava/lang/String;[II)LX/0lx;

    move-result-object v0

    .line 819132
    iget-object v1, v0, LX/0lx;->a:Ljava/lang/String;

    move-object v0, v1

    .line 819133
    goto :goto_0
.end method

.method private f(I)Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 819105
    iget-object v1, p0, LX/4sE;->P:Ljava/io/InputStream;

    if-nez v1, :cond_1

    .line 819106
    :cond_0
    :goto_0
    return v0

    .line 819107
    :cond_1
    iget v1, p0, LX/15u;->e:I

    iget v2, p0, LX/15u;->d:I

    sub-int/2addr v1, v2

    .line 819108
    if-lez v1, :cond_2

    iget v2, p0, LX/15u;->d:I

    if-lez v2, :cond_2

    .line 819109
    iget-wide v2, p0, LX/15u;->f:J

    iget v4, p0, LX/15u;->d:I

    int-to-long v4, v4

    add-long/2addr v2, v4

    iput-wide v2, p0, LX/4sE;->f:J

    .line 819110
    iget-object v2, p0, LX/4sE;->Q:[B

    iget v3, p0, LX/15u;->d:I

    iget-object v4, p0, LX/4sE;->Q:[B

    invoke-static {v2, v3, v4, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 819111
    iput v1, p0, LX/4sE;->e:I

    .line 819112
    :goto_1
    iput v0, p0, LX/4sE;->d:I

    .line 819113
    :goto_2
    iget v2, p0, LX/15u;->e:I

    if-ge v2, p1, :cond_4

    .line 819114
    iget-object v2, p0, LX/4sE;->P:Ljava/io/InputStream;

    iget-object v3, p0, LX/4sE;->Q:[B

    iget v4, p0, LX/15u;->e:I

    iget-object v5, p0, LX/4sE;->Q:[B

    array-length v5, v5

    iget v6, p0, LX/15u;->e:I

    sub-int/2addr v5, v6

    invoke-virtual {v2, v3, v4, v5}, Ljava/io/InputStream;->read([BII)I

    move-result v2

    .line 819115
    if-gtz v2, :cond_3

    .line 819116
    invoke-virtual {p0}, LX/4sE;->N()V

    .line 819117
    if-nez v2, :cond_0

    .line 819118
    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "InputStream.read() returned 0 characters when trying to read "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " bytes"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 819119
    :cond_2
    iput v0, p0, LX/4sE;->e:I

    goto :goto_1

    .line 819120
    :cond_3
    iget v3, p0, LX/15u;->e:I

    add-int/2addr v2, v3

    iput v2, p0, LX/4sE;->e:I

    goto :goto_2

    .line 819121
    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private final g(I)LX/15z;
    .locals 2

    .prologue
    .line 819101
    iget v0, p0, LX/4sE;->ac:I

    if-lt p1, v0, :cond_0

    .line 819102
    invoke-direct {p0, p1}, LX/4sE;->u(I)V

    .line 819103
    :cond_0
    iget-object v0, p0, LX/15u;->n:LX/15x;

    iget-object v1, p0, LX/4sE;->ab:[Ljava/lang/String;

    aget-object v1, v1, p1

    invoke-virtual {v0, v1}, LX/15x;->a(Ljava/lang/String;)V

    .line 819104
    sget-object v0, LX/15z;->VALUE_STRING:LX/15z;

    iput-object v0, p0, LX/4sE;->K:LX/15z;

    return-object v0
.end method

.method private final h(I)Ljava/lang/String;
    .locals 8

    .prologue
    .line 819081
    iget-object v0, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v0}, LX/15x;->l()[C

    move-result-object v3

    .line 819082
    const/4 v0, 0x0

    .line 819083
    iget-object v4, p0, LX/4sE;->Q:[B

    .line 819084
    iget v1, p0, LX/15u;->d:I

    .line 819085
    add-int v2, v1, p1

    add-int/lit8 v5, v2, -0x3

    move v2, v0

    :goto_0
    if-ge v1, v5, :cond_0

    .line 819086
    add-int/lit8 v0, v2, 0x1

    add-int/lit8 v6, v1, 0x1

    aget-byte v1, v4, v1

    int-to-char v1, v1

    aput-char v1, v3, v2

    .line 819087
    add-int/lit8 v1, v0, 0x1

    add-int/lit8 v2, v6, 0x1

    aget-byte v6, v4, v6

    int-to-char v6, v6

    aput-char v6, v3, v0

    .line 819088
    add-int/lit8 v6, v1, 0x1

    add-int/lit8 v7, v2, 0x1

    aget-byte v0, v4, v2

    int-to-char v0, v0

    aput-char v0, v3, v1

    .line 819089
    add-int/lit8 v0, v6, 0x1

    add-int/lit8 v1, v7, 0x1

    aget-byte v2, v4, v7

    int-to-char v2, v2

    aput-char v2, v3, v6

    move v2, v0

    goto :goto_0

    .line 819090
    :cond_0
    and-int/lit8 v5, p1, 0x3

    .line 819091
    if-lez v5, :cond_2

    .line 819092
    add-int/lit8 v6, v2, 0x1

    add-int/lit8 v0, v1, 0x1

    aget-byte v1, v4, v1

    int-to-char v1, v1

    aput-char v1, v3, v2

    .line 819093
    const/4 v1, 0x1

    if-le v5, v1, :cond_1

    .line 819094
    add-int/lit8 v2, v6, 0x1

    add-int/lit8 v1, v0, 0x1

    aget-byte v0, v4, v0

    int-to-char v0, v0

    aput-char v0, v3, v6

    .line 819095
    const/4 v0, 0x2

    if-le v5, v0, :cond_2

    .line 819096
    add-int/lit8 v0, v1, 0x1

    aget-byte v1, v4, v1

    int-to-char v1, v1

    aput-char v1, v3, v2

    .line 819097
    :cond_1
    :goto_1
    iput v0, p0, LX/4sE;->d:I

    .line 819098
    iget-object v0, p0, LX/15u;->n:LX/15x;

    .line 819099
    iput p1, v0, LX/15x;->j:I

    .line 819100
    iget-object v0, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v0}, LX/15x;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method private final i(I)Ljava/lang/String;
    .locals 10

    .prologue
    .line 819056
    const/4 v3, 0x0

    .line 819057
    iget-object v0, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v0}, LX/15x;->l()[C

    move-result-object v4

    .line 819058
    iget v1, p0, LX/15u;->d:I

    .line 819059
    iget v0, p0, LX/15u;->d:I

    add-int/2addr v0, p1

    iput v0, p0, LX/4sE;->d:I

    .line 819060
    sget-object v5, LX/4s7;->a:[I

    .line 819061
    iget-object v6, p0, LX/4sE;->Q:[B

    .line 819062
    add-int v7, v1, p1

    :goto_0
    if-ge v1, v7, :cond_1

    .line 819063
    add-int/lit8 v2, v1, 0x1

    aget-byte v0, v6, v1

    and-int/lit16 v0, v0, 0xff

    .line 819064
    aget v1, v5, v0

    .line 819065
    if-eqz v1, :cond_0

    .line 819066
    packed-switch v1, :pswitch_data_0

    .line 819067
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v8, "Invalid byte "

    invoke-direct {v1, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v8, " in short Unicode text block"

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/15v;->e(Ljava/lang/String;)V

    :cond_0
    move v1, v2

    move v2, v3

    .line 819068
    :goto_1
    add-int/lit8 v3, v2, 0x1

    int-to-char v0, v0

    aput-char v0, v4, v2

    goto :goto_0

    .line 819069
    :pswitch_0
    and-int/lit8 v0, v0, 0x1f

    shl-int/lit8 v0, v0, 0x6

    add-int/lit8 v1, v2, 0x1

    aget-byte v2, v6, v2

    and-int/lit8 v2, v2, 0x3f

    or-int/2addr v0, v2

    move v2, v3

    .line 819070
    goto :goto_1

    .line 819071
    :pswitch_1
    and-int/lit8 v0, v0, 0xf

    shl-int/lit8 v0, v0, 0xc

    add-int/lit8 v8, v2, 0x1

    aget-byte v1, v6, v2

    and-int/lit8 v1, v1, 0x3f

    shl-int/lit8 v1, v1, 0x6

    or-int/2addr v0, v1

    add-int/lit8 v1, v8, 0x1

    aget-byte v2, v6, v8

    and-int/lit8 v2, v2, 0x3f

    or-int/2addr v0, v2

    move v2, v3

    .line 819072
    goto :goto_1

    .line 819073
    :pswitch_2
    and-int/lit8 v0, v0, 0x7

    shl-int/lit8 v0, v0, 0x12

    add-int/lit8 v1, v2, 0x1

    aget-byte v2, v6, v2

    and-int/lit8 v2, v2, 0x3f

    shl-int/lit8 v2, v2, 0xc

    or-int/2addr v0, v2

    add-int/lit8 v2, v1, 0x1

    aget-byte v1, v6, v1

    and-int/lit8 v1, v1, 0x3f

    shl-int/lit8 v1, v1, 0x6

    or-int/2addr v0, v1

    add-int/lit8 v1, v2, 0x1

    aget-byte v2, v6, v2

    and-int/lit8 v2, v2, 0x3f

    or-int/2addr v0, v2

    .line 819074
    const/high16 v2, 0x10000

    sub-int/2addr v0, v2

    .line 819075
    add-int/lit8 v2, v3, 0x1

    const v8, 0xd800

    shr-int/lit8 v9, v0, 0xa

    or-int/2addr v8, v9

    int-to-char v8, v8

    aput-char v8, v4, v3

    .line 819076
    const v3, 0xdc00

    and-int/lit16 v0, v0, 0x3ff

    or-int/2addr v0, v3

    .line 819077
    goto :goto_1

    .line 819078
    :cond_1
    iget-object v0, p0, LX/15u;->n:LX/15x;

    .line 819079
    iput v3, v0, LX/15x;->j:I

    .line 819080
    iget-object v0, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v0}, LX/15x;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private final j(I)LX/0lx;
    .locals 6

    .prologue
    .line 819020
    iget v0, p0, LX/15u;->e:I

    iget v1, p0, LX/15u;->d:I

    sub-int/2addr v0, v1

    if-ge v0, p1, :cond_0

    .line 819021
    invoke-direct {p0, p1}, LX/4sE;->f(I)Z

    .line 819022
    :cond_0
    const/4 v0, 0x5

    if-ge p1, v0, :cond_2

    .line 819023
    iget v1, p0, LX/15u;->d:I

    .line 819024
    iget-object v2, p0, LX/4sE;->Q:[B

    .line 819025
    aget-byte v0, v2, v1

    and-int/lit16 v0, v0, 0xff

    .line 819026
    add-int/lit8 v3, p1, -0x1

    if-lez v3, :cond_1

    .line 819027
    shl-int/lit8 v0, v0, 0x8

    add-int/lit8 v1, v1, 0x1

    aget-byte v4, v2, v1

    and-int/lit16 v4, v4, 0xff

    add-int/2addr v0, v4

    .line 819028
    add-int/lit8 v3, v3, -0x1

    if-lez v3, :cond_1

    .line 819029
    shl-int/lit8 v0, v0, 0x8

    add-int/lit8 v1, v1, 0x1

    aget-byte v4, v2, v1

    and-int/lit16 v4, v4, 0xff

    add-int/2addr v0, v4

    .line 819030
    add-int/lit8 v3, v3, -0x1

    if-lez v3, :cond_1

    .line 819031
    shl-int/lit8 v0, v0, 0x8

    add-int/lit8 v1, v1, 0x1

    aget-byte v1, v2, v1

    and-int/lit16 v1, v1, 0xff

    add-int/2addr v0, v1

    .line 819032
    :cond_1
    iput v0, p0, LX/4sE;->X:I

    .line 819033
    iget-object v1, p0, LX/4sE;->V:LX/0lv;

    invoke-virtual {v1, v0}, LX/0lv;->a(I)LX/0lx;

    move-result-object v0

    .line 819034
    :goto_0
    return-object v0

    .line 819035
    :cond_2
    const/16 v0, 0x9

    if-ge p1, v0, :cond_4

    .line 819036
    iget v0, p0, LX/15u;->d:I

    .line 819037
    iget-object v1, p0, LX/4sE;->Q:[B

    .line 819038
    aget-byte v2, v1, v0

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x8

    .line 819039
    add-int/lit8 v0, v0, 0x1

    aget-byte v3, v1, v0

    and-int/lit16 v3, v3, 0xff

    add-int/2addr v2, v3

    .line 819040
    shl-int/lit8 v2, v2, 0x8

    .line 819041
    add-int/lit8 v0, v0, 0x1

    aget-byte v3, v1, v0

    and-int/lit16 v3, v3, 0xff

    add-int/2addr v2, v3

    .line 819042
    shl-int/lit8 v2, v2, 0x8

    .line 819043
    add-int/lit8 v0, v0, 0x1

    aget-byte v3, v1, v0

    and-int/lit16 v3, v3, 0xff

    add-int/2addr v2, v3

    .line 819044
    add-int/lit8 v3, v0, 0x1

    aget-byte v0, v1, v3

    and-int/lit16 v0, v0, 0xff

    .line 819045
    add-int/lit8 v4, p1, -0x5

    .line 819046
    if-lez v4, :cond_3

    .line 819047
    shl-int/lit8 v0, v0, 0x8

    add-int/lit8 v3, v3, 0x1

    aget-byte v5, v1, v3

    and-int/lit16 v5, v5, 0xff

    add-int/2addr v0, v5

    .line 819048
    add-int/lit8 v4, v4, -0x1

    if-lez v4, :cond_3

    .line 819049
    shl-int/lit8 v0, v0, 0x8

    add-int/lit8 v3, v3, 0x1

    aget-byte v5, v1, v3

    and-int/lit16 v5, v5, 0xff

    add-int/2addr v0, v5

    .line 819050
    add-int/lit8 v4, v4, -0x1

    if-lez v4, :cond_3

    .line 819051
    shl-int/lit8 v0, v0, 0x8

    add-int/lit8 v3, v3, 0x1

    aget-byte v1, v1, v3

    and-int/lit16 v1, v1, 0xff

    add-int/2addr v0, v1

    .line 819052
    :cond_3
    iput v2, p0, LX/4sE;->X:I

    .line 819053
    iput v0, p0, LX/4sE;->Y:I

    .line 819054
    iget-object v1, p0, LX/4sE;->V:LX/0lv;

    invoke-virtual {v1, v2, v0}, LX/0lv;->a(II)LX/0lx;

    move-result-object v0

    goto :goto_0

    .line 819055
    :cond_4
    invoke-direct {p0, p1}, LX/4sE;->k(I)LX/0lx;

    move-result-object v0

    goto :goto_0
.end method

.method private final k(I)LX/0lx;
    .locals 6

    .prologue
    .line 818998
    add-int/lit8 v0, p1, 0x3

    shr-int/lit8 v0, v0, 0x2

    .line 818999
    iget-object v1, p0, LX/4sE;->W:[I

    array-length v1, v1

    if-le v0, v1, :cond_0

    .line 819000
    iget-object v1, p0, LX/4sE;->W:[I

    invoke-static {v1, v0}, LX/4sE;->a([II)[I

    move-result-object v0

    iput-object v0, p0, LX/4sE;->W:[I

    .line 819001
    :cond_0
    const/4 v1, 0x0

    .line 819002
    iget v0, p0, LX/15u;->d:I

    .line 819003
    iget-object v3, p0, LX/4sE;->Q:[B

    .line 819004
    :goto_0
    add-int/lit8 v2, v0, 0x1

    aget-byte v0, v3, v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x8

    .line 819005
    add-int/lit8 v4, v2, 0x1

    aget-byte v2, v3, v2

    and-int/lit16 v2, v2, 0xff

    or-int/2addr v0, v2

    .line 819006
    shl-int/lit8 v0, v0, 0x8

    .line 819007
    add-int/lit8 v2, v4, 0x1

    aget-byte v4, v3, v4

    and-int/lit16 v4, v4, 0xff

    or-int/2addr v0, v4

    .line 819008
    shl-int/lit8 v4, v0, 0x8

    .line 819009
    add-int/lit8 v0, v2, 0x1

    aget-byte v2, v3, v2

    and-int/lit16 v2, v2, 0xff

    or-int/2addr v4, v2

    .line 819010
    iget-object v5, p0, LX/4sE;->W:[I

    add-int/lit8 v2, v1, 0x1

    aput v4, v5, v1

    .line 819011
    add-int/lit8 p1, p1, -0x4

    const/4 v1, 0x3

    if-gt p1, v1, :cond_4

    .line 819012
    if-lez p1, :cond_2

    .line 819013
    aget-byte v1, v3, v0

    and-int/lit16 v1, v1, 0xff

    .line 819014
    add-int/lit8 v4, p1, -0x1

    if-lez v4, :cond_3

    .line 819015
    shl-int/lit8 v1, v1, 0x8

    add-int/lit8 v5, v0, 0x1

    aget-byte v0, v3, v5

    and-int/lit16 v0, v0, 0xff

    add-int/2addr v0, v1

    .line 819016
    add-int/lit8 v1, v4, -0x1

    if-lez v1, :cond_1

    .line 819017
    shl-int/lit8 v0, v0, 0x8

    add-int/lit8 v1, v5, 0x1

    aget-byte v1, v3, v1

    and-int/lit16 v1, v1, 0xff

    add-int/2addr v0, v1

    .line 819018
    :cond_1
    :goto_1
    iget-object v3, p0, LX/4sE;->W:[I

    add-int/lit8 v1, v2, 0x1

    aput v0, v3, v2

    move v2, v1

    .line 819019
    :cond_2
    iget-object v0, p0, LX/4sE;->V:LX/0lv;

    iget-object v1, p0, LX/4sE;->W:[I

    invoke-virtual {v0, v1, v2}, LX/0lv;->a([II)LX/0lx;

    move-result-object v0

    return-object v0

    :cond_3
    move v0, v1

    goto :goto_1

    :cond_4
    move v1, v2

    goto :goto_0
.end method

.method private l(I)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 818980
    and-int/lit8 v0, p1, 0x1f

    .line 818981
    shr-int/lit8 v1, v0, 0x2

    .line 818982
    if-ne v1, v2, :cond_3

    .line 818983
    and-int/lit8 v0, v0, 0x3

    .line 818984
    if-nez v0, :cond_0

    .line 818985
    invoke-direct {p0}, LX/4sE;->ab()V

    .line 818986
    :goto_0
    return-void

    .line 818987
    :cond_0
    if-ne v0, v2, :cond_1

    .line 818988
    invoke-direct {p0}, LX/4sE;->ac()V

    goto :goto_0

    .line 818989
    :cond_1
    if-ne v0, v3, :cond_2

    .line 818990
    invoke-direct {p0}, LX/4sE;->ad()V

    goto :goto_0

    .line 818991
    :cond_2
    invoke-static {}, LX/4pl;->b()V

    goto :goto_0

    .line 818992
    :cond_3
    if-ne v1, v3, :cond_4

    .line 818993
    and-int/lit8 v0, v0, 0x3

    packed-switch v0, :pswitch_data_0

    .line 818994
    :cond_4
    invoke-static {}, LX/4pl;->b()V

    goto :goto_0

    .line 818995
    :pswitch_0
    invoke-direct {p0}, LX/4sE;->ae()V

    goto :goto_0

    .line 818996
    :pswitch_1
    invoke-direct {p0}, LX/4sE;->af()V

    goto :goto_0

    .line 818997
    :pswitch_2
    invoke-direct {p0}, LX/4sE;->ah()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private m(I)V
    .locals 7

    .prologue
    .line 818774
    iget v0, p0, LX/15u;->e:I

    iget v1, p0, LX/15u;->d:I

    sub-int/2addr v0, v1

    if-ge v0, p1, :cond_0

    .line 818775
    invoke-direct {p0, p1}, LX/4sE;->f(I)Z

    .line 818776
    :cond_0
    iget-object v0, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v0}, LX/15x;->l()[C

    move-result-object v3

    .line 818777
    const/4 v1, 0x0

    .line 818778
    iget-object v4, p0, LX/4sE;->Q:[B

    .line 818779
    iget v0, p0, LX/15u;->d:I

    .line 818780
    add-int v5, v0, p1

    :goto_0
    if-ge v0, v5, :cond_1

    .line 818781
    add-int/lit8 v2, v1, 0x1

    aget-byte v6, v4, v0

    int-to-char v6, v6

    aput-char v6, v3, v1

    .line 818782
    add-int/lit8 v0, v0, 0x1

    move v1, v2

    goto :goto_0

    .line 818783
    :cond_1
    iput v0, p0, LX/4sE;->d:I

    .line 818784
    iget-object v0, p0, LX/15u;->n:LX/15x;

    .line 818785
    iput p1, v0, LX/15x;->j:I

    .line 818786
    return-void
.end method

.method private n(I)V
    .locals 10

    .prologue
    .line 818948
    iget v0, p0, LX/15u;->e:I

    iget v1, p0, LX/15u;->d:I

    sub-int/2addr v0, v1

    if-ge v0, p1, :cond_0

    .line 818949
    invoke-direct {p0, p1}, LX/4sE;->f(I)Z

    .line 818950
    :cond_0
    const/4 v3, 0x0

    .line 818951
    iget-object v0, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v0}, LX/15x;->l()[C

    move-result-object v4

    .line 818952
    iget v1, p0, LX/15u;->d:I

    .line 818953
    iget v0, p0, LX/15u;->d:I

    add-int/2addr v0, p1

    iput v0, p0, LX/4sE;->d:I

    .line 818954
    sget-object v5, LX/4s7;->a:[I

    .line 818955
    iget-object v6, p0, LX/4sE;->Q:[B

    .line 818956
    add-int v7, v1, p1

    :goto_0
    if-ge v1, v7, :cond_2

    .line 818957
    add-int/lit8 v2, v1, 0x1

    aget-byte v0, v6, v1

    and-int/lit16 v0, v0, 0xff

    .line 818958
    aget v1, v5, v0

    .line 818959
    if-eqz v1, :cond_1

    .line 818960
    packed-switch v1, :pswitch_data_0

    .line 818961
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v8, "Invalid byte "

    invoke-direct {v1, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v8, " in short Unicode text block"

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/15v;->e(Ljava/lang/String;)V

    :cond_1
    move v1, v2

    move v2, v3

    .line 818962
    :goto_1
    add-int/lit8 v3, v2, 0x1

    int-to-char v0, v0

    aput-char v0, v4, v2

    goto :goto_0

    .line 818963
    :pswitch_0
    and-int/lit8 v0, v0, 0x1f

    shl-int/lit8 v0, v0, 0x6

    add-int/lit8 v1, v2, 0x1

    aget-byte v2, v6, v2

    and-int/lit8 v2, v2, 0x3f

    or-int/2addr v0, v2

    move v2, v3

    .line 818964
    goto :goto_1

    .line 818965
    :pswitch_1
    and-int/lit8 v0, v0, 0xf

    shl-int/lit8 v0, v0, 0xc

    add-int/lit8 v8, v2, 0x1

    aget-byte v1, v6, v2

    and-int/lit8 v1, v1, 0x3f

    shl-int/lit8 v1, v1, 0x6

    or-int/2addr v0, v1

    add-int/lit8 v1, v8, 0x1

    aget-byte v2, v6, v8

    and-int/lit8 v2, v2, 0x3f

    or-int/2addr v0, v2

    move v2, v3

    .line 818966
    goto :goto_1

    .line 818967
    :pswitch_2
    and-int/lit8 v0, v0, 0x7

    shl-int/lit8 v0, v0, 0x12

    add-int/lit8 v1, v2, 0x1

    aget-byte v2, v6, v2

    and-int/lit8 v2, v2, 0x3f

    shl-int/lit8 v2, v2, 0xc

    or-int/2addr v0, v2

    add-int/lit8 v2, v1, 0x1

    aget-byte v1, v6, v1

    and-int/lit8 v1, v1, 0x3f

    shl-int/lit8 v1, v1, 0x6

    or-int/2addr v0, v1

    add-int/lit8 v1, v2, 0x1

    aget-byte v2, v6, v2

    and-int/lit8 v2, v2, 0x3f

    or-int/2addr v0, v2

    .line 818968
    const/high16 v2, 0x10000

    sub-int/2addr v0, v2

    .line 818969
    add-int/lit8 v2, v3, 0x1

    const v8, 0xd800

    shr-int/lit8 v9, v0, 0xa

    or-int/2addr v8, v9

    int-to-char v8, v8

    aput-char v8, v4, v3

    .line 818970
    const v3, 0xdc00

    and-int/lit16 v0, v0, 0x3ff

    or-int/2addr v0, v3

    .line 818971
    goto :goto_1

    .line 818972
    :cond_2
    iget-object v0, p0, LX/15u;->n:LX/15x;

    .line 818973
    iput v3, v0, LX/15x;->j:I

    .line 818974
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private o(I)V
    .locals 2

    .prologue
    .line 818351
    :goto_0
    iget v0, p0, LX/15u;->e:I

    iget v1, p0, LX/15u;->d:I

    sub-int/2addr v0, v1

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 818352
    iget v1, p0, LX/15u;->d:I

    add-int/2addr v1, v0

    iput v1, p0, LX/4sE;->d:I

    .line 818353
    sub-int/2addr p1, v0

    .line 818354
    if-gtz p1, :cond_0

    .line 818355
    return-void

    .line 818356
    :cond_0
    invoke-virtual {p0}, LX/15u;->K()V

    goto :goto_0
.end method

.method private final p(I)I
    .locals 3

    .prologue
    .line 818345
    iget v0, p0, LX/15u;->d:I

    iget v1, p0, LX/15u;->e:I

    if-lt v0, v1, :cond_0

    .line 818346
    invoke-virtual {p0}, LX/15u;->K()V

    .line 818347
    :cond_0
    iget-object v0, p0, LX/4sE;->Q:[B

    iget v1, p0, LX/15u;->d:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/4sE;->d:I

    aget-byte v0, v0, v1

    .line 818348
    and-int/lit16 v1, v0, 0xc0

    const/16 v2, 0x80

    if-eq v1, v2, :cond_1

    .line 818349
    and-int/lit16 v1, v0, 0xff

    iget v2, p0, LX/15u;->d:I

    invoke-direct {p0, v1, v2}, LX/4sE;->a(II)V

    .line 818350
    :cond_1
    and-int/lit8 v1, p1, 0x1f

    shl-int/lit8 v1, v1, 0x6

    and-int/lit8 v0, v0, 0x3f

    or-int/2addr v0, v1

    return v0
.end method

.method private final q(I)I
    .locals 5

    .prologue
    const/16 v4, 0x80

    .line 818331
    iget v0, p0, LX/15u;->d:I

    iget v1, p0, LX/15u;->e:I

    if-lt v0, v1, :cond_0

    .line 818332
    invoke-virtual {p0}, LX/15u;->K()V

    .line 818333
    :cond_0
    and-int/lit8 v0, p1, 0xf

    .line 818334
    iget-object v1, p0, LX/4sE;->Q:[B

    iget v2, p0, LX/15u;->d:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/4sE;->d:I

    aget-byte v1, v1, v2

    .line 818335
    and-int/lit16 v2, v1, 0xc0

    if-eq v2, v4, :cond_1

    .line 818336
    and-int/lit16 v2, v1, 0xff

    iget v3, p0, LX/15u;->d:I

    invoke-direct {p0, v2, v3}, LX/4sE;->a(II)V

    .line 818337
    :cond_1
    shl-int/lit8 v0, v0, 0x6

    and-int/lit8 v1, v1, 0x3f

    or-int/2addr v0, v1

    .line 818338
    iget v1, p0, LX/15u;->d:I

    iget v2, p0, LX/15u;->e:I

    if-lt v1, v2, :cond_2

    .line 818339
    invoke-virtual {p0}, LX/15u;->K()V

    .line 818340
    :cond_2
    iget-object v1, p0, LX/4sE;->Q:[B

    iget v2, p0, LX/15u;->d:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/4sE;->d:I

    aget-byte v1, v1, v2

    .line 818341
    and-int/lit16 v2, v1, 0xc0

    if-eq v2, v4, :cond_3

    .line 818342
    and-int/lit16 v2, v1, 0xff

    iget v3, p0, LX/15u;->d:I

    invoke-direct {p0, v2, v3}, LX/4sE;->a(II)V

    .line 818343
    :cond_3
    shl-int/lit8 v0, v0, 0x6

    and-int/lit8 v1, v1, 0x3f

    or-int/2addr v0, v1

    .line 818344
    return v0
.end method

.method private final r(I)I
    .locals 5

    .prologue
    const/16 v4, 0x80

    .line 818321
    and-int/lit8 v0, p1, 0xf

    .line 818322
    iget-object v1, p0, LX/4sE;->Q:[B

    iget v2, p0, LX/15u;->d:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/4sE;->d:I

    aget-byte v1, v1, v2

    .line 818323
    and-int/lit16 v2, v1, 0xc0

    if-eq v2, v4, :cond_0

    .line 818324
    and-int/lit16 v2, v1, 0xff

    iget v3, p0, LX/15u;->d:I

    invoke-direct {p0, v2, v3}, LX/4sE;->a(II)V

    .line 818325
    :cond_0
    shl-int/lit8 v0, v0, 0x6

    and-int/lit8 v1, v1, 0x3f

    or-int/2addr v0, v1

    .line 818326
    iget-object v1, p0, LX/4sE;->Q:[B

    iget v2, p0, LX/15u;->d:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/4sE;->d:I

    aget-byte v1, v1, v2

    .line 818327
    and-int/lit16 v2, v1, 0xc0

    if-eq v2, v4, :cond_1

    .line 818328
    and-int/lit16 v2, v1, 0xff

    iget v3, p0, LX/15u;->d:I

    invoke-direct {p0, v2, v3}, LX/4sE;->a(II)V

    .line 818329
    :cond_1
    shl-int/lit8 v0, v0, 0x6

    and-int/lit8 v1, v1, 0x3f

    or-int/2addr v0, v1

    .line 818330
    return v0
.end method

.method private final s(I)I
    .locals 5

    .prologue
    const/16 v4, 0x80

    .line 818303
    iget v0, p0, LX/15u;->d:I

    iget v1, p0, LX/15u;->e:I

    if-lt v0, v1, :cond_0

    .line 818304
    invoke-virtual {p0}, LX/15u;->K()V

    .line 818305
    :cond_0
    iget-object v0, p0, LX/4sE;->Q:[B

    iget v1, p0, LX/15u;->d:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/4sE;->d:I

    aget-byte v0, v0, v1

    .line 818306
    and-int/lit16 v1, v0, 0xc0

    if-eq v1, v4, :cond_1

    .line 818307
    and-int/lit16 v1, v0, 0xff

    iget v2, p0, LX/15u;->d:I

    invoke-direct {p0, v1, v2}, LX/4sE;->a(II)V

    .line 818308
    :cond_1
    and-int/lit8 v1, p1, 0x7

    shl-int/lit8 v1, v1, 0x6

    and-int/lit8 v0, v0, 0x3f

    or-int/2addr v0, v1

    .line 818309
    iget v1, p0, LX/15u;->d:I

    iget v2, p0, LX/15u;->e:I

    if-lt v1, v2, :cond_2

    .line 818310
    invoke-virtual {p0}, LX/15u;->K()V

    .line 818311
    :cond_2
    iget-object v1, p0, LX/4sE;->Q:[B

    iget v2, p0, LX/15u;->d:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/4sE;->d:I

    aget-byte v1, v1, v2

    .line 818312
    and-int/lit16 v2, v1, 0xc0

    if-eq v2, v4, :cond_3

    .line 818313
    and-int/lit16 v2, v1, 0xff

    iget v3, p0, LX/15u;->d:I

    invoke-direct {p0, v2, v3}, LX/4sE;->a(II)V

    .line 818314
    :cond_3
    shl-int/lit8 v0, v0, 0x6

    and-int/lit8 v1, v1, 0x3f

    or-int/2addr v0, v1

    .line 818315
    iget v1, p0, LX/15u;->d:I

    iget v2, p0, LX/15u;->e:I

    if-lt v1, v2, :cond_4

    .line 818316
    invoke-virtual {p0}, LX/15u;->K()V

    .line 818317
    :cond_4
    iget-object v1, p0, LX/4sE;->Q:[B

    iget v2, p0, LX/15u;->d:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/4sE;->d:I

    aget-byte v1, v1, v2

    .line 818318
    and-int/lit16 v2, v1, 0xc0

    if-eq v2, v4, :cond_5

    .line 818319
    and-int/lit16 v2, v1, 0xff

    iget v3, p0, LX/15u;->d:I

    invoke-direct {p0, v2, v3}, LX/4sE;->a(II)V

    .line 818320
    :cond_5
    shl-int/lit8 v0, v0, 0x6

    and-int/lit8 v1, v1, 0x3f

    or-int/2addr v0, v1

    const/high16 v1, 0x10000

    sub-int/2addr v0, v1

    return v0
.end method

.method private t(I)V
    .locals 2

    .prologue
    .line 818299
    iget-object v0, p0, LX/4sE;->Z:[Ljava/lang/String;

    if-nez v0, :cond_0

    .line 818300
    const-string v0, "Encountered shared name reference, even though document header explicitly declared no shared name references are included"

    invoke-virtual {p0, v0}, LX/15v;->e(Ljava/lang/String;)V

    .line 818301
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Invalid shared name reference "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; only got "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/4sE;->aa:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " names in buffer (invalid content)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/15v;->e(Ljava/lang/String;)V

    .line 818302
    return-void
.end method

.method private u(I)V
    .locals 2

    .prologue
    .line 818295
    iget-object v0, p0, LX/4sE;->ab:[Ljava/lang/String;

    if-nez v0, :cond_0

    .line 818296
    const-string v0, "Encountered shared text value reference, even though document header did not declared shared text value references may be included"

    invoke-virtual {p0, v0}, LX/15v;->e(Ljava/lang/String;)V

    .line 818297
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Invalid shared text value reference "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; only got "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/4sE;->ac:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " names in buffer (invalid content)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/15v;->e(Ljava/lang/String;)V

    .line 818298
    return-void
.end method

.method private v(I)V
    .locals 1

    .prologue
    .line 818291
    const/16 v0, 0x20

    if-ge p1, v0, :cond_0

    .line 818292
    invoke-virtual {p0, p1}, LX/15v;->d(I)V

    .line 818293
    :cond_0
    invoke-direct {p0, p1}, LX/4sE;->w(I)V

    .line 818294
    return-void
.end method

.method private w(I)V
    .locals 2

    .prologue
    .line 818238
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Invalid UTF-8 start byte 0x"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/15v;->e(Ljava/lang/String;)V

    .line 818239
    return-void
.end method

.method private x(I)V
    .locals 2

    .prologue
    .line 818287
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Invalid UTF-8 middle byte 0x"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/15v;->e(Ljava/lang/String;)V

    .line 818288
    return-void
.end method


# virtual methods
.method public final D()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 818282
    iget-boolean v0, p0, LX/4sE;->S:Z

    if-eqz v0, :cond_0

    .line 818283
    invoke-direct {p0}, LX/4sE;->aa()V

    .line 818284
    :cond_0
    iget-object v0, p0, LX/15v;->K:LX/15z;

    sget-object v1, LX/15z;->VALUE_EMBEDDED_OBJECT:LX/15z;

    if-ne v0, v1, :cond_1

    .line 818285
    iget-object v0, p0, LX/15u;->r:[B

    .line 818286
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final I()Ljava/lang/String;
    .locals 2

    .prologue
    .line 818278
    iget-object v0, p0, LX/15v;->K:LX/15z;

    sget-object v1, LX/15z;->VALUE_STRING:LX/15z;

    if-eq v0, v1, :cond_1

    .line 818279
    iget-object v0, p0, LX/15v;->K:LX/15z;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/15v;->K:LX/15z;

    sget-object v1, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/15v;->K:LX/15z;

    invoke-virtual {v0}, LX/15z;->isScalarValue()Z

    move-result v0

    if-nez v0, :cond_1

    .line 818280
    :cond_0
    const/4 v0, 0x0

    .line 818281
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final L()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 818361
    iget-wide v2, p0, LX/15u;->f:J

    iget v1, p0, LX/15u;->e:I

    int-to-long v4, v1

    add-long/2addr v2, v4

    iput-wide v2, p0, LX/4sE;->f:J

    .line 818362
    iget-object v1, p0, LX/4sE;->P:Ljava/io/InputStream;

    if-eqz v1, :cond_0

    .line 818363
    iget-object v1, p0, LX/4sE;->P:Ljava/io/InputStream;

    iget-object v2, p0, LX/4sE;->Q:[B

    iget-object v3, p0, LX/4sE;->Q:[B

    array-length v3, v3

    invoke-virtual {v1, v2, v0, v3}, Ljava/io/InputStream;->read([BII)I

    move-result v1

    .line 818364
    if-lez v1, :cond_1

    .line 818365
    iput v0, p0, LX/4sE;->d:I

    .line 818366
    iput v1, p0, LX/4sE;->e:I

    .line 818367
    const/4 v0, 0x1

    .line 818368
    :cond_0
    return v0

    .line 818369
    :cond_1
    invoke-virtual {p0}, LX/4sE;->N()V

    .line 818370
    if-nez v1, :cond_0

    .line 818371
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "InputStream.read() returned 0 characters when trying to read "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/4sE;->Q:[B

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " bytes"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final N()V
    .locals 2

    .prologue
    .line 818271
    iget-object v0, p0, LX/4sE;->P:Ljava/io/InputStream;

    if-eqz v0, :cond_2

    .line 818272
    iget-object v0, p0, LX/15u;->b:LX/12A;

    .line 818273
    iget-boolean v1, v0, LX/12A;->c:Z

    move v0, v1

    .line 818274
    if-nez v0, :cond_0

    sget-object v0, LX/0lr;->AUTO_CLOSE_SOURCE:LX/0lr;

    invoke-virtual {p0, v0}, LX/15w;->a(LX/0lr;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 818275
    :cond_0
    iget-object v0, p0, LX/4sE;->P:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 818276
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, LX/4sE;->P:Ljava/io/InputStream;

    .line 818277
    :cond_2
    return-void
.end method

.method public final O()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 818250
    invoke-super {p0}, LX/15u;->O()V

    .line 818251
    iget-boolean v0, p0, LX/4sE;->R:Z

    if-eqz v0, :cond_0

    .line 818252
    iget-object v0, p0, LX/4sE;->Q:[B

    .line 818253
    if-eqz v0, :cond_0

    .line 818254
    iput-object v2, p0, LX/4sE;->Q:[B

    .line 818255
    iget-object v1, p0, LX/15u;->b:LX/12A;

    invoke-virtual {v1, v0}, LX/12A;->a([B)V

    .line 818256
    :cond_0
    iget-object v0, p0, LX/4sE;->Z:[Ljava/lang/String;

    .line 818257
    if-eqz v0, :cond_2

    array-length v1, v0

    if-lez v1, :cond_2

    .line 818258
    iput-object v2, p0, LX/4sE;->Z:[Ljava/lang/String;

    .line 818259
    iget v1, p0, LX/4sE;->aa:I

    if-lez v1, :cond_1

    .line 818260
    iget v1, p0, LX/4sE;->aa:I

    invoke-static {v0, v3, v1, v2}, Ljava/util/Arrays;->fill([Ljava/lang/Object;IILjava/lang/Object;)V

    .line 818261
    :cond_1
    iget-object v1, p0, LX/4sE;->O:LX/4s6;

    .line 818262
    iput-object v0, v1, LX/4s6;->a:[Ljava/lang/Object;

    .line 818263
    :cond_2
    iget-object v0, p0, LX/4sE;->ab:[Ljava/lang/String;

    .line 818264
    if-eqz v0, :cond_4

    array-length v1, v0

    if-lez v1, :cond_4

    .line 818265
    iput-object v2, p0, LX/4sE;->ab:[Ljava/lang/String;

    .line 818266
    iget v1, p0, LX/4sE;->ac:I

    if-lez v1, :cond_3

    .line 818267
    iget v1, p0, LX/4sE;->ac:I

    invoke-static {v0, v3, v1, v2}, Ljava/util/Arrays;->fill([Ljava/lang/Object;IILjava/lang/Object;)V

    .line 818268
    :cond_3
    iget-object v1, p0, LX/4sE;->O:LX/4s6;

    .line 818269
    iput-object v0, v1, LX/4s6;->b:[Ljava/lang/Object;

    .line 818270
    :cond_4
    return-void
.end method

.method public final a(I)I
    .locals 2

    .prologue
    .line 818247
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v0

    sget-object v1, LX/15z;->VALUE_NUMBER_INT:LX/15z;

    if-ne v0, v1, :cond_0

    .line 818248
    invoke-virtual {p0}, LX/15w;->x()I

    move-result p1

    .line 818249
    :cond_0
    return p1
.end method

.method public final a(Ljava/io/OutputStream;)I
    .locals 3

    .prologue
    .line 818241
    iget v0, p0, LX/15u;->e:I

    iget v1, p0, LX/15u;->d:I

    sub-int/2addr v0, v1

    .line 818242
    if-gtz v0, :cond_0

    .line 818243
    const/4 v0, 0x0

    .line 818244
    :goto_0
    return v0

    .line 818245
    :cond_0
    iget v1, p0, LX/15u;->d:I

    .line 818246
    iget-object v2, p0, LX/4sE;->Q:[B

    invoke-virtual {p1, v2, v1, v0}, Ljava/io/OutputStream;->write([BII)V

    goto :goto_0
.end method

.method public final a()LX/0lD;
    .locals 1

    .prologue
    .line 818240
    iget-object v0, p0, LX/4sE;->M:LX/0lD;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 818434
    iget-object v0, p0, LX/15v;->K:LX/15z;

    sget-object v1, LX/15z;->VALUE_STRING:LX/15z;

    if-eq v0, v1, :cond_1

    .line 818435
    iget-object v0, p0, LX/15v;->K:LX/15z;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/15v;->K:LX/15z;

    sget-object v1, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/15v;->K:LX/15z;

    invoke-virtual {v0}, LX/15z;->isScalarValue()Z

    move-result v0

    if-nez v0, :cond_1

    .line 818436
    :cond_0
    :goto_0
    return-object p1

    :cond_1
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method public final a(LX/0lD;)V
    .locals 0

    .prologue
    .line 818289
    iput-object p1, p0, LX/4sE;->M:LX/0lD;

    .line 818290
    return-void
.end method

.method public final a(ZZ)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 818585
    if-eqz p1, :cond_0

    .line 818586
    iget v2, p0, LX/15u;->d:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, LX/4sE;->d:I

    .line 818587
    :cond_0
    iget v2, p0, LX/15u;->d:I

    iget v3, p0, LX/15u;->e:I

    if-lt v2, v3, :cond_1

    .line 818588
    invoke-virtual {p0}, LX/15u;->K()V

    .line 818589
    :cond_1
    iget-object v2, p0, LX/4sE;->Q:[B

    iget v3, p0, LX/15u;->d:I

    aget-byte v2, v2, v3

    const/16 v3, 0x29

    if-eq v2, v3, :cond_3

    .line 818590
    if-eqz p2, :cond_2

    .line 818591
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Malformed content: signature not valid, starts with 0x3a but followed by 0x"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/4sE;->Q:[B

    iget v3, p0, LX/15u;->d:I

    aget-byte v2, v2, v3

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", not 0x29"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/15v;->e(Ljava/lang/String;)V

    .line 818592
    :cond_2
    :goto_0
    return v0

    .line 818593
    :cond_3
    iget v2, p0, LX/15u;->d:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, LX/4sE;->d:I

    iget v3, p0, LX/15u;->e:I

    if-lt v2, v3, :cond_4

    .line 818594
    invoke-virtual {p0}, LX/15u;->K()V

    .line 818595
    :cond_4
    iget-object v2, p0, LX/4sE;->Q:[B

    iget v3, p0, LX/15u;->d:I

    aget-byte v2, v2, v3

    const/16 v3, 0xa

    if-eq v2, v3, :cond_5

    .line 818596
    if-eqz p2, :cond_2

    .line 818597
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Malformed content: signature not valid, starts with 0x3a, 0x29, but followed by 0x"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/4sE;->Q:[B

    iget v3, p0, LX/15u;->d:I

    aget-byte v2, v2, v3

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", not 0xA"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/15v;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 818598
    :cond_5
    iget v2, p0, LX/15u;->d:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, LX/4sE;->d:I

    iget v3, p0, LX/15u;->e:I

    if-lt v2, v3, :cond_6

    .line 818599
    invoke-virtual {p0}, LX/15u;->K()V

    .line 818600
    :cond_6
    iget-object v2, p0, LX/4sE;->Q:[B

    iget v3, p0, LX/15u;->d:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/4sE;->d:I

    aget-byte v2, v2, v3

    .line 818601
    shr-int/lit8 v3, v2, 0x4

    and-int/lit8 v3, v3, 0xf

    .line 818602
    if-eqz v3, :cond_7

    .line 818603
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Header version number bits (0x"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") indicate unrecognized version; only 0x0 handled by parser"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/15v;->e(Ljava/lang/String;)V

    .line 818604
    :cond_7
    and-int/lit8 v3, v2, 0x1

    if-nez v3, :cond_8

    .line 818605
    const/4 v3, 0x0

    iput-object v3, p0, LX/4sE;->Z:[Ljava/lang/String;

    .line 818606
    const/4 v3, -0x1

    iput v3, p0, LX/4sE;->aa:I

    .line 818607
    :cond_8
    and-int/lit8 v3, v2, 0x2

    if-eqz v3, :cond_9

    .line 818608
    sget-object v3, LX/4sE;->af:[Ljava/lang/String;

    iput-object v3, p0, LX/4sE;->ab:[Ljava/lang/String;

    .line 818609
    iput v0, p0, LX/4sE;->ac:I

    .line 818610
    :cond_9
    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_a

    move v0, v1

    :cond_a
    iput-boolean v0, p0, LX/4sE;->N:Z

    move v0, v1

    .line 818611
    goto/16 :goto_0
.end method

.method public final a(LX/0ln;)[B
    .locals 2

    .prologue
    .line 818580
    iget-boolean v0, p0, LX/4sE;->S:Z

    if-eqz v0, :cond_0

    .line 818581
    invoke-direct {p0}, LX/4sE;->aa()V

    .line 818582
    :cond_0
    iget-object v0, p0, LX/15v;->K:LX/15z;

    sget-object v1, LX/15z;->VALUE_EMBEDDED_OBJECT:LX/15z;

    if-eq v0, v1, :cond_1

    .line 818583
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Current token ("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/15v;->K:LX/15z;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") not VALUE_EMBEDDED_OBJECT, can not access as binary"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/15v;->e(Ljava/lang/String;)V

    .line 818584
    :cond_1
    iget-object v0, p0, LX/15u;->r:[B

    return-object v0
.end method

.method public final b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 818579
    iget-object v0, p0, LX/4sE;->P:Ljava/io/InputStream;

    return-object v0
.end method

.method public final c()LX/15z;
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v8, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 818501
    iput v1, p0, LX/4sE;->A:I

    .line 818502
    iget-boolean v3, p0, LX/4sE;->S:Z

    if-eqz v3, :cond_0

    .line 818503
    invoke-direct {p0}, LX/4sE;->an()V

    .line 818504
    :cond_0
    iget-wide v4, p0, LX/15u;->f:J

    iget v3, p0, LX/15u;->d:I

    int-to-long v6, v3

    add-long/2addr v4, v6

    iput-wide v4, p0, LX/4sE;->i:J

    .line 818505
    iput-object v2, p0, LX/4sE;->r:[B

    .line 818506
    iget-object v3, p0, LX/15u;->l:LX/15y;

    invoke-virtual {v3}, LX/12V;->d()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, LX/15v;->K:LX/15z;

    sget-object v4, LX/15z;->FIELD_NAME:LX/15z;

    if-eq v3, v4, :cond_1

    .line 818507
    invoke-direct {p0}, LX/4sE;->Y()LX/15z;

    move-result-object v0

    iput-object v0, p0, LX/4sE;->K:LX/15z;

    .line 818508
    :goto_0
    return-object v0

    .line 818509
    :cond_1
    iget v3, p0, LX/15u;->d:I

    iget v4, p0, LX/15u;->e:I

    if-lt v3, v4, :cond_2

    .line 818510
    invoke-virtual {p0}, LX/4sE;->L()Z

    move-result v3

    if-nez v3, :cond_2

    .line 818511
    invoke-virtual {p0}, LX/15u;->P()V

    .line 818512
    invoke-virtual {p0}, LX/15w;->close()V

    .line 818513
    iput-object v2, p0, LX/4sE;->K:LX/15z;

    move-object v0, v2

    goto :goto_0

    .line 818514
    :cond_2
    iget-object v3, p0, LX/4sE;->Q:[B

    iget v4, p0, LX/15u;->d:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, LX/4sE;->d:I

    aget-byte v3, v3, v4

    .line 818515
    iput v3, p0, LX/4sE;->T:I

    .line 818516
    shr-int/lit8 v4, v3, 0x5

    and-int/lit8 v4, v4, 0x7

    packed-switch v4, :pswitch_data_0

    .line 818517
    :cond_3
    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Invalid type marker byte 0x"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    and-int/lit16 v1, v3, 0xff

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " for expected value token"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/15v;->e(Ljava/lang/String;)V

    move-object v0, v2

    .line 818518
    goto :goto_0

    .line 818519
    :pswitch_0
    if-nez v3, :cond_4

    .line 818520
    const-string v0, "Invalid token byte 0x00"

    invoke-virtual {p0, v0}, LX/15v;->e(Ljava/lang/String;)V

    .line 818521
    :cond_4
    add-int/lit8 v0, v3, -0x1

    invoke-direct {p0, v0}, LX/4sE;->g(I)LX/15z;

    move-result-object v0

    goto :goto_0

    .line 818522
    :pswitch_1
    and-int/lit8 v4, v3, 0x1f

    .line 818523
    const/4 v5, 0x4

    if-ge v4, v5, :cond_5

    .line 818524
    packed-switch v4, :pswitch_data_1

    .line 818525
    sget-object v0, LX/15z;->VALUE_TRUE:LX/15z;

    iput-object v0, p0, LX/4sE;->K:LX/15z;

    goto :goto_0

    .line 818526
    :pswitch_2
    iget-object v0, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v0}, LX/15x;->b()V

    .line 818527
    sget-object v0, LX/15z;->VALUE_STRING:LX/15z;

    iput-object v0, p0, LX/4sE;->K:LX/15z;

    goto :goto_0

    .line 818528
    :pswitch_3
    sget-object v0, LX/15z;->VALUE_NULL:LX/15z;

    iput-object v0, p0, LX/4sE;->K:LX/15z;

    goto :goto_0

    .line 818529
    :pswitch_4
    sget-object v0, LX/15z;->VALUE_FALSE:LX/15z;

    iput-object v0, p0, LX/4sE;->K:LX/15z;

    goto :goto_0

    .line 818530
    :cond_5
    const/16 v5, 0x8

    if-ge v4, v5, :cond_6

    .line 818531
    and-int/lit8 v4, v4, 0x3

    if-gt v4, v9, :cond_3

    .line 818532
    iput-boolean v0, p0, LX/4sE;->S:Z

    .line 818533
    iput v1, p0, LX/4sE;->A:I

    .line 818534
    sget-object v0, LX/15z;->VALUE_NUMBER_INT:LX/15z;

    iput-object v0, p0, LX/4sE;->K:LX/15z;

    goto/16 :goto_0

    .line 818535
    :cond_6
    const/16 v5, 0xc

    if-ge v4, v5, :cond_8

    .line 818536
    and-int/lit8 v4, v4, 0x3

    .line 818537
    if-gt v4, v9, :cond_3

    .line 818538
    iput-boolean v0, p0, LX/4sE;->S:Z

    .line 818539
    iput v1, p0, LX/4sE;->A:I

    .line 818540
    if-nez v4, :cond_7

    :goto_2
    iput-boolean v0, p0, LX/4sE;->U:Z

    .line 818541
    sget-object v0, LX/15z;->VALUE_NUMBER_FLOAT:LX/15z;

    iput-object v0, p0, LX/4sE;->K:LX/15z;

    goto/16 :goto_0

    :cond_7
    move v0, v1

    .line 818542
    goto :goto_2

    .line 818543
    :cond_8
    const/16 v0, 0x1a

    if-ne v4, v0, :cond_a

    .line 818544
    invoke-virtual {p0, v1, v1}, LX/4sE;->a(ZZ)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 818545
    iget-object v0, p0, LX/15v;->K:LX/15z;

    if-nez v0, :cond_9

    .line 818546
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v0

    goto/16 :goto_0

    .line 818547
    :cond_9
    iput-object v2, p0, LX/4sE;->K:LX/15z;

    move-object v0, v2

    goto/16 :goto_0

    .line 818548
    :cond_a
    const-string v0, "Unrecognized token byte 0x3A (malformed segment header?"

    invoke-virtual {p0, v0}, LX/15v;->e(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 818549
    :pswitch_5
    sget-object v1, LX/15z;->VALUE_STRING:LX/15z;

    iput-object v1, p0, LX/4sE;->K:LX/15z;

    .line 818550
    iget v1, p0, LX/4sE;->ac:I

    if-ltz v1, :cond_b

    .line 818551
    invoke-direct {p0}, LX/4sE;->W()V

    .line 818552
    :goto_3
    iget-object v0, p0, LX/15v;->K:LX/15z;

    goto/16 :goto_0

    .line 818553
    :cond_b
    iput-boolean v0, p0, LX/4sE;->S:Z

    goto :goto_3

    .line 818554
    :pswitch_6
    and-int/lit8 v1, v3, 0x1f

    invoke-static {v1}, LX/4sG;->b(I)I

    move-result v1

    iput v1, p0, LX/4sE;->B:I

    .line 818555
    iput v0, p0, LX/4sE;->A:I

    .line 818556
    sget-object v0, LX/15z;->VALUE_NUMBER_INT:LX/15z;

    iput-object v0, p0, LX/4sE;->K:LX/15z;

    goto/16 :goto_0

    .line 818557
    :pswitch_7
    and-int/lit8 v1, v3, 0x1f

    sparse-switch v1, :sswitch_data_0

    goto/16 :goto_1

    .line 818558
    :sswitch_0
    iput-boolean v0, p0, LX/4sE;->S:Z

    .line 818559
    sget-object v0, LX/15z;->VALUE_STRING:LX/15z;

    iput-object v0, p0, LX/4sE;->K:LX/15z;

    goto/16 :goto_0

    .line 818560
    :sswitch_1
    iput-boolean v0, p0, LX/4sE;->S:Z

    .line 818561
    sget-object v0, LX/15z;->VALUE_EMBEDDED_OBJECT:LX/15z;

    iput-object v0, p0, LX/4sE;->K:LX/15z;

    goto/16 :goto_0

    .line 818562
    :sswitch_2
    iget v0, p0, LX/15u;->d:I

    iget v1, p0, LX/15u;->e:I

    if-lt v0, v1, :cond_c

    .line 818563
    invoke-virtual {p0}, LX/15u;->K()V

    .line 818564
    :cond_c
    and-int/lit8 v0, v3, 0x3

    shl-int/lit8 v0, v0, 0x8

    iget-object v1, p0, LX/4sE;->Q:[B

    iget v2, p0, LX/15u;->d:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/4sE;->d:I

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    add-int/2addr v0, v1

    invoke-direct {p0, v0}, LX/4sE;->g(I)LX/15z;

    move-result-object v0

    goto/16 :goto_0

    .line 818565
    :sswitch_3
    iget-object v0, p0, LX/15u;->l:LX/15y;

    invoke-virtual {v0, v8, v8}, LX/15y;->b(II)LX/15y;

    move-result-object v0

    iput-object v0, p0, LX/4sE;->l:LX/15y;

    .line 818566
    sget-object v0, LX/15z;->START_ARRAY:LX/15z;

    iput-object v0, p0, LX/4sE;->K:LX/15z;

    goto/16 :goto_0

    .line 818567
    :sswitch_4
    iget-object v0, p0, LX/15u;->l:LX/15y;

    invoke-virtual {v0}, LX/12V;->b()Z

    move-result v0

    if-nez v0, :cond_d

    .line 818568
    const/16 v0, 0x5d

    const/16 v1, 0x7d

    invoke-virtual {p0, v0, v1}, LX/15u;->a(IC)V

    .line 818569
    :cond_d
    iget-object v0, p0, LX/15u;->l:LX/15y;

    .line 818570
    iget-object v1, v0, LX/15y;->c:LX/15y;

    move-object v0, v1

    .line 818571
    iput-object v0, p0, LX/4sE;->l:LX/15y;

    .line 818572
    sget-object v0, LX/15z;->END_ARRAY:LX/15z;

    iput-object v0, p0, LX/4sE;->K:LX/15z;

    goto/16 :goto_0

    .line 818573
    :sswitch_5
    iget-object v0, p0, LX/15u;->l:LX/15y;

    invoke-virtual {v0, v8, v8}, LX/15y;->c(II)LX/15y;

    move-result-object v0

    iput-object v0, p0, LX/4sE;->l:LX/15y;

    .line 818574
    sget-object v0, LX/15z;->START_OBJECT:LX/15z;

    iput-object v0, p0, LX/4sE;->K:LX/15z;

    goto/16 :goto_0

    .line 818575
    :sswitch_6
    const-string v1, "Invalid type marker byte 0xFB in value mode (would be END_OBJECT in key mode)"

    invoke-virtual {p0, v1}, LX/15v;->e(Ljava/lang/String;)V

    .line 818576
    :sswitch_7
    iput-boolean v0, p0, LX/4sE;->S:Z

    .line 818577
    sget-object v0, LX/15z;->VALUE_EMBEDDED_OBJECT:LX/15z;

    iput-object v0, p0, LX/4sE;->K:LX/15z;

    goto/16 :goto_0

    .line 818578
    :sswitch_8
    iput-object v2, p0, LX/4sE;->K:LX/15z;

    move-object v0, v2

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x4 -> :sswitch_0
        0x8 -> :sswitch_1
        0xc -> :sswitch_2
        0xd -> :sswitch_2
        0xe -> :sswitch_2
        0xf -> :sswitch_2
        0x18 -> :sswitch_3
        0x19 -> :sswitch_4
        0x1a -> :sswitch_5
        0x1b -> :sswitch_6
        0x1d -> :sswitch_7
        0x1f -> :sswitch_8
    .end sparse-switch
.end method

.method public final c(I)V
    .locals 3

    .prologue
    .line 818494
    iget-boolean v0, p0, LX/4sE;->S:Z

    if-eqz v0, :cond_1

    .line 818495
    iget v0, p0, LX/4sE;->T:I

    .line 818496
    shr-int/lit8 v1, v0, 0x5

    and-int/lit8 v1, v1, 0x7

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    .line 818497
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Current token ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/15v;->K:LX/15z;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") not numeric, can not use numeric value accessors"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/15v;->e(Ljava/lang/String;)V

    .line 818498
    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/4sE;->S:Z

    .line 818499
    invoke-direct {p0, v0}, LX/4sE;->l(I)V

    .line 818500
    :cond_1
    return-void
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 818491
    invoke-super {p0}, LX/15u;->close()V

    .line 818492
    iget-object v0, p0, LX/4sE;->V:LX/0lv;

    invoke-virtual {v0}, LX/0lv;->b()V

    .line 818493
    return-void
.end method

.method public final e()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 818440
    iget-object v0, p0, LX/15u;->l:LX/15y;

    invoke-virtual {v0}, LX/12V;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/15v;->K:LX/15z;

    sget-object v2, LX/15z;->FIELD_NAME:LX/15z;

    if-ne v0, v2, :cond_4

    .line 818441
    :cond_0
    iget-boolean v0, p0, LX/4sE;->S:Z

    if-eqz v0, :cond_1

    .line 818442
    invoke-direct {p0}, LX/4sE;->an()V

    .line 818443
    :cond_1
    iget v0, p0, LX/15u;->d:I

    .line 818444
    iget v2, p0, LX/15u;->e:I

    if-lt v0, v2, :cond_3

    .line 818445
    invoke-virtual {p0}, LX/4sE;->L()Z

    move-result v0

    if-nez v0, :cond_2

    .line 818446
    invoke-virtual {p0}, LX/15u;->P()V

    .line 818447
    invoke-virtual {p0}, LX/15w;->close()V

    .line 818448
    iput-object v1, p0, LX/4sE;->K:LX/15z;

    move-object v0, v1

    .line 818449
    :goto_0
    return-object v0

    .line 818450
    :cond_2
    iget v0, p0, LX/15u;->d:I

    .line 818451
    :cond_3
    iget-object v2, p0, LX/4sE;->Q:[B

    add-int/lit8 v3, v0, 0x1

    aget-byte v0, v2, v0

    .line 818452
    iget-wide v4, p0, LX/15u;->f:J

    iget v2, p0, LX/15u;->d:I

    int-to-long v6, v2

    add-long/2addr v4, v6

    iput-wide v4, p0, LX/4sE;->i:J

    .line 818453
    iput-object v1, p0, LX/4sE;->r:[B

    .line 818454
    iput v0, p0, LX/4sE;->T:I

    .line 818455
    shr-int/lit8 v2, v0, 0x5

    and-int/lit8 v2, v2, 0x7

    packed-switch v2, :pswitch_data_0

    .line 818456
    :cond_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->VALUE_STRING:LX/15z;

    if-ne v0, v2, :cond_b

    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 818457
    :pswitch_0
    if-nez v0, :cond_5

    .line 818458
    const-string v1, "Invalid token byte 0x00"

    invoke-virtual {p0, v1}, LX/15v;->e(Ljava/lang/String;)V

    .line 818459
    :cond_5
    add-int/lit8 v0, v0, -0x1

    .line 818460
    iget v1, p0, LX/4sE;->ac:I

    if-lt v0, v1, :cond_6

    .line 818461
    invoke-direct {p0, v0}, LX/4sE;->u(I)V

    .line 818462
    :cond_6
    iput v3, p0, LX/4sE;->d:I

    .line 818463
    iget-object v1, p0, LX/4sE;->ab:[Ljava/lang/String;

    aget-object v0, v1, v0

    .line 818464
    iget-object v1, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v1, v0}, LX/15x;->a(Ljava/lang/String;)V

    .line 818465
    sget-object v1, LX/15z;->VALUE_STRING:LX/15z;

    iput-object v1, p0, LX/4sE;->K:LX/15z;

    goto :goto_0

    .line 818466
    :pswitch_1
    and-int/lit8 v0, v0, 0x1f

    .line 818467
    if-nez v0, :cond_4

    .line 818468
    iput v3, p0, LX/4sE;->d:I

    .line 818469
    iget-object v0, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v0}, LX/15x;->b()V

    .line 818470
    sget-object v0, LX/15z;->VALUE_STRING:LX/15z;

    iput-object v0, p0, LX/4sE;->K:LX/15z;

    .line 818471
    const-string v0, ""

    goto :goto_0

    .line 818472
    :pswitch_2
    sget-object v1, LX/15z;->VALUE_STRING:LX/15z;

    iput-object v1, p0, LX/4sE;->K:LX/15z;

    .line 818473
    iput v3, p0, LX/4sE;->d:I

    .line 818474
    and-int/lit8 v0, v0, 0x3f

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, LX/4sE;->m(I)V

    .line 818475
    iget v0, p0, LX/4sE;->ac:I

    if-ltz v0, :cond_8

    .line 818476
    iget v0, p0, LX/4sE;->ac:I

    iget-object v1, p0, LX/4sE;->ab:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_7

    .line 818477
    iget-object v0, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v0}, LX/15x;->g()Ljava/lang/String;

    move-result-object v0

    .line 818478
    iget-object v1, p0, LX/4sE;->ab:[Ljava/lang/String;

    iget v2, p0, LX/4sE;->ac:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/4sE;->ac:I

    aput-object v0, v1, v2

    goto/16 :goto_0

    .line 818479
    :cond_7
    invoke-direct {p0}, LX/4sE;->X()V

    .line 818480
    :cond_8
    iget-object v0, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v0}, LX/15x;->g()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 818481
    :pswitch_3
    sget-object v1, LX/15z;->VALUE_STRING:LX/15z;

    iput-object v1, p0, LX/4sE;->K:LX/15z;

    .line 818482
    iput v3, p0, LX/4sE;->d:I

    .line 818483
    and-int/lit8 v0, v0, 0x3f

    add-int/lit8 v0, v0, 0x2

    invoke-direct {p0, v0}, LX/4sE;->n(I)V

    .line 818484
    iget v0, p0, LX/4sE;->ac:I

    if-ltz v0, :cond_a

    .line 818485
    iget v0, p0, LX/4sE;->ac:I

    iget-object v1, p0, LX/4sE;->ab:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_9

    .line 818486
    iget-object v0, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v0}, LX/15x;->g()Ljava/lang/String;

    move-result-object v0

    .line 818487
    iget-object v1, p0, LX/4sE;->ab:[Ljava/lang/String;

    iget v2, p0, LX/4sE;->ac:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LX/4sE;->ac:I

    aput-object v0, v1, v2

    goto/16 :goto_0

    .line 818488
    :cond_9
    invoke-direct {p0}, LX/4sE;->X()V

    .line 818489
    :cond_a
    iget-object v0, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v0}, LX/15x;->g()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_b
    move-object v0, v1

    .line 818490
    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public final k()LX/28G;
    .locals 10

    .prologue
    .line 818437
    new-instance v0, LX/28G;

    iget-object v1, p0, LX/15u;->b:LX/12A;

    .line 818438
    iget-object v2, v1, LX/12A;->a:Ljava/lang/Object;

    move-object v1, v2

    .line 818439
    iget-wide v2, p0, LX/15u;->i:J

    const-wide/16 v4, -0x1

    const/4 v6, -0x1

    iget-wide v8, p0, LX/15u;->i:J

    long-to-int v7, v8

    invoke-direct/range {v0 .. v7}, LX/28G;-><init>(Ljava/lang/Object;JJII)V

    return-object v0
.end method

.method public final l()LX/28G;
    .locals 8

    .prologue
    .line 818357
    iget-wide v0, p0, LX/15u;->f:J

    iget v2, p0, LX/15u;->d:I

    int-to-long v2, v2

    add-long/2addr v2, v0

    .line 818358
    new-instance v0, LX/28G;

    iget-object v1, p0, LX/15u;->b:LX/12A;

    .line 818359
    iget-object v4, v1, LX/12A;->a:Ljava/lang/Object;

    move-object v1, v4

    .line 818360
    const-wide/16 v4, -0x1

    const/4 v6, -0x1

    long-to-int v7, v2

    invoke-direct/range {v0 .. v7}, LX/28G;-><init>(Ljava/lang/Object;JJII)V

    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 3

    .prologue
    .line 818412
    iget-boolean v0, p0, LX/4sE;->S:Z

    if-eqz v0, :cond_4

    .line 818413
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/4sE;->S:Z

    .line 818414
    iget v0, p0, LX/4sE;->T:I

    .line 818415
    shr-int/lit8 v1, v0, 0x5

    and-int/lit8 v1, v1, 0x7

    .line 818416
    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    const/4 v2, 0x3

    if-ne v1, v2, :cond_1

    .line 818417
    :cond_0
    and-int/lit8 v0, v0, 0x3f

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, LX/4sE;->m(I)V

    .line 818418
    iget-object v0, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v0}, LX/15x;->g()Ljava/lang/String;

    move-result-object v0

    .line 818419
    :goto_0
    return-object v0

    .line 818420
    :cond_1
    const/4 v2, 0x4

    if-eq v1, v2, :cond_2

    const/4 v2, 0x5

    if-ne v1, v2, :cond_3

    .line 818421
    :cond_2
    and-int/lit8 v0, v0, 0x3f

    add-int/lit8 v0, v0, 0x2

    invoke-direct {p0, v0}, LX/4sE;->n(I)V

    .line 818422
    iget-object v0, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v0}, LX/15x;->g()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 818423
    :cond_3
    invoke-direct {p0}, LX/4sE;->aa()V

    .line 818424
    :cond_4
    iget-object v0, p0, LX/15v;->K:LX/15z;

    sget-object v1, LX/15z;->VALUE_STRING:LX/15z;

    if-ne v0, v1, :cond_5

    .line 818425
    iget-object v0, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v0}, LX/15x;->g()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 818426
    :cond_5
    iget-object v0, p0, LX/15v;->K:LX/15z;

    .line 818427
    if-nez v0, :cond_6

    .line 818428
    const/4 v0, 0x0

    goto :goto_0

    .line 818429
    :cond_6
    sget-object v1, LX/15z;->FIELD_NAME:LX/15z;

    if-ne v0, v1, :cond_7

    .line 818430
    iget-object v0, p0, LX/15u;->l:LX/15y;

    invoke-virtual {v0}, LX/12V;->h()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 818431
    :cond_7
    invoke-virtual {v0}, LX/15z;->isNumeric()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 818432
    invoke-virtual {p0}, LX/15w;->t()Ljava/lang/Number;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 818433
    :cond_8
    iget-object v0, p0, LX/15v;->K:LX/15z;

    invoke-virtual {v0}, LX/15z;->asString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final p()[C
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 818393
    iget-object v0, p0, LX/15v;->K:LX/15z;

    if-eqz v0, :cond_4

    .line 818394
    iget-boolean v0, p0, LX/4sE;->S:Z

    if-eqz v0, :cond_0

    .line 818395
    invoke-direct {p0}, LX/4sE;->aa()V

    .line 818396
    :cond_0
    sget-object v0, LX/4sC;->a:[I

    iget-object v1, p0, LX/15v;->K:LX/15z;

    invoke-virtual {v1}, LX/15z;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 818397
    iget-object v0, p0, LX/15v;->K:LX/15z;

    invoke-virtual {v0}, LX/15z;->asCharArray()[C

    move-result-object v0

    .line 818398
    :goto_0
    return-object v0

    .line 818399
    :pswitch_0
    iget-object v0, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v0}, LX/15x;->f()[C

    move-result-object v0

    goto :goto_0

    .line 818400
    :pswitch_1
    iget-boolean v0, p0, LX/15u;->p:Z

    if-nez v0, :cond_2

    .line 818401
    iget-object v0, p0, LX/15u;->l:LX/15y;

    invoke-virtual {v0}, LX/12V;->h()Ljava/lang/String;

    move-result-object v0

    .line 818402
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    .line 818403
    iget-object v2, p0, LX/15u;->o:[C

    if-nez v2, :cond_3

    .line 818404
    iget-object v2, p0, LX/15u;->b:LX/12A;

    invoke-virtual {v2, v1}, LX/12A;->a(I)[C

    move-result-object v2

    iput-object v2, p0, LX/4sE;->o:[C

    .line 818405
    :cond_1
    :goto_1
    iget-object v2, p0, LX/15u;->o:[C

    invoke-virtual {v0, v3, v1, v2, v3}, Ljava/lang/String;->getChars(II[CI)V

    .line 818406
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4sE;->p:Z

    .line 818407
    :cond_2
    iget-object v0, p0, LX/15u;->o:[C

    goto :goto_0

    .line 818408
    :cond_3
    iget-object v2, p0, LX/15u;->o:[C

    array-length v2, v2

    if-ge v2, v1, :cond_1

    .line 818409
    new-array v2, v1, [C

    iput-object v2, p0, LX/4sE;->o:[C

    goto :goto_1

    .line 818410
    :pswitch_2
    invoke-virtual {p0}, LX/15w;->t()Ljava/lang/Number;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    goto :goto_0

    .line 818411
    :cond_4
    const/4 v0, 0x0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public final q()I
    .locals 2

    .prologue
    .line 818383
    iget-object v0, p0, LX/15v;->K:LX/15z;

    if-eqz v0, :cond_1

    .line 818384
    iget-boolean v0, p0, LX/4sE;->S:Z

    if-eqz v0, :cond_0

    .line 818385
    invoke-direct {p0}, LX/4sE;->aa()V

    .line 818386
    :cond_0
    sget-object v0, LX/4sC;->a:[I

    iget-object v1, p0, LX/15v;->K:LX/15z;

    invoke-virtual {v1}, LX/15z;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 818387
    iget-object v0, p0, LX/15v;->K:LX/15z;

    invoke-virtual {v0}, LX/15z;->asCharArray()[C

    move-result-object v0

    array-length v0, v0

    .line 818388
    :goto_0
    return v0

    .line 818389
    :pswitch_0
    iget-object v0, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v0}, LX/15x;->c()I

    move-result v0

    goto :goto_0

    .line 818390
    :pswitch_1
    iget-object v0, p0, LX/15u;->l:LX/15y;

    invoke-virtual {v0}, LX/12V;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    goto :goto_0

    .line 818391
    :pswitch_2
    invoke-virtual {p0}, LX/15w;->t()Ljava/lang/Number;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    goto :goto_0

    .line 818392
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public final r()I
    .locals 1

    .prologue
    .line 818382
    const/4 v0, 0x0

    return v0
.end method

.method public final s()Z
    .locals 2

    .prologue
    .line 818376
    iget-object v0, p0, LX/15v;->K:LX/15z;

    sget-object v1, LX/15z;->VALUE_STRING:LX/15z;

    if-ne v0, v1, :cond_0

    .line 818377
    iget-object v0, p0, LX/15u;->n:LX/15x;

    invoke-virtual {v0}, LX/15x;->e()Z

    move-result v0

    .line 818378
    :goto_0
    return v0

    .line 818379
    :cond_0
    iget-object v0, p0, LX/15v;->K:LX/15z;

    sget-object v1, LX/15z;->FIELD_NAME:LX/15z;

    if-ne v0, v1, :cond_1

    .line 818380
    iget-boolean v0, p0, LX/15u;->p:Z

    goto :goto_0

    .line 818381
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final u()LX/16L;
    .locals 1

    .prologue
    .line 818373
    iget-boolean v0, p0, LX/4sE;->U:Z

    if-eqz v0, :cond_0

    .line 818374
    sget-object v0, LX/16L;->FLOAT:LX/16L;

    .line 818375
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, LX/15u;->u()LX/16L;

    move-result-object v0

    goto :goto_0
.end method

.method public final version()LX/0ne;
    .locals 1

    .prologue
    .line 818372
    sget-object v0, LX/4s5;->VERSION:LX/0ne;

    return-object v0
.end method
