.class public final LX/3go;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "LX/0Px",
        "<",
        "Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionsVectorDataQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;",
        ">;",
        "Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionsVectorDataQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel$AnimationModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:I

.field public final synthetic b:LX/1zr;


# direct methods
.method public constructor <init>(LX/1zr;I)V
    .locals 0

    .prologue
    .line 624848
    iput-object p1, p0, LX/3go;->b:LX/1zr;

    iput p2, p0, LX/3go;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 624849
    check-cast p1, LX/0Px;

    const/4 v1, 0x0

    .line 624850
    if-nez p1, :cond_0

    move-object v0, v1

    .line 624851
    :goto_0
    return-object v0

    .line 624852
    :cond_0
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v3, :cond_2

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionsVectorDataQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;

    .line 624853
    invoke-virtual {v0}, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionsVectorDataQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;->j()I

    move-result v4

    iget v5, p0, LX/3go;->a:I

    if-ne v4, v5, :cond_1

    .line 624854
    invoke-virtual {v0}, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionsVectorDataQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;->a()Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionsVectorDataQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel$AnimationModel;

    move-result-object v0

    goto :goto_0

    .line 624855
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 624856
    goto :goto_0
.end method
