.class public LX/4Ov;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 699127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 699121
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 699122
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 699123
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 699124
    invoke-static {p0, p1}, LX/4Ov;->b(LX/15w;LX/186;)I

    move-result v1

    .line 699125
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 699126
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 699115
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 699116
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 699117
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2, p3}, LX/4Ov;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 699118
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 699119
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 699120
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 22

    .prologue
    .line 698998
    const/16 v18, 0x0

    .line 698999
    const/16 v17, 0x0

    .line 699000
    const/16 v16, 0x0

    .line 699001
    const/4 v15, 0x0

    .line 699002
    const/4 v14, 0x0

    .line 699003
    const/4 v13, 0x0

    .line 699004
    const/4 v12, 0x0

    .line 699005
    const/4 v11, 0x0

    .line 699006
    const/4 v10, 0x0

    .line 699007
    const/4 v9, 0x0

    .line 699008
    const/4 v8, 0x0

    .line 699009
    const/4 v7, 0x0

    .line 699010
    const/4 v6, 0x0

    .line 699011
    const/4 v5, 0x0

    .line 699012
    const/4 v4, 0x0

    .line 699013
    const/4 v3, 0x0

    .line 699014
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v19

    sget-object v20, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    if-eq v0, v1, :cond_1

    .line 699015
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 699016
    const/4 v3, 0x0

    .line 699017
    :goto_0
    return v3

    .line 699018
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 699019
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v19

    sget-object v20, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    if-eq v0, v1, :cond_c

    .line 699020
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v19

    .line 699021
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 699022
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v20

    sget-object v21, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    if-eq v0, v1, :cond_1

    if-eqz v19, :cond_1

    .line 699023
    const-string v20, "customized_tokens"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_2

    .line 699024
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v18

    goto :goto_1

    .line 699025
    :cond_2
    const-string v20, "field_key"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_3

    .line 699026
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v17

    goto :goto_1

    .line 699027
    :cond_3
    const-string v20, "input_domain"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_4

    .line 699028
    const/4 v7, 0x1

    .line 699029
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

    move-result-object v16

    goto :goto_1

    .line 699030
    :cond_4
    const-string v20, "input_type"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_5

    .line 699031
    const/4 v6, 0x1

    .line 699032
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    move-result-object v15

    goto :goto_1

    .line 699033
    :cond_5
    const-string v20, "is_custom_type"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_6

    .line 699034
    const/4 v5, 0x1

    .line 699035
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v14

    goto :goto_1

    .line 699036
    :cond_6
    const-string v20, "is_editable"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_7

    .line 699037
    const/4 v4, 0x1

    .line 699038
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v13

    goto/16 :goto_1

    .line 699039
    :cond_7
    const-string v20, "is_required"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_8

    .line 699040
    const/4 v3, 0x1

    .line 699041
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v12

    goto/16 :goto_1

    .line 699042
    :cond_8
    const-string v20, "name"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_9

    .line 699043
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    goto/16 :goto_1

    .line 699044
    :cond_9
    const-string v20, "place_holder"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_a

    .line 699045
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto/16 :goto_1

    .line 699046
    :cond_a
    const-string v20, "values"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_b

    .line 699047
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v9

    goto/16 :goto_1

    .line 699048
    :cond_b
    const-string v20, "validation_spec"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_0

    .line 699049
    invoke-static/range {p0 .. p1}, LX/4P0;->a(LX/15w;LX/186;)I

    move-result v8

    goto/16 :goto_1

    .line 699050
    :cond_c
    const/16 v19, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 699051
    const/16 v19, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 699052
    const/16 v18, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 699053
    if-eqz v7, :cond_d

    .line 699054
    const/4 v7, 0x2

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v7, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 699055
    :cond_d
    if-eqz v6, :cond_e

    .line 699056
    const/4 v6, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v15}, LX/186;->a(ILjava/lang/Enum;)V

    .line 699057
    :cond_e
    if-eqz v5, :cond_f

    .line 699058
    const/4 v5, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v14}, LX/186;->a(IZ)V

    .line 699059
    :cond_f
    if-eqz v4, :cond_10

    .line 699060
    const/4 v4, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v13}, LX/186;->a(IZ)V

    .line 699061
    :cond_10
    if-eqz v3, :cond_11

    .line 699062
    const/4 v3, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v12}, LX/186;->a(IZ)V

    .line 699063
    :cond_11
    const/4 v3, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, LX/186;->b(II)V

    .line 699064
    const/16 v3, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10}, LX/186;->b(II)V

    .line 699065
    const/16 v3, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 699066
    const/16 v3, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 699067
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const/16 v5, 0x9

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 699068
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 699069
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 699070
    if-eqz v0, :cond_0

    .line 699071
    const-string v0, "customized_tokens"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 699072
    invoke-virtual {p0, p1, v2}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 699073
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 699074
    if-eqz v0, :cond_1

    .line 699075
    const-string v1, "field_key"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 699076
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 699077
    :cond_1
    invoke-virtual {p0, p1, v3, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 699078
    if-eqz v0, :cond_2

    .line 699079
    const-string v0, "input_domain"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 699080
    const-class v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

    invoke-virtual {p0, p1, v3, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 699081
    :cond_2
    invoke-virtual {p0, p1, v4, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 699082
    if-eqz v0, :cond_3

    .line 699083
    const-string v0, "input_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 699084
    const-class v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    invoke-virtual {p0, p1, v4, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 699085
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 699086
    if-eqz v0, :cond_4

    .line 699087
    const-string v1, "is_custom_type"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 699088
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 699089
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 699090
    if-eqz v0, :cond_5

    .line 699091
    const-string v1, "is_editable"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 699092
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 699093
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 699094
    if-eqz v0, :cond_6

    .line 699095
    const-string v1, "is_required"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 699096
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 699097
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 699098
    if-eqz v0, :cond_7

    .line 699099
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 699100
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 699101
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 699102
    if-eqz v0, :cond_8

    .line 699103
    const-string v1, "place_holder"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 699104
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 699105
    :cond_8
    invoke-virtual {p0, p1, v5}, LX/15i;->g(II)I

    move-result v0

    .line 699106
    if-eqz v0, :cond_9

    .line 699107
    const-string v0, "values"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 699108
    invoke-virtual {p0, p1, v5}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 699109
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 699110
    if-eqz v0, :cond_a

    .line 699111
    const-string v1, "validation_spec"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 699112
    invoke-static {p0, v0, p2}, LX/4P0;->a(LX/15i;ILX/0nX;)V

    .line 699113
    :cond_a
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 699114
    return-void
.end method
