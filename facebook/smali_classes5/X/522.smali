.class public final LX/522;
.super LX/1vI;
.source ""


# instance fields
.field public final a:J

.field public final b:J

.field public final synthetic c:LX/1vI;


# direct methods
.method public constructor <init>(LX/1vI;JJ)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 825684
    iput-object p1, p0, LX/522;->c:LX/1vI;

    invoke-direct {p0}, LX/1vI;-><init>()V

    .line 825685
    cmp-long v0, p2, v6

    if-ltz v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "offset (%s) may not be negative"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, LX/0PB;->checkArgument(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 825686
    cmp-long v0, p4, v6

    if-ltz v0, :cond_1

    move v0, v1

    :goto_1
    const-string v3, "length (%s) may not be negative"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, LX/0PB;->checkArgument(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 825687
    iput-wide p2, p0, LX/522;->a:J

    .line 825688
    iput-wide p4, p0, LX/522;->b:J

    .line 825689
    return-void

    :cond_0
    move v0, v2

    .line 825690
    goto :goto_0

    :cond_1
    move v0, v2

    .line 825691
    goto :goto_1
.end method

.method private a(Ljava/io/InputStream;)Ljava/io/InputStream;
    .locals 4

    .prologue
    .line 825694
    iget-wide v0, p0, LX/522;->a:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 825695
    :try_start_0
    iget-wide v0, p0, LX/522;->a:J

    invoke-static {p1, v0, v1}, LX/0hW;->c(Ljava/io/InputStream;J)J
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 825696
    iget-wide v2, p0, LX/522;->a:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 825697
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V

    .line 825698
    new-instance v0, Ljava/io/ByteArrayInputStream;

    const/4 v1, 0x0

    new-array v1, v1, [B

    invoke-direct {v0, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 825699
    :goto_0
    return-object v0

    .line 825700
    :catch_0
    move-exception v0

    .line 825701
    invoke-static {}, LX/1vJ;->a()LX/1vJ;

    move-result-object v1

    .line 825702
    invoke-virtual {v1, p1}, LX/1vJ;->a(Ljava/io/Closeable;)Ljava/io/Closeable;

    .line 825703
    :try_start_1
    invoke-virtual {v1, v0}, LX/1vJ;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 825704
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, LX/1vJ;->close()V

    throw v0

    .line 825705
    :cond_0
    iget-wide v0, p0, LX/522;->b:J

    .line 825706
    new-instance v2, LX/524;

    invoke-direct {v2, p1, v0, v1}, LX/524;-><init>(Ljava/io/InputStream;J)V

    move-object v0, v2

    .line 825707
    goto :goto_0
.end method


# virtual methods
.method public final a(JJ)LX/1vI;
    .locals 9

    .prologue
    const-wide/16 v6, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 825708
    cmp-long v0, p1, v6

    if-ltz v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "offset (%s) may not be negative"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, LX/0PB;->checkArgument(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 825709
    cmp-long v0, p3, v6

    if-ltz v0, :cond_1

    move v0, v1

    :goto_1
    const-string v3, "length (%s) may not be negative"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, LX/0PB;->checkArgument(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 825710
    iget-wide v0, p0, LX/522;->b:J

    sub-long/2addr v0, p1

    .line 825711
    iget-object v2, p0, LX/522;->c:LX/1vI;

    iget-wide v4, p0, LX/522;->a:J

    add-long/2addr v4, p1

    invoke-static {p3, p4, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    invoke-virtual {v2, v4, v5, v0, v1}, LX/1vI;->a(JJ)LX/1vI;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v2

    .line 825712
    goto :goto_0

    :cond_1
    move v0, v2

    .line 825713
    goto :goto_1
.end method

.method public final a()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 825693
    iget-object v0, p0, LX/522;->c:LX/1vI;

    invoke-virtual {v0}, LX/1vI;->a()Ljava/io/InputStream;

    move-result-object v0

    invoke-direct {p0, v0}, LX/522;->a(Ljava/io/InputStream;)Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 825692
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, LX/522;->c:LX/1vI;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".slice("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, LX/522;->a:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, LX/522;->b:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
