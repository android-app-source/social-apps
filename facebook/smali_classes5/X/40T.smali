.class public abstract LX/40T;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/4VK;


# direct methods
.method public constructor <init>(LX/4VK;)V
    .locals 0

    .prologue
    .line 663458
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 663459
    iput-object p1, p0, LX/40T;->a:LX/4VK;

    .line 663460
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 663461
    iget-object v0, p0, LX/40T;->a:LX/4VK;

    const/4 p0, 0x0

    const/4 v2, 0x1

    .line 663462
    iget-object v1, v0, LX/4VK;->f:LX/16f;

    if-nez v1, :cond_1

    move v1, v2

    :goto_0
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 663463
    iget-boolean v1, v0, LX/4VK;->h:Z

    if-nez v1, :cond_0

    move p0, v2

    :cond_0
    invoke-static {p0}, LX/0PB;->checkState(Z)V

    .line 663464
    iput-boolean v2, v0, LX/4VK;->g:Z

    .line 663465
    return-void

    :cond_1
    move v1, p0

    .line 663466
    goto :goto_0
.end method

.method public final a(LX/16f;)V
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 663467
    iget-object v0, p0, LX/40T;->a:LX/4VK;

    const/4 v2, 0x1

    const/4 p0, 0x0

    .line 663468
    iget-boolean v1, v0, LX/4VK;->g:Z

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 663469
    iget-boolean v1, v0, LX/4VK;->h:Z

    if-nez v1, :cond_1

    :goto_1
    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 663470
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    iget-object v2, v0, LX/4VK;->d:LX/16f;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string p0, "Model of type "

    invoke-direct {v2, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object p0, v0, LX/4VK;->d:LX/16f;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string p0, " cannot be replaced by "

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 663471
    iput-object p1, v0, LX/4VK;->f:LX/16f;

    .line 663472
    return-void

    :cond_0
    move v1, p0

    .line 663473
    goto :goto_0

    :cond_1
    move v2, p0

    .line 663474
    goto :goto_1
.end method
