.class public LX/4ZX;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Lcom/facebook/graphql/model/GraphQLPhoto;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/facebook/graphql/model/GraphQLPhoto;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 790093
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 790094
    iput-object p1, p0, LX/4ZX;->a:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 790095
    iput-object p2, p0, LX/4ZX;->b:Ljava/lang/String;

    .line 790096
    iput-object p3, p0, LX/4ZX;->c:Ljava/lang/String;

    .line 790097
    return-void
.end method


# virtual methods
.method public final a()LX/162;
    .locals 2

    .prologue
    .line 790098
    new-instance v0, LX/162;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/162;-><init>(LX/0mC;)V

    .line 790099
    iget-object v1, p0, LX/4ZX;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 790100
    iget-object v1, p0, LX/4ZX;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 790101
    :cond_0
    iget-object v1, p0, LX/4ZX;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 790102
    iget-object v1, p0, LX/4ZX;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 790103
    :cond_1
    return-object v0
.end method
