.class public LX/3nM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/26t;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/3nM;


# instance fields
.field public final a:LX/0jU;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/69q;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3iV;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0jU;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0jU;",
            "LX/0Ot",
            "<",
            "LX/69q;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/3iV;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 638464
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 638465
    iput-object p1, p0, LX/3nM;->a:LX/0jU;

    .line 638466
    iput-object p2, p0, LX/3nM;->b:LX/0Ot;

    .line 638467
    iput-object p3, p0, LX/3nM;->c:LX/0Ot;

    .line 638468
    return-void
.end method

.method public static a(LX/0QB;)LX/3nM;
    .locals 6

    .prologue
    .line 638469
    sget-object v0, LX/3nM;->d:LX/3nM;

    if-nez v0, :cond_1

    .line 638470
    const-class v1, LX/3nM;

    monitor-enter v1

    .line 638471
    :try_start_0
    sget-object v0, LX/3nM;->d:LX/3nM;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 638472
    if-eqz v2, :cond_0

    .line 638473
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 638474
    new-instance v4, LX/3nM;

    invoke-static {v0}, LX/0jU;->a(LX/0QB;)LX/0jU;

    move-result-object v3

    check-cast v3, LX/0jU;

    const/16 v5, 0x175b

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 p0, 0x474

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v4, v3, v5, p0}, LX/3nM;-><init>(LX/0jU;LX/0Ot;LX/0Ot;)V

    .line 638475
    move-object v0, v4

    .line 638476
    sput-object v0, LX/3nM;->d:LX/3nM;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 638477
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 638478
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 638479
    :cond_1
    sget-object v0, LX/3nM;->d:LX/3nM;

    return-object v0

    .line 638480
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 638481
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/3nM;Lcom/facebook/saved/server/UpdateSavedStateParams;Lcom/facebook/graphql/enums/GraphQLSavedState;)V
    .locals 5

    .prologue
    .line 638448
    iget-object v0, p1, Lcom/facebook/saved/server/UpdateSavedStateParams;->i:LX/0Px;

    move-object v0, v0

    .line 638449
    if-nez v0, :cond_1

    .line 638450
    :cond_0
    return-void

    .line 638451
    :cond_1
    iget-object v0, p1, Lcom/facebook/saved/server/UpdateSavedStateParams;->i:LX/0Px;

    move-object v3, v0

    .line 638452
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_0

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    .line 638453
    iget-object v0, p1, Lcom/facebook/saved/server/UpdateSavedStateParams;->c:LX/0am;

    move-object v0, v0

    .line 638454
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 638455
    iget-object v0, p0, LX/3nM;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3iV;

    .line 638456
    iget-object v1, p1, Lcom/facebook/saved/server/UpdateSavedStateParams;->c:LX/0am;

    move-object v1, v1

    .line 638457
    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v1, p2}, LX/3iV;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLSavedState;)V

    .line 638458
    :cond_2
    iget-object v0, p1, Lcom/facebook/saved/server/UpdateSavedStateParams;->b:LX/0am;

    move-object v0, v0

    .line 638459
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 638460
    iget-object v0, p0, LX/3nM;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3iV;

    .line 638461
    iget-object v1, p1, Lcom/facebook/saved/server/UpdateSavedStateParams;->b:LX/0am;

    move-object v1, v1

    .line 638462
    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v1, p2}, LX/3iV;->b(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLSavedState;)V

    .line 638463
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0
.end method

.method private static a(LX/3nM;Lcom/facebook/story/UpdateTimelineAppCollectionParams;Lcom/facebook/graphql/enums/GraphQLSavedState;)V
    .locals 9

    .prologue
    .line 638482
    if-eqz p1, :cond_0

    .line 638483
    iget-object v0, p1, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->h:LX/0Px;

    move-object v0, v0

    .line 638484
    if-eqz v0, :cond_0

    .line 638485
    iget-object v0, p1, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->h:LX/0Px;

    move-object v0, v0

    .line 638486
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 638487
    iget-object v0, p1, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->f:Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-object v0, v0

    .line 638488
    if-nez v0, :cond_1

    .line 638489
    :cond_0
    :goto_0
    return-void

    .line 638490
    :cond_1
    iget-object v0, p1, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->f:Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-object v0, v0

    .line 638491
    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    .line 638492
    const v1, -0x72442839

    if-ne v0, v1, :cond_3

    .line 638493
    iget-object v0, p1, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->h:LX/0Px;

    move-object v0, v0

    .line 638494
    if-nez v0, :cond_5

    .line 638495
    :cond_2
    goto :goto_0

    .line 638496
    :cond_3
    const/4 v3, 0x0

    .line 638497
    iget-object v0, p1, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->h:LX/0Px;

    move-object v0, v0

    .line 638498
    if-nez v0, :cond_7

    .line 638499
    :cond_4
    goto :goto_0

    .line 638500
    :cond_5
    iget-object v0, p1, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->h:LX/0Px;

    move-object v2, v0

    .line 638501
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_2

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    .line 638502
    iget-object v0, p1, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->b:Ljava/lang/String;

    move-object v0, v0

    .line 638503
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 638504
    iget-object v0, p0, LX/3nM;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3iV;

    .line 638505
    iget-object v4, p1, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->b:Ljava/lang/String;

    move-object v4, v4

    .line 638506
    invoke-virtual {v0, v4, p2}, LX/3iV;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLSavedState;)V

    .line 638507
    :cond_6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 638508
    :cond_7
    iget-object v0, p1, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->h:LX/0Px;

    move-object v4, v0

    .line 638509
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v2, v3

    :goto_2
    if-ge v2, v5, :cond_4

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 638510
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/CharSequence;

    .line 638511
    iget-object v6, p1, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->i:Ljava/lang/String;

    move-object v6, v6

    .line 638512
    aput-object v6, v1, v3

    const/4 v6, 0x1

    .line 638513
    iget-object v7, p1, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->a:Ljava/lang/String;

    move-object v7, v7

    .line 638514
    aput-object v7, v1, v6

    invoke-static {v1}, LX/0YN;->a([Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 638515
    iget-object v1, p0, LX/3nM;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3iV;

    .line 638516
    iget-object v6, p1, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->i:Ljava/lang/String;

    move-object v6, v6

    .line 638517
    iget-object v7, p1, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->a:Ljava/lang/String;

    move-object v7, v7

    .line 638518
    iget-object v8, p1, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->c:LX/5vL;

    move-object v8, v8

    .line 638519
    invoke-virtual {v1, v0, v6, v7, v8}, LX/3iV;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/5vL;)V

    .line 638520
    :cond_8
    iget-object v0, p1, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->b:Ljava/lang/String;

    move-object v0, v0

    .line 638521
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 638522
    iget-object v0, p0, LX/3nM;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3iV;

    .line 638523
    iget-object v1, p1, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->b:Ljava/lang/String;

    move-object v1, v1

    .line 638524
    invoke-virtual {v0, v1, p2}, LX/3iV;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLSavedState;)V

    .line 638525
    :cond_9
    iget-object v0, p1, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->l:Ljava/lang/String;

    move-object v0, v0

    .line 638526
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 638527
    iget-object v0, p0, LX/3nM;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3iV;

    .line 638528
    iget-object v1, p1, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->l:Ljava/lang/String;

    move-object v1, v1

    .line 638529
    invoke-virtual {v0, v1, p2}, LX/3iV;->b(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLSavedState;)V

    .line 638530
    :cond_a
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2
.end method

.method private b(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 638431
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 638432
    const-string v1, "timelineAppCollectionParamsKey"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/story/UpdateTimelineAppCollectionParams;

    .line 638433
    iget-object v1, v0, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->c:LX/5vL;

    move-object v1, v1

    .line 638434
    sget-object v2, LX/5vL;->ADD:LX/5vL;

    if-ne v1, v2, :cond_1

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 638435
    :goto_0
    iget-object v2, v0, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->c:LX/5vL;

    move-object v2, v2

    .line 638436
    sget-object v3, LX/5vL;->ADD:LX/5vL;

    if-ne v2, v3, :cond_2

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSavedState;->NOT_SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 638437
    :goto_1
    :try_start_0
    invoke-static {p0, v0, v1}, LX/3nM;->a(LX/3nM;Lcom/facebook/story/UpdateTimelineAppCollectionParams;Lcom/facebook/graphql/enums/GraphQLSavedState;)V

    .line 638438
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v1

    .line 638439
    iget-boolean v3, v1, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    move v3, v3

    .line 638440
    if-nez v3, :cond_0

    .line 638441
    invoke-static {p0, v0, v2}, LX/3nM;->a(LX/3nM;Lcom/facebook/story/UpdateTimelineAppCollectionParams;Lcom/facebook/graphql/enums/GraphQLSavedState;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 638442
    :cond_0
    return-object v1

    .line 638443
    :cond_1
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSavedState;->NOT_SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    goto :goto_0

    .line 638444
    :cond_2
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    goto :goto_1

    .line 638445
    :catch_0
    move-exception v1

    .line 638446
    invoke-static {p0, v0, v2}, LX/3nM;->a(LX/3nM;Lcom/facebook/story/UpdateTimelineAppCollectionParams;Lcom/facebook/graphql/enums/GraphQLSavedState;)V

    .line 638447
    throw v1
.end method

.method private c(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 5

    .prologue
    .line 638189
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 638190
    const-string v1, "togglePlaceParamsKey"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/feedcache/memory/ToggleSaveParams;

    .line 638191
    iget-object v1, p0, LX/3nM;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3iV;

    iget-object v3, v0, Lcom/facebook/api/feedcache/memory/ToggleSaveParams;->b:Ljava/lang/String;

    iget-boolean v2, v0, Lcom/facebook/api/feedcache/memory/ToggleSaveParams;->e:Z

    if-eqz v2, :cond_1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    :goto_0
    invoke-virtual {v1, v3, v2}, LX/3iV;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLSavedState;)V

    .line 638192
    :try_start_0
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v3

    .line 638193
    iget-boolean v1, v3, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    move v1, v1

    .line 638194
    if-nez v1, :cond_0

    .line 638195
    iget-object v1, p0, LX/3nM;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3iV;

    iget-object v4, v0, Lcom/facebook/api/feedcache/memory/ToggleSaveParams;->b:Ljava/lang/String;

    iget-boolean v2, v0, Lcom/facebook/api/feedcache/memory/ToggleSaveParams;->e:Z

    if-eqz v2, :cond_2

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSavedState;->NOT_SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    :goto_1
    invoke-virtual {v1, v4, v2}, LX/3iV;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLSavedState;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 638196
    :cond_0
    return-object v3

    .line 638197
    :cond_1
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSavedState;->NOT_SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    goto :goto_0

    .line 638198
    :cond_2
    :try_start_1
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 638199
    :catch_0
    move-exception v1

    move-object v2, v1

    .line 638200
    iget-object v1, p0, LX/3nM;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3iV;

    iget-object v3, v0, Lcom/facebook/api/feedcache/memory/ToggleSaveParams;->b:Ljava/lang/String;

    iget-boolean v0, v0, Lcom/facebook/api/feedcache/memory/ToggleSaveParams;->e:Z

    if-eqz v0, :cond_3

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSavedState;->NOT_SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    :goto_2
    invoke-virtual {v1, v3, v0}, LX/3iV;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLSavedState;)V

    .line 638201
    throw v2

    .line 638202
    :cond_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    goto :goto_2
.end method


# virtual methods
.method public final handleOperation(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 9

    .prologue
    .line 638203
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 638204
    invoke-static {v0}, LX/3nN;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 638205
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v1, v0

    .line 638206
    const-string v0, "fetchFeedParams"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/feed/FetchFeedParams;

    .line 638207
    if-nez v0, :cond_1d

    .line 638208
    sget-object v0, LX/1nY;->OTHER:LX/1nY;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid params "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 638209
    :goto_0
    move-object v0, v0

    .line 638210
    :goto_1
    return-object v0

    .line 638211
    :cond_0
    const-string v1, "feed_hide_story"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 638212
    const/4 v6, 0x0

    .line 638213
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 638214
    const-string v1, "hideFeedStoryParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/feed/HideFeedStoryMethod$Params;

    .line 638215
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v2

    .line 638216
    iget-boolean v1, v2, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    move v1, v1

    .line 638217
    if-eqz v1, :cond_2

    .line 638218
    iget-object v1, v0, Lcom/facebook/api/feed/HideFeedStoryMethod$Params;->f:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 638219
    iget-object v3, v0, Lcom/facebook/api/feed/HideFeedStoryMethod$Params;->d:Lcom/facebook/graphql/enums/StoryVisibility;

    .line 638220
    iget-object v1, p0, LX/3nM;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3iV;

    iget-object v4, v0, Lcom/facebook/api/feed/HideFeedStoryMethod$Params;->f:Ljava/lang/String;

    iget v5, v0, Lcom/facebook/api/feed/HideFeedStoryMethod$Params;->g:I

    invoke-virtual {v1, v4, v3, v5}, LX/3iV;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/StoryVisibility;I)V

    .line 638221
    iget-object v1, v0, Lcom/facebook/api/feed/HideFeedStoryMethod$Params;->h:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 638222
    iget-object v1, p0, LX/3nM;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3iV;

    iget-object v4, v0, Lcom/facebook/api/feed/HideFeedStoryMethod$Params;->h:Ljava/lang/String;

    iget v5, v0, Lcom/facebook/api/feed/HideFeedStoryMethod$Params;->g:I

    invoke-virtual {v1, v4, v3, v5}, LX/3iV;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/StoryVisibility;I)V

    .line 638223
    :cond_1
    invoke-virtual {v3}, Lcom/facebook/graphql/enums/StoryVisibility;->isHiddenOrVisible()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 638224
    iget-object v1, p0, LX/3nM;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3iV;

    iget-object v3, v0, Lcom/facebook/api/feed/HideFeedStoryMethod$Params;->f:Ljava/lang/String;

    invoke-virtual {v1, v3, v6}, LX/3iV;->a(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;)V

    .line 638225
    iget-object v1, v0, Lcom/facebook/api/feed/HideFeedStoryMethod$Params;->h:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 638226
    iget-object v1, p0, LX/3nM;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3iV;

    iget-object v0, v0, Lcom/facebook/api/feed/HideFeedStoryMethod$Params;->h:Ljava/lang/String;

    invoke-virtual {v1, v0, v6}, LX/3iV;->a(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;)V

    .line 638227
    :cond_2
    move-object v0, v2

    .line 638228
    goto :goto_1

    .line 638229
    :cond_3
    const-string v1, "feed_negative_feedback_story"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 638230
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 638231
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 638232
    const-string v1, "negativeFeedbackActionOnFeedParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/feed/NegativeFeedbackActionOnFeedMethod$Params;

    .line 638233
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v6

    .line 638234
    iget-boolean v1, v6, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    move v1, v1

    .line 638235
    if-eqz v1, :cond_5

    .line 638236
    iget-object v1, v0, Lcom/facebook/api/feed/NegativeFeedbackActionOnFeedMethod$Params;->e:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 638237
    iget-boolean v7, v0, Lcom/facebook/api/feed/NegativeFeedbackActionOnFeedMethod$Params;->i:Z

    .line 638238
    iget-object v1, v0, Lcom/facebook/api/feed/NegativeFeedbackActionOnFeedMethod$Params;->g:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->b()Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->DONT_LIKE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    invoke-virtual {v1, v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 638239
    iget-object v1, p0, LX/3nM;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3iV;

    iget-object v8, v0, Lcom/facebook/api/feed/NegativeFeedbackActionOnFeedMethod$Params;->e:Ljava/lang/String;

    if-eqz v7, :cond_20

    sget-object v2, Lcom/facebook/graphql/enums/StoryVisibility;->VISIBLE:Lcom/facebook/graphql/enums/StoryVisibility;

    move-object v5, v2

    :goto_2
    if-eqz v7, :cond_21

    move v2, v3

    :goto_3
    invoke-virtual {v1, v8, v5, v2}, LX/3iV;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/StoryVisibility;I)V

    .line 638240
    iget-object v1, v0, Lcom/facebook/api/feed/NegativeFeedbackActionOnFeedMethod$Params;->c:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 638241
    iget-object v1, p0, LX/3nM;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3iV;

    iget-object v5, v0, Lcom/facebook/api/feed/NegativeFeedbackActionOnFeedMethod$Params;->c:Ljava/lang/String;

    if-eqz v7, :cond_22

    sget-object v2, Lcom/facebook/graphql/enums/StoryVisibility;->VISIBLE:Lcom/facebook/graphql/enums/StoryVisibility;

    :goto_4
    if-eqz v7, :cond_23

    :goto_5
    invoke-virtual {v1, v5, v2, v3}, LX/3iV;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/StoryVisibility;I)V

    .line 638242
    :cond_4
    iget-object v1, p0, LX/3nM;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3iV;

    iget-object v3, v0, Lcom/facebook/api/feed/NegativeFeedbackActionOnFeedMethod$Params;->e:Ljava/lang/String;

    if-eqz v7, :cond_24

    move-object v2, v4

    :goto_6
    invoke-virtual {v1, v3, v2}, LX/3iV;->a(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;)V

    .line 638243
    iget-object v1, v0, Lcom/facebook/api/feed/NegativeFeedbackActionOnFeedMethod$Params;->c:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 638244
    iget-object v1, p0, LX/3nM;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3iV;

    iget-object v2, v0, Lcom/facebook/api/feed/NegativeFeedbackActionOnFeedMethod$Params;->c:Ljava/lang/String;

    if-eqz v7, :cond_25

    :goto_7
    invoke-virtual {v1, v2, v4}, LX/3iV;->a(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;)V

    .line 638245
    :cond_5
    move-object v0, v6

    .line 638246
    goto/16 :goto_1

    .line 638247
    :cond_6
    const-string v1, "feed_delete_story"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 638248
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v1, v0

    .line 638249
    const-string v0, "deleteStoryParams"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/feed/DeleteStoryMethod$Params;

    .line 638250
    iget-object v2, v0, Lcom/facebook/api/feed/DeleteStoryMethod$Params;->c:Ljava/lang/String;

    if-eqz v2, :cond_7

    .line 638251
    iget-object v2, p0, LX/3nM;->a:LX/0jU;

    iget-object v3, v0, Lcom/facebook/api/feed/DeleteStoryMethod$Params;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/0jU;->c(Ljava/lang/String;)LX/0Px;

    move-result-object v2

    .line 638252
    new-instance v3, Lcom/facebook/api/feed/DeleteStoryMethod$Params;

    iget-object v4, v0, Lcom/facebook/api/feed/DeleteStoryMethod$Params;->a:Ljava/lang/String;

    iget-object v5, v0, Lcom/facebook/api/feed/DeleteStoryMethod$Params;->c:Ljava/lang/String;

    iget-object v6, v0, Lcom/facebook/api/feed/DeleteStoryMethod$Params;->d:LX/55G;

    invoke-direct {v3, v4, v2, v5, v6}, Lcom/facebook/api/feed/DeleteStoryMethod$Params;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;LX/55G;)V

    .line 638253
    const-string v2, "deleteStoryParams"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 638254
    :cond_7
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v1

    .line 638255
    iget-boolean v2, v1, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    move v2, v2

    .line 638256
    if-eqz v2, :cond_8

    .line 638257
    iget-object v0, v0, Lcom/facebook/api/feed/DeleteStoryMethod$Params;->c:Ljava/lang/String;

    .line 638258
    if-eqz v0, :cond_8

    .line 638259
    iget-object v2, p0, LX/3nM;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/3iV;

    sget-object v3, Lcom/facebook/graphql/enums/StoryVisibility;->GONE:Lcom/facebook/graphql/enums/StoryVisibility;

    const/4 v4, 0x0

    invoke-virtual {v2, v0, v3, v4}, LX/3iV;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/StoryVisibility;I)V

    .line 638260
    :cond_8
    move-object v0, v1

    .line 638261
    goto/16 :goto_1

    .line 638262
    :cond_9
    const-string v1, "feed_edit_privacy"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 638263
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 638264
    const-string v1, "editPrivacyFeedStoryParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/protocol/EditFeedStoryPrivacyParams;

    .line 638265
    const/4 v1, 0x0

    .line 638266
    iget-object v2, v0, Lcom/facebook/privacy/protocol/EditFeedStoryPrivacyParams;->a:Ljava/lang/String;

    if-eqz v2, :cond_26

    .line 638267
    iget-object v1, v0, Lcom/facebook/privacy/protocol/EditFeedStoryPrivacyParams;->a:Ljava/lang/String;

    move-object v2, v1

    .line 638268
    :goto_8
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v3

    .line 638269
    iget-boolean v1, v3, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    move v1, v1

    .line 638270
    if-eqz v1, :cond_a

    .line 638271
    iget-object v1, p0, LX/3nM;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3iV;

    .line 638272
    new-instance v4, LX/4YJ;

    invoke-direct {v4}, LX/4YJ;-><init>()V

    iget-object p0, v0, Lcom/facebook/privacy/protocol/EditFeedStoryPrivacyParams;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 638273
    const/4 p1, 0x1

    .line 638274
    iput-boolean p1, v4, LX/4YJ;->b:Z

    .line 638275
    move-object p1, v4

    .line 638276
    iput-object p0, p1, LX/4YJ;->c:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 638277
    move-object p1, p1

    .line 638278
    sget-object p2, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;->SELECTED:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    .line 638279
    iput-object p2, p1, LX/4YJ;->d:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    .line 638280
    move-object p1, p1

    .line 638281
    move-object v4, p1

    .line 638282
    invoke-virtual {v4}, LX/4YJ;->a()Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentEdge;

    move-result-object v4

    .line 638283
    new-instance p0, LX/4YI;

    invoke-direct {p0}, LX/4YI;-><init>()V

    invoke-static {v4}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    .line 638284
    iput-object v4, p0, LX/4YI;->b:LX/0Px;

    .line 638285
    move-object v4, p0

    .line 638286
    invoke-virtual {v4}, LX/4YI;->a()Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;

    move-result-object v4

    .line 638287
    new-instance p0, LX/4YL;

    invoke-direct {p0}, LX/4YL;-><init>()V

    const/4 p1, 0x1

    .line 638288
    iput-boolean p1, p0, LX/4YL;->b:Z

    .line 638289
    move-object p0, p0

    .line 638290
    iget-object p1, v0, Lcom/facebook/privacy/protocol/EditFeedStoryPrivacyParams;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLImage;->d()Ljava/lang/String;

    move-result-object p1

    .line 638291
    iput-object p1, p0, LX/4YL;->m:Ljava/lang/String;

    .line 638292
    move-object p0, p0

    .line 638293
    iget-object p1, v0, Lcom/facebook/privacy/protocol/EditFeedStoryPrivacyParams;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->d()Ljava/lang/String;

    move-result-object p1

    .line 638294
    iput-object p1, p0, LX/4YL;->h:Ljava/lang/String;

    .line 638295
    move-object p0, p0

    .line 638296
    iput-object v4, p0, LX/4YL;->j:Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;

    .line 638297
    move-object v4, p0

    .line 638298
    invoke-virtual {v4}, LX/4YL;->a()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v4

    move-object v0, v4

    .line 638299
    iget-object v4, v1, LX/3iV;->j:LX/3iW;

    .line 638300
    new-instance v5, LX/4VL;

    iget-object v6, v4, LX/3iW;->c:LX/0Uh;

    const/4 p0, 0x1

    new-array p0, p0, [LX/4VT;

    const/4 p1, 0x0

    new-instance p2, LX/6Pd;

    invoke-direct {p2, v2, v0}, LX/6Pd;-><init>(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLPrivacyScope;)V

    aput-object p2, p0, p1

    invoke-direct {v5, v6, p0}, LX/4VL;-><init>(LX/0Uh;[LX/4VT;)V

    move-object v4, v5

    .line 638301
    invoke-static {v1, v4}, LX/3iV;->a(LX/3iV;LX/3Bq;)V

    .line 638302
    :cond_a
    move-object v0, v3

    .line 638303
    goto/16 :goto_1

    .line 638304
    :cond_b
    const-string v1, "feed_clear_cache"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 638305
    iget-object v0, p0, LX/3nM;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/69q;

    invoke-virtual {v0}, LX/69q;->clearUserData()V

    .line 638306
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    move-object v0, v0

    .line 638307
    goto/16 :goto_1

    .line 638308
    :cond_c
    const-string v1, "feed_trim_cache"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 638309
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 638310
    const-string v1, "trimCacheParamIdsToKeep"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 638311
    iget-object v1, p0, LX/3nM;->a:LX/0jU;

    invoke-virtual {v1, v0}, LX/0jU;->a(Ljava/util/ArrayList;)V

    .line 638312
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 638313
    move-object v0, v0

    .line 638314
    goto/16 :goto_1

    .line 638315
    :cond_d
    const-string v1, "feed_mark_impression_logged"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 638316
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 638317
    const-string v1, "markImpressionLoggedParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/feed/MarkImpressionsLoggedParams;

    .line 638318
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v1

    .line 638319
    iget-boolean v2, v1, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    move v2, v2

    .line 638320
    if-nez v2, :cond_29

    move-object v0, v1

    .line 638321
    :goto_9
    move-object v0, v0

    .line 638322
    goto/16 :goto_1

    .line 638323
    :cond_e
    const-string v1, "publish_edit_post"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 638324
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v1, v0

    .line 638325
    const-string v0, "publishEditPostParamsKey"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/common/EditPostParams;

    .line 638326
    iget-object v2, p0, LX/3nM;->a:LX/0jU;

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/EditPostParams;->getStoryId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0jU;->c(Ljava/lang/String;)LX/0Px;

    move-result-object v2

    .line 638327
    invoke-static {v0}, Lcom/facebook/composer/publish/common/EditPostParams;->a(Lcom/facebook/composer/publish/common/EditPostParams;)Lcom/facebook/composer/publish/common/EditPostParams$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->setCacheIds(LX/0Px;)Lcom/facebook/composer/publish/common/EditPostParams$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->a()Lcom/facebook/composer/publish/common/EditPostParams;

    move-result-object v2

    .line 638328
    const-string v3, "publishEditPostParamsKey"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 638329
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v2

    .line 638330
    iget-boolean v1, v2, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    move v1, v1

    .line 638331
    if-eqz v1, :cond_f

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/EditPostParams;->getStoryId()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_f

    .line 638332
    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/EditPostParams;->getProductItemAttachment()Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    move-result-object v1

    if-eqz v1, :cond_2a

    .line 638333
    iget-object v1, p0, LX/3nM;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3iV;

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/EditPostParams;->getStoryId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/EditPostParams;->getProductItemAttachment()Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, LX/3iV;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/model/ProductItemAttachment;)V

    .line 638334
    :cond_f
    :goto_a
    move-object v0, v2

    .line 638335
    goto/16 :goto_1

    .line 638336
    :cond_10
    const-string v1, "feed_mark_survey_completed"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 638337
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 638338
    const-string v1, "markSurveyCompletedParamsKey"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/feed/MarkSurveyCompletedParams;

    .line 638339
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v1

    .line 638340
    iget-boolean v2, v1, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    move v2, v2

    .line 638341
    if-eqz v2, :cond_11

    iget-object v2, v0, Lcom/facebook/api/feed/MarkSurveyCompletedParams;->a:Ljava/lang/String;

    if-eqz v2, :cond_11

    .line 638342
    iget-object v2, p0, LX/3nM;->a:LX/0jU;

    iget-object v0, v0, Lcom/facebook/api/feed/MarkSurveyCompletedParams;->a:Ljava/lang/String;

    invoke-virtual {v2, v0}, LX/0jU;->f(Ljava/lang/String;)V

    .line 638343
    :cond_11
    move-object v0, v1

    .line 638344
    goto/16 :goto_1

    .line 638345
    :cond_12
    const-string v1, "feed_mark_research_poll_completed"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_14

    .line 638346
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 638347
    const-string v1, "markResearchPollCompletedParamsKey"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/feed/MarkResearchPollCompletedParams;

    .line 638348
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v1

    .line 638349
    iget-boolean v2, v1, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    move v2, v2

    .line 638350
    if-eqz v2, :cond_13

    iget-object v2, v0, Lcom/facebook/api/feed/MarkResearchPollCompletedParams;->a:Ljava/lang/String;

    if-eqz v2, :cond_13

    .line 638351
    iget-object v2, p0, LX/3nM;->a:LX/0jU;

    iget-object v0, v0, Lcom/facebook/api/feed/MarkResearchPollCompletedParams;->a:Ljava/lang/String;

    invoke-virtual {v2, v0}, LX/0jU;->f(Ljava/lang/String;)V

    .line 638352
    :cond_13
    move-object v0, v1

    .line 638353
    goto/16 :goto_1

    .line 638354
    :cond_14
    const-string v1, "set_hscroll_unit_visible_item_index"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_16

    .line 638355
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 638356
    const-string v1, "setHScrollUnitVisibleItemIndexKey"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/feed/SetHScrollUnitVisibleItemIndexParams;

    .line 638357
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v1

    .line 638358
    iget-boolean v2, v1, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    move v2, v2

    .line 638359
    if-eqz v2, :cond_15

    iget-object v2, v0, Lcom/facebook/api/feed/SetHScrollUnitVisibleItemIndexParams;->a:Ljava/lang/String;

    if-eqz v2, :cond_15

    .line 638360
    iget-object v2, p0, LX/3nM;->a:LX/0jU;

    iget-object v3, v0, Lcom/facebook/api/feed/SetHScrollUnitVisibleItemIndexParams;->a:Ljava/lang/String;

    iget-object v0, v0, Lcom/facebook/api/feed/SetHScrollUnitVisibleItemIndexParams;->c:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v2, v3, v0}, LX/0jU;->a(Ljava/lang/String;I)V

    .line 638361
    :cond_15
    move-object v0, v1

    .line 638362
    goto/16 :goto_1

    .line 638363
    :cond_16
    const-string v1, "toggle_save_place"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_17

    .line 638364
    invoke-direct {p0, p1, p2}, LX/3nM;->c(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto/16 :goto_1

    .line 638365
    :cond_17
    const-string v1, "update_story_saved_state"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_18

    .line 638366
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 638367
    if-nez v0, :cond_2b

    .line 638368
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 638369
    :goto_b
    move-object v0, v0

    .line 638370
    goto/16 :goto_1

    .line 638371
    :cond_18
    const-string v1, "update_timeline_app_collection_in_newsfeed"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_19

    .line 638372
    invoke-direct {p0, p1, p2}, LX/3nM;->b(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto/16 :goto_1

    .line 638373
    :cond_19
    const-string v1, "feed_fetch_followup_feed_unit"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1b

    .line 638374
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v2

    .line 638375
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 638376
    const-string v1, "fetchFollowUpFeedUnitParamsKey"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/feed/FetchFollowUpFeedUnitParams;

    .line 638377
    iget-object v1, v0, Lcom/facebook/api/feed/FetchFollowUpFeedUnitParams;->c:Ljava/lang/String;

    move-object v3, v1

    .line 638378
    iget-boolean v0, v2, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    move v0, v0

    .line 638379
    if-eqz v0, :cond_31

    invoke-virtual {v2}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    move-object v1, v0

    .line 638380
    :goto_c
    if-eqz v1, :cond_1a

    if-eqz v3, :cond_1a

    .line 638381
    iget-object v0, p0, LX/3nM;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3iV;

    invoke-virtual {v0, v3, v1}, LX/3iV;->a(Ljava/lang/String;Lcom/facebook/graphql/model/FeedUnit;)V

    .line 638382
    :cond_1a
    move-object v0, v2

    .line 638383
    goto/16 :goto_1

    .line 638384
    :cond_1b
    const-string v1, "xOutPlaceReviewItem"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 638385
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 638386
    const-string v1, "xOutPlaceReviewItemParamKey"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/feedcache/memory/XOutPlaceReviewItemParams;

    .line 638387
    iget-object v1, p0, LX/3nM;->a:LX/0jU;

    iget-object v2, v0, Lcom/facebook/api/feedcache/memory/XOutPlaceReviewItemParams;->a:Ljava/lang/String;

    iget-object v0, v0, Lcom/facebook/api/feedcache/memory/XOutPlaceReviewItemParams;->c:LX/0Px;

    invoke-virtual {v1, v2, v0}, LX/0jU;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 638388
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    move-object v0, v0

    .line 638389
    goto/16 :goto_1

    .line 638390
    :cond_1c
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto/16 :goto_1

    .line 638391
    :cond_1d
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v1

    .line 638392
    iget-boolean v2, v1, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    move v2, v2

    .line 638393
    if-nez v2, :cond_1e

    move-object v0, v1

    .line 638394
    goto/16 :goto_0

    .line 638395
    :cond_1e
    invoke-virtual {v1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/feed/FetchFeedResult;

    .line 638396
    if-eqz v1, :cond_1f

    .line 638397
    iget-object v2, v0, Lcom/facebook/api/feed/FetchFeedParams;->b:Lcom/facebook/api/feedtype/FeedType;

    move-object v0, v2

    .line 638398
    invoke-virtual {v0}, Lcom/facebook/api/feedtype/FeedType;->e()LX/0pL;

    move-result-object v0

    sget-object v2, LX/0pL;->NO_CACHE:LX/0pL;

    if-eq v0, v2, :cond_1f

    .line 638399
    iget-object v0, p0, LX/3nM;->a:LX/0jU;

    invoke-virtual {v0, v1}, LX/0jU;->a(Lcom/facebook/api/feed/FetchFeedResult;)V

    .line 638400
    :cond_1f
    invoke-static {v1}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto/16 :goto_0

    .line 638401
    :cond_20
    sget-object v2, Lcom/facebook/graphql/enums/StoryVisibility;->HIDDEN:Lcom/facebook/graphql/enums/StoryVisibility;

    move-object v5, v2

    goto/16 :goto_2

    :cond_21
    iget v2, v0, Lcom/facebook/api/feed/NegativeFeedbackActionOnFeedMethod$Params;->f:I

    goto/16 :goto_3

    .line 638402
    :cond_22
    sget-object v2, Lcom/facebook/graphql/enums/StoryVisibility;->HIDDEN:Lcom/facebook/graphql/enums/StoryVisibility;

    goto/16 :goto_4

    :cond_23
    iget v3, v0, Lcom/facebook/api/feed/NegativeFeedbackActionOnFeedMethod$Params;->f:I

    goto/16 :goto_5

    .line 638403
    :cond_24
    iget-object v2, v0, Lcom/facebook/api/feed/NegativeFeedbackActionOnFeedMethod$Params;->g:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;

    goto/16 :goto_6

    .line 638404
    :cond_25
    iget-object v4, v0, Lcom/facebook/api/feed/NegativeFeedbackActionOnFeedMethod$Params;->g:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;

    goto/16 :goto_7

    .line 638405
    :cond_26
    iget-object v2, v0, Lcom/facebook/privacy/protocol/EditFeedStoryPrivacyParams;->c:Ljava/lang/String;

    if-eqz v2, :cond_27

    .line 638406
    iget-object v1, v0, Lcom/facebook/privacy/protocol/EditFeedStoryPrivacyParams;->c:Ljava/lang/String;

    move-object v2, v1

    goto/16 :goto_8

    .line 638407
    :cond_27
    iget-object v2, v0, Lcom/facebook/privacy/protocol/EditFeedStoryPrivacyParams;->b:Ljava/lang/String;

    if-eqz v2, :cond_28

    .line 638408
    iget-object v1, v0, Lcom/facebook/privacy/protocol/EditFeedStoryPrivacyParams;->b:Ljava/lang/String;

    move-object v2, v1

    goto/16 :goto_8

    :cond_28
    move-object v2, v1

    goto/16 :goto_8

    .line 638409
    :cond_29
    iget-object v2, p0, LX/3nM;->a:LX/0jU;

    iget-object v0, v0, Lcom/facebook/api/feed/MarkImpressionsLoggedParams;->a:Ljava/lang/String;

    invoke-virtual {v2, v0}, LX/0jU;->e(Ljava/lang/String;)V

    move-object v0, v1

    .line 638410
    goto/16 :goto_9

    .line 638411
    :cond_2a
    iget-object v1, p0, LX/3nM;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3iV;

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/EditPostParams;->getStoryId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1, v3, v0}, LX/3iV;->a(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)V

    goto/16 :goto_a

    .line 638412
    :cond_2b
    const-string v1, "updateStorySavedStateParamsKey"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/saved/server/UpdateSavedStateParams;

    .line 638413
    iget-object v1, v0, Lcom/facebook/saved/server/UpdateSavedStateParams;->i:LX/0Px;

    move-object v1, v1

    .line 638414
    if-eqz v1, :cond_2c

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2d

    .line 638415
    :cond_2c
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto/16 :goto_b

    .line 638416
    :cond_2d
    iget-object v1, v0, Lcom/facebook/saved/server/UpdateSavedStateParams;->f:LX/5uo;

    move-object v1, v1

    .line 638417
    sget-object v2, LX/5uo;->SAVE:LX/5uo;

    if-ne v1, v2, :cond_2f

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 638418
    :goto_d
    iget-object v2, v0, Lcom/facebook/saved/server/UpdateSavedStateParams;->f:LX/5uo;

    move-object v2, v2

    .line 638419
    sget-object v3, LX/5uo;->SAVE:LX/5uo;

    if-ne v2, v3, :cond_30

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSavedState;->NOT_SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 638420
    :goto_e
    invoke-static {p0, v0, v1}, LX/3nM;->a(LX/3nM;Lcom/facebook/saved/server/UpdateSavedStateParams;Lcom/facebook/graphql/enums/GraphQLSavedState;)V

    .line 638421
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v1

    .line 638422
    iget-boolean v3, v1, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    move v3, v3

    .line 638423
    if-nez v3, :cond_2e

    .line 638424
    iget-object v3, v1, Lcom/facebook/fbservice/service/OperationResult;->errorCode:LX/1nY;

    move-object v3, v3

    .line 638425
    sget-object v4, LX/1nY;->CONNECTION_FAILURE:LX/1nY;

    if-eq v3, v4, :cond_2e

    .line 638426
    invoke-static {p0, v0, v2}, LX/3nM;->a(LX/3nM;Lcom/facebook/saved/server/UpdateSavedStateParams;Lcom/facebook/graphql/enums/GraphQLSavedState;)V

    :cond_2e
    move-object v0, v1

    .line 638427
    goto/16 :goto_b

    .line 638428
    :cond_2f
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSavedState;->NOT_SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    goto :goto_d

    .line 638429
    :cond_30
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    goto :goto_e

    .line 638430
    :cond_31
    const/4 v0, 0x0

    move-object v1, v0

    goto/16 :goto_c
.end method
