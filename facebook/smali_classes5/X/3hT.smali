.class public LX/3hT;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/user/model/UserKey;

.field public final b:Z

.field public final c:Z


# direct methods
.method public constructor <init>(Lcom/facebook/user/model/UserKey;ZZ)V
    .locals 0

    .prologue
    .line 626813
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 626814
    iput-object p1, p0, LX/3hT;->a:Lcom/facebook/user/model/UserKey;

    .line 626815
    iput-boolean p2, p0, LX/3hT;->b:Z

    .line 626816
    iput-boolean p3, p0, LX/3hT;->c:Z

    .line 626817
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 626825
    if-ne p0, p1, :cond_1

    .line 626826
    :cond_0
    :goto_0
    return v0

    .line 626827
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    .line 626828
    :cond_3
    check-cast p1, LX/3hT;

    .line 626829
    iget-boolean v2, p0, LX/3hT;->b:Z

    iget-boolean v3, p1, LX/3hT;->b:Z

    if-eq v2, v3, :cond_4

    move v0, v1

    goto :goto_0

    .line 626830
    :cond_4
    iget-object v2, p0, LX/3hT;->a:Lcom/facebook/user/model/UserKey;

    if-eqz v2, :cond_6

    iget-object v2, p0, LX/3hT;->a:Lcom/facebook/user/model/UserKey;

    iget-object v3, p1, LX/3hT;->a:Lcom/facebook/user/model/UserKey;

    invoke-virtual {v2, v3}, Lcom/facebook/user/model/UserKey;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    :cond_5
    move v0, v1

    goto :goto_0

    :cond_6
    iget-object v2, p1, LX/3hT;->a:Lcom/facebook/user/model/UserKey;

    if-nez v2, :cond_5

    .line 626831
    :cond_7
    iget-boolean v2, p0, LX/3hT;->c:Z

    iget-boolean v3, p1, LX/3hT;->c:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 626818
    iget-object v0, p0, LX/3hT;->a:Lcom/facebook/user/model/UserKey;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3hT;->a:Lcom/facebook/user/model/UserKey;

    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->hashCode()I

    move-result v0

    .line 626819
    :goto_0
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, LX/3hT;->b:Z

    if-eqz v0, :cond_1

    move v0, v2

    :goto_1
    add-int/2addr v0, v3

    .line 626820
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v3, p0, LX/3hT;->c:Z

    if-eqz v3, :cond_2

    :goto_2
    add-int/2addr v0, v2

    .line 626821
    return v0

    :cond_0
    move v0, v1

    .line 626822
    goto :goto_0

    :cond_1
    move v0, v1

    .line 626823
    goto :goto_1

    :cond_2
    move v2, v1

    .line 626824
    goto :goto_2
.end method
