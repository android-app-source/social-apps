.class public LX/3kM;
.super LX/3Jj;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3Jj",
        "<",
        "LX/3kI;",
        "LX/9Um;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/3kL;


# direct methods
.method private constructor <init>(Ljava/util/List;[[[FLX/3kL;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/3kI;",
            ">;[[[F",
            "LX/3kL;",
            ")V"
        }
    .end annotation

    .prologue
    .line 632821
    invoke-direct {p0, p1, p2}, LX/3Jj;-><init>(Ljava/util/List;[[[F)V

    .line 632822
    iput-object p3, p0, LX/3kM;->a:LX/3kL;

    .line 632823
    return-void
.end method

.method private static a(FII)I
    .locals 8

    .prologue
    .line 632824
    invoke-static {p1}, Landroid/graphics/Color;->alpha(I)I

    move-result v0

    .line 632825
    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    move-result v1

    .line 632826
    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    move-result v2

    .line 632827
    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    move-result v3

    .line 632828
    invoke-static {p2}, Landroid/graphics/Color;->alpha(I)I

    move-result v4

    .line 632829
    invoke-static {p2}, Landroid/graphics/Color;->red(I)I

    move-result v5

    .line 632830
    invoke-static {p2}, Landroid/graphics/Color;->green(I)I

    move-result v6

    .line 632831
    invoke-static {p2}, Landroid/graphics/Color;->blue(I)I

    move-result v7

    .line 632832
    sub-int/2addr v4, v0

    int-to-float v4, v4

    mul-float/2addr v4, p0

    float-to-int v4, v4

    add-int/2addr v0, v4

    shl-int/lit8 v0, v0, 0x18

    sub-int v4, v5, v1

    int-to-float v4, v4

    mul-float/2addr v4, p0

    float-to-int v4, v4

    add-int/2addr v1, v4

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    sub-int v1, v6, v2

    int-to-float v1, v1

    mul-float/2addr v1, p0

    float-to-int v1, v1

    add-int/2addr v1, v2

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    sub-int v1, v7, v3

    int-to-float v1, v1

    mul-float/2addr v1, p0

    float-to-int v1, v1

    add-int/2addr v1, v3

    or-int/2addr v0, v1

    return v0
.end method

.method public static a(LX/3kJ;LX/3kL;)LX/3kM;
    .locals 3

    .prologue
    .line 632833
    new-instance v0, LX/3kM;

    .line 632834
    iget-object v1, p0, LX/3kJ;->a:Ljava/util/List;

    move-object v1, v1

    .line 632835
    iget-object v2, p0, LX/3kJ;->b:[[[F

    move-object v2, v2

    .line 632836
    invoke-direct {v0, v1, v2, p1}, LX/3kM;-><init>(Ljava/util/List;[[[FLX/3kL;)V

    return-object v0
.end method


# virtual methods
.method public final a(LX/3Jd;LX/3Jd;FLjava/lang/Object;)V
    .locals 2

    .prologue
    .line 632837
    check-cast p1, LX/3kI;

    check-cast p2, LX/3kI;

    check-cast p4, LX/9Um;

    .line 632838
    if-nez p2, :cond_1

    .line 632839
    iget-object v0, p0, LX/3kM;->a:LX/3kL;

    sget-object v1, LX/3kL;->START:LX/3kL;

    if-ne v0, v1, :cond_0

    .line 632840
    iget v0, p1, LX/3kI;->b:I

    move v0, v0

    .line 632841
    iput v0, p4, LX/9Um;->a:I

    .line 632842
    :goto_0
    return-void

    .line 632843
    :cond_0
    iget v0, p1, LX/3kI;->b:I

    move v0, v0

    .line 632844
    iput v0, p4, LX/9Um;->b:I

    .line 632845
    goto :goto_0

    .line 632846
    :cond_1
    iget-object v0, p0, LX/3kM;->a:LX/3kL;

    sget-object v1, LX/3kL;->START:LX/3kL;

    if-ne v0, v1, :cond_2

    .line 632847
    iget v0, p1, LX/3kI;->b:I

    move v0, v0

    .line 632848
    iget v1, p2, LX/3kI;->b:I

    move v1, v1

    .line 632849
    invoke-static {p3, v0, v1}, LX/3kM;->a(FII)I

    move-result v0

    .line 632850
    iput v0, p4, LX/9Um;->a:I

    .line 632851
    goto :goto_0

    .line 632852
    :cond_2
    iget v0, p1, LX/3kI;->b:I

    move v0, v0

    .line 632853
    iget v1, p2, LX/3kI;->b:I

    move v1, v1

    .line 632854
    invoke-static {p3, v0, v1}, LX/3kM;->a(FII)I

    move-result v0

    .line 632855
    iput v0, p4, LX/9Um;->b:I

    .line 632856
    goto :goto_0
.end method
