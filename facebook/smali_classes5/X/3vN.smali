.class public abstract LX/3vN;
.super LX/3vM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3vM",
        "<",
        "Landroid/widget/SpinnerAdapter;",
        ">;"
    }
.end annotation


# instance fields
.field private E:Landroid/database/DataSetObserver;

.field public a:Landroid/widget/SpinnerAdapter;

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field public g:I

.field public final h:Landroid/graphics/Rect;

.field public final i:LX/3vK;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 651478
    invoke-direct {p0, p1, p2, p3}, LX/3vM;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 651479
    iput v0, p0, LX/3vN;->d:I

    .line 651480
    iput v0, p0, LX/3vN;->e:I

    .line 651481
    iput v0, p0, LX/3vN;->f:I

    .line 651482
    iput v0, p0, LX/3vN;->g:I

    .line 651483
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/3vN;->h:Landroid/graphics/Rect;

    .line 651484
    new-instance v0, LX/3vK;

    invoke-direct {v0, p0}, LX/3vK;-><init>(LX/3vN;)V

    iput-object v0, p0, LX/3vN;->i:LX/3vK;

    .line 651485
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/3vN;->setFocusable(Z)V

    .line 651486
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/3vN;->setWillNotDraw(Z)V

    .line 651487
    return-void
.end method

.method public static synthetic a(LX/3vN;Landroid/view/View;Z)V
    .locals 0

    .prologue
    .line 651477
    invoke-virtual {p0, p1, p2}, LX/3vN;->removeDetachedView(Landroid/view/View;Z)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v2, -0x1

    .line 651468
    iput-boolean v0, p0, LX/3vN;->u:Z

    .line 651469
    iput-boolean v0, p0, LX/3vN;->o:Z

    .line 651470
    invoke-virtual {p0}, LX/3vN;->removeAllViewsInLayout()V

    .line 651471
    iput v2, p0, LX/3vN;->B:I

    .line 651472
    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, LX/3vN;->C:J

    .line 651473
    invoke-virtual {p0, v2}, LX/3vM;->setSelectedPositionInt(I)V

    .line 651474
    invoke-virtual {p0, v2}, LX/3vM;->setNextSelectedPositionInt(I)V

    .line 651475
    invoke-virtual {p0}, LX/3vN;->invalidate()V

    .line 651476
    return-void
.end method

.method public a(Landroid/widget/SpinnerAdapter;)V
    .locals 4

    .prologue
    const/4 v0, -0x1

    .line 651446
    iget-object v1, p0, LX/3vN;->a:Landroid/widget/SpinnerAdapter;

    if-eqz v1, :cond_0

    .line 651447
    iget-object v1, p0, LX/3vN;->a:Landroid/widget/SpinnerAdapter;

    iget-object v2, p0, LX/3vN;->E:Landroid/database/DataSetObserver;

    invoke-interface {v1, v2}, Landroid/widget/SpinnerAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 651448
    invoke-virtual {p0}, LX/3vN;->a()V

    .line 651449
    :cond_0
    iput-object p1, p0, LX/3vN;->a:Landroid/widget/SpinnerAdapter;

    .line 651450
    iput v0, p0, LX/3vN;->B:I

    .line 651451
    const-wide/high16 v2, -0x8000000000000000L

    iput-wide v2, p0, LX/3vN;->C:J

    .line 651452
    iget-object v1, p0, LX/3vN;->a:Landroid/widget/SpinnerAdapter;

    if-eqz v1, :cond_3

    .line 651453
    iget v1, p0, LX/3vM;->z:I

    iput v1, p0, LX/3vN;->A:I

    .line 651454
    iget-object v1, p0, LX/3vN;->a:Landroid/widget/SpinnerAdapter;

    invoke-interface {v1}, Landroid/widget/SpinnerAdapter;->getCount()I

    move-result v1

    iput v1, p0, LX/3vN;->z:I

    .line 651455
    invoke-virtual {p0}, LX/3vM;->e()V

    .line 651456
    new-instance v1, LX/3ve;

    invoke-direct {v1, p0}, LX/3ve;-><init>(LX/3vM;)V

    iput-object v1, p0, LX/3vN;->E:Landroid/database/DataSetObserver;

    .line 651457
    iget-object v1, p0, LX/3vN;->a:Landroid/widget/SpinnerAdapter;

    iget-object v2, p0, LX/3vN;->E:Landroid/database/DataSetObserver;

    invoke-interface {v1, v2}, Landroid/widget/SpinnerAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 651458
    iget v1, p0, LX/3vM;->z:I

    if-lez v1, :cond_1

    const/4 v0, 0x0

    .line 651459
    :cond_1
    invoke-virtual {p0, v0}, LX/3vM;->setSelectedPositionInt(I)V

    .line 651460
    invoke-virtual {p0, v0}, LX/3vM;->setNextSelectedPositionInt(I)V

    .line 651461
    iget v0, p0, LX/3vM;->z:I

    if-nez v0, :cond_2

    .line 651462
    invoke-virtual {p0}, LX/3vM;->g()V

    .line 651463
    :cond_2
    :goto_0
    invoke-virtual {p0}, LX/3vN;->requestLayout()V

    .line 651464
    return-void

    .line 651465
    :cond_3
    invoke-virtual {p0}, LX/3vM;->e()V

    .line 651466
    invoke-virtual {p0}, LX/3vN;->a()V

    .line 651467
    invoke-virtual {p0}, LX/3vM;->g()V

    goto :goto_0
.end method

.method public final generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 3

    .prologue
    .line 651389
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    return-object v0
.end method

.method public final synthetic getAdapter()Landroid/widget/Adapter;
    .locals 1

    .prologue
    .line 651444
    iget-object v0, p0, LX/3vN;->a:Landroid/widget/SpinnerAdapter;

    move-object v0, v0

    .line 651445
    return-object v0
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 651443
    iget v0, p0, LX/3vM;->z:I

    return v0
.end method

.method public final getSelectedView()Landroid/view/View;
    .locals 2

    .prologue
    .line 651488
    iget v0, p0, LX/3vM;->z:I

    if-lez v0, :cond_0

    iget v0, p0, LX/3vM;->x:I

    if-ltz v0, :cond_0

    .line 651489
    iget v0, p0, LX/3vM;->x:I

    iget v1, p0, LX/3vM;->j:I

    sub-int/2addr v0, v1

    invoke-virtual {p0, v0}, LX/3vN;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 651490
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onMeasure(II)V
    .locals 9

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 651400
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v6

    .line 651401
    invoke-virtual {p0}, LX/3vN;->getPaddingLeft()I

    move-result v0

    .line 651402
    invoke-virtual {p0}, LX/3vN;->getPaddingTop()I

    move-result v1

    .line 651403
    invoke-virtual {p0}, LX/3vN;->getPaddingRight()I

    move-result v2

    .line 651404
    invoke-virtual {p0}, LX/3vN;->getPaddingBottom()I

    move-result v3

    .line 651405
    iget-object v7, p0, LX/3vN;->h:Landroid/graphics/Rect;

    iget v8, p0, LX/3vN;->d:I

    if-le v0, v8, :cond_4

    :goto_0
    iput v0, v7, Landroid/graphics/Rect;->left:I

    .line 651406
    iget-object v7, p0, LX/3vN;->h:Landroid/graphics/Rect;

    iget v0, p0, LX/3vN;->e:I

    if-le v1, v0, :cond_5

    move v0, v1

    :goto_1
    iput v0, v7, Landroid/graphics/Rect;->top:I

    .line 651407
    iget-object v1, p0, LX/3vN;->h:Landroid/graphics/Rect;

    iget v0, p0, LX/3vN;->f:I

    if-le v2, v0, :cond_6

    move v0, v2

    :goto_2
    iput v0, v1, Landroid/graphics/Rect;->right:I

    .line 651408
    iget-object v1, p0, LX/3vN;->h:Landroid/graphics/Rect;

    iget v0, p0, LX/3vN;->g:I

    if-le v3, v0, :cond_7

    move v0, v3

    :goto_3
    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    .line 651409
    iget-boolean v0, p0, LX/3vM;->u:Z

    if-eqz v0, :cond_0

    .line 651410
    invoke-virtual {p0}, LX/3vM;->f()V

    .line 651411
    :cond_0
    iget v0, p0, LX/3vM;->v:I

    move v1, v0

    .line 651412
    if-ltz v1, :cond_8

    iget-object v0, p0, LX/3vN;->a:Landroid/widget/SpinnerAdapter;

    if-eqz v0, :cond_8

    iget-object v0, p0, LX/3vN;->a:Landroid/widget/SpinnerAdapter;

    invoke-interface {v0}, Landroid/widget/SpinnerAdapter;->getCount()I

    move-result v0

    if-ge v1, v0, :cond_8

    .line 651413
    iget-object v0, p0, LX/3vN;->i:LX/3vK;

    invoke-virtual {v0, v1}, LX/3vK;->a(I)Landroid/view/View;

    move-result-object v0

    .line 651414
    if-nez v0, :cond_1

    .line 651415
    iget-object v0, p0, LX/3vN;->a:Landroid/widget/SpinnerAdapter;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2, p0}, Landroid/widget/SpinnerAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 651416
    :cond_1
    if-eqz v0, :cond_8

    .line 651417
    iget-object v2, p0, LX/3vN;->i:LX/3vK;

    invoke-virtual {v2, v1, v0}, LX/3vK;->a(ILandroid/view/View;)V

    .line 651418
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    if-nez v1, :cond_2

    .line 651419
    iput-boolean v5, p0, LX/3vN;->D:Z

    .line 651420
    invoke-virtual {p0}, LX/3vN;->generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 651421
    iput-boolean v4, p0, LX/3vN;->D:Z

    .line 651422
    :cond_2
    invoke-virtual {p0, v0, p1, p2}, LX/3vN;->measureChild(Landroid/view/View;II)V

    .line 651423
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    move v1, v1

    .line 651424
    iget-object v2, p0, LX/3vN;->h:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    add-int/2addr v1, v2

    iget-object v2, p0, LX/3vN;->h:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v1, v2

    .line 651425
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    move v0, v2

    .line 651426
    iget-object v2, p0, LX/3vN;->h:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    add-int/2addr v0, v2

    iget-object v2, p0, LX/3vN;->h:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v2

    move v2, v4

    .line 651427
    :goto_4
    if-eqz v2, :cond_3

    .line 651428
    iget-object v1, p0, LX/3vN;->h:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iget-object v2, p0, LX/3vN;->h:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v1, v2

    .line 651429
    if-nez v6, :cond_3

    .line 651430
    iget-object v0, p0, LX/3vN;->h:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, LX/3vN;->h:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v2

    .line 651431
    :cond_3
    invoke-virtual {p0}, LX/3vN;->getSuggestedMinimumHeight()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 651432
    invoke-virtual {p0}, LX/3vN;->getSuggestedMinimumWidth()I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 651433
    invoke-static {v1, p2, v4}, LX/0vv;->a(III)I

    move-result v1

    .line 651434
    invoke-static {v0, p1, v4}, LX/0vv;->a(III)I

    move-result v0

    .line 651435
    invoke-virtual {p0, v0, v1}, LX/3vN;->setMeasuredDimension(II)V

    .line 651436
    iput p2, p0, LX/3vN;->b:I

    .line 651437
    iput p1, p0, LX/3vN;->c:I

    .line 651438
    return-void

    .line 651439
    :cond_4
    iget v0, p0, LX/3vN;->d:I

    goto/16 :goto_0

    .line 651440
    :cond_5
    iget v0, p0, LX/3vN;->e:I

    goto/16 :goto_1

    .line 651441
    :cond_6
    iget v0, p0, LX/3vN;->f:I

    goto/16 :goto_2

    .line 651442
    :cond_7
    iget v0, p0, LX/3vN;->g:I

    goto/16 :goto_3

    :cond_8
    move v2, v5

    move v0, v4

    move v1, v4

    goto :goto_4
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 651390
    check-cast p1, Landroid/support/v7/internal/widget/AbsSpinnerCompat$SavedState;

    .line 651391
    invoke-virtual {p1}, Landroid/support/v7/internal/widget/AbsSpinnerCompat$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, LX/3vM;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 651392
    iget-wide v0, p1, Landroid/support/v7/internal/widget/AbsSpinnerCompat$SavedState;->a:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    .line 651393
    iput-boolean v4, p0, LX/3vN;->u:Z

    .line 651394
    iput-boolean v4, p0, LX/3vN;->o:Z

    .line 651395
    iget-wide v0, p1, Landroid/support/v7/internal/widget/AbsSpinnerCompat$SavedState;->a:J

    iput-wide v0, p0, LX/3vN;->m:J

    .line 651396
    iget v0, p1, Landroid/support/v7/internal/widget/AbsSpinnerCompat$SavedState;->b:I

    iput v0, p0, LX/3vN;->l:I

    .line 651397
    const/4 v0, 0x0

    iput v0, p0, LX/3vN;->p:I

    .line 651398
    invoke-virtual {p0}, LX/3vN;->requestLayout()V

    .line 651399
    :cond_0
    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 8

    .prologue
    .line 651380
    invoke-super {p0}, LX/3vM;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 651381
    new-instance v1, Landroid/support/v7/internal/widget/AbsSpinnerCompat$SavedState;

    invoke-direct {v1, v0}, Landroid/support/v7/internal/widget/AbsSpinnerCompat$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 651382
    iget-wide v6, p0, LX/3vM;->w:J

    move-wide v2, v6

    .line 651383
    iput-wide v2, v1, Landroid/support/v7/internal/widget/AbsSpinnerCompat$SavedState;->a:J

    .line 651384
    iget-wide v2, v1, Landroid/support/v7/internal/widget/AbsSpinnerCompat$SavedState;->a:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-ltz v0, :cond_0

    .line 651385
    iget v0, p0, LX/3vM;->v:I

    move v0, v0

    .line 651386
    iput v0, v1, Landroid/support/v7/internal/widget/AbsSpinnerCompat$SavedState;->b:I

    .line 651387
    :goto_0
    return-object v1

    .line 651388
    :cond_0
    const/4 v0, -0x1

    iput v0, v1, Landroid/support/v7/internal/widget/AbsSpinnerCompat$SavedState;->b:I

    goto :goto_0
.end method

.method public final requestLayout()V
    .locals 1

    .prologue
    .line 651377
    iget-boolean v0, p0, LX/3vM;->D:Z

    if-nez v0, :cond_0

    .line 651378
    invoke-super {p0}, LX/3vM;->requestLayout()V

    .line 651379
    :cond_0
    return-void
.end method

.method public synthetic setAdapter(Landroid/widget/Adapter;)V
    .locals 0

    .prologue
    .line 651376
    check-cast p1, Landroid/widget/SpinnerAdapter;

    invoke-virtual {p0, p1}, LX/3vN;->a(Landroid/widget/SpinnerAdapter;)V

    return-void
.end method

.method public final setSelection(I)V
    .locals 0

    .prologue
    .line 651372
    invoke-virtual {p0, p1}, LX/3vM;->setNextSelectedPositionInt(I)V

    .line 651373
    invoke-virtual {p0}, LX/3vN;->requestLayout()V

    .line 651374
    invoke-virtual {p0}, LX/3vN;->invalidate()V

    .line 651375
    return-void
.end method
