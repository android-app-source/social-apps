.class public final LX/4fU;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0RI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0RI",
            "<*>;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/4fU;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/45T;

.field public d:LX/45U;

.field public e:Ljava/lang/Long;

.field public f:Ljava/lang/Long;

.field public g:J

.field public h:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0RI;LX/45T;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0RI",
            "<*>;",
            "LX/45T;",
            ")V"
        }
    .end annotation

    .prologue
    .line 798461
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 798462
    iput-object p1, p0, LX/4fU;->a:LX/0RI;

    .line 798463
    invoke-static {}, LX/0R9;->b()Ljava/util/LinkedList;

    move-result-object v0

    iput-object v0, p0, LX/4fU;->b:Ljava/util/List;

    .line 798464
    iput-object p2, p0, LX/4fU;->c:LX/45T;

    .line 798465
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/4fU;->g:J

    .line 798466
    return-void
.end method


# virtual methods
.method public final a(Lorg/json/JSONStringer;)V
    .locals 2

    .prologue
    .line 798467
    invoke-virtual {p1}, Lorg/json/JSONStringer;->object()Lorg/json/JSONStringer;

    .line 798468
    iget-object v0, p0, LX/4fU;->h:Ljava/lang/Class;

    if-eqz v0, :cond_0

    .line 798469
    const-string v0, "class"

    invoke-virtual {p1, v0}, Lorg/json/JSONStringer;->key(Ljava/lang/String;)Lorg/json/JSONStringer;

    .line 798470
    iget-object v0, p0, LX/4fU;->h:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/json/JSONStringer;->value(Ljava/lang/Object;)Lorg/json/JSONStringer;

    .line 798471
    :cond_0
    iget-object v0, p0, LX/4fU;->a:LX/0RI;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/4fU;->a:LX/0RI;

    invoke-virtual {v0}, LX/0RI;->b()Ljava/lang/Class;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 798472
    const-string v0, "annotationClass"

    invoke-virtual {p1, v0}, Lorg/json/JSONStringer;->key(Ljava/lang/String;)Lorg/json/JSONStringer;

    .line 798473
    iget-object v0, p0, LX/4fU;->a:LX/0RI;

    invoke-virtual {v0}, LX/0RI;->b()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/json/JSONStringer;->value(Ljava/lang/Object;)Lorg/json/JSONStringer;

    .line 798474
    :cond_1
    iget-object v0, p0, LX/4fU;->e:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 798475
    const-string v0, "startTime"

    invoke-virtual {p1, v0}, Lorg/json/JSONStringer;->key(Ljava/lang/String;)Lorg/json/JSONStringer;

    .line 798476
    iget-object v0, p0, LX/4fU;->e:Ljava/lang/Long;

    invoke-virtual {p1, v0}, Lorg/json/JSONStringer;->value(Ljava/lang/Object;)Lorg/json/JSONStringer;

    .line 798477
    :cond_2
    iget-object v0, p0, LX/4fU;->f:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 798478
    const-string v0, "duration"

    invoke-virtual {p1, v0}, Lorg/json/JSONStringer;->key(Ljava/lang/String;)Lorg/json/JSONStringer;

    .line 798479
    iget-object v0, p0, LX/4fU;->f:Ljava/lang/Long;

    invoke-virtual {p1, v0}, Lorg/json/JSONStringer;->value(Ljava/lang/Object;)Lorg/json/JSONStringer;

    .line 798480
    :cond_3
    iget-object v0, p0, LX/4fU;->d:LX/45U;

    if-eqz v0, :cond_4

    .line 798481
    const-string v0, "callCnt"

    invoke-virtual {p1, v0}, Lorg/json/JSONStringer;->key(Ljava/lang/String;)Lorg/json/JSONStringer;

    .line 798482
    iget-object v0, p0, LX/4fU;->d:LX/45U;

    .line 798483
    iget-object v1, v0, LX/45U;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    move v0, v1

    .line 798484
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONStringer;->value(J)Lorg/json/JSONStringer;

    .line 798485
    :cond_4
    const-string v0, "overheadCorrection"

    invoke-virtual {p1, v0}, Lorg/json/JSONStringer;->key(Ljava/lang/String;)Lorg/json/JSONStringer;

    .line 798486
    iget-wide v0, p0, LX/4fU;->g:J

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONStringer;->value(J)Lorg/json/JSONStringer;

    .line 798487
    const-string v0, "dependencies"

    invoke-virtual {p1, v0}, Lorg/json/JSONStringer;->key(Ljava/lang/String;)Lorg/json/JSONStringer;

    .line 798488
    invoke-virtual {p1}, Lorg/json/JSONStringer;->array()Lorg/json/JSONStringer;

    .line 798489
    iget-object v0, p0, LX/4fU;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4fU;

    .line 798490
    invoke-virtual {v0, p1}, LX/4fU;->a(Lorg/json/JSONStringer;)V

    goto :goto_0

    .line 798491
    :cond_5
    invoke-virtual {p1}, Lorg/json/JSONStringer;->endArray()Lorg/json/JSONStringer;

    .line 798492
    invoke-virtual {p1}, Lorg/json/JSONStringer;->endObject()Lorg/json/JSONStringer;

    .line 798493
    return-void
.end method
