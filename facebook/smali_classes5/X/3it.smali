.class public final LX/3it;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0gf;

.field public final synthetic b:LX/1Cc;


# direct methods
.method public constructor <init>(LX/1Cc;LX/0gf;)V
    .locals 0

    .prologue
    .line 630639
    iput-object p1, p0, LX/3it;->b:LX/1Cc;

    iput-object p2, p0, LX/3it;->a:LX/0gf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 13

    .prologue
    .line 630640
    iget-object v0, p0, LX/3it;->b:LX/1Cc;

    iget-object v0, v0, LX/1Cc;->i:LX/1Cg;

    iget-object v1, p0, LX/3it;->a:LX/0gf;

    .line 630641
    const/4 v6, 0x0

    .line 630642
    new-instance v5, LX/3iw;

    invoke-direct {v5}, LX/3iw;-><init>()V

    move-object v7, v5

    .line 630643
    const-string v5, "scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v8

    invoke-virtual {v7, v5, v8}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    .line 630644
    const-string v5, "width"

    iget-object v8, v0, LX/1Cg;->d:LX/0hB;

    invoke-virtual {v8}, LX/0hB;->c()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v5, v8}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 630645
    const-string v5, "gmt_offset_minutes"

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/TimeZone;->getRawOffset()I

    move-result v8

    int-to-long v9, v8

    const-wide/32 v11, 0xea60

    div-long/2addr v9, v11

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v7, v5, v8}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 630646
    const-string v8, "refresh_mode"

    sget-object v5, LX/0gf;->PULL_TO_REFRESH:LX/0gf;

    if-ne v1, v5, :cond_1

    const-string v5, "MANUAL"

    :goto_0
    invoke-virtual {v7, v8, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 630647
    iget-object v5, v0, LX/1Cg;->e:LX/0y2;

    iget-object v8, v0, LX/1Cg;->f:LX/0ad;

    sget-wide v9, LX/1Cs;->d:J

    const-wide/16 v11, 0x12c

    invoke-interface {v8, v9, v10, v11, v12}, LX/0ad;->a(JJ)J

    move-result-wide v9

    const-wide/16 v11, 0x3e8

    mul-long/2addr v9, v11

    invoke-virtual {v5, v9, v10}, LX/0y2;->a(J)Lcom/facebook/location/ImmutableLocation;

    move-result-object v8

    .line 630648
    const-string v9, "latitude"

    if-eqz v8, :cond_2

    invoke-virtual {v8}, Lcom/facebook/location/ImmutableLocation;->a()D

    move-result-wide v11

    invoke-static {v11, v12}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    :goto_1
    invoke-virtual {v7, v9, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 630649
    const-string v5, "longitude"

    if-eqz v8, :cond_0

    invoke-virtual {v8}, Lcom/facebook/location/ImmutableLocation;->b()D

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    :cond_0
    invoke-virtual {v7, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 630650
    invoke-static {v7}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v5

    sget-object v6, LX/0zS;->c:LX/0zS;

    invoke-virtual {v5, v6}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v5

    .line 630651
    move-object v2, v5

    .line 630652
    iget-object v3, v0, LX/1Cg;->c:LX/0tX;

    invoke-virtual {v3, v2}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v2

    .line 630653
    iget-object v3, v0, LX/1Cg;->a:LX/0QK;

    iget-object v4, v0, LX/1Cg;->b:Ljava/util/concurrent/Executor;

    invoke-static {v2, v3, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    move-object v0, v2

    .line 630654
    return-object v0

    .line 630655
    :cond_1
    const-string v5, "AUTO"

    goto :goto_0

    :cond_2
    move-object v5, v6

    .line 630656
    goto :goto_1
.end method
