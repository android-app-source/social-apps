.class public final LX/4lD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/1MT;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/1MT;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 803843
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 803844
    iput-object p1, p0, LX/4lD;->a:LX/0QB;

    .line 803845
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 803846
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/4lD;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 803847
    packed-switch p2, :pswitch_data_0

    .line 803848
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 803849
    :pswitch_0
    new-instance v1, LX/3yS;

    invoke-static {p1}, LX/3yR;->a(LX/0QB;)LX/3yR;

    move-result-object v0

    check-cast v0, LX/3yR;

    invoke-direct {v1, v0}, LX/3yS;-><init>(LX/3yR;)V

    .line 803850
    move-object v0, v1

    .line 803851
    :goto_0
    return-object v0

    .line 803852
    :pswitch_1
    invoke-static {p1}, LX/3Rl;->a(LX/0QB;)LX/3Rl;

    move-result-object v0

    goto :goto_0

    .line 803853
    :pswitch_2
    new-instance v1, LX/44H;

    invoke-static {p1}, LX/44D;->b(LX/0QB;)LX/44D;

    move-result-object v0

    check-cast v0, LX/44D;

    invoke-direct {v1, v0}, LX/44H;-><init>(LX/44D;)V

    .line 803854
    move-object v0, v1

    .line 803855
    goto :goto_0

    .line 803856
    :pswitch_3
    new-instance v0, LX/44J;

    invoke-direct {v0}, LX/44J;-><init>()V

    .line 803857
    move-object v0, v0

    .line 803858
    move-object v0, v0

    .line 803859
    goto :goto_0

    .line 803860
    :pswitch_4
    invoke-static {p1}, LX/7m4;->a(LX/0QB;)LX/7m4;

    move-result-object v0

    goto :goto_0

    .line 803861
    :pswitch_5
    invoke-static {p1}, LX/6N6;->a(LX/0QB;)LX/6N6;

    move-result-object v0

    goto :goto_0

    .line 803862
    :pswitch_6
    new-instance v1, LX/87c;

    invoke-static {p1}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v0

    check-cast v0, LX/0Uh;

    invoke-direct {v1, v0}, LX/87c;-><init>(LX/0Uh;)V

    .line 803863
    move-object v0, v1

    .line 803864
    goto :goto_0

    .line 803865
    :pswitch_7
    invoke-static {p1}, LX/1MU;->a(LX/0QB;)LX/1MU;

    move-result-object v0

    goto :goto_0

    .line 803866
    :pswitch_8
    invoke-static {p1}, LX/1MR;->a(LX/0QB;)LX/1MR;

    move-result-object v0

    goto :goto_0

    .line 803867
    :pswitch_9
    invoke-static {p1}, LX/Jeo;->a(LX/0QB;)LX/Jeo;

    move-result-object v0

    goto :goto_0

    .line 803868
    :pswitch_a
    invoke-static {p1}, LX/Jep;->a(LX/0QB;)LX/Jep;

    move-result-object v0

    goto :goto_0

    .line 803869
    :pswitch_b
    invoke-static {p1}, LX/Jer;->a(LX/0QB;)LX/Jer;

    move-result-object v0

    goto :goto_0

    .line 803870
    :pswitch_c
    invoke-static {p1}, LX/Jm0;->a(LX/0QB;)LX/Jm0;

    move-result-object v0

    goto :goto_0

    .line 803871
    :pswitch_d
    invoke-static {p1}, LX/3QS;->a(LX/0QB;)LX/3QS;

    move-result-object v0

    goto :goto_0

    .line 803872
    :pswitch_e
    invoke-static {p1}, LX/DqF;->a(LX/0QB;)LX/DqF;

    move-result-object v0

    goto :goto_0

    .line 803873
    :pswitch_f
    invoke-static {p1}, LX/6n7;->getInstance__com_facebook_omnistore_module_OmnistoreExtraFileProvider__INJECTED_BY_TemplateInjector(LX/0QB;)LX/6n7;

    move-result-object v0

    goto :goto_0

    .line 803874
    :pswitch_10
    invoke-static {p1}, LX/2Pt;->getInstance__com_facebook_omnistore_module_OmnistoreInitTimeBugReportInfo__INJECTED_BY_TemplateInjector(LX/0QB;)LX/2Pt;

    move-result-object v0

    goto :goto_0

    .line 803875
    :pswitch_11
    invoke-static {p1}, LX/2S9;->a(LX/0QB;)LX/2S9;

    move-result-object v0

    goto :goto_0

    .line 803876
    :pswitch_12
    invoke-static {p1}, LX/2Sd;->a(LX/0QB;)LX/2Sd;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 803877
    const/16 v0, 0x13

    return v0
.end method
