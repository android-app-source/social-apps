.class public final LX/4en;
.super LX/1eP;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1eP",
        "<",
        "LX/1FL;",
        "LX/1FL;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/4eo;

.field private b:LX/1cW;


# direct methods
.method public constructor <init>(LX/4eo;LX/1cd;LX/1cW;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/imagepipeline/producers/Consumer",
            "<",
            "LX/1FL;",
            ">;",
            "Lcom/facebook/imagepipeline/producers/ProducerContext;",
            ")V"
        }
    .end annotation

    .prologue
    .line 797496
    iput-object p1, p0, LX/4en;->a:LX/4eo;

    .line 797497
    invoke-direct {p0, p2}, LX/1eP;-><init>(LX/1cd;)V

    .line 797498
    iput-object p3, p0, LX/4en;->b:LX/1cW;

    .line 797499
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Z)V
    .locals 3

    .prologue
    .line 797500
    check-cast p1, LX/1FL;

    .line 797501
    iget-object v0, p0, LX/4en;->b:LX/1cW;

    .line 797502
    iget-object v1, v0, LX/1cW;->a:LX/1bf;

    move-object v0, v1

    .line 797503
    iget-object v1, v0, LX/1bf;->h:LX/1o9;

    move-object v1, v1

    .line 797504
    invoke-static {p1, v1}, LX/4fN;->a(LX/1FL;LX/1o9;)Z

    move-result v1

    .line 797505
    if-eqz p1, :cond_1

    if-nez v1, :cond_0

    .line 797506
    iget-boolean v2, v0, LX/1bf;->f:Z

    move v0, v2

    .line 797507
    if-eqz v0, :cond_1

    .line 797508
    :cond_0
    iget-object v0, p0, LX/1eP;->a:LX/1cd;

    move-object v2, v0

    .line 797509
    if-eqz p2, :cond_3

    if-eqz v1, :cond_3

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, p1, v0}, LX/1cd;->b(Ljava/lang/Object;Z)V

    .line 797510
    :cond_1
    if-eqz p2, :cond_2

    if-nez v1, :cond_2

    .line 797511
    invoke-static {p1}, LX/1FL;->d(LX/1FL;)V

    .line 797512
    iget-object v0, p0, LX/4en;->a:LX/4eo;

    iget-object v0, v0, LX/4eo;->b:LX/1cF;

    .line 797513
    iget-object v1, p0, LX/1eP;->a:LX/1cd;

    move-object v1, v1

    .line 797514
    iget-object v2, p0, LX/4en;->b:LX/1cW;

    invoke-interface {v0, v1, v2}, LX/1cF;->a(LX/1cd;LX/1cW;)V

    .line 797515
    :cond_2
    return-void

    .line 797516
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 797517
    iget-object v0, p0, LX/4en;->a:LX/4eo;

    iget-object v0, v0, LX/4eo;->b:LX/1cF;

    .line 797518
    iget-object v1, p0, LX/1eP;->a:LX/1cd;

    move-object v1, v1

    .line 797519
    iget-object v2, p0, LX/4en;->b:LX/1cW;

    invoke-interface {v0, v1, v2}, LX/1cF;->a(LX/1cd;LX/1cW;)V

    .line 797520
    return-void
.end method
