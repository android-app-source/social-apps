.class public final enum LX/4Zg;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/4Zg;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/4Zg;

.field public static final enum ALMOST_DONE:LX/4Zg;

.field public static final enum BEFORE_START:LX/4Zg;

.field public static final enum DONE:LX/4Zg;

.field public static final enum FINISHING_UP:LX/4Zg;

.field public static final enum RUNNING:LX/4Zg;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 790134
    new-instance v0, LX/4Zg;

    const-string v1, "BEFORE_START"

    invoke-direct {v0, v1, v2}, LX/4Zg;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4Zg;->BEFORE_START:LX/4Zg;

    .line 790135
    new-instance v0, LX/4Zg;

    const-string v1, "RUNNING"

    invoke-direct {v0, v1, v3}, LX/4Zg;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4Zg;->RUNNING:LX/4Zg;

    .line 790136
    new-instance v0, LX/4Zg;

    const-string v1, "FINISHING_UP"

    invoke-direct {v0, v1, v4}, LX/4Zg;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4Zg;->FINISHING_UP:LX/4Zg;

    .line 790137
    new-instance v0, LX/4Zg;

    const-string v1, "ALMOST_DONE"

    invoke-direct {v0, v1, v5}, LX/4Zg;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4Zg;->ALMOST_DONE:LX/4Zg;

    .line 790138
    new-instance v0, LX/4Zg;

    const-string v1, "DONE"

    invoke-direct {v0, v1, v6}, LX/4Zg;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4Zg;->DONE:LX/4Zg;

    .line 790139
    const/4 v0, 0x5

    new-array v0, v0, [LX/4Zg;

    sget-object v1, LX/4Zg;->BEFORE_START:LX/4Zg;

    aput-object v1, v0, v2

    sget-object v1, LX/4Zg;->RUNNING:LX/4Zg;

    aput-object v1, v0, v3

    sget-object v1, LX/4Zg;->FINISHING_UP:LX/4Zg;

    aput-object v1, v0, v4

    sget-object v1, LX/4Zg;->ALMOST_DONE:LX/4Zg;

    aput-object v1, v0, v5

    sget-object v1, LX/4Zg;->DONE:LX/4Zg;

    aput-object v1, v0, v6

    sput-object v0, LX/4Zg;->$VALUES:[LX/4Zg;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 790140
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/4Zg;
    .locals 1

    .prologue
    .line 790133
    const-class v0, LX/4Zg;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/4Zg;

    return-object v0
.end method

.method public static values()[LX/4Zg;
    .locals 1

    .prologue
    .line 790132
    sget-object v0, LX/4Zg;->$VALUES:[LX/4Zg;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/4Zg;

    return-object v0
.end method
