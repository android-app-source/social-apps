.class public LX/49T;
.super LX/1sz;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/49T;


# instance fields
.field private final a:LX/3xs;


# direct methods
.method public constructor <init>(Landroid/app/ActivityManager;LX/3xs;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 674474
    invoke-direct {p0, p1}, LX/1sz;-><init>(Landroid/app/ActivityManager;)V

    .line 674475
    iput-object p2, p0, LX/49T;->a:LX/3xs;

    .line 674476
    return-void
.end method

.method public static a(LX/0QB;)LX/49T;
    .locals 5

    .prologue
    .line 674477
    sget-object v0, LX/49T;->b:LX/49T;

    if-nez v0, :cond_1

    .line 674478
    const-class v1, LX/49T;

    monitor-enter v1

    .line 674479
    :try_start_0
    sget-object v0, LX/49T;->b:LX/49T;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 674480
    if-eqz v2, :cond_0

    .line 674481
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 674482
    new-instance p0, LX/49T;

    invoke-static {v0}, LX/0VU;->b(LX/0QB;)Landroid/app/ActivityManager;

    move-result-object v3

    check-cast v3, Landroid/app/ActivityManager;

    invoke-static {v0}, LX/49S;->a(LX/0QB;)LX/3xs;

    move-result-object v4

    check-cast v4, LX/3xs;

    invoke-direct {p0, v3, v4}, LX/49T;-><init>(Landroid/app/ActivityManager;LX/3xs;)V

    .line 674483
    move-object v0, p0

    .line 674484
    sput-object v0, LX/49T;->b:LX/49T;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 674485
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 674486
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 674487
    :cond_1
    sget-object v0, LX/49T;->b:LX/49T;

    return-object v0

    .line 674488
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 674489
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/app/ActivityManager$MemoryInfo;)J
    .locals 8
    .param p1    # Landroid/app/ActivityManager$MemoryInfo;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 674490
    iget-object v0, p0, LX/49T;->a:LX/3xs;

    const/4 v2, 0x0

    .line 674491
    const-wide/16 v4, 0x0

    :try_start_0
    iput-wide v4, v0, LX/3xs;->b:J

    .line 674492
    const-wide/16 v4, 0x0

    iput-wide v4, v0, LX/3xs;->c:J

    .line 674493
    const-wide/16 v4, 0x0

    iput-wide v4, v0, LX/3xs;->d:J

    .line 674494
    new-instance v3, Ljava/io/FileInputStream;

    const-string v4, "/proc/meminfo"

    invoke-direct {v3, v4}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 674495
    iget-object v4, v0, LX/3xs;->a:[B

    invoke-virtual {v3, v4}, Ljava/io/FileInputStream;->read([B)I

    move-result v4

    .line 674496
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V

    .line 674497
    iget-object v3, v0, LX/3xs;->a:[B

    array-length v5, v3

    move v3, v2

    .line 674498
    :goto_0
    if-ge v2, v4, :cond_4

    const/4 v6, 0x3

    if-ge v3, v6, :cond_4

    .line 674499
    iget-object v6, v0, LX/3xs;->a:[B

    const-string v7, "MemTotal"

    invoke-static {v6, v2, v7}, LX/3xs;->a([BILjava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 674500
    add-int/lit8 v2, v2, 0x8

    .line 674501
    iget-object v6, v0, LX/3xs;->a:[B

    invoke-static {v6, v2}, LX/3xs;->a([BI)J

    move-result-wide v6

    iput-wide v6, v0, LX/3xs;->b:J

    .line 674502
    add-int/lit8 v3, v3, 0x1

    .line 674503
    :cond_0
    :goto_1
    if-ge v2, v5, :cond_3

    iget-object v6, v0, LX/3xs;->a:[B

    aget-byte v6, v6, v2

    const/16 v7, 0xa

    if-eq v6, v7, :cond_3

    .line 674504
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 674505
    :cond_1
    iget-object v6, v0, LX/3xs;->a:[B

    const-string v7, "MemFree"

    invoke-static {v6, v2, v7}, LX/3xs;->a([BILjava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 674506
    add-int/lit8 v2, v2, 0x7

    .line 674507
    iget-object v6, v0, LX/3xs;->a:[B

    invoke-static {v6, v2}, LX/3xs;->a([BI)J

    move-result-wide v6

    iput-wide v6, v0, LX/3xs;->c:J

    .line 674508
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 674509
    :cond_2
    iget-object v6, v0, LX/3xs;->a:[B

    const-string v7, "Cached"

    invoke-static {v6, v2, v7}, LX/3xs;->a([BILjava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 674510
    add-int/lit8 v2, v2, 0x6

    .line 674511
    iget-object v6, v0, LX/3xs;->a:[B

    invoke-static {v6, v2}, LX/3xs;->a([BI)J

    move-result-wide v6

    iput-wide v6, v0, LX/3xs;->d:J
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 674512
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 674513
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 674514
    :catch_0
    :cond_4
    :goto_2
    iget-object v0, p0, LX/49T;->a:LX/3xs;

    .line 674515
    iget-wide v2, v0, LX/3xs;->b:J

    move-wide v0, v2

    .line 674516
    return-wide v0

    .line 674517
    :catch_1
    goto :goto_2
.end method
