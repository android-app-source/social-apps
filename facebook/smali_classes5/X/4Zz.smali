.class public final enum LX/4Zz;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/4Zz;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/4Zz;

.field public static final enum ALL:LX/4Zz;

.field public static final enum EACH:LX/4Zz;

.field public static final enum FIRST:LX/4Zz;

.field public static final enum LAST:LX/4Zz;

.field public static final enum NO_FAN_OUT:LX/4Zz;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 790354
    new-instance v0, LX/4Zz;

    const-string v1, "EACH"

    invoke-direct {v0, v1, v2}, LX/4Zz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4Zz;->EACH:LX/4Zz;

    new-instance v0, LX/4Zz;

    const-string v1, "ALL"

    invoke-direct {v0, v1, v3}, LX/4Zz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4Zz;->ALL:LX/4Zz;

    new-instance v0, LX/4Zz;

    const-string v1, "FIRST"

    invoke-direct {v0, v1, v4}, LX/4Zz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4Zz;->FIRST:LX/4Zz;

    new-instance v0, LX/4Zz;

    const-string v1, "LAST"

    invoke-direct {v0, v1, v5}, LX/4Zz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4Zz;->LAST:LX/4Zz;

    new-instance v0, LX/4Zz;

    const-string v1, "NO_FAN_OUT"

    invoke-direct {v0, v1, v6}, LX/4Zz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4Zz;->NO_FAN_OUT:LX/4Zz;

    const/4 v0, 0x5

    new-array v0, v0, [LX/4Zz;

    sget-object v1, LX/4Zz;->EACH:LX/4Zz;

    aput-object v1, v0, v2

    sget-object v1, LX/4Zz;->ALL:LX/4Zz;

    aput-object v1, v0, v3

    sget-object v1, LX/4Zz;->FIRST:LX/4Zz;

    aput-object v1, v0, v4

    sget-object v1, LX/4Zz;->LAST:LX/4Zz;

    aput-object v1, v0, v5

    sget-object v1, LX/4Zz;->NO_FAN_OUT:LX/4Zz;

    aput-object v1, v0, v6

    sput-object v0, LX/4Zz;->$VALUES:[LX/4Zz;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 790351
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/4Zz;
    .locals 1

    .prologue
    .line 790352
    const-class v0, LX/4Zz;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/4Zz;

    return-object v0
.end method

.method public static values()[LX/4Zz;
    .locals 1

    .prologue
    .line 790353
    sget-object v0, LX/4Zz;->$VALUES:[LX/4Zz;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/4Zz;

    return-object v0
.end method
