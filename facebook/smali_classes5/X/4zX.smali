.class public final enum LX/4zX;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/0qF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/4zX;",
        ">;",
        "LX/0qF",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/4zX;

.field public static final enum INSTANCE:LX/4zX;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 822895
    new-instance v0, LX/4zX;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, LX/4zX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4zX;->INSTANCE:LX/4zX;

    .line 822896
    const/4 v0, 0x1

    new-array v0, v0, [LX/4zX;

    sget-object v1, LX/4zX;->INSTANCE:LX/4zX;

    aput-object v1, v0, v2

    sput-object v0, LX/4zX;->$VALUES:[LX/4zX;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 822894
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/4zX;
    .locals 1

    .prologue
    .line 822893
    const-class v0, LX/4zX;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/4zX;

    return-object v0
.end method

.method public static values()[LX/4zX;
    .locals 1

    .prologue
    .line 822892
    sget-object v0, LX/4zX;->$VALUES:[LX/4zX;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/4zX;

    return-object v0
.end method


# virtual methods
.method public final getExpirationTime()J
    .locals 2

    .prologue
    .line 822891
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final getHash()I
    .locals 1

    .prologue
    .line 822890
    const/4 v0, 0x0

    return v0
.end method

.method public final getKey()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 822889
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getNext()LX/0qF;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0qF",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 822888
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getNextEvictable()LX/0qF;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0qF",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 822887
    return-object p0
.end method

.method public final getNextExpirable()LX/0qF;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0qF",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 822877
    return-object p0
.end method

.method public final getPreviousEvictable()LX/0qF;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0qF",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 822886
    return-object p0
.end method

.method public final getPreviousExpirable()LX/0qF;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0qF",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 822885
    return-object p0
.end method

.method public final getValueReference()LX/0cp;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0cp",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 822884
    const/4 v0, 0x0

    return-object v0
.end method

.method public final setExpirationTime(J)V
    .locals 0

    .prologue
    .line 822883
    return-void
.end method

.method public final setNextEvictable(LX/0qF;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0qF",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 822882
    return-void
.end method

.method public final setNextExpirable(LX/0qF;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0qF",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 822881
    return-void
.end method

.method public final setPreviousEvictable(LX/0qF;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0qF",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 822880
    return-void
.end method

.method public final setPreviousExpirable(LX/0qF;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0qF",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 822879
    return-void
.end method

.method public final setValueReference(LX/0cp;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0cp",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 822878
    return-void
.end method
