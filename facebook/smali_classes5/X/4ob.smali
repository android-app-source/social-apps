.class public LX/4ob;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Landroid/view/View;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static final d:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/view/View;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "UI-thread"
    .end annotation
.end field


# instance fields
.field private a:Landroid/support/v7/internal/widget/ViewStubCompat;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private b:Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:LX/4oa;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4oa",
            "<TT;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 810204
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, LX/4ob;->d:Landroid/util/SparseArray;

    return-void
.end method

.method public constructor <init>(Landroid/support/v7/internal/widget/ViewStubCompat;)V
    .locals 0

    .prologue
    .line 810205
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 810206
    iput-object p1, p0, LX/4ob;->a:Landroid/support/v7/internal/widget/ViewStubCompat;

    .line 810207
    return-void
.end method

.method public static a(Landroid/support/v7/internal/widget/ViewStubCompat;)LX/4ob;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Landroid/support/v7/internal/widget/ViewStubCompat;",
            ")",
            "LX/4ob",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 810208
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 810209
    new-instance v0, LX/4ob;

    invoke-direct {v0, p0}, LX/4ob;-><init>(Landroid/support/v7/internal/widget/ViewStubCompat;)V

    return-object v0
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 810210
    iget-object v0, p0, LX/4ob;->b:Landroid/view/View;

    if-nez v0, :cond_3

    iget-object v0, p0, LX/4ob;->a:Landroid/support/v7/internal/widget/ViewStubCompat;

    if-eqz v0, :cond_3

    .line 810211
    iget-object v0, p0, LX/4ob;->a:Landroid/support/v7/internal/widget/ViewStubCompat;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ViewStubCompat;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, LX/4ob;->a:Landroid/support/v7/internal/widget/ViewStubCompat;

    invoke-virtual {v1}, Landroid/support/v7/internal/widget/ViewStubCompat;->getLayoutResource()I

    move-result v1

    .line 810212
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    if-ne v2, v3, :cond_a

    const/4 v2, 0x1

    :goto_0
    move v2, v2

    .line 810213
    const-string v3, "Must be called from GUI thread"

    invoke-static {v2, v3}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 810214
    const/4 v3, 0x0

    .line 810215
    sget-object v2, LX/4ob;->d:Landroid/util/SparseArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/ref/WeakReference;

    .line 810216
    if-eqz v2, :cond_9

    .line 810217
    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 810218
    sget-object v3, LX/4ob;->d:Landroid/util/SparseArray;

    invoke-virtual {v3, v1}, Landroid/util/SparseArray;->remove(I)V

    .line 810219
    :goto_1
    const/4 v3, 0x3

    invoke-static {v3}, LX/01m;->b(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 810220
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getResourceName(I)Ljava/lang/String;

    .line 810221
    :cond_0
    move-object v0, v2

    .line 810222
    if-eqz v0, :cond_5

    .line 810223
    iput-object v0, p0, LX/4ob;->b:Landroid/view/View;

    .line 810224
    iget-object v0, p0, LX/4ob;->a:Landroid/support/v7/internal/widget/ViewStubCompat;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ViewStubCompat;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 810225
    iget-object v1, p0, LX/4ob;->a:Landroid/support/v7/internal/widget/ViewStubCompat;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v1

    .line 810226
    iget-object v2, p0, LX/4ob;->a:Landroid/support/v7/internal/widget/ViewStubCompat;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->removeViewInLayout(Landroid/view/View;)V

    .line 810227
    iget-object v2, p0, LX/4ob;->a:Landroid/support/v7/internal/widget/ViewStubCompat;

    invoke-virtual {v2}, Landroid/support/v7/internal/widget/ViewStubCompat;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 810228
    if-eqz v2, :cond_4

    .line 810229
    iget-object v3, p0, LX/4ob;->b:Landroid/view/View;

    invoke-virtual {v0, v3, v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 810230
    :cond_1
    :goto_2
    iget-object v0, p0, LX/4ob;->c:LX/4oa;

    if-eqz v0, :cond_2

    .line 810231
    iget-object v0, p0, LX/4ob;->c:LX/4oa;

    iget-object v1, p0, LX/4ob;->b:Landroid/view/View;

    invoke-interface {v0, v1}, LX/4oa;->a(Landroid/view/View;)V

    .line 810232
    :cond_2
    iput-object v4, p0, LX/4ob;->a:Landroid/support/v7/internal/widget/ViewStubCompat;

    .line 810233
    iput-object v4, p0, LX/4ob;->c:LX/4oa;

    .line 810234
    :cond_3
    iget-object v0, p0, LX/4ob;->b:Landroid/view/View;

    return-object v0

    .line 810235
    :cond_4
    iget-object v2, p0, LX/4ob;->b:Landroid/view/View;

    invoke-virtual {v0, v2, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    goto :goto_2

    .line 810236
    :cond_5
    iget-object v0, p0, LX/4ob;->a:Landroid/support/v7/internal/widget/ViewStubCompat;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ViewStubCompat;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    if-eqz v0, :cond_7

    iget-object v0, p0, LX/4ob;->a:Landroid/support/v7/internal/widget/ViewStubCompat;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ViewStubCompat;->getLayoutResource()I

    move-result v0

    if-eqz v0, :cond_7

    const/4 v0, 0x1

    move v1, v0

    .line 810237
    :goto_3
    if-eqz v1, :cond_6

    .line 810238
    const-string v0, "getView: inflate(%s)"

    iget-object v2, p0, LX/4ob;->a:Landroid/support/v7/internal/widget/ViewStubCompat;

    invoke-virtual {v2}, Landroid/support/v7/internal/widget/ViewStubCompat;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v3, p0, LX/4ob;->a:Landroid/support/v7/internal/widget/ViewStubCompat;

    invoke-virtual {v3}, Landroid/support/v7/internal/widget/ViewStubCompat;->getLayoutResource()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getResourceName(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x2e7dec0a

    invoke-static {v0, v2, v3}, LX/02m;->a(Ljava/lang/String;Ljava/lang/Object;I)V

    .line 810239
    :cond_6
    :try_start_0
    iget-object v0, p0, LX/4ob;->a:Landroid/support/v7/internal/widget/ViewStubCompat;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ViewStubCompat;->a()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/4ob;->b:Landroid/view/View;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 810240
    if-eqz v1, :cond_1

    .line 810241
    const v0, -0x2a84b01f

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_2

    .line 810242
    :cond_7
    const/4 v0, 0x0

    move v1, v0

    goto :goto_3

    .line 810243
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_8

    .line 810244
    const v1, 0x6d750222

    invoke-static {v1}, LX/02m;->a(I)V

    :cond_8
    throw v0

    :cond_9
    move-object v2, v3

    goto/16 :goto_1

    :cond_a
    const/4 v2, 0x0

    goto/16 :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 810245
    iget-object v0, p0, LX/4ob;->b:Landroid/view/View;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 810246
    invoke-virtual {p0}, LX/4ob;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/4ob;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 810247
    invoke-virtual {p0}, LX/4ob;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 810248
    iget-object v0, p0, LX/4ob;->b:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 810249
    :cond_0
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 810250
    invoke-virtual {p0}, LX/4ob;->a()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 810251
    return-void
.end method
