.class public LX/4OP;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 696249
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    .line 696250
    const/4 v9, 0x0

    .line 696251
    const/4 v8, 0x0

    .line 696252
    const/4 v7, 0x0

    .line 696253
    const/4 v6, 0x0

    .line 696254
    const/4 v5, 0x0

    .line 696255
    const/4 v4, 0x0

    .line 696256
    const/4 v3, 0x0

    .line 696257
    const/4 v2, 0x0

    .line 696258
    const/4 v1, 0x0

    .line 696259
    const/4 v0, 0x0

    .line 696260
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->START_OBJECT:LX/15z;

    if-eq v10, v11, :cond_1

    .line 696261
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 696262
    const/4 v0, 0x0

    .line 696263
    :goto_0
    return v0

    .line 696264
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 696265
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_9

    .line 696266
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 696267
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 696268
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_1

    if-eqz v10, :cond_1

    .line 696269
    const-string v11, "default_cover_photo"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 696270
    invoke-static {p0, p1}, LX/2sX;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 696271
    :cond_2
    const-string v11, "default_group_name"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 696272
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 696273
    :cond_3
    const-string v11, "purpose_description"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 696274
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 696275
    :cond_4
    const-string v11, "purpose_enum"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 696276
    const/4 v1, 0x1

    .line 696277
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    move-result-object v6

    goto :goto_1

    .line 696278
    :cond_5
    const-string v11, "purpose_image"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 696279
    invoke-static {p0, p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 696280
    :cond_6
    const-string v11, "purpose_name"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 696281
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 696282
    :cond_7
    const-string v11, "purpose_type"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 696283
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto/16 :goto_1

    .line 696284
    :cond_8
    const-string v11, "visibility"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 696285
    const/4 v0, 0x1

    .line 696286
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v2

    goto/16 :goto_1

    .line 696287
    :cond_9
    const/16 v10, 0x8

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 696288
    const/4 v10, 0x0

    invoke-virtual {p1, v10, v9}, LX/186;->b(II)V

    .line 696289
    const/4 v9, 0x1

    invoke-virtual {p1, v9, v8}, LX/186;->b(II)V

    .line 696290
    const/4 v8, 0x2

    invoke-virtual {p1, v8, v7}, LX/186;->b(II)V

    .line 696291
    if-eqz v1, :cond_a

    .line 696292
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v6}, LX/186;->a(ILjava/lang/Enum;)V

    .line 696293
    :cond_a
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 696294
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 696295
    const/4 v1, 0x6

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 696296
    if-eqz v0, :cond_b

    .line 696297
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v2}, LX/186;->a(ILjava/lang/Enum;)V

    .line 696298
    :cond_b
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    const/4 v4, 0x7

    const/4 v3, 0x3

    const/4 v2, 0x0

    .line 696299
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 696300
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 696301
    if-eqz v0, :cond_0

    .line 696302
    const-string v1, "default_cover_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 696303
    invoke-static {p0, v0, p2, p3}, LX/2sX;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 696304
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 696305
    if-eqz v0, :cond_1

    .line 696306
    const-string v1, "default_group_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 696307
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 696308
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 696309
    if-eqz v0, :cond_2

    .line 696310
    const-string v1, "purpose_description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 696311
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 696312
    :cond_2
    invoke-virtual {p0, p1, v3, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 696313
    if-eqz v0, :cond_3

    .line 696314
    const-string v0, "purpose_enum"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 696315
    const-class v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    invoke-virtual {p0, p1, v3, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 696316
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 696317
    if-eqz v0, :cond_4

    .line 696318
    const-string v1, "purpose_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 696319
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 696320
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 696321
    if-eqz v0, :cond_5

    .line 696322
    const-string v1, "purpose_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 696323
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 696324
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 696325
    if-eqz v0, :cond_6

    .line 696326
    const-string v1, "purpose_type"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 696327
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 696328
    :cond_6
    invoke-virtual {p0, p1, v4, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 696329
    if-eqz v0, :cond_7

    .line 696330
    const-string v0, "visibility"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 696331
    const-class v0, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-virtual {p0, p1, v4, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 696332
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 696333
    return-void
.end method
