.class public LX/4lj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lorg/apache/http/conn/scheme/LayeredSocketFactory;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "DeprecatedInterface"
    }
.end annotation


# instance fields
.field private final a:Lorg/apache/harmony/xnet/provider/jsse/SSLParametersImpl;

.field private final b:LX/4ly;

.field private final c:LX/4lw;

.field private final d:LX/4lk;

.field private final e:LX/03V;


# direct methods
.method public constructor <init>(Ljavax/net/ssl/SSLSocketFactory;LX/4ly;LX/4lu;LX/4lv;LX/4lw;LX/4lk;ILX/03V;)V
    .locals 1

    .prologue
    .line 804713
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 804714
    iput-object p2, p0, LX/4lj;->b:LX/4ly;

    .line 804715
    invoke-static {p1}, LX/4lu;->a(Ljavax/net/ssl/SSLSocketFactory;)Lorg/apache/harmony/xnet/provider/jsse/SSLParametersImpl;

    move-result-object v0

    iput-object v0, p0, LX/4lj;->a:Lorg/apache/harmony/xnet/provider/jsse/SSLParametersImpl;

    .line 804716
    iget-object v0, p0, LX/4lj;->a:Lorg/apache/harmony/xnet/provider/jsse/SSLParametersImpl;

    invoke-static {v0, p7}, LX/4lv;->a(Lorg/apache/harmony/xnet/provider/jsse/SSLParametersImpl;I)V

    .line 804717
    iput-object p5, p0, LX/4lj;->c:LX/4lw;

    .line 804718
    iput-object p6, p0, LX/4lj;->d:LX/4lk;

    .line 804719
    iput-object p8, p0, LX/4lj;->e:LX/03V;

    .line 804720
    return-void
.end method


# virtual methods
.method public final connectSocket(Ljava/net/Socket;Ljava/lang/String;ILjava/net/InetAddress;ILorg/apache/http/params/HttpParams;)Ljava/net/Socket;
    .locals 2

    .prologue
    .line 804721
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "connectSocket() is not supported by the socket factory"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final createSocket()Ljava/net/Socket;
    .locals 2

    .prologue
    .line 804722
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "createSocket() is not supported by the socket factory"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final createSocket(Ljava/net/Socket;Ljava/lang/String;IZ)Ljava/net/Socket;
    .locals 6

    .prologue
    .line 804701
    iget-object v4, p0, LX/4lj;->a:Lorg/apache/harmony/xnet/provider/jsse/SSLParametersImpl;

    iget-object v5, p0, LX/4lj;->e:LX/03V;

    move-object v0, p1

    move-object v1, p2

    move v2, p3

    move v3, p4

    invoke-static/range {v0 .. v5}, LX/4lk;->a(Ljava/net/Socket;Ljava/lang/String;IZLorg/apache/harmony/xnet/provider/jsse/SSLParametersImpl;LX/03V;)Lcom/facebook/ssl/openssl/TicketEnabledOpenSSLSocketImplWrapper;

    move-result-object v0

    .line 804702
    :try_start_0
    invoke-virtual {v0, p2}, Lcom/facebook/ssl/openssl/TicketEnabledOpenSSLSocketImplWrapper;->setHostname(Ljava/lang/String;)V

    .line 804703
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/ssl/openssl/TicketEnabledOpenSSLSocketImplWrapper;->setUseSessionTickets(Z)V

    .line 804704
    invoke-virtual {p1}, Ljava/net/Socket;->getSoTimeout()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/ssl/openssl/TicketEnabledOpenSSLSocketImplWrapper;->setHandshakeTimeout(I)V

    .line 804705
    invoke-virtual {p1}, Ljava/net/Socket;->getInetAddress()Ljava/net/InetAddress;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/InetAddress;->getAddress()[B

    move-result-object v1

    invoke-static {v0, v1, p2, p3}, LX/4lw;->a(Ljava/net/Socket;[BLjava/lang/String;I)V

    .line 804706
    iget-object v1, p0, LX/4lj;->b:LX/4ly;

    invoke-virtual {v1, v0, p2}, LX/4ly;->a(Ljavax/net/ssl/SSLSocket;Ljava/lang/String;)V
    :try_end_0
    .catch LX/4ll; {:try_start_0 .. :try_end_0} :catch_0

    .line 804707
    return-object v0

    .line 804708
    :catch_0
    move-exception v0

    .line 804709
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final isSecure(Ljava/net/Socket;)Z
    .locals 2

    .prologue
    .line 804710
    const-string v0, "The socket may not be null"

    invoke-static {p1, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 804711
    instance-of v0, p1, Lcom/facebook/ssl/openssl/TicketEnabledOpenSSLSocketImplWrapper;

    const-string v1, "Socket not created by this factory."

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 804712
    const/4 v0, 0x1

    return v0
.end method
