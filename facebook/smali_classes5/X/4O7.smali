.class public LX/4O7;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 695668
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 695669
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v2, :cond_7

    .line 695670
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 695671
    :goto_0
    return v0

    .line 695672
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 695673
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_6

    .line 695674
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 695675
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 695676
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_1

    if-eqz v4, :cond_1

    .line 695677
    const-string v5, "id"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 695678
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 695679
    :cond_2
    const-string v5, "template_images"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 695680
    invoke-static {p0, p1}, LX/2ax;->b(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 695681
    :cond_3
    const-string v5, "template_themes"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 695682
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 695683
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->START_ARRAY:LX/15z;

    if-ne v4, v5, :cond_4

    .line 695684
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_ARRAY:LX/15z;

    if-eq v4, v5, :cond_4

    .line 695685
    invoke-static {p0, p1}, LX/4O8;->b(LX/15w;LX/186;)I

    move-result v4

    .line 695686
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 695687
    :cond_4
    invoke-static {v1, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v1

    move v1, v1

    .line 695688
    goto :goto_1

    .line 695689
    :cond_5
    const-string v5, "url"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 695690
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 695691
    :cond_6
    const/4 v4, 0x5

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 695692
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, LX/186;->b(II)V

    .line 695693
    const/4 v3, 0x2

    invoke-virtual {p1, v3, v2}, LX/186;->b(II)V

    .line 695694
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v1}, LX/186;->b(II)V

    .line 695695
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 695696
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :cond_7
    move v1, v0

    move v2, v0

    move v3, v0

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 695697
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 695698
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 695699
    if-eqz v0, :cond_0

    .line 695700
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 695701
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 695702
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 695703
    if-eqz v0, :cond_1

    .line 695704
    const-string v1, "template_images"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 695705
    invoke-static {p0, v0, p2, p3}, LX/2ax;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 695706
    :cond_1
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 695707
    if-eqz v0, :cond_3

    .line 695708
    const-string v1, "template_themes"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 695709
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 695710
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 695711
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2}, LX/4O8;->a(LX/15i;ILX/0nX;)V

    .line 695712
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 695713
    :cond_2
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 695714
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 695715
    if-eqz v0, :cond_4

    .line 695716
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 695717
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 695718
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 695719
    return-void
.end method
