.class public LX/4Q4;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 702735
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 702736
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v2, :cond_8

    .line 702737
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 702738
    :goto_0
    return v0

    .line 702739
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 702740
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_7

    .line 702741
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 702742
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 702743
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_1

    if-eqz v6, :cond_1

    .line 702744
    const-string v7, "android_urls"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 702745
    invoke-static {p0, p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 702746
    :cond_2
    const-string v7, "application"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 702747
    invoke-static {p0, p1}, LX/2uk;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 702748
    :cond_3
    const-string v7, "feedback"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 702749
    invoke-static {p0, p1}, LX/2bG;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 702750
    :cond_4
    const-string v7, "id"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 702751
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 702752
    :cond_5
    const-string v7, "primary_object_node"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 702753
    invoke-static {p0, p1}, LX/2bR;->a(LX/15w;LX/186;)I

    move-result v1

    goto :goto_1

    .line 702754
    :cond_6
    const-string v7, "url"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 702755
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 702756
    :cond_7
    const/4 v6, 0x7

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 702757
    const/4 v6, 0x1

    invoke-virtual {p1, v6, v5}, LX/186;->b(II)V

    .line 702758
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, LX/186;->b(II)V

    .line 702759
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, LX/186;->b(II)V

    .line 702760
    const/4 v3, 0x4

    invoke-virtual {p1, v3, v2}, LX/186;->b(II)V

    .line 702761
    const/4 v2, 0x5

    invoke-virtual {p1, v2, v1}, LX/186;->b(II)V

    .line 702762
    const/4 v1, 0x6

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 702763
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :cond_8
    move v1, v0

    move v2, v0

    move v3, v0

    move v4, v0

    move v5, v0

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 702764
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 702765
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 702766
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2, p3}, LX/4Q4;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 702767
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 702768
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 702769
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 702770
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 702771
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 702772
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 702773
    invoke-static {p0, p1}, LX/4Q4;->a(LX/15w;LX/186;)I

    move-result v1

    .line 702774
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 702775
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 702776
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 702777
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 702778
    if-eqz v0, :cond_0

    .line 702779
    const-string v0, "android_urls"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 702780
    invoke-virtual {p0, p1, v1}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 702781
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 702782
    if-eqz v0, :cond_1

    .line 702783
    const-string v1, "application"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 702784
    invoke-static {p0, v0, p2, p3}, LX/2uk;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 702785
    :cond_1
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 702786
    if-eqz v0, :cond_2

    .line 702787
    const-string v1, "feedback"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 702788
    invoke-static {p0, v0, p2, p3}, LX/2bG;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 702789
    :cond_2
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 702790
    if-eqz v0, :cond_3

    .line 702791
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 702792
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 702793
    :cond_3
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 702794
    if-eqz v0, :cond_4

    .line 702795
    const-string v1, "primary_object_node"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 702796
    invoke-static {p0, v0, p2, p3}, LX/2bR;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 702797
    :cond_4
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 702798
    if-eqz v0, :cond_5

    .line 702799
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 702800
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 702801
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 702802
    return-void
.end method
