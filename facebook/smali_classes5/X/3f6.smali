.class public LX/3f6;
.super LX/1qS;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/3f6;


# instance fields
.field private final a:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Tt;LX/1qU;LX/3f7;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 6
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 621389
    invoke-static {p4}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    const-string v5, "admined_pages_db"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, LX/1qS;-><init>(Landroid/content/Context;LX/0Tt;LX/1qU;LX/0Px;Ljava/lang/String;)V

    .line 621390
    iput-object p5, p0, LX/3f6;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 621391
    return-void
.end method

.method public static a(LX/0QB;)LX/3f6;
    .locals 9

    .prologue
    .line 621392
    sget-object v0, LX/3f6;->b:LX/3f6;

    if-nez v0, :cond_1

    .line 621393
    const-class v1, LX/3f6;

    monitor-enter v1

    .line 621394
    :try_start_0
    sget-object v0, LX/3f6;->b:LX/3f6;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 621395
    if-eqz v2, :cond_0

    .line 621396
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 621397
    new-instance v3, LX/3f6;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0Ts;->a(LX/0QB;)LX/0Ts;

    move-result-object v5

    check-cast v5, LX/0Tt;

    invoke-static {v0}, LX/1qT;->a(LX/0QB;)LX/1qT;

    move-result-object v6

    check-cast v6, LX/1qU;

    invoke-static {v0}, LX/3f7;->a(LX/0QB;)LX/3f7;

    move-result-object v7

    check-cast v7, LX/3f7;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v8

    check-cast v8, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct/range {v3 .. v8}, LX/3f6;-><init>(Landroid/content/Context;LX/0Tt;LX/1qU;LX/3f7;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 621398
    move-object v0, v3

    .line 621399
    sput-object v0, LX/3f6;->b:LX/3f6;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 621400
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 621401
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 621402
    :cond_1
    sget-object v0, LX/3f6;->b:LX/3f6;

    return-object v0

    .line 621403
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 621404
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final clearUserData()V
    .locals 2

    .prologue
    .line 621405
    invoke-virtual {p0}, LX/0Tr;->f()V

    .line 621406
    iget-object v0, p0, LX/3f6;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/3ex;->b:LX/0Tn;

    invoke-interface {v0, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 621407
    return-void
.end method
