.class public LX/3gc;
.super LX/3gB;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/3gc;


# instance fields
.field public final a:LX/3gd;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3lZ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/3gd;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3gd;",
            "LX/0Ot",
            "<",
            "LX/3lZ;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 624615
    invoke-direct {p0}, LX/3gB;-><init>()V

    .line 624616
    iput-object p1, p0, LX/3gc;->a:LX/3gd;

    .line 624617
    iput-object p2, p0, LX/3gc;->b:LX/0Ot;

    .line 624618
    return-void
.end method

.method public static a(LX/0QB;)LX/3gc;
    .locals 5

    .prologue
    .line 624619
    sget-object v0, LX/3gc;->c:LX/3gc;

    if-nez v0, :cond_1

    .line 624620
    const-class v1, LX/3gc;

    monitor-enter v1

    .line 624621
    :try_start_0
    sget-object v0, LX/3gc;->c:LX/3gc;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 624622
    if-eqz v2, :cond_0

    .line 624623
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 624624
    new-instance v4, LX/3gc;

    invoke-static {v0}, LX/3gd;->b(LX/0QB;)LX/3gd;

    move-result-object v3

    check-cast v3, LX/3gd;

    const/16 p0, 0xfb6

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v4, v3, p0}, LX/3gc;-><init>(LX/3gd;LX/0Ot;)V

    .line 624625
    move-object v0, v4

    .line 624626
    sput-object v0, LX/3gc;->c:LX/3gc;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 624627
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 624628
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 624629
    :cond_1
    sget-object v0, LX/3gc;->c:LX/3gc;

    return-object v0

    .line 624630
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 624631
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final b()LX/2ZE;
    .locals 2

    .prologue
    .line 624632
    new-instance v0, LX/3ge;

    invoke-direct {v0, p0}, LX/3ge;-><init>(LX/3gc;)V

    return-object v0
.end method

.method public final dr_()J
    .locals 2

    .prologue
    .line 624633
    const-wide/32 v0, 0xdbba00

    return-wide v0
.end method
