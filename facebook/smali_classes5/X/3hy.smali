.class public final LX/3hy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/3hq;

.field public final synthetic b:LX/3hx;

.field public final synthetic c:Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;LX/3hq;LX/3hx;)V
    .locals 0

    .prologue
    .line 628296
    iput-object p1, p0, LX/3hy;->c:Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;

    iput-object p2, p0, LX/3hy;->a:LX/3hq;

    iput-object p3, p0, LX/3hy;->b:LX/3hx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x32cf3cc3

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 628297
    instance-of v1, p1, Lcom/facebook/feedplugins/multishare/MultiShareInlineVideoView;

    if-eqz v1, :cond_0

    .line 628298
    check-cast p1, Lcom/facebook/feedplugins/multishare/MultiShareInlineVideoView;

    .line 628299
    iget-object v1, p0, LX/3hy;->a:LX/3hq;

    .line 628300
    iget-boolean v2, v1, LX/3hq;->g:Z

    move v1, v2

    .line 628301
    if-eqz v1, :cond_1

    .line 628302
    iget-object v1, p0, LX/3hy;->b:LX/3hx;

    invoke-static {v1, p1}, Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;->a(LX/3hx;Landroid/view/View;)V

    .line 628303
    :cond_0
    :goto_0
    const v1, 0x79446789

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 628304
    :cond_1
    iget-object v1, p0, LX/3hy;->c:Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;

    iget-object v2, p0, LX/3hy;->a:LX/3hq;

    iget-object v3, p0, LX/3hy;->b:LX/3hx;

    invoke-virtual {p1}, Lcom/facebook/video/player/RichVideoPlayer;->getLastStartPosition()I

    move-result v4

    invoke-virtual {v1, v2, v3, p1, v4}, Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;->a(LX/3hq;LX/3hx;Landroid/view/View;I)V

    goto :goto_0
.end method
