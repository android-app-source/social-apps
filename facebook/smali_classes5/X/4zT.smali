.class public final LX/4zT;
.super Ljava/util/AbstractQueue;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/AbstractQueue",
        "<",
        "LX/0qF",
        "<TK;TV;>;>;"
    }
.end annotation


# instance fields
.field public final a:LX/0qF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0qF",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 822822
    invoke-direct {p0}, Ljava/util/AbstractQueue;-><init>()V

    .line 822823
    new-instance v0, LX/4zR;

    invoke-direct {v0, p0}, LX/4zR;-><init>(LX/4zT;)V

    iput-object v0, p0, LX/4zT;->a:LX/0qF;

    return-void
.end method

.method private a()LX/0qF;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0qF",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 822786
    iget-object v0, p0, LX/4zT;->a:LX/0qF;

    invoke-interface {v0}, LX/0qF;->getNextEvictable()LX/0qF;

    move-result-object v0

    .line 822787
    iget-object v1, p0, LX/4zT;->a:LX/0qF;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    return-object v0
.end method


# virtual methods
.method public final clear()V
    .locals 2

    .prologue
    .line 822814
    iget-object v0, p0, LX/4zT;->a:LX/0qF;

    invoke-interface {v0}, LX/0qF;->getNextEvictable()LX/0qF;

    move-result-object v0

    .line 822815
    :goto_0
    iget-object v1, p0, LX/4zT;->a:LX/0qF;

    if-eq v0, v1, :cond_0

    .line 822816
    invoke-interface {v0}, LX/0qF;->getNextEvictable()LX/0qF;

    move-result-object v1

    .line 822817
    invoke-static {v0}, LX/0cn;->e(LX/0qF;)V

    move-object v0, v1

    .line 822818
    goto :goto_0

    .line 822819
    :cond_0
    iget-object v0, p0, LX/4zT;->a:LX/0qF;

    iget-object v1, p0, LX/4zT;->a:LX/0qF;

    invoke-interface {v0, v1}, LX/0qF;->setNextEvictable(LX/0qF;)V

    .line 822820
    iget-object v0, p0, LX/4zT;->a:LX/0qF;

    iget-object v1, p0, LX/4zT;->a:LX/0qF;

    invoke-interface {v0, v1}, LX/0qF;->setPreviousEvictable(LX/0qF;)V

    .line 822821
    return-void
.end method

.method public final contains(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 822812
    check-cast p1, LX/0qF;

    .line 822813
    invoke-interface {p1}, LX/0qF;->getNextEvictable()LX/0qF;

    move-result-object v0

    sget-object v1, LX/4zX;->INSTANCE:LX/4zX;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isEmpty()Z
    .locals 2

    .prologue
    .line 822811
    iget-object v0, p0, LX/4zT;->a:LX/0qF;

    invoke-interface {v0}, LX/0qF;->getNextEvictable()LX/0qF;

    move-result-object v0

    iget-object v1, p0, LX/4zT;->a:LX/0qF;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "LX/0qF",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 822810
    new-instance v0, LX/4zS;

    invoke-direct {p0}, LX/4zT;->a()LX/0qF;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LX/4zS;-><init>(LX/4zT;LX/0qF;)V

    return-object v0
.end method

.method public final offer(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 822805
    check-cast p1, LX/0qF;

    .line 822806
    invoke-interface {p1}, LX/0qF;->getPreviousEvictable()LX/0qF;

    move-result-object v0

    invoke-interface {p1}, LX/0qF;->getNextEvictable()LX/0qF;

    move-result-object v1

    invoke-static {v0, v1}, LX/0cn;->b(LX/0qF;LX/0qF;)V

    .line 822807
    iget-object v0, p0, LX/4zT;->a:LX/0qF;

    invoke-interface {v0}, LX/0qF;->getPreviousEvictable()LX/0qF;

    move-result-object v0

    invoke-static {v0, p1}, LX/0cn;->b(LX/0qF;LX/0qF;)V

    .line 822808
    iget-object v0, p0, LX/4zT;->a:LX/0qF;

    invoke-static {p1, v0}, LX/0cn;->b(LX/0qF;LX/0qF;)V

    .line 822809
    const/4 v0, 0x1

    return v0
.end method

.method public final synthetic peek()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 822804
    invoke-direct {p0}, LX/4zT;->a()LX/0qF;

    move-result-object v0

    return-object v0
.end method

.method public final poll()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 822799
    iget-object v0, p0, LX/4zT;->a:LX/0qF;

    invoke-interface {v0}, LX/0qF;->getNextEvictable()LX/0qF;

    move-result-object v0

    .line 822800
    iget-object v1, p0, LX/4zT;->a:LX/0qF;

    if-ne v0, v1, :cond_0

    .line 822801
    const/4 v0, 0x0

    .line 822802
    :goto_0
    return-object v0

    .line 822803
    :cond_0
    invoke-virtual {p0, v0}, LX/4zT;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 822793
    check-cast p1, LX/0qF;

    .line 822794
    invoke-interface {p1}, LX/0qF;->getPreviousEvictable()LX/0qF;

    move-result-object v0

    .line 822795
    invoke-interface {p1}, LX/0qF;->getNextEvictable()LX/0qF;

    move-result-object v1

    .line 822796
    invoke-static {v0, v1}, LX/0cn;->b(LX/0qF;LX/0qF;)V

    .line 822797
    invoke-static {p1}, LX/0cn;->e(LX/0qF;)V

    .line 822798
    sget-object v0, LX/4zX;->INSTANCE:LX/4zX;

    if-eq v1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final size()I
    .locals 3

    .prologue
    .line 822788
    const/4 v1, 0x0

    .line 822789
    iget-object v0, p0, LX/4zT;->a:LX/0qF;

    invoke-interface {v0}, LX/0qF;->getNextEvictable()LX/0qF;

    move-result-object v0

    :goto_0
    iget-object v2, p0, LX/4zT;->a:LX/0qF;

    if-eq v0, v2, :cond_0

    .line 822790
    add-int/lit8 v1, v1, 0x1

    .line 822791
    invoke-interface {v0}, LX/0qF;->getNextEvictable()LX/0qF;

    move-result-object v0

    goto :goto_0

    .line 822792
    :cond_0
    return v1
.end method
