.class public LX/3pp;
.super LX/3po;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 641768
    invoke-direct {p0}, LX/3po;-><init>()V

    return-void
.end method


# virtual methods
.method public a(LX/2HB;)Landroid/app/Notification;
    .locals 23

    .prologue
    .line 641764
    new-instance v1, LX/3q6;

    move-object/from16 v0, p1

    iget-object v2, v0, LX/2HB;->a:Landroid/content/Context;

    move-object/from16 v0, p1

    iget-object v3, v0, LX/2HB;->B:Landroid/app/Notification;

    move-object/from16 v0, p1

    iget-object v4, v0, LX/2HB;->b:Ljava/lang/CharSequence;

    move-object/from16 v0, p1

    iget-object v5, v0, LX/2HB;->c:Ljava/lang/CharSequence;

    move-object/from16 v0, p1

    iget-object v6, v0, LX/2HB;->h:Ljava/lang/CharSequence;

    move-object/from16 v0, p1

    iget-object v7, v0, LX/2HB;->f:Landroid/widget/RemoteViews;

    move-object/from16 v0, p1

    iget v8, v0, LX/2HB;->i:I

    move-object/from16 v0, p1

    iget-object v9, v0, LX/2HB;->d:Landroid/app/PendingIntent;

    move-object/from16 v0, p1

    iget-object v10, v0, LX/2HB;->e:Landroid/app/PendingIntent;

    move-object/from16 v0, p1

    iget-object v11, v0, LX/2HB;->g:Landroid/graphics/Bitmap;

    move-object/from16 v0, p1

    iget v12, v0, LX/2HB;->o:I

    move-object/from16 v0, p1

    iget v13, v0, LX/2HB;->p:I

    move-object/from16 v0, p1

    iget-boolean v14, v0, LX/2HB;->q:Z

    move-object/from16 v0, p1

    iget-boolean v15, v0, LX/2HB;->l:Z

    move-object/from16 v0, p1

    iget v0, v0, LX/2HB;->j:I

    move/from16 v16, v0

    move-object/from16 v0, p1

    iget-object v0, v0, LX/2HB;->n:Ljava/lang/CharSequence;

    move-object/from16 v17, v0

    move-object/from16 v0, p1

    iget-boolean v0, v0, LX/2HB;->v:Z

    move/from16 v18, v0

    move-object/from16 v0, p1

    iget-object v0, v0, LX/2HB;->x:Landroid/os/Bundle;

    move-object/from16 v19, v0

    move-object/from16 v0, p1

    iget-object v0, v0, LX/2HB;->r:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, p1

    iget-boolean v0, v0, LX/2HB;->s:Z

    move/from16 v21, v0

    move-object/from16 v0, p1

    iget-object v0, v0, LX/2HB;->t:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-direct/range {v1 .. v22}, LX/3q6;-><init>(Landroid/content/Context;Landroid/app/Notification;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/widget/RemoteViews;ILandroid/app/PendingIntent;Landroid/app/PendingIntent;Landroid/graphics/Bitmap;IIZZILjava/lang/CharSequence;ZLandroid/os/Bundle;Ljava/lang/String;ZLjava/lang/String;)V

    .line 641765
    move-object/from16 v0, p1

    iget-object v2, v0, LX/2HB;->u:Ljava/util/ArrayList;

    invoke-static {v1, v2}, LX/3px;->b(LX/3pT;Ljava/util/ArrayList;)V

    .line 641766
    move-object/from16 v0, p1

    iget-object v2, v0, LX/2HB;->m:LX/3pc;

    invoke-static {v1, v2}, LX/3px;->b(LX/3pU;LX/3pc;)V

    .line 641767
    invoke-virtual {v1}, LX/3q6;->b()Landroid/app/Notification;

    move-result-object v1

    return-object v1
.end method

.method public a(Landroid/app/Notification;)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 641748
    invoke-static {p1}, LX/3q7;->a(Landroid/app/Notification;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public a([LX/3pb;)Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "LX/3pb;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/Parcelable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 641749
    if-nez p1, :cond_1

    .line 641750
    const/4 v0, 0x0

    .line 641751
    :cond_0
    move-object v0, v0

    .line 641752
    return-object v0

    .line 641753
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    array-length v1, p1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 641754
    array-length v2, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, p1, v1

    .line 641755
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 641756
    const-string v5, "icon"

    invoke-virtual {v3}, LX/3pa;->a()I

    move-result p0

    invoke-virtual {v4, v5, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 641757
    const-string v5, "title"

    invoke-virtual {v3}, LX/3pa;->b()Ljava/lang/CharSequence;

    move-result-object p0

    invoke-virtual {v4, v5, p0}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 641758
    const-string v5, "actionIntent"

    invoke-virtual {v3}, LX/3pa;->c()Landroid/app/PendingIntent;

    move-result-object p0

    invoke-virtual {v4, v5, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 641759
    const-string v5, "extras"

    invoke-virtual {v3}, LX/3pa;->d()Landroid/os/Bundle;

    move-result-object p0

    invoke-virtual {v4, v5, p0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 641760
    const-string v5, "remoteInputs"

    invoke-virtual {v3}, LX/3pa;->e()[LX/3qK;

    move-result-object p0

    invoke-static {p0}, LX/3qO;->a([LX/3qK;)[Landroid/os/Bundle;

    move-result-object p0

    invoke-virtual {v4, v5, p0}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    .line 641761
    move-object v3, v4

    .line 641762
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 641763
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
