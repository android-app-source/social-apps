.class public final LX/3ys;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/0Px",
        "<",
        "LX/3yQ;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;Z)V
    .locals 0

    .prologue
    .line 661749
    iput-object p1, p0, LX/3ys;->b:Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;

    iput-boolean p2, p0, LX/3ys;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(LX/0Px;)V
    .locals 2
    .param p1    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/3yQ;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 661750
    iget-object v0, p0, LX/3ys;->b:Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;

    .line 661751
    iput-object p1, v0, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->o:LX/0Px;

    .line 661752
    iget-object v0, p0, LX/3ys;->b:Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;

    iget-boolean v1, p0, LX/3ys;->a:Z

    .line 661753
    invoke-static {v0, v1}, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->b$redex0(Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;Z)V

    .line 661754
    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 661755
    sget-object v0, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->j:Ljava/lang/Class;

    const-string v1, "Could not cache experiments for QE list activity"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 661756
    iget-object v0, p0, LX/3ys;->b:Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;

    const/4 v1, 0x0

    .line 661757
    iput-object v1, v0, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->o:LX/0Px;

    .line 661758
    iget-object v0, p0, LX/3ys;->b:Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;

    iget-boolean v1, p0, LX/3ys;->a:Z

    .line 661759
    invoke-static {v0, v1}, Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;->b$redex0(Lcom/facebook/abtest/qe/settings/QuickExperimentListActivity;Z)V

    .line 661760
    return-void
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 661761
    check-cast p1, LX/0Px;

    invoke-direct {p0, p1}, LX/3ys;->a(LX/0Px;)V

    return-void
.end method
