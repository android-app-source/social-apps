.class public LX/4ok;
.super LX/4oj;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 810356
    invoke-direct {p0, p1}, LX/4oj;-><init>(Landroid/content/Context;)V

    .line 810357
    return-void
.end method


# virtual methods
.method public final setWidgetLayoutResource(I)V
    .locals 5

    .prologue
    .line 810358
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 810359
    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    .line 810360
    invoke-virtual {p0}, LX/4ok;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v3

    const v4, 0x7f0103ab

    invoke-virtual {v3, v4, v2, v0}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 810361
    iget v2, v2, Landroid/util/TypedValue;->data:I

    sget-object v3, LX/4oh;->SWITCH_COMPAT:LX/4oh;

    invoke-virtual {v3}, LX/4oh;->ordinal()I

    move-result v3

    if-ne v2, v3, :cond_1

    .line 810362
    :goto_0
    move v0, v0

    .line 810363
    if-eqz v0, :cond_0

    .line 810364
    invoke-super {p0, p1}, LX/4oj;->setWidgetLayoutResource(I)V

    .line 810365
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 810366
    goto :goto_0

    :cond_2
    move v0, v1

    .line 810367
    goto :goto_0
.end method
