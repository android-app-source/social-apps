.class public LX/4gp;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 800179
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 800180
    iput-object p1, p0, LX/4gp;->a:LX/0Or;

    .line 800181
    return-void
.end method

.method public static a(LX/0QB;)LX/4gp;
    .locals 2

    .prologue
    .line 800176
    new-instance v0, LX/4gp;

    const/16 v1, 0xdf4

    invoke-static {p0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-direct {v0, v1}, LX/4gp;-><init>(LX/0Or;)V

    .line 800177
    move-object v0, v0

    .line 800178
    return-object v0
.end method

.method private static a(LX/0W4;J)Ljava/lang/String;
    .locals 3

    .prologue
    .line 800168
    invoke-static {p1, p2}, LX/0X6;->c(J)LX/0oE;

    move-result-object v1

    .line 800169
    const-string v0, "UNSUPPORTED TYPE"

    .line 800170
    sget-object v2, LX/4go;->a:[I

    invoke-virtual {v1}, LX/0oE;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    .line 800171
    :goto_0
    return-object v0

    .line 800172
    :pswitch_0
    invoke-interface {p0, p1, p2}, LX/0W4;->b(J)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 800173
    :pswitch_1
    invoke-interface {p0, p1, p2}, LX/0W4;->d(J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 800174
    :pswitch_2
    invoke-interface {p0, p1, p2}, LX/0W4;->h(J)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 800175
    :pswitch_3
    invoke-interface {p0, p1, p2}, LX/0W4;->f(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public final a(Ljava/lang/String;ZLX/0Px;)Ljava/lang/String;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z",
            "LX/0Px",
            "<",
            "LX/4gs;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 800154
    iget-object v0, p0, LX/4gp;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0W3;

    .line 800155
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0W3;->a(I)LX/0W4;

    move-result-object v4

    .line 800156
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    .line 800157
    invoke-virtual {p3}, LX/0Px;->size()I

    move-result v6

    const/4 v0, 0x0

    move v3, v0

    :goto_0
    if-ge v3, v6, :cond_5

    invoke-virtual {p3, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4gs;

    .line 800158
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    .line 800159
    :goto_1
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    .line 800160
    :goto_2
    if-nez v1, :cond_0

    if-eqz v2, :cond_1

    .line 800161
    :cond_0
    if-eqz p2, :cond_4

    .line 800162
    const-string v1, "%s_%s (%d) = %s\n"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, v0, LX/4gs;->a:Ljava/lang/String;

    aput-object v8, v2, v7

    const/4 v7, 0x1

    iget-object v8, v0, LX/4gs;->b:Ljava/lang/String;

    aput-object v8, v2, v7

    const/4 v7, 0x2

    invoke-virtual {v0}, LX/4gs;->a()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v2, v7

    const/4 v7, 0x3

    invoke-virtual {v0}, LX/4gs;->a()J

    move-result-wide v8

    invoke-static {v4, v8, v9}, LX/4gp;->a(LX/0W4;J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v7

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 800163
    :cond_1
    :goto_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 800164
    :cond_2
    iget-object v1, v0, LX/4gs;->a:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    goto :goto_1

    .line 800165
    :cond_3
    iget-object v2, v0, LX/4gs;->b:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    goto :goto_2

    .line 800166
    :cond_4
    const-string v1, "%s_%s = %s\n"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, v0, LX/4gs;->a:Ljava/lang/String;

    aput-object v8, v2, v7

    const/4 v7, 0x1

    iget-object v8, v0, LX/4gs;->b:Ljava/lang/String;

    aput-object v8, v2, v7

    const/4 v7, 0x2

    invoke-virtual {v0}, LX/4gs;->a()J

    move-result-wide v8

    invoke-static {v4, v8, v9}, LX/4gp;->a(LX/0W4;J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v7

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_3

    .line 800167
    :cond_5
    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
