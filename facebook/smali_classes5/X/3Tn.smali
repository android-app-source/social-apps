.class public LX/3Tn;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/3U5;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 584800
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 584801
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;LX/1PT;LX/1SX;Ljava/lang/Runnable;Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;LX/3Tv;LX/3Tu;LX/2jY;LX/1PY;LX/3Tt;LX/1QW;)LX/3U5;
    .locals 24

    .prologue
    .line 584802
    new-instance v1, LX/3U5;

    const-class v2, LX/3UA;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v13

    check-cast v13, LX/3UA;

    const-class v2, LX/3UB;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v14

    check-cast v14, LX/3UB;

    const-class v2, LX/2kq;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v15

    check-cast v15, LX/2kq;

    const-class v2, LX/2kr;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v16

    check-cast v16, LX/2kr;

    invoke-static/range {p0 .. p0}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a(LX/0QB;)Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    move-result-object v17

    check-cast v17, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    invoke-static/range {p0 .. p0}, LX/0gh;->a(LX/0QB;)LX/0gh;

    move-result-object v18

    check-cast v18, LX/0gh;

    invoke-static/range {p0 .. p0}, LX/2Yg;->a(LX/0QB;)LX/2Yg;

    move-result-object v19

    check-cast v19, LX/2Yg;

    invoke-static/range {p0 .. p0}, LX/1rn;->a(LX/0QB;)LX/1rn;

    move-result-object v20

    check-cast v20, LX/1rn;

    invoke-static/range {p0 .. p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v21

    check-cast v21, LX/0Sh;

    invoke-static/range {p0 .. p0}, LX/0WG;->a(LX/0QB;)LX/0SI;

    move-result-object v22

    check-cast v22, LX/0SI;

    const-class v2, LX/2ks;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v23

    check-cast v23, LX/2ks;

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    invoke-direct/range {v1 .. v23}, LX/3U5;-><init>(Landroid/content/Context;LX/1PT;LX/1SX;Ljava/lang/Runnable;Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;LX/3Tv;LX/3Tu;LX/2jY;LX/1PY;LX/3Tt;LX/1QW;LX/3UA;LX/3UB;LX/2kq;LX/2kr;Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;LX/0gh;LX/2Yg;LX/1rn;LX/0Sh;LX/0SI;LX/2ks;)V

    .line 584803
    return-object v1
.end method
