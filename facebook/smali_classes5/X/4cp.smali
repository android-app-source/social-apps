.class public abstract LX/4cp;
.super LX/4cO;
.source ""


# instance fields
.field private final b:J

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;JLjava/lang/String;)V
    .locals 0

    .prologue
    .line 795339
    invoke-direct {p0, p1}, LX/4cO;-><init>(Ljava/lang/String;)V

    .line 795340
    iput-wide p2, p0, LX/4cp;->b:J

    .line 795341
    iput-object p4, p0, LX/4cp;->c:Ljava/lang/String;

    .line 795342
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 795338
    iget-object v0, p0, LX/4cp;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Ljava/io/OutputStream;)V
    .locals 8

    .prologue
    .line 795330
    new-instance v0, LX/1iJ;

    new-instance v1, LX/4d4;

    iget-wide v2, p0, LX/4cp;->b:J

    invoke-direct {v1, p1, v2, v3}, LX/4d4;-><init>(Ljava/io/OutputStream;J)V

    invoke-direct {v0, v1}, LX/1iJ;-><init>(Ljava/io/OutputStream;)V

    .line 795331
    invoke-virtual {p0, v0}, LX/4cp;->b(Ljava/io/OutputStream;)V

    .line 795332
    iget-wide v6, v0, LX/1iJ;->a:J

    move-wide v0, v6

    .line 795333
    iget-wide v2, p0, LX/4cp;->b:J

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    .line 795334
    new-instance v2, LX/4d2;

    iget-wide v4, p0, LX/4cp;->b:J

    invoke-direct {v2, v0, v1, v4, v5}, LX/4d2;-><init>(JJ)V

    throw v2

    .line 795335
    :cond_0
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 795343
    const/4 v0, 0x0

    return-object v0
.end method

.method public abstract b(Ljava/io/OutputStream;)V
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 795337
    const-string v0, "binary"

    return-object v0
.end method

.method public final d()J
    .locals 2

    .prologue
    .line 795336
    iget-wide v0, p0, LX/4cp;->b:J

    return-wide v0
.end method
