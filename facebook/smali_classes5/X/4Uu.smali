.class public final LX/4Uu;
.super LX/4Ut;
.source ""


# instance fields
.field public final synthetic a:LX/15i;

.field public final synthetic b:LX/0t8;

.field public final synthetic c:I

.field public final synthetic d:LX/4Ud;

.field public final synthetic e:LX/4Uy;

.field public final synthetic f:LX/4Uw;

.field public final synthetic g:[[I


# direct methods
.method public constructor <init>(LX/15i;LX/0t8;ILX/4Ud;LX/4Uy;LX/4Uw;[[I)V
    .locals 1

    .prologue
    .line 741795
    iput-object p1, p0, LX/4Uu;->a:LX/15i;

    iput-object p2, p0, LX/4Uu;->b:LX/0t8;

    iput p3, p0, LX/4Uu;->c:I

    iput-object p4, p0, LX/4Uu;->d:LX/4Ud;

    iput-object p5, p0, LX/4Uu;->e:LX/4Uy;

    iput-object p6, p0, LX/4Uu;->f:LX/4Uw;

    iput-object p7, p0, LX/4Uu;->g:[[I

    invoke-direct {p0}, LX/4Ut;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 9

    .prologue
    .line 741796
    instance-of v0, p1, Ljava/lang/Integer;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 741797
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 741798
    if-ltz v6, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 741799
    iget-boolean v0, p0, LX/4Ut;->b:Z

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 741800
    iget v0, p0, LX/4Ut;->a:I

    move v0, v0

    .line 741801
    invoke-static {v0}, LX/4V0;->d(I)Z

    move-result v7

    .line 741802
    if-lez v6, :cond_0

    .line 741803
    iget-object v0, p0, LX/4Uu;->a:LX/15i;

    iget-object v1, p0, LX/4Uu;->b:LX/0t8;

    iget v2, p0, LX/4Uu;->c:I

    iget-object v3, p0, LX/4Uu;->d:LX/4Ud;

    iget-object v4, p0, LX/4Uu;->e:LX/4Uy;

    iget-object v5, p0, LX/4Uu;->f:LX/4Uw;

    iget-object v8, p0, LX/4Uu;->g:[[I

    .line 741804
    invoke-static/range {v0 .. v8}, LX/4V0;->b(LX/15i;LX/0t8;ILX/4Ud;LX/4Uy;LX/4Uw;IZ[[I)V

    .line 741805
    :cond_0
    return-void

    .line 741806
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
