.class public LX/3f3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/26t;


# instance fields
.field public a:LX/3f4;

.field public b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/3f4;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3f4;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 621254
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 621255
    iput-object p1, p0, LX/3f3;->a:LX/3f4;

    .line 621256
    iput-object p2, p0, LX/3f3;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 621257
    iput-object p3, p0, LX/3f3;->c:LX/0Ot;

    .line 621258
    return-void
.end method

.method public static b(LX/0QB;)LX/3f3;
    .locals 4

    .prologue
    .line 621259
    new-instance v2, LX/3f3;

    invoke-static {p0}, LX/3f4;->a(LX/0QB;)LX/3f4;

    move-result-object v0

    check-cast v0, LX/3f4;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v3, 0x259

    invoke-static {p0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-direct {v2, v0, v1, v3}, LX/3f3;-><init>(LX/3f4;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;)V

    .line 621260
    return-object v2
.end method


# virtual methods
.method public final handleOperation(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 10

    .prologue
    .line 621261
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 621262
    const-string v1, "admined_pages_prefetch"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 621263
    iget-object v2, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v2, v2

    .line 621264
    const-string v3, "loadAdminedPagesParam"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/facebook/pages/adminedpages/service/LoadAdminedPagesParams;

    .line 621265
    iget-object v2, v2, Lcom/facebook/pages/adminedpages/service/LoadAdminedPagesParams;->a:LX/0rS;

    .line 621266
    sget-object v3, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    if-eq v2, v3, :cond_1

    .line 621267
    iget-object v2, p0, LX/3f3;->a:LX/3f4;

    invoke-virtual {v2}, LX/3f4;->a()Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v2

    .line 621268
    :goto_0
    move-object v0, v2

    .line 621269
    :goto_1
    return-object v0

    :cond_0
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_1

    .line 621270
    :cond_1
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v4

    .line 621271
    invoke-virtual {v4}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableListNullOk()Ljava/util/ArrayList;

    move-result-object v3

    .line 621272
    if-eqz v3, :cond_2

    .line 621273
    const/4 v2, 0x0

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchMethod$Result;

    .line 621274
    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/pages/adminedpages/protocol/PagesAccessTokenPrefetchMethod$Result;

    .line 621275
    iget-object v5, p0, LX/3f3;->a:LX/3f4;

    invoke-virtual {v5, v2, v3}, LX/3f4;->a(Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchMethod$Result;Lcom/facebook/pages/adminedpages/protocol/PagesAccessTokenPrefetchMethod$Result;)V

    .line 621276
    iget-object v3, p0, LX/3f3;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v3

    sget-object v5, LX/3ex;->b:LX/0Tn;

    .line 621277
    iget-wide v8, v2, Lcom/facebook/fbservice/results/BaseResult;->clientTimeMs:J

    move-wide v6, v8

    .line 621278
    invoke-interface {v3, v5, v6, v7}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    :goto_2
    move-object v2, v4

    .line 621279
    goto :goto_0

    .line 621280
    :cond_2
    iget-object v2, p0, LX/3f3;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/03V;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    const-string v5, "Admined pages prefetch result is null."

    invoke-virtual {v2, v3, v5}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method
