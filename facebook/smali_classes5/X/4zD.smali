.class public LX/4zD;
.super LX/0Xt;
.source ""

# interfaces
.implements LX/0Xv;
.implements Ljava/io/Serializable;


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LX/0Xt",
        "<TK;TV;>;",
        "LX/0Xv",
        "<TK;TV;>;",
        "Ljava/io/Serializable;"
    }
.end annotation


# instance fields
.field public transient a:LX/4zA;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4zA",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public transient b:LX/4zA;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4zA",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public transient c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<TK;",
            "LX/4z9",
            "<TK;TV;>;>;"
        }
    .end annotation
.end field

.field public transient d:I

.field public transient e:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 822504
    invoke-direct {p0}, LX/0Xt;-><init>()V

    .line 822505
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/4zD;->c:Ljava/util/Map;

    .line 822506
    return-void
.end method

.method public static a$redex0(LX/4zD;Ljava/lang/Object;Ljava/lang/Object;LX/4zA;)LX/4zA;
    .locals 3
    .param p0    # LX/4zD;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;",
            "LX/4zA",
            "<TK;TV;>;)",
            "LX/4zA",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 822507
    new-instance v1, LX/4zA;

    invoke-direct {v1, p1, p2}, LX/4zA;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 822508
    iget-object v0, p0, LX/4zD;->a:LX/4zA;

    if-nez v0, :cond_0

    .line 822509
    iput-object v1, p0, LX/4zD;->b:LX/4zA;

    iput-object v1, p0, LX/4zD;->a:LX/4zA;

    .line 822510
    iget-object v0, p0, LX/4zD;->c:Ljava/util/Map;

    new-instance v2, LX/4z9;

    invoke-direct {v2, v1}, LX/4z9;-><init>(LX/4zA;)V

    invoke-interface {v0, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 822511
    iget v0, p0, LX/4zD;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/4zD;->e:I

    .line 822512
    :goto_0
    iget v0, p0, LX/4zD;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/4zD;->d:I

    .line 822513
    return-object v1

    .line 822514
    :cond_0
    if-nez p3, :cond_2

    .line 822515
    iget-object v0, p0, LX/4zD;->b:LX/4zA;

    iput-object v1, v0, LX/4zA;->c:LX/4zA;

    .line 822516
    iget-object v0, p0, LX/4zD;->b:LX/4zA;

    iput-object v0, v1, LX/4zA;->d:LX/4zA;

    .line 822517
    iput-object v1, p0, LX/4zD;->b:LX/4zA;

    .line 822518
    iget-object v0, p0, LX/4zD;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4z9;

    .line 822519
    if-nez v0, :cond_1

    .line 822520
    iget-object v0, p0, LX/4zD;->c:Ljava/util/Map;

    new-instance v2, LX/4z9;

    invoke-direct {v2, v1}, LX/4z9;-><init>(LX/4zA;)V

    invoke-interface {v0, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 822521
    iget v0, p0, LX/4zD;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/4zD;->e:I

    goto :goto_0

    .line 822522
    :cond_1
    iget v2, v0, LX/4z9;->c:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v0, LX/4z9;->c:I

    .line 822523
    iget-object v2, v0, LX/4z9;->b:LX/4zA;

    .line 822524
    iput-object v1, v2, LX/4zA;->e:LX/4zA;

    .line 822525
    iput-object v2, v1, LX/4zA;->f:LX/4zA;

    .line 822526
    iput-object v1, v0, LX/4z9;->b:LX/4zA;

    goto :goto_0

    .line 822527
    :cond_2
    iget-object v0, p0, LX/4zD;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4z9;

    .line 822528
    iget v2, v0, LX/4z9;->c:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v0, LX/4z9;->c:I

    .line 822529
    iget-object v0, p3, LX/4zA;->d:LX/4zA;

    iput-object v0, v1, LX/4zA;->d:LX/4zA;

    .line 822530
    iget-object v0, p3, LX/4zA;->f:LX/4zA;

    iput-object v0, v1, LX/4zA;->f:LX/4zA;

    .line 822531
    iput-object p3, v1, LX/4zA;->c:LX/4zA;

    .line 822532
    iput-object p3, v1, LX/4zA;->e:LX/4zA;

    .line 822533
    iget-object v0, p3, LX/4zA;->f:LX/4zA;

    if-nez v0, :cond_3

    .line 822534
    iget-object v0, p0, LX/4zD;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4z9;

    iput-object v1, v0, LX/4z9;->a:LX/4zA;

    .line 822535
    :goto_1
    iget-object v0, p3, LX/4zA;->d:LX/4zA;

    if-nez v0, :cond_4

    .line 822536
    iput-object v1, p0, LX/4zD;->a:LX/4zA;

    .line 822537
    :goto_2
    iput-object v1, p3, LX/4zA;->d:LX/4zA;

    .line 822538
    iput-object v1, p3, LX/4zA;->f:LX/4zA;

    goto :goto_0

    .line 822539
    :cond_3
    iget-object v0, p3, LX/4zA;->f:LX/4zA;

    iput-object v1, v0, LX/4zA;->e:LX/4zA;

    goto :goto_1

    .line 822540
    :cond_4
    iget-object v0, p3, LX/4zA;->d:LX/4zA;

    iput-object v1, v0, LX/4zA;->c:LX/4zA;

    goto :goto_2
.end method

.method public static a$redex0(LX/4zD;LX/4zA;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4zA",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 822541
    iget-object v0, p1, LX/4zA;->d:LX/4zA;

    if-eqz v0, :cond_0

    .line 822542
    iget-object v0, p1, LX/4zA;->d:LX/4zA;

    iget-object v1, p1, LX/4zA;->c:LX/4zA;

    iput-object v1, v0, LX/4zA;->c:LX/4zA;

    .line 822543
    :goto_0
    iget-object v0, p1, LX/4zA;->c:LX/4zA;

    if-eqz v0, :cond_1

    .line 822544
    iget-object v0, p1, LX/4zA;->c:LX/4zA;

    iget-object v1, p1, LX/4zA;->d:LX/4zA;

    iput-object v1, v0, LX/4zA;->d:LX/4zA;

    .line 822545
    :goto_1
    iget-object v0, p1, LX/4zA;->f:LX/4zA;

    if-nez v0, :cond_2

    iget-object v0, p1, LX/4zA;->e:LX/4zA;

    if-nez v0, :cond_2

    .line 822546
    iget-object v0, p0, LX/4zD;->c:Ljava/util/Map;

    iget-object v1, p1, LX/4zA;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4z9;

    .line 822547
    const/4 v1, 0x0

    iput v1, v0, LX/4z9;->c:I

    .line 822548
    iget v0, p0, LX/4zD;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/4zD;->e:I

    .line 822549
    :goto_2
    iget v0, p0, LX/4zD;->d:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/4zD;->d:I

    .line 822550
    return-void

    .line 822551
    :cond_0
    iget-object v0, p1, LX/4zA;->c:LX/4zA;

    iput-object v0, p0, LX/4zD;->a:LX/4zA;

    goto :goto_0

    .line 822552
    :cond_1
    iget-object v0, p1, LX/4zA;->d:LX/4zA;

    iput-object v0, p0, LX/4zD;->b:LX/4zA;

    goto :goto_1

    .line 822553
    :cond_2
    iget-object v0, p0, LX/4zD;->c:Ljava/util/Map;

    iget-object v1, p1, LX/4zA;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4z9;

    .line 822554
    iget v1, v0, LX/4z9;->c:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, LX/4z9;->c:I

    .line 822555
    iget-object v1, p1, LX/4zA;->f:LX/4zA;

    if-nez v1, :cond_3

    .line 822556
    iget-object v1, p1, LX/4zA;->e:LX/4zA;

    iput-object v1, v0, LX/4z9;->a:LX/4zA;

    .line 822557
    :goto_3
    iget-object v1, p1, LX/4zA;->e:LX/4zA;

    if-nez v1, :cond_4

    .line 822558
    iget-object v1, p1, LX/4zA;->f:LX/4zA;

    iput-object v1, v0, LX/4z9;->b:LX/4zA;

    goto :goto_2

    .line 822559
    :cond_3
    iget-object v1, p1, LX/4zA;->f:LX/4zA;

    iget-object v2, p1, LX/4zA;->e:LX/4zA;

    iput-object v2, v1, LX/4zA;->e:LX/4zA;

    goto :goto_3

    .line 822560
    :cond_4
    iget-object v0, p1, LX/4zA;->e:LX/4zA;

    iget-object v1, p1, LX/4zA;->f:LX/4zA;

    iput-object v1, v0, LX/4zA;->f:LX/4zA;

    goto :goto_2
.end method

.method private c()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 822561
    invoke-super {p0}, LX/0Xt;->i()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method private e()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 822562
    invoke-super {p0}, LX/0Xt;->k()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public static h(LX/4zD;Ljava/lang/Object;)V
    .locals 1
    .param p0    # LX/4zD;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 822563
    new-instance v0, LX/4zC;

    invoke-direct {v0, p0, p1}, LX/4zC;-><init>(LX/4zD;Ljava/lang/Object;)V

    invoke-static {v0}, LX/0RZ;->h(Ljava/util/Iterator;)V

    .line 822564
    return-void
.end method

.method public static i(Ljava/lang/Object;)V
    .locals 1
    .param p0    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 822584
    if-nez p0, :cond_0

    .line 822585
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 822586
    :cond_0
    return-void
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 4
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "java.io.ObjectInputStream"
    .end annotation

    .prologue
    .line 822565
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 822566
    invoke-static {}, LX/0PM;->d()Ljava/util/LinkedHashMap;

    move-result-object v0

    iput-object v0, p0, LX/4zD;->c:Ljava/util/Map;

    .line 822567
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v1

    .line 822568
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 822569
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v2

    .line 822570
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v3

    .line 822571
    invoke-virtual {p0, v2, v3}, LX/0Xt;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 822572
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 822573
    :cond_0
    return-void
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 3
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "java.io.ObjectOutputStream"
    .end annotation

    .prologue
    .line 822574
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V

    .line 822575
    invoke-virtual {p0}, LX/4zD;->f()I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    .line 822576
    invoke-direct {p0}, LX/4zD;->e()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 822577
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 822578
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    goto :goto_0

    .line 822579
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Ljava/util/List;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)",
            "Ljava/util/List",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 822580
    new-instance v0, LX/4z3;

    invoke-direct {v0, p0, p1}, LX/4z3;-><init>(LX/4zD;Ljava/lang/Object;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)Z"
        }
    .end annotation

    .prologue
    .line 822581
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, LX/4zD;->a$redex0(LX/4zD;Ljava/lang/Object;Ljava/lang/Object;LX/4zA;)LX/4zA;

    .line 822582
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;)Ljava/util/List;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Ljava/util/List",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 822501
    new-instance v0, LX/4zC;

    invoke-direct {v0, p0, p1}, LX/4zC;-><init>(LX/4zD;Ljava/lang/Object;)V

    invoke-static {v0}, LX/0R9;->a(Ljava/util/Iterator;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    move-object v0, v0

    .line 822502
    invoke-static {p0, p1}, LX/4zD;->h(LX/4zD;Ljava/lang/Object;)V

    .line 822503
    return-object v0
.end method

.method public final synthetic c(Ljava/lang/Object;)Ljava/util/Collection;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 822583
    invoke-virtual {p0, p1}, LX/4zD;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d(Ljava/lang/Object;)Ljava/util/Collection;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 822483
    invoke-virtual {p0, p1}, LX/4zD;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 822490
    iget v0, p0, LX/4zD;->d:I

    return v0
.end method

.method public final f(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 822491
    iget-object v0, p0, LX/4zD;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final g()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 822484
    iput-object v0, p0, LX/4zD;->a:LX/4zA;

    .line 822485
    iput-object v0, p0, LX/4zD;->b:LX/4zA;

    .line 822486
    iget-object v0, p0, LX/4zD;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 822487
    const/4 v0, 0x0

    iput v0, p0, LX/4zD;->d:I

    .line 822488
    iget v0, p0, LX/4zD;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/4zD;->e:I

    .line 822489
    return-void
.end method

.method public final g(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 822492
    invoke-direct {p0}, LX/4zD;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final h()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 822493
    new-instance v0, LX/4z5;

    invoke-direct {v0, p0}, LX/4z5;-><init>(LX/4zD;)V

    return-object v0
.end method

.method public final synthetic i()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 822494
    invoke-direct {p0}, LX/4zD;->c()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic k()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 822495
    invoke-direct {p0}, LX/4zD;->e()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final l()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 822496
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "should never be called"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public final m()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<TK;",
            "Ljava/util/Collection",
            "<TV;>;>;"
        }
    .end annotation

    .prologue
    .line 822497
    new-instance v0, LX/50B;

    invoke-direct {v0, p0}, LX/50B;-><init>(LX/0Xu;)V

    return-object v0
.end method

.method public final n()Z
    .locals 1

    .prologue
    .line 822498
    iget-object v0, p0, LX/4zD;->a:LX/4zA;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final o()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 822499
    new-instance v0, LX/4z4;

    invoke-direct {v0, p0}, LX/4z4;-><init>(LX/4zD;)V

    return-object v0
.end method

.method public final s()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 822500
    new-instance v0, LX/4z7;

    invoke-direct {v0, p0}, LX/4z7;-><init>(LX/4zD;)V

    return-object v0
.end method
