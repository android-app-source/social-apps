.class public LX/3fr;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile g:LX/3fr;


# instance fields
.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2Jp;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3fh;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2RC;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6N7;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/2RE;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 623656
    const-class v0, LX/3fr;

    sput-object v0, LX/3fr;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/0Ot;LX/0Ot;LX/0Ot;LX/2RE;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/2Jp;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/3fh;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2RC;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/6N7;",
            ">;",
            "LX/2RE;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 623669
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 623670
    iput-object p1, p0, LX/3fr;->b:LX/0Or;

    .line 623671
    iput-object p2, p0, LX/3fr;->c:LX/0Ot;

    .line 623672
    iput-object p3, p0, LX/3fr;->d:LX/0Ot;

    .line 623673
    iput-object p4, p0, LX/3fr;->e:LX/0Ot;

    .line 623674
    iput-object p5, p0, LX/3fr;->f:LX/2RE;

    .line 623675
    return-void
.end method

.method public static a(LX/0QB;)LX/3fr;
    .locals 9

    .prologue
    .line 623676
    sget-object v0, LX/3fr;->g:LX/3fr;

    if-nez v0, :cond_1

    .line 623677
    const-class v1, LX/3fr;

    monitor-enter v1

    .line 623678
    :try_start_0
    sget-object v0, LX/3fr;->g:LX/3fr;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 623679
    if-eqz v2, :cond_0

    .line 623680
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 623681
    new-instance v3, LX/3fr;

    const/16 v4, 0x438

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0x408

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x41b

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x1a12

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {v0}, LX/2RE;->a(LX/0QB;)LX/2RE;

    move-result-object v8

    check-cast v8, LX/2RE;

    invoke-direct/range {v3 .. v8}, LX/3fr;-><init>(LX/0Or;LX/0Ot;LX/0Ot;LX/0Ot;LX/2RE;)V

    .line 623682
    move-object v0, v3

    .line 623683
    sput-object v0, LX/3fr;->g:LX/3fr;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 623684
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 623685
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 623686
    :cond_1
    sget-object v0, LX/3fr;->g:LX/3fr;

    return-object v0

    .line 623687
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 623688
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/2RR;)LX/6N1;
    .locals 1

    .prologue
    .line 623668
    iget-object v0, p0, LX/3fr;->f:LX/2RE;

    invoke-virtual {v0}, LX/2RE;->a()Ljava/util/Set;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, LX/3fr;->a(LX/2RR;Ljava/util/Set;)LX/6N1;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/2RR;Ljava/util/Set;)LX/6N1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2RR;",
            "Ljava/util/Set",
            "<",
            "LX/0PH;",
            ">;)",
            "LX/6N1;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 623657
    iget-object v0, p0, LX/3fr;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Jp;

    .line 623658
    sget-object v1, LX/6N4;->a:[I

    invoke-virtual {v0}, LX/2Jp;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 623659
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected contact storage mode "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 623660
    :pswitch_0
    iget-object v0, p0, LX/3fr;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2RC;

    sget-object v1, LX/2RV;->CONTACT:LX/2RV;

    invoke-virtual {v0, p1, v1, p2}, LX/2RC;->a(LX/2RR;LX/2RV;Ljava/util/Set;)Landroid/database/Cursor;

    move-result-object v2

    .line 623661
    if-eqz v2, :cond_0

    new-instance v1, LX/6N2;

    iget-object v0, p0, LX/3fr;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3fh;

    invoke-direct {v1, v2, v0}, LX/6N2;-><init>(Landroid/database/Cursor;LX/3fh;)V

    move-object v0, v1

    .line 623662
    :goto_0
    return-object v0

    .line 623663
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 623664
    :pswitch_1
    iget-object v0, p0, LX/3fr;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6N7;

    .line 623665
    iget-object v1, v0, LX/6N7;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6NX;

    sget-object v2, LX/6Nb;->CONTACT_QUERY:LX/6Nb;

    invoke-virtual {v1, v2}, LX/6NX;->a(LX/6Nb;)Lcom/facebook/omnistore/Collection;

    move-result-object v1

    .line 623666
    new-instance v2, LX/6N5;

    invoke-static {v0, p1, p2, v1}, LX/6N7;->a(LX/6N7;LX/2RR;Ljava/util/Set;Lcom/facebook/omnistore/Collection;)Lcom/facebook/omnistore/Cursor;

    move-result-object v1

    invoke-direct {v2, v1}, LX/6N5;-><init>(Lcom/facebook/omnistore/Cursor;)V

    move-object v0, v2

    .line 623667
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
