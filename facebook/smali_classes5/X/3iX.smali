.class public LX/3iX;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/0aq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0aq",
            "<",
            "Ljava/lang/String;",
            "LX/5Og;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 629855
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 629856
    new-instance v0, LX/0aq;

    const/16 v1, 0x10

    invoke-direct {v0, v1}, LX/0aq;-><init>(I)V

    iput-object v0, p0, LX/3iX;->a:LX/0aq;

    .line 629857
    return-void
.end method

.method public static a(LX/0QB;)LX/3iX;
    .locals 3

    .prologue
    .line 629858
    const-class v1, LX/3iX;

    monitor-enter v1

    .line 629859
    :try_start_0
    sget-object v0, LX/3iX;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 629860
    sput-object v2, LX/3iX;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 629861
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 629862
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 629863
    new-instance v0, LX/3iX;

    invoke-direct {v0}, LX/3iX;-><init>()V

    .line 629864
    move-object v0, v0

    .line 629865
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 629866
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/3iX;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 629867
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 629868
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static c(LX/3iX;Ljava/lang/String;)LX/5Og;
    .locals 2

    .prologue
    .line 629869
    iget-object v0, p0, LX/3iX;->a:LX/0aq;

    invoke-virtual {v0, p1}, LX/0aq;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5Og;

    .line 629870
    if-nez v0, :cond_0

    .line 629871
    new-instance v0, LX/5Og;

    invoke-direct {v0}, LX/5Og;-><init>()V

    .line 629872
    iget-object v1, p0, LX/3iX;->a:LX/0aq;

    invoke-virtual {v1, p1, v0}, LX/0aq;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 629873
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 629874
    invoke-static {p0, p1}, LX/3iX;->c(LX/3iX;Ljava/lang/String;)LX/5Og;

    move-result-object v0

    .line 629875
    iput-boolean p2, v0, LX/5Og;->a:Z

    .line 629876
    return-void
.end method

.method public final b(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;
    .locals 1

    .prologue
    .line 629877
    invoke-static {p0, p1}, LX/3iX;->c(LX/3iX;Ljava/lang/String;)LX/5Og;

    move-result-object v0

    .line 629878
    iget-object p0, v0, LX/5Og;->b:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    move-object v0, p0

    .line 629879
    return-object v0
.end method
