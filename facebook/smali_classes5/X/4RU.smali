.class public LX/4RU;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 708999
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 709000
    const/4 v0, 0x0

    .line 709001
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v4, :cond_6

    .line 709002
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 709003
    :goto_0
    return v1

    .line 709004
    :cond_0
    const-string v7, "suggestion_type"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 709005
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionType;

    move-result-object v0

    move-object v3, v0

    move v0, v2

    .line 709006
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_4

    .line 709007
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 709008
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 709009
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_1

    if-eqz v6, :cond_1

    .line 709010
    const-string v7, "place"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 709011
    invoke-static {p0, p1}, LX/2bc;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 709012
    :cond_2
    const-string v7, "session_id"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 709013
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 709014
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 709015
    :cond_4
    const/4 v6, 0x3

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 709016
    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 709017
    invoke-virtual {p1, v2, v4}, LX/186;->b(II)V

    .line 709018
    if-eqz v0, :cond_5

    .line 709019
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v3}, LX/186;->a(ILjava/lang/Enum;)V

    .line 709020
    :cond_5
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move-object v3, v0

    move v4, v1

    move v5, v1

    move v0, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 709021
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 709022
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 709023
    if-eqz v0, :cond_0

    .line 709024
    const-string v1, "place"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 709025
    invoke-static {p0, v0, p2, p3}, LX/2bc;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 709026
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 709027
    if-eqz v0, :cond_1

    .line 709028
    const-string v1, "session_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 709029
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 709030
    :cond_1
    invoke-virtual {p0, p1, v3, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 709031
    if-eqz v0, :cond_2

    .line 709032
    const-string v0, "suggestion_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 709033
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionType;

    invoke-virtual {p0, p1, v3, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 709034
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 709035
    return-void
.end method
