.class public final LX/4hy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/4hq;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/4hq;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 802270
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 802271
    iput-object p1, p0, LX/4hy;->a:LX/0QB;

    .line 802272
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 802273
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/4hy;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 802254
    packed-switch p2, :pswitch_data_0

    .line 802255
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 802256
    :pswitch_0
    invoke-static {p1}, LX/GwB;->a(LX/0QB;)LX/GwB;

    move-result-object v0

    .line 802257
    :goto_0
    return-object v0

    .line 802258
    :pswitch_1
    new-instance v2, Lcom/facebook/platform/auth/service/AKSeamlessLoginServiceHandler;

    const/16 v0, 0x2f88

    invoke-static {p1, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {p1}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v0

    check-cast v0, LX/0aG;

    const/16 v1, 0x19e

    invoke-static {p1, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p2

    invoke-static {p1}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v1

    check-cast v1, LX/0TD;

    invoke-direct {v2, p0, v0, p2, v1}, Lcom/facebook/platform/auth/service/AKSeamlessLoginServiceHandler;-><init>(LX/0Or;LX/0aG;LX/0Or;LX/0TD;)V

    .line 802259
    move-object v0, v2

    .line 802260
    goto :goto_0

    .line 802261
    :pswitch_2
    new-instance v1, LX/75q;

    const/16 v0, 0x2f89

    invoke-static {p1, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-static {p1}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v0

    check-cast v0, LX/0aG;

    invoke-direct {v1, v2, v0}, LX/75q;-><init>(LX/0Or;LX/0aG;)V

    .line 802262
    move-object v0, v1

    .line 802263
    goto :goto_0

    .line 802264
    :pswitch_3
    new-instance v0, LX/4hs;

    const/16 v1, 0x2f91

    invoke-static {p1, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-direct {v0, v1}, LX/4hs;-><init>(LX/0Or;)V

    .line 802265
    move-object v0, v0

    .line 802266
    goto :goto_0

    .line 802267
    :pswitch_4
    new-instance v1, LX/4hw;

    const/16 v0, 0x2f92

    invoke-static {p1, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-static {p1}, LX/25w;->a(LX/0QB;)LX/25w;

    move-result-object v0

    check-cast v0, LX/25w;

    invoke-direct {v1, v2, v0}, LX/4hw;-><init>(LX/0Or;LX/25w;)V

    .line 802268
    move-object v0, v1

    .line 802269
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 802253
    const/4 v0, 0x5

    return v0
.end method
