.class public LX/4RB;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 707972
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 707973
    const/4 v2, 0x0

    .line 707974
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v4, :cond_8

    .line 707975
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 707976
    :goto_0
    return v0

    .line 707977
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_6

    .line 707978
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 707979
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 707980
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_0

    if-eqz v7, :cond_0

    .line 707981
    const-string v8, "block_status"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 707982
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLBlockStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLBlockStatus;

    move-result-object v0

    move-object v6, v0

    move v0, v1

    goto :goto_1

    .line 707983
    :cond_1
    const-string v8, "country_code"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 707984
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 707985
    :cond_2
    const-string v8, "display_number"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 707986
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 707987
    :cond_3
    const-string v8, "national_number"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 707988
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 707989
    :cond_4
    const-string v8, "universal_number"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 707990
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 707991
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 707992
    :cond_6
    const/4 v7, 0x6

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 707993
    if-eqz v0, :cond_7

    .line 707994
    invoke-virtual {p1, v1, v6}, LX/186;->a(ILjava/lang/Enum;)V

    .line 707995
    :cond_7
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 707996
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 707997
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 707998
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 707999
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :cond_8
    move v3, v0

    move v4, v0

    move v5, v0

    move-object v6, v2

    move v2, v0

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 708000
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 708001
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->a(IIS)S

    move-result v0

    .line 708002
    if-eqz v0, :cond_0

    .line 708003
    const-string v0, "block_status"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 708004
    const-class v0, Lcom/facebook/graphql/enums/GraphQLBlockStatus;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLBlockStatus;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLBlockStatus;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 708005
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 708006
    if-eqz v0, :cond_1

    .line 708007
    const-string v1, "country_code"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 708008
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 708009
    :cond_1
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 708010
    if-eqz v0, :cond_2

    .line 708011
    const-string v1, "display_number"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 708012
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 708013
    :cond_2
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 708014
    if-eqz v0, :cond_3

    .line 708015
    const-string v1, "national_number"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 708016
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 708017
    :cond_3
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 708018
    if-eqz v0, :cond_4

    .line 708019
    const-string v1, "universal_number"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 708020
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 708021
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 708022
    return-void
.end method
