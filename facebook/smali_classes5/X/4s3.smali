.class public final LX/4s3;
.super LX/4s1;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private final _views:[Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>([Ljava/lang/Class;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 817291
    invoke-direct {p0}, LX/4s1;-><init>()V

    iput-object p1, p0, LX/4s3;->_views:[Ljava/lang/Class;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Class;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 817292
    iget-object v1, p0, LX/4s3;->_views:[Ljava/lang/Class;

    array-length v2, v1

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 817293
    iget-object v3, p0, LX/4s3;->_views:[Ljava/lang/Class;

    aget-object v3, v3, v1

    .line 817294
    if-eq p1, v3, :cond_0

    invoke-virtual {v3, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 817295
    :cond_0
    const/4 v0, 0x1

    .line 817296
    :cond_1
    return v0

    .line 817297
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
