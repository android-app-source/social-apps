.class public final LX/4wv;
.super LX/11f;
.source ""

# interfaces
.implements Ljava/util/ListIterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Xs",
        "<TK;TV;>.WrappedCollection.WrappedIterator;",
        "Ljava/util/ListIterator",
        "<TV;>;"
    }
.end annotation


# instance fields
.field public final synthetic d:LX/11d;


# direct methods
.method public constructor <init>(LX/11d;)V
    .locals 0

    .prologue
    .line 820603
    iput-object p1, p0, LX/4wv;->d:LX/11d;

    invoke-direct {p0, p1}, LX/11f;-><init>(LX/11e;)V

    return-void
.end method

.method public constructor <init>(LX/11d;I)V
    .locals 1

    .prologue
    .line 820600
    iput-object p1, p0, LX/4wv;->d:LX/11d;

    .line 820601
    invoke-virtual {p1}, LX/11d;->g()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/List;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LX/11f;-><init>(LX/11e;Ljava/util/Iterator;)V

    .line 820602
    return-void
.end method

.method private b()Ljava/util/ListIterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ListIterator",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 820597
    invoke-static {p0}, LX/11f;->b(LX/11f;)V

    .line 820598
    iget-object v0, p0, LX/11f;->a:Ljava/util/Iterator;

    move-object v0, v0

    .line 820599
    check-cast v0, Ljava/util/ListIterator;

    return-object v0
.end method


# virtual methods
.method public final add(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 820591
    iget-object v0, p0, LX/4wv;->d:LX/11d;

    invoke-virtual {v0}, LX/11d;->isEmpty()Z

    move-result v0

    .line 820592
    invoke-direct {p0}, LX/4wv;->b()Ljava/util/ListIterator;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/ListIterator;->add(Ljava/lang/Object;)V

    .line 820593
    iget-object v1, p0, LX/4wv;->d:LX/11d;

    iget-object v1, v1, LX/11d;->g:LX/0Xs;

    invoke-static {v1}, LX/0Xs;->c(LX/0Xs;)I

    .line 820594
    if-eqz v0, :cond_0

    .line 820595
    iget-object v0, p0, LX/4wv;->d:LX/11d;

    invoke-virtual {v0}, LX/11e;->d()V

    .line 820596
    :cond_0
    return-void
.end method

.method public final hasPrevious()Z
    .locals 1

    .prologue
    .line 820585
    invoke-direct {p0}, LX/4wv;->b()Ljava/util/ListIterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/ListIterator;->hasPrevious()Z

    move-result v0

    return v0
.end method

.method public final nextIndex()I
    .locals 1

    .prologue
    .line 820590
    invoke-direct {p0}, LX/4wv;->b()Ljava/util/ListIterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/ListIterator;->nextIndex()I

    move-result v0

    return v0
.end method

.method public final previous()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .prologue
    .line 820589
    invoke-direct {p0}, LX/4wv;->b()Ljava/util/ListIterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final previousIndex()I
    .locals 1

    .prologue
    .line 820588
    invoke-direct {p0}, LX/4wv;->b()Ljava/util/ListIterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/ListIterator;->previousIndex()I

    move-result v0

    return v0
.end method

.method public final set(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 820586
    invoke-direct {p0}, LX/4wv;->b()Ljava/util/ListIterator;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/ListIterator;->set(Ljava/lang/Object;)V

    .line 820587
    return-void
.end method
