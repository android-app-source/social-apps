.class public final LX/3lS;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const-wide/16 v4, 0x0

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 633949
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_8

    .line 633950
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 633951
    :goto_0
    return v1

    .line 633952
    :cond_0
    const-string v12, "eligible_for_guardrail"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 633953
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v7

    move v9, v7

    move v7, v6

    .line 633954
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_5

    .line 633955
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 633956
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 633957
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_1

    if-eqz v11, :cond_1

    .line 633958
    const-string v12, "current_privacy_option"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 633959
    invoke-static {p0, p1}, LX/4iA;->a(LX/15w;LX/186;)I

    move-result v10

    goto :goto_1

    .line 633960
    :cond_2
    const-string v12, "suggested_option_timestamp"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 633961
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    move v0, v6

    goto :goto_1

    .line 633962
    :cond_3
    const-string v12, "suggested_privacy_option"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 633963
    invoke-static {p0, p1}, LX/4iA;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 633964
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 633965
    :cond_5
    const/4 v11, 0x4

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 633966
    invoke-virtual {p1, v1, v10}, LX/186;->b(II)V

    .line 633967
    if-eqz v7, :cond_6

    .line 633968
    invoke-virtual {p1, v6, v9}, LX/186;->a(IZ)V

    .line 633969
    :cond_6
    if-eqz v0, :cond_7

    .line 633970
    const/4 v1, 0x2

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 633971
    :cond_7
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 633972
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_8
    move v0, v1

    move v7, v1

    move v8, v1

    move-wide v2, v4

    move v9, v1

    move v10, v1

    goto :goto_1
.end method
