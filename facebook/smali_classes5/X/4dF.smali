.class public LX/4dF;
.super LX/4dC;
.source ""

# interfaces
.implements LX/4dD;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ScheduledExecutorService;LX/4dH;LX/4dI;LX/0So;)V
    .locals 0

    .prologue
    .line 795706
    invoke-direct {p0, p1, p2, p3, p4}, LX/4dC;-><init>(Ljava/util/concurrent/ScheduledExecutorService;LX/4dH;LX/4dI;LX/0So;)V

    .line 795707
    return-void
.end method


# virtual methods
.method public final a(I)Landroid/animation/ValueAnimator;
    .locals 8

    .prologue
    .line 795708
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 795709
    iget-object v3, p0, LX/4dC;->l:LX/4dH;

    move-object v3, v3

    .line 795710
    invoke-interface {v3}, LX/4dG;->d()I

    move-result v3

    .line 795711
    new-instance v4, Landroid/animation/ValueAnimator;

    invoke-direct {v4}, Landroid/animation/ValueAnimator;-><init>()V

    .line 795712
    const/4 v5, 0x2

    new-array v5, v5, [I

    aput v6, v5, v6

    .line 795713
    iget v6, p0, LX/4dC;->e:I

    move v6, v6

    .line 795714
    aput v6, v5, v7

    invoke-virtual {v4, v5}, Landroid/animation/ValueAnimator;->setIntValues([I)V

    .line 795715
    iget v5, p0, LX/4dC;->e:I

    move v5, v5

    .line 795716
    int-to-long v5, v5

    invoke-virtual {v4, v5, v6}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 795717
    if-eqz v3, :cond_0

    :goto_0
    invoke-virtual {v4, v3}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 795718
    invoke-virtual {v4, v7}, Landroid/animation/ValueAnimator;->setRepeatMode(I)V

    .line 795719
    new-instance v3, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v3}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v4, v3}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 795720
    new-instance v3, LX/4dE;

    invoke-direct {v3, p0}, LX/4dE;-><init>(LX/4dF;)V

    move-object v3, v3

    .line 795721
    invoke-virtual {v4, v3}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 795722
    move-object v0, v4

    .line 795723
    iget-object v1, p0, LX/4dC;->l:LX/4dH;

    move-object v1, v1

    .line 795724
    invoke-interface {v1}, LX/4dG;->b()I

    move-result v1

    div-int v1, p1, v1

    const/4 v2, 0x1

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 795725
    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 795726
    return-object v0

    .line 795727
    :cond_0
    const/4 v3, -0x1

    goto :goto_0
.end method
