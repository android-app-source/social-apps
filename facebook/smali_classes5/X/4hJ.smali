.class public LX/4hJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Externalizable;


# instance fields
.field public domesticCarrierCodeFormattingRule_:Ljava/lang/String;

.field public format_:Ljava/lang/String;

.field public hasDomesticCarrierCodeFormattingRule:Z

.field public hasFormat:Z

.field public hasNationalPrefixFormattingRule:Z

.field public hasNationalPrefixOptionalWhenFormatting:Z

.field public hasPattern:Z

.field private leadingDigitsPattern_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public nationalPrefixFormattingRule_:Ljava/lang/String;

.field public nationalPrefixOptionalWhenFormatting_:Z

.field public pattern_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 800893
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 800894
    const-string v0, ""

    iput-object v0, p0, LX/4hJ;->pattern_:Ljava/lang/String;

    .line 800895
    const-string v0, ""

    iput-object v0, p0, LX/4hJ;->format_:Ljava/lang/String;

    .line 800896
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/4hJ;->leadingDigitsPattern_:Ljava/util/List;

    .line 800897
    const-string v0, ""

    iput-object v0, p0, LX/4hJ;->nationalPrefixFormattingRule_:Ljava/lang/String;

    .line 800898
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/4hJ;->nationalPrefixOptionalWhenFormatting_:Z

    .line 800899
    const-string v0, ""

    iput-object v0, p0, LX/4hJ;->domesticCarrierCodeFormattingRule_:Ljava/lang/String;

    .line 800900
    return-void
.end method

.method public static newBuilder()LX/4hK;
    .locals 1

    .prologue
    .line 800892
    new-instance v0, LX/4hK;

    invoke-direct {v0}, LX/4hK;-><init>()V

    return-object v0
.end method


# virtual methods
.method public getLeadingDigitsPattern(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 800901
    iget-object v0, p0, LX/4hJ;->leadingDigitsPattern_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public leadingDigitsPatternSize()I
    .locals 1

    .prologue
    .line 800891
    iget-object v0, p0, LX/4hJ;->leadingDigitsPattern_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public readExternal(Ljava/io/ObjectInput;)V
    .locals 4

    .prologue
    .line 800869
    invoke-interface {p1}, Ljava/io/ObjectInput;->readUTF()Ljava/lang/String;

    move-result-object v0

    .line 800870
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/4hJ;->hasPattern:Z

    .line 800871
    iput-object v0, p0, LX/4hJ;->pattern_:Ljava/lang/String;

    .line 800872
    invoke-interface {p1}, Ljava/io/ObjectInput;->readUTF()Ljava/lang/String;

    move-result-object v0

    .line 800873
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/4hJ;->hasFormat:Z

    .line 800874
    iput-object v0, p0, LX/4hJ;->format_:Ljava/lang/String;

    .line 800875
    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v1

    .line 800876
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 800877
    iget-object v2, p0, LX/4hJ;->leadingDigitsPattern_:Ljava/util/List;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readUTF()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 800878
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 800879
    :cond_0
    invoke-interface {p1}, Ljava/io/ObjectInput;->readBoolean()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 800880
    invoke-interface {p1}, Ljava/io/ObjectInput;->readUTF()Ljava/lang/String;

    move-result-object v0

    .line 800881
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/4hJ;->hasNationalPrefixFormattingRule:Z

    .line 800882
    iput-object v0, p0, LX/4hJ;->nationalPrefixFormattingRule_:Ljava/lang/String;

    .line 800883
    :cond_1
    invoke-interface {p1}, Ljava/io/ObjectInput;->readBoolean()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 800884
    invoke-interface {p1}, Ljava/io/ObjectInput;->readUTF()Ljava/lang/String;

    move-result-object v0

    .line 800885
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/4hJ;->hasDomesticCarrierCodeFormattingRule:Z

    .line 800886
    iput-object v0, p0, LX/4hJ;->domesticCarrierCodeFormattingRule_:Ljava/lang/String;

    .line 800887
    :cond_2
    invoke-interface {p1}, Ljava/io/ObjectInput;->readBoolean()Z

    move-result v0

    .line 800888
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/4hJ;->hasNationalPrefixOptionalWhenFormatting:Z

    .line 800889
    iput-boolean v0, p0, LX/4hJ;->nationalPrefixOptionalWhenFormatting_:Z

    .line 800890
    return-void
.end method

.method public writeExternal(Ljava/io/ObjectOutput;)V
    .locals 3

    .prologue
    .line 800854
    iget-object v0, p0, LX/4hJ;->pattern_:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeUTF(Ljava/lang/String;)V

    .line 800855
    iget-object v0, p0, LX/4hJ;->format_:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeUTF(Ljava/lang/String;)V

    .line 800856
    invoke-virtual {p0}, LX/4hJ;->leadingDigitsPatternSize()I

    move-result v2

    .line 800857
    invoke-interface {p1, v2}, Ljava/io/ObjectOutput;->writeInt(I)V

    .line 800858
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 800859
    iget-object v0, p0, LX/4hJ;->leadingDigitsPattern_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeUTF(Ljava/lang/String;)V

    .line 800860
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 800861
    :cond_0
    iget-boolean v0, p0, LX/4hJ;->hasNationalPrefixFormattingRule:Z

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeBoolean(Z)V

    .line 800862
    iget-boolean v0, p0, LX/4hJ;->hasNationalPrefixFormattingRule:Z

    if-eqz v0, :cond_1

    .line 800863
    iget-object v0, p0, LX/4hJ;->nationalPrefixFormattingRule_:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeUTF(Ljava/lang/String;)V

    .line 800864
    :cond_1
    iget-boolean v0, p0, LX/4hJ;->hasDomesticCarrierCodeFormattingRule:Z

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeBoolean(Z)V

    .line 800865
    iget-boolean v0, p0, LX/4hJ;->hasDomesticCarrierCodeFormattingRule:Z

    if-eqz v0, :cond_2

    .line 800866
    iget-object v0, p0, LX/4hJ;->domesticCarrierCodeFormattingRule_:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeUTF(Ljava/lang/String;)V

    .line 800867
    :cond_2
    iget-boolean v0, p0, LX/4hJ;->nationalPrefixOptionalWhenFormatting_:Z

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeBoolean(Z)V

    .line 800868
    return-void
.end method
