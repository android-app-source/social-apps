.class public LX/3YI;
.super LX/35n;
.source ""


# static fields
.field public static final a:LX/1Cz;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 595746
    new-instance v0, LX/3YJ;

    invoke-direct {v0}, LX/3YJ;-><init>()V

    sput-object v0, LX/3YI;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 595744
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/3YI;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 595745
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 595747
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/3YI;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 595748
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 595742
    const v0, 0x7f030954

    invoke-direct {p0, p1, p2, p3, v0}, LX/3YI;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 595743
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 595740
    invoke-direct {p0, p1, p2, p3, p4}, LX/35n;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 595741
    return-void
.end method
