.class public LX/4Sh;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 715294
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 715295
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_c

    .line 715296
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 715297
    :goto_0
    return v1

    .line 715298
    :cond_0
    const-string v11, "total_electoral_votes_available"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 715299
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v3

    move v6, v3

    move v3, v2

    .line 715300
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_9

    .line 715301
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 715302
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 715303
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_1

    if-eqz v10, :cond_1

    .line 715304
    const-string v11, "all_candidate_data"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 715305
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 715306
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->START_ARRAY:LX/15z;

    if-ne v10, v11, :cond_2

    .line 715307
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_ARRAY:LX/15z;

    if-eq v10, v11, :cond_2

    .line 715308
    invoke-static {p0, p1}, LX/4Sl;->b(LX/15w;LX/186;)I

    move-result v10

    .line 715309
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 715310
    :cond_2
    invoke-static {v9, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v9

    move v9, v9

    .line 715311
    goto :goto_1

    .line 715312
    :cond_3
    const-string v11, "all_election_data"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 715313
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 715314
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->START_ARRAY:LX/15z;

    if-ne v10, v11, :cond_4

    .line 715315
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_ARRAY:LX/15z;

    if-eq v10, v11, :cond_4

    .line 715316
    invoke-static {p0, p1}, LX/4Sk;->b(LX/15w;LX/186;)I

    move-result v10

    .line 715317
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 715318
    :cond_4
    invoke-static {v8, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v8

    move v8, v8

    .line 715319
    goto :goto_1

    .line 715320
    :cond_5
    const-string v11, "topic_id"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 715321
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto/16 :goto_1

    .line 715322
    :cond_6
    const-string v11, "total_electoral_votes_needed"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 715323
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v5, v0

    move v0, v2

    goto/16 :goto_1

    .line 715324
    :cond_7
    const-string v11, "data_source"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 715325
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto/16 :goto_1

    .line 715326
    :cond_8
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 715327
    :cond_9
    const/4 v10, 0x6

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 715328
    invoke-virtual {p1, v1, v9}, LX/186;->b(II)V

    .line 715329
    invoke-virtual {p1, v2, v8}, LX/186;->b(II)V

    .line 715330
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v7}, LX/186;->b(II)V

    .line 715331
    if-eqz v3, :cond_a

    .line 715332
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v6, v1}, LX/186;->a(III)V

    .line 715333
    :cond_a
    if-eqz v0, :cond_b

    .line 715334
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v5, v1}, LX/186;->a(III)V

    .line 715335
    :cond_b
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 715336
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_c
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    move v9, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 715337
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 715338
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 715339
    if-eqz v0, :cond_1

    .line 715340
    const-string v1, "all_candidate_data"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 715341
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 715342
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 715343
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v3

    invoke-static {p0, v3, p2, p3}, LX/4Sl;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 715344
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 715345
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 715346
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 715347
    if-eqz v0, :cond_3

    .line 715348
    const-string v1, "all_election_data"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 715349
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 715350
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v3

    if-ge v1, v3, :cond_2

    .line 715351
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v3

    invoke-static {p0, v3, p2, p3}, LX/4Sk;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 715352
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 715353
    :cond_2
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 715354
    :cond_3
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 715355
    if-eqz v0, :cond_4

    .line 715356
    const-string v1, "topic_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 715357
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 715358
    :cond_4
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 715359
    if-eqz v0, :cond_5

    .line 715360
    const-string v1, "total_electoral_votes_available"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 715361
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 715362
    :cond_5
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 715363
    if-eqz v0, :cond_6

    .line 715364
    const-string v1, "total_electoral_votes_needed"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 715365
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 715366
    :cond_6
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 715367
    if-eqz v0, :cond_7

    .line 715368
    const-string v1, "data_source"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 715369
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 715370
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 715371
    return-void
.end method
