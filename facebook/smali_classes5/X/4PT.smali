.class public LX/4PT;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 700552
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 19

    .prologue
    .line 700553
    const/4 v14, 0x0

    .line 700554
    const/4 v11, 0x0

    .line 700555
    const-wide/16 v12, 0x0

    .line 700556
    const/4 v10, 0x0

    .line 700557
    const/4 v9, 0x0

    .line 700558
    const/4 v8, 0x0

    .line 700559
    const/4 v7, 0x0

    .line 700560
    const/4 v6, 0x0

    .line 700561
    const/4 v5, 0x0

    .line 700562
    const/4 v4, 0x0

    .line 700563
    const/4 v3, 0x0

    .line 700564
    const/4 v2, 0x0

    .line 700565
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v15

    sget-object v16, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v16

    if-eq v15, v0, :cond_e

    .line 700566
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 700567
    const/4 v2, 0x0

    .line 700568
    :goto_0
    return v2

    .line 700569
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v7, :cond_a

    .line 700570
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 700571
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 700572
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v17, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v17

    if-eq v7, v0, :cond_0

    if-eqz v2, :cond_0

    .line 700573
    const-string v7, "can_stop_sending_location"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 700574
    const/4 v2, 0x1

    .line 700575
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move/from16 v16, v6

    move v6, v2

    goto :goto_1

    .line 700576
    :cond_1
    const-string v7, "coordinate"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 700577
    invoke-static/range {p0 .. p1}, LX/4LW;->a(LX/15w;LX/186;)I

    move-result v2

    move v15, v2

    goto :goto_1

    .line 700578
    :cond_2
    const-string v7, "expiration_time"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 700579
    const/4 v2, 0x1

    .line 700580
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 700581
    :cond_3
    const-string v7, "id"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 700582
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v14, v2

    goto :goto_1

    .line 700583
    :cond_4
    const-string v7, "sender"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 700584
    invoke-static/range {p0 .. p1}, LX/2bO;->a(LX/15w;LX/186;)I

    move-result v2

    move v13, v2

    goto :goto_1

    .line 700585
    :cond_5
    const-string v7, "should_show_eta"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 700586
    const/4 v2, 0x1

    .line 700587
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v7

    move v8, v2

    move v12, v7

    goto :goto_1

    .line 700588
    :cond_6
    const-string v7, "url"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 700589
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v11, v2

    goto/16 :goto_1

    .line 700590
    :cond_7
    const-string v7, "location_title"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 700591
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v10, v2

    goto/16 :goto_1

    .line 700592
    :cond_8
    const-string v7, "offline_threading_id"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 700593
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v9, v2

    goto/16 :goto_1

    .line 700594
    :cond_9
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 700595
    :cond_a
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 700596
    if-eqz v6, :cond_b

    .line 700597
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 700598
    :cond_b
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 700599
    if-eqz v3, :cond_c

    .line 700600
    const/4 v3, 0x3

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 700601
    :cond_c
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 700602
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 700603
    if-eqz v8, :cond_d

    .line 700604
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->a(IZ)V

    .line 700605
    :cond_d
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 700606
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 700607
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 700608
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_e
    move v15, v11

    move/from16 v16, v14

    move v11, v7

    move v14, v10

    move v10, v6

    move v6, v4

    move/from16 v18, v9

    move v9, v5

    move-wide v4, v12

    move/from16 v13, v18

    move v12, v8

    move v8, v2

    goto/16 :goto_1
.end method
