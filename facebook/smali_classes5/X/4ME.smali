.class public LX/4ME;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 687198
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 13

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 687199
    const/4 v2, 0x0

    .line 687200
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v6, :cond_7

    .line 687201
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 687202
    :goto_0
    return v0

    .line 687203
    :cond_0
    const-string v11, "rsvp_time"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 687204
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    move v7, v1

    .line 687205
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_4

    .line 687206
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 687207
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 687208
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_1

    if-eqz v10, :cond_1

    .line 687209
    const-string v11, "node"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 687210
    invoke-static {p0, p1}, LX/2bM;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 687211
    :cond_2
    const-string v11, "seen_state"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 687212
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/graphql/enums/GraphQLEventSeenState;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    move-result-object v6

    move-object v8, v6

    move v6, v1

    goto :goto_1

    .line 687213
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 687214
    :cond_4
    const/4 v10, 0x3

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 687215
    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 687216
    if-eqz v7, :cond_5

    move-object v0, p1

    .line 687217
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 687218
    :cond_5
    if-eqz v6, :cond_6

    .line 687219
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v8}, LX/186;->a(ILjava/lang/Enum;)V

    .line 687220
    :cond_6
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto :goto_0

    :cond_7
    move v6, v0

    move v7, v0

    move-object v8, v2

    move v9, v0

    move-wide v2, v4

    goto :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v4, 0x2

    const/4 v3, 0x0

    .line 687221
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 687222
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 687223
    if-eqz v0, :cond_0

    .line 687224
    const-string v1, "node"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 687225
    invoke-static {p0, v0, p2, p3}, LX/2bM;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 687226
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v6, v7}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 687227
    cmp-long v2, v0, v6

    if-eqz v2, :cond_1

    .line 687228
    const-string v2, "rsvp_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 687229
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 687230
    :cond_1
    invoke-virtual {p0, p1, v4, v3}, LX/15i;->a(IIS)S

    move-result v0

    .line 687231
    if-eqz v0, :cond_2

    .line 687232
    const-string v0, "seen_state"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 687233
    const-class v0, Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    invoke-virtual {p0, p1, v4, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLEventSeenState;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 687234
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 687235
    return-void
.end method
