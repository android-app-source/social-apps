.class public final LX/3mm;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/3mm;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/3mp;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/3mo;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 637270
    const/4 v0, 0x0

    sput-object v0, LX/3mm;->a:LX/3mm;

    .line 637271
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/3mm;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 637267
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 637268
    new-instance v0, LX/3mo;

    invoke-direct {v0}, LX/3mo;-><init>()V

    iput-object v0, p0, LX/3mm;->c:LX/3mo;

    .line 637269
    return-void
.end method

.method public static c(LX/1De;)LX/3mp;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 637259
    new-instance v1, LX/3mn;

    invoke-direct {v1}, LX/3mn;-><init>()V

    .line 637260
    sget-object v2, LX/3mm;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/3mp;

    .line 637261
    if-nez v2, :cond_0

    .line 637262
    new-instance v2, LX/3mp;

    invoke-direct {v2}, LX/3mp;-><init>()V

    .line 637263
    :cond_0
    invoke-static {v2, p0, v0, v0, v1}, LX/3mp;->a$redex0(LX/3mp;LX/1De;IILX/3mn;)V

    .line 637264
    move-object v1, v2

    .line 637265
    move-object v0, v1

    .line 637266
    return-object v0
.end method

.method public static declared-synchronized q()LX/3mm;
    .locals 2

    .prologue
    .line 637255
    const-class v1, LX/3mm;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/3mm;->a:LX/3mm;

    if-nez v0, :cond_0

    .line 637256
    new-instance v0, LX/3mm;

    invoke-direct {v0}, LX/3mm;-><init>()V

    sput-object v0, LX/3mm;->a:LX/3mm;

    .line 637257
    :cond_0
    sget-object v0, LX/3mm;->a:LX/3mm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 637258
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 7

    .prologue
    .line 637246
    check-cast p2, LX/3mn;

    .line 637247
    iget-object v0, p2, LX/3mn;->a:Ljava/lang/String;

    iget-object v1, p2, LX/3mn;->b:LX/1dQ;

    const/4 v5, 0x2

    const/4 p2, 0x0

    const/4 p0, 0x4

    const/4 v6, 0x1

    .line 637248
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-interface {v2, v3}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v2

    const v3, 0x7f0b0919

    invoke-interface {v2, p2, v3}, LX/1Dh;->q(II)LX/1Dh;

    move-result-object v2

    const v3, 0x7f0b0919

    invoke-interface {v2, v5, v3}, LX/1Dh;->q(II)LX/1Dh;

    move-result-object v2

    const v3, 0x7f0b0919

    invoke-interface {v2, p0, v3}, LX/1Dh;->q(II)LX/1Dh;

    move-result-object v2

    const/4 v3, 0x5

    const v4, 0x7f0b0919

    invoke-interface {v2, v3, v4}, LX/1Dh;->q(II)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v6}, LX/1Dh;->I(I)LX/1Dh;

    move-result-object v2

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0442

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-interface {v2, v3}, LX/1Dh;->W(I)LX/1Dh;

    move-result-object v2

    .line 637249
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v5}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v6}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v6, p0}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x3

    invoke-interface {v3, v4, p0}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v3

    const/16 v4, 0x20

    invoke-interface {v3, v4}, LX/1Dh;->K(I)LX/1Dh;

    move-result-object v3

    const v4, 0x7f020a84

    invoke-interface {v3, v4}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v3

    .line 637250
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, v0}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v4

    const v5, 0x7f0b0050

    invoke-virtual {v4, v5}, LX/1ne;->q(I)LX/1ne;

    move-result-object v4

    const v5, 0x7f0a0443

    invoke-virtual {v4, v5}, LX/1ne;->n(I)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, v6}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v4

    sget-object v5, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v4, v5}, LX/1ne;->a(Landroid/graphics/Typeface;)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, p2}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v1}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    .line 637251
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, p2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, p0}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v6}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v6, p0}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v4

    const/4 v5, -0x1

    invoke-interface {v4, v5}, LX/1Dh;->W(I)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 637252
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 637253
    invoke-static {}, LX/1dS;->b()V

    .line 637254
    const/4 v0, 0x0

    return-object v0
.end method
