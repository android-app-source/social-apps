.class public LX/4L5;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 681820
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 681821
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v4, :cond_b

    .line 681822
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 681823
    :goto_0
    return v1

    .line 681824
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_6

    .line 681825
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 681826
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 681827
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_0

    if-eqz v11, :cond_0

    .line 681828
    const-string v12, "amount_offset"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 681829
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v5

    move v10, v5

    move v5, v2

    goto :goto_1

    .line 681830
    :cond_1
    const-string v12, "budget"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 681831
    invoke-static {p0, p1}, LX/4Lf;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 681832
    :cond_2
    const-string v12, "estimated_reach"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 681833
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v4

    move v8, v4

    move v4, v2

    goto :goto_1

    .line 681834
    :cond_3
    const-string v12, "secondary_estimate_type"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 681835
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

    move-result-object v3

    move-object v7, v3

    move v3, v2

    goto :goto_1

    .line 681836
    :cond_4
    const-string v12, "estimate_type"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 681837
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

    move-result-object v0

    move-object v6, v0

    move v0, v2

    goto :goto_1

    .line 681838
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 681839
    :cond_6
    const/4 v11, 0x5

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 681840
    if-eqz v5, :cond_7

    .line 681841
    invoke-virtual {p1, v1, v10, v1}, LX/186;->a(III)V

    .line 681842
    :cond_7
    invoke-virtual {p1, v2, v9}, LX/186;->b(II)V

    .line 681843
    if-eqz v4, :cond_8

    .line 681844
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v8, v1}, LX/186;->a(III)V

    .line 681845
    :cond_8
    if-eqz v3, :cond_9

    .line 681846
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v7}, LX/186;->a(ILjava/lang/Enum;)V

    .line 681847
    :cond_9
    if-eqz v0, :cond_a

    .line 681848
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v6}, LX/186;->a(ILjava/lang/Enum;)V

    .line 681849
    :cond_a
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_b
    move v3, v1

    move v4, v1

    move v5, v1

    move-object v6, v0

    move-object v7, v0

    move v8, v1

    move v9, v1

    move v10, v1

    move v0, v1

    goto/16 :goto_1
.end method
