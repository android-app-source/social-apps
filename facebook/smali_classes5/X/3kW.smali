.class public LX/3kW;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile j:LX/3kW;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/privacy/PrivacyOperationsClient;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2c9;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0lC;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/content/res/Resources;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0wM;",
            ">;"
        }
    .end annotation
.end field

.field private i:Lcom/facebook/privacy/nux/PrivacyEducationBannerConfig;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/privacy/PrivacyOperationsClient;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2c9;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0lC;",
            ">;",
            "LX/0Ot",
            "<",
            "Landroid/content/res/Resources;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0wM;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 633105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 633106
    iput-object p1, p0, LX/3kW;->a:LX/0Ot;

    .line 633107
    iput-object p2, p0, LX/3kW;->b:LX/0Ot;

    .line 633108
    iput-object p3, p0, LX/3kW;->c:LX/0Ot;

    .line 633109
    iput-object p4, p0, LX/3kW;->d:LX/0Ot;

    .line 633110
    iput-object p5, p0, LX/3kW;->e:LX/0Ot;

    .line 633111
    iput-object p6, p0, LX/3kW;->f:LX/0Ot;

    .line 633112
    iput-object p7, p0, LX/3kW;->g:LX/0Ot;

    .line 633113
    iput-object p8, p0, LX/3kW;->h:LX/0Ot;

    .line 633114
    return-void
.end method

.method public static a(LX/0QB;)LX/3kW;
    .locals 12

    .prologue
    .line 633036
    sget-object v0, LX/3kW;->j:LX/3kW;

    if-nez v0, :cond_1

    .line 633037
    const-class v1, LX/3kW;

    monitor-enter v1

    .line 633038
    :try_start_0
    sget-object v0, LX/3kW;->j:LX/3kW;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 633039
    if-eqz v2, :cond_0

    .line 633040
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 633041
    new-instance v3, LX/3kW;

    const/16 v4, 0xf9a

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x2e3

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x2fc7

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x259

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0xfc2

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x2ba

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x1b

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x5c8

    invoke-static {v0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-direct/range {v3 .. v11}, LX/3kW;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 633042
    move-object v0, v3

    .line 633043
    sput-object v0, LX/3kW;->j:LX/3kW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 633044
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 633045
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 633046
    :cond_1
    sget-object v0, LX/3kW;->j:LX/3kW;

    return-object v0

    .line 633047
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 633048
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 633104
    sget-object v0, LX/0ax;->do:Ljava/lang/String;

    invoke-static {p0}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/3kW;LX/8Qa;LX/5nx;)V
    .locals 8

    .prologue
    .line 633102
    iget-object v0, p0, LX/3kW;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/PrivacyOperationsClient;

    invoke-virtual {p1}, LX/8Qa;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v1, p0, LX/3kW;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v2, p2, v1}, Lcom/facebook/privacy/PrivacyOperationsClient;->a(Ljava/lang/String;LX/5nx;Ljava/lang/Long;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 633103
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 633080
    if-nez p0, :cond_1

    .line 633081
    :cond_0
    :goto_0
    return v0

    .line 633082
    :cond_1
    invoke-static {p0}, LX/0sa;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v3

    .line 633083
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->k()Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;

    move-result-object v2

    if-eqz v2, :cond_2

    move v2, v1

    .line 633084
    :goto_1
    if-eqz v2, :cond_0

    .line 633085
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->k()Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;->k()Lcom/facebook/graphql/model/GraphQLReshareEducationInfo;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->k()Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;->k()Lcom/facebook/graphql/model/GraphQLReshareEducationInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLReshareEducationInfo;->j()Z

    move-result v2

    if-eqz v2, :cond_3

    move v2, v1

    .line 633086
    :goto_2
    if-eqz v2, :cond_4

    move v0, v1

    .line 633087
    goto :goto_0

    :cond_2
    move v2, v0

    .line 633088
    goto :goto_1

    :cond_3
    move v2, v0

    .line 633089
    goto :goto_2

    .line 633090
    :cond_4
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->k()Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;->l()Lcom/facebook/graphql/model/GraphQLTagExpansionEducationInfo;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->k()Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;->l()Lcom/facebook/graphql/model/GraphQLTagExpansionEducationInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTagExpansionEducationInfo;->j()Z

    move-result v2

    if-eqz v2, :cond_5

    move v2, v1

    .line 633091
    :goto_3
    if-eqz v2, :cond_6

    move v0, v1

    .line 633092
    goto :goto_0

    :cond_5
    move v2, v0

    .line 633093
    goto :goto_3

    .line 633094
    :cond_6
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->k()Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;->a()Lcom/facebook/graphql/model/GraphQLFullIndexEducationInfo;

    move-result-object v2

    if-eqz v2, :cond_7

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->k()Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;->a()Lcom/facebook/graphql/model/GraphQLFullIndexEducationInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFullIndexEducationInfo;->j()Z

    move-result v2

    if-eqz v2, :cond_7

    move v2, v1

    .line 633095
    :goto_4
    if-eqz v2, :cond_8

    move v0, v1

    .line 633096
    goto :goto_0

    :cond_7
    move v2, v0

    .line 633097
    goto :goto_4

    .line 633098
    :cond_8
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->k()Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;->j()Lcom/facebook/graphql/model/GraphQLGroupMallAdsEducationInfo;

    move-result-object v2

    if-eqz v2, :cond_9

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->k()Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;->j()Lcom/facebook/graphql/model/GraphQLGroupMallAdsEducationInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGroupMallAdsEducationInfo;->j()Z

    move-result v2

    if-eqz v2, :cond_9

    move v2, v1

    .line 633099
    :goto_5
    if-eqz v2, :cond_0

    move v0, v1

    .line 633100
    goto/16 :goto_0

    :cond_9
    move v2, v0

    .line 633101
    goto :goto_5
.end method

.method public static b(LX/3kW;)Lcom/facebook/privacy/nux/PrivacyEducationBannerConfig;
    .locals 3

    .prologue
    .line 633069
    iget-object v0, p0, LX/3kW;->i:Lcom/facebook/privacy/nux/PrivacyEducationBannerConfig;

    if-eqz v0, :cond_0

    .line 633070
    iget-object v0, p0, LX/3kW;->i:Lcom/facebook/privacy/nux/PrivacyEducationBannerConfig;

    .line 633071
    :goto_0
    return-object v0

    .line 633072
    :cond_0
    iget-object v0, p0, LX/3kW;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2bs;->g:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 633073
    if-nez v1, :cond_1

    .line 633074
    new-instance v0, Lcom/facebook/privacy/nux/PrivacyEducationBannerConfig;

    invoke-direct {v0}, Lcom/facebook/privacy/nux/PrivacyEducationBannerConfig;-><init>()V

    iput-object v0, p0, LX/3kW;->i:Lcom/facebook/privacy/nux/PrivacyEducationBannerConfig;

    .line 633075
    :goto_1
    iget-object v0, p0, LX/3kW;->i:Lcom/facebook/privacy/nux/PrivacyEducationBannerConfig;

    goto :goto_0

    .line 633076
    :cond_1
    :try_start_0
    iget-object v0, p0, LX/3kW;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lC;

    const-class v2, Lcom/facebook/privacy/nux/PrivacyEducationBannerConfig;

    invoke-virtual {v0, v1, v2}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/nux/PrivacyEducationBannerConfig;

    iput-object v0, p0, LX/3kW;->i:Lcom/facebook/privacy/nux/PrivacyEducationBannerConfig;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 633077
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 633078
    iget-object v0, p0, LX/3kW;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v2, "privacy_education_banner_controller_deserialize_error"

    invoke-virtual {v0, v2, v1}, LX/03V;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 633079
    new-instance v0, Lcom/facebook/privacy/nux/PrivacyEducationBannerConfig;

    invoke-direct {v0}, Lcom/facebook/privacy/nux/PrivacyEducationBannerConfig;-><init>()V

    iput-object v0, p0, LX/3kW;->i:Lcom/facebook/privacy/nux/PrivacyEducationBannerConfig;

    goto :goto_1
.end method

.method public static c(LX/3kW;)V
    .locals 4

    .prologue
    .line 633062
    const/4 v2, 0x0

    .line 633063
    :try_start_0
    iget-object v0, p0, LX/3kW;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lC;

    invoke-static {p0}, LX/3kW;->b(LX/3kW;)Lcom/facebook/privacy/nux/PrivacyEducationBannerConfig;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    move-object v1, v0

    .line 633064
    :goto_0
    if-eqz v1, :cond_0

    .line 633065
    iget-object v0, p0, LX/3kW;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v2, LX/2bs;->g:LX/0Tn;

    invoke-interface {v0, v2, v1}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 633066
    :cond_0
    return-void

    .line 633067
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 633068
    iget-object v0, p0, LX/3kW;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v3, "privacy_education_banner_controller_deserialize_error"

    invoke-virtual {v0, v3, v1}, LX/03V;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v1, v2

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;)LX/8QZ;
    .locals 12

    .prologue
    .line 633050
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;->k()Lcom/facebook/graphql/model/GraphQLReshareEducationInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;->k()Lcom/facebook/graphql/model/GraphQLReshareEducationInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLReshareEducationInfo;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 633051
    new-instance v0, LX/8QZ;

    sget-object v2, LX/8Qa;->ReshareEducation:LX/8Qa;

    iget-object v1, p0, LX/3kW;->g:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/res/Resources;

    const v3, 0x7f081310

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;->k()Lcom/facebook/graphql/model/GraphQLReshareEducationInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLReshareEducationInfo;->a()Ljava/lang/String;

    move-result-object v4

    iget-object v1, p0, LX/3kW;->g:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/res/Resources;

    const v5, 0x7f08002c

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v1, "https://m.facebook.com/help/mobile-touch/569567333138410"

    invoke-static {v1}, LX/3kW;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;->k()Lcom/facebook/graphql/model/GraphQLReshareEducationInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLReshareEducationInfo;->k()Z

    move-result v7

    iget-object v1, p0, LX/3kW;->h:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0wM;

    const v9, 0x7f0209b7

    iget-object v8, p0, LX/3kW;->g:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/res/Resources;

    const v10, 0x7f0a00d5

    invoke-virtual {v8, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    invoke-virtual {v1, v9, v8}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    iget-object v1, p0, LX/3kW;->h:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0wM;

    const v10, 0x7f0209b7

    iget-object v9, p0, LX/3kW;->g:LX/0Ot;

    invoke-interface {v9}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/content/res/Resources;

    const v11, 0x7f0a00d2

    invoke-virtual {v9, v11}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    invoke-virtual {v1, v10, v9}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    move-object v1, p0

    invoke-direct/range {v0 .. v9}, LX/8QZ;-><init>(LX/3kW;LX/8Qa;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLandroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 633052
    :goto_0
    return-object v0

    .line 633053
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;->l()Lcom/facebook/graphql/model/GraphQLTagExpansionEducationInfo;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;->l()Lcom/facebook/graphql/model/GraphQLTagExpansionEducationInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTagExpansionEducationInfo;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 633054
    new-instance v0, LX/8QZ;

    sget-object v2, LX/8Qa;->TagExpansionEducation:LX/8Qa;

    iget-object v1, p0, LX/3kW;->g:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/res/Resources;

    const v3, 0x7f081311

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;->l()Lcom/facebook/graphql/model/GraphQLTagExpansionEducationInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTagExpansionEducationInfo;->a()Ljava/lang/String;

    move-result-object v4

    iget-object v1, p0, LX/3kW;->g:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/res/Resources;

    const v5, 0x7f081313

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 633055
    sget-object v1, LX/0ax;->dP:Ljava/lang/String;

    move-object v6, v1

    .line 633056
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;->l()Lcom/facebook/graphql/model/GraphQLTagExpansionEducationInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTagExpansionEducationInfo;->k()Z

    move-result v7

    iget-object v1, p0, LX/3kW;->h:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0wM;

    const v9, 0x7f0209f0

    iget-object v8, p0, LX/3kW;->g:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/res/Resources;

    const v10, 0x7f0a00d5

    invoke-virtual {v8, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    invoke-virtual {v1, v9, v8}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    iget-object v1, p0, LX/3kW;->h:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0wM;

    const v10, 0x7f0209f0

    iget-object v9, p0, LX/3kW;->g:LX/0Ot;

    invoke-interface {v9}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/content/res/Resources;

    const v11, 0x7f0a00d2

    invoke-virtual {v9, v11}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    invoke-virtual {v1, v10, v9}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    move-object v1, p0

    invoke-direct/range {v0 .. v9}, LX/8QZ;-><init>(LX/3kW;LX/8Qa;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLandroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    .line 633057
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;->a()Lcom/facebook/graphql/model/GraphQLFullIndexEducationInfo;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;->a()Lcom/facebook/graphql/model/GraphQLFullIndexEducationInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFullIndexEducationInfo;->j()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 633058
    new-instance v0, LX/8QZ;

    sget-object v2, LX/8Qa;->FullIndexEducation:LX/8Qa;

    iget-object v1, p0, LX/3kW;->g:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/res/Resources;

    const v3, 0x7f081312

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;->a()Lcom/facebook/graphql/model/GraphQLFullIndexEducationInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFullIndexEducationInfo;->a()Ljava/lang/String;

    move-result-object v4

    iget-object v1, p0, LX/3kW;->g:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/res/Resources;

    const v5, 0x7f08002c

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;->a()Lcom/facebook/graphql/model/GraphQLFullIndexEducationInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFullIndexEducationInfo;->k()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/3kW;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;->a()Lcom/facebook/graphql/model/GraphQLFullIndexEducationInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFullIndexEducationInfo;->l()Z

    move-result v7

    iget-object v1, p0, LX/3kW;->h:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0wM;

    const v9, 0x7f0208b2

    iget-object v8, p0, LX/3kW;->g:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/res/Resources;

    const v10, 0x7f0a00d5

    invoke-virtual {v8, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    invoke-virtual {v1, v9, v8}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    iget-object v1, p0, LX/3kW;->h:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0wM;

    const v10, 0x7f0208b2

    iget-object v9, p0, LX/3kW;->g:LX/0Ot;

    invoke-interface {v9}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/content/res/Resources;

    const v11, 0x7f0a00d2

    invoke-virtual {v9, v11}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    invoke-virtual {v1, v10, v9}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    move-object v1, p0

    invoke-direct/range {v0 .. v9}, LX/8QZ;-><init>(LX/3kW;LX/8Qa;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLandroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    .line 633059
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;->j()Lcom/facebook/graphql/model/GraphQLGroupMallAdsEducationInfo;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;->j()Lcom/facebook/graphql/model/GraphQLGroupMallAdsEducationInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroupMallAdsEducationInfo;->j()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 633060
    new-instance v0, LX/8QZ;

    sget-object v2, LX/8Qa;->GroupMallAdsEducation:LX/8Qa;

    iget-object v1, p0, LX/3kW;->g:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/res/Resources;

    const v3, 0x7f08131b

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;->j()Lcom/facebook/graphql/model/GraphQLGroupMallAdsEducationInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGroupMallAdsEducationInfo;->a()Ljava/lang/String;

    move-result-object v4

    const-string v5, ""

    const-string v6, ""

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;->j()Lcom/facebook/graphql/model/GraphQLGroupMallAdsEducationInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGroupMallAdsEducationInfo;->k()Z

    move-result v7

    iget-object v1, p0, LX/3kW;->h:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0wM;

    const v9, 0x7f0208ed

    iget-object v8, p0, LX/3kW;->g:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/res/Resources;

    const v10, 0x7f0a00d5

    invoke-virtual {v8, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    invoke-virtual {v1, v9, v8}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    iget-object v1, p0, LX/3kW;->h:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0wM;

    const v10, 0x7f0208ed

    iget-object v9, p0, LX/3kW;->g:LX/0Ot;

    invoke-interface {v9}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/content/res/Resources;

    const v11, 0x7f0a00d2

    invoke-virtual {v9, v11}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    invoke-virtual {v1, v10, v9}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    move-object v1, p0

    invoke-direct/range {v0 .. v9}, LX/8QZ;-><init>(LX/3kW;LX/8Qa;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLandroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    .line 633061
    :cond_3
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public final b(LX/8Qa;)Z
    .locals 1

    .prologue
    .line 633049
    invoke-static {p0}, LX/3kW;->b(LX/3kW;)Lcom/facebook/privacy/nux/PrivacyEducationBannerConfig;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/facebook/privacy/nux/PrivacyEducationBannerConfig;->b(LX/8Qa;)Z

    move-result v0

    return v0
.end method
