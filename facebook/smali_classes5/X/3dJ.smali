.class public final LX/3dJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3d4;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/3d4",
        "<",
        "LX/0jT;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/3dG;


# direct methods
.method public constructor <init>(LX/3dG;)V
    .locals 0

    .prologue
    .line 616521
    iput-object p1, p0, LX/3dJ;->a:LX/3dG;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 616522
    check-cast p1, LX/0jT;

    .line 616523
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLFeedback;

    if-eqz v0, :cond_1

    .line 616524
    iget-object v0, p0, LX/3dJ;->a:LX/3dG;

    check-cast p1, Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v0, p1}, LX/3dG;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object p1

    .line 616525
    :cond_0
    :goto_0
    return-object p1

    .line 616526
    :cond_1
    instance-of v0, p1, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;

    if-eqz v0, :cond_3

    .line 616527
    check-cast p1, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;

    invoke-static {p1}, LX/5k9;->a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    .line 616528
    iget-object v1, p0, LX/3dJ;->a:LX/3dG;

    invoke-virtual {v1, v0}, LX/3dG;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    .line 616529
    const/4 v4, 0x0

    .line 616530
    if-nez v0, :cond_4

    .line 616531
    :cond_2
    :goto_1
    move-object v0, v4

    .line 616532
    invoke-static {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;->a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;)Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;

    move-result-object p1

    goto :goto_0

    .line 616533
    :cond_3
    invoke-interface {p1}, LX/0jT;->f()I

    move-result v0

    const v1, -0x78fb05b

    if-ne v0, v1, :cond_0

    .line 616534
    iget-object v0, p0, LX/3dJ;->a:LX/3dG;

    .line 616535
    iget-object v1, v0, LX/3dG;->d:LX/2lk;

    if-nez v1, :cond_6

    .line 616536
    :goto_2
    move-object p1, p1

    .line 616537
    goto :goto_0

    .line 616538
    :cond_4
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 616539
    invoke-static {v2, v0}, LX/5k9;->a(LX/186;Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v3

    .line 616540
    if-eqz v3, :cond_2

    .line 616541
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 616542
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 616543
    const/4 v2, 0x0

    invoke-virtual {v3, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 616544
    new-instance v2, LX/15i;

    const/4 v6, 0x1

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 616545
    instance-of v3, v0, Lcom/facebook/flatbuffers/Flattenable;

    if-eqz v3, :cond_5

    .line 616546
    const-string v3, "PhotosMetadataConversionHelper.getSimpleMediaFeedback"

    invoke-virtual {v2, v3, v0}, LX/15i;->a(Ljava/lang/String;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 616547
    :cond_5
    new-instance v4, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;

    invoke-direct {v4, v2}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;-><init>(LX/15i;)V

    goto :goto_1

    :cond_6
    iget-object v1, v0, LX/3dG;->d:LX/2lk;

    invoke-interface {v1, p1}, LX/2lk;->c(LX/0jT;)LX/0jT;

    move-result-object p1

    goto :goto_2
.end method
