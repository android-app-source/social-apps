.class public final LX/3vE;
.super Ljava/lang/Object;
.source ""


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 650957
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 650958
    return-void
.end method

.method public static a(Landroid/content/Context;LX/3qu;)Landroid/view/Menu;
    .locals 2

    .prologue
    .line 650954
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    .line 650955
    new-instance v0, LX/3vF;

    invoke-direct {v0, p0, p1}, LX/3vF;-><init>(Landroid/content/Context;LX/3qu;)V

    return-object v0

    .line 650956
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public static a(Landroid/content/Context;LX/3qv;)Landroid/view/MenuItem;
    .locals 2

    .prologue
    .line 650948
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 650949
    new-instance v0, LX/3vB;

    invoke-direct {v0, p0, p1}, LX/3vB;-><init>(Landroid/content/Context;LX/3qv;)V

    .line 650950
    :goto_0
    return-object v0

    .line 650951
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_1

    .line 650952
    new-instance v0, LX/3v9;

    invoke-direct {v0, p0, p1}, LX/3v9;-><init>(Landroid/content/Context;LX/3qv;)V

    goto :goto_0

    .line 650953
    :cond_1
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public static a(Landroid/content/Context;LX/3qw;)Landroid/view/SubMenu;
    .locals 2

    .prologue
    .line 650945
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    .line 650946
    new-instance v0, LX/3vH;

    invoke-direct {v0, p0, p1}, LX/3vH;-><init>(Landroid/content/Context;LX/3qw;)V

    return-object v0

    .line 650947
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
