.class public LX/3nK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1qM;


# instance fields
.field private final mFilter:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<+",
            "LX/26t;",
            ">;"
        }
    .end annotation
.end field

.field private final mLazyFilter:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<+",
            "Lcom/facebook/fbservice/service/BlueServiceHandler$LazyFilter;",
            ">;"
        }
    .end annotation
.end field

.field private final mLazyNext:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<+",
            "LX/1qM;",
            ">;"
        }
    .end annotation
.end field

.field private final mNext:LX/1qM;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<+",
            "Lcom/facebook/fbservice/service/BlueServiceHandler$LazyFilter;",
            ">;",
            "LX/0Ot",
            "<+",
            "LX/1qM;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 638145
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 638146
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 638147
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 638148
    iput-object p1, p0, LX/3nK;->mLazyFilter:LX/0Ot;

    .line 638149
    iput-object p2, p0, LX/3nK;->mLazyNext:LX/0Ot;

    .line 638150
    iput-object v0, p0, LX/3nK;->mFilter:LX/0Ot;

    .line 638151
    iput-object v0, p0, LX/3nK;->mNext:LX/1qM;

    .line 638152
    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/1qM;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<+",
            "LX/26t;",
            ">;",
            "LX/1qM;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 638153
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 638154
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 638155
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 638156
    iput-object p1, p0, LX/3nK;->mFilter:LX/0Ot;

    .line 638157
    iput-object p2, p0, LX/3nK;->mNext:LX/1qM;

    .line 638158
    iput-object v0, p0, LX/3nK;->mLazyFilter:LX/0Ot;

    .line 638159
    iput-object v0, p0, LX/3nK;->mLazyNext:LX/0Ot;

    .line 638160
    return-void
.end method


# virtual methods
.method public handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    const-wide/16 v2, 0x1

    .line 638161
    iget-object v0, p0, LX/3nK;->mFilter:LX/0Ot;

    if-eqz v0, :cond_1

    .line 638162
    const-string v0, "Filter.get"

    invoke-static {v2, v3, v0}, LX/018;->a(JLjava/lang/String;)V

    .line 638163
    :try_start_0
    iget-object v0, p0, LX/3nK;->mFilter:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/26t;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 638164
    invoke-static {v2, v3}, LX/0BL;->a(J)LX/0BN;

    move-result-object v2

    const-string v3, "FilterClassName"

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-virtual {v2, v3, v1}, LX/0BN;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0BN;

    move-result-object v1

    invoke-virtual {v1}, LX/0BN;->a()V

    .line 638165
    iget-object v1, p0, LX/3nK;->mNext:LX/1qM;

    invoke-interface {v0, p1, v1}, LX/26t;->handleOperation(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 638166
    :goto_1
    return-object v0

    .line 638167
    :cond_0
    const-string v1, "null"

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v2, v3}, LX/0BL;->a(J)LX/0BN;

    move-result-object v1

    const-string v2, "FilterClassName"

    const-string v3, "null"

    invoke-virtual {v1, v2, v3}, LX/0BN;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0BN;

    move-result-object v1

    invoke-virtual {v1}, LX/0BN;->a()V

    throw v0

    .line 638168
    :cond_1
    iget-object v0, p0, LX/3nK;->mLazyFilter:LX/0Ot;

    if-eqz v0, :cond_3

    .line 638169
    const-string v0, "LazyFilter.get"

    invoke-static {v2, v3, v0}, LX/018;->a(JLjava/lang/String;)V

    .line 638170
    :try_start_1
    iget-object v0, p0, LX/3nK;->mLazyFilter:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4BH;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 638171
    invoke-static {v2, v3}, LX/0BL;->a(J)LX/0BN;

    move-result-object v2

    const-string v3, "LazyFilterClassName"

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_2
    invoke-virtual {v2, v3, v1}, LX/0BN;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0BN;

    move-result-object v1

    invoke-virtual {v1}, LX/0BN;->a()V

    .line 638172
    iget-object v1, p0, LX/3nK;->mLazyNext:LX/0Ot;

    invoke-virtual {v0, p1, v1}, LX/4BH;->handleOperation(LX/1qK;LX/0Ot;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_1

    .line 638173
    :cond_2
    const-string v1, "null"

    goto :goto_2

    :catchall_1
    move-exception v0

    invoke-static {v2, v3}, LX/0BL;->a(J)LX/0BN;

    move-result-object v1

    const-string v2, "LazyFilterClassName"

    const-string v3, "null"

    invoke-virtual {v1, v2, v3}, LX/0BN;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0BN;

    move-result-object v1

    invoke-virtual {v1}, LX/0BN;->a()V

    throw v0

    .line 638174
    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot handle current operation"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
