.class public LX/3eR;
.super LX/0ro;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0ro",
        "<",
        "Lcom/facebook/stickers/service/FetchStickerPacksApiParams;",
        "Lcom/facebook/stickers/service/FetchStickerPacksResult;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile d:LX/3eR;


# instance fields
.field private final c:LX/3eL;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 620078
    const-class v0, LX/3eR;

    sput-object v0, LX/3eR;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0sO;LX/3eL;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 620079
    invoke-direct {p0, p1}, LX/0ro;-><init>(LX/0sO;)V

    .line 620080
    iput-object p2, p0, LX/3eR;->c:LX/3eL;

    .line 620081
    return-void
.end method

.method public static a(LX/0QB;)LX/3eR;
    .locals 5

    .prologue
    .line 620082
    sget-object v0, LX/3eR;->d:LX/3eR;

    if-nez v0, :cond_1

    .line 620083
    const-class v1, LX/3eR;

    monitor-enter v1

    .line 620084
    :try_start_0
    sget-object v0, LX/3eR;->d:LX/3eR;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 620085
    if-eqz v2, :cond_0

    .line 620086
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 620087
    new-instance p0, LX/3eR;

    invoke-static {v0}, LX/0sO;->a(LX/0QB;)LX/0sO;

    move-result-object v3

    check-cast v3, LX/0sO;

    invoke-static {v0}, LX/3eL;->a(LX/0QB;)LX/3eL;

    move-result-object v4

    check-cast v4, LX/3eL;

    invoke-direct {p0, v3, v4}, LX/3eR;-><init>(LX/0sO;LX/3eL;)V

    .line 620088
    move-object v0, p0

    .line 620089
    sput-object v0, LX/3eR;->d:LX/3eR;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 620090
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 620091
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 620092
    :cond_1
    sget-object v0, LX/3eR;->d:LX/3eR;

    return-object v0

    .line 620093
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 620094
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/1pN;LX/15w;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 620095
    check-cast p1, Lcom/facebook/stickers/service/FetchStickerPacksApiParams;

    const/4 v2, 0x0

    .line 620096
    sget-object v0, LX/8m1;->a:[I

    .line 620097
    iget-object v1, p1, Lcom/facebook/stickers/service/FetchStickerPacksApiParams;->a:Lcom/facebook/stickers/service/FetchStickerPacksParams;

    move-object v1, v1

    .line 620098
    iget-object v3, v1, Lcom/facebook/stickers/service/FetchStickerPacksParams;->a:LX/3do;

    move-object v1, v3

    .line 620099
    invoke-virtual {v1}, LX/3do;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 620100
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unrecognized sticker pack type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 620101
    iget-object v2, p1, Lcom/facebook/stickers/service/FetchStickerPacksApiParams;->a:Lcom/facebook/stickers/service/FetchStickerPacksParams;

    move-object v2, v2

    .line 620102
    iget-object v3, v2, Lcom/facebook/stickers/service/FetchStickerPacksParams;->a:LX/3do;

    move-object v2, v3

    .line 620103
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 620104
    :pswitch_0
    const-class v0, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchDownloadedStickerPacksQueryModel;

    invoke-virtual {p3, v0}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchDownloadedStickerPacksQueryModel;

    invoke-virtual {v0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchDownloadedStickerPacksQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const-class v3, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchDownloadedStickerPacksQueryModel$StickerStoreModel$TrayPacksModel;

    invoke-virtual {v1, v0, v2, v3}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchDownloadedStickerPacksQueryModel$StickerStoreModel$TrayPacksModel;

    invoke-virtual {v0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchDownloadedStickerPacksQueryModel$StickerStoreModel$TrayPacksModel;->a()LX/0Px;

    move-result-object v0

    move-object v1, v0

    .line 620105
    :goto_0
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 620106
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v4

    :goto_1
    if-ge v2, v4, :cond_1

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerPackFieldsModel;

    .line 620107
    :try_start_0
    invoke-static {v0}, LX/3eL;->a(Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerPackFieldsModel;)Lcom/facebook/stickers/model/StickerPack;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_0
    .catch LX/8m7; {:try_start_0 .. :try_end_0} :catch_0

    .line 620108
    :goto_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 620109
    :pswitch_1
    const-class v0, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchOwnedStickerPacksQueryModel;

    invoke-virtual {p3, v0}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchOwnedStickerPacksQueryModel;

    invoke-virtual {v0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchOwnedStickerPacksQueryModel;->a()Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchOwnedStickerPacksQueryModel$StickerStoreModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchOwnedStickerPacksQueryModel$StickerStoreModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const-class v3, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerPackFieldsModel;

    invoke-virtual {v1, v0, v2, v3}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_3
    move-object v1, v0

    .line 620110
    goto :goto_0

    .line 620111
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 620112
    goto :goto_3

    .line 620113
    :pswitch_2
    const-class v0, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchStoreStickerPacksQueryModel;

    invoke-virtual {p3, v0}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchStoreStickerPacksQueryModel;

    invoke-virtual {v0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchStoreStickerPacksQueryModel;->a()Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchStoreStickerPacksQueryModel$StickerStoreModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchStoreStickerPacksQueryModel$StickerStoreModel;->a()Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchStoreStickerPacksQueryModel$StickerStoreModel$AvailablePacksModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchStoreStickerPacksQueryModel$StickerStoreModel$AvailablePacksModel;->a()LX/0Px;

    move-result-object v0

    move-object v1, v0

    .line 620114
    goto :goto_0

    .line 620115
    :pswitch_3
    const-class v0, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchStoreStickerPacksQueryModel;

    invoke-virtual {p3, v0}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchStoreStickerPacksQueryModel;

    invoke-virtual {v0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchStoreStickerPacksQueryModel;->a()Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchStoreStickerPacksQueryModel$StickerStoreModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchStoreStickerPacksQueryModel$StickerStoreModel;->a()Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchStoreStickerPacksQueryModel$StickerStoreModel$AvailablePacksModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchStoreStickerPacksQueryModel$StickerStoreModel$AvailablePacksModel;->a()LX/0Px;

    move-result-object v0

    move-object v1, v0

    .line 620116
    goto :goto_0

    .line 620117
    :catch_0
    move-exception v0

    .line 620118
    sget-object v5, LX/3eR;->b:Ljava/lang/Class;

    const-string v6, "Invalid sticker pack received from server. Probably safe to ignore this."

    invoke-static {v5, v6, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 620119
    :cond_1
    new-instance v0, Lcom/facebook/stickers/service/FetchStickerPacksResult;

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/stickers/service/FetchStickerPacksResult;-><init>(Ljava/util/List;)V

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final b(Ljava/lang/Object;LX/1pN;)I
    .locals 1

    .prologue
    .line 620120
    const/4 v0, 0x1

    return v0
.end method

.method public final e(Ljava/lang/Object;)LX/0w7;
    .locals 6

    .prologue
    .line 620121
    check-cast p1, Lcom/facebook/stickers/service/FetchStickerPacksApiParams;

    .line 620122
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v1

    .line 620123
    const-string v2, "is_auto_downloadable"

    .line 620124
    iget-object v0, p1, Lcom/facebook/stickers/service/FetchStickerPacksApiParams;->a:Lcom/facebook/stickers/service/FetchStickerPacksParams;

    move-object v0, v0

    .line 620125
    iget-boolean v3, v0, Lcom/facebook/stickers/service/FetchStickerPacksParams;->d:Z

    move v0, v3

    .line 620126
    if-nez v0, :cond_0

    .line 620127
    iget-object v0, p1, Lcom/facebook/stickers/service/FetchStickerPacksApiParams;->a:Lcom/facebook/stickers/service/FetchStickerPacksParams;

    move-object v0, v0

    .line 620128
    iget-object v3, v0, Lcom/facebook/stickers/service/FetchStickerPacksParams;->a:LX/3do;

    move-object v0, v3

    .line 620129
    sget-object v3, LX/3do;->AUTODOWNLOADED_PACKS:LX/3do;

    invoke-virtual {v0, v3}, LX/3do;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 620130
    const-string v0, "is_promoted"

    .line 620131
    iget-object v2, p1, Lcom/facebook/stickers/service/FetchStickerPacksApiParams;->a:Lcom/facebook/stickers/service/FetchStickerPacksParams;

    move-object v2, v2

    .line 620132
    iget-boolean v3, v2, Lcom/facebook/stickers/service/FetchStickerPacksParams;->e:Z

    move v2, v3

    .line 620133
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 620134
    const-string v0, "is_featured"

    .line 620135
    iget-object v2, p1, Lcom/facebook/stickers/service/FetchStickerPacksApiParams;->a:Lcom/facebook/stickers/service/FetchStickerPacksParams;

    move-object v2, v2

    .line 620136
    iget-boolean v3, v2, Lcom/facebook/stickers/service/FetchStickerPacksParams;->f:Z

    move v2, v3

    .line 620137
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 620138
    iget-object v0, p1, Lcom/facebook/stickers/service/FetchStickerPacksApiParams;->c:Ljava/lang/String;

    move-object v0, v0

    .line 620139
    if-eqz v0, :cond_1

    .line 620140
    const-string v0, "after"

    .line 620141
    iget-object v2, p1, Lcom/facebook/stickers/service/FetchStickerPacksApiParams;->c:Ljava/lang/String;

    move-object v2, v2

    .line 620142
    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 620143
    :cond_1
    iget v0, p1, Lcom/facebook/stickers/service/FetchStickerPacksApiParams;->b:I

    move v0, v0

    .line 620144
    if-lez v0, :cond_2

    .line 620145
    const-string v0, "first"

    .line 620146
    iget v2, p1, Lcom/facebook/stickers/service/FetchStickerPacksApiParams;->b:I

    move v2, v2

    .line 620147
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 620148
    :cond_2
    iget-object v0, p1, Lcom/facebook/stickers/service/FetchStickerPacksApiParams;->a:Lcom/facebook/stickers/service/FetchStickerPacksParams;

    move-object v0, v0

    .line 620149
    iget-boolean v2, v0, Lcom/facebook/stickers/service/FetchStickerPacksParams;->g:Z

    move v0, v2

    .line 620150
    if-eqz v0, :cond_3

    .line 620151
    const-string v0, "update_time"

    .line 620152
    iget-wide v4, p1, Lcom/facebook/stickers/service/FetchStickerPacksApiParams;->d:J

    move-wide v2, v4

    .line 620153
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 620154
    :cond_3
    const-string v0, "media_type"

    iget-object v2, p0, LX/3eR;->c:LX/3eL;

    invoke-virtual {v2}, LX/3eL;->c()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 620155
    const-string v0, "scaling_factor"

    iget-object v2, p0, LX/3eR;->c:LX/3eL;

    invoke-virtual {v2}, LX/3eL;->d()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 620156
    iget-object v0, p1, Lcom/facebook/stickers/service/FetchStickerPacksApiParams;->a:Lcom/facebook/stickers/service/FetchStickerPacksParams;

    move-object v0, v0

    .line 620157
    iget-object v2, v0, Lcom/facebook/stickers/service/FetchStickerPacksParams;->c:Ljava/lang/String;

    move-object v0, v2

    .line 620158
    if-eqz v0, :cond_4

    .line 620159
    const-string v0, "interface"

    .line 620160
    iget-object v2, p1, Lcom/facebook/stickers/service/FetchStickerPacksApiParams;->a:Lcom/facebook/stickers/service/FetchStickerPacksParams;

    move-object v2, v2

    .line 620161
    iget-object v3, v2, Lcom/facebook/stickers/service/FetchStickerPacksParams;->c:Ljava/lang/String;

    move-object v2, v3

    .line 620162
    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 620163
    :cond_4
    new-instance v0, LX/0w7;

    invoke-direct {v0, v1}, LX/0w7;-><init>(Ljava/util/Map;)V

    return-object v0

    .line 620164
    :cond_5
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public final f(Ljava/lang/Object;)LX/0gW;
    .locals 3

    .prologue
    .line 620165
    check-cast p1, Lcom/facebook/stickers/service/FetchStickerPacksApiParams;

    .line 620166
    sget-object v0, LX/8m1;->a:[I

    .line 620167
    iget-object v1, p1, Lcom/facebook/stickers/service/FetchStickerPacksApiParams;->a:Lcom/facebook/stickers/service/FetchStickerPacksParams;

    move-object v1, v1

    .line 620168
    iget-object v2, v1, Lcom/facebook/stickers/service/FetchStickerPacksParams;->a:LX/3do;

    move-object v1, v2

    .line 620169
    invoke-virtual {v1}, LX/3do;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 620170
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unrecognized sticker pack type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 620171
    iget-object v2, p1, Lcom/facebook/stickers/service/FetchStickerPacksApiParams;->a:Lcom/facebook/stickers/service/FetchStickerPacksParams;

    move-object v2, v2

    .line 620172
    iget-object p0, v2, Lcom/facebook/stickers/service/FetchStickerPacksParams;->a:LX/3do;

    move-object v2, p0

    .line 620173
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 620174
    :pswitch_0
    new-instance v0, LX/8jh;

    invoke-direct {v0}, LX/8jh;-><init>()V

    move-object v0, v0

    .line 620175
    :goto_0
    return-object v0

    .line 620176
    :pswitch_1
    new-instance v0, LX/8jj;

    invoke-direct {v0}, LX/8jj;-><init>()V

    move-object v0, v0

    .line 620177
    goto :goto_0

    .line 620178
    :pswitch_2
    invoke-static {}, LX/8js;->m()LX/8jo;

    move-result-object v0

    goto :goto_0

    .line 620179
    :pswitch_3
    invoke-static {}, LX/8js;->m()LX/8jo;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
