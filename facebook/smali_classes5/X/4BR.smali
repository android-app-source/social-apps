.class public final LX/4BR;
.super Landroid/widget/CursorAdapter;
.source ""


# instance fields
.field public final synthetic a:Landroid/widget/ListView;

.field public final synthetic b:LX/4BW;

.field public final synthetic c:LX/31a;

.field private final d:I

.field private final e:I


# direct methods
.method public constructor <init>(LX/31a;Landroid/content/Context;Landroid/database/Cursor;ZLandroid/widget/ListView;LX/4BW;)V
    .locals 2

    .prologue
    .line 677725
    iput-object p1, p0, LX/4BR;->c:LX/31a;

    iput-object p5, p0, LX/4BR;->a:Landroid/widget/ListView;

    iput-object p6, p0, LX/4BR;->b:LX/4BW;

    invoke-direct {p0, p2, p3, p4}, Landroid/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;Z)V

    .line 677726
    invoke-virtual {p0}, LX/4BR;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 677727
    iget-object v1, p0, LX/4BR;->c:LX/31a;

    iget-object v1, v1, LX/31a;->K:Ljava/lang/String;

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, LX/4BR;->d:I

    .line 677728
    iget-object v1, p0, LX/4BR;->c:LX/31a;

    iget-object v1, v1, LX/31a;->L:Ljava/lang/String;

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/4BR;->e:I

    .line 677729
    return-void
.end method


# virtual methods
.method public final bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 677730
    const v0, 0x7f0d0cec

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    .line 677731
    iget v2, p0, LX/4BR;->d:I

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/CheckedTextView;->setText(Ljava/lang/CharSequence;)V

    .line 677732
    iget-object v2, p0, LX/4BR;->a:Landroid/widget/ListView;

    invoke-interface {p3}, Landroid/database/Cursor;->getPosition()I

    move-result v3

    iget v0, p0, LX/4BR;->e:I

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v2, v3, v0}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 677733
    return-void

    .line 677734
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 677735
    iget-object v0, p0, LX/4BR;->c:LX/31a;

    iget-object v0, v0, LX/31a;->b:Landroid/view/LayoutInflater;

    iget-object v1, p0, LX/4BR;->b:LX/4BW;

    iget v1, v1, LX/4BW;->I:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
