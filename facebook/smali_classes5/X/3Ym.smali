.class public LX/3Ym;
.super Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;
.source ""

# interfaces
.implements LX/2eZ;


# static fields
.field public static final a:LX/1Cz;


# instance fields
.field public final b:Landroid/widget/LinearLayout;

.field public final c:Landroid/widget/TextView;

.field public final d:Landroid/widget/TextView;

.field public final e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public final f:Lcom/facebook/maps/FbStaticMapView;

.field private final g:Landroid/widget/ImageView;

.field private final h:Landroid/view/View;

.field private final i:Landroid/view/ViewGroup;

.field private j:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 597125
    new-instance v0, LX/3Yn;

    invoke-direct {v0}, LX/3Yn;-><init>()V

    sput-object v0, LX/3Ym;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 597127
    invoke-direct {p0, p1}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;-><init>(Landroid/content/Context;)V

    .line 597128
    const v0, 0x7f030f7b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 597129
    const v0, 0x7f0d2557

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/3Ym;->b:Landroid/widget/LinearLayout;

    .line 597130
    const v0, 0x7f0d255d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/3Ym;->c:Landroid/widget/TextView;

    .line 597131
    const v0, 0x7f0d255e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/3Ym;->d:Landroid/widget/TextView;

    .line 597132
    const v0, 0x7f0d2558

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/3Ym;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 597133
    const v0, 0x7f0d255a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/3Ym;->g:Landroid/widget/ImageView;

    .line 597134
    const v0, 0x7f0d255f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/3Ym;->h:Landroid/view/View;

    .line 597135
    const v0, 0x7f0d2559

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/maps/FbStaticMapView;

    iput-object v0, p0, LX/3Ym;->f:Lcom/facebook/maps/FbStaticMapView;

    .line 597136
    const v0, 0x7f0d255b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, LX/3Ym;->i:Landroid/view/ViewGroup;

    .line 597137
    invoke-virtual {p0}, LX/3Ym;->getContext()Landroid/content/Context;

    move-result-object v0

    instance-of v0, v0, Landroid/support/v4/app/FragmentActivity;

    if-eqz v0, :cond_0

    .line 597138
    iget-object v1, p0, LX/3Ym;->f:Lcom/facebook/maps/FbStaticMapView;

    sget-object v2, LX/0yY;->VIEW_MAP_INTERSTITIAL:LX/0yY;

    invoke-virtual {p0}, LX/3Ym;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, Lcom/facebook/maps/FbStaticMapView;->a(LX/0yY;LX/0gc;LX/6Zs;)V

    .line 597139
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 597126
    iget-boolean v0, p0, LX/3Ym;->j:Z

    return v0
.end method

.method public setBottomSectionContainerOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 597121
    iget-object v0, p0, LX/3Ym;->i:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 597122
    return-void
.end method

.method public setHasBeenAttached(Z)V
    .locals 0

    .prologue
    .line 597123
    iput-boolean p1, p0, LX/3Ym;->j:Z

    .line 597124
    return-void
.end method

.method public setMainImageVisibility(I)V
    .locals 1

    .prologue
    .line 597140
    iget-object v0, p0, LX/3Ym;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 597141
    return-void
.end method

.method public setMapImageVisibility(I)V
    .locals 1

    .prologue
    .line 597111
    iget-object v0, p0, LX/3Ym;->f:Lcom/facebook/maps/FbStaticMapView;

    invoke-virtual {v0, p1}, Lcom/facebook/maps/FbStaticMapView;->setVisibility(I)V

    .line 597112
    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 597113
    iget-object v0, p0, LX/3Ym;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 597114
    return-void
.end method

.method public setReviewButtonOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 597115
    iget-object v0, p0, LX/3Ym;->h:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 597116
    return-void
.end method

.method public setTitleOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 597117
    iget-object v0, p0, LX/3Ym;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 597118
    return-void
.end method

.method public setXOutIconOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 597119
    iget-object v0, p0, LX/3Ym;->g:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 597120
    return-void
.end method
