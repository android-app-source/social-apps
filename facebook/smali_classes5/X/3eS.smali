.class public LX/3eS;
.super LX/0ro;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0ro",
        "<",
        "Lcom/facebook/stickers/service/FetchStickerPacksByIdParams;",
        "Lcom/facebook/stickers/service/FetchStickerPacksResult;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile d:LX/3eS;


# instance fields
.field private final c:LX/3eL;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 620216
    const-class v0, LX/3eS;

    sput-object v0, LX/3eS;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0sO;LX/3eL;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 620217
    invoke-direct {p0, p1}, LX/0ro;-><init>(LX/0sO;)V

    .line 620218
    iput-object p2, p0, LX/3eS;->c:LX/3eL;

    .line 620219
    return-void
.end method

.method public static a(LX/0QB;)LX/3eS;
    .locals 5

    .prologue
    .line 620187
    sget-object v0, LX/3eS;->d:LX/3eS;

    if-nez v0, :cond_1

    .line 620188
    const-class v1, LX/3eS;

    monitor-enter v1

    .line 620189
    :try_start_0
    sget-object v0, LX/3eS;->d:LX/3eS;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 620190
    if-eqz v2, :cond_0

    .line 620191
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 620192
    new-instance p0, LX/3eS;

    invoke-static {v0}, LX/0sO;->a(LX/0QB;)LX/0sO;

    move-result-object v3

    check-cast v3, LX/0sO;

    invoke-static {v0}, LX/3eL;->a(LX/0QB;)LX/3eL;

    move-result-object v4

    check-cast v4, LX/3eL;

    invoke-direct {p0, v3, v4}, LX/3eS;-><init>(LX/0sO;LX/3eL;)V

    .line 620193
    move-object v0, p0

    .line 620194
    sput-object v0, LX/3eS;->d:LX/3eS;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 620195
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 620196
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 620197
    :cond_1
    sget-object v0, LX/3eS;->d:LX/3eS;

    return-object v0

    .line 620198
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 620199
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/1pN;LX/15w;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 620200
    iget-object v0, p0, LX/0ro;->a:LX/0sO;

    const-class v1, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerPackFieldsModel;

    invoke-static {v1}, LX/0w5;->b(Ljava/lang/Class;)LX/0w5;

    move-result-object v1

    invoke-virtual {v0, v1, p3}, LX/0sO;->a(LX/0w5;LX/15w;)Ljava/util/List;

    move-result-object v0

    .line 620201
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 620202
    new-instance v2, LX/0P2;

    invoke-direct {v2}, LX/0P2;-><init>()V

    .line 620203
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerPackFieldsModel;

    .line 620204
    invoke-static {v0}, LX/3eL;->a(Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerPackFieldsModel;)Lcom/facebook/stickers/model/StickerPack;

    move-result-object v4

    .line 620205
    invoke-virtual {v1, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 620206
    invoke-virtual {v0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerPackFieldsModel;->o()Z

    move-result v5

    .line 620207
    invoke-virtual {v0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerPackFieldsModel;->k()Z

    move-result v6

    .line 620208
    sget-object v0, LX/8m2;->NOT_AVAILABLE:LX/8m2;

    .line 620209
    if-eqz v5, :cond_1

    .line 620210
    sget-object v0, LX/8m2;->DOWNLOADED:LX/8m2;

    .line 620211
    :cond_0
    :goto_1
    iget-object v5, v4, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v4, v5

    .line 620212
    invoke-virtual {v2, v4, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    goto :goto_0

    .line 620213
    :cond_1
    if-eqz v6, :cond_0

    .line 620214
    sget-object v0, LX/8m2;->IN_STORE:LX/8m2;

    goto :goto_1

    .line 620215
    :cond_2
    new-instance v0, Lcom/facebook/stickers/service/FetchStickerPacksResult;

    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v2}, LX/0P2;->b()LX/0P1;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/facebook/stickers/service/FetchStickerPacksResult;-><init>(Ljava/util/List;Ljava/util/Map;)V

    return-object v0
.end method

.method public final b(Ljava/lang/Object;LX/1pN;)I
    .locals 1

    .prologue
    .line 620180
    const/4 v0, 0x0

    return v0
.end method

.method public final f(Ljava/lang/Object;)LX/0gW;
    .locals 4

    .prologue
    .line 620181
    check-cast p1, Lcom/facebook/stickers/service/FetchStickerPacksByIdParams;

    .line 620182
    new-instance v0, LX/8jk;

    invoke-direct {v0}, LX/8jk;-><init>()V

    move-object v0, v0

    .line 620183
    const-string v1, "pack_ids"

    .line 620184
    iget-object v2, p1, Lcom/facebook/stickers/service/FetchStickerPacksByIdParams;->a:LX/0Px;

    move-object v2, v2

    .line 620185
    invoke-virtual {v0, v1, v2}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    move-result-object v1

    const-string v2, "media_type"

    iget-object v3, p0, LX/3eS;->c:LX/3eL;

    invoke-virtual {v3}, LX/3eL;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "scaling_factor"

    iget-object v3, p0, LX/3eS;->c:LX/3eL;

    invoke-virtual {v3}, LX/3eL;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 620186
    return-object v0
.end method
