.class public LX/3pr;
.super LX/3pq;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 641776
    invoke-direct {p0}, LX/3pq;-><init>()V

    return-void
.end method


# virtual methods
.method public a(LX/2HB;)Landroid/app/Notification;
    .locals 25

    .prologue
    .line 641777
    new-instance v1, LX/3py;

    move-object/from16 v0, p1

    iget-object v2, v0, LX/2HB;->a:Landroid/content/Context;

    move-object/from16 v0, p1

    iget-object v3, v0, LX/2HB;->B:Landroid/app/Notification;

    move-object/from16 v0, p1

    iget-object v4, v0, LX/2HB;->b:Ljava/lang/CharSequence;

    move-object/from16 v0, p1

    iget-object v5, v0, LX/2HB;->c:Ljava/lang/CharSequence;

    move-object/from16 v0, p1

    iget-object v6, v0, LX/2HB;->h:Ljava/lang/CharSequence;

    move-object/from16 v0, p1

    iget-object v7, v0, LX/2HB;->f:Landroid/widget/RemoteViews;

    move-object/from16 v0, p1

    iget v8, v0, LX/2HB;->i:I

    move-object/from16 v0, p1

    iget-object v9, v0, LX/2HB;->d:Landroid/app/PendingIntent;

    move-object/from16 v0, p1

    iget-object v10, v0, LX/2HB;->e:Landroid/app/PendingIntent;

    move-object/from16 v0, p1

    iget-object v11, v0, LX/2HB;->g:Landroid/graphics/Bitmap;

    move-object/from16 v0, p1

    iget v12, v0, LX/2HB;->o:I

    move-object/from16 v0, p1

    iget v13, v0, LX/2HB;->p:I

    move-object/from16 v0, p1

    iget-boolean v14, v0, LX/2HB;->q:Z

    move-object/from16 v0, p1

    iget-boolean v15, v0, LX/2HB;->k:Z

    move-object/from16 v0, p1

    iget-boolean v0, v0, LX/2HB;->l:Z

    move/from16 v16, v0

    move-object/from16 v0, p1

    iget v0, v0, LX/2HB;->j:I

    move/from16 v17, v0

    move-object/from16 v0, p1

    iget-object v0, v0, LX/2HB;->n:Ljava/lang/CharSequence;

    move-object/from16 v18, v0

    move-object/from16 v0, p1

    iget-boolean v0, v0, LX/2HB;->v:Z

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget-object v0, v0, LX/2HB;->C:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    move-object/from16 v0, p1

    iget-object v0, v0, LX/2HB;->x:Landroid/os/Bundle;

    move-object/from16 v21, v0

    move-object/from16 v0, p1

    iget-object v0, v0, LX/2HB;->r:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, p1

    iget-boolean v0, v0, LX/2HB;->s:Z

    move/from16 v23, v0

    move-object/from16 v0, p1

    iget-object v0, v0, LX/2HB;->t:Ljava/lang/String;

    move-object/from16 v24, v0

    invoke-direct/range {v1 .. v24}, LX/3py;-><init>(Landroid/content/Context;Landroid/app/Notification;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/widget/RemoteViews;ILandroid/app/PendingIntent;Landroid/app/PendingIntent;Landroid/graphics/Bitmap;IIZZZILjava/lang/CharSequence;ZLjava/util/ArrayList;Landroid/os/Bundle;Ljava/lang/String;ZLjava/lang/String;)V

    .line 641778
    move-object/from16 v0, p1

    iget-object v2, v0, LX/2HB;->u:Ljava/util/ArrayList;

    invoke-static {v1, v2}, LX/3px;->b(LX/3pT;Ljava/util/ArrayList;)V

    .line 641779
    move-object/from16 v0, p1

    iget-object v2, v0, LX/2HB;->m:LX/3pc;

    invoke-static {v1, v2}, LX/3px;->b(LX/3pU;LX/3pc;)V

    .line 641780
    invoke-virtual {v1}, LX/3py;->b()Landroid/app/Notification;

    move-result-object v1

    return-object v1
.end method

.method public final a([LX/3pb;)Ljava/util/ArrayList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "LX/3pb;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/Parcelable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 641781
    if-nez p1, :cond_1

    .line 641782
    const/4 v0, 0x0

    .line 641783
    :cond_0
    move-object v0, v0

    .line 641784
    return-object v0

    .line 641785
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    array-length v1, p1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 641786
    array-length v2, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, p1, v1

    .line 641787
    new-instance v4, Landroid/app/Notification$Action$Builder;

    invoke-virtual {v3}, LX/3pa;->a()I

    move-result v5

    invoke-virtual {v3}, LX/3pa;->b()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v3}, LX/3pa;->c()Landroid/app/PendingIntent;

    move-result-object v7

    invoke-direct {v4, v5, v6, v7}, Landroid/app/Notification$Action$Builder;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V

    invoke-virtual {v3}, LX/3pa;->d()Landroid/os/Bundle;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/app/Notification$Action$Builder;->addExtras(Landroid/os/Bundle;)Landroid/app/Notification$Action$Builder;

    move-result-object v5

    .line 641788
    invoke-virtual {v3}, LX/3pa;->e()[LX/3qK;

    move-result-object v4

    .line 641789
    if-eqz v4, :cond_2

    .line 641790
    invoke-static {v4}, LX/3qM;->a([LX/3qK;)[Landroid/app/RemoteInput;

    move-result-object v6

    .line 641791
    array-length v7, v6

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v7, :cond_2

    aget-object p0, v6, v4

    .line 641792
    invoke-virtual {v5, p0}, Landroid/app/Notification$Action$Builder;->addRemoteInput(Landroid/app/RemoteInput;)Landroid/app/Notification$Action$Builder;

    .line 641793
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 641794
    :cond_2
    invoke-virtual {v5}, Landroid/app/Notification$Action$Builder;->build()Landroid/app/Notification$Action;

    move-result-object v4

    move-object v3, v4

    .line 641795
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 641796
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
