.class public LX/475;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:LX/474;

.field public final c:LX/46n;

.field private final d:LX/0Zb;


# direct methods
.method public constructor <init>(Landroid/view/View;LX/46n;LX/0Zb;)V
    .locals 2
    .param p1    # Landroid/view/View;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 671878
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 671879
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 671880
    const-class v0, Landroid/app/Activity;

    invoke-static {v1, v0}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, LX/475;->a:Landroid/app/Activity;

    .line 671881
    const-class v0, LX/474;

    invoke-static {v1, v0}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/474;

    iput-object v0, p0, LX/475;->b:LX/474;

    .line 671882
    iput-object p2, p0, LX/475;->c:LX/46n;

    .line 671883
    iput-object p3, p0, LX/475;->d:LX/0Zb;

    .line 671884
    return-void
.end method

.method private a(I)V
    .locals 1

    .prologue
    .line 671885
    iget-object v0, p0, LX/475;->a:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 671886
    iget-object v0, p0, LX/475;->a:Landroid/app/Activity;

    invoke-virtual {v0, p1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 671887
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 671888
    iget-object v0, p0, LX/475;->c:LX/46n;

    invoke-virtual {v0}, LX/46n;->a()I

    move-result v0

    .line 671889
    iget-object v1, p0, LX/475;->c:LX/46n;

    invoke-virtual {v1}, LX/46n;->a()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 671890
    const/4 v1, -0x1

    :goto_0
    move v1, v1

    .line 671891
    invoke-direct {p0, v1}, LX/475;->a(I)V

    .line 671892
    iget-object v1, p0, LX/475;->c:LX/46n;

    invoke-virtual {v1}, LX/46n;->a()I

    move-result v1

    .line 671893
    if-eq v1, v0, :cond_0

    .line 671894
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "rotation_lock_changed_rotation"

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v3, "view_orientation_lock_helper"

    .line 671895
    iput-object v3, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 671896
    move-object v2, v2

    .line 671897
    const-string v3, "original_rotation"

    invoke-virtual {v2, v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v2, "new_rotation"

    invoke-virtual {v0, v2, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "model_name"

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 671898
    iget-object v1, p0, LX/475;->d:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 671899
    :cond_0
    return-void

    .line 671900
    :pswitch_0
    const/4 v1, 0x1

    goto :goto_0

    .line 671901
    :pswitch_1
    const/4 v1, 0x0

    goto :goto_0

    .line 671902
    :pswitch_2
    const/16 v1, 0x9

    goto :goto_0

    .line 671903
    :pswitch_3
    const/16 v1, 0x8

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 671904
    const/4 v0, -0x1

    invoke-direct {p0, v0}, LX/475;->a(I)V

    .line 671905
    return-void
.end method
