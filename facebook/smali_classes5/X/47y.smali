.class public final LX/47y;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/47a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/47a",
        "<",
        "LX/03R;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 672606
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 672607
    check-cast p1, LX/03R;

    check-cast p2, LX/03R;

    .line 672608
    invoke-virtual {p1}, LX/03R;->isSet()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p2}, LX/03R;->isSet()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 672609
    invoke-virtual {p1}, LX/03R;->asBoolean()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p2}, LX/03R;->asBoolean()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object p1, LX/03R;->YES:LX/03R;

    .line 672610
    :cond_0
    :goto_0
    return-object p1

    .line 672611
    :cond_1
    sget-object p1, LX/03R;->NO:LX/03R;

    goto :goto_0

    .line 672612
    :cond_2
    invoke-virtual {p1}, LX/03R;->isSet()Z

    move-result v0

    if-nez v0, :cond_0

    move-object p1, p2

    .line 672613
    goto :goto_0
.end method
