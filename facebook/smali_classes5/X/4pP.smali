.class public final enum LX/4pP;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/4pP;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/4pP;

.field public static final enum CLASS:LX/4pP;

.field public static final enum CUSTOM:LX/4pP;

.field public static final enum MINIMAL_CLASS:LX/4pP;

.field public static final enum NAME:LX/4pP;

.field public static final enum NONE:LX/4pP;


# instance fields
.field private final _defaultPropertyName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 811399
    new-instance v0, LX/4pP;

    const-string v1, "NONE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v3, v2}, LX/4pP;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/4pP;->NONE:LX/4pP;

    .line 811400
    new-instance v0, LX/4pP;

    const-string v1, "CLASS"

    const-string v2, "@class"

    invoke-direct {v0, v1, v4, v2}, LX/4pP;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/4pP;->CLASS:LX/4pP;

    .line 811401
    new-instance v0, LX/4pP;

    const-string v1, "MINIMAL_CLASS"

    const-string v2, "@c"

    invoke-direct {v0, v1, v5, v2}, LX/4pP;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/4pP;->MINIMAL_CLASS:LX/4pP;

    .line 811402
    new-instance v0, LX/4pP;

    const-string v1, "NAME"

    const-string v2, "@type"

    invoke-direct {v0, v1, v6, v2}, LX/4pP;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/4pP;->NAME:LX/4pP;

    .line 811403
    new-instance v0, LX/4pP;

    const-string v1, "CUSTOM"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v7, v2}, LX/4pP;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/4pP;->CUSTOM:LX/4pP;

    .line 811404
    const/4 v0, 0x5

    new-array v0, v0, [LX/4pP;

    sget-object v1, LX/4pP;->NONE:LX/4pP;

    aput-object v1, v0, v3

    sget-object v1, LX/4pP;->CLASS:LX/4pP;

    aput-object v1, v0, v4

    sget-object v1, LX/4pP;->MINIMAL_CLASS:LX/4pP;

    aput-object v1, v0, v5

    sget-object v1, LX/4pP;->NAME:LX/4pP;

    aput-object v1, v0, v6

    sget-object v1, LX/4pP;->CUSTOM:LX/4pP;

    aput-object v1, v0, v7

    sput-object v0, LX/4pP;->$VALUES:[LX/4pP;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 811405
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 811406
    iput-object p3, p0, LX/4pP;->_defaultPropertyName:Ljava/lang/String;

    .line 811407
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/4pP;
    .locals 1

    .prologue
    .line 811408
    const-class v0, LX/4pP;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/4pP;

    return-object v0
.end method

.method public static values()[LX/4pP;
    .locals 1

    .prologue
    .line 811409
    sget-object v0, LX/4pP;->$VALUES:[LX/4pP;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/4pP;

    return-object v0
.end method


# virtual methods
.method public final getDefaultPropertyName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 811410
    iget-object v0, p0, LX/4pP;->_defaultPropertyName:Ljava/lang/String;

    return-object v0
.end method
