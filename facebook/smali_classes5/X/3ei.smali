.class public LX/3ei;
.super LX/0ro;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0ro",
        "<",
        "Lcom/facebook/stickers/service/FetchStickersParams;",
        "Lcom/facebook/stickers/service/FetchStickersResult;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile d:LX/3ei;


# instance fields
.field private final c:LX/3eL;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 620653
    const-class v0, LX/3ei;

    sput-object v0, LX/3ei;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0sO;LX/3eL;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 620654
    invoke-direct {p0, p1}, LX/0ro;-><init>(LX/0sO;)V

    .line 620655
    iput-object p2, p0, LX/3ei;->c:LX/3eL;

    .line 620656
    return-void
.end method

.method public static a(LX/0QB;)LX/3ei;
    .locals 5

    .prologue
    .line 620657
    sget-object v0, LX/3ei;->d:LX/3ei;

    if-nez v0, :cond_1

    .line 620658
    const-class v1, LX/3ei;

    monitor-enter v1

    .line 620659
    :try_start_0
    sget-object v0, LX/3ei;->d:LX/3ei;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 620660
    if-eqz v2, :cond_0

    .line 620661
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 620662
    new-instance p0, LX/3ei;

    invoke-static {v0}, LX/0sO;->a(LX/0QB;)LX/0sO;

    move-result-object v3

    check-cast v3, LX/0sO;

    invoke-static {v0}, LX/3eL;->a(LX/0QB;)LX/3eL;

    move-result-object v4

    check-cast v4, LX/3eL;

    invoke-direct {p0, v3, v4}, LX/3ei;-><init>(LX/0sO;LX/3eL;)V

    .line 620663
    move-object v0, p0

    .line 620664
    sput-object v0, LX/3ei;->d:LX/3ei;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 620665
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 620666
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 620667
    :cond_1
    sget-object v0, LX/3ei;->d:LX/3ei;

    return-object v0

    .line 620668
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 620669
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/1pN;LX/15w;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 620670
    iget-object v0, p0, LX/0ro;->a:LX/0sO;

    const-class v1, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerFieldsWithPreviewModel;

    invoke-static {v1}, LX/0w5;->b(Ljava/lang/Class;)LX/0w5;

    move-result-object v1

    invoke-virtual {v0, v1, p3}, LX/0sO;->a(LX/0w5;LX/15w;)Ljava/util/List;

    move-result-object v0

    .line 620671
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 620672
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerFieldsWithPreviewModel;

    .line 620673
    :try_start_0
    invoke-static {v0}, LX/3eL;->a(Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerFieldsWithPreviewModel;)Lcom/facebook/stickers/model/Sticker;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 620674
    :catch_0
    move-exception v0

    .line 620675
    sget-object v3, LX/3ei;->b:Ljava/lang/Class;

    const-string v4, "Error parsing sticker node"

    invoke-static {v3, v4, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 620676
    :cond_0
    new-instance v0, Lcom/facebook/stickers/service/FetchStickersResult;

    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/stickers/service/FetchStickersResult;-><init>(Ljava/util/List;)V

    return-object v0
.end method

.method public final b(Ljava/lang/Object;LX/1pN;)I
    .locals 1

    .prologue
    .line 620677
    const/4 v0, 0x0

    return v0
.end method

.method public final f(Ljava/lang/Object;)LX/0gW;
    .locals 3

    .prologue
    .line 620678
    check-cast p1, Lcom/facebook/stickers/service/FetchStickersParams;

    .line 620679
    iget-object v0, p0, LX/3ei;->c:LX/3eL;

    .line 620680
    new-instance v1, LX/8jm;

    invoke-direct {v1}, LX/8jm;-><init>()V

    move-object v1, v1

    .line 620681
    invoke-static {v0, v1}, LX/3eL;->a(LX/3eL;LX/0gW;)V

    .line 620682
    move-object v0, v1

    .line 620683
    const-string v1, "sticker_ids"

    .line 620684
    iget-object v2, p1, Lcom/facebook/stickers/service/FetchStickersParams;->a:LX/0Px;

    move-object v2, v2

    .line 620685
    invoke-virtual {v0, v1, v2}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    .line 620686
    return-object v0
.end method
