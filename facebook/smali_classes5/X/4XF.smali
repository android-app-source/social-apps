.class public final LX/4XF;
.super LX/0ur;
.source ""


# instance fields
.field public b:Lcom/facebook/graphql/model/GraphQLMegaphoneAction;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Z

.field public j:Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;

.field public k:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 765840
    invoke-direct {p0}, LX/0ur;-><init>()V

    .line 765841
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;

    iput-object v0, p0, LX/4XF;->j:Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;

    .line 765842
    const/4 v0, 0x0

    iput-object v0, p0, LX/4XF;->o:LX/0x2;

    .line 765843
    instance-of v0, p0, LX/4XF;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 765844
    return-void
.end method
