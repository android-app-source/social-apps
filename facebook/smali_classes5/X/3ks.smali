.class public LX/3ks;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0i1;


# instance fields
.field private final a:LX/2ua;

.field public b:Z


# direct methods
.method public constructor <init>(LX/2ua;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 633366
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 633367
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/3ks;->b:Z

    .line 633368
    iput-object p1, p0, LX/3ks;->a:LX/2ua;

    .line 633369
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 633370
    const-wide/32 v0, 0xf731400

    return-wide v0
.end method

.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/10S;
    .locals 1

    .prologue
    .line 633371
    iget-boolean v0, p0, LX/3ks;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3ks;->a:LX/2ua;

    invoke-virtual {v0}, LX/2ua;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 633372
    sget-object v0, LX/10S;->ELIGIBLE:LX/10S;

    .line 633373
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;

    goto :goto_0
.end method

.method public final a(J)V
    .locals 0

    .prologue
    .line 633365
    return-void
.end method

.method public final a(Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 633364
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 633362
    const-string v0, "3972"

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 633363
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->TIMELINE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
