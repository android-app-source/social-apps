.class public LX/4iK;
.super Landroid/webkit/WebView;
.source ""


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "DeprecatedSuperclass",
        "BadMethodUse-android.webkit.WebView.loadUrl",
        "BadSuperclassWebView.ProxyWebView"
    }
.end annotation

.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "LX/4iK;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:LX/0dy;

.field public c:LX/1h2;

.field private d:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 802991
    const-class v0, LX/4iK;

    sput-object v0, LX/4iK;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 802989
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/4iK;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 802990
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 802987
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/4iK;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 802988
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 802992
    invoke-direct {p0, p1, p2, p3}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 802993
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/4iK;->d:Z

    .line 802994
    const-class v0, LX/4iK;

    invoke-static {v0, p0}, LX/4iK;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 802995
    iget-object v0, p0, LX/4iK;->b:LX/0dy;

    invoke-interface {v0, p0}, LX/0dy;->a(Landroid/webkit/WebView;)V

    .line 802996
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, LX/4iK;

    new-instance v2, LX/0U8;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v3

    new-instance v0, LX/4iN;

    invoke-direct {v0, v1}, LX/4iN;-><init>(LX/0QB;)V

    invoke-direct {v2, v3, v0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v1, v2

    const v4, 0x7fffffff

    const/4 v3, 0x0

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v2, LX/4iK;->a:Ljava/lang/Class;

    const-string v3, "No ProxyWrapper instances provided by DI"

    invoke-static {v2, v3}, LX/01m;->c(Ljava/lang/Class;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/4cf;

    const/16 v0, 0x64

    move v0, v0

    if-ge v0, v4, :cond_2

    const/16 v3, 0x64

    move v3, v3

    :goto_2
    move v4, v3

    move-object v3, v2

    goto :goto_1

    :cond_1
    iget-object v2, v3, LX/4cf;->a:LX/0dx;

    move-object v2, v2

    iput-object v2, p1, LX/4iK;->b:LX/0dy;

    iget-object v2, v3, LX/4cf;->b:LX/1h1;

    move-object v2, v2

    iput-object v2, p1, LX/4iK;->c:LX/1h2;

    goto :goto_0

    :cond_2
    move-object v2, v3

    move v3, v4

    goto :goto_2
.end method


# virtual methods
.method public loadUrl(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 802985
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/4iK;->loadUrl(Ljava/lang/String;Ljava/util/Map;)V

    .line 802986
    return-void
.end method

.method public loadUrl(Ljava/lang/String;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 802982
    iget-object v0, p0, LX/4iK;->c:LX/1h2;

    invoke-interface {v0, p1}, LX/1h2;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 802983
    invoke-super {p0, v0, p2}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;Ljava/util/Map;)V

    .line 802984
    return-void
.end method

.method public setWebViewClient(Landroid/webkit/WebViewClient;)V
    .locals 1

    .prologue
    .line 802976
    iget-object v0, p0, LX/4iK;->b:LX/0dy;

    invoke-interface {v0}, LX/0dy;->f()Landroid/webkit/WebViewClient;

    move-result-object v0

    .line 802977
    if-eqz v0, :cond_0

    .line 802978
    invoke-super {p0, v0}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 802979
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4iK;->d:Z

    .line 802980
    return-void

    .line 802981
    :cond_0
    invoke-super {p0, p1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    goto :goto_0
.end method
