.class public LX/3fk;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile e:LX/3fk;


# instance fields
.field private final b:LX/0SG;

.field public final c:LX/2Iv;

.field private final d:LX/2It;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 623187
    const-class v0, LX/3fk;

    sput-object v0, LX/3fk;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0SG;LX/2Iv;LX/2It;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 623244
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 623245
    iput-object p1, p0, LX/3fk;->b:LX/0SG;

    .line 623246
    iput-object p2, p0, LX/3fk;->c:LX/2Iv;

    .line 623247
    iput-object p3, p0, LX/3fk;->d:LX/2It;

    .line 623248
    return-void
.end method

.method public static a(LX/0QB;)LX/3fk;
    .locals 6

    .prologue
    .line 623231
    sget-object v0, LX/3fk;->e:LX/3fk;

    if-nez v0, :cond_1

    .line 623232
    const-class v1, LX/3fk;

    monitor-enter v1

    .line 623233
    :try_start_0
    sget-object v0, LX/3fk;->e:LX/3fk;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 623234
    if-eqz v2, :cond_0

    .line 623235
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 623236
    new-instance p0, LX/3fk;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    invoke-static {v0}, LX/2Iv;->a(LX/0QB;)LX/2Iv;

    move-result-object v4

    check-cast v4, LX/2Iv;

    invoke-static {v0}, LX/2It;->b(LX/0QB;)LX/2It;

    move-result-object v5

    check-cast v5, LX/2It;

    invoke-direct {p0, v3, v4, v5}, LX/3fk;-><init>(LX/0SG;LX/2Iv;LX/2It;)V

    .line 623237
    move-object v0, p0

    .line 623238
    sput-object v0, LX/3fk;->e:LX/3fk;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 623239
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 623240
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 623241
    :cond_1
    sget-object v0, LX/3fk;->e:LX/3fk;

    return-object v0

    .line 623242
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 623243
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/util/Collection;Z)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 623188
    iget-object v0, p0, LX/3fk;->c:LX/2Iv;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 623189
    const v0, -0x577302c8

    invoke-static {v3, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 623190
    :try_start_0
    const-string v0, "favorite_contacts"

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v3, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 623191
    if-nez p2, :cond_0

    .line 623192
    const-string v0, "favorite_sms_contacts"

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v3, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 623193
    :cond_0
    const/4 v2, 0x0

    .line 623194
    const/4 v1, 0x0

    .line 623195
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 623196
    invoke-virtual {v0}, Lcom/facebook/user/model/User;->b()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 623197
    invoke-virtual {v0}, Lcom/facebook/user/model/User;->t()Lcom/facebook/user/model/UserPhoneNumber;

    move-result-object v5

    if-eqz v5, :cond_3

    invoke-virtual {v0}, Lcom/facebook/user/model/User;->t()Lcom/facebook/user/model/UserPhoneNumber;

    move-result-object v5

    .line 623198
    iget-object p2, v5, Lcom/facebook/user/model/UserPhoneNumber;->b:Ljava/lang/String;

    move-object v5, p2

    .line 623199
    invoke-static {v5}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 623200
    const v5, 0x3a83126f    # 0.001f

    add-float/2addr v1, v5

    .line 623201
    invoke-virtual {v0}, Lcom/facebook/user/model/User;->t()Lcom/facebook/user/model/UserPhoneNumber;

    move-result-object v0

    .line 623202
    iget-object v5, v0, Lcom/facebook/user/model/UserPhoneNumber;->b:Ljava/lang/String;

    move-object v0, v5

    .line 623203
    iget-object v5, p0, LX/3fk;->c:LX/2Iv;

    invoke-virtual {v5}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    .line 623204
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 623205
    sget-object v7, LX/30h;->a:LX/0U1;

    .line 623206
    iget-object v8, v7, LX/0U1;->d:Ljava/lang/String;

    move-object v7, v8

    .line 623207
    invoke-virtual {v6, v7, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 623208
    sget-object v7, LX/30h;->b:LX/0U1;

    .line 623209
    iget-object v8, v7, LX/0U1;->d:Ljava/lang/String;

    move-object v7, v8

    .line 623210
    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 623211
    const-string v7, "favorite_sms_contacts"

    const-string v8, ""

    const p2, 0x6b6621bc

    invoke-static {p2}, LX/03h;->a(I)V

    invoke-virtual {v5, v7, v8, v6}, Landroid/database/sqlite/SQLiteDatabase;->replaceOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v5, 0x3a429bd8

    invoke-static {v5}, LX/03h;->a(I)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 623212
    goto :goto_0

    .line 623213
    :catch_0
    move-exception v0

    .line 623214
    :try_start_1
    sget-object v1, LX/3fk;->a:Ljava/lang/Class;

    const-string v2, "SQLException"

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 623215
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 623216
    :catchall_0
    move-exception v0

    const v1, -0xf237f7e

    invoke-static {v3, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0

    .line 623217
    :cond_1
    add-int/lit8 v1, v2, 0x1

    .line 623218
    :try_start_2
    iget-object v2, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, v2

    .line 623219
    iget-object v2, p0, LX/3fk;->c:LX/2Iv;

    invoke-virtual {v2}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 623220
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 623221
    const-string v6, "fbid"

    invoke-virtual {v5, v6, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 623222
    const-string v6, "display_order"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 623223
    const-string v6, "favorite_contacts"

    const-string v7, ""

    const v8, 0x3d9ff3fa

    invoke-static {v8}, LX/03h;->a(I)V

    invoke-virtual {v2, v6, v7, v5}, Landroid/database/sqlite/SQLiteDatabase;->replaceOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v2, 0x32a0ae21

    invoke-static {v2}, LX/03h;->a(I)V

    .line 623224
    int-to-float v0, v1

    :goto_1
    move v2, v1

    move v1, v0

    .line 623225
    goto/16 :goto_0

    .line 623226
    :cond_2
    iget-object v0, p0, LX/3fk;->d:LX/2It;

    sget-object v1, LX/3Fg;->e:LX/3OK;

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/2Iu;->b(LX/0To;I)V

    .line 623227
    iget-object v0, p0, LX/3fk;->d:LX/2It;

    sget-object v1, LX/3Fg;->f:LX/3OK;

    iget-object v2, p0, LX/3fk;->b:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v4

    invoke-virtual {v0, v1, v4, v5}, LX/2Iu;->b(LX/0To;J)V

    .line 623228
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_2
    .catch Landroid/database/SQLException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 623229
    const v0, 0x2277e3e7

    invoke-static {v3, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 623230
    return-void

    :cond_3
    move v0, v1

    move v1, v2

    goto :goto_1
.end method
