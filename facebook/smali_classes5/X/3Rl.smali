.class public LX/3Rl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1MS;
.implements LX/1MT;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/3Rl;


# instance fields
.field public final a:LX/0SG;

.field public final b:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "LX/6FZ;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/03V;

.field private final d:LX/0W3;


# direct methods
.method public constructor <init>(LX/0SG;LX/03V;LX/0W3;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 581525
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 581526
    iput-object p1, p0, LX/3Rl;->a:LX/0SG;

    .line 581527
    iput-object p2, p0, LX/3Rl;->c:LX/03V;

    .line 581528
    invoke-static {}, LX/0UK;->b()Ljava/util/concurrent/ConcurrentLinkedQueue;

    move-result-object v0

    iput-object v0, p0, LX/3Rl;->b:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 581529
    iput-object p3, p0, LX/3Rl;->d:LX/0W3;

    .line 581530
    return-void
.end method

.method public static a(LX/0QB;)LX/3Rl;
    .locals 6

    .prologue
    .line 581574
    sget-object v0, LX/3Rl;->e:LX/3Rl;

    if-nez v0, :cond_1

    .line 581575
    const-class v1, LX/3Rl;

    monitor-enter v1

    .line 581576
    :try_start_0
    sget-object v0, LX/3Rl;->e:LX/3Rl;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 581577
    if-eqz v2, :cond_0

    .line 581578
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 581579
    new-instance p0, LX/3Rl;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v5

    check-cast v5, LX/0W3;

    invoke-direct {p0, v3, v4, v5}, LX/3Rl;-><init>(LX/0SG;LX/03V;LX/0W3;)V

    .line 581580
    move-object v0, p0

    .line 581581
    sput-object v0, LX/3Rl;->e:LX/3Rl;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 581582
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 581583
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 581584
    :cond_1
    sget-object v0, LX/3Rl;->e:LX/3Rl;

    return-object v0

    .line 581585
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 581586
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Ljava/io/File;)Landroid/net/Uri;
    .locals 14

    .prologue
    .line 581557
    new-instance v0, Ljava/io/File;

    const-string v1, "bugreport_operation_json.txt"

    invoke-direct {v0, p1, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 581558
    new-instance v2, Ljava/io/PrintWriter;

    invoke-direct {v2, v0}, Ljava/io/PrintWriter;-><init>(Ljava/io/File;)V

    const/4 v1, 0x0

    .line 581559
    :try_start_0
    new-instance v7, Lorg/json/JSONObject;

    invoke-direct {v7}, Lorg/json/JSONObject;-><init>()V

    .line 581560
    const/4 v5, 0x1

    .line 581561
    iget-object v6, p0, LX/3Rl;->b:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v6}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v6, v5

    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/6FZ;

    .line 581562
    iget-object v10, p0, LX/3Rl;->a:LX/0SG;

    invoke-interface {v10}, LX/0SG;->a()J

    move-result-wide v10

    iget-wide v12, v5, LX/6FZ;->a:J

    sub-long/2addr v10, v12

    const-wide/32 v12, 0xdbba0

    cmp-long v10, v10, v12

    if-lez v10, :cond_3

    const/4 v10, 0x1

    :goto_1
    move v9, v10

    .line 581563
    if-nez v9, :cond_0

    .line 581564
    invoke-static {v5}, LX/3Rl;->a(LX/6FZ;)Lorg/json/JSONObject;

    move-result-object v5

    .line 581565
    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 581566
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    .line 581567
    goto :goto_0

    .line 581568
    :cond_1
    move-object v3, v7

    .line 581569
    invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintWriter;->write(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 581570
    invoke-virtual {v2}, Ljava/io/PrintWriter;->close()V

    .line 581571
    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    return-object v0

    .line 581572
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 581573
    :catchall_0
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    :goto_2
    if-eqz v1, :cond_2

    :try_start_2
    invoke-virtual {v2}, Ljava/io/PrintWriter;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :goto_3
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_3

    :cond_2
    invoke-virtual {v2}, Ljava/io/PrintWriter;->close()V

    goto :goto_3

    :catchall_1
    move-exception v0

    goto :goto_2

    :cond_3
    const/4 v10, 0x0

    goto :goto_1
.end method

.method public static a(LX/6FZ;)Lorg/json/JSONObject;
    .locals 6
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 581555
    iget-object v0, p0, LX/6FZ;->c:LX/6FV;

    invoke-virtual {v0}, LX/6FV;->toString()Ljava/lang/String;

    move-result-object v0

    .line 581556
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    const-string v2, "recordTime"

    iget-wide v4, p0, LX/6FZ;->a:J

    invoke-virtual {v1, v2, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    move-result-object v1

    const-string v2, "category"

    invoke-virtual {v1, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "operation"

    iget-object v2, p0, LX/6FZ;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/6FV;)V
    .locals 3

    .prologue
    .line 581549
    iget-object v0, p0, LX/3Rl;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    .line 581550
    new-instance v2, LX/6FZ;

    invoke-direct {v2, v0, v1, p1, p2}, LX/6FZ;-><init>(JLjava/lang/String;LX/6FV;)V

    .line 581551
    iget-object v0, p0, LX/3Rl;->b:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    .line 581552
    iget-object v0, p0, LX/3Rl;->b:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->size()I

    move-result v0

    const/16 v1, 0x32

    if-le v0, v1, :cond_0

    .line 581553
    iget-object v0, p0, LX/3Rl;->b:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->remove()Ljava/lang/Object;

    .line 581554
    :cond_0
    return-void
.end method

.method public final getExtraFileFromWorkerThread(Ljava/io/File;)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 581539
    :try_start_0
    invoke-direct {p0, p1}, LX/3Rl;->a(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    .line 581540
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 581541
    const-string v2, "bugreport_operation_json.txt"

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 581542
    :goto_0
    return-object v0

    .line 581543
    :catch_0
    move-exception v0

    .line 581544
    iget-object v1, p0, LX/3Rl;->c:LX/03V;

    const-string v2, "BugReportOperationLogger"

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 581545
    throw v0

    .line 581546
    :catch_1
    move-exception v0

    .line 581547
    iget-object v1, p0, LX/3Rl;->c:LX/03V;

    const-string v2, "BugReportOperationLogger"

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 581548
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getFilesFromWorkerThread(Ljava/io/File;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;",
            ">;"
        }
    .end annotation

    .prologue
    .line 581533
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 581534
    :try_start_0
    invoke-direct {p0, p1}, LX/3Rl;->a(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    .line 581535
    new-instance v2, Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;

    const-string v3, "bugreport_operation_json.txt"

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v4, "text/plain"

    invoke-direct {v2, v3, v1, v4}, Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 581536
    return-object v0

    .line 581537
    :catch_0
    move-exception v0

    .line 581538
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Failed to write recent events file"

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final prepareDataForWriting()V
    .locals 0

    .prologue
    .line 581532
    return-void
.end method

.method public final shouldSendAsync()Z
    .locals 4

    .prologue
    .line 581531
    iget-object v0, p0, LX/3Rl;->d:LX/0W3;

    sget-wide v2, LX/0X5;->aM:J

    const/4 v1, 0x0

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JZ)Z

    move-result v0

    return v0
.end method
