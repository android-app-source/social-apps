.class public final LX/3pZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3pY;


# instance fields
.field public a:I

.field private b:Ljava/lang/CharSequence;

.field private c:Ljava/lang/CharSequence;

.field private d:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 641644
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 641645
    const/4 v0, 0x1

    iput v0, p0, LX/3pZ;->a:I

    .line 641646
    return-void
.end method


# virtual methods
.method public final a(LX/3pX;)LX/3pX;
    .locals 3

    .prologue
    .line 641621
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 641622
    iget v1, p0, LX/3pZ;->a:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    .line 641623
    const-string v1, "flags"

    iget v2, p0, LX/3pZ;->a:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 641624
    :cond_0
    iget-object v1, p0, LX/3pZ;->b:Ljava/lang/CharSequence;

    if-eqz v1, :cond_1

    .line 641625
    const-string v1, "inProgressLabel"

    iget-object v2, p0, LX/3pZ;->b:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 641626
    :cond_1
    iget-object v1, p0, LX/3pZ;->c:Ljava/lang/CharSequence;

    if-eqz v1, :cond_2

    .line 641627
    const-string v1, "confirmLabel"

    iget-object v2, p0, LX/3pZ;->c:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 641628
    :cond_2
    iget-object v1, p0, LX/3pZ;->d:Ljava/lang/CharSequence;

    if-eqz v1, :cond_3

    .line 641629
    const-string v1, "cancelLabel"

    iget-object v2, p0, LX/3pZ;->d:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 641630
    :cond_3
    iget-object v1, p1, LX/3pX;->d:Landroid/os/Bundle;

    move-object v1, v1

    .line 641631
    const-string v2, "android.wearable.EXTENSIONS"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 641632
    return-object p1
.end method

.method public final a(Z)LX/3pZ;
    .locals 3

    .prologue
    .line 641639
    const/4 v0, 0x1

    .line 641640
    if-eqz p1, :cond_0

    .line 641641
    iget v1, p0, LX/3pZ;->a:I

    or-int/2addr v1, v0

    iput v1, p0, LX/3pZ;->a:I

    .line 641642
    :goto_0
    return-object p0

    .line 641643
    :cond_0
    iget v1, p0, LX/3pZ;->a:I

    xor-int/lit8 v2, v0, -0x1

    and-int/2addr v1, v2

    iput v1, p0, LX/3pZ;->a:I

    goto :goto_0
.end method

.method public final clone()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 641633
    new-instance v0, LX/3pZ;

    invoke-direct {v0}, LX/3pZ;-><init>()V

    .line 641634
    iget v1, p0, LX/3pZ;->a:I

    iput v1, v0, LX/3pZ;->a:I

    .line 641635
    iget-object v1, p0, LX/3pZ;->b:Ljava/lang/CharSequence;

    iput-object v1, v0, LX/3pZ;->b:Ljava/lang/CharSequence;

    .line 641636
    iget-object v1, p0, LX/3pZ;->c:Ljava/lang/CharSequence;

    iput-object v1, v0, LX/3pZ;->c:Ljava/lang/CharSequence;

    .line 641637
    iget-object v1, p0, LX/3pZ;->d:Ljava/lang/CharSequence;

    iput-object v1, v0, LX/3pZ;->d:Ljava/lang/CharSequence;

    .line 641638
    return-object v0
.end method
