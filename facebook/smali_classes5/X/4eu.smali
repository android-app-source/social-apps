.class public LX/4eu;
.super LX/1Gj;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1Gj",
        "<",
        "LX/1us;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/concurrent/ExecutorService;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 797667
    const/4 v0, 0x3

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    invoke-direct {p0, v0}, LX/4eu;-><init>(Ljava/util/concurrent/ExecutorService;)V

    .line 797668
    return-void
.end method

.method private constructor <init>(Ljava/util/concurrent/ExecutorService;)V
    .locals 0
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .prologue
    .line 797669
    invoke-direct {p0}, LX/1Gj;-><init>()V

    .line 797670
    iput-object p1, p0, LX/4eu;->a:Ljava/util/concurrent/ExecutorService;

    .line 797671
    return-void
.end method

.method private static varargs a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 797672
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0, p0, p1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/4eu;Landroid/net/Uri;I)Ljava/net/HttpURLConnection;
    .locals 7

    .prologue
    const/4 v4, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 797673
    new-instance v0, Ljava/net/URL;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 797674
    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    move-object v0, v0

    .line 797675
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v1

    .line 797676
    const/16 v2, 0xc8

    if-lt v1, v2, :cond_5

    const/16 v2, 0x12c

    if-ge v1, v2, :cond_5

    const/4 v2, 0x1

    :goto_0
    move v2, v2

    .line 797677
    if-eqz v2, :cond_0

    .line 797678
    :goto_1
    return-object v0

    .line 797679
    :cond_0
    packed-switch v1, :pswitch_data_0

    .line 797680
    :pswitch_0
    const/4 v2, 0x0

    :goto_2
    move v2, v2

    .line 797681
    if-eqz v2, :cond_4

    .line 797682
    const-string v2, "Location"

    invoke-virtual {v0, v2}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 797683
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 797684
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 797685
    :goto_3
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    .line 797686
    if-lez p2, :cond_2

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 797687
    add-int/lit8 v1, p2, -0x1

    invoke-static {p0, v0, v1}, LX/4eu;->a(LX/4eu;Landroid/net/Uri;I)Ljava/net/HttpURLConnection;

    move-result-object v0

    goto :goto_1

    .line 797688
    :cond_1
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_3

    .line 797689
    :cond_2
    if-nez p2, :cond_3

    const-string v0, "URL %s follows too many redirects"

    new-array v1, v6, [Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {v0, v1}, LX/4eu;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 797690
    :goto_4
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 797691
    :cond_3
    const-string v0, "URL %s returned %d without a valid redirect"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v6

    invoke-static {v0, v2}, LX/4eu;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    .line 797692
    :cond_4
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 797693
    new-instance v0, Ljava/io/IOException;

    const-string v2, "Image URL %s returned HTTP code %d"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    const/4 v2, 0x0

    goto :goto_0

    .line 797694
    :pswitch_1
    const/4 v2, 0x1

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x12c
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/1cd;LX/1cW;)LX/1us;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/imagepipeline/producers/Consumer",
            "<",
            "LX/1FL;",
            ">;",
            "Lcom/facebook/imagepipeline/producers/ProducerContext;",
            ")",
            "LX/1us;"
        }
    .end annotation

    .prologue
    .line 797695
    new-instance v0, LX/1us;

    invoke-direct {v0, p1, p2}, LX/1us;-><init>(LX/1cd;LX/1cW;)V

    return-object v0
.end method

.method public final a(LX/1us;LX/1ut;)V
    .locals 3

    .prologue
    .line 797696
    iget-object v0, p0, LX/4eu;->a:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/imagepipeline/producers/HttpUrlConnectionNetworkFetcher$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/facebook/imagepipeline/producers/HttpUrlConnectionNetworkFetcher$1;-><init>(LX/4eu;LX/1us;LX/1ut;)V

    const v2, -0x68f59e8

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    move-result-object v0

    .line 797697
    iget-object v1, p1, LX/1us;->b:LX/1cW;

    move-object v1, v1

    .line 797698
    new-instance v2, LX/4et;

    invoke-direct {v2, p0, v0, p2}, LX/4et;-><init>(LX/4eu;Ljava/util/concurrent/Future;LX/1ut;)V

    invoke-virtual {v1, v2}, LX/1cW;->a(LX/1cg;)V

    .line 797699
    return-void
.end method

.method public final b(LX/1us;LX/1ut;)V
    .locals 3
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .prologue
    .line 797700
    const/4 v1, 0x0

    .line 797701
    :try_start_0
    invoke-virtual {p1}, LX/1us;->e()Landroid/net/Uri;

    move-result-object v0

    const/4 v2, 0x5

    invoke-static {p0, v0, v2}, LX/4eu;->a(LX/4eu;Landroid/net/Uri;I)Ljava/net/HttpURLConnection;

    move-result-object v1

    .line 797702
    if-eqz v1, :cond_0

    .line 797703
    const v0, 0x569636cb

    invoke-static {v1, v0}, LX/04e;->b(Ljava/net/URLConnection;I)Ljava/io/InputStream;

    move-result-object v0

    const/4 v2, -0x1

    invoke-virtual {p2, v0, v2}, LX/1ut;->a(Ljava/io/InputStream;I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 797704
    :cond_0
    if-eqz v1, :cond_1

    .line 797705
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 797706
    :cond_1
    :goto_0
    return-void

    .line 797707
    :catch_0
    move-exception v0

    .line 797708
    :try_start_1
    invoke-virtual {p2, v0}, LX/1ut;->a(Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 797709
    if-eqz v1, :cond_1

    .line 797710
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    goto :goto_0

    .line 797711
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_2

    .line 797712
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_2
    throw v0
.end method
