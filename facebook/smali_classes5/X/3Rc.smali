.class public LX/3Rc;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/3Rc;


# instance fields
.field public a:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Landroid/content/res/Resources;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 580959
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 580960
    return-void
.end method

.method public static a(LX/0QB;)LX/3Rc;
    .locals 5

    .prologue
    .line 580944
    sget-object v0, LX/3Rc;->c:LX/3Rc;

    if-nez v0, :cond_1

    .line 580945
    const-class v1, LX/3Rc;

    monitor-enter v1

    .line 580946
    :try_start_0
    sget-object v0, LX/3Rc;->c:LX/3Rc;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 580947
    if-eqz v2, :cond_0

    .line 580948
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 580949
    new-instance p0, LX/3Rc;

    invoke-direct {p0}, LX/3Rc;-><init>()V

    .line 580950
    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    .line 580951
    iput-object v3, p0, LX/3Rc;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object v4, p0, LX/3Rc;->b:Landroid/content/res/Resources;

    .line 580952
    move-object v0, p0

    .line 580953
    sput-object v0, LX/3Rc;->c:LX/3Rc;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 580954
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 580955
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 580956
    :cond_1
    sget-object v0, LX/3Rc;->c:LX/3Rc;

    return-object v0

    .line 580957
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 580958
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()I
    .locals 4
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation

    .prologue
    .line 580943
    iget-object v0, p0, LX/3Rc;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/3Kr;->Z:LX/0Tn;

    iget-object v2, p0, LX/3Rc;->b:Landroid/content/res/Resources;

    const v3, 0x7f0a01a6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v0

    return v0
.end method

.method public final a(I)I
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation

    .prologue
    .line 580938
    if-nez p1, :cond_0

    .line 580939
    invoke-virtual {p0}, LX/3Rc;->a()I

    move-result p1

    .line 580940
    :cond_0
    iget-object v0, p0, LX/3Rc;->b:Landroid/content/res/Resources;

    const v1, 0x7f0a01a6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    if-ne p1, v0, :cond_1

    .line 580941
    iget-object v0, p0, LX/3Rc;->b:Landroid/content/res/Resources;

    const v1, 0x7f0a03ed

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    .line 580942
    :cond_1
    return p1
.end method

.method public final b()I
    .locals 1
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation

    .prologue
    .line 580937
    invoke-virtual {p0}, LX/3Rc;->a()I

    move-result v0

    invoke-virtual {p0, v0}, LX/3Rc;->a(I)I

    move-result v0

    return v0
.end method
