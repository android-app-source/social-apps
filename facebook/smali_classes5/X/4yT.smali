.class public final LX/4yT;
.super LX/12m;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/12m",
        "<",
        "Lcom/google/common/collect/Multiset$Entry",
        "<TE;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:LX/4yO;


# direct methods
.method public constructor <init>(LX/4yO;)V
    .locals 0

    .prologue
    .line 821814
    iput-object p1, p0, LX/4yT;->this$0:LX/4yO;

    invoke-direct {p0}, LX/12m;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 821804
    iget-object v0, p0, LX/4yT;->this$0:LX/4yO;

    invoke-virtual {v0, p1}, LX/4yO;->a(I)LX/4wx;

    move-result-object v0

    return-object v0
.end method

.method public final contains(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 821807
    instance-of v1, p1, LX/4wx;

    if-eqz v1, :cond_0

    .line 821808
    check-cast p1, LX/4wx;

    .line 821809
    invoke-virtual {p1}, LX/4wx;->b()I

    move-result v1

    if-gtz v1, :cond_1

    .line 821810
    :cond_0
    :goto_0
    return v0

    .line 821811
    :cond_1
    iget-object v1, p0, LX/4yT;->this$0:LX/4yO;

    invoke-virtual {p1}, LX/4wx;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/4yO;->a(Ljava/lang/Object;)I

    move-result v1

    .line 821812
    invoke-virtual {p1}, LX/4wx;->b()I

    move-result v2

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 821813
    iget-object v0, p0, LX/4yT;->this$0:LX/4yO;

    invoke-virtual {v0}, LX/4yO;->hashCode()I

    move-result v0

    return v0
.end method

.method public final isPartialView()Z
    .locals 1

    .prologue
    .line 821806
    iget-object v0, p0, LX/4yT;->this$0:LX/4yO;

    invoke-virtual {v0}, LX/0Py;->isPartialView()Z

    move-result v0

    return v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 821805
    iget-object v0, p0, LX/4yT;->this$0:LX/4yO;

    invoke-virtual {v0}, LX/4yO;->d()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    return v0
.end method

.method public final writeReplace()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 821803
    new-instance v0, LX/4yU;

    iget-object v1, p0, LX/4yT;->this$0:LX/4yO;

    invoke-direct {v0, v1}, LX/4yU;-><init>(LX/4yO;)V

    return-object v0
.end method
