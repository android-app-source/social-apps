.class public LX/3qU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable",
        "<",
        "Landroid/content/Intent;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:LX/3qR;


# instance fields
.field public final b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Landroid/content/Context;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 642215
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 642216
    new-instance v0, LX/3qT;

    invoke-direct {v0}, LX/3qT;-><init>()V

    sput-object v0, LX/3qU;->a:LX/3qR;

    .line 642217
    :goto_0
    return-void

    .line 642218
    :cond_0
    new-instance v0, LX/3qS;

    invoke-direct {v0}, LX/3qS;-><init>()V

    sput-object v0, LX/3qU;->a:LX/3qR;

    goto :goto_0
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 642219
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 642220
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/3qU;->b:Ljava/util/ArrayList;

    .line 642221
    iput-object p1, p0, LX/3qU;->c:Landroid/content/Context;

    .line 642222
    return-void
.end method

.method private a(Landroid/content/ComponentName;)LX/3qU;
    .locals 3

    .prologue
    .line 642223
    iget-object v0, p0, LX/3qU;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 642224
    :try_start_0
    iget-object v0, p0, LX/3qU;->c:Landroid/content/Context;

    invoke-static {v0, p1}, LX/3pR;->a(Landroid/content/Context;Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    .line 642225
    :goto_0
    if-eqz v0, :cond_0

    .line 642226
    iget-object v2, p0, LX/3qU;->b:Ljava/util/ArrayList;

    invoke-virtual {v2, v1, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 642227
    iget-object v2, p0, LX/3qU;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    invoke-static {v2, v0}, LX/3pR;->a(Landroid/content/Context;Landroid/content/ComponentName;)Landroid/content/Intent;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 642228
    :catch_0
    move-exception v0

    .line 642229
    const-string v1, "TaskStackBuilder"

    const-string v2, "Bad ComponentName while traversing activity parent metadata"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 642230
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 642231
    :cond_0
    return-object p0
.end method

.method public static a(Landroid/content/Context;)LX/3qU;
    .locals 1

    .prologue
    .line 642232
    new-instance v0, LX/3qU;

    invoke-direct {v0, p0}, LX/3qU;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private a(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 642233
    iget-object v0, p0, LX/3qU;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 642234
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No intents added to TaskStackBuilder; cannot startActivities"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 642235
    :cond_0
    iget-object v0, p0, LX/3qU;->b:Ljava/util/ArrayList;

    iget-object v1, p0, LX/3qU;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Landroid/content/Intent;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/content/Intent;

    .line 642236
    new-instance v1, Landroid/content/Intent;

    aget-object v2, v0, v3

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    const v2, 0x1000c000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v1

    aput-object v1, v0, v3

    .line 642237
    iget-object v1, p0, LX/3qU;->c:Landroid/content/Context;

    const/4 v2, 0x1

    .line 642238
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 642239
    const/16 v4, 0x10

    if-lt v3, v4, :cond_2

    .line 642240
    invoke-virtual {v1, v0, p1}, Landroid/content/Context;->startActivities([Landroid/content/Intent;Landroid/os/Bundle;)V

    .line 642241
    :goto_0
    move v1, v2

    .line 642242
    if-nez v1, :cond_1

    .line 642243
    new-instance v1, Landroid/content/Intent;

    array-length v2, v0

    add-int/lit8 v2, v2, -0x1

    aget-object v0, v0, v2

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 642244
    const/high16 v0, 0x10000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 642245
    iget-object v0, p0, LX/3qU;->c:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 642246
    :cond_1
    return-void

    .line 642247
    :cond_2
    const/16 v4, 0xb

    if-lt v3, v4, :cond_3

    .line 642248
    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivities([Landroid/content/Intent;)V

    .line 642249
    goto :goto_0

    .line 642250
    :cond_3
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/app/Activity;)LX/3qU;
    .locals 2

    .prologue
    .line 642251
    const/4 v0, 0x0

    .line 642252
    instance-of v1, p1, LX/3qQ;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 642253
    check-cast v0, LX/3qQ;

    invoke-interface {v0}, LX/3qQ;->a()Landroid/content/Intent;

    move-result-object v0

    .line 642254
    :cond_0
    if-nez v0, :cond_3

    .line 642255
    invoke-static {p1}, LX/3pR;->b(Landroid/app/Activity;)Landroid/content/Intent;

    move-result-object v0

    move-object v1, v0

    .line 642256
    :goto_0
    if-eqz v1, :cond_2

    .line 642257
    invoke-virtual {v1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    .line 642258
    if-nez v0, :cond_1

    .line 642259
    iget-object v0, p0, LX/3qU;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v0

    .line 642260
    :cond_1
    invoke-direct {p0, v0}, LX/3qU;->a(Landroid/content/ComponentName;)LX/3qU;

    .line 642261
    invoke-virtual {p0, v1}, LX/3qU;->a(Landroid/content/Intent;)LX/3qU;

    .line 642262
    :cond_2
    return-object p0

    :cond_3
    move-object v1, v0

    goto :goto_0
.end method

.method public final a(Landroid/content/Intent;)LX/3qU;
    .locals 1

    .prologue
    .line 642263
    iget-object v0, p0, LX/3qU;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 642264
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 642265
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/3qU;->a(Landroid/os/Bundle;)V

    .line 642266
    return-void
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 642267
    iget-object v0, p0, LX/3qU;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method
