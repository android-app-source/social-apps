.class public LX/4rE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4qy;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/4qy",
        "<",
        "LX/4rE;",
        ">;"
    }
.end annotation


# instance fields
.field public _customIdResolver:LX/4qx;

.field public _defaultImpl:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public _idType:LX/4pP;

.field public _includeAs:LX/4pO;

.field public _typeIdVisible:Z

.field public _typeProperty:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 815316
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 815317
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/4rE;->_typeIdVisible:Z

    .line 815318
    return-void
.end method

.method private a(LX/0m4;LX/0lJ;Ljava/util/Collection;ZZ)LX/4qx;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0m4",
            "<*>;",
            "LX/0lJ;",
            "Ljava/util/Collection",
            "<",
            "LX/4qu;",
            ">;ZZ)",
            "LX/4qx;"
        }
    .end annotation

    .prologue
    .line 815305
    iget-object v0, p0, LX/4rE;->_customIdResolver:LX/4qx;

    if-eqz v0, :cond_0

    .line 815306
    iget-object v0, p0, LX/4rE;->_customIdResolver:LX/4qx;

    .line 815307
    :goto_0
    return-object v0

    .line 815308
    :cond_0
    iget-object v0, p0, LX/4rE;->_idType:LX/4pP;

    if-nez v0, :cond_1

    .line 815309
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can not build, \'init()\' not yet called"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 815310
    :cond_1
    sget-object v0, LX/4rD;->b:[I

    iget-object v1, p0, LX/4rE;->_idType:LX/4pP;

    invoke-virtual {v1}, LX/4pP;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 815311
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Do not know how to construct standard type id resolver for idType: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/4rE;->_idType:LX/4pP;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 815312
    :pswitch_0
    new-instance v0, LX/4rB;

    invoke-virtual {p1}, LX/0m4;->n()LX/0li;

    move-result-object v1

    invoke-direct {v0, p2, v1}, LX/4rB;-><init>(LX/0lJ;LX/0li;)V

    goto :goto_0

    .line 815313
    :pswitch_1
    new-instance v0, LX/4rC;

    invoke-virtual {p1}, LX/0m4;->n()LX/0li;

    move-result-object v1

    invoke-direct {v0, p2, v1}, LX/4rC;-><init>(LX/0lJ;LX/0li;)V

    goto :goto_0

    .line 815314
    :pswitch_2
    invoke-static {p1, p2, p3, p4, p5}, LX/4rF;->a(LX/0m4;LX/0lJ;Ljava/util/Collection;ZZ)LX/4rF;

    move-result-object v0

    goto :goto_0

    .line 815315
    :pswitch_3
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static b()LX/4rE;
    .locals 3

    .prologue
    .line 815304
    new-instance v0, LX/4rE;

    invoke-direct {v0}, LX/4rE;-><init>()V

    sget-object v1, LX/4pP;->NONE:LX/4pP;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, LX/4rE;->b(LX/4pP;LX/4qx;)LX/4rE;

    move-result-object v0

    return-object v0
.end method

.method private b(LX/4pO;)LX/4rE;
    .locals 2

    .prologue
    .line 815300
    if-nez p1, :cond_0

    .line 815301
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "includeAs can not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 815302
    :cond_0
    iput-object p1, p0, LX/4rE;->_includeAs:LX/4pO;

    .line 815303
    return-object p0
.end method

.method private b(LX/4pP;LX/4qx;)LX/4rE;
    .locals 2

    .prologue
    .line 815294
    if-nez p1, :cond_0

    .line 815295
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "idType can not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 815296
    :cond_0
    iput-object p1, p0, LX/4rE;->_idType:LX/4pP;

    .line 815297
    iput-object p2, p0, LX/4rE;->_customIdResolver:LX/4qx;

    .line 815298
    invoke-virtual {p1}, LX/4pP;->getDefaultPropertyName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/4rE;->_typeProperty:Ljava/lang/String;

    .line 815299
    return-object p0
.end method

.method private b(Ljava/lang/String;)LX/4rE;
    .locals 1

    .prologue
    .line 815290
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 815291
    :cond_0
    iget-object v0, p0, LX/4rE;->_idType:LX/4pP;

    invoke-virtual {v0}, LX/4pP;->getDefaultPropertyName()Ljava/lang/String;

    move-result-object p1

    .line 815292
    :cond_1
    iput-object p1, p0, LX/4rE;->_typeProperty:Ljava/lang/String;

    .line 815293
    return-object p0
.end method


# virtual methods
.method public final a(LX/0mu;LX/0lJ;Ljava/util/Collection;)LX/4qw;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0mu;",
            "LX/0lJ;",
            "Ljava/util/Collection",
            "<",
            "LX/4qu;",
            ">;)",
            "LX/4qw;"
        }
    .end annotation

    .prologue
    .line 815280
    iget-object v0, p0, LX/4rE;->_idType:LX/4pP;

    sget-object v1, LX/4pP;->NONE:LX/4pP;

    if-ne v0, v1, :cond_0

    .line 815281
    const/4 v0, 0x0

    .line 815282
    :goto_0
    return-object v0

    .line 815283
    :cond_0
    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, LX/4rE;->a(LX/0m4;LX/0lJ;Ljava/util/Collection;ZZ)LX/4qx;

    move-result-object v2

    .line 815284
    sget-object v0, LX/4rD;->a:[I

    iget-object v1, p0, LX/4rE;->_includeAs:LX/4pO;

    invoke-virtual {v1}, LX/4pO;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 815285
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Do not know how to construct standard type serializer for inclusion type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/4rE;->_includeAs:LX/4pO;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 815286
    :pswitch_0
    new-instance v0, LX/4r1;

    iget-object v3, p0, LX/4rE;->_typeProperty:Ljava/lang/String;

    iget-boolean v4, p0, LX/4rE;->_typeIdVisible:Z

    iget-object v5, p0, LX/4rE;->_defaultImpl:Ljava/lang/Class;

    move-object v1, p2

    invoke-direct/range {v0 .. v5}, LX/4r1;-><init>(LX/0lJ;LX/4qx;Ljava/lang/String;ZLjava/lang/Class;)V

    goto :goto_0

    .line 815287
    :pswitch_1
    new-instance v0, LX/4r6;

    iget-object v3, p0, LX/4rE;->_typeProperty:Ljava/lang/String;

    iget-boolean v4, p0, LX/4rE;->_typeIdVisible:Z

    iget-object v5, p0, LX/4rE;->_defaultImpl:Ljava/lang/Class;

    move-object v1, p2

    invoke-direct/range {v0 .. v5}, LX/4r6;-><init>(LX/0lJ;LX/4qx;Ljava/lang/String;ZLjava/lang/Class;)V

    goto :goto_0

    .line 815288
    :pswitch_2
    new-instance v0, LX/4r8;

    iget-object v1, p0, LX/4rE;->_typeProperty:Ljava/lang/String;

    iget-boolean v3, p0, LX/4rE;->_typeIdVisible:Z

    invoke-direct {v0, p2, v2, v1, v3}, LX/4r8;-><init>(LX/0lJ;LX/4qx;Ljava/lang/String;Z)V

    goto :goto_0

    .line 815289
    :pswitch_3
    new-instance v0, LX/4r4;

    iget-object v3, p0, LX/4rE;->_typeProperty:Ljava/lang/String;

    iget-boolean v4, p0, LX/4rE;->_typeIdVisible:Z

    iget-object v5, p0, LX/4rE;->_defaultImpl:Ljava/lang/Class;

    move-object v1, p2

    invoke-direct/range {v0 .. v5}, LX/4r4;-><init>(LX/0lJ;LX/4qx;Ljava/lang/String;ZLjava/lang/Class;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final synthetic a(LX/4pO;)LX/4qy;
    .locals 1

    .prologue
    .line 815319
    invoke-direct {p0, p1}, LX/4rE;->b(LX/4pO;)LX/4rE;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(LX/4pP;LX/4qx;)LX/4qy;
    .locals 1

    .prologue
    .line 815279
    invoke-direct {p0, p1, p2}, LX/4rE;->b(LX/4pP;LX/4qx;)LX/4rE;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Ljava/lang/Class;)LX/4qy;
    .locals 1

    .prologue
    .line 815276
    iput-object p1, p0, LX/4rE;->_defaultImpl:Ljava/lang/Class;

    .line 815277
    move-object v0, p0

    .line 815278
    return-object v0
.end method

.method public final synthetic a(Ljava/lang/String;)LX/4qy;
    .locals 1

    .prologue
    .line 815275
    invoke-direct {p0, p1}, LX/4rE;->b(Ljava/lang/String;)LX/4rE;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Z)LX/4qy;
    .locals 1

    .prologue
    .line 815272
    iput-boolean p1, p0, LX/4rE;->_typeIdVisible:Z

    .line 815273
    move-object v0, p0

    .line 815274
    return-object v0
.end method

.method public final a(LX/0m2;LX/0lJ;Ljava/util/Collection;)LX/4qz;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0m2;",
            "LX/0lJ;",
            "Ljava/util/Collection",
            "<",
            "LX/4qu;",
            ">;)",
            "LX/4qz;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 815263
    iget-object v0, p0, LX/4rE;->_idType:LX/4pP;

    sget-object v1, LX/4pP;->NONE:LX/4pP;

    if-ne v0, v1, :cond_0

    move-object v0, v6

    .line 815264
    :goto_0
    return-object v0

    .line 815265
    :cond_0
    const/4 v4, 0x1

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, LX/4rE;->a(LX/0m4;LX/0lJ;Ljava/util/Collection;ZZ)LX/4qx;

    move-result-object v1

    .line 815266
    sget-object v0, LX/4rD;->a:[I

    iget-object v2, p0, LX/4rE;->_includeAs:LX/4pO;

    invoke-virtual {v2}, LX/4pO;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 815267
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Do not know how to construct standard type serializer for inclusion type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/4rE;->_includeAs:LX/4pO;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 815268
    :pswitch_0
    new-instance v0, LX/4r3;

    invoke-direct {v0, v1, v6}, LX/4r3;-><init>(LX/4qx;LX/2Ay;)V

    goto :goto_0

    .line 815269
    :pswitch_1
    new-instance v0, LX/4r7;

    iget-object v2, p0, LX/4rE;->_typeProperty:Ljava/lang/String;

    invoke-direct {v0, v1, v6, v2}, LX/4r7;-><init>(LX/4qx;LX/2Ay;Ljava/lang/String;)V

    goto :goto_0

    .line 815270
    :pswitch_2
    new-instance v0, LX/4r9;

    invoke-direct {v0, v1, v6}, LX/4r9;-><init>(LX/4qx;LX/2Ay;)V

    goto :goto_0

    .line 815271
    :pswitch_3
    new-instance v0, LX/4r5;

    iget-object v2, p0, LX/4rE;->_typeProperty:Ljava/lang/String;

    invoke-direct {v0, v1, v6, v2}, LX/4r5;-><init>(LX/4qx;LX/2Ay;Ljava/lang/String;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 815262
    iget-object v0, p0, LX/4rE;->_defaultImpl:Ljava/lang/Class;

    return-object v0
.end method
