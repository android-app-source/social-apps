.class public LX/3V8;
.super LX/3V4;
.source ""

# interfaces
.implements LX/3V9;


# static fields
.field public static final l:LX/1Cz;


# instance fields
.field private final n:Landroid/widget/ImageView;

.field private final o:Landroid/widget/ImageView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 587559
    new-instance v0, LX/3VA;

    invoke-direct {v0}, LX/3VA;-><init>()V

    sput-object v0, LX/3V8;->l:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 587560
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/3V8;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 587561
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 587562
    const v0, 0x7f03087c

    invoke-direct {p0, p1, p2, v0}, LX/3V4;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 587563
    const v0, 0x7f0d0bde

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/3V8;->n:Landroid/widget/ImageView;

    .line 587564
    const v0, 0x7f0d1614

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/3V8;->o:Landroid/widget/ImageView;

    .line 587565
    return-void
.end method


# virtual methods
.method public final a_(LX/0hs;)V
    .locals 1

    .prologue
    .line 587566
    iget-object v0, p0, LX/3V8;->o:Landroid/widget/ImageView;

    invoke-virtual {p1, v0}, LX/0ht;->f(Landroid/view/View;)V

    .line 587567
    return-void
.end method
