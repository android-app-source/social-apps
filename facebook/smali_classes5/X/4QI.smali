.class public LX/4QI;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 704053
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 16

    .prologue
    .line 704054
    const/4 v12, 0x0

    .line 704055
    const/4 v11, 0x0

    .line 704056
    const/4 v10, 0x0

    .line 704057
    const/4 v9, 0x0

    .line 704058
    const/4 v8, 0x0

    .line 704059
    const/4 v7, 0x0

    .line 704060
    const/4 v6, 0x0

    .line 704061
    const/4 v5, 0x0

    .line 704062
    const/4 v4, 0x0

    .line 704063
    const/4 v3, 0x0

    .line 704064
    const/4 v2, 0x0

    .line 704065
    const/4 v1, 0x0

    .line 704066
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->START_OBJECT:LX/15z;

    if-eq v13, v14, :cond_1

    .line 704067
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 704068
    const/4 v1, 0x0

    .line 704069
    :goto_0
    return v1

    .line 704070
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 704071
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->END_OBJECT:LX/15z;

    if-eq v13, v14, :cond_b

    .line 704072
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v13

    .line 704073
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 704074
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v14, v15, :cond_1

    if-eqz v13, :cond_1

    .line 704075
    const-string v14, "body"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 704076
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v12

    goto :goto_1

    .line 704077
    :cond_2
    const-string v14, "field_key"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 704078
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    goto :goto_1

    .line 704079
    :cond_3
    const-string v14, "field_type"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_4

    .line 704080
    const/4 v2, 0x1

    .line 704081
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/facebook/graphql/enums/GraphQLPageCtaConfigFieldType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageCtaConfigFieldType;

    move-result-object v10

    goto :goto_1

    .line 704082
    :cond_4
    const-string v14, "field_value"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 704083
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 704084
    :cond_5
    const-string v14, "hint"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 704085
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 704086
    :cond_6
    const-string v14, "optional"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_7

    .line 704087
    const/4 v1, 0x1

    .line 704088
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v7

    goto :goto_1

    .line 704089
    :cond_7
    const-string v14, "options"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_8

    .line 704090
    invoke-static/range {p0 .. p1}, LX/4QL;->a(LX/15w;LX/186;)I

    move-result v6

    goto/16 :goto_1

    .line 704091
    :cond_8
    const-string v14, "subfields"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_9

    .line 704092
    invoke-static/range {p0 .. p1}, LX/4QJ;->a(LX/15w;LX/186;)I

    move-result v5

    goto/16 :goto_1

    .line 704093
    :cond_9
    const-string v14, "subtitle"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_a

    .line 704094
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto/16 :goto_1

    .line 704095
    :cond_a
    const-string v14, "title"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 704096
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto/16 :goto_1

    .line 704097
    :cond_b
    const/16 v13, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->c(I)V

    .line 704098
    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v12}, LX/186;->b(II)V

    .line 704099
    const/4 v12, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v11}, LX/186;->b(II)V

    .line 704100
    if-eqz v2, :cond_c

    .line 704101
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->a(ILjava/lang/Enum;)V

    .line 704102
    :cond_c
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 704103
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 704104
    if-eqz v1, :cond_d

    .line 704105
    const/4 v1, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v7}, LX/186;->a(IZ)V

    .line 704106
    :cond_d
    const/4 v1, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 704107
    const/4 v1, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 704108
    const/16 v1, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 704109
    const/16 v1, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 704110
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 704111
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 704112
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 704113
    if-eqz v0, :cond_0

    .line 704114
    const-string v1, "body"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 704115
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 704116
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 704117
    if-eqz v0, :cond_1

    .line 704118
    const-string v1, "field_key"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 704119
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 704120
    :cond_1
    invoke-virtual {p0, p1, v3, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 704121
    if-eqz v0, :cond_2

    .line 704122
    const-string v0, "field_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 704123
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPageCtaConfigFieldType;

    invoke-virtual {p0, p1, v3, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageCtaConfigFieldType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPageCtaConfigFieldType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 704124
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 704125
    if-eqz v0, :cond_3

    .line 704126
    const-string v1, "field_value"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 704127
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 704128
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 704129
    if-eqz v0, :cond_4

    .line 704130
    const-string v1, "hint"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 704131
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 704132
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 704133
    if-eqz v0, :cond_5

    .line 704134
    const-string v1, "optional"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 704135
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 704136
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 704137
    if-eqz v0, :cond_7

    .line 704138
    const-string v1, "options"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 704139
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 704140
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_6

    .line 704141
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2}, LX/4QL;->a(LX/15i;ILX/0nX;)V

    .line 704142
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 704143
    :cond_6
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 704144
    :cond_7
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 704145
    if-eqz v0, :cond_8

    .line 704146
    const-string v1, "subfields"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 704147
    invoke-static {p0, v0, p2, p3}, LX/4QJ;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 704148
    :cond_8
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 704149
    if-eqz v0, :cond_9

    .line 704150
    const-string v1, "subtitle"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 704151
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 704152
    :cond_9
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 704153
    if-eqz v0, :cond_a

    .line 704154
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 704155
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 704156
    :cond_a
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 704157
    return-void
.end method
