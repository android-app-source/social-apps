.class public LX/4sF;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/12A;

.field public final b:Ljava/io/InputStream;

.field public final c:[B

.field public d:I

.field public e:I

.field public final f:Z

.field public g:I


# direct methods
.method public constructor <init>(LX/12A;Ljava/io/InputStream;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 819325
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 819326
    iput-object p1, p0, LX/4sF;->a:LX/12A;

    .line 819327
    iput-object p2, p0, LX/4sF;->b:Ljava/io/InputStream;

    .line 819328
    invoke-virtual {p1}, LX/12A;->e()[B

    move-result-object v0

    iput-object v0, p0, LX/4sF;->c:[B

    .line 819329
    iput v1, p0, LX/4sF;->d:I

    iput v1, p0, LX/4sF;->e:I

    .line 819330
    iput v1, p0, LX/4sF;->g:I

    .line 819331
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4sF;->f:Z

    .line 819332
    return-void
.end method

.method public constructor <init>(LX/12A;[BII)V
    .locals 1

    .prologue
    .line 819316
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 819317
    iput-object p1, p0, LX/4sF;->a:LX/12A;

    .line 819318
    const/4 v0, 0x0

    iput-object v0, p0, LX/4sF;->b:Ljava/io/InputStream;

    .line 819319
    iput-object p2, p0, LX/4sF;->c:[B

    .line 819320
    iput p3, p0, LX/4sF;->d:I

    .line 819321
    add-int v0, p3, p4

    iput v0, p0, LX/4sF;->e:I

    .line 819322
    neg-int v0, p3

    iput v0, p0, LX/4sF;->g:I

    .line 819323
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/4sF;->f:Z

    .line 819324
    return-void
.end method

.method private a(I)Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 819306
    iget-object v1, p0, LX/4sF;->b:Ljava/io/InputStream;

    if-nez v1, :cond_1

    .line 819307
    :cond_0
    :goto_0
    return v0

    .line 819308
    :cond_1
    iget v1, p0, LX/4sF;->e:I

    iget v2, p0, LX/4sF;->d:I

    sub-int/2addr v1, v2

    .line 819309
    :goto_1
    if-ge v1, p1, :cond_2

    .line 819310
    iget-object v2, p0, LX/4sF;->b:Ljava/io/InputStream;

    iget-object v3, p0, LX/4sF;->c:[B

    iget v4, p0, LX/4sF;->e:I

    iget-object v5, p0, LX/4sF;->c:[B

    array-length v5, v5

    iget v6, p0, LX/4sF;->e:I

    sub-int/2addr v5, v6

    invoke-virtual {v2, v3, v4, v5}, Ljava/io/InputStream;->read([BII)I

    move-result v2

    .line 819311
    if-lez v2, :cond_0

    .line 819312
    iget v3, p0, LX/4sF;->e:I

    add-int/2addr v3, v2

    iput v3, p0, LX/4sF;->e:I

    .line 819313
    add-int/2addr v1, v2

    .line 819314
    goto :goto_1

    .line 819315
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(IIZLX/0lD;LX/0lv;)LX/4sE;
    .locals 10

    .prologue
    .line 819291
    invoke-virtual {p5, p3}, LX/0lv;->a(Z)LX/0lv;

    move-result-object v4

    .line 819292
    const/4 v0, 0x1

    invoke-direct {p0, v0}, LX/4sF;->a(I)Z

    .line 819293
    new-instance v0, LX/4sE;

    iget-object v1, p0, LX/4sF;->a:LX/12A;

    iget-object v5, p0, LX/4sF;->b:Ljava/io/InputStream;

    iget-object v6, p0, LX/4sF;->c:[B

    iget v7, p0, LX/4sF;->d:I

    iget v8, p0, LX/4sF;->e:I

    iget-boolean v9, p0, LX/4sF;->f:Z

    move v2, p1

    move-object v3, p4

    invoke-direct/range {v0 .. v9}, LX/4sE;-><init>(LX/12A;ILX/0lD;LX/0lv;Ljava/io/InputStream;[BIIZ)V

    .line 819294
    const/4 v1, 0x0

    .line 819295
    iget v2, p0, LX/4sF;->d:I

    iget v3, p0, LX/4sF;->e:I

    if-ge v2, v3, :cond_4

    .line 819296
    iget-object v2, p0, LX/4sF;->c:[B

    iget v3, p0, LX/4sF;->d:I

    aget-byte v2, v2, v3

    const/16 v3, 0x3a

    if-ne v2, v3, :cond_0

    .line 819297
    const/4 v1, 0x1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LX/4sE;->a(ZZ)Z

    move-result v1

    .line 819298
    :cond_0
    if-nez v1, :cond_4

    sget-object v1, LX/4sD;->REQUIRE_HEADER:LX/4sD;

    invoke-virtual {v1}, LX/4sD;->getMask()I

    move-result v1

    and-int/2addr v1, p2

    if-eqz v1, :cond_4

    .line 819299
    iget v0, p0, LX/4sF;->d:I

    iget v1, p0, LX/4sF;->e:I

    if-ge v0, v1, :cond_2

    iget-object v0, p0, LX/4sF;->c:[B

    iget v1, p0, LX/4sF;->d:I

    aget-byte v0, v0, v1

    .line 819300
    :goto_0
    const/16 v1, 0x7b

    if-eq v0, v1, :cond_1

    const/16 v1, 0x5b

    if-ne v0, v1, :cond_3

    .line 819301
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Input does not start with Smile format header (first byte = 0x"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    and-int/lit16 v2, v0, 0xff

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") -- rather, it starts with \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    int-to-char v0, v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\' (plain JSON input?) -- can not parse"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 819302
    :goto_1
    new-instance v1, LX/2aQ;

    sget-object v2, LX/28G;->a:LX/28G;

    invoke-direct {v1, v0, v2}, LX/2aQ;-><init>(Ljava/lang/String;LX/28G;)V

    throw v1

    .line 819303
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 819304
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Input does not start with Smile format header (first byte = 0x"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    and-int/lit16 v0, v0, 0xff

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") and parser has REQUIRE_HEADER enabled: can not parse"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 819305
    :cond_4
    return-object v0
.end method
