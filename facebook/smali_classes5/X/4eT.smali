.class public LX/4eT;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "LX/1G9;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/1G9;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 797330
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/1G9;
    .locals 3

    .prologue
    .line 797318
    sget-object v0, LX/4eT;->a:LX/1G9;

    if-nez v0, :cond_1

    .line 797319
    const-class v1, LX/4eT;

    monitor-enter v1

    .line 797320
    :try_start_0
    sget-object v0, LX/4eT;->a:LX/1G9;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 797321
    if-eqz v2, :cond_0

    .line 797322
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 797323
    invoke-static {v0}, LX/1Fr;->a(LX/0QB;)Lcom/facebook/imagepipeline/animated/factory/AnimatedFactoryImpl;

    invoke-static {v0}, LX/4eU;->a(LX/0QB;)LX/1bw;

    move-result-object p0

    check-cast p0, LX/1bw;

    invoke-static {p0}, LX/1Aq;->a(LX/1bw;)LX/1G9;

    move-result-object p0

    move-object v0, p0

    .line 797324
    sput-object v0, LX/4eT;->a:LX/1G9;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 797325
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 797326
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 797327
    :cond_1
    sget-object v0, LX/4eT;->a:LX/1G9;

    return-object v0

    .line 797328
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 797329
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 797317
    invoke-static {p0}, LX/1Fr;->a(LX/0QB;)Lcom/facebook/imagepipeline/animated/factory/AnimatedFactoryImpl;

    invoke-static {p0}, LX/4eU;->a(LX/0QB;)LX/1bw;

    move-result-object v0

    check-cast v0, LX/1bw;

    invoke-static {v0}, LX/1Aq;->a(LX/1bw;)LX/1G9;

    move-result-object v0

    return-object v0
.end method
