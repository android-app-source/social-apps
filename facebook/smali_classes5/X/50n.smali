.class public LX/50n;
.super LX/4x0;
.source ""

# interfaces
.implements Ljava/util/SortedSet;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "LX/4x0",
        "<TE;>;",
        "Ljava/util/SortedSet",
        "<TE;>;"
    }
.end annotation


# instance fields
.field public final a:LX/4x9;
    .annotation build Lcom/google/j2objc/annotations/Weak;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4x9",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/4x9;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4x9",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 824073
    invoke-direct {p0}, LX/4x0;-><init>()V

    .line 824074
    iput-object p1, p0, LX/50n;->a:LX/4x9;

    .line 824075
    return-void
.end method


# virtual methods
.method public final synthetic a()LX/1M1;
    .locals 1

    .prologue
    .line 824071
    iget-object v0, p0, LX/50n;->a:LX/4x9;

    move-object v0, v0

    .line 824072
    return-object v0
.end method

.method public final comparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<-TE;>;"
        }
    .end annotation

    .prologue
    .line 824069
    iget-object v0, p0, LX/50n;->a:LX/4x9;

    move-object v0, v0

    .line 824070
    invoke-interface {v0}, LX/4x9;->comparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public final first()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 824067
    iget-object v0, p0, LX/50n;->a:LX/4x9;

    move-object v0, v0

    .line 824068
    invoke-interface {v0}, LX/4x9;->h()LX/4wx;

    move-result-object v0

    invoke-static {v0}, LX/50p;->c(LX/4wx;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final headSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)",
            "Ljava/util/SortedSet",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 824059
    iget-object v0, p0, LX/50n;->a:LX/4x9;

    move-object v0, v0

    .line 824060
    sget-object v1, LX/4xG;->OPEN:LX/4xG;

    invoke-interface {v0, p1, v1}, LX/4x9;->a(Ljava/lang/Object;LX/4xG;)LX/4x9;

    move-result-object v0

    invoke-interface {v0}, LX/4x9;->g()Ljava/util/NavigableSet;

    move-result-object v0

    return-object v0
.end method

.method public final last()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 824065
    iget-object v0, p0, LX/50n;->a:LX/4x9;

    move-object v0, v0

    .line 824066
    invoke-interface {v0}, LX/4x9;->i()LX/4wx;

    move-result-object v0

    invoke-static {v0}, LX/50p;->c(LX/4wx;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;TE;)",
            "Ljava/util/SortedSet",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 824063
    iget-object v0, p0, LX/50n;->a:LX/4x9;

    move-object v0, v0

    .line 824064
    sget-object v1, LX/4xG;->CLOSED:LX/4xG;

    sget-object v2, LX/4xG;->OPEN:LX/4xG;

    invoke-interface {v0, p1, v1, p2, v2}, LX/4x9;->a(Ljava/lang/Object;LX/4xG;Ljava/lang/Object;LX/4xG;)LX/4x9;

    move-result-object v0

    invoke-interface {v0}, LX/4x9;->g()Ljava/util/NavigableSet;

    move-result-object v0

    return-object v0
.end method

.method public final tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)",
            "Ljava/util/SortedSet",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 824061
    iget-object v0, p0, LX/50n;->a:LX/4x9;

    move-object v0, v0

    .line 824062
    sget-object v1, LX/4xG;->CLOSED:LX/4xG;

    invoke-interface {v0, p1, v1}, LX/4x9;->b(Ljava/lang/Object;LX/4xG;)LX/4x9;

    move-result-object v0

    invoke-interface {v0}, LX/4x9;->g()Ljava/util/NavigableSet;

    move-result-object v0

    return-object v0
.end method
