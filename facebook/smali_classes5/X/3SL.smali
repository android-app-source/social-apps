.class public LX/3SL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0ug;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/3SL;


# instance fields
.field private final a:LX/2oU;


# direct methods
.method public constructor <init>(LX/2oU;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 582159
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 582160
    iput-object p1, p0, LX/3SL;->a:LX/2oU;

    .line 582161
    return-void
.end method

.method public static a(LX/0QB;)LX/3SL;
    .locals 4

    .prologue
    .line 582162
    sget-object v0, LX/3SL;->b:LX/3SL;

    if-nez v0, :cond_1

    .line 582163
    const-class v1, LX/3SL;

    monitor-enter v1

    .line 582164
    :try_start_0
    sget-object v0, LX/3SL;->b:LX/3SL;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 582165
    if-eqz v2, :cond_0

    .line 582166
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 582167
    new-instance p0, LX/3SL;

    invoke-static {v0}, LX/2oU;->a(LX/0QB;)LX/2oU;

    move-result-object v3

    check-cast v3, LX/2oU;

    invoke-direct {p0, v3}, LX/3SL;-><init>(LX/2oU;)V

    .line 582168
    move-object v0, p0

    .line 582169
    sput-object v0, LX/3SL;->b:LX/3SL;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 582170
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 582171
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 582172
    :cond_1
    sget-object v0, LX/3SL;->b:LX/3SL;

    return-object v0

    .line 582173
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 582174
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 582175
    const/16 v0, 0x3d

    return v0
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 582176
    iget-object v0, p0, LX/3SL;->a:LX/2oU;

    invoke-virtual {v0}, LX/2oU;->b()V

    .line 582177
    return-void
.end method
