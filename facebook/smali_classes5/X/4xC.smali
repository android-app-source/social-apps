.class public abstract LX/4xC;
.super LX/1M0;
.source ""

# interfaces
.implements LX/4x9;


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "LX/1M0",
        "<TE;>;",
        "LX/4x9",
        "<TE;>;"
    }
.end annotation


# instance fields
.field private transient a:LX/4x9;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4x9",
            "<TE;>;"
        }
    .end annotation
.end field

.field public final comparator:Ljava/util/Comparator;
    .annotation runtime Lcom/google/common/collect/GwtTransient;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<-TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 820853
    sget-object v0, LX/1zb;->a:LX/1zb;

    move-object v0, v0

    .line 820854
    invoke-direct {p0, v0}, LX/4xC;-><init>(Ljava/util/Comparator;)V

    .line 820855
    return-void
.end method

.method public constructor <init>(Ljava/util/Comparator;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Comparator",
            "<-TE;>;)V"
        }
    .end annotation

    .prologue
    .line 820856
    invoke-direct {p0}, LX/1M0;-><init>()V

    .line 820857
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Comparator;

    iput-object v0, p0, LX/4xC;->comparator:Ljava/util/Comparator;

    .line 820858
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;LX/4xG;Ljava/lang/Object;LX/4xG;)LX/4x9;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;",
            "LX/4xG;",
            "TE;",
            "LX/4xG;",
            ")",
            "LX/4x9",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 820859
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 820860
    invoke-static {p4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 820861
    invoke-virtual {p0, p1, p2}, LX/4xC;->b(Ljava/lang/Object;LX/4xG;)LX/4x9;

    move-result-object v0

    invoke-interface {v0, p3, p4}, LX/4x9;->a(Ljava/lang/Object;LX/4xG;)LX/4x9;

    move-result-object v0

    return-object v0
.end method

.method public comparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<-TE;>;"
        }
    .end annotation

    .prologue
    .line 820864
    iget-object v0, p0, LX/4xC;->comparator:Ljava/util/Comparator;

    return-object v0
.end method

.method public final synthetic d()Ljava/util/Set;
    .locals 1

    .prologue
    .line 820862
    invoke-virtual {p0}, LX/4xC;->g()Ljava/util/NavigableSet;

    move-result-object v0

    return-object v0
.end method

.method public final e()Ljava/util/Set;
    .locals 1

    .prologue
    .line 820863
    new-instance v0, LX/50o;

    invoke-direct {v0, p0}, LX/50o;-><init>(LX/4x9;)V

    return-object v0
.end method

.method public g()Ljava/util/NavigableSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/NavigableSet",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 820852
    invoke-super {p0}, LX/1M0;->d()Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableSet;

    return-object v0
.end method

.method public h()LX/4wx;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/Multiset$Entry",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 820832
    invoke-virtual {p0}, LX/1M0;->b()Ljava/util/Iterator;

    move-result-object v0

    .line 820833
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4wx;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i()LX/4wx;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/Multiset$Entry",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 820850
    invoke-virtual {p0}, LX/4xC;->l()Ljava/util/Iterator;

    move-result-object v0

    .line 820851
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4wx;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public j()LX/4wx;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/Multiset$Entry",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 820844
    invoke-virtual {p0}, LX/1M0;->b()Ljava/util/Iterator;

    move-result-object v1

    .line 820845
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 820846
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4wx;

    .line 820847
    invoke-virtual {v0}, LX/4wx;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0}, LX/4wx;->b()I

    move-result v0

    invoke-static {v2, v0}, LX/2Tc;->a(Ljava/lang/Object;I)LX/4wx;

    move-result-object v0

    .line 820848
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 820849
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public k()LX/4wx;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/Multiset$Entry",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 820838
    invoke-virtual {p0}, LX/4xC;->l()Ljava/util/Iterator;

    move-result-object v1

    .line 820839
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 820840
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4wx;

    .line 820841
    invoke-virtual {v0}, LX/4wx;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0}, LX/4wx;->b()I

    move-result v0

    invoke-static {v2, v0}, LX/2Tc;->a(Ljava/lang/Object;I)LX/4wx;

    move-result-object v0

    .line 820842
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 820843
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract l()Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lcom/google/common/collect/Multiset$Entry",
            "<TE;>;>;"
        }
    .end annotation
.end method

.method public n()LX/4x9;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/4x9",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 820834
    iget-object v0, p0, LX/4xC;->a:LX/4x9;

    .line 820835
    if-nez v0, :cond_0

    .line 820836
    new-instance v0, LX/4xB;

    invoke-direct {v0, p0}, LX/4xB;-><init>(LX/4xC;)V

    move-object v0, v0

    .line 820837
    iput-object v0, p0, LX/4xC;->a:LX/4x9;

    :cond_0
    return-object v0
.end method
