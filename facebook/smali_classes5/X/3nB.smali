.class public final enum LX/3nB;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/3nB;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/3nB;

.field public static final enum COMPLETED:LX/3nB;

.field public static final enum FAILED:LX/3nB;

.field public static final enum FALLBACK:LX/3nB;

.field public static final enum STARTED:LX/3nB;

.field public static final enum STARTING:LX/3nB;

.field public static final enum VIEWPING:LX/3nB;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 638043
    new-instance v0, LX/3nB;

    const-string v1, "STARTING"

    invoke-direct {v0, v1, v3}, LX/3nB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3nB;->STARTING:LX/3nB;

    .line 638044
    new-instance v0, LX/3nB;

    const-string v1, "STARTED"

    invoke-direct {v0, v1, v4}, LX/3nB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3nB;->STARTED:LX/3nB;

    .line 638045
    new-instance v0, LX/3nB;

    const-string v1, "FAILED"

    invoke-direct {v0, v1, v5}, LX/3nB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3nB;->FAILED:LX/3nB;

    .line 638046
    new-instance v0, LX/3nB;

    const-string v1, "VIEWPING"

    invoke-direct {v0, v1, v6}, LX/3nB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3nB;->VIEWPING:LX/3nB;

    .line 638047
    new-instance v0, LX/3nB;

    const-string v1, "FALLBACK"

    invoke-direct {v0, v1, v7}, LX/3nB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3nB;->FALLBACK:LX/3nB;

    .line 638048
    new-instance v0, LX/3nB;

    const-string v1, "COMPLETED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/3nB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3nB;->COMPLETED:LX/3nB;

    .line 638049
    const/4 v0, 0x6

    new-array v0, v0, [LX/3nB;

    sget-object v1, LX/3nB;->STARTING:LX/3nB;

    aput-object v1, v0, v3

    sget-object v1, LX/3nB;->STARTED:LX/3nB;

    aput-object v1, v0, v4

    sget-object v1, LX/3nB;->FAILED:LX/3nB;

    aput-object v1, v0, v5

    sget-object v1, LX/3nB;->VIEWPING:LX/3nB;

    aput-object v1, v0, v6

    sget-object v1, LX/3nB;->FALLBACK:LX/3nB;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/3nB;->COMPLETED:LX/3nB;

    aput-object v2, v0, v1

    sput-object v0, LX/3nB;->$VALUES:[LX/3nB;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 638050
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/3nB;
    .locals 1

    .prologue
    .line 638051
    const-class v0, LX/3nB;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/3nB;

    return-object v0
.end method

.method public static values()[LX/3nB;
    .locals 1

    .prologue
    .line 638052
    sget-object v0, LX/3nB;->$VALUES:[LX/3nB;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/3nB;

    return-object v0
.end method
