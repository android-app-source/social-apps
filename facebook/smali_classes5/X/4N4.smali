.class public LX/4N4;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 690554
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 17

    .prologue
    .line 690555
    const/4 v12, 0x0

    .line 690556
    const/4 v11, 0x0

    .line 690557
    const/4 v10, 0x0

    .line 690558
    const/4 v9, 0x0

    .line 690559
    const/4 v8, 0x0

    .line 690560
    const/4 v5, 0x0

    .line 690561
    const-wide/16 v6, 0x0

    .line 690562
    const/4 v4, 0x0

    .line 690563
    const/4 v3, 0x0

    .line 690564
    const/4 v2, 0x0

    .line 690565
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->START_OBJECT:LX/15z;

    if-eq v13, v14, :cond_c

    .line 690566
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 690567
    const/4 v2, 0x0

    .line 690568
    :goto_0
    return v2

    .line 690569
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v14, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v14, :cond_a

    .line 690570
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 690571
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 690572
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v14, v15, :cond_0

    if-eqz v3, :cond_0

    .line 690573
    const-string v14, "dialog_subtitle"

    invoke-virtual {v3, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_1

    .line 690574
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v13, v3

    goto :goto_1

    .line 690575
    :cond_1
    const-string v14, "dialog_title"

    invoke-virtual {v3, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 690576
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v12, v3

    goto :goto_1

    .line 690577
    :cond_2
    const-string v14, "highlighted_charity"

    invoke-virtual {v3, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 690578
    invoke-static/range {p0 .. p1}, LX/4LB;->a(LX/15w;LX/186;)I

    move-result v3

    move v11, v3

    goto :goto_1

    .line 690579
    :cond_3
    const-string v14, "highlighted_charity_label"

    invoke-virtual {v3, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_4

    .line 690580
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v10, v3

    goto :goto_1

    .line 690581
    :cond_4
    const-string v14, "id"

    invoke-virtual {v3, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 690582
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v7, v3

    goto :goto_1

    .line 690583
    :cond_5
    const-string v14, "prefill_description"

    invoke-virtual {v3, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 690584
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v6, v3

    goto/16 :goto_1

    .line 690585
    :cond_6
    const-string v14, "prefill_end_time"

    invoke-virtual {v3, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_7

    .line 690586
    const/4 v2, 0x1

    .line 690587
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    goto/16 :goto_1

    .line 690588
    :cond_7
    const-string v14, "prefill_title"

    invoke-virtual {v3, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_8

    .line 690589
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v9, v3

    goto/16 :goto_1

    .line 690590
    :cond_8
    const-string v14, "url"

    invoke-virtual {v3, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 690591
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v8, v3

    goto/16 :goto_1

    .line 690592
    :cond_9
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 690593
    :cond_a
    const/16 v3, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 690594
    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v13}, LX/186;->b(II)V

    .line 690595
    const/4 v3, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v12}, LX/186;->b(II)V

    .line 690596
    const/4 v3, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, LX/186;->b(II)V

    .line 690597
    const/4 v3, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10}, LX/186;->b(II)V

    .line 690598
    const/4 v3, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 690599
    const/4 v3, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 690600
    if-eqz v2, :cond_b

    .line 690601
    const/4 v3, 0x6

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 690602
    :cond_b
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 690603
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 690604
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_c
    move v13, v12

    move v12, v11

    move v11, v10

    move v10, v9

    move v9, v4

    move/from16 v16, v5

    move-wide v4, v6

    move/from16 v6, v16

    move v7, v8

    move v8, v3

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 690605
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 690606
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 690607
    if-eqz v0, :cond_0

    .line 690608
    const-string v1, "dialog_subtitle"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 690609
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 690610
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 690611
    if-eqz v0, :cond_1

    .line 690612
    const-string v1, "dialog_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 690613
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 690614
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 690615
    if-eqz v0, :cond_2

    .line 690616
    const-string v1, "highlighted_charity"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 690617
    invoke-static {p0, v0, p2, p3}, LX/4LB;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 690618
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 690619
    if-eqz v0, :cond_3

    .line 690620
    const-string v1, "highlighted_charity_label"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 690621
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 690622
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 690623
    if-eqz v0, :cond_4

    .line 690624
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 690625
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 690626
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 690627
    if-eqz v0, :cond_5

    .line 690628
    const-string v1, "prefill_description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 690629
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 690630
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 690631
    cmp-long v2, v0, v2

    if-eqz v2, :cond_6

    .line 690632
    const-string v2, "prefill_end_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 690633
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 690634
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 690635
    if-eqz v0, :cond_7

    .line 690636
    const-string v1, "prefill_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 690637
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 690638
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 690639
    if-eqz v0, :cond_8

    .line 690640
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 690641
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 690642
    :cond_8
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 690643
    return-void
.end method
