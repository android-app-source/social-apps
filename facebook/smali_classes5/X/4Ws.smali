.class public final LX/4Ws;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/model/GraphQLActor;",
        "Lcom/facebook/graphql/model/GraphQLEntity;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 761463
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 761464
    check-cast p1, Lcom/facebook/graphql/model/GraphQLActor;

    .line 761465
    new-instance v0, LX/170;

    invoke-direct {v0}, LX/170;-><init>()V

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v1

    .line 761466
    iput-object v1, v0, LX/170;->o:Ljava/lang/String;

    .line 761467
    move-object v0, v0

    .line 761468
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v1

    .line 761469
    iput-object v1, v0, LX/170;->A:Ljava/lang/String;

    .line 761470
    move-object v0, v0

    .line 761471
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 761472
    iput-object v1, v0, LX/170;->L:Lcom/facebook/graphql/model/GraphQLImage;

    .line 761473
    move-object v0, v0

    .line 761474
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLActor;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    .line 761475
    iput-object v1, v0, LX/170;->ab:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 761476
    move-object v0, v0

    .line 761477
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLActor;->az()Ljava/lang/String;

    move-result-object v1

    .line 761478
    iput-object v1, v0, LX/170;->Y:Ljava/lang/String;

    .line 761479
    move-object v0, v0

    .line 761480
    invoke-virtual {v0}, LX/170;->a()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v0

    return-object v0
.end method
