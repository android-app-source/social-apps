.class public final LX/4qA;
.super LX/32s;
.source ""


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final _backProperty:LX/32s;

.field public final _isContainer:Z

.field public final _managedProperty:LX/32s;

.field public final _referenceName:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/32s;Ljava/lang/String;LX/32s;LX/0lQ;Z)V
    .locals 7

    .prologue
    .line 813125
    iget-object v0, p1, LX/32s;->_propName:Ljava/lang/String;

    move-object v1, v0

    .line 813126
    invoke-virtual {p1}, LX/32s;->a()LX/0lJ;

    move-result-object v2

    .line 813127
    iget-object v0, p1, LX/32s;->_wrapperName:LX/2Vb;

    move-object v3, v0

    .line 813128
    iget-object v0, p1, LX/32s;->_valueTypeDeserializer:LX/4qw;

    move-object v4, v0

    .line 813129
    iget-boolean v0, p1, LX/32s;->_isRequired:Z

    move v6, v0

    .line 813130
    move-object v0, p0

    move-object v5, p4

    invoke-direct/range {v0 .. v6}, LX/32s;-><init>(Ljava/lang/String;LX/0lJ;LX/2Vb;LX/4qw;LX/0lQ;Z)V

    .line 813131
    iput-object p2, p0, LX/4qA;->_referenceName:Ljava/lang/String;

    .line 813132
    iput-object p1, p0, LX/4qA;->_managedProperty:LX/32s;

    .line 813133
    iput-object p3, p0, LX/4qA;->_backProperty:LX/32s;

    .line 813134
    iput-boolean p5, p0, LX/4qA;->_isContainer:Z

    .line 813135
    return-void
.end method

.method private constructor <init>(LX/4qA;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4qA;",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 813119
    invoke-direct {p0, p1, p2}, LX/32s;-><init>(LX/32s;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 813120
    iget-object v0, p1, LX/4qA;->_referenceName:Ljava/lang/String;

    iput-object v0, p0, LX/4qA;->_referenceName:Ljava/lang/String;

    .line 813121
    iget-boolean v0, p1, LX/4qA;->_isContainer:Z

    iput-boolean v0, p0, LX/4qA;->_isContainer:Z

    .line 813122
    iget-object v0, p1, LX/4qA;->_managedProperty:LX/32s;

    iput-object v0, p0, LX/4qA;->_managedProperty:LX/32s;

    .line 813123
    iget-object v0, p1, LX/4qA;->_backProperty:LX/32s;

    iput-object v0, p0, LX/4qA;->_backProperty:LX/32s;

    .line 813124
    return-void
.end method

.method private constructor <init>(LX/4qA;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 813113
    invoke-direct {p0, p1, p2}, LX/32s;-><init>(LX/32s;Ljava/lang/String;)V

    .line 813114
    iget-object v0, p1, LX/4qA;->_referenceName:Ljava/lang/String;

    iput-object v0, p0, LX/4qA;->_referenceName:Ljava/lang/String;

    .line 813115
    iget-boolean v0, p1, LX/4qA;->_isContainer:Z

    iput-boolean v0, p0, LX/4qA;->_isContainer:Z

    .line 813116
    iget-object v0, p1, LX/4qA;->_managedProperty:LX/32s;

    iput-object v0, p0, LX/4qA;->_managedProperty:LX/32s;

    .line 813117
    iget-object v0, p1, LX/4qA;->_backProperty:LX/32s;

    iput-object v0, p0, LX/4qA;->_backProperty:LX/32s;

    .line 813118
    return-void
.end method

.method private a(Lcom/fasterxml/jackson/databind/JsonDeserializer;)LX/4qA;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<*>;)",
            "LX/4qA;"
        }
    .end annotation

    .prologue
    .line 813112
    new-instance v0, LX/4qA;

    invoke-direct {v0, p0, p1}, LX/4qA;-><init>(LX/4qA;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    return-object v0
.end method

.method private c(Ljava/lang/String;)LX/4qA;
    .locals 1

    .prologue
    .line 813111
    new-instance v0, LX/4qA;

    invoke-direct {v0, p0, p1}, LX/4qA;-><init>(LX/4qA;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/String;)LX/32s;
    .locals 1

    .prologue
    .line 813110
    invoke-direct {p0, p1}, LX/4qA;->c(Ljava/lang/String;)LX/4qA;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15w;LX/0n3;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 813084
    iget-object v0, p0, LX/4qA;->_managedProperty:LX/32s;

    invoke-virtual {v0, p1, p2}, LX/32s;->a(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, p3, v0}, LX/32s;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 813085
    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 813108
    invoke-virtual {p0, p1, p2}, LX/32s;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 813109
    return-void
.end method

.method public final b()LX/2An;
    .locals 1

    .prologue
    .line 813107
    iget-object v0, p0, LX/4qA;->_managedProperty:LX/32s;

    invoke-virtual {v0}, LX/32s;->b()LX/2An;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Lcom/fasterxml/jackson/databind/JsonDeserializer;)LX/32s;
    .locals 1

    .prologue
    .line 813106
    invoke-direct {p0, p1}, LX/4qA;->a(Lcom/fasterxml/jackson/databind/JsonDeserializer;)LX/4qA;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15w;LX/0n3;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 813105
    invoke-virtual {p0, p1, p2}, LX/32s;->a(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, p3, v0}, LX/32s;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 813086
    iget-object v0, p0, LX/4qA;->_managedProperty:LX/32s;

    invoke-virtual {v0, p1, p2}, LX/32s;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 813087
    if-eqz p2, :cond_7

    .line 813088
    iget-boolean v0, p0, LX/4qA;->_isContainer:Z

    if-eqz v0, :cond_6

    .line 813089
    instance-of v0, p2, [Ljava/lang/Object;

    if-eqz v0, :cond_1

    .line 813090
    check-cast p2, [Ljava/lang/Object;

    check-cast p2, [Ljava/lang/Object;

    array-length v2, p2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_7

    aget-object v3, p2, v0

    .line 813091
    if-eqz v3, :cond_0

    .line 813092
    iget-object v4, p0, LX/4qA;->_backProperty:LX/32s;

    invoke-virtual {v4, v3, p1}, LX/32s;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 813093
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 813094
    :cond_1
    instance-of v0, p2, Ljava/util/Collection;

    if-eqz v0, :cond_3

    .line 813095
    check-cast p2, Ljava/util/Collection;

    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 813096
    if-eqz v2, :cond_2

    .line 813097
    iget-object v3, p0, LX/4qA;->_backProperty:LX/32s;

    invoke-virtual {v3, v2, p1}, LX/32s;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1

    .line 813098
    :cond_3
    instance-of v0, p2, Ljava/util/Map;

    if-eqz v0, :cond_5

    .line 813099
    check-cast p2, Ljava/util/Map;

    invoke-interface {p2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_4
    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 813100
    if-eqz v2, :cond_4

    .line 813101
    iget-object v3, p0, LX/4qA;->_backProperty:LX/32s;

    invoke-virtual {v3, v2, p1}, LX/32s;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_2

    .line 813102
    :cond_5
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported container type ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") when resolving reference \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/4qA;->_referenceName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 813103
    :cond_6
    iget-object v0, p0, LX/4qA;->_backProperty:LX/32s;

    invoke-virtual {v0, p2, p1}, LX/32s;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 813104
    :cond_7
    return-object v1
.end method
