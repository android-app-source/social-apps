.class public final LX/3i5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic b:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

.field public final synthetic e:LX/1nA;


# direct methods
.method public constructor <init>(LX/1nA;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V
    .locals 0

    .prologue
    .line 628668
    iput-object p1, p0, LX/3i5;->e:LX/1nA;

    iput-object p2, p0, LX/3i5;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object p3, p0, LX/3i5;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object p4, p0, LX/3i5;->c:Ljava/lang/String;

    iput-object p5, p0, LX/3i5;->d:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x4614fb5d

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 628669
    iget-object v0, p0, LX/3i5;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 628670
    if-eqz v0, :cond_3

    invoke-static {v0}, LX/182;->s(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :goto_0
    move v3, v1

    .line 628671
    iget-object v0, p0, LX/3i5;->e:LX/1nA;

    iget-object v1, p0, LX/3i5;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0, v1}, LX/1nA;->c(LX/1nA;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v4

    .line 628672
    iget-object v0, p0, LX/3i5;->c:Ljava/lang/String;

    invoke-static {v0}, LX/17d;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 628673
    invoke-static {v3, v4}, LX/17Q;->c(ZLX/0lF;)Ljava/util/Map;

    move-result-object v0

    .line 628674
    invoke-static {p1}, LX/1vZ;->a(Landroid/view/View;)LX/1vY;

    move-result-object v2

    .line 628675
    if-eqz v2, :cond_0

    .line 628676
    invoke-static {v2}, LX/1vZ;->b(LX/1vY;)LX/162;

    move-result-object v2

    .line 628677
    if-eqz v2, :cond_0

    .line 628678
    const-string v1, "tn"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 628679
    :cond_0
    const v2, 0x7f0d0083

    invoke-virtual {p1, v2}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 628680
    if-eqz v2, :cond_4

    .line 628681
    const-string v1, "1"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 628682
    :goto_1
    iget-object v1, p0, LX/3i5;->e:LX/1nA;

    iget-object v2, p0, LX/3i5;->c:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, LX/1nA;->a(Ljava/lang/String;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/util/Map;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 628683
    :cond_1
    :goto_2
    const v0, 0x4f21e579

    invoke-static {v0, v6}, LX/02F;->a(II)V

    return-void

    .line 628684
    :cond_2
    iget-object v0, p0, LX/3i5;->e:LX/1nA;

    iget-object v2, p0, LX/3i5;->c:Ljava/lang/String;

    iget-object v1, p0, LX/3i5;->d:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v1}, LX/1VO;->z(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v5

    move-object v1, p1

    invoke-static/range {v0 .. v5}, LX/1nA;->a(LX/1nA;Landroid/view/View;Ljava/lang/String;ZLX/162;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 628685
    if-eqz v0, :cond_1

    .line 628686
    iget-object v1, p0, LX/3i5;->e:LX/1nA;

    iget-object v2, p0, LX/3i5;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p0, LX/3i5;->c:Ljava/lang/String;

    invoke-virtual {v1, v2, v0, v3}, LX/1nA;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    goto :goto_2

    :cond_3
    const/4 v1, 0x0

    goto :goto_0

    .line 628687
    :cond_4
    const-string v2, "cta_click"

    const-string v1, "1"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method
