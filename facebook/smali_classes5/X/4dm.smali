.class public LX/4dm;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/1Fi;

.field private final b:LX/1FO;


# direct methods
.method public constructor <init>(LX/1FB;)V
    .locals 2

    .prologue
    .line 796427
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 796428
    invoke-virtual {p1}, LX/1FB;->b()LX/1FO;

    move-result-object v0

    iput-object v0, p0, LX/4dm;->b:LX/1FO;

    .line 796429
    new-instance v0, LX/1Fi;

    invoke-virtual {p1}, LX/1FB;->e()LX/1Fj;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1Fi;-><init>(LX/1Fj;)V

    iput-object v0, p0, LX/4dm;->a:LX/1Fi;

    .line 796430
    return-void
.end method


# virtual methods
.method public final a(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    .locals 9
    .annotation build Landroid/annotation/TargetApi;
        value = 0xc
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 796431
    iget-object v0, p0, LX/4dm;->a:LX/1Fi;

    int-to-short v2, p1

    int-to-short v3, p2

    invoke-virtual {v0, v2, v3}, LX/1Fi;->a(SS)LX/1FJ;

    move-result-object v4

    .line 796432
    :try_start_0
    new-instance v3, LX/1FL;

    invoke-direct {v3, v4}, LX/1FL;-><init>(LX/1FJ;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 796433
    :try_start_1
    sget-object v0, LX/1ld;->a:LX/1lW;

    .line 796434
    iput-object v0, v3, LX/1FL;->c:LX/1lW;

    .line 796435
    iget v0, v3, LX/1FL;->g:I

    move v0, v0

    .line 796436
    const/4 v7, 0x1

    .line 796437
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 796438
    iput-boolean v7, v2, Landroid/graphics/BitmapFactory$Options;->inDither:Z

    .line 796439
    iput-object p3, v2, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 796440
    iput-boolean v7, v2, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    .line 796441
    iput-boolean v7, v2, Landroid/graphics/BitmapFactory$Options;->inInputShareable:Z

    .line 796442
    iput v0, v2, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 796443
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0xb

    if-lt v5, v6, :cond_0

    .line 796444
    iput-boolean v7, v2, Landroid/graphics/BitmapFactory$Options;->inMutable:Z

    .line 796445
    :cond_0
    move-object v5, v2

    .line 796446
    invoke-virtual {v4}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FK;

    invoke-virtual {v0}, LX/1FK;->a()I

    move-result v6

    .line 796447
    invoke-virtual {v4}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FK;

    .line 796448
    iget-object v2, p0, LX/4dm;->b:LX/1FO;

    add-int/lit8 v7, v6, 0x2

    invoke-virtual {v2, v7}, LX/1FO;->a(I)LX/1FJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v2

    .line 796449
    :try_start_2
    invoke-virtual {v2}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [B

    .line 796450
    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v0, v7, v1, v8, v6}, LX/1FK;->a(I[BII)V

    .line 796451
    const/4 v0, 0x0

    invoke-static {v1, v0, v6, v5}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 796452
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Bitmap;->setHasAlpha(Z)V

    .line 796453
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Bitmap;->eraseColor(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 796454
    invoke-static {v2}, LX/1FJ;->c(LX/1FJ;)V

    .line 796455
    invoke-static {v3}, LX/1FL;->d(LX/1FL;)V

    .line 796456
    invoke-static {v4}, LX/1FJ;->c(LX/1FJ;)V

    return-object v0

    .line 796457
    :catchall_0
    move-exception v0

    move-object v2, v1

    :goto_0
    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    .line 796458
    invoke-static {v2}, LX/1FL;->d(LX/1FL;)V

    .line 796459
    invoke-static {v4}, LX/1FJ;->c(LX/1FJ;)V

    throw v0

    .line 796460
    :catchall_1
    move-exception v0

    move-object v2, v3

    goto :goto_0

    :catchall_2
    move-exception v0

    move-object v1, v2

    move-object v2, v3

    goto :goto_0
.end method
