.class public final LX/4XY;
.super LX/0ur;
.source ""


# instance fields
.field public A:Z

.field public B:Z

.field public C:Z

.field public D:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public E:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public F:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public G:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public H:Lcom/facebook/graphql/enums/GraphQLPageCategoryType;

.field public I:Lcom/facebook/graphql/model/GraphQLCelebrityBasicInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public J:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public K:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;",
            ">;"
        }
    .end annotation
.end field

.field public L:Lcom/facebook/graphql/enums/GraphQLCommercePageType;

.field public M:Lcom/facebook/graphql/model/GraphQLContact;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public N:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public O:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public P:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Q:Z

.field public R:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public S:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public T:Z

.field public U:I

.field public V:Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

.field public W:Lcom/facebook/graphql/model/GraphQLEventsOccurringHereConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public X:Z

.field public Y:Lcom/facebook/graphql/model/GraphQLVideo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Z:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aA:Z

.field public aB:Z

.field public aC:Z

.field public aD:Z

.field public aE:Z

.field public aF:Z

.field public aG:Z

.field public aH:Z

.field public aI:Lcom/facebook/graphql/model/GraphQLLikedProfilesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aJ:Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

.field public aK:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aL:Lcom/facebook/graphql/model/GraphQLLocation;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aM:Lcom/facebook/graphql/model/GraphQLGeoRectangle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aN:I

.field public aO:Lcom/facebook/graphql/model/GraphQLPageMenuInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aP:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aQ:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aR:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aS:Lcom/facebook/graphql/model/GraphQLOpenGraphObject;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aT:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aU:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public aV:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aW:I

.field public aX:Z

.field public aY:Lcom/facebook/graphql/model/GraphQLStoryAttachment;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aZ:D

.field public aa:Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ab:Lcom/facebook/graphql/model/GraphQLFollowUpFeedUnitsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ac:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ad:Z

.field public ae:Z

.field public af:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTimeRange;",
            ">;"
        }
    .end annotation
.end field

.field public ag:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ah:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ai:I

.field public aj:Z

.field public ak:Z

.field public al:Z

.field public am:Z

.field public an:Z

.field public ao:Z

.field public ap:Z

.field public aq:Z

.field public ar:Z

.field public as:Z

.field public at:Z

.field public au:Z

.field public av:Z

.field public aw:Z

.field public ax:Z

.field public ay:Z

.field public az:Z

.field public b:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bA:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bB:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bC:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bD:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bE:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bF:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bG:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bH:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bI:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bJ:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bK:Lcom/facebook/graphql/model/GraphQLPageActionChannel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bL:Lcom/facebook/graphql/model/GraphQLPageActionChannel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bM:Lcom/facebook/graphql/model/GraphQLPhoto;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bN:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bO:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bP:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bQ:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bR:Z

.field public bS:Lcom/facebook/graphql/model/GraphQLProfileVideo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bT:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bU:Lcom/facebook/graphql/model/GraphQLPageStarRatersConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bV:I

.field public bW:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bX:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLRedirectionInfo;",
            ">;"
        }
    .end annotation
.end field

.field public bY:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bZ:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPhoto;",
            ">;"
        }
    .end annotation
.end field

.field public ba:Lcom/facebook/graphql/model/GraphQLRating;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bb:Lcom/facebook/graphql/model/GraphQLOwnedEventsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bc:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bd:Lcom/facebook/graphql/model/GraphQLPageCallToAction;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public be:Lcom/facebook/graphql/model/GraphQLPageLikersConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bf:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bg:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

.field public bh:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;",
            ">;"
        }
    .end annotation
.end field

.field public bi:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPage;",
            ">;"
        }
    .end annotation
.end field

.field public bj:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bk:Lcom/facebook/graphql/model/GraphQLPageVisitsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bl:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLPaymentModulesClient;",
            ">;"
        }
    .end annotation
.end field

.field public bm:I

.field public bn:Lcom/facebook/graphql/enums/GraphQLPermanentlyClosedStatus;

.field public bo:Lcom/facebook/graphql/model/GraphQLPhoneNumber;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bp:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bq:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public br:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bs:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bt:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

.field public bu:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bv:Lcom/facebook/graphql/enums/GraphQLPlaceType;

.field public bw:Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bx:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public by:Lcom/facebook/graphql/model/GraphQLPageActionChannel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bz:Lcom/facebook/graphql/model/GraphQLPrivacyOption;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Z

.field public cA:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public cB:Lcom/facebook/graphql/model/GraphQLPageActionChannel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cC:Lcom/facebook/graphql/model/GraphQLPageActionChannel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cD:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cE:Lcom/facebook/graphql/model/GraphQLTarotPublisherInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cF:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cG:Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cH:Lcom/facebook/graphql/model/GraphQLTimelineStoriesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cI:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cJ:Lcom/facebook/graphql/model/GraphQLNode;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cK:Z

.field public cL:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cM:Lcom/facebook/graphql/model/GraphQLTrendingTopicData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cN:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cO:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cP:I

.field public cQ:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cR:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cS:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cT:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

.field public cU:Z

.field public cV:Z

.field public cW:Lcom/facebook/graphql/model/GraphQLNode;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cX:Lcom/facebook/graphql/model/GraphQLProfile;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cY:Z

.field public cZ:Z

.field public ca:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cb:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cc:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cd:Lcom/facebook/graphql/model/GraphQLContactRecommendationField;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ce:Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cf:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

.field public cg:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public ch:Z

.field public ci:Z

.field public cj:Z

.field public ck:Z

.field public cl:Z

.field public cm:Z

.field public cn:Z

.field public co:Z

.field public cp:Z

.field public cq:Z

.field public cr:Lcom/facebook/graphql/model/GraphQLSinglePublisherVideoChannelsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cs:Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ct:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public cu:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cv:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cw:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cx:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cy:Lcom/facebook/graphql/model/GraphQLStreamingImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cz:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

.field public d:Lcom/facebook/graphql/model/GraphQLPageActionChannel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public da:Z

.field public db:Z

.field public dc:I

.field public dd:I

.field public de:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public df:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public dg:Z

.field public dh:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public di:Lcom/facebook/graphql/model/GraphQLContactRecommendationField;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public dj:Lcom/facebook/graphql/enums/GraphQLSavedState;

.field public dk:Lcom/facebook/graphql/model/GraphQLRating;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public dl:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;",
            ">;"
        }
    .end annotation
.end field

.field public dm:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;",
            ">;"
        }
    .end annotation
.end field

.field public dn:Lcom/facebook/graphql/model/GraphQLViewerVisitsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public do:Lcom/facebook/graphql/model/GraphQLWeatherCondition;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public dp:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLWeatherHourlyForecast;",
            ">;"
        }
    .end annotation
.end field

.field public dq:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public dr:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lcom/facebook/graphql/model/GraphQLPageActionChannel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLPageAdminInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLPageActionChannel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLPageActionChannel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/model/GraphQLPageActionChannel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/model/GraphQLStreetAddress;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/model/GraphQLPageAdminInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/graphql/model/GraphQLAlbumsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public o:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLAttributionEntry;",
            ">;"
        }
    .end annotation
.end field

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:I

.field public r:Lcom/facebook/graphql/model/GraphQLAYMTChannel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Z

.field public v:Z

.field public w:Z

.field public x:Z

.field public y:Z

.field public z:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 770557
    invoke-direct {p0}, LX/0ur;-><init>()V

    .line 770558
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageCategoryType;

    iput-object v0, p0, LX/4XY;->H:Lcom/facebook/graphql/enums/GraphQLPageCategoryType;

    .line 770559
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCommercePageType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    iput-object v0, p0, LX/4XY;->L:Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    .line 770560
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    iput-object v0, p0, LX/4XY;->V:Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    .line 770561
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    iput-object v0, p0, LX/4XY;->aJ:Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    .line 770562
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    iput-object v0, p0, LX/4XY;->bg:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 770563
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPermanentlyClosedStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPermanentlyClosedStatus;

    iput-object v0, p0, LX/4XY;->bn:Lcom/facebook/graphql/enums/GraphQLPermanentlyClosedStatus;

    .line 770564
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    iput-object v0, p0, LX/4XY;->bt:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    .line 770565
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    iput-object v0, p0, LX/4XY;->bv:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    .line 770566
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    iput-object v0, p0, LX/4XY;->cf:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    .line 770567
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    iput-object v0, p0, LX/4XY;->cz:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 770568
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    iput-object v0, p0, LX/4XY;->cA:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 770569
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    iput-object v0, p0, LX/4XY;->cT:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    .line 770570
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSavedState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    iput-object v0, p0, LX/4XY;->dj:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 770571
    instance-of v0, p0, LX/4XY;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 770572
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/graphql/model/GraphQLPage;
    .locals 2

    .prologue
    .line 770573
    new-instance v0, Lcom/facebook/graphql/model/GraphQLPage;

    invoke-direct {v0, p0}, Lcom/facebook/graphql/model/GraphQLPage;-><init>(LX/4XY;)V

    .line 770574
    return-object v0
.end method

.method public final c(Lcom/facebook/graphql/model/GraphQLImage;)LX/4XY;
    .locals 0
    .param p1    # Lcom/facebook/graphql/model/GraphQLImage;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 770575
    iput-object p1, p0, LX/4XY;->bQ:Lcom/facebook/graphql/model/GraphQLImage;

    .line 770576
    return-object p0
.end method

.method public final d(Ljava/lang/String;)LX/4XY;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 770577
    iput-object p1, p0, LX/4XY;->ag:Ljava/lang/String;

    .line 770578
    return-object p0
.end method

.method public final f(Ljava/lang/String;)LX/4XY;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 770579
    iput-object p1, p0, LX/4XY;->aT:Ljava/lang/String;

    .line 770580
    return-object p0
.end method
