.class public LX/4Ty;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 720129
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 36

    .prologue
    .line 720130
    const/16 v32, 0x0

    .line 720131
    const/16 v31, 0x0

    .line 720132
    const/16 v30, 0x0

    .line 720133
    const/16 v29, 0x0

    .line 720134
    const/16 v28, 0x0

    .line 720135
    const-wide/16 v26, 0x0

    .line 720136
    const/16 v25, 0x0

    .line 720137
    const/16 v24, 0x0

    .line 720138
    const/16 v21, 0x0

    .line 720139
    const-wide/16 v22, 0x0

    .line 720140
    const/16 v20, 0x0

    .line 720141
    const/16 v19, 0x0

    .line 720142
    const/16 v18, 0x0

    .line 720143
    const/16 v17, 0x0

    .line 720144
    const/16 v16, 0x0

    .line 720145
    const/4 v15, 0x0

    .line 720146
    const/4 v14, 0x0

    .line 720147
    const/4 v13, 0x0

    .line 720148
    const/4 v12, 0x0

    .line 720149
    const/4 v11, 0x0

    .line 720150
    const/4 v10, 0x0

    .line 720151
    const/4 v9, 0x0

    .line 720152
    const/4 v8, 0x0

    .line 720153
    const/4 v7, 0x0

    .line 720154
    const/4 v6, 0x0

    .line 720155
    const/4 v5, 0x0

    .line 720156
    const/4 v4, 0x0

    .line 720157
    const/4 v3, 0x0

    .line 720158
    const/4 v2, 0x0

    .line 720159
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v33

    sget-object v34, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v33

    move-object/from16 v1, v34

    if-eq v0, v1, :cond_1f

    .line 720160
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 720161
    const/4 v2, 0x0

    .line 720162
    :goto_0
    return v2

    .line 720163
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_1b

    .line 720164
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 720165
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 720166
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_0

    if-eqz v2, :cond_0

    .line 720167
    const-string v6, "action_links"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 720168
    invoke-static/range {p0 .. p1}, LX/2bb;->b(LX/15w;LX/186;)I

    move-result v2

    move/from16 v35, v2

    goto :goto_1

    .line 720169
    :cond_1
    const-string v6, "actors"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 720170
    invoke-static/range {p0 .. p1}, LX/2bM;->b(LX/15w;LX/186;)I

    move-result v2

    move/from16 v34, v2

    goto :goto_1

    .line 720171
    :cond_2
    const-string v6, "app_icon"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 720172
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v33, v2

    goto :goto_1

    .line 720173
    :cond_3
    const-string v6, "attachments"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 720174
    invoke-static/range {p0 .. p1}, LX/2as;->b(LX/15w;LX/186;)I

    move-result v2

    move/from16 v32, v2

    goto :goto_1

    .line 720175
    :cond_4
    const-string v6, "cache_id"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 720176
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v31, v2

    goto :goto_1

    .line 720177
    :cond_5
    const-string v6, "creation_time"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 720178
    const/4 v2, 0x1

    .line 720179
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 720180
    :cond_6
    const-string v6, "debug_info"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 720181
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v30, v2

    goto/16 :goto_1

    .line 720182
    :cond_7
    const-string v6, "feedback"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 720183
    invoke-static/range {p0 .. p1}, LX/2bG;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v29, v2

    goto/16 :goto_1

    .line 720184
    :cond_8
    const-string v6, "feedback_context"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 720185
    invoke-static/range {p0 .. p1}, LX/2aq;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v28, v2

    goto/16 :goto_1

    .line 720186
    :cond_9
    const-string v6, "fetchTimeMs"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 720187
    const/4 v2, 0x1

    .line 720188
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v6

    move v9, v2

    move-wide/from16 v26, v6

    goto/16 :goto_1

    .line 720189
    :cond_a
    const-string v6, "hideable_token"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 720190
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v25, v2

    goto/16 :goto_1

    .line 720191
    :cond_b
    const-string v6, "id"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 720192
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v24, v2

    goto/16 :goto_1

    .line 720193
    :cond_c
    const-string v6, "message"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_d

    .line 720194
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v23, v2

    goto/16 :goto_1

    .line 720195
    :cond_d
    const-string v6, "negative_feedback_actions"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_e

    .line 720196
    invoke-static/range {p0 .. p1}, LX/2bY;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v22, v2

    goto/16 :goto_1

    .line 720197
    :cond_e
    const-string v6, "privacy_scope"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_f

    .line 720198
    invoke-static/range {p0 .. p1}, LX/2bo;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v21, v2

    goto/16 :goto_1

    .line 720199
    :cond_f
    const-string v6, "seen_state"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_10

    .line 720200
    const/4 v2, 0x1

    .line 720201
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v6

    move v8, v2

    move-object/from16 v20, v6

    goto/16 :goto_1

    .line 720202
    :cond_10
    const-string v6, "shareable"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_11

    .line 720203
    invoke-static/range {p0 .. p1}, LX/2ap;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v19, v2

    goto/16 :goto_1

    .line 720204
    :cond_11
    const-string v6, "short_term_cache_key"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_12

    .line 720205
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v18, v2

    goto/16 :goto_1

    .line 720206
    :cond_12
    const-string v6, "story_header"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_13

    .line 720207
    invoke-static/range {p0 .. p1}, LX/32O;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v17, v2

    goto/16 :goto_1

    .line 720208
    :cond_13
    const-string v6, "substories_grouping_reasons"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_14

    .line 720209
    const-class v2, Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v2}, LX/2gu;->a(LX/15w;LX/186;Ljava/lang/Class;)I

    move-result v2

    move/from16 v16, v2

    goto/16 :goto_1

    .line 720210
    :cond_14
    const-string v6, "subtopic_options"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_15

    .line 720211
    invoke-static/range {p0 .. p1}, LX/4U6;->a(LX/15w;LX/186;)I

    move-result v2

    move v15, v2

    goto/16 :goto_1

    .line 720212
    :cond_15
    const-string v6, "title"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_16

    .line 720213
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v2

    move v14, v2

    goto/16 :goto_1

    .line 720214
    :cond_16
    const-string v6, "titleForSummary"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_17

    .line 720215
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v2

    move v13, v2

    goto/16 :goto_1

    .line 720216
    :cond_17
    const-string v6, "topic_section"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_18

    .line 720217
    invoke-static/range {p0 .. p1}, LX/4MT;->a(LX/15w;LX/186;)I

    move-result v2

    move v12, v2

    goto/16 :goto_1

    .line 720218
    :cond_18
    const-string v6, "tracking"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_19

    .line 720219
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v11, v2

    goto/16 :goto_1

    .line 720220
    :cond_19
    const-string v6, "url"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1a

    .line 720221
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v10, v2

    goto/16 :goto_1

    .line 720222
    :cond_1a
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 720223
    :cond_1b
    const/16 v2, 0x1b

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 720224
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 720225
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 720226
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 720227
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 720228
    const/4 v2, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 720229
    if-eqz v3, :cond_1c

    .line 720230
    const/4 v3, 0x6

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 720231
    :cond_1c
    const/4 v2, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 720232
    const/16 v2, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 720233
    const/16 v2, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 720234
    if-eqz v9, :cond_1d

    .line 720235
    const/16 v3, 0xa

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v26

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 720236
    :cond_1d
    const/16 v2, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 720237
    const/16 v2, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 720238
    const/16 v2, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 720239
    const/16 v2, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 720240
    const/16 v2, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 720241
    if-eqz v8, :cond_1e

    .line 720242
    const/16 v2, 0x10

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 720243
    :cond_1e
    const/16 v2, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 720244
    const/16 v2, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 720245
    const/16 v2, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 720246
    const/16 v2, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 720247
    const/16 v2, 0x15

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 720248
    const/16 v2, 0x16

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 720249
    const/16 v2, 0x17

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 720250
    const/16 v2, 0x18

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 720251
    const/16 v2, 0x19

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 720252
    const/16 v2, 0x1a

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 720253
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_1f
    move/from16 v33, v30

    move/from16 v34, v31

    move/from16 v35, v32

    move/from16 v30, v25

    move/from16 v31, v28

    move/from16 v32, v29

    move/from16 v25, v20

    move/from16 v28, v21

    move/from16 v29, v24

    move-object/from16 v20, v15

    move/from16 v21, v16

    move/from16 v24, v19

    move/from16 v19, v14

    move/from16 v16, v11

    move v15, v10

    move v14, v9

    move v10, v5

    move v11, v6

    move v9, v3

    move v3, v4

    move-wide/from16 v4, v26

    move-wide/from16 v26, v22

    move/from16 v23, v18

    move/from16 v22, v17

    move/from16 v18, v13

    move/from16 v17, v12

    move v13, v8

    move v12, v7

    move v8, v2

    goto/16 :goto_1
.end method

.method public static a(LX/15w;S)LX/15i;
    .locals 5

    .prologue
    .line 720254
    const/4 v0, 0x1

    const/4 v4, 0x0

    .line 720255
    new-instance v2, LX/186;

    const/16 v1, 0x80

    invoke-direct {v2, v1}, LX/186;-><init>(I)V

    .line 720256
    invoke-static {p0, v2}, LX/4Ty;->a(LX/15w;LX/186;)I

    move-result v1

    .line 720257
    if-eqz v0, :cond_0

    .line 720258
    const/4 v3, 0x2

    invoke-virtual {v2, v3}, LX/186;->c(I)V

    .line 720259
    invoke-virtual {v2, v4, p1, v4}, LX/186;->a(ISI)V

    .line 720260
    const/4 v3, 0x1

    invoke-virtual {v2, v3, v1}, LX/186;->b(II)V

    .line 720261
    invoke-virtual {v2}, LX/186;->d()I

    move-result v1

    .line 720262
    :cond_0
    invoke-virtual {v2, v1}, LX/186;->d(I)V

    .line 720263
    invoke-static {v2}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v1

    move-object v0, v1

    .line 720264
    return-object v0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 7

    .prologue
    const/16 v6, 0x14

    const/16 v3, 0x10

    const-wide/16 v4, 0x0

    .line 720265
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 720266
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 720267
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 720268
    const-string v0, "name"

    const-string v1, "TopicCustomizationStory"

    invoke-virtual {p2, v0, v1}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 720269
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 720270
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 720271
    if-eqz v0, :cond_0

    .line 720272
    const-string v1, "action_links"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 720273
    invoke-static {p0, v0, p2, p3}, LX/2bb;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 720274
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 720275
    if-eqz v0, :cond_1

    .line 720276
    const-string v1, "actors"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 720277
    invoke-static {p0, v0, p2, p3}, LX/2bM;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 720278
    :cond_1
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 720279
    if-eqz v0, :cond_2

    .line 720280
    const-string v1, "app_icon"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 720281
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 720282
    :cond_2
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 720283
    if-eqz v0, :cond_3

    .line 720284
    const-string v1, "attachments"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 720285
    invoke-static {p0, v0, p2, p3}, LX/2as;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 720286
    :cond_3
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 720287
    if-eqz v0, :cond_4

    .line 720288
    const-string v1, "cache_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 720289
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 720290
    :cond_4
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 720291
    cmp-long v2, v0, v4

    if-eqz v2, :cond_5

    .line 720292
    const-string v2, "creation_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 720293
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 720294
    :cond_5
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 720295
    if-eqz v0, :cond_6

    .line 720296
    const-string v1, "debug_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 720297
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 720298
    :cond_6
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 720299
    if-eqz v0, :cond_7

    .line 720300
    const-string v1, "feedback"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 720301
    invoke-static {p0, v0, p2, p3}, LX/2bG;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 720302
    :cond_7
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 720303
    if-eqz v0, :cond_8

    .line 720304
    const-string v1, "feedback_context"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 720305
    invoke-static {p0, v0, p2, p3}, LX/2aq;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 720306
    :cond_8
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 720307
    cmp-long v2, v0, v4

    if-eqz v2, :cond_9

    .line 720308
    const-string v2, "fetchTimeMs"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 720309
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 720310
    :cond_9
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 720311
    if-eqz v0, :cond_a

    .line 720312
    const-string v1, "hideable_token"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 720313
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 720314
    :cond_a
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 720315
    if-eqz v0, :cond_b

    .line 720316
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 720317
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 720318
    :cond_b
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 720319
    if-eqz v0, :cond_c

    .line 720320
    const-string v1, "message"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 720321
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 720322
    :cond_c
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 720323
    if-eqz v0, :cond_d

    .line 720324
    const-string v1, "negative_feedback_actions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 720325
    invoke-static {p0, v0, p2, p3}, LX/2bY;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 720326
    :cond_d
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 720327
    if-eqz v0, :cond_e

    .line 720328
    const-string v1, "privacy_scope"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 720329
    invoke-static {p0, v0, p2, p3}, LX/2bo;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 720330
    :cond_e
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v3, v0}, LX/15i;->a(IIS)S

    move-result v0

    .line 720331
    if-eqz v0, :cond_f

    .line 720332
    const-string v0, "seen_state"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 720333
    const-class v0, Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-virtual {p0, p1, v3, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 720334
    :cond_f
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 720335
    if-eqz v0, :cond_10

    .line 720336
    const-string v1, "shareable"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 720337
    invoke-static {p0, v0, p2, p3}, LX/2ap;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 720338
    :cond_10
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 720339
    if-eqz v0, :cond_11

    .line 720340
    const-string v1, "short_term_cache_key"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 720341
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 720342
    :cond_11
    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 720343
    if-eqz v0, :cond_12

    .line 720344
    const-string v1, "story_header"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 720345
    invoke-static {p0, v0, p2, p3}, LX/32O;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 720346
    :cond_12
    invoke-virtual {p0, p1, v6}, LX/15i;->g(II)I

    move-result v0

    .line 720347
    if-eqz v0, :cond_13

    .line 720348
    const-string v0, "substories_grouping_reasons"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 720349
    const-class v0, Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;

    invoke-virtual {p0, p1, v6, v0}, LX/15i;->b(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->b(Ljava/util/Iterator;LX/0nX;)V

    .line 720350
    :cond_13
    const/16 v0, 0x15

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 720351
    if-eqz v0, :cond_14

    .line 720352
    const-string v1, "subtopic_options"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 720353
    invoke-static {p0, v0, p2, p3}, LX/4U6;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 720354
    :cond_14
    const/16 v0, 0x16

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 720355
    if-eqz v0, :cond_15

    .line 720356
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 720357
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 720358
    :cond_15
    const/16 v0, 0x17

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 720359
    if-eqz v0, :cond_16

    .line 720360
    const-string v1, "titleForSummary"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 720361
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 720362
    :cond_16
    const/16 v0, 0x18

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 720363
    if-eqz v0, :cond_17

    .line 720364
    const-string v1, "topic_section"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 720365
    invoke-static {p0, v0, p2, p3}, LX/4MT;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 720366
    :cond_17
    const/16 v0, 0x19

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 720367
    if-eqz v0, :cond_18

    .line 720368
    const-string v1, "tracking"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 720369
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 720370
    :cond_18
    const/16 v0, 0x1a

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 720371
    if-eqz v0, :cond_19

    .line 720372
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 720373
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 720374
    :cond_19
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 720375
    return-void
.end method
