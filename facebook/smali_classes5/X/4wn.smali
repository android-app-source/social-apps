.class public final LX/4wn;
.super LX/305;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/305",
        "<",
        "Ljava/util/Map$Entry",
        "<TK;TV;>;>;"
    }
.end annotation


# instance fields
.field public final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/4wp;


# direct methods
.method public constructor <init>(LX/4wp;)V
    .locals 1

    .prologue
    .line 820447
    iput-object p1, p0, LX/4wn;->b:LX/4wp;

    invoke-direct {p0}, LX/305;-><init>()V

    .line 820448
    iget-object v0, p0, LX/4wn;->b:LX/4wp;

    iget-object v0, v0, LX/4wp;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, LX/4wn;->a:Ljava/util/Set;

    return-void
.end method


# virtual methods
.method public final a()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 820426
    iget-object v0, p0, LX/4wn;->a:Ljava/util/Set;

    return-object v0
.end method

.method public final synthetic b()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 820446
    invoke-virtual {p0}, LX/4wn;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final clear()V
    .locals 1

    .prologue
    .line 820444
    iget-object v0, p0, LX/4wn;->b:LX/4wp;

    invoke-virtual {v0}, LX/4wp;->clear()V

    .line 820445
    return-void
.end method

.method public final contains(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 820443
    invoke-virtual {p0}, LX/4wn;->a()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0, p1}, LX/0PM;->a(Ljava/util/Collection;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final containsAll(Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 820441
    invoke-static {p0, p1}, LX/0PN;->a(Ljava/util/Collection;Ljava/util/Collection;)Z

    move-result v0

    move v0, v0

    .line 820442
    return v0
.end method

.method public final synthetic e()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 820440
    invoke-virtual {p0}, LX/4wn;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 820438
    iget-object v0, p0, LX/4wn;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 820439
    new-instance v1, LX/4wm;

    invoke-direct {v1, p0, v0}, LX/4wm;-><init>(LX/4wn;Ljava/util/Iterator;)V

    return-object v1
.end method

.method public final remove(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 820431
    iget-object v0, p0, LX/4wn;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 820432
    const/4 v0, 0x0

    .line 820433
    :goto_0
    return v0

    .line 820434
    :cond_0
    check-cast p1, Ljava/util/Map$Entry;

    .line 820435
    iget-object v0, p0, LX/4wn;->b:LX/4wp;

    iget-object v0, v0, LX/4wp;->a:LX/4wp;

    iget-object v0, v0, LX/4wp;->b:Ljava/util/Map;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 820436
    iget-object v0, p0, LX/4wn;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 820437
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final removeAll(Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 820430
    invoke-virtual {p0, p1}, LX/305;->c(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public final retainAll(Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 820429
    invoke-virtual {p0, p1}, LX/306;->d(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public final toArray()[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 820428
    invoke-virtual {p0}, LX/306;->o()[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;)[TT;"
        }
    .end annotation

    .prologue
    .line 820427
    invoke-virtual {p0, p1}, LX/306;->a([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
