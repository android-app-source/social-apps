.class public final LX/4ZQ;
.super LX/0ur;
.source ""


# instance fields
.field public b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:I

.field public d:Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

.field public e:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

.field public g:Z

.field public h:Z

.field public i:Z

.field public j:Lcom/facebook/graphql/model/GraphQLNode;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/model/GraphQLProfile;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Z

.field public m:Z

.field public n:Z

.field public o:Z

.field public p:I

.field public q:I

.field public r:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 789261
    invoke-direct {p0}, LX/0ur;-><init>()V

    .line 789262
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    iput-object v0, p0, LX/4ZQ;->d:Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    .line 789263
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    iput-object v0, p0, LX/4ZQ;->f:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 789264
    const/4 v0, 0x0

    iput-object v0, p0, LX/4ZQ;->t:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 789265
    instance-of v0, p0, LX/4ZQ;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 789266
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/graphql/model/GraphQLVideoChannel;
    .locals 2

    .prologue
    .line 789267
    new-instance v0, Lcom/facebook/graphql/model/GraphQLVideoChannel;

    invoke-direct {v0, p0}, Lcom/facebook/graphql/model/GraphQLVideoChannel;-><init>(LX/4ZQ;)V

    .line 789268
    return-object v0
.end method
