.class public final LX/3ci;
.super Ljava/util/concurrent/FutureTask;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/concurrent/FutureTask",
        "<TResult;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/3cc;


# direct methods
.method public constructor <init>(LX/3cc;Ljava/util/concurrent/Callable;)V
    .locals 0

    .prologue
    .line 614801
    iput-object p1, p0, LX/3ci;->a:LX/3cc;

    invoke-direct {p0, p2}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    return-void
.end method


# virtual methods
.method public final done()V
    .locals 3

    .prologue
    .line 614802
    const v0, 0x4c832f08    # 6.8778048E7f

    :try_start_0
    invoke-static {p0, v0}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v0

    .line 614803
    iget-object v1, p0, LX/3ci;->a:LX/3cc;

    invoke-static {v1, v0}, LX/3cc;->b$redex0(LX/3cc;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/util/concurrent/CancellationException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_3

    .line 614804
    :goto_0
    return-void

    .line 614805
    :catch_0
    move-exception v0

    .line 614806
    const-string v1, "AsyncTask"

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 614807
    :catch_1
    move-exception v0

    .line 614808
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "An error occured while executing doInBackground()"

    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 614809
    :catch_2
    iget-object v0, p0, LX/3ci;->a:LX/3cc;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/3cc;->b$redex0(LX/3cc;Ljava/lang/Object;)V

    goto :goto_0

    .line 614810
    :catch_3
    move-exception v0

    .line 614811
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "An error occured while executing doInBackground()"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method
