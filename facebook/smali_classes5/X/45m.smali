.class public abstract LX/45m;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static a:LX/45m;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "JobScheduler.class"
    .end annotation
.end field


# instance fields
.field private final b:LX/460;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 670540
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 670541
    new-instance v0, LX/460;

    invoke-direct {v0, p1, p2}, LX/460;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, LX/45m;->b:LX/460;

    .line 670542
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)LX/45m;
    .locals 3
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 670543
    const-class v1, LX/45m;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/45m;->a:LX/45m;

    if-nez v0, :cond_0

    .line 670544
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v0, v2, :cond_1

    .line 670545
    new-instance v0, LX/45x;

    invoke-direct {v0, p0}, LX/45x;-><init>(Landroid/content/Context;)V

    sput-object v0, LX/45m;->a:LX/45m;

    .line 670546
    :cond_0
    :goto_0
    sget-object v0, LX/45m;->a:LX/45m;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 670547
    :cond_1
    :try_start_1
    invoke-static {p0}, LX/45r;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 670548
    sget-object v0, LX/1vX;->c:LX/1vX;

    move-object v0, v0

    .line 670549
    invoke-virtual {v0, p0}, LX/1od;->a(Landroid/content/Context;)I

    move-result v2

    .line 670550
    packed-switch v2, :pswitch_data_0

    .line 670551
    invoke-virtual {v0, v2}, LX/1od;->c(I)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 670552
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 670553
    :pswitch_0
    :try_start_2
    new-instance v0, LX/45n;

    invoke-direct {v0, p0}, LX/45n;-><init>(Landroid/content/Context;)V

    sput-object v0, LX/45m;->a:LX/45m;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(I)V
    .locals 3

    .prologue
    .line 670530
    iget-object v0, p0, LX/45m;->b:LX/460;

    invoke-virtual {v0, p1}, LX/460;->a(I)Ljava/lang/Class;

    move-result-object v0

    .line 670531
    if-nez v0, :cond_0

    .line 670532
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "jobId: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " was not found."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 670533
    :cond_0
    invoke-virtual {p0, p1, v0}, LX/45m;->a(ILjava/lang/Class;)V

    .line 670534
    return-void
.end method

.method public abstract a(ILjava/lang/Class;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/Class",
            "<+TT;>;)V"
        }
    .end annotation
.end method

.method public final a(LX/45u;)V
    .locals 3

    .prologue
    .line 670535
    iget-object v0, p0, LX/45m;->b:LX/460;

    iget v1, p1, LX/45u;->a:I

    invoke-virtual {v0, v1}, LX/460;->a(I)Ljava/lang/Class;

    move-result-object v0

    .line 670536
    if-nez v0, :cond_0

    .line 670537
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "jobId: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, LX/45u;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " was not found."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 670538
    :cond_0
    invoke-virtual {p0, p1, v0}, LX/45m;->a(LX/45u;Ljava/lang/Class;)V

    .line 670539
    return-void
.end method

.method public abstract a(LX/45u;Ljava/lang/Class;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/45u;",
            "Ljava/lang/Class",
            "<+TT;>;)V"
        }
    .end annotation
.end method
