.class public final LX/4ls;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/4lm;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/4lm;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 804879
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 804880
    iput-object p1, p0, LX/4ls;->a:LX/0QB;

    .line 804881
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 804882
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/4ls;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 804883
    packed-switch p2, :pswitch_data_0

    .line 804884
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 804885
    :pswitch_0
    invoke-static {p1}, LX/4ln;->a(LX/0QB;)LX/4ln;

    move-result-object v0

    .line 804886
    :goto_0
    return-object v0

    .line 804887
    :pswitch_1
    invoke-static {p1}, LX/4lo;->a(LX/0QB;)LX/4lo;

    move-result-object v0

    goto :goto_0

    .line 804888
    :pswitch_2
    invoke-static {p1}, LX/4lp;->a(LX/0QB;)LX/4lp;

    move-result-object v0

    goto :goto_0

    .line 804889
    :pswitch_3
    invoke-static {p1}, LX/4lq;->a(LX/0QB;)LX/4lq;

    move-result-object v0

    goto :goto_0

    .line 804890
    :pswitch_4
    invoke-static {p1}, LX/4lr;->a(LX/0QB;)LX/4lr;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 804891
    const/4 v0, 0x5

    return v0
.end method
