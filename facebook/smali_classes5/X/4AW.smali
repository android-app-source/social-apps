.class public LX/4AW;
.super Landroid/graphics/drawable/Drawable;
.source ""

# interfaces
.implements LX/1oM;


# instance fields
.field public final a:[F
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field

.field public final b:Landroid/graphics/Paint;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field

.field public final c:Landroid/graphics/Path;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field

.field public final d:Landroid/graphics/Path;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field

.field private final e:[F

.field private f:Z

.field private g:F

.field private h:F

.field private i:I

.field public j:I

.field private final k:Landroid/graphics/RectF;

.field private l:I


# direct methods
.method public constructor <init>(FI)V
    .locals 0

    .prologue
    .line 676494
    invoke-direct {p0, p2}, LX/4AW;-><init>(I)V

    .line 676495
    invoke-virtual {p0, p1}, LX/4AW;->a(F)V

    .line 676496
    return-void
.end method

.method public constructor <init>(I)V
    .locals 4

    .prologue
    const/16 v1, 0x8

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 676530
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 676531
    new-array v0, v1, [F

    iput-object v0, p0, LX/4AW;->e:[F

    .line 676532
    new-array v0, v1, [F

    iput-object v0, p0, LX/4AW;->a:[F

    .line 676533
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, LX/4AW;->b:Landroid/graphics/Paint;

    .line 676534
    iput-boolean v2, p0, LX/4AW;->f:Z

    .line 676535
    iput v3, p0, LX/4AW;->g:F

    .line 676536
    iput v3, p0, LX/4AW;->h:F

    .line 676537
    iput v2, p0, LX/4AW;->i:I

    .line 676538
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, LX/4AW;->c:Landroid/graphics/Path;

    .line 676539
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, LX/4AW;->d:Landroid/graphics/Path;

    .line 676540
    iput v2, p0, LX/4AW;->j:I

    .line 676541
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, LX/4AW;->k:Landroid/graphics/RectF;

    .line 676542
    const/16 v0, 0xff

    iput v0, p0, LX/4AW;->l:I

    .line 676543
    iget v0, p0, LX/4AW;->j:I

    if-eq v0, p1, :cond_0

    .line 676544
    iput p1, p0, LX/4AW;->j:I

    .line 676545
    invoke-virtual {p0}, LX/4AW;->invalidateSelf()V

    .line 676546
    :cond_0
    return-void
.end method

.method private a()V
    .locals 6

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    .line 676511
    iget-object v0, p0, LX/4AW;->c:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 676512
    iget-object v0, p0, LX/4AW;->d:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 676513
    iget-object v0, p0, LX/4AW;->k:Landroid/graphics/RectF;

    invoke-virtual {p0}, LX/4AW;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 676514
    iget-object v0, p0, LX/4AW;->k:Landroid/graphics/RectF;

    iget v1, p0, LX/4AW;->g:F

    div-float/2addr v1, v5

    iget v2, p0, LX/4AW;->g:F

    div-float/2addr v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/graphics/RectF;->inset(FF)V

    .line 676515
    iget-boolean v0, p0, LX/4AW;->f:Z

    if-eqz v0, :cond_0

    .line 676516
    iget-object v0, p0, LX/4AW;->k:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    iget-object v1, p0, LX/4AW;->k:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    div-float/2addr v0, v5

    .line 676517
    iget-object v1, p0, LX/4AW;->d:Landroid/graphics/Path;

    iget-object v2, p0, LX/4AW;->k:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->centerX()F

    move-result v2

    iget-object v3, p0, LX/4AW;->k:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->centerY()F

    move-result v3

    sget-object v4, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/graphics/Path;->addCircle(FFFLandroid/graphics/Path$Direction;)V

    .line 676518
    :goto_0
    iget-object v0, p0, LX/4AW;->k:Landroid/graphics/RectF;

    iget v1, p0, LX/4AW;->g:F

    neg-float v1, v1

    div-float/2addr v1, v5

    iget v2, p0, LX/4AW;->g:F

    neg-float v2, v2

    div-float/2addr v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/graphics/RectF;->inset(FF)V

    .line 676519
    iget-object v0, p0, LX/4AW;->k:Landroid/graphics/RectF;

    iget v1, p0, LX/4AW;->h:F

    iget v2, p0, LX/4AW;->h:F

    invoke-virtual {v0, v1, v2}, Landroid/graphics/RectF;->inset(FF)V

    .line 676520
    iget-boolean v0, p0, LX/4AW;->f:Z

    if-eqz v0, :cond_2

    .line 676521
    iget-object v0, p0, LX/4AW;->k:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    iget-object v1, p0, LX/4AW;->k:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    div-float/2addr v0, v5

    .line 676522
    iget-object v1, p0, LX/4AW;->c:Landroid/graphics/Path;

    iget-object v2, p0, LX/4AW;->k:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->centerX()F

    move-result v2

    iget-object v3, p0, LX/4AW;->k:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->centerY()F

    move-result v3

    sget-object v4, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/graphics/Path;->addCircle(FFFLandroid/graphics/Path$Direction;)V

    .line 676523
    :goto_1
    iget-object v0, p0, LX/4AW;->k:Landroid/graphics/RectF;

    iget v1, p0, LX/4AW;->h:F

    neg-float v1, v1

    iget v2, p0, LX/4AW;->h:F

    neg-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/RectF;->inset(FF)V

    .line 676524
    return-void

    .line 676525
    :cond_0
    const/4 v0, 0x0

    :goto_2
    iget-object v1, p0, LX/4AW;->a:[F

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 676526
    iget-object v1, p0, LX/4AW;->a:[F

    iget-object v2, p0, LX/4AW;->e:[F

    aget v2, v2, v0

    iget v3, p0, LX/4AW;->h:F

    add-float/2addr v2, v3

    iget v3, p0, LX/4AW;->g:F

    div-float/2addr v3, v5

    sub-float/2addr v2, v3

    aput v2, v1, v0

    .line 676527
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 676528
    :cond_1
    iget-object v0, p0, LX/4AW;->d:Landroid/graphics/Path;

    iget-object v1, p0, LX/4AW;->k:Landroid/graphics/RectF;

    iget-object v2, p0, LX/4AW;->a:[F

    sget-object v3, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Path;->addRoundRect(Landroid/graphics/RectF;[FLandroid/graphics/Path$Direction;)V

    goto :goto_0

    .line 676529
    :cond_2
    iget-object v0, p0, LX/4AW;->c:Landroid/graphics/Path;

    iget-object v1, p0, LX/4AW;->k:Landroid/graphics/RectF;

    iget-object v2, p0, LX/4AW;->e:[F

    sget-object v3, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Path;->addRoundRect(Landroid/graphics/RectF;[FLandroid/graphics/Path$Direction;)V

    goto :goto_1
.end method


# virtual methods
.method public final a(F)V
    .locals 2

    .prologue
    .line 676505
    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "radius should be non negative"

    invoke-static {v0, v1}, LX/03g;->a(ZLjava/lang/Object;)V

    .line 676506
    iget-object v0, p0, LX/4AW;->e:[F

    invoke-static {v0, p1}, Ljava/util/Arrays;->fill([FF)V

    .line 676507
    invoke-direct {p0}, LX/4AW;->a()V

    .line 676508
    invoke-virtual {p0}, LX/4AW;->invalidateSelf()V

    .line 676509
    return-void

    .line 676510
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(IF)V
    .locals 1

    .prologue
    .line 676497
    iget v0, p0, LX/4AW;->i:I

    if-eq v0, p1, :cond_0

    .line 676498
    iput p1, p0, LX/4AW;->i:I

    .line 676499
    invoke-virtual {p0}, LX/4AW;->invalidateSelf()V

    .line 676500
    :cond_0
    iget v0, p0, LX/4AW;->g:F

    cmpl-float v0, v0, p2

    if-eqz v0, :cond_1

    .line 676501
    iput p2, p0, LX/4AW;->g:F

    .line 676502
    invoke-direct {p0}, LX/4AW;->a()V

    .line 676503
    invoke-virtual {p0}, LX/4AW;->invalidateSelf()V

    .line 676504
    :cond_1
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 676490
    iput-boolean p1, p0, LX/4AW;->f:Z

    .line 676491
    invoke-direct {p0}, LX/4AW;->a()V

    .line 676492
    invoke-virtual {p0}, LX/4AW;->invalidateSelf()V

    .line 676493
    return-void
.end method

.method public final a([F)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v1, 0x0

    .line 676547
    if-nez p1, :cond_0

    .line 676548
    iget-object v0, p0, LX/4AW;->e:[F

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([FF)V

    .line 676549
    :goto_0
    invoke-direct {p0}, LX/4AW;->a()V

    .line 676550
    invoke-virtual {p0}, LX/4AW;->invalidateSelf()V

    .line 676551
    return-void

    .line 676552
    :cond_0
    array-length v0, p1

    if-ne v0, v3, :cond_1

    const/4 v0, 0x1

    :goto_1
    const-string v2, "radii should have exactly 8 values"

    invoke-static {v0, v2}, LX/03g;->a(ZLjava/lang/Object;)V

    .line 676553
    iget-object v0, p0, LX/4AW;->e:[F

    invoke-static {p1, v1, v0, v1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 676554
    goto :goto_1
.end method

.method public final b(F)V
    .locals 1

    .prologue
    .line 676485
    iget v0, p0, LX/4AW;->h:F

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_0

    .line 676486
    iput p1, p0, LX/4AW;->h:F

    .line 676487
    invoke-direct {p0}, LX/4AW;->a()V

    .line 676488
    invoke-virtual {p0}, LX/4AW;->invalidateSelf()V

    .line 676489
    :cond_0
    return-void
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    .line 676476
    iget-object v0, p0, LX/4AW;->b:Landroid/graphics/Paint;

    iget v1, p0, LX/4AW;->j:I

    iget v2, p0, LX/4AW;->l:I

    invoke-static {v1, v2}, LX/1am;->a(II)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 676477
    iget-object v0, p0, LX/4AW;->b:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 676478
    iget-object v0, p0, LX/4AW;->c:Landroid/graphics/Path;

    iget-object v1, p0, LX/4AW;->b:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 676479
    iget v0, p0, LX/4AW;->g:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    .line 676480
    iget-object v0, p0, LX/4AW;->b:Landroid/graphics/Paint;

    iget v1, p0, LX/4AW;->i:I

    iget v2, p0, LX/4AW;->l:I

    invoke-static {v1, v2}, LX/1am;->a(II)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 676481
    iget-object v0, p0, LX/4AW;->b:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 676482
    iget-object v0, p0, LX/4AW;->b:Landroid/graphics/Paint;

    iget v1, p0, LX/4AW;->g:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 676483
    iget-object v0, p0, LX/4AW;->d:Landroid/graphics/Path;

    iget-object v1, p0, LX/4AW;->b:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 676484
    :cond_0
    return-void
.end method

.method public final getAlpha()I
    .locals 1

    .prologue
    .line 676466
    iget v0, p0, LX/4AW;->l:I

    return v0
.end method

.method public final getOpacity()I
    .locals 2

    .prologue
    .line 676475
    iget v0, p0, LX/4AW;->j:I

    iget v1, p0, LX/4AW;->l:I

    invoke-static {v0, v1}, LX/1am;->a(II)I

    move-result v0

    invoke-static {v0}, LX/1am;->a(I)I

    move-result v0

    return v0
.end method

.method public final onBoundsChange(Landroid/graphics/Rect;)V
    .locals 0

    .prologue
    .line 676472
    invoke-super {p0, p1}, Landroid/graphics/drawable/Drawable;->onBoundsChange(Landroid/graphics/Rect;)V

    .line 676473
    invoke-direct {p0}, LX/4AW;->a()V

    .line 676474
    return-void
.end method

.method public final setAlpha(I)V
    .locals 1

    .prologue
    .line 676468
    iget v0, p0, LX/4AW;->l:I

    if-eq p1, v0, :cond_0

    .line 676469
    iput p1, p0, LX/4AW;->l:I

    .line 676470
    invoke-virtual {p0}, LX/4AW;->invalidateSelf()V

    .line 676471
    :cond_0
    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 0

    .prologue
    .line 676467
    return-void
.end method
