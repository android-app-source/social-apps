.class public abstract LX/4xA;
.super LX/4x7;
.source ""

# interfaces
.implements LX/4x9;


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "LX/4x7",
        "<TE;>;",
        "LX/4x9",
        "<TE;>;"
    }
.end annotation


# instance fields
.field private transient a:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<-TE;>;"
        }
    .end annotation
.end field

.field private transient b:Ljava/util/NavigableSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/NavigableSet",
            "<TE;>;"
        }
    .end annotation
.end field

.field private transient c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/common/collect/Multiset$Entry",
            "<TE;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 820812
    invoke-direct {p0}, LX/4x7;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/4xG;)LX/4x9;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;",
            "LX/4xG;",
            ")",
            "LX/4x9",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 820813
    invoke-virtual {p0}, LX/4xA;->c()LX/4x9;

    move-result-object v0

    invoke-interface {v0, p1, p2}, LX/4x9;->b(Ljava/lang/Object;LX/4xG;)LX/4x9;

    move-result-object v0

    invoke-interface {v0}, LX/4x9;->n()LX/4x9;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/4xG;Ljava/lang/Object;LX/4xG;)LX/4x9;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;",
            "LX/4xG;",
            "TE;",
            "LX/4xG;",
            ")",
            "LX/4x9",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 820814
    invoke-virtual {p0}, LX/4xA;->c()LX/4x9;

    move-result-object v0

    invoke-interface {v0, p3, p4, p1, p2}, LX/4x9;->a(Ljava/lang/Object;LX/4xG;Ljava/lang/Object;LX/4xG;)LX/4x9;

    move-result-object v0

    invoke-interface {v0}, LX/4x9;->n()LX/4x9;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/google/common/collect/Multiset$Entry",
            "<TE;>;>;"
        }
    .end annotation

    .prologue
    .line 820815
    iget-object v0, p0, LX/4xA;->c:Ljava/util/Set;

    .line 820816
    if-nez v0, :cond_0

    .line 820817
    new-instance v0, LX/4xS;

    invoke-direct {v0, p0}, LX/4xS;-><init>(LX/4xA;)V

    move-object v0, v0

    .line 820818
    iput-object v0, p0, LX/4xA;->c:Ljava/util/Set;

    :cond_0
    return-object v0
.end method

.method public final b(Ljava/lang/Object;LX/4xG;)LX/4x9;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;",
            "LX/4xG;",
            ")",
            "LX/4x9",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 820819
    invoke-virtual {p0}, LX/4xA;->c()LX/4x9;

    move-result-object v0

    invoke-interface {v0, p1, p2}, LX/4x9;->a(Ljava/lang/Object;LX/4xG;)LX/4x9;

    move-result-object v0

    invoke-interface {v0}, LX/4x9;->n()LX/4x9;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 820820
    invoke-virtual {p0}, LX/4xA;->l()LX/1M1;

    move-result-object v0

    return-object v0
.end method

.method public abstract c()LX/4x9;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/4x9",
            "<TE;>;"
        }
    .end annotation
.end method

.method public final comparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<-TE;>;"
        }
    .end annotation

    .prologue
    .line 820821
    iget-object v0, p0, LX/4xA;->a:Ljava/util/Comparator;

    .line 820822
    if-nez v0, :cond_0

    .line 820823
    invoke-virtual {p0}, LX/4xA;->c()LX/4x9;

    move-result-object v0

    invoke-interface {v0}, LX/4x9;->comparator()Ljava/util/Comparator;

    move-result-object v0

    invoke-static {v0}, LX/1sm;->a(Ljava/util/Comparator;)LX/1sm;

    move-result-object v0

    invoke-virtual {v0}, LX/1sm;->a()LX/1sm;

    move-result-object v0

    iput-object v0, p0, LX/4xA;->a:Ljava/util/Comparator;

    .line 820824
    :cond_0
    return-object v0
.end method

.method public final synthetic d()Ljava/util/Set;
    .locals 1

    .prologue
    .line 820825
    invoke-virtual {p0}, LX/4xA;->g()Ljava/util/NavigableSet;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic e()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 820811
    invoke-virtual {p0}, LX/4xA;->l()LX/1M1;

    move-result-object v0

    return-object v0
.end method

.method public abstract f()Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lcom/google/common/collect/Multiset$Entry",
            "<TE;>;>;"
        }
    .end annotation
.end method

.method public final g()Ljava/util/NavigableSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/NavigableSet",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 820797
    iget-object v0, p0, LX/4xA;->b:Ljava/util/NavigableSet;

    .line 820798
    if-nez v0, :cond_0

    .line 820799
    new-instance v0, LX/50o;

    invoke-direct {v0, p0}, LX/50o;-><init>(LX/4x9;)V

    iput-object v0, p0, LX/4xA;->b:Ljava/util/NavigableSet;

    .line 820800
    :cond_0
    return-object v0
.end method

.method public final h()LX/4wx;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/Multiset$Entry",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 820801
    invoke-virtual {p0}, LX/4xA;->c()LX/4x9;

    move-result-object v0

    invoke-interface {v0}, LX/4x9;->i()LX/4wx;

    move-result-object v0

    return-object v0
.end method

.method public final i()LX/4wx;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/Multiset$Entry",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 820802
    invoke-virtual {p0}, LX/4xA;->c()LX/4x9;

    move-result-object v0

    invoke-interface {v0}, LX/4x9;->h()LX/4wx;

    move-result-object v0

    return-object v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 820803
    invoke-static {p0}, LX/2Tc;->a(LX/1M1;)Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public final j()LX/4wx;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/Multiset$Entry",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 820804
    invoke-virtual {p0}, LX/4xA;->c()LX/4x9;

    move-result-object v0

    invoke-interface {v0}, LX/4x9;->k()LX/4wx;

    move-result-object v0

    return-object v0
.end method

.method public final k()LX/4wx;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/Multiset$Entry",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 820805
    invoke-virtual {p0}, LX/4xA;->c()LX/4x9;

    move-result-object v0

    invoke-interface {v0}, LX/4x9;->j()LX/4wx;

    move-result-object v0

    return-object v0
.end method

.method public final l()LX/1M1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1M1",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 820806
    invoke-virtual {p0}, LX/4xA;->c()LX/4x9;

    move-result-object v0

    return-object v0
.end method

.method public final n()LX/4x9;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/4x9",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 820807
    invoke-virtual {p0}, LX/4xA;->c()LX/4x9;

    move-result-object v0

    return-object v0
.end method

.method public final toArray()[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 820808
    invoke-virtual {p0}, LX/306;->o()[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;)[TT;"
        }
    .end annotation

    .prologue
    .line 820809
    invoke-virtual {p0, p1}, LX/306;->a([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 820810
    invoke-virtual {p0}, LX/4xA;->a()Ljava/util/Set;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
