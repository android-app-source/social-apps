.class public LX/4Mt;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 689605
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 11

    .prologue
    .line 689621
    const/4 v3, 0x0

    .line 689622
    const-wide/16 v6, 0x0

    .line 689623
    const-wide/16 v4, 0x0

    .line 689624
    const/4 v2, 0x0

    .line 689625
    const/4 v1, 0x0

    .line 689626
    const/4 v0, 0x0

    .line 689627
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v8, v9, :cond_8

    .line 689628
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 689629
    const/4 v0, 0x0

    .line 689630
    :goto_0
    return v0

    .line 689631
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v0

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v0, v7, :cond_4

    .line 689632
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v0

    .line 689633
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 689634
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v10, :cond_0

    if-eqz v0, :cond_0

    .line 689635
    const-string v7, "horizontal_alignment_within_box"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 689636
    const/4 v0, 0x1

    .line 689637
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    move-result-object v4

    move-object v5, v4

    move v4, v0

    goto :goto_1

    .line 689638
    :cond_1
    const-string v7, "text_box_height"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 689639
    const/4 v0, 0x1

    .line 689640
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v2

    move v1, v0

    goto :goto_1

    .line 689641
    :cond_2
    const-string v7, "text_box_width"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 689642
    const/4 v0, 0x1

    .line 689643
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v6

    move-wide v8, v6

    move v6, v0

    goto :goto_1

    .line 689644
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 689645
    :cond_4
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 689646
    if-eqz v4, :cond_5

    .line 689647
    const/4 v0, 0x0

    invoke-virtual {p1, v0, v5}, LX/186;->a(ILjava/lang/Enum;)V

    .line 689648
    :cond_5
    if-eqz v1, :cond_6

    .line 689649
    const/4 v1, 0x1

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 689650
    :cond_6
    if-eqz v6, :cond_7

    .line 689651
    const/4 v1, 0x2

    const-wide/16 v4, 0x0

    move-object v0, p1

    move-wide v2, v8

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 689652
    :cond_7
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto :goto_0

    :cond_8
    move-wide v8, v4

    move v4, v2

    move-object v5, v3

    move-wide v2, v6

    move v6, v0

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    .line 689606
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 689607
    invoke-virtual {p0, p1, v1, v1}, LX/15i;->a(IIS)S

    move-result v0

    .line 689608
    if-eqz v0, :cond_0

    .line 689609
    const-string v0, "horizontal_alignment_within_box"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 689610
    const-class v0, Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 689611
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 689612
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_1

    .line 689613
    const-string v2, "text_box_height"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 689614
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 689615
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 689616
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_2

    .line 689617
    const-string v2, "text_box_width"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 689618
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 689619
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 689620
    return-void
.end method
