.class public LX/4hp;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0aG;


# direct methods
.method public constructor <init>(LX/0aG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 802107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 802108
    iput-object p1, p0, LX/4hp;->a:LX/0aG;

    .line 802109
    return-void
.end method

.method public static a(LX/0QB;)LX/4hp;
    .locals 1

    .prologue
    .line 802110
    invoke-static {p0}, LX/4hp;->b(LX/0QB;)LX/4hp;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/4hp;
    .locals 2

    .prologue
    .line 802111
    new-instance v1, LX/4hp;

    invoke-static {p0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v0

    check-cast v0, LX/0aG;

    invoke-direct {v1, v0}, LX/4hp;-><init>(LX/0aG;)V

    .line 802112
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 802113
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 802114
    const-string v1, "platform_delete_temp_files_params"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 802115
    const-string v1, "platform_delete_temp_files"

    .line 802116
    iget-object v2, p0, LX/4hp;->a:LX/0aG;

    const p1, -0x3dfa935f

    invoke-static {v2, v1, v0, p1}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v2

    invoke-interface {v2}, LX/1MF;->start()LX/1ML;

    move-result-object v2

    .line 802117
    move-object v0, v2

    .line 802118
    return-object v0
.end method
