.class public final LX/4Xu;
.super LX/0ur;
.source ""


# instance fields
.field public A:Lcom/facebook/graphql/model/GraphQLStoryHeader;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public B:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;",
            ">;"
        }
    .end annotation
.end field

.field public C:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public D:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public E:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public F:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public G:LX/0x2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:J

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/model/GraphQLFeedback;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/model/GraphQLFeedbackContext;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:J

.field public m:I

.field public n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public r:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:I

.field public t:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

.field public y:Lcom/facebook/graphql/model/GraphQLEntity;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 773595
    invoke-direct {p0}, LX/0ur;-><init>()V

    .line 773596
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    iput-object v0, p0, LX/4Xu;->x:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    .line 773597
    const/4 v0, 0x0

    iput-object v0, p0, LX/4Xu;->G:LX/0x2;

    .line 773598
    instance-of v0, p0, LX/4Xu;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 773599
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;)LX/4Xu;
    .locals 4

    .prologue
    .line 773559
    new-instance v1, LX/4Xu;

    invoke-direct {v1}, LX/4Xu;-><init>()V

    .line 773560
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 773561
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->s()LX/0Px;

    move-result-object v0

    iput-object v0, v1, LX/4Xu;->b:LX/0Px;

    .line 773562
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->t()LX/0Px;

    move-result-object v0

    iput-object v0, v1, LX/4Xu;->c:LX/0Px;

    .line 773563
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsConnection;

    move-result-object v0

    iput-object v0, v1, LX/4Xu;->d:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsConnection;

    .line 773564
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->v()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    iput-object v0, v1, LX/4Xu;->e:Lcom/facebook/graphql/model/GraphQLImage;

    .line 773565
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->w()LX/0Px;

    move-result-object v0

    iput-object v0, v1, LX/4Xu;->f:LX/0Px;

    .line 773566
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/4Xu;->g:Ljava/lang/String;

    .line 773567
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->x()J

    move-result-wide v2

    iput-wide v2, v1, LX/4Xu;->h:J

    .line 773568
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->E_()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/4Xu;->i:Ljava/lang/String;

    .line 773569
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->y()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    iput-object v0, v1, LX/4Xu;->j:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 773570
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->z()Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    move-result-object v0

    iput-object v0, v1, LX/4Xu;->k:Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    .line 773571
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->F_()J

    move-result-wide v2

    iput-wide v2, v1, LX/4Xu;->l:J

    .line 773572
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->A()I

    move-result v0

    iput v0, v1, LX/4Xu;->m:I

    .line 773573
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->C_()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/4Xu;->n:Ljava/lang/String;

    .line 773574
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->B()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/4Xu;->o:Ljava/lang/String;

    .line 773575
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->C()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/4Xu;->p:Ljava/lang/String;

    .line 773576
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->R()LX/0Px;

    move-result-object v0

    iput-object v0, v1, LX/4Xu;->q:LX/0Px;

    .line 773577
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->D()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/4Xu;->r:Ljava/lang/String;

    .line 773578
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->E()I

    move-result v0

    iput v0, v1, LX/4Xu;->s:I

    .line 773579
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->F()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    iput-object v0, v1, LX/4Xu;->t:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 773580
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->G()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v0

    iput-object v0, v1, LX/4Xu;->u:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 773581
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->H()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    iput-object v0, v1, LX/4Xu;->v:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 773582
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->I()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    iput-object v0, v1, LX/4Xu;->w:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 773583
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->J()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v0

    iput-object v0, v1, LX/4Xu;->x:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    .line 773584
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->K()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v0

    iput-object v0, v1, LX/4Xu;->y:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 773585
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->L()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/4Xu;->z:Ljava/lang/String;

    .line 773586
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->M()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v0

    iput-object v0, v1, LX/4Xu;->A:Lcom/facebook/graphql/model/GraphQLStoryHeader;

    .line 773587
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->N()LX/0Px;

    move-result-object v0

    iput-object v0, v1, LX/4Xu;->B:LX/0Px;

    .line 773588
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->O()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    iput-object v0, v1, LX/4Xu;->C:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 773589
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->P()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    iput-object v0, v1, LX/4Xu;->D:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 773590
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/4Xu;->E:Ljava/lang/String;

    .line 773591
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->Q()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/4Xu;->F:Ljava/lang/String;

    .line 773592
    invoke-static {v1, p0}, LX/0ur;->a(LX/0ur;Lcom/facebook/graphql/modelutil/BaseModel;)V

    .line 773593
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;->L_()LX/0x2;

    move-result-object v0

    invoke-virtual {v0}, LX/0x2;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0x2;

    iput-object v0, v1, LX/4Xu;->G:LX/0x2;

    .line 773594
    return-object v1
.end method


# virtual methods
.method public final a()Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;
    .locals 2

    .prologue
    .line 773557
    new-instance v0, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;

    invoke-direct {v0, p0}, Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;-><init>(LX/4Xu;)V

    .line 773558
    return-object v0
.end method
