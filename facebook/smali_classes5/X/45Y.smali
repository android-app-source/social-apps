.class public final LX/45Y;
.super Ljava/lang/Object;
.source ""


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 670385
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(II)V
    .locals 3

    .prologue
    .line 670386
    if-eq p0, p1, :cond_0

    .line 670387
    new-instance v0, LX/45X;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "expected = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " , actual = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/45X;-><init>(Ljava/lang/String;)V

    throw v0

    .line 670388
    :cond_0
    return-void
.end method

.method public static a(Z)V
    .locals 1

    .prologue
    .line 670389
    if-nez p0, :cond_0

    .line 670390
    new-instance v0, LX/45X;

    invoke-direct {v0}, LX/45X;-><init>()V

    throw v0

    .line 670391
    :cond_0
    return-void
.end method

.method public static a(ZLjava/lang/String;)V
    .locals 1

    .prologue
    .line 670392
    if-nez p0, :cond_0

    .line 670393
    new-instance v0, LX/45X;

    invoke-direct {v0, p1}, LX/45X;-><init>(Ljava/lang/String;)V

    throw v0

    .line 670394
    :cond_0
    return-void
.end method
