.class public abstract LX/3lI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3l4;


# instance fields
.field private a:Landroid/view/View;

.field private b:Landroid/view/View;

.field private c:LX/0hs;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 633738
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private h()Z
    .locals 1

    .prologue
    .line 633737
    iget-object v0, p0, LX/3lI;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 633736
    const-wide/32 v0, 0x5265c00

    return-wide v0
.end method

.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/10S;
    .locals 1

    .prologue
    .line 633735
    invoke-direct {p0}, LX/3lI;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/10S;->ELIGIBLE:LX/10S;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;

    goto :goto_0
.end method

.method public final a(LX/5L2;)LX/ASZ;
    .locals 1

    .prologue
    .line 633734
    sget-object v0, LX/ASZ;->NONE:LX/ASZ;

    return-object v0
.end method

.method public final a(J)V
    .locals 0

    .prologue
    .line 633708
    return-void
.end method

.method public final a(Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 633733
    return-void
.end method

.method public final a(Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 633739
    iput-object p1, p0, LX/3lI;->a:Landroid/view/View;

    .line 633740
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 633728
    invoke-direct {p0}, LX/3lI;->h()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 633729
    invoke-virtual {p0}, LX/3lI;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 633730
    iget-object v0, p0, LX/3lI;->c:LX/0hs;

    invoke-virtual {v0}, LX/0ht;->l()V

    .line 633731
    const/4 v0, 0x0

    iput-object v0, p0, LX/3lI;->c:LX/0hs;

    .line 633732
    :cond_0
    return-void
.end method

.method public final c()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 633727
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->COMPOSER:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final d()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 633723
    iput-object v0, p0, LX/3lI;->a:Landroid/view/View;

    .line 633724
    iput-object v0, p0, LX/3lI;->b:Landroid/view/View;

    .line 633725
    iput-object v0, p0, LX/3lI;->c:LX/0hs;

    .line 633726
    return-void
.end method

.method public final e()V
    .locals 3

    .prologue
    .line 633712
    invoke-direct {p0}, LX/3lI;->h()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 633713
    invoke-virtual {p0}, LX/3lI;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 633714
    :goto_0
    return-void

    .line 633715
    :cond_0
    iget-object v0, p0, LX/3lI;->b:Landroid/view/View;

    if-nez v0, :cond_1

    .line 633716
    iget-object v0, p0, LX/3lI;->a:Landroid/view/View;

    const v1, 0x7f0d2c18

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/3lI;->b:Landroid/view/View;

    .line 633717
    :cond_1
    new-instance v0, LX/0hs;

    iget-object v1, p0, LX/3lI;->b:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, LX/0hs;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, LX/3lI;->c:LX/0hs;

    .line 633718
    iget-object v0, p0, LX/3lI;->c:LX/0hs;

    iget-object v1, p0, LX/3lI;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, LX/0ht;->c(Landroid/view/View;)V

    .line 633719
    iget-object v0, p0, LX/3lI;->c:LX/0hs;

    invoke-virtual {p0}, LX/3lI;->g()I

    move-result v1

    invoke-virtual {v0, v1}, LX/0hs;->b(I)V

    .line 633720
    iget-object v0, p0, LX/3lI;->c:LX/0hs;

    const/4 v1, -0x1

    .line 633721
    iput v1, v0, LX/0hs;->t:I

    .line 633722
    iget-object v0, p0, LX/3lI;->c:LX/0hs;

    invoke-virtual {v0}, LX/0ht;->d()V

    goto :goto_0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 633709
    iget-object v0, p0, LX/3lI;->c:LX/0hs;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3lI;->c:LX/0hs;

    .line 633710
    iget-boolean p0, v0, LX/0ht;->r:Z

    move v0, p0

    .line 633711
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract g()I
.end method
