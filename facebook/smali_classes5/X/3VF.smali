.class public LX/3VF;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/3VG;


# static fields
.field public static final a:LX/1Cz;


# instance fields
.field private final b:Landroid/widget/TextView;

.field private final c:Landroid/widget/TextView;

.field private final d:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Landroid/view/View;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 587664
    new-instance v0, LX/3VH;

    invoke-direct {v0}, LX/3VH;-><init>()V

    sput-object v0, LX/3VF;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 587665
    const/4 v0, 0x0

    const v1, 0x7f0301a0

    invoke-direct {p0, p1, v0, v1}, LX/3VF;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 587666
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 587667
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, LX/3VF;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 587668
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 587670
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 587671
    invoke-virtual {p0, p3}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 587672
    const v0, 0x7f0d0701

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/3VF;->b:Landroid/widget/TextView;

    .line 587673
    const v0, 0x7f0d0702

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/3VF;->c:Landroid/widget/TextView;

    .line 587674
    const v0, 0x7f0d0703

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->d(I)LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/3VF;->d:LX/0am;

    .line 587675
    const v0, 0x7f0d0704

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/3VF;->e:Landroid/view/View;

    .line 587676
    invoke-virtual {p0}, LX/3VF;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 587677
    const v1, 0x7f0810d0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/3VF;->f:Ljava/lang/String;

    .line 587678
    const v1, 0x7f0810d1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/3VF;->g:Ljava/lang/String;

    .line 587679
    const v1, 0x7f0810d2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/3VF;->h:Ljava/lang/String;

    .line 587680
    const v1, 0x7f0810d3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/3VF;->i:Ljava/lang/String;

    .line 587681
    const v1, 0x7f0810d4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/3VF;->j:Ljava/lang/String;

    .line 587682
    const v1, 0x7f0810d5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/3VF;->k:Ljava/lang/String;

    .line 587683
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 587669
    iget-object v0, p0, LX/3VF;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getContainerView()Landroid/view/View;
    .locals 1

    .prologue
    .line 587647
    iget-object v0, p0, LX/3VF;->e:Landroid/view/View;

    return-object v0
.end method

.method public getLikersCountView()Landroid/view/View;
    .locals 1

    .prologue
    .line 587663
    iget-object v0, p0, LX/3VF;->b:Landroid/widget/TextView;

    return-object v0
.end method

.method public setComments(I)V
    .locals 3

    .prologue
    .line 587661
    iget-object v0, p0, LX/3VF;->c:Landroid/widget/TextView;

    iget-object v1, p0, LX/3VF;->h:Ljava/lang/String;

    iget-object v2, p0, LX/3VF;->i:Ljava/lang/String;

    invoke-static {v0, p1, v1, v2}, LX/Ano;->a(Landroid/widget/TextView;ILjava/lang/String;Ljava/lang/String;)V

    .line 587662
    return-void
.end method

.method public setHeight(I)V
    .locals 2

    .prologue
    .line 587656
    invoke-virtual {p0}, LX/3VF;->getContainerView()Landroid/view/View;

    move-result-object v0

    .line 587657
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iget v1, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-eq v1, p1, :cond_0

    .line 587658
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iput p1, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 587659
    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 587660
    :cond_0
    return-void
.end method

.method public setIsExpanded(Z)V
    .locals 2

    .prologue
    .line 587653
    iget-object v1, p0, LX/3VF;->e:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 587654
    return-void

    .line 587655
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setLikes(I)V
    .locals 3

    .prologue
    .line 587651
    iget-object v0, p0, LX/3VF;->b:Landroid/widget/TextView;

    iget-object v1, p0, LX/3VF;->f:Ljava/lang/String;

    iget-object v2, p0, LX/3VF;->g:Ljava/lang/String;

    invoke-static {v0, p1, v1, v2}, LX/Ano;->a(Landroid/widget/TextView;ILjava/lang/String;Ljava/lang/String;)V

    .line 587652
    return-void
.end method

.method public setShares(I)V
    .locals 3

    .prologue
    .line 587648
    iget-object v0, p0, LX/3VF;->d:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 587649
    iget-object v0, p0, LX/3VF;->d:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, LX/3VF;->j:Ljava/lang/String;

    iget-object v2, p0, LX/3VF;->k:Ljava/lang/String;

    invoke-static {v0, p1, v1, v2}, LX/Ano;->a(Landroid/widget/TextView;ILjava/lang/String;Ljava/lang/String;)V

    .line 587650
    :cond_0
    return-void
.end method
