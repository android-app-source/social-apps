.class public LX/4QZ;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 705237
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 24

    .prologue
    .line 705238
    const/16 v17, 0x0

    .line 705239
    const/16 v16, 0x0

    .line 705240
    const-wide/16 v14, 0x0

    .line 705241
    const/4 v13, 0x0

    .line 705242
    const/4 v12, 0x0

    .line 705243
    const/4 v11, 0x0

    .line 705244
    const/4 v10, 0x0

    .line 705245
    const/4 v9, 0x0

    .line 705246
    const/4 v8, 0x0

    .line 705247
    const/4 v7, 0x0

    .line 705248
    const/4 v6, 0x0

    .line 705249
    const/4 v5, 0x0

    .line 705250
    const/4 v4, 0x0

    .line 705251
    const/4 v3, 0x0

    .line 705252
    const/4 v2, 0x0

    .line 705253
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v18

    sget-object v19, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-eq v0, v1, :cond_11

    .line 705254
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 705255
    const/4 v2, 0x0

    .line 705256
    :goto_0
    return v2

    .line 705257
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_e

    .line 705258
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 705259
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 705260
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v20, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v20

    if-eq v6, v0, :cond_0

    if-eqz v2, :cond_0

    .line 705261
    const-string v6, "cache_id"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 705262
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v19, v2

    goto :goto_1

    .line 705263
    :cond_1
    const-string v6, "debug_info"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 705264
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v7, v2

    goto :goto_1

    .line 705265
    :cond_2
    const-string v6, "fetchTimeMs"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 705266
    const/4 v2, 0x1

    .line 705267
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 705268
    :cond_3
    const-string v6, "hideable_token"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 705269
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v18, v2

    goto :goto_1

    .line 705270
    :cond_4
    const-string v6, "local_last_negative_feedback_action_type"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 705271
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v17, v2

    goto :goto_1

    .line 705272
    :cond_5
    const-string v6, "local_story_visibility"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 705273
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v16, v2

    goto/16 :goto_1

    .line 705274
    :cond_6
    const-string v6, "local_story_visible_height"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 705275
    const/4 v2, 0x1

    .line 705276
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move v8, v2

    move v15, v6

    goto/16 :goto_1

    .line 705277
    :cond_7
    const-string v6, "pymlItems"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 705278
    invoke-static/range {p0 .. p1}, LX/4Qb;->a(LX/15w;LX/186;)I

    move-result v2

    move v14, v2

    goto/16 :goto_1

    .line 705279
    :cond_8
    const-string v6, "pymlPageBrowserCategory"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 705280
    invoke-static/range {p0 .. p1}, LX/4QG;->a(LX/15w;LX/186;)I

    move-result v2

    move v13, v2

    goto/16 :goto_1

    .line 705281
    :cond_9
    const-string v6, "pymlTitle"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 705282
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v2

    move v12, v2

    goto/16 :goto_1

    .line 705283
    :cond_a
    const-string v6, "short_term_cache_key"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 705284
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v11, v2

    goto/16 :goto_1

    .line 705285
    :cond_b
    const-string v6, "title"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 705286
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v2

    move v10, v2

    goto/16 :goto_1

    .line 705287
    :cond_c
    const-string v6, "tracking"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 705288
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v9, v2

    goto/16 :goto_1

    .line 705289
    :cond_d
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 705290
    :cond_e
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 705291
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 705292
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 705293
    if-eqz v3, :cond_f

    .line 705294
    const/4 v3, 0x2

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 705295
    :cond_f
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 705296
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 705297
    const/4 v2, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 705298
    if-eqz v8, :cond_10

    .line 705299
    const/4 v2, 0x6

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15, v3}, LX/186;->a(III)V

    .line 705300
    :cond_10
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 705301
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 705302
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 705303
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 705304
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 705305
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 705306
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_11
    move/from16 v18, v13

    move/from16 v19, v17

    move v13, v8

    move/from16 v17, v12

    move v12, v7

    move v8, v2

    move/from16 v7, v16

    move/from16 v16, v11

    move v11, v6

    move/from16 v21, v10

    move v10, v5

    move-wide/from16 v22, v14

    move/from16 v15, v21

    move v14, v9

    move v9, v4

    move-wide/from16 v4, v22

    goto/16 :goto_1
.end method

.method public static a(LX/15w;S)LX/15i;
    .locals 5

    .prologue
    .line 705307
    const/4 v0, 0x1

    const/4 v4, 0x0

    .line 705308
    new-instance v2, LX/186;

    const/16 v1, 0x80

    invoke-direct {v2, v1}, LX/186;-><init>(I)V

    .line 705309
    invoke-static {p0, v2}, LX/4QZ;->a(LX/15w;LX/186;)I

    move-result v1

    .line 705310
    if-eqz v0, :cond_0

    .line 705311
    const/4 v3, 0x2

    invoke-virtual {v2, v3}, LX/186;->c(I)V

    .line 705312
    invoke-virtual {v2, v4, p1, v4}, LX/186;->a(ISI)V

    .line 705313
    const/4 v3, 0x1

    invoke-virtual {v2, v3, v1}, LX/186;->b(II)V

    .line 705314
    invoke-virtual {v2}, LX/186;->d()I

    move-result v1

    .line 705315
    :cond_0
    invoke-virtual {v2, v1}, LX/186;->d(I)V

    .line 705316
    invoke-static {v2}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v1

    move-object v0, v1

    .line 705317
    return-object v0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    .line 705318
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 705319
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 705320
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 705321
    const-string v0, "name"

    const-string v1, "PagesYouMayLikeFeedUnit"

    invoke-virtual {p2, v0, v1}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 705322
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 705323
    invoke-virtual {p0, p1, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 705324
    if-eqz v0, :cond_0

    .line 705325
    const-string v1, "cache_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 705326
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 705327
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 705328
    if-eqz v0, :cond_1

    .line 705329
    const-string v1, "debug_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 705330
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 705331
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 705332
    cmp-long v2, v0, v4

    if-eqz v2, :cond_2

    .line 705333
    const-string v2, "fetchTimeMs"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 705334
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 705335
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 705336
    if-eqz v0, :cond_3

    .line 705337
    const-string v1, "hideable_token"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 705338
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 705339
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 705340
    if-eqz v0, :cond_4

    .line 705341
    const-string v1, "local_last_negative_feedback_action_type"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 705342
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 705343
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 705344
    if-eqz v0, :cond_5

    .line 705345
    const-string v1, "local_story_visibility"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 705346
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 705347
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 705348
    if-eqz v0, :cond_6

    .line 705349
    const-string v1, "local_story_visible_height"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 705350
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 705351
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 705352
    if-eqz v0, :cond_8

    .line 705353
    const-string v1, "pymlItems"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 705354
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 705355
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_7

    .line 705356
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/4Qb;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 705357
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 705358
    :cond_7
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 705359
    :cond_8
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 705360
    if-eqz v0, :cond_9

    .line 705361
    const-string v1, "pymlPageBrowserCategory"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 705362
    invoke-static {p0, v0, p2}, LX/4QG;->a(LX/15i;ILX/0nX;)V

    .line 705363
    :cond_9
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 705364
    if-eqz v0, :cond_a

    .line 705365
    const-string v1, "pymlTitle"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 705366
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 705367
    :cond_a
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 705368
    if-eqz v0, :cond_b

    .line 705369
    const-string v1, "short_term_cache_key"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 705370
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 705371
    :cond_b
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 705372
    if-eqz v0, :cond_c

    .line 705373
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 705374
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 705375
    :cond_c
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 705376
    if-eqz v0, :cond_d

    .line 705377
    const-string v1, "tracking"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 705378
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 705379
    :cond_d
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 705380
    return-void
.end method
