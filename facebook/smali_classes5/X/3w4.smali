.class public LX/3w4;
.super LX/3vN;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public E:I

.field private F:LX/3um;

.field public G:LX/3vx;

.field private H:LX/3vz;

.field private I:I

.field private J:Z

.field public K:Landroid/graphics/Rect;

.field private final L:LX/3wA;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 654278
    const/4 v0, -0x1

    invoke-direct {p0, p1, p2, p3, v0}, LX/3w4;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 654279
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 654306
    invoke-direct {p0, p1, p2, p3}, LX/3vN;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 654307
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/3w4;->K:Landroid/graphics/Rect;

    .line 654308
    sget-object v0, LX/03r;->Spinner:[I

    invoke-static {p1, p2, v0, p3, v4}, LX/3wC;->a(Landroid/content/Context;Landroid/util/AttributeSet;[III)LX/3wC;

    move-result-object v0

    .line 654309
    const/16 v1, 0x1

    invoke-virtual {v0, v1}, LX/3wC;->d(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 654310
    const/16 v1, 0x1

    invoke-virtual {v0, v1}, LX/3wC;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/3w4;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 654311
    :cond_0
    const/4 v1, -0x1

    if-ne p4, v1, :cond_1

    .line 654312
    const/16 v1, 0x8

    invoke-virtual {v0, v1, v4}, LX/3wC;->a(II)I

    move-result p4

    .line 654313
    :cond_1
    packed-switch p4, :pswitch_data_0

    .line 654314
    :goto_0
    const/16 v1, 0x0

    const/16 v2, 0x11

    invoke-virtual {v0, v1, v2}, LX/3wC;->a(II)I

    move-result v1

    iput v1, p0, LX/3w4;->I:I

    .line 654315
    iget-object v1, p0, LX/3w4;->G:LX/3vx;

    const/16 v2, 0x7

    .line 654316
    iget-object v3, v0, LX/3wC;->b:Landroid/content/res/TypedArray;

    invoke-virtual {v3, v2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object v2, v3

    .line 654317
    invoke-interface {v1, v2}, LX/3vx;->a(Ljava/lang/CharSequence;)V

    .line 654318
    const/16 v1, 0xa

    invoke-virtual {v0, v1, v4}, LX/3wC;->a(IZ)Z

    move-result v1

    iput-boolean v1, p0, LX/3w4;->J:Z

    .line 654319
    invoke-virtual {v0}, LX/3wC;->b()V

    .line 654320
    iget-object v1, p0, LX/3w4;->H:LX/3vz;

    if-eqz v1, :cond_2

    .line 654321
    iget-object v1, p0, LX/3w4;->G:LX/3vx;

    iget-object v2, p0, LX/3w4;->H:LX/3vz;

    invoke-interface {v1, v2}, LX/3vx;->a(Landroid/widget/ListAdapter;)V

    .line 654322
    const/4 v1, 0x0

    iput-object v1, p0, LX/3w4;->H:LX/3vz;

    .line 654323
    :cond_2
    invoke-virtual {v0}, LX/3wC;->c()LX/3wA;

    move-result-object v0

    iput-object v0, p0, LX/3w4;->L:LX/3wA;

    .line 654324
    return-void

    .line 654325
    :pswitch_0
    new-instance v1, LX/3vy;

    invoke-direct {v1, p0}, LX/3vy;-><init>(LX/3w4;)V

    iput-object v1, p0, LX/3w4;->G:LX/3vx;

    goto :goto_0

    .line 654326
    :pswitch_1
    new-instance v1, LX/3w2;

    invoke-direct {v1, p0, p1, p2, p3}, LX/3w2;-><init>(LX/3w4;Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 654327
    const/16 v2, 0x4

    const/4 v3, -0x2

    invoke-virtual {v0, v2, v3}, LX/3wC;->e(II)I

    move-result v2

    iput v2, p0, LX/3w4;->E:I

    .line 654328
    const/16 v2, 0x3

    invoke-virtual {v0, v2}, LX/3wC;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 654329
    iget-object v3, v1, LX/3w1;->d:Landroid/widget/PopupWindow;

    invoke-virtual {v3, v2}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 654330
    iput-object v1, p0, LX/3w4;->G:LX/3vx;

    .line 654331
    new-instance v2, LX/3vv;

    invoke-direct {v2, p0, p0, v1}, LX/3vv;-><init>(LX/3w4;Landroid/view/View;LX/3w2;)V

    iput-object v2, p0, LX/3w4;->F:LX/3um;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(IZ)Landroid/view/View;
    .locals 2

    .prologue
    .line 654299
    iget-boolean v0, p0, LX/3vM;->u:Z

    if-nez v0, :cond_0

    .line 654300
    iget-object v0, p0, LX/3vN;->i:LX/3vK;

    invoke-virtual {v0, p1}, LX/3vK;->a(I)Landroid/view/View;

    move-result-object v0

    .line 654301
    if-eqz v0, :cond_0

    .line 654302
    invoke-direct {p0, v0, p2}, LX/3w4;->a(Landroid/view/View;Z)V

    .line 654303
    :goto_0
    return-object v0

    .line 654304
    :cond_0
    iget-object v0, p0, LX/3vN;->a:Landroid/widget/SpinnerAdapter;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1, p0}, Landroid/widget/SpinnerAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 654305
    invoke-direct {p0, v0, p2}, LX/3w4;->a(Landroid/view/View;Z)V

    goto :goto_0
.end method

.method private a(Landroid/view/View;Z)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 654282
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 654283
    if-nez v0, :cond_0

    .line 654284
    invoke-virtual {p0}, LX/3vN;->generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 654285
    :cond_0
    if-eqz p2, :cond_1

    .line 654286
    invoke-virtual {p0, p1, v5, v0}, LX/3w4;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)Z

    .line 654287
    :cond_1
    invoke-virtual {p0}, LX/3w4;->hasFocus()Z

    move-result v1

    invoke-virtual {p1, v1}, Landroid/view/View;->setSelected(Z)V

    .line 654288
    iget-boolean v1, p0, LX/3w4;->J:Z

    if-eqz v1, :cond_2

    .line 654289
    invoke-virtual {p0}, LX/3w4;->isEnabled()Z

    move-result v1

    invoke-virtual {p1, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 654290
    :cond_2
    iget v1, p0, LX/3vN;->b:I

    iget-object v2, p0, LX/3vN;->h:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, LX/3vN;->h:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v2, v3

    iget v3, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {v1, v2, v3}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    move-result v1

    .line 654291
    iget v2, p0, LX/3vN;->c:I

    iget-object v3, p0, LX/3vN;->h:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget-object v4, p0, LX/3vN;->h:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    add-int/2addr v3, v4

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-static {v2, v3, v0}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    move-result v0

    .line 654292
    invoke-virtual {p1, v0, v1}, Landroid/view/View;->measure(II)V

    .line 654293
    iget-object v0, p0, LX/3vN;->h:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    invoke-virtual {p0}, LX/3w4;->getMeasuredHeight()I

    move-result v1

    iget-object v2, p0, LX/3vN;->h:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v1, v2

    iget-object v2, p0, LX/3vN;->h:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    sub-int/2addr v1, v2

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    .line 654294
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v1, v0

    .line 654295
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    .line 654296
    add-int/lit8 v2, v2, 0x0

    .line 654297
    invoke-virtual {p1, v5, v0, v2, v1}, Landroid/view/View;->layout(IIII)V

    .line 654298
    return-void
.end method


# virtual methods
.method public final a(LX/3vf;)V
    .locals 0

    .prologue
    .line 654280
    invoke-super {p0, p1}, LX/3vN;->setOnItemClickListener(LX/3vf;)V

    .line 654281
    return-void
.end method

.method public final a(Landroid/widget/SpinnerAdapter;)V
    .locals 2

    .prologue
    .line 654269
    invoke-super {p0, p1}, LX/3vN;->a(Landroid/widget/SpinnerAdapter;)V

    .line 654270
    iget-object v0, p0, LX/3vN;->i:LX/3vK;

    invoke-virtual {v0}, LX/3vK;->a()V

    .line 654271
    invoke-virtual {p0}, LX/3w4;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    .line 654272
    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/widget/SpinnerAdapter;->getViewTypeCount()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 654273
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Spinner adapter view type count must be 1"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 654274
    :cond_0
    iget-object v0, p0, LX/3w4;->G:LX/3vx;

    if-eqz v0, :cond_1

    .line 654275
    iget-object v0, p0, LX/3w4;->G:LX/3vx;

    new-instance v1, LX/3vz;

    invoke-direct {v1, p1}, LX/3vz;-><init>(Landroid/widget/SpinnerAdapter;)V

    invoke-interface {v0, v1}, LX/3vx;->a(Landroid/widget/ListAdapter;)V

    .line 654276
    :goto_0
    return-void

    .line 654277
    :cond_1
    new-instance v0, LX/3vz;

    invoke-direct {v0, p1}, LX/3vz;-><init>(Landroid/widget/SpinnerAdapter;)V

    iput-object v0, p0, LX/3w4;->H:LX/3vz;

    goto :goto_0
.end method

.method public final c()V
    .locals 9

    .prologue
    const/4 v6, 0x0

    .line 654234
    iget-object v0, p0, LX/3vN;->h:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    .line 654235
    invoke-virtual {p0}, LX/3w4;->getRight()I

    move-result v1

    invoke-virtual {p0}, LX/3w4;->getLeft()I

    move-result v2

    sub-int/2addr v1, v2

    iget-object v2, p0, LX/3vN;->h:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    sub-int/2addr v1, v2

    iget-object v2, p0, LX/3vN;->h:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    sub-int/2addr v1, v2

    .line 654236
    iget-boolean v2, p0, LX/3vM;->u:Z

    if-eqz v2, :cond_0

    .line 654237
    invoke-virtual {p0}, LX/3vM;->f()V

    .line 654238
    :cond_0
    iget v2, p0, LX/3vM;->z:I

    if-nez v2, :cond_1

    .line 654239
    invoke-virtual {p0}, LX/3vN;->a()V

    .line 654240
    :goto_0
    return-void

    .line 654241
    :cond_1
    iget v2, p0, LX/3vM;->v:I

    if-ltz v2, :cond_2

    .line 654242
    iget v2, p0, LX/3vM;->v:I

    invoke-virtual {p0, v2}, LX/3vM;->setSelectedPositionInt(I)V

    .line 654243
    :cond_2
    invoke-virtual {p0}, LX/3vN;->getChildCount()I

    move-result v3

    .line 654244
    iget-object v4, p0, LX/3vN;->i:LX/3vK;

    .line 654245
    iget v5, p0, LX/3vM;->j:I

    .line 654246
    const/4 v2, 0x0

    :goto_1
    if-ge v2, v3, :cond_3

    .line 654247
    invoke-virtual {p0, v2}, LX/3vN;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    .line 654248
    add-int v8, v5, v2

    .line 654249
    invoke-virtual {v4, v8, v7}, LX/3vK;->a(ILandroid/view/View;)V

    .line 654250
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 654251
    :cond_3
    invoke-virtual {p0}, LX/3w4;->removeAllViewsInLayout()V

    .line 654252
    iget v2, p0, LX/3vM;->x:I

    iput v2, p0, LX/3w4;->j:I

    .line 654253
    iget-object v2, p0, LX/3vN;->a:Landroid/widget/SpinnerAdapter;

    if-eqz v2, :cond_4

    .line 654254
    iget v2, p0, LX/3vM;->x:I

    const/4 v3, 0x1

    invoke-direct {p0, v2, v3}, LX/3w4;->a(IZ)Landroid/view/View;

    move-result-object v2

    .line 654255
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    .line 654256
    invoke-static {p0}, LX/0vv;->h(Landroid/view/View;)I

    move-result v4

    .line 654257
    iget v5, p0, LX/3w4;->I:I

    invoke-static {v5, v4}, LX/1uf;->a(II)I

    move-result v4

    .line 654258
    and-int/lit8 v4, v4, 0x7

    sparse-switch v4, :sswitch_data_0

    .line 654259
    :goto_2
    invoke-virtual {v2, v0}, Landroid/view/View;->offsetLeftAndRight(I)V

    .line 654260
    :cond_4
    iget-object v0, p0, LX/3vN;->i:LX/3vK;

    invoke-virtual {v0}, LX/3vK;->a()V

    .line 654261
    invoke-virtual {p0}, LX/3w4;->invalidate()V

    .line 654262
    invoke-virtual {p0}, LX/3vM;->g()V

    .line 654263
    iput-boolean v6, p0, LX/3w4;->u:Z

    .line 654264
    iput-boolean v6, p0, LX/3w4;->o:Z

    .line 654265
    iget v0, p0, LX/3vM;->x:I

    invoke-virtual {p0, v0}, LX/3vM;->setNextSelectedPositionInt(I)V

    goto :goto_0

    .line 654266
    :sswitch_0
    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    div-int/lit8 v1, v3, 0x2

    sub-int/2addr v0, v1

    .line 654267
    goto :goto_2

    .line 654268
    :sswitch_1
    add-int/2addr v0, v1

    sub-int/2addr v0, v3

    goto :goto_2

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x5 -> :sswitch_1
    .end sparse-switch
.end method

.method public final getBaseline()I
    .locals 4

    .prologue
    const/4 v0, -0x1

    const/4 v3, 0x0

    .line 654224
    const/4 v1, 0x0

    .line 654225
    invoke-virtual {p0}, LX/3w4;->getChildCount()I

    move-result v2

    if-lez v2, :cond_2

    .line 654226
    invoke-virtual {p0, v3}, LX/3w4;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 654227
    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    .line 654228
    invoke-virtual {v1}, Landroid/view/View;->getBaseline()I

    move-result v2

    .line 654229
    if-ltz v2, :cond_1

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v0

    add-int/2addr v0, v2

    .line 654230
    :cond_1
    return v0

    .line 654231
    :cond_2
    iget-object v2, p0, LX/3vN;->a:Landroid/widget/SpinnerAdapter;

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/3vN;->a:Landroid/widget/SpinnerAdapter;

    invoke-interface {v2}, Landroid/widget/SpinnerAdapter;->getCount()I

    move-result v2

    if-lez v2, :cond_0

    .line 654232
    invoke-direct {p0, v3, v3}, LX/3w4;->a(IZ)Landroid/view/View;

    move-result-object v1

    .line 654233
    iget-object v2, p0, LX/3vN;->i:LX/3vK;

    invoke-virtual {v2, v3, v1}, LX/3vK;->a(ILandroid/view/View;)V

    goto :goto_0
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 0

    .prologue
    .line 654221
    invoke-virtual {p0, p2}, LX/3vM;->setSelection(I)V

    .line 654222
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 654223
    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x25ba8593

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 654332
    invoke-super {p0}, LX/3vN;->onDetachedFromWindow()V

    .line 654333
    iget-object v1, p0, LX/3w4;->G:LX/3vx;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/3w4;->G:LX/3vx;

    invoke-interface {v1}, LX/3vx;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 654334
    iget-object v1, p0, LX/3w4;->G:LX/3vx;

    invoke-interface {v1}, LX/3vx;->a()V

    .line 654335
    :cond_0
    const/16 v1, 0x2d

    const v2, 0x753f7b02

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onLayout(ZIIII)V
    .locals 1

    .prologue
    .line 654216
    invoke-super/range {p0 .. p5}, LX/3vN;->onLayout(ZIIII)V

    .line 654217
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/3w4;->q:Z

    .line 654218
    invoke-virtual {p0}, LX/3w4;->c()V

    .line 654219
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/3w4;->q:Z

    .line 654220
    return-void
.end method

.method public final onMeasure(II)V
    .locals 12

    .prologue
    .line 654188
    invoke-super {p0, p1, p2}, LX/3vN;->onMeasure(II)V

    .line 654189
    iget-object v0, p0, LX/3w4;->G:LX/3vx;

    if-eqz v0, :cond_0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    const/high16 v1, -0x80000000

    if-ne v0, v1, :cond_0

    .line 654190
    invoke-virtual {p0}, LX/3w4;->getMeasuredWidth()I

    move-result v0

    .line 654191
    iget-object v1, p0, LX/3vN;->a:Landroid/widget/SpinnerAdapter;

    move-object v1, v1

    .line 654192
    invoke-virtual {p0}, LX/3w4;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    const/4 v5, 0x0

    const/4 p2, -0x2

    const/4 v3, 0x0

    .line 654193
    if-nez v1, :cond_1

    .line 654194
    :goto_0
    move v1, v3

    .line 654195
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-virtual {p0}, LX/3w4;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/3w4;->setMeasuredDimension(II)V

    .line 654196
    :cond_0
    return-void

    .line 654197
    :cond_1
    invoke-static {v3, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    .line 654198
    invoke-static {v3, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v10

    .line 654199
    iget v4, p0, LX/3vM;->v:I

    move v4, v4

    .line 654200
    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 654201
    invoke-interface {v1}, Landroid/widget/SpinnerAdapter;->getCount()I

    move-result v6

    add-int/lit8 v7, v4, 0xf

    invoke-static {v6, v7}, Ljava/lang/Math;->min(II)I

    move-result v11

    .line 654202
    sub-int v6, v11, v4

    .line 654203
    rsub-int/lit8 v6, v6, 0xf

    sub-int/2addr v4, v6

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    move v8, v4

    move-object v6, v5

    move v7, v3

    move v4, v3

    .line 654204
    :goto_1
    if-ge v8, v11, :cond_3

    .line 654205
    invoke-interface {v1, v8}, Landroid/widget/SpinnerAdapter;->getItemViewType(I)I

    move-result v3

    .line 654206
    if-eq v3, v4, :cond_5

    move-object v4, v5

    .line 654207
    :goto_2
    invoke-interface {v1, v8, v4, p0}, Landroid/widget/SpinnerAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    .line 654208
    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    if-nez v4, :cond_2

    .line 654209
    new-instance v4, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v4, p2, p2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v6, v4}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 654210
    :cond_2
    invoke-virtual {v6, v9, v10}, Landroid/view/View;->measure(II)V

    .line 654211
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    invoke-static {v7, v4}, Ljava/lang/Math;->max(II)I

    move-result v7

    .line 654212
    add-int/lit8 v4, v8, 0x1

    move v8, v4

    move v4, v3

    goto :goto_1

    .line 654213
    :cond_3
    if-eqz v2, :cond_4

    .line 654214
    iget-object v3, p0, LX/3w4;->K:Landroid/graphics/Rect;

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 654215
    iget-object v3, p0, LX/3w4;->K:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget-object v4, p0, LX/3w4;->K:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    add-int/2addr v3, v4

    add-int/2addr v3, v7

    goto :goto_0

    :cond_4
    move v3, v7

    goto :goto_0

    :cond_5
    move v3, v4

    move-object v4, v6

    goto :goto_2
.end method

.method public final onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    .prologue
    .line 654180
    check-cast p1, Landroid/support/v7/internal/widget/SpinnerCompat$SavedState;

    .line 654181
    invoke-virtual {p1}, Landroid/support/v7/internal/widget/SpinnerCompat$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, LX/3vN;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 654182
    iget-boolean v0, p1, Landroid/support/v7/internal/widget/SpinnerCompat$SavedState;->c:Z

    if-eqz v0, :cond_0

    .line 654183
    invoke-virtual {p0}, LX/3w4;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 654184
    if-eqz v0, :cond_0

    .line 654185
    new-instance v1, LX/3vw;

    invoke-direct {v1, p0}, LX/3vw;-><init>(LX/3w4;)V

    .line 654186
    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 654187
    :cond_0
    return-void
.end method

.method public final onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .prologue
    .line 654176
    new-instance v1, Landroid/support/v7/internal/widget/SpinnerCompat$SavedState;

    invoke-super {p0}, LX/3vN;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/support/v7/internal/widget/SpinnerCompat$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 654177
    iget-object v0, p0, LX/3w4;->G:LX/3vx;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3w4;->G:LX/3vx;

    invoke-interface {v0}, LX/3vx;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, v1, Landroid/support/v7/internal/widget/SpinnerCompat$SavedState;->c:Z

    .line 654178
    return-object v1

    .line 654179
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v3, 0x2

    const v1, -0x71dcb447

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 654158
    iget-object v2, p0, LX/3w4;->F:LX/3um;

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/3w4;->F:LX/3um;

    invoke-virtual {v2, p0, p1}, LX/3um;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 654159
    const v2, -0x34940581    # -1.5465087E7f

    invoke-static {v3, v3, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 654160
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, LX/3vN;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    const v2, -0x6fcd7146

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto :goto_0
.end method

.method public final performClick()Z
    .locals 2

    .prologue
    .line 654170
    invoke-super {p0}, LX/3vN;->performClick()Z

    move-result v0

    .line 654171
    if-nez v0, :cond_0

    .line 654172
    const/4 v0, 0x1

    .line 654173
    iget-object v1, p0, LX/3w4;->G:LX/3vx;

    invoke-interface {v1}, LX/3vx;->b()Z

    move-result v1

    if-nez v1, :cond_0

    .line 654174
    iget-object v1, p0, LX/3w4;->G:LX/3vx;

    invoke-interface {v1}, LX/3vx;->c()V

    .line 654175
    :cond_0
    return v0
.end method

.method public final synthetic setAdapter(Landroid/widget/Adapter;)V
    .locals 0

    .prologue
    .line 654169
    check-cast p1, Landroid/widget/SpinnerAdapter;

    invoke-virtual {p0, p1}, LX/3w4;->a(Landroid/widget/SpinnerAdapter;)V

    return-void
.end method

.method public final setEnabled(Z)V
    .locals 3

    .prologue
    .line 654162
    invoke-super {p0, p1}, LX/3vN;->setEnabled(Z)V

    .line 654163
    iget-boolean v0, p0, LX/3w4;->J:Z

    if-eqz v0, :cond_0

    .line 654164
    invoke-virtual {p0}, LX/3w4;->getChildCount()I

    move-result v1

    .line 654165
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 654166
    invoke-virtual {p0, v0}, LX/3w4;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 654167
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 654168
    :cond_0
    return-void
.end method

.method public final setOnItemClickListener(LX/3vf;)V
    .locals 2

    .prologue
    .line 654161
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "setOnItemClickListener cannot be used with a spinner."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
