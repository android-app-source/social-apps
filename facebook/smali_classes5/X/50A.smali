.class public final LX/50A;
.super LX/2QI;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2QI",
        "<TK;",
        "Ljava/util/Collection",
        "<TV;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/50B;


# direct methods
.method public constructor <init>(LX/50B;)V
    .locals 0

    .prologue
    .line 823508
    iput-object p1, p0, LX/50A;->a:LX/50B;

    invoke-direct {p0}, LX/2QI;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<TK;",
            "Ljava/util/Collection",
            "<TV;>;>;"
        }
    .end annotation

    .prologue
    .line 823499
    iget-object v0, p0, LX/50A;->a:LX/50B;

    return-object v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;",
            "Ljava/util/Collection",
            "<TV;>;>;>;"
        }
    .end annotation

    .prologue
    .line 823507
    iget-object v0, p0, LX/50A;->a:LX/50B;

    iget-object v0, v0, LX/50B;->a:LX/0Xu;

    invoke-interface {v0}, LX/0Xu;->p()Ljava/util/Set;

    move-result-object v0

    new-instance v1, LX/509;

    invoke-direct {v1, p0}, LX/509;-><init>(LX/50A;)V

    invoke-static {v0, v1}, LX/0PM;->a(Ljava/util/Set;LX/0QK;)Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 823500
    invoke-virtual {p0, p1}, LX/2QI;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 823501
    const/4 v0, 0x0

    .line 823502
    :goto_0
    return v0

    .line 823503
    :cond_0
    check-cast p1, Ljava/util/Map$Entry;

    .line 823504
    iget-object v0, p0, LX/50A;->a:LX/50B;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    .line 823505
    iget-object p0, v0, LX/50B;->a:LX/0Xu;

    invoke-interface {p0}, LX/0Xu;->p()Ljava/util/Set;

    move-result-object p0

    invoke-interface {p0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 823506
    const/4 v0, 0x1

    goto :goto_0
.end method
