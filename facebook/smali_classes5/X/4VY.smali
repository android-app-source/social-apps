.class public final enum LX/4VY;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/4VY;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/4VY;

.field public static final enum CANCELLED:LX/4VY;

.field public static final enum FAILED:LX/4VY;

.field public static final enum FAILED_PERMANENTLY:LX/4VY;

.field public static final enum FAILED_WILL_RETRY:LX/4VY;

.field public static final enum NOT_STARTED:LX/4VY;

.field public static final enum PAUSED:LX/4VY;

.field public static final enum STARTED:LX/4VY;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 742505
    new-instance v0, LX/4VY;

    const-string v1, "NOT_STARTED"

    invoke-direct {v0, v1, v3}, LX/4VY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4VY;->NOT_STARTED:LX/4VY;

    .line 742506
    new-instance v0, LX/4VY;

    const-string v1, "STARTED"

    invoke-direct {v0, v1, v4}, LX/4VY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4VY;->STARTED:LX/4VY;

    .line 742507
    new-instance v0, LX/4VY;

    const-string v1, "PAUSED"

    invoke-direct {v0, v1, v5}, LX/4VY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4VY;->PAUSED:LX/4VY;

    .line 742508
    new-instance v0, LX/4VY;

    const-string v1, "FAILED"

    invoke-direct {v0, v1, v6}, LX/4VY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4VY;->FAILED:LX/4VY;

    .line 742509
    new-instance v0, LX/4VY;

    const-string v1, "FAILED_PERMANENTLY"

    invoke-direct {v0, v1, v7}, LX/4VY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4VY;->FAILED_PERMANENTLY:LX/4VY;

    .line 742510
    new-instance v0, LX/4VY;

    const-string v1, "FAILED_WILL_RETRY"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/4VY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4VY;->FAILED_WILL_RETRY:LX/4VY;

    .line 742511
    new-instance v0, LX/4VY;

    const-string v1, "CANCELLED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/4VY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4VY;->CANCELLED:LX/4VY;

    .line 742512
    const/4 v0, 0x7

    new-array v0, v0, [LX/4VY;

    sget-object v1, LX/4VY;->NOT_STARTED:LX/4VY;

    aput-object v1, v0, v3

    sget-object v1, LX/4VY;->STARTED:LX/4VY;

    aput-object v1, v0, v4

    sget-object v1, LX/4VY;->PAUSED:LX/4VY;

    aput-object v1, v0, v5

    sget-object v1, LX/4VY;->FAILED:LX/4VY;

    aput-object v1, v0, v6

    sget-object v1, LX/4VY;->FAILED_PERMANENTLY:LX/4VY;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/4VY;->FAILED_WILL_RETRY:LX/4VY;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/4VY;->CANCELLED:LX/4VY;

    aput-object v2, v0, v1

    sput-object v0, LX/4VY;->$VALUES:[LX/4VY;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 742513
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/4VY;
    .locals 1

    .prologue
    .line 742514
    const-class v0, LX/4VY;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/4VY;

    return-object v0
.end method

.method public static values()[LX/4VY;
    .locals 1

    .prologue
    .line 742515
    sget-object v0, LX/4VY;->$VALUES:[LX/4VY;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/4VY;

    return-object v0
.end method
