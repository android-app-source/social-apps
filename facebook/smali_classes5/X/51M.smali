.class public final LX/51M;
.super LX/4wx;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "LX/4wx",
        "<TE;>;"
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:I

.field public c:I

.field public d:J

.field private e:I

.field public f:LX/51M;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/51M",
            "<TE;>;"
        }
    .end annotation
.end field

.field public g:LX/51M;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/51M",
            "<TE;>;"
        }
    .end annotation
.end field

.field public h:LX/51M;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/51M",
            "<TE;>;"
        }
    .end annotation
.end field

.field public i:LX/51M;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/51M",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Object;I)V
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;I)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x1

    .line 824774
    invoke-direct {p0}, LX/4wx;-><init>()V

    .line 824775
    if-lez p2, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 824776
    iput-object p1, p0, LX/51M;->a:Ljava/lang/Object;

    .line 824777
    iput p2, p0, LX/51M;->b:I

    .line 824778
    int-to-long v2, p2

    iput-wide v2, p0, LX/51M;->d:J

    .line 824779
    iput v1, p0, LX/51M;->c:I

    .line 824780
    iput v1, p0, LX/51M;->e:I

    .line 824781
    iput-object v4, p0, LX/51M;->f:LX/51M;

    .line 824782
    iput-object v4, p0, LX/51M;->g:LX/51M;

    .line 824783
    return-void

    .line 824784
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/Object;I)LX/51M;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;I)",
            "LX/51M",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 824785
    new-instance v0, LX/51M;

    invoke-direct {v0, p1, p2}, LX/51M;-><init>(Ljava/lang/Object;I)V

    iput-object v0, p0, LX/51M;->g:LX/51M;

    .line 824786
    iget-object v0, p0, LX/51M;->g:LX/51M;

    iget-object v1, p0, LX/51M;->i:LX/51M;

    invoke-static {p0, v0, v1}, LX/51O;->b(LX/51M;LX/51M;LX/51M;)V

    .line 824787
    const/4 v0, 0x2

    iget v1, p0, LX/51M;->e:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, LX/51M;->e:I

    .line 824788
    iget v0, p0, LX/51M;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/51M;->c:I

    .line 824789
    iget-wide v0, p0, LX/51M;->d:J

    int-to-long v2, p2

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/51M;->d:J

    .line 824790
    return-object p0
.end method

.method private b(Ljava/lang/Object;I)LX/51M;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;I)",
            "LX/51M",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 824791
    new-instance v0, LX/51M;

    invoke-direct {v0, p1, p2}, LX/51M;-><init>(Ljava/lang/Object;I)V

    iput-object v0, p0, LX/51M;->f:LX/51M;

    .line 824792
    iget-object v0, p0, LX/51M;->h:LX/51M;

    iget-object v1, p0, LX/51M;->f:LX/51M;

    invoke-static {v0, v1, p0}, LX/51O;->b(LX/51M;LX/51M;LX/51M;)V

    .line 824793
    const/4 v0, 0x2

    iget v1, p0, LX/51M;->e:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, LX/51M;->e:I

    .line 824794
    iget v0, p0, LX/51M;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/51M;->c:I

    .line 824795
    iget-wide v0, p0, LX/51M;->d:J

    int-to-long v2, p2

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/51M;->d:J

    .line 824796
    return-object p0
.end method

.method public static b$redex0(LX/51M;Ljava/util/Comparator;Ljava/lang/Object;)LX/51M;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Comparator",
            "<-TE;>;TE;)",
            "LX/51M",
            "<TE;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 824797
    iget-object v0, p0, LX/51M;->a:Ljava/lang/Object;

    invoke-interface {p1, p2, v0}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    .line 824798
    if-gez v0, :cond_2

    .line 824799
    iget-object v0, p0, LX/51M;->f:LX/51M;

    if-nez v0, :cond_1

    .line 824800
    :cond_0
    :goto_0
    return-object p0

    .line 824801
    :cond_1
    iget-object v0, p0, LX/51M;->f:LX/51M;

    invoke-static {v0, p1, p2}, LX/51M;->b$redex0(LX/51M;Ljava/util/Comparator;Ljava/lang/Object;)LX/51M;

    move-result-object v0

    invoke-static {v0, p0}, LX/0Qh;->firstNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/51M;

    move-object p0, v0

    goto :goto_0

    .line 824802
    :cond_2
    if-eqz v0, :cond_0

    .line 824803
    iget-object v0, p0, LX/51M;->g:LX/51M;

    if-nez v0, :cond_3

    const/4 p0, 0x0

    goto :goto_0

    :cond_3
    iget-object v0, p0, LX/51M;->g:LX/51M;

    invoke-static {v0, p1, p2}, LX/51M;->b$redex0(LX/51M;Ljava/util/Comparator;Ljava/lang/Object;)LX/51M;

    move-result-object p0

    goto :goto_0
.end method

.method private c()LX/51M;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/51M",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 824804
    iget v0, p0, LX/51M;->b:I

    .line 824805
    const/4 v1, 0x0

    iput v1, p0, LX/51M;->b:I

    .line 824806
    iget-object v1, p0, LX/51M;->h:LX/51M;

    iget-object v2, p0, LX/51M;->i:LX/51M;

    invoke-static {v1, v2}, LX/51O;->b(LX/51M;LX/51M;)V

    .line 824807
    iget-object v1, p0, LX/51M;->f:LX/51M;

    if-nez v1, :cond_0

    .line 824808
    iget-object v0, p0, LX/51M;->g:LX/51M;

    .line 824809
    :goto_0
    return-object v0

    .line 824810
    :cond_0
    iget-object v1, p0, LX/51M;->g:LX/51M;

    if-nez v1, :cond_1

    .line 824811
    iget-object v0, p0, LX/51M;->f:LX/51M;

    goto :goto_0

    .line 824812
    :cond_1
    iget-object v1, p0, LX/51M;->f:LX/51M;

    iget v1, v1, LX/51M;->e:I

    iget-object v2, p0, LX/51M;->g:LX/51M;

    iget v2, v2, LX/51M;->e:I

    if-lt v1, v2, :cond_2

    .line 824813
    iget-object v1, p0, LX/51M;->h:LX/51M;

    .line 824814
    iget-object v2, p0, LX/51M;->f:LX/51M;

    invoke-direct {v2, v1}, LX/51M;->j(LX/51M;)LX/51M;

    move-result-object v2

    iput-object v2, v1, LX/51M;->f:LX/51M;

    .line 824815
    iget-object v2, p0, LX/51M;->g:LX/51M;

    iput-object v2, v1, LX/51M;->g:LX/51M;

    .line 824816
    iget v2, p0, LX/51M;->c:I

    add-int/lit8 v2, v2, -0x1

    iput v2, v1, LX/51M;->c:I

    .line 824817
    iget-wide v2, p0, LX/51M;->d:J

    int-to-long v4, v0

    sub-long/2addr v2, v4

    iput-wide v2, v1, LX/51M;->d:J

    .line 824818
    invoke-direct {v1}, LX/51M;->g()LX/51M;

    move-result-object v0

    goto :goto_0

    .line 824819
    :cond_2
    iget-object v1, p0, LX/51M;->i:LX/51M;

    .line 824820
    iget-object v2, p0, LX/51M;->g:LX/51M;

    invoke-direct {v2, v1}, LX/51M;->i(LX/51M;)LX/51M;

    move-result-object v2

    iput-object v2, v1, LX/51M;->g:LX/51M;

    .line 824821
    iget-object v2, p0, LX/51M;->f:LX/51M;

    iput-object v2, v1, LX/51M;->f:LX/51M;

    .line 824822
    iget v2, p0, LX/51M;->c:I

    add-int/lit8 v2, v2, -0x1

    iput v2, v1, LX/51M;->c:I

    .line 824823
    iget-wide v2, p0, LX/51M;->d:J

    int-to-long v4, v0

    sub-long/2addr v2, v4

    iput-wide v2, v1, LX/51M;->d:J

    .line 824824
    invoke-direct {v1}, LX/51M;->g()LX/51M;

    move-result-object v0

    goto :goto_0
.end method

.method public static c(LX/51M;Ljava/util/Comparator;Ljava/lang/Object;)LX/51M;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Comparator",
            "<-TE;>;TE;)",
            "LX/51M",
            "<TE;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 824825
    iget-object v0, p0, LX/51M;->a:Ljava/lang/Object;

    invoke-interface {p1, p2, v0}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    .line 824826
    if-lez v0, :cond_2

    .line 824827
    iget-object v0, p0, LX/51M;->g:LX/51M;

    if-nez v0, :cond_1

    .line 824828
    :cond_0
    :goto_0
    return-object p0

    .line 824829
    :cond_1
    iget-object v0, p0, LX/51M;->g:LX/51M;

    invoke-static {v0, p1, p2}, LX/51M;->c(LX/51M;Ljava/util/Comparator;Ljava/lang/Object;)LX/51M;

    move-result-object v0

    invoke-static {v0, p0}, LX/0Qh;->firstNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/51M;

    move-object p0, v0

    goto :goto_0

    .line 824830
    :cond_2
    if-eqz v0, :cond_0

    .line 824831
    iget-object v0, p0, LX/51M;->f:LX/51M;

    if-nez v0, :cond_3

    const/4 p0, 0x0

    goto :goto_0

    :cond_3
    iget-object v0, p0, LX/51M;->f:LX/51M;

    invoke-static {v0, p1, p2}, LX/51M;->c(LX/51M;Ljava/util/Comparator;Ljava/lang/Object;)LX/51M;

    move-result-object p0

    goto :goto_0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 824832
    iget-object v0, p0, LX/51M;->f:LX/51M;

    invoke-static {v0}, LX/51M;->l(LX/51M;)I

    move-result v0

    iget-object v1, p0, LX/51M;->g:LX/51M;

    invoke-static {v1}, LX/51M;->l(LX/51M;)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/51M;->e:I

    .line 824833
    return-void
.end method

.method private f()V
    .locals 4

    .prologue
    .line 824841
    iget-object v0, p0, LX/51M;->f:LX/51M;

    invoke-static {v0}, LX/51O;->a(LX/51M;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, LX/51M;->g:LX/51M;

    invoke-static {v1}, LX/51O;->a(LX/51M;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, LX/51M;->c:I

    .line 824842
    iget v0, p0, LX/51M;->b:I

    int-to-long v0, v0

    iget-object v2, p0, LX/51M;->f:LX/51M;

    invoke-static {v2}, LX/51M;->k(LX/51M;)J

    move-result-wide v2

    add-long/2addr v0, v2

    iget-object v2, p0, LX/51M;->g:LX/51M;

    invoke-static {v2}, LX/51M;->k(LX/51M;)J

    move-result-wide v2

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/51M;->d:J

    .line 824843
    invoke-direct {p0}, LX/51M;->e()V

    .line 824844
    return-void
.end method

.method private g()LX/51M;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/51M",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 824845
    invoke-direct {p0}, LX/51M;->h()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 824846
    invoke-direct {p0}, LX/51M;->e()V

    .line 824847
    :goto_0
    return-object p0

    .line 824848
    :sswitch_0
    iget-object v0, p0, LX/51M;->g:LX/51M;

    invoke-direct {v0}, LX/51M;->h()I

    move-result v0

    if-lez v0, :cond_0

    .line 824849
    iget-object v0, p0, LX/51M;->g:LX/51M;

    invoke-direct {v0}, LX/51M;->j()LX/51M;

    move-result-object v0

    iput-object v0, p0, LX/51M;->g:LX/51M;

    .line 824850
    :cond_0
    invoke-direct {p0}, LX/51M;->i()LX/51M;

    move-result-object p0

    goto :goto_0

    .line 824851
    :sswitch_1
    iget-object v0, p0, LX/51M;->f:LX/51M;

    invoke-direct {v0}, LX/51M;->h()I

    move-result v0

    if-gez v0, :cond_1

    .line 824852
    iget-object v0, p0, LX/51M;->f:LX/51M;

    invoke-direct {v0}, LX/51M;->i()LX/51M;

    move-result-object v0

    iput-object v0, p0, LX/51M;->f:LX/51M;

    .line 824853
    :cond_1
    invoke-direct {p0}, LX/51M;->j()LX/51M;

    move-result-object p0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x2 -> :sswitch_0
        0x2 -> :sswitch_1
    .end sparse-switch
.end method

.method private h()I
    .locals 2

    .prologue
    .line 824854
    iget-object v0, p0, LX/51M;->f:LX/51M;

    invoke-static {v0}, LX/51M;->l(LX/51M;)I

    move-result v0

    iget-object v1, p0, LX/51M;->g:LX/51M;

    invoke-static {v1}, LX/51M;->l(LX/51M;)I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method private i()LX/51M;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/51M",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 824764
    iget-object v0, p0, LX/51M;->g:LX/51M;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 824765
    iget-object v0, p0, LX/51M;->g:LX/51M;

    .line 824766
    iget-object v1, v0, LX/51M;->f:LX/51M;

    iput-object v1, p0, LX/51M;->g:LX/51M;

    .line 824767
    iput-object p0, v0, LX/51M;->f:LX/51M;

    .line 824768
    iget-wide v2, p0, LX/51M;->d:J

    iput-wide v2, v0, LX/51M;->d:J

    .line 824769
    iget v1, p0, LX/51M;->c:I

    iput v1, v0, LX/51M;->c:I

    .line 824770
    invoke-direct {p0}, LX/51M;->f()V

    .line 824771
    invoke-direct {v0}, LX/51M;->e()V

    .line 824772
    return-object v0

    .line 824773
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private i(LX/51M;)LX/51M;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/51M",
            "<TE;>;)",
            "LX/51M",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 824834
    iget-object v0, p0, LX/51M;->f:LX/51M;

    if-nez v0, :cond_0

    .line 824835
    iget-object v0, p0, LX/51M;->g:LX/51M;

    .line 824836
    :goto_0
    return-object v0

    .line 824837
    :cond_0
    iget-object v0, p0, LX/51M;->f:LX/51M;

    invoke-direct {v0, p1}, LX/51M;->i(LX/51M;)LX/51M;

    move-result-object v0

    iput-object v0, p0, LX/51M;->f:LX/51M;

    .line 824838
    iget v0, p0, LX/51M;->c:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/51M;->c:I

    .line 824839
    iget-wide v0, p0, LX/51M;->d:J

    iget v2, p1, LX/51M;->b:I

    int-to-long v2, v2

    sub-long/2addr v0, v2

    iput-wide v0, p0, LX/51M;->d:J

    .line 824840
    invoke-direct {p0}, LX/51M;->g()LX/51M;

    move-result-object v0

    goto :goto_0
.end method

.method private j()LX/51M;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/51M",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 824609
    iget-object v0, p0, LX/51M;->f:LX/51M;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 824610
    iget-object v0, p0, LX/51M;->f:LX/51M;

    .line 824611
    iget-object v1, v0, LX/51M;->g:LX/51M;

    iput-object v1, p0, LX/51M;->f:LX/51M;

    .line 824612
    iput-object p0, v0, LX/51M;->g:LX/51M;

    .line 824613
    iget-wide v2, p0, LX/51M;->d:J

    iput-wide v2, v0, LX/51M;->d:J

    .line 824614
    iget v1, p0, LX/51M;->c:I

    iput v1, v0, LX/51M;->c:I

    .line 824615
    invoke-direct {p0}, LX/51M;->f()V

    .line 824616
    invoke-direct {v0}, LX/51M;->e()V

    .line 824617
    return-object v0

    .line 824618
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private j(LX/51M;)LX/51M;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/51M",
            "<TE;>;)",
            "LX/51M",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 824655
    iget-object v0, p0, LX/51M;->g:LX/51M;

    if-nez v0, :cond_0

    .line 824656
    iget-object v0, p0, LX/51M;->f:LX/51M;

    .line 824657
    :goto_0
    return-object v0

    .line 824658
    :cond_0
    iget-object v0, p0, LX/51M;->g:LX/51M;

    invoke-direct {v0, p1}, LX/51M;->j(LX/51M;)LX/51M;

    move-result-object v0

    iput-object v0, p0, LX/51M;->g:LX/51M;

    .line 824659
    iget v0, p0, LX/51M;->c:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/51M;->c:I

    .line 824660
    iget-wide v0, p0, LX/51M;->d:J

    iget v2, p1, LX/51M;->b:I

    int-to-long v2, v2

    sub-long/2addr v0, v2

    iput-wide v0, p0, LX/51M;->d:J

    .line 824661
    invoke-direct {p0}, LX/51M;->g()LX/51M;

    move-result-object v0

    goto :goto_0
.end method

.method public static k(LX/51M;)J
    .locals 2
    .param p0    # LX/51M;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/51M",
            "<*>;)J"
        }
    .end annotation

    .prologue
    .line 824662
    if-nez p0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, LX/51M;->d:J

    goto :goto_0
.end method

.method private static l(LX/51M;)I
    .locals 1
    .param p0    # LX/51M;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/51M",
            "<*>;)I"
        }
    .end annotation

    .prologue
    .line 824663
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, LX/51M;->e:I

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/util/Comparator;Ljava/lang/Object;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Comparator",
            "<-TE;>;TE;)I"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 824664
    iget-object v1, p0, LX/51M;->a:Ljava/lang/Object;

    invoke-interface {p1, p2, v1}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v1

    .line 824665
    if-gez v1, :cond_2

    .line 824666
    iget-object v1, p0, LX/51M;->f:LX/51M;

    if-nez v1, :cond_1

    .line 824667
    :cond_0
    :goto_0
    return v0

    .line 824668
    :cond_1
    iget-object v0, p0, LX/51M;->f:LX/51M;

    invoke-virtual {v0, p1, p2}, LX/51M;->a(Ljava/util/Comparator;Ljava/lang/Object;)I

    move-result v0

    goto :goto_0

    .line 824669
    :cond_2
    if-lez v1, :cond_3

    .line 824670
    iget-object v1, p0, LX/51M;->g:LX/51M;

    if-eqz v1, :cond_0

    iget-object v0, p0, LX/51M;->g:LX/51M;

    invoke-virtual {v0, p1, p2}, LX/51M;->a(Ljava/util/Comparator;Ljava/lang/Object;)I

    move-result v0

    goto :goto_0

    .line 824671
    :cond_3
    iget v0, p0, LX/51M;->b:I

    goto :goto_0
.end method

.method public final a(Ljava/util/Comparator;Ljava/lang/Object;II[I)LX/51M;
    .locals 7
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Comparator",
            "<-TE;>;TE;II[I)",
            "LX/51M",
            "<TE;>;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 824619
    iget-object v0, p0, LX/51M;->a:Ljava/lang/Object;

    invoke-interface {p1, p2, v0}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    .line 824620
    if-gez v0, :cond_5

    .line 824621
    iget-object v0, p0, LX/51M;->f:LX/51M;

    .line 824622
    if-nez v0, :cond_1

    .line 824623
    aput v6, p5, v6

    .line 824624
    if-nez p3, :cond_0

    if-lez p4, :cond_0

    .line 824625
    invoke-direct {p0, p2, p4}, LX/51M;->b(Ljava/lang/Object;I)LX/51M;

    move-result-object p0

    .line 824626
    :cond_0
    :goto_0
    return-object p0

    :cond_1
    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    .line 824627
    invoke-virtual/range {v0 .. v5}, LX/51M;->a(Ljava/util/Comparator;Ljava/lang/Object;II[I)LX/51M;

    move-result-object v0

    iput-object v0, p0, LX/51M;->f:LX/51M;

    .line 824628
    aget v0, p5, v6

    if-ne v0, p3, :cond_3

    .line 824629
    if-nez p4, :cond_4

    aget v0, p5, v6

    if-eqz v0, :cond_4

    .line 824630
    iget v0, p0, LX/51M;->c:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/51M;->c:I

    .line 824631
    :cond_2
    :goto_1
    iget-wide v0, p0, LX/51M;->d:J

    aget v2, p5, v6

    sub-int v2, p4, v2

    int-to-long v2, v2

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/51M;->d:J

    .line 824632
    :cond_3
    invoke-direct {p0}, LX/51M;->g()LX/51M;

    move-result-object p0

    goto :goto_0

    .line 824633
    :cond_4
    if-lez p4, :cond_2

    aget v0, p5, v6

    if-nez v0, :cond_2

    .line 824634
    iget v0, p0, LX/51M;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/51M;->c:I

    goto :goto_1

    .line 824635
    :cond_5
    if-lez v0, :cond_a

    .line 824636
    iget-object v0, p0, LX/51M;->g:LX/51M;

    .line 824637
    if-nez v0, :cond_6

    .line 824638
    aput v6, p5, v6

    .line 824639
    if-nez p3, :cond_0

    if-lez p4, :cond_0

    .line 824640
    invoke-direct {p0, p2, p4}, LX/51M;->a(Ljava/lang/Object;I)LX/51M;

    move-result-object p0

    goto :goto_0

    :cond_6
    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    .line 824641
    invoke-virtual/range {v0 .. v5}, LX/51M;->a(Ljava/util/Comparator;Ljava/lang/Object;II[I)LX/51M;

    move-result-object v0

    iput-object v0, p0, LX/51M;->g:LX/51M;

    .line 824642
    aget v0, p5, v6

    if-ne v0, p3, :cond_8

    .line 824643
    if-nez p4, :cond_9

    aget v0, p5, v6

    if-eqz v0, :cond_9

    .line 824644
    iget v0, p0, LX/51M;->c:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/51M;->c:I

    .line 824645
    :cond_7
    :goto_2
    iget-wide v0, p0, LX/51M;->d:J

    aget v2, p5, v6

    sub-int v2, p4, v2

    int-to-long v2, v2

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/51M;->d:J

    .line 824646
    :cond_8
    invoke-direct {p0}, LX/51M;->g()LX/51M;

    move-result-object p0

    goto :goto_0

    .line 824647
    :cond_9
    if-lez p4, :cond_7

    aget v0, p5, v6

    if-nez v0, :cond_7

    .line 824648
    iget v0, p0, LX/51M;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/51M;->c:I

    goto :goto_2

    .line 824649
    :cond_a
    iget v0, p0, LX/51M;->b:I

    aput v0, p5, v6

    .line 824650
    iget v0, p0, LX/51M;->b:I

    if-ne p3, v0, :cond_0

    .line 824651
    if-nez p4, :cond_b

    .line 824652
    invoke-direct {p0}, LX/51M;->c()LX/51M;

    move-result-object p0

    goto/16 :goto_0

    .line 824653
    :cond_b
    iget-wide v0, p0, LX/51M;->d:J

    iget v2, p0, LX/51M;->b:I

    sub-int v2, p4, v2

    int-to-long v2, v2

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/51M;->d:J

    .line 824654
    iput p4, p0, LX/51M;->b:I

    goto/16 :goto_0
.end method

.method public final a(Ljava/util/Comparator;Ljava/lang/Object;I[I)LX/51M;
    .locals 6
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Comparator",
            "<-TE;>;TE;I[I)",
            "LX/51M",
            "<TE;>;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 824672
    iget-object v1, p0, LX/51M;->a:Ljava/lang/Object;

    invoke-interface {p1, p2, v1}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v1

    .line 824673
    if-gez v1, :cond_3

    .line 824674
    iget-object v1, p0, LX/51M;->f:LX/51M;

    .line 824675
    if-nez v1, :cond_1

    .line 824676
    aput v0, p4, v0

    .line 824677
    invoke-direct {p0, p2, p3}, LX/51M;->b(Ljava/lang/Object;I)LX/51M;

    move-result-object p0

    .line 824678
    :cond_0
    :goto_0
    return-object p0

    .line 824679
    :cond_1
    iget v2, v1, LX/51M;->e:I

    .line 824680
    invoke-virtual {v1, p1, p2, p3, p4}, LX/51M;->a(Ljava/util/Comparator;Ljava/lang/Object;I[I)LX/51M;

    move-result-object v1

    iput-object v1, p0, LX/51M;->f:LX/51M;

    .line 824681
    aget v0, p4, v0

    if-nez v0, :cond_2

    .line 824682
    iget v0, p0, LX/51M;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/51M;->c:I

    .line 824683
    :cond_2
    iget-wide v0, p0, LX/51M;->d:J

    int-to-long v4, p3

    add-long/2addr v0, v4

    iput-wide v0, p0, LX/51M;->d:J

    .line 824684
    iget-object v0, p0, LX/51M;->f:LX/51M;

    iget v0, v0, LX/51M;->e:I

    if-eq v0, v2, :cond_0

    invoke-direct {p0}, LX/51M;->g()LX/51M;

    move-result-object p0

    goto :goto_0

    .line 824685
    :cond_3
    if-lez v1, :cond_6

    .line 824686
    iget-object v1, p0, LX/51M;->g:LX/51M;

    .line 824687
    if-nez v1, :cond_4

    .line 824688
    aput v0, p4, v0

    .line 824689
    invoke-direct {p0, p2, p3}, LX/51M;->a(Ljava/lang/Object;I)LX/51M;

    move-result-object p0

    goto :goto_0

    .line 824690
    :cond_4
    iget v2, v1, LX/51M;->e:I

    .line 824691
    invoke-virtual {v1, p1, p2, p3, p4}, LX/51M;->a(Ljava/util/Comparator;Ljava/lang/Object;I[I)LX/51M;

    move-result-object v1

    iput-object v1, p0, LX/51M;->g:LX/51M;

    .line 824692
    aget v0, p4, v0

    if-nez v0, :cond_5

    .line 824693
    iget v0, p0, LX/51M;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/51M;->c:I

    .line 824694
    :cond_5
    iget-wide v0, p0, LX/51M;->d:J

    int-to-long v4, p3

    add-long/2addr v0, v4

    iput-wide v0, p0, LX/51M;->d:J

    .line 824695
    iget-object v0, p0, LX/51M;->g:LX/51M;

    iget v0, v0, LX/51M;->e:I

    if-eq v0, v2, :cond_0

    invoke-direct {p0}, LX/51M;->g()LX/51M;

    move-result-object p0

    goto :goto_0

    .line 824696
    :cond_6
    iget v1, p0, LX/51M;->b:I

    aput v1, p4, v0

    .line 824697
    iget v1, p0, LX/51M;->b:I

    int-to-long v2, v1

    int-to-long v4, p3

    add-long/2addr v2, v4

    .line 824698
    const-wide/32 v4, 0x7fffffff

    cmp-long v1, v2, v4

    if-gtz v1, :cond_7

    const/4 v0, 0x1

    :cond_7
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 824699
    iget v0, p0, LX/51M;->b:I

    add-int/2addr v0, p3

    iput v0, p0, LX/51M;->b:I

    .line 824700
    iget-wide v0, p0, LX/51M;->d:J

    int-to-long v2, p3

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/51M;->d:J

    goto :goto_0
.end method

.method public final a()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 824701
    iget-object v0, p0, LX/51M;->a:Ljava/lang/Object;

    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 824702
    iget v0, p0, LX/51M;->b:I

    return v0
.end method

.method public final b(Ljava/util/Comparator;Ljava/lang/Object;I[I)LX/51M;
    .locals 5
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Comparator",
            "<-TE;>;TE;I[I)",
            "LX/51M",
            "<TE;>;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 824703
    iget-object v0, p0, LX/51M;->a:Ljava/lang/Object;

    invoke-interface {p1, p2, v0}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    .line 824704
    if-gez v0, :cond_4

    .line 824705
    iget-object v0, p0, LX/51M;->f:LX/51M;

    .line 824706
    if-nez v0, :cond_1

    .line 824707
    aput v4, p4, v4

    .line 824708
    :cond_0
    :goto_0
    return-object p0

    .line 824709
    :cond_1
    invoke-virtual {v0, p1, p2, p3, p4}, LX/51M;->b(Ljava/util/Comparator;Ljava/lang/Object;I[I)LX/51M;

    move-result-object v0

    iput-object v0, p0, LX/51M;->f:LX/51M;

    .line 824710
    aget v0, p4, v4

    if-lez v0, :cond_2

    .line 824711
    aget v0, p4, v4

    if-lt p3, v0, :cond_3

    .line 824712
    iget v0, p0, LX/51M;->c:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/51M;->c:I

    .line 824713
    iget-wide v0, p0, LX/51M;->d:J

    aget v2, p4, v4

    int-to-long v2, v2

    sub-long/2addr v0, v2

    iput-wide v0, p0, LX/51M;->d:J

    .line 824714
    :cond_2
    :goto_1
    aget v0, p4, v4

    if-eqz v0, :cond_0

    invoke-direct {p0}, LX/51M;->g()LX/51M;

    move-result-object p0

    goto :goto_0

    .line 824715
    :cond_3
    iget-wide v0, p0, LX/51M;->d:J

    int-to-long v2, p3

    sub-long/2addr v0, v2

    iput-wide v0, p0, LX/51M;->d:J

    goto :goto_1

    .line 824716
    :cond_4
    if-lez v0, :cond_8

    .line 824717
    iget-object v0, p0, LX/51M;->g:LX/51M;

    .line 824718
    if-nez v0, :cond_5

    .line 824719
    aput v4, p4, v4

    goto :goto_0

    .line 824720
    :cond_5
    invoke-virtual {v0, p1, p2, p3, p4}, LX/51M;->b(Ljava/util/Comparator;Ljava/lang/Object;I[I)LX/51M;

    move-result-object v0

    iput-object v0, p0, LX/51M;->g:LX/51M;

    .line 824721
    aget v0, p4, v4

    if-lez v0, :cond_6

    .line 824722
    aget v0, p4, v4

    if-lt p3, v0, :cond_7

    .line 824723
    iget v0, p0, LX/51M;->c:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/51M;->c:I

    .line 824724
    iget-wide v0, p0, LX/51M;->d:J

    aget v2, p4, v4

    int-to-long v2, v2

    sub-long/2addr v0, v2

    iput-wide v0, p0, LX/51M;->d:J

    .line 824725
    :cond_6
    :goto_2
    invoke-direct {p0}, LX/51M;->g()LX/51M;

    move-result-object p0

    goto :goto_0

    .line 824726
    :cond_7
    iget-wide v0, p0, LX/51M;->d:J

    int-to-long v2, p3

    sub-long/2addr v0, v2

    iput-wide v0, p0, LX/51M;->d:J

    goto :goto_2

    .line 824727
    :cond_8
    iget v0, p0, LX/51M;->b:I

    aput v0, p4, v4

    .line 824728
    iget v0, p0, LX/51M;->b:I

    if-lt p3, v0, :cond_9

    .line 824729
    invoke-direct {p0}, LX/51M;->c()LX/51M;

    move-result-object p0

    goto :goto_0

    .line 824730
    :cond_9
    iget v0, p0, LX/51M;->b:I

    sub-int/2addr v0, p3

    iput v0, p0, LX/51M;->b:I

    .line 824731
    iget-wide v0, p0, LX/51M;->d:J

    int-to-long v2, p3

    sub-long/2addr v0, v2

    iput-wide v0, p0, LX/51M;->d:J

    goto :goto_0
.end method

.method public final c(Ljava/util/Comparator;Ljava/lang/Object;I[I)LX/51M;
    .locals 4
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Comparator",
            "<-TE;>;TE;I[I)",
            "LX/51M",
            "<TE;>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 824732
    iget-object v0, p0, LX/51M;->a:Ljava/lang/Object;

    invoke-interface {p1, p2, v0}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    .line 824733
    if-gez v0, :cond_4

    .line 824734
    iget-object v0, p0, LX/51M;->f:LX/51M;

    .line 824735
    if-nez v0, :cond_1

    .line 824736
    aput v2, p4, v2

    .line 824737
    if-lez p3, :cond_0

    invoke-direct {p0, p2, p3}, LX/51M;->b(Ljava/lang/Object;I)LX/51M;

    move-result-object p0

    .line 824738
    :cond_0
    :goto_0
    return-object p0

    .line 824739
    :cond_1
    invoke-virtual {v0, p1, p2, p3, p4}, LX/51M;->c(Ljava/util/Comparator;Ljava/lang/Object;I[I)LX/51M;

    move-result-object v0

    iput-object v0, p0, LX/51M;->f:LX/51M;

    .line 824740
    if-nez p3, :cond_3

    aget v0, p4, v2

    if-eqz v0, :cond_3

    .line 824741
    iget v0, p0, LX/51M;->c:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/51M;->c:I

    .line 824742
    :cond_2
    :goto_1
    iget-wide v0, p0, LX/51M;->d:J

    aget v2, p4, v2

    sub-int v2, p3, v2

    int-to-long v2, v2

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/51M;->d:J

    .line 824743
    invoke-direct {p0}, LX/51M;->g()LX/51M;

    move-result-object p0

    goto :goto_0

    .line 824744
    :cond_3
    if-lez p3, :cond_2

    aget v0, p4, v2

    if-nez v0, :cond_2

    .line 824745
    iget v0, p0, LX/51M;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/51M;->c:I

    goto :goto_1

    .line 824746
    :cond_4
    if-lez v0, :cond_8

    .line 824747
    iget-object v0, p0, LX/51M;->g:LX/51M;

    .line 824748
    if-nez v0, :cond_5

    .line 824749
    aput v2, p4, v2

    .line 824750
    if-lez p3, :cond_0

    invoke-direct {p0, p2, p3}, LX/51M;->a(Ljava/lang/Object;I)LX/51M;

    move-result-object p0

    goto :goto_0

    .line 824751
    :cond_5
    invoke-virtual {v0, p1, p2, p3, p4}, LX/51M;->c(Ljava/util/Comparator;Ljava/lang/Object;I[I)LX/51M;

    move-result-object v0

    iput-object v0, p0, LX/51M;->g:LX/51M;

    .line 824752
    if-nez p3, :cond_7

    aget v0, p4, v2

    if-eqz v0, :cond_7

    .line 824753
    iget v0, p0, LX/51M;->c:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/51M;->c:I

    .line 824754
    :cond_6
    :goto_2
    iget-wide v0, p0, LX/51M;->d:J

    aget v2, p4, v2

    sub-int v2, p3, v2

    int-to-long v2, v2

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/51M;->d:J

    .line 824755
    invoke-direct {p0}, LX/51M;->g()LX/51M;

    move-result-object p0

    goto :goto_0

    .line 824756
    :cond_7
    if-lez p3, :cond_6

    aget v0, p4, v2

    if-nez v0, :cond_6

    .line 824757
    iget v0, p0, LX/51M;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/51M;->c:I

    goto :goto_2

    .line 824758
    :cond_8
    iget v0, p0, LX/51M;->b:I

    aput v0, p4, v2

    .line 824759
    if-nez p3, :cond_9

    .line 824760
    invoke-direct {p0}, LX/51M;->c()LX/51M;

    move-result-object p0

    goto :goto_0

    .line 824761
    :cond_9
    iget-wide v0, p0, LX/51M;->d:J

    iget v2, p0, LX/51M;->b:I

    sub-int v2, p3, v2

    int-to-long v2, v2

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/51M;->d:J

    .line 824762
    iput p3, p0, LX/51M;->b:I

    goto/16 :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 824763
    invoke-virtual {p0}, LX/51M;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0}, LX/51M;->b()I

    move-result v1

    invoke-static {v0, v1}, LX/2Tc;->a(Ljava/lang/Object;I)LX/4wx;

    move-result-object v0

    invoke-virtual {v0}, LX/4wx;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
