.class public final enum LX/4fo;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/4fo;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/4fo;

.field public static final enum INJECT_COMPONENT:LX/4fo;

.field public static final enum INSTANCE_GET:LX/4fo;

.field public static final enum PROVIDER_GET:LX/4fo;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 798713
    new-instance v0, LX/4fo;

    const-string v1, "PROVIDER_GET"

    invoke-direct {v0, v1, v2}, LX/4fo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4fo;->PROVIDER_GET:LX/4fo;

    .line 798714
    new-instance v0, LX/4fo;

    const-string v1, "INSTANCE_GET"

    invoke-direct {v0, v1, v3}, LX/4fo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4fo;->INSTANCE_GET:LX/4fo;

    .line 798715
    new-instance v0, LX/4fo;

    const-string v1, "INJECT_COMPONENT"

    invoke-direct {v0, v1, v4}, LX/4fo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4fo;->INJECT_COMPONENT:LX/4fo;

    .line 798716
    const/4 v0, 0x3

    new-array v0, v0, [LX/4fo;

    sget-object v1, LX/4fo;->PROVIDER_GET:LX/4fo;

    aput-object v1, v0, v2

    sget-object v1, LX/4fo;->INSTANCE_GET:LX/4fo;

    aput-object v1, v0, v3

    sget-object v1, LX/4fo;->INJECT_COMPONENT:LX/4fo;

    aput-object v1, v0, v4

    sput-object v0, LX/4fo;->$VALUES:[LX/4fo;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 798718
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/4fo;
    .locals 1

    .prologue
    .line 798719
    const-class v0, LX/4fo;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/4fo;

    return-object v0
.end method

.method public static values()[LX/4fo;
    .locals 1

    .prologue
    .line 798717
    sget-object v0, LX/4fo;->$VALUES:[LX/4fo;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/4fo;

    return-object v0
.end method
