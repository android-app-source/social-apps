.class public LX/4fS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/0Or",
        "<TT;>;"
    }
.end annotation


# static fields
.field private static final a:[B


# instance fields
.field private final b:LX/0RB;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<TT;>;"
        }
    .end annotation
.end field

.field private d:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 798423
    new-array v0, v2, [B

    const/4 v1, 0x0

    aput-byte v2, v0, v1

    sput-object v0, LX/4fS;->a:[B

    return-void
.end method

.method public constructor <init>(LX/0RB;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0RB;",
            "LX/0Or",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 798424
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 798425
    iput-object p1, p0, LX/4fS;->b:LX/0RB;

    .line 798426
    iput-object p2, p0, LX/4fS;->c:LX/0Or;

    .line 798427
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    const/16 v4, 0x8

    .line 798428
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v1

    .line 798429
    iget-object v0, p0, LX/4fS;->b:LX/0RB;

    invoke-virtual {v0}, LX/0RB;->getInjectorThreadStack()LX/0S7;

    move-result-object v2

    .line 798430
    invoke-virtual {v2}, LX/0S7;->d()Landroid/content/Context;

    move-result-object v3

    .line 798431
    if-nez v3, :cond_0

    .line 798432
    new-instance v0, LX/4fr;

    const-string v1, "Called context scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 798433
    :cond_0
    instance-of v0, v3, Landroid/app/Application;

    if-nez v0, :cond_1

    .line 798434
    sget-object v0, LX/4fS;->a:[B

    invoke-virtual {v1, v4, v0}, LX/0SD;->a(B[B)V

    .line 798435
    :cond_1
    invoke-virtual {v1, v4}, LX/0SD;->b(B)B

    move-result v4

    .line 798436
    :try_start_0
    invoke-static {v3}, LX/0RB;->a(Landroid/content/Context;)LX/0Xn;

    move-result-object v5

    .line 798437
    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 798438
    if-eqz v5, :cond_3

    .line 798439
    :try_start_1
    invoke-interface {v5, p0}, LX/0Xn;->getProperty(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 798440
    :goto_0
    if-nez v0, :cond_2

    .line 798441
    iget-object v0, p0, LX/4fS;->b:LX/0RB;

    invoke-virtual {v0, v3, v2}, LX/0RB;->a(Landroid/content/Context;LX/0S7;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 798442
    :try_start_2
    iget-object v0, p0, LX/4fS;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    .line 798443
    :try_start_3
    invoke-virtual {v2}, LX/0S7;->c()V

    .line 798444
    if-eqz v5, :cond_4

    .line 798445
    invoke-interface {v5, p0, v0}, LX/0Xn;->setProperty(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 798446
    :cond_2
    :goto_1
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 798447
    iput-byte v4, v1, LX/0SD;->a:B

    .line 798448
    return-object v0

    .line 798449
    :cond_3
    :try_start_4
    iget-object v0, p0, LX/4fS;->d:Ljava/lang/Object;

    goto :goto_0

    .line 798450
    :catchall_0
    move-exception v0

    .line 798451
    invoke-virtual {v2}, LX/0S7;->c()V

    .line 798452
    throw v0

    .line 798453
    :catchall_1
    move-exception v0

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 798454
    :catchall_2
    move-exception v0

    .line 798455
    iput-byte v4, v1, LX/0SD;->a:B

    .line 798456
    throw v0

    .line 798457
    :cond_4
    :try_start_6
    iput-object v0, p0, LX/4fS;->d:Ljava/lang/Object;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_1
.end method
