.class public final LX/4mx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Ve;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/0Ve",
        "<TT;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1Ck;

.field private b:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TKey;"
        }
    .end annotation
.end field

.field private c:LX/0Ve;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ve",
            "<TT;>;"
        }
    .end annotation
.end field

.field private d:LX/1Cl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cl",
            "<TT;>;"
        }
    .end annotation
.end field

.field public e:LX/4mx;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<TKey;>.OrderedDisposableFutureCallback<TT;>;"
        }
    .end annotation
.end field

.field public f:LX/4mx;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<TKey;>.OrderedDisposableFutureCallback<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1Ck;Ljava/lang/Object;LX/0Ve;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TKey;",
            "LX/0Ve",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 806449
    iput-object p1, p0, LX/4mx;->a:LX/1Ck;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 806450
    iput-object p3, p0, LX/4mx;->c:LX/0Ve;

    .line 806451
    iput-object p2, p0, LX/4mx;->b:Ljava/lang/Object;

    .line 806452
    iput-object v0, p0, LX/4mx;->e:LX/4mx;

    .line 806453
    iput-object v0, p0, LX/4mx;->f:LX/4mx;

    .line 806454
    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 806427
    iget-object v0, p0, LX/4mx;->d:LX/1Cl;

    if-nez v0, :cond_1

    .line 806428
    :cond_0
    :goto_0
    return-void

    .line 806429
    :cond_1
    iget-object v0, p0, LX/4mx;->e:LX/4mx;

    if-nez v0, :cond_0

    .line 806430
    iget-object v0, p0, LX/4mx;->a:LX/1Ck;

    iget-object v1, p0, LX/4mx;->b:Ljava/lang/Object;

    invoke-static {v0, v1, p0}, LX/1Ck;->a$redex0(LX/1Ck;Ljava/lang/Object;LX/0Ve;)V

    .line 806431
    sget-object v0, LX/4mv;->a:[I

    iget-object v1, p0, LX/4mx;->d:LX/1Cl;

    .line 806432
    iget-object v3, v1, LX/1Cl;->c:LX/1Cm;

    move-object v1, v3

    .line 806433
    invoke-virtual {v1}, LX/1Cm;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 806434
    :cond_2
    :goto_1
    iget-object v0, p0, LX/4mx;->f:LX/4mx;

    if-eqz v0, :cond_0

    .line 806435
    iget-object v0, p0, LX/4mx;->f:LX/4mx;

    .line 806436
    iput-object v2, p0, LX/4mx;->f:LX/4mx;

    .line 806437
    iput-object v2, v0, LX/4mx;->e:LX/4mx;

    .line 806438
    invoke-direct {v0}, LX/4mx;->a()V

    goto :goto_0

    .line 806439
    :pswitch_0
    iget-object v0, p0, LX/4mx;->c:LX/0Ve;

    if-eqz v0, :cond_2

    .line 806440
    iget-object v0, p0, LX/4mx;->c:LX/0Ve;

    iget-object v1, p0, LX/4mx;->d:LX/1Cl;

    .line 806441
    iget-object v3, v1, LX/1Cl;->a:Ljava/lang/Object;

    move-object v1, v3

    .line 806442
    invoke-interface {v0, v1}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    goto :goto_1

    .line 806443
    :pswitch_1
    iget-object v0, p0, LX/4mx;->c:LX/0Ve;

    if-eqz v0, :cond_2

    .line 806444
    iget-object v0, p0, LX/4mx;->c:LX/0Ve;

    iget-object v1, p0, LX/4mx;->d:LX/1Cl;

    .line 806445
    iget-object v3, v1, LX/1Cl;->b:Ljava/lang/Throwable;

    move-object v1, v3

    .line 806446
    invoke-interface {v0, v1}, LX/0TF;->onFailure(Ljava/lang/Throwable;)V

    goto :goto_1

    .line 806447
    :pswitch_2
    iget-object v0, p0, LX/4mx;->c:LX/0Ve;

    invoke-interface {v0}, LX/0Ve;->dispose()V

    .line 806448
    iput-object v2, p0, LX/4mx;->c:LX/0Ve;

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private declared-synchronized b()Z
    .locals 3

    .prologue
    .line 806399
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, LX/4mx;->a:LX/1Ck;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 806400
    :try_start_1
    iget-object v0, p0, LX/4mx;->a:LX/1Ck;

    iget-object v0, v0, LX/1Ck;->a:LX/0Xq;

    iget-object v2, p0, LX/4mx;->b:Ljava/lang/Object;

    invoke-virtual {v0, v2}, LX/0Xr;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 806401
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 806402
    :try_start_2
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Mv;

    .line 806403
    iget-object v2, v0, LX/1Mv;->b:LX/0Ve;

    move-object v0, v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 806404
    if-ne v0, p0, :cond_0

    .line 806405
    const/4 v0, 0x1

    .line 806406
    :goto_0
    monitor-exit p0

    return v0

    .line 806407
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 806408
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 806409
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final dispose()V
    .locals 1

    .prologue
    .line 806423
    invoke-direct {p0}, LX/4mx;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 806424
    :goto_0
    return-void

    .line 806425
    :cond_0
    sget-object v0, LX/1Ck;->d:LX/1Cl;

    iput-object v0, p0, LX/4mx;->d:LX/1Cl;

    .line 806426
    invoke-direct {p0}, LX/4mx;->a()V

    goto :goto_0
.end method

.method public final isDisposed()Z
    .locals 1

    .prologue
    .line 806420
    monitor-enter p0

    .line 806421
    :try_start_0
    iget-object v0, p0, LX/4mx;->c:LX/0Ve;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/4mx;->c:LX/0Ve;

    invoke-interface {v0}, LX/0Vf;->isDisposed()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 806422
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 806415
    invoke-direct {p0}, LX/4mx;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 806416
    :goto_0
    return-void

    .line 806417
    :cond_0
    new-instance v0, LX/1Cl;

    const/4 v1, 0x0

    sget-object v2, LX/1Cm;->FAILED:LX/1Cm;

    invoke-direct {v0, v1, p1, v2}, LX/1Cl;-><init>(Ljava/lang/Object;Ljava/lang/Throwable;LX/1Cm;)V

    move-object v0, v0

    .line 806418
    iput-object v0, p0, LX/4mx;->d:LX/1Cl;

    .line 806419
    invoke-direct {p0}, LX/4mx;->a()V

    goto :goto_0
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 806410
    invoke-direct {p0}, LX/4mx;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 806411
    :goto_0
    return-void

    .line 806412
    :cond_0
    new-instance v0, LX/1Cl;

    const/4 v1, 0x0

    sget-object v2, LX/1Cm;->SUCCESSFUL:LX/1Cm;

    invoke-direct {v0, p1, v1, v2}, LX/1Cl;-><init>(Ljava/lang/Object;Ljava/lang/Throwable;LX/1Cm;)V

    move-object v0, v0

    .line 806413
    iput-object v0, p0, LX/4mx;->d:LX/1Cl;

    .line 806414
    invoke-direct {p0}, LX/4mx;->a()V

    goto :goto_0
.end method
