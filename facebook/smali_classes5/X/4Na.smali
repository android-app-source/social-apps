.class public LX/4Na;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 693734
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 12

    .prologue
    .line 693766
    const/4 v5, 0x0

    .line 693767
    const-wide/16 v6, 0x0

    .line 693768
    const/4 v4, 0x0

    .line 693769
    const/4 v3, 0x0

    .line 693770
    const/4 v2, 0x0

    .line 693771
    const/4 v1, 0x0

    .line 693772
    const/4 v0, 0x0

    .line 693773
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v8, v9, :cond_9

    .line 693774
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 693775
    const/4 v0, 0x0

    .line 693776
    :goto_0
    return v0

    .line 693777
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v1, v5, :cond_7

    .line 693778
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v1

    .line 693779
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 693780
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v10, :cond_0

    if-eqz v1, :cond_0

    .line 693781
    const-string v5, "accent_image"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 693782
    invoke-static {p0, p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v1

    move v4, v1

    goto :goto_1

    .line 693783
    :cond_1
    const-string v5, "fetchTimeMs"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 693784
    const/4 v0, 0x1

    .line 693785
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    goto :goto_1

    .line 693786
    :cond_2
    const-string v5, "friend_list"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 693787
    invoke-static {p0, p1}, LX/4NX;->a(LX/15w;LX/186;)I

    move-result v1

    move v9, v1

    goto :goto_1

    .line 693788
    :cond_3
    const-string v5, "render_style"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 693789
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    move v8, v1

    goto :goto_1

    .line 693790
    :cond_4
    const-string v5, "section_header"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 693791
    invoke-static {p0, p1}, LX/4Ni;->a(LX/15w;LX/186;)I

    move-result v1

    move v7, v1

    goto :goto_1

    .line 693792
    :cond_5
    const-string v5, "title"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 693793
    invoke-static {p0, p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v1

    move v6, v1

    goto :goto_1

    .line 693794
    :cond_6
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 693795
    :cond_7
    const/4 v1, 0x6

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 693796
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 693797
    if-eqz v0, :cond_8

    .line 693798
    const/4 v1, 0x1

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 693799
    :cond_8
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 693800
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 693801
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 693802
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 693803
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :cond_9
    move v8, v3

    move v9, v4

    move v4, v5

    move v11, v2

    move-wide v2, v6

    move v7, v11

    move v6, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15w;S)LX/15i;
    .locals 5

    .prologue
    .line 693804
    const/4 v0, 0x1

    const/4 v4, 0x0

    .line 693805
    new-instance v2, LX/186;

    const/16 v1, 0x80

    invoke-direct {v2, v1}, LX/186;-><init>(I)V

    .line 693806
    invoke-static {p0, v2}, LX/4Na;->a(LX/15w;LX/186;)I

    move-result v1

    .line 693807
    if-eqz v0, :cond_0

    .line 693808
    const/4 v3, 0x2

    invoke-virtual {v2, v3}, LX/186;->c(I)V

    .line 693809
    invoke-virtual {v2, v4, p1, v4}, LX/186;->a(ISI)V

    .line 693810
    const/4 v3, 0x1

    invoke-virtual {v2, v3, v1}, LX/186;->b(II)V

    .line 693811
    invoke-virtual {v2}, LX/186;->d()I

    move-result v1

    .line 693812
    :cond_0
    invoke-virtual {v2, v1}, LX/186;->d(I)V

    .line 693813
    invoke-static {v2}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v1

    move-object v0, v1

    .line 693814
    return-object v0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 693735
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 693736
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 693737
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 693738
    const-string v0, "name"

    const-string v1, "GoodwillThrowbackFriendversaryStory"

    invoke-virtual {p2, v0, v1}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 693739
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 693740
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 693741
    if-eqz v0, :cond_0

    .line 693742
    const-string v1, "accent_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 693743
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 693744
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 693745
    cmp-long v2, v0, v2

    if-eqz v2, :cond_1

    .line 693746
    const-string v2, "fetchTimeMs"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 693747
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 693748
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 693749
    if-eqz v0, :cond_2

    .line 693750
    const-string v1, "friend_list"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 693751
    invoke-static {p0, v0, p2, p3}, LX/4NX;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 693752
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 693753
    if-eqz v0, :cond_3

    .line 693754
    const-string v1, "render_style"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 693755
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 693756
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 693757
    if-eqz v0, :cond_4

    .line 693758
    const-string v1, "section_header"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 693759
    invoke-static {p0, v0, p2, p3}, LX/4Ni;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 693760
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 693761
    if-eqz v0, :cond_5

    .line 693762
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 693763
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 693764
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 693765
    return-void
.end method
