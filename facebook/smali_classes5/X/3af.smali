.class public final LX/3af;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3ag;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 606938
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/CvY;LX/CxV;Lcom/facebook/search/results/model/contract/SearchResultsSeeMoreFeedUnit;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<N::",
            "LX/CxV;",
            ":",
            "LX/CxG;",
            ">(",
            "LX/CvY;",
            "TN;",
            "Lcom/facebook/search/results/model/contract/SearchResultsSeeMoreFeedUnit;",
            ")V"
        }
    .end annotation

    .prologue
    .line 606939
    invoke-interface {p2}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v1

    move-object v0, p2

    check-cast v0, LX/CxG;

    invoke-interface {v0, p3}, LX/CxG;->a(Ljava/lang/Object;)I

    move-result v2

    invoke-interface {p2}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v3

    move-object v0, p3

    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsEntityPivotUnit;

    invoke-virtual {v0}, Lcom/facebook/search/results/model/unit/SearchResultsEntityPivotUnit;->m()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {p3}, Lcom/facebook/search/results/model/contract/SearchResultsSeeMoreFeedUnit;->q()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v4

    invoke-interface {p3}, Lcom/facebook/search/results/model/contract/SearchResultsSeeMoreFeedUnit;->r()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v5

    .line 606940
    sget-object v6, LX/CvJ;->SEE_MORE_ON_MODULE_TAPPED:LX/CvJ;

    invoke-static {v6, v3}, LX/CvY;->a(LX/CvJ;Lcom/facebook/search/results/model/SearchResultsMutableContext;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string p0, "results_module_role"

    sget-object p2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->ENTITY_PIVOTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    invoke-virtual {v6, p0, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string p0, "results_query_role"

    invoke-virtual {v6, p0, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string p0, "results_module_extra_logging"

    invoke-virtual {v6, p0, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string p0, "results_module_display_style"

    invoke-virtual {v6, p0, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    move-object v0, v6

    .line 606941
    invoke-virtual {p1, v1, v2, p3, v0}, LX/CvY;->b(Lcom/facebook/search/results/model/SearchResultsMutableContext;ILX/CvV;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 606942
    return-void
.end method
