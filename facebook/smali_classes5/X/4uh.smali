.class public final LX/4uh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1qf;
.implements LX/1qg;


# instance fields
.field public final synthetic a:LX/2wr;


# direct methods
.method public constructor <init>(LX/2wr;)V
    .locals 0

    iput-object p1, p0, LX/4uh;->a:LX/2wr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 0

    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3

    iget-object v0, p0, LX/4uh;->a:LX/2wr;

    iget-object v0, v0, LX/2wr;->k:LX/3KI;

    new-instance v1, LX/4ug;

    iget-object v2, p0, LX/4uh;->a:LX/2wr;

    invoke-direct {v1, v2}, LX/4ug;-><init>(LX/2wr;)V

    invoke-interface {v0, v1}, LX/3KI;->a(LX/4ud;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 2
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, LX/4uh;->a:LX/2wr;

    iget-object v0, v0, LX/2wr;->b:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget-object v0, p0, LX/4uh;->a:LX/2wr;

    invoke-static {v0, p1}, LX/2wr;->b$redex0(LX/2wr;Lcom/google/android/gms/common/ConnectionResult;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/4uh;->a:LX/2wr;

    invoke-static {v0}, LX/2wr;->h(LX/2wr;)V

    iget-object v0, p0, LX/4uh;->a:LX/2wr;

    invoke-static {v0}, LX/2wr;->e(LX/2wr;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    iget-object v0, p0, LX/4uh;->a:LX/2wr;

    iget-object v0, v0, LX/2wr;->b:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, LX/4uh;->a:LX/2wr;

    invoke-static {v0, p1}, LX/2wr;->c(LX/2wr;Lcom/google/android/gms/common/ConnectionResult;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/4uh;->a:LX/2wr;

    iget-object v1, v1, LX/2wr;->b:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method
