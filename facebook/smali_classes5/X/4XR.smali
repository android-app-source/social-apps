.class public final LX/4XR;
.super LX/0ur;
.source ""


# instance fields
.field public A:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public B:Lcom/facebook/graphql/model/GraphQLAlbum;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public C:J

.field public D:Lcom/facebook/graphql/model/GraphQLAlbumsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public E:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public F:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public G:Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public H:Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public I:Lcom/facebook/graphql/model/GraphQLAllShareStoriesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public J:Lcom/facebook/graphql/model/GraphQLStorySetStoriesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public K:Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public L:Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public M:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public N:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public O:Lcom/facebook/graphql/model/GraphQLCurrencyAmount;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public P:Lcom/facebook/graphql/model/GraphQLAndroidAppConfig;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Q:I

.field public R:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public S:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public T:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public U:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public V:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public W:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public X:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Y:Lcom/facebook/graphql/model/GraphQLApplication;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Z:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aA:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aB:Lcom/facebook/graphql/model/GraphQLDate;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aC:I

.field public aD:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aE:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aF:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aG:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aH:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aI:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aJ:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

.field public aK:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

.field public aL:Lcom/facebook/graphql/enums/GraphQLMessengerCommerceBubbleType;

.field public aM:Lcom/facebook/graphql/model/GraphQLActor;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aN:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aO:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aP:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLBylineFragment;",
            ">;"
        }
    .end annotation
.end field

.field public aQ:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aR:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aS:Lcom/facebook/graphql/model/GraphQLFundraiserCampaign;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aT:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aU:Z

.field public aV:Z

.field public aW:Z

.field public aX:Z

.field public aY:Z

.field public aZ:Z

.field public aa:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ab:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ac:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ad:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ae:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public af:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLProfile;",
            ">;"
        }
    .end annotation
.end field

.field public ag:I

.field public ah:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;",
            ">;"
        }
    .end annotation
.end field

.field public ai:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aj:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public ak:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLAttributionEntry;",
            ">;"
        }
    .end annotation
.end field

.field public al:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public am:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public an:Lcom/facebook/graphql/model/GraphQLActor;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ao:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ap:D

.field public aq:Lcom/facebook/graphql/model/GraphQLAYMTChannel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ar:Lcom/facebook/graphql/model/GraphQLBackdatedTime;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public as:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public at:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public au:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public av:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aw:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ax:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ay:Lcom/facebook/graphql/model/GraphQLCurrencyAmount;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public az:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bA:Z

.field public bB:Z

.field public bC:Z

.field public bD:Z

.field public bE:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bF:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bG:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bH:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public bI:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public bJ:Lcom/facebook/graphql/enums/GraphQLPageCategoryType;

.field public bK:Lcom/facebook/graphql/model/GraphQLCelebrityBasicInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bL:Lcom/facebook/graphql/model/GraphQLCharity;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bM:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bN:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bO:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bP:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bQ:J

.field public bR:I

.field public bS:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public bT:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bU:Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bV:Lcom/facebook/graphql/enums/GraphQLPageCommPlatform;

.field public bW:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bX:Lcom/facebook/graphql/enums/GraphQLPageCommStatus;

.field public bY:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bZ:Lcom/facebook/graphql/enums/GraphQLPageCommType;

.field public ba:Z

.field public bb:Z

.field public bc:Z

.field public bd:Z

.field public be:Z

.field public bf:Z

.field public bg:Z

.field public bh:Z

.field public bi:Z

.field public bj:Z

.field public bk:Z

.field public bl:Z

.field public bm:Z

.field public bn:Z

.field public bo:Z

.field public bp:Z

.field public bq:Z

.field public br:Z

.field public bs:Z

.field public bt:Z

.field public bu:Z

.field public bv:Z

.field public bw:Z

.field public bx:Z

.field public by:Z

.field public bz:Z

.field public c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cA:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cB:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cC:Lcom/facebook/graphql/model/GraphQLGroup;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cD:J

.field public cE:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cF:J

.field public cG:Lcom/facebook/graphql/model/GraphQLActor;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cH:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cI:Lcom/facebook/graphql/model/GraphQLVideo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cJ:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cK:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cL:Lcom/facebook/graphql/model/GraphQLLocation;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cM:Lcom/facebook/graphql/model/GraphQLLocation;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cN:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cO:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPointsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cP:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cQ:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cR:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cS:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cT:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cU:Lcom/facebook/graphql/model/GraphQLDocumentFontResource;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cV:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cW:D

.field public cX:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cY:Lcom/facebook/graphql/model/GraphQLLocation;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cZ:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ca:Lcom/facebook/graphql/model/GraphQLComment;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cb:Lcom/facebook/graphql/model/GraphQLCommentsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cc:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cd:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ce:Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;

.field public cf:Z

.field public cg:Lcom/facebook/graphql/enums/GraphQLCommercePageType;

.field public ch:Lcom/facebook/graphql/enums/GraphQLCommerceProductVisibility;

.field public ci:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

.field public cj:J

.field public ck:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cl:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cm:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPage;",
            ">;"
        }
    .end annotation
.end field

.field public cn:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;"
        }
    .end annotation
.end field

.field public co:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

.field public cp:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cq:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cr:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cs:Lcom/facebook/graphql/model/GraphQLCurrencyAmount;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ct:Lcom/facebook/graphql/model/GraphQLCoordinate;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cu:Lcom/facebook/graphql/model/GraphQLLocation;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cv:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cw:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public cx:I

.field public cy:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cz:Lcom/facebook/graphql/enums/GraphQLCouponClaimLocation;

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public dA:D

.field public dB:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public dC:I

.field public dD:Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public dE:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public dF:Lcom/facebook/graphql/model/GraphQLEmotionalAnalysis;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public dG:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public dH:Z

.field public dI:J

.field public dJ:J

.field public dK:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLLeadGenErrorNode;",
            ">;"
        }
    .end annotation
.end field

.field public dL:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public dM:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public dN:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public dO:I

.field public dP:I

.field public dQ:Lcom/facebook/graphql/model/GraphQLEvent;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public dR:Lcom/facebook/graphql/model/GraphQLEventCategoryData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public dS:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public dT:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public dU:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public dV:Lcom/facebook/graphql/model/GraphQLLocation;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public dW:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public dX:Lcom/facebook/graphql/model/GraphQLActor;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public dY:Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public dZ:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public da:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public db:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public dc:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTarotCard;",
            ">;"
        }
    .end annotation
.end field

.field public dd:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public de:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public df:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public dg:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public dh:Lcom/facebook/graphql/model/GraphQLPhoto;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public di:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public dj:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public dk:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public dl:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public dm:Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

.field public dn:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public do:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public dp:D

.field public dq:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public dr:I

.field public ds:Z

.field public dt:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public du:Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharityDonorsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public dv:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public dw:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public dx:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public dy:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public dz:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public eA:Lcom/facebook/graphql/model/GraphQLFeedback;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public eB:Lcom/facebook/graphql/model/GraphQLFeedbackContext;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public eC:Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValuesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public eD:I

.field public eE:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public eF:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public eG:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public eH:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public eI:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public eJ:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public eK:D

.field public eL:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public eM:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public eN:Lcom/facebook/graphql/model/GraphQLFollowUpFeedUnitsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public eO:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public eP:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public eQ:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public eR:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public eS:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public eT:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public eU:Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public eV:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public eW:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public eX:Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public eY:Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public eZ:Lcom/facebook/graphql/model/GraphQLFundraiserFriendDonorsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ea:Lcom/facebook/graphql/model/GraphQLEventHostsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public eb:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

.field public ec:Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ed:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ee:Lcom/facebook/graphql/model/GraphQLPlace;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ef:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

.field public eg:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

.field public eh:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ei:Lcom/facebook/graphql/enums/GraphQLEventType;

.field public ej:Lcom/facebook/graphql/model/GraphQLEventViewerCapability;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ek:Lcom/facebook/graphql/enums/GraphQLEventVisibility;

.field public el:Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public em:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public en:J

.field public eo:J

.field public ep:Lcom/facebook/graphql/model/GraphQLPlace;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public eq:Z

.field public er:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public es:Lcom/facebook/graphql/model/GraphQLProfile;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public et:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public eu:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ev:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ew:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ex:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ey:Lcom/facebook/graphql/model/GraphQLFeedTopicContent;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ez:Lcom/facebook/graphql/model/FeedUnit;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLPageActionChannel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public fA:I

.field public fB:Lcom/facebook/graphql/model/GraphQLPhoto;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public fC:I

.field public fD:Z

.field public fE:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public fF:Lcom/facebook/graphql/enums/GraphQLBookmarkHighlightStyle;

.field public fG:Lcom/facebook/graphql/model/GraphQLCharity;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public fH:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public fI:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTimeRange;",
            ">;"
        }
    .end annotation
.end field

.field public fJ:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public fK:Lcom/facebook/graphql/model/GraphQLIcon;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public fL:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public fM:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public fN:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public fO:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public fP:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public fQ:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public fR:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public fS:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public fT:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public fU:Lcom/facebook/graphql/model/GraphQLPlace;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public fV:Lcom/facebook/graphql/model/GraphQLImportantReactorsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public fW:Z

.field public fX:I

.field public fY:I

.field public fZ:I

.field public fa:Lcom/facebook/graphql/model/GraphQLFriendsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public fb:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

.field public fc:Lcom/facebook/graphql/model/GraphQLActor;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public fd:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public fe:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ff:I

.field public fg:Lcom/facebook/graphql/model/GraphQLStoryAttachment;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public fh:Lcom/facebook/graphql/model/GraphQLExternalUrl;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public fi:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public fj:Lcom/facebook/graphql/model/GraphQLCurrencyAmount;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public fk:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public fl:Lcom/facebook/graphql/model/GraphQLGreetingCardTemplate;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public fm:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public fn:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public fo:Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public fp:Lcom/facebook/graphql/model/GraphQLGroupOwnerAuthoredStoriesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public fq:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public fr:Lcom/facebook/graphql/model/GraphQLGroupPinnedStoriesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public fs:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ft:Lcom/facebook/graphql/model/GraphQLVideoGuidedTour;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public fu:Z

.field public fv:Z

.field public fw:Z

.field public fx:Z

.field public fy:Z

.field public fz:I

.field public g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public gA:Z

.field public gB:Z

.field public gC:Z

.field public gD:Z

.field public gE:Z

.field public gF:Z

.field public gG:Z

.field public gH:Z

.field public gI:Z

.field public gJ:Z

.field public gK:Z

.field public gL:Z

.field public gM:Z

.field public gN:Z

.field public gO:Z

.field public gP:Z

.field public gQ:Z

.field public gR:Z

.field public gS:Z

.field public gT:Z

.field public gU:Z

.field public gV:Z

.field public gW:Z

.field public gX:Z

.field public gY:Z

.field public gZ:Z

.field public ga:Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public gb:Lcom/facebook/graphql/model/GraphQLStoryInsights;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public gc:I

.field public gd:Lcom/facebook/graphql/model/GraphQLInstantArticle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ge:Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public gf:Z

.field public gg:Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public gh:Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public gi:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public gj:Lcom/facebook/graphql/model/GraphQLPlaceListInvitedFriendsInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public gk:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public gl:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public gm:Z

.field public gn:Z

.field public go:Z

.field public gp:Z

.field public gq:Z

.field public gr:Z

.field public gs:Z

.field public gt:Z

.field public gu:Z

.field public gv:Z

.field public gw:Z

.field public gx:Z

.field public gy:Z

.field public gz:Z

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public hA:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public hB:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public hC:Lcom/facebook/graphql/enums/GraphQLLightweightEventStatus;

.field public hD:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

.field public hE:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;",
            ">;"
        }
    .end annotation
.end field

.field public hF:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public hG:Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public hH:Lcom/facebook/graphql/model/GraphQLMedia;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public hI:Lcom/facebook/graphql/model/GraphQLFriendListFeedConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public hJ:Lcom/facebook/graphql/model/GraphQLPlaceListItemsFromPlaceListConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public hK:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public hL:Lcom/facebook/graphql/enums/GraphQLFriendListType;

.field public hM:I

.field public hN:I

.field public hO:I

.field public hP:I

.field public hQ:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public hR:Lcom/facebook/graphql/model/GraphQLLocation;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public hS:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public hT:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public hU:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public hV:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public hW:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public hX:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public hY:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public hZ:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLLocation;",
            ">;"
        }
    .end annotation
.end field

.field public ha:Z

.field public hb:Z

.field public hc:Z

.field public hd:Z

.field public he:Z

.field public hf:Z

.field public hg:Z

.field public hh:Z

.field public hi:Z

.field public hj:Z

.field public hk:Z

.field public hl:Z

.field public hm:Z

.field public hn:Z

.field public ho:Z

.field public hp:Z

.field public hq:Z

.field public hr:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public hs:Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;

.field public ht:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public hu:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public hv:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public hw:J

.field public hx:Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public hy:Lcom/facebook/graphql/model/GraphQLLeadGenData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public hz:Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;",
            ">;"
        }
    .end annotation
.end field

.field public iA:Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public iB:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public iC:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public iD:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public iE:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public iF:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public iG:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public iH:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public iI:Z

.field public iJ:D

.field public iK:Lcom/facebook/graphql/model/GraphQLOffer;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public iL:Lcom/facebook/graphql/model/GraphQLOfferView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public iM:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public iN:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public iO:Lcom/facebook/graphql/model/GraphQLStoryAttachment;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public iP:Lcom/facebook/graphql/model/GraphQLOpenGraphMetadata;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public iQ:Lcom/facebook/graphql/model/GraphQLNode;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public iR:Lcom/facebook/graphql/model/GraphQLQuestionOptionsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public iS:Lcom/facebook/graphql/model/GraphQLStoryActionLink;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public iT:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public iU:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public iV:Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;

.field public iW:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public iX:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLProductImage;",
            ">;"
        }
    .end annotation
.end field

.field public iY:Lcom/facebook/graphql/model/GraphQLGroup;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public iZ:D

.field public ia:I

.field public ib:Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ic:Lcom/facebook/graphql/model/GraphQLSouvenirMediaConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public id:Lcom/facebook/graphql/model/GraphQLMediaQuestionOptionsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ie:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPhoto;",
            ">;"
        }
    .end annotation
.end field

.field public if:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ig:Lcom/facebook/graphql/model/GraphQLMediaSet;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ih:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ii:Lcom/facebook/graphql/model/GraphQLPageMenuInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ij:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ik:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public il:Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;

.field public im:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public in:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public io:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ip:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComposedBlockWithEntities;",
            ">;"
        }
    .end annotation
.end field

.field public iq:Lcom/facebook/graphql/model/GraphQLContact;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ir:Lcom/facebook/graphql/model/GraphQLMessengerContentSubscriptionOption;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public is:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public it:J

.field public iu:Lcom/facebook/graphql/enums/GraphQLMovieBotMovieListStyle;

.field public iv:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public iw:Lcom/facebook/graphql/model/GraphQLOpenGraphObject;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ix:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public iy:Lcom/facebook/graphql/enums/GraphQLMusicType;

.field public iz:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLOpenGraphObject;",
            ">;"
        }
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

.field public jA:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;",
            ">;"
        }
    .end annotation
.end field

.field public jB:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPage;",
            ">;"
        }
    .end annotation
.end field

.field public jC:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;"
        }
    .end annotation
.end field

.field public jD:D

.field public jE:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public jF:Lcom/facebook/graphql/enums/GraphQLPermanentlyClosedStatus;

.field public jG:Lcom/facebook/graphql/model/GraphQLName;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public jH:Lcom/facebook/graphql/model/GraphQLPhoto;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public jI:Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public jJ:Lcom/facebook/graphql/model/GraphQLActor;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public jK:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPhoto;",
            ">;"
        }
    .end annotation
.end field

.field public jL:Lcom/facebook/graphql/model/GraphQLPhrasesAnalysis;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public jM:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public jN:Lcom/facebook/graphql/model/GraphQLPlace;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public jO:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public jP:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

.field public jQ:Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public jR:Lcom/facebook/graphql/enums/GraphQLPlaceType;

.field public jS:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public jT:I

.field public jU:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public jV:I

.field public jW:I

.field public jX:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public jY:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public jZ:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ja:Lcom/facebook/graphql/model/GraphQLRating;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public jb:Lcom/facebook/graphql/model/GraphQLActor;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public jc:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public jd:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public je:Lcom/facebook/graphql/model/GraphQLPageCallToAction;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public jf:Lcom/facebook/graphql/model/GraphQLPageLikersConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public jg:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;",
            ">;"
        }
    .end annotation
.end field

.field public jh:I

.field public ji:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public jj:Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public jk:Lcom/facebook/graphql/model/GraphQLGroup;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public jl:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public jm:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public jn:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public jo:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public jp:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public jq:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public jr:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public js:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public jt:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ju:Lcom/facebook/graphql/enums/GraphQLPaymentModulesClient;

.field public jv:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public jw:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public jx:Lcom/facebook/graphql/model/GraphQLCurrencyAmount;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public jy:I

.field public jz:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLOpenGraphAction;",
            ">;"
        }
    .end annotation
.end field

.field public kA:Lcom/facebook/graphql/model/GraphQLPhoto;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public kB:Lcom/facebook/graphql/model/GraphQLPrivacyOption;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public kC:Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public kD:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public kE:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLProductImage;",
            ">;"
        }
    .end annotation
.end field

.field public kF:Lcom/facebook/graphql/model/GraphQLProductItem;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public kG:Lcom/facebook/graphql/model/GraphQLCurrencyAmount;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public kH:D

.field public kI:D

.field public kJ:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public kK:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public kL:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public kM:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public kN:Lcom/facebook/graphql/model/GraphQLPageActionChannel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public kO:Lcom/facebook/graphql/model/GraphQLPageActionChannel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public kP:Lcom/facebook/graphql/model/GraphQLProfileDiscoveryBucket;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public kQ:Lcom/facebook/graphql/model/GraphQLPhoto;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public kR:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public kS:Z

.field public kT:Lcom/facebook/graphql/model/GraphQLProfileVideo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public kU:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public kV:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public kW:Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public kX:Lcom/facebook/graphql/model/GraphQLComposedDocument;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public kY:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public kZ:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ka:Lcom/facebook/graphql/enums/GraphQLQuestionPollAnswersState;

.field public kb:Z

.field public kc:Lcom/facebook/graphql/model/GraphQLBoostedComponent;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public kd:Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ke:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public kf:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public kg:J

.field public kh:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ki:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public kj:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public kk:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public kl:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public km:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public kn:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ko:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public kp:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public kq:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLAudio;",
            ">;"
        }
    .end annotation
.end field

.field public kr:I

.field public ks:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public kt:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ku:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public kv:Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;

.field public kw:Lcom/facebook/graphql/model/GraphQLPageActionChannel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public kx:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ky:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public kz:Lcom/facebook/graphql/model/GraphQLNode;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/graphql/model/GraphQLPageAdminInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public lA:Lcom/facebook/graphql/model/GraphQLActor;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public lB:Lcom/facebook/graphql/model/GraphQLResharesOfContentConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public lC:Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;

.field public lD:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public lE:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public lF:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public lG:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public lH:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public lI:Lcom/facebook/graphql/model/GraphQLStorySaveInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public lJ:Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public lK:J

.field public lL:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public lM:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public lN:Lcom/facebook/graphql/model/GraphQLSearchElectionAllData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public lO:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public lP:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

.field public lQ:I

.field public lR:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public lS:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

.field public lT:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public lU:Lcom/facebook/graphql/model/GraphQLSeenByConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public lV:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

.field public lW:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public lX:Lcom/facebook/graphql/model/GraphQLMailingAddress;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public lY:Lcom/facebook/graphql/model/GraphQLActor;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public lZ:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public la:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public lb:Lcom/facebook/graphql/model/GraphQLGraphSearchQueryTitle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public lc:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ld:Lcom/facebook/graphql/model/GraphQLQuotesAnalysis;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public le:Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public lf:Lcom/facebook/graphql/model/GraphQLRating;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public lg:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public lh:Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public li:Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public lj:Lcom/facebook/graphql/model/GraphQLUser;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public lk:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ll:Lcom/facebook/graphql/model/GraphQLPhoto;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public lm:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ln:Lcom/facebook/graphql/model/GraphQLUser;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public lo:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public lp:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public lq:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public lr:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ls:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLRedirectionInfo;",
            ">;"
        }
    .end annotation
.end field

.field public lt:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public lu:Lcom/facebook/graphql/model/GraphQLSticker;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public lv:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public lw:Lcom/facebook/graphql/model/GraphQLActor;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public lx:Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

.field public ly:J

.field public lz:Lcom/facebook/graphql/model/GraphQLActor;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;"
        }
    .end annotation
.end field

.field public mA:Z

.field public mB:Z

.field public mC:Z

.field public mD:Z

.field public mE:Z

.field public mF:Z

.field public mG:Z

.field public mH:Lcom/facebook/graphql/model/GraphQLGreetingCardSlidesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public mI:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public mJ:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public mK:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public mL:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public mM:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public mN:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public mO:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public mP:Lcom/facebook/graphql/model/GraphQLLocation;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public mQ:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public mR:Lcom/facebook/graphql/model/GraphQLPhoto;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public mS:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public mT:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public mU:D

.field public mV:D

.field public mW:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public mX:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public mY:I

.field public mZ:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ma:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public mb:Lcom/facebook/graphql/model/GraphQLUser;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public mc:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public md:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public me:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public mf:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public mg:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public mh:Lcom/facebook/graphql/model/GraphQLEntity;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public mi:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public mj:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public mk:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ml:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public mm:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public mn:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public mo:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public mp:Z

.field public mq:Z

.field public mr:Z

.field public ms:Z

.field public mt:Z

.field public mu:Z

.field public mv:Z

.field public mw:Z

.field public mx:Z

.field public my:Z

.field public mz:Z

.field public n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public nA:Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public nB:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public nC:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

.field public nD:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public nE:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedbackReaction;",
            ">;"
        }
    .end annotation
.end field

.field public nF:Z

.field public nG:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public nH:Lcom/facebook/graphql/model/GraphQLGroup;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public nI:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public nJ:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public nK:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public nL:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public nM:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLImage;",
            ">;"
        }
    .end annotation
.end field

.field public nN:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public nO:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public nP:Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public nQ:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public nR:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public nS:Lcom/facebook/graphql/model/GraphQLOpenGraphMetadata;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public nT:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public nU:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public nV:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public nW:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLMedia;",
            ">;"
        }
    .end annotation
.end field

.field public nX:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public nY:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public nZ:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public na:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public nb:Lcom/facebook/graphql/model/GraphQLSponsoredData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public nc:Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public nd:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ne:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public nf:J

.field public ng:J

.field public nh:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ni:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public nj:Lcom/facebook/graphql/enums/GraphQLMessengerRetailItemStatus;

.field public nk:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public nl:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public nm:Lcom/facebook/graphql/model/GraphQLStoryAttachment;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public nn:Lcom/facebook/graphql/model/GraphQLStoryHeader;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public no:Lcom/facebook/graphql/model/GraphQLName;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public np:Lcom/facebook/graphql/model/GraphQLStructuredSurvey;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public nq:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;",
            ">;"
        }
    .end annotation
.end field

.field public nr:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ns:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

.field public nt:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLSubstoriesGroupingReason;",
            ">;"
        }
    .end annotation
.end field

.field public nu:I

.field public nv:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public nw:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public nx:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ny:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public nz:Lcom/facebook/graphql/model/GraphQLTimeRange;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Lcom/facebook/graphql/enums/GraphQLAdsExperienceStatusEnum;

.field public oA:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public oB:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public oC:I

.field public oD:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public oE:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public oF:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

.field public oG:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public oH:I

.field public oI:I

.field public oJ:Lcom/facebook/graphql/model/GraphQLPostTranslatability;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public oK:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public oL:Lcom/facebook/graphql/model/GraphQLTranslation;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public oM:Lcom/facebook/graphql/model/GraphQLTrendingTopicData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public oN:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public oO:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public oP:I

.field public oQ:Z

.field public oR:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public oS:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public oT:J

.field public oU:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public oV:Lcom/facebook/graphql/model/GraphQLUser;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public oW:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public oX:Lcom/facebook/graphql/model/GraphQLFundraiserDonorsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public oY:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public oZ:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public oa:I

.field public ob:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPhotoTile;",
            ">;"
        }
    .end annotation
.end field

.field public oc:J

.field public od:Lcom/facebook/graphql/model/GraphQLEventTimeRange;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public oe:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public of:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public og:Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public oh:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public oi:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public oj:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ok:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ol:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public om:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public on:Lcom/facebook/graphql/model/GraphQLDocumentFontResource;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public oo:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public op:D

.field public oq:Lcom/facebook/graphql/model/GraphQLProfile;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public or:Lcom/facebook/graphql/model/GraphQLNode;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public os:Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ot:Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ou:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ov:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ow:Lcom/facebook/graphql/model/GraphQLCurrencyAmount;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ox:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public oy:Lcom/facebook/graphql/model/GraphQLCurrencyAmount;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public oz:I

.field public p:Lcom/facebook/graphql/model/GraphQLPageActionChannel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public pA:Z

.field public pB:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;"
        }
    .end annotation
.end field

.field public pC:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

.field public pD:I

.field public pE:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public pF:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public pG:Z

.field public pH:Lcom/facebook/graphql/model/GraphQLContactRecommendationField;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public pI:Lcom/facebook/graphql/enums/GraphQLSavedState;

.field public pJ:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;",
            ">;"
        }
    .end annotation
.end field

.field public pK:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;",
            ">;"
        }
    .end annotation
.end field

.field public pL:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

.field public pM:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

.field public pN:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public pO:Lcom/facebook/graphql/model/GraphQLVoiceSwitcherActorsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public pP:Lcom/facebook/graphql/model/GraphQLVoiceSwitcherPagesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public pQ:Lcom/facebook/graphql/model/GraphQLQuestionOptionVotersConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public pR:Lcom/facebook/graphql/model/GraphQLWeatherCondition;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public pS:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLWeatherHourlyForecast;",
            ">;"
        }
    .end annotation
.end field

.field public pT:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public pU:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public pV:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public pW:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public pX:I

.field public pY:Lcom/facebook/graphql/model/GraphQLWithTagsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public pZ:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public pa:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public pb:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public pc:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public pd:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

.field public pe:Lcom/facebook/graphql/model/GraphQLActor;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public pf:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public pg:Lcom/facebook/graphql/model/GraphQLVideoChannel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ph:Z

.field public pi:I

.field public pj:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public pk:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public pl:Lcom/facebook/graphql/model/GraphQLVideoShare;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public pm:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLVideo;",
            ">;"
        }
    .end annotation
.end field

.field public pn:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public po:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public pp:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public pq:Lcom/facebook/graphql/model/GraphQLUser;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public pr:Lcom/facebook/graphql/model/GraphQLUser;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ps:Z

.field public pt:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

.field public pu:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public pv:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;",
            ">;"
        }
    .end annotation
.end field

.field public pw:Lcom/facebook/graphql/model/GraphQLFeedbackReaction;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public px:I

.field public py:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

.field public pz:Z

.field public q:Lcom/facebook/graphql/model/GraphQLPageActionChannel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public qa:Lcom/facebook/graphql/model/GraphQLGeoRectangle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public qb:I

.field public qc:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:J

.field public s:Lcom/facebook/graphql/model/GraphQLPageActionChannel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLImage;",
            ">;"
        }
    .end annotation
.end field

.field public u:Lcom/facebook/graphql/model/GraphQLStreetAddress;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Lcom/facebook/graphql/model/GraphQLPageAdminInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 769229
    invoke-direct {p0}, LX/0ur;-><init>()V

    .line 769230
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventActionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    iput-object v0, p0, LX/4XR;->j:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    .line 769231
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLAdsExperienceStatusEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAdsExperienceStatusEnum;

    iput-object v0, p0, LX/4XR;->o:Lcom/facebook/graphql/enums/GraphQLAdsExperienceStatusEnum;

    .line 769232
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    iput-object v0, p0, LX/4XR;->aJ:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    .line 769233
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    iput-object v0, p0, LX/4XR;->aK:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 769234
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerCommerceBubbleType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerCommerceBubbleType;

    iput-object v0, p0, LX/4XR;->aL:Lcom/facebook/graphql/enums/GraphQLMessengerCommerceBubbleType;

    .line 769235
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageCategoryType;

    iput-object v0, p0, LX/4XR;->bJ:Lcom/facebook/graphql/enums/GraphQLPageCategoryType;

    .line 769236
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCommPlatform;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageCommPlatform;

    iput-object v0, p0, LX/4XR;->bV:Lcom/facebook/graphql/enums/GraphQLPageCommPlatform;

    .line 769237
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCommStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageCommStatus;

    iput-object v0, p0, LX/4XR;->bX:Lcom/facebook/graphql/enums/GraphQLPageCommStatus;

    .line 769238
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCommType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageCommType;

    iput-object v0, p0, LX/4XR;->bZ:Lcom/facebook/graphql/enums/GraphQLPageCommType;

    .line 769239
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;

    iput-object v0, p0, LX/4XR;->ce:Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;

    .line 769240
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCommercePageType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    iput-object v0, p0, LX/4XR;->cg:Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    .line 769241
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCommerceProductVisibility;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCommerceProductVisibility;

    iput-object v0, p0, LX/4XR;->ch:Lcom/facebook/graphql/enums/GraphQLCommerceProductVisibility;

    .line 769242
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    iput-object v0, p0, LX/4XR;->ci:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    .line 769243
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    iput-object v0, p0, LX/4XR;->co:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    .line 769244
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCouponClaimLocation;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCouponClaimLocation;

    iput-object v0, p0, LX/4XR;->cz:Lcom/facebook/graphql/enums/GraphQLCouponClaimLocation;

    .line 769245
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

    iput-object v0, p0, LX/4XR;->dm:Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

    .line 769246
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    iput-object v0, p0, LX/4XR;->eb:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    .line 769247
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    iput-object v0, p0, LX/4XR;->ef:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    .line 769248
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    iput-object v0, p0, LX/4XR;->eg:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    .line 769249
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventType;

    iput-object v0, p0, LX/4XR;->ei:Lcom/facebook/graphql/enums/GraphQLEventType;

    .line 769250
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventVisibility;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    iput-object v0, p0, LX/4XR;->ek:Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    .line 769251
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iput-object v0, p0, LX/4XR;->fb:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 769252
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBookmarkHighlightStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBookmarkHighlightStyle;

    iput-object v0, p0, LX/4XR;->fF:Lcom/facebook/graphql/enums/GraphQLBookmarkHighlightStyle;

    .line 769253
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;

    iput-object v0, p0, LX/4XR;->hs:Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;

    .line 769254
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLightweightEventStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLightweightEventStatus;

    iput-object v0, p0, LX/4XR;->hC:Lcom/facebook/graphql/enums/GraphQLLightweightEventStatus;

    .line 769255
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLightweightEventType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    iput-object v0, p0, LX/4XR;->hD:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    .line 769256
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendListType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendListType;

    iput-object v0, p0, LX/4XR;->hL:Lcom/facebook/graphql/enums/GraphQLFriendListType;

    .line 769257
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;

    iput-object v0, p0, LX/4XR;->il:Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;

    .line 769258
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMovieBotMovieListStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMovieBotMovieListStyle;

    iput-object v0, p0, LX/4XR;->iu:Lcom/facebook/graphql/enums/GraphQLMovieBotMovieListStyle;

    .line 769259
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMusicType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMusicType;

    iput-object v0, p0, LX/4XR;->iy:Lcom/facebook/graphql/enums/GraphQLMusicType;

    .line 769260
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;

    iput-object v0, p0, LX/4XR;->iV:Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;

    .line 769261
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPaymentModulesClient;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPaymentModulesClient;

    iput-object v0, p0, LX/4XR;->ju:Lcom/facebook/graphql/enums/GraphQLPaymentModulesClient;

    .line 769262
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPermanentlyClosedStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPermanentlyClosedStatus;

    iput-object v0, p0, LX/4XR;->jF:Lcom/facebook/graphql/enums/GraphQLPermanentlyClosedStatus;

    .line 769263
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    iput-object v0, p0, LX/4XR;->jP:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    .line 769264
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPlaceType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    iput-object v0, p0, LX/4XR;->jR:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    .line 769265
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLQuestionPollAnswersState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLQuestionPollAnswersState;

    iput-object v0, p0, LX/4XR;->ka:Lcom/facebook/graphql/enums/GraphQLQuestionPollAnswersState;

    .line 769266
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;

    iput-object v0, p0, LX/4XR;->kv:Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;

    .line 769267
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

    iput-object v0, p0, LX/4XR;->lx:Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

    .line 769268
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;

    iput-object v0, p0, LX/4XR;->lC:Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;

    .line 769269
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    iput-object v0, p0, LX/4XR;->lP:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    .line 769270
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    iput-object v0, p0, LX/4XR;->lS:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    .line 769271
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    iput-object v0, p0, LX/4XR;->lV:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    .line 769272
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMessengerRetailItemStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLMessengerRetailItemStatus;

    iput-object v0, p0, LX/4XR;->nj:Lcom/facebook/graphql/enums/GraphQLMessengerRetailItemStatus;

    .line 769273
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    iput-object v0, p0, LX/4XR;->ns:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 769274
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    iput-object v0, p0, LX/4XR;->nC:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 769275
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

    iput-object v0, p0, LX/4XR;->oF:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

    .line 769276
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    iput-object v0, p0, LX/4XR;->pd:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    .line 769277
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    iput-object v0, p0, LX/4XR;->pt:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    .line 769278
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    iput-object v0, p0, LX/4XR;->py:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 769279
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    iput-object v0, p0, LX/4XR;->pC:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 769280
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSavedState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    iput-object v0, p0, LX/4XR;->pI:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 769281
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    iput-object v0, p0, LX/4XR;->pL:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 769282
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    iput-object v0, p0, LX/4XR;->pM:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    .line 769283
    const/4 v0, 0x0

    iput-object v0, p0, LX/4XR;->qc:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 769284
    instance-of v0, p0, LX/4XR;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 769285
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLNode;)LX/4XR;
    .locals 4

    .prologue
    .line 768339
    new-instance v0, LX/4XR;

    invoke-direct {v0}, LX/4XR;-><init>()V

    .line 768340
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 768341
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->k()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->b:Lcom/facebook/graphql/model/GraphQLImage;

    .line 768342
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->l()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->c:Ljava/lang/String;

    .line 768343
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->mD()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->d:Ljava/lang/String;

    .line 768344
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->oh()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->e:Ljava/lang/String;

    .line 768345
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->m()Lcom/facebook/graphql/model/GraphQLPageActionChannel;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->f:Lcom/facebook/graphql/model/GraphQLPageActionChannel;

    .line 768346
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 768347
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->o()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->h:Ljava/lang/String;

    .line 768348
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->p()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->i:LX/0Px;

    .line 768349
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->q()Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->j:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    .line 768350
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->r()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->k:LX/0Px;

    .line 768351
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->s()Lcom/facebook/graphql/model/GraphQLPageAdminInfo;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->l:Lcom/facebook/graphql/model/GraphQLPageAdminInfo;

    .line 768352
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->t()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->m:LX/0Px;

    .line 768353
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->u()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->n:Ljava/lang/String;

    .line 768354
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->v()Lcom/facebook/graphql/enums/GraphQLAdsExperienceStatusEnum;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->o:Lcom/facebook/graphql/enums/GraphQLAdsExperienceStatusEnum;

    .line 768355
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->lW()Lcom/facebook/graphql/model/GraphQLPageActionChannel;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->p:Lcom/facebook/graphql/model/GraphQLPageActionChannel;

    .line 768356
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->oG()Lcom/facebook/graphql/model/GraphQLPageActionChannel;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->q:Lcom/facebook/graphql/model/GraphQLPageActionChannel;

    .line 768357
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->nD()J

    move-result-wide v2

    iput-wide v2, v0, LX/4XR;->r:J

    .line 768358
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->lJ()Lcom/facebook/graphql/model/GraphQLPageActionChannel;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->s:Lcom/facebook/graphql/model/GraphQLPageActionChannel;

    .line 768359
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->w()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->t:LX/0Px;

    .line 768360
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->x()Lcom/facebook/graphql/model/GraphQLStreetAddress;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->u:Lcom/facebook/graphql/model/GraphQLStreetAddress;

    .line 768361
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->y()Lcom/facebook/graphql/model/GraphQLPageAdminInfo;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->v:Lcom/facebook/graphql/model/GraphQLPageAdminInfo;

    .line 768362
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->pv()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->w:Ljava/lang/String;

    .line 768363
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->pa()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->x:Ljava/lang/String;

    .line 768364
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->oE()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->y:Ljava/lang/String;

    .line 768365
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->z()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->z:Ljava/lang/String;

    .line 768366
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->A()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->A:Ljava/lang/String;

    .line 768367
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->B()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->B:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 768368
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->C()J

    move-result-wide v2

    iput-wide v2, v0, LX/4XR;->C:J

    .line 768369
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->D()Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->D:Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    .line 768370
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->E()Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsConnection;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->E:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnitContactsConnection;

    .line 768371
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->mw()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->F:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 768372
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->F()Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsConnection;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->G:Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsConnection;

    .line 768373
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->G()Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsConnection;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->H:Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsConnection;

    .line 768374
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->H()Lcom/facebook/graphql/model/GraphQLAllShareStoriesConnection;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->I:Lcom/facebook/graphql/model/GraphQLAllShareStoriesConnection;

    .line 768375
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->I()Lcom/facebook/graphql/model/GraphQLStorySetStoriesConnection;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->J:Lcom/facebook/graphql/model/GraphQLStorySetStoriesConnection;

    .line 768376
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->J()Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->K:Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    .line 768377
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->K()Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersConnection;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->L:Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersConnection;

    .line 768378
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->nE()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->M:Ljava/lang/String;

    .line 768379
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->L()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->N:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    .line 768380
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->op()Lcom/facebook/graphql/model/GraphQLCurrencyAmount;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->O:Lcom/facebook/graphql/model/GraphQLCurrencyAmount;

    .line 768381
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->M()Lcom/facebook/graphql/model/GraphQLAndroidAppConfig;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->P:Lcom/facebook/graphql/model/GraphQLAndroidAppConfig;

    .line 768382
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->N()I

    move-result v1

    iput v1, v0, LX/4XR;->Q:I

    .line 768383
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->O()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->R:Ljava/lang/String;

    .line 768384
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->P()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->S:LX/0Px;

    .line 768385
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->Q()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->T:Lcom/facebook/graphql/model/GraphQLImage;

    .line 768386
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->R()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->U:Lcom/facebook/graphql/model/GraphQLImage;

    .line 768387
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->S()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->V:LX/0Px;

    .line 768388
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->T()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->W:Lcom/facebook/graphql/model/GraphQLImage;

    .line 768389
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->X:Lcom/facebook/graphql/model/GraphQLImage;

    .line 768390
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->V()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->Y:Lcom/facebook/graphql/model/GraphQLApplication;

    .line 768391
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->W()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->Z:Ljava/lang/String;

    .line 768392
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->X()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->aa:Ljava/lang/String;

    .line 768393
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->Y()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ab:Ljava/lang/String;

    .line 768394
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->Z()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ac:Ljava/lang/String;

    .line 768395
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->mE()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ad:Ljava/lang/String;

    .line 768396
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->aa()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ae:LX/0Px;

    .line 768397
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ab()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->af:LX/0Px;

    .line 768398
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ac()I

    move-result v1

    iput v1, v0, LX/4XR;->ag:I

    .line 768399
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ad()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ah:LX/0Px;

    .line 768400
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ae()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ai:Lcom/facebook/graphql/model/GraphQLStory;

    .line 768401
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->af()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->aj:LX/0Px;

    .line 768402
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ag()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ak:LX/0Px;

    .line 768403
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->lj()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->al:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 768404
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ah()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->am:Ljava/lang/String;

    .line 768405
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->lG()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->an:Lcom/facebook/graphql/model/GraphQLActor;

    .line 768406
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ai()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ao:Ljava/lang/String;

    .line 768407
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->aj()D

    move-result-wide v2

    iput-wide v2, v0, LX/4XR;->ap:D

    .line 768408
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->lT()Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->aq:Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    .line 768409
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ak()Lcom/facebook/graphql/model/GraphQLBackdatedTime;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ar:Lcom/facebook/graphql/model/GraphQLBackdatedTime;

    .line 768410
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->al()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->as:Ljava/lang/String;

    .line 768411
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->am()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->at:Ljava/lang/String;

    .line 768412
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->an()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->au:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 768413
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->nF()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->av:Lcom/facebook/graphql/model/GraphQLImage;

    .line 768414
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ao()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->aw:Lcom/facebook/graphql/model/GraphQLImage;

    .line 768415
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->pb()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ax:Ljava/lang/String;

    .line 768416
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->pc()Lcom/facebook/graphql/model/GraphQLCurrencyAmount;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ay:Lcom/facebook/graphql/model/GraphQLCurrencyAmount;

    .line 768417
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->oi()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->az:Ljava/lang/String;

    .line 768418
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ap()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->aA:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 768419
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->lt()Lcom/facebook/graphql/model/GraphQLDate;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->aB:Lcom/facebook/graphql/model/GraphQLDate;

    .line 768420
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->aq()I

    move-result v1

    iput v1, v0, LX/4XR;->aC:I

    .line 768421
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ar()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->aD:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 768422
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->as()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->aE:Ljava/lang/String;

    .line 768423
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->at()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->aF:Ljava/lang/String;

    .line 768424
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->lH()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->aG:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 768425
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->nG()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->aH:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 768426
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->au()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->aI:Ljava/lang/String;

    .line 768427
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->av()Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->aJ:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    .line 768428
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->aw()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->aK:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 768429
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ax()Lcom/facebook/graphql/enums/GraphQLMessengerCommerceBubbleType;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->aL:Lcom/facebook/graphql/enums/GraphQLMessengerCommerceBubbleType;

    .line 768430
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->mF()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->aM:Lcom/facebook/graphql/model/GraphQLActor;

    .line 768431
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ay()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->aN:Ljava/lang/String;

    .line 768432
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->az()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->aO:Ljava/lang/String;

    .line 768433
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->aA()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->aP:LX/0Px;

    .line 768434
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->aB()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->aQ:Ljava/lang/String;

    .line 768435
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->aC()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->aR:Ljava/lang/String;

    .line 768436
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->aD()Lcom/facebook/graphql/model/GraphQLFundraiserCampaign;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->aS:Lcom/facebook/graphql/model/GraphQLFundraiserCampaign;

    .line 768437
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->aE()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->aT:Ljava/lang/String;

    .line 768438
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->mG()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->aU:Z

    .line 768439
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->nH()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->aV:Z

    .line 768440
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->aF()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->aW:Z

    .line 768441
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ln()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->aX:Z

    .line 768442
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->aG()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->aY:Z

    .line 768443
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->aH()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->aZ:Z

    .line 768444
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->aI()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->ba:Z

    .line 768445
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->nI()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->bb:Z

    .line 768446
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->pJ()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->bc:Z

    .line 768447
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->aJ()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->bd:Z

    .line 768448
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->nJ()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->be:Z

    .line 768449
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->aK()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->bf:Z

    .line 768450
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->aL()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->bg:Z

    .line 768451
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->aM()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->bh:Z

    .line 768452
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->aN()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->bi:Z

    .line 768453
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->aO()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->bj:Z

    .line 768454
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->aP()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->bk:Z

    .line 768455
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->aQ()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->bl:Z

    .line 768456
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->aR()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->bm:Z

    .line 768457
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->aS()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->bn:Z

    .line 768458
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->pZ()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->bo:Z

    .line 768459
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->aT()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->bp:Z

    .line 768460
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->aU()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->bq:Z

    .line 768461
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->aV()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->br:Z

    .line 768462
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->aW()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->bs:Z

    .line 768463
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->aX()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->bt:Z

    .line 768464
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->aY()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->bu:Z

    .line 768465
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->aZ()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->bv:Z

    .line 768466
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->mH()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->bw:Z

    .line 768467
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ba()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->bx:Z

    .line 768468
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bb()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->by:Z

    .line 768469
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bc()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->bz:Z

    .line 768470
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->pK()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->bA:Z

    .line 768471
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bd()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->bB:Z

    .line 768472
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->be()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->bC:Z

    .line 768473
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bf()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->bD:Z

    .line 768474
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->mI()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->bE:Ljava/lang/String;

    .line 768475
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bg()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->bF:Ljava/lang/String;

    .line 768476
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bh()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->bG:Ljava/lang/String;

    .line 768477
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bi()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->bH:LX/0Px;

    .line 768478
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bj()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->bI:LX/0Px;

    .line 768479
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bk()Lcom/facebook/graphql/enums/GraphQLPageCategoryType;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->bJ:Lcom/facebook/graphql/enums/GraphQLPageCategoryType;

    .line 768480
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->lu()Lcom/facebook/graphql/model/GraphQLCelebrityBasicInfo;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->bK:Lcom/facebook/graphql/model/GraphQLCelebrityBasicInfo;

    .line 768481
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->lz()Lcom/facebook/graphql/model/GraphQLCharity;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->bL:Lcom/facebook/graphql/model/GraphQLCharity;

    .line 768482
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bl()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->bM:Ljava/lang/String;

    .line 768483
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bm()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->bN:Ljava/lang/String;

    .line 768484
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bn()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->bO:Lcom/facebook/graphql/model/GraphQLPage;

    .line 768485
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bo()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->bP:Ljava/lang/String;

    .line 768486
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bp()J

    move-result-wide v2

    iput-wide v2, v0, LX/4XR;->bQ:J

    .line 768487
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->og()I

    move-result v1

    iput v1, v0, LX/4XR;->bR:I

    .line 768488
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bq()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->bS:LX/0Px;

    .line 768489
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->pQ()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->bT:Ljava/lang/String;

    .line 768490
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ol()Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->bU:Lcom/facebook/graphql/model/GraphQLTimelineAppCollectionsConnection;

    .line 768491
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->pX()Lcom/facebook/graphql/enums/GraphQLPageCommPlatform;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->bV:Lcom/facebook/graphql/enums/GraphQLPageCommPlatform;

    .line 768492
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->mi()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->bW:Ljava/lang/String;

    .line 768493
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->mj()Lcom/facebook/graphql/enums/GraphQLPageCommStatus;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->bX:Lcom/facebook/graphql/enums/GraphQLPageCommStatus;

    .line 768494
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->mk()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->bY:Ljava/lang/String;

    .line 768495
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ml()Lcom/facebook/graphql/enums/GraphQLPageCommType;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->bZ:Lcom/facebook/graphql/enums/GraphQLPageCommType;

    .line 768496
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->lI()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ca:Lcom/facebook/graphql/model/GraphQLComment;

    .line 768497
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->br()Lcom/facebook/graphql/model/GraphQLCommentsConnection;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->cb:Lcom/facebook/graphql/model/GraphQLCommentsConnection;

    .line 768498
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->nK()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->cc:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 768499
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bs()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->cd:Ljava/lang/String;

    .line 768500
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->oN()Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ce:Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;

    .line 768501
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bt()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->cf:Z

    .line 768502
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bu()Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->cg:Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    .line 768503
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bv()Lcom/facebook/graphql/enums/GraphQLCommerceProductVisibility;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ch:Lcom/facebook/graphql/enums/GraphQLCommerceProductVisibility;

    .line 768504
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->mq()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ci:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    .line 768505
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->pd()J

    move-result-wide v2

    iput-wide v2, v0, LX/4XR;->cj:J

    .line 768506
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bw()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ck:Ljava/lang/String;

    .line 768507
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->qc()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->cl:Lcom/facebook/graphql/model/GraphQLPage;

    .line 768508
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->lR()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->cm:LX/0Px;

    .line 768509
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->pE()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->cn:LX/0Px;

    .line 768510
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bx()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->co:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    .line 768511
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->nL()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->cp:Ljava/lang/String;

    .line 768512
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->nM()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->cq:Lcom/facebook/graphql/model/GraphQLStory;

    .line 768513
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ly()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->cr:Ljava/lang/String;

    .line 768514
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->oq()Lcom/facebook/graphql/model/GraphQLCurrencyAmount;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->cs:Lcom/facebook/graphql/model/GraphQLCurrencyAmount;

    .line 768515
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->by()Lcom/facebook/graphql/model/GraphQLCoordinate;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ct:Lcom/facebook/graphql/model/GraphQLCoordinate;

    .line 768516
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bz()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->cu:Lcom/facebook/graphql/model/GraphQLLocation;

    .line 768517
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bA()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->cv:Ljava/lang/String;

    .line 768518
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->mJ()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->cw:LX/0Px;

    .line 768519
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->pf()I

    move-result v1

    iput v1, v0, LX/4XR;->cx:I

    .line 768520
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->lw()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->cy:Ljava/lang/String;

    .line 768521
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bB()Lcom/facebook/graphql/enums/GraphQLCouponClaimLocation;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->cz:Lcom/facebook/graphql/enums/GraphQLCouponClaimLocation;

    .line 768522
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bC()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->cA:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 768523
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bD()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->cB:Ljava/lang/String;

    .line 768524
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bE()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->cC:Lcom/facebook/graphql/model/GraphQLGroup;

    .line 768525
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bF()J

    move-result-wide v2

    iput-wide v2, v0, LX/4XR;->cD:J

    .line 768526
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bG()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->cE:Lcom/facebook/graphql/model/GraphQLStory;

    .line 768527
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bH()J

    move-result-wide v2

    iput-wide v2, v0, LX/4XR;->cF:J

    .line 768528
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bI()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->cG:Lcom/facebook/graphql/model/GraphQLActor;

    .line 768529
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bJ()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->cH:Lcom/facebook/graphql/model/GraphQLImage;

    .line 768530
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bK()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->cI:Lcom/facebook/graphql/model/GraphQLVideo;

    .line 768531
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->mK()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->cJ:Ljava/lang/String;

    .line 768532
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bL()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->cK:Ljava/lang/String;

    .line 768533
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->lF()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->cL:Lcom/facebook/graphql/model/GraphQLLocation;

    .line 768534
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bM()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->cM:Lcom/facebook/graphql/model/GraphQLLocation;

    .line 768535
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bN()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->cN:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    .line 768536
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bO()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPointsConnection;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->cO:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPointsConnection;

    .line 768537
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->nN()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->cP:Ljava/lang/String;

    .line 768538
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bP()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->cQ:Ljava/lang/String;

    .line 768539
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bQ()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->cR:Ljava/lang/String;

    .line 768540
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bR()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->cS:Ljava/lang/String;

    .line 768541
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bS()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->cT:Ljava/lang/String;

    .line 768542
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->pU()Lcom/facebook/graphql/model/GraphQLDocumentFontResource;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->cU:Lcom/facebook/graphql/model/GraphQLDocumentFontResource;

    .line 768543
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ps()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->cV:Ljava/lang/String;

    .line 768544
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->qf()D

    move-result-wide v2

    iput-wide v2, v0, LX/4XR;->cW:D

    .line 768545
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bT()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->cX:Ljava/lang/String;

    .line 768546
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bU()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->cY:Lcom/facebook/graphql/model/GraphQLLocation;

    .line 768547
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->mx()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->cZ:Ljava/lang/String;

    .line 768548
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ma()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->da:Ljava/lang/String;

    .line 768549
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->mb()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->db:Ljava/lang/String;

    .line 768550
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->pD()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->dc:LX/0Px;

    .line 768551
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->pq()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->dd:Lcom/facebook/graphql/model/GraphQLPage;

    .line 768552
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->pt()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->de:Ljava/lang/String;

    .line 768553
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bV()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->df:Ljava/lang/String;

    .line 768554
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bW()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->dg:Ljava/lang/String;

    .line 768555
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->mg()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->dh:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 768556
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->mh()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->di:Ljava/lang/String;

    .line 768557
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->oS()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->dj:Ljava/lang/String;

    .line 768558
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bX()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->dk:Ljava/lang/String;

    .line 768559
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bY()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->dl:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 768560
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->pm()Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->dm:Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

    .line 768561
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bZ()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->dn:Ljava/lang/String;

    .line 768562
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->pe()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->do:Ljava/lang/String;

    .line 768563
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ca()D

    move-result-wide v2

    iput-wide v2, v0, LX/4XR;->dp:D

    .line 768564
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cb()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->dq:Ljava/lang/String;

    .line 768565
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->lX()I

    move-result v1

    iput v1, v0, LX/4XR;->dr:I

    .line 768566
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cc()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->ds:Z

    .line 768567
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cd()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->dt:Ljava/lang/String;

    .line 768568
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ce()Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharityDonorsConnection;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->du:Lcom/facebook/graphql/model/GraphQLFundraiserPersonToCharityDonorsConnection;

    .line 768569
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->my()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->dv:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 768570
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cf()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->dw:Ljava/lang/String;

    .line 768571
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cg()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->dx:Ljava/lang/String;

    .line 768572
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ch()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->dy:Ljava/lang/String;

    .line 768573
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ci()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->dz:Ljava/lang/String;

    .line 768574
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cj()D

    move-result-wide v2

    iput-wide v2, v0, LX/4XR;->dA:D

    .line 768575
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->pw()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->dB:Ljava/lang/String;

    .line 768576
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ck()I

    move-result v1

    iput v1, v0, LX/4XR;->dC:I

    .line 768577
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cl()Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->dD:Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;

    .line 768578
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cm()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->dE:LX/0Px;

    .line 768579
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cn()Lcom/facebook/graphql/model/GraphQLEmotionalAnalysis;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->dF:Lcom/facebook/graphql/model/GraphQLEmotionalAnalysis;

    .line 768580
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->co()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->dG:Lcom/facebook/graphql/model/GraphQLPage;

    .line 768581
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->oK()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->dH:Z

    .line 768582
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->lS()J

    move-result-wide v2

    iput-wide v2, v0, LX/4XR;->dI:J

    .line 768583
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cp()J

    move-result-wide v2

    iput-wide v2, v0, LX/4XR;->dJ:J

    .line 768584
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cq()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->dK:LX/0Px;

    .line 768585
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cr()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->dL:Ljava/lang/String;

    .line 768586
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cs()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->dM:Ljava/lang/String;

    .line 768587
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ct()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->dN:Ljava/lang/String;

    .line 768588
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cu()I

    move-result v1

    iput v1, v0, LX/4XR;->dO:I

    .line 768589
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cv()I

    move-result v1

    iput v1, v0, LX/4XR;->dP:I

    .line 768590
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cw()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->dQ:Lcom/facebook/graphql/model/GraphQLEvent;

    .line 768591
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cx()Lcom/facebook/graphql/model/GraphQLEventCategoryData;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->dR:Lcom/facebook/graphql/model/GraphQLEventCategoryData;

    .line 768592
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cy()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->dS:Lcom/facebook/graphql/model/GraphQLImage;

    .line 768593
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cz()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->dT:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 768594
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cA()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->dU:Ljava/lang/String;

    .line 768595
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cB()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->dV:Lcom/facebook/graphql/model/GraphQLLocation;

    .line 768596
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cC()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->dW:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 768597
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cD()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->dX:Lcom/facebook/graphql/model/GraphQLActor;

    .line 768598
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->pj()Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->dY:Lcom/facebook/graphql/model/GraphQLEventDeclinesConnection;

    .line 768599
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cE()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->dZ:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 768600
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cF()Lcom/facebook/graphql/model/GraphQLEventHostsConnection;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ea:Lcom/facebook/graphql/model/GraphQLEventHostsConnection;

    .line 768601
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cG()Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->eb:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    .line 768602
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->pk()Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ec:Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    .line 768603
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cH()Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ed:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    .line 768604
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cI()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ee:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 768605
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cJ()Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ef:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    .line 768606
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cK()Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->eg:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    .line 768607
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cL()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->eh:Ljava/lang/String;

    .line 768608
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cM()Lcom/facebook/graphql/enums/GraphQLEventType;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ei:Lcom/facebook/graphql/enums/GraphQLEventType;

    .line 768609
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cN()Lcom/facebook/graphql/model/GraphQLEventViewerCapability;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ej:Lcom/facebook/graphql/model/GraphQLEventViewerCapability;

    .line 768610
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cO()Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ek:Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    .line 768611
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cP()Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->el:Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    .line 768612
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cQ()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->em:Ljava/lang/String;

    .line 768613
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cR()J

    move-result-wide v2

    iput-wide v2, v0, LX/4XR;->en:J

    .line 768614
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cS()J

    move-result-wide v2

    iput-wide v2, v0, LX/4XR;->eo:J

    .line 768615
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cT()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ep:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 768616
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cU()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->eq:Z

    .line 768617
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cV()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->er:Ljava/lang/String;

    .line 768618
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->pR()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->es:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 768619
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->lA()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->et:Ljava/lang/String;

    .line 768620
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cW()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->eu:Lcom/facebook/graphql/model/GraphQLImage;

    .line 768621
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cX()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ev:Ljava/lang/String;

    .line 768622
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cY()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ew:Ljava/lang/String;

    .line 768623
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cZ()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ex:Ljava/lang/String;

    .line 768624
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->da()Lcom/facebook/graphql/model/GraphQLFeedTopicContent;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ey:Lcom/facebook/graphql/model/GraphQLFeedTopicContent;

    .line 768625
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->db()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ez:Lcom/facebook/graphql/model/FeedUnit;

    .line 768626
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dc()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->eA:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 768627
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dd()Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->eB:Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    .line 768628
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->de()Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValuesConnection;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->eC:Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterValuesConnection;

    .line 768629
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->df()I

    move-result v1

    iput v1, v0, LX/4XR;->eD:I

    .line 768630
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dg()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->eE:Ljava/lang/String;

    .line 768631
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dh()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->eF:Ljava/lang/String;

    .line 768632
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->di()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->eG:Ljava/lang/String;

    .line 768633
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dj()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->eH:Ljava/lang/String;

    .line 768634
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dk()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->eI:Ljava/lang/String;

    .line 768635
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dl()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->eJ:Ljava/lang/String;

    .line 768636
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->oL()D

    move-result-wide v2

    iput-wide v2, v0, LX/4XR;->eK:D

    .line 768637
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dm()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->eL:Ljava/lang/String;

    .line 768638
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dn()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->eM:Ljava/lang/String;

    .line 768639
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->do()Lcom/facebook/graphql/model/GraphQLFollowUpFeedUnitsConnection;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->eN:Lcom/facebook/graphql/model/GraphQLFollowUpFeedUnitsConnection;

    .line 768640
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->pn()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->eO:Ljava/lang/String;

    .line 768641
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dp()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->eP:Ljava/lang/String;

    .line 768642
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dq()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->eQ:Ljava/lang/String;

    .line 768643
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dr()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->eR:Ljava/lang/String;

    .line 768644
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ds()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->eS:Ljava/lang/String;

    .line 768645
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->nO()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->eT:Ljava/lang/String;

    .line 768646
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dt()Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->eU:Lcom/facebook/graphql/model/GraphQLEventMaybesConnection;

    .line 768647
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->oC()Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->eV:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    .line 768648
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->du()Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->eW:Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    .line 768649
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->oD()Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->eX:Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    .line 768650
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dv()Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->eY:Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    .line 768651
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->pC()Lcom/facebook/graphql/model/GraphQLFundraiserFriendDonorsConnection;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->eZ:Lcom/facebook/graphql/model/GraphQLFundraiserFriendDonorsConnection;

    .line 768652
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dw()Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->fa:Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    .line 768653
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dx()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->fb:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 768654
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->mL()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->fc:Lcom/facebook/graphql/model/GraphQLActor;

    .line 768655
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dy()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->fd:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 768656
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dz()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->fe:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 768657
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dA()I

    move-result v1

    iput v1, v0, LX/4XR;->ff:I

    .line 768658
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->mM()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->fg:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 768659
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dB()Lcom/facebook/graphql/model/GraphQLExternalUrl;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->fh:Lcom/facebook/graphql/model/GraphQLExternalUrl;

    .line 768660
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dC()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->fi:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 768661
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->or()Lcom/facebook/graphql/model/GraphQLCurrencyAmount;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->fj:Lcom/facebook/graphql/model/GraphQLCurrencyAmount;

    .line 768662
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dD()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->fk:Ljava/lang/String;

    .line 768663
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dE()Lcom/facebook/graphql/model/GraphQLGreetingCardTemplate;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->fl:Lcom/facebook/graphql/model/GraphQLGreetingCardTemplate;

    .line 768664
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dF()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->fm:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 768665
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->mr()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->fn:Ljava/lang/String;

    .line 768666
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dG()Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->fo:Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;

    .line 768667
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dH()Lcom/facebook/graphql/model/GraphQLGroupOwnerAuthoredStoriesConnection;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->fp:Lcom/facebook/graphql/model/GraphQLGroupOwnerAuthoredStoriesConnection;

    .line 768668
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dI()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->fq:Lcom/facebook/graphql/model/GraphQLImage;

    .line 768669
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->pI()Lcom/facebook/graphql/model/GraphQLGroupPinnedStoriesConnection;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->fr:Lcom/facebook/graphql/model/GraphQLGroupPinnedStoriesConnection;

    .line 768670
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->mN()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->fs:Ljava/lang/String;

    .line 768671
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dJ()Lcom/facebook/graphql/model/GraphQLVideoGuidedTour;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ft:Lcom/facebook/graphql/model/GraphQLVideoGuidedTour;

    .line 768672
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dK()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->fu:Z

    .line 768673
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dL()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->fv:Z

    .line 768674
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dM()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->fw:Z

    .line 768675
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dN()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->fx:Z

    .line 768676
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->nP()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->fy:Z

    .line 768677
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dO()I

    move-result v1

    iput v1, v0, LX/4XR;->fz:I

    .line 768678
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dP()I

    move-result v1

    iput v1, v0, LX/4XR;->fA:I

    .line 768679
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dQ()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->fB:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 768680
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dR()I

    move-result v1

    iput v1, v0, LX/4XR;->fC:I

    .line 768681
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->pi()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->fD:Z

    .line 768682
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dS()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->fE:Ljava/lang/String;

    .line 768683
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->qh()Lcom/facebook/graphql/enums/GraphQLBookmarkHighlightStyle;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->fF:Lcom/facebook/graphql/enums/GraphQLBookmarkHighlightStyle;

    .line 768684
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->mc()Lcom/facebook/graphql/model/GraphQLCharity;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->fG:Lcom/facebook/graphql/model/GraphQLCharity;

    .line 768685
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->mu()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->fH:Ljava/lang/String;

    .line 768686
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dT()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->fI:LX/0Px;

    .line 768687
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->nQ()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->fJ:Lcom/facebook/graphql/model/GraphQLImage;

    .line 768688
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dU()Lcom/facebook/graphql/model/GraphQLIcon;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->fK:Lcom/facebook/graphql/model/GraphQLIcon;

    .line 768689
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dV()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->fL:Lcom/facebook/graphql/model/GraphQLImage;

    .line 768690
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->om()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->fM:Lcom/facebook/graphql/model/GraphQLImage;

    .line 768691
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->lB()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->fN:Ljava/lang/String;

    .line 768692
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->fO:Ljava/lang/String;

    .line 768693
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dX()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->fP:Lcom/facebook/graphql/model/GraphQLImage;

    .line 768694
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dY()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->fQ:Lcom/facebook/graphql/model/GraphQLImage;

    .line 768695
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dZ()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->fR:Lcom/facebook/graphql/model/GraphQLImage;

    .line 768696
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ea()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->fS:Ljava/lang/String;

    .line 768697
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->eb()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->fT:Ljava/lang/String;

    .line 768698
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ec()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->fU:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 768699
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ed()Lcom/facebook/graphql/model/GraphQLImportantReactorsConnection;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->fV:Lcom/facebook/graphql/model/GraphQLImportantReactorsConnection;

    .line 768700
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->mO()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->fW:Z

    .line 768701
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ee()I

    move-result v1

    iput v1, v0, LX/4XR;->fX:I

    .line 768702
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ef()I

    move-result v1

    iput v1, v0, LX/4XR;->fY:I

    .line 768703
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->eg()I

    move-result v1

    iput v1, v0, LX/4XR;->fZ:I

    .line 768704
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->eh()Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ga:Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    .line 768705
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ei()Lcom/facebook/graphql/model/GraphQLStoryInsights;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->gb:Lcom/facebook/graphql/model/GraphQLStoryInsights;

    .line 768706
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ej()I

    move-result v1

    iput v1, v0, LX/4XR;->gc:I

    .line 768707
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ek()Lcom/facebook/graphql/model/GraphQLInstantArticle;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->gd:Lcom/facebook/graphql/model/GraphQLInstantArticle;

    .line 768708
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->mP()Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ge:Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    .line 768709
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->el()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->gf:Z

    .line 768710
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->oe()Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->gg:Lcom/facebook/graphql/model/GraphQLInstantExperiencesSetting;

    .line 768711
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->em()Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->gh:Lcom/facebook/graphql/model/GraphQLGamesInstantPlayStyleInfo;

    .line 768712
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->oQ()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->gi:Ljava/lang/String;

    .line 768713
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->pl()Lcom/facebook/graphql/model/GraphQLPlaceListInvitedFriendsInfo;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->gj:Lcom/facebook/graphql/model/GraphQLPlaceListInvitedFriendsInfo;

    .line 768714
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->en()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->gk:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 768715
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->eo()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->gl:Ljava/lang/String;

    .line 768716
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ep()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->gm:Z

    .line 768717
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->eq()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->gn:Z

    .line 768718
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->er()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->go:Z

    .line 768719
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->mQ()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->gp:Z

    .line 768720
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->es()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->gq:Z

    .line 768721
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->et()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->gr:Z

    .line 768722
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->mR()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->gs:Z

    .line 768723
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->mS()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->gt:Z

    .line 768724
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->eu()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->gu:Z

    .line 768725
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ev()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->gv:Z

    .line 768726
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ew()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->gw:Z

    .line 768727
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ex()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->gx:Z

    .line 768728
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ey()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->gy:Z

    .line 768729
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->mT()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->gz:Z

    .line 768730
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->nR()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->gA:Z

    .line 768731
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->qi()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->gB:Z

    .line 768732
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->mU()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->gC:Z

    .line 768733
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ez()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->gD:Z

    .line 768734
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->nS()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->gE:Z

    .line 768735
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->mV()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->gF:Z

    .line 768736
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->lY()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->gG:Z

    .line 768737
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->lQ()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->gH:Z

    .line 768738
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->oJ()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->gI:Z

    .line 768739
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->oz()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->gJ:Z

    .line 768740
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->eA()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->gK:Z

    .line 768741
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->eB()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->gL:Z

    .line 768742
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->nT()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->gM:Z

    .line 768743
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->pY()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->gN:Z

    .line 768744
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->eC()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->gO:Z

    .line 768745
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->eD()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->gP:Z

    .line 768746
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->nU()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->gQ:Z

    .line 768747
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->eE()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->gR:Z

    .line 768748
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->mW()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->gS:Z

    .line 768749
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->eF()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->gT:Z

    .line 768750
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->mX()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->gU:Z

    .line 768751
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->qa()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->gV:Z

    .line 768752
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->mm()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->gW:Z

    .line 768753
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->pz()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->gX:Z

    .line 768754
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->eG()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->gY:Z

    .line 768755
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->pW()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->gZ:Z

    .line 768756
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->oj()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->ha:Z

    .line 768757
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->eH()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->hb:Z

    .line 768758
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->mY()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->hc:Z

    .line 768759
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->eI()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->hd:Z

    .line 768760
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->eJ()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->he:Z

    .line 768761
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->eK()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->hf:Z

    .line 768762
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->pB()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->hg:Z

    .line 768763
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->eL()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->hh:Z

    .line 768764
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->eM()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->hi:Z

    .line 768765
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->lv()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->hj:Z

    .line 768766
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->eN()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->hk:Z

    .line 768767
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->pg()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->hl:Z

    .line 768768
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->eO()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->hm:Z

    .line 768769
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->pL()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->hn:Z

    .line 768770
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->eP()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->ho:Z

    .line 768771
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->eQ()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->hp:Z

    .line 768772
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->nV()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->hq:Z

    .line 768773
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->eR()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->hr:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    .line 768774
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->eS()Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->hs:Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;

    .line 768775
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->mZ()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ht:Ljava/lang/String;

    .line 768776
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->eT()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->hu:Ljava/lang/String;

    .line 768777
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->eU()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->hv:Ljava/lang/String;

    .line 768778
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->mn()J

    move-result-wide v2

    iput-wide v2, v0, LX/4XR;->hw:J

    .line 768779
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->eV()Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->hx:Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    .line 768780
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->eW()Lcom/facebook/graphql/model/GraphQLLeadGenData;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->hy:Lcom/facebook/graphql/model/GraphQLLeadGenData;

    .line 768781
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->eX()Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->hz:Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;

    .line 768782
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->eY()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->hA:Ljava/lang/String;

    .line 768783
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->eZ()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->hB:Ljava/lang/String;

    .line 768784
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->lZ()Lcom/facebook/graphql/enums/GraphQLLightweightEventStatus;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->hC:Lcom/facebook/graphql/enums/GraphQLLightweightEventStatus;

    .line 768785
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->lE()Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->hD:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    .line 768786
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->oT()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->hE:LX/0Px;

    .line 768787
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fa()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->hF:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 768788
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fb()Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->hG:Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    .line 768789
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fc()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->hH:Lcom/facebook/graphql/model/GraphQLMedia;

    .line 768790
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fd()Lcom/facebook/graphql/model/GraphQLFriendListFeedConnection;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->hI:Lcom/facebook/graphql/model/GraphQLFriendListFeedConnection;

    .line 768791
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fe()Lcom/facebook/graphql/model/GraphQLPlaceListItemsFromPlaceListConnection;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->hJ:Lcom/facebook/graphql/model/GraphQLPlaceListItemsFromPlaceListConnection;

    .line 768792
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ff()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->hK:Ljava/lang/String;

    .line 768793
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->qj()Lcom/facebook/graphql/enums/GraphQLFriendListType;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->hL:Lcom/facebook/graphql/enums/GraphQLFriendListType;

    .line 768794
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->oO()I

    move-result v1

    iput v1, v0, LX/4XR;->hM:I

    .line 768795
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->oP()I

    move-result v1

    iput v1, v0, LX/4XR;->hN:I

    .line 768796
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fg()I

    move-result v1

    iput v1, v0, LX/4XR;->hO:I

    .line 768797
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fh()I

    move-result v1

    iput v1, v0, LX/4XR;->hP:I

    .line 768798
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fi()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->hQ:Ljava/lang/String;

    .line 768799
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fj()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->hR:Lcom/facebook/graphql/model/GraphQLLocation;

    .line 768800
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->po()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->hS:Ljava/lang/String;

    .line 768801
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ow()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->hT:Ljava/lang/String;

    .line 768802
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fk()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->hU:Lcom/facebook/graphql/model/GraphQLImage;

    .line 768803
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fl()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->hV:Lcom/facebook/graphql/model/GraphQLImage;

    .line 768804
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->lr()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->hW:Lcom/facebook/graphql/model/GraphQLImage;

    .line 768805
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->oW()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->hX:Ljava/lang/String;

    .line 768806
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->oX()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->hY:Ljava/lang/String;

    .line 768807
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fm()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->hZ:LX/0Px;

    .line 768808
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fn()I

    move-result v1

    iput v1, v0, LX/4XR;->ia:I

    .line 768809
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fo()Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ib:Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    .line 768810
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fp()Lcom/facebook/graphql/model/GraphQLSouvenirMediaConnection;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ic:Lcom/facebook/graphql/model/GraphQLSouvenirMediaConnection;

    .line 768811
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fq()Lcom/facebook/graphql/model/GraphQLMediaQuestionOptionsConnection;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->id:Lcom/facebook/graphql/model/GraphQLMediaQuestionOptionsConnection;

    .line 768812
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fr()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ie:LX/0Px;

    .line 768813
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fs()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->if:Ljava/lang/String;

    .line 768814
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ft()Lcom/facebook/graphql/model/GraphQLMediaSet;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ig:Lcom/facebook/graphql/model/GraphQLMediaSet;

    .line 768815
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->na()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ih:Ljava/lang/String;

    .line 768816
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fu()Lcom/facebook/graphql/model/GraphQLPageMenuInfo;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ii:Lcom/facebook/graphql/model/GraphQLPageMenuInfo;

    .line 768817
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->nb()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ij:Ljava/lang/String;

    .line 768818
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fv()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ik:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 768819
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->of()Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->il:Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;

    .line 768820
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fw()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->im:Ljava/lang/String;

    .line 768821
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fx()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->in:Ljava/lang/String;

    .line 768822
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fy()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->io:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 768823
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->lP()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ip:LX/0Px;

    .line 768824
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fz()Lcom/facebook/graphql/model/GraphQLContact;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->iq:Lcom/facebook/graphql/model/GraphQLContact;

    .line 768825
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fA()Lcom/facebook/graphql/model/GraphQLMessengerContentSubscriptionOption;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ir:Lcom/facebook/graphql/model/GraphQLMessengerContentSubscriptionOption;

    .line 768826
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->mz()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->is:Ljava/lang/String;

    .line 768827
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fB()J

    move-result-wide v2

    iput-wide v2, v0, LX/4XR;->it:J

    .line 768828
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fC()Lcom/facebook/graphql/enums/GraphQLMovieBotMovieListStyle;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->iu:Lcom/facebook/graphql/enums/GraphQLMovieBotMovieListStyle;

    .line 768829
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fD()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->iv:LX/0Px;

    .line 768830
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fE()Lcom/facebook/graphql/model/GraphQLOpenGraphObject;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->iw:Lcom/facebook/graphql/model/GraphQLOpenGraphObject;

    .line 768831
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fF()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ix:Ljava/lang/String;

    .line 768832
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fG()Lcom/facebook/graphql/enums/GraphQLMusicType;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->iy:Lcom/facebook/graphql/enums/GraphQLMusicType;

    .line 768833
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fH()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->iz:LX/0Px;

    .line 768834
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fI()Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->iA:Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;

    .line 768835
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fJ()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->iB:Ljava/lang/String;

    .line 768836
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fK()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->iC:Ljava/lang/String;

    .line 768837
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fL()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->iD:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    .line 768838
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fM()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->iE:Ljava/lang/String;

    .line 768839
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->lM()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->iF:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 768840
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->lN()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->iG:Lcom/facebook/graphql/model/GraphQLImage;

    .line 768841
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fN()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->iH:Ljava/lang/String;

    .line 768842
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fO()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->iI:Z

    .line 768843
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->oM()D

    move-result-wide v2

    iput-wide v2, v0, LX/4XR;->iJ:D

    .line 768844
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->pp()Lcom/facebook/graphql/model/GraphQLOffer;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->iK:Lcom/facebook/graphql/model/GraphQLOffer;

    .line 768845
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->pH()Lcom/facebook/graphql/model/GraphQLOfferView;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->iL:Lcom/facebook/graphql/model/GraphQLOfferView;

    .line 768846
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ox()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->iM:Ljava/lang/String;

    .line 768847
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->oR()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->iN:Ljava/lang/String;

    .line 768848
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fP()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->iO:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 768849
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fQ()Lcom/facebook/graphql/model/GraphQLOpenGraphMetadata;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->iP:Lcom/facebook/graphql/model/GraphQLOpenGraphMetadata;

    .line 768850
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fR()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->iQ:Lcom/facebook/graphql/model/GraphQLNode;

    .line 768851
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fS()Lcom/facebook/graphql/model/GraphQLQuestionOptionsConnection;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->iR:Lcom/facebook/graphql/model/GraphQLQuestionOptionsConnection;

    .line 768852
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fT()Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->iS:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 768853
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fU()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->iT:Ljava/lang/String;

    .line 768854
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->nc()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->iU:Ljava/lang/String;

    .line 768855
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fV()Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->iV:Lcom/facebook/graphql/enums/GraphQLEventTicketOrderStatus;

    .line 768856
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->nd()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->iW:Ljava/lang/String;

    .line 768857
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->os()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->iX:LX/0Px;

    .line 768858
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ms()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->iY:Lcom/facebook/graphql/model/GraphQLGroup;

    .line 768859
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fW()D

    move-result-wide v2

    iput-wide v2, v0, LX/4XR;->iZ:D

    .line 768860
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fX()Lcom/facebook/graphql/model/GraphQLRating;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ja:Lcom/facebook/graphql/model/GraphQLRating;

    .line 768861
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fY()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->jb:Lcom/facebook/graphql/model/GraphQLActor;

    .line 768862
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fZ()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->jc:Lcom/facebook/graphql/model/GraphQLPage;

    .line 768863
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ga()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->jd:Lcom/facebook/graphql/model/GraphQLPage;

    .line 768864
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->gb()Lcom/facebook/graphql/model/GraphQLPageCallToAction;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->je:Lcom/facebook/graphql/model/GraphQLPageCallToAction;

    .line 768865
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->gc()Lcom/facebook/graphql/model/GraphQLPageLikersConnection;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->jf:Lcom/facebook/graphql/model/GraphQLPageLikersConnection;

    .line 768866
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->gd()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->jg:LX/0Px;

    .line 768867
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ge()I

    move-result v1

    iput v1, v0, LX/4XR;->jh:I

    .line 768868
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->lC()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ji:Ljava/lang/String;

    .line 768869
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->gf()Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeConnection;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->jj:Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeConnection;

    .line 768870
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->gg()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->jk:Lcom/facebook/graphql/model/GraphQLGroup;

    .line 768871
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->gh()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->jl:Lcom/facebook/graphql/model/GraphQLStory;

    .line 768872
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->oY()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->jm:Ljava/lang/String;

    .line 768873
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->gi()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->jn:Lcom/facebook/graphql/model/GraphQLImage;

    .line 768874
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->gj()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->jo:Ljava/lang/String;

    .line 768875
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->gk()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->jp:Ljava/lang/String;

    .line 768876
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->gl()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->jq:Ljava/lang/String;

    .line 768877
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->lD()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->jr:Ljava/lang/String;

    .line 768878
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->px()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->js:Ljava/lang/String;

    .line 768879
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->gm()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->jt:Ljava/lang/String;

    .line 768880
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->pM()Lcom/facebook/graphql/enums/GraphQLPaymentModulesClient;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ju:Lcom/facebook/graphql/enums/GraphQLPaymentModulesClient;

    .line 768881
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->gn()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->jv:Ljava/lang/String;

    .line 768882
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->pN()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->jw:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 768883
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->pO()Lcom/facebook/graphql/model/GraphQLCurrencyAmount;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->jx:Lcom/facebook/graphql/model/GraphQLCurrencyAmount;

    .line 768884
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->oU()I

    move-result v1

    iput v1, v0, LX/4XR;->jy:I

    .line 768885
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->qd()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->jz:Lcom/facebook/graphql/model/GraphQLPage;

    .line 768886
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->oV()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->jA:LX/0Px;

    .line 768887
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->go()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->jB:LX/0Px;

    .line 768888
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->pF()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->jC:LX/0Px;

    .line 768889
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->gp()D

    move-result-wide v2

    iput-wide v2, v0, LX/4XR;->jD:D

    .line 768890
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->nW()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->jE:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 768891
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->gq()Lcom/facebook/graphql/enums/GraphQLPermanentlyClosedStatus;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->jF:Lcom/facebook/graphql/enums/GraphQLPermanentlyClosedStatus;

    .line 768892
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->nX()Lcom/facebook/graphql/model/GraphQLName;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->jG:Lcom/facebook/graphql/model/GraphQLName;

    .line 768893
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->gr()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->jH:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 768894
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->gs()Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->jI:Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    .line 768895
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->lx()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->jJ:Lcom/facebook/graphql/model/GraphQLActor;

    .line 768896
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->gt()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->jK:LX/0Px;

    .line 768897
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->gu()Lcom/facebook/graphql/model/GraphQLPhrasesAnalysis;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->jL:Lcom/facebook/graphql/model/GraphQLPhrasesAnalysis;

    .line 768898
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->gv()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->jM:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 768899
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->gw()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->jN:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 768900
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->gx()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->jO:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 768901
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->gy()Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->jP:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    .line 768902
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->gz()Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->jQ:Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;

    .line 768903
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->gA()Lcom/facebook/graphql/enums/GraphQLPlaceType;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->jR:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    .line 768904
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->gB()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->jS:Ljava/lang/String;

    .line 768905
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->gC()I

    move-result v1

    iput v1, v0, LX/4XR;->jT:I

    .line 768906
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->gD()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->jU:Ljava/lang/String;

    .line 768907
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->gE()I

    move-result v1

    iput v1, v0, LX/4XR;->jV:I

    .line 768908
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->gF()I

    move-result v1

    iput v1, v0, LX/4XR;->jW:I

    .line 768909
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->gG()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->jX:Ljava/lang/String;

    .line 768910
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->gH()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->jY:Ljava/lang/String;

    .line 768911
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->gI()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->jZ:Ljava/lang/String;

    .line 768912
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->gJ()Lcom/facebook/graphql/enums/GraphQLQuestionPollAnswersState;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ka:Lcom/facebook/graphql/enums/GraphQLQuestionPollAnswersState;

    .line 768913
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->gK()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->kb:Z

    .line 768914
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->gL()Lcom/facebook/graphql/model/GraphQLBoostedComponent;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->kc:Lcom/facebook/graphql/model/GraphQLBoostedComponent;

    .line 768915
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->nY()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->kd:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 768916
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->gM()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ke:Ljava/lang/String;

    .line 768917
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->md()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->kf:Ljava/lang/String;

    .line 768918
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->me()J

    move-result-wide v2

    iput-wide v2, v0, LX/4XR;->kg:J

    .line 768919
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->mf()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->kh:Ljava/lang/String;

    .line 768920
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->nZ()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ki:Lcom/facebook/graphql/model/GraphQLImage;

    .line 768921
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->gN()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->kj:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 768922
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->gO()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->kk:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 768923
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->gP()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->kl:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 768924
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->gQ()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->km:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 768925
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->gR()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->kn:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 768926
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->gS()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ko:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 768927
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->gT()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->kp:Lcom/facebook/graphql/model/GraphQLImage;

    .line 768928
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->gU()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->kq:LX/0Px;

    .line 768929
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ne()I

    move-result v1

    iput v1, v0, LX/4XR;->kr:I

    .line 768930
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->gV()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ks:Ljava/lang/String;

    .line 768931
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->gW()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->kt:Ljava/lang/String;

    .line 768932
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->gX()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ku:Ljava/lang/String;

    .line 768933
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->gY()Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->kv:Lcom/facebook/graphql/enums/GraphQLGroupCommercePriceType;

    .line 768934
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->lL()Lcom/facebook/graphql/model/GraphQLPageActionChannel;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->kw:Lcom/facebook/graphql/model/GraphQLPageActionChannel;

    .line 768935
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->gZ()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->kx:Ljava/lang/String;

    .line 768936
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ha()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ky:Lcom/facebook/graphql/model/GraphQLImage;

    .line 768937
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hb()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->kz:Lcom/facebook/graphql/model/GraphQLNode;

    .line 768938
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->mt()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->kA:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 768939
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hc()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->kB:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 768940
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hd()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->kC:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 768941
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->he()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->kD:Ljava/lang/String;

    .line 768942
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->oA()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->kE:LX/0Px;

    .line 768943
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hf()Lcom/facebook/graphql/model/GraphQLProductItem;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->kF:Lcom/facebook/graphql/model/GraphQLProductItem;

    .line 768944
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ot()Lcom/facebook/graphql/model/GraphQLCurrencyAmount;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->kG:Lcom/facebook/graphql/model/GraphQLCurrencyAmount;

    .line 768945
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hg()D

    move-result-wide v2

    iput-wide v2, v0, LX/4XR;->kH:D

    .line 768946
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hh()D

    move-result-wide v2

    iput-wide v2, v0, LX/4XR;->kI:D

    .line 768947
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hi()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->kJ:Lcom/facebook/graphql/model/GraphQLImage;

    .line 768948
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->kK:Lcom/facebook/graphql/model/GraphQLImage;

    .line 768949
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hk()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->kL:Lcom/facebook/graphql/model/GraphQLImage;

    .line 768950
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hl()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->kM:Lcom/facebook/graphql/model/GraphQLImage;

    .line 768951
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ll()Lcom/facebook/graphql/model/GraphQLPageActionChannel;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->kN:Lcom/facebook/graphql/model/GraphQLPageActionChannel;

    .line 768952
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->lO()Lcom/facebook/graphql/model/GraphQLPageActionChannel;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->kO:Lcom/facebook/graphql/model/GraphQLPageActionChannel;

    .line 768953
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->qb()Lcom/facebook/graphql/model/GraphQLProfileDiscoveryBucket;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->kP:Lcom/facebook/graphql/model/GraphQLProfileDiscoveryBucket;

    .line 768954
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hm()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->kQ:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 768955
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hn()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->kR:Lcom/facebook/graphql/model/GraphQLImage;

    .line 768956
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ho()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->kS:Z

    .line 768957
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hp()Lcom/facebook/graphql/model/GraphQLProfileVideo;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->kT:Lcom/facebook/graphql/model/GraphQLProfileVideo;

    .line 768958
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hq()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->kU:Ljava/lang/String;

    .line 768959
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hr()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->kV:Ljava/lang/String;

    .line 768960
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hs()Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->kW:Lcom/facebook/graphql/model/GraphQLPagePostPromotionInfo;

    .line 768961
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->nf()Lcom/facebook/graphql/model/GraphQLComposedDocument;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->kX:Lcom/facebook/graphql/model/GraphQLComposedDocument;

    .line 768962
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ht()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->kY:Ljava/lang/String;

    .line 768963
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hu()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->kZ:Ljava/lang/String;

    .line 768964
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hv()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->la:Ljava/lang/String;

    .line 768965
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hw()Lcom/facebook/graphql/model/GraphQLGraphSearchQueryTitle;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->lb:Lcom/facebook/graphql/model/GraphQLGraphSearchQueryTitle;

    .line 768966
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hx()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->lc:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 768967
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hy()Lcom/facebook/graphql/model/GraphQLQuotesAnalysis;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ld:Lcom/facebook/graphql/model/GraphQLQuotesAnalysis;

    .line 768968
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->lU()Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->le:Lcom/facebook/graphql/model/GraphQLRapidReportingPrompt;

    .line 768969
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hz()Lcom/facebook/graphql/model/GraphQLRating;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->lf:Lcom/facebook/graphql/model/GraphQLRating;

    .line 768970
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ng()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->lg:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 768971
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hA()Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->lh:Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    .line 768972
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->oa()Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->li:Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;

    .line 768973
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->nh()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->lj:Lcom/facebook/graphql/model/GraphQLUser;

    .line 768974
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ni()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->lk:Ljava/lang/String;

    .line 768975
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hB()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ll:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 768976
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ls()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->lm:Ljava/lang/String;

    .line 768977
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hC()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ln:Lcom/facebook/graphql/model/GraphQLUser;

    .line 768978
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->nj()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->lo:Ljava/lang/String;

    .line 768979
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hD()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->lp:Lcom/facebook/graphql/model/GraphQLImage;

    .line 768980
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hE()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->lq:Ljava/lang/String;

    .line 768981
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hF()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->lr:Ljava/lang/String;

    .line 768982
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hG()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ls:LX/0Px;

    .line 768983
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->oF()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->lt:Ljava/lang/String;

    .line 768984
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hH()Lcom/facebook/graphql/model/GraphQLSticker;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->lu:Lcom/facebook/graphql/model/GraphQLSticker;

    .line 768985
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hI()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->lv:Ljava/lang/String;

    .line 768986
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hJ()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->lw:Lcom/facebook/graphql/model/GraphQLActor;

    .line 768987
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->nk()Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->lx:Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

    .line 768988
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->lm()J

    move-result-wide v2

    iput-wide v2, v0, LX/4XR;->ly:J

    .line 768989
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hK()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->lz:Lcom/facebook/graphql/model/GraphQLActor;

    .line 768990
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hL()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->lA:Lcom/facebook/graphql/model/GraphQLActor;

    .line 768991
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ob()Lcom/facebook/graphql/model/GraphQLResharesOfContentConnection;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->lB:Lcom/facebook/graphql/model/GraphQLResharesOfContentConnection;

    .line 768992
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hM()Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->lC:Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;

    .line 768993
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hN()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->lD:Ljava/lang/String;

    .line 768994
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hO()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->lE:Ljava/lang/String;

    .line 768995
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hP()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->lF:Ljava/lang/String;

    .line 768996
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hQ()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->lG:Lcom/facebook/graphql/model/GraphQLStory;

    .line 768997
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hR()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->lH:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    .line 768998
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hS()Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->lI:Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    .line 768999
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hT()Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->lJ:Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    .line 769000
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hU()J

    move-result-wide v2

    iput-wide v2, v0, LX/4XR;->lK:J

    .line 769001
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hV()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->lL:Lcom/facebook/graphql/model/GraphQLPage;

    .line 769002
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hW()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->lM:Lcom/facebook/graphql/model/GraphQLPage;

    .line 769003
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->lp()Lcom/facebook/graphql/model/GraphQLSearchElectionAllData;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->lN:Lcom/facebook/graphql/model/GraphQLSearchElectionAllData;

    .line 769004
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hX()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->lO:Ljava/lang/String;

    .line 769005
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hY()Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->lP:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    .line 769006
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->mC()I

    move-result v1

    iput v1, v0, LX/4XR;->lQ:I

    .line 769007
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->lK()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->lR:Ljava/lang/String;

    .line 769008
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hZ()Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->lS:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    .line 769009
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ia()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->lT:Ljava/lang/String;

    .line 769010
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ib()Lcom/facebook/graphql/model/GraphQLSeenByConnection;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->lU:Lcom/facebook/graphql/model/GraphQLSeenByConnection;

    .line 769011
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ic()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->lV:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    .line 769012
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->id()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->lW:Ljava/lang/String;

    .line 769013
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->nl()Lcom/facebook/graphql/model/GraphQLMailingAddress;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->lX:Lcom/facebook/graphql/model/GraphQLMailingAddress;

    .line 769014
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ie()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->lY:Lcom/facebook/graphql/model/GraphQLActor;

    .line 769015
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->if()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->lZ:Ljava/lang/String;

    .line 769016
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ig()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ma:Ljava/lang/String;

    .line 769017
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ih()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->mb:Lcom/facebook/graphql/model/GraphQLUser;

    .line 769018
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ii()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->mc:Ljava/lang/String;

    .line 769019
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->oZ()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->md:Ljava/lang/String;

    .line 769020
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ij()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->me:Ljava/lang/String;

    .line 769021
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ik()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->mf:Ljava/lang/String;

    .line 769022
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->il()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->mg:Lcom/facebook/graphql/model/GraphQLStory;

    .line 769023
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->im()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->mh:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 769024
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->in()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->mi:Ljava/lang/String;

    .line 769025
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->nm()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->mj:Ljava/lang/String;

    .line 769026
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->nn()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->mk:Ljava/lang/String;

    .line 769027
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->io()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ml:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 769028
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ip()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->mm:LX/0Px;

    .line 769029
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->iq()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->mn:Ljava/lang/String;

    .line 769030
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ir()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->mo:Ljava/lang/String;

    .line 769031
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->pA()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->mp:Z

    .line 769032
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->is()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->mq:Z

    .line 769033
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->it()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->mr:Z

    .line 769034
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->iu()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->ms:Z

    .line 769035
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->no()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->mt:Z

    .line 769036
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->iv()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->mu:Z

    .line 769037
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->iw()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->mv:Z

    .line 769038
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ix()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->mw:Z

    .line 769039
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->iy()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->mx:Z

    .line 769040
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->iz()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->my:Z

    .line 769041
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->iA()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->mz:Z

    .line 769042
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->np()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->mA:Z

    .line 769043
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->nq()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->mB:Z

    .line 769044
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->nr()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->mC:Z

    .line 769045
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->iB()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->mD:Z

    .line 769046
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->lk()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->mE:Z

    .line 769047
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->iC()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->mF:Z

    .line 769048
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->iD()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->mG:Z

    .line 769049
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->iE()Lcom/facebook/graphql/model/GraphQLGreetingCardSlidesConnection;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->mH:Lcom/facebook/graphql/model/GraphQLGreetingCardSlidesConnection;

    .line 769050
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->oc()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->mI:Lcom/facebook/graphql/model/GraphQLImage;

    .line 769051
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->iF()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->mJ:Ljava/lang/String;

    .line 769052
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->mv()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->mK:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 769053
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->iG()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->mL:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 769054
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->iH()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->mM:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 769055
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->iI()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->mN:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 769056
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->iJ()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->mO:Ljava/lang/String;

    .line 769057
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->iK()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->mP:Lcom/facebook/graphql/model/GraphQLLocation;

    .line 769058
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->iL()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->mQ:Ljava/lang/String;

    .line 769059
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->iM()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->mR:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 769060
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->pT()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->mS:Ljava/lang/String;

    .line 769061
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->iN()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->mT:Ljava/lang/String;

    .line 769062
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->iO()D

    move-result-wide v2

    iput-wide v2, v0, LX/4XR;->mU:D

    .line 769063
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->iP()D

    move-result-wide v2

    iput-wide v2, v0, LX/4XR;->mV:D

    .line 769064
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->iQ()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->mW:Ljava/lang/String;

    .line 769065
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->iR()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->mX:Ljava/lang/String;

    .line 769066
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->iS()I

    move-result v1

    iput v1, v0, LX/4XR;->mY:I

    .line 769067
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->iT()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->mZ:Ljava/lang/String;

    .line 769068
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->iU()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->na:Ljava/lang/String;

    .line 769069
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->iV()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->nb:Lcom/facebook/graphql/model/GraphQLSponsoredData;

    .line 769070
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->iW()Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->nc:Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;

    .line 769071
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->iX()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->nd:Lcom/facebook/graphql/model/GraphQLImage;

    .line 769072
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->on()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ne:Ljava/lang/String;

    .line 769073
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->iY()J

    move-result-wide v2

    iput-wide v2, v0, LX/4XR;->nf:J

    .line 769074
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->iZ()J

    move-result-wide v2

    iput-wide v2, v0, LX/4XR;->ng:J

    .line 769075
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ja()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->nh:Ljava/lang/String;

    .line 769076
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->jb()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ni:Ljava/lang/String;

    .line 769077
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->jc()Lcom/facebook/graphql/enums/GraphQLMessengerRetailItemStatus;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->nj:Lcom/facebook/graphql/enums/GraphQLMessengerRetailItemStatus;

    .line 769078
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ns()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->nk:Lcom/facebook/graphql/model/GraphQLImage;

    .line 769079
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->jd()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->nl:Lcom/facebook/graphql/model/GraphQLStory;

    .line 769080
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->je()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->nm:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 769081
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->jf()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->nn:Lcom/facebook/graphql/model/GraphQLStoryHeader;

    .line 769082
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->jg()Lcom/facebook/graphql/model/GraphQLName;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->no:Lcom/facebook/graphql/model/GraphQLName;

    .line 769083
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->jh()Lcom/facebook/graphql/model/GraphQLStructuredSurvey;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->np:Lcom/facebook/graphql/model/GraphQLStructuredSurvey;

    .line 769084
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->nt()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->nq:LX/0Px;

    .line 769085
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ji()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->nr:Ljava/lang/String;

    .line 769086
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->jj()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ns:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 769087
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->jk()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->nt:LX/0Px;

    .line 769088
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->jl()I

    move-result v1

    iput v1, v0, LX/4XR;->nu:I

    .line 769089
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->oo()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->nv:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 769090
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->nu()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->nw:Ljava/lang/String;

    .line 769091
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->jm()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->nx:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 769092
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->jn()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ny:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 769093
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ph()Lcom/facebook/graphql/model/GraphQLTimeRange;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->nz:Lcom/facebook/graphql/model/GraphQLTimeRange;

    .line 769094
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->py()Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnection;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->nA:Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnection;

    .line 769095
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->jo()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->nB:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 769096
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->jp()Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->nC:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 769097
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->jq()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->nD:Lcom/facebook/graphql/model/GraphQLStory;

    .line 769098
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->jr()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->nE:LX/0Px;

    .line 769099
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->nv()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->nF:Z

    .line 769100
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->js()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->nG:Ljava/lang/String;

    .line 769101
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->pG()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->nH:Lcom/facebook/graphql/model/GraphQLGroup;

    .line 769102
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->mo()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->nI:Ljava/lang/String;

    .line 769103
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->jt()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->nJ:Ljava/lang/String;

    .line 769104
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->nw()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->nK:Ljava/lang/String;

    .line 769105
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ju()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->nL:Ljava/lang/String;

    .line 769106
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->nx()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->nM:LX/0Px;

    .line 769107
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->jv()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->nN:Ljava/lang/String;

    .line 769108
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->jw()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->nO:Ljava/lang/String;

    .line 769109
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->oH()Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->nP:Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;

    .line 769110
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->jx()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->nQ:Ljava/lang/String;

    .line 769111
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->jy()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->nR:Lcom/facebook/graphql/model/GraphQLImage;

    .line 769112
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->jz()Lcom/facebook/graphql/model/GraphQLOpenGraphMetadata;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->nS:Lcom/facebook/graphql/model/GraphQLOpenGraphMetadata;

    .line 769113
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->jA()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->nT:Ljava/lang/String;

    .line 769114
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->jB()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->nU:Lcom/facebook/graphql/model/GraphQLImage;

    .line 769115
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->pS()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->nV:Ljava/lang/String;

    .line 769116
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->jC()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->nW:LX/0Px;

    .line 769117
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->jD()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->nX:LX/0Px;

    .line 769118
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->mp()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->nY:Lcom/facebook/graphql/model/GraphQLImage;

    .line 769119
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ny()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->nZ:Lcom/facebook/graphql/model/GraphQLImage;

    .line 769120
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->jE()I

    move-result v1

    iput v1, v0, LX/4XR;->oa:I

    .line 769121
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->pP()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ob:LX/0Px;

    .line 769122
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->mB()J

    move-result-wide v2

    iput-wide v2, v0, LX/4XR;->oc:J

    .line 769123
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->jF()Lcom/facebook/graphql/model/GraphQLEventTimeRange;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->od:Lcom/facebook/graphql/model/GraphQLEventTimeRange;

    .line 769124
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->jG()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->oe:Ljava/lang/String;

    .line 769125
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->jH()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->of:Lcom/facebook/graphql/model/GraphQLStory;

    .line 769126
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->nz()Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsConnection;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->og:Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsConnection;

    .line 769127
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->jI()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->oh:Ljava/lang/String;

    .line 769128
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->jJ()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->oi:Ljava/lang/String;

    .line 769129
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->jK()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->oj:Lcom/facebook/graphql/model/GraphQLImage;

    .line 769130
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->jL()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ok:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 769131
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->jM()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ol:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 769132
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->jN()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->om:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 769133
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->pV()Lcom/facebook/graphql/model/GraphQLDocumentFontResource;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->on:Lcom/facebook/graphql/model/GraphQLDocumentFontResource;

    .line 769134
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->pu()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->oo:Ljava/lang/String;

    .line 769135
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->qg()D

    move-result-wide v2

    iput-wide v2, v0, LX/4XR;->op:D

    .line 769136
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->jO()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->oq:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 769137
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->jP()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->or:Lcom/facebook/graphql/model/GraphQLNode;

    .line 769138
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->jQ()Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->os:Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    .line 769139
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->jR()Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ot:Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;

    .line 769140
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->jS()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ou:Lcom/facebook/graphql/model/GraphQLImage;

    .line 769141
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->nA()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ov:Ljava/lang/String;

    .line 769142
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ou()Lcom/facebook/graphql/model/GraphQLCurrencyAmount;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ow:Lcom/facebook/graphql/model/GraphQLCurrencyAmount;

    .line 769143
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->jT()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->ox:Ljava/lang/String;

    .line 769144
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ov()Lcom/facebook/graphql/model/GraphQLCurrencyAmount;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->oy:Lcom/facebook/graphql/model/GraphQLCurrencyAmount;

    .line 769145
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->jU()I

    move-result v1

    iput v1, v0, LX/4XR;->oz:I

    .line 769146
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->jV()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->oA:Ljava/lang/String;

    .line 769147
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->jW()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->oB:Ljava/lang/String;

    .line 769148
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->jX()I

    move-result v1

    iput v1, v0, LX/4XR;->oC:I

    .line 769149
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->jY()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->oD:Ljava/lang/String;

    .line 769150
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->jZ()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->oE:Ljava/lang/String;

    .line 769151
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ka()Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->oF:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

    .line 769152
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kb()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->oG:Ljava/lang/String;

    .line 769153
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kc()I

    move-result v1

    iput v1, v0, LX/4XR;->oH:I

    .line 769154
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kd()I

    move-result v1

    iput v1, v0, LX/4XR;->oI:I

    .line 769155
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ke()Lcom/facebook/graphql/model/GraphQLPostTranslatability;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->oJ:Lcom/facebook/graphql/model/GraphQLPostTranslatability;

    .line 769156
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kf()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->oK:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 769157
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kg()Lcom/facebook/graphql/model/GraphQLTranslation;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->oL:Lcom/facebook/graphql/model/GraphQLTranslation;

    .line 769158
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kh()Lcom/facebook/graphql/model/GraphQLTrendingTopicData;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->oM:Lcom/facebook/graphql/model/GraphQLTrendingTopicData;

    .line 769159
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ki()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->oN:Ljava/lang/String;

    .line 769160
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kj()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->oO:Ljava/lang/String;

    .line 769161
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kk()I

    move-result v1

    iput v1, v0, LX/4XR;->oP:I

    .line 769162
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->pr()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->oQ:Z

    .line 769163
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kl()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->oR:Ljava/lang/String;

    .line 769164
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->km()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->oS:Ljava/lang/String;

    .line 769165
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->nB()J

    move-result-wide v2

    iput-wide v2, v0, LX/4XR;->oT:J

    .line 769166
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kn()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->oU:Ljava/lang/String;

    .line 769167
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ko()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->oV:Lcom/facebook/graphql/model/GraphQLUser;

    .line 769168
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->lq()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->oW:Ljava/lang/String;

    .line 769169
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->mA()Lcom/facebook/graphql/model/GraphQLFundraiserDonorsConnection;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->oX:Lcom/facebook/graphql/model/GraphQLFundraiserDonorsConnection;

    .line 769170
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kp()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->oY:Ljava/lang/String;

    .line 769171
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kq()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->oZ:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 769172
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kr()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->pa:Ljava/lang/String;

    .line 769173
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ks()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->pb:Ljava/lang/String;

    .line 769174
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kt()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->pc:Ljava/lang/String;

    .line 769175
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ku()Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->pd:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    .line 769176
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kv()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->pe:Lcom/facebook/graphql/model/GraphQLActor;

    .line 769177
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kw()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->pf:LX/0Px;

    .line 769178
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kx()Lcom/facebook/graphql/model/GraphQLVideoChannel;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->pg:Lcom/facebook/graphql/model/GraphQLVideoChannel;

    .line 769179
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->lo()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->ph:Z

    .line 769180
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ky()I

    move-result v1

    iput v1, v0, LX/4XR;->pi:I

    .line 769181
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kz()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->pj:Ljava/lang/String;

    .line 769182
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kA()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->pk:Ljava/lang/String;

    .line 769183
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kB()Lcom/facebook/graphql/model/GraphQLVideoShare;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->pl:Lcom/facebook/graphql/model/GraphQLVideoShare;

    .line 769184
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kC()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->pm:LX/0Px;

    .line 769185
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kD()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->pn:Ljava/lang/String;

    .line 769186
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kE()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->po:Ljava/lang/String;

    .line 769187
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kF()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->pp:Lcom/facebook/graphql/model/GraphQLPage;

    .line 769188
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kG()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->pq:Lcom/facebook/graphql/model/GraphQLUser;

    .line 769189
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->oB()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->pr:Lcom/facebook/graphql/model/GraphQLUser;

    .line 769190
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->qk()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->ps:Z

    .line 769191
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ok()Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->pt:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    .line 769192
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kH()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->pu:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 769193
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kI()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->pv:LX/0Px;

    .line 769194
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->lV()Lcom/facebook/graphql/model/GraphQLFeedbackReaction;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->pw:Lcom/facebook/graphql/model/GraphQLFeedbackReaction;

    .line 769195
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kJ()I

    move-result v1

    iput v1, v0, LX/4XR;->px:I

    .line 769196
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kK()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->py:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 769197
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kL()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->pz:Z

    .line 769198
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kM()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->pA:Z

    .line 769199
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kN()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->pB:LX/0Px;

    .line 769200
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kO()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->pC:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 769201
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->qe()I

    move-result v1

    iput v1, v0, LX/4XR;->pD:I

    .line 769202
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kP()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->pE:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 769203
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kQ()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->pF:LX/0Px;

    .line 769204
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kR()Z

    move-result v1

    iput-boolean v1, v0, LX/4XR;->pG:Z

    .line 769205
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kS()Lcom/facebook/graphql/model/GraphQLContactRecommendationField;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->pH:Lcom/facebook/graphql/model/GraphQLContactRecommendationField;

    .line 769206
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kT()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->pI:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 769207
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kU()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->pJ:LX/0Px;

    .line 769208
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kV()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->pK:LX/0Px;

    .line 769209
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kW()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->pL:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 769210
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kX()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->pM:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    .line 769211
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kY()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->pN:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 769212
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->oI()Lcom/facebook/graphql/model/GraphQLVoiceSwitcherActorsConnection;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->pO:Lcom/facebook/graphql/model/GraphQLVoiceSwitcherActorsConnection;

    .line 769213
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kZ()Lcom/facebook/graphql/model/GraphQLVoiceSwitcherPagesConnection;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->pP:Lcom/facebook/graphql/model/GraphQLVoiceSwitcherPagesConnection;

    .line 769214
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->od()Lcom/facebook/graphql/model/GraphQLQuestionOptionVotersConnection;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->pQ:Lcom/facebook/graphql/model/GraphQLQuestionOptionVotersConnection;

    .line 769215
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->la()Lcom/facebook/graphql/model/GraphQLWeatherCondition;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->pR:Lcom/facebook/graphql/model/GraphQLWeatherCondition;

    .line 769216
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->lb()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->pS:LX/0Px;

    .line 769217
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->li()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->pT:Ljava/lang/String;

    .line 769218
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->lc()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->pU:LX/0Px;

    .line 769219
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ld()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->pV:Ljava/lang/String;

    .line 769220
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->le()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->pW:Ljava/lang/String;

    .line 769221
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->lf()I

    move-result v1

    iput v1, v0, LX/4XR;->pX:I

    .line 769222
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->lg()Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->pY:Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    .line 769223
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->lh()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->pZ:Lcom/facebook/graphql/model/GraphQLPage;

    .line 769224
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->oy()Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->qa:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    .line 769225
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->nC()I

    move-result v1

    iput v1, v0, LX/4XR;->qb:I

    .line 769226
    invoke-static {v0, p0}, LX/0ur;->a(LX/0ur;Lcom/facebook/graphql/modelutil/BaseModel;)V

    .line 769227
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    iput-object v1, v0, LX/4XR;->qc:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 769228
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)LX/4XR;
    .locals 0

    .prologue
    .line 768337
    iput-object p1, p0, LX/4XR;->fb:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 768338
    return-object p0
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLObjectType;)LX/4XR;
    .locals 0
    .param p1    # Lcom/facebook/graphql/enums/GraphQLObjectType;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 768335
    iput-object p1, p0, LX/4XR;->qc:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 768336
    return-object p0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLNode;
    .locals 2

    .prologue
    .line 768333
    new-instance v0, Lcom/facebook/graphql/model/GraphQLNode;

    invoke-direct {v0, p0}, Lcom/facebook/graphql/model/GraphQLNode;-><init>(LX/4XR;)V

    .line 768334
    return-object v0
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)LX/4XR;
    .locals 0
    .param p1    # Lcom/facebook/graphql/model/GraphQLTextWithEntities;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 768321
    iput-object p1, p0, LX/4XR;->aA:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 768322
    return-object p0
.end method

.method public final e(Ljava/lang/String;)LX/4XR;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 768331
    iput-object p1, p0, LX/4XR;->fO:Ljava/lang/String;

    .line 768332
    return-object p0
.end method

.method public final f(Lcom/facebook/graphql/model/GraphQLImage;)LX/4XR;
    .locals 0
    .param p1    # Lcom/facebook/graphql/model/GraphQLImage;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 768329
    iput-object p1, p0, LX/4XR;->kR:Lcom/facebook/graphql/model/GraphQLImage;

    .line 768330
    return-object p0
.end method

.method public final g(Ljava/lang/String;)LX/4XR;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 768327
    iput-object p1, p0, LX/4XR;->iB:Ljava/lang/String;

    .line 768328
    return-object p0
.end method

.method public final h(Ljava/lang/String;)LX/4XR;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 768325
    iput-object p1, p0, LX/4XR;->oe:Ljava/lang/String;

    .line 768326
    return-object p0
.end method

.method public final m(Z)LX/4XR;
    .locals 0

    .prologue
    .line 768323
    iput-boolean p1, p0, LX/4XR;->hi:Z

    .line 768324
    return-object p0
.end method
