.class public abstract LX/4pZ;
.super Ljava/io/Reader;
.source ""


# instance fields
.field public final a:LX/12A;

.field public b:Ljava/io/InputStream;

.field public c:[B

.field public d:I

.field public e:I

.field public f:[C


# direct methods
.method public constructor <init>(LX/12A;Ljava/io/InputStream;[BII)V
    .locals 1

    .prologue
    .line 811441
    invoke-direct {p0}, Ljava/io/Reader;-><init>()V

    .line 811442
    const/4 v0, 0x0

    iput-object v0, p0, LX/4pZ;->f:[C

    .line 811443
    iput-object p1, p0, LX/4pZ;->a:LX/12A;

    .line 811444
    iput-object p2, p0, LX/4pZ;->b:Ljava/io/InputStream;

    .line 811445
    iput-object p3, p0, LX/4pZ;->c:[B

    .line 811446
    iput p4, p0, LX/4pZ;->d:I

    .line 811447
    iput p5, p0, LX/4pZ;->e:I

    .line 811448
    return-void
.end method

.method public static a([CII)V
    .locals 3

    .prologue
    .line 811449
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "read(buf,"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "), cbuf["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    array-length v2, p0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static b()V
    .locals 2

    .prologue
    .line 811450
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Strange I/O stream, returned 0 bytes on read"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 811451
    iget-object v0, p0, LX/4pZ;->c:[B

    .line 811452
    if-eqz v0, :cond_0

    .line 811453
    const/4 v1, 0x0

    iput-object v1, p0, LX/4pZ;->c:[B

    .line 811454
    iget-object v1, p0, LX/4pZ;->a:LX/12A;

    invoke-virtual {v1, v0}, LX/12A;->a([B)V

    .line 811455
    :cond_0
    return-void
.end method

.method public close()V
    .locals 2

    .prologue
    .line 811456
    iget-object v0, p0, LX/4pZ;->b:Ljava/io/InputStream;

    .line 811457
    if-eqz v0, :cond_0

    .line 811458
    const/4 v1, 0x0

    iput-object v1, p0, LX/4pZ;->b:Ljava/io/InputStream;

    .line 811459
    invoke-virtual {p0}, LX/4pZ;->a()V

    .line 811460
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 811461
    :cond_0
    return-void
.end method

.method public read()I
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 811462
    iget-object v0, p0, LX/4pZ;->f:[C

    if-nez v0, :cond_0

    .line 811463
    new-array v0, v2, [C

    iput-object v0, p0, LX/4pZ;->f:[C

    .line 811464
    :cond_0
    iget-object v0, p0, LX/4pZ;->f:[C

    invoke-virtual {p0, v0, v1, v2}, LX/4pZ;->read([CII)I

    move-result v0

    if-gtz v0, :cond_1

    .line 811465
    const/4 v0, -0x1

    .line 811466
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, LX/4pZ;->f:[C

    aget-char v0, v0, v1

    goto :goto_0
.end method
