.class public LX/3TL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1jv;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1jv",
        "<",
        "Ljava/util/List",
        "<",
        "LX/2nq;",
        ">;>;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private b:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

.field private c:LX/1qv;

.field private d:LX/0o6;

.field private e:Lcom/facebook/auth/viewercontext/ViewerContext;

.field private f:Landroid/content/Context;

.field private g:Landroid/net/Uri;

.field private h:I

.field private i:Ljava/lang/String;

.field public j:Z

.field private k:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 584097
    const-class v0, LX/3TL;

    sput-object v0, LX/3TL;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;LX/1qv;)V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 584098
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 584099
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/3TL;->k:Z

    .line 584100
    iput-object p1, p0, LX/3TL;->b:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    .line 584101
    iput-object p2, p0, LX/3TL;->c:LX/1qv;

    .line 584102
    return-void
.end method

.method public static a(LX/0QB;)LX/3TL;
    .locals 3

    .prologue
    .line 584103
    new-instance v2, LX/3TL;

    invoke-static {p0}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a(LX/0QB;)Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    invoke-static {p0}, LX/1qv;->b(LX/0QB;)LX/1qv;

    move-result-object v1

    check-cast v1, LX/1qv;

    invoke-direct {v2, v0, v1}, LX/3TL;-><init>(Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;LX/1qv;)V

    .line 584104
    move-object v0, v2

    .line 584105
    return-object v0
.end method


# virtual methods
.method public final a(I)LX/0k9;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/0k9",
            "<",
            "Ljava/util/List",
            "<",
            "LX/2nq;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 584106
    iget-object v0, p0, LX/3TL;->e:Lcom/facebook/auth/viewercontext/ViewerContext;

    if-eqz v0, :cond_1

    const/16 v0, 0x64

    if-ne p1, v0, :cond_1

    .line 584107
    new-instance v0, Lcom/facebook/notifications/loader/NotificationsLoader;

    iget-object v1, p0, LX/3TL;->f:Landroid/content/Context;

    iget-object v2, p0, LX/3TL;->b:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    iget-object v3, p0, LX/3TL;->f:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    iget-object v4, p0, LX/3TL;->g:Landroid/net/Uri;

    iget-object v5, p0, LX/3TL;->e:Lcom/facebook/auth/viewercontext/ViewerContext;

    iget v6, p0, LX/3TL;->h:I

    iget-object v7, p0, LX/3TL;->c:LX/1qv;

    invoke-direct/range {v0 .. v7}, Lcom/facebook/notifications/loader/NotificationsLoader;-><init>(Landroid/content/Context;Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;Landroid/content/ContentResolver;Landroid/net/Uri;Lcom/facebook/auth/viewercontext/ViewerContext;ILX/1qv;)V

    .line 584108
    iget-object v1, p0, LX/3TL;->i:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 584109
    iget-object v1, p0, LX/3TL;->i:Ljava/lang/String;

    .line 584110
    invoke-static {v1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v2

    iput-object v2, v0, Lcom/facebook/notifications/loader/NotificationsLoader;->f:LX/0am;

    .line 584111
    :cond_0
    :goto_0
    return-object v0

    .line 584112
    :cond_1
    sget-object v0, LX/3TL;->a:Ljava/lang/Class;

    const-string v1, "Unexpected onCreateLoader: %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 584113
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 584114
    iget-object v0, p0, LX/3TL;->d:LX/0o6;

    invoke-interface {v0}, LX/0o6;->kn_()V

    .line 584115
    return-void
.end method

.method public final a(LX/0k9;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 584116
    check-cast p2, Ljava/util/List;

    .line 584117
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/3TL;->j:Z

    .line 584118
    invoke-static {p2}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/3TL;->k:Z

    if-eqz v0, :cond_0

    .line 584119
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/3TL;->k:Z

    .line 584120
    iget-object v0, p0, LX/3TL;->d:LX/0o6;

    invoke-interface {v0}, LX/0o6;->e()V

    .line 584121
    :goto_0
    return-void

    .line 584122
    :cond_0
    iget-object v0, p0, LX/3TL;->d:LX/0o6;

    invoke-interface {v0, p2}, LX/0o6;->a(Ljava/util/List;)V

    goto :goto_0
.end method

.method public final a(LX/0o6;LX/0k5;Landroid/content/Context;Lcom/facebook/auth/viewercontext/ViewerContext;Landroid/net/Uri;ILjava/lang/String;)V
    .locals 2
    .param p7    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 584123
    iput-object p1, p0, LX/3TL;->d:LX/0o6;

    .line 584124
    iput-object p4, p0, LX/3TL;->e:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 584125
    iput-object p3, p0, LX/3TL;->f:Landroid/content/Context;

    .line 584126
    iput-object p5, p0, LX/3TL;->g:Landroid/net/Uri;

    .line 584127
    iput p6, p0, LX/3TL;->h:I

    .line 584128
    iput-object p7, p0, LX/3TL;->i:Ljava/lang/String;

    .line 584129
    const/16 v0, 0x64

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1, p0}, LX/0k5;->a(ILandroid/os/Bundle;LX/1jv;)LX/0k9;

    .line 584130
    return-void
.end method
