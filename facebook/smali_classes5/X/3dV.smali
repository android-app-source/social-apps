.class public final enum LX/3dV;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/3dV;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/3dV;

.field public static final enum FAILURE:LX/3dV;

.field public static final enum SUCCESS:LX/3dV;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 617165
    new-instance v0, LX/3dV;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v2}, LX/3dV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3dV;->SUCCESS:LX/3dV;

    .line 617166
    new-instance v0, LX/3dV;

    const-string v1, "FAILURE"

    invoke-direct {v0, v1, v3}, LX/3dV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3dV;->FAILURE:LX/3dV;

    .line 617167
    const/4 v0, 0x2

    new-array v0, v0, [LX/3dV;

    sget-object v1, LX/3dV;->SUCCESS:LX/3dV;

    aput-object v1, v0, v2

    sget-object v1, LX/3dV;->FAILURE:LX/3dV;

    aput-object v1, v0, v3

    sput-object v0, LX/3dV;->$VALUES:[LX/3dV;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 617168
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/3dV;
    .locals 1

    .prologue
    .line 617169
    const-class v0, LX/3dV;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/3dV;

    return-object v0
.end method

.method public static values()[LX/3dV;
    .locals 1

    .prologue
    .line 617170
    sget-object v0, LX/3dV;->$VALUES:[LX/3dV;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/3dV;

    return-object v0
.end method
