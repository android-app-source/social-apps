.class public final enum LX/41D;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/41D;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/41D;

.field public static final enum OPENID_CONNECT_TYPE:LX/41D;


# instance fields
.field private final mServerValue:Ljava/lang/String;
    .annotation build Lcom/facebook/infer/annotation/PrivacySource;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 666061
    new-instance v0, LX/41D;

    const-string v1, "OPENID_CONNECT_TYPE"

    const-string v2, "openid_connect"

    invoke-direct {v0, v1, v3, v2}, LX/41D;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/41D;->OPENID_CONNECT_TYPE:LX/41D;

    .line 666062
    const/4 v0, 0x1

    new-array v0, v0, [LX/41D;

    sget-object v1, LX/41D;->OPENID_CONNECT_TYPE:LX/41D;

    aput-object v1, v0, v3

    sput-object v0, LX/41D;->$VALUES:[LX/41D;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 666063
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 666064
    iput-object p3, p0, LX/41D;->mServerValue:Ljava/lang/String;

    .line 666065
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/41D;
    .locals 1

    .prologue
    .line 666066
    const-class v0, LX/41D;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/41D;

    return-object v0
.end method

.method public static values()[LX/41D;
    .locals 1

    .prologue
    .line 666067
    sget-object v0, LX/41D;->$VALUES:[LX/41D;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/41D;

    return-object v0
.end method


# virtual methods
.method public final getServerValue()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 666068
    iget-object v0, p0, LX/41D;->mServerValue:Ljava/lang/String;

    return-object v0
.end method
