.class public final LX/4uw;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<",
        "L:Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:LX/4uu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4uu;"
        }
    .end annotation
.end field

.field public volatile b:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "T",
            "L;"
        }
    .end annotation
.end field


# virtual methods
.method public final a(LX/4uv;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4uv",
            "<-T",
            "L;",
            ">;)V"
        }
    .end annotation

    const-string v0, "Notifier must not be null"

    invoke-static {p1, v0}, LX/1ol;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, LX/4uw;->a:LX/4uu;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, LX/4uu;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, LX/4uw;->a:LX/4uu;

    invoke-virtual {v1, v0}, LX/4uu;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public final b(LX/4uv;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4uv",
            "<-T",
            "L;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, LX/4uw;->b:Ljava/lang/Object;

    if-nez v0, :cond_0

    invoke-interface {p1}, LX/4uv;->a()V

    :goto_0
    return-void

    :cond_0
    :try_start_0
    invoke-interface {p1, v0}, LX/4uv;->a(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-interface {p1}, LX/4uv;->a()V

    throw v0
.end method
