.class public LX/3RG;
.super LX/3R9;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile K:LX/3RG;

.field private static final a:Ljava/lang/String;


# instance fields
.field public final A:LX/0Uh;

.field private final B:LX/2Or;

.field private final C:LX/3S2;

.field private final D:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/JkU;",
            ">;"
        }
    .end annotation
.end field

.field private final E:LX/3Ec;

.field public final F:LX/3A0;

.field private final G:Landroid/content/pm/PackageManager;

.field public final H:LX/0aU;

.field private final I:LX/2Fa;

.field private final J:LX/3S3;

.field public final b:Landroid/content/Context;

.field public final c:Landroid/content/res/Resources;

.field public final d:LX/3RK;

.field public final e:LX/2zj;

.field public final f:LX/3RQ;

.field public final g:LX/3GK;

.field public final h:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final i:Landroid/app/KeyguardManager;

.field private final j:Landroid/os/PowerManager;

.field private final k:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final l:Ljava/util/Random;

.field public final m:LX/2bC;

.field private final n:LX/0c4;

.field private final o:LX/01T;

.field private final p:LX/15P;

.field public final q:LX/3Re;

.field public final r:Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;

.field private final s:LX/2Mk;

.field private final t:Landroid/media/AudioManager;

.field private final u:LX/0pu;

.field private final v:LX/3Rj;

.field private final w:LX/0SG;

.field private final x:LX/297;

.field private final y:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public final z:LX/00H;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 579789
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, LX/3RH;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "accounts"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/3RG;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/content/res/Resources;LX/3RK;LX/2zj;LX/3RQ;LX/3GK;Lcom/facebook/prefs/shared/FbSharedPreferences;Landroid/app/KeyguardManager;Landroid/os/PowerManager;Ljava/util/Random;LX/0Or;LX/2bC;LX/0c4;LX/01T;LX/15P;Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;LX/2Mk;LX/3Re;Landroid/media/AudioManager;LX/0pu;LX/3Rj;LX/3A0;LX/3Ec;LX/0SG;Landroid/content/pm/PackageManager;LX/297;LX/0Ot;LX/00H;LX/0aU;LX/2Fa;LX/0Uh;LX/3S2;LX/0Or;LX/2Or;LX/3S3;)V
    .locals 1
    .param p10    # Ljava/util/Random;
        .annotation runtime Lcom/facebook/common/random/InsecureRandom;
        .end annotation
    .end param
    .param p11    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/chatheads/annotations/IsPrimaryChatHeadsEnabled;
        .end annotation
    .end param
    .param p13    # LX/0c4;
        .annotation runtime Lcom/facebook/messages/ipc/peer/MessageNotificationPeer;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/content/res/Resources;",
            "LX/3RK;",
            "LX/2zj;",
            "LX/3RQ;",
            "LX/3GK;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "Landroid/app/KeyguardManager;",
            "Landroid/os/PowerManager;",
            "Ljava/util/Random;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/2bC;",
            "Lcom/facebook/multiprocess/peer/state/StatefulPeerManager;",
            "LX/01T;",
            "LX/15P;",
            "Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;",
            "LX/2Mk;",
            "LX/3Re;",
            "Landroid/media/AudioManager;",
            "LX/0pu;",
            "LX/3Rj;",
            "LX/3A0;",
            "Lcom/facebook/messaging/model/threadkey/ThreadKeyFactory;",
            "LX/0SG;",
            "Landroid/content/pm/PackageManager;",
            "LX/297;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/00H;",
            "LX/0aU;",
            "LX/2Fa;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/3S2;",
            "LX/0Or",
            "<",
            "LX/JkU;",
            ">;",
            "LX/2Or;",
            "LX/3S3;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 579752
    invoke-direct {p0}, LX/3R9;-><init>()V

    .line 579753
    iput-object p1, p0, LX/3RG;->b:Landroid/content/Context;

    .line 579754
    iput-object p2, p0, LX/3RG;->c:Landroid/content/res/Resources;

    .line 579755
    iput-object p3, p0, LX/3RG;->d:LX/3RK;

    .line 579756
    iput-object p5, p0, LX/3RG;->f:LX/3RQ;

    .line 579757
    iput-object p4, p0, LX/3RG;->e:LX/2zj;

    .line 579758
    iput-object p6, p0, LX/3RG;->g:LX/3GK;

    .line 579759
    iput-object p7, p0, LX/3RG;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 579760
    iput-object p8, p0, LX/3RG;->i:Landroid/app/KeyguardManager;

    .line 579761
    iput-object p9, p0, LX/3RG;->j:Landroid/os/PowerManager;

    .line 579762
    iput-object p10, p0, LX/3RG;->l:Ljava/util/Random;

    .line 579763
    iput-object p11, p0, LX/3RG;->k:LX/0Or;

    .line 579764
    iput-object p12, p0, LX/3RG;->m:LX/2bC;

    .line 579765
    iput-object p13, p0, LX/3RG;->n:LX/0c4;

    .line 579766
    iput-object p14, p0, LX/3RG;->o:LX/01T;

    .line 579767
    move-object/from16 v0, p15

    iput-object v0, p0, LX/3RG;->p:LX/15P;

    .line 579768
    move-object/from16 v0, p18

    iput-object v0, p0, LX/3RG;->q:LX/3Re;

    .line 579769
    move-object/from16 v0, p16

    iput-object v0, p0, LX/3RG;->r:Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;

    .line 579770
    move-object/from16 v0, p17

    iput-object v0, p0, LX/3RG;->s:LX/2Mk;

    .line 579771
    move-object/from16 v0, p19

    iput-object v0, p0, LX/3RG;->t:Landroid/media/AudioManager;

    .line 579772
    move-object/from16 v0, p20

    iput-object v0, p0, LX/3RG;->u:LX/0pu;

    .line 579773
    move-object/from16 v0, p21

    iput-object v0, p0, LX/3RG;->v:LX/3Rj;

    .line 579774
    move-object/from16 v0, p22

    iput-object v0, p0, LX/3RG;->F:LX/3A0;

    .line 579775
    move-object/from16 v0, p23

    iput-object v0, p0, LX/3RG;->E:LX/3Ec;

    .line 579776
    move-object/from16 v0, p24

    iput-object v0, p0, LX/3RG;->w:LX/0SG;

    .line 579777
    move-object/from16 v0, p25

    iput-object v0, p0, LX/3RG;->G:Landroid/content/pm/PackageManager;

    .line 579778
    move-object/from16 v0, p26

    iput-object v0, p0, LX/3RG;->x:LX/297;

    .line 579779
    move-object/from16 v0, p27

    iput-object v0, p0, LX/3RG;->y:LX/0Ot;

    .line 579780
    move-object/from16 v0, p28

    iput-object v0, p0, LX/3RG;->z:LX/00H;

    .line 579781
    move-object/from16 v0, p29

    iput-object v0, p0, LX/3RG;->H:LX/0aU;

    .line 579782
    move-object/from16 v0, p30

    iput-object v0, p0, LX/3RG;->I:LX/2Fa;

    .line 579783
    move-object/from16 v0, p32

    iput-object v0, p0, LX/3RG;->C:LX/3S2;

    .line 579784
    move-object/from16 v0, p33

    iput-object v0, p0, LX/3RG;->D:LX/0Or;

    .line 579785
    move-object/from16 v0, p31

    iput-object v0, p0, LX/3RG;->A:LX/0Uh;

    .line 579786
    move-object/from16 v0, p34

    iput-object v0, p0, LX/3RG;->B:LX/2Or;

    .line 579787
    move-object/from16 v0, p35

    iput-object v0, p0, LX/3RG;->J:LX/3S3;

    .line 579788
    return-void
.end method

.method public static a(LX/3RG;Ljava/lang/String;Ljava/lang/String;)LX/2HB;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 579749
    new-instance v0, LX/2HB;

    iget-object v1, p0, LX/3RG;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/2HB;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, LX/3RG;->e:LX/2zj;

    invoke-interface {v1}, LX/2zj;->g()I

    move-result v1

    invoke-virtual {v0, v1}, LX/2HB;->a(I)LX/2HB;

    move-result-object v0

    iget-object v1, p0, LX/3RG;->w:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LX/2HB;->a(J)LX/2HB;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/2HB;->a(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/2HB;->b(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/2HB;->e(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/2HB;->c(Z)LX/2HB;

    move-result-object v0

    .line 579750
    iget-object v1, p0, LX/3RG;->f:LX/3RQ;

    new-instance v2, LX/Dhq;

    invoke-direct {v2}, LX/Dhq;-><init>()V

    invoke-virtual {v1, v0, v2, v4, v4}, LX/3RQ;->a(LX/2HB;LX/Dhq;Lcom/facebook/messaging/push/flags/ServerMessageAlertFlags;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 579751
    return-object v0
.end method

.method public static a(LX/0QB;)LX/3RG;
    .locals 3

    .prologue
    .line 579739
    sget-object v0, LX/3RG;->K:LX/3RG;

    if-nez v0, :cond_1

    .line 579740
    const-class v1, LX/3RG;

    monitor-enter v1

    .line 579741
    :try_start_0
    sget-object v0, LX/3RG;->K:LX/3RG;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 579742
    if-eqz v2, :cond_0

    .line 579743
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/3RG;->b(LX/0QB;)LX/3RG;

    move-result-object v0

    sput-object v0, LX/3RG;->K:LX/3RG;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 579744
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 579745
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 579746
    :cond_1
    sget-object v0, LX/3RG;->K:LX/3RG;

    return-object v0

    .line 579747
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 579748
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/3RG;Landroid/content/Intent;)Landroid/app/PendingIntent;
    .locals 3

    .prologue
    .line 579737
    iget-object v0, p0, LX/3RG;->l:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextInt()I

    move-result v0

    .line 579738
    iget-object v1, p0, LX/3RG;->b:Landroid/content/Context;

    const/high16 v2, 0x10000000

    invoke-static {v1, v0, p1, v2}, LX/0nt;->a(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/3RG;Ljava/lang/String;Landroid/content/Intent;Ljava/util/HashMap;)Landroid/app/PendingIntent;
    .locals 4
    .param p2    # Landroid/content/Intent;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/content/Intent;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Landroid/app/PendingIntent;"
        }
    .end annotation

    .prologue
    .line 579735
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LX/3RG;->b:Landroid/content/Context;

    const-class v2, LX/Jug;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "event_type"

    sget-object v2, LX/3zJ;->CLICK_FROM_TRAY:LX/3zJ;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "notif_type"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "redirect_intent"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "event_params"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v0

    .line 579736
    iget-object v1, p0, LX/3RG;->b:Landroid/content/Context;

    iget-object v2, p0, LX/3RG;->l:Ljava/util/Random;

    invoke-virtual {v2}, Ljava/util/Random;->nextInt()I

    move-result v2

    const/high16 v3, 0x8000000

    invoke-static {v1, v2, v0, v3}, LX/0nt;->c(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private a(LX/Juc;J)Landroid/app/PendingIntent;
    .locals 4

    .prologue
    .line 579725
    const/4 v0, 0x0

    .line 579726
    sget-object v1, LX/JuT;->b:[I

    invoke-virtual {p1}, LX/Juc;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 579727
    :goto_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 579728
    const-string v0, "CONTACT_ID"

    invoke-virtual {v1, v0, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 579729
    const-string v2, "AUTO_ACCEPT"

    sget-object v0, LX/Juc;->ANSWER:LX/Juc;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 579730
    iget-object v0, p0, LX/3RG;->l:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextInt()I

    move-result v0

    .line 579731
    iget-object v2, p0, LX/3RG;->b:Landroid/content/Context;

    const/high16 v3, 0x10000000

    invoke-static {v2, v0, v1, v3}, LX/0nt;->b(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0

    .line 579732
    :pswitch_0
    iget-object v0, p0, LX/3RG;->H:LX/0aU;

    const-string v1, "RTC_SHOW_IN_CALL_ACTION"

    invoke-virtual {v0, v1}, LX/0aU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 579733
    :pswitch_1
    iget-object v0, p0, LX/3RG;->H:LX/0aU;

    const-string v1, "RTC_DECLINE_CALL_ACTION"

    invoke-virtual {v0, v1}, LX/0aU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 579734
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(Ljava/lang/String;Ljava/util/HashMap;)Landroid/app/PendingIntent;
    .locals 4
    .param p2    # Ljava/util/HashMap;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Landroid/app/PendingIntent;"
        }
    .end annotation

    .prologue
    .line 579617
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LX/3RG;->b:Landroid/content/Context;

    const-class v2, LX/Jug;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "event_type"

    sget-object v2, LX/3zJ;->DISMISS_FROM_TRAY:LX/3zJ;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "notif_type"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "event_params"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v0

    .line 579618
    iget-object v1, p0, LX/3RG;->b:Landroid/content/Context;

    iget-object v2, p0, LX/3RG;->l:Ljava/util/Random;

    invoke-virtual {v2}, Ljava/util/Random;->nextInt()I

    move-result v2

    const/high16 v3, 0x8000000

    invoke-static {v1, v2, v0, v3}, LX/0nt;->c(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/content/Intent;Landroid/net/Uri;)Landroid/content/Intent;
    .locals 11

    .prologue
    const-wide/16 v2, 0x0

    .line 579713
    if-nez p1, :cond_1

    .line 579714
    :cond_0
    :goto_0
    return-object p0

    .line 579715
    :cond_1
    :try_start_0
    const-string v0, "artPickerSectionId"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 579716
    :try_start_1
    const-string v4, "compositionId"

    invoke-virtual {p1, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-wide v4

    move-wide v9, v4

    move-wide v4, v0

    move-wide v0, v9

    .line 579717
    :goto_1
    cmp-long v6, v4, v2

    if-lez v6, :cond_0

    cmp-long v2, v0, v2

    if-ltz v2, :cond_0

    .line 579718
    const-string v2, "art_picker_section_id"

    invoke-virtual {p0, v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 579719
    const-string v2, "composition_id"

    invoke-virtual {p0, v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    goto :goto_0

    .line 579720
    :catch_0
    move-wide v0, v2

    :goto_2
    const-string v4, "DefaultMessagingNotificationHandler"

    const-string v5, "sectionId or compositionId is not a long, uri = %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v4, v5, v6}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move-wide v4, v0

    move-wide v0, v2

    goto :goto_1

    :catch_1
    goto :goto_2
.end method

.method public static a(LX/3RG;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;LX/Dhu;)V
    .locals 5

    .prologue
    .line 579704
    invoke-static {p0, p2, p3}, LX/3RG;->a(LX/3RG;Ljava/lang/String;Ljava/lang/String;)LX/2HB;

    move-result-object v0

    .line 579705
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 579706
    const/high16 v2, 0x4000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 579707
    const-string v2, "from_notification"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 579708
    const-string v2, "send_failure_reason"

    invoke-virtual {v1, v2, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 579709
    iget-object v2, p0, LX/3RG;->b:Landroid/content/Context;

    const/4 v3, 0x0

    const/high16 v4, 0x10000000

    invoke-static {v2, v3, v1, v4}, LX/0nt;->a(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 579710
    iput-object v1, v0, LX/2HB;->d:Landroid/app/PendingIntent;

    .line 579711
    iget-object v1, p0, LX/3RG;->d:LX/3RK;

    const/16 v2, 0x2711

    invoke-virtual {v0}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/3RK;->a(ILandroid/app/Notification;)V

    .line 579712
    return-void
.end method

.method public static a(LX/3RG;Lcom/facebook/messaging/notify/NewMessageNotification;LX/2HB;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 579683
    sget-object v0, LX/JuT;->a:[I

    iget-object v3, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->e:LX/DiC;

    invoke-virtual {v3}, LX/DiC;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    move v0, v2

    .line 579684
    :goto_0
    iget-object v4, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->g:LX/Dhq;

    .line 579685
    iget-boolean v3, v4, LX/Dhq;->d:Z

    move v3, v3

    .line 579686
    if-nez v3, :cond_4

    .line 579687
    iget-object v3, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->i:Lcom/facebook/messaging/push/flags/ServerMessageAlertFlags;

    if-eqz v3, :cond_6

    iget-object v3, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->i:Lcom/facebook/messaging/push/flags/ServerMessageAlertFlags;

    iget-boolean v3, v3, Lcom/facebook/messaging/push/flags/ServerMessageAlertFlags;->b:Z

    if-eqz v3, :cond_6

    const/4 v3, 0x1

    :goto_1
    move v3, v3

    .line 579688
    if-nez v3, :cond_4

    move v3, v1

    .line 579689
    :goto_2
    if-eqz v3, :cond_2

    if-eqz v0, :cond_2

    .line 579690
    iget-object v0, p0, LX/3RG;->F:LX/3A0;

    invoke-virtual {v0}, LX/3A0;->c()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/3RG;->u:LX/0pu;

    invoke-virtual {v0}, LX/0pu;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->e:LX/DiC;

    sget-object v3, LX/DiC;->IN_APP_ACTIVE_10S:LX/DiC;

    if-ne v0, v3, :cond_1

    :cond_0
    move v2, v1

    .line 579691
    :cond_1
    if-eqz v2, :cond_5

    iget-object v0, p0, LX/3RG;->f:LX/3RQ;

    .line 579692
    invoke-static {v0}, LX/3RQ;->c(LX/3RQ;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 579693
    const/4 v1, 0x0

    .line 579694
    :goto_3
    move v0, v1

    .line 579695
    :goto_4
    if-eqz v0, :cond_2

    .line 579696
    invoke-virtual {v4}, LX/Dhq;->e()V

    .line 579697
    :cond_2
    return-void

    .line 579698
    :pswitch_1
    invoke-virtual {p1}, Lcom/facebook/messaging/notify/NewMessageNotification;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/3RG;->t:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isMusicActive()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_0

    :pswitch_2
    move v0, v1

    .line 579699
    goto :goto_0

    :cond_4
    move v3, v2

    .line 579700
    goto :goto_2

    .line 579701
    :cond_5
    iget-object v0, p0, LX/3RG;->f:LX/3RQ;

    invoke-virtual {v0, p2}, LX/3RQ;->a(LX/2HB;)Z

    move-result v0

    goto :goto_4

    :cond_6
    const/4 v3, 0x0

    goto :goto_1

    .line 579702
    :cond_7
    iget-object v1, v0, LX/3RQ;->f:Landroid/os/Vibrator;

    sget-object v2, LX/3RQ;->a:[J

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3}, Landroid/os/Vibrator;->vibrate([JI)V

    .line 579703
    const/4 v1, 0x1

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(Lcom/facebook/messaging/notify/MessagingNotification;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 579670
    iget-object v0, p0, LX/3RG;->e:LX/2zj;

    invoke-interface {v0}, LX/2zj;->g()I

    move-result v0

    .line 579671
    iget-object v1, p0, LX/3RG;->g:LX/3GK;

    invoke-interface {v1}, LX/3GK;->a()Landroid/net/Uri;

    move-result-object v1

    .line 579672
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 579673
    const-string v1, "from_notification"

    invoke-virtual {v2, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 579674
    iget-object v1, p0, LX/3RG;->b:Landroid/content/Context;

    const/4 v3, 0x0

    const/high16 v4, 0x8000000

    invoke-static {v1, v3, v2, v4}, LX/0nt;->a(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 579675
    new-instance v2, LX/2HB;

    iget-object v3, p0, LX/3RG;->b:Landroid/content/Context;

    invoke-direct {v2, v3}, LX/2HB;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, p2}, LX/2HB;->a(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v2

    invoke-virtual {v2, p3}, LX/2HB;->b(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v2

    invoke-virtual {v2, p4}, LX/2HB;->e(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/2HB;->a(I)LX/2HB;

    move-result-object v0

    .line 579676
    iput-object v1, v0, LX/2HB;->d:Landroid/app/PendingIntent;

    .line 579677
    move-object v0, v0

    .line 579678
    invoke-virtual {v0, v6}, LX/2HB;->c(Z)LX/2HB;

    move-result-object v0

    .line 579679
    iget-object v1, p0, LX/3RG;->f:LX/3RQ;

    new-instance v2, LX/Dhq;

    invoke-direct {v2}, LX/Dhq;-><init>()V

    invoke-virtual {v1, v0, v2, v5, v5}, LX/3RQ;->a(LX/2HB;LX/Dhq;Lcom/facebook/messaging/push/flags/ServerMessageAlertFlags;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 579680
    iget-object v1, p0, LX/3RG;->d:LX/3RK;

    const/16 v2, 0x2714

    invoke-virtual {v0}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v0

    invoke-virtual {v1, v5, v2, v0}, LX/3RK;->a(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 579681
    invoke-virtual {p1}, Lcom/facebook/messaging/notify/MessagingNotification;->i()V

    .line 579682
    return-void
.end method

.method private a(Lcom/facebook/messaging/notify/SimpleMessageNotification;I)V
    .locals 4

    .prologue
    .line 579661
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    iget-object v2, p0, LX/3RG;->g:LX/3GK;

    invoke-interface {v2}, LX/3GK;->a()Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 579662
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 579663
    const-string v1, "from_notification"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 579664
    const-string v1, "trigger"

    const-string v2, "notification"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 579665
    iget-object v1, p0, LX/3RG;->b:Landroid/content/Context;

    const/4 v2, 0x0

    const/high16 v3, 0x8000000

    invoke-static {v1, v2, v0, v3}, LX/0nt;->a(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 579666
    new-instance v1, LX/3pe;

    invoke-direct {v1}, LX/3pe;-><init>()V

    .line 579667
    iget-object v2, p1, Lcom/facebook/messaging/notify/SimpleMessageNotification;->a:Ljava/lang/String;

    move-object v2, v2

    .line 579668
    invoke-virtual {v1, v2}, LX/3pe;->b(Ljava/lang/CharSequence;)LX/3pe;

    move-result-object v1

    invoke-direct {p0, p1, p2, v0, v1}, LX/3RG;->a(Lcom/facebook/messaging/notify/SimpleMessageNotification;ILandroid/app/PendingIntent;LX/3pc;)V

    .line 579669
    return-void
.end method

.method private a(Lcom/facebook/messaging/notify/SimpleMessageNotification;ILandroid/app/PendingIntent;LX/3pc;)V
    .locals 6
    .param p3    # Landroid/app/PendingIntent;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/3pc;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 579647
    iget-object v0, p0, LX/3RG;->c:Landroid/content/res/Resources;

    const v1, 0x7f080011

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    .line 579648
    iget-object v0, p1, Lcom/facebook/messaging/notify/SimpleMessageNotification;->a:Ljava/lang/String;

    move-object v0, v0

    .line 579649
    iget-object v1, p0, LX/3RG;->w:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    .line 579650
    iget-object v1, p0, LX/3RG;->e:LX/2zj;

    invoke-interface {v1}, LX/2zj;->g()I

    move-result v1

    .line 579651
    new-instance v4, LX/2HB;

    iget-object v5, p0, LX/3RG;->b:Landroid/content/Context;

    invoke-direct {v4, v5}, LX/2HB;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4, v1}, LX/2HB;->a(I)LX/2HB;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/2HB;->e(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, LX/2HB;->a(J)LX/2HB;

    move-result-object v1

    .line 579652
    if-eqz p3, :cond_0

    .line 579653
    iput-object p3, v1, LX/2HB;->d:Landroid/app/PendingIntent;

    .line 579654
    :cond_0
    if-eqz p4, :cond_1

    .line 579655
    invoke-virtual {v1, p4}, LX/2HB;->a(LX/3pc;)LX/2HB;

    .line 579656
    :cond_1
    iget-object v2, p0, LX/3RG;->b:Landroid/content/Context;

    const v3, 0x7f080011

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/2HB;->a(Ljava/lang/CharSequence;)LX/2HB;

    .line 579657
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/2HB;->c(Z)LX/2HB;

    .line 579658
    invoke-virtual {v1, v0}, LX/2HB;->b(Ljava/lang/CharSequence;)LX/2HB;

    .line 579659
    iget-object v0, p0, LX/3RG;->d:LX/3RK;

    invoke-virtual {v1}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, LX/3RK;->a(ILandroid/app/Notification;)V

    .line 579660
    return-void
.end method

.method public static a$redex0(LX/3RG;I)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 579646
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "android.resource"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    iget-object v1, p0, LX/3RG;->c:Landroid/content/res/Resources;

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getResourcePackageName(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    iget-object v1, p0, LX/3RG;->c:Landroid/content/res/Resources;

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getResourceTypeName(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    iget-object v1, p0, LX/3RG;->c:Landroid/content/res/Resources;

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getResourceEntryName(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static a$redex0(LX/3RG;Landroid/graphics/Bitmap;Lcom/facebook/messaging/notify/IncomingCallNotification;J)V
    .locals 9
    .param p0    # LX/3RG;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v8, 0x1

    .line 579619
    sget-object v0, LX/Juc;->DECLINE:LX/Juc;

    invoke-direct {p0, v0, p3, p4}, LX/3RG;->a(LX/Juc;J)Landroid/app/PendingIntent;

    move-result-object v0

    .line 579620
    sget-object v1, LX/Juc;->ANSWER:LX/Juc;

    invoke-direct {p0, v1, p3, p4}, LX/3RG;->a(LX/Juc;J)Landroid/app/PendingIntent;

    move-result-object v1

    .line 579621
    sget-object v2, LX/Juc;->SHOW_INCALL:LX/Juc;

    invoke-direct {p0, v2, p3, p4}, LX/3RG;->a(LX/Juc;J)Landroid/app/PendingIntent;

    move-result-object v2

    .line 579622
    iget-object v3, p0, LX/3RG;->b:Landroid/content/Context;

    const v4, 0x7f010592

    iget-object v5, p0, LX/3RG;->b:Landroid/content/Context;

    const v6, 0x7f0a019a

    invoke-static {v5, v6}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v5

    invoke-static {v3, v4, v5}, LX/0WH;->c(Landroid/content/Context;II)I

    move-result v3

    .line 579623
    iget-object v4, p2, Lcom/facebook/messaging/notify/IncomingCallNotification;->a:Ljava/lang/String;

    .line 579624
    iget-object v5, p2, Lcom/facebook/messaging/notify/IncomingCallNotification;->b:Ljava/lang/String;

    .line 579625
    new-instance v6, LX/2HB;

    iget-object v7, p0, LX/3RG;->b:Landroid/content/Context;

    invoke-direct {v6, v7}, LX/2HB;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v4}, LX/2HB;->a(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v4

    invoke-virtual {v4, v5}, LX/2HB;->b(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v4

    const v5, 0x7f0212e2

    invoke-virtual {v4, v5}, LX/2HB;->a(I)LX/2HB;

    move-result-object v4

    .line 579626
    iput-object v2, v4, LX/2HB;->d:Landroid/app/PendingIntent;

    .line 579627
    move-object v4, v4

    .line 579628
    iput-object v2, v4, LX/2HB;->e:Landroid/app/PendingIntent;

    .line 579629
    const/16 v5, 0x80

    invoke-static {v4, v5, v8}, LX/2HB;->a(LX/2HB;IZ)V

    .line 579630
    move-object v2, v4

    .line 579631
    const/4 v4, 0x2

    .line 579632
    iput v4, v2, LX/2HB;->j:I

    .line 579633
    move-object v2, v2

    .line 579634
    iget-wide v4, p2, Lcom/facebook/messaging/notify/IncomingCallNotification;->d:J

    invoke-virtual {v2, v4, v5}, LX/2HB;->a(J)LX/2HB;

    move-result-object v2

    invoke-virtual {v2, v8}, LX/2HB;->a(Z)LX/2HB;

    move-result-object v2

    .line 579635
    iput v3, v2, LX/2HB;->y:I

    .line 579636
    move-object v2, v2

    .line 579637
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, LX/2HB;->c(Z)LX/2HB;

    move-result-object v2

    .line 579638
    if-eqz p1, :cond_0

    .line 579639
    iput-object p1, v2, LX/2HB;->g:Landroid/graphics/Bitmap;

    .line 579640
    :cond_0
    iget-object v3, p2, Lcom/facebook/messaging/notify/IncomingCallNotification;->e:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 579641
    const v3, 0x7f021a9a

    iget-object v4, p0, LX/3RG;->b:Landroid/content/Context;

    const v5, 0x7f0807e3

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4, v0}, LX/2HB;->a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)LX/2HB;

    .line 579642
    const v0, 0x7f021a80

    iget-object v3, p0, LX/3RG;->b:Landroid/content/Context;

    const v4, 0x7f0807e2

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3, v1}, LX/2HB;->a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)LX/2HB;

    .line 579643
    :cond_1
    iget-object v0, p0, LX/3RG;->d:LX/3RK;

    const-string v1, "10027"

    const/16 v3, 0x272b

    invoke-virtual {v2}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v2

    invoke-virtual {v0, v1, v3, v2}, LX/3RK;->a(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 579644
    invoke-virtual {p2}, Lcom/facebook/messaging/notify/MessagingNotification;->i()V

    .line 579645
    return-void
.end method

.method private static b(LX/0QB;)LX/3RG;
    .locals 38

    .prologue
    .line 579803
    new-instance v2, LX/3RG;

    const-class v3, Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static/range {p0 .. p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    invoke-static/range {p0 .. p0}, LX/3RI;->a(LX/0QB;)LX/3RK;

    move-result-object v5

    check-cast v5, LX/3RK;

    invoke-static/range {p0 .. p0}, LX/2Gv;->a(LX/0QB;)LX/2Gv;

    move-result-object v6

    check-cast v6, LX/2zj;

    invoke-static/range {p0 .. p0}, LX/3RQ;->a(LX/0QB;)LX/3RQ;

    move-result-object v7

    check-cast v7, LX/3RQ;

    invoke-static/range {p0 .. p0}, LX/35f;->a(LX/0QB;)LX/35f;

    move-result-object v8

    check-cast v8, LX/3GK;

    invoke-static/range {p0 .. p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v9

    check-cast v9, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static/range {p0 .. p0}, LX/1sh;->a(LX/0QB;)Landroid/app/KeyguardManager;

    move-result-object v10

    check-cast v10, Landroid/app/KeyguardManager;

    invoke-static/range {p0 .. p0}, LX/0kZ;->a(LX/0QB;)Landroid/os/PowerManager;

    move-result-object v11

    check-cast v11, Landroid/os/PowerManager;

    invoke-static/range {p0 .. p0}, LX/0U5;->a(LX/0QB;)Ljava/util/Random;

    move-result-object v12

    check-cast v12, Ljava/util/Random;

    const/16 v13, 0x150a

    move-object/from16 v0, p0

    invoke-static {v0, v13}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v13

    invoke-static/range {p0 .. p0}, LX/2bC;->a(LX/0QB;)LX/2bC;

    move-result-object v14

    check-cast v14, LX/2bC;

    invoke-static/range {p0 .. p0}, LX/0c4;->a(LX/0QB;)LX/0c4;

    move-result-object v15

    check-cast v15, LX/0c4;

    invoke-static/range {p0 .. p0}, LX/15N;->a(LX/0QB;)LX/01T;

    move-result-object v16

    check-cast v16, LX/01T;

    invoke-static/range {p0 .. p0}, LX/15P;->a(LX/0QB;)LX/15P;

    move-result-object v17

    check-cast v17, LX/15P;

    invoke-static/range {p0 .. p0}, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->a(LX/0QB;)Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;

    move-result-object v18

    check-cast v18, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;

    invoke-static/range {p0 .. p0}, LX/2Mk;->a(LX/0QB;)LX/2Mk;

    move-result-object v19

    check-cast v19, LX/2Mk;

    invoke-static/range {p0 .. p0}, LX/3Re;->a(LX/0QB;)LX/3Re;

    move-result-object v20

    check-cast v20, LX/3Re;

    invoke-static/range {p0 .. p0}, LX/19T;->a(LX/0QB;)Landroid/media/AudioManager;

    move-result-object v21

    check-cast v21, Landroid/media/AudioManager;

    invoke-static/range {p0 .. p0}, LX/0pu;->a(LX/0QB;)LX/0pu;

    move-result-object v22

    check-cast v22, LX/0pu;

    invoke-static/range {p0 .. p0}, LX/3Rj;->a(LX/0QB;)LX/3Rj;

    move-result-object v23

    check-cast v23, LX/3Rj;

    invoke-static/range {p0 .. p0}, LX/3A0;->a(LX/0QB;)LX/3A0;

    move-result-object v24

    check-cast v24, LX/3A0;

    invoke-static/range {p0 .. p0}, LX/3Ec;->a(LX/0QB;)LX/3Ec;

    move-result-object v25

    check-cast v25, LX/3Ec;

    invoke-static/range {p0 .. p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v26

    check-cast v26, LX/0SG;

    invoke-static/range {p0 .. p0}, LX/0WF;->a(LX/0QB;)Landroid/content/pm/PackageManager;

    move-result-object v27

    check-cast v27, Landroid/content/pm/PackageManager;

    invoke-static/range {p0 .. p0}, LX/297;->a(LX/0QB;)LX/297;

    move-result-object v28

    check-cast v28, LX/297;

    const/16 v29, 0x259

    move-object/from16 v0, p0

    move/from16 v1, v29

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v29

    const-class v30, LX/00H;

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-interface {v0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v30

    check-cast v30, LX/00H;

    invoke-static/range {p0 .. p0}, LX/0aU;->a(LX/0QB;)LX/0aU;

    move-result-object v31

    check-cast v31, LX/0aU;

    invoke-static/range {p0 .. p0}, LX/2Fa;->a(LX/0QB;)LX/2Fa;

    move-result-object v32

    check-cast v32, LX/2Fa;

    invoke-static/range {p0 .. p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v33

    check-cast v33, LX/0Uh;

    invoke-static/range {p0 .. p0}, LX/3S2;->a(LX/0QB;)LX/3S2;

    move-result-object v34

    check-cast v34, LX/3S2;

    const/16 v35, 0x2798

    move-object/from16 v0, p0

    move/from16 v1, v35

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v35

    invoke-static/range {p0 .. p0}, LX/2Or;->a(LX/0QB;)LX/2Or;

    move-result-object v36

    check-cast v36, LX/2Or;

    invoke-static/range {p0 .. p0}, LX/3S3;->a(LX/0QB;)LX/3S3;

    move-result-object v37

    check-cast v37, LX/3S3;

    invoke-direct/range {v2 .. v37}, LX/3RG;-><init>(Landroid/content/Context;Landroid/content/res/Resources;LX/3RK;LX/2zj;LX/3RQ;LX/3GK;Lcom/facebook/prefs/shared/FbSharedPreferences;Landroid/app/KeyguardManager;Landroid/os/PowerManager;Ljava/util/Random;LX/0Or;LX/2bC;LX/0c4;LX/01T;LX/15P;Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;LX/2Mk;LX/3Re;Landroid/media/AudioManager;LX/0pu;LX/3Rj;LX/3A0;LX/3Ec;LX/0SG;Landroid/content/pm/PackageManager;LX/297;LX/0Ot;LX/00H;LX/0aU;LX/2Fa;LX/0Uh;LX/3S2;LX/0Or;LX/2Or;LX/3S3;)V

    .line 579804
    return-object v2
.end method

.method public static c(LX/3RG;Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 579797
    iget-object v0, p0, LX/3RG;->n:LX/0c4;

    .line 579798
    invoke-static {p1}, LX/0cB;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0c4;->a(Landroid/net/Uri;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Set;

    .line 579799
    if-nez v1, :cond_0

    .line 579800
    const/4 v1, 0x1

    .line 579801
    :goto_0
    move v0, v1

    .line 579802
    return v0

    :cond_0
    invoke-interface {v1, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    goto :goto_0

    :cond_1
    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static c(LX/3RG;Ljava/lang/String;)Landroid/app/PendingIntent;
    .locals 4

    .prologue
    .line 580153
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LX/3RG;->b:Landroid/content/Context;

    const-class v2, LX/Jug;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "event_type"

    sget-object v2, LX/3zJ;->DISMISS_FROM_TRAY:LX/3zJ;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "notif_type"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 580154
    iget-object v1, p0, LX/3RG;->b:Landroid/content/Context;

    iget-object v2, p0, LX/3RG;->l:Ljava/util/Random;

    invoke-virtual {v2}, Ljava/util/Random;->nextInt()I

    move-result v2

    const/high16 v3, 0x8000000

    invoke-static {v1, v2, v0, v3}, LX/0nt;->c(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private c(Lcom/facebook/messaging/notify/NewMessageNotification;)V
    .locals 12

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 580061
    sget-object v0, LX/JuT;->a:[I

    iget-object v3, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->e:LX/DiC;

    invoke-virtual {v3}, LX/DiC;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    .line 580062
    invoke-virtual {p1}, Lcom/facebook/messaging/notify/NewMessageNotification;->b()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/3RG;->t:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isMusicActive()Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    .line 580063
    :goto_0
    iget-object v3, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->g:LX/Dhq;

    .line 580064
    iget-boolean v4, v3, LX/Dhq;->c:Z

    move v4, v4

    .line 580065
    if-nez v4, :cond_5

    .line 580066
    iget-object v4, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->i:Lcom/facebook/messaging/push/flags/ServerMessageAlertFlags;

    if-eqz v4, :cond_6

    iget-object v4, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->i:Lcom/facebook/messaging/push/flags/ServerMessageAlertFlags;

    iget-boolean v4, v4, Lcom/facebook/messaging/push/flags/ServerMessageAlertFlags;->a:Z

    if-eqz v4, :cond_6

    const/4 v4, 0x1

    :goto_1
    move v4, v4

    .line 580067
    if-nez v4, :cond_5

    iget-object v4, p0, LX/3RG;->F:LX/3A0;

    invoke-virtual {v4}, LX/3A0;->c()Z

    move-result v4

    if-nez v4, :cond_5

    .line 580068
    :goto_2
    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    .line 580069
    iget-object v0, p0, LX/3RG;->f:LX/3RQ;

    iget-object v1, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->b:Lcom/facebook/messaging/model/messages/Message;

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 580070
    iget-object v2, v0, LX/3RQ;->k:LX/01T;

    sget-object v6, LX/01T;->MESSENGER:LX/01T;

    if-ne v2, v6, :cond_13

    .line 580071
    iget-object v2, v0, LX/3RQ;->j:LX/0Uo;

    invoke-virtual {v2}, LX/0Uo;->l()Z

    move-result v2

    if-eqz v2, :cond_10

    .line 580072
    invoke-static {v1}, LX/2Mk;->u(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, v0, LX/3RQ;->h:LX/3RS;

    invoke-virtual {v2, v1}, LX/3RS;->a(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 580073
    :cond_0
    const-string v2, "369239263222822"

    iget-object v5, v1, Lcom/facebook/messaging/model/messages/Message;->k:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 580074
    iget-object v2, v0, LX/3RQ;->l:LX/3RW;

    .line 580075
    invoke-static {v2}, LX/3RW;->e(LX/3RW;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 580076
    const-string v5, "incoming_hot_like_small_message"

    const v6, 0x3e99999a    # 0.3f

    invoke-virtual {v2, v5, v6}, LX/3RX;->a(Ljava/lang/String;F)LX/7Cb;

    .line 580077
    :cond_1
    :goto_3
    move v0, v4

    .line 580078
    if-eqz v0, :cond_2

    .line 580079
    invoke-virtual {v3}, LX/Dhq;->c()V

    .line 580080
    :cond_2
    return-void

    :pswitch_0
    move v0, v1

    .line 580081
    goto :goto_0

    .line 580082
    :pswitch_1
    iget-object v0, p0, LX/3RG;->o:LX/01T;

    sget-object v3, LX/01T;->MESSENGER:LX/01T;

    if-ne v0, v3, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_0

    :cond_4
    move v0, v2

    .line 580083
    goto :goto_0

    :cond_5
    move v1, v2

    .line 580084
    goto :goto_2

    :cond_6
    const/4 v4, 0x0

    goto :goto_1

    .line 580085
    :cond_7
    const-string v2, "369239343222814"

    iget-object v5, v1, Lcom/facebook/messaging/model/messages/Message;->k:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 580086
    iget-object v2, v0, LX/3RQ;->l:LX/3RW;

    .line 580087
    invoke-static {v2}, LX/3RW;->e(LX/3RW;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 580088
    const-string v5, "incoming_hot_like_medium_message"

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-virtual {v2, v5, v6}, LX/3RX;->a(Ljava/lang/String;F)LX/7Cb;

    .line 580089
    :cond_8
    goto :goto_3

    .line 580090
    :cond_9
    iget-object v2, v0, LX/3RQ;->l:LX/3RW;

    .line 580091
    invoke-static {v2}, LX/3RW;->e(LX/3RW;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 580092
    const-string v5, "incoming_hot_like_large_message"

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-virtual {v2, v5, v6}, LX/3RX;->a(Ljava/lang/String;F)LX/7Cb;

    .line 580093
    :cond_a
    goto :goto_3

    .line 580094
    :cond_b
    invoke-static {v1}, LX/2Mk;->t(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 580095
    iget-object v2, v1, Lcom/facebook/messaging/model/messages/Message;->k:Ljava/lang/String;

    invoke-static {v2}, LX/4m9;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 580096
    iget-object v2, v0, LX/3RQ;->l:LX/3RW;

    const-string v5, "incoming_like_message"

    invoke-virtual {v2, v5}, LX/3RW;->d(Ljava/lang/String;)V

    goto :goto_3

    .line 580097
    :cond_c
    iget-object v2, v0, LX/3RQ;->l:LX/3RW;

    const-string v5, "incoming_sticker_message"

    invoke-virtual {v2, v5}, LX/3RW;->d(Ljava/lang/String;)V

    goto :goto_3

    .line 580098
    :cond_d
    iget-object v2, v0, LX/3RQ;->m:LX/2Ly;

    invoke-virtual {v2, v1}, LX/2Ly;->a(Lcom/facebook/messaging/model/messages/Message;)LX/6eh;

    move-result-object v2

    sget-object v5, LX/6eh;->PAYMENT:LX/6eh;

    if-ne v2, v5, :cond_f

    .line 580099
    iget-object v2, v0, LX/3RQ;->l:LX/3RW;

    const-string v5, "incoming_payment_message"

    .line 580100
    invoke-static {v2}, LX/3RW;->e(LX/3RW;)Z

    move-result v6

    if-eqz v6, :cond_e

    .line 580101
    const/high16 v6, 0x3e800000    # 0.25f

    invoke-virtual {v2, v5, v6}, LX/3RX;->a(Ljava/lang/String;F)LX/7Cb;

    .line 580102
    :cond_e
    goto :goto_3

    .line 580103
    :cond_f
    iget-object v2, v0, LX/3RQ;->l:LX/3RW;

    const-string v5, "in_app_message"

    invoke-virtual {v2, v5}, LX/3RW;->d(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 580104
    :cond_10
    iget-object v2, v0, LX/3RQ;->l:LX/3RW;

    iget-object v5, v1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v0, v5}, LX/3RQ;->a(LX/3RQ;Lcom/facebook/messaging/model/threadkey/ThreadKey;)Landroid/net/Uri;

    move-result-object v5

    iget-object v7, v0, LX/3RQ;->e:Landroid/content/Context;

    .line 580105
    iget-object v8, v2, LX/3RW;->b:LX/3RU;

    invoke-virtual {v8}, LX/3RU;->c()Z

    move-result v8

    if-eqz v8, :cond_19

    const/4 v8, 0x1

    .line 580106
    iget-object v9, v2, LX/3RW;->d:Landroid/telephony/TelephonyManager;

    invoke-virtual {v9}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v9

    .line 580107
    if-eq v9, v8, :cond_11

    const/4 v6, 0x2

    if-ne v9, v6, :cond_1a

    :cond_11
    :goto_4
    move v8, v8

    .line 580108
    if-nez v8, :cond_19

    const/4 v8, 0x1

    :goto_5
    move v8, v8

    .line 580109
    if-eqz v8, :cond_12

    if-nez v5, :cond_17

    .line 580110
    :cond_12
    :goto_6
    goto/16 :goto_3

    .line 580111
    :cond_13
    iget-object v2, v0, LX/3RQ;->i:LX/3RT;

    invoke-virtual {v2}, LX/3RT;->e()Z

    move-result v2

    if-eqz v2, :cond_15

    iget-object v2, v0, LX/3RQ;->g:Landroid/media/AudioManager;

    invoke-virtual {v2}, Landroid/media/AudioManager;->isMusicActive()Z

    move-result v2

    if-nez v2, :cond_15

    move v2, v4

    .line 580112
    :goto_7
    invoke-static {v0}, LX/3RQ;->d(LX/3RQ;)Landroid/net/Uri;

    move-result-object v6

    .line 580113
    if-eqz v2, :cond_16

    .line 580114
    iget-object v2, v0, LX/3RQ;->g:Landroid/media/AudioManager;

    iget-object v7, v0, LX/3RQ;->p:LX/3RZ;

    invoke-virtual {v7}, LX/3RZ;->a()I

    move-result v7

    invoke-virtual {v2, v7}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v2

    if-lez v2, :cond_1f

    const/4 v2, 0x1

    :goto_8
    move v2, v2

    .line 580115
    if-eqz v2, :cond_16

    .line 580116
    const/4 v2, 0x3

    invoke-static {v2}, LX/01m;->b(I)Z

    move-result v2

    if-eqz v2, :cond_14

    .line 580117
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "Played new message sound, "

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 580118
    :cond_14
    iget-object v2, v0, LX/3RQ;->l:LX/3RW;

    .line 580119
    iget-object v5, v2, LX/3RX;->c:LX/3RZ;

    invoke-virtual {v5}, LX/3RZ;->a()I

    move-result v5

    .line 580120
    invoke-static {v2}, LX/3RX;->b(LX/3RX;)LX/7Cb;

    move-result-object v7

    .line 580121
    invoke-static {v2, v7, v6}, LX/3RX;->a(LX/3RX;LX/7Cb;Ljava/lang/Object;)V

    .line 580122
    invoke-virtual {v7, v6, v5}, LX/7Cb;->a(Landroid/net/Uri;I)V

    .line 580123
    goto/16 :goto_3

    :cond_15
    move v2, v5

    .line 580124
    goto :goto_7

    :cond_16
    move v4, v5

    .line 580125
    goto/16 :goto_3

    .line 580126
    :cond_17
    const/4 v11, 0x1

    .line 580127
    const v9, 0x3e4ccccd    # 0.2f

    .line 580128
    invoke-static {}, LX/FO3;->values()[LX/FO3;

    move-result-object p1

    array-length v0, p1

    const/4 v10, 0x0

    move p0, v10

    :goto_9
    if-ge p0, v0, :cond_1e

    aget-object v10, p1, p0

    .line 580129
    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v10, v7}, LX/FO3;->getUri(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1d

    .line 580130
    :goto_a
    move-object v10, v10

    .line 580131
    if-nez v10, :cond_1c

    .line 580132
    const/high16 v9, 0x3f800000    # 1.0f

    .line 580133
    :goto_b
    :pswitch_2
    move v9, v9

    .line 580134
    iget-object v10, v2, LX/3RW;->c:Landroid/media/AudioManager;

    invoke-virtual {v10}, Landroid/media/AudioManager;->isMusicActive()Z

    move-result v10

    if-eqz v10, :cond_1b

    .line 580135
    iget-object v10, v2, LX/3RW;->c:Landroid/media/AudioManager;

    invoke-virtual {v10, v11}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v10

    if-lez v10, :cond_18

    .line 580136
    invoke-virtual {v2, v5, v11, v9}, LX/3RX;->a(Landroid/net/Uri;IF)LX/7Cb;

    .line 580137
    :cond_18
    :goto_c
    goto/16 :goto_6

    :cond_19
    const/4 v8, 0x0

    goto/16 :goto_5

    :cond_1a
    const/4 v8, 0x0

    goto/16 :goto_4

    .line 580138
    :cond_1b
    iget-object v10, v2, LX/3RW;->c:Landroid/media/AudioManager;

    iget-object v11, v2, LX/3RW;->g:LX/3RZ;

    invoke-virtual {v11}, LX/3RZ;->a()I

    move-result v11

    invoke-virtual {v10, v11}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v10

    if-lez v10, :cond_18

    .line 580139
    iget-object v8, v2, LX/3RX;->c:LX/3RZ;

    invoke-virtual {v8}, LX/3RZ;->a()I

    move-result v8

    invoke-virtual {v2, v5, v8, v9}, LX/3RX;->a(Landroid/net/Uri;IF)LX/7Cb;

    .line 580140
    goto :goto_c

    .line 580141
    :cond_1c
    sget-object p0, LX/FO2;->a:[I

    invoke-virtual {v10}, LX/FO3;->ordinal()I

    move-result v10

    aget v10, p0, v10

    packed-switch v10, :pswitch_data_1

    goto :goto_b

    .line 580142
    :pswitch_3
    const v9, 0x3c75c28f    # 0.015f

    goto :goto_b

    .line 580143
    :pswitch_4
    const v9, 0x3d99999a    # 0.075f

    goto :goto_b

    .line 580144
    :pswitch_5
    const v9, 0x3dcccccd    # 0.1f

    goto :goto_b

    .line 580145
    :pswitch_6
    const v9, 0x3e19999a    # 0.15f

    goto :goto_b

    .line 580146
    :pswitch_7
    const v9, 0x3e333333    # 0.175f

    goto :goto_b

    .line 580147
    :pswitch_8
    const/high16 v9, 0x3e800000    # 0.25f

    goto :goto_b

    .line 580148
    :pswitch_9
    const v9, 0x3e99999a    # 0.3f

    goto :goto_b

    .line 580149
    :pswitch_a
    const v9, 0x3eb33333    # 0.35f

    goto :goto_b

    .line 580150
    :pswitch_b
    const v9, 0x3ecccccd    # 0.4f

    goto :goto_b

    .line 580151
    :cond_1d
    add-int/lit8 v10, p0, 0x1

    move p0, v10

    goto :goto_9

    .line 580152
    :cond_1e
    const/4 v10, 0x0

    goto :goto_a

    :cond_1f
    const/4 v2, 0x0

    goto/16 :goto_8

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_7
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_8
        :pswitch_8
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_b
    .end packed-switch
.end method

.method private d(Lcom/facebook/messaging/notify/NewMessageNotification;)V
    .locals 12

    .prologue
    const/4 v3, 0x0

    const/4 v6, 0x1

    .line 579993
    iget-object v0, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    if-nez v0, :cond_1

    .line 579994
    :cond_0
    :goto_0
    return-void

    .line 579995
    :cond_1
    iget-object v2, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->b:Lcom/facebook/messaging/model/messages/Message;

    .line 579996
    invoke-virtual {p1}, Lcom/facebook/messaging/notify/NewMessageNotification;->b()Z

    move-result v0

    .line 579997
    iget-object v1, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->e:LX/DiC;

    sget-object v4, LX/DiC;->IN_APP_ACTIVE_30S:LX/DiC;

    if-eq v1, v4, :cond_2

    iget-object v1, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->e:LX/DiC;

    sget-object v4, LX/DiC;->IN_APP_ACTIVE_10S:LX/DiC;

    if-ne v1, v4, :cond_3

    .line 579998
    :cond_2
    iget-object v0, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {p0, v0}, LX/3RG;->d(LX/3RG;Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v0

    .line 579999
    :cond_3
    iget-object v11, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->g:LX/Dhq;

    .line 580000
    iget-boolean v1, v11, LX/Dhq;->g:Z

    move v1, v1

    .line 580001
    if-nez v1, :cond_4

    move v1, v6

    .line 580002
    :goto_1
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 580003
    iget-object v0, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 580004
    invoke-static {v0}, LX/0db;->g(Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/0Tn;

    move-result-object v1

    .line 580005
    iget-object v4, v2, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-static {p0, v0, v4}, LX/3RG;->c(LX/3RG;Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)I

    move-result v4

    .line 580006
    iget-object v5, p0, LX/3RG;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v5

    .line 580007
    invoke-interface {v5, v1, v4}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    .line 580008
    invoke-interface {v5}, LX/0hN;->commit()V

    .line 580009
    iget-object v0, p0, LX/3RG;->s:LX/2Mk;

    iget-object v1, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->b:Lcom/facebook/messaging/model/messages/Message;

    invoke-virtual {v0, v1}, LX/2Mk;->l(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 580010
    iget-object v0, p0, LX/3RG;->s:LX/2Mk;

    iget-object v1, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->b:Lcom/facebook/messaging/model/messages/Message;

    invoke-virtual {v0, v1}, LX/2Mk;->k(Lcom/facebook/messaging/model/messages/Message;)LX/6hS;

    move-result-object v4

    .line 580011
    iget-object v0, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->b:Lcom/facebook/messaging/model/messages/Message;

    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    iget-object v1, v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;->c:Ljava/lang/String;

    .line 580012
    iget-object v2, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->a:Ljava/lang/String;

    .line 580013
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 580014
    iget-object v0, p0, LX/3RG;->b:Landroid/content/Context;

    const v1, 0x7f0807bc

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 580015
    :goto_2
    new-instance v0, Lcom/facebook/messaging/notify/MissedCallNotification;

    iget-object v3, v4, LX/6hS;->e:Ljava/lang/String;

    iget-object v4, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->b:Lcom/facebook/messaging/model/messages/Message;

    iget-wide v4, v4, Lcom/facebook/messaging/model/messages/Message;->c:J

    const-string v7, "missed_call"

    iget-object v8, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->g:LX/Dhq;

    sget-object v9, LX/Di7;->P2P:LX/Di7;

    iget-object v10, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-direct/range {v0 .. v10}, Lcom/facebook/messaging/notify/MissedCallNotification;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZLjava/lang/String;LX/Dhq;LX/Di7;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 580016
    invoke-virtual {p0, v0}, LX/3RG;->a(Lcom/facebook/messaging/notify/MissedCallNotification;)V

    .line 580017
    :goto_3
    const/4 v0, 0x1

    iput-boolean v0, v11, LX/Dhq;->g:Z

    .line 580018
    goto :goto_0

    :cond_4
    move v1, v3

    .line 580019
    goto :goto_1

    .line 580020
    :cond_5
    iget-object v0, p0, LX/3RG;->b:Landroid/content/Context;

    const v2, 0x7f080766

    new-array v5, v6, [Ljava/lang/Object;

    iget-object v7, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->b:Lcom/facebook/messaging/model/messages/Message;

    iget-object v7, v7, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    iget-object v7, v7, Lcom/facebook/messaging/model/messages/ParticipantInfo;->c:Ljava/lang/String;

    aput-object v7, v5, v3

    invoke-virtual {v0, v2, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    .line 580021
    :cond_6
    invoke-static {v2}, LX/2Mk;->m(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 580022
    const/4 v10, 0x0

    .line 580023
    iget-object v0, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 580024
    if-nez v0, :cond_9

    .line 580025
    :cond_7
    :goto_4
    goto :goto_3

    .line 580026
    :cond_8
    invoke-static {p0, p1}, LX/3RG;->f(LX/3RG;Lcom/facebook/messaging/notify/NewMessageNotification;)V

    goto :goto_3

    .line 580027
    :cond_9
    iget-object v1, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->b:Lcom/facebook/messaging/model/messages/Message;

    iget-object v1, v1, Lcom/facebook/messaging/model/messages/Message;->I:Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;

    .line 580028
    if-eqz v1, :cond_7

    .line 580029
    iget-boolean v2, v1, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->o:Z

    move v2, v2

    .line 580030
    iget-object v3, v1, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->n:Ljava/lang/String;

    move-object v1, v3

    .line 580031
    const-string v3, "multiway_join_via_push_notification"

    invoke-virtual {p0, v0, v2, v1, v3}, LX/3RG;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;ZLjava/lang/String;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v1

    .line 580032
    if-eqz v1, :cond_7

    .line 580033
    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2}, LX/3RG;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v2

    .line 580034
    iget-object v3, p0, LX/3RG;->F:LX/3A0;

    invoke-virtual {v3}, LX/3A0;->d()Z

    move-result v3

    .line 580035
    iget-object v4, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->b:Lcom/facebook/messaging/model/messages/Message;

    .line 580036
    iget-object v5, p0, LX/3RG;->r:Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;

    iget-object v6, v4, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v5, v6}, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v5

    .line 580037
    iget-object v6, p0, LX/3RG;->r:Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;

    invoke-virtual {v6, v4, v5}, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->a(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threads/ThreadSummary;)Ljava/lang/String;

    move-result-object v4

    .line 580038
    iget-object v5, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->g:LX/Dhq;

    .line 580039
    iget-object v6, p0, LX/3RG;->b:Landroid/content/Context;

    const v7, 0x7f010592

    iget-object v8, p0, LX/3RG;->b:Landroid/content/Context;

    const v9, 0x7f0a019a

    invoke-static {v8, v9}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v8

    invoke-static {v6, v7, v8}, LX/0WH;->c(Landroid/content/Context;II)I

    move-result v6

    .line 580040
    new-instance v7, LX/2HB;

    iget-object v8, p0, LX/3RG;->b:Landroid/content/Context;

    invoke-direct {v7, v8}, LX/2HB;-><init>(Landroid/content/Context;)V

    iget-object v8, p0, LX/3RG;->e:LX/2zj;

    invoke-interface {v8}, LX/2zj;->g()I

    move-result v8

    invoke-virtual {v7, v8}, LX/2HB;->a(I)LX/2HB;

    move-result-object v7

    .line 580041
    iput v6, v7, LX/2HB;->y:I

    .line 580042
    move-object v6, v7

    .line 580043
    invoke-virtual {v6, v4}, LX/2HB;->a(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v4

    iget-object v6, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->a:Ljava/lang/String;

    invoke-virtual {v4, v6}, LX/2HB;->b(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v4

    .line 580044
    if-eqz v3, :cond_b

    .line 580045
    const/4 v6, -0x2

    .line 580046
    iput v6, v4, LX/2HB;->j:I

    .line 580047
    :goto_5
    if-nez v3, :cond_a

    .line 580048
    iget-boolean v3, v5, LX/Dhq;->f:Z

    move v3, v3

    .line 580049
    if-eqz v3, :cond_c

    invoke-static {p0}, LX/3RG;->g(LX/3RG;)Z

    move-result v3

    if-nez v3, :cond_c

    invoke-static {p0, v0}, LX/3RG;->d(LX/3RG;Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v3

    if-nez v3, :cond_c

    .line 580050
    :cond_a
    const-string v3, ""

    invoke-virtual {v4, v3}, LX/2HB;->e(Ljava/lang/CharSequence;)LX/2HB;

    .line 580051
    :goto_6
    iget-object v3, p0, LX/3RG;->f:LX/3RQ;

    iget-object v6, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->i:Lcom/facebook/messaging/push/flags/ServerMessageAlertFlags;

    iget-object v7, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v3, v4, v5, v6, v7}, LX/3RQ;->a(LX/2HB;LX/Dhq;Lcom/facebook/messaging/push/flags/ServerMessageAlertFlags;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 580052
    invoke-static {p0, p1, v4}, LX/3RG;->a(LX/3RG;Lcom/facebook/messaging/notify/NewMessageNotification;LX/2HB;)V

    .line 580053
    iput-object v1, v4, LX/2HB;->d:Landroid/app/PendingIntent;

    .line 580054
    iget-object v3, p0, LX/3RG;->b:Landroid/content/Context;

    const v5, 0x7f080723

    invoke-virtual {v3, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v10, v3, v1}, LX/2HB;->a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)LX/2HB;

    .line 580055
    iget-object v1, p0, LX/3RG;->b:Landroid/content/Context;

    const v3, 0x7f080724

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v10, v1, v2}, LX/2HB;->a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)LX/2HB;

    .line 580056
    iget-object v1, p0, LX/3RG;->r:Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;

    new-instance v2, LX/JuQ;

    invoke-direct {v2, p0, v4, p1, v0}, LX/JuQ;-><init>(LX/3RG;LX/2HB;Lcom/facebook/messaging/notify/NewMessageNotification;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    invoke-virtual {v1, p1, v2}, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->a(Lcom/facebook/messaging/notify/NewMessageNotification;LX/FJf;)V

    goto/16 :goto_4

    .line 580057
    :cond_b
    const/4 v6, 0x2

    .line 580058
    iput v6, v4, LX/2HB;->j:I

    .line 580059
    goto :goto_5

    .line 580060
    :cond_c
    iget-object v3, p0, LX/3RG;->q:LX/3Re;

    invoke-virtual {v3, p1}, LX/3Re;->a(Lcom/facebook/messaging/notify/NewMessageNotification;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, LX/2HB;->e(Ljava/lang/CharSequence;)LX/2HB;

    goto :goto_6
.end method

.method public static d(LX/3RG;Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 579976
    iget-object v0, p0, LX/3RG;->o:LX/01T;

    sget-object v3, LX/01T;->FB4A:LX/01T;

    if-ne v0, v3, :cond_0

    .line 579977
    :goto_0
    return v1

    .line 579978
    :cond_0
    iget-object v0, p0, LX/3RG;->p:LX/15P;

    .line 579979
    iget-object v3, v0, LX/15P;->b:Landroid/app/Activity;

    move-object v0, v3

    .line 579980
    if-nez v0, :cond_1

    move v1, v2

    .line 579981
    goto :goto_0

    .line 579982
    :cond_1
    :goto_1
    invoke-virtual {v0}, Landroid/app/Activity;->isChild()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 579983
    invoke-virtual {v0}, Landroid/app/Activity;->getParent()Landroid/app/Activity;

    move-result-object v0

    goto :goto_1

    .line 579984
    :cond_2
    instance-of v3, v0, LX/Jup;

    if-eqz v3, :cond_4

    .line 579985
    check-cast v0, LX/Jup;

    .line 579986
    invoke-interface {v0}, LX/Jup;->a()Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v3

    invoke-static {v3, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    .line 579987
    :try_start_0
    invoke-interface {v0}, LX/Jup;->b()Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 579988
    :goto_2
    if-nez v3, :cond_3

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    move v1, v0

    .line 579989
    goto :goto_0

    .line 579990
    :catch_0
    move-exception v4

    .line 579991
    iget-object v0, p0, LX/3RG;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v5, "Messaging_Notification_CanSeeTopThreadIsUnread_Npe"

    invoke-virtual {v0, v5, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    move v0, v1

    goto :goto_2

    :cond_3
    move v0, v2

    .line 579992
    goto :goto_3

    :cond_4
    move v0, v1

    goto :goto_3
.end method

.method private static f(LX/3RG;Lcom/facebook/messaging/notify/NewMessageNotification;)V
    .locals 10

    .prologue
    .line 579944
    iget-object v0, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 579945
    if-nez v0, :cond_0

    .line 579946
    :goto_0
    return-void

    .line 579947
    :cond_0
    iget-object v1, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->g:LX/Dhq;

    .line 579948
    new-instance v2, LX/2HB;

    iget-object v3, p0, LX/3RG;->b:Landroid/content/Context;

    invoke-direct {v2, v3}, LX/2HB;-><init>(Landroid/content/Context;)V

    .line 579949
    iget-boolean v3, v1, LX/Dhq;->f:Z

    move v3, v3

    .line 579950
    if-eqz v3, :cond_1

    invoke-static {p0}, LX/3RG;->g(LX/3RG;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-static {p0, v0}, LX/3RG;->d(LX/3RG;Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 579951
    const-string v0, ""

    invoke-virtual {v2, v0}, LX/2HB;->e(Ljava/lang/CharSequence;)LX/2HB;

    .line 579952
    :goto_1
    iget-object v0, p0, LX/3RG;->f:LX/3RQ;

    iget-object v3, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->i:Lcom/facebook/messaging/push/flags/ServerMessageAlertFlags;

    iget-object v4, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0, v2, v1, v3, v4}, LX/3RQ;->a(LX/2HB;LX/Dhq;Lcom/facebook/messaging/push/flags/ServerMessageAlertFlags;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 579953
    invoke-static {p0, p1, v2}, LX/3RG;->a(LX/3RG;Lcom/facebook/messaging/notify/NewMessageNotification;LX/2HB;)V

    .line 579954
    iget-object v6, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->b:Lcom/facebook/messaging/model/messages/Message;

    if-eqz v6, :cond_4

    iget-object v6, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->b:Lcom/facebook/messaging/model/messages/Message;

    iget-object v6, v6, Lcom/facebook/messaging/model/messages/Message;->R:LX/0P1;

    if-eqz v6, :cond_4

    sget-object v6, LX/5ds;->MARKETPLACE_TAB_MESSAGE:LX/5ds;

    iget-object v7, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->b:Lcom/facebook/messaging/model/messages/Message;

    iget-object v7, v7, Lcom/facebook/messaging/model/messages/Message;->R:LX/0P1;

    invoke-static {v6, v7}, LX/5dt;->a(LX/5ds;LX/0P1;)Lcom/facebook/messaging/model/messagemetadata/PlatformMetadata;

    move-result-object v6

    check-cast v6, Lcom/facebook/messaging/model/messagemetadata/MarketplaceTabPlatformMetadata;

    .line 579955
    :goto_2
    if-eqz v6, :cond_5

    iget-boolean v6, v6, Lcom/facebook/messaging/model/messagemetadata/MarketplaceTabPlatformMetadata;->a:Z

    if-eqz v6, :cond_5

    iget-object v6, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    if-eqz v6, :cond_5

    .line 579956
    new-instance v6, Landroid/content/Intent;

    const-string v7, "android.intent.action.VIEW"

    invoke-direct {v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 579957
    iget-object v7, p0, LX/3RG;->g:LX/3GK;

    iget-object v8, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-wide v8, v8, Lcom/facebook/messaging/model/threadkey/ThreadKey;->b:J

    invoke-interface {v7, v8, v9}, LX/3GK;->a(J)Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 579958
    :goto_3
    const/high16 v7, 0x4000000

    invoke-virtual {v6, v7}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 579959
    const-string v7, "from_notification"

    const/4 v8, 0x1

    invoke-virtual {v6, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 579960
    const-string v7, "trigger"

    const-string v8, "notification"

    invoke-virtual {v6, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 579961
    move-object v0, v6

    .line 579962
    invoke-static {p0, v0}, LX/3RG;->a(LX/3RG;Landroid/content/Intent;)Landroid/app/PendingIntent;

    move-result-object v1

    .line 579963
    iget-object v3, p0, LX/3RG;->A:LX/0Uh;

    const/16 v4, 0x192

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, LX/0Uh;->a(IZ)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 579964
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 579965
    const-string v4, "mid"

    iget-object v5, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->b:Lcom/facebook/messaging/model/messages/Message;

    iget-object v5, v5, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 579966
    iget-object v4, p1, Lcom/facebook/messaging/notify/MessagingNotification;->j:LX/3RF;

    invoke-virtual {v4}, LX/3RF;->name()Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v4, v0, v3}, LX/3RG;->a(LX/3RG;Ljava/lang/String;Landroid/content/Intent;Ljava/util/HashMap;)Landroid/app/PendingIntent;

    move-result-object v0

    .line 579967
    iput-object v0, v2, LX/2HB;->d:Landroid/app/PendingIntent;

    .line 579968
    :goto_4
    iget-object v0, p0, LX/3RG;->q:LX/3Re;

    new-instance v3, LX/JuS;

    invoke-direct {v3, p0, v2, v1, p1}, LX/JuS;-><init>(LX/3RG;LX/2HB;Landroid/app/PendingIntent;Lcom/facebook/messaging/notify/NewMessageNotification;)V

    invoke-virtual {v0, p1, v2, v3}, LX/3Re;->a(Lcom/facebook/messaging/notify/NewMessageNotification;LX/2HB;LX/JuS;)V

    goto/16 :goto_0

    .line 579969
    :cond_1
    invoke-static {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->i(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 579970
    const-string v0, ""

    invoke-virtual {v2, v0}, LX/2HB;->e(Ljava/lang/CharSequence;)LX/2HB;

    goto/16 :goto_1

    .line 579971
    :cond_2
    iget-object v0, p0, LX/3RG;->q:LX/3Re;

    invoke-virtual {v0, p1}, LX/3Re;->a(Lcom/facebook/messaging/notify/NewMessageNotification;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/2HB;->e(Ljava/lang/CharSequence;)LX/2HB;

    goto/16 :goto_1

    .line 579972
    :cond_3
    iput-object v1, v2, LX/2HB;->d:Landroid/app/PendingIntent;

    .line 579973
    goto :goto_4

    .line 579974
    :cond_4
    const/4 v6, 0x0

    goto/16 :goto_2

    .line 579975
    :cond_5
    iget-object v6, p0, LX/3RG;->g:LX/3GK;

    iget-object v7, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-interface {v6, v7}, LX/3GK;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Landroid/content/Intent;

    move-result-object v6

    goto :goto_3
.end method

.method public static g(LX/3RG;)Z
    .locals 1

    .prologue
    .line 579371
    iget-object v0, p0, LX/3RG;->i:Landroid/app/KeyguardManager;

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/3RG;->j:Landroid/os/PowerManager;

    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static h(LX/3RG;Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z
    .locals 1

    .prologue
    .line 579943
    invoke-static {p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->i(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/3RG;->e:LX/2zj;

    invoke-interface {v0}, LX/2zj;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3RG;->e:LX/2zj;

    invoke-interface {v0}, LX/2zj;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(JLjava/lang/String;)Landroid/app/PendingIntent;
    .locals 5

    .prologue
    .line 579938
    iget-object v0, p0, LX/3RG;->H:LX/0aU;

    const-string v1, "RTC_DISMISS_MISSED_CALL_ACTION"

    invoke-virtual {v0, v1}, LX/0aU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 579939
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 579940
    const-string v0, "CONTACT_ID"

    invoke-virtual {v1, v0, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    const-string v2, "trigger"

    invoke-virtual {v0, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 579941
    iget-object v0, p0, LX/3RG;->l:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextInt()I

    move-result v0

    .line 579942
    iget-object v2, p0, LX/3RG;->b:Landroid/content/Context;

    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v2, v0, v1, v3}, LX/0nt;->b(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method public final a(JZLjava/lang/String;)Landroid/app/PendingIntent;
    .locals 7

    .prologue
    .line 579933
    iget-object v0, p0, LX/3RG;->H:LX/0aU;

    const-string v1, "RTC_START_CALL_ACTION"

    invoke-virtual {v0, v1}, LX/0aU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 579934
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 579935
    const-string v0, "CONTACT_ID"

    invoke-virtual {v1, v0, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    const-string v2, "IS_VIDEO_CALL"

    invoke-virtual {v0, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string v2, "CALLBACK_NOTIF_TIME"

    iget-object v3, p0, LX/3RG;->w:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    invoke-virtual {v0, v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    const-string v2, "trigger"

    invoke-virtual {v0, v2, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 579936
    iget-object v0, p0, LX/3RG;->l:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextInt()I

    move-result v0

    .line 579937
    iget-object v2, p0, LX/3RG;->b:Landroid/content/Context;

    const/high16 v3, 0x10000000

    invoke-static {v2, v0, v1, v3}, LX/0nt;->b(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;JJLjava/lang/String;)Landroid/app/PendingIntent;
    .locals 4
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x1

    .line 579921
    iget-object v0, p0, LX/3RG;->g:LX/3GK;

    invoke-interface {v0, p1}, LX/3GK;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Landroid/content/Intent;

    move-result-object v1

    .line 579922
    const/high16 v0, 0x4000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 579923
    const-string v0, "from_notification"

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 579924
    const-string v0, "CONTACT_ID"

    invoke-virtual {v1, v0, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 579925
    const-string v0, "trigger"

    const-string v2, "voip_notification"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 579926
    invoke-static {p6}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 579927
    const-string v0, "rtc_when"

    invoke-virtual {v1, v0, p4, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 579928
    const-string v0, "rtc_message"

    invoke-virtual {v1, v0, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 579929
    :cond_0
    iget-object v0, p0, LX/3RG;->k:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p0, p1}, LX/3RG;->d(LX/3RG;Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 579930
    const-string v0, "prefer_chat_if_possible"

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 579931
    :cond_1
    iget-object v0, p0, LX/3RG;->l:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextInt()I

    move-result v0

    .line 579932
    iget-object v2, p0, LX/3RG;->b:Landroid/content/Context;

    const/high16 v3, 0x10000000

    invoke-static {v2, v0, v1, v3}, LX/0nt;->a(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;ZLjava/lang/String;Ljava/lang/String;)Landroid/app/PendingIntent;
    .locals 6
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 579909
    iget-object v0, p0, LX/3RG;->H:LX/0aU;

    const-string v1, "RTC_JOIN_CONFERENCE_CALL_ACTION"

    invoke-virtual {v0, v1}, LX/0aU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 579910
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 579911
    iget-object v0, p0, LX/3RG;->r:Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;

    invoke-virtual {v0, p1}, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v0

    .line 579912
    if-nez v0, :cond_0

    .line 579913
    const-string v0, "DefaultMessagingNotificationHandler"

    const-string v1, "createPendingIntentForJoinConferenceCall cannot fetch threadSummary"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 579914
    const/4 v0, 0x0

    .line 579915
    :goto_0
    return-object v0

    .line 579916
    :cond_0
    const-string v2, "THREAD_SUMMARY"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string v2, "IS_CONFERENCE_CALL"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string v2, "IS_VIDEO_CALL"

    invoke-virtual {v0, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string v2, "CALLBACK_NOTIF_TIME"

    iget-object v3, p0, LX/3RG;->w:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    invoke-virtual {v0, v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    const-string v2, "trigger"

    invoke-virtual {v0, v2, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 579917
    if-eqz p3, :cond_1

    .line 579918
    const-string v0, "SERVER_INFO_DATA"

    invoke-virtual {v1, v0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 579919
    :cond_1
    iget-object v0, p0, LX/3RG;->l:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextInt()I

    move-result v0

    .line 579920
    iget-object v2, p0, LX/3RG;->b:Landroid/content/Context;

    const/high16 v3, 0x10000000

    invoke-static {v2, v0, v1, v3}, LX/0nt;->b(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    goto :goto_0
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 579907
    iget-object v0, p0, LX/3RG;->d:LX/3RK;

    const/16 v1, 0x2730

    invoke-virtual {v0, v1}, LX/3RK;->a(I)V

    .line 579908
    return-void
.end method

.method public final a(LX/3RF;)V
    .locals 2

    .prologue
    .line 579898
    sget-object v0, LX/3RF;->USER_LOGGED_OUT:LX/3RF;

    if-ne p1, v0, :cond_1

    .line 579899
    iget-object v0, p0, LX/3RG;->d:LX/3RK;

    const/16 v1, 0x2714

    invoke-virtual {v0, v1}, LX/3RK;->a(I)V

    .line 579900
    :cond_0
    :goto_0
    return-void

    .line 579901
    :cond_1
    sget-object v0, LX/3RF;->NEW_BUILD:LX/3RF;

    if-ne p1, v0, :cond_2

    .line 579902
    iget-object v0, p0, LX/3RG;->d:LX/3RK;

    const/16 v1, 0x2717

    invoke-virtual {v0, v1}, LX/3RK;->a(I)V

    goto :goto_0

    .line 579903
    :cond_2
    sget-object v0, LX/3RF;->TINCAN_MESSAGE_REQUEST:LX/3RF;

    if-ne p1, v0, :cond_3

    .line 579904
    iget-object v0, p0, LX/3RG;->d:LX/3RK;

    const/16 v1, 0x272e

    invoke-virtual {v0, v1}, LX/3RK;->a(I)V

    goto :goto_0

    .line 579905
    :cond_3
    sget-object v0, LX/3RF;->JOIN_REQUEST:LX/3RF;

    if-ne p1, v0, :cond_0

    .line 579906
    iget-object v0, p0, LX/3RG;->D:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JkU;

    invoke-virtual {v0}, LX/JkU;->a()V

    goto :goto_0
.end method

.method public final a(LX/3pw;LX/3pl;LX/2HB;Landroid/app/PendingIntent;Lcom/facebook/messaging/notify/NewMessageNotification;)V
    .locals 10
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v9, 0x1

    const/high16 v8, 0x10000000

    const/4 v2, 0x0

    .line 579808
    iget-object v4, p5, Lcom/facebook/messaging/notify/NewMessageNotification;->b:Lcom/facebook/messaging/model/messages/Message;

    .line 579809
    iget-object v5, p5, Lcom/facebook/messaging/notify/NewMessageNotification;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 579810
    invoke-static {v4}, LX/6jR;->a(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 579811
    invoke-static {p0, v5}, LX/3RG;->h(LX/3RG;Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 579812
    if-eqz v5, :cond_4

    invoke-static {v5}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/3RG;->B:LX/2Or;

    invoke-virtual {v0}, LX/2Or;->p()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 579813
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LX/3RG;->b:Landroid/content/Context;

    const-class v3, LX/FMm;

    invoke-direct {v0, v1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 579814
    const-string v1, "com.facebook.messaging.sms.LIKE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 579815
    const-string v1, "from_notification"

    invoke-virtual {v0, v1, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 579816
    const-string v1, "trigger"

    const-string v3, "notification"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 579817
    const-string v1, "thread_key"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 579818
    const-string v1, "thread_id"

    invoke-virtual {v5}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v6

    invoke-virtual {v0, v1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 579819
    iget-object v1, p0, LX/3RG;->b:Landroid/content/Context;

    iget-object v3, p0, LX/3RG;->l:Ljava/util/Random;

    invoke-virtual {v3}, Ljava/util/Random;->nextInt()I

    move-result v3

    invoke-static {v1, v3, v0, v8}, LX/0nt;->b(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 579820
    new-instance v1, Landroid/content/Intent;

    iget-object v3, p0, LX/3RG;->b:Landroid/content/Context;

    const-class v6, LX/FMm;

    invoke-direct {v1, v3, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 579821
    const-string v3, "com.facebook.messaging.sms.DISMISS_NOTIFICATION"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 579822
    const-string v3, "thread_key"

    invoke-virtual {v1, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 579823
    iget-object v3, p0, LX/3RG;->b:Landroid/content/Context;

    iget-object v6, p0, LX/3RG;->l:Ljava/util/Random;

    invoke-virtual {v6}, Ljava/util/Random;->nextInt()I

    move-result v6

    invoke-static {v3, v6, v1, v8}, LX/0nt;->b(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 579824
    invoke-virtual {p3, v1}, LX/2HB;->b(Landroid/app/PendingIntent;)LX/2HB;

    .line 579825
    :goto_0
    const v1, 0x7f021253

    iget-object v3, p0, LX/3RG;->b:Landroid/content/Context;

    const v6, 0x7f0801e8

    invoke-virtual {v3, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p3, v1, v3, v0}, LX/2HB;->a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)LX/2HB;

    .line 579826
    new-instance v1, LX/3pX;

    const v3, 0x7f021254

    iget-object v6, p0, LX/3RG;->b:Landroid/content/Context;

    const v7, 0x7f0801e8

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v1, v3, v6, v0}, LX/3pX;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 579827
    new-instance v0, LX/3pZ;

    invoke-direct {v0}, LX/3pZ;-><init>()V

    invoke-virtual {v0, v2}, LX/3pZ;->a(Z)LX/3pZ;

    move-result-object v0

    .line 579828
    invoke-interface {v0, v1}, LX/3pY;->a(LX/3pX;)LX/3pX;

    .line 579829
    invoke-virtual {v1}, LX/3pX;->b()LX/3pb;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/3pw;->a(LX/3pb;)LX/3pw;

    .line 579830
    :cond_0
    :goto_1
    invoke-static {v5}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v0

    if-nez v0, :cond_9

    invoke-static {p0, v5}, LX/3RG;->h(LX/3RG;Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v0

    if-eqz v0, :cond_9

    const/4 v0, 0x1

    :goto_2
    move v0, v0

    .line 579831
    if-eqz v0, :cond_1

    .line 579832
    new-instance v0, LX/3pX;

    const v1, 0x7f021259

    iget-object v3, p0, LX/3RG;->c:Landroid/content/res/Resources;

    const v6, 0x7f0804a9

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v3, p4}, LX/3pX;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 579833
    new-instance v1, LX/3qF;

    const-string v3, "voice_reply"

    invoke-direct {v1, v3}, LX/3qF;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/3RG;->c:Landroid/content/res/Resources;

    const v6, 0x7f0804a9

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 579834
    iput-object v3, v1, LX/3qF;->b:Ljava/lang/CharSequence;

    .line 579835
    move-object v1, v1

    .line 579836
    invoke-virtual {v1}, LX/3qF;->a()LX/3qL;

    move-result-object v1

    .line 579837
    invoke-virtual {v0, v1}, LX/3pX;->a(LX/3qL;)LX/3pX;

    .line 579838
    new-instance v1, LX/3pZ;

    invoke-direct {v1}, LX/3pZ;-><init>()V

    invoke-virtual {v1, v2}, LX/3pZ;->a(Z)LX/3pZ;

    move-result-object v1

    .line 579839
    invoke-interface {v1, v0}, LX/3pY;->a(LX/3pX;)LX/3pX;

    .line 579840
    invoke-virtual {v0}, LX/3pX;->b()LX/3pb;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/3pw;->a(LX/3pb;)LX/3pw;

    .line 579841
    :cond_1
    const/4 v0, 0x0

    .line 579842
    iget-object v1, p0, LX/3RG;->A:LX/0Uh;

    const/16 v3, 0x216

    invoke-virtual {v1, v3, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {p0, v5}, LX/3RG;->h(LX/3RG;Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x1

    :cond_2
    move v0, v0

    .line 579843
    if-eqz v0, :cond_3

    .line 579844
    new-instance v0, LX/3pX;

    const v1, 0x7f021259

    iget-object v3, p0, LX/3RG;->c:Landroid/content/res/Resources;

    const v6, 0x7f0804a9

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v3, p4}, LX/3pX;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 579845
    new-instance v1, LX/3qF;

    const-string v3, "direct_reply"

    invoke-direct {v1, v3}, LX/3qF;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/3RG;->c:Landroid/content/res/Resources;

    const v6, 0x7f0804a9

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 579846
    iput-object v3, v1, LX/3qF;->b:Ljava/lang/CharSequence;

    .line 579847
    move-object v1, v1

    .line 579848
    invoke-virtual {v1}, LX/3qF;->a()LX/3qL;

    move-result-object v1

    .line 579849
    invoke-virtual {v0, v1}, LX/3pX;->a(LX/3qL;)LX/3pX;

    .line 579850
    invoke-virtual {v0}, LX/3pX;->b()LX/3pb;

    move-result-object v0

    invoke-virtual {p3, v0}, LX/2HB;->a(LX/3pb;)LX/2HB;

    .line 579851
    :cond_3
    iget-object v0, p0, LX/3RG;->q:LX/3Re;

    invoke-virtual {v0, v5}, LX/3Re;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 579852
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LX/3RG;->b:Landroid/content/Context;

    const-class v3, Lcom/facebook/messaging/mutators/ThreadNotificationsDialogActivity;

    invoke-direct {v0, v1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 579853
    const-string v1, "thread_key"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 579854
    iget-object v1, p0, LX/3RG;->l:Ljava/util/Random;

    invoke-virtual {v1}, Ljava/util/Random;->nextInt()I

    move-result v1

    .line 579855
    iget-object v3, p0, LX/3RG;->b:Landroid/content/Context;

    const/4 v6, 0x0

    invoke-static {v3, v1, v0, v6}, LX/0nt;->a(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    move-object v0, v0

    .line 579856
    const v1, 0x7f021257

    iget-object v3, p0, LX/3RG;->b:Landroid/content/Context;

    const v6, 0x7f080507

    invoke-virtual {v3, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p3, v1, v3, v0}, LX/2HB;->a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)LX/2HB;

    .line 579857
    new-instance v6, LX/3pX;

    const v1, 0x7f021258

    iget-object v3, p0, LX/3RG;->b:Landroid/content/Context;

    const v7, 0x7f080507

    invoke-virtual {v3, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v6, v1, v3, v0}, LX/3pX;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 579858
    iget-object v0, p0, LX/3RG;->v:LX/3Rj;

    invoke-virtual {v0, v5}, LX/3Rj;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Ljava/util/List;

    move-result-object v0

    .line 579859
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v7, v1, [Ljava/lang/String;

    .line 579860
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v1, v2

    :goto_3
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jui;

    .line 579861
    add-int/lit8 v3, v1, 0x1

    iget-object v0, v0, LX/Jui;->b:Ljava/lang/String;

    aput-object v0, v7, v1

    move v1, v3

    .line 579862
    goto :goto_3

    .line 579863
    :cond_4
    sget-object v0, LX/3GK;->b:Ljava/lang/String;

    invoke-virtual {p0, v5, v0}, LX/3RG;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v0

    goto/16 :goto_0

    .line 579864
    :cond_5
    if-eqz v5, :cond_0

    .line 579865
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LX/3RG;->b:Landroid/content/Context;

    const-class v3, LX/FMm;

    invoke-direct {v0, v1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 579866
    const-string v1, "com.facebook.messaging.sms.SHORTCODE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 579867
    const-string v1, "thread_id"

    invoke-virtual {v5}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v6

    invoke-virtual {v0, v1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 579868
    iget-object v1, p0, LX/3RG;->l:Ljava/util/Random;

    invoke-virtual {v1}, Ljava/util/Random;->nextInt()I

    move-result v1

    .line 579869
    iget-object v3, p0, LX/3RG;->b:Landroid/content/Context;

    invoke-static {v3, v1, v0, v8}, LX/0nt;->b(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 579870
    iget-object v3, p0, LX/3RG;->b:Landroid/content/Context;

    const v6, 0x7f080228

    invoke-virtual {v3, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p3, v2, v3, v1}, LX/2HB;->a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)LX/2HB;

    .line 579871
    invoke-static {v4}, LX/6jR;->b(Lcom/facebook/messaging/model/messages/Message;)Ljava/lang/String;

    move-result-object v1

    .line 579872
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 579873
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3, v0}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 579874
    const-string v0, "string_to_copy"

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 579875
    iget-object v0, p0, LX/3RG;->l:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextInt()I

    move-result v0

    .line 579876
    iget-object v6, p0, LX/3RG;->b:Landroid/content/Context;

    invoke-static {v6, v0, v3, v8}, LX/0nt;->b(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 579877
    iget-object v3, p0, LX/3RG;->b:Landroid/content/Context;

    const v6, 0x7f0804aa

    new-array v7, v9, [Ljava/lang/Object;

    aput-object v1, v7, v2

    invoke-virtual {v3, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v2, v1, v0}, LX/2HB;->a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)LX/2HB;

    goto/16 :goto_1

    .line 579878
    :cond_6
    new-instance v0, LX/3qF;

    const-string v1, "voice_reply"

    invoke-direct {v0, v1}, LX/3qF;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/3RG;->c:Landroid/content/res/Resources;

    const v3, 0x7f08029f

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 579879
    iput-object v1, v0, LX/3qF;->b:Ljava/lang/CharSequence;

    .line 579880
    move-object v0, v0

    .line 579881
    iput-boolean v2, v0, LX/3qF;->d:Z

    .line 579882
    move-object v0, v0

    .line 579883
    iput-object v7, v0, LX/3qF;->c:[Ljava/lang/CharSequence;

    .line 579884
    move-object v0, v0

    .line 579885
    invoke-virtual {v0}, LX/3qF;->a()LX/3qL;

    move-result-object v0

    .line 579886
    invoke-virtual {v6, v0}, LX/3pX;->a(LX/3qL;)LX/3pX;

    .line 579887
    new-instance v0, LX/3pZ;

    invoke-direct {v0}, LX/3pZ;-><init>()V

    invoke-virtual {v0, v2}, LX/3pZ;->a(Z)LX/3pZ;

    move-result-object v0

    .line 579888
    invoke-interface {v0, v6}, LX/3pY;->a(LX/3pX;)LX/3pX;

    .line 579889
    invoke-virtual {v6}, LX/3pX;->b()LX/3pb;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/3pw;->a(LX/3pb;)LX/3pw;

    .line 579890
    :cond_7
    const/4 v0, 0x1

    invoke-static {p1, v0, v2}, LX/3pw;->a(LX/3pw;IZ)V

    .line 579891
    invoke-interface {p1, p3}, LX/3pk;->a(LX/2HB;)LX/2HB;

    .line 579892
    iget-object v0, p0, LX/3RG;->A:LX/0Uh;

    const/16 v1, 0x102

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 579893
    invoke-interface {p2, p3}, LX/3pk;->a(LX/2HB;)LX/2HB;

    .line 579894
    :cond_8
    invoke-virtual {p3}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v0

    .line 579895
    iget-object v1, p0, LX/3RG;->m:LX/2bC;

    iget-object v2, v4, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    iget-object v3, p5, Lcom/facebook/messaging/notify/NewMessageNotification;->f:Lcom/facebook/push/PushProperty;

    iget-object v3, v3, Lcom/facebook/push/PushProperty;->a:LX/3B4;

    invoke-virtual {v3}, LX/3B4;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p5, Lcom/facebook/messaging/notify/NewMessageNotification;->f:Lcom/facebook/push/PushProperty;

    iget-object v4, v4, Lcom/facebook/push/PushProperty;->b:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-virtual {v1, v2, v3, v4, v6}, LX/2bC;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 579896
    iget-object v1, p0, LX/3RG;->d:LX/3RK;

    invoke-virtual {v5}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p5}, Lcom/facebook/messaging/notify/NewMessageNotification;->a()I

    move-result v3

    invoke-virtual {v1, v2, v3, v0}, LX/3RK;->a(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 579897
    return-void

    :cond_9
    const/4 v0, 0x0

    goto/16 :goto_2
.end method

.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V
    .locals 4

    .prologue
    .line 579805
    iget-object v0, p0, LX/3RG;->d:LX/3RK;

    invoke-virtual {p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x271a

    invoke-virtual {v0, v1, v2}, LX/3RK;->a(Ljava/lang/String;I)V

    .line 579806
    iget-object v0, p0, LX/3RG;->d:LX/3RK;

    invoke-virtual {p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->toString()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x2739

    invoke-virtual {v0, v1, v2}, LX/3RK;->a(Ljava/lang/String;I)V

    .line 579807
    return-void
.end method

.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 579721
    iget-object v0, p0, LX/3RG;->d:LX/3RK;

    invoke-virtual {p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->toString()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x2710

    invoke-virtual {v0, v1, v2}, LX/3RK;->a(Ljava/lang/String;I)V

    .line 579722
    iget-object v0, p0, LX/3RG;->d:LX/3RK;

    invoke-virtual {p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->toString()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x2724

    invoke-virtual {v0, v1, v2}, LX/3RK;->a(Ljava/lang/String;I)V

    .line 579723
    iget-object v0, p0, LX/3RG;->q:LX/3Re;

    invoke-virtual {p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/3Re;->a(Ljava/lang/String;)V

    .line 579724
    return-void
.end method

.method public final a(Lcom/facebook/messaging/notify/CalleeReadyNotification;)V
    .locals 7

    .prologue
    .line 579790
    iget-object v0, p1, Lcom/facebook/messaging/notify/CalleeReadyNotification;->c:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 579791
    iget-object v0, p0, LX/3RG;->E:LX/3Ec;

    invoke-virtual {v0, v4, v5}, LX/3Ec;->a(J)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v3

    .line 579792
    new-instance v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;

    new-instance v1, Lcom/facebook/user/model/UserKey;

    sget-object v2, LX/0XG;->FACEBOOK:LX/0XG;

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v1, v2, v6}, Lcom/facebook/user/model/UserKey;-><init>(LX/0XG;Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/facebook/messaging/model/messages/ParticipantInfo;-><init>(Lcom/facebook/user/model/UserKey;Ljava/lang/String;)V

    .line 579793
    iget-object v1, p0, LX/3RG;->r:Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;

    invoke-virtual {v1, v0}, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->a(Lcom/facebook/messaging/model/messages/ParticipantInfo;)LX/1ca;

    move-result-object v6

    .line 579794
    if-eqz v6, :cond_0

    .line 579795
    new-instance v0, LX/Jub;

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, LX/Jub;-><init>(LX/3RG;Lcom/facebook/messaging/notify/CalleeReadyNotification;Lcom/facebook/messaging/model/threadkey/ThreadKey;J)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v1

    invoke-interface {v6, v0, v1}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V

    .line 579796
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/messaging/notify/EventReminderNotification;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 579302
    iget-object v0, p1, Lcom/facebook/messaging/notify/EventReminderNotification;->f:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 579303
    iget-object v1, p0, LX/3RG;->r:Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;

    new-instance v2, LX/JuW;

    invoke-direct {v2, p0, p1, v0}, LX/JuW;-><init>(LX/3RG;Lcom/facebook/messaging/notify/EventReminderNotification;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    invoke-virtual {v1, v0, v2, v3, v3}, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/FJf;Lcom/facebook/messaging/model/messages/ParticipantInfo;Landroid/graphics/Bitmap;)V

    .line 579304
    return-void
.end method

.method public final a(Lcom/facebook/messaging/notify/FailedToSendMessageNotification;)V
    .locals 4

    .prologue
    .line 579441
    sget-object v0, LX/JuT;->c:[I

    iget-object v1, p1, Lcom/facebook/messaging/notify/FailedToSendMessageNotification;->b:LX/Dhu;

    invoke-virtual {v1}, LX/Dhu;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 579442
    :cond_0
    :goto_0
    :pswitch_0
    const/4 v0, 0x1

    .line 579443
    iput-boolean v0, p1, Lcom/facebook/messaging/notify/FailedToSendMessageNotification;->c:Z

    .line 579444
    invoke-virtual {p1}, Lcom/facebook/messaging/notify/MessagingNotification;->i()V

    .line 579445
    return-void

    .line 579446
    :pswitch_1
    iget-object v0, p1, Lcom/facebook/messaging/notify/FailedToSendMessageNotification;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    if-eqz v0, :cond_0

    .line 579447
    iget-object v0, p0, LX/3RG;->g:LX/3GK;

    iget-object v1, p1, Lcom/facebook/messaging/notify/FailedToSendMessageNotification;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-interface {v0, v1}, LX/3GK;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, LX/3RG;->c:Landroid/content/res/Resources;

    const v2, 0x7f0802ce

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/3RG;->c:Landroid/content/res/Resources;

    const v3, 0x7f0802d1

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p1, Lcom/facebook/messaging/notify/FailedToSendMessageNotification;->b:LX/Dhu;

    invoke-static {p0, v0, v1, v2, v3}, LX/3RG;->a(LX/3RG;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;LX/Dhu;)V

    goto :goto_0

    .line 579448
    :pswitch_2
    iget-object v0, p1, Lcom/facebook/messaging/notify/FailedToSendMessageNotification;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    if-eqz v0, :cond_0

    .line 579449
    iget-object v0, p0, LX/3RG;->g:LX/3GK;

    iget-object v1, p1, Lcom/facebook/messaging/notify/FailedToSendMessageNotification;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-interface {v0, v1}, LX/3GK;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, LX/3RG;->c:Landroid/content/res/Resources;

    const v2, 0x7f0802ce

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/3RG;->c:Landroid/content/res/Resources;

    const v3, 0x7f0802d2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p1, Lcom/facebook/messaging/notify/FailedToSendMessageNotification;->b:LX/Dhu;

    invoke-static {p0, v0, v1, v2, v3}, LX/3RG;->a(LX/3RG;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;LX/Dhu;)V

    goto :goto_0

    .line 579450
    :pswitch_3
    iget-object v0, p1, Lcom/facebook/messaging/notify/FailedToSendMessageNotification;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    if-eqz v0, :cond_0

    .line 579451
    iget-object v0, p0, LX/3RG;->g:LX/3GK;

    iget-object v1, p1, Lcom/facebook/messaging/notify/FailedToSendMessageNotification;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-interface {v0, v1}, LX/3GK;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Landroid/content/Intent;

    move-result-object v0

    .line 579452
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 579453
    const-string v1, "from_notification"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 579454
    const-string v1, "trigger"

    const-string v2, "notification"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 579455
    const-string v1, "send_failure_reason"

    sget-object v2, LX/Dhu;->SMS_MSS_ERROR:LX/Dhu;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 579456
    invoke-static {p0, v0}, LX/3RG;->a(LX/3RG;Landroid/content/Intent;)Landroid/app/PendingIntent;

    move-result-object v0

    .line 579457
    iget-object v1, p0, LX/3RG;->c:Landroid/content/res/Resources;

    const v2, 0x7f080011

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/3RG;->c:Landroid/content/res/Resources;

    const v3, 0x7f080304

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v1, v2}, LX/3RG;->a(LX/3RG;Ljava/lang/String;Ljava/lang/String;)LX/2HB;

    move-result-object v1

    .line 579458
    iput-object v0, v1, LX/2HB;->d:Landroid/app/PendingIntent;

    .line 579459
    iget-object v0, p0, LX/3RG;->d:LX/3RK;

    const/16 v2, 0x2711

    invoke-virtual {v1}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, LX/3RK;->a(ILandroid/app/Notification;)V

    goto/16 :goto_0

    .line 579460
    :pswitch_4
    iget-object v0, p1, Lcom/facebook/messaging/notify/FailedToSendMessageNotification;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    if-eqz v0, :cond_0

    .line 579461
    iget-object v0, p0, LX/3RG;->g:LX/3GK;

    iget-object v1, p1, Lcom/facebook/messaging/notify/FailedToSendMessageNotification;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-interface {v0, v1}, LX/3GK;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, LX/3RG;->c:Landroid/content/res/Resources;

    const v2, 0x7f080011

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/3RG;->c:Landroid/content/res/Resources;

    const v3, 0x7f0802c6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p1, Lcom/facebook/messaging/notify/FailedToSendMessageNotification;->b:LX/Dhu;

    invoke-static {p0, v0, v1, v2, v3}, LX/3RG;->a(LX/3RG;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;LX/Dhu;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final a(Lcom/facebook/messaging/notify/FailedToSetProfilePictureNotification;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 579424
    iget-object v0, p0, LX/3RG;->e:LX/2zj;

    invoke-interface {v0}, LX/2zj;->g()I

    move-result v0

    .line 579425
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, LX/3RH;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "settings/retryprofilepictureupload"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 579426
    const-string v2, "from_notification"

    invoke-virtual {v1, v2, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 579427
    iget-object v2, p0, LX/3RG;->b:Landroid/content/Context;

    const/high16 v3, 0x8000000

    invoke-static {v2, v7, v1, v3}, LX/0nt;->a(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 579428
    new-instance v2, LX/2HB;

    iget-object v3, p0, LX/3RG;->b:Landroid/content/Context;

    invoke-direct {v2, v3}, LX/2HB;-><init>(Landroid/content/Context;)V

    iget-object v3, p1, Lcom/facebook/messaging/notify/FailedToSetProfilePictureNotification;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/2HB;->a(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v2

    iget-object v3, p1, Lcom/facebook/messaging/notify/FailedToSetProfilePictureNotification;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/2HB;->b(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v2

    iget-object v3, p1, Lcom/facebook/messaging/notify/FailedToSetProfilePictureNotification;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/2HB;->e(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v2

    .line 579429
    iput v6, v2, LX/2HB;->j:I

    .line 579430
    move-object v2, v2

    .line 579431
    new-instance v3, LX/3pe;

    invoke-direct {v3}, LX/3pe;-><init>()V

    iget-object v4, p1, Lcom/facebook/messaging/notify/FailedToSetProfilePictureNotification;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, LX/3pe;->b(Ljava/lang/CharSequence;)LX/3pe;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/2HB;->a(LX/3pc;)LX/2HB;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/2HB;->a(I)LX/2HB;

    move-result-object v0

    .line 579432
    iput-object v1, v0, LX/2HB;->d:Landroid/app/PendingIntent;

    .line 579433
    move-object v0, v0

    .line 579434
    invoke-virtual {v0, v6}, LX/2HB;->c(Z)LX/2HB;

    move-result-object v0

    iget-object v2, p1, Lcom/facebook/messaging/notify/FailedToSetProfilePictureNotification;->c:Ljava/lang/String;

    invoke-virtual {v0, v7, v2, v1}, LX/2HB;->a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)LX/2HB;

    move-result-object v0

    .line 579435
    iget-object v1, p0, LX/3RG;->f:LX/3RQ;

    new-instance v2, LX/Dhq;

    invoke-direct {v2}, LX/Dhq;-><init>()V

    invoke-virtual {v1, v0, v2, v5, v5}, LX/3RQ;->a(LX/2HB;LX/Dhq;Lcom/facebook/messaging/push/flags/ServerMessageAlertFlags;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 579436
    iget-object v1, p0, LX/3RG;->m:LX/2bC;

    const-string v2, "FAILED_TO_UPLOAD_PROFILE_PIC"

    invoke-virtual {v1, v5, v2, v5, v5}, LX/2bC;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 579437
    iget-object v1, p0, LX/3RG;->d:LX/3RK;

    const/16 v2, 0x2733

    invoke-virtual {v0}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/3RK;->a(ILandroid/app/Notification;)V

    .line 579438
    iput-boolean v6, p1, Lcom/facebook/messaging/notify/FailedToSetProfilePictureNotification;->d:Z

    .line 579439
    invoke-virtual {p1}, Lcom/facebook/messaging/notify/MessagingNotification;->i()V

    .line 579440
    return-void
.end method

.method public final a(Lcom/facebook/messaging/notify/FriendInstallNotification;)V
    .locals 6

    .prologue
    .line 579410
    iget-object v0, p0, LX/3RG;->e:LX/2zj;

    invoke-interface {v0}, LX/2zj;->g()I

    move-result v0

    .line 579411
    iget-object v1, p0, LX/3RG;->g:LX/3GK;

    .line 579412
    iget-object v2, p1, Lcom/facebook/messaging/notify/FriendInstallNotification;->a:Ljava/lang/String;

    move-object v2, v2

    .line 579413
    invoke-interface {v1, v2}, LX/3GK;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 579414
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 579415
    invoke-virtual {v2, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 579416
    const-string v1, "from_notification"

    const/4 v3, 0x1

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 579417
    new-instance v1, Lcom/facebook/messaging/model/messages/ParticipantInfo;

    new-instance v3, Lcom/facebook/user/model/UserKey;

    sget-object v4, LX/0XG;->FACEBOOK:LX/0XG;

    .line 579418
    iget-object v5, p1, Lcom/facebook/messaging/notify/FriendInstallNotification;->a:Ljava/lang/String;

    move-object v5, v5

    .line 579419
    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/facebook/user/model/UserKey;-><init>(LX/0XG;Ljava/lang/String;)V

    const/4 v4, 0x0

    invoke-direct {v1, v3, v4}, Lcom/facebook/messaging/model/messages/ParticipantInfo;-><init>(Lcom/facebook/user/model/UserKey;Ljava/lang/String;)V

    .line 579420
    iget-object v3, p0, LX/3RG;->r:Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;

    invoke-virtual {v3, v1}, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->a(Lcom/facebook/messaging/model/messages/ParticipantInfo;)LX/1ca;

    move-result-object v1

    .line 579421
    if-eqz v1, :cond_0

    .line 579422
    new-instance v3, LX/JuU;

    invoke-direct {v3, p0, v2, p1, v0}, LX/JuU;-><init>(LX/3RG;Landroid/content/Intent;Lcom/facebook/messaging/notify/FriendInstallNotification;I)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v0

    invoke-interface {v1, v3, v0}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V

    .line 579423
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/messaging/notify/IncomingCallNotification;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 579402
    iget-object v0, p1, Lcom/facebook/messaging/notify/IncomingCallNotification;->c:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 579403
    new-instance v2, Lcom/facebook/messaging/model/messages/ParticipantInfo;

    new-instance v3, Lcom/facebook/user/model/UserKey;

    sget-object v4, LX/0XG;->FACEBOOK:LX/0XG;

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/facebook/user/model/UserKey;-><init>(LX/0XG;Ljava/lang/String;)V

    invoke-direct {v2, v3, v6}, Lcom/facebook/messaging/model/messages/ParticipantInfo;-><init>(Lcom/facebook/user/model/UserKey;Ljava/lang/String;)V

    .line 579404
    iget-object v3, p1, Lcom/facebook/messaging/notify/IncomingCallNotification;->i:LX/Dhy;

    sget-object v4, LX/Dhy;->CONFERENCE:LX/Dhy;

    if-ne v3, v4, :cond_1

    .line 579405
    iget-object v3, p0, LX/3RG;->r:Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;

    iget-object v4, p1, Lcom/facebook/messaging/notify/IncomingCallNotification;->h:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    new-instance v5, LX/JuZ;

    invoke-direct {v5, p0, p1, v0, v1}, LX/JuZ;-><init>(LX/3RG;Lcom/facebook/messaging/notify/IncomingCallNotification;J)V

    invoke-virtual {v3, v4, v5, v2, v6}, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/FJf;Lcom/facebook/messaging/model/messages/ParticipantInfo;Landroid/graphics/Bitmap;)V

    .line 579406
    :cond_0
    :goto_0
    return-void

    .line 579407
    :cond_1
    iget-object v3, p0, LX/3RG;->r:Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;

    invoke-virtual {v3, v2}, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->a(Lcom/facebook/messaging/model/messages/ParticipantInfo;)LX/1ca;

    move-result-object v2

    .line 579408
    if-eqz v2, :cond_0

    .line 579409
    new-instance v3, LX/Jua;

    invoke-direct {v3, p0, p1, v0, v1}, LX/Jua;-><init>(LX/3RG;Lcom/facebook/messaging/notify/IncomingCallNotification;J)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v0

    invoke-interface {v2, v3, v0}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/messaging/notify/JoinRequestNotification;)V
    .locals 3

    .prologue
    .line 579400
    iget-object v0, p0, LX/3RG;->D:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JkU;

    iget-object v1, p0, LX/3RG;->e:LX/2zj;

    invoke-interface {v1}, LX/2zj;->g()I

    move-result v1

    const-string v2, "join_requests"

    invoke-static {p0, v2}, LX/3RG;->c(LX/3RG;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v0, p1, v1, v2}, LX/JkU;->a(Lcom/facebook/messaging/notify/JoinRequestNotification;ILandroid/app/PendingIntent;)V

    .line 579401
    return-void
.end method

.method public final a(Lcom/facebook/messaging/notify/LoggedOutMessageNotification;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 579392
    iget-object v0, p0, LX/3RG;->m:LX/2bC;

    invoke-virtual {p1}, Lcom/facebook/messaging/notify/LoggedOutMessageNotification;->d()LX/3B4;

    move-result-object v1

    invoke-virtual {v1}, LX/3B4;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/messaging/notify/LoggedOutMessageNotification;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v3, v1, v2, v3}, LX/2bC;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 579393
    iget-object v0, p1, Lcom/facebook/messaging/notify/LoggedOutMessageNotification;->a:Ljava/lang/String;

    move-object v0, v0

    .line 579394
    iget-object v1, p1, Lcom/facebook/messaging/notify/LoggedOutMessageNotification;->b:Ljava/lang/String;

    move-object v1, v1

    .line 579395
    iget-object v2, p1, Lcom/facebook/messaging/notify/LoggedOutMessageNotification;->b:Ljava/lang/String;

    move-object v2, v2

    .line 579396
    invoke-direct {p0, p1, v0, v1, v2}, LX/3RG;->a(Lcom/facebook/messaging/notify/MessagingNotification;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 579397
    const/4 v0, 0x1

    .line 579398
    iput-boolean v0, p1, Lcom/facebook/messaging/notify/LoggedOutMessageNotification;->d:Z

    .line 579399
    return-void
.end method

.method public final a(Lcom/facebook/messaging/notify/MentionNotification;)V
    .locals 5

    .prologue
    .line 579388
    iget-object v0, p1, Lcom/facebook/messaging/notify/MentionNotification;->b:Lcom/facebook/messaging/model/messages/Message;

    .line 579389
    iget-object v1, v0, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 579390
    iget-object v2, p0, LX/3RG;->r:Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;

    new-instance v3, LX/JuX;

    invoke-direct {v3, p0, v1, v0, p1}, LX/JuX;-><init>(LX/3RG;Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/notify/MentionNotification;)V

    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    const/4 v4, 0x0

    invoke-virtual {v2, v1, v3, v0, v4}, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/FJf;Lcom/facebook/messaging/model/messages/ParticipantInfo;Landroid/graphics/Bitmap;)V

    .line 579391
    return-void
.end method

.method public final a(Lcom/facebook/messaging/notify/MessageReactionNotification;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 579386
    iget-object v6, p0, LX/3RG;->r:Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;

    iget-object v7, p1, Lcom/facebook/messaging/notify/MessageReactionNotification;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    new-instance v0, LX/Jud;

    iget-object v2, p1, Lcom/facebook/messaging/notify/MessageReactionNotification;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v3, p1, Lcom/facebook/messaging/notify/MessageReactionNotification;->b:Ljava/lang/String;

    new-instance v5, LX/JuR;

    invoke-direct {v5, p0, p1}, LX/JuR;-><init>(LX/3RG;Lcom/facebook/messaging/notify/MessageReactionNotification;)V

    move-object v1, p0

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, LX/Jud;-><init>(LX/3RG;Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;Lcom/facebook/messaging/notify/MessagingNotification;LX/JuR;)V

    invoke-virtual {v6, v7, v0, v8, v8}, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/FJf;Lcom/facebook/messaging/model/messages/ParticipantInfo;Landroid/graphics/Bitmap;)V

    .line 579387
    return-void
.end method

.method public final a(Lcom/facebook/messaging/notify/MessageRequestNotification;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 579372
    iget-object v0, p0, LX/3RG;->e:LX/2zj;

    invoke-interface {v0}, LX/2zj;->g()I

    move-result v0

    .line 579373
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    sget-object v3, LX/3RH;->M:Ljava/lang/String;

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 579374
    const-string v2, "from_notification"

    invoke-virtual {v1, v2, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 579375
    iget-object v2, p0, LX/3RG;->b:Landroid/content/Context;

    const/4 v3, 0x0

    const/high16 v4, 0x10000000

    invoke-static {v2, v3, v1, v4}, LX/0nt;->a(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 579376
    const-string v2, "message_request"

    invoke-static {p0, v2}, LX/3RG;->c(LX/3RG;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v2

    .line 579377
    new-instance v3, LX/2HB;

    iget-object v4, p0, LX/3RG;->b:Landroid/content/Context;

    invoke-direct {v3, v4}, LX/2HB;-><init>(Landroid/content/Context;)V

    iget-object v4, p1, Lcom/facebook/messaging/notify/MessageRequestNotification;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, LX/2HB;->a(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v3

    iget-object v4, p1, Lcom/facebook/messaging/notify/MessageRequestNotification;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, LX/2HB;->b(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v3

    invoke-virtual {v3, v0}, LX/2HB;->a(I)LX/2HB;

    move-result-object v0

    .line 579378
    iput-object v1, v0, LX/2HB;->d:Landroid/app/PendingIntent;

    .line 579379
    move-object v0, v0

    .line 579380
    invoke-virtual {v0, v2}, LX/2HB;->b(Landroid/app/PendingIntent;)LX/2HB;

    move-result-object v0

    invoke-virtual {v0, v6}, LX/2HB;->c(Z)LX/2HB;

    move-result-object v0

    .line 579381
    iget-object v1, p0, LX/3RG;->m:LX/2bC;

    const-string v2, "MESSAGE_REQUEST"

    invoke-virtual {v1, v5, v2, v5, v5}, LX/2bC;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 579382
    iget-object v1, p0, LX/3RG;->d:LX/3RK;

    const/16 v2, 0x2722

    invoke-virtual {v0}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v0

    invoke-virtual {v1, v5, v2, v0}, LX/3RK;->a(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 579383
    iput-boolean v6, p1, Lcom/facebook/messaging/notify/MessageRequestNotification;->c:Z

    .line 579384
    invoke-virtual {p1}, Lcom/facebook/messaging/notify/MessagingNotification;->i()V

    .line 579385
    return-void
.end method

.method public final a(Lcom/facebook/messaging/notify/MissedCallNotification;)V
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 579365
    iget-object v0, p1, Lcom/facebook/messaging/notify/MissedCallNotification;->c:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 579366
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-le v0, v1, :cond_0

    const v6, 0x7f021abd

    .line 579367
    :goto_0
    new-instance v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;

    new-instance v1, Lcom/facebook/user/model/UserKey;

    sget-object v2, LX/0XG;->FACEBOOK:LX/0XG;

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/facebook/user/model/UserKey;-><init>(LX/0XG;Ljava/lang/String;)V

    invoke-direct {v0, v1, v9}, Lcom/facebook/messaging/model/messages/ParticipantInfo;-><init>(Lcom/facebook/user/model/UserKey;Ljava/lang/String;)V

    .line 579368
    iget-object v7, p0, LX/3RG;->r:Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;

    iget-object v8, p1, Lcom/facebook/messaging/notify/MissedCallNotification;->i:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    new-instance v1, LX/JuY;

    move-object v2, p0

    move-object v3, p1

    invoke-direct/range {v1 .. v6}, LX/JuY;-><init>(LX/3RG;Lcom/facebook/messaging/notify/MissedCallNotification;JI)V

    invoke-virtual {v7, v8, v1, v0, v9}, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/FJf;Lcom/facebook/messaging/model/messages/ParticipantInfo;Landroid/graphics/Bitmap;)V

    .line 579369
    return-void

    .line 579370
    :cond_0
    const v6, 0x7f021abc

    goto :goto_0
.end method

.method public final a(Lcom/facebook/messaging/notify/MultipleAccountsNewMessagesNotification;)V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 579343
    iget-object v0, p0, LX/3RG;->e:LX/2zj;

    invoke-interface {v0}, LX/2zj;->g()I

    move-result v1

    .line 579344
    sget-object v0, LX/3RG;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 579345
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 579346
    const-string v0, "from_notification"

    invoke-virtual {v2, v0, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 579347
    const-string v0, "extra_account_switch_redirect_source"

    const-string v3, "new_messages_notif"

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 579348
    const-string v3, "multiple_accounts"

    .line 579349
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 579350
    const-string v0, "notif_uid"

    iget-object v5, p1, Lcom/facebook/messaging/notify/MultipleAccountsNewMessagesNotification;->a:Ljava/lang/String;

    invoke-virtual {v4, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 579351
    const-string v0, "unseen_count"

    iget v5, p1, Lcom/facebook/messaging/notify/MultipleAccountsNewMessagesNotification;->e:I

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 579352
    invoke-static {p0, v3, v2, v4}, LX/3RG;->a(LX/3RG;Ljava/lang/String;Landroid/content/Intent;Ljava/util/HashMap;)Landroid/app/PendingIntent;

    move-result-object v2

    .line 579353
    invoke-direct {p0, v3, v4}, LX/3RG;->a(Ljava/lang/String;Ljava/util/HashMap;)Landroid/app/PendingIntent;

    move-result-object v5

    .line 579354
    new-instance v6, LX/2HB;

    iget-object v0, p0, LX/3RG;->b:Landroid/content/Context;

    invoke-direct {v6, v0}, LX/2HB;-><init>(Landroid/content/Context;)V

    iget-object v0, p1, Lcom/facebook/messaging/notify/MultipleAccountsNewMessagesNotification;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3RG;->b:Landroid/content/Context;

    const v7, 0x7f080011

    invoke-virtual {v0, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v6, v0}, LX/2HB;->a(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v0

    iget-object v6, p1, Lcom/facebook/messaging/notify/MultipleAccountsNewMessagesNotification;->c:Ljava/lang/String;

    invoke-virtual {v0, v6}, LX/2HB;->b(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v6

    iget-object v0, p1, Lcom/facebook/messaging/notify/MultipleAccountsNewMessagesNotification;->d:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/facebook/messaging/notify/MultipleAccountsNewMessagesNotification;->c:Ljava/lang/String;

    :goto_1
    invoke-virtual {v6, v0}, LX/2HB;->e(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/2HB;->a(I)LX/2HB;

    move-result-object v0

    new-instance v1, LX/3pe;

    invoke-direct {v1}, LX/3pe;-><init>()V

    iget-object v6, p1, Lcom/facebook/messaging/notify/MultipleAccountsNewMessagesNotification;->c:Ljava/lang/String;

    invoke-virtual {v1, v6}, LX/3pe;->b(Ljava/lang/CharSequence;)LX/3pe;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2HB;->a(LX/3pc;)LX/2HB;

    move-result-object v0

    .line 579355
    iput-object v2, v0, LX/2HB;->d:Landroid/app/PendingIntent;

    .line 579356
    move-object v0, v0

    .line 579357
    invoke-virtual {v0, v5}, LX/2HB;->b(Landroid/app/PendingIntent;)LX/2HB;

    move-result-object v0

    invoke-virtual {v0, v8}, LX/2HB;->c(Z)LX/2HB;

    move-result-object v0

    .line 579358
    iget-object v1, p0, LX/3RG;->C:LX/3S2;

    invoke-virtual {v1, v3, v4}, LX/3S2;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 579359
    iget-object v1, p0, LX/3RG;->f:LX/3RQ;

    new-instance v2, LX/Dhq;

    invoke-direct {v2}, LX/Dhq;-><init>()V

    invoke-virtual {v1, v0, v2, v9, v9}, LX/3RQ;->a(LX/2HB;LX/Dhq;Lcom/facebook/messaging/push/flags/ServerMessageAlertFlags;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 579360
    iget-object v1, p0, LX/3RG;->d:LX/3RK;

    iget-object v2, p1, Lcom/facebook/messaging/notify/MultipleAccountsNewMessagesNotification;->a:Ljava/lang/String;

    const/16 v3, 0x272a

    invoke-virtual {v0}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v0

    invoke-virtual {v1, v2, v3, v0}, LX/3RK;->a(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 579361
    iput-boolean v8, p1, Lcom/facebook/messaging/notify/MultipleAccountsNewMessagesNotification;->f:Z

    .line 579362
    invoke-virtual {p1}, Lcom/facebook/messaging/notify/MessagingNotification;->i()V

    .line 579363
    return-void

    .line 579364
    :cond_0
    iget-object v0, p1, Lcom/facebook/messaging/notify/MultipleAccountsNewMessagesNotification;->b:Ljava/lang/String;

    goto :goto_0

    :cond_1
    iget-object v0, p1, Lcom/facebook/messaging/notify/MultipleAccountsNewMessagesNotification;->d:Ljava/lang/String;

    goto :goto_1
.end method

.method public final a(Lcom/facebook/messaging/notify/NewBuildNotification;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 579324
    iget-object v0, p0, LX/3RG;->e:LX/2zj;

    invoke-interface {v0}, LX/2zj;->g()I

    move-result v0

    .line 579325
    iget-object v1, p0, LX/3RG;->b:Landroid/content/Context;

    const/4 v2, 0x0

    .line 579326
    iget-object v3, p1, Lcom/facebook/messaging/notify/NewBuildNotification;->d:Landroid/content/Intent;

    move-object v3, v3

    .line 579327
    const/high16 v4, 0x8000000

    invoke-static {v1, v2, v3, v4}, LX/0nt;->a(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 579328
    new-instance v2, LX/2HB;

    iget-object v3, p0, LX/3RG;->b:Landroid/content/Context;

    invoke-direct {v2, v3}, LX/2HB;-><init>(Landroid/content/Context;)V

    .line 579329
    iget-object v3, p1, Lcom/facebook/messaging/notify/NewBuildNotification;->a:Ljava/lang/String;

    move-object v3, v3

    .line 579330
    invoke-virtual {v2, v3}, LX/2HB;->a(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v2

    .line 579331
    iget-object v3, p1, Lcom/facebook/messaging/notify/NewBuildNotification;->b:Ljava/lang/String;

    move-object v3, v3

    .line 579332
    invoke-virtual {v2, v3}, LX/2HB;->b(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v2

    .line 579333
    iget-object v3, p1, Lcom/facebook/messaging/notify/NewBuildNotification;->c:Ljava/lang/String;

    move-object v3, v3

    .line 579334
    invoke-virtual {v2, v3}, LX/2HB;->e(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/2HB;->a(I)LX/2HB;

    move-result-object v0

    .line 579335
    iput-object v1, v0, LX/2HB;->d:Landroid/app/PendingIntent;

    .line 579336
    move-object v0, v0

    .line 579337
    invoke-virtual {v0, v5}, LX/2HB;->c(Z)LX/2HB;

    move-result-object v0

    .line 579338
    sget-object v1, LX/3px;->a:LX/3pn;

    invoke-interface {v1, v0}, LX/3pn;->a(LX/2HB;)Landroid/app/Notification;

    move-result-object v1

    move-object v0, v1

    .line 579339
    iget-object v1, p0, LX/3RG;->d:LX/3RK;

    const/4 v2, 0x0

    const/16 v3, 0x2717

    invoke-virtual {v1, v2, v3, v0}, LX/3RK;->a(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 579340
    iput-boolean v5, p1, Lcom/facebook/messaging/notify/NewBuildNotification;->e:Z

    .line 579341
    invoke-virtual {p1}, Lcom/facebook/messaging/notify/MessagingNotification;->i()V

    .line 579342
    return-void
.end method

.method public final a(Lcom/facebook/messaging/notify/NewMessageNotification;)V
    .locals 8

    .prologue
    .line 579313
    iget-object v0, p0, LX/3RG;->r:Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;

    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 579314
    iget-object v1, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->b:Lcom/facebook/messaging/model/messages/Message;

    .line 579315
    iget-object v2, v1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0, v2}, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v2

    .line 579316
    if-nez v2, :cond_0

    .line 579317
    invoke-virtual {v0, v1}, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->b(Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/model/messages/ParticipantInfo;

    move-result-object v1

    invoke-static {v0, v1, v5, v6}, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->a(Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;Lcom/facebook/messaging/model/messages/ParticipantInfo;ZLX/FJf;)LX/1ca;

    .line 579318
    :goto_0
    return-void

    .line 579319
    :cond_0
    iget-object v1, v0, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->h:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FJv;

    invoke-virtual {v1, v2}, LX/FJv;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)LX/8Vc;

    move-result-object v1

    .line 579320
    invoke-interface {v1}, LX/8Vc;->a()I

    move-result v2

    if-ne v2, v5, :cond_1

    .line 579321
    invoke-interface {v1, v3, v3, v3}, LX/8Vc;->a(III)LX/1bf;

    move-result-object v2

    move-object v1, v0

    move v4, v3

    move-object v7, v6

    .line 579322
    invoke-static/range {v1 .. v7}, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->a(Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;LX/1bf;IIZLX/33B;LX/8Vc;)LX/1ca;

    goto :goto_0

    .line 579323
    :cond_1
    invoke-static {v0, v3, v3, v1}, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->a(Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;IILX/8Vc;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/messaging/notify/PaymentNotification;)V
    .locals 4

    .prologue
    .line 579305
    iget-object v0, p0, LX/3RG;->e:LX/2zj;

    invoke-interface {v0}, LX/2zj;->g()I

    move-result v1

    .line 579306
    new-instance v2, Lcom/facebook/user/model/UserKey;

    sget-object v3, LX/0XG;->FACEBOOK:LX/0XG;

    iget-object v0, p1, Lcom/facebook/messaging/notify/PaymentNotification;->d:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p1, Lcom/facebook/messaging/notify/PaymentNotification;->d:Ljava/lang/String;

    :goto_0
    invoke-direct {v2, v3, v0}, Lcom/facebook/user/model/UserKey;-><init>(LX/0XG;Ljava/lang/String;)V

    .line 579307
    new-instance v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;

    const/4 v3, 0x0

    invoke-direct {v0, v2, v3}, Lcom/facebook/messaging/model/messages/ParticipantInfo;-><init>(Lcom/facebook/user/model/UserKey;Ljava/lang/String;)V

    .line 579308
    iget-object v2, p0, LX/3RG;->r:Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;

    invoke-virtual {v2, v0}, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->a(Lcom/facebook/messaging/model/messages/ParticipantInfo;)LX/1ca;

    move-result-object v0

    .line 579309
    if-eqz v0, :cond_0

    .line 579310
    new-instance v2, LX/JuV;

    invoke-direct {v2, p0, p1, v1}, LX/JuV;-><init>(LX/3RG;Lcom/facebook/messaging/notify/PaymentNotification;I)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v1

    invoke-interface {v0, v2, v1}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V

    .line 579311
    :cond_0
    return-void

    .line 579312
    :cond_1
    iget-object v0, p1, Lcom/facebook/messaging/notify/PaymentNotification;->c:Ljava/lang/String;

    goto :goto_0
.end method

.method public final a(Lcom/facebook/messaging/notify/PromotionNotification;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 579286
    iget-object v0, p0, LX/3RG;->e:LX/2zj;

    invoke-interface {v0}, LX/2zj;->g()I

    move-result v0

    .line 579287
    iget-object v1, p1, Lcom/facebook/messaging/notify/PromotionNotification;->d:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 579288
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 579289
    const-string v3, "from_notification"

    invoke-virtual {v2, v3, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 579290
    invoke-static {v2, v1}, LX/3RG;->a(Landroid/content/Intent;Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    .line 579291
    iget-object v2, p0, LX/3RG;->b:Landroid/content/Context;

    const/4 v3, 0x0

    const/high16 v4, 0x8000000

    invoke-static {v2, v3, v1, v4}, LX/0nt;->a(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 579292
    const-string v2, "promotion"

    invoke-static {p0, v2}, LX/3RG;->c(LX/3RG;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v2

    .line 579293
    new-instance v3, LX/2HB;

    iget-object v4, p0, LX/3RG;->b:Landroid/content/Context;

    invoke-direct {v3, v4}, LX/2HB;-><init>(Landroid/content/Context;)V

    iget-object v4, p1, Lcom/facebook/messaging/notify/PromotionNotification;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, LX/2HB;->a(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v3

    iget-object v4, p1, Lcom/facebook/messaging/notify/PromotionNotification;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, LX/2HB;->b(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v3

    iget-object v4, p1, Lcom/facebook/messaging/notify/PromotionNotification;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, LX/2HB;->e(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v3

    invoke-virtual {v3, v0}, LX/2HB;->a(I)LX/2HB;

    move-result-object v0

    new-instance v3, LX/3pe;

    invoke-direct {v3}, LX/3pe;-><init>()V

    iget-object v4, p1, Lcom/facebook/messaging/notify/PromotionNotification;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, LX/3pe;->b(Ljava/lang/CharSequence;)LX/3pe;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/2HB;->a(LX/3pc;)LX/2HB;

    move-result-object v0

    .line 579294
    iput-object v1, v0, LX/2HB;->d:Landroid/app/PendingIntent;

    .line 579295
    move-object v0, v0

    .line 579296
    invoke-virtual {v0, v2}, LX/2HB;->b(Landroid/app/PendingIntent;)LX/2HB;

    move-result-object v0

    invoke-virtual {v0, v6}, LX/2HB;->c(Z)LX/2HB;

    move-result-object v0

    .line 579297
    iget-object v1, p0, LX/3RG;->m:LX/2bC;

    const-string v2, "PROMOTION_PUSH"

    invoke-virtual {v1, v5, v2, v5, v5}, LX/2bC;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 579298
    iget-object v1, p0, LX/3RG;->d:LX/3RK;

    const/16 v2, 0x271f

    invoke-virtual {v0}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v0

    invoke-virtual {v1, v5, v2, v0}, LX/3RK;->a(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 579299
    iput-boolean v6, p1, Lcom/facebook/messaging/notify/PromotionNotification;->e:Z

    .line 579300
    invoke-virtual {p1}, Lcom/facebook/messaging/notify/MessagingNotification;->i()V

    .line 579301
    return-void
.end method

.method public final a(Lcom/facebook/messaging/notify/ReadThreadNotification;)V
    .locals 3

    .prologue
    .line 579520
    iget-object v0, p1, Lcom/facebook/messaging/notify/ReadThreadNotification;->a:LX/0P1;

    move-object v0, v0

    .line 579521
    invoke-virtual {v0}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 579522
    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2}, LX/3R9;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)V

    goto :goto_0

    .line 579523
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/messaging/notify/SimpleMessageNotification;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 579615
    const/16 v0, 0x2721

    invoke-direct {p0, p1, v0, v1, v1}, LX/3RG;->a(Lcom/facebook/messaging/notify/SimpleMessageNotification;ILandroid/app/PendingIntent;LX/3pc;)V

    .line 579616
    return-void
.end method

.method public final a(Lcom/facebook/messaging/notify/StaleNotification;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 579600
    iget-object v0, p0, LX/3RG;->e:LX/2zj;

    invoke-interface {v0}, LX/2zj;->g()I

    move-result v0

    .line 579601
    iget-object v1, p0, LX/3RG;->g:LX/3GK;

    invoke-interface {v1}, LX/3GK;->a()Landroid/net/Uri;

    move-result-object v1

    .line 579602
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 579603
    const-string v1, "from_notification"

    invoke-virtual {v2, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 579604
    iget-object v1, p0, LX/3RG;->b:Landroid/content/Context;

    const/4 v3, 0x0

    const/high16 v4, 0x8000000

    invoke-static {v1, v3, v2, v4}, LX/0nt;->a(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 579605
    const-string v2, "stale"

    invoke-static {p0, v2}, LX/3RG;->c(LX/3RG;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v2

    .line 579606
    new-instance v3, LX/2HB;

    iget-object v4, p0, LX/3RG;->b:Landroid/content/Context;

    invoke-direct {v3, v4}, LX/2HB;-><init>(Landroid/content/Context;)V

    iget-object v4, p1, Lcom/facebook/messaging/notify/StaleNotification;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, LX/2HB;->a(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v3

    iget-object v4, p1, Lcom/facebook/messaging/notify/StaleNotification;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, LX/2HB;->b(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v3

    iget-object v4, p1, Lcom/facebook/messaging/notify/StaleNotification;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, LX/2HB;->e(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v3

    invoke-virtual {v3, v0}, LX/2HB;->a(I)LX/2HB;

    move-result-object v0

    .line 579607
    iput-object v1, v0, LX/2HB;->d:Landroid/app/PendingIntent;

    .line 579608
    move-object v0, v0

    .line 579609
    invoke-virtual {v0, v2}, LX/2HB;->b(Landroid/app/PendingIntent;)LX/2HB;

    move-result-object v0

    invoke-virtual {v0, v6}, LX/2HB;->c(Z)LX/2HB;

    move-result-object v0

    .line 579610
    iget-object v1, p0, LX/3RG;->m:LX/2bC;

    const-string v2, "STALE_PUSH"

    invoke-virtual {v1, v5, v2, v5, v5}, LX/2bC;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 579611
    iget-object v1, p0, LX/3RG;->d:LX/3RK;

    const/16 v2, 0x2720

    invoke-virtual {v0}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v0

    invoke-virtual {v1, v5, v2, v0}, LX/3RK;->a(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 579612
    iput-boolean v6, p1, Lcom/facebook/messaging/notify/StaleNotification;->d:Z

    .line 579613
    invoke-virtual {p1}, Lcom/facebook/messaging/notify/MessagingNotification;->i()V

    .line 579614
    return-void
.end method

.method public final a(Lcom/facebook/messaging/notify/TincanMessageRequestNotification;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 579585
    iget-object v0, p0, LX/3RG;->e:LX/2zj;

    invoke-interface {v0}, LX/2zj;->g()I

    move-result v0

    .line 579586
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 579587
    sget-object v2, LX/3RH;->g:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 579588
    const-string v2, "from_notification"

    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 579589
    iget-object v2, p0, LX/3RG;->b:Landroid/content/Context;

    const/4 v3, 0x0

    const/high16 v4, 0x10000000

    invoke-static {v2, v3, v1, v4}, LX/0nt;->a(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 579590
    const-string v2, "tincan_message_request"

    invoke-static {p0, v2}, LX/3RG;->c(LX/3RG;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v2

    .line 579591
    new-instance v3, LX/2HB;

    iget-object v4, p0, LX/3RG;->b:Landroid/content/Context;

    invoke-direct {v3, v4}, LX/2HB;-><init>(Landroid/content/Context;)V

    iget-object v4, p1, Lcom/facebook/messaging/notify/TincanMessageRequestNotification;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, LX/2HB;->a(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v3

    iget-object v4, p1, Lcom/facebook/messaging/notify/TincanMessageRequestNotification;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, LX/2HB;->b(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v3

    invoke-virtual {v3, v0}, LX/2HB;->a(I)LX/2HB;

    move-result-object v0

    .line 579592
    iput-object v1, v0, LX/2HB;->d:Landroid/app/PendingIntent;

    .line 579593
    move-object v0, v0

    .line 579594
    invoke-virtual {v0, v2}, LX/2HB;->b(Landroid/app/PendingIntent;)LX/2HB;

    move-result-object v0

    invoke-virtual {v0, v5}, LX/2HB;->c(Z)LX/2HB;

    move-result-object v0

    .line 579595
    iget-object v1, p0, LX/3RG;->f:LX/3RQ;

    invoke-virtual {v1, v0}, LX/3RQ;->a(LX/2HB;)Z

    .line 579596
    iget-object v1, p0, LX/3RG;->d:LX/3RK;

    const/4 v2, 0x0

    const/16 v3, 0x272e

    invoke-virtual {v0}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v0

    invoke-virtual {v1, v2, v3, v0}, LX/3RK;->a(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 579597
    iput-boolean v5, p1, Lcom/facebook/messaging/notify/TincanMessageRequestNotification;->c:Z

    .line 579598
    invoke-virtual {p1}, Lcom/facebook/messaging/notify/MessagingNotification;->i()V

    .line 579599
    return-void
.end method

.method public final a(Lcom/facebook/orca/notify/LoggedOutNotification;)V
    .locals 3

    .prologue
    .line 579581
    iget-object v0, p1, Lcom/facebook/orca/notify/LoggedOutNotification;->a:Ljava/lang/String;

    iget-object v1, p1, Lcom/facebook/orca/notify/LoggedOutNotification;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/orca/notify/LoggedOutNotification;->c:Ljava/lang/String;

    invoke-direct {p0, p1, v0, v1, v2}, LX/3RG;->a(Lcom/facebook/messaging/notify/MessagingNotification;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 579582
    const/4 v0, 0x1

    .line 579583
    iput-boolean v0, p1, Lcom/facebook/orca/notify/LoggedOutNotification;->d:Z

    .line 579584
    return-void
.end method

.method public final a(Lcom/facebook/orca/notify/SwitchToFbAccountNotification;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 579562
    iget-object v0, p0, LX/3RG;->e:LX/2zj;

    invoke-interface {v0}, LX/2zj;->g()I

    move-result v0

    .line 579563
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, LX/3RH;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "accounts/triggersso"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 579564
    const-string v2, "from_notification"

    invoke-virtual {v1, v2, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v2

    const-string v3, "extra_account_switch_redirect_source"

    const-string v4, "local_switch_notif"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 579565
    const-string v2, "switch_to_fb_account"

    .line 579566
    invoke-static {p0, v2, v1, v7}, LX/3RG;->a(LX/3RG;Ljava/lang/String;Landroid/content/Intent;Ljava/util/HashMap;)Landroid/app/PendingIntent;

    move-result-object v1

    .line 579567
    invoke-direct {p0, v2, v7}, LX/3RG;->a(Ljava/lang/String;Ljava/util/HashMap;)Landroid/app/PendingIntent;

    move-result-object v3

    .line 579568
    new-instance v4, LX/2HB;

    iget-object v5, p0, LX/3RG;->b:Landroid/content/Context;

    invoke-direct {v4, v5}, LX/2HB;-><init>(Landroid/content/Context;)V

    iget-object v5, p1, Lcom/facebook/orca/notify/SwitchToFbAccountNotification;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, LX/2HB;->a(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v4

    iget-object v5, p1, Lcom/facebook/orca/notify/SwitchToFbAccountNotification;->b:Ljava/lang/String;

    invoke-virtual {v4, v5}, LX/2HB;->b(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v4

    iget-object v5, p1, Lcom/facebook/orca/notify/SwitchToFbAccountNotification;->b:Ljava/lang/String;

    invoke-virtual {v4, v5}, LX/2HB;->e(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v4

    .line 579569
    iput v8, v4, LX/2HB;->j:I

    .line 579570
    move-object v4, v4

    .line 579571
    new-instance v5, LX/3pe;

    invoke-direct {v5}, LX/3pe;-><init>()V

    iget-object v6, p1, Lcom/facebook/orca/notify/SwitchToFbAccountNotification;->b:Ljava/lang/String;

    invoke-virtual {v5, v6}, LX/3pe;->b(Ljava/lang/CharSequence;)LX/3pe;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/2HB;->a(LX/3pc;)LX/2HB;

    move-result-object v4

    invoke-virtual {v4, v0}, LX/2HB;->a(I)LX/2HB;

    move-result-object v0

    .line 579572
    iput-object v1, v0, LX/2HB;->d:Landroid/app/PendingIntent;

    .line 579573
    move-object v0, v0

    .line 579574
    invoke-virtual {v0, v3}, LX/2HB;->b(Landroid/app/PendingIntent;)LX/2HB;

    move-result-object v0

    invoke-virtual {v0, v8}, LX/2HB;->c(Z)LX/2HB;

    move-result-object v0

    const v3, 0x7f020cec

    iget-object v4, p1, Lcom/facebook/orca/notify/SwitchToFbAccountNotification;->c:Ljava/lang/String;

    invoke-virtual {v0, v3, v4, v1}, LX/2HB;->a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)LX/2HB;

    move-result-object v0

    .line 579575
    iget-object v1, p0, LX/3RG;->f:LX/3RQ;

    new-instance v3, LX/Dhq;

    invoke-direct {v3}, LX/Dhq;-><init>()V

    invoke-virtual {v1, v0, v3, v7, v7}, LX/3RQ;->a(LX/2HB;LX/Dhq;Lcom/facebook/messaging/push/flags/ServerMessageAlertFlags;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 579576
    iget-object v1, p0, LX/3RG;->C:LX/3S2;

    invoke-virtual {v1, v2, v7}, LX/3S2;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 579577
    iget-object v1, p0, LX/3RG;->d:LX/3RK;

    const/16 v2, 0x2730

    invoke-virtual {v0}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/3RK;->a(ILandroid/app/Notification;)V

    .line 579578
    iput-boolean v8, p1, Lcom/facebook/orca/notify/SwitchToFbAccountNotification;->d:Z

    .line 579579
    invoke-virtual {p1}, Lcom/facebook/messaging/notify/MessagingNotification;->i()V

    .line 579580
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 579527
    iget-object v0, p0, LX/3RG;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/0db;->ae:LX/0Tn;

    invoke-interface {v0, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->d(LX/0Tn;)Ljava/util/Set;

    move-result-object v0

    .line 579528
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 579529
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 579530
    :goto_0
    move-object v3, v0

    .line 579531
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_0

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 579532
    iget-object v5, p0, LX/3RG;->d:LX/3RK;

    const/16 v6, 0x2710

    invoke-virtual {v5, v0, v6}, LX/3RK;->a(Ljava/lang/String;I)V

    .line 579533
    iget-object v5, p0, LX/3RG;->q:LX/3Re;

    invoke-virtual {v5, v0}, LX/3Re;->a(Ljava/lang/String;)V

    .line 579534
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 579535
    :cond_0
    iget-object v0, p0, LX/3RG;->d:LX/3RK;

    const/16 v2, 0x2711

    invoke-virtual {v0, v2}, LX/3RK;->a(I)V

    .line 579536
    iget-object v0, p0, LX/3RG;->d:LX/3RK;

    const/16 v2, 0x2712

    invoke-virtual {v0, v2}, LX/3RK;->a(I)V

    .line 579537
    iget-object v0, p0, LX/3RG;->d:LX/3RK;

    const/16 v2, 0x2714

    invoke-virtual {v0, v2}, LX/3RK;->a(I)V

    .line 579538
    iget-object v0, p0, LX/3RG;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/0db;->af:LX/0Tn;

    invoke-interface {v0, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->d(LX/0Tn;)Ljava/util/Set;

    move-result-object v0

    .line 579539
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 579540
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 579541
    :goto_2
    move-object v2, v0

    .line 579542
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    :goto_3
    if-ge v1, v3, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 579543
    iget-object v4, p0, LX/3RG;->d:LX/3RK;

    const/16 v5, 0x271a

    invoke-virtual {v4, v0, v5}, LX/3RK;->a(Ljava/lang/String;I)V

    .line 579544
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 579545
    :cond_1
    iget-object v0, p0, LX/3RG;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    .line 579546
    sget-object v1, LX/0db;->ae:LX/0Tn;

    invoke-interface {v0, v1}, LX/0hN;->b(LX/0Tn;)LX/0hN;

    .line 579547
    invoke-interface {v0}, LX/0hN;->commit()V

    .line 579548
    iget-object v0, p0, LX/3RG;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    .line 579549
    sget-object v1, LX/0db;->af:LX/0Tn;

    invoke-interface {v0, v1}, LX/0hN;->b(LX/0Tn;)LX/0hN;

    .line 579550
    invoke-interface {v0}, LX/0hN;->commit()V

    .line 579551
    return-void

    .line 579552
    :cond_2
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 579553
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 579554
    sget-object v4, LX/0db;->ae:LX/0Tn;

    invoke-virtual {v0, v4}, LX/0To;->b(LX/0To;)Ljava/lang/String;

    move-result-object v0

    .line 579555
    invoke-static {v0}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_4

    .line 579556
    :cond_3
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto/16 :goto_0

    .line 579557
    :cond_4
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 579558
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 579559
    iget-object v4, p0, LX/3RG;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    const-string v5, ""

    invoke-interface {v4, v0, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 579560
    invoke-static {v0}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_5

    .line 579561
    :cond_5
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto/16 :goto_2
.end method

.method public final a(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 579524
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 579525
    iget-object v2, p0, LX/3RG;->d:LX/3RK;

    const/16 v3, 0x272a

    invoke-virtual {v2, v0, v3}, LX/3RK;->a(Ljava/lang/String;I)V

    goto :goto_0

    .line 579526
    :cond_0
    return-void
.end method

.method public final b(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)Landroid/app/PendingIntent;
    .locals 4
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 579462
    iget-object v0, p0, LX/3RG;->g:LX/3GK;

    invoke-interface {v0, p1}, LX/3GK;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Landroid/content/Intent;

    move-result-object v1

    .line 579463
    const/high16 v0, 0x4000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 579464
    const-string v0, "from_notification"

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 579465
    const-string v0, "trigger"

    const-string v2, "notification"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 579466
    if-eqz p2, :cond_0

    .line 579467
    invoke-virtual {v1, p2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 579468
    :cond_0
    iget-object v0, p0, LX/3RG;->k:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p0, p1}, LX/3RG;->d(LX/3RG;Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 579469
    const-string v0, "prefer_chat_if_possible"

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 579470
    :cond_1
    invoke-static {p0, v1}, LX/3RG;->a(LX/3RG;Landroid/content/Intent;)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 579518
    iget-object v0, p0, LX/3RG;->d:LX/3RK;

    const/16 v1, 0x2733

    invoke-virtual {v0, v1}, LX/3RK;->a(I)V

    .line 579519
    return-void
.end method

.method public final b(Lcom/facebook/messaging/notify/NewMessageNotification;)V
    .locals 14

    .prologue
    .line 579495
    sget-object v0, LX/3B4;->SMS_READONLY_MODE:LX/3B4;

    iget-object v1, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->f:Lcom/facebook/push/PushProperty;

    iget-object v1, v1, Lcom/facebook/push/PushProperty;->a:LX/3B4;

    invoke-virtual {v0, v1}, LX/3B4;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 579496
    :cond_0
    :goto_0
    return-void

    .line 579497
    :cond_1
    sget-object v0, LX/3B4;->SMS_DEFAULT_APP:LX/3B4;

    iget-object v1, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->f:Lcom/facebook/push/PushProperty;

    iget-object v1, v1, Lcom/facebook/push/PushProperty;->a:LX/3B4;

    invoke-virtual {v0, v1}, LX/3B4;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, LX/3B4;->SMS_READONLY_MODE:LX/3B4;

    iget-object v1, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->f:Lcom/facebook/push/PushProperty;

    iget-object v1, v1, Lcom/facebook/push/PushProperty;->a:LX/3B4;

    invoke-virtual {v0, v1}, LX/3B4;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, LX/3RG;->J:LX/3S3;

    const/4 v4, 0x0

    .line 579498
    iget-object v5, v0, LX/3S3;->a:LX/0Uh;

    const/16 v6, 0x12b

    invoke-virtual {v5, v6, v4}, LX/0Uh;->a(IZ)Z

    move-result v5

    if-nez v5, :cond_3

    .line 579499
    :goto_1
    move v0, v4

    .line 579500
    if-nez v0, :cond_0

    .line 579501
    :cond_2
    invoke-direct {p0, p1}, LX/3RG;->c(Lcom/facebook/messaging/notify/NewMessageNotification;)V

    .line 579502
    invoke-direct {p0, p1}, LX/3RG;->d(Lcom/facebook/messaging/notify/NewMessageNotification;)V

    .line 579503
    iget-object v0, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->b:Lcom/facebook/messaging/model/messages/Message;

    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->l:LX/2uW;

    sget-object v1, LX/2uW;->REGULAR:LX/2uW;

    if-ne v0, v1, :cond_0

    .line 579504
    iget-object v0, p0, LX/3RG;->F:LX/3A0;

    iget-object v1, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-wide v2, v1, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d:J

    .line 579505
    iget-object v1, v0, LX/3A0;->f:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/ECY;

    invoke-virtual {v1, v2, v3}, LX/ECY;->b(J)V

    .line 579506
    goto :goto_0

    .line 579507
    :cond_3
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    iget-wide v6, v0, LX/3S3;->h:J

    sub-long/2addr v4, v6

    const-wide/16 v6, 0x2710

    cmp-long v4, v4, v6

    if-lez v4, :cond_5

    .line 579508
    const/4 v8, 0x0

    .line 579509
    iget-object v9, v0, LX/3S3;->b:LX/23k;

    invoke-virtual {v9}, LX/23k;->b()Landroid/content/pm/PackageInfo;

    move-result-object v9

    .line 579510
    if-nez v9, :cond_6

    .line 579511
    :cond_4
    :goto_2
    move v4, v8

    .line 579512
    iput-boolean v4, v0, LX/3S3;->g:Z

    .line 579513
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    iput-wide v4, v0, LX/3S3;->h:J

    .line 579514
    :cond_5
    iget-boolean v4, v0, LX/3S3;->g:Z

    goto :goto_1

    .line 579515
    :cond_6
    iget-wide v10, v0, LX/3S3;->f:J

    iget-wide v12, v9, Landroid/content/pm/PackageInfo;->firstInstallTime:J

    cmp-long v9, v10, v12

    if-gtz v9, :cond_4

    .line 579516
    iget-object v9, v0, LX/3S3;->d:LX/23j;

    iget-object v8, v0, LX/3S3;->e:LX/0Or;

    invoke-interface {v8}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-virtual {v9, v8}, LX/151;->a(Ljava/lang/String;)LX/152;

    move-result-object v8

    .line 579517
    iget-boolean v8, v8, LX/152;->a:Z

    goto :goto_2
.end method

.method public final b(Lcom/facebook/messaging/notify/SimpleMessageNotification;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 579482
    iget-object v0, p1, Lcom/facebook/messaging/notify/SimpleMessageNotification;->b:Lcom/facebook/push/PushProperty;

    iget-object v0, v0, Lcom/facebook/push/PushProperty;->a:LX/3B4;

    move-object v0, v0

    .line 579483
    invoke-virtual {v0}, LX/3B4;->toString()Ljava/lang/String;

    move-result-object v0

    .line 579484
    iget-object v1, p1, Lcom/facebook/messaging/notify/SimpleMessageNotification;->b:Lcom/facebook/push/PushProperty;

    iget-object v1, v1, Lcom/facebook/push/PushProperty;->b:Ljava/lang/String;

    move-object v1, v1

    .line 579485
    const-string v2, "pre_reg_push"

    .line 579486
    iget-object v3, p0, LX/3RG;->b:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    .line 579487
    iget-object v4, p0, LX/3RG;->m:LX/2bC;

    invoke-virtual {v4, v6, v0, v1, v2}, LX/2bC;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 579488
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 579489
    const-string v5, "push_source"

    invoke-virtual {v4, v5, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 579490
    const-string v0, "push_id"

    invoke-virtual {v4, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 579491
    const-string v0, "push_type"

    invoke-virtual {v4, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 579492
    iget-object v0, p0, LX/3RG;->G:Landroid/content/pm/PackageManager;

    invoke-virtual {v0, v3}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-static {p0, v2, v0, v4}, LX/3RG;->a(LX/3RG;Ljava/lang/String;Landroid/content/Intent;Ljava/util/HashMap;)Landroid/app/PendingIntent;

    move-result-object v0

    .line 579493
    const/16 v1, 0x2723

    invoke-direct {p0, p1, v1, v0, v6}, LX/3RG;->a(Lcom/facebook/messaging/notify/SimpleMessageNotification;ILandroid/app/PendingIntent;LX/3pc;)V

    .line 579494
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 579479
    iget-object v0, p0, LX/3RG;->d:LX/3RK;

    const/16 v1, 0x2713

    invoke-virtual {v0, p1, v1}, LX/3RK;->a(Ljava/lang/String;I)V

    .line 579480
    iget-object v0, p0, LX/3RG;->d:LX/3RK;

    const/16 v1, 0x271a

    invoke-virtual {v0, p1, v1}, LX/3RK;->a(Ljava/lang/String;I)V

    .line 579481
    return-void
.end method

.method public final c(Lcom/facebook/messaging/notify/SimpleMessageNotification;)V
    .locals 1

    .prologue
    .line 579477
    const/16 v0, 0x2735

    invoke-direct {p0, p1, v0}, LX/3RG;->a(Lcom/facebook/messaging/notify/SimpleMessageNotification;I)V

    .line 579478
    return-void
.end method

.method public final d(Lcom/facebook/messaging/notify/SimpleMessageNotification;)V
    .locals 1

    .prologue
    .line 579475
    const/16 v0, 0x2736

    invoke-direct {p0, p1, v0}, LX/3RG;->a(Lcom/facebook/messaging/notify/SimpleMessageNotification;I)V

    .line 579476
    return-void
.end method

.method public final e(Lcom/facebook/messaging/notify/SimpleMessageNotification;)V
    .locals 1

    .prologue
    .line 579473
    const/16 v0, 0x2737

    invoke-direct {p0, p1, v0}, LX/3RG;->a(Lcom/facebook/messaging/notify/SimpleMessageNotification;I)V

    .line 579474
    return-void
.end method

.method public final f(Lcom/facebook/messaging/notify/SimpleMessageNotification;)V
    .locals 1

    .prologue
    .line 579471
    const/16 v0, 0x2738

    invoke-direct {p0, p1, v0}, LX/3RG;->a(Lcom/facebook/messaging/notify/SimpleMessageNotification;I)V

    .line 579472
    return-void
.end method
