.class public LX/4oR;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field public a:LX/4oS;

.field private final b:LX/4oQ;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 809626
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 809627
    new-instance v0, LX/4oQ;

    invoke-direct {v0, p0}, LX/4oQ;-><init>(LX/4oR;)V

    iput-object v0, p0, LX/4oR;->b:LX/4oQ;

    .line 809628
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, LX/4oR;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 809629
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 809630
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 809631
    new-instance v0, LX/4oQ;

    invoke-direct {v0, p0}, LX/4oQ;-><init>(LX/4oR;)V

    iput-object v0, p0, LX/4oR;->b:LX/4oQ;

    .line 809632
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/4oR;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 809633
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 809634
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 809635
    new-instance v0, LX/4oQ;

    invoke-direct {v0, p0}, LX/4oQ;-><init>(LX/4oR;)V

    iput-object v0, p0, LX/4oR;->b:LX/4oQ;

    .line 809636
    invoke-direct {p0, p1, p2, p3}, LX/4oR;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 809637
    return-void
.end method

.method public static synthetic a(LX/4oR;Landroid/graphics/Canvas;)V
    .locals 0

    .prologue
    .line 809638
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 809639
    invoke-static {p1, p2, p3}, LX/4oN;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)LX/4oN;

    move-result-object v0

    .line 809640
    new-instance v1, LX/4oS;

    invoke-direct {v1, p0, v0}, LX/4oS;-><init>(Landroid/view/View;LX/4oN;)V

    iput-object v1, p0, LX/4oR;->a:LX/4oS;

    .line 809641
    return-void
.end method


# virtual methods
.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 8

    .prologue
    .line 809642
    iget-object v0, p0, LX/4oR;->a:LX/4oS;

    iget-object v1, p0, LX/4oR;->b:LX/4oQ;

    .line 809643
    iget-object v2, v0, LX/4oS;->d:LX/4oL;

    .line 809644
    invoke-static {v2}, LX/4oL;->d(LX/4oL;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 809645
    invoke-virtual {v1, p1}, LX/4oQ;->a(Landroid/graphics/Canvas;)V

    .line 809646
    :goto_0
    return-void

    .line 809647
    :cond_0
    invoke-static {v2}, LX/4oL;->e(LX/4oL;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 809648
    invoke-static {v2, p1, v1}, LX/4oL;->b(LX/4oL;Landroid/graphics/Canvas;LX/4oQ;)V

    goto :goto_0

    .line 809649
    :cond_1
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 809650
    iget-object v3, v2, LX/4oL;->k:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_5

    .line 809651
    iget-object v3, v2, LX/4oL;->k:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    iget-object v6, v2, LX/4oL;->c:LX/4oP;

    iget v6, v6, LX/4oP;->a:I

    if-ne v3, v6, :cond_3

    move v3, v4

    :goto_1
    invoke-static {v3}, LX/0PB;->checkArgument(Z)V

    .line 809652
    iget-object v3, v2, LX/4oL;->k:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    iget-object v6, v2, LX/4oL;->c:LX/4oP;

    iget v6, v6, LX/4oP;->b:I

    if-ne v3, v6, :cond_4

    :goto_2
    invoke-static {v4}, LX/0PB;->checkArgument(Z)V

    .line 809653
    iget-object v3, v2, LX/4oL;->k:Landroid/graphics/Bitmap;

    .line 809654
    :goto_3
    move-object v3, v3

    .line 809655
    if-nez v3, :cond_2

    .line 809656
    invoke-static {v2, p1, v1}, LX/4oL;->b(LX/4oL;Landroid/graphics/Canvas;LX/4oQ;)V

    goto :goto_0

    .line 809657
    :cond_2
    iget-object v4, v2, LX/4oL;->k:Landroid/graphics/Bitmap;

    if-ne v3, v4, :cond_6

    iget-object v4, v2, LX/4oL;->n:Landroid/graphics/Canvas;

    if-eqz v4, :cond_6

    .line 809658
    iget-object v4, v2, LX/4oL;->n:Landroid/graphics/Canvas;

    .line 809659
    :goto_4
    move-object v4, v4

    .line 809660
    const/4 v5, 0x0

    sget-object v6, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 809661
    invoke-virtual {v1, v4}, LX/4oQ;->a(Landroid/graphics/Canvas;)V

    .line 809662
    invoke-virtual {v2, p1, v3}, LX/4oL;->a(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;)V

    goto :goto_0

    :cond_3
    move v3, v5

    .line 809663
    goto :goto_1

    :cond_4
    move v4, v5

    .line 809664
    goto :goto_2

    .line 809665
    :cond_5
    :try_start_0
    iget-object v3, v2, LX/4oL;->c:LX/4oP;

    iget v3, v3, LX/4oP;->a:I

    iget-object v6, v2, LX/4oL;->c:LX/4oP;

    iget v6, v6, LX/4oP;->b:I

    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v6, v7}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    move-result-object v3

    .line 809666
    :goto_5
    move-object v3, v3

    .line 809667
    iput-object v3, v2, LX/4oL;->k:Landroid/graphics/Bitmap;

    .line 809668
    iget-object v3, v2, LX/4oL;->k:Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3

    .line 809669
    :catch_0
    move-exception v3

    .line 809670
    const-string v6, "RoundedBitmapHelper failed to create working bitmap (width = %d, height = %d)"

    .line 809671
    sget-object v7, LX/4oL;->a:Ljava/lang/Class;

    const/4 p0, 0x2

    new-array p0, p0, [Ljava/lang/Object;

    iget-object v0, v2, LX/4oL;->c:LX/4oP;

    iget v0, v0, LX/4oP;->a:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, p0, v5

    iget-object v5, v2, LX/4oL;->c:LX/4oP;

    iget v5, v5, LX/4oP;->b:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, p0, v4

    invoke-static {v7, v3, v6, p0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 809672
    const/4 v3, 0x0

    goto :goto_3

    .line 809673
    :catch_1
    :try_start_2
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 809674
    iget-object v3, v2, LX/4oL;->c:LX/4oP;

    iget v3, v3, LX/4oP;->a:I

    iget-object v6, v2, LX/4oL;->c:LX/4oP;

    iget v6, v6, LX/4oP;->b:I

    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v6, v7}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    goto :goto_5
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_0

    .line 809675
    :cond_6
    new-instance v4, Landroid/graphics/Canvas;

    invoke-direct {v4, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v4, v2, LX/4oL;->n:Landroid/graphics/Canvas;

    .line 809676
    iget-object v4, v2, LX/4oL;->n:Landroid/graphics/Canvas;

    goto :goto_4
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x7b67de09

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 809677
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onDetachedFromWindow()V

    .line 809678
    iget-object v1, p0, LX/4oR;->a:LX/4oS;

    invoke-virtual {v1}, LX/4oS;->b()V

    .line 809679
    const/16 v1, 0x2d

    const v2, 0xc9f8876

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSizeChanged(IIII)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x64acd02a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 809680
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/widget/CustomFrameLayout;->onSizeChanged(IIII)V

    .line 809681
    iget-object v1, p0, LX/4oR;->a:LX/4oS;

    invoke-virtual {v1}, LX/4oS;->a()V

    .line 809682
    const/16 v1, 0x2d

    const v2, 0x3d75ec38

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1

    .prologue
    .line 809683
    iget-object v0, p0, LX/4oR;->a:LX/4oS;

    .line 809684
    iget-object p0, v0, LX/4oS;->e:Landroid/graphics/ColorFilter;

    if-eq p1, p0, :cond_0

    .line 809685
    iput-object p1, v0, LX/4oS;->e:Landroid/graphics/ColorFilter;

    .line 809686
    iget-object p0, v0, LX/4oS;->d:LX/4oL;

    if-eqz p0, :cond_0

    .line 809687
    iget-object p0, v0, LX/4oS;->d:LX/4oL;

    invoke-virtual {p0, p1}, LX/4oL;->a(Landroid/graphics/ColorFilter;)V

    .line 809688
    iget-object p0, v0, LX/4oS;->a:Landroid/view/View;

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 809689
    :cond_0
    return-void
.end method

.method public setOverlayAlpha(I)V
    .locals 1

    .prologue
    .line 809690
    iget-object v0, p0, LX/4oR;->a:LX/4oS;

    .line 809691
    iget p0, v0, LX/4oS;->g:I

    if-eq p1, p0, :cond_0

    .line 809692
    iput p1, v0, LX/4oS;->g:I

    .line 809693
    iget-object p0, v0, LX/4oS;->d:LX/4oL;

    if-eqz p0, :cond_0

    .line 809694
    iget-object p0, v0, LX/4oS;->d:LX/4oL;

    invoke-virtual {p0, p1}, LX/4oL;->b(I)V

    .line 809695
    iget-object p0, v0, LX/4oS;->a:Landroid/view/View;

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 809696
    :cond_0
    return-void
.end method

.method public setOverlayColor(I)V
    .locals 1

    .prologue
    .line 809697
    iget-object v0, p0, LX/4oR;->a:LX/4oS;

    .line 809698
    iget p0, v0, LX/4oS;->f:I

    if-eq p1, p0, :cond_0

    .line 809699
    iput p1, v0, LX/4oS;->f:I

    .line 809700
    iget-object p0, v0, LX/4oS;->d:LX/4oL;

    if-eqz p0, :cond_0

    .line 809701
    iget-object p0, v0, LX/4oS;->d:LX/4oL;

    invoke-virtual {p0, p1}, LX/4oL;->a(I)V

    .line 809702
    iget-object p0, v0, LX/4oS;->a:Landroid/view/View;

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 809703
    :cond_0
    return-void
.end method

.method public setRoundBorderColor(I)V
    .locals 1

    .prologue
    .line 809704
    iget-object v0, p0, LX/4oR;->a:LX/4oS;

    .line 809705
    iget-object p0, v0, LX/4oS;->b:LX/4oN;

    iget p0, p0, LX/4oN;->i:I

    if-eq p1, p0, :cond_0

    .line 809706
    iput p1, v0, LX/4oS;->h:I

    .line 809707
    iget-object p0, v0, LX/4oS;->d:LX/4oL;

    if-eqz p0, :cond_0

    .line 809708
    iget-object p0, v0, LX/4oS;->d:LX/4oL;

    invoke-virtual {p0, p1}, LX/4oL;->c(I)V

    .line 809709
    iget-object p0, v0, LX/4oS;->a:Landroid/view/View;

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 809710
    :cond_0
    return-void
.end method
