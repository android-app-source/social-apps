.class public final LX/409;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2fJ;


# instance fields
.field public final synthetic a:Lcom/facebook/analytics2/logger/LollipopUploadService;

.field private final b:Landroid/app/job/JobParameters;


# direct methods
.method public constructor <init>(Lcom/facebook/analytics2/logger/LollipopUploadService;Landroid/app/job/JobParameters;)V
    .locals 0

    .prologue
    .line 662959
    iput-object p1, p0, LX/409;->a:Lcom/facebook/analytics2/logger/LollipopUploadService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 662960
    iput-object p2, p0, LX/409;->b:Landroid/app/job/JobParameters;

    .line 662961
    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 2

    .prologue
    .line 662962
    iget-object v0, p0, LX/409;->b:Landroid/app/job/JobParameters;

    invoke-virtual {v0}, Landroid/app/job/JobParameters;->getJobId()I

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 662963
    iget-object v0, p0, LX/409;->a:Lcom/facebook/analytics2/logger/LollipopUploadService;

    iget-object v1, p0, LX/409;->b:Landroid/app/job/JobParameters;

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics2/logger/LollipopUploadService;->jobFinished(Landroid/app/job/JobParameters;Z)V

    .line 662964
    return-void
.end method
