.class public LX/4Rb;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 709455
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 709433
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v2, :cond_6

    .line 709434
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 709435
    :goto_0
    return v0

    .line 709436
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 709437
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_5

    .line 709438
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 709439
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 709440
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_1

    if-eqz v4, :cond_1

    .line 709441
    const-string v5, "id"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 709442
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 709443
    :cond_2
    const-string v5, "issue_summary"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 709444
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 709445
    :cond_3
    const-string v5, "politician"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 709446
    invoke-static {p0, p1}, LX/2bc;->a(LX/15w;LX/186;)I

    move-result v1

    goto :goto_1

    .line 709447
    :cond_4
    const-string v5, "url"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 709448
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 709449
    :cond_5
    const/4 v4, 0x5

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 709450
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, LX/186;->b(II)V

    .line 709451
    const/4 v3, 0x2

    invoke-virtual {p1, v3, v2}, LX/186;->b(II)V

    .line 709452
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v1}, LX/186;->b(II)V

    .line 709453
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 709454
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto :goto_0

    :cond_6
    move v1, v0

    move v2, v0

    move v3, v0

    goto :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 709414
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 709415
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 709416
    if-eqz v0, :cond_0

    .line 709417
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 709418
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 709419
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 709420
    if-eqz v0, :cond_1

    .line 709421
    const-string v1, "issue_summary"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 709422
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 709423
    :cond_1
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 709424
    if-eqz v0, :cond_2

    .line 709425
    const-string v1, "politician"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 709426
    invoke-static {p0, v0, p2, p3}, LX/2bc;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 709427
    :cond_2
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 709428
    if-eqz v0, :cond_3

    .line 709429
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 709430
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 709431
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 709432
    return-void
.end method
