.class public LX/3aa;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1TE;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/1Cz;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile b:LX/3aa;


# direct methods
.method public static constructor <clinit>()V
    .locals 15

    .prologue
    .line 605803
    sget-object v0, LX/3ab;->a:LX/1Cz;

    sget-object v1, LX/3ab;->c:LX/1Cz;

    sget-object v2, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitlePartDefinition;->a:LX/1Cz;

    sget-object v3, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleActionPartDefinition;->a:LX/1Cz;

    sget-object v4, Lcom/facebook/components/feed/ComponentPartDefinition;->a:LX/1Cz;

    sget-object v5, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityPivotRowPartDefinition;->a:LX/1Cz;

    sget-object v6, Lcom/facebook/search/results/rows/sections/common/SearchResultsEmptyPartDefinition;->a:LX/1Cz;

    sget-object v7, Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyHeaderPartDefinition;->a:LX/1Cz;

    sget-object v8, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;->a:LX/1Cz;

    sget-object v9, Lcom/facebook/search/results/rows/sections/stories/SearchResultsStorySnippetPartDefinition;->a:LX/1Cz;

    sget-object v10, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsDividerPartDefinition;->a:LX/1Cz;

    sget-object v11, Lcom/facebook/search/results/rows/sections/common/SearchResultsDividerFigPartDefinition;->a:LX/1Cz;

    const/16 v12, 0x57

    new-array v12, v12, [LX/1Cz;

    const/4 v13, 0x0

    sget-object v14, Lcom/facebook/search/results/rows/sections/common/SearchResultsZeroPadDividerPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/4 v13, 0x1

    sget-object v14, Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/4 v13, 0x2

    sget-object v14, Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationWithPostCountPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/4 v13, 0x3

    sget-object v14, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePostsPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/4 v13, 0x4

    sget-object v14, Lcom/facebook/search/results/rows/sections/collection/SearchResultsArticlesItemPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/4 v13, 0x5

    sget-object v14, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/4 v13, 0x6

    sget-object v14, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageRowPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/4 v13, 0x7

    sget-object v14, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowFigPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x8

    sget-object v14, LX/3ap;->c:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x9

    sget-object v14, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityLargeRowPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0xa

    sget-object v14, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsPageLargeRowPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0xb

    sget-object v14, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0xc

    sget-object v14, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileLargeRowPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0xd

    sget-object v14, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowFigPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0xe

    sget-object v14, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoChannelLargeRowPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0xf

    sget-object v14, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x10

    sget-object v14, Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x11

    sget-object v14, Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellCorrectionEscapePartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x12

    sget-object v14, Lcom/facebook/search/results/rows/sections/noresults/SearchResultsEmptyUnitPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x13

    sget-object v14, Lcom/facebook/search/results/rows/sections/noresults/SearchResultsNoResultsContentPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x14

    sget-object v14, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x15

    sget-object v14, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoWithAutoplayPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x16

    sget-object v14, Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideoPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x17

    sget-object v14, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlacesMapPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x18

    sget-object v14, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlacePartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x19

    sget-object v14, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlaceModuleMapPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x1a

    sget-object v14, Lcom/facebook/components/feed/ComponentPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x1b

    sget-object v14, Lcom/facebook/search/results/rows/sections/unsupported/SearchResultsUnsupportedResultPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x1c

    sget-object v14, Lcom/facebook/feed/rows/sections/common/EmptyPartDefinition;->d:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x1d

    sget-object v14, Lcom/facebook/feed/rows/sections/common/unknown/UnknownFeedUnitPartDefinition;->d:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x1e

    sget-object v14, Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsContentPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x1f

    sget-object v14, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextHeaderPhotoPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x20

    sget-object v14, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextHeaderVideoPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x21

    sget-object v14, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextTitlePartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x22

    sget-object v14, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextSummaryPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x23

    sget-object v14, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextAttributionPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x24

    sget-object v14, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextTitlePartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x25

    sget-object v14, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextPhotoPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x26

    sget-object v14, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextVideoPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x27

    sget-object v14, Lcom/facebook/search/results/rows/sections/composer/SearchComposerSinglePartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x28

    sget-object v14, Lcom/facebook/search/results/rows/sections/pulse/OldPulseRelatedLinksItemPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x29

    sget-object v14, Lcom/facebook/components/feed/ComponentPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x2a

    sget-object v14, Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x2b

    sget-object v14, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x2c

    sget-object v14, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderTextPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x2d

    sget-object v14, Lcom/facebook/search/results/rows/sections/pulse/PulseContextSummaryPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x2e

    sget-object v14, Lcom/facebook/search/results/rows/sections/pulse/PulseCommonPhraseItemPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x2f

    sget-object v14, Lcom/facebook/search/results/rows/sections/pulse/PulseCommonQuoteContentPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x30

    sget-object v14, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalItemPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x31

    sget-object v14, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalVerticalItemPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x32

    sget-object v14, Lcom/facebook/search/results/rows/sections/pulsetopic/PulseTopicRelatedTopicsPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x33

    sget-object v14, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityPhotoPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x34

    sget-object v14, Lcom/facebook/search/results/rows/sections/noresults/SearchResultsEmptyEntityModuleBodyPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x35

    sget-object v14, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosHScrollPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x36

    sget-object v14, Lcom/facebook/search/results/rows/sections/videos/SearchResultsLiveVideoPagePartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x37

    sget-object v14, Lcom/facebook/search/results/rows/sections/videos/SearchResultsLiveVideoPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x38

    sget-object v14, Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsInlineVideoPagePartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x39

    sget-object v14, Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsInlineVideoPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x3a

    sget-object v14, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x3b

    sget-object v14, Lcom/facebook/search/results/rows/sections/entities/SearchResultsNavigationalExternalUrlPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x3c

    sget-object v14, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x3d

    sget-object v14, Lcom/facebook/search/results/rows/sections/collection/SearchResultsAwarenessNodePartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x3e

    sget-object v14, Lcom/facebook/search/results/rows/sections/header/SearchResultsWayfinderPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x3f

    sget-object v14, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserItemPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x40

    sget-object v14, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserResultItemPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x41

    sget-object v14, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x42

    sget-object v14, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x43

    sget-object v14, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductCarouselSeeMorePartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x44

    sget-object v14, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGridRowPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x45

    sget-object v14, Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceTitleTogglePartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x46

    sget-object v14, Lcom/facebook/search/results/rows/sections/noresults/SearchResultsCommerceNoResultsPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x47

    sget-object v14, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsTopTabGridRowPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x48

    sget-object v14, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x49

    sget-object v14, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x4a

    sget-object v14, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductCarouselSeeMorePartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x4b

    sget-object v14, Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceModuleTitleTogglePartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x4c

    sget-object v14, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsTopTabGridRowPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x4d

    sget-object v14, Lcom/facebook/search/results/rows/sections/pulse/PulseContextSummaryConvertedPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x4e

    sget-object v14, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderTextConvertedPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x4f

    sget-object v14, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoConvertedPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x50

    sget-object v14, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiTitleConvertedPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x51

    sget-object v14, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiAboutTextConvertedPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x52

    sget-object v14, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiReadMoreConvertedPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x53

    sget-object v14, Lcom/facebook/search/results/rows/sections/derp/SmallPhotoShareAttachmentWithMarginPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x54

    sget-object v14, LX/3bQ;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x55

    sget-object v14, Lcom/facebook/components/feed/ComponentPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    const/16 v13, 0x56

    sget-object v14, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherForecastItemPartDefinition;->a:LX/1Cz;

    aput-object v14, v12, v13

    invoke-static/range {v0 .. v12}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/3aa;->a:LX/0Px;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 605801
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 605802
    return-void
.end method

.method public static a(LX/0QB;)LX/3aa;
    .locals 3

    .prologue
    .line 605785
    sget-object v0, LX/3aa;->b:LX/3aa;

    if-nez v0, :cond_1

    .line 605786
    const-class v1, LX/3aa;

    monitor-enter v1

    .line 605787
    :try_start_0
    sget-object v0, LX/3aa;->b:LX/3aa;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 605788
    if-eqz v2, :cond_0

    .line 605789
    :try_start_1
    new-instance v0, LX/3aa;

    invoke-direct {v0}, LX/3aa;-><init>()V

    .line 605790
    move-object v0, v0

    .line 605791
    sput-object v0, LX/3aa;->b:LX/3aa;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 605792
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 605793
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 605794
    :cond_1
    sget-object v0, LX/3aa;->b:LX/3aa;

    return-object v0

    .line 605795
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 605796
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1KB;)V
    .locals 3

    .prologue
    .line 605797
    sget-object v0, LX/3aa;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    sget-object v0, LX/3aa;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Cz;

    .line 605798
    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 605799
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 605800
    :cond_0
    return-void
.end method
