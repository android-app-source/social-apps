.class public final LX/4xL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation


# instance fields
.field public value:I


# direct methods
.method public constructor <init>(I)V
    .locals 0

    .prologue
    .line 820927
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 820928
    iput p1, p0, LX/4xL;->value:I

    .line 820929
    return-void
.end method


# virtual methods
.method public final b(I)I
    .locals 1

    .prologue
    .line 820930
    iget v0, p0, LX/4xL;->value:I

    add-int/2addr v0, p1

    iput v0, p0, LX/4xL;->value:I

    return v0
.end method

.method public final d(I)I
    .locals 1

    .prologue
    .line 820931
    iget v0, p0, LX/4xL;->value:I

    .line 820932
    iput p1, p0, LX/4xL;->value:I

    .line 820933
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 820934
    instance-of v0, p1, LX/4xL;

    if-eqz v0, :cond_0

    check-cast p1, LX/4xL;

    iget v0, p1, LX/4xL;->value:I

    iget v1, p0, LX/4xL;->value:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 820935
    iget v0, p0, LX/4xL;->value:I

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 820936
    iget v0, p0, LX/4xL;->value:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
