.class public LX/3rl;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/3re;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 643315
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 643316
    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    .line 643317
    new-instance v0, LX/3rj;

    invoke-direct {v0}, LX/3rj;-><init>()V

    sput-object v0, LX/3rl;->a:LX/3re;

    .line 643318
    :goto_0
    return-void

    .line 643319
    :cond_0
    const/16 v1, 0xb

    if-lt v0, v1, :cond_1

    .line 643320
    new-instance v0, LX/3rg;

    invoke-direct {v0}, LX/3rg;-><init>()V

    sput-object v0, LX/3rl;->a:LX/3re;

    goto :goto_0

    .line 643321
    :cond_1
    new-instance v0, LX/3rf;

    invoke-direct {v0}, LX/3rf;-><init>()V

    sput-object v0, LX/3rl;->a:LX/3re;

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 643322
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 643323
    return-void
.end method

.method public static a(Landroid/view/MenuItem;LX/3rk;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 643324
    instance-of v0, p0, LX/3qv;

    if-eqz v0, :cond_0

    .line 643325
    check-cast p0, LX/3qv;

    invoke-interface {p0, p1}, LX/3qv;->a(LX/3rk;)LX/3qv;

    move-result-object v0

    .line 643326
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/3rl;->a:LX/3re;

    invoke-interface {v0, p0, p1}, LX/3re;->a(Landroid/view/MenuItem;LX/3rk;)Landroid/view/MenuItem;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/view/MenuItem;Landroid/view/View;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 643327
    instance-of v0, p0, LX/3qv;

    if-eqz v0, :cond_0

    .line 643328
    check-cast p0, LX/3qv;

    invoke-interface {p0, p1}, LX/3qv;->setActionView(Landroid/view/View;)Landroid/view/MenuItem;

    move-result-object v0

    .line 643329
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/3rl;->a:LX/3re;

    invoke-interface {v0, p0, p1}, LX/3re;->a(Landroid/view/MenuItem;Landroid/view/View;)Landroid/view/MenuItem;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/view/MenuItem;)Landroid/view/View;
    .locals 1

    .prologue
    .line 643330
    instance-of v0, p0, LX/3qv;

    if-eqz v0, :cond_0

    .line 643331
    check-cast p0, LX/3qv;

    invoke-interface {p0}, LX/3qv;->getActionView()Landroid/view/View;

    move-result-object v0

    .line 643332
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/3rl;->a:LX/3re;

    invoke-interface {v0, p0}, LX/3re;->a(Landroid/view/MenuItem;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/view/MenuItem;I)Z
    .locals 1

    .prologue
    .line 643333
    instance-of v0, p0, LX/3qv;

    if-eqz v0, :cond_0

    .line 643334
    check-cast p0, LX/3qv;

    invoke-interface {p0, p1}, LX/3qv;->setShowAsAction(I)V

    .line 643335
    const/4 v0, 0x1

    .line 643336
    :goto_0
    return v0

    :cond_0
    sget-object v0, LX/3rl;->a:LX/3re;

    invoke-interface {v0, p0, p1}, LX/3re;->a(Landroid/view/MenuItem;I)Z

    move-result v0

    goto :goto_0
.end method

.method public static b(Landroid/view/MenuItem;I)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 643337
    instance-of v0, p0, LX/3qv;

    if-eqz v0, :cond_0

    .line 643338
    check-cast p0, LX/3qv;

    invoke-interface {p0, p1}, LX/3qv;->setActionView(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 643339
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/3rl;->a:LX/3re;

    invoke-interface {v0, p0, p1}, LX/3re;->b(Landroid/view/MenuItem;I)Landroid/view/MenuItem;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 643340
    instance-of v0, p0, LX/3qv;

    if-eqz v0, :cond_0

    .line 643341
    check-cast p0, LX/3qv;

    invoke-interface {p0}, LX/3qv;->expandActionView()Z

    move-result v0

    .line 643342
    :goto_0
    return v0

    :cond_0
    sget-object v0, LX/3rl;->a:LX/3re;

    invoke-interface {v0, p0}, LX/3re;->b(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public static c(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 643343
    instance-of v0, p0, LX/3qv;

    if-eqz v0, :cond_0

    .line 643344
    check-cast p0, LX/3qv;

    invoke-interface {p0}, LX/3qv;->isActionViewExpanded()Z

    move-result v0

    .line 643345
    :goto_0
    return v0

    :cond_0
    sget-object v0, LX/3rl;->a:LX/3re;

    invoke-interface {v0, p0}, LX/3re;->c(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method
