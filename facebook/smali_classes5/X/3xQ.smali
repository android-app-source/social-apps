.class public final LX/3xQ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:[I

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 657586
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 657587
    return-void
.end method

.method private static f(LX/3xQ;I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 657476
    iget-object v0, p0, LX/3xQ;->a:[I

    if-nez v0, :cond_1

    .line 657477
    const/16 v0, 0xa

    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, LX/3xQ;->a:[I

    .line 657478
    iget-object v0, p0, LX/3xQ;->a:[I

    invoke-static {v0, v3}, Ljava/util/Arrays;->fill([II)V

    .line 657479
    :cond_0
    :goto_0
    return-void

    .line 657480
    :cond_1
    iget-object v0, p0, LX/3xQ;->a:[I

    array-length v0, v0

    if-lt p1, v0, :cond_0

    .line 657481
    iget-object v0, p0, LX/3xQ;->a:[I

    .line 657482
    iget-object v1, p0, LX/3xQ;->a:[I

    array-length v1, v1

    .line 657483
    :goto_1
    if-gt v1, p1, :cond_2

    .line 657484
    mul-int/lit8 v1, v1, 0x2

    goto :goto_1

    .line 657485
    :cond_2
    move v1, v1

    .line 657486
    new-array v1, v1, [I

    iput-object v1, p0, LX/3xQ;->a:[I

    .line 657487
    iget-object v1, p0, LX/3xQ;->a:[I

    array-length v2, v0

    invoke-static {v0, v4, v1, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 657488
    iget-object v1, p0, LX/3xQ;->a:[I

    array-length v0, v0

    iget-object v2, p0, LX/3xQ;->a:[I

    array-length v2, v2

    invoke-static {v1, v0, v2, v3}, Ljava/util/Arrays;->fill([IIII)V

    goto :goto_0
.end method


# virtual methods
.method public final a(I)I
    .locals 2

    .prologue
    .line 657576
    iget-object v0, p0, LX/3xQ;->b:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 657577
    iget-object v0, p0, LX/3xQ;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    .line 657578
    iget-object v0, p0, LX/3xQ;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;

    .line 657579
    iget v0, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;->a:I

    if-lt v0, p1, :cond_0

    .line 657580
    iget-object v0, p0, LX/3xQ;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 657581
    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 657582
    :cond_1
    invoke-virtual {p0, p1}, LX/3xQ;->b(I)I

    move-result v0

    return v0
.end method

.method public final a(IIIZ)Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 657566
    iget-object v0, p0, LX/3xQ;->b:Ljava/util/List;

    if-nez v0, :cond_1

    move-object v0, v1

    .line 657567
    :cond_0
    :goto_0
    return-object v0

    .line 657568
    :cond_1
    iget-object v0, p0, LX/3xQ;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    .line 657569
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v3, :cond_4

    .line 657570
    iget-object v0, p0, LX/3xQ;->b:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;

    .line 657571
    iget v4, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;->a:I

    if-lt v4, p2, :cond_2

    move-object v0, v1

    .line 657572
    goto :goto_0

    .line 657573
    :cond_2
    iget v4, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;->a:I

    if-lt v4, p1, :cond_3

    if-eqz p3, :cond_0

    iget v4, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;->b:I

    if-eq v4, p3, :cond_0

    if-eqz p4, :cond_3

    iget-boolean v4, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;->d:Z

    if-nez v4, :cond_0

    .line 657574
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_4
    move-object v0, v1

    .line 657575
    goto :goto_0
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 657562
    iget-object v0, p0, LX/3xQ;->a:[I

    if-eqz v0, :cond_0

    .line 657563
    iget-object v0, p0, LX/3xQ;->a:[I

    const/4 v1, -0x1

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    .line 657564
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/3xQ;->b:Ljava/util/List;

    .line 657565
    return-void
.end method

.method public final a(II)V
    .locals 4

    .prologue
    .line 657547
    iget-object v0, p0, LX/3xQ;->a:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3xQ;->a:[I

    array-length v0, v0

    if-lt p1, v0, :cond_1

    .line 657548
    :cond_0
    :goto_0
    return-void

    .line 657549
    :cond_1
    add-int v0, p1, p2

    invoke-static {p0, v0}, LX/3xQ;->f(LX/3xQ;I)V

    .line 657550
    iget-object v0, p0, LX/3xQ;->a:[I

    add-int v1, p1, p2

    iget-object v2, p0, LX/3xQ;->a:[I

    iget-object v3, p0, LX/3xQ;->a:[I

    array-length v3, v3

    sub-int/2addr v3, p1

    sub-int/2addr v3, p2

    invoke-static {v0, v1, v2, p1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 657551
    iget-object v0, p0, LX/3xQ;->a:[I

    iget-object v1, p0, LX/3xQ;->a:[I

    array-length v1, v1

    sub-int/2addr v1, p2

    iget-object v2, p0, LX/3xQ;->a:[I

    array-length v2, v2

    const/4 v3, -0x1

    invoke-static {v0, v1, v2, v3}, Ljava/util/Arrays;->fill([IIII)V

    .line 657552
    iget-object v0, p0, LX/3xQ;->b:Ljava/util/List;

    if-nez v0, :cond_3

    .line 657553
    :cond_2
    goto :goto_0

    .line 657554
    :cond_3
    add-int v2, p1, p2

    .line 657555
    iget-object v0, p0, LX/3xQ;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_1
    if-ltz v1, :cond_2

    .line 657556
    iget-object v0, p0, LX/3xQ;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;

    .line 657557
    iget v3, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;->a:I

    if-lt v3, p1, :cond_4

    .line 657558
    iget v3, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;->a:I

    if-ge v3, v2, :cond_5

    .line 657559
    iget-object v0, p0, LX/3xQ;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 657560
    :cond_4
    :goto_2
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_1

    .line 657561
    :cond_5
    iget v3, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;->a:I

    sub-int/2addr v3, p2

    iput v3, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;->a:I

    goto :goto_2
.end method

.method public final a(ILX/3xS;)V
    .locals 2

    .prologue
    .line 657583
    invoke-static {p0, p1}, LX/3xQ;->f(LX/3xQ;I)V

    .line 657584
    iget-object v0, p0, LX/3xQ;->a:[I

    iget v1, p2, LX/3xS;->d:I

    aput v1, v0, p1

    .line 657585
    return-void
.end method

.method public final a(Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;)V
    .locals 5

    .prologue
    .line 657535
    iget-object v0, p0, LX/3xQ;->b:Ljava/util/List;

    if-nez v0, :cond_0

    .line 657536
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/3xQ;->b:Ljava/util/List;

    .line 657537
    :cond_0
    iget-object v0, p0, LX/3xQ;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 657538
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_3

    .line 657539
    iget-object v0, p0, LX/3xQ;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;

    .line 657540
    iget v3, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;->a:I

    iget v4, p1, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;->a:I

    if-ne v3, v4, :cond_1

    .line 657541
    iget-object v3, p0, LX/3xQ;->b:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 657542
    :cond_1
    iget v0, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;->a:I

    iget v3, p1, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;->a:I

    if-lt v0, v3, :cond_2

    .line 657543
    iget-object v0, p0, LX/3xQ;->b:Ljava/util/List;

    invoke-interface {v0, v1, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 657544
    :goto_1
    return-void

    .line 657545
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 657546
    :cond_3
    iget-object v0, p0, LX/3xQ;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public final b(I)I
    .locals 5

    .prologue
    const/4 v0, -0x1

    .line 657511
    iget-object v1, p0, LX/3xQ;->a:[I

    if-nez v1, :cond_1

    .line 657512
    :cond_0
    :goto_0
    return v0

    .line 657513
    :cond_1
    iget-object v1, p0, LX/3xQ;->a:[I

    array-length v1, v1

    if-ge p1, v1, :cond_0

    .line 657514
    const/4 v2, -0x1

    .line 657515
    iget-object v1, p0, LX/3xQ;->b:Ljava/util/List;

    if-nez v1, :cond_3

    move v1, v2

    .line 657516
    :goto_1
    move v1, v1

    .line 657517
    if-ne v1, v0, :cond_2

    .line 657518
    iget-object v1, p0, LX/3xQ;->a:[I

    iget-object v2, p0, LX/3xQ;->a:[I

    array-length v2, v2

    invoke-static {v1, p1, v2, v0}, Ljava/util/Arrays;->fill([IIII)V

    .line 657519
    iget-object v0, p0, LX/3xQ;->a:[I

    array-length v0, v0

    goto :goto_0

    .line 657520
    :cond_2
    iget-object v2, p0, LX/3xQ;->a:[I

    add-int/lit8 v3, v1, 0x1

    invoke-static {v2, p1, v3, v0}, Ljava/util/Arrays;->fill([IIII)V

    .line 657521
    add-int/lit8 v0, v1, 0x1

    goto :goto_0

    .line 657522
    :cond_3
    invoke-virtual {p0, p1}, LX/3xQ;->d(I)Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;

    move-result-object v1

    .line 657523
    if-eqz v1, :cond_4

    .line 657524
    iget-object v3, p0, LX/3xQ;->b:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 657525
    :cond_4
    iget-object v1, p0, LX/3xQ;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    .line 657526
    const/4 v3, 0x0

    :goto_2
    if-ge v3, v4, :cond_7

    .line 657527
    iget-object v1, p0, LX/3xQ;->b:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;

    .line 657528
    iget v1, v1, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;->a:I

    if-lt v1, p1, :cond_5

    .line 657529
    :goto_3
    if-eq v3, v2, :cond_6

    .line 657530
    iget-object v1, p0, LX/3xQ;->b:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;

    .line 657531
    iget-object v2, p0, LX/3xQ;->b:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 657532
    iget v1, v1, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;->a:I

    goto :goto_1

    .line 657533
    :cond_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_6
    move v1, v2

    .line 657534
    goto :goto_1

    :cond_7
    move v3, v2

    goto :goto_3
.end method

.method public final b(II)V
    .locals 4

    .prologue
    .line 657499
    iget-object v0, p0, LX/3xQ;->a:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3xQ;->a:[I

    array-length v0, v0

    if-lt p1, v0, :cond_1

    .line 657500
    :cond_0
    :goto_0
    return-void

    .line 657501
    :cond_1
    add-int v0, p1, p2

    invoke-static {p0, v0}, LX/3xQ;->f(LX/3xQ;I)V

    .line 657502
    iget-object v0, p0, LX/3xQ;->a:[I

    iget-object v1, p0, LX/3xQ;->a:[I

    add-int v2, p1, p2

    iget-object v3, p0, LX/3xQ;->a:[I

    array-length v3, v3

    sub-int/2addr v3, p1

    sub-int/2addr v3, p2

    invoke-static {v0, p1, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 657503
    iget-object v0, p0, LX/3xQ;->a:[I

    add-int v1, p1, p2

    const/4 v2, -0x1

    invoke-static {v0, p1, v1, v2}, Ljava/util/Arrays;->fill([IIII)V

    .line 657504
    iget-object v0, p0, LX/3xQ;->b:Ljava/util/List;

    if-nez v0, :cond_3

    .line 657505
    :cond_2
    goto :goto_0

    .line 657506
    :cond_3
    iget-object v0, p0, LX/3xQ;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_1
    if-ltz v1, :cond_2

    .line 657507
    iget-object v0, p0, LX/3xQ;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;

    .line 657508
    iget v2, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;->a:I

    if-lt v2, p1, :cond_4

    .line 657509
    iget v2, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;->a:I

    add-int/2addr v2, p2

    iput v2, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;->a:I

    .line 657510
    :cond_4
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_1
.end method

.method public final c(I)I
    .locals 1

    .prologue
    .line 657496
    iget-object v0, p0, LX/3xQ;->a:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3xQ;->a:[I

    array-length v0, v0

    if-lt p1, v0, :cond_1

    .line 657497
    :cond_0
    const/4 v0, -0x1

    .line 657498
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, LX/3xQ;->a:[I

    aget v0, v0, p1

    goto :goto_0
.end method

.method public final d(I)Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 657489
    iget-object v0, p0, LX/3xQ;->b:Ljava/util/List;

    if-nez v0, :cond_1

    move-object v0, v1

    .line 657490
    :cond_0
    :goto_0
    return-object v0

    .line 657491
    :cond_1
    iget-object v0, p0, LX/3xQ;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_1
    if-ltz v2, :cond_2

    .line 657492
    iget-object v0, p0, LX/3xQ;->b:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;

    .line 657493
    iget v3, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;->a:I

    if-eq v3, p1, :cond_0

    .line 657494
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 657495
    goto :goto_0
.end method
