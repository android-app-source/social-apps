.class public final LX/4ul;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2wt;


# instance fields
.field public final synthetic a:LX/4um;

.field private final b:LX/2wJ;

.field private final c:LX/4uL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4uL",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/4um;LX/2wJ;LX/4uL;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2wJ;",
            "LX/4uL",
            "<*>;)V"
        }
    .end annotation

    iput-object p1, p0, LX/4ul;->a:LX/4um;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, LX/4ul;->b:LX/2wJ;

    iput-object p3, p0, LX/4ul;->c:LX/4uL;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 3
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/4ul;->b:LX/2wJ;

    const/4 v1, 0x0

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/2wJ;->a(LX/4st;Ljava/util/Set;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, LX/4ul;->a:LX/4um;

    iget-object v0, v0, LX/4um;->j:Ljava/util/Map;

    iget-object v1, p0, LX/4ul;->c:LX/4uL;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Kb;

    invoke-virtual {v0, p1}, LX/3Kb;->a(Lcom/google/android/gms/common/ConnectionResult;)V

    goto :goto_0
.end method
