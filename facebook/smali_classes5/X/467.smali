.class public abstract LX/467;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/facebook/common/locale/LocaleMember;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:LX/0QJ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QJ",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0QJ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QJ",
            "<",
            "Ljava/lang/String;",
            "TT;>;"
        }
    .end annotation
.end field

.field public final c:LX/0QR;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QR",
            "<",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "TT;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 670837
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 670838
    invoke-static {}, LX/0QN;->newBuilder()LX/0QN;

    move-result-object v0

    new-instance v1, LX/46B;

    invoke-direct {v1, p0}, LX/46B;-><init>(LX/467;)V

    invoke-virtual {v0, v1}, LX/0QN;->a(LX/0QM;)LX/0QJ;

    move-result-object v0

    iput-object v0, p0, LX/467;->a:LX/0QJ;

    .line 670839
    invoke-static {}, LX/0QN;->newBuilder()LX/0QN;

    move-result-object v0

    new-instance v1, LX/46C;

    invoke-direct {v1, p0}, LX/46C;-><init>(LX/467;)V

    invoke-virtual {v0, v1}, LX/0QN;->a(LX/0QM;)LX/0QJ;

    move-result-object v0

    iput-object v0, p0, LX/467;->b:LX/0QJ;

    .line 670840
    new-instance v0, LX/46D;

    invoke-direct {v0, p0}, LX/46D;-><init>(LX/467;)V

    invoke-static {v0}, LX/0Wf;->memoize(LX/0QR;)LX/0QR;

    move-result-object v0

    iput-object v0, p0, LX/467;->c:LX/0QR;

    return-void
.end method

.method public static b$redex0(LX/467;)LX/0P1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "TT;>;"
        }
    .end annotation

    .prologue
    .line 670854
    :try_start_0
    invoke-static {p0}, LX/467;->c(LX/467;)Ljava/util/List;

    move-result-object v0

    new-instance v1, LX/46E;

    invoke-direct {v1, p0}, LX/46E;-><init>(LX/467;)V

    invoke-static {v0, v1}, LX/0PM;->a(Ljava/lang/Iterable;LX/0QK;)LX/0P1;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 670855
    :catch_0
    move-exception v0

    .line 670856
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to construct a unique ISO3 index of items: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, LX/467;->c(LX/467;)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static c(LX/467;Ljava/lang/String;)Lcom/facebook/common/locale/LocaleMember;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 670851
    :try_start_0
    iget-object v0, p0, LX/467;->b:LX/0QJ;

    invoke-interface {v0, p1}, LX/0QJ;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/locale/LocaleMember;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 670852
    :catch_0
    move-exception v0

    .line 670853
    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method private static c(LX/467;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 670850
    invoke-virtual {p0}, LX/467;->a()[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    new-instance v1, LX/46F;

    invoke-direct {v1, p0}, LX/46F;-><init>(LX/467;)V

    invoke-static {v0, v1}, LX/0R9;->a(Ljava/util/List;LX/0QK;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private static e(Ljava/lang/String;)Ljava/lang/IllegalArgumentException;
    .locals 3
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 670857
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Not a legal code: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public abstract a(Ljava/util/Locale;)Lcom/facebook/common/locale/LocaleMember;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Locale;",
            ")TT;"
        }
    .end annotation
.end method

.method public abstract a(Ljava/lang/String;)Ljava/util/Locale;
.end method

.method public abstract a()[Ljava/lang/String;
.end method

.method public final b(Ljava/lang/String;)Lcom/facebook/common/locale/LocaleMember;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 670841
    if-nez p1, :cond_0

    .line 670842
    invoke-static {p1}, LX/467;->e(Ljava/lang/String;)Ljava/lang/IllegalArgumentException;

    move-result-object v0

    throw v0

    .line 670843
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 670844
    invoke-static {p0, p1}, LX/467;->c(LX/467;Ljava/lang/String;)Lcom/facebook/common/locale/LocaleMember;

    move-result-object v0

    .line 670845
    :goto_0
    return-object v0

    .line 670846
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    .line 670847
    iget-object v0, p0, LX/467;->c:LX/0QR;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/locale/LocaleMember;

    move-object v0, v0

    .line 670848
    goto :goto_0

    .line 670849
    :cond_2
    invoke-static {p1}, LX/467;->e(Ljava/lang/String;)Ljava/lang/IllegalArgumentException;

    move-result-object v0

    throw v0
.end method
