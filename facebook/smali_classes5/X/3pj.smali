.class public final LX/3pj;
.super LX/3pi;
.source ""


# static fields
.field public static final a:LX/3pf;


# instance fields
.field private final b:[Ljava/lang/String;

.field private final c:LX/3qL;

.field private final d:Landroid/app/PendingIntent;

.field private final e:Landroid/app/PendingIntent;

.field private final f:[Ljava/lang/String;

.field private final g:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 641697
    new-instance v0, LX/3pg;

    invoke-direct {v0}, LX/3pg;-><init>()V

    sput-object v0, LX/3pj;->a:LX/3pf;

    return-void
.end method

.method public constructor <init>([Ljava/lang/String;LX/3qL;Landroid/app/PendingIntent;Landroid/app/PendingIntent;[Ljava/lang/String;J)V
    .locals 0

    .prologue
    .line 641698
    invoke-direct {p0}, LX/3pi;-><init>()V

    .line 641699
    iput-object p1, p0, LX/3pj;->b:[Ljava/lang/String;

    .line 641700
    iput-object p2, p0, LX/3pj;->c:LX/3qL;

    .line 641701
    iput-object p4, p0, LX/3pj;->e:Landroid/app/PendingIntent;

    .line 641702
    iput-object p3, p0, LX/3pj;->d:Landroid/app/PendingIntent;

    .line 641703
    iput-object p5, p0, LX/3pj;->f:[Ljava/lang/String;

    .line 641704
    iput-wide p6, p0, LX/3pj;->g:J

    .line 641705
    return-void
.end method


# virtual methods
.method public final a()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 641706
    iget-object v0, p0, LX/3pj;->b:[Ljava/lang/String;

    return-object v0
.end method

.method public final b()Landroid/app/PendingIntent;
    .locals 1

    .prologue
    .line 641707
    iget-object v0, p0, LX/3pj;->d:Landroid/app/PendingIntent;

    return-object v0
.end method

.method public final c()Landroid/app/PendingIntent;
    .locals 1

    .prologue
    .line 641708
    iget-object v0, p0, LX/3pj;->e:Landroid/app/PendingIntent;

    return-object v0
.end method

.method public final d()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 641709
    iget-object v0, p0, LX/3pj;->f:[Ljava/lang/String;

    return-object v0
.end method

.method public final e()J
    .locals 2

    .prologue
    .line 641710
    iget-wide v0, p0, LX/3pj;->g:J

    return-wide v0
.end method

.method public final f()LX/3qK;
    .locals 1

    .prologue
    .line 641711
    iget-object v0, p0, LX/3pj;->c:LX/3qL;

    return-object v0
.end method
