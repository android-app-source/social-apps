.class public LX/4e8;
.super LX/0Tr;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/4e8;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Tt;LX/31F;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 796721
    invoke-static {p3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    const-string v1, "disk_cache_image_histories_db"

    invoke-direct {p0, p1, p2, v0, v1}, LX/0Tr;-><init>(Landroid/content/Context;LX/0Tt;LX/0Px;Ljava/lang/String;)V

    .line 796722
    return-void
.end method

.method public static a(LX/0QB;)LX/4e8;
    .locals 6

    .prologue
    .line 796723
    sget-object v0, LX/4e8;->a:LX/4e8;

    if-nez v0, :cond_1

    .line 796724
    const-class v1, LX/4e8;

    monitor-enter v1

    .line 796725
    :try_start_0
    sget-object v0, LX/4e8;->a:LX/4e8;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 796726
    if-eqz v2, :cond_0

    .line 796727
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 796728
    new-instance p0, LX/4e8;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0Ts;->a(LX/0QB;)LX/0Ts;

    move-result-object v4

    check-cast v4, LX/0Tt;

    invoke-static {v0}, LX/31F;->a(LX/0QB;)LX/31F;

    move-result-object v5

    check-cast v5, LX/31F;

    invoke-direct {p0, v3, v4, v5}, LX/4e8;-><init>(Landroid/content/Context;LX/0Tt;LX/31F;)V

    .line 796729
    move-object v0, p0

    .line 796730
    sput-object v0, LX/4e8;->a:LX/4e8;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 796731
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 796732
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 796733
    :cond_1
    sget-object v0, LX/4e8;->a:LX/4e8;

    return-object v0

    .line 796734
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 796735
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
