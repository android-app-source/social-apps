.class public LX/3UD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3U8;


# instance fields
.field private final a:LX/3Tw;


# direct methods
.method public constructor <init>(LX/3Tw;)V
    .locals 0
    .param p1    # LX/3Tw;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 585553
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 585554
    iput-object p1, p0, LX/3UD;->a:LX/3Tw;

    .line 585555
    return-void
.end method


# virtual methods
.method public final b(Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/9uc;)V
    .locals 3

    .prologue
    .line 585556
    iget-object v0, p0, LX/3UD;->a:LX/3Tw;

    invoke-interface {v0}, LX/3Tw;->u()LX/0o8;

    move-result-object v0

    .line 585557
    if-nez v0, :cond_1

    .line 585558
    :cond_0
    :goto_0
    return-void

    .line 585559
    :cond_1
    iget-object v1, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v1, v1

    .line 585560
    invoke-interface {v0, v1}, LX/0o8;->b(Ljava/lang/String;)Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v1

    .line 585561
    if-eqz v1, :cond_0

    .line 585562
    iget-object v2, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v2, v2

    .line 585563
    invoke-interface {v2}, LX/9uc;->R()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, p2, v2}, LX/Cfu;->a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;LX/9uc;Ljava/lang/String;)Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v1

    .line 585564
    iget-object v2, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v2, v2

    .line 585565
    invoke-interface {v0, v1, v2}, LX/0o8;->a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;Ljava/lang/String;)Z

    goto :goto_0
.end method
