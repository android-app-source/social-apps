.class public LX/3T4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Landroid/content/Context;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/10M;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/2JZ;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:LX/0dC;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 583556
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 583557
    return-void
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/Integer;
    .locals 2

    .prologue
    .line 583617
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "1993267864233146"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 583610
    if-eqz p0, :cond_0

    const-string v0, "data"

    invoke-virtual {p0, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 583611
    const-string v0, "data"

    invoke-virtual {p0, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 583612
    invoke-virtual {v0}, LX/0lF;->e()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 583613
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0lF;->a(I)LX/0lF;

    move-result-object v0

    .line 583614
    invoke-virtual {v0, p1}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, p1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-virtual {v1}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 583615
    invoke-virtual {v0, p1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v0

    .line 583616
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/3T4;
    .locals 7

    .prologue
    .line 583606
    new-instance v0, LX/3T4;

    invoke-direct {v0}, LX/3T4;-><init>()V

    .line 583607
    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/10M;->b(LX/0QB;)LX/10M;

    move-result-object v2

    check-cast v2, LX/10M;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v4, 0x15e7

    invoke-static {p0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {p0}, LX/2JZ;->b(LX/0QB;)LX/2JZ;

    move-result-object v5

    check-cast v5, LX/2JZ;

    invoke-static {p0}, LX/0dB;->b(LX/0QB;)LX/0dC;

    move-result-object v6

    check-cast v6, LX/0dC;

    .line 583608
    iput-object v1, v0, LX/3T4;->a:Landroid/content/Context;

    iput-object v2, v0, LX/3T4;->b:LX/10M;

    iput-object v3, v0, LX/3T4;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object v4, v0, LX/3T4;->d:LX/0Or;

    iput-object v5, v0, LX/3T4;->e:LX/2JZ;

    iput-object v6, v0, LX/3T4;->f:LX/0dC;

    .line 583609
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 11

    .prologue
    .line 583582
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v0

    const-string v1, "logged_out_push"

    .line 583583
    iput-object v1, v0, LX/14O;->b:Ljava/lang/String;

    .line 583584
    move-object v0, v0

    .line 583585
    const-string v1, "GET"

    .line 583586
    iput-object v1, v0, LX/14O;->c:Ljava/lang/String;

    .line 583587
    move-object v0, v0

    .line 583588
    const-string v1, "dbl/logged_out_notifs"

    .line 583589
    iput-object v1, v0, LX/14O;->d:Ljava/lang/String;

    .line 583590
    move-object v0, v0

    .line 583591
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 583592
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "machine_id"

    iget-object v4, p0, LX/3T4;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v5, LX/26p;->f:LX/0Tn;

    const/4 v6, 0x0

    invoke-interface {v4, v5, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v8, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 583593
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "device_id"

    iget-object v4, p0, LX/3T4;->f:LX/0dC;

    invoke-virtual {v4}, LX/0dC;->a()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v8, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 583594
    iget-object v2, p0, LX/3T4;->b:LX/10M;

    invoke-virtual {v2}, LX/10M;->a()Ljava/util/List;

    move-result-object v2

    .line 583595
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_0
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v7, v2

    check-cast v7, Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    .line 583596
    if-eqz v7, :cond_0

    iget-object v2, v7, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mAlternativeAccessToken:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/3T4;->e:LX/2JZ;

    iget-object v3, v7, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mUserId:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/2JZ;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 583597
    const-string v2, "uid"

    iget-object v3, v7, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mUserId:Ljava/lang/String;

    const-string v4, "session_token"

    iget-object v5, v7, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mAlternativeAccessToken:Ljava/lang/String;

    const-string v6, "is_logged_in"

    iget-object v7, v7, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mUserId:Ljava/lang/String;

    iget-object v10, p0, LX/3T4;->d:LX/0Or;

    invoke-interface {v10}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-static/range {v2 .. v7}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v2

    .line 583598
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "accounts[]"

    invoke-static {v2}, LX/16N;->a(Ljava/lang/Object;)LX/0lF;

    move-result-object v2

    invoke-virtual {v2}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v4, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v8, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 583599
    :cond_1
    move-object v1, v8

    .line 583600
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 583601
    move-object v0, v0

    .line 583602
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 583603
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 583604
    move-object v0, v0

    .line 583605
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 583558
    const/4 v8, 0x0

    .line 583559
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 583560
    const-string v1, "target_uid"

    invoke-static {v0, v1}, LX/3T4;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 583561
    const-string v2, "ndid"

    invoke-static {v0, v2}, LX/3T4;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 583562
    const-string v3, "message"

    invoke-static {v0, v3}, LX/3T4;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 583563
    const-string v4, "landing_experience"

    invoke-static {v0, v4}, LX/3T4;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;

    move-result-object v4

    .line 583564
    const-string v5, "landing_interstitial_text"

    invoke-static {v0, v5}, LX/3T4;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 583565
    iget-object v5, p0, LX/3T4;->b:LX/10M;

    invoke-virtual {v5, v1}, LX/10M;->c(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, LX/3T4;->b:LX/10M;

    invoke-virtual {v5, v1}, LX/10M;->e(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    :cond_0
    iget-object v5, p0, LX/3T4;->d:LX/0Or;

    invoke-interface {v5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_1

    iget-object v5, p0, LX/3T4;->d:LX/0Or;

    invoke-interface {v5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    :cond_1
    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_3

    const/4 v5, 0x1

    :goto_0
    move v5, v5

    .line 583566
    if-nez v5, :cond_2

    .line 583567
    :goto_1
    return-object v8

    .line 583568
    :cond_2
    new-instance v5, LX/2HB;

    iget-object v6, p0, LX/3T4;->a:Landroid/content/Context;

    invoke-direct {v5, v6}, LX/2HB;-><init>(Landroid/content/Context;)V

    const v6, 0x7f0218e4

    invoke-virtual {v5, v6}, LX/2HB;->a(I)LX/2HB;

    move-result-object v5

    iget-object v6, p0, LX/3T4;->a:Landroid/content/Context;

    const v7, 0x7f0800c7

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/2HB;->a(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v5

    const/4 v6, 0x2

    .line 583569
    iput v6, v5, LX/2HB;->j:I

    .line 583570
    move-object v5, v5

    .line 583571
    invoke-virtual {v5, v3}, LX/2HB;->b(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v3

    const/4 v5, 0x1

    invoke-virtual {v3, v5}, LX/2HB;->c(Z)LX/2HB;

    move-result-object v3

    .line 583572
    new-instance v5, Landroid/content/Intent;

    iget-object v6, p0, LX/3T4;->a:Landroid/content/Context;

    const-class v7, Lcom/facebook/notifications/push/loggedoutpush/NotificationsLoggedOutPushIntentService;

    invoke-direct {v5, v6, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 583573
    const-string v6, "uid"

    invoke-virtual {v5, v6, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 583574
    const-string v6, "ndid"

    invoke-virtual {v5, v6, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 583575
    const-string v6, "landing_experience"

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLASNotificationLandingExperience;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 583576
    const-string v6, "landing_interstitial_text"

    invoke-virtual {v5, v6, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 583577
    iget-object v6, p0, LX/3T4;->a:Landroid/content/Context;

    invoke-static {v1}, LX/3T4;->a(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    const/high16 p1, 0x8000000

    invoke-static {v6, v7, v5, p1}, LX/0nt;->c(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v5

    move-object v0, v5

    .line 583578
    iput-object v0, v3, LX/2HB;->d:Landroid/app/PendingIntent;

    .line 583579
    move-object v2, v3

    .line 583580
    iget-object v0, p0, LX/3T4;->a:Landroid/content/Context;

    const-string v3, "notification"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 583581
    invoke-static {v1}, LX/3T4;->a(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v2}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto :goto_1

    :cond_3
    const/4 v5, 0x0

    goto :goto_0
.end method
