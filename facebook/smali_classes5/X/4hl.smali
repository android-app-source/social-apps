.class public final LX/4hl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/4hc;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/4hc;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 802073
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 802074
    iput-object p1, p0, LX/4hl;->a:LX/0QB;

    .line 802075
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 802076
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/4hl;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 802077
    packed-switch p2, :pswitch_data_0

    .line 802078
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 802079
    :pswitch_0
    new-instance v1, LX/GwA;

    const-class v0, LX/Gw9;

    invoke-interface {p1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/Gw9;

    invoke-direct {v1, v0}, LX/GwA;-><init>(LX/Gw9;)V

    .line 802080
    move-object v0, v1

    .line 802081
    :goto_0
    return-object v0

    .line 802082
    :pswitch_1
    new-instance v1, LX/GwF;

    const-class v0, LX/GwE;

    invoke-interface {p1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/GwE;

    invoke-direct {v1, v0}, LX/GwF;-><init>(LX/GwE;)V

    .line 802083
    move-object v0, v1

    .line 802084
    goto :goto_0

    .line 802085
    :pswitch_2
    new-instance p0, LX/GwM;

    const-class v0, LX/GwL;

    invoke-interface {p1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/GwL;

    invoke-static {p1}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v1

    check-cast v1, LX/0lC;

    invoke-direct {p0, v0, v1}, LX/GwM;-><init>(LX/GwL;LX/0lC;)V

    .line 802086
    move-object v0, p0

    .line 802087
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 802088
    const/4 v0, 0x3

    return v0
.end method
