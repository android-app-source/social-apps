.class public LX/3w1;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static a:Ljava/lang/reflect/Method;


# instance fields
.field public A:Landroid/os/Handler;

.field public B:Landroid/graphics/Rect;

.field public C:Z

.field private D:I

.field public b:I

.field public c:Landroid/content/Context;

.field public d:Landroid/widget/PopupWindow;

.field public e:Landroid/widget/ListAdapter;

.field public f:LX/3wy;

.field public g:I

.field public h:I

.field private i:I

.field public j:I

.field public k:Z

.field public l:I

.field public m:Z

.field private n:Z

.field public o:Landroid/view/View;

.field public p:I

.field private q:Landroid/database/DataSetObserver;

.field public r:Landroid/view/View;

.field public s:Landroid/graphics/drawable/Drawable;

.field public t:Landroid/widget/AdapterView$OnItemClickListener;

.field public u:Landroid/widget/AdapterView$OnItemSelectedListener;

.field public final v:Landroid/support/v7/widget/ListPopupWindow$ResizePopupRunnable;

.field private final w:LX/3x1;

.field public final x:LX/3x0;

.field private final y:Landroid/support/v7/widget/ListPopupWindow$ListSelectorHider;

.field public z:Ljava/lang/Runnable;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    .line 654128
    :try_start_0
    const-class v0, Landroid/widget/PopupWindow;

    const-string v1, "setClipToScreenEnabled"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    sget-object v4, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, LX/3w1;->a:Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    .line 654129
    :goto_0
    return-void

    :catch_0
    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 653953
    const/4 v0, 0x0

    const v1, 0x7f01004a

    invoke-direct {p0, p1, v0, v1}, LX/3w1;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 653954
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 654126
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, LX/3w1;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 654127
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v0, -0x2

    const/4 v2, 0x0

    .line 654100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 654101
    iput v0, p0, LX/3w1;->g:I

    .line 654102
    iput v0, p0, LX/3w1;->h:I

    .line 654103
    iput v2, p0, LX/3w1;->l:I

    .line 654104
    iput-boolean v2, p0, LX/3w1;->m:Z

    .line 654105
    iput-boolean v2, p0, LX/3w1;->n:Z

    .line 654106
    const v0, 0x7fffffff

    iput v0, p0, LX/3w1;->b:I

    .line 654107
    iput v2, p0, LX/3w1;->p:I

    .line 654108
    new-instance v0, Landroid/support/v7/widget/ListPopupWindow$ResizePopupRunnable;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/ListPopupWindow$ResizePopupRunnable;-><init>(LX/3w1;)V

    iput-object v0, p0, LX/3w1;->v:Landroid/support/v7/widget/ListPopupWindow$ResizePopupRunnable;

    .line 654109
    new-instance v0, LX/3x1;

    invoke-direct {v0, p0}, LX/3x1;-><init>(LX/3w1;)V

    iput-object v0, p0, LX/3w1;->w:LX/3x1;

    .line 654110
    new-instance v0, LX/3x0;

    invoke-direct {v0, p0}, LX/3x0;-><init>(LX/3w1;)V

    iput-object v0, p0, LX/3w1;->x:LX/3x0;

    .line 654111
    new-instance v0, Landroid/support/v7/widget/ListPopupWindow$ListSelectorHider;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/ListPopupWindow$ListSelectorHider;-><init>(LX/3w1;)V

    iput-object v0, p0, LX/3w1;->y:Landroid/support/v7/widget/ListPopupWindow$ListSelectorHider;

    .line 654112
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, LX/3w1;->A:Landroid/os/Handler;

    .line 654113
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/3w1;->B:Landroid/graphics/Rect;

    .line 654114
    iput-object p1, p0, LX/3w1;->c:Landroid/content/Context;

    .line 654115
    sget-object v0, LX/03r;->ListPopupWindow:[I

    invoke-virtual {p1, p2, v0, p3, p4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 654116
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v1

    iput v1, p0, LX/3w1;->i:I

    .line 654117
    const/16 v1, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v1

    iput v1, p0, LX/3w1;->j:I

    .line 654118
    iget v1, p0, LX/3w1;->j:I

    if-eqz v1, :cond_0

    .line 654119
    iput-boolean v3, p0, LX/3w1;->k:Z

    .line 654120
    :cond_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 654121
    new-instance v0, LX/3vj;

    invoke-direct {v0, p1, p2, p3}, LX/3vj;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, LX/3w1;->d:Landroid/widget/PopupWindow;

    .line 654122
    iget-object v0, p0, LX/3w1;->d:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v3}, Landroid/widget/PopupWindow;->setInputMethodMode(I)V

    .line 654123
    iget-object v0, p0, LX/3w1;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 654124
    invoke-static {v0}, LX/3rK;->a(Ljava/util/Locale;)I

    move-result v0

    iput v0, p0, LX/3w1;->D:I

    .line 654125
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 654090
    iget-object v0, p0, LX/3w1;->d:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 654091
    iget-object v0, p0, LX/3w1;->o:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 654092
    iget-object v0, p0, LX/3w1;->o:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 654093
    instance-of v2, v0, Landroid/view/ViewGroup;

    if-eqz v2, :cond_0

    .line 654094
    check-cast v0, Landroid/view/ViewGroup;

    .line 654095
    iget-object v2, p0, LX/3w1;->o:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 654096
    :cond_0
    iget-object v0, p0, LX/3w1;->d:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    .line 654097
    iput-object v1, p0, LX/3w1;->f:LX/3wy;

    .line 654098
    iget-object v0, p0, LX/3w1;->A:Landroid/os/Handler;

    iget-object v1, p0, LX/3w1;->v:Landroid/support/v7/widget/ListPopupWindow$ResizePopupRunnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 654099
    return-void
.end method

.method public a(Landroid/widget/ListAdapter;)V
    .locals 2

    .prologue
    .line 654080
    iget-object v0, p0, LX/3w1;->q:Landroid/database/DataSetObserver;

    if-nez v0, :cond_3

    .line 654081
    new-instance v0, LX/3wz;

    invoke-direct {v0, p0}, LX/3wz;-><init>(LX/3w1;)V

    iput-object v0, p0, LX/3w1;->q:Landroid/database/DataSetObserver;

    .line 654082
    :cond_0
    :goto_0
    iput-object p1, p0, LX/3w1;->e:Landroid/widget/ListAdapter;

    .line 654083
    iget-object v0, p0, LX/3w1;->e:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_1

    .line 654084
    iget-object v0, p0, LX/3w1;->q:Landroid/database/DataSetObserver;

    invoke-interface {p1, v0}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 654085
    :cond_1
    iget-object v0, p0, LX/3w1;->f:LX/3wy;

    if-eqz v0, :cond_2

    .line 654086
    iget-object v0, p0, LX/3w1;->f:LX/3wy;

    iget-object v1, p0, LX/3w1;->e:Landroid/widget/ListAdapter;

    invoke-virtual {v0, v1}, LX/3wy;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 654087
    :cond_2
    return-void

    .line 654088
    :cond_3
    iget-object v0, p0, LX/3w1;->e:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_0

    .line 654089
    iget-object v0, p0, LX/3w1;->e:Landroid/widget/ListAdapter;

    iget-object v1, p0, LX/3w1;->q:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    goto :goto_0
.end method

.method public final a(Landroid/widget/PopupWindow$OnDismissListener;)V
    .locals 1

    .prologue
    .line 654078
    iget-object v0, p0, LX/3w1;->d:Landroid/widget/PopupWindow;

    invoke-virtual {v0, p1}, Landroid/widget/PopupWindow;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    .line 654079
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 654075
    iput-boolean p1, p0, LX/3w1;->C:Z

    .line 654076
    iget-object v0, p0, LX/3w1;->d:Landroid/widget/PopupWindow;

    invoke-virtual {v0, p1}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    .line 654077
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 654074
    iget-object v0, p0, LX/3w1;->d:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    return v0
.end method

.method public final c()V
    .locals 14

    .prologue
    const/4 v3, 0x1

    const/4 v7, -0x2

    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 653969
    const/high16 v12, 0x40000000    # 2.0f

    const/high16 v11, -0x80000000

    const/4 v10, -0x1

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 653970
    iget-object v2, p0, LX/3w1;->f:LX/3wy;

    if-nez v2, :cond_18

    .line 653971
    iget-object v8, p0, LX/3w1;->c:Landroid/content/Context;

    .line 653972
    new-instance v2, Landroid/support/v7/widget/ListPopupWindow$2;

    invoke-direct {v2, p0}, Landroid/support/v7/widget/ListPopupWindow$2;-><init>(LX/3w1;)V

    iput-object v2, p0, LX/3w1;->z:Ljava/lang/Runnable;

    .line 653973
    new-instance v6, LX/3wy;

    iget-boolean v2, p0, LX/3w1;->C:Z

    if-nez v2, :cond_17

    move v2, v4

    :goto_0
    invoke-direct {v6, v8, v2}, LX/3wy;-><init>(Landroid/content/Context;Z)V

    iput-object v6, p0, LX/3w1;->f:LX/3wy;

    .line 653974
    iget-object v2, p0, LX/3w1;->s:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_0

    .line 653975
    iget-object v2, p0, LX/3w1;->f:LX/3wy;

    iget-object v6, p0, LX/3w1;->s:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v6}, LX/3wy;->setSelector(Landroid/graphics/drawable/Drawable;)V

    .line 653976
    :cond_0
    iget-object v2, p0, LX/3w1;->f:LX/3wy;

    iget-object v6, p0, LX/3w1;->e:Landroid/widget/ListAdapter;

    invoke-virtual {v2, v6}, LX/3wy;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 653977
    iget-object v2, p0, LX/3w1;->f:LX/3wy;

    iget-object v6, p0, LX/3w1;->t:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v2, v6}, LX/3wy;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 653978
    iget-object v2, p0, LX/3w1;->f:LX/3wy;

    invoke-virtual {v2, v4}, LX/3wy;->setFocusable(Z)V

    .line 653979
    iget-object v2, p0, LX/3w1;->f:LX/3wy;

    invoke-virtual {v2, v4}, LX/3wy;->setFocusableInTouchMode(Z)V

    .line 653980
    iget-object v2, p0, LX/3w1;->f:LX/3wy;

    new-instance v6, LX/3wx;

    invoke-direct {v6, p0}, LX/3wx;-><init>(LX/3w1;)V

    invoke-virtual {v2, v6}, LX/3wy;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 653981
    iget-object v2, p0, LX/3w1;->f:LX/3wy;

    iget-object v6, p0, LX/3w1;->x:LX/3x0;

    invoke-virtual {v2, v6}, LX/3wy;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 653982
    iget-object v2, p0, LX/3w1;->u:Landroid/widget/AdapterView$OnItemSelectedListener;

    if-eqz v2, :cond_1

    .line 653983
    iget-object v2, p0, LX/3w1;->f:LX/3wy;

    iget-object v6, p0, LX/3w1;->u:Landroid/widget/AdapterView$OnItemSelectedListener;

    invoke-virtual {v2, v6}, LX/3wy;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 653984
    :cond_1
    iget-object v2, p0, LX/3w1;->f:LX/3wy;

    .line 653985
    iget-object v9, p0, LX/3w1;->o:Landroid/view/View;

    .line 653986
    if-eqz v9, :cond_1d

    .line 653987
    new-instance v6, Landroid/widget/LinearLayout;

    invoke-direct {v6, v8}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 653988
    invoke-virtual {v6, v4}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 653989
    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v8, 0x3f800000    # 1.0f

    invoke-direct {v4, v10, v5, v8}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 653990
    iget v8, p0, LX/3w1;->p:I

    packed-switch v8, :pswitch_data_0

    .line 653991
    const-string v2, "ListPopupWindow"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v8, "Invalid hint position "

    invoke-direct {v4, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v8, p0, LX/3w1;->p:I

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 653992
    :goto_1
    iget v2, p0, LX/3w1;->h:I

    invoke-static {v2, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 653993
    invoke-virtual {v9, v2, v5}, Landroid/view/View;->measure(II)V

    .line 653994
    invoke-virtual {v9}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout$LayoutParams;

    .line 653995
    invoke-virtual {v9}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    iget v8, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v4, v8

    iget v2, v2, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v2, v4

    move v4, v2

    move-object v2, v6

    .line 653996
    :goto_2
    iget-object v6, p0, LX/3w1;->d:Landroid/widget/PopupWindow;

    invoke-virtual {v6, v2}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    .line 653997
    :goto_3
    iget-object v2, p0, LX/3w1;->d:Landroid/widget/PopupWindow;

    invoke-virtual {v2}, Landroid/widget/PopupWindow;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 653998
    if-eqz v2, :cond_19

    .line 653999
    iget-object v5, p0, LX/3w1;->B:Landroid/graphics/Rect;

    invoke-virtual {v2, v5}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 654000
    iget-object v2, p0, LX/3w1;->B:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iget-object v5, p0, LX/3w1;->B:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v5, v2

    .line 654001
    iget-boolean v2, p0, LX/3w1;->k:Z

    if-nez v2, :cond_2

    .line 654002
    iget-object v2, p0, LX/3w1;->B:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    neg-int v2, v2

    iput v2, p0, LX/3w1;->j:I

    .line 654003
    :cond_2
    :goto_4
    iget-object v2, p0, LX/3w1;->d:Landroid/widget/PopupWindow;

    invoke-virtual {v2}, Landroid/widget/PopupWindow;->getInputMethodMode()I

    .line 654004
    iget-object v2, p0, LX/3w1;->d:Landroid/widget/PopupWindow;

    .line 654005
    iget-object v6, p0, LX/3w1;->r:Landroid/view/View;

    move-object v6, v6

    .line 654006
    iget v8, p0, LX/3w1;->j:I

    invoke-virtual {v2, v6, v8}, Landroid/widget/PopupWindow;->getMaxAvailableHeight(Landroid/view/View;I)I

    move-result v6

    .line 654007
    iget-boolean v2, p0, LX/3w1;->m:Z

    if-nez v2, :cond_3

    iget v2, p0, LX/3w1;->g:I

    if-ne v2, v10, :cond_1a

    .line 654008
    :cond_3
    add-int v2, v6, v5

    .line 654009
    :goto_5
    move v5, v2

    .line 654010
    invoke-virtual {p0}, LX/3w1;->f()Z

    move-result v2

    .line 654011
    iget-object v4, p0, LX/3w1;->d:Landroid/widget/PopupWindow;

    invoke-virtual {v4}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v4

    if-eqz v4, :cond_e

    .line 654012
    iget v4, p0, LX/3w1;->h:I

    if-ne v4, v0, :cond_7

    move v4, v0

    .line 654013
    :goto_6
    iget v6, p0, LX/3w1;->g:I

    if-ne v6, v0, :cond_d

    .line 654014
    if-eqz v2, :cond_9

    .line 654015
    :goto_7
    if-eqz v2, :cond_b

    .line 654016
    iget-object v2, p0, LX/3w1;->d:Landroid/widget/PopupWindow;

    iget v6, p0, LX/3w1;->h:I

    if-ne v6, v0, :cond_a

    :goto_8
    invoke-virtual {v2, v0, v1}, Landroid/widget/PopupWindow;->setWindowLayoutMode(II)V

    .line 654017
    :cond_4
    :goto_9
    iget-object v0, p0, LX/3w1;->d:Landroid/widget/PopupWindow;

    iget-boolean v2, p0, LX/3w1;->n:Z

    if-nez v2, :cond_5

    iget-boolean v2, p0, LX/3w1;->m:Z

    if-nez v2, :cond_5

    move v1, v3

    :cond_5
    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    .line 654018
    iget-object v0, p0, LX/3w1;->d:Landroid/widget/PopupWindow;

    .line 654019
    iget-object v1, p0, LX/3w1;->r:Landroid/view/View;

    move-object v1, v1

    .line 654020
    iget v2, p0, LX/3w1;->i:I

    iget v3, p0, LX/3w1;->j:I

    invoke-virtual/range {v0 .. v5}, Landroid/widget/PopupWindow;->update(Landroid/view/View;IIII)V

    .line 654021
    :cond_6
    :goto_a
    return-void

    .line 654022
    :cond_7
    iget v4, p0, LX/3w1;->h:I

    if-ne v4, v7, :cond_8

    .line 654023
    iget-object v4, p0, LX/3w1;->r:Landroid/view/View;

    move-object v4, v4

    .line 654024
    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v4

    goto :goto_6

    .line 654025
    :cond_8
    iget v4, p0, LX/3w1;->h:I

    goto :goto_6

    :cond_9
    move v5, v0

    .line 654026
    goto :goto_7

    :cond_a
    move v0, v1

    .line 654027
    goto :goto_8

    .line 654028
    :cond_b
    iget-object v6, p0, LX/3w1;->d:Landroid/widget/PopupWindow;

    iget v2, p0, LX/3w1;->h:I

    if-ne v2, v0, :cond_c

    move v2, v0

    :goto_b
    invoke-virtual {v6, v2, v0}, Landroid/widget/PopupWindow;->setWindowLayoutMode(II)V

    goto :goto_9

    :cond_c
    move v2, v1

    goto :goto_b

    .line 654029
    :cond_d
    iget v0, p0, LX/3w1;->g:I

    if-eq v0, v7, :cond_4

    .line 654030
    iget v5, p0, LX/3w1;->g:I

    goto :goto_9

    .line 654031
    :cond_e
    iget v2, p0, LX/3w1;->h:I

    if-ne v2, v0, :cond_12

    move v2, v0

    .line 654032
    :goto_c
    iget v4, p0, LX/3w1;->g:I

    if-ne v4, v0, :cond_14

    move v4, v0

    .line 654033
    :goto_d
    iget-object v5, p0, LX/3w1;->d:Landroid/widget/PopupWindow;

    invoke-virtual {v5, v2, v4}, Landroid/widget/PopupWindow;->setWindowLayoutMode(II)V

    .line 654034
    sget-object v2, LX/3w1;->a:Ljava/lang/reflect/Method;

    if-eqz v2, :cond_f

    .line 654035
    :try_start_0
    sget-object v2, LX/3w1;->a:Ljava/lang/reflect/Method;

    iget-object v4, p0, LX/3w1;->d:Landroid/widget/PopupWindow;

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v2, v4, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 654036
    :cond_f
    :goto_e
    iget-object v2, p0, LX/3w1;->d:Landroid/widget/PopupWindow;

    iget-boolean v4, p0, LX/3w1;->n:Z

    if-nez v4, :cond_16

    iget-boolean v4, p0, LX/3w1;->m:Z

    if-nez v4, :cond_16

    :goto_f
    invoke-virtual {v2, v3}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    .line 654037
    iget-object v1, p0, LX/3w1;->d:Landroid/widget/PopupWindow;

    iget-object v2, p0, LX/3w1;->w:LX/3x1;

    invoke-virtual {v1, v2}, Landroid/widget/PopupWindow;->setTouchInterceptor(Landroid/view/View$OnTouchListener;)V

    .line 654038
    iget-object v1, p0, LX/3w1;->d:Landroid/widget/PopupWindow;

    .line 654039
    iget-object v2, p0, LX/3w1;->r:Landroid/view/View;

    move-object v2, v2

    .line 654040
    iget v3, p0, LX/3w1;->i:I

    iget v4, p0, LX/3w1;->j:I

    iget v5, p0, LX/3w1;->l:I

    .line 654041
    sget-object v8, LX/3th;->a:LX/3te;

    move-object v9, v1

    move-object v10, v2

    move v11, v3

    move v12, v4

    move v13, v5

    invoke-interface/range {v8 .. v13}, LX/3te;->a(Landroid/widget/PopupWindow;Landroid/view/View;III)V

    .line 654042
    iget-object v1, p0, LX/3w1;->f:LX/3wy;

    invoke-virtual {v1, v0}, LX/3wy;->setSelection(I)V

    .line 654043
    iget-boolean v0, p0, LX/3w1;->C:Z

    if-eqz v0, :cond_10

    iget-object v0, p0, LX/3w1;->f:LX/3wy;

    invoke-virtual {v0}, LX/3wy;->isInTouchMode()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 654044
    :cond_10
    invoke-virtual {p0}, LX/3w1;->e()V

    .line 654045
    :cond_11
    iget-boolean v0, p0, LX/3w1;->C:Z

    if-nez v0, :cond_6

    .line 654046
    iget-object v0, p0, LX/3w1;->A:Landroid/os/Handler;

    iget-object v1, p0, LX/3w1;->y:Landroid/support/v7/widget/ListPopupWindow$ListSelectorHider;

    const v2, 0x2d910688

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto/16 :goto_a

    .line 654047
    :cond_12
    iget v2, p0, LX/3w1;->h:I

    if-ne v2, v7, :cond_13

    .line 654048
    iget-object v2, p0, LX/3w1;->d:Landroid/widget/PopupWindow;

    .line 654049
    iget-object v4, p0, LX/3w1;->r:Landroid/view/View;

    move-object v4, v4

    .line 654050
    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v4

    invoke-virtual {v2, v4}, Landroid/widget/PopupWindow;->setWidth(I)V

    move v2, v1

    goto :goto_c

    .line 654051
    :cond_13
    iget-object v2, p0, LX/3w1;->d:Landroid/widget/PopupWindow;

    iget v4, p0, LX/3w1;->h:I

    invoke-virtual {v2, v4}, Landroid/widget/PopupWindow;->setWidth(I)V

    move v2, v1

    goto/16 :goto_c

    .line 654052
    :cond_14
    iget v4, p0, LX/3w1;->g:I

    if-ne v4, v7, :cond_15

    .line 654053
    iget-object v4, p0, LX/3w1;->d:Landroid/widget/PopupWindow;

    invoke-virtual {v4, v5}, Landroid/widget/PopupWindow;->setHeight(I)V

    move v4, v1

    goto/16 :goto_d

    .line 654054
    :cond_15
    iget-object v4, p0, LX/3w1;->d:Landroid/widget/PopupWindow;

    iget v5, p0, LX/3w1;->g:I

    invoke-virtual {v4, v5}, Landroid/widget/PopupWindow;->setHeight(I)V

    move v4, v1

    goto/16 :goto_d

    :cond_16
    move v3, v1

    .line 654055
    goto :goto_f

    :cond_17
    move v2, v5

    .line 654056
    goto/16 :goto_0

    .line 654057
    :pswitch_0
    invoke-virtual {v6, v2, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 654058
    invoke-virtual {v6, v9}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_1

    .line 654059
    :pswitch_1
    invoke-virtual {v6, v9}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 654060
    invoke-virtual {v6, v2, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_1

    .line 654061
    :cond_18
    iget-object v2, p0, LX/3w1;->d:Landroid/widget/PopupWindow;

    invoke-virtual {v2}, Landroid/widget/PopupWindow;->getContentView()Landroid/view/View;

    .line 654062
    iget-object v4, p0, LX/3w1;->o:Landroid/view/View;

    .line 654063
    if-eqz v4, :cond_1c

    .line 654064
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout$LayoutParams;

    .line 654065
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    iget v6, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v4, v6

    iget v2, v2, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v4, v2

    goto/16 :goto_3

    .line 654066
    :cond_19
    iget-object v2, p0, LX/3w1;->B:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->setEmpty()V

    goto/16 :goto_4

    .line 654067
    :cond_1a
    iget v2, p0, LX/3w1;->h:I

    packed-switch v2, :pswitch_data_1

    .line 654068
    iget v2, p0, LX/3w1;->h:I

    invoke-static {v2, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 654069
    :goto_10
    iget-object v8, p0, LX/3w1;->f:LX/3wy;

    sub-int/2addr v6, v4

    invoke-virtual {v8, v2, v6, v10}, LX/3vn;->a(III)I

    move-result v2

    .line 654070
    if-lez v2, :cond_1b

    add-int/2addr v4, v5

    .line 654071
    :cond_1b
    add-int/2addr v2, v4

    goto/16 :goto_5

    .line 654072
    :pswitch_2
    iget-object v2, p0, LX/3w1;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v8, p0, LX/3w1;->B:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->left:I

    iget-object v9, p0, LX/3w1;->B:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->right:I

    add-int/2addr v8, v9

    sub-int/2addr v2, v8

    invoke-static {v2, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    goto :goto_10

    .line 654073
    :pswitch_3
    iget-object v2, p0, LX/3w1;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v8, p0, LX/3w1;->B:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->left:I

    iget-object v9, p0, LX/3w1;->B:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->right:I

    add-int/2addr v8, v9

    sub-int/2addr v2, v8

    invoke-static {v2, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    goto :goto_10

    :cond_1c
    move v4, v5

    goto/16 :goto_3

    :cond_1d
    move v4, v5

    goto/16 :goto_2

    :catch_0
    goto/16 :goto_e

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch -0x2
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final c(I)V
    .locals 2

    .prologue
    .line 653962
    iget-object v0, p0, LX/3w1;->d:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 653963
    if-eqz v0, :cond_0

    .line 653964
    iget-object v1, p0, LX/3w1;->B:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 653965
    iget-object v0, p0, LX/3w1;->B:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, LX/3w1;->B:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v1

    add-int/2addr v0, p1

    iput v0, p0, LX/3w1;->h:I

    .line 653966
    :goto_0
    return-void

    .line 653967
    :cond_0
    iput p1, p0, LX/3w1;->h:I

    .line 653968
    goto :goto_0
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 653956
    iget-object v0, p0, LX/3w1;->f:LX/3wy;

    .line 653957
    if-eqz v0, :cond_0

    .line 653958
    const/4 v1, 0x1

    .line 653959
    iput-boolean v1, v0, LX/3wy;->f:Z

    .line 653960
    invoke-virtual {v0}, LX/3wy;->requestLayout()V

    .line 653961
    :cond_0
    return-void
.end method

.method public final f()Z
    .locals 2

    .prologue
    .line 653955
    iget-object v0, p0, LX/3w1;->d:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getInputMethodMode()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
