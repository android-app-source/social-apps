.class public final LX/4Ao;
.super LX/1ML;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 677070
    invoke-direct {p0}, LX/1ML;-><init>()V

    return-void
.end method


# virtual methods
.method public cancelOperation()Z
    .locals 1

    .prologue
    .line 677071
    const/4 v0, 0x0

    invoke-super {p0, v0}, LX/1ML;->cancel(Z)Z

    move-result v0

    return v0
.end method

.method public set(Lcom/facebook/fbservice/service/OperationResult;)Z
    .locals 1
    .param p1    # Lcom/facebook/fbservice/service/OperationResult;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 677072
    invoke-super {p0, p1}, LX/1ML;->set(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic set(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 677073
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    invoke-virtual {p0, p1}, LX/4Ao;->set(Lcom/facebook/fbservice/service/OperationResult;)Z

    move-result v0

    return v0
.end method

.method public setException(Ljava/lang/Throwable;)Z
    .locals 1

    .prologue
    .line 677074
    invoke-super {p0, p1}, LX/1ML;->setException(Ljava/lang/Throwable;)Z

    move-result v0

    return v0
.end method
