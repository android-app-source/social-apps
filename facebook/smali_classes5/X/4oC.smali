.class public LX/4oC;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:Landroid/graphics/drawable/Drawable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 809045
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 809046
    return-void
.end method


# virtual methods
.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 809040
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 809041
    iget-object v0, p0, LX/4oC;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 809042
    iget-object v0, p0, LX/4oC;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, LX/4oC;->getWidth()I

    move-result v1

    invoke-virtual {p0}, LX/4oC;->getHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 809043
    iget-object v0, p0, LX/4oC;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 809044
    :cond_0
    return-void
.end method

.method public getOverlayDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 809047
    iget-object v0, p0, LX/4oC;->a:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 809039
    iget-object v0, p0, LX/4oC;->a:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x1ff97bb6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 809036
    iget-object v0, p0, LX/4oC;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 809037
    const/4 v0, 0x0

    const v2, -0x487f4e90

    invoke-static {v3, v3, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 809038
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    const v2, -0x4ca6950c

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto :goto_0
.end method
