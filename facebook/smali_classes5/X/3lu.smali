.class public final LX/3lu;
.super LX/0wa;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/common/perftest/DrawFrameLogger;


# direct methods
.method public constructor <init>(Lcom/facebook/common/perftest/DrawFrameLogger;)V
    .locals 0

    .prologue
    .line 635235
    iput-object p1, p0, LX/3lu;->a:Lcom/facebook/common/perftest/DrawFrameLogger;

    invoke-direct {p0}, LX/0wa;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(J)V
    .locals 8

    .prologue
    .line 635236
    const-wide/32 v0, 0xf4240

    div-long v0, p1, v0

    .line 635237
    iget-object v2, p0, LX/3lu;->a:Lcom/facebook/common/perftest/DrawFrameLogger;

    iget-wide v2, v2, Lcom/facebook/common/perftest/DrawFrameLogger;->j:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 635238
    iget-object v2, p0, LX/3lu;->a:Lcom/facebook/common/perftest/DrawFrameLogger;

    .line 635239
    iput-wide v0, v2, Lcom/facebook/common/perftest/DrawFrameLogger;->j:J

    .line 635240
    iget-object v0, p0, LX/3lu;->a:Lcom/facebook/common/perftest/DrawFrameLogger;

    iget-object v0, v0, Lcom/facebook/common/perftest/DrawFrameLogger;->h:LX/0wY;

    iget-object v1, p0, LX/3lu;->a:Lcom/facebook/common/perftest/DrawFrameLogger;

    iget-object v1, v1, Lcom/facebook/common/perftest/DrawFrameLogger;->i:LX/0wa;

    invoke-interface {v0, v1}, LX/0wY;->a(LX/0wa;)V

    .line 635241
    :goto_0
    return-void

    .line 635242
    :cond_0
    iget-object v2, p0, LX/3lu;->a:Lcom/facebook/common/perftest/DrawFrameLogger;

    iget-wide v2, v2, Lcom/facebook/common/perftest/DrawFrameLogger;->j:J

    sub-long v2, v0, v2

    .line 635243
    iget-object v4, p0, LX/3lu;->a:Lcom/facebook/common/perftest/DrawFrameLogger;

    .line 635244
    iput-wide v0, v4, Lcom/facebook/common/perftest/DrawFrameLogger;->j:J

    .line 635245
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/common/perftest/DrawFrameLogger;->b(JJ)V

    .line 635246
    iget-object v0, p0, LX/3lu;->a:Lcom/facebook/common/perftest/DrawFrameLogger;

    iget-object v0, v0, Lcom/facebook/common/perftest/DrawFrameLogger;->h:LX/0wY;

    iget-object v1, p0, LX/3lu;->a:Lcom/facebook/common/perftest/DrawFrameLogger;

    iget-object v1, v1, Lcom/facebook/common/perftest/DrawFrameLogger;->i:LX/0wa;

    invoke-interface {v0, v1}, LX/0wY;->a(LX/0wa;)V

    goto :goto_0
.end method
