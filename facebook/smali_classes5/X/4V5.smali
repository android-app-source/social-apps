.class public LX/4V5;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/4V5;


# instance fields
.field public final a:Landroid/os/Handler;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/os/Handler;)V
    .locals 0
    .param p1    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 742050
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 742051
    iput-object p1, p0, LX/4V5;->a:Landroid/os/Handler;

    .line 742052
    return-void
.end method

.method public static a(LX/0QB;)LX/4V5;
    .locals 4

    .prologue
    .line 742053
    sget-object v0, LX/4V5;->b:LX/4V5;

    if-nez v0, :cond_1

    .line 742054
    const-class v1, LX/4V5;

    monitor-enter v1

    .line 742055
    :try_start_0
    sget-object v0, LX/4V5;->b:LX/4V5;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 742056
    if-eqz v2, :cond_0

    .line 742057
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 742058
    new-instance p0, LX/4V5;

    invoke-static {v0}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v3

    check-cast v3, Landroid/os/Handler;

    invoke-direct {p0, v3}, LX/4V5;-><init>(Landroid/os/Handler;)V

    .line 742059
    move-object v0, p0

    .line 742060
    sput-object v0, LX/4V5;->b:LX/4V5;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 742061
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 742062
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 742063
    :cond_1
    sget-object v0, LX/4V5;->b:LX/4V5;

    return-object v0

    .line 742064
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 742065
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0zX;LX/0rl;)LX/0zi;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0zX",
            "<TT;>;",
            "LX/0rl",
            "<TT;>;)",
            "LX/0zi;"
        }
    .end annotation

    .prologue
    .line 742066
    new-instance v0, LX/4V4;

    invoke-direct {v0, p0, p2}, LX/4V4;-><init>(LX/4V5;LX/0rl;)V

    invoke-virtual {p1, v0}, LX/0zX;->a(LX/0rl;)LX/0zi;

    move-result-object v0

    return-object v0
.end method
