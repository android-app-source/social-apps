.class public final LX/3pK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# instance fields
.field public final synthetic a:Landroid/view/View;

.field public final synthetic b:LX/0xR;

.field public final synthetic c:Ljava/util/Map;

.field public final synthetic d:Ljava/util/Map;

.field public final synthetic e:Landroid/transition/Transition;

.field public final synthetic f:Ljava/util/ArrayList;

.field public final synthetic g:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;LX/0xR;Ljava/util/Map;Ljava/util/Map;Landroid/transition/Transition;Ljava/util/ArrayList;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 641468
    iput-object p1, p0, LX/3pK;->a:Landroid/view/View;

    iput-object p2, p0, LX/3pK;->b:LX/0xR;

    iput-object p3, p0, LX/3pK;->c:Ljava/util/Map;

    iput-object p4, p0, LX/3pK;->d:Ljava/util/Map;

    iput-object p5, p0, LX/3pK;->e:Landroid/transition/Transition;

    iput-object p6, p0, LX/3pK;->f:Ljava/util/ArrayList;

    iput-object p7, p0, LX/3pK;->g:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreDraw()Z
    .locals 5

    .prologue
    .line 641469
    iget-object v0, p0, LX/3pK;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 641470
    iget-object v0, p0, LX/3pK;->b:LX/0xR;

    invoke-interface {v0}, LX/0xR;->a()Landroid/view/View;

    move-result-object v2

    .line 641471
    if-eqz v2, :cond_2

    .line 641472
    iget-object v0, p0, LX/3pK;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 641473
    iget-object v0, p0, LX/3pK;->d:Ljava/util/Map;

    invoke-static {v0, v2}, LX/0xL;->a(Ljava/util/Map;Landroid/view/View;)V

    .line 641474
    iget-object v0, p0, LX/3pK;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, LX/3pK;->c:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->retainAll(Ljava/util/Collection;)Z

    .line 641475
    iget-object v0, p0, LX/3pK;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 641476
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 641477
    iget-object v4, p0, LX/3pK;->d:Ljava/util/Map;

    invoke-interface {v4, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 641478
    if-eqz v1, :cond_0

    .line 641479
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 641480
    invoke-virtual {v1, v0}, Landroid/view/View;->setTransitionName(Ljava/lang/String;)V

    goto :goto_0

    .line 641481
    :cond_1
    iget-object v0, p0, LX/3pK;->e:Landroid/transition/Transition;

    if-eqz v0, :cond_2

    .line 641482
    iget-object v0, p0, LX/3pK;->f:Ljava/util/ArrayList;

    invoke-static {v0, v2}, LX/0xL;->b(Ljava/util/ArrayList;Landroid/view/View;)V

    .line 641483
    iget-object v0, p0, LX/3pK;->f:Ljava/util/ArrayList;

    iget-object v1, p0, LX/3pK;->d:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    .line 641484
    iget-object v0, p0, LX/3pK;->f:Ljava/util/ArrayList;

    iget-object v1, p0, LX/3pK;->g:Landroid/view/View;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 641485
    iget-object v0, p0, LX/3pK;->e:Landroid/transition/Transition;

    iget-object v1, p0, LX/3pK;->g:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/transition/Transition;->removeTarget(Landroid/view/View;)Landroid/transition/Transition;

    .line 641486
    iget-object v0, p0, LX/3pK;->e:Landroid/transition/Transition;

    iget-object v1, p0, LX/3pK;->f:Ljava/util/ArrayList;

    invoke-static {v0, v1}, LX/0xL;->b(Ljava/lang/Object;Ljava/util/ArrayList;)V

    .line 641487
    :cond_2
    const/4 v0, 0x1

    return v0
.end method
