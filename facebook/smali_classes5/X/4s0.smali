.class public final LX/4s0;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final d:[LX/15z;


# instance fields
.field public a:LX/4s0;

.field public b:J

.field public final c:[Ljava/lang/Object;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 817266
    const/16 v0, 0x10

    new-array v0, v0, [LX/15z;

    sput-object v0, LX/4s0;->d:[LX/15z;

    .line 817267
    invoke-static {}, LX/15z;->values()[LX/15z;

    move-result-object v0

    .line 817268
    sget-object v1, LX/4s0;->d:[LX/15z;

    const/16 v2, 0xf

    array-length v3, v0

    add-int/lit8 v3, v3, -0x1

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-static {v0, v4, v1, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 817269
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 817270
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 817271
    const/16 v0, 0x10

    new-array v0, v0, [Ljava/lang/Object;

    iput-object v0, p0, LX/4s0;->c:[Ljava/lang/Object;

    .line 817272
    return-void
.end method

.method private b(ILX/15z;)V
    .locals 4

    .prologue
    .line 817261
    invoke-virtual {p2}, LX/15z;->ordinal()I

    move-result v0

    int-to-long v0, v0

    .line 817262
    if-lez p1, :cond_0

    .line 817263
    shl-int/lit8 v2, p1, 0x2

    shl-long/2addr v0, v2

    .line 817264
    :cond_0
    iget-wide v2, p0, LX/4s0;->b:J

    or-long/2addr v0, v2

    iput-wide v0, p0, LX/4s0;->b:J

    .line 817265
    return-void
.end method

.method private b(ILX/15z;Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 817273
    iget-object v0, p0, LX/4s0;->c:[Ljava/lang/Object;

    aput-object p3, v0, p1

    .line 817274
    invoke-virtual {p2}, LX/15z;->ordinal()I

    move-result v0

    int-to-long v0, v0

    .line 817275
    if-lez p1, :cond_0

    .line 817276
    shl-int/lit8 v2, p1, 0x2

    shl-long/2addr v0, v2

    .line 817277
    :cond_0
    iget-wide v2, p0, LX/4s0;->b:J

    or-long/2addr v0, v2

    iput-wide v0, p0, LX/4s0;->b:J

    .line 817278
    return-void
.end method


# virtual methods
.method public final a(I)LX/15z;
    .locals 3

    .prologue
    .line 817241
    iget-wide v0, p0, LX/4s0;->b:J

    .line 817242
    if-lez p1, :cond_0

    .line 817243
    shl-int/lit8 v2, p1, 0x2

    shr-long/2addr v0, v2

    .line 817244
    :cond_0
    long-to-int v0, v0

    and-int/lit8 v0, v0, 0xf

    .line 817245
    sget-object v1, LX/4s0;->d:[LX/15z;

    aget-object v0, v1, v0

    return-object v0
.end method

.method public final a(ILX/15z;)LX/4s0;
    .locals 2

    .prologue
    .line 817246
    const/16 v0, 0x10

    if-ge p1, v0, :cond_0

    .line 817247
    invoke-direct {p0, p1, p2}, LX/4s0;->b(ILX/15z;)V

    .line 817248
    const/4 v0, 0x0

    .line 817249
    :goto_0
    return-object v0

    .line 817250
    :cond_0
    new-instance v0, LX/4s0;

    invoke-direct {v0}, LX/4s0;-><init>()V

    iput-object v0, p0, LX/4s0;->a:LX/4s0;

    .line 817251
    iget-object v0, p0, LX/4s0;->a:LX/4s0;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p2}, LX/4s0;->b(ILX/15z;)V

    .line 817252
    iget-object v0, p0, LX/4s0;->a:LX/4s0;

    goto :goto_0
.end method

.method public final a(ILX/15z;Ljava/lang/Object;)LX/4s0;
    .locals 2

    .prologue
    .line 817254
    const/16 v0, 0x10

    if-ge p1, v0, :cond_0

    .line 817255
    invoke-direct {p0, p1, p2, p3}, LX/4s0;->b(ILX/15z;Ljava/lang/Object;)V

    .line 817256
    const/4 v0, 0x0

    .line 817257
    :goto_0
    return-object v0

    .line 817258
    :cond_0
    new-instance v0, LX/4s0;

    invoke-direct {v0}, LX/4s0;-><init>()V

    iput-object v0, p0, LX/4s0;->a:LX/4s0;

    .line 817259
    iget-object v0, p0, LX/4s0;->a:LX/4s0;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p2, p3}, LX/4s0;->b(ILX/15z;Ljava/lang/Object;)V

    .line 817260
    iget-object v0, p0, LX/4s0;->a:LX/4s0;

    goto :goto_0
.end method

.method public final b(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 817253
    iget-object v0, p0, LX/4s0;->c:[Ljava/lang/Object;

    aget-object v0, v0, p1

    return-object v0
.end method
