.class public final LX/4iL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/util/List;

.field public final synthetic b:Lorg/apache/http/HttpHost;

.field public final synthetic c:Z

.field public final synthetic d:LX/4iM;


# direct methods
.method public constructor <init>(LX/4iM;Ljava/util/List;Lorg/apache/http/HttpHost;Z)V
    .locals 0

    .prologue
    .line 802997
    iput-object p1, p0, LX/4iL;->d:LX/4iM;

    iput-object p2, p0, LX/4iL;->a:Ljava/util/List;

    iput-object p3, p0, LX/4iL;->b:Lorg/apache/http/HttpHost;

    iput-boolean p4, p0, LX/4iL;->c:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 802998
    const/4 v0, 0x1

    .line 802999
    iget-object v1, p0, LX/4iL;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    .line 803000
    :try_start_0
    iget-object v3, p0, LX/4iL;->b:Lorg/apache/http/HttpHost;

    if-eqz v3, :cond_1

    .line 803001
    iget-boolean v3, p0, LX/4iL;->c:Z

    if-eqz v3, :cond_0

    .line 803002
    iget-object v3, p0, LX/4iL;->d:LX/4iM;

    iget-object v3, v3, LX/4iM;->d:LX/4nf;

    iget-object v4, p0, LX/4iL;->b:Lorg/apache/http/HttpHost;

    invoke-virtual {v4}, Lorg/apache/http/HttpHost;->getHostName()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, LX/4iL;->b:Lorg/apache/http/HttpHost;

    invoke-virtual {v5}, Lorg/apache/http/HttpHost;->getPort()I

    move-result v5

    invoke-virtual {v3, v4, v5, v0}, LX/4nf;->a(Ljava/lang/String;ILandroid/webkit/WebView;)V

    move v0, v1

    :goto_1
    move v1, v0

    .line 803003
    goto :goto_0

    .line 803004
    :cond_0
    iget-object v3, p0, LX/4iL;->d:LX/4iM;

    iget-object v3, v3, LX/4iM;->d:LX/4nf;

    iget-object v4, p0, LX/4iL;->b:Lorg/apache/http/HttpHost;

    invoke-virtual {v4}, Lorg/apache/http/HttpHost;->getHostName()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, LX/4iL;->b:Lorg/apache/http/HttpHost;

    invoke-virtual {v5}, Lorg/apache/http/HttpHost;->getPort()I

    move-result v5

    invoke-virtual {v3, v4, v5, v0}, LX/4nf;->b(Ljava/lang/String;ILandroid/webkit/WebView;)V

    move v0, v1

    goto :goto_1

    .line 803005
    :cond_1
    iget-boolean v3, p0, LX/4iL;->c:Z

    if-eqz v3, :cond_2

    .line 803006
    iget-object v3, p0, LX/4iL;->d:LX/4iM;

    iget-object v3, v3, LX/4iM;->d:LX/4nf;

    .line 803007
    const-string v4, ""

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5, v0}, LX/4nf;->a(Ljava/lang/String;ILandroid/webkit/WebView;)V

    .line 803008
    move v0, v1

    goto :goto_1

    .line 803009
    :cond_2
    iget-object v3, p0, LX/4iL;->d:LX/4iM;

    iget-object v3, v3, LX/4iM;->d:LX/4nf;

    .line 803010
    const-string v4, ""

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5, v0}, LX/4nf;->b(Ljava/lang/String;ILandroid/webkit/WebView;)V
    :try_end_0
    .catch LX/4ne; {:try_start_0 .. :try_end_0} :catch_0

    .line 803011
    goto :goto_0

    .line 803012
    :catch_0
    move-exception v0

    .line 803013
    sget-object v1, LX/4iM;->a:Ljava/lang/Class;

    const-string v3, "Failed to set proxy for WebView"

    invoke-static {v1, v3, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 803014
    const/4 v1, 0x0

    move v0, v1

    goto :goto_1

    .line 803015
    :cond_3
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
