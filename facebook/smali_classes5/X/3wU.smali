.class public final LX/3wU;
.super LX/3vD;
.source ""


# instance fields
.field public final synthetic c:LX/3wb;

.field private d:LX/3vG;


# direct methods
.method public constructor <init>(LX/3wb;Landroid/content/Context;LX/3vG;)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 655305
    iput-object p1, p0, LX/3wU;->c:LX/3wb;

    .line 655306
    const/4 v3, 0x0

    const v5, 0x7f01000f

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    invoke-direct/range {v0 .. v5}, LX/3vD;-><init>(Landroid/content/Context;LX/3v0;Landroid/view/View;ZI)V

    .line 655307
    iput-object p3, p0, LX/3wU;->d:LX/3vG;

    .line 655308
    invoke-virtual {p3}, LX/3vG;->getItem()Landroid/view/MenuItem;

    move-result-object v0

    check-cast v0, LX/3v3;

    .line 655309
    invoke-virtual {v0}, LX/3v3;->j()Z

    move-result v0

    if-nez v0, :cond_0

    .line 655310
    iget-object v0, p1, LX/3wb;->i:Landroid/view/View;

    if-nez v0, :cond_2

    iget-object v0, p1, LX/3ut;->f:LX/3ux;

    check-cast v0, Landroid/view/View;

    .line 655311
    :goto_0
    iput-object v0, p0, LX/3vD;->k:Landroid/view/View;

    .line 655312
    :cond_0
    iget-object v0, p1, LX/3wb;->g:LX/3wZ;

    .line 655313
    iput-object v0, p0, LX/3vD;->n:LX/3uE;

    .line 655314
    invoke-virtual {p3}, LX/3v0;->size()I

    move-result v1

    move v0, v4

    .line 655315
    :goto_1
    if-ge v0, v1, :cond_1

    .line 655316
    invoke-virtual {p3, v0}, LX/3v0;->getItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 655317
    invoke-interface {v2}, Landroid/view/MenuItem;->isVisible()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 655318
    const/4 v4, 0x1

    .line 655319
    :cond_1
    iput-boolean v4, p0, LX/3vD;->b:Z

    .line 655320
    return-void

    .line 655321
    :cond_2
    iget-object v0, p1, LX/3wb;->i:Landroid/view/View;

    goto :goto_0

    .line 655322
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method


# virtual methods
.method public final onDismiss()V
    .locals 2

    .prologue
    .line 655323
    invoke-super {p0}, LX/3vD;->onDismiss()V

    .line 655324
    iget-object v0, p0, LX/3wU;->c:LX/3wb;

    const/4 v1, 0x0

    .line 655325
    iput-object v1, v0, LX/3wb;->w:LX/3wU;

    .line 655326
    iget-object v0, p0, LX/3wU;->c:LX/3wb;

    const/4 v1, 0x0

    iput v1, v0, LX/3wb;->h:I

    .line 655327
    return-void
.end method
