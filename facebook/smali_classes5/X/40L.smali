.class public final enum LX/40L;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/40L;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/40L;

.field public static final enum ANY:LX/40L;

.field public static final enum UI_THREAD:LX/40L;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 663135
    new-instance v0, LX/40L;

    const-string v1, "ANY"

    invoke-direct {v0, v1, v2}, LX/40L;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/40L;->ANY:LX/40L;

    new-instance v0, LX/40L;

    const-string v1, "UI_THREAD"

    invoke-direct {v0, v1, v3}, LX/40L;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/40L;->UI_THREAD:LX/40L;

    const/4 v0, 0x2

    new-array v0, v0, [LX/40L;

    sget-object v1, LX/40L;->ANY:LX/40L;

    aput-object v1, v0, v2

    sget-object v1, LX/40L;->UI_THREAD:LX/40L;

    aput-object v1, v0, v3

    sput-object v0, LX/40L;->$VALUES:[LX/40L;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 663136
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/40L;
    .locals 1

    .prologue
    .line 663137
    const-class v0, LX/40L;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/40L;

    return-object v0
.end method

.method public static values()[LX/40L;
    .locals 1

    .prologue
    .line 663138
    sget-object v0, LX/40L;->$VALUES:[LX/40L;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/40L;

    return-object v0
.end method
