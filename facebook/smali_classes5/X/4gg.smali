.class public LX/4gg;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 799969
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 799970
    return-void
.end method

.method public static a(Ljava/lang/String;)LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;",
            ">;"
        }
    .end annotation

    .prologue
    .line 799971
    invoke-static {p0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 799972
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 799973
    :goto_0
    return-object v0

    .line 799974
    :cond_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 799975
    :try_start_0
    invoke-static {}, LX/0lB;->i()LX/0lB;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 799976
    invoke-virtual {v0}, LX/0lF;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 799977
    invoke-static {v0}, LX/4gg;->a(LX/0lF;)Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    move-result-object v0

    .line 799978
    if-eqz v0, :cond_1

    .line 799979
    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 799980
    :catch_0
    const/4 v0, 0x0

    goto :goto_0

    .line 799981
    :cond_2
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;)LX/0m9;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 799982
    new-instance v2, LX/0m9;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v2, v0}, LX/0m9;-><init>(LX/0mC;)V

    .line 799983
    const-string v0, "id"

    iget-object v3, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->a:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 799984
    const-string v0, "action_title"

    iget-object v3, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->b:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 799985
    const-string v3, "action_url"

    iget-object v0, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->c:Landroid/net/Uri;

    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    invoke-virtual {v2, v3, v0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 799986
    const-string v3, "native_url"

    iget-object v0, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->d:Landroid/net/Uri;

    if-nez v0, :cond_1

    move-object v0, v1

    :goto_1
    invoke-virtual {v2, v3, v0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 799987
    const-string v0, "action_open_type"

    iget-object v3, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->e:LX/4ge;

    if-nez v3, :cond_2

    :goto_2
    invoke-virtual {v2, v0, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 799988
    const-string v0, "action_targets"

    iget-object v1, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->f:Ljava/lang/String;

    .line 799989
    new-instance v3, LX/162;

    sget-object v4, LX/0mC;->a:LX/0mC;

    invoke-direct {v3, v4}, LX/162;-><init>(LX/0mC;)V

    invoke-virtual {v3, v1}, LX/162;->g(Ljava/lang/String;)LX/162;

    move-result-object v3

    move-object v1, v3

    .line 799990
    invoke-virtual {v2, v0, v1}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 799991
    const-string v0, "is_mutable_by_server"

    iget-boolean v1, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->g:Z

    invoke-virtual {v2, v0, v1}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 799992
    const-string v0, "is_disabled"

    iget-boolean v1, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->h:Z

    invoke-virtual {v2, v0, v1}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 799993
    const-string v0, "user_confirmation"

    iget-object v1, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->k:Lcom/facebook/messaging/business/common/calltoaction/model/CTAUserConfirmation;

    .line 799994
    if-nez v1, :cond_3

    .line 799995
    const/4 v3, 0x0

    .line 799996
    :goto_3
    move-object v1, v3

    .line 799997
    invoke-virtual {v2, v0, v1}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 799998
    const-string v0, "height_ratio"

    iget-wide v4, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->i:D

    invoke-virtual {v2, v0, v4, v5}, LX/0m9;->a(Ljava/lang/String;D)LX/0m9;

    .line 799999
    const-string v0, "hide_share_button"

    iget-boolean v1, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->j:Z

    invoke-virtual {v2, v0, v1}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 800000
    const-string v0, "payment_metadata"

    iget-object v1, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->l:Lcom/facebook/messaging/business/common/calltoaction/model/CTAPaymentInfo;

    .line 800001
    if-nez v1, :cond_4

    .line 800002
    const/4 v3, 0x0

    .line 800003
    :goto_4
    move-object v1, v3

    .line 800004
    invoke-virtual {v2, v0, v1}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 800005
    const-string v0, "postback_ref_code"

    iget-object v1, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->m:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 800006
    return-object v2

    .line 800007
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->c:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 800008
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->d:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 800009
    :cond_2
    iget-object v1, p0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->e:LX/4ge;

    invoke-virtual {v1}, LX/4ge;->name()Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 800010
    :cond_3
    new-instance v3, LX/0m9;

    sget-object v4, LX/0mC;->a:LX/0mC;

    invoke-direct {v3, v4}, LX/0m9;-><init>(LX/0mC;)V

    .line 800011
    const-string v4, "confirmation_title"

    iget-object v5, v1, Lcom/facebook/messaging/business/common/calltoaction/model/CTAUserConfirmation;->a:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 800012
    const-string v4, "confirmation_message"

    iget-object v5, v1, Lcom/facebook/messaging/business/common/calltoaction/model/CTAUserConfirmation;->b:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 800013
    const-string v4, "continue_button_label"

    iget-object v5, v1, Lcom/facebook/messaging/business/common/calltoaction/model/CTAUserConfirmation;->c:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 800014
    const-string v4, "cancel_button_label"

    iget-object v5, v1, Lcom/facebook/messaging/business/common/calltoaction/model/CTAUserConfirmation;->d:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    goto :goto_3

    .line 800015
    :cond_4
    new-instance v3, LX/0m9;

    sget-object v4, LX/0mC;->a:LX/0mC;

    invoke-direct {v3, v4}, LX/0m9;-><init>(LX/0mC;)V

    .line 800016
    const-string v4, "total_price"

    iget-object v5, v1, Lcom/facebook/messaging/business/common/calltoaction/model/CTAPaymentInfo;->a:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 800017
    const-string v4, "payment_module_config"

    iget-object v5, v1, Lcom/facebook/messaging/business/common/calltoaction/model/CTAPaymentInfo;->b:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    goto :goto_4
.end method

.method public static a(LX/0lF;)Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;
    .locals 15
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 800018
    const-string v0, "id"

    invoke-virtual {p0, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    const-string v0, "action_title"

    invoke-virtual {p0, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v2

    const-string v0, "action_url"

    invoke-virtual {p0, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v3

    const-string v0, "native_url"

    invoke-virtual {p0, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v4

    const-string v0, "action_open_type"

    invoke-virtual {p0, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v5

    const-string v0, "action_targets"

    invoke-virtual {p0, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 800019
    invoke-virtual {v0}, LX/0lF;->e()I

    move-result v6

    if-lez v6, :cond_0

    const/4 v6, 0x0

    invoke-virtual {v0, v6}, LX/0lF;->a(I)LX/0lF;

    move-result-object v6

    invoke-virtual {v6}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v6

    :goto_0
    move-object v6, v6

    .line 800020
    const-string v0, "is_mutable_by_server"

    invoke-virtual {p0, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    const/4 v7, 0x0

    invoke-static {v0, v7}, LX/16N;->a(LX/0lF;Z)Z

    move-result v7

    const-string v0, "user_confirmation"

    invoke-virtual {p0, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 800021
    if-nez v0, :cond_1

    .line 800022
    const/4 v8, 0x0

    .line 800023
    :goto_1
    move-object v8, v8

    .line 800024
    const-string v0, "payment_metadata"

    invoke-virtual {p0, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 800025
    if-nez v0, :cond_2

    .line 800026
    const/4 v9, 0x0

    .line 800027
    :goto_2
    move-object v9, v9

    .line 800028
    const-string v0, "height_ratio"

    invoke-virtual {p0, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->e(LX/0lF;)D

    move-result-wide v10

    const-string v0, "hide_share_button"

    invoke-virtual {p0, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->g(LX/0lF;)Z

    move-result v12

    const-string v0, "postback_ref_code"

    invoke-virtual {p0, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v13

    const-string v0, "is_disabled"

    invoke-virtual {p0, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->g(LX/0lF;)Z

    move-result v14

    invoke-static/range {v1 .. v14}, LX/4gg;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/facebook/messaging/business/common/calltoaction/model/CTAUserConfirmation;Lcom/facebook/messaging/business/common/calltoaction/model/CTAPaymentInfo;DZLjava/lang/String;Z)Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v6, 0x0

    goto :goto_0

    :cond_1
    new-instance v8, LX/4gc;

    invoke-direct {v8}, LX/4gc;-><init>()V

    const-string v9, "confirmation_title"

    invoke-virtual {v0, v9}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v9

    invoke-static {v9}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v9

    .line 800029
    iput-object v9, v8, LX/4gc;->a:Ljava/lang/String;

    .line 800030
    move-object v8, v8

    .line 800031
    const-string v9, "confirmation_message"

    invoke-virtual {v0, v9}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v9

    invoke-static {v9}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v9

    .line 800032
    iput-object v9, v8, LX/4gc;->b:Ljava/lang/String;

    .line 800033
    move-object v8, v8

    .line 800034
    const-string v9, "continue_button_label"

    invoke-virtual {v0, v9}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v9

    invoke-static {v9}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v9

    .line 800035
    iput-object v9, v8, LX/4gc;->c:Ljava/lang/String;

    .line 800036
    move-object v8, v8

    .line 800037
    const-string v9, "cancel_button_label"

    invoke-virtual {v0, v9}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v9

    invoke-static {v9}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v9

    .line 800038
    iput-object v9, v8, LX/4gc;->d:Ljava/lang/String;

    .line 800039
    move-object v8, v8

    .line 800040
    invoke-virtual {v8}, LX/4gc;->e()Lcom/facebook/messaging/business/common/calltoaction/model/CTAUserConfirmation;

    move-result-object v8

    goto :goto_1

    :cond_2
    new-instance v9, LX/4ga;

    invoke-direct {v9}, LX/4ga;-><init>()V

    const-string v10, "total_price"

    invoke-virtual {v0, v10}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v10

    invoke-static {v10}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v10

    .line 800041
    iput-object v10, v9, LX/4ga;->a:Ljava/lang/String;

    .line 800042
    move-object v9, v9

    .line 800043
    const-string v10, "payment_module_config"

    invoke-virtual {v0, v10}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v10

    invoke-static {v10}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v10

    .line 800044
    iput-object v10, v9, LX/4ga;->b:Ljava/lang/String;

    .line 800045
    move-object v9, v9

    .line 800046
    invoke-virtual {v9}, LX/4ga;->a()Lcom/facebook/messaging/business/common/calltoaction/model/CTAPaymentInfo;

    move-result-object v9

    goto/16 :goto_2
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/facebook/messaging/business/common/calltoaction/model/CTAUserConfirmation;Lcom/facebook/messaging/business/common/calltoaction/model/CTAPaymentInfo;DZLjava/lang/String;Z)Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 800047
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 800048
    :cond_0
    :goto_0
    return-object v1

    .line 800049
    :cond_1
    invoke-static {p4}, LX/4ge;->fromDbValue(Ljava/lang/String;)LX/4ge;

    move-result-object v2

    .line 800050
    if-eqz v2, :cond_0

    .line 800051
    new-instance v1, LX/4gf;

    invoke-direct {v1}, LX/4gf;-><init>()V

    invoke-virtual {v1, p0}, LX/4gf;->a(Ljava/lang/String;)LX/4gf;

    move-result-object v1

    invoke-virtual {v1, p1}, LX/4gf;->b(Ljava/lang/String;)LX/4gf;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/4gf;->c(Ljava/lang/String;)LX/4gf;

    move-result-object v1

    invoke-virtual {v1, p3}, LX/4gf;->d(Ljava/lang/String;)LX/4gf;

    move-result-object v1

    invoke-virtual {v1, v2}, LX/4gf;->a(LX/4ge;)LX/4gf;

    move-result-object v1

    invoke-virtual {v1, p5}, LX/4gf;->e(Ljava/lang/String;)LX/4gf;

    move-result-object v1

    invoke-virtual {v1, p6}, LX/4gf;->a(Z)LX/4gf;

    move-result-object v1

    invoke-virtual {v1, p7}, LX/4gf;->a(Lcom/facebook/messaging/business/common/calltoaction/model/CTAUserConfirmation;)LX/4gf;

    move-result-object v1

    invoke-virtual {v1, p8}, LX/4gf;->a(Lcom/facebook/messaging/business/common/calltoaction/model/CTAPaymentInfo;)LX/4gf;

    move-result-object v1

    invoke-virtual {v1, p9, p10}, LX/4gf;->a(D)LX/4gf;

    move-result-object v1

    move/from16 v0, p11

    invoke-virtual {v1, v0}, LX/4gf;->c(Z)LX/4gf;

    move-result-object v1

    move-object/from16 v0, p12

    invoke-virtual {v1, v0}, LX/4gf;->f(Ljava/lang/String;)LX/4gf;

    move-result-object v1

    move/from16 v0, p13

    invoke-virtual {v1, v0}, LX/4gf;->b(Z)LX/4gf;

    move-result-object v1

    invoke-virtual {v1}, LX/4gf;->n()Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    move-result-object v1

    goto :goto_0
.end method

.method public static a(Ljava/util/List;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 800052
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 800053
    :cond_0
    const/4 v0, 0x0

    .line 800054
    :goto_0
    return-object v0

    .line 800055
    :cond_1
    new-instance v1, LX/162;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v1, v0}, LX/162;-><init>(LX/0mC;)V

    .line 800056
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    .line 800057
    invoke-static {v0}, LX/4gg;->a(Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;)LX/0m9;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/162;->a(LX/0lF;)LX/162;

    goto :goto_1

    .line 800058
    :cond_2
    invoke-virtual {v1}, LX/162;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
