.class public LX/3xs;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:[B

.field public b:J

.field public c:J

.field public d:J


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 660407
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 660408
    const/16 v0, 0x400

    new-array v0, v0, [B

    iput-object v0, p0, LX/3xs;->a:[B

    return-void
.end method

.method public static a([BI)J
    .locals 4

    .prologue
    const/16 v3, 0x39

    const/16 v2, 0x30

    .line 660409
    :goto_0
    array-length v0, p0

    if-ge p1, v0, :cond_2

    aget-byte v0, p0, p1

    const/16 v1, 0xa

    if-eq v0, v1, :cond_2

    .line 660410
    aget-byte v0, p0, p1

    if-lt v0, v2, :cond_1

    aget-byte v0, p0, p1

    if-gt v0, v3, :cond_1

    .line 660411
    add-int/lit8 v0, p1, 0x1

    .line 660412
    :goto_1
    array-length v1, p0

    if-ge v0, v1, :cond_0

    aget-byte v1, p0, v0

    if-lt v1, v2, :cond_0

    aget-byte v1, p0, v0

    if-gt v1, v3, :cond_0

    .line 660413
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 660414
    :cond_0
    new-instance v1, Ljava/lang/String;

    const/4 v2, 0x0

    sub-int/2addr v0, p1

    invoke-direct {v1, p0, v2, p1, v0}, Ljava/lang/String;-><init>([BIII)V

    .line 660415
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    int-to-long v0, v0

    const-wide/16 v2, 0x400

    mul-long/2addr v0, v2

    .line 660416
    :goto_2
    return-wide v0

    .line 660417
    :cond_1
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    .line 660418
    :cond_2
    const-wide/16 v0, 0x0

    goto :goto_2
.end method

.method public static a([BILjava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 660419
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    .line 660420
    add-int v1, p1, v2

    array-length v3, p0

    if-lt v1, v3, :cond_1

    .line 660421
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v1, v0

    .line 660422
    :goto_1
    if-ge v1, v2, :cond_2

    .line 660423
    add-int v3, p1, v1

    aget-byte v3, p0, v3

    invoke-virtual {p2, v1}, Ljava/lang/String;->charAt(I)C

    move-result v4

    if-ne v3, v4, :cond_0

    .line 660424
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 660425
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method
