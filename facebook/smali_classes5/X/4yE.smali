.class public final LX/4yE;
.super LX/4y5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/4y5",
        "<TK;",
        "LX/0Rf",
        "<TV;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:LX/0P1;


# direct methods
.method public constructor <init>(LX/0P1;)V
    .locals 0

    .prologue
    .line 821667
    iput-object p1, p0, LX/4yE;->this$0:LX/0P1;

    invoke-direct {p0}, LX/4y5;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(LX/0P1;B)V
    .locals 0

    .prologue
    .line 821666
    invoke-direct {p0, p1}, LX/4yE;-><init>(LX/0P1;)V

    return-void
.end method

.method private a(Ljava/lang/Object;)LX/0Rf;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "LX/0Rf",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 821664
    iget-object v0, p0, LX/4yE;->this$0:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 821665
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/0Rc;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rc",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;",
            "LX/0Rf",
            "<TV;>;>;>;"
        }
    .end annotation

    .prologue
    .line 821662
    iget-object v0, p0, LX/4yE;->this$0:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Py;->iterator()LX/0Rc;

    move-result-object v0

    .line 821663
    new-instance v1, LX/4yD;

    invoke-direct {v1, p0, v0}, LX/4yD;-><init>(LX/4yE;Ljava/util/Iterator;)V

    return-object v1
.end method

.method public final containsKey(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 821654
    iget-object v0, p0, LX/4yE;->this$0:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final synthetic get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 821661
    invoke-direct {p0, p1}, LX/4yE;->a(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 821660
    iget-object v0, p0, LX/4yE;->this$0:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->hashCode()I

    move-result v0

    return v0
.end method

.method public final isHashCodeFast()Z
    .locals 1

    .prologue
    .line 821659
    iget-object v0, p0, LX/4yE;->this$0:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->isHashCodeFast()Z

    move-result v0

    return v0
.end method

.method public final isPartialView()Z
    .locals 1

    .prologue
    .line 821658
    iget-object v0, p0, LX/4yE;->this$0:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->isPartialView()Z

    move-result v0

    return v0
.end method

.method public final keySet()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 821657
    iget-object v0, p0, LX/4yE;->this$0:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic keySet()Ljava/util/Set;
    .locals 1

    .prologue
    .line 821656
    invoke-virtual {p0}, LX/4yE;->keySet()LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 821655
    iget-object v0, p0, LX/4yE;->this$0:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->size()I

    move-result v0

    return v0
.end method
