.class public LX/4cG;
.super Ljava/io/FilterOutputStream;
.source ""


# instance fields
.field private final a:LX/1iX;


# direct methods
.method public constructor <init>(Ljava/io/OutputStream;LX/1iX;)V
    .locals 1

    .prologue
    .line 794799
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/OutputStream;

    invoke-direct {p0, v0}, Ljava/io/FilterOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 794800
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1iX;

    iput-object v0, p0, LX/4cG;->a:LX/1iX;

    .line 794801
    return-void
.end method


# virtual methods
.method public final write(I)V
    .locals 4

    .prologue
    .line 794805
    iget-object v0, p0, Ljava/io/FilterOutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write(I)V

    .line 794806
    iget-object v0, p0, LX/4cG;->a:LX/1iX;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, LX/1iX;->b(J)V

    .line 794807
    return-void
.end method

.method public final write([BII)V
    .locals 4

    .prologue
    .line 794802
    iget-object v0, p0, Ljava/io/FilterOutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V

    .line 794803
    iget-object v0, p0, LX/4cG;->a:LX/1iX;

    int-to-long v2, p3

    invoke-virtual {v0, v2, v3}, LX/1iX;->b(J)V

    .line 794804
    return-void
.end method
