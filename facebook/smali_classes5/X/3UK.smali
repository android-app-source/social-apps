.class public LX/3UK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Tl;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/3Tl",
        "<",
        "Lcom/facebook/friends/model/PersonYouMayKnow;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/0xW;

.field public final b:LX/17W;

.field public final c:LX/2iS;

.field private final d:Landroid/content/res/Resources;

.field public final e:LX/3Tg;

.field public final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/friends/model/PersonYouMayKnow;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/3UL;

.field public i:Z


# direct methods
.method public constructor <init>(LX/0xW;LX/17W;LX/2iO;Landroid/content/res/Resources;LX/3Tg;)V
    .locals 2
    .param p5    # LX/3Tg;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 585799
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 585800
    sget-object v0, LX/3UL;->LOADING:LX/3UL;

    iput-object v0, p0, LX/3UK;->h:LX/3UL;

    .line 585801
    iput-object p1, p0, LX/3UK;->a:LX/0xW;

    .line 585802
    iput-object p2, p0, LX/3UK;->b:LX/17W;

    .line 585803
    sget-object v0, LX/2h7;->JEWEL:LX/2h7;

    invoke-virtual {p3, v0}, LX/2iO;->a(LX/2h7;)LX/2iS;

    move-result-object v0

    iput-object v0, p0, LX/3UK;->c:LX/2iS;

    .line 585804
    iput-object p5, p0, LX/3UK;->e:LX/3Tg;

    .line 585805
    iput-object p4, p0, LX/3UK;->d:Landroid/content/res/Resources;

    .line 585806
    iget-object v0, p0, LX/3UK;->c:LX/2iS;

    new-instance v1, LX/3UM;

    invoke-direct {v1, p0}, LX/3UM;-><init>(LX/3UK;)V

    .line 585807
    iput-object v1, v0, LX/2iS;->n:LX/2ia;

    .line 585808
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/3UK;->f:Ljava/util/List;

    .line 585809
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/3UK;->g:Ljava/util/Set;

    .line 585810
    return-void
.end method

.method public static c(LX/3UK;J)I
    .locals 5

    .prologue
    .line 585794
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/3UK;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 585795
    iget-object v0, p0, LX/3UK;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friends/model/PersonYouMayKnow;

    invoke-virtual {v0}, Lcom/facebook/friends/model/PersonYouMayKnow;->a()J

    move-result-wide v2

    cmp-long v0, v2, p1

    if-nez v0, :cond_0

    .line 585796
    :goto_1
    return v1

    .line 585797
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 585798
    :cond_1
    const/4 v1, -0x1

    goto :goto_1
.end method

.method private c(I)Lcom/facebook/friends/model/PersonYouMayKnow;
    .locals 1

    .prologue
    .line 585793
    iget-object v0, p0, LX/3UK;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friends/model/PersonYouMayKnow;

    return-object v0
.end method

.method private d(I)LX/3cW;
    .locals 2

    .prologue
    .line 585789
    iget-object v0, p0, LX/3UK;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friends/model/PersonYouMayKnow;

    invoke-virtual {v0}, Lcom/facebook/friends/model/PersonYouMayKnow;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    .line 585790
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v1, v0}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->INCOMING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v1, v0}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 585791
    :cond_0
    sget-object v0, LX/3cW;->PERSON_YOU_MAY_KNOW:LX/3cW;

    .line 585792
    :goto_0
    return-object v0

    :cond_1
    sget-object v0, LX/3cW;->RESPONDED_PERSON_YOU_MAY_KNOW:LX/3cW;

    goto :goto_0
.end method


# virtual methods
.method public final a(I)I
    .locals 1

    .prologue
    .line 585788
    invoke-direct {p0, p1}, LX/3UK;->d(I)LX/3cW;

    move-result-object v0

    invoke-virtual {v0}, LX/3cW;->ordinal()I

    move-result v0

    return v0
.end method

.method public final a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 585777
    sget-object v0, LX/Iw5;->a:[I

    invoke-direct {p0, p1}, LX/3UK;->d(I)LX/3cW;

    move-result-object v1

    invoke-virtual {v1}, LX/3cW;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 585778
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unexpected child view type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 585779
    :pswitch_0
    if-nez p2, :cond_0

    .line 585780
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0309a4

    const/4 p2, 0x0

    invoke-virtual {v0, v1, p3, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/friending/common/list/FriendRequestItemView;

    move-object p2, v0

    .line 585781
    :goto_0
    iget-object v0, p0, LX/3UK;->c:LX/2iS;

    invoke-direct {p0, p1}, LX/3UK;->c(I)Lcom/facebook/friends/model/PersonYouMayKnow;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, LX/2iS;->a(Lcom/facebook/fbui/widget/contentview/ContentView;Lcom/facebook/friends/model/PersonYouMayKnow;)V

    .line 585782
    :goto_1
    return-object p2

    .line 585783
    :cond_0
    check-cast p2, Lcom/facebook/friending/common/list/FriendRequestItemView;

    goto :goto_0

    .line 585784
    :pswitch_1
    if-nez p2, :cond_1

    .line 585785
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0309a3

    const/4 p2, 0x0

    invoke-virtual {v0, v1, p3, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/friending/common/list/FriendListItemView;

    move-object p2, v0

    .line 585786
    :goto_2
    iget-object v0, p0, LX/3UK;->c:LX/2iS;

    invoke-direct {p0, p1}, LX/3UK;->c(I)Lcom/facebook/friends/model/PersonYouMayKnow;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, LX/2iS;->a(Lcom/facebook/fbui/widget/contentview/ContentView;Lcom/facebook/friends/model/PersonYouMayKnow;)V

    goto :goto_1

    .line 585787
    :cond_1
    check-cast p2, Lcom/facebook/friending/common/list/FriendListItemView;

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(J)V
    .locals 3

    .prologue
    .line 585770
    iget-object v0, p0, LX/3UK;->g:Ljava/util/Set;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 585771
    :cond_0
    :goto_0
    return-void

    .line 585772
    :cond_1
    invoke-static {p0, p1, p2}, LX/3UK;->c(LX/3UK;J)I

    move-result v0

    .line 585773
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 585774
    iget-object v1, p0, LX/3UK;->f:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 585775
    iget-object v0, p0, LX/3UK;->g:Ljava/util/Set;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 585776
    iget-object v0, p0, LX/3UK;->e:LX/3Tg;

    invoke-interface {v0}, LX/3Tg;->e()V

    goto :goto_0
.end method

.method public final synthetic b(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 585769
    invoke-direct {p0, p1}, LX/3UK;->c(I)Lcom/facebook/friends/model/PersonYouMayKnow;

    move-result-object v0

    return-object v0
.end method

.method public final b(J)Z
    .locals 3

    .prologue
    .line 585768
    iget-object v0, p0, LX/3UK;->g:Ljava/util/Set;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 585755
    iget-object v0, p0, LX/3UK;->d:Landroid/content/res/Resources;

    const v1, 0x7f080f77

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 585765
    iget-boolean v0, p0, LX/3UK;->i:Z

    if-nez v0, :cond_1

    .line 585766
    iget-object v0, p0, LX/3UK;->a:LX/0xW;

    invoke-virtual {v0}, LX/0xW;->q()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/3UK;->a:LX/0xW;

    invoke-virtual {v0}, LX/0xW;->r()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 585767
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Ljava/lang/String;
    .locals 2

    .prologue
    .line 585764
    iget-object v0, p0, LX/3UK;->d:Landroid/content/res/Resources;

    const v1, 0x7f080faa

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 585763
    new-instance v0, LX/Iw4;

    invoke-direct {v0, p0}, LX/Iw4;-><init>(LX/3UK;)V

    return-object v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 585762
    iget-object v0, p0, LX/3UK;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 585761
    const/4 v0, 0x1

    return v0
.end method

.method public final i()I
    .locals 1

    .prologue
    .line 585760
    invoke-static {}, LX/3cW;->values()[LX/3cW;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 585759
    const/4 v0, 0x0

    return v0
.end method

.method public final k()LX/3UN;
    .locals 1

    .prologue
    .line 585758
    sget-object v0, LX/3UN;->SEE_ALL_FOOTER:LX/3UN;

    return-object v0
.end method

.method public final l()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 585757
    const/4 v0, 0x0

    return-object v0
.end method

.method public final m()Z
    .locals 2

    .prologue
    .line 585756
    iget-object v0, p0, LX/3UK;->h:LX/3UL;

    sget-object v1, LX/3UL;->LOADING:LX/3UL;

    invoke-virtual {v0, v1}, LX/3UL;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
