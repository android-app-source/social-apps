.class public LX/3eN;
.super LX/0ro;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0ro",
        "<",
        "Lcom/facebook/stickers/service/FetchTaggedStickersParams;",
        "Lcom/facebook/stickers/service/FetchTaggedStickersResult;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/3eN;


# instance fields
.field private b:LX/3eL;


# direct methods
.method public constructor <init>(LX/0sO;LX/3eL;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 619725
    invoke-direct {p0, p1}, LX/0ro;-><init>(LX/0sO;)V

    .line 619726
    iput-object p2, p0, LX/3eN;->b:LX/3eL;

    .line 619727
    return-void
.end method

.method public static a(LX/0QB;)LX/3eN;
    .locals 5

    .prologue
    .line 619637
    sget-object v0, LX/3eN;->c:LX/3eN;

    if-nez v0, :cond_1

    .line 619638
    const-class v1, LX/3eN;

    monitor-enter v1

    .line 619639
    :try_start_0
    sget-object v0, LX/3eN;->c:LX/3eN;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 619640
    if-eqz v2, :cond_0

    .line 619641
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 619642
    new-instance p0, LX/3eN;

    invoke-static {v0}, LX/0sO;->a(LX/0QB;)LX/0sO;

    move-result-object v3

    check-cast v3, LX/0sO;

    invoke-static {v0}, LX/3eL;->a(LX/0QB;)LX/3eL;

    move-result-object v4

    check-cast v4, LX/3eL;

    invoke-direct {p0, v3, v4}, LX/3eN;-><init>(LX/0sO;LX/3eL;)V

    .line 619643
    move-object v0, p0

    .line 619644
    sput-object v0, LX/3eN;->c:LX/3eN;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 619645
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 619646
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 619647
    :cond_1
    sget-object v0, LX/3eN;->c:LX/3eN;

    return-object v0

    .line 619648
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 619649
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/1pN;LX/15w;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 619668
    check-cast p1, Lcom/facebook/stickers/service/FetchTaggedStickersParams;

    .line 619669
    sget-object v0, LX/8m9;->a:[I

    .line 619670
    iget-object v1, p1, Lcom/facebook/stickers/service/FetchTaggedStickersParams;->b:LX/8mB;

    move-object v1, v1

    .line 619671
    invoke-virtual {v1}, LX/8mB;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 619672
    const/4 v1, 0x0

    .line 619673
    iget-object v0, p1, Lcom/facebook/stickers/service/FetchTaggedStickersParams;->a:LX/0Px;

    move-object v4, v0

    .line 619674
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 619675
    iget-object v0, p0, LX/0ro;->a:LX/0sO;

    const-class v2, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchTaggedStickersWithPreviewsQueryModel;

    invoke-static {v2}, LX/0w5;->b(Ljava/lang/Class;)LX/0w5;

    move-result-object v2

    invoke-virtual {v0, v2, p3}, LX/0sO;->a(LX/0w5;LX/15w;)Ljava/util/List;

    move-result-object v6

    .line 619676
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    .line 619677
    iget-object v2, p1, Lcom/facebook/stickers/service/FetchTaggedStickersParams;->a:LX/0Px;

    move-object v2, v2

    .line 619678
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    move v2, v1

    .line 619679
    :goto_1
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 619680
    new-instance v7, LX/0Pz;

    invoke-direct {v7}, LX/0Pz;-><init>()V

    .line 619681
    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchTaggedStickersWithPreviewsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchTaggedStickersWithPreviewsQueryModel;->a()Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchTaggedStickersWithPreviewsQueryModel$TaggedStickersModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchTaggedStickersWithPreviewsQueryModel$TaggedStickersModel;->a()LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result p2

    move v3, v1

    .line 619682
    :goto_2
    if-ge v3, p2, :cond_1

    invoke-virtual {v8, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerFieldsWithPreviewModel;

    .line 619683
    invoke-static {v0}, LX/3eL;->a(Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerFieldsWithPreviewModel;)Lcom/facebook/stickers/model/Sticker;

    move-result-object v0

    invoke-virtual {v7, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 619684
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    :cond_0
    move v0, v1

    .line 619685
    goto :goto_0

    .line 619686
    :cond_1
    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    invoke-interface {v5, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 619687
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 619688
    :cond_2
    move-object v0, v5

    .line 619689
    :goto_3
    new-instance v1, Lcom/facebook/stickers/service/FetchTaggedStickersResult;

    invoke-direct {v1, v0}, Lcom/facebook/stickers/service/FetchTaggedStickersResult;-><init>(Ljava/util/Map;)V

    return-object v1

    .line 619690
    :pswitch_0
    const/4 v1, 0x0

    .line 619691
    iget-object v0, p1, Lcom/facebook/stickers/service/FetchTaggedStickersParams;->a:LX/0Px;

    move-object v4, v0

    .line 619692
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 619693
    iget-object v0, p0, LX/0ro;->a:LX/0sO;

    const-class v2, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchAvailableTaggedStickersWithPreviewsQueryModel;

    invoke-static {v2}, LX/0w5;->b(Ljava/lang/Class;)LX/0w5;

    move-result-object v2

    invoke-virtual {v0, v2, p3}, LX/0sO;->a(LX/0w5;LX/15w;)Ljava/util/List;

    move-result-object v6

    .line 619694
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    .line 619695
    iget-object v2, p1, Lcom/facebook/stickers/service/FetchTaggedStickersParams;->a:LX/0Px;

    move-object v2, v2

    .line 619696
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-ne v0, v2, :cond_3

    const/4 v0, 0x1

    :goto_4
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    move v2, v1

    .line 619697
    :goto_5
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_5

    .line 619698
    new-instance v7, LX/0Pz;

    invoke-direct {v7}, LX/0Pz;-><init>()V

    .line 619699
    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchAvailableTaggedStickersWithPreviewsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchAvailableTaggedStickersWithPreviewsQueryModel;->a()Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchAvailableTaggedStickersWithPreviewsQueryModel$TaggedStickersModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchAvailableTaggedStickersWithPreviewsQueryModel$TaggedStickersModel;->a()LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result p2

    move v3, v1

    .line 619700
    :goto_6
    if-ge v3, p2, :cond_4

    invoke-virtual {v8, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerFieldsWithPreviewModel;

    .line 619701
    invoke-static {v0}, LX/3eL;->a(Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerFieldsWithPreviewModel;)Lcom/facebook/stickers/model/Sticker;

    move-result-object v0

    invoke-virtual {v7, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 619702
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_6

    :cond_3
    move v0, v1

    .line 619703
    goto :goto_4

    .line 619704
    :cond_4
    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    invoke-interface {v5, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 619705
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_5

    .line 619706
    :cond_5
    move-object v0, v5

    .line 619707
    goto :goto_3

    .line 619708
    :pswitch_1
    const/4 v1, 0x0

    .line 619709
    iget-object v0, p1, Lcom/facebook/stickers/service/FetchTaggedStickersParams;->a:LX/0Px;

    move-object v4, v0

    .line 619710
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 619711
    iget-object v0, p0, LX/0ro;->a:LX/0sO;

    const-class v2, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchTrayTaggedStickersWithPreviewsQueryModel;

    invoke-static {v2}, LX/0w5;->b(Ljava/lang/Class;)LX/0w5;

    move-result-object v2

    invoke-virtual {v0, v2, p3}, LX/0sO;->a(LX/0w5;LX/15w;)Ljava/util/List;

    move-result-object v6

    .line 619712
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    .line 619713
    iget-object v2, p1, Lcom/facebook/stickers/service/FetchTaggedStickersParams;->a:LX/0Px;

    move-object v2, v2

    .line 619714
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-ne v0, v2, :cond_6

    const/4 v0, 0x1

    :goto_7
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    move v2, v1

    .line 619715
    :goto_8
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_8

    .line 619716
    new-instance v7, LX/0Pz;

    invoke-direct {v7}, LX/0Pz;-><init>()V

    .line 619717
    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchTrayTaggedStickersWithPreviewsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchTrayTaggedStickersWithPreviewsQueryModel;->a()Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchTrayTaggedStickersWithPreviewsQueryModel$TaggedStickersModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$FetchTrayTaggedStickersWithPreviewsQueryModel$TaggedStickersModel;->a()LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result p2

    move v3, v1

    :goto_9
    if-ge v3, p2, :cond_7

    invoke-virtual {v8, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerFieldsWithPreviewModel;

    .line 619718
    invoke-static {v0}, LX/3eL;->a(Lcom/facebook/stickers/graphql/FetchStickersGraphQLModels$StickerFieldsWithPreviewModel;)Lcom/facebook/stickers/model/Sticker;

    move-result-object v0

    invoke-virtual {v7, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 619719
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_9

    :cond_6
    move v0, v1

    .line 619720
    goto :goto_7

    .line 619721
    :cond_7
    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    invoke-interface {v5, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 619722
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_8

    .line 619723
    :cond_8
    move-object v0, v5

    .line 619724
    goto/16 :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b(Ljava/lang/Object;LX/1pN;)I
    .locals 1

    .prologue
    .line 619667
    const/4 v0, 0x0

    return v0
.end method

.method public final f(Ljava/lang/Object;)LX/0gW;
    .locals 3

    .prologue
    .line 619650
    check-cast p1, Lcom/facebook/stickers/service/FetchTaggedStickersParams;

    .line 619651
    iget-object v0, p0, LX/3eN;->b:LX/3eL;

    .line 619652
    sget-object v1, LX/8m6;->a:[I

    .line 619653
    iget-object v2, p1, Lcom/facebook/stickers/service/FetchTaggedStickersParams;->b:LX/8mB;

    move-object v2, v2

    .line 619654
    invoke-virtual {v2}, LX/8mB;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 619655
    new-instance v1, LX/8jp;

    invoke-direct {v1}, LX/8jp;-><init>()V

    move-object v1, v1

    .line 619656
    :goto_0
    invoke-static {v0, v1}, LX/3eL;->a(LX/3eL;LX/0gW;)V

    .line 619657
    move-object v0, v1

    .line 619658
    const-string v1, "sticker_tag_ids"

    .line 619659
    iget-object v2, p1, Lcom/facebook/stickers/service/FetchTaggedStickersParams;->a:LX/0Px;

    move-object v2, v2

    .line 619660
    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/util/List;)LX/0gW;

    move-result-object v0

    const-string v1, "interface"

    .line 619661
    iget-object v2, p1, Lcom/facebook/stickers/service/FetchTaggedStickersParams;->c:LX/4m4;

    move-object v2, v2

    .line 619662
    invoke-static {v2}, LX/8jc;->a(LX/4m4;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    return-object v0

    .line 619663
    :pswitch_0
    new-instance v1, LX/8jf;

    invoke-direct {v1}, LX/8jf;-><init>()V

    move-object v1, v1

    .line 619664
    goto :goto_0

    .line 619665
    :pswitch_1
    new-instance v1, LX/8jq;

    invoke-direct {v1}, LX/8jq;-><init>()V

    move-object v1, v1

    .line 619666
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
