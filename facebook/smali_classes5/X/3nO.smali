.class public LX/3nO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/26t;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0qV;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0qV;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 638533
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 638534
    iput-object p1, p0, LX/3nO;->a:LX/0Ot;

    .line 638535
    return-void
.end method

.method public static a$redex0(LX/3nO;Ljava/lang/String;LX/1qK;Lcom/facebook/fbservice/service/OperationResult;)V
    .locals 6

    .prologue
    .line 638536
    iget-boolean v0, p3, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    move v0, v0

    .line 638537
    if-nez v0, :cond_1

    .line 638538
    :cond_0
    :goto_0
    return-void

    .line 638539
    :cond_1
    iget-object v0, p0, LX/3nO;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0qV;

    .line 638540
    invoke-static {p1}, LX/3nN;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 638541
    invoke-virtual {p3}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/feed/FetchFeedResult;

    .line 638542
    if-eqz v1, :cond_0

    .line 638543
    iget-object v2, v1, Lcom/facebook/api/feed/FetchFeedResult;->b:Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    move-object v2, v2

    .line 638544
    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/facebook/api/feed/FetchFeedResult;->b()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 638545
    iget-object v2, p2, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v2, v2

    .line 638546
    const-string v3, "fetchFeedParams"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/facebook/api/feed/FetchFeedParams;

    .line 638547
    iget-object v3, v2, Lcom/facebook/api/feed/FetchFeedParams;->h:Lcom/facebook/api/feed/FeedFetchContext;

    move-object v3, v3

    .line 638548
    invoke-virtual {v1}, Lcom/facebook/api/feed/FetchFeedResult;->b()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v5, :cond_0

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 638549
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, LX/0qV;->a(Lcom/facebook/graphql/model/FeedUnit;Lcom/facebook/api/feed/FeedFetchContext;)V

    .line 638550
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 638551
    :cond_2
    const-string v1, "feed_fetch_followup_feed_unit"

    if-ne p1, v1, :cond_3

    .line 638552
    invoke-virtual {p3}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/FeedUnit;

    .line 638553
    if-eqz v1, :cond_0

    .line 638554
    sget-object v2, Lcom/facebook/api/feed/FeedFetchContext;->a:Lcom/facebook/api/feed/FeedFetchContext;

    invoke-virtual {v0, v1, v2}, LX/0qV;->a(Lcom/facebook/graphql/model/FeedUnit;Lcom/facebook/api/feed/FeedFetchContext;)V

    goto :goto_0

    .line 638555
    :cond_3
    const-string v1, "feed_fetch_paginated_related_story"

    if-ne p1, v1, :cond_0

    .line 638556
    invoke-virtual {p3}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStorySet;

    .line 638557
    if-eqz v1, :cond_0

    .line 638558
    sget-object v2, Lcom/facebook/api/feed/FeedFetchContext;->a:Lcom/facebook/api/feed/FeedFetchContext;

    invoke-virtual {v0, v1, v2}, LX/0qV;->a(Lcom/facebook/graphql/model/FeedUnit;Lcom/facebook/api/feed/FeedFetchContext;)V

    goto :goto_0
.end method


# virtual methods
.method public final handleOperation(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 638559
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v1, v0

    .line 638560
    const/4 v0, 0x0

    .line 638561
    invoke-static {v1}, LX/3nN;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 638562
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 638563
    const-string v2, "fetchFeedParams"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/feed/FetchFeedParams;

    .line 638564
    iget-boolean v2, v0, Lcom/facebook/api/feed/FetchFeedParams;->r:Z

    move v0, v2

    .line 638565
    :cond_0
    if-nez v0, :cond_1

    move-object v0, p1

    :goto_0
    invoke-interface {p2, v0}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 638566
    invoke-static {p0, v1, p1, v0}, LX/3nO;->a$redex0(LX/3nO;Ljava/lang/String;LX/1qK;Lcom/facebook/fbservice/service/OperationResult;)V

    .line 638567
    return-object v0

    .line 638568
    :cond_1
    new-instance v0, LX/AmZ;

    invoke-direct {v0, p0, v1, p1}, LX/AmZ;-><init>(LX/3nO;Ljava/lang/String;LX/1qK;)V

    invoke-virtual {p1, v0}, LX/1qK;->chain(LX/4B6;)LX/1qK;

    move-result-object v0

    goto :goto_0
.end method
