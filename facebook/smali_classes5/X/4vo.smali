.class public LX/4vo;
.super LX/2wH;
.source ""

# interfaces
.implements LX/3KI;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2wH",
        "<",
        "LX/4vl;",
        ">;",
        "LX/3KI;"
    }
.end annotation


# instance fields
.field private final d:Z

.field public final e:LX/2wA;

.field private final f:Landroid/os/Bundle;

.field public g:Ljava/lang/Integer;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;ZLX/2wA;LX/1qf;LX/1qg;)V
    .locals 8

    invoke-static {p4}, LX/4vo;->a(LX/2wA;)Landroid/os/Bundle;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, LX/4vo;-><init>(Landroid/content/Context;Landroid/os/Looper;ZLX/2wA;Landroid/os/Bundle;LX/1qf;LX/1qg;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;ZLX/2wA;Landroid/os/Bundle;LX/1qf;LX/1qg;)V
    .locals 7

    const/16 v3, 0x2c

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p4

    move-object v5, p6

    move-object v6, p7

    invoke-direct/range {v0 .. v6}, LX/2wH;-><init>(Landroid/content/Context;Landroid/os/Looper;ILX/2wA;LX/1qf;LX/1qg;)V

    iput-boolean p3, p0, LX/4vo;->d:Z

    iput-object p4, p0, LX/4vo;->e:LX/2wA;

    iput-object p5, p0, LX/4vo;->f:Landroid/os/Bundle;

    iget-object v0, p4, LX/2wA;->j:Ljava/lang/Integer;

    move-object v0, v0

    iput-object v0, p0, LX/4vo;->g:Ljava/lang/Integer;

    return-void
.end method

.method private static a(LX/2wA;)Landroid/os/Bundle;
    .locals 6

    iget-object v0, p0, LX/2wA;->i:LX/2w4;

    move-object v0, v0

    iget-object v1, p0, LX/2wA;->j:Ljava/lang/Integer;

    move-object v1, v1

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v3, "com.google.android.gms.signin.internal.clientRequestedAccount"

    iget-object v4, p0, LX/2wA;->a:Landroid/accounts/Account;

    move-object v4, v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    if-eqz v1, :cond_0

    const-string v3, "com.google.android.gms.common.internal.ClientSettings.sessionId"

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_0
    if-eqz v0, :cond_2

    const-string v1, "com.google.android.gms.signin.internal.offlineAccessRequested"

    iget-boolean v3, v0, LX/2w4;->b:Z

    move v3, v3

    invoke-virtual {v2, v1, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v1, "com.google.android.gms.signin.internal.idTokenRequested"

    iget-boolean v3, v0, LX/2w4;->c:Z

    move v3, v3

    invoke-virtual {v2, v1, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v1, "com.google.android.gms.signin.internal.serverClientId"

    iget-object v3, v0, LX/2w4;->d:Ljava/lang/String;

    move-object v3, v3

    invoke-virtual {v2, v1, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "com.google.android.gms.signin.internal.usePromptModeForAuthCode"

    const/4 v3, 0x1

    invoke-virtual {v2, v1, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v1, "com.google.android.gms.signin.internal.forceCodeForRefreshToken"

    iget-boolean v3, v0, LX/2w4;->e:Z

    move v3, v3

    invoke-virtual {v2, v1, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v1, "com.google.android.gms.signin.internal.hostedDomain"

    iget-object v3, v0, LX/2w4;->f:Ljava/lang/String;

    move-object v3, v3

    invoke-virtual {v2, v1, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "com.google.android.gms.signin.internal.waitForAccessTokenRefresh"

    iget-boolean v3, v0, LX/2w4;->g:Z

    move v3, v3

    invoke-virtual {v2, v1, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v1, v0, LX/2w4;->h:Ljava/lang/Long;

    move-object v1, v1

    if-eqz v1, :cond_1

    const-string v1, "com.google.android.gms.signin.internal.authApiSignInModuleVersion"

    iget-object v3, v0, LX/2w4;->h:Ljava/lang/Long;

    move-object v3, v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v2, v1, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    :cond_1
    iget-object v1, v0, LX/2w4;->i:Ljava/lang/Long;

    move-object v1, v1

    if-eqz v1, :cond_2

    const-string v1, "com.google.android.gms.signin.internal.realClientLibraryVersion"

    iget-object v3, v0, LX/2w4;->i:Ljava/lang/Long;

    move-object v0, v3

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v2, v1, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    :cond_2
    return-object v2
.end method


# virtual methods
.method public final a(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    move-object v0, v0

    return-object v0

    :cond_0
    const-string v0, "com.google.android.gms.signin.internal.ISignInService"

    invoke-interface {p1, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    if-eqz v0, :cond_1

    instance-of p0, v0, LX/4vl;

    if-eqz p0, :cond_1

    check-cast v0, LX/4vl;

    goto :goto_0

    :cond_1
    new-instance v0, LX/4vm;

    invoke-direct {v0, p1}, LX/4vm;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.signin.service.START"

    return-object v0
.end method

.method public final a(LX/4st;Z)V
    .locals 2

    :try_start_0
    invoke-virtual {p0}, LX/2wI;->m()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, LX/4vl;

    iget-object v1, p0, LX/4vo;->g:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0, p1, v1, p2}, LX/4vl;->a(LX/4st;IZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    const-string v0, "SignInClientImpl"

    const-string v1, "Remote service probably died when saveDefaultAccount is called"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public final a(LX/4ud;)V
    .locals 4

    const-string v0, "Expecting a valid ISignInCallbacks"

    invoke-static {p1, v0}, LX/1ol;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :try_start_0
    iget-object v0, p0, LX/4vo;->e:LX/2wA;

    iget-object v1, v0, LX/2wA;->a:Landroid/accounts/Account;

    if-eqz v1, :cond_1

    iget-object v1, v0, LX/2wA;->a:Landroid/accounts/Account;

    :goto_0
    move-object v1, v1

    const/4 v0, 0x0

    const-string v2, "<<default account>>"

    iget-object v3, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v0, p0, LX/2wI;->i:Landroid/content/Context;

    move-object v0, v0

    invoke-static {v0}, LX/4sO;->a(Landroid/content/Context;)LX/4sO;

    move-result-object v0

    invoke-virtual {v0}, LX/4sO;->a()Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;

    move-result-object v0

    :cond_0
    new-instance v2, Lcom/google/android/gms/common/internal/ResolveAccountRequest;

    iget-object v3, p0, LX/4vo;->g:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-direct {v2, v1, v3, v0}, Lcom/google/android/gms/common/internal/ResolveAccountRequest;-><init>(Landroid/accounts/Account;ILcom/google/android/gms/auth/api/signin/GoogleSignInAccount;)V

    move-object v1, v2

    invoke-virtual {p0}, LX/2wI;->m()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, LX/4vl;

    new-instance v2, Lcom/google/android/gms/signin/internal/SignInRequest;

    invoke-direct {v2, v1}, Lcom/google/android/gms/signin/internal/SignInRequest;-><init>(Lcom/google/android/gms/common/internal/ResolveAccountRequest;)V

    invoke-interface {v0, v2, p1}, LX/4vl;->a(Lcom/google/android/gms/signin/internal/SignInRequest;LX/4ud;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    return-void

    :catch_0
    move-exception v0

    const-string v1, "SignInClientImpl"

    const-string v2, "Remote service probably died when signIn is called"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_1
    new-instance v1, Lcom/google/android/gms/signin/internal/SignInResponse;

    const/16 v2, 0x8

    invoke-direct {v1, v2}, Lcom/google/android/gms/signin/internal/SignInResponse;-><init>(I)V

    invoke-interface {p1, v1}, LX/4ud;->a(Lcom/google/android/gms/signin/internal/SignInResponse;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    const-string v1, "SignInClientImpl"

    const-string v2, "ISignInCallbacks#onSignInComplete should be executed from the same process, unexpected RemoteException."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    :cond_1
    new-instance v1, Landroid/accounts/Account;

    const-string v2, "<<default account>>"

    const-string v3, "com.google"

    invoke-direct {v1, v2, v3}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.signin.internal.ISignInService"

    return-object v0
.end method

.method public final g()V
    .locals 1

    new-instance v0, LX/3GJ;

    invoke-direct {v0, p0}, LX/3GJ;-><init>(LX/2wI;)V

    invoke-virtual {p0, v0}, LX/2wI;->a(LX/2wt;)V

    return-void
.end method

.method public final j()Landroid/os/Bundle;
    .locals 4

    iget-object v0, p0, LX/4vo;->e:LX/2wA;

    iget-object v1, v0, LX/2wA;->g:Ljava/lang/String;

    move-object v0, v1

    iget-object v1, p0, LX/2wI;->i:Landroid/content/Context;

    move-object v1, v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/4vo;->f:Landroid/os/Bundle;

    const-string v1, "com.google.android.gms.signin.internal.realClientPackageName"

    iget-object v2, p0, LX/4vo;->e:LX/2wA;

    iget-object v3, v2, LX/2wA;->g:Ljava/lang/String;

    move-object v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, LX/4vo;->f:Landroid/os/Bundle;

    return-object v0
.end method

.method public final j_()V
    .locals 2

    :try_start_0
    invoke-virtual {p0}, LX/2wI;->m()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, LX/4vl;

    iget-object v1, p0, LX/4vo;->g:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0, v1}, LX/4vl;->a(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    const-string v0, "SignInClientImpl"

    const-string v1, "Remote service probably died when clearAccountFromSessionStore is called"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public final n()Z
    .locals 1

    iget-boolean v0, p0, LX/4vo;->d:Z

    return v0
.end method
