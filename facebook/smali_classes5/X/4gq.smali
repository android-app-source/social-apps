.class public LX/4gq;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;

.field public b:LX/0X7;

.field public c:LX/0W4;

.field private d:LX/0Ww;

.field private e:LX/0W3;


# direct methods
.method public constructor <init>(LX/0Ww;LX/0W3;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 800188
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 800189
    iput-object p1, p0, LX/4gq;->d:LX/0Ww;

    .line 800190
    iput-object p2, p0, LX/4gq;->e:LX/0W3;

    .line 800191
    iget-object v0, p0, LX/4gq;->d:LX/0Ww;

    invoke-virtual {v0}, LX/0Ww;->b()Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;

    move-result-object v0

    iput-object v0, p0, LX/4gq;->a:Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;

    .line 800192
    invoke-virtual {p0}, LX/4gq;->a()V

    .line 800193
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 800182
    iget-object v0, p0, LX/4gq;->e:LX/0W3;

    instance-of v0, v0, LX/0W3;

    if-eqz v0, :cond_0

    .line 800183
    iget-object v0, p0, LX/4gq;->e:LX/0W3;

    invoke-virtual {v0}, LX/0W3;->a()LX/0W4;

    move-result-object v0

    check-cast v0, LX/0X7;

    iput-object v0, p0, LX/4gq;->b:LX/0X7;

    .line 800184
    iget-object v0, p0, LX/4gq;->e:LX/0W3;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0W3;->a(I)LX/0W4;

    move-result-object v0

    iput-object v0, p0, LX/4gq;->c:LX/0W4;

    .line 800185
    :goto_0
    return-void

    .line 800186
    :cond_0
    iput-object v1, p0, LX/4gq;->b:LX/0X7;

    .line 800187
    iput-object v1, p0, LX/4gq;->c:LX/0W4;

    goto :goto_0
.end method

.method public final c(J)Z
    .locals 1

    .prologue
    .line 800224
    iget-object v0, p0, LX/4gq;->a:Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;

    if-eqz v0, :cond_0

    .line 800225
    iget-object v0, p0, LX/4gq;->a:Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;->hasBoolOverrideForParam(J)Z

    move-result v0

    .line 800226
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(J)Z
    .locals 1

    .prologue
    .line 800221
    iget-object v0, p0, LX/4gq;->a:Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;

    if-eqz v0, :cond_0

    .line 800222
    iget-object v0, p0, LX/4gq;->a:Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;->boolOverrideForParam(J)Z

    move-result v0

    .line 800223
    :goto_0
    return v0

    :cond_0
    invoke-static {p1, p2}, LX/0ok;->a(J)Z

    move-result v0

    goto :goto_0
.end method

.method public final e(J)Z
    .locals 7

    .prologue
    .line 800218
    iget-object v0, p0, LX/4gq;->b:LX/0X7;

    if-eqz v0, :cond_0

    .line 800219
    iget-object v1, p0, LX/4gq;->b:LX/0X7;

    invoke-static {p1, p2}, LX/0ok;->a(J)Z

    move-result v4

    const/4 v5, 0x1

    const/4 v6, 0x0

    move-wide v2, p1

    invoke-virtual/range {v1 .. v6}, LX/0X7;->a(JZZZ)Z

    move-result v0

    .line 800220
    :goto_0
    return v0

    :cond_0
    invoke-static {p1, p2}, LX/0ok;->a(J)Z

    move-result v0

    goto :goto_0
.end method

.method public final i(J)Z
    .locals 1

    .prologue
    .line 800215
    iget-object v0, p0, LX/4gq;->a:Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;

    if-eqz v0, :cond_0

    .line 800216
    iget-object v0, p0, LX/4gq;->a:Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;->hasDoubleOverrideForParam(J)Z

    move-result v0

    .line 800217
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j(J)D
    .locals 3

    .prologue
    .line 800212
    iget-object v0, p0, LX/4gq;->a:Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;

    if-eqz v0, :cond_0

    .line 800213
    iget-object v0, p0, LX/4gq;->a:Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;->doubleOverrideForParam(J)D

    move-result-wide v0

    .line 800214
    :goto_0
    return-wide v0

    :cond_0
    invoke-static {p1, p2}, LX/0ok;->c(J)D

    move-result-wide v0

    goto :goto_0
.end method

.method public final k(J)D
    .locals 9

    .prologue
    .line 800227
    iget-object v0, p0, LX/4gq;->b:LX/0X7;

    if-eqz v0, :cond_0

    .line 800228
    iget-object v1, p0, LX/4gq;->b:LX/0X7;

    invoke-static {p1, p2}, LX/0ok;->c(J)D

    move-result-wide v4

    const/4 v6, 0x1

    const/4 v7, 0x0

    move-wide v2, p1

    invoke-virtual/range {v1 .. v7}, LX/0X7;->a(JDZZ)D

    move-result-wide v0

    .line 800229
    :goto_0
    return-wide v0

    :cond_0
    invoke-static {p1, p2}, LX/0ok;->c(J)D

    move-result-wide v0

    goto :goto_0
.end method

.method public final o(J)Z
    .locals 1

    .prologue
    .line 800209
    iget-object v0, p0, LX/4gq;->a:Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;

    if-eqz v0, :cond_0

    .line 800210
    iget-object v0, p0, LX/4gq;->a:Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;->hasIntOverrideForParam(J)Z

    move-result v0

    .line 800211
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final p(J)J
    .locals 3

    .prologue
    .line 800206
    iget-object v0, p0, LX/4gq;->a:Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;

    if-eqz v0, :cond_0

    .line 800207
    iget-object v0, p0, LX/4gq;->a:Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;->intOverrideForParam(J)J

    move-result-wide v0

    .line 800208
    :goto_0
    return-wide v0

    :cond_0
    invoke-static {p1, p2}, LX/0ok;->b(J)J

    move-result-wide v0

    goto :goto_0
.end method

.method public final q(J)J
    .locals 9

    .prologue
    .line 800203
    iget-object v0, p0, LX/4gq;->b:LX/0X7;

    if-eqz v0, :cond_0

    .line 800204
    iget-object v1, p0, LX/4gq;->b:LX/0X7;

    invoke-static {p1, p2}, LX/0ok;->b(J)J

    move-result-wide v4

    const/4 v6, 0x1

    const/4 v7, 0x0

    move-wide v2, p1

    invoke-virtual/range {v1 .. v7}, LX/0X7;->a(JJZZ)J

    move-result-wide v0

    .line 800205
    :goto_0
    return-wide v0

    :cond_0
    invoke-static {p1, p2}, LX/0ok;->b(J)J

    move-result-wide v0

    goto :goto_0
.end method

.method public final u(J)Z
    .locals 1

    .prologue
    .line 800200
    iget-object v0, p0, LX/4gq;->a:Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;

    if-eqz v0, :cond_0

    .line 800201
    iget-object v0, p0, LX/4gq;->a:Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;->hasStringOverrideForParam(J)Z

    move-result v0

    .line 800202
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final v(J)Ljava/lang/String;
    .locals 1

    .prologue
    .line 800197
    iget-object v0, p0, LX/4gq;->a:Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;

    if-eqz v0, :cond_0

    .line 800198
    iget-object v0, p0, LX/4gq;->a:Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/mobileconfig/MobileConfigOverridesTableHolder;->stringOverrideForParam(J)Ljava/lang/String;

    move-result-object v0

    .line 800199
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1, p2}, LX/0ok;->d(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final w(J)Ljava/lang/String;
    .locals 7

    .prologue
    .line 800194
    iget-object v0, p0, LX/4gq;->b:LX/0X7;

    if-eqz v0, :cond_0

    .line 800195
    iget-object v1, p0, LX/4gq;->b:LX/0X7;

    invoke-static {p1, p2}, LX/0ok;->d(J)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    const/4 v6, 0x0

    move-wide v2, p1

    invoke-virtual/range {v1 .. v6}, LX/0X7;->a(JLjava/lang/String;ZZ)Ljava/lang/String;

    move-result-object v0

    .line 800196
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1, p2}, LX/0ok;->d(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
