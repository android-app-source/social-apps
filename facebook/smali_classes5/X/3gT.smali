.class public LX/3gT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2ZK;


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0Ww;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2Yb;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/mobileconfig/MobileConfigCxxChangeListener;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Uh;

.field private final g:LX/0Wd;

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/mobileconfig/init/MobileConfigInit;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 624453
    const-class v0, LX/3gT;

    sput-object v0, LX/3gT;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Uh;LX/0Wd;LX/0Or;LX/0Ot;)V
    .locals 0
    .param p5    # LX/0Uh;
        .annotation runtime Lcom/facebook/gk/sessionless/Sessionless;
        .end annotation
    .end param
    .param p6    # LX/0Wd;
        .annotation runtime Lcom/facebook/common/idleexecutor/DefaultIdleExecutor;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/0Ww;",
            ">;",
            "LX/0Or",
            "<",
            "LX/2Yb;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/mobileconfig/MobileConfigCxxChangeListener;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "Lcom/facebook/common/idleexecutor/IdleExecutor;",
            "LX/0Or",
            "<",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/mobileconfig/init/MobileConfigInit;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 624476
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 624477
    iput-object p1, p0, LX/3gT;->b:LX/0Or;

    .line 624478
    iput-object p2, p0, LX/3gT;->c:LX/0Or;

    .line 624479
    iput-object p3, p0, LX/3gT;->e:LX/0Or;

    .line 624480
    iput-object p4, p0, LX/3gT;->d:LX/0Or;

    .line 624481
    iput-object p5, p0, LX/3gT;->f:LX/0Uh;

    .line 624482
    iput-object p6, p0, LX/3gT;->g:LX/0Wd;

    .line 624483
    iput-object p7, p0, LX/3gT;->i:LX/0Or;

    .line 624484
    iput-object p8, p0, LX/3gT;->h:LX/0Ot;

    .line 624485
    return-void
.end method

.method public static b(LX/0QB;)LX/3gT;
    .locals 9

    .prologue
    .line 624474
    new-instance v0, LX/3gT;

    const/16 v1, 0xdf5

    invoke-static {p0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    const/16 v2, 0xb8b

    invoke-static {p0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    const/16 v3, 0x19e

    invoke-static {p0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    const/16 v4, 0xdf3

    invoke-static {p0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {p0}, LX/0WW;->a(LX/0QB;)LX/0Uh;

    move-result-object v5

    check-cast v5, LX/0Uh;

    invoke-static {p0}, LX/0Wa;->a(LX/0QB;)LX/0Wd;

    move-result-object v6

    check-cast v6, LX/0Wd;

    const/16 v7, 0xdf4

    invoke-static {p0, v7}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    const/16 v8, 0xdf8

    invoke-static {p0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-direct/range {v0 .. v8}, LX/3gT;-><init>(LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Uh;LX/0Wd;LX/0Or;LX/0Ot;)V

    .line 624475
    return-object v0
.end method

.method public static b(LX/3gT;LX/0Wx;)V
    .locals 3

    .prologue
    .line 624469
    iget-object v0, p0, LX/3gT;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/mobileconfig/init/MobileConfigInit;

    iget-object v1, p0, LX/3gT;->e:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/auth/viewercontext/ViewerContext;

    iget-object v2, p0, LX/3gT;->i:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0W3;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/mobileconfig/init/MobileConfigInit;->a(Lcom/facebook/auth/viewercontext/ViewerContext;LX/0W3;)Z

    .line 624470
    invoke-interface {p1}, LX/0Wx;->isTigonServiceSet()Z

    move-result v0

    if-nez v0, :cond_0

    .line 624471
    iget-object v0, p0, LX/3gT;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/tigon/iface/TigonServiceHolder;

    const/4 v1, 0x1

    invoke-interface {p1, v0, v1}, LX/0Wx;->setTigonService(Lcom/facebook/tigon/iface/TigonServiceHolder;Z)V

    .line 624472
    iget-object v0, p0, LX/3gT;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/mobileconfig/MobileConfigCxxChangeListener;

    invoke-interface {p1, v0}, LX/0Wx;->registerConfigChangeListener(Lcom/facebook/mobileconfig/MobileConfigCxxChangeListener;)Z

    .line 624473
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/0Wx;I)Z
    .locals 1

    .prologue
    .line 624465
    invoke-static {p0, p1}, LX/3gT;->b(LX/3gT;LX/0Wx;)V

    .line 624466
    invoke-interface {p1, p2}, LX/0Wx;->updateConfigsSynchronously(I)Z

    move-result v0

    .line 624467
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 624468
    return v0
.end method

.method public final b()LX/2ZE;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 624459
    iget-object v0, p0, LX/3gT;->f:LX/0Uh;

    const/16 v1, 0xd

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    .line 624460
    if-nez v0, :cond_1

    .line 624461
    :cond_0
    :goto_0
    return-object v3

    .line 624462
    :cond_1
    iget-object v0, p0, LX/3gT;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Wx;

    .line 624463
    if-eqz v0, :cond_0

    .line 624464
    iget-object v1, p0, LX/3gT;->g:LX/0Wd;

    new-instance v2, Lcom/facebook/mobileconfig/init/MobileConfigConfigurationComponent$1;

    invoke-direct {v2, p0, v0}, Lcom/facebook/mobileconfig/init/MobileConfigConfigurationComponent$1;-><init>(LX/3gT;LX/0Wx;)V

    const v0, -0x4aabbe1c

    invoke-static {v1, v2, v0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0
.end method

.method public final b(LX/0Wx;I)Z
    .locals 1

    .prologue
    .line 624455
    invoke-static {p0, p1}, LX/3gT;->b(LX/3gT;LX/0Wx;)V

    .line 624456
    invoke-interface {p1, p2}, LX/0Wx;->refreshConfigInfos(I)Z

    move-result v0

    .line 624457
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 624458
    return v0
.end method

.method public final dr_()J
    .locals 2

    .prologue
    .line 624454
    const-wide/32 v0, 0xdbba00

    return-wide v0
.end method
