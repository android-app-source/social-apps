.class public LX/4or;
.super Landroid/preference/ListPreference;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field private final a:LX/2qy;

.field public b:LX/2qx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 810451
    invoke-direct {p0, p1}, Landroid/preference/ListPreference;-><init>(Landroid/content/Context;)V

    .line 810452
    const-class v0, LX/4or;

    invoke-static {v0, p0}, LX/4or;->a(Ljava/lang/Class;Landroid/preference/Preference;)V

    .line 810453
    iget-object v0, p0, LX/4or;->b:LX/2qx;

    invoke-virtual {v0, p0}, LX/2qx;->a(Landroid/preference/Preference;)LX/2qy;

    move-result-object v0

    iput-object v0, p0, LX/4or;->a:LX/2qy;

    .line 810454
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/preference/Preference;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/preference/Preference;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, LX/4or;

    const-class p0, LX/2qx;

    invoke-interface {v1, p0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/2qx;

    iput-object v1, p1, LX/4or;->b:LX/2qx;

    return-void
.end method


# virtual methods
.method public final a(LX/0Tn;)V
    .locals 1

    .prologue
    .line 810455
    iget-object v0, p0, LX/4or;->a:LX/2qy;

    invoke-virtual {v0, p1}, LX/2qy;->a(LX/0Tn;)V

    .line 810456
    return-void
.end method

.method public getPersistedString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 810457
    iget-object v0, p0, LX/4or;->a:LX/2qy;

    invoke-virtual {v0, p1}, LX/2qy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getSharedPreferences()Landroid/content/SharedPreferences;
    .locals 1

    .prologue
    .line 810458
    iget-object v0, p0, LX/4or;->a:LX/2qy;

    .line 810459
    iget-object p0, v0, LX/2qy;->b:Landroid/content/SharedPreferences;

    move-object v0, p0

    .line 810460
    return-object v0
.end method

.method public persistString(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 810461
    iget-object v0, p0, LX/4or;->a:LX/2qy;

    invoke-virtual {v0, p1}, LX/2qy;->b(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
