.class public final LX/3gF;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/2y0;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/3gE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2y0",
            "<TE;>.ExplanationComponentImpl;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/2y0;

.field private c:[Ljava/lang/String;

.field private d:I

.field private e:Ljava/util/BitSet;


# direct methods
.method public constructor <init>(LX/2y0;)V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 624178
    iput-object p1, p0, LX/3gF;->b:LX/2y0;

    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 624179
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "storyProps"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "environment"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "textHeaderStyle"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/3gF;->c:[Ljava/lang/String;

    .line 624180
    iput v3, p0, LX/3gF;->d:I

    .line 624181
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/3gF;->d:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/3gF;->e:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/3gF;LX/1De;IILX/3gE;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "II",
            "LX/2y0",
            "<TE;>.ExplanationComponentImpl;)V"
        }
    .end annotation

    .prologue
    .line 624182
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 624183
    iput-object p4, p0, LX/3gF;->a:LX/3gE;

    .line 624184
    iget-object v0, p0, LX/3gF;->e:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 624185
    return-void
.end method


# virtual methods
.method public final a(LX/1Pq;)LX/3gF;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)",
            "LX/2y0",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 624186
    iget-object v0, p0, LX/3gF;->a:LX/3gE;

    iput-object p1, v0, LX/3gE;->b:LX/1Pq;

    .line 624187
    iget-object v0, p0, LX/3gF;->e:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 624188
    return-object p0
.end method

.method public final a(LX/2ei;)LX/3gF;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2ei;",
            ")",
            "LX/2y0",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 624189
    iget-object v0, p0, LX/3gF;->a:LX/3gE;

    iput-object p1, v0, LX/3gE;->c:LX/2ei;

    .line 624190
    iget-object v0, p0, LX/3gF;->e:Ljava/util/BitSet;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 624191
    return-object p0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/3gF;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "LX/2y0",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 624192
    iget-object v0, p0, LX/3gF;->a:LX/3gE;

    iput-object p1, v0, LX/3gE;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 624193
    iget-object v0, p0, LX/3gF;->e:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 624194
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 624195
    invoke-super {p0}, LX/1X5;->a()V

    .line 624196
    const/4 v0, 0x0

    iput-object v0, p0, LX/3gF;->a:LX/3gE;

    .line 624197
    iget-object v0, p0, LX/3gF;->b:LX/2y0;

    iget-object v0, v0, LX/2y0;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 624198
    return-void
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/2y0;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 624199
    iget-object v1, p0, LX/3gF;->e:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/3gF;->e:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/3gF;->d:I

    if-ge v1, v2, :cond_2

    .line 624200
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 624201
    :goto_0
    iget v2, p0, LX/3gF;->d:I

    if-ge v0, v2, :cond_1

    .line 624202
    iget-object v2, p0, LX/3gF;->e:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 624203
    iget-object v2, p0, LX/3gF;->c:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 624204
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 624205
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 624206
    :cond_2
    iget-object v0, p0, LX/3gF;->a:LX/3gE;

    .line 624207
    invoke-virtual {p0}, LX/3gF;->a()V

    .line 624208
    return-object v0
.end method
