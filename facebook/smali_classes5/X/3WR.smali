.class public LX/3WR;
.super LX/3Vv;
.source ""


# static fields
.field public static final a:LX/1Cz;


# instance fields
.field private final g:Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 590313
    new-instance v0, LX/3WS;

    invoke-direct {v0}, LX/3WS;-><init>()V

    sput-object v0, LX/3WR;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 590302
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/3WR;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 590303
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 590304
    invoke-direct {p0, p1, p2}, LX/3Vv;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 590305
    const v0, 0x7f0309bc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 590306
    const v0, 0x7f0d18c4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    iput-object v0, p0, LX/3WR;->g:Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    .line 590307
    const v0, 0x7f0d084b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/3WR;->c:Landroid/widget/TextView;

    .line 590308
    const v0, 0x7f0d084d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/3WR;->d:Landroid/widget/TextView;

    .line 590309
    const v0, 0x7f0d084e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/3WR;->e:Landroid/widget/TextView;

    .line 590310
    const v0, 0x7f0d084c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RatingBar;

    iput-object v0, p0, LX/3WR;->f:Landroid/widget/RatingBar;

    .line 590311
    const v0, 0x7f0d084a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/3WR;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 590312
    return-void
.end method


# virtual methods
.method public getActionButton()Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;
    .locals 1

    .prologue
    .line 590301
    iget-object v0, p0, LX/3WR;->g:Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    return-object v0
.end method

.method public setActionButtonOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 590299
    iget-object v0, p0, LX/3WR;->g:Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    invoke-virtual {v0, p1}, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 590300
    return-void
.end method
