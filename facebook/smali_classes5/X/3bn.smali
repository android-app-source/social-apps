.class public LX/3bn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1TE;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 612146
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 612147
    return-void
.end method

.method public static a(LX/0QB;)LX/3bn;
    .locals 3

    .prologue
    .line 612148
    const-class v1, LX/3bn;

    monitor-enter v1

    .line 612149
    :try_start_0
    sget-object v0, LX/3bn;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 612150
    sput-object v2, LX/3bn;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 612151
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 612152
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 612153
    new-instance v0, LX/3bn;

    invoke-direct {v0}, LX/3bn;-><init>()V

    .line 612154
    move-object v0, v0

    .line 612155
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 612156
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/3bn;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 612157
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 612158
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1KB;)V
    .locals 1

    .prologue
    .line 612159
    sget-object v0, Lcom/facebook/timeline/favmediapicker/rows/parts/ClickablePhotoPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 612160
    sget-object v0, Lcom/facebook/timeline/favmediapicker/rows/parts/OpenCameraPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 612161
    sget-object v0, Lcom/facebook/timeline/favmediapicker/rows/parts/SectionTitlePartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 612162
    sget-object v0, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraGridRowPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 612163
    sget-object v0, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedForYouLabelPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 612164
    sget-object v0, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetGridRowPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 612165
    sget-object v0, Lcom/facebook/timeline/favmediapicker/rows/parts/LoadingIndicatorPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 612166
    return-void
.end method
