.class public final LX/3dz;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0U1;

.field public static final b:LX/0U1;

.field public static final c:LX/0U1;

.field public static final d:LX/0U1;

.field public static final e:LX/0U1;

.field public static final f:LX/0U1;

.field public static final g:LX/0U1;

.field public static final h:LX/0U1;

.field public static final i:LX/0U1;

.field public static final j:LX/0U1;

.field public static final k:LX/0U1;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 618968
    new-instance v0, LX/0U1;

    const-string v1, "id"

    const-string v2, "TEXT PRIMARY KEY"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3dz;->a:LX/0U1;

    .line 618969
    new-instance v0, LX/0U1;

    const-string v1, "pack_id"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3dz;->b:LX/0U1;

    .line 618970
    new-instance v0, LX/0U1;

    const-string v1, "uri"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3dz;->c:LX/0U1;

    .line 618971
    new-instance v0, LX/0U1;

    const-string v1, "animated_uri"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3dz;->d:LX/0U1;

    .line 618972
    new-instance v0, LX/0U1;

    const-string v1, "preview_uri"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3dz;->e:LX/0U1;

    .line 618973
    new-instance v0, LX/0U1;

    const-string v1, "is_comments_capable"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3dz;->f:LX/0U1;

    .line 618974
    new-instance v0, LX/0U1;

    const-string v1, "is_composer_capable"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3dz;->g:LX/0U1;

    .line 618975
    new-instance v0, LX/0U1;

    const-string v1, "is_messenger_capable"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3dz;->h:LX/0U1;

    .line 618976
    new-instance v0, LX/0U1;

    const-string v1, "is_sms_capable"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3dz;->i:LX/0U1;

    .line 618977
    new-instance v0, LX/0U1;

    const-string v1, "is_posts_capable"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3dz;->j:LX/0U1;

    .line 618978
    new-instance v0, LX/0U1;

    const-string v1, "is_montage_capable"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/3dz;->k:LX/0U1;

    return-void
.end method
