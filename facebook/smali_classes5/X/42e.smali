.class public final LX/42e;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field public final synthetic a:LX/42f;


# direct methods
.method public constructor <init>(LX/42f;)V
    .locals 0

    .prologue
    .line 667614
    iput-object p1, p0, LX/42e;->a:LX/42f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGlobalLayout()V
    .locals 4

    .prologue
    .line 667615
    iget-object v0, p0, LX/42e;->a:LX/42f;

    .line 667616
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 667617
    iget-object v2, v0, LX/42f;->d:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v1}, Landroid/widget/FrameLayout;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 667618
    iget v2, v1, Landroid/graphics/Rect;->bottom:I

    iget v1, v1, Landroid/graphics/Rect;->top:I

    sub-int v1, v2, v1

    move v1, v1

    .line 667619
    iget v2, v0, LX/42f;->b:I

    if-eq v1, v2, :cond_0

    .line 667620
    iget-object v2, v0, LX/42f;->d:Landroid/widget/FrameLayout;

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getHeight()I

    move-result v2

    .line 667621
    sub-int v3, v2, v1

    .line 667622
    div-int/lit8 p0, v2, 0x4

    if-le v3, p0, :cond_1

    .line 667623
    iget-object p0, v0, LX/42f;->c:Landroid/widget/FrameLayout$LayoutParams;

    sub-int/2addr v2, v3

    iget v3, v0, LX/42f;->e:I

    add-int/2addr v2, v3

    iput v2, p0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 667624
    :goto_0
    iget-object v2, v0, LX/42f;->a:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->requestLayout()V

    .line 667625
    iput v1, v0, LX/42f;->b:I

    .line 667626
    :cond_0
    return-void

    .line 667627
    :cond_1
    iget-object v3, v0, LX/42f;->c:Landroid/widget/FrameLayout$LayoutParams;

    iput v2, v3, Landroid/widget/FrameLayout$LayoutParams;->height:I

    goto :goto_0
.end method
