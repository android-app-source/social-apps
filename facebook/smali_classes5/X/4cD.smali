.class public LX/4cD;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/4cD;


# instance fields
.field public a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/4cC;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0ad;


# direct methods
.method public constructor <init>(LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 794715
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 794716
    iput-object p1, p0, LX/4cD;->b:LX/0ad;

    .line 794717
    return-void
.end method

.method public static a(LX/0QB;)LX/4cD;
    .locals 4

    .prologue
    .line 794718
    sget-object v0, LX/4cD;->c:LX/4cD;

    if-nez v0, :cond_1

    .line 794719
    const-class v1, LX/4cD;

    monitor-enter v1

    .line 794720
    :try_start_0
    sget-object v0, LX/4cD;->c:LX/4cD;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 794721
    if-eqz v2, :cond_0

    .line 794722
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 794723
    new-instance p0, LX/4cD;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-direct {p0, v3}, LX/4cD;-><init>(LX/0ad;)V

    .line 794724
    move-object v0, p0

    .line 794725
    sput-object v0, LX/4cD;->c:LX/4cD;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 794726
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 794727
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 794728
    :cond_1
    sget-object v0, LX/4cD;->c:LX/4cD;

    return-object v0

    .line 794729
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 794730
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/4bV;)Z
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 794731
    iget-object v0, p0, LX/4cD;->a:Ljava/util/ArrayList;

    if-nez v0, :cond_2

    .line 794732
    const/4 v2, 0x0

    .line 794733
    iget-object v0, p0, LX/4cD;->b:LX/0ad;

    sget-char v3, LX/0by;->R:C

    const-string v4, "image::ImagePushSubscriber"

    invoke-interface {v0, v3, v4}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 794734
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 794735
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 794736
    const-string v4, ","

    invoke-virtual {v0, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    array-length v5, v4

    move v0, v2

    :goto_0
    if-ge v0, v5, :cond_1

    aget-object v6, v4, v0

    .line 794737
    const-string v7, "::"

    invoke-virtual {v6, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 794738
    array-length v7, v6

    const/4 v8, 0x2

    if-lt v7, v8, :cond_0

    .line 794739
    new-instance v7, LX/4cC;

    aget-object v8, v6, v2

    const/4 v9, 0x1

    aget-object v6, v6, v9

    invoke-direct {v7, v8, v6}, LX/4cC;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 794740
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 794741
    :cond_1
    iput-object v3, p0, LX/4cD;->a:Ljava/util/ArrayList;

    .line 794742
    :cond_2
    iget-object v0, p0, LX/4cD;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v2, v1

    :goto_1
    if-ge v2, v3, :cond_4

    iget-object v0, p0, LX/4cD;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4cC;

    .line 794743
    iget-object v4, p1, LX/4bV;->c:LX/15D;

    .line 794744
    invoke-static {v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 794745
    iget-object v5, v0, LX/4cC;->a:Ljava/lang/String;

    .line 794746
    iget-object v6, v4, LX/15D;->c:Ljava/lang/String;

    move-object v6, v6

    .line 794747
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    iget-object v5, v0, LX/4cC;->b:Ljava/lang/String;

    invoke-static {v4}, Lcom/facebook/http/common/FbHttpUtils;->b(LX/15D;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    const/4 v5, 0x1

    :goto_2
    move v0, v5

    .line 794748
    if-eqz v0, :cond_3

    .line 794749
    const/4 v0, 0x1

    .line 794750
    :goto_3
    return v0

    .line 794751
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_4
    move v0, v1

    .line 794752
    goto :goto_3

    :cond_5
    const/4 v5, 0x0

    goto :goto_2
.end method
