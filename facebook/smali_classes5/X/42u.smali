.class public abstract LX/42u;
.super LX/0te;
.source ""


# instance fields
.field public final a:Ljava/lang/Object;

.field private volatile b:Z

.field public c:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "LX/42t;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 668293
    invoke-direct {p0}, LX/0te;-><init>()V

    .line 668294
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/42u;->a:Ljava/lang/Object;

    .line 668295
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 668288
    iget-object v1, p0, LX/42u;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 668289
    :try_start_0
    iget-boolean v0, p0, LX/42u;->b:Z

    if-nez v0, :cond_0

    .line 668290
    invoke-virtual {p0}, LX/42u;->c()V

    .line 668291
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/42u;->b:Z

    .line 668292
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public abstract a(Landroid/content/Intent;)V
.end method

.method public abstract b()Landroid/os/Looper;
.end method

.method public abstract c()V
.end method

.method public d()Ljava/lang/Integer;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 668224
    const/4 v0, 0x0

    return-object v0
.end method

.method public final dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 668285
    invoke-virtual {p0}, LX/42u;->a()V

    .line 668286
    invoke-super {p0, p1, p2, p3}, LX/0te;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 668287
    return-void
.end method

.method public h()V
    .locals 0

    .prologue
    .line 668284
    return-void
.end method

.method public final onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 668283
    const/4 v0, 0x0

    return-object v0
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 668278
    invoke-super {p0, p1}, LX/0te;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 668279
    iget-object v0, p0, LX/42u;->c:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/42t;

    .line 668280
    if-eqz v0, :cond_0

    .line 668281
    const/4 p0, 0x3

    invoke-virtual {v0, p0, p1}, LX/42t;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object p0

    invoke-virtual {v0, p0}, LX/42t;->sendMessage(Landroid/os/Message;)Z

    .line 668282
    :cond_0
    return-void
.end method

.method public final onFbCreate()V
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x24

    const v2, -0x58f7520d

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 668267
    invoke-super {p0}, LX/0te;->onFbCreate()V

    .line 668268
    invoke-virtual {p0}, LX/42u;->b()Landroid/os/Looper;

    move-result-object v2

    .line 668269
    if-eqz v2, :cond_1

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-eq v2, v1, :cond_1

    .line 668270
    new-instance v1, LX/42t;

    invoke-direct {v1, p0, v2}, LX/42t;-><init>(LX/42u;Landroid/os/Looper;)V

    .line 668271
    :goto_0
    new-instance v2, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v2, v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    iput-object v2, p0, LX/42u;->c:Ljava/util/concurrent/atomic/AtomicReference;

    .line 668272
    move-object v1, v1

    .line 668273
    if-eqz v1, :cond_0

    .line 668274
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/42t;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/42t;->sendMessage(Landroid/os/Message;)Z

    .line 668275
    :goto_1
    const v1, -0x7f21986d

    invoke-static {v1, v0}, LX/02F;->d(II)V

    return-void

    .line 668276
    :cond_0
    invoke-virtual {p0}, LX/42u;->a()V

    goto :goto_1

    .line 668277
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final onFbDestroy()V
    .locals 6

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x24

    const v2, 0x7238117b

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 668259
    invoke-super {p0}, LX/0te;->onFbDestroy()V

    .line 668260
    iget-object v0, p0, LX/42u;->c:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/42t;

    .line 668261
    if-eqz v0, :cond_0

    .line 668262
    const-wide/16 v3, 0x0

    .line 668263
    const/4 v5, 0x7

    invoke-virtual {v0, v5}, LX/42t;->obtainMessage(I)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/42t;->sendMessage(Landroid/os/Message;)Z

    move-result v5

    move v5, v5

    .line 668264
    if-nez v5, :cond_1

    .line 668265
    :goto_0
    const v0, 0x6a036aa6

    invoke-static {v0, v1}, LX/02F;->d(II)V

    return-void

    .line 668266
    :cond_0
    invoke-virtual {p0}, LX/42u;->h()V

    goto :goto_0

    :cond_1
    invoke-static {v0, v3, v4}, LX/42t;->b(LX/42t;J)Z

    goto :goto_0
.end method

.method public final onFbStartCommand(Landroid/content/Intent;II)I
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x24

    const v2, -0xc0c763c

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 668251
    invoke-super {p0, p1, p2, p3}, LX/0te;->onFbStartCommand(Landroid/content/Intent;II)I

    move-result v1

    .line 668252
    invoke-virtual {p0}, LX/42u;->d()Ljava/lang/Integer;

    move-result-object v3

    .line 668253
    iget-object v0, p0, LX/42u;->c:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/42t;

    .line 668254
    if-eqz v0, :cond_0

    .line 668255
    invoke-virtual {v0, p1, p2, p3}, LX/42t;->a(Landroid/content/Intent;II)Z

    .line 668256
    :goto_0
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_1
    const v1, -0x7fa92c30

    invoke-static {v1, v2}, LX/02F;->d(II)V

    return v0

    .line 668257
    :cond_0
    invoke-virtual {p0, p1}, LX/42u;->a(Landroid/content/Intent;)V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 668258
    goto :goto_1
.end method

.method public final onLowMemory()V
    .locals 1

    .prologue
    .line 668246
    invoke-super {p0}, LX/0te;->onLowMemory()V

    .line 668247
    iget-object v0, p0, LX/42u;->c:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/42t;

    .line 668248
    if-eqz v0, :cond_0

    .line 668249
    const/4 p0, 0x4

    invoke-virtual {v0, p0}, LX/42t;->obtainMessage(I)Landroid/os/Message;

    move-result-object p0

    invoke-virtual {v0, p0}, LX/42t;->sendMessage(Landroid/os/Message;)Z

    .line 668250
    :cond_0
    return-void
.end method

.method public final onRebind(Landroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, -0xc48c916

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 668244
    invoke-super {p0, p1}, LX/0te;->onRebind(Landroid/content/Intent;)V

    .line 668245
    const/16 v1, 0x25

    const v2, -0x63e5bd09

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart(Landroid/content/Intent;I)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 668238
    invoke-super {p0, p1, p2}, LX/0te;->onStart(Landroid/content/Intent;I)V

    .line 668239
    iget-object v0, p0, LX/42u;->c:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/42t;

    .line 668240
    if-eqz v0, :cond_0

    .line 668241
    const/4 p0, -0x1

    invoke-virtual {v0, p1, p0, p2}, LX/42t;->a(Landroid/content/Intent;II)Z

    .line 668242
    :goto_0
    return-void

    .line 668243
    :cond_0
    invoke-virtual {p0, p1}, LX/42u;->a(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public final onTaskRemoved(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 668233
    invoke-super {p0, p1}, LX/0te;->onTaskRemoved(Landroid/content/Intent;)V

    .line 668234
    iget-object v0, p0, LX/42u;->c:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/42t;

    .line 668235
    if-eqz v0, :cond_0

    .line 668236
    const/4 p0, 0x6

    invoke-virtual {v0, p0, p1}, LX/42t;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object p0

    invoke-virtual {v0, p0}, LX/42t;->sendMessage(Landroid/os/Message;)Z

    .line 668237
    :cond_0
    return-void
.end method

.method public final onTrimMemory(I)V
    .locals 1

    .prologue
    .line 668226
    invoke-super {p0, p1}, LX/0te;->onTrimMemory(I)V

    .line 668227
    iget-object v0, p0, LX/42u;->c:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/42t;

    .line 668228
    if-eqz v0, :cond_0

    .line 668229
    const/4 p0, 0x5

    invoke-virtual {v0, p0}, LX/42t;->obtainMessage(I)Landroid/os/Message;

    move-result-object p0

    .line 668230
    iput p1, p0, Landroid/os/Message;->arg1:I

    .line 668231
    invoke-virtual {v0, p0}, LX/42t;->sendMessage(Landroid/os/Message;)Z

    .line 668232
    :cond_0
    return-void
.end method

.method public final onUnbind(Landroid/content/Intent;)Z
    .locals 1

    .prologue
    .line 668225
    invoke-super {p0, p1}, LX/0te;->onUnbind(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method
