.class public LX/3TN;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements LX/3TO;


# instance fields
.field public a:LX/0gw;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0gy;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:LX/2iI;

.field private e:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

.field private f:Lcom/facebook/widget/FbSwipeRefreshLayout;

.field private g:LX/1DI;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 584215
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 584216
    invoke-direct {p0}, LX/3TN;->b()V

    .line 584217
    return-void
.end method

.method private static a(LX/3TN;Ljava/lang/ClassLoader;Ljava/lang/StringBuilder;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 584218
    const-string v0, "%s ^^\n Hashcode %d <<>>\n"

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 584219
    invoke-virtual {p1}, Ljava/lang/ClassLoader;->getParent()Ljava/lang/ClassLoader;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/ClassLoader;->getParent()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 584220
    :cond_0
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 584221
    const/4 v1, 0x0

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 584222
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p1}, Ljava/lang/ClassLoader;->getParent()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-static {p0, v0, p2}, LX/3TN;->a(LX/3TN;Ljava/lang/ClassLoader;Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/3TN;

    invoke-static {p0}, LX/0gw;->a(LX/0QB;)LX/0gw;

    move-result-object v1

    check-cast v1, LX/0gw;

    invoke-static {p0}, LX/0gy;->a(LX/0QB;)LX/0gy;

    move-result-object v2

    check-cast v2, LX/0gy;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object p0

    check-cast p0, LX/03V;

    iput-object v1, p1, LX/3TN;->a:LX/0gw;

    iput-object v2, p1, LX/3TN;->b:LX/0gy;

    iput-object p0, p1, LX/3TN;->c:LX/03V;

    return-void
.end method

.method private b()V
    .locals 9

    .prologue
    const v1, 0x102000a

    const/4 v3, 0x1

    .line 584228
    const-class v0, LX/3TN;

    invoke-static {v0, p0}, LX/3TN;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 584229
    const v0, 0x7f030c3b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 584230
    const v0, 0x7f0d0595

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FbSwipeRefreshLayout;

    iput-object v0, p0, LX/3TN;->f:Lcom/facebook/widget/FbSwipeRefreshLayout;

    .line 584231
    invoke-virtual {p0, v3}, LX/3TN;->setOrientation(I)V

    .line 584232
    const v0, 0x102000a

    :try_start_0
    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 584233
    new-instance v1, LX/2iI;

    invoke-direct {v1, v0}, LX/2iI;-><init>(Lcom/facebook/widget/listview/BetterListView;)V

    iput-object v1, p0, LX/3TN;->d:LX/2iI;

    .line 584234
    const v0, 0x7f0d05b0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iput-object v0, p0, LX/3TN;->e:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 584235
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 584236
    invoke-virtual {p0}, LX/3TN;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const v2, 0x7f0101e2

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 584237
    iget-object v1, p0, LX/3TN;->d:LX/2iI;

    invoke-virtual {v1}, LX/2iI;->k()V

    .line 584238
    iget-object v1, p0, LX/3TN;->e:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iget v2, v0, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v1, v2}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setBackgroundResource(I)V

    .line 584239
    iget-object v1, p0, LX/3TN;->f:Lcom/facebook/widget/FbSwipeRefreshLayout;

    iget v0, v0, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v1, v0}, Lcom/facebook/widget/FbSwipeRefreshLayout;->setBackgroundResource(I)V

    .line 584240
    invoke-static {p0}, LX/3TN;->c(LX/3TN;)V

    .line 584241
    return-void

    .line 584242
    :catch_0
    move-exception v0

    .line 584243
    invoke-virtual {p0, v1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v1

    .line 584244
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    .line 584245
    const-class v3, Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v3}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    .line 584246
    const-class v4, Lcom/facebook/notifications/widget/NotificationsListView;

    invoke-virtual {v4}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v4

    .line 584247
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 584248
    iget-object v6, p0, LX/3TN;->c:LX/03V;

    const-string v7, "CLASSCAST_ERROR"

    const-string v8, "The view itself is of class %s\nand its loader tree is:\n %s\n>><<\nThe loader tree for BetterListView is:\n %s\n>><<The loader tree for NotificationsListView is:\n%s\n>><<"

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v2, v5}, LX/3TN;->a(LX/3TN;Ljava/lang/ClassLoader;Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v3, v5}, LX/3TN;->a(LX/3TN;Ljava/lang/ClassLoader;Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v4, v5}, LX/3TN;->a(LX/3TN;Ljava/lang/ClassLoader;Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v8, v1, v2, v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v7, v1, v0}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 584249
    throw v0
.end method

.method private static c(LX/3TN;)V
    .locals 5

    .prologue
    .line 584223
    iget-object v0, p0, LX/3TN;->a:LX/0gw;

    invoke-virtual {v0}, LX/0gw;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 584224
    iget-object v0, p0, LX/3TN;->b:LX/0gy;

    sget-object v1, LX/0cQ;->NOTIFICATIONS_FRAGMENT:LX/0cQ;

    invoke-virtual {p0}, LX/3TN;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, LX/3TN;->f:Lcom/facebook/widget/FbSwipeRefreshLayout;

    const v4, 0x7f0101e2

    invoke-virtual {v0, v1, v2, v3, v4}, LX/0gy;->a(LX/0cQ;Landroid/content/Context;Landroid/support/v4/widget/SwipeRefreshLayout;I)V

    .line 584225
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 584226
    invoke-static {p0}, LX/3TN;->c(LX/3TN;)V

    .line 584227
    return-void
.end method

.method public final a(Landroid/view/View$OnClickListener;LX/1DI;)V
    .locals 1

    .prologue
    .line 584197
    iput-object p2, p0, LX/3TN;->g:LX/1DI;

    .line 584198
    const v0, 0x7f0d1ded

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 584199
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 584212
    iget-object v1, p0, LX/3TN;->e:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setVisibility(I)V

    .line 584213
    return-void

    .line 584214
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 584208
    if-eqz p1, :cond_0

    .line 584209
    iget-object v0, p0, LX/3TN;->e:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 584210
    :goto_0
    return-void

    .line 584211
    :cond_0
    iget-object v0, p0, LX/3TN;->e:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    goto :goto_0
.end method

.method public final c(Z)V
    .locals 3

    .prologue
    .line 584204
    if-eqz p1, :cond_0

    .line 584205
    iget-object v0, p0, LX/3TN;->e:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {p0}, LX/3TN;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08006f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/3TN;->g:LX/1DI;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(Ljava/lang/String;LX/1DI;)V

    .line 584206
    :goto_0
    return-void

    .line 584207
    :cond_0
    iget-object v0, p0, LX/3TN;->e:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    goto :goto_0
.end method

.method public getListView()Lcom/facebook/widget/listview/BetterListView;
    .locals 1

    .prologue
    .line 584203
    iget-object v0, p0, LX/3TN;->d:LX/2iI;

    invoke-virtual {v0}, LX/2iI;->c()Lcom/facebook/widget/listview/BetterListView;

    move-result-object v0

    return-object v0
.end method

.method public getRefreshableContainerLike()LX/62k;
    .locals 1

    .prologue
    .line 584202
    const/4 v0, 0x0

    return-object v0
.end method

.method public getScrollingViewProxy()LX/0g8;
    .locals 1

    .prologue
    .line 584201
    iget-object v0, p0, LX/3TN;->d:LX/2iI;

    return-object v0
.end method

.method public getSwipeRefreshLayout()Lcom/facebook/widget/FbSwipeRefreshLayout;
    .locals 1

    .prologue
    .line 584200
    iget-object v0, p0, LX/3TN;->f:Lcom/facebook/widget/FbSwipeRefreshLayout;

    return-object v0
.end method
