.class public final LX/3sP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3oQ;


# instance fields
.field public a:LX/3sU;


# direct methods
.method public constructor <init>(LX/3sU;)V
    .locals 0

    .prologue
    .line 643915
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 643916
    iput-object p1, p0, LX/3sP;->a:LX/3sU;

    .line 643917
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 643886
    iget-object v0, p0, LX/3sP;->a:LX/3sU;

    iget v0, v0, LX/3sU;->e:I

    if-ltz v0, :cond_0

    .line 643887
    const/4 v0, 0x2

    invoke-static {p1, v0, v1}, LX/0vv;->a(Landroid/view/View;ILandroid/graphics/Paint;)V

    .line 643888
    :cond_0
    iget-object v0, p0, LX/3sP;->a:LX/3sU;

    iget-object v0, v0, LX/3sU;->c:Ljava/lang/Runnable;

    if-eqz v0, :cond_1

    .line 643889
    iget-object v0, p0, LX/3sP;->a:LX/3sU;

    iget-object v0, v0, LX/3sU;->c:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 643890
    :cond_1
    const/high16 v0, 0x7e000000

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    .line 643891
    instance-of v2, v0, LX/3oQ;

    if-eqz v2, :cond_3

    .line 643892
    check-cast v0, LX/3oQ;

    .line 643893
    :goto_0
    if-eqz v0, :cond_2

    .line 643894
    invoke-interface {v0, p1}, LX/3oQ;->a(Landroid/view/View;)V

    .line 643895
    :cond_2
    return-void

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method

.method public final b(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 643903
    iget-object v0, p0, LX/3sP;->a:LX/3sU;

    iget v0, v0, LX/3sU;->e:I

    if-ltz v0, :cond_0

    .line 643904
    iget-object v0, p0, LX/3sP;->a:LX/3sU;

    iget v0, v0, LX/3sU;->e:I

    invoke-static {p1, v0, v1}, LX/0vv;->a(Landroid/view/View;ILandroid/graphics/Paint;)V

    .line 643905
    iget-object v0, p0, LX/3sP;->a:LX/3sU;

    const/4 v2, -0x1

    .line 643906
    iput v2, v0, LX/3sU;->e:I

    .line 643907
    :cond_0
    iget-object v0, p0, LX/3sP;->a:LX/3sU;

    iget-object v0, v0, LX/3sU;->d:Ljava/lang/Runnable;

    if-eqz v0, :cond_1

    .line 643908
    iget-object v0, p0, LX/3sP;->a:LX/3sU;

    iget-object v0, v0, LX/3sU;->d:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 643909
    :cond_1
    const/high16 v0, 0x7e000000

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    .line 643910
    instance-of v2, v0, LX/3oQ;

    if-eqz v2, :cond_3

    .line 643911
    check-cast v0, LX/3oQ;

    .line 643912
    :goto_0
    if-eqz v0, :cond_2

    .line 643913
    invoke-interface {v0, p1}, LX/3oQ;->b(Landroid/view/View;)V

    .line 643914
    :cond_2
    return-void

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method

.method public final c(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 643896
    const/high16 v0, 0x7e000000

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    .line 643897
    const/4 v1, 0x0

    .line 643898
    instance-of v2, v0, LX/3oQ;

    if-eqz v2, :cond_1

    .line 643899
    check-cast v0, LX/3oQ;

    .line 643900
    :goto_0
    if-eqz v0, :cond_0

    .line 643901
    invoke-interface {v0, p1}, LX/3oQ;->c(Landroid/view/View;)V

    .line 643902
    :cond_0
    return-void

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method
