.class public LX/4TP;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 718179
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 11

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 718180
    const/4 v2, 0x0

    .line 718181
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v4, :cond_9

    .line 718182
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 718183
    :goto_0
    return v0

    .line 718184
    :cond_0
    const-string v9, "survey_flow_type"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 718185
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyFlowType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLStructuredSurveyFlowType;

    move-result-object v0

    move-object v3, v0

    move v0, v1

    .line 718186
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_7

    .line 718187
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 718188
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 718189
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_1

    if-eqz v8, :cond_1

    .line 718190
    const-string v9, "firstQuestion"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 718191
    invoke-static {p0, p1}, LX/4TR;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 718192
    :cond_2
    const-string v9, "id"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 718193
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 718194
    :cond_3
    const-string v9, "name"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 718195
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 718196
    :cond_4
    const-string v9, "structured_questions"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 718197
    invoke-static {p0, p1}, LX/4TR;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 718198
    :cond_5
    const-string v9, "url"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 718199
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 718200
    :cond_6
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 718201
    :cond_7
    const/4 v8, 0x7

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 718202
    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 718203
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 718204
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 718205
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 718206
    if-eqz v0, :cond_8

    .line 718207
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v3}, LX/186;->a(ILjava/lang/Enum;)V

    .line 718208
    :cond_8
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 718209
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :cond_9
    move-object v3, v2

    move v4, v0

    move v5, v0

    move v6, v0

    move v7, v0

    move v2, v0

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x5

    .line 718210
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 718211
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 718212
    if-eqz v0, :cond_0

    .line 718213
    const-string v1, "firstQuestion"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 718214
    invoke-static {p0, v0, p2, p3}, LX/4TR;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 718215
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 718216
    if-eqz v0, :cond_1

    .line 718217
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 718218
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 718219
    :cond_1
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 718220
    if-eqz v0, :cond_2

    .line 718221
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 718222
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 718223
    :cond_2
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 718224
    if-eqz v0, :cond_3

    .line 718225
    const-string v1, "structured_questions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 718226
    invoke-static {p0, v0, p2, p3}, LX/4TR;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 718227
    :cond_3
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v2, v0}, LX/15i;->a(IIS)S

    move-result v0

    .line 718228
    if-eqz v0, :cond_4

    .line 718229
    const-string v0, "survey_flow_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 718230
    const-class v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyFlowType;

    invoke-virtual {p0, p1, v2, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyFlowType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyFlowType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 718231
    :cond_4
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 718232
    if-eqz v0, :cond_5

    .line 718233
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 718234
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 718235
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 718236
    return-void
.end method
