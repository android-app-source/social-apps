.class public LX/4TO;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 718119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 17

    .prologue
    .line 718120
    const/4 v13, 0x0

    .line 718121
    const/4 v12, 0x0

    .line 718122
    const/4 v11, 0x0

    .line 718123
    const/4 v10, 0x0

    .line 718124
    const/4 v9, 0x0

    .line 718125
    const/4 v8, 0x0

    .line 718126
    const/4 v7, 0x0

    .line 718127
    const/4 v6, 0x0

    .line 718128
    const/4 v5, 0x0

    .line 718129
    const/4 v4, 0x0

    .line 718130
    const/4 v3, 0x0

    .line 718131
    const/4 v2, 0x0

    .line 718132
    const/4 v1, 0x0

    .line 718133
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->START_OBJECT:LX/15z;

    if-eq v14, v15, :cond_1

    .line 718134
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 718135
    const/4 v1, 0x0

    .line 718136
    :goto_0
    return v1

    .line 718137
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 718138
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->END_OBJECT:LX/15z;

    if-eq v14, v15, :cond_a

    .line 718139
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v14

    .line 718140
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 718141
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v15

    sget-object v16, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v16

    if-eq v15, v0, :cond_1

    if-eqz v14, :cond_1

    .line 718142
    const-string v15, "allow_write_in_response"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_2

    .line 718143
    const/4 v4, 0x1

    .line 718144
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v13

    goto :goto_1

    .line 718145
    :cond_2
    const-string v15, "body"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 718146
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v12

    goto :goto_1

    .line 718147
    :cond_3
    const-string v15, "custom_question_type"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_4

    .line 718148
    const/4 v3, 0x1

    .line 718149
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    move-result-object v11

    goto :goto_1

    .line 718150
    :cond_4
    const-string v15, "is_required"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_5

    .line 718151
    const/4 v2, 0x1

    .line 718152
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v10

    goto :goto_1

    .line 718153
    :cond_5
    const-string v15, "message"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_6

    .line 718154
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 718155
    :cond_6
    const-string v15, "question_class"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_7

    .line 718156
    const/4 v1, 0x1

    .line 718157
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    move-result-object v8

    goto :goto_1

    .line 718158
    :cond_7
    const-string v15, "question_id"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_8

    .line 718159
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto/16 :goto_1

    .line 718160
    :cond_8
    const-string v15, "response_options"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_9

    .line 718161
    invoke-static/range {p0 .. p1}, LX/4TS;->a(LX/15w;LX/186;)I

    move-result v6

    goto/16 :goto_1

    .line 718162
    :cond_9
    const-string v15, "subquestion_labels"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_0

    .line 718163
    invoke-static/range {p0 .. p1}, LX/2aE;->b(LX/15w;LX/186;)I

    move-result v5

    goto/16 :goto_1

    .line 718164
    :cond_a
    const/16 v14, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->c(I)V

    .line 718165
    if-eqz v4, :cond_b

    .line 718166
    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v13}, LX/186;->a(IZ)V

    .line 718167
    :cond_b
    const/4 v4, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v12}, LX/186;->b(II)V

    .line 718168
    if-eqz v3, :cond_c

    .line 718169
    const/4 v3, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, LX/186;->a(ILjava/lang/Enum;)V

    .line 718170
    :cond_c
    if-eqz v2, :cond_d

    .line 718171
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->a(IZ)V

    .line 718172
    :cond_d
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 718173
    if-eqz v1, :cond_e

    .line 718174
    const/4 v1, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v8}, LX/186;->a(ILjava/lang/Enum;)V

    .line 718175
    :cond_e
    const/4 v1, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 718176
    const/4 v1, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 718177
    const/16 v1, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 718178
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0
.end method
