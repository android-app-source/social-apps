.class public abstract LX/3i9;
.super Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;
.source ""

# interfaces
.implements LX/35p;
.implements LX/2eZ;


# instance fields
.field public a:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 628834
    invoke-direct {p0, p1}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;-><init>(Landroid/content/Context;)V

    .line 628835
    return-void
.end method


# virtual methods
.method public getActionButton()Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;
    .locals 1

    .prologue
    .line 628836
    const v0, 0x7f0d1cc8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    return-object v0
.end method

.method public getCallToActionView()Landroid/view/View;
    .locals 1

    .prologue
    .line 628837
    const v0, 0x7f0d1cc8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getItemPageIndex()I
    .locals 1

    .prologue
    .line 628838
    iget v0, p0, LX/3i9;->a:I

    return v0
.end method

.method public setActionButtonOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 628839
    const v0, 0x7f0d1cc8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    .line 628840
    iget-object p0, v0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->a:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    move-object v0, p0

    .line 628841
    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 628842
    return-void
.end method
