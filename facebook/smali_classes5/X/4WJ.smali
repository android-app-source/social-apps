.class public final LX/4WJ;
.super LX/40T;
.source ""

# interfaces
.implements LX/40U;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/40T;",
        "LX/40U",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFeedback;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(LX/4VK;)V
    .locals 0

    .prologue
    .line 751220
    invoke-direct {p0, p1}, LX/40T;-><init>(LX/4VK;)V

    .line 751221
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;)V
    .locals 2

    .prologue
    .line 751222
    iget-object v0, p0, LX/40T;->a:LX/4VK;

    const-string v1, "likers"

    invoke-virtual {v0, v1, p1}, LX/4VK;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 751223
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;)V
    .locals 2

    .prologue
    .line 751224
    iget-object v0, p0, LX/40T;->a:LX/4VK;

    const-string v1, "top_level_comments"

    invoke-virtual {v0, v1, p1}, LX/4VK;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 751225
    return-void
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 751226
    iget-object v0, p0, LX/40T;->a:LX/4VK;

    const-string v1, "does_viewer_like"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4VK;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 751227
    return-void
.end method
