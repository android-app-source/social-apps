.class public LX/3ed;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/32Z;


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Ljava/util/Deque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Deque",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public c:Landroid/database/sqlite/SQLiteStatement;

.field public d:Z

.field private final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 620409
    const-class v0, LX/3ed;

    sput-object v0, LX/3ed;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 620410
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 620411
    new-instance v0, Ljava/util/ArrayDeque;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/ArrayDeque;-><init>(I)V

    iput-object v0, p0, LX/3ed;->b:Ljava/util/Deque;

    .line 620412
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/3ed;->e:Ljava/util/Map;

    .line 620413
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 620414
    iget-object v0, p0, LX/3ed;->c:Landroid/database/sqlite/SQLiteStatement;

    if-eqz v0, :cond_0

    .line 620415
    iget-object v0, p0, LX/3ed;->c:Landroid/database/sqlite/SQLiteStatement;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 620416
    const/4 v0, 0x0

    iput-object v0, p0, LX/3ed;->c:Landroid/database/sqlite/SQLiteStatement;

    .line 620417
    :cond_0
    iget-object v0, p0, LX/3ed;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 620418
    return-void
.end method

.method public final a(Ljava/io/File;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 620419
    iget-object v0, p0, LX/3ed;->b:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->size()I

    move-result v0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_0

    .line 620420
    sget-object v0, LX/3ed;->a:Ljava/lang/Class;

    const-string v1, "unexpected directory %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 620421
    :cond_0
    iget-boolean v0, p0, LX/3ed;->d:Z

    if-eqz v0, :cond_1

    .line 620422
    iput-boolean v4, p0, LX/3ed;->d:Z

    .line 620423
    :goto_0
    return-void

    .line 620424
    :cond_1
    iget-object v0, p0, LX/3ed;->b:Ljava/util/Deque;

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Deque;->push(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final b(Ljava/io/File;)V
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 620425
    iget-object v0, p0, LX/3ed;->b:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->size()I

    move-result v0

    if-eq v0, v10, :cond_0

    .line 620426
    sget-object v0, LX/3ed;->a:Ljava/lang/Class;

    const-string v1, "unexpected file %s"

    new-array v2, v9, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v8

    invoke-static {v0, v1, v2}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 620427
    :goto_0
    return-void

    .line 620428
    :cond_0
    const/16 v0, 0x2e

    invoke-static {v0}, LX/2Cb;->on(C)LX/2Cb;

    move-result-object v0

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2Cb;->split(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v1

    .line 620429
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v10, :cond_1

    invoke-interface {v1, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LX/3e1;->isDbString(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 620430
    :cond_1
    sget-object v0, LX/3ed;->a:Ljava/lang/Class;

    const-string v1, "unexpected file name %s"

    new-array v2, v9, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v8

    invoke-static {v0, v1, v2}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 620431
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, LX/3ed;->b:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v1, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 620432
    iget-object v0, p0, LX/3ed;->e:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 620433
    iget-object v0, p0, LX/3ed;->e:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {p1}, Ljava/io/File;->lastModified()J

    move-result-wide v6

    cmp-long v0, v4, v6

    if-lez v0, :cond_3

    .line 620434
    sget-object v0, LX/3ed;->a:Ljava/lang/Class;

    const-string v2, "A more recent file was found for sticker id %s asset type %s, using that file."

    new-array v3, v10, [Ljava/lang/Object;

    iget-object v4, p0, LX/3ed;->b:Ljava/util/Deque;

    invoke-interface {v4}, Ljava/util/Deque;->getFirst()Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v3, v8

    invoke-interface {v1, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    aput-object v1, v3, v9

    invoke-static {v0, v2, v3}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 620435
    :cond_3
    sget-object v0, LX/3ed;->a:Ljava/lang/Class;

    const-string v3, "Replacing less recent file for sticker id %s asset type %s."

    new-array v4, v10, [Ljava/lang/Object;

    iget-object v5, p0, LX/3ed;->b:Ljava/util/Deque;

    invoke-interface {v5}, Ljava/util/Deque;->getFirst()Ljava/lang/Object;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-interface {v1, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    aput-object v5, v4, v9

    invoke-static {v0, v3, v4}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 620436
    :cond_4
    iget-object v0, p0, LX/3ed;->c:Landroid/database/sqlite/SQLiteStatement;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    .line 620437
    iget-object v3, p0, LX/3ed;->c:Landroid/database/sqlite/SQLiteStatement;

    iget-object v0, p0, LX/3ed;->b:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v9, v0}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 620438
    iget-object v3, p0, LX/3ed;->c:Landroid/database/sqlite/SQLiteStatement;

    iget-object v0, p0, LX/3ed;->b:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v10, v0}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 620439
    iget-object v3, p0, LX/3ed;->c:Landroid/database/sqlite/SQLiteStatement;

    const/4 v4, 0x3

    invoke-interface {v1, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v4, v0}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 620440
    iget-object v3, p0, LX/3ed;->c:Landroid/database/sqlite/SQLiteStatement;

    const/4 v4, 0x4

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v0, "image/"

    invoke-direct {v5, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 620441
    iget-object v0, p0, LX/3ed;->c:Landroid/database/sqlite/SQLiteStatement;

    const/4 v3, 0x5

    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 620442
    iget-object v0, p0, LX/3ed;->c:Landroid/database/sqlite/SQLiteStatement;

    const v3, 0x112a9b81

    invoke-static {v3}, LX/03h;->a(I)V

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteStatement;->execute()V

    const v0, 0xd80470

    invoke-static {v0}, LX/03h;->a(I)V

    .line 620443
    sget-object v0, LX/3ed;->a:Ljava/lang/Class;

    const-string v3, "Inserted %s asset for sticker id %s into db."

    new-array v4, v10, [Ljava/lang/Object;

    invoke-interface {v1, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    aput-object v1, v4, v8

    iget-object v1, p0, LX/3ed;->b:Ljava/util/Deque;

    invoke-interface {v1}, Ljava/util/Deque;->getFirst()Ljava/lang/Object;

    move-result-object v1

    aput-object v1, v4, v9

    invoke-static {v0, v3, v4}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 620444
    iget-object v0, p0, LX/3ed;->e:Ljava/util/Map;

    invoke-virtual {p1}, Ljava/io/File;->lastModified()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0
.end method

.method public final c(Ljava/io/File;)V
    .locals 1

    .prologue
    .line 620445
    iget-object v0, p0, LX/3ed;->b:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 620446
    iget-object v0, p0, LX/3ed;->b:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->pop()Ljava/lang/Object;

    .line 620447
    :cond_0
    return-void
.end method
