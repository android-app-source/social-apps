.class public final LX/51f;
.super LX/4wo;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LX/4wo",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field private b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 825342
    invoke-direct {p0}, LX/4wo;-><init>()V

    .line 825343
    iput-object p1, p0, LX/51f;->a:Ljava/util/Map;

    .line 825344
    return-void
.end method

.method public static a(Ljava/util/Map;)LX/51f;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Map",
            "<TK;TV;>;)",
            "LX/51f",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 825345
    new-instance v0, LX/51f;

    invoke-direct {v0, p0}, LX/51f;-><init>(Ljava/util/Map;)V

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 825346
    iget-object v0, p0, LX/51f;->a:Ljava/util/Map;

    return-object v0
.end method

.method public final synthetic e()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 825347
    invoke-virtual {p0}, LX/51f;->a()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final entrySet()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 825348
    iget-object v0, p0, LX/51f;->b:Ljava/util/Set;

    .line 825349
    if-eqz v0, :cond_0

    .line 825350
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/51e;

    invoke-direct {v0, p0}, LX/51e;-><init>(LX/51f;)V

    iput-object v0, p0, LX/51f;->b:Ljava/util/Set;

    goto :goto_0
.end method
