.class public final LX/3mI;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/3mD;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/1Po;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public b:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/GroupsYouShouldJoinFeedUnit;",
            ">;"
        }
    .end annotation
.end field

.field public final synthetic c:LX/3mD;


# direct methods
.method public constructor <init>(LX/3mD;)V
    .locals 1

    .prologue
    .line 636131
    iput-object p1, p0, LX/3mI;->c:LX/3mD;

    .line 636132
    move-object v0, p1

    .line 636133
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 636134
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 636135
    const-string v0, "GroupsYouShouldJoinHScrollComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 636136
    if-ne p0, p1, :cond_1

    .line 636137
    :cond_0
    :goto_0
    return v0

    .line 636138
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 636139
    goto :goto_0

    .line 636140
    :cond_3
    check-cast p1, LX/3mI;

    .line 636141
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 636142
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 636143
    if-eq v2, v3, :cond_0

    .line 636144
    iget-object v2, p0, LX/3mI;->a:LX/1Po;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/3mI;->a:LX/1Po;

    iget-object v3, p1, LX/3mI;->a:LX/1Po;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 636145
    goto :goto_0

    .line 636146
    :cond_5
    iget-object v2, p1, LX/3mI;->a:LX/1Po;

    if-nez v2, :cond_4

    .line 636147
    :cond_6
    iget-object v2, p0, LX/3mI;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/3mI;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/3mI;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 636148
    goto :goto_0

    .line 636149
    :cond_7
    iget-object v2, p1, LX/3mI;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
