.class public final enum LX/3lw;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/3lw;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/3lw;

.field public static final enum DAILYDIALOGUE_LIGHTWEIGHT_HEADER_TAPPED:LX/3lw;

.field public static final enum GOODWILL_COMPOSER_CANCEL:LX/3lw;

.field public static final enum GOODWILL_COMPOSER_FINAL_STEP:LX/3lw;

.field public static final enum GOODWILL_COMPOSER_LAUNCH:LX/3lw;

.field public static final enum GOODWILL_COMPOSER_POST_FAILED:LX/3lw;

.field public static final enum GOODWILL_COMPOSER_POST_SUBMITTED:LX/3lw;

.field public static final enum GOODWILL_COMPOSER_POST_SUBMITTING:LX/3lw;

.field public static final enum GOODWILL_DAILY_DIALOGUE_CTA_CLICKED:LX/3lw;

.field public static final enum GOODWILL_DAILY_DIALOGUE_GOOD_MORNING_DISMISS:LX/3lw;

.field public static final enum GOODWILL_DD_GREETING_MISMATCH:LX/3lw;

.field public static final enum GOODWILL_PTR_UNIT_DECODE_FAILED:LX/3lw;

.field public static final enum GOODWILL_THROWBACK_ADD_PROFILE_FRAME_CLICK:LX/3lw;

.field public static final enum GOODWILL_THROWBACK_MESSAGE_COMPOSER_OPEN:LX/3lw;

.field public static final enum GOODWILL_THROWBACK_RESHARE_PROMOTION_SEE_ORIGINAL_POST:LX/3lw;

.field public static final enum GOODWILL_THROWBACK_SHARE_COMPOSER_OPEN:LX/3lw;

.field public static final enum GOODWILL_THROWBACK_SHARE_COMPOSER_POST:LX/3lw;

.field public static final enum GOODWILL_THROWBACK_SHARE_MENU_OPEN:LX/3lw;

.field public static final enum GOODWILL_THROWBACK_SHARE_MESSAGE_OPEN:LX/3lw;

.field public static final enum GOODWILL_VIDEO_SHARE_COMPOSER_OPENED:LX/3lw;

.field public static final enum THROWBACK_CHANGED_NOTIFICATION_SUBSCRIPTION:LX/3lw;

.field public static final enum THROWBACK_NOTIFICATION_MEGAPHONE_DISMISSED:LX/3lw;


# instance fields
.field public final name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 635256
    new-instance v0, LX/3lw;

    const-string v1, "THROWBACK_CHANGED_NOTIFICATION_SUBSCRIPTION"

    const-string v2, "goodwill_throwback_notification_subscription_change"

    invoke-direct {v0, v1, v4, v2}, LX/3lw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3lw;->THROWBACK_CHANGED_NOTIFICATION_SUBSCRIPTION:LX/3lw;

    .line 635257
    new-instance v0, LX/3lw;

    const-string v1, "THROWBACK_NOTIFICATION_MEGAPHONE_DISMISSED"

    const-string v2, "goodwill_throwback_nux_megaphone_dismiss"

    invoke-direct {v0, v1, v5, v2}, LX/3lw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3lw;->THROWBACK_NOTIFICATION_MEGAPHONE_DISMISSED:LX/3lw;

    .line 635258
    new-instance v0, LX/3lw;

    const-string v1, "GOODWILL_COMPOSER_LAUNCH"

    const-string v2, "goodwill_campaign_viewed_preview"

    invoke-direct {v0, v1, v6, v2}, LX/3lw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3lw;->GOODWILL_COMPOSER_LAUNCH:LX/3lw;

    .line 635259
    new-instance v0, LX/3lw;

    const-string v1, "GOODWILL_COMPOSER_CANCEL"

    const-string v2, "goodwill_campaign_dismissed_preview"

    invoke-direct {v0, v1, v7, v2}, LX/3lw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3lw;->GOODWILL_COMPOSER_CANCEL:LX/3lw;

    .line 635260
    new-instance v0, LX/3lw;

    const-string v1, "GOODWILL_COMPOSER_FINAL_STEP"

    const-string v2, "goodwill_campaign_final_step_composer"

    invoke-direct {v0, v1, v8, v2}, LX/3lw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3lw;->GOODWILL_COMPOSER_FINAL_STEP:LX/3lw;

    .line 635261
    new-instance v0, LX/3lw;

    const-string v1, "GOODWILL_COMPOSER_POST_SUBMITTING"

    const/4 v2, 0x5

    const-string v3, "goodwill_campaign_post_submitting"

    invoke-direct {v0, v1, v2, v3}, LX/3lw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3lw;->GOODWILL_COMPOSER_POST_SUBMITTING:LX/3lw;

    .line 635262
    new-instance v0, LX/3lw;

    const-string v1, "GOODWILL_COMPOSER_POST_SUBMITTED"

    const/4 v2, 0x6

    const-string v3, "goodwill_campaign_post_submitted"

    invoke-direct {v0, v1, v2, v3}, LX/3lw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3lw;->GOODWILL_COMPOSER_POST_SUBMITTED:LX/3lw;

    .line 635263
    new-instance v0, LX/3lw;

    const-string v1, "GOODWILL_COMPOSER_POST_FAILED"

    const/4 v2, 0x7

    const-string v3, "goodwill_campaign_post_failed"

    invoke-direct {v0, v1, v2, v3}, LX/3lw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3lw;->GOODWILL_COMPOSER_POST_FAILED:LX/3lw;

    .line 635264
    new-instance v0, LX/3lw;

    const-string v1, "GOODWILL_DAILY_DIALOGUE_GOOD_MORNING_DISMISS"

    const/16 v2, 0x8

    const-string v3, "goodwill_dailydialogue_goodmorning_dismiss"

    invoke-direct {v0, v1, v2, v3}, LX/3lw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3lw;->GOODWILL_DAILY_DIALOGUE_GOOD_MORNING_DISMISS:LX/3lw;

    .line 635265
    new-instance v0, LX/3lw;

    const-string v1, "GOODWILL_DAILY_DIALOGUE_CTA_CLICKED"

    const/16 v2, 0x9

    const-string v3, "goodwill_dailydialogue_cta_clicked"

    invoke-direct {v0, v1, v2, v3}, LX/3lw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3lw;->GOODWILL_DAILY_DIALOGUE_CTA_CLICKED:LX/3lw;

    .line 635266
    new-instance v0, LX/3lw;

    const-string v1, "GOODWILL_THROWBACK_SHARE_COMPOSER_OPEN"

    const/16 v2, 0xa

    const-string v3, "goodwill_throwback_share_composer_open"

    invoke-direct {v0, v1, v2, v3}, LX/3lw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3lw;->GOODWILL_THROWBACK_SHARE_COMPOSER_OPEN:LX/3lw;

    .line 635267
    new-instance v0, LX/3lw;

    const-string v1, "GOODWILL_THROWBACK_SHARE_COMPOSER_POST"

    const/16 v2, 0xb

    const-string v3, "goodwill_throwback_share_composer_post"

    invoke-direct {v0, v1, v2, v3}, LX/3lw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3lw;->GOODWILL_THROWBACK_SHARE_COMPOSER_POST:LX/3lw;

    .line 635268
    new-instance v0, LX/3lw;

    const-string v1, "GOODWILL_THROWBACK_SHARE_MESSAGE_OPEN"

    const/16 v2, 0xc

    const-string v3, "goodwill_throwback_share_message_open"

    invoke-direct {v0, v1, v2, v3}, LX/3lw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3lw;->GOODWILL_THROWBACK_SHARE_MESSAGE_OPEN:LX/3lw;

    .line 635269
    new-instance v0, LX/3lw;

    const-string v1, "GOODWILL_THROWBACK_MESSAGE_COMPOSER_OPEN"

    const/16 v2, 0xd

    const-string v3, "goodwill_throwback_message_composer_open"

    invoke-direct {v0, v1, v2, v3}, LX/3lw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3lw;->GOODWILL_THROWBACK_MESSAGE_COMPOSER_OPEN:LX/3lw;

    .line 635270
    new-instance v0, LX/3lw;

    const-string v1, "GOODWILL_THROWBACK_SHARE_MENU_OPEN"

    const/16 v2, 0xe

    const-string v3, "goodwill_throwback_share_menu_open"

    invoke-direct {v0, v1, v2, v3}, LX/3lw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3lw;->GOODWILL_THROWBACK_SHARE_MENU_OPEN:LX/3lw;

    .line 635271
    new-instance v0, LX/3lw;

    const-string v1, "GOODWILL_THROWBACK_ADD_PROFILE_FRAME_CLICK"

    const/16 v2, 0xf

    const-string v3, "goodwill_throwback_add_profile_frame_click"

    invoke-direct {v0, v1, v2, v3}, LX/3lw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3lw;->GOODWILL_THROWBACK_ADD_PROFILE_FRAME_CLICK:LX/3lw;

    .line 635272
    new-instance v0, LX/3lw;

    const-string v1, "GOODWILL_VIDEO_SHARE_COMPOSER_OPENED"

    const/16 v2, 0x10

    const-string v3, "goodwill_video_share_composer_opened"

    invoke-direct {v0, v1, v2, v3}, LX/3lw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3lw;->GOODWILL_VIDEO_SHARE_COMPOSER_OPENED:LX/3lw;

    .line 635273
    new-instance v0, LX/3lw;

    const-string v1, "GOODWILL_THROWBACK_RESHARE_PROMOTION_SEE_ORIGINAL_POST"

    const/16 v2, 0x11

    const-string v3, "goodwill_throwback_reshare_original_post_click"

    invoke-direct {v0, v1, v2, v3}, LX/3lw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3lw;->GOODWILL_THROWBACK_RESHARE_PROMOTION_SEE_ORIGINAL_POST:LX/3lw;

    .line 635274
    new-instance v0, LX/3lw;

    const-string v1, "DAILYDIALOGUE_LIGHTWEIGHT_HEADER_TAPPED"

    const/16 v2, 0x12

    const-string v3, "goodwill_dailydialogue_lightweight_header_tapped"

    invoke-direct {v0, v1, v2, v3}, LX/3lw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3lw;->DAILYDIALOGUE_LIGHTWEIGHT_HEADER_TAPPED:LX/3lw;

    .line 635275
    new-instance v0, LX/3lw;

    const-string v1, "GOODWILL_DD_GREETING_MISMATCH"

    const/16 v2, 0x13

    const-string v3, "goodwill_dd_greeting_mismatch"

    invoke-direct {v0, v1, v2, v3}, LX/3lw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3lw;->GOODWILL_DD_GREETING_MISMATCH:LX/3lw;

    .line 635276
    new-instance v0, LX/3lw;

    const-string v1, "GOODWILL_PTR_UNIT_DECODE_FAILED"

    const/16 v2, 0x14

    const-string v3, "goodwill_ptr_unit_decode_failed"

    invoke-direct {v0, v1, v2, v3}, LX/3lw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3lw;->GOODWILL_PTR_UNIT_DECODE_FAILED:LX/3lw;

    .line 635277
    const/16 v0, 0x15

    new-array v0, v0, [LX/3lw;

    sget-object v1, LX/3lw;->THROWBACK_CHANGED_NOTIFICATION_SUBSCRIPTION:LX/3lw;

    aput-object v1, v0, v4

    sget-object v1, LX/3lw;->THROWBACK_NOTIFICATION_MEGAPHONE_DISMISSED:LX/3lw;

    aput-object v1, v0, v5

    sget-object v1, LX/3lw;->GOODWILL_COMPOSER_LAUNCH:LX/3lw;

    aput-object v1, v0, v6

    sget-object v1, LX/3lw;->GOODWILL_COMPOSER_CANCEL:LX/3lw;

    aput-object v1, v0, v7

    sget-object v1, LX/3lw;->GOODWILL_COMPOSER_FINAL_STEP:LX/3lw;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/3lw;->GOODWILL_COMPOSER_POST_SUBMITTING:LX/3lw;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/3lw;->GOODWILL_COMPOSER_POST_SUBMITTED:LX/3lw;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/3lw;->GOODWILL_COMPOSER_POST_FAILED:LX/3lw;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/3lw;->GOODWILL_DAILY_DIALOGUE_GOOD_MORNING_DISMISS:LX/3lw;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/3lw;->GOODWILL_DAILY_DIALOGUE_CTA_CLICKED:LX/3lw;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/3lw;->GOODWILL_THROWBACK_SHARE_COMPOSER_OPEN:LX/3lw;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/3lw;->GOODWILL_THROWBACK_SHARE_COMPOSER_POST:LX/3lw;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/3lw;->GOODWILL_THROWBACK_SHARE_MESSAGE_OPEN:LX/3lw;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/3lw;->GOODWILL_THROWBACK_MESSAGE_COMPOSER_OPEN:LX/3lw;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/3lw;->GOODWILL_THROWBACK_SHARE_MENU_OPEN:LX/3lw;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/3lw;->GOODWILL_THROWBACK_ADD_PROFILE_FRAME_CLICK:LX/3lw;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/3lw;->GOODWILL_VIDEO_SHARE_COMPOSER_OPENED:LX/3lw;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/3lw;->GOODWILL_THROWBACK_RESHARE_PROMOTION_SEE_ORIGINAL_POST:LX/3lw;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/3lw;->DAILYDIALOGUE_LIGHTWEIGHT_HEADER_TAPPED:LX/3lw;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/3lw;->GOODWILL_DD_GREETING_MISMATCH:LX/3lw;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/3lw;->GOODWILL_PTR_UNIT_DECODE_FAILED:LX/3lw;

    aput-object v2, v0, v1

    sput-object v0, LX/3lw;->$VALUES:[LX/3lw;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 635253
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 635254
    iput-object p3, p0, LX/3lw;->name:Ljava/lang/String;

    .line 635255
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/3lw;
    .locals 1

    .prologue
    .line 635251
    const-class v0, LX/3lw;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/3lw;

    return-object v0
.end method

.method public static values()[LX/3lw;
    .locals 1

    .prologue
    .line 635252
    sget-object v0, LX/3lw;->$VALUES:[LX/3lw;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/3lw;

    return-object v0
.end method
