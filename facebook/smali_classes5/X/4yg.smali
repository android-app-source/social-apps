.class public final LX/4yg;
.super LX/0P2;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LX/0P2",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field private final e:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<-TK;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Comparator;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Comparator",
            "<-TK;>;)V"
        }
    .end annotation

    .prologue
    .line 821955
    invoke-direct {p0}, LX/0P2;-><init>()V

    .line 821956
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Comparator;

    iput-object v0, p0, LX/4yg;->e:Ljava/util/Comparator;

    .line 821957
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Iterable;)LX/0P2;
    .locals 1
    .annotation build Lcom/google/common/annotations/Beta;
    .end annotation

    .prologue
    .line 821953
    invoke-super {p0, p1}, LX/0P2;->a(Ljava/lang/Iterable;)LX/0P2;

    .line 821954
    return-object p0
.end method

.method public final a(Ljava/util/Map$Entry;)LX/0P2;
    .locals 1

    .prologue
    .line 821951
    invoke-super {p0, p1}, LX/0P2;->a(Ljava/util/Map$Entry;)LX/0P2;

    .line 821952
    return-object p0
.end method

.method public final a(Ljava/util/Map;)LX/0P2;
    .locals 1

    .prologue
    .line 821949
    invoke-super {p0, p1}, LX/0P2;->a(Ljava/util/Map;)LX/0P2;

    .line 821950
    return-object p0
.end method

.method public final b()LX/0P1;
    .locals 4

    .prologue
    .line 821944
    const/4 v3, 0x0

    .line 821945
    iget v0, p0, LX/0P2;->c:I

    packed-switch v0, :pswitch_data_0

    .line 821946
    iget-object v0, p0, LX/4yg;->e:Ljava/util/Comparator;

    iget-object v1, p0, LX/0P2;->b:[LX/0P3;

    iget v2, p0, LX/0P2;->c:I

    invoke-static {v0, v3, v1, v2}, LX/146;->b(Ljava/util/Comparator;Z[Ljava/util/Map$Entry;I)LX/146;

    move-result-object v0

    :goto_0
    return-object v0

    .line 821947
    :pswitch_0
    iget-object v0, p0, LX/4yg;->e:Ljava/util/Comparator;

    invoke-static {v0}, LX/146;->a(Ljava/util/Comparator;)LX/146;

    move-result-object v0

    goto :goto_0

    .line 821948
    :pswitch_1
    iget-object v0, p0, LX/4yg;->e:Ljava/util/Comparator;

    iget-object v1, p0, LX/0P2;->b:[LX/0P3;

    aget-object v1, v1, v3

    invoke-virtual {v1}, LX/0P4;->getKey()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LX/0P2;->b:[LX/0P3;

    aget-object v2, v2, v3

    invoke-virtual {v2}, LX/0P4;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/146;->b(Ljava/util/Comparator;Ljava/lang/Object;Ljava/lang/Object;)LX/146;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;
    .locals 1

    .prologue
    .line 821942
    invoke-super {p0, p1, p2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 821943
    return-object p0
.end method
