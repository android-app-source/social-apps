.class public LX/3V4;
.super LX/3V5;
.source ""

# interfaces
.implements LX/24a;


# static fields
.field public static final m:LX/1Cz;


# instance fields
.field private final l:Landroid/widget/ImageView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 587508
    new-instance v0, LX/3V7;

    invoke-direct {v0}, LX/3V7;-><init>()V

    sput-object v0, LX/3V4;->m:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 587506
    const/4 v0, 0x0

    const v1, 0x7f030880

    invoke-direct {p0, p1, v0, v1}, LX/3V4;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 587507
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 587494
    const v0, 0x7f030880

    invoke-direct {p0, p1, p2, v0}, LX/3V4;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 587495
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 587503
    invoke-direct {p0, p1, p2, p3}, LX/3V5;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 587504
    const v0, 0x7f0d0bde

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/3V4;->l:Landroid/widget/ImageView;

    .line 587505
    return-void
.end method

.method private d()Z
    .locals 1

    .prologue
    .line 587502
    iget-object v0, p0, LX/3V4;->l:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0hs;)V
    .locals 1

    .prologue
    .line 587500
    iget-object v0, p0, LX/3V4;->l:Landroid/widget/ImageView;

    invoke-virtual {p1, v0}, LX/0ht;->f(Landroid/view/View;)V

    .line 587501
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 587499
    invoke-direct {p0}, LX/3V4;->d()Z

    move-result v0

    return v0
.end method

.method public setMenuButtonActive(Z)V
    .locals 2

    .prologue
    .line 587496
    iget-object v1, p0, LX/3V4;->l:Landroid/widget/ImageView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 587497
    return-void

    .line 587498
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method
