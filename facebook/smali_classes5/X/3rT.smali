.class public final LX/3rT;
.super Landroid/os/Handler;
.source ""


# instance fields
.field public final synthetic a:LX/3rU;


# direct methods
.method public constructor <init>(LX/3rU;)V
    .locals 0

    .prologue
    .line 643029
    iput-object p1, p0, LX/3rT;->a:LX/3rU;

    .line 643030
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 643031
    return-void
.end method

.method public constructor <init>(LX/3rU;Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 643032
    iput-object p1, p0, LX/3rT;->a:LX/3rU;

    .line 643033
    invoke-virtual {p2}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 643034
    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 643035
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 643036
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown message "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 643037
    :pswitch_0
    iget-object v0, p0, LX/3rT;->a:LX/3rU;

    iget-object v0, v0, LX/3rU;->i:Landroid/view/GestureDetector$OnGestureListener;

    iget-object v1, p0, LX/3rT;->a:LX/3rU;

    iget-object v1, v1, LX/3rU;->p:Landroid/view/MotionEvent;

    invoke-interface {v0, v1}, Landroid/view/GestureDetector$OnGestureListener;->onShowPress(Landroid/view/MotionEvent;)V

    .line 643038
    :cond_0
    :goto_0
    return-void

    .line 643039
    :pswitch_1
    iget-object v0, p0, LX/3rT;->a:LX/3rU;

    .line 643040
    iget-object v1, v0, LX/3rU;->h:Landroid/os/Handler;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 643041
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/3rU;->l:Z

    .line 643042
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/3rU;->m:Z

    .line 643043
    iget-object v1, v0, LX/3rU;->i:Landroid/view/GestureDetector$OnGestureListener;

    iget-object v2, v0, LX/3rU;->p:Landroid/view/MotionEvent;

    invoke-interface {v1, v2}, Landroid/view/GestureDetector$OnGestureListener;->onLongPress(Landroid/view/MotionEvent;)V

    .line 643044
    goto :goto_0

    .line 643045
    :pswitch_2
    iget-object v0, p0, LX/3rT;->a:LX/3rU;

    iget-object v0, v0, LX/3rU;->j:Landroid/view/GestureDetector$OnDoubleTapListener;

    if-eqz v0, :cond_0

    .line 643046
    iget-object v0, p0, LX/3rT;->a:LX/3rU;

    iget-boolean v0, v0, LX/3rU;->k:Z

    if-nez v0, :cond_1

    .line 643047
    iget-object v0, p0, LX/3rT;->a:LX/3rU;

    iget-object v0, v0, LX/3rU;->j:Landroid/view/GestureDetector$OnDoubleTapListener;

    iget-object v1, p0, LX/3rT;->a:LX/3rU;

    iget-object v1, v1, LX/3rU;->p:Landroid/view/MotionEvent;

    invoke-interface {v0, v1}, Landroid/view/GestureDetector$OnDoubleTapListener;->onSingleTapConfirmed(Landroid/view/MotionEvent;)Z

    goto :goto_0

    .line 643048
    :cond_1
    iget-object v0, p0, LX/3rT;->a:LX/3rU;

    const/4 v1, 0x1

    .line 643049
    iput-boolean v1, v0, LX/3rU;->l:Z

    .line 643050
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
