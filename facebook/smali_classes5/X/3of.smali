.class public final LX/3of;
.super LX/0vn;
.source ""


# instance fields
.field public final synthetic b:Landroid/support/design/widget/TextInputLayout;


# direct methods
.method public constructor <init>(Landroid/support/design/widget/TextInputLayout;)V
    .locals 0

    .prologue
    .line 640490
    iput-object p1, p0, LX/3of;->b:Landroid/support/design/widget/TextInputLayout;

    invoke-direct {p0}, LX/0vn;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/support/design/widget/TextInputLayout;B)V
    .locals 0

    .prologue
    .line 640500
    invoke-direct {p0, p1}, LX/3of;-><init>(Landroid/support/design/widget/TextInputLayout;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;LX/3sp;)V
    .locals 2

    .prologue
    .line 640501
    invoke-super {p0, p1, p2}, LX/0vn;->a(Landroid/view/View;LX/3sp;)V

    .line 640502
    const-class v0, Landroid/support/design/widget/TextInputLayout;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/3sp;->b(Ljava/lang/CharSequence;)V

    .line 640503
    iget-object v0, p0, LX/3of;->b:Landroid/support/design/widget/TextInputLayout;

    iget-object v0, v0, Landroid/support/design/widget/TextInputLayout;->q:LX/3o0;

    .line 640504
    iget-object v1, v0, LX/3o0;->x:Ljava/lang/CharSequence;

    move-object v0, v1

    .line 640505
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 640506
    invoke-virtual {p2, v0}, LX/3sp;->c(Ljava/lang/CharSequence;)V

    .line 640507
    :cond_0
    iget-object v0, p0, LX/3of;->b:Landroid/support/design/widget/TextInputLayout;

    iget-object v0, v0, Landroid/support/design/widget/TextInputLayout;->a:Landroid/widget/EditText;

    if-eqz v0, :cond_1

    .line 640508
    :cond_1
    iget-object v0, p0, LX/3of;->b:Landroid/support/design/widget/TextInputLayout;

    iget-object v0, v0, Landroid/support/design/widget/TextInputLayout;->f:Landroid/widget/TextView;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/3of;->b:Landroid/support/design/widget/TextInputLayout;

    iget-object v0, v0, Landroid/support/design/widget/TextInputLayout;->f:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 640509
    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 640510
    const/4 v1, 0x1

    .line 640511
    sget-object p0, LX/3sp;->a:LX/3sg;

    iget-object p1, p2, LX/3sp;->b:Ljava/lang/Object;

    invoke-interface {p0, p1, v1}, LX/3sg;->j(Ljava/lang/Object;Z)V

    .line 640512
    sget-object v1, LX/3sp;->a:LX/3sg;

    iget-object p0, p2, LX/3sp;->b:Ljava/lang/Object;

    invoke-interface {v1, p0, v0}, LX/3sg;->a(Ljava/lang/Object;Ljava/lang/CharSequence;)V

    .line 640513
    :cond_2
    return-void

    .line 640514
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 2

    .prologue
    .line 640494
    invoke-super {p0, p1, p2}, LX/0vn;->c(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    .line 640495
    iget-object v0, p0, LX/3of;->b:Landroid/support/design/widget/TextInputLayout;

    iget-object v0, v0, Landroid/support/design/widget/TextInputLayout;->q:LX/3o0;

    .line 640496
    iget-object v1, v0, LX/3o0;->x:Ljava/lang/CharSequence;

    move-object v0, v1

    .line 640497
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 640498
    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 640499
    :cond_0
    return-void
.end method

.method public final d(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    .prologue
    .line 640491
    invoke-super {p0, p1, p2}, LX/0vn;->d(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    .line 640492
    const-class v0, Landroid/support/design/widget/TextInputLayout;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 640493
    return-void
.end method
