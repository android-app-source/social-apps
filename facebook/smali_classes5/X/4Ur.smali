.class public final LX/4Ur;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult;",
        "Ljava/util/Map",
        "<",
        "Ljava/lang/String;",
        "TT;>;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 741769
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 741770
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 741771
    if-nez p1, :cond_0

    .line 741772
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 741773
    :goto_0
    return-object v0

    .line 741774
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/executor/GraphQLResult;->d()Ljava/util/Collection;

    move-result-object v0

    .line 741775
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v2

    .line 741776
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 741777
    if-eqz v1, :cond_1

    .line 741778
    instance-of v0, v1, LX/16i;

    if-nez v0, :cond_2

    .line 741779
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Not compatible with model types that don\'t have an ID"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    move-object v0, v1

    .line 741780
    check-cast v0, LX/16i;

    invoke-interface {v0}, LX/16i;->a()Ljava/lang/String;

    move-result-object v0

    .line 741781
    if-eqz v0, :cond_1

    .line 741782
    check-cast v1, LX/16i;

    invoke-virtual {v2, v0, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    goto :goto_1

    .line 741783
    :cond_3
    invoke-virtual {v2}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    goto :goto_0
.end method
