.class public LX/3xe;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3ot;


# instance fields
.field public final a:LX/3ow;

.field public final b:I

.field public c:Z

.field public final d:F

.field public final e:F

.field public final f:F

.field public final g:F

.field public final h:LX/1a1;

.field public final i:I

.field public j:Z

.field public k:F

.field public l:F

.field public m:Z

.field public final synthetic n:LX/3xm;

.field public o:F


# direct methods
.method public constructor <init>(LX/3xm;LX/1a1;IIFFFF)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 659735
    iput-object p1, p0, LX/3xe;->n:LX/3xm;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 659736
    iput-boolean v0, p0, LX/3xe;->m:Z

    .line 659737
    iput-boolean v0, p0, LX/3xe;->c:Z

    .line 659738
    iput p4, p0, LX/3xe;->i:I

    .line 659739
    iput p3, p0, LX/3xe;->b:I

    .line 659740
    iput-object p2, p0, LX/3xe;->h:LX/1a1;

    .line 659741
    iput p5, p0, LX/3xe;->d:F

    .line 659742
    iput p6, p0, LX/3xe;->e:F

    .line 659743
    iput p7, p0, LX/3xe;->f:F

    .line 659744
    iput p8, p0, LX/3xe;->g:F

    .line 659745
    sget-object v0, LX/3os;->a:LX/3ou;

    invoke-interface {v0}, LX/3ou;->a()LX/3ow;

    move-result-object v0

    move-object v0, v0

    .line 659746
    iput-object v0, p0, LX/3xe;->a:LX/3ow;

    .line 659747
    iget-object v0, p0, LX/3xe;->a:LX/3ow;

    new-instance v1, LX/3xl;

    invoke-direct {v1, p0, p1}, LX/3xl;-><init>(LX/3xe;LX/3xm;)V

    invoke-interface {v0, v1}, LX/3ow;->a(LX/3ov;)V

    .line 659748
    iget-object v0, p0, LX/3xe;->a:LX/3ow;

    iget-object v1, p2, LX/1a1;->a:Landroid/view/View;

    invoke-interface {v0, v1}, LX/3ow;->a(Landroid/view/View;)V

    .line 659749
    iget-object v0, p0, LX/3xe;->a:LX/3ow;

    invoke-interface {v0, p0}, LX/3ow;->a(LX/3ot;)V

    .line 659750
    const/4 v0, 0x0

    .line 659751
    iput v0, p0, LX/3xe;->o:F

    .line 659752
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 659753
    const/high16 v0, 0x3f800000    # 1.0f

    .line 659754
    iput v0, p0, LX/3xe;->o:F

    .line 659755
    return-void
.end method

.method public a(LX/3ow;)V
    .locals 1

    .prologue
    .line 659756
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/3xe;->c:Z

    .line 659757
    return-void
.end method

.method public final d()V
    .locals 4

    .prologue
    .line 659758
    iget v0, p0, LX/3xe;->d:F

    iget v1, p0, LX/3xe;->f:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 659759
    iget-object v0, p0, LX/3xe;->h:LX/1a1;

    iget-object v0, v0, LX/1a1;->a:Landroid/view/View;

    invoke-static {v0}, LX/0vv;->r(Landroid/view/View;)F

    move-result v0

    iput v0, p0, LX/3xe;->k:F

    .line 659760
    :goto_0
    iget v0, p0, LX/3xe;->e:F

    iget v1, p0, LX/3xe;->g:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    .line 659761
    iget-object v0, p0, LX/3xe;->h:LX/1a1;

    iget-object v0, v0, LX/1a1;->a:Landroid/view/View;

    invoke-static {v0}, LX/0vv;->s(Landroid/view/View;)F

    move-result v0

    iput v0, p0, LX/3xe;->l:F

    .line 659762
    :goto_1
    return-void

    .line 659763
    :cond_0
    iget v0, p0, LX/3xe;->d:F

    iget v1, p0, LX/3xe;->o:F

    iget v2, p0, LX/3xe;->f:F

    iget v3, p0, LX/3xe;->d:F

    sub-float/2addr v2, v3

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iput v0, p0, LX/3xe;->k:F

    goto :goto_0

    .line 659764
    :cond_1
    iget v0, p0, LX/3xe;->e:F

    iget v1, p0, LX/3xe;->o:F

    iget v2, p0, LX/3xe;->g:F

    iget v3, p0, LX/3xe;->e:F

    sub-float/2addr v2, v3

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iput v0, p0, LX/3xe;->l:F

    goto :goto_1
.end method
