.class public final LX/4wO;
.super LX/4wN;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LX/4wN",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field public volatile a:J

.field public b:LX/0R1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0R1",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public c:LX/0R1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0R1",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Object;ILX/0R1;)V
    .locals 2
    .param p3    # LX/0R1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;I",
            "LX/0R1",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 820086
    invoke-direct {p0, p1, p2, p3}, LX/4wN;-><init>(Ljava/lang/Object;ILX/0R1;)V

    .line 820087
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, LX/4wO;->a:J

    .line 820088
    sget-object v0, LX/0SZ;->INSTANCE:LX/0SZ;

    move-object v0, v0

    .line 820089
    iput-object v0, p0, LX/4wO;->b:LX/0R1;

    .line 820090
    sget-object v0, LX/0SZ;->INSTANCE:LX/0SZ;

    move-object v0, v0

    .line 820091
    iput-object v0, p0, LX/4wO;->c:LX/0R1;

    .line 820092
    return-void
.end method


# virtual methods
.method public final getAccessTime()J
    .locals 2

    .prologue
    .line 820101
    iget-wide v0, p0, LX/4wO;->a:J

    return-wide v0
.end method

.method public final getNextInAccessQueue()LX/0R1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0R1",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 820100
    iget-object v0, p0, LX/4wO;->b:LX/0R1;

    return-object v0
.end method

.method public final getPreviousInAccessQueue()LX/0R1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0R1",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 820099
    iget-object v0, p0, LX/4wO;->c:LX/0R1;

    return-object v0
.end method

.method public final setAccessTime(J)V
    .locals 1

    .prologue
    .line 820097
    iput-wide p1, p0, LX/4wO;->a:J

    .line 820098
    return-void
.end method

.method public final setNextInAccessQueue(LX/0R1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0R1",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 820095
    iput-object p1, p0, LX/4wO;->b:LX/0R1;

    .line 820096
    return-void
.end method

.method public final setPreviousInAccessQueue(LX/0R1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0R1",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 820093
    iput-object p1, p0, LX/4wO;->c:LX/0R1;

    .line 820094
    return-void
.end method
