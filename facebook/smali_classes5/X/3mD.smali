.class public LX/3mD;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3mK;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/3mD",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/3mK;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 635542
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 635543
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/3mD;->b:LX/0Zi;

    .line 635544
    iput-object p1, p0, LX/3mD;->a:LX/0Ot;

    .line 635545
    return-void
.end method

.method public static a(LX/0QB;)LX/3mD;
    .locals 4

    .prologue
    .line 635546
    const-class v1, LX/3mD;

    monitor-enter v1

    .line 635547
    :try_start_0
    sget-object v0, LX/3mD;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 635548
    sput-object v2, LX/3mD;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 635549
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 635550
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 635551
    new-instance v3, LX/3mD;

    const/16 p0, 0x8a3

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/3mD;-><init>(LX/0Ot;)V

    .line 635552
    move-object v0, v3

    .line 635553
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 635554
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/3mD;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 635555
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 635556
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 10

    .prologue
    .line 635557
    check-cast p2, LX/3mI;

    .line 635558
    iget-object v0, p0, LX/3mD;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3mK;

    iget-object v1, p2, LX/3mI;->a:LX/1Po;

    iget-object v2, p2, LX/3mI;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 635559
    new-instance v4, LX/3mO;

    invoke-direct {v4, v0, v2}, LX/3mO;-><init>(LX/3mK;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 635560
    invoke-static {}, LX/3mP;->newBuilder()LX/3mP;

    move-result-object v5

    .line 635561
    iget-object v3, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 635562
    check-cast v3, Lcom/facebook/graphql/model/GroupsYouShouldJoinFeedUnit;

    invoke-interface {v3}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/25L;->a(Ljava/lang/String;)LX/25L;

    move-result-object v3

    .line 635563
    iput-object v3, v5, LX/3mP;->d:LX/25L;

    .line 635564
    move-object v5, v5

    .line 635565
    iget-object v3, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 635566
    check-cast v3, LX/0jW;

    .line 635567
    iput-object v3, v5, LX/3mP;->e:LX/0jW;

    .line 635568
    move-object v3, v5

    .line 635569
    const/16 v5, 0x8

    .line 635570
    iput v5, v3, LX/3mP;->b:I

    .line 635571
    move-object v3, v3

    .line 635572
    iput-object v4, v3, LX/3mP;->g:LX/25K;

    .line 635573
    move-object v3, v3

    .line 635574
    invoke-virtual {v3}, LX/3mP;->a()LX/25M;

    move-result-object v9

    .line 635575
    iget v3, v0, LX/3mK;->f:I

    const/4 v4, -0x2

    if-ne v3, v4, :cond_0

    .line 635576
    iget-object v3, v0, LX/3mK;->e:LX/0ad;

    sget v4, LX/3mS;->a:I

    const/4 v5, -0x1

    invoke-interface {v3, v4, v5}, LX/0ad;->a(II)I

    move-result v3

    iput v3, v0, LX/3mK;->f:I

    .line 635577
    :cond_0
    iget-object v3, v0, LX/3mK;->c:LX/3mM;

    iget v4, v0, LX/3mK;->f:I

    invoke-static {v2, v4}, LX/3mK;->a(Lcom/facebook/feed/rows/core/props/FeedProps;I)LX/0Px;

    move-result-object v5

    new-instance v6, LX/3mT;

    .line 635578
    iget-object v4, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v4

    .line 635579
    check-cast v4, Lcom/facebook/graphql/model/GroupsYouShouldJoinFeedUnit;

    iget-object v7, v0, LX/3mK;->d:LX/3mN;

    invoke-direct {v6, v4, v7}, LX/3mT;-><init>(Lcom/facebook/graphql/model/GroupsYouShouldJoinFeedUnit;LX/3mN;)V

    move-object v4, p1

    move-object v7, v1

    move-object v8, v2

    invoke-virtual/range {v3 .. v9}, LX/3mM;->a(Landroid/content/Context;LX/0Px;LX/3mU;Ljava/lang/Object;Lcom/facebook/feed/rows/core/props/FeedProps;LX/25M;)LX/3mV;

    move-result-object v3

    .line 635580
    iget-object v4, v0, LX/3mK;->b:LX/3mL;

    invoke-virtual {v4, p1}, LX/3mL;->c(LX/1De;)LX/3ml;

    move-result-object v4

    invoke-virtual {v4, v3}, LX/3ml;->a(LX/3mX;)LX/3ml;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const/4 v4, 0x7

    const/4 v5, 0x2

    invoke-interface {v3, v4, v5}, LX/1Di;->h(II)LX/1Di;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 635581
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 635582
    invoke-static {}, LX/1dS;->b()V

    .line 635583
    const/4 v0, 0x0

    return-object v0
.end method
