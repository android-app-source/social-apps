.class public abstract LX/4ep;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1cF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1cF",
        "<",
        "LX/1FL;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/concurrent/Executor;

.field private final b:LX/1Fj;

.field private final c:Z


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;LX/1Fj;Z)V
    .locals 2

    .prologue
    .line 797528
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 797529
    iput-object p1, p0, LX/4ep;->a:Ljava/util/concurrent/Executor;

    .line 797530
    iput-object p2, p0, LX/4ep;->b:LX/1Fj;

    .line 797531
    if-eqz p3, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/4ep;->c:Z

    .line 797532
    return-void

    .line 797533
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public abstract a(LX/1bf;)LX/1FL;
.end method

.method public final a(Ljava/io/InputStream;I)LX/1FL;
    .locals 2

    .prologue
    .line 797534
    const/4 v1, 0x0

    .line 797535
    if-gtz p2, :cond_0

    .line 797536
    :try_start_0
    iget-object v0, p0, LX/4ep;->b:LX/1Fj;

    invoke-virtual {v0, p1}, LX/1Fj;->a(Ljava/io/InputStream;)LX/1FK;

    move-result-object v0

    invoke-static {v0}, LX/1FJ;->a(Ljava/io/Closeable;)LX/1FJ;

    move-result-object v1

    .line 797537
    :goto_0
    new-instance v0, LX/1FL;

    invoke-direct {v0, v1}, LX/1FL;-><init>(LX/1FJ;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 797538
    invoke-static {p1}, LX/1vz;->a(Ljava/io/InputStream;)V

    .line 797539
    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    return-object v0

    .line 797540
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/4ep;->b:LX/1Fj;

    invoke-virtual {v0, p1, p2}, LX/1Fj;->a(Ljava/io/InputStream;I)LX/1FK;

    move-result-object v0

    invoke-static {v0}, LX/1FJ;->a(Ljava/io/Closeable;)LX/1FJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    goto :goto_0

    .line 797541
    :catchall_0
    move-exception v0

    invoke-static {p1}, LX/1vz;->a(Ljava/io/InputStream;)V

    .line 797542
    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    throw v0
.end method

.method public abstract a()Ljava/lang/String;
.end method

.method public final a(LX/1cd;LX/1cW;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/imagepipeline/producers/Consumer",
            "<",
            "LX/1FL;",
            ">;",
            "Lcom/facebook/imagepipeline/producers/ProducerContext;",
            ")V"
        }
    .end annotation

    .prologue
    .line 797543
    iget-object v0, p2, LX/1cW;->c:LX/1BV;

    move-object v3, v0

    .line 797544
    iget-object v0, p2, LX/1cW;->b:Ljava/lang/String;

    move-object v5, v0

    .line 797545
    iget-object v0, p2, LX/1cW;->a:LX/1bf;

    move-object v6, v0

    .line 797546
    new-instance v0, Lcom/facebook/imagepipeline/producers/LocalFetchProducer$1;

    invoke-virtual {p0}, LX/4ep;->a()Ljava/lang/String;

    move-result-object v4

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v6}, Lcom/facebook/imagepipeline/producers/LocalFetchProducer$1;-><init>(LX/4ep;LX/1cd;LX/1BV;Ljava/lang/String;Ljava/lang/String;LX/1bf;)V

    .line 797547
    new-instance v1, LX/4f2;

    invoke-direct {v1, p0, v0}, LX/4f2;-><init>(LX/4ep;Lcom/facebook/imagepipeline/producers/StatefulProducerRunnable;)V

    invoke-virtual {p2, v1}, LX/1cW;->a(LX/1cg;)V

    .line 797548
    iget-object v1, p0, LX/4ep;->a:Ljava/util/concurrent/Executor;

    const v2, 0x7cb25b67

    invoke-static {v1, v0, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 797549
    return-void
.end method

.method public final b(Ljava/io/InputStream;I)LX/1FL;
    .locals 6

    .prologue
    .line 797550
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    .line 797551
    invoke-virtual {v0}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v2

    .line 797552
    invoke-virtual {v0}, Ljava/lang/Runtime;->totalMemory()J

    move-result-wide v4

    invoke-virtual {v0}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v0

    sub-long v0, v4, v0

    .line 797553
    sub-long v0, v2, v0

    const-wide/32 v4, 0x800000

    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    .line 797554
    iget-boolean v4, p0, LX/4ep;->c:Z

    if-eqz v4, :cond_0

    instance-of v4, p1, Ljava/io/FileInputStream;

    if-eqz v4, :cond_0

    const-wide/16 v4, 0x40

    mul-long/2addr v0, v4

    cmp-long v0, v2, v0

    if-ltz v0, :cond_0

    .line 797555
    new-instance v0, Ljava/io/File;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 797556
    new-instance v1, LX/4f3;

    invoke-direct {v1, p0, v0}, LX/4f3;-><init>(LX/4ep;Ljava/io/File;)V

    .line 797557
    new-instance v2, LX/1FL;

    invoke-direct {v2, v1, p2}, LX/1FL;-><init>(LX/1Gd;I)V

    move-object v0, v2

    .line 797558
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, p1, p2}, LX/4ep;->a(Ljava/io/InputStream;I)LX/1FL;

    move-result-object v0

    goto :goto_0
.end method
