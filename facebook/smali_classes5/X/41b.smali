.class public final enum LX/41b;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/41b;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/41b;

.field public static final enum LOGIN_DEVICE_TYPE_APPLICATION:LX/41b;

.field public static final enum LOGIN_DEVICE_TYPE_CHAT_CLIENT:LX/41b;

.field public static final enum LOGIN_DEVICE_TYPE_DESKTOP_BROWSER:LX/41b;

.field public static final enum LOGIN_DEVICE_TYPE_MOBILE_APP:LX/41b;

.field public static final enum LOGIN_DEVICE_TYPE_MOBILE_BROWSER:LX/41b;

.field public static final enum LOGIN_DEVICE_TYPE_UNKNOWN:LX/41b;


# instance fields
.field private final mDeviceType:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 666416
    new-instance v0, LX/41b;

    const-string v1, "LOGIN_DEVICE_TYPE_MOBILE_BROWSER"

    const-string v2, "login_device_mobile_browser"

    invoke-direct {v0, v1, v4, v2}, LX/41b;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/41b;->LOGIN_DEVICE_TYPE_MOBILE_BROWSER:LX/41b;

    .line 666417
    new-instance v0, LX/41b;

    const-string v1, "LOGIN_DEVICE_TYPE_DESKTOP_BROWSER"

    const-string v2, "login_device_desktop_browser"

    invoke-direct {v0, v1, v5, v2}, LX/41b;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/41b;->LOGIN_DEVICE_TYPE_DESKTOP_BROWSER:LX/41b;

    .line 666418
    new-instance v0, LX/41b;

    const-string v1, "LOGIN_DEVICE_TYPE_MOBILE_APP"

    const-string v2, "login_device_mobile_app"

    invoke-direct {v0, v1, v6, v2}, LX/41b;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/41b;->LOGIN_DEVICE_TYPE_MOBILE_APP:LX/41b;

    .line 666419
    new-instance v0, LX/41b;

    const-string v1, "LOGIN_DEVICE_TYPE_APPLICATION"

    const-string v2, "login_device_application"

    invoke-direct {v0, v1, v7, v2}, LX/41b;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/41b;->LOGIN_DEVICE_TYPE_APPLICATION:LX/41b;

    .line 666420
    new-instance v0, LX/41b;

    const-string v1, "LOGIN_DEVICE_TYPE_CHAT_CLIENT"

    const-string v2, "login_device_chat_client"

    invoke-direct {v0, v1, v8, v2}, LX/41b;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/41b;->LOGIN_DEVICE_TYPE_CHAT_CLIENT:LX/41b;

    .line 666421
    new-instance v0, LX/41b;

    const-string v1, "LOGIN_DEVICE_TYPE_UNKNOWN"

    const/4 v2, 0x5

    const-string v3, "login_device_unknown"

    invoke-direct {v0, v1, v2, v3}, LX/41b;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/41b;->LOGIN_DEVICE_TYPE_UNKNOWN:LX/41b;

    .line 666422
    const/4 v0, 0x6

    new-array v0, v0, [LX/41b;

    sget-object v1, LX/41b;->LOGIN_DEVICE_TYPE_MOBILE_BROWSER:LX/41b;

    aput-object v1, v0, v4

    sget-object v1, LX/41b;->LOGIN_DEVICE_TYPE_DESKTOP_BROWSER:LX/41b;

    aput-object v1, v0, v5

    sget-object v1, LX/41b;->LOGIN_DEVICE_TYPE_MOBILE_APP:LX/41b;

    aput-object v1, v0, v6

    sget-object v1, LX/41b;->LOGIN_DEVICE_TYPE_APPLICATION:LX/41b;

    aput-object v1, v0, v7

    sget-object v1, LX/41b;->LOGIN_DEVICE_TYPE_CHAT_CLIENT:LX/41b;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/41b;->LOGIN_DEVICE_TYPE_UNKNOWN:LX/41b;

    aput-object v2, v0, v1

    sput-object v0, LX/41b;->$VALUES:[LX/41b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 666411
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 666412
    iput-object p3, p0, LX/41b;->mDeviceType:Ljava/lang/String;

    .line 666413
    return-void
.end method

.method public static getDeviceType(Ljava/lang/String;)LX/41b;
    .locals 1

    .prologue
    .line 666423
    const-string v0, "login_device_mobile_browser"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 666424
    sget-object v0, LX/41b;->LOGIN_DEVICE_TYPE_MOBILE_BROWSER:LX/41b;

    .line 666425
    :goto_0
    return-object v0

    .line 666426
    :cond_0
    const-string v0, "login_device_desktop_browser"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 666427
    sget-object v0, LX/41b;->LOGIN_DEVICE_TYPE_DESKTOP_BROWSER:LX/41b;

    goto :goto_0

    .line 666428
    :cond_1
    const-string v0, "login_device_mobile_app"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 666429
    sget-object v0, LX/41b;->LOGIN_DEVICE_TYPE_MOBILE_APP:LX/41b;

    goto :goto_0

    .line 666430
    :cond_2
    const-string v0, "login_device_application"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 666431
    sget-object v0, LX/41b;->LOGIN_DEVICE_TYPE_APPLICATION:LX/41b;

    goto :goto_0

    .line 666432
    :cond_3
    const-string v0, "login_device_chat_client"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 666433
    sget-object v0, LX/41b;->LOGIN_DEVICE_TYPE_CHAT_CLIENT:LX/41b;

    goto :goto_0

    .line 666434
    :cond_4
    sget-object v0, LX/41b;->LOGIN_DEVICE_TYPE_UNKNOWN:LX/41b;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/41b;
    .locals 1

    .prologue
    .line 666415
    const-class v0, LX/41b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/41b;

    return-object v0
.end method

.method public static values()[LX/41b;
    .locals 1

    .prologue
    .line 666414
    sget-object v0, LX/41b;->$VALUES:[LX/41b;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/41b;

    return-object v0
.end method
