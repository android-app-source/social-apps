.class public LX/4fA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1cF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1cF",
        "<",
        "LX/1FJ",
        "<",
        "LX/1ln;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/1Fh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Fh",
            "<",
            "LX/1bh;",
            "LX/1ln;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/1Ao;

.field private final c:LX/1cF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1cF",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1Fh;LX/1Ao;LX/1cF;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Fh",
            "<",
            "LX/1bh;",
            "LX/1ln;",
            ">;",
            "Lcom/facebook/imagepipeline/cache/CacheKeyFactory;",
            "LX/1cF",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 798017
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 798018
    iput-object p1, p0, LX/4fA;->a:LX/1Fh;

    .line 798019
    iput-object p2, p0, LX/4fA;->b:LX/1Ao;

    .line 798020
    iput-object p3, p0, LX/4fA;->c:LX/1cF;

    .line 798021
    return-void
.end method


# virtual methods
.method public final a(LX/1cd;LX/1cW;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/imagepipeline/producers/Consumer",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;",
            "Lcom/facebook/imagepipeline/producers/ProducerContext;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 798022
    iget-object v1, p2, LX/1cW;->c:LX/1BV;

    move-object v1, v1

    .line 798023
    iget-object v2, p2, LX/1cW;->b:Ljava/lang/String;

    move-object v2, v2

    .line 798024
    iget-object v3, p2, LX/1cW;->a:LX/1bf;

    move-object v3, v3

    .line 798025
    iget-object v4, p2, LX/1cW;->d:Ljava/lang/Object;

    move-object v4, v4

    .line 798026
    iget-object v5, v3, LX/1bf;->m:LX/33B;

    move-object v5, v5

    .line 798027
    if-eqz v5, :cond_0

    invoke-interface {v5}, LX/33B;->a()LX/1bh;

    move-result-object v6

    if-nez v6, :cond_1

    .line 798028
    :cond_0
    iget-object v0, p0, LX/4fA;->c:LX/1cF;

    invoke-interface {v0, p1, p2}, LX/1cF;->a(LX/1cd;LX/1cW;)V

    .line 798029
    :goto_0
    return-void

    .line 798030
    :cond_1
    const-string v6, "PostprocessedBitmapMemoryCacheProducer"

    move-object v6, v6

    .line 798031
    invoke-interface {v1, v2, v6}, LX/1BV;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 798032
    iget-object v6, p0, LX/4fA;->b:LX/1Ao;

    invoke-virtual {v6, v3, v4}, LX/1Ao;->b(LX/1bf;Ljava/lang/Object;)LX/1bh;

    move-result-object v3

    .line 798033
    iget-object v4, p0, LX/4fA;->a:LX/1Fh;

    invoke-interface {v4, v3}, LX/1Fh;->a(Ljava/lang/Object;)LX/1FJ;

    move-result-object v4

    .line 798034
    if-eqz v4, :cond_3

    .line 798035
    const-string v3, "PostprocessedBitmapMemoryCacheProducer"

    move-object v3, v3

    .line 798036
    invoke-interface {v1, v2}, LX/1BV;->b(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    const-string v0, "cached_value_found"

    const-string v5, "true"

    invoke-static {v0, v5}, LX/2oC;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    :cond_2
    invoke-interface {v1, v2, v3, v0}, LX/1BV;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 798037
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p1, v0}, LX/1cd;->b(F)V

    .line 798038
    const/4 v0, 0x1

    invoke-virtual {p1, v4, v0}, LX/1cd;->b(Ljava/lang/Object;Z)V

    .line 798039
    invoke-virtual {v4}, LX/1FJ;->close()V

    goto :goto_0

    .line 798040
    :cond_3
    instance-of v4, v5, LX/4fO;

    .line 798041
    new-instance v5, LX/4f9;

    iget-object v6, p0, LX/4fA;->a:LX/1Fh;

    invoke-direct {v5, p1, v3, v4, v6}, LX/4f9;-><init>(LX/1cd;LX/1bh;ZLX/1Fh;)V

    .line 798042
    const-string v3, "PostprocessedBitmapMemoryCacheProducer"

    move-object v3, v3

    .line 798043
    invoke-interface {v1, v2}, LX/1BV;->b(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    const-string v0, "cached_value_found"

    const-string v4, "false"

    invoke-static {v0, v4}, LX/2oC;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    :cond_4
    invoke-interface {v1, v2, v3, v0}, LX/1BV;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 798044
    iget-object v0, p0, LX/4fA;->c:LX/1cF;

    invoke-interface {v0, v5, p2}, LX/1cF;->a(LX/1cd;LX/1cW;)V

    goto :goto_0
.end method
