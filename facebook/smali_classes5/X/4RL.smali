.class public LX/4RL;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 708570
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 708571
    const/4 v0, 0x0

    .line 708572
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v4, :cond_7

    .line 708573
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 708574
    :goto_0
    return v1

    .line 708575
    :cond_0
    const-string v8, "show_all_contextual_places"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 708576
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;

    move-result-object v0

    move-object v4, v0

    move v0, v2

    .line 708577
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_5

    .line 708578
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 708579
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 708580
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_1

    if-eqz v7, :cond_1

    .line 708581
    const-string v8, "place_ids"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 708582
    invoke-static {p0, p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 708583
    :cond_2
    const-string v8, "place_topic_ids"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 708584
    invoke-static {p0, p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 708585
    :cond_3
    const-string v8, "suggestion_mechanism"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 708586
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 708587
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 708588
    :cond_5
    const/4 v7, 0x4

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 708589
    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 708590
    invoke-virtual {p1, v2, v5}, LX/186;->b(II)V

    .line 708591
    if-eqz v0, :cond_6

    .line 708592
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v4}, LX/186;->a(ILjava/lang/Enum;)V

    .line 708593
    :cond_6
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 708594
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_7
    move v3, v1

    move-object v4, v0

    move v5, v1

    move v6, v1

    move v0, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 708595
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 708596
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 708597
    if-eqz v0, :cond_0

    .line 708598
    const-string v0, "place_ids"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 708599
    invoke-virtual {p0, p1, v1}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 708600
    :cond_0
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 708601
    if-eqz v0, :cond_1

    .line 708602
    const-string v0, "place_topic_ids"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 708603
    invoke-virtual {p0, p1, v2}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 708604
    :cond_1
    invoke-virtual {p0, p1, v3, v1}, LX/15i;->a(IIS)S

    move-result v0

    .line 708605
    if-eqz v0, :cond_2

    .line 708606
    const-string v0, "show_all_contextual_places"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 708607
    const-class v0, Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;

    invoke-virtual {p0, p1, v3, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 708608
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 708609
    if-eqz v0, :cond_3

    .line 708610
    const-string v1, "suggestion_mechanism"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 708611
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 708612
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 708613
    return-void
.end method
