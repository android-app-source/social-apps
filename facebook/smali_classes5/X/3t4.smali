.class public final LX/3t4;
.super Landroid/view/accessibility/AccessibilityNodeProvider;
.source ""


# instance fields
.field public final synthetic a:LX/3sw;


# direct methods
.method public constructor <init>(LX/3sw;)V
    .locals 0

    .prologue
    .line 644458
    iput-object p1, p0, LX/3t4;->a:LX/3sw;

    invoke-direct {p0}, Landroid/view/accessibility/AccessibilityNodeProvider;-><init>()V

    return-void
.end method


# virtual methods
.method public final createAccessibilityNodeInfo(I)Landroid/view/accessibility/AccessibilityNodeInfo;
    .locals 1

    .prologue
    .line 644459
    iget-object v0, p0, LX/3t4;->a:LX/3sw;

    invoke-interface {v0, p1}, LX/3sw;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityNodeInfo;

    return-object v0
.end method

.method public final findAccessibilityNodeInfosByText(Ljava/lang/String;I)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/List",
            "<",
            "Landroid/view/accessibility/AccessibilityNodeInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 644460
    iget-object v0, p0, LX/3t4;->a:LX/3sw;

    invoke-interface {v0, p1, p2}, LX/3sw;->a(Ljava/lang/String;I)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final performAction(IILandroid/os/Bundle;)Z
    .locals 1

    .prologue
    .line 644461
    iget-object v0, p0, LX/3t4;->a:LX/3sw;

    invoke-interface {v0, p1, p2, p3}, LX/3sw;->a(IILandroid/os/Bundle;)Z

    move-result v0

    return v0
.end method
