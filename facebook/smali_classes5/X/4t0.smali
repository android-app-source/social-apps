.class public final LX/4t0;
.super LX/3Kd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3Kd",
        "<",
        "LX/4tB;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/4t0;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, LX/4t0;

    invoke-direct {v0}, LX/4t0;-><init>()V

    sput-object v0, LX/4t0;->a:LX/4t0;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const-string v0, "com.google.android.gms.common.ui.SignInButtonCreatorImpl"

    invoke-direct {p0, v0}, LX/3Kd;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public static b(LX/4t0;Landroid/content/Context;II[Lcom/google/android/gms/common/api/Scope;)Landroid/view/View;
    .locals 4

    :try_start_0
    new-instance v1, Lcom/google/android/gms/common/internal/SignInButtonConfig;

    invoke-direct {v1, p2, p3, p4}, Lcom/google/android/gms/common/internal/SignInButtonConfig;-><init>(II[Lcom/google/android/gms/common/api/Scope;)V

    invoke-static {p1}, LX/1or;->a(Ljava/lang/Object;)LX/1ot;

    move-result-object v2

    invoke-virtual {p0, p1}, LX/3Kd;->a(Landroid/content/Context;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4tB;

    invoke-interface {v0, v2, v1}, LX/4tB;->a(LX/1ot;Lcom/google/android/gms/common/internal/SignInButtonConfig;)LX/1ot;

    move-result-object v0

    invoke-static {v0}, LX/1or;->a(LX/1ot;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, LX/4tj;

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x40

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Could not get button with size "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " and color "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, LX/4tj;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public final a(Landroid/os/IBinder;)Ljava/lang/Object;
    .locals 1

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    move-object v0, v0

    return-object v0

    :cond_0
    const-string v0, "com.google.android.gms.common.internal.ISignInButtonCreator"

    invoke-interface {p1, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    if-eqz v0, :cond_1

    instance-of p0, v0, LX/4tB;

    if-eqz p0, :cond_1

    check-cast v0, LX/4tB;

    goto :goto_0

    :cond_1
    new-instance v0, LX/4tC;

    invoke-direct {v0, p1}, LX/4tC;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method
