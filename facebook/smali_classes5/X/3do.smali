.class public final enum LX/3do;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/3do;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/3do;

.field public static final enum AUTODOWNLOADED_PACKS:LX/3do;

.field public static final enum DOWNLOADED_PACKS:LX/3do;

.field public static final enum OWNED_PACKS:LX/3do;

.field public static final enum STORE_PACKS:LX/3do;


# instance fields
.field private mFieldName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 617385
    new-instance v0, LX/3do;

    const-string v1, "DOWNLOADED_PACKS"

    const-string v2, "tray_packs"

    invoke-direct {v0, v1, v3, v2}, LX/3do;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3do;->DOWNLOADED_PACKS:LX/3do;

    .line 617386
    new-instance v0, LX/3do;

    const-string v1, "OWNED_PACKS"

    const-string v2, "owned_packs"

    invoke-direct {v0, v1, v4, v2}, LX/3do;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3do;->OWNED_PACKS:LX/3do;

    .line 617387
    new-instance v0, LX/3do;

    const-string v1, "STORE_PACKS"

    const-string v2, "available_packs"

    invoke-direct {v0, v1, v5, v2}, LX/3do;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3do;->STORE_PACKS:LX/3do;

    .line 617388
    new-instance v0, LX/3do;

    const-string v1, "AUTODOWNLOADED_PACKS"

    const-string v2, "available_packs"

    invoke-direct {v0, v1, v6, v2}, LX/3do;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3do;->AUTODOWNLOADED_PACKS:LX/3do;

    .line 617389
    const/4 v0, 0x4

    new-array v0, v0, [LX/3do;

    sget-object v1, LX/3do;->DOWNLOADED_PACKS:LX/3do;

    aput-object v1, v0, v3

    sget-object v1, LX/3do;->OWNED_PACKS:LX/3do;

    aput-object v1, v0, v4

    sget-object v1, LX/3do;->STORE_PACKS:LX/3do;

    aput-object v1, v0, v5

    sget-object v1, LX/3do;->AUTODOWNLOADED_PACKS:LX/3do;

    aput-object v1, v0, v6

    sput-object v0, LX/3do;->$VALUES:[LX/3do;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 617390
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 617391
    iput-object p3, p0, LX/3do;->mFieldName:Ljava/lang/String;

    .line 617392
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/3do;
    .locals 1

    .prologue
    .line 617393
    const-class v0, LX/3do;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/3do;

    return-object v0
.end method

.method public static values()[LX/3do;
    .locals 1

    .prologue
    .line 617394
    sget-object v0, LX/3do;->$VALUES:[LX/3do;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/3do;

    return-object v0
.end method


# virtual methods
.method public final getFieldName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 617395
    iget-object v0, p0, LX/3do;->mFieldName:Ljava/lang/String;

    return-object v0
.end method
