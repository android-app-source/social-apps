.class public final LX/3mP;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Z

.field public b:I

.field public c:Ljava/lang/Integer;

.field public d:LX/25L;

.field public e:LX/0jW;

.field public f:LX/HJK;

.field public g:LX/25K;

.field public h:Lcom/facebook/feed/rows/core/props/FeedProps;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 636356
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static newBuilder()LX/3mP;
    .locals 1

    .prologue
    .line 636355
    new-instance v0, LX/3mP;

    invoke-direct {v0}, LX/3mP;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()LX/25M;
    .locals 10

    .prologue
    .line 636357
    new-instance v0, LX/25M;

    iget-boolean v1, p0, LX/3mP;->a:Z

    iget v2, p0, LX/3mP;->b:I

    iget-object v3, p0, LX/3mP;->c:Ljava/lang/Integer;

    iget-object v4, p0, LX/3mP;->d:LX/25L;

    iget-object v5, p0, LX/3mP;->e:LX/0jW;

    iget-object v6, p0, LX/3mP;->f:LX/HJK;

    iget-object v7, p0, LX/3mP;->g:LX/25K;

    iget-object v8, p0, LX/3mP;->h:Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, LX/25M;-><init>(ZILjava/lang/Integer;LX/25L;LX/0jW;LX/HJK;LX/25K;Lcom/facebook/feed/rows/core/props/FeedProps;B)V

    return-object v0
.end method

.method public final a(LX/0jW;)LX/3mP;
    .locals 0

    .prologue
    .line 636353
    iput-object p1, p0, LX/3mP;->e:LX/0jW;

    .line 636354
    return-object p0
.end method

.method public final a(LX/25K;)LX/3mP;
    .locals 0

    .prologue
    .line 636351
    iput-object p1, p0, LX/3mP;->g:LX/25K;

    .line 636352
    return-object p0
.end method

.method public final a(LX/25L;)LX/3mP;
    .locals 0

    .prologue
    .line 636349
    iput-object p1, p0, LX/3mP;->d:LX/25L;

    .line 636350
    return-object p0
.end method

.method public final a(Z)LX/3mP;
    .locals 0

    .prologue
    .line 636347
    iput-boolean p1, p0, LX/3mP;->a:Z

    .line 636348
    return-object p0
.end method
