.class public LX/4Rf;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 709588
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 709626
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 709627
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 709628
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 709629
    invoke-static {p0, p1}, LX/4Rf;->b(LX/15w;LX/186;)I

    move-result v1

    .line 709630
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 709631
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 709632
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 709633
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 709634
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2, p3}, LX/4Rf;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 709635
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 709636
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 709637
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 709604
    const/4 v0, 0x0

    .line 709605
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v4, :cond_7

    .line 709606
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 709607
    :goto_0
    return v1

    .line 709608
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_4

    .line 709609
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 709610
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 709611
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_0

    if-eqz v7, :cond_0

    .line 709612
    const-string v8, "is_currently_selected"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 709613
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v3

    move v6, v3

    move v3, v2

    goto :goto_1

    .line 709614
    :cond_1
    const-string v8, "node"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 709615
    invoke-static {p0, p1}, LX/39L;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 709616
    :cond_2
    const-string v8, "option_type"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 709617
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    move-result-object v0

    move-object v4, v0

    move v0, v2

    goto :goto_1

    .line 709618
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 709619
    :cond_4
    const/4 v7, 0x3

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 709620
    if-eqz v3, :cond_5

    .line 709621
    invoke-virtual {p1, v1, v6}, LX/186;->a(IZ)V

    .line 709622
    :cond_5
    invoke-virtual {p1, v2, v5}, LX/186;->b(II)V

    .line 709623
    if-eqz v0, :cond_6

    .line 709624
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v4}, LX/186;->a(ILjava/lang/Enum;)V

    .line 709625
    :cond_6
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_7
    move v3, v1

    move-object v4, v0

    move v5, v1

    move v6, v1

    move v0, v1

    goto :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 709589
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 709590
    invoke-virtual {p0, p1, v2}, LX/15i;->b(II)Z

    move-result v0

    .line 709591
    if-eqz v0, :cond_0

    .line 709592
    const-string v1, "is_currently_selected"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 709593
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 709594
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 709595
    if-eqz v0, :cond_1

    .line 709596
    const-string v1, "node"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 709597
    invoke-static {p0, v0, p2, p3}, LX/39L;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 709598
    :cond_1
    invoke-virtual {p0, p1, v3, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 709599
    if-eqz v0, :cond_2

    .line 709600
    const-string v0, "option_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 709601
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    invoke-virtual {p0, p1, v3, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 709602
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 709603
    return-void
.end method
