.class public final LX/3cs;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/facebook/notifications/server/FetchNotificationSeenStatesParams;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:J

.field public final synthetic b:LX/1rk;


# direct methods
.method public constructor <init>(LX/1rk;J)V
    .locals 0

    .prologue
    .line 615822
    iput-object p1, p0, LX/3cs;->b:LX/1rk;

    iput-wide p2, p0, LX/3cs;->a:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 615823
    iget-object v0, p0, LX/3cs;->b:LX/1rk;

    iget-object v0, v0, LX/1rk;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    iget-wide v2, p0, LX/3cs;->a:J

    invoke-virtual {v0, v2, v3}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a(J)LX/0Px;

    move-result-object v0

    .line 615824
    new-instance v1, Lcom/facebook/notifications/server/FetchNotificationSeenStatesParams;

    invoke-direct {v1, v0}, Lcom/facebook/notifications/server/FetchNotificationSeenStatesParams;-><init>(Ljava/util/Collection;)V

    return-object v1
.end method
