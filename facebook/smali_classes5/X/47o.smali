.class public final LX/47o;
.super Ljava/util/AbstractMap;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/AbstractMap",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/util/Map$Entry",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 672476
    invoke-direct {p0}, Ljava/util/AbstractMap;-><init>()V

    .line 672477
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    move-result-object v0

    .line 672478
    invoke-static {v0}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v1}, LX/0wv;->a(Ljava/lang/Iterable;)LX/0wv;

    move-result-object v1

    move-object v0, v1

    .line 672479
    new-instance v1, LX/47n;

    invoke-direct {v1, p0, p1}, LX/47n;-><init>(LX/47o;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, LX/0wv;->a(LX/0QK;)LX/0wv;

    move-result-object v0

    invoke-virtual {v0}, LX/0wv;->c()LX/0Rf;

    move-result-object v0

    iput-object v0, p0, LX/47o;->a:LX/0Rf;

    .line 672480
    return-void
.end method


# virtual methods
.method public final entrySet()Ljava/util/Set;
    .locals 1

    .prologue
    .line 672481
    iget-object v0, p0, LX/47o;->a:LX/0Rf;

    return-object v0
.end method
