.class public final LX/50L;
.super LX/1sm;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1sm",
        "<",
        "LX/50M",
        "<*>;>;",
        "Ljava/io/Serializable;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 823608
    invoke-direct {p0}, LX/1sm;-><init>()V

    return-void
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 3

    .prologue
    .line 823609
    check-cast p1, LX/50M;

    check-cast p2, LX/50M;

    .line 823610
    sget-object v0, LX/3lo;->a:LX/3lo;

    move-object v0, v0

    .line 823611
    iget-object v1, p1, LX/50M;->lowerBound:LX/4xM;

    iget-object v2, p2, LX/50M;->lowerBound:LX/4xM;

    invoke-virtual {v0, v1, v2}, LX/3lo;->a(Ljava/lang/Comparable;Ljava/lang/Comparable;)LX/3lo;

    move-result-object v0

    iget-object v1, p1, LX/50M;->upperBound:LX/4xM;

    iget-object v2, p2, LX/50M;->upperBound:LX/4xM;

    invoke-virtual {v0, v1, v2}, LX/3lo;->a(Ljava/lang/Comparable;Ljava/lang/Comparable;)LX/3lo;

    move-result-object v0

    invoke-virtual {v0}, LX/3lo;->b()I

    move-result v0

    return v0
.end method
