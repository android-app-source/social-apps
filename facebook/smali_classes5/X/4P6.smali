.class public LX/4P6;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 699655
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 12

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 699679
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_9

    .line 699680
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 699681
    :goto_0
    return v1

    .line 699682
    :cond_0
    const-string v10, "show_beeper"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 699683
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v3

    move v6, v3

    move v3, v2

    .line 699684
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_6

    .line 699685
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 699686
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 699687
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_1

    if-eqz v9, :cond_1

    .line 699688
    const-string v10, "icon_url"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 699689
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 699690
    :cond_2
    const-string v10, "name"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 699691
    invoke-static {p0, p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 699692
    :cond_3
    const-string v10, "show_store_overlay"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 699693
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v5, v0

    move v0, v2

    goto :goto_1

    .line 699694
    :cond_4
    const-string v10, "store_context"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 699695
    invoke-static {p0, p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 699696
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 699697
    :cond_6
    const/4 v9, 0x5

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 699698
    invoke-virtual {p1, v1, v8}, LX/186;->b(II)V

    .line 699699
    invoke-virtual {p1, v2, v7}, LX/186;->b(II)V

    .line 699700
    if-eqz v3, :cond_7

    .line 699701
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v6}, LX/186;->a(IZ)V

    .line 699702
    :cond_7
    if-eqz v0, :cond_8

    .line 699703
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v5}, LX/186;->a(IZ)V

    .line 699704
    :cond_8
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 699705
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_9
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 699656
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 699657
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 699658
    if-eqz v0, :cond_0

    .line 699659
    const-string v1, "icon_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 699660
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 699661
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 699662
    if-eqz v0, :cond_1

    .line 699663
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 699664
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 699665
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 699666
    if-eqz v0, :cond_2

    .line 699667
    const-string v1, "show_beeper"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 699668
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 699669
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 699670
    if-eqz v0, :cond_3

    .line 699671
    const-string v1, "show_store_overlay"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 699672
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 699673
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 699674
    if-eqz v0, :cond_4

    .line 699675
    const-string v1, "store_context"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 699676
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 699677
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 699678
    return-void
.end method
