.class public LX/489;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# instance fields
.field private final a:Lcom/facebook/compactdisk/StoreManagerFactory;

.field private b:Lcom/facebook/compactdisk/DiskCache;


# direct methods
.method public constructor <init>(Lcom/facebook/compactdisk/StoreManagerFactory;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 672749
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 672750
    iput-object p1, p0, LX/489;->a:Lcom/facebook/compactdisk/StoreManagerFactory;

    .line 672751
    return-void
.end method


# virtual methods
.method public final init()V
    .locals 2

    .prologue
    .line 672752
    new-instance v0, Lcom/facebook/compactdisk/DiskCacheConfig;

    invoke-direct {v0}, Lcom/facebook/compactdisk/DiskCacheConfig;-><init>()V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/compactdisk/DiskCacheConfig;->sessionScoped(Z)Lcom/facebook/compactdisk/DiskCacheConfig;

    move-result-object v0

    sget-object v1, Lcom/facebook/compactdisk/DiskArea;->CACHES:Lcom/facebook/compactdisk/DiskArea;

    invoke-virtual {v0, v1}, Lcom/facebook/compactdisk/DiskCacheConfig;->diskArea(Lcom/facebook/compactdisk/DiskArea;)Lcom/facebook/compactdisk/DiskCacheConfig;

    move-result-object v0

    const-string v1, "disk_cache"

    invoke-virtual {v0, v1}, Lcom/facebook/compactdisk/DiskCacheConfig;->name(Ljava/lang/String;)Lcom/facebook/compactdisk/DiskCacheConfig;

    move-result-object v0

    new-instance v1, Lcom/facebook/compactdisk/ManagedConfig;

    invoke-direct {v1}, Lcom/facebook/compactdisk/ManagedConfig;-><init>()V

    invoke-virtual {v0, v1}, Lcom/facebook/compactdisk/DiskCacheConfig;->subConfig(Lcom/facebook/compactdisk/SubConfig;)Lcom/facebook/compactdisk/DiskCacheConfig;

    move-result-object v0

    .line 672753
    iget-object v1, p0, LX/489;->a:Lcom/facebook/compactdisk/StoreManagerFactory;

    invoke-virtual {v1, v0}, Lcom/facebook/compactdisk/StoreManagerFactory;->a(Lcom/facebook/compactdisk/DiskCacheConfig;)Lcom/facebook/compactdisk/StoreManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/facebook/compactdisk/StoreManager;->a(Lcom/facebook/compactdisk/DiskCacheConfig;)Lcom/facebook/compactdisk/DiskCache;

    move-result-object v0

    iput-object v0, p0, LX/489;->b:Lcom/facebook/compactdisk/DiskCache;

    .line 672754
    new-instance v0, Lcom/facebook/compactdisk/DiskCacheTester;

    iget-object v1, p0, LX/489;->b:Lcom/facebook/compactdisk/DiskCache;

    invoke-direct {v0, v1}, Lcom/facebook/compactdisk/DiskCacheTester;-><init>(Lcom/facebook/compactdisk/DiskCache;)V

    .line 672755
    invoke-virtual {v0}, Lcom/facebook/compactdisk/DiskCacheTester;->runExperiments()V

    .line 672756
    return-void
.end method
