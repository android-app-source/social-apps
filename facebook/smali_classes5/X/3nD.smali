.class public final LX/3nD;
.super LX/3nE;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3nE",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;

.field private final b:LX/1g1;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;LX/1g1;)V
    .locals 0

    .prologue
    .line 638055
    iput-object p1, p0, LX/3nD;->a:Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;

    invoke-direct {p0}, LX/3nE;-><init>()V

    .line 638056
    iput-object p2, p0, LX/3nD;->b:LX/1g1;

    .line 638057
    return-void
.end method


# virtual methods
.method public final a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 638058
    const/4 v6, 0x0

    const/4 v2, 0x0

    .line 638059
    iget-object v0, p0, LX/3nD;->a:Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;

    iget-object v1, p0, LX/3nD;->b:LX/1g1;

    sget-object v3, LX/3nB;->STARTED:LX/3nB;

    const/4 v4, 0x1

    invoke-static {v0, v1, v3, v4, v2}, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;->a$redex0(Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;LX/1g1;LX/3nB;ZLjava/lang/String;)V

    .line 638060
    iget-object v0, p0, LX/3nD;->a:Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;

    iget-object v1, p0, LX/3nD;->b:LX/1g1;

    invoke-virtual {v0, v1}, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;->b(LX/1g1;)Z

    move-result v1

    .line 638061
    if-nez v1, :cond_2

    .line 638062
    iget-object v0, p0, LX/3nD;->a:Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;

    iget-object v3, p0, LX/3nD;->b:LX/1g1;

    sget-object v4, LX/3nB;->FAILED:LX/3nB;

    iget-object v5, p0, LX/3nD;->a:Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;

    iget-object v5, v5, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;->o:Ljava/lang/String;

    invoke-static {v0, v3, v4, v6, v5}, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;->a$redex0(Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;LX/1g1;LX/3nB;ZLjava/lang/String;)V

    .line 638063
    iget-object v0, p0, LX/3nD;->a:Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;

    iget-object v0, v0, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;->h:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/3nD;->b:LX/1g1;

    .line 638064
    iget-object v3, v0, LX/1g1;->d:LX/3EA;

    move-object v0, v3

    .line 638065
    sget-object v3, LX/3EA;->SUBSEQUENT:LX/3EA;

    if-eq v0, v3, :cond_2

    .line 638066
    iget-object v0, p0, LX/3nD;->a:Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;

    iget-object v3, p0, LX/3nD;->b:LX/1g1;

    sget-object v4, LX/3nB;->FALLBACK:LX/3nB;

    invoke-static {v0, v3, v4, v1, v2}, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;->a$redex0(Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;LX/1g1;LX/3nB;ZLjava/lang/String;)V

    .line 638067
    iget-object v0, p0, LX/3nD;->a:Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;

    iget-object v1, p0, LX/3nD;->b:LX/1g1;

    invoke-static {v0, v1, v6}, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;->a$redex0(Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;LX/1g1;Z)Z

    move-result v0

    .line 638068
    sget-object v1, LX/3nB;->FALLBACK:LX/3nB;

    invoke-virtual {v1}, LX/3nB;->toString()Ljava/lang/String;

    move-result-object v1

    .line 638069
    :goto_0
    if-eqz v0, :cond_1

    iget-object v2, p0, LX/3nD;->b:LX/1g1;

    .line 638070
    iget-object v3, v2, LX/1g1;->d:LX/3EA;

    move-object v3, v3

    .line 638071
    sget-object v4, LX/3EA;->ORIGINAL:LX/3EA;

    if-eq v3, v4, :cond_0

    .line 638072
    iget-object v3, v2, LX/1g1;->d:LX/3EA;

    move-object v3, v3

    .line 638073
    sget-object v4, LX/3EA;->VIEWABILITY:LX/3EA;

    if-ne v3, v4, :cond_3

    .line 638074
    :cond_0
    iget-object v3, v2, LX/1g1;->b:Ljava/lang/Object;

    move-object v3, v3

    .line 638075
    instance-of v3, v3, Lcom/facebook/graphql/model/Sponsorable;

    if-eqz v3, :cond_3

    const/4 v3, 0x1

    :goto_1
    move v2, v3

    .line 638076
    if-eqz v2, :cond_1

    .line 638077
    iget-object v2, p0, LX/3nD;->a:Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;

    iget-object v3, p0, LX/3nD;->b:LX/1g1;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;->c(LX/1g1;)V

    .line 638078
    :cond_1
    iget-object v2, p0, LX/3nD;->a:Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;

    iget-object v3, p0, LX/3nD;->b:LX/1g1;

    sget-object v4, LX/3nB;->COMPLETED:LX/3nB;

    invoke-static {v2, v3, v4, v0, v1}, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;->a$redex0(Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;LX/1g1;LX/3nB;ZLjava/lang/String;)V

    .line 638079
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_2
    move v0, v1

    move-object v1, v2

    goto :goto_0

    :cond_3
    const/4 v3, 0x0

    goto :goto_1
.end method
