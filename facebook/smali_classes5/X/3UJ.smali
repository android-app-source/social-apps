.class public LX/3UJ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/2dj;

.field public final b:LX/2do;

.field public final c:LX/2hZ;

.field public final d:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/2dj;LX/2do;LX/2hZ;LX/1Ck;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 585747
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 585748
    iput-object p1, p0, LX/3UJ;->a:LX/2dj;

    .line 585749
    iput-object p2, p0, LX/3UJ;->b:LX/2do;

    .line 585750
    iput-object p3, p0, LX/3UJ;->c:LX/2hZ;

    .line 585751
    iput-object p4, p0, LX/3UJ;->d:LX/1Ck;

    .line 585752
    return-void
.end method

.method private a(JLX/2na;LX/84H;Ljava/util/concurrent/Callable;)V
    .locals 9
    .param p4    # LX/84H;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "LX/2na;",
            "LX/84H;",
            "Ljava/util/concurrent/Callable",
            "<",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 585743
    const/4 v0, 0x0

    invoke-static {p3, v0}, LX/2nY;->a(LX/2na;Z)Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v6

    .line 585744
    iget-object v0, p0, LX/3UJ;->b:LX/2do;

    new-instance v1, LX/2f2;

    const/4 v2, 0x1

    invoke-direct {v1, p1, p2, v6, v2}, LX/2f2;-><init>(JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 585745
    iget-object v0, p0, LX/3UJ;->d:LX/1Ck;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "RESPOND_TO_FRIEND_REQUEST_TASK"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    new-instance v1, LX/84D;

    move-object v2, p0

    move-object v3, p4

    move-wide v4, p1

    invoke-direct/range {v1 .. v6}, LX/84D;-><init>(LX/3UJ;LX/84H;JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    invoke-virtual {v0, v7, p5, v1}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 585746
    return-void
.end method

.method public static b(LX/0QB;)LX/3UJ;
    .locals 5

    .prologue
    .line 585753
    new-instance v4, LX/3UJ;

    invoke-static {p0}, LX/2dj;->b(LX/0QB;)LX/2dj;

    move-result-object v0

    check-cast v0, LX/2dj;

    invoke-static {p0}, LX/2do;->a(LX/0QB;)LX/2do;

    move-result-object v1

    check-cast v1, LX/2do;

    invoke-static {p0}, LX/2hZ;->b(LX/0QB;)LX/2hZ;

    move-result-object v2

    check-cast v2, LX/2hZ;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v3

    check-cast v3, LX/1Ck;

    invoke-direct {v4, v0, v1, v2, v3}, LX/3UJ;-><init>(LX/2dj;LX/2do;LX/2hZ;LX/1Ck;)V

    .line 585754
    return-object v4
.end method


# virtual methods
.method public final a(JJLX/2hA;LX/2na;LX/84H;)V
    .locals 9
    .param p7    # LX/84H;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 585740
    new-instance v0, LX/848;

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    move-object v6, p6

    move-object v7, p5

    invoke-direct/range {v0 .. v7}, LX/848;-><init>(LX/3UJ;JJLX/2na;LX/2hA;)V

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p6

    move-object/from16 v5, p7

    move-object v6, v0

    .line 585741
    invoke-direct/range {v1 .. v6}, LX/3UJ;->a(JLX/2na;LX/84H;Ljava/util/concurrent/Callable;)V

    .line 585742
    return-void
.end method

.method public final a(JLX/2hA;LX/2na;LX/84H;)V
    .locals 7
    .param p5    # LX/84H;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 585737
    new-instance v0, LX/847;

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p4

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, LX/847;-><init>(LX/3UJ;JLX/2na;LX/2hA;)V

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p4

    move-object v5, p5

    move-object v6, v0

    .line 585738
    invoke-direct/range {v1 .. v6}, LX/3UJ;->a(JLX/2na;LX/84H;Ljava/util/concurrent/Callable;)V

    .line 585739
    return-void
.end method

.method public final a(JLX/2na;LX/84H;)V
    .locals 11
    .param p4    # LX/84H;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x1

    .line 585732
    sget-object v0, LX/2na;->CONFIRM:LX/2na;

    invoke-virtual {v0, p3}, LX/2na;->equals(Ljava/lang/Object;)Z

    move-result v7

    .line 585733
    invoke-static {p3, v2}, LX/2nY;->a(LX/2na;Z)Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v6

    .line 585734
    iget-object v0, p0, LX/3UJ;->b:LX/2do;

    new-instance v1, LX/2f2;

    invoke-direct {v1, p1, p2, v6, v2}, LX/2f2;-><init>(JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 585735
    iget-object v0, p0, LX/3UJ;->d:LX/1Ck;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "RESPOND_TO_FRIEND_SUGGEST_TASK"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    new-instance v9, LX/849;

    invoke-direct {v9, p0, v7, p1, p2}, LX/849;-><init>(LX/3UJ;ZJ)V

    new-instance v1, LX/84A;

    move-object v2, p0

    move-object v3, p4

    move-wide v4, p1

    invoke-direct/range {v1 .. v7}, LX/84A;-><init>(LX/3UJ;LX/84H;JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)V

    invoke-virtual {v0, v8, v9, v1}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 585736
    return-void
.end method
