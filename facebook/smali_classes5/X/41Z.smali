.class public final LX/41Z;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field public final synthetic a:LX/27d;


# direct methods
.method public constructor <init>(LX/27d;)V
    .locals 0

    .prologue
    .line 666377
    iput-object p1, p0, LX/41Z;->a:LX/27d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 4

    .prologue
    .line 666378
    iget-object v1, p0, LX/41Z;->a:LX/27d;

    monitor-enter v1

    .line 666379
    :try_start_0
    iget-object v0, p0, LX/41Z;->a:LX/27d;

    .line 666380
    iput-object p2, v0, LX/27d;->f:Landroid/os/IBinder;

    .line 666381
    iget-object v0, p0, LX/41Z;->a:LX/27d;

    iget-object v0, v0, LX/27d;->b:Ljava/util/concurrent/Executor;

    iget-object v2, p0, LX/41Z;->a:LX/27d;

    iget-object v2, v2, LX/27d;->i:Ljava/lang/Runnable;

    const v3, -0x47772bf5

    invoke-static {v0, v2, v3}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 666382
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 0

    .prologue
    .line 666383
    return-void
.end method
