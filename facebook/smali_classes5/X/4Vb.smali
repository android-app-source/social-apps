.class public final LX/4Vb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "LX/4Ve",
        "<TT;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/0zO;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:LX/1Mz;


# direct methods
.method public constructor <init>(LX/1Mz;Ljava/lang/String;LX/0zO;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 742637
    iput-object p1, p0, LX/4Vb;->d:LX/1Mz;

    iput-object p2, p0, LX/4Vb;->a:Ljava/lang/String;

    iput-object p3, p0, LX/4Vb;->b:LX/0zO;

    iput-object p4, p0, LX/4Vb;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 7

    .prologue
    .line 742638
    iget-object v0, p0, LX/4Vb;->d:LX/1Mz;

    iget-object v1, p0, LX/4Vb;->a:Ljava/lang/String;

    iget-object v2, p0, LX/4Vb;->b:LX/0zO;

    invoke-static {v0, v1, v2}, LX/1Mz;->a$redex0(LX/1Mz;Ljava/lang/String;LX/0zO;)Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v0

    .line 742639
    iget-object v1, p0, LX/4Vb;->d:LX/1Mz;

    iget-object v2, p0, LX/4Vb;->c:Ljava/lang/String;

    const/4 v4, 0x0

    .line 742640
    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    move-object v3, v4

    .line 742641
    :goto_0
    move-object v1, v3

    .line 742642
    new-instance v2, LX/4Ve;

    invoke-direct {v2, v0, v1}, LX/4Ve;-><init>(Lcom/facebook/graphql/executor/GraphQLResult;Lcom/facebook/graphql/executor/live/ResponseMetadata;)V

    return-object v2

    .line 742643
    :cond_0
    :try_start_0
    const-class v3, Lcom/facebook/graphql/executor/live/ResponseMetadata;

    invoke-static {v1, v2, v3}, LX/1Mz;->a(LX/1Mz;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/executor/live/ResponseMetadata;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 742644
    :catch_0
    move-exception v3

    .line 742645
    iget-object v5, v1, LX/1Mz;->m:LX/03V;

    const-string v6, "GraphQLLiveQueryExecutor"

    const-string p0, "failed to parse response metadata"

    invoke-virtual {v5, v6, p0, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v3, v4

    .line 742646
    goto :goto_0
.end method
