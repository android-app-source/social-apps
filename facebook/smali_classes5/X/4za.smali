.class public LX/4za;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0qF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/0qF",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TK;"
        }
    .end annotation
.end field

.field public final b:I

.field public final c:LX/0qF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0qF",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public volatile d:LX/0cp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0cp",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Object;ILX/0qF;)V
    .locals 1
    .param p3    # LX/0qF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;I",
            "LX/0qF",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 822938
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 822939
    sget-object v0, LX/0cn;->e:LX/0cp;

    move-object v0, v0

    .line 822940
    iput-object v0, p0, LX/4za;->d:LX/0cp;

    .line 822941
    iput-object p1, p0, LX/4za;->a:Ljava/lang/Object;

    .line 822942
    iput p2, p0, LX/4za;->b:I

    .line 822943
    iput-object p3, p0, LX/4za;->c:LX/0qF;

    .line 822944
    return-void
.end method


# virtual methods
.method public getExpirationTime()J
    .locals 1

    .prologue
    .line 822946
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final getHash()I
    .locals 1

    .prologue
    .line 822945
    iget v0, p0, LX/4za;->b:I

    return v0
.end method

.method public final getKey()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TK;"
        }
    .end annotation

    .prologue
    .line 822923
    iget-object v0, p0, LX/4za;->a:Ljava/lang/Object;

    return-object v0
.end method

.method public final getNext()LX/0qF;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0qF",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 822937
    iget-object v0, p0, LX/4za;->c:LX/0qF;

    return-object v0
.end method

.method public getNextEvictable()LX/0qF;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0qF",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 822936
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getNextExpirable()LX/0qF;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0qF",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 822935
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getPreviousEvictable()LX/0qF;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0qF",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 822934
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getPreviousExpirable()LX/0qF;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0qF",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 822947
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final getValueReference()LX/0cp;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0cp",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 822933
    iget-object v0, p0, LX/4za;->d:LX/0cp;

    return-object v0
.end method

.method public setExpirationTime(J)V
    .locals 1

    .prologue
    .line 822932
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public setNextEvictable(LX/0qF;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0qF",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 822931
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public setNextExpirable(LX/0qF;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0qF",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 822930
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public setPreviousEvictable(LX/0qF;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0qF",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 822929
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public setPreviousExpirable(LX/0qF;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0qF",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 822928
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final setValueReference(LX/0cp;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0cp",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 822924
    iget-object v0, p0, LX/4za;->d:LX/0cp;

    .line 822925
    iput-object p1, p0, LX/4za;->d:LX/0cp;

    .line 822926
    invoke-interface {v0}, LX/0cp;->c()V

    .line 822927
    return-void
.end method
