.class public final LX/407;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2Eu;
.implements LX/2Ev;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/2Eu;",
        "LX/2Ev",
        "<",
        "Landroid/os/PersistableBundle;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/os/PersistableBundle;


# direct methods
.method public constructor <init>(Landroid/os/PersistableBundle;)V
    .locals 0

    .prologue
    .line 662931
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 662932
    iput-object p1, p0, LX/407;->a:Landroid/os/PersistableBundle;

    .line 662933
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;I)I
    .locals 1

    .prologue
    .line 662934
    iget-object v0, p0, LX/407;->a:Landroid/os/PersistableBundle;

    invoke-virtual {v0, p1, p2}, Landroid/os/PersistableBundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 662935
    iget-object v0, p0, LX/407;->a:Landroid/os/PersistableBundle;

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 662936
    iget-object v0, p0, LX/407;->a:Landroid/os/PersistableBundle;

    invoke-virtual {v0, p1, p2}, Landroid/os/PersistableBundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 662937
    iget-object v0, p0, LX/407;->a:Landroid/os/PersistableBundle;

    invoke-virtual {v0, p1, p2}, Landroid/os/PersistableBundle;->putInt(Ljava/lang/String;I)V

    .line 662938
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 662939
    iget-object v0, p0, LX/407;->a:Landroid/os/PersistableBundle;

    invoke-virtual {v0, p1, p2}, Landroid/os/PersistableBundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 662940
    return-void
.end method
