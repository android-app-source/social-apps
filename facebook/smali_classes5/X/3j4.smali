.class public LX/3j4;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Ps;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static d:LX/0Xm;


# instance fields
.field private final b:LX/3j5;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1V0;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 632016
    const-class v0, LX/3j4;

    sput-object v0, LX/3j4;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/3j5;LX/0Ot;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3j5;",
            "LX/0Ot",
            "<",
            "LX/1V0;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 632017
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 632018
    iput-object p1, p0, LX/3j4;->b:LX/3j5;

    .line 632019
    iput-object p2, p0, LX/3j4;->c:LX/0Ot;

    .line 632020
    sget-object v0, LX/3j6;->a:LX/0P1;

    sget-object v1, LX/3jH;->a:LX/0P1;

    sget-object v2, LX/3jL;->a:LX/0P1;

    invoke-static {v0, v1, v2}, LX/3jU;->a(LX/0P1;LX/0P1;LX/0P1;)V

    .line 632021
    return-void
.end method

.method public static a(LX/0QB;)LX/3j4;
    .locals 5

    .prologue
    .line 632022
    const-class v1, LX/3j4;

    monitor-enter v1

    .line 632023
    :try_start_0
    sget-object v0, LX/3j4;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 632024
    sput-object v2, LX/3j4;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 632025
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 632026
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 632027
    new-instance v4, LX/3j4;

    const-class v3, LX/3j5;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/3j5;

    const/16 p0, 0x6fd

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v4, v3, p0}, LX/3j4;-><init>(LX/3j5;LX/0Ot;)V

    .line 632028
    move-object v0, v4

    .line 632029
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 632030
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/3j4;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 632031
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 632032
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLNativeTemplateView;LX/1Pq;LX/0jW;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1X1;
    .locals 7
    .param p3    # LX/0jW;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLNativeTemplateView;",
            "TE;",
            "LX/0jW;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;)",
            "LX/1X1;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 632033
    const-string v1, "%s.getComponent"

    sget-object v3, LX/3j4;->a:Ljava/lang/Class;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    const v4, 0x154fecf8

    invoke-static {v1, v3, v4}, LX/02m;->a(Ljava/lang/String;Ljava/lang/Object;I)V

    .line 632034
    if-eqz p3, :cond_1

    .line 632035
    :try_start_0
    move-object v0, p2

    check-cast v0, LX/1Pr;

    move-object v1, v0

    new-instance v3, LX/COB;

    invoke-direct {v3, p1}, LX/COB;-><init>(Lcom/facebook/graphql/model/GraphQLNativeTemplateView;)V

    invoke-interface {v1, v3, p3}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/COC;

    move-object v4, v1

    .line 632036
    :goto_0
    iget-object v0, v4, LX/COC;->a:LX/CNc;

    move-object v1, v0

    .line 632037
    if-nez v1, :cond_0

    .line 632038
    instance-of v1, p2, LX/1Pf;

    if-eqz v1, :cond_2

    move-object v0, p2

    check-cast v0, LX/1Pf;

    move-object v1, v0

    move-object v3, v1

    .line 632039
    :goto_1
    iget-object v5, p0, LX/3j4;->b:LX/3j5;

    move-object v0, p2

    check-cast v0, LX/1Pn;

    move-object v1, v0

    invoke-interface {v1}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v6

    if-eqz p4, :cond_3

    .line 632040
    iget-object v0, p4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v0

    .line 632041
    check-cast v1, Lcom/facebook/graphql/model/FeedUnit;

    :goto_2
    invoke-virtual {v5, v6, v1, v3}, LX/3j5;->a(Landroid/content/Context;Lcom/facebook/graphql/model/FeedUnit;LX/1Pf;)LX/CNr;

    move-result-object v1

    .line 632042
    invoke-static {p1, v1}, LX/CNu;->a(Lcom/facebook/graphql/model/GraphQLNativeTemplateView;LX/CNc;)LX/0Px;

    move-result-object v2

    .line 632043
    iget-object v3, v1, LX/CNc;->b:LX/CNS;

    invoke-virtual {v3, v2}, LX/CNS;->a(LX/0Px;)V

    .line 632044
    iget-object v2, v1, LX/CNc;->b:LX/CNS;

    new-instance v3, LX/COA;

    invoke-direct {v3, p2, v4, v1, p4}, LX/COA;-><init>(LX/1Pq;LX/COC;LX/CNc;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    iput-object v3, v2, LX/CNS;->b:LX/CNT;

    .line 632045
    :cond_0
    new-instance v2, LX/COE;

    invoke-direct {v2, p0, v1}, LX/COE;-><init>(LX/3j4;LX/CNc;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 632046
    const v1, 0x59f89565

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v2

    .line 632047
    :cond_1
    :try_start_1
    move-object v0, p2

    check-cast v0, LX/1Pr;

    move-object v1, v0

    new-instance v3, LX/COB;

    invoke-direct {v3, p1}, LX/COB;-><init>(Lcom/facebook/graphql/model/GraphQLNativeTemplateView;)V

    invoke-interface {v1, v3}, LX/1Pr;->a(LX/1KL;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/COC;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v4, v1

    goto :goto_0

    :cond_2
    move-object v3, v2

    .line 632048
    goto :goto_1

    :cond_3
    move-object v1, v2

    .line 632049
    goto :goto_2

    .line 632050
    :catchall_0
    move-exception v1

    const v2, -0x6d03961e

    invoke-static {v2}, LX/02m;->a(I)V

    throw v1
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLNativeTemplateView;LX/1Pq;LX/0jW;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1De;)LX/1X1;
    .locals 4
    .param p3    # LX/0jW;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLNativeTemplateView;",
            "TE;",
            "LX/0jW;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;",
            "LX/1De;",
            ")",
            "LX/1X1;"
        }
    .end annotation

    .prologue
    .line 632051
    invoke-virtual {p0, p1, p2, p3, p4}, LX/3j4;->a(Lcom/facebook/graphql/model/GraphQLNativeTemplateView;LX/1Pq;LX/0jW;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1X1;

    move-result-object v1

    .line 632052
    iget-object v0, p0, LX/3j4;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1V0;

    check-cast p2, LX/1Ps;

    new-instance v2, LX/1X6;

    sget-object v3, LX/1Ua;->a:LX/1Ua;

    invoke-direct {v2, p4, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-virtual {v0, p5, p2, v2, v1}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method
