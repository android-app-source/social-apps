.class public LX/3lB;
.super LX/3lC;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/3lB;


# instance fields
.field private final a:LX/3lD;


# direct methods
.method public constructor <init>(LX/3lD;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 633622
    invoke-direct {p0}, LX/3lC;-><init>()V

    .line 633623
    iput-object p1, p0, LX/3lB;->a:LX/3lD;

    .line 633624
    return-void
.end method

.method public static a(LX/0QB;)LX/3lB;
    .locals 4

    .prologue
    .line 633609
    sget-object v0, LX/3lB;->b:LX/3lB;

    if-nez v0, :cond_1

    .line 633610
    const-class v1, LX/3lB;

    monitor-enter v1

    .line 633611
    :try_start_0
    sget-object v0, LX/3lB;->b:LX/3lB;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 633612
    if-eqz v2, :cond_0

    .line 633613
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 633614
    new-instance p0, LX/3lB;

    invoke-static {v0}, LX/3lD;->a(LX/0QB;)LX/3lD;

    move-result-object v3

    check-cast v3, LX/3lD;

    invoke-direct {p0, v3}, LX/3lB;-><init>(LX/3lD;)V

    .line 633615
    move-object v0, p0

    .line 633616
    sput-object v0, LX/3lB;->b:LX/3lB;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 633617
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 633618
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 633619
    :cond_1
    sget-object v0, LX/3lB;->b:LX/3lB;

    return-object v0

    .line 633620
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 633621
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()J
    .locals 4

    .prologue
    .line 633607
    const-wide/32 v2, 0x5265c00

    move-wide v0, v2

    .line 633608
    return-wide v0
.end method

.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/10S;
    .locals 1

    .prologue
    .line 633606
    iget-object v0, p0, LX/3lB;->a:LX/3lD;

    invoke-virtual {v0}, LX/3lD;->c()LX/10S;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/view/View;Lcom/facebook/graphql/model/GraphQLFeedback;)V
    .locals 4

    .prologue
    .line 633597
    new-instance v0, LX/0hs;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 633598
    invoke-virtual {v0}, LX/0ht;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 633599
    if-nez v1, :cond_0

    .line 633600
    :goto_0
    return-void

    .line 633601
    :cond_0
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f081990

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0hs;->a(Ljava/lang/CharSequence;)V

    .line 633602
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081991

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0hs;->b(Ljava/lang/CharSequence;)V

    .line 633603
    const/4 v1, -0x1

    .line 633604
    iput v1, v0, LX/0hs;->t:I

    .line 633605
    invoke-virtual {v0, p1}, LX/0ht;->a(Landroid/view/View;)V

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 633596
    const-string v0, "4305"

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 633595
    invoke-static {}, LX/3lD;->a()LX/0Px;

    move-result-object v0

    return-object v0
.end method
