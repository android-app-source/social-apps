.class public final LX/4FQ;
.super LX/0gS;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 679301
    invoke-direct {p0}, LX/0gS;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0zR;)LX/4FQ;
    .locals 1

    .prologue
    .line 679271
    const-string v0, "nt_context"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 679272
    return-object p0
.end method

.method public final a(Ljava/lang/Boolean;)LX/4FQ;
    .locals 1

    .prologue
    .line 679299
    const-string v0, "should_return_top_independent_modules_only"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 679300
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/4FQ;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/GraphSearchMatchType;
        .end annotation
    .end param

    .prologue
    .line 679297
    const-string v0, "match"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 679298
    return-object p0
.end method

.method public final a(Ljava/util/List;)LX/4FQ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/4FP;",
            ">;)",
            "LX/4FQ;"
        }
    .end annotation

    .prologue
    .line 679295
    const-string v0, "filters"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 679296
    return-object p0
.end method

.method public final b(Ljava/lang/Boolean;)LX/4FQ;
    .locals 1

    .prologue
    .line 679293
    const-string v0, "are_top_independent_modules_already_shown"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 679294
    return-object p0
.end method

.method public final b(Ljava/lang/String;)LX/4FQ;
    .locals 1

    .prologue
    .line 679291
    const-string v0, "callsite"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 679292
    return-object p0
.end method

.method public final b(Ljava/util/List;)LX/4FQ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/4FQ;"
        }
    .end annotation

    .prologue
    .line 679289
    const-string v0, "supported_experiences"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 679290
    return-object p0
.end method

.method public final c(Ljava/lang/Boolean;)LX/4FQ;
    .locals 1

    .prologue
    .line 679287
    const-string v0, "first_unit_only"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 679288
    return-object p0
.end method

.method public final c(Ljava/lang/String;)LX/4FQ;
    .locals 1

    .prologue
    .line 679285
    const-string v0, "tsid"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 679286
    return-object p0
.end method

.method public final c(Ljava/util/List;)LX/4FQ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/4FS;",
            ">;)",
            "LX/4FQ;"
        }
    .end annotation

    .prologue
    .line 679283
    const-string v0, "module_sizes"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 679284
    return-object p0
.end method

.method public final d(Ljava/lang/Boolean;)LX/4FQ;
    .locals 1

    .prologue
    .line 679281
    const-string v0, "independent_module_or_first_unit"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 679282
    return-object p0
.end method

.method public final d(Ljava/lang/String;)LX/4FQ;
    .locals 1

    .prologue
    .line 679279
    const-string v0, "bsid"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 679280
    return-object p0
.end method

.method public final d(Ljava/util/List;)LX/4FQ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/4FQ;"
        }
    .end annotation

    .prologue
    .line 679277
    const-string v0, "supported_roles"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 679278
    return-object p0
.end method

.method public final e(Ljava/lang/String;)LX/4FQ;
    .locals 1

    .prologue
    .line 679275
    const-string v0, "query_source"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 679276
    return-object p0
.end method

.method public final e(Ljava/util/List;)LX/4FQ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/4FQ;"
        }
    .end annotation

    .prologue
    .line 679273
    const-string v0, "preloaded_story_ids"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 679274
    return-object p0
.end method
