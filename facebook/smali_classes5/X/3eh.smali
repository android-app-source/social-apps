.class public LX/3eh;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/3dt;

.field private final b:LX/3e2;

.field public final c:LX/3dx;

.field public final d:LX/18V;

.field public final e:LX/3ei;

.field public final f:Lcom/facebook/stickers/data/StickerAssetDownloader;


# direct methods
.method public constructor <init>(LX/3dt;LX/3e2;LX/3dx;LX/18V;LX/3ei;Lcom/facebook/stickers/data/StickerAssetDownloader;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 620629
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 620630
    iput-object p1, p0, LX/3eh;->a:LX/3dt;

    .line 620631
    iput-object p2, p0, LX/3eh;->b:LX/3e2;

    .line 620632
    iput-object p3, p0, LX/3eh;->c:LX/3dx;

    .line 620633
    iput-object p4, p0, LX/3eh;->d:LX/18V;

    .line 620634
    iput-object p5, p0, LX/3eh;->e:LX/3ei;

    .line 620635
    iput-object p6, p0, LX/3eh;->f:Lcom/facebook/stickers/data/StickerAssetDownloader;

    .line 620636
    return-void
.end method

.method public static a(LX/3eh;Ljava/util/Collection;)Ljava/util/Set;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/facebook/stickers/model/Sticker;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/stickers/model/Sticker;",
            ">;"
        }
    .end annotation

    .prologue
    .line 620596
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 620597
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/model/Sticker;

    .line 620598
    iget-object v0, v0, Lcom/facebook/stickers/model/Sticker;->b:Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 620599
    :cond_0
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 620600
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 620601
    iget-object v3, p0, LX/3eh;->a:LX/3dt;

    invoke-virtual {v3, v0}, LX/3dt;->b(Ljava/lang/String;)Lcom/facebook/stickers/model/StickerPack;

    move-result-object v3

    .line 620602
    if-eqz v3, :cond_2

    .line 620603
    invoke-interface {v2, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 620604
    :cond_2
    :try_start_0
    iget-object v3, p0, LX/3eh;->c:LX/3dx;

    invoke-virtual {v3, v0}, LX/3dx;->b(Ljava/lang/String;)Lcom/facebook/stickers/model/StickerPack;

    move-result-object v3

    .line 620605
    if-eqz v3, :cond_1

    .line 620606
    invoke-interface {v2, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 620607
    :catch_0
    goto :goto_1

    .line 620608
    :cond_3
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 620609
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_4
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/model/Sticker;

    .line 620610
    iget-object v1, v0, Lcom/facebook/stickers/model/Sticker;->b:Ljava/lang/String;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/stickers/model/StickerPack;

    .line 620611
    if-eqz v1, :cond_4

    .line 620612
    invoke-static {}, Lcom/facebook/stickers/model/StickerCapabilities;->newBuilder()LX/4m3;

    move-result-object v5

    .line 620613
    iget-object p0, v1, Lcom/facebook/stickers/model/StickerPack;->r:Lcom/facebook/stickers/model/StickerCapabilities;

    move-object v1, p0

    .line 620614
    iget-object p0, v1, Lcom/facebook/stickers/model/StickerCapabilities;->a:LX/03R;

    iput-object p0, v5, LX/4m3;->a:LX/03R;

    .line 620615
    iget-object p0, v1, Lcom/facebook/stickers/model/StickerCapabilities;->b:LX/03R;

    iput-object p0, v5, LX/4m3;->b:LX/03R;

    .line 620616
    iget-object p0, v1, Lcom/facebook/stickers/model/StickerCapabilities;->c:LX/03R;

    iput-object p0, v5, LX/4m3;->c:LX/03R;

    .line 620617
    iget-object p0, v1, Lcom/facebook/stickers/model/StickerCapabilities;->d:LX/03R;

    iput-object p0, v5, LX/4m3;->d:LX/03R;

    .line 620618
    iget-object p0, v1, Lcom/facebook/stickers/model/StickerCapabilities;->e:LX/03R;

    iput-object p0, v5, LX/4m3;->e:LX/03R;

    .line 620619
    iget-object p0, v1, Lcom/facebook/stickers/model/StickerCapabilities;->f:LX/03R;

    iput-object p0, v5, LX/4m3;->f:LX/03R;

    .line 620620
    move-object v1, v5

    .line 620621
    invoke-virtual {v1}, LX/4m3;->a()Lcom/facebook/stickers/model/StickerCapabilities;

    move-result-object v1

    .line 620622
    new-instance v5, LX/4m0;

    invoke-direct {v5}, LX/4m0;-><init>()V

    move-object v5, v5

    .line 620623
    invoke-virtual {v5, v0}, LX/4m0;->a(Lcom/facebook/stickers/model/Sticker;)LX/4m0;

    move-result-object v0

    .line 620624
    iput-object v1, v0, LX/4m0;->i:Lcom/facebook/stickers/model/StickerCapabilities;

    .line 620625
    move-object v0, v0

    .line 620626
    invoke-virtual {v0}, LX/4m0;->a()Lcom/facebook/stickers/model/Sticker;

    move-result-object v0

    .line 620627
    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 620628
    :cond_5
    return-object v3
.end method

.method public static a(LX/3eh;Ljava/util/Map;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/stickers/model/Sticker;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 620637
    iget-object v1, p0, LX/3eh;->a:LX/3dt;

    invoke-virtual {v1}, LX/3dt;->a()LX/0Px;

    move-result-object v1

    .line 620638
    if-nez v1, :cond_0

    .line 620639
    :try_start_0
    iget-object v2, p0, LX/3eh;->c:LX/3dx;

    invoke-virtual {v2}, LX/3dx;->a()LX/0Px;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 620640
    :cond_0
    :goto_0
    if-nez v1, :cond_2

    .line 620641
    :cond_1
    :goto_1
    return-void

    .line 620642
    :cond_2
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move v2, v0

    move v3, v0

    .line 620643
    :goto_2
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 620644
    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/model/Sticker;

    .line 620645
    iget-object v1, v0, Lcom/facebook/stickers/model/Sticker;->a:Ljava/lang/String;

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/stickers/model/Sticker;

    .line 620646
    if-eqz v1, :cond_4

    iget-object v0, v0, Lcom/facebook/stickers/model/Sticker;->i:Lcom/facebook/stickers/model/StickerCapabilities;

    invoke-virtual {v0}, Lcom/facebook/stickers/model/StickerCapabilities;->b()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, v1, Lcom/facebook/stickers/model/Sticker;->i:Lcom/facebook/stickers/model/StickerCapabilities;

    invoke-virtual {v0}, Lcom/facebook/stickers/model/StickerCapabilities;->b()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 620647
    invoke-interface {v4, v2, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 620648
    const/4 v3, 0x1

    move v1, v3

    .line 620649
    :goto_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v3, v1

    goto :goto_2

    .line 620650
    :cond_3
    if-eqz v3, :cond_1

    .line 620651
    iget-object v0, p0, LX/3eh;->c:LX/3dx;

    invoke-virtual {v0, v4}, LX/3dx;->a(Ljava/util/List;)V

    .line 620652
    iget-object v0, p0, LX/3eh;->a:LX/3dt;

    invoke-virtual {v0, v4}, LX/3dt;->a(Ljava/util/List;)V

    goto :goto_1

    :catch_0
    goto :goto_0

    :cond_4
    move v1, v3

    goto :goto_3
.end method

.method public static a(Ljava/util/Map;Ljava/util/Collection;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/stickers/model/Sticker;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "Lcom/facebook/stickers/model/Sticker;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 620593
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/model/Sticker;

    .line 620594
    iget-object v2, v0, Lcom/facebook/stickers/model/Sticker;->a:Ljava/lang/String;

    invoke-interface {p0, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 620595
    :cond_0
    return-void
.end method

.method public static a(Landroid/net/Uri;)Z
    .locals 2
    .param p0    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 620592
    if-eqz p0, :cond_0

    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Lcom/facebook/stickers/service/FetchStickersParams;)Lcom/facebook/stickers/service/FetchStickersResult;
    .locals 5

    .prologue
    .line 620562
    iget-object v0, p1, Lcom/facebook/stickers/service/FetchStickersParams;->a:LX/0Px;

    move-object v0, v0

    .line 620563
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 620564
    new-instance v0, Lcom/facebook/stickers/service/FetchStickersResult;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/stickers/service/FetchStickersResult;-><init>(Ljava/util/List;)V

    .line 620565
    :goto_0
    return-object v0

    .line 620566
    :cond_0
    iget-object v0, p0, LX/3eh;->d:LX/18V;

    iget-object v1, p0, LX/3eh;->e:LX/3ei;

    invoke-virtual {v0, v1, p1}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/service/FetchStickersResult;

    .line 620567
    iget-object v1, v0, Lcom/facebook/stickers/service/FetchStickersResult;->a:LX/0Px;

    move-object v1, v1

    .line 620568
    iget-object v0, p0, LX/3eh;->c:LX/3dx;

    .line 620569
    iget-object v2, p1, Lcom/facebook/stickers/service/FetchStickersParams;->a:LX/0Px;

    move-object v2, v2

    .line 620570
    invoke-virtual {v0, v2}, LX/3dx;->a(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    .line 620571
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/model/Sticker;

    .line 620572
    iget-object v3, v0, Lcom/facebook/stickers/model/Sticker;->d:Landroid/net/Uri;

    if-eqz v3, :cond_2

    .line 620573
    new-instance v3, Ljava/io/File;

    iget-object v4, v0, Lcom/facebook/stickers/model/Sticker;->d:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, LX/2W9;->b(Ljava/io/File;)Z

    .line 620574
    :cond_2
    iget-object v3, v0, Lcom/facebook/stickers/model/Sticker;->f:Landroid/net/Uri;

    if-eqz v3, :cond_3

    .line 620575
    new-instance v3, Ljava/io/File;

    iget-object v4, v0, Lcom/facebook/stickers/model/Sticker;->f:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, LX/2W9;->b(Ljava/io/File;)Z

    .line 620576
    :cond_3
    iget-object v3, v0, Lcom/facebook/stickers/model/Sticker;->h:Landroid/net/Uri;

    if-eqz v3, :cond_1

    .line 620577
    new-instance v3, Ljava/io/File;

    iget-object v0, v0, Lcom/facebook/stickers/model/Sticker;->h:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, LX/2W9;->b(Ljava/io/File;)Z

    goto :goto_1

    .line 620578
    :cond_4
    iget-object v0, p0, LX/3eh;->b:LX/3e2;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 620579
    const v0, -0x10da11bf

    invoke-static {v2, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 620580
    :try_start_0
    iget-object v0, p0, LX/3eh;->c:LX/3dx;

    .line 620581
    iget-object v3, p1, Lcom/facebook/stickers/service/FetchStickersParams;->a:LX/0Px;

    move-object v3, v3

    .line 620582
    invoke-virtual {v0, v3}, LX/3dx;->c(Ljava/util/Collection;)V

    .line 620583
    iget-object v0, p0, LX/3eh;->c:LX/3dx;

    invoke-virtual {v0, v1}, LX/3dx;->b(Ljava/util/Collection;)V

    .line 620584
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 620585
    const v0, 0x74323269

    invoke-static {v2, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 620586
    iget-object v0, p0, LX/3eh;->a:LX/3dt;

    invoke-virtual {v0, v1}, LX/3dt;->b(Ljava/util/Collection;)V

    .line 620587
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 620588
    invoke-static {v0, v1}, LX/3eh;->a(Ljava/util/Map;Ljava/util/Collection;)V

    .line 620589
    invoke-static {p0, v0}, LX/3eh;->a(LX/3eh;Ljava/util/Map;)V

    .line 620590
    new-instance v0, Lcom/facebook/stickers/service/FetchStickersResult;

    invoke-direct {v0, v1}, Lcom/facebook/stickers/service/FetchStickersResult;-><init>(Ljava/util/List;)V

    goto/16 :goto_0

    .line 620591
    :catchall_0
    move-exception v0

    const v1, -0x425384b

    invoke-static {v2, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/stickers/service/FetchStickersParams;)Lcom/facebook/stickers/service/FetchStickersResult;
    .locals 8

    .prologue
    .line 620503
    const-string v0, "StickersHandler.handleFetchStickers"

    const v1, -0x3fc837fb

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 620504
    :try_start_0
    sget-object v0, LX/3eo;->a:[I

    .line 620505
    iget-object v1, p1, Lcom/facebook/stickers/service/FetchStickersParams;->b:LX/3eg;

    move-object v1, v1

    .line 620506
    invoke-virtual {v1}, LX/3eg;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 620507
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot fetch stickers without operation type specified"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 620508
    :catchall_0
    move-exception v0

    const v1, 0x1210306

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 620509
    :pswitch_0
    :try_start_1
    invoke-direct {p0, p1}, LX/3eh;->b(Lcom/facebook/stickers/service/FetchStickersParams;)Lcom/facebook/stickers/service/FetchStickersResult;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 620510
    const v1, 0x23aab42b

    invoke-static {v1}, LX/02m;->a(I)V

    :goto_0
    return-object v0

    .line 620511
    :pswitch_1
    :try_start_2
    const/4 v2, 0x0

    .line 620512
    iget-object v0, p1, Lcom/facebook/stickers/service/FetchStickersParams;->a:LX/0Px;

    move-object v0, v0

    .line 620513
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v1}, LX/0PM;->c(I)Ljava/util/LinkedHashMap;

    move-result-object v4

    .line 620514
    iget-object v1, p0, LX/3eh;->a:LX/3dt;

    invoke-virtual {v1, v0}, LX/3dt;->a(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    .line 620515
    invoke-static {v4, v0}, LX/3eh;->a(Ljava/util/Map;Ljava/util/Collection;)V

    .line 620516
    new-instance v3, Ljava/util/HashSet;

    .line 620517
    iget-object v1, p1, Lcom/facebook/stickers/service/FetchStickersParams;->a:LX/0Px;

    move-object v1, v1

    .line 620518
    invoke-direct {v3, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 620519
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/stickers/model/Sticker;

    .line 620520
    iget-object v1, v1, Lcom/facebook/stickers/model/Sticker;->a:Ljava/lang/String;

    invoke-interface {v3, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    .line 620521
    :cond_0
    new-instance v1, Lcom/facebook/stickers/service/FetchStickersParams;

    sget-object v5, LX/3eg;->DO_NOT_UPDATE_IF_CACHED:LX/3eg;

    invoke-direct {v1, v3, v5}, Lcom/facebook/stickers/service/FetchStickersParams;-><init>(Ljava/util/Collection;LX/3eg;)V

    move-object v1, v1

    .line 620522
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/model/Sticker;

    .line 620523
    iget-object v0, v0, Lcom/facebook/stickers/model/Sticker;->i:Lcom/facebook/stickers/model/StickerCapabilities;

    invoke-virtual {v0}, Lcom/facebook/stickers/model/StickerCapabilities;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 620524
    const/4 v0, 0x1

    .line 620525
    :goto_2
    iget-object v3, v1, Lcom/facebook/stickers/service/FetchStickersParams;->a:LX/0Px;

    move-object v3, v3

    .line 620526
    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    if-eqz v0, :cond_a

    .line 620527
    :cond_2
    iget-object v0, p0, LX/3eh;->c:LX/3dx;

    .line 620528
    iget-object v3, v1, Lcom/facebook/stickers/service/FetchStickersParams;->a:LX/0Px;

    move-object v1, v3

    .line 620529
    invoke-virtual {v0, v1}, LX/3dx;->a(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    .line 620530
    invoke-static {v4, v0}, LX/3eh;->a(Ljava/util/Map;Ljava/util/Collection;)V

    .line 620531
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 620532
    invoke-virtual {v4}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/model/Sticker;

    .line 620533
    iget-object v5, v0, Lcom/facebook/stickers/model/Sticker;->i:Lcom/facebook/stickers/model/StickerCapabilities;

    invoke-virtual {v5}, Lcom/facebook/stickers/model/StickerCapabilities;->b()Z

    move-result v5

    if-nez v5, :cond_3

    .line 620534
    iget-object v5, v0, Lcom/facebook/stickers/model/Sticker;->a:Ljava/lang/String;

    invoke-interface {v1, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    .line 620535
    :cond_4
    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-static {p0, v0}, LX/3eh;->a(LX/3eh;Ljava/util/Collection;)Ljava/util/Set;

    move-result-object v0

    .line 620536
    invoke-static {v4, v0}, LX/3eh;->a(Ljava/util/Map;Ljava/util/Collection;)V

    .line 620537
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    .line 620538
    iget-object v1, p0, LX/3eh;->c:LX/3dx;

    invoke-virtual {v1, v0}, LX/3dx;->b(Ljava/util/Collection;)V

    .line 620539
    :cond_5
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 620540
    iget-object v0, p1, Lcom/facebook/stickers/service/FetchStickersParams;->a:LX/0Px;

    move-object v6, v0

    .line 620541
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v3, v2

    :goto_4
    if-ge v3, v7, :cond_8

    invoke-virtual {v6, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 620542
    invoke-virtual {v4, v0}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/stickers/model/Sticker;

    .line 620543
    if-eqz v1, :cond_6

    iget-object v1, v1, Lcom/facebook/stickers/model/Sticker;->i:Lcom/facebook/stickers/model/StickerCapabilities;

    invoke-virtual {v1}, Lcom/facebook/stickers/model/StickerCapabilities;->b()Z

    move-result v1

    if-nez v1, :cond_7

    .line 620544
    :cond_6
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 620545
    :cond_7
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_4

    .line 620546
    :cond_8
    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    .line 620547
    new-instance v0, Lcom/facebook/stickers/service/FetchStickersParams;

    sget-object v1, LX/3eg;->DO_NOT_UPDATE_IF_CACHED:LX/3eg;

    invoke-direct {v0, v5, v1}, Lcom/facebook/stickers/service/FetchStickersParams;-><init>(Ljava/util/Collection;LX/3eg;)V

    .line 620548
    iget-object v1, p0, LX/3eh;->d:LX/18V;

    iget-object v3, p0, LX/3eh;->e:LX/3ei;

    invoke-virtual {v1, v3, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/service/FetchStickersResult;

    .line 620549
    iget-object v1, v0, Lcom/facebook/stickers/service/FetchStickersResult;->a:LX/0Px;

    move-object v0, v1

    .line 620550
    invoke-static {v4, v0}, LX/3eh;->a(Ljava/util/Map;Ljava/util/Collection;)V

    .line 620551
    iget-object v1, p0, LX/3eh;->c:LX/3dx;

    invoke-virtual {v1, v0}, LX/3dx;->b(Ljava/util/Collection;)V

    .line 620552
    :cond_9
    iget-object v0, p0, LX/3eh;->a:LX/3dt;

    invoke-virtual {v4}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/3dt;->b(Ljava/util/Collection;)V

    .line 620553
    :cond_a
    invoke-static {p0, v4}, LX/3eh;->a(LX/3eh;Ljava/util/Map;)V

    .line 620554
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 620555
    iget-object v0, p1, Lcom/facebook/stickers/service/FetchStickersParams;->a:LX/0Px;

    move-object v3, v0

    .line 620556
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v5

    :goto_5
    if-ge v2, v5, :cond_c

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 620557
    invoke-virtual {v4, v0}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 620558
    invoke-virtual {v4, v0}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 620559
    :cond_b
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 620560
    :cond_c
    new-instance v0, Lcom/facebook/stickers/service/FetchStickersResult;

    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/stickers/service/FetchStickersResult;-><init>(Ljava/util/List;)V

    move-object v0, v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 620561
    const v1, 0x552447eb

    invoke-static {v1}, LX/02m;->a(I)V

    goto/16 :goto_0

    :cond_d
    move v0, v2

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
