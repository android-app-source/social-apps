.class public LX/4QR;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 704635
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 28

    .prologue
    .line 704636
    const/16 v22, 0x0

    .line 704637
    const/16 v21, 0x0

    .line 704638
    const/16 v20, 0x0

    .line 704639
    const/16 v19, 0x0

    .line 704640
    const/16 v18, 0x0

    .line 704641
    const/16 v17, 0x0

    .line 704642
    const/16 v16, 0x0

    .line 704643
    const/4 v15, 0x0

    .line 704644
    const/4 v14, 0x0

    .line 704645
    const/4 v13, 0x0

    .line 704646
    const/4 v12, 0x0

    .line 704647
    const-wide/16 v10, 0x0

    .line 704648
    const/4 v9, 0x0

    .line 704649
    const/4 v8, 0x0

    .line 704650
    const/4 v7, 0x0

    .line 704651
    const/4 v6, 0x0

    .line 704652
    const/4 v5, 0x0

    .line 704653
    const/4 v4, 0x0

    .line 704654
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v23

    sget-object v24, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    if-eq v0, v1, :cond_14

    .line 704655
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 704656
    const/4 v4, 0x0

    .line 704657
    :goto_0
    return v4

    .line 704658
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 704659
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v23

    sget-object v24, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    if-eq v0, v1, :cond_d

    .line 704660
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v23

    .line 704661
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 704662
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v24

    sget-object v25, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    if-eq v0, v1, :cond_1

    if-eqz v23, :cond_1

    .line 704663
    const-string v24, "ad_account"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_2

    .line 704664
    invoke-static/range {p0 .. p1}, LX/4Ke;->a(LX/15w;LX/186;)I

    move-result v22

    goto :goto_1

    .line 704665
    :cond_2
    const-string v24, "audience_option"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_3

    .line 704666
    const/4 v11, 0x1

    .line 704667
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-result-object v21

    goto :goto_1

    .line 704668
    :cond_3
    const-string v24, "budget"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_4

    .line 704669
    invoke-static/range {p0 .. p1}, LX/4Lf;->a(LX/15w;LX/186;)I

    move-result v20

    goto :goto_1

    .line 704670
    :cond_4
    const-string v24, "feed_unit_preview"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_5

    .line 704671
    invoke-static/range {p0 .. p1}, LX/2ao;->a(LX/15w;LX/186;)I

    move-result v19

    goto :goto_1

    .line 704672
    :cond_5
    const-string v24, "has_ad_conversion_pixel_domain"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_6

    .line 704673
    const/4 v10, 0x1

    .line 704674
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v18

    goto :goto_1

    .line 704675
    :cond_6
    const-string v24, "ineligible_reason"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_7

    .line 704676
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v17

    goto :goto_1

    .line 704677
    :cond_7
    const-string v24, "paid_reach"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_8

    .line 704678
    const/4 v9, 0x1

    .line 704679
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v16

    goto/16 :goto_1

    .line 704680
    :cond_8
    const-string v24, "promotion_id"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_9

    .line 704681
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    goto/16 :goto_1

    .line 704682
    :cond_9
    const-string v24, "rejection_reason"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_a

    .line 704683
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v14

    goto/16 :goto_1

    .line 704684
    :cond_a
    const-string v24, "spent"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_b

    .line 704685
    const/4 v8, 0x1

    .line 704686
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v13

    goto/16 :goto_1

    .line 704687
    :cond_b
    const-string v24, "status"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_c

    .line 704688
    const/4 v5, 0x1

    .line 704689
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    move-result-object v12

    goto/16 :goto_1

    .line 704690
    :cond_c
    const-string v24, "stop_time"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_0

    .line 704691
    const/4 v4, 0x1

    .line 704692
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v6

    goto/16 :goto_1

    .line 704693
    :cond_d
    const/16 v23, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 704694
    const/16 v23, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v23

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 704695
    if-eqz v11, :cond_e

    .line 704696
    const/4 v11, 0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v11, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 704697
    :cond_e
    const/4 v11, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v11, v1}, LX/186;->b(II)V

    .line 704698
    const/4 v11, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v11, v1}, LX/186;->b(II)V

    .line 704699
    if-eqz v10, :cond_f

    .line 704700
    const/4 v10, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v10, v1}, LX/186;->a(IZ)V

    .line 704701
    :cond_f
    const/4 v10, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v10, v1}, LX/186;->b(II)V

    .line 704702
    if-eqz v9, :cond_10

    .line 704703
    const/4 v9, 0x6

    const/4 v10, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v9, v1, v10}, LX/186;->a(III)V

    .line 704704
    :cond_10
    const/4 v9, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v15}, LX/186;->b(II)V

    .line 704705
    const/16 v9, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v14}, LX/186;->b(II)V

    .line 704706
    if-eqz v8, :cond_11

    .line 704707
    const/16 v8, 0x9

    const/4 v9, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v13, v9}, LX/186;->a(III)V

    .line 704708
    :cond_11
    if-eqz v5, :cond_12

    .line 704709
    const/16 v5, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v12}, LX/186;->a(ILjava/lang/Enum;)V

    .line 704710
    :cond_12
    if-eqz v4, :cond_13

    .line 704711
    const/16 v5, 0xb

    const-wide/16 v8, 0x0

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, LX/186;->a(IJJ)V

    .line 704712
    :cond_13
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v4

    goto/16 :goto_0

    :cond_14
    move/from16 v26, v6

    move/from16 v27, v7

    move-wide v6, v10

    move v10, v8

    move v11, v9

    move/from16 v8, v26

    move/from16 v9, v27

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/16 v4, 0xa

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 704713
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 704714
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 704715
    if-eqz v0, :cond_0

    .line 704716
    const-string v1, "ad_account"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 704717
    invoke-static {p0, v0, p2, p3}, LX/4Ke;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 704718
    :cond_0
    invoke-virtual {p0, p1, v3, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 704719
    if-eqz v0, :cond_1

    .line 704720
    const-string v0, "audience_option"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 704721
    const-class v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    invoke-virtual {p0, p1, v3, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 704722
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 704723
    if-eqz v0, :cond_2

    .line 704724
    const-string v1, "budget"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 704725
    invoke-static {p0, v0, p2}, LX/4Lf;->a(LX/15i;ILX/0nX;)V

    .line 704726
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 704727
    if-eqz v0, :cond_3

    .line 704728
    const-string v1, "feed_unit_preview"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 704729
    invoke-static {p0, v0, p2, p3}, LX/2ao;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 704730
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 704731
    if-eqz v0, :cond_4

    .line 704732
    const-string v1, "has_ad_conversion_pixel_domain"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 704733
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 704734
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 704735
    if-eqz v0, :cond_5

    .line 704736
    const-string v1, "ineligible_reason"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 704737
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 704738
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 704739
    if-eqz v0, :cond_6

    .line 704740
    const-string v1, "paid_reach"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 704741
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 704742
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 704743
    if-eqz v0, :cond_7

    .line 704744
    const-string v1, "promotion_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 704745
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 704746
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 704747
    if-eqz v0, :cond_8

    .line 704748
    const-string v1, "rejection_reason"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 704749
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 704750
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 704751
    if-eqz v0, :cond_9

    .line 704752
    const-string v1, "spent"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 704753
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 704754
    :cond_9
    invoke-virtual {p0, p1, v4, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 704755
    if-eqz v0, :cond_a

    .line 704756
    const-string v0, "status"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 704757
    const-class v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    invoke-virtual {p0, p1, v4, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 704758
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0, v6, v7}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 704759
    cmp-long v2, v0, v6

    if-eqz v2, :cond_b

    .line 704760
    const-string v2, "stop_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 704761
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 704762
    :cond_b
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 704763
    return-void
.end method
