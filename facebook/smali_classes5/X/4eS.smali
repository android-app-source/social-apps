.class public LX/4eS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0rf;


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# instance fields
.field public final a:I
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field

.field public final b:I
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field

.field public final c:LX/46X;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/46X",
            "<[B>;"
        }
    .end annotation
.end field

.field public final d:Ljava/util/concurrent/Semaphore;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field

.field private final e:LX/1FN;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1FN",
            "<[B>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0rb;LX/1F7;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 797301
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 797302
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 797303
    iget v0, p2, LX/1F7;->d:I

    if-lez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/03g;->a(Z)V

    .line 797304
    iget v0, p2, LX/1F7;->e:I

    iget v3, p2, LX/1F7;->d:I

    if-lt v0, v3, :cond_0

    move v2, v1

    :cond_0
    invoke-static {v2}, LX/03g;->a(Z)V

    .line 797305
    iget v0, p2, LX/1F7;->e:I

    iput v0, p0, LX/4eS;->b:I

    .line 797306
    iget v0, p2, LX/1F7;->d:I

    iput v0, p0, LX/4eS;->a:I

    .line 797307
    new-instance v0, LX/46X;

    invoke-direct {v0}, LX/46X;-><init>()V

    iput-object v0, p0, LX/4eS;->c:LX/46X;

    .line 797308
    new-instance v0, Ljava/util/concurrent/Semaphore;

    invoke-direct {v0, v1}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v0, p0, LX/4eS;->d:Ljava/util/concurrent/Semaphore;

    .line 797309
    new-instance v0, LX/4eR;

    invoke-direct {v0, p0}, LX/4eR;-><init>(LX/4eS;)V

    iput-object v0, p0, LX/4eS;->e:LX/1FN;

    .line 797310
    invoke-interface {p1, p0}, LX/0rb;->a(LX/0rf;)V

    .line 797311
    return-void

    :cond_1
    move v0, v2

    .line 797312
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/32G;)V
    .locals 2

    .prologue
    .line 797313
    iget-object v0, p0, LX/4eS;->d:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->tryAcquire()Z

    move-result v0

    if-nez v0, :cond_0

    .line 797314
    :goto_0
    return-void

    .line 797315
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/4eS;->c:LX/46X;

    invoke-virtual {v0}, LX/46X;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 797316
    iget-object v0, p0, LX/4eS;->d:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/4eS;->d:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    throw v0
.end method
