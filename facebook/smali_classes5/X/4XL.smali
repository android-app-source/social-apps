.class public LX/4XL;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 766997
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15i;)Lcom/facebook/flatbuffers/Flattenable;
    .locals 1

    .prologue
    .line 766998
    sget-object v0, LX/16Z;->a:LX/16Z;

    invoke-virtual {p0, v0}, LX/15i;->a(LX/16a;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)TT;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 766999
    instance-of v0, p0, Lcom/facebook/flatbuffers/Flattenable;

    if-nez v0, :cond_0

    .line 767000
    :goto_0
    return-object p0

    :cond_0
    move-object v0, p0

    .line 767001
    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    sget-object v1, LX/16Z;->a:LX/16Z;

    invoke-static {v0, v1}, LX/186;->b(Lcom/facebook/flatbuffers/Flattenable;LX/16a;)[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 767002
    new-instance v0, LX/15i;

    const/4 v4, 0x1

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 767003
    const-string v2, "GraphQLModelFlatbufferHelper"

    move-object v1, p0

    check-cast v1, Lcom/facebook/flatbuffers/Flattenable;

    invoke-virtual {v0, v2, v1}, LX/15i;->a(Ljava/lang/String;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 767004
    instance-of v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;

    if-eqz v1, :cond_1

    .line 767005
    check-cast p0, Lcom/facebook/graphql/modelutil/BaseModel;

    .line 767006
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->b:LX/49K;

    move-object v2, v1

    .line 767007
    if-eqz v2, :cond_2

    .line 767008
    iget-object v1, v2, LX/49K;->j:LX/49J;

    move-object v1, v1

    .line 767009
    if-eqz v1, :cond_2

    .line 767010
    invoke-virtual {v2}, LX/49K;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/49J;->a(Ljava/lang/String;)V

    .line 767011
    invoke-static {v0, v1}, LX/0sR;->a(Ljava/lang/Object;LX/49J;)Z

    .line 767012
    :cond_1
    :goto_1
    invoke-static {v0}, LX/4XL;->a(LX/15i;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object p0

    goto :goto_0

    :cond_2
    goto :goto_1
.end method
