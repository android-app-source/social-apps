.class public final LX/47z;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/47a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/47a",
        "<",
        "LX/03R;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 672614
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 672615
    check-cast p1, LX/03R;

    check-cast p2, LX/03R;

    .line 672616
    invoke-virtual {p1}, LX/03R;->isSet()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p2}, LX/03R;->isSet()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 672617
    invoke-virtual {p1}, LX/03R;->asBoolean()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p2}, LX/03R;->asBoolean()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    sget-object p1, LX/03R;->YES:LX/03R;

    .line 672618
    :cond_1
    :goto_0
    return-object p1

    .line 672619
    :cond_2
    sget-object p1, LX/03R;->NO:LX/03R;

    goto :goto_0

    .line 672620
    :cond_3
    invoke-virtual {p1}, LX/03R;->isSet()Z

    move-result v0

    if-nez v0, :cond_1

    move-object p1, p2

    .line 672621
    goto :goto_0
.end method
