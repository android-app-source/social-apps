.class public final LX/4pN;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:LX/2BA;

.field public final c:Ljava/util/Locale;

.field public final d:Ljava/util/TimeZone;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 811378
    const-string v0, ""

    sget-object v1, LX/2BA;->ANY:LX/2BA;

    const-string v2, ""

    const-string v3, ""

    invoke-direct {p0, v0, v1, v2, v3}, LX/4pN;-><init>(Ljava/lang/String;LX/2BA;Ljava/lang/String;Ljava/lang/String;)V

    .line 811379
    return-void
.end method

.method public constructor <init>(Lcom/fasterxml/jackson/annotation/JsonFormat;)V
    .locals 4

    .prologue
    .line 811386
    invoke-interface {p1}, Lcom/fasterxml/jackson/annotation/JsonFormat;->pattern()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Lcom/fasterxml/jackson/annotation/JsonFormat;->shape()LX/2BA;

    move-result-object v1

    invoke-interface {p1}, Lcom/fasterxml/jackson/annotation/JsonFormat;->locale()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, Lcom/fasterxml/jackson/annotation/JsonFormat;->timezone()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v0, v1, v2, v3}, LX/4pN;-><init>(Ljava/lang/String;LX/2BA;Ljava/lang/String;Ljava/lang/String;)V

    .line 811387
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;LX/2BA;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 811388
    if-eqz p3, :cond_0

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "##default"

    invoke-virtual {v1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    move-object v1, v0

    :goto_0
    if-eqz p4, :cond_1

    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "##default"

    invoke-virtual {v2, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_1
    :goto_1
    invoke-direct {p0, p1, p2, v1, v0}, LX/4pN;-><init>(Ljava/lang/String;LX/2BA;Ljava/util/Locale;Ljava/util/TimeZone;)V

    .line 811389
    return-void

    .line 811390
    :cond_2
    new-instance v1, Ljava/util/Locale;

    invoke-direct {v1, p3}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    invoke-static {p4}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    goto :goto_1
.end method

.method private constructor <init>(Ljava/lang/String;LX/2BA;Ljava/util/Locale;Ljava/util/TimeZone;)V
    .locals 0

    .prologue
    .line 811380
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 811381
    iput-object p1, p0, LX/4pN;->a:Ljava/lang/String;

    .line 811382
    iput-object p2, p0, LX/4pN;->b:LX/2BA;

    .line 811383
    iput-object p3, p0, LX/4pN;->c:Ljava/util/Locale;

    .line 811384
    iput-object p4, p0, LX/4pN;->d:Ljava/util/TimeZone;

    .line 811385
    return-void
.end method
