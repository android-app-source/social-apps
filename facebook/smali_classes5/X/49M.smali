.class public LX/49M;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/util/SparseArray",
            "<",
            "LX/14r;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mIndexAccessLock"
    .end annotation
.end field

.field private final b:Ljava/lang/Object;

.field public volatile c:Z


# direct methods
.method public constructor <init>(Landroid/util/SparseArray;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/util/SparseArray",
            "<",
            "LX/14r;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 674430
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 674431
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/49M;->b:Ljava/lang/Object;

    .line 674432
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/49M;->c:Z

    .line 674433
    iput-object p1, p0, LX/49M;->a:Landroid/util/SparseArray;

    .line 674434
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/ArrayList;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "LX/49L;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 674402
    iget-boolean v0, p0, LX/49M;->c:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "logging must be disabled before calling this getter"

    invoke-static {v0, v1}, LX/0Tp;->a(ZLjava/lang/String;)V

    .line 674403
    iget-object v5, p0, LX/49M;->b:Ljava/lang/Object;

    monitor-enter v5

    .line 674404
    :try_start_0
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    move v4, v2

    .line 674405
    :goto_1
    iget-object v0, p0, LX/49M;->a:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v4, v0, :cond_2

    .line 674406
    iget-object v0, p0, LX/49M;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, v4}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/SparseArray;

    move v3, v2

    .line 674407
    :goto_2
    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v1

    if-ge v3, v1, :cond_1

    .line 674408
    new-instance v7, LX/49L;

    iget-object v1, p0, LX/49M;->a:Landroid/util/SparseArray;

    invoke-virtual {v1, v4}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v8

    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v9

    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/14r;

    iget v1, v1, LX/14r;->a:I

    invoke-direct {v7, v8, v9, v1}, LX/49L;-><init>(III)V

    .line 674409
    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 674410
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_2

    :cond_0
    move v0, v2

    .line 674411
    goto :goto_0

    .line 674412
    :cond_1
    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 674413
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    .line 674414
    :cond_2
    iget-object v0, p0, LX/49M;->a:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 674415
    monitor-exit v5

    return-object v6

    .line 674416
    :catchall_0
    move-exception v0

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(II)V
    .locals 4

    .prologue
    .line 674417
    iget-boolean v0, p0, LX/49M;->c:Z

    if-nez v0, :cond_0

    .line 674418
    :goto_0
    return-void

    .line 674419
    :cond_0
    iget-object v2, p0, LX/49M;->b:Ljava/lang/Object;

    monitor-enter v2

    .line 674420
    :try_start_0
    iget-object v0, p0, LX/49M;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/SparseArray;

    .line 674421
    if-eqz v0, :cond_2

    .line 674422
    invoke-virtual {v0, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/14r;

    .line 674423
    if-eqz v1, :cond_1

    .line 674424
    iget v0, v1, LX/14r;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v1, LX/14r;->a:I

    .line 674425
    :goto_1
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 674426
    :cond_1
    :try_start_1
    new-instance v1, LX/14r;

    const/4 v3, 0x1

    invoke-direct {v1, v3}, LX/14r;-><init>(I)V

    invoke-virtual {v0, p2, v1}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    goto :goto_1

    .line 674427
    :cond_2
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    .line 674428
    new-instance v1, LX/14r;

    const/4 v3, 0x1

    invoke-direct {v1, v3}, LX/14r;-><init>(I)V

    invoke-virtual {v0, p2, v1}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 674429
    iget-object v1, p0, LX/49M;->a:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method
