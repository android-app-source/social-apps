.class public LX/3WP;
.super Lcom/facebook/attachments/angora/AngoraAttachmentView;
.source ""

# interfaces
.implements LX/3Vx;


# static fields
.field public static final c:LX/1Cz;


# instance fields
.field private final e:Landroid/widget/TextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 590287
    new-instance v0, LX/3WQ;

    invoke-direct {v0}, LX/3WQ;-><init>()V

    sput-object v0, LX/3WP;->c:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 590288
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/3WP;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 590289
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 590290
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/3WP;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 590291
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 590292
    const v0, 0x7f0300de

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/facebook/attachments/angora/AngoraAttachmentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 590293
    const v0, 0x7f0d0536

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/3WP;->e:Landroid/widget/TextView;

    .line 590294
    return-void
.end method


# virtual methods
.method public setSubcontextText(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 590295
    iget-object v0, p0, LX/3WP;->e:Landroid/widget/TextView;

    invoke-static {v0, p1}, LX/35n;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 590296
    return-void
.end method
