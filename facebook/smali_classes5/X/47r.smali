.class public LX/47r;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 672522
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static varargs a(Landroid/content/res/Resources;I[LX/47s;)Landroid/text/SpannableString;
    .locals 6

    .prologue
    .line 672505
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 672506
    array-length v0, p2

    new-array v2, v0, [Ljava/lang/Object;

    .line 672507
    const/4 v0, 0x0

    :goto_0
    array-length v3, p2

    if-ge v0, v3, :cond_1

    .line 672508
    aget-object v3, p2, v0

    .line 672509
    iget-object v4, v3, LX/47s;->c:Ljava/lang/Object;

    if-nez v4, :cond_0

    .line 672510
    invoke-static {p0, v3}, LX/47r;->a(Landroid/content/res/Resources;LX/47s;)Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v2, v0

    .line 672511
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 672512
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "[[placeholder_"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v4, v4

    .line 672513
    aput-object v4, v2, v0

    .line 672514
    invoke-interface {v1, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 672515
    :cond_1
    invoke-virtual {p0, p1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 672516
    new-instance v2, LX/47x;

    invoke-direct {v2, p0}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    .line 672517
    invoke-virtual {v2, v0}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    .line 672518
    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 672519
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/47s;

    .line 672520
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {p0, v1}, LX/47r;->a(Landroid/content/res/Resources;LX/47s;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, v1, LX/47s;->c:Ljava/lang/Object;

    iget v1, v1, LX/47s;->d:I

    invoke-virtual {v2, v0, v4, v5, v1}, LX/47x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;

    goto :goto_2

    .line 672521
    :cond_2
    invoke-virtual {v2}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/content/res/Resources;LX/47s;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 672502
    iget-object v0, p1, LX/47s;->a:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 672503
    iget-object v0, p1, LX/47s;->a:Ljava/lang/Object;

    .line 672504
    :goto_0
    return-object v0

    :cond_0
    iget v0, p1, LX/47s;->b:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
