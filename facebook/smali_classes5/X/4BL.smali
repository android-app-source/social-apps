.class public LX/4BL;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 677686
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static with(Lcom/facebook/fbservice/service/OperationResult;Ljava/lang/String;Ljava/io/Serializable;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 2

    .prologue
    .line 677675
    iget-boolean v0, p0, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    move v0, v0

    .line 677676
    if-eqz v0, :cond_1

    .line 677677
    invoke-virtual {p0}, Lcom/facebook/fbservice/service/OperationResult;->getResultData()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_0

    .line 677678
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object p0

    .line 677679
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/fbservice/service/OperationResult;->getResultData()Landroid/os/Bundle;

    move-result-object v0

    .line 677680
    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 677681
    :goto_0
    return-object p0

    .line 677682
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/fbservice/service/OperationResult;->getResultData()Landroid/os/Bundle;

    move-result-object v0

    .line 677683
    invoke-virtual {p0}, Lcom/facebook/fbservice/service/OperationResult;->getResultData()Landroid/os/Bundle;

    move-result-object v1

    if-nez v1, :cond_2

    .line 677684
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 677685
    :cond_2
    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    goto :goto_0
.end method
