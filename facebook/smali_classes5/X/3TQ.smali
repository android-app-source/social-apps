.class public final LX/3TQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:LX/2Pn;

.field public final synthetic val$payload:[B

.field public final synthetic val$topicName:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/2Pn;Ljava/lang/String;[B)V
    .locals 0

    .prologue
    .line 584273
    iput-object p1, p0, LX/3TQ;->this$0:LX/2Pn;

    iput-object p2, p0, LX/3TQ;->val$topicName:Ljava/lang/String;

    iput-object p3, p0, LX/3TQ;->val$payload:[B

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 584274
    invoke-virtual {p0}, LX/3TQ;->call()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public call()Ljava/lang/Void;
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 584268
    iget-object v0, p0, LX/3TQ;->this$0:LX/2Pn;

    iget-object v0, v0, LX/2Pn;->mMqttPushServiceClientManager:LX/2Hu;

    invoke-virtual {v0}, LX/2Hu;->a()LX/2gV;

    move-result-object v1

    .line 584269
    :try_start_0
    iget-object v2, p0, LX/3TQ;->val$topicName:Ljava/lang/String;

    iget-object v3, p0, LX/3TQ;->val$payload:[B

    const-wide/32 v4, 0xea60

    iget-object v0, p0, LX/3TQ;->this$0:LX/2Pn;

    iget-object v0, v0, LX/2Pn;->mMonotonicClock:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v6

    invoke-virtual/range {v1 .. v7}, LX/2gV;->a(Ljava/lang/String;[BJJ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 584270
    new-instance v0, LX/6nE;

    invoke-direct {v0}, LX/6nE;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 584271
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, LX/2gV;->f()V

    throw v0

    :cond_0
    invoke-virtual {v1}, LX/2gV;->f()V

    .line 584272
    return-object v8
.end method
