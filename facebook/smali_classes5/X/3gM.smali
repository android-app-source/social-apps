.class public final LX/3gM;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/3gL;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/1Pq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public c:I

.field public d:I

.field public e:I

.field public final synthetic f:LX/3gL;


# direct methods
.method public constructor <init>(LX/3gL;)V
    .locals 1

    .prologue
    .line 624335
    iput-object p1, p0, LX/3gM;->f:LX/3gL;

    .line 624336
    move-object v0, p1

    .line 624337
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 624338
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 624314
    const-string v0, "ExplanationTextComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 624315
    if-ne p0, p1, :cond_1

    .line 624316
    :cond_0
    :goto_0
    return v0

    .line 624317
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 624318
    goto :goto_0

    .line 624319
    :cond_3
    check-cast p1, LX/3gM;

    .line 624320
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 624321
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 624322
    if-eq v2, v3, :cond_0

    .line 624323
    iget-object v2, p0, LX/3gM;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/3gM;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/3gM;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 624324
    goto :goto_0

    .line 624325
    :cond_5
    iget-object v2, p1, LX/3gM;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 624326
    :cond_6
    iget-object v2, p0, LX/3gM;->b:LX/1Pq;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/3gM;->b:LX/1Pq;

    iget-object v3, p1, LX/3gM;->b:LX/1Pq;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 624327
    goto :goto_0

    .line 624328
    :cond_8
    iget-object v2, p1, LX/3gM;->b:LX/1Pq;

    if-nez v2, :cond_7

    .line 624329
    :cond_9
    iget v2, p0, LX/3gM;->c:I

    iget v3, p1, LX/3gM;->c:I

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 624330
    goto :goto_0

    .line 624331
    :cond_a
    iget v2, p0, LX/3gM;->d:I

    iget v3, p1, LX/3gM;->d:I

    if-eq v2, v3, :cond_b

    move v0, v1

    .line 624332
    goto :goto_0

    .line 624333
    :cond_b
    iget v2, p0, LX/3gM;->e:I

    iget v3, p1, LX/3gM;->e:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 624334
    goto :goto_0
.end method
