.class public LX/4Mq;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 689425
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 12

    .prologue
    const-wide/16 v4, 0x0

    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 689426
    const/4 v0, 0x0

    .line 689427
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v3, :cond_6

    .line 689428
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 689429
    :goto_0
    return v1

    .line 689430
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_3

    .line 689431
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 689432
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 689433
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_0

    if-eqz v9, :cond_0

    .line 689434
    const-string v10, "size"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 689435
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v2

    move v0, v7

    goto :goto_1

    .line 689436
    :cond_1
    const-string v10, "size_dimension"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 689437
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/graphql/enums/GraphQLAssetSizeDimensionType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLAssetSizeDimensionType;

    move-result-object v6

    move-object v8, v6

    move v6, v7

    goto :goto_1

    .line 689438
    :cond_2
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 689439
    :cond_3
    const/4 v9, 0x2

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 689440
    if-eqz v0, :cond_4

    move-object v0, p1

    .line 689441
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 689442
    :cond_4
    if-eqz v6, :cond_5

    .line 689443
    invoke-virtual {p1, v7, v8}, LX/186;->a(ILjava/lang/Enum;)V

    .line 689444
    :cond_5
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move v6, v1

    move-object v8, v0

    move-wide v2, v4

    move v0, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const-wide/16 v2, 0x0

    .line 689445
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 689446
    invoke-virtual {p0, p1, v4, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 689447
    cmpl-double v2, v0, v2

    if-eqz v2, :cond_0

    .line 689448
    const-string v2, "size"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 689449
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 689450
    :cond_0
    invoke-virtual {p0, p1, v5, v4}, LX/15i;->a(IIS)S

    move-result v0

    .line 689451
    if-eqz v0, :cond_1

    .line 689452
    const-string v0, "size_dimension"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 689453
    const-class v0, Lcom/facebook/graphql/enums/GraphQLAssetSizeDimensionType;

    invoke-virtual {p0, p1, v5, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLAssetSizeDimensionType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLAssetSizeDimensionType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 689454
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 689455
    return-void
.end method
