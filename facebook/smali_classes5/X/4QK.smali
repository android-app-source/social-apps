.class public LX/4QK;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 704189
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 27

    .prologue
    .line 704190
    const/16 v23, 0x0

    .line 704191
    const/16 v22, 0x0

    .line 704192
    const/16 v21, 0x0

    .line 704193
    const/16 v20, 0x0

    .line 704194
    const/16 v19, 0x0

    .line 704195
    const/16 v18, 0x0

    .line 704196
    const/16 v17, 0x0

    .line 704197
    const/16 v16, 0x0

    .line 704198
    const/4 v15, 0x0

    .line 704199
    const/4 v14, 0x0

    .line 704200
    const/4 v13, 0x0

    .line 704201
    const/4 v12, 0x0

    .line 704202
    const/4 v11, 0x0

    .line 704203
    const/4 v10, 0x0

    .line 704204
    const/4 v9, 0x0

    .line 704205
    const/4 v8, 0x0

    .line 704206
    const/4 v7, 0x0

    .line 704207
    const/4 v6, 0x0

    .line 704208
    const/4 v5, 0x0

    .line 704209
    const/4 v4, 0x0

    .line 704210
    const/4 v3, 0x0

    .line 704211
    const/4 v2, 0x0

    .line 704212
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v24

    sget-object v25, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    if-eq v0, v1, :cond_1

    .line 704213
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 704214
    const/4 v2, 0x0

    .line 704215
    :goto_0
    return v2

    .line 704216
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 704217
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v24

    sget-object v25, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    if-eq v0, v1, :cond_13

    .line 704218
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v24

    .line 704219
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 704220
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v25

    sget-object v26, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    if-eq v0, v1, :cond_1

    if-eqz v24, :cond_1

    .line 704221
    const-string v25, "ads_cta_type"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_2

    .line 704222
    const/4 v5, 0x1

    .line 704223
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v23 .. v23}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v23

    goto :goto_1

    .line 704224
    :cond_2
    const-string v25, "android_deep_link"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_3

    .line 704225
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v22

    goto :goto_1

    .line 704226
    :cond_3
    const-string v25, "android_package_name"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_4

    .line 704227
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v21

    goto :goto_1

    .line 704228
    :cond_4
    const-string v25, "application"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_5

    .line 704229
    invoke-static/range {p0 .. p1}, LX/2uk;->a(LX/15w;LX/186;)I

    move-result v20

    goto :goto_1

    .line 704230
    :cond_5
    const-string v25, "autofill_enabled_on_fallback"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_6

    .line 704231
    const/4 v4, 0x1

    .line 704232
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v19

    goto :goto_1

    .line 704233
    :cond_6
    const-string v25, "cta_admin_info"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_7

    .line 704234
    invoke-static/range {p0 .. p1}, LX/4QH;->a(LX/15w;LX/186;)I

    move-result v18

    goto/16 :goto_1

    .line 704235
    :cond_7
    const-string v25, "cta_type"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_8

    .line 704236
    const/4 v3, 0x1

    .line 704237
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    move-result-object v17

    goto/16 :goto_1

    .line 704238
    :cond_8
    const-string v25, "desktop_uri"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_9

    .line 704239
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    goto/16 :goto_1

    .line 704240
    :cond_9
    const-string v25, "email_address"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_a

    .line 704241
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    goto/16 :goto_1

    .line 704242
    :cond_a
    const-string v25, "fallback_uri"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_b

    .line 704243
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    goto/16 :goto_1

    .line 704244
    :cond_b
    const-string v25, "form_fields"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_c

    .line 704245
    invoke-static/range {p0 .. p1}, LX/4QJ;->a(LX/15w;LX/186;)I

    move-result v13

    goto/16 :goto_1

    .line 704246
    :cond_c
    const-string v25, "id"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_d

    .line 704247
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    goto/16 :goto_1

    .line 704248
    :cond_d
    const-string v25, "is_first_party"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_e

    .line 704249
    const/4 v2, 0x1

    .line 704250
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v11

    goto/16 :goto_1

    .line 704251
    :cond_e
    const-string v25, "label"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_f

    .line 704252
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto/16 :goto_1

    .line 704253
    :cond_f
    const-string v25, "phone_number"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_10

    .line 704254
    invoke-static/range {p0 .. p1}, LX/4RB;->a(LX/15w;LX/186;)I

    move-result v9

    goto/16 :goto_1

    .line 704255
    :cond_10
    const-string v25, "status"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_11

    .line 704256
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto/16 :goto_1

    .line 704257
    :cond_11
    const-string v25, "url"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_12

    .line 704258
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto/16 :goto_1

    .line 704259
    :cond_12
    const-string v25, "icon_info"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_0

    .line 704260
    invoke-static/range {p0 .. p1}, LX/4Lw;->a(LX/15w;LX/186;)I

    move-result v6

    goto/16 :goto_1

    .line 704261
    :cond_13
    const/16 v24, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 704262
    if-eqz v5, :cond_14

    .line 704263
    const/4 v5, 0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v5, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 704264
    :cond_14
    const/4 v5, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 704265
    const/4 v5, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 704266
    const/4 v5, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 704267
    if-eqz v4, :cond_15

    .line 704268
    const/4 v4, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v4, v1}, LX/186;->a(IZ)V

    .line 704269
    :cond_15
    const/4 v4, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 704270
    if-eqz v3, :cond_16

    .line 704271
    const/4 v3, 0x7

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v3, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 704272
    :cond_16
    const/16 v3, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 704273
    const/16 v3, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v15}, LX/186;->b(II)V

    .line 704274
    const/16 v3, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v14}, LX/186;->b(II)V

    .line 704275
    const/16 v3, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v13}, LX/186;->b(II)V

    .line 704276
    const/16 v3, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v12}, LX/186;->b(II)V

    .line 704277
    if-eqz v2, :cond_17

    .line 704278
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->a(IZ)V

    .line 704279
    :cond_17
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 704280
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 704281
    const/16 v2, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 704282
    const/16 v2, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 704283
    const/16 v2, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 704284
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/4 v3, 0x7

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 704285
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 704286
    invoke-virtual {p0, p1, v1, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 704287
    if-eqz v0, :cond_0

    .line 704288
    const-string v0, "ads_cta_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 704289
    const-class v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 704290
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 704291
    if-eqz v0, :cond_1

    .line 704292
    const-string v1, "android_deep_link"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 704293
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 704294
    :cond_1
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 704295
    if-eqz v0, :cond_2

    .line 704296
    const-string v1, "android_package_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 704297
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 704298
    :cond_2
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 704299
    if-eqz v0, :cond_3

    .line 704300
    const-string v1, "application"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 704301
    invoke-static {p0, v0, p2, p3}, LX/2uk;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 704302
    :cond_3
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 704303
    if-eqz v0, :cond_4

    .line 704304
    const-string v1, "autofill_enabled_on_fallback"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 704305
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 704306
    :cond_4
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 704307
    if-eqz v0, :cond_5

    .line 704308
    const-string v1, "cta_admin_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 704309
    invoke-static {p0, v0, p2}, LX/4QH;->a(LX/15i;ILX/0nX;)V

    .line 704310
    :cond_5
    invoke-virtual {p0, p1, v3, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 704311
    if-eqz v0, :cond_6

    .line 704312
    const-string v0, "cta_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 704313
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    invoke-virtual {p0, p1, v3, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 704314
    :cond_6
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 704315
    if-eqz v0, :cond_7

    .line 704316
    const-string v1, "desktop_uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 704317
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 704318
    :cond_7
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 704319
    if-eqz v0, :cond_8

    .line 704320
    const-string v1, "email_address"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 704321
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 704322
    :cond_8
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 704323
    if-eqz v0, :cond_9

    .line 704324
    const-string v1, "fallback_uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 704325
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 704326
    :cond_9
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 704327
    if-eqz v0, :cond_a

    .line 704328
    const-string v1, "form_fields"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 704329
    invoke-static {p0, v0, p2, p3}, LX/4QJ;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 704330
    :cond_a
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 704331
    if-eqz v0, :cond_b

    .line 704332
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 704333
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 704334
    :cond_b
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 704335
    if-eqz v0, :cond_c

    .line 704336
    const-string v1, "is_first_party"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 704337
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 704338
    :cond_c
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 704339
    if-eqz v0, :cond_d

    .line 704340
    const-string v1, "label"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 704341
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 704342
    :cond_d
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 704343
    if-eqz v0, :cond_e

    .line 704344
    const-string v1, "phone_number"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 704345
    invoke-static {p0, v0, p2}, LX/4RB;->a(LX/15i;ILX/0nX;)V

    .line 704346
    :cond_e
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 704347
    if-eqz v0, :cond_f

    .line 704348
    const-string v1, "status"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 704349
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 704350
    :cond_f
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 704351
    if-eqz v0, :cond_10

    .line 704352
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 704353
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 704354
    :cond_10
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 704355
    if-eqz v0, :cond_11

    .line 704356
    const-string v1, "icon_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 704357
    invoke-static {p0, v0, p2, p3}, LX/4Lw;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 704358
    :cond_11
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 704359
    return-void
.end method
