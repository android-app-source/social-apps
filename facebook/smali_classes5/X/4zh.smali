.class public final LX/4zh;
.super LX/0qE;
.source ""

# interfaces
.implements LX/0qF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LX/0qE",
        "<TK;TV;>;",
        "LX/0qF",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field public volatile d:J

.field public e:LX/0qF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0qF",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public f:LX/0qF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0qF",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;ILX/0qF;)V
    .locals 2
    .param p4    # LX/0qF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/ReferenceQueue",
            "<TK;>;TK;I",
            "LX/0qF",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 823025
    invoke-direct {p0, p1, p2, p3, p4}, LX/0qE;-><init>(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;ILX/0qF;)V

    .line 823026
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, LX/4zh;->d:J

    .line 823027
    sget-object v0, LX/4zX;->INSTANCE:LX/4zX;

    move-object v0, v0

    .line 823028
    iput-object v0, p0, LX/4zh;->e:LX/0qF;

    .line 823029
    sget-object v0, LX/4zX;->INSTANCE:LX/4zX;

    move-object v0, v0

    .line 823030
    iput-object v0, p0, LX/4zh;->f:LX/0qF;

    .line 823031
    return-void
.end method


# virtual methods
.method public final getExpirationTime()J
    .locals 2

    .prologue
    .line 823032
    iget-wide v0, p0, LX/4zh;->d:J

    return-wide v0
.end method

.method public final getNextExpirable()LX/0qF;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0qF",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 823033
    iget-object v0, p0, LX/4zh;->e:LX/0qF;

    return-object v0
.end method

.method public final getPreviousExpirable()LX/0qF;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0qF",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 823034
    iget-object v0, p0, LX/4zh;->f:LX/0qF;

    return-object v0
.end method

.method public final setExpirationTime(J)V
    .locals 1

    .prologue
    .line 823035
    iput-wide p1, p0, LX/4zh;->d:J

    .line 823036
    return-void
.end method

.method public final setNextExpirable(LX/0qF;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0qF",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 823037
    iput-object p1, p0, LX/4zh;->e:LX/0qF;

    .line 823038
    return-void
.end method

.method public final setPreviousExpirable(LX/0qF;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0qF",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 823039
    iput-object p1, p0, LX/4zh;->f:LX/0qF;

    .line 823040
    return-void
.end method
