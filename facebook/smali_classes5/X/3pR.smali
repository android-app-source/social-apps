.class public LX/3pR;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:LX/3pO;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 641598
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 641599
    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 641600
    new-instance v0, LX/3pQ;

    invoke-direct {v0}, LX/3pQ;-><init>()V

    sput-object v0, LX/3pR;->a:LX/3pO;

    .line 641601
    :goto_0
    return-void

    .line 641602
    :cond_0
    new-instance v0, LX/3pP;

    invoke-direct {v0}, LX/3pP;-><init>()V

    sput-object v0, LX/3pR;->a:LX/3pO;

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 641596
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 641597
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/content/ComponentName;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 641590
    invoke-static {p0, p1}, LX/3pR;->b(Landroid/content/Context;Landroid/content/ComponentName;)Ljava/lang/String;

    move-result-object v0

    .line 641591
    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 641592
    :goto_0
    return-object v0

    .line 641593
    :cond_0
    new-instance v1, Landroid/content/ComponentName;

    invoke-virtual {p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 641594
    invoke-static {p0, v1}, LX/3pR;->b(Landroid/content/Context;Landroid/content/ComponentName;)Ljava/lang/String;

    move-result-object v0

    .line 641595
    if-nez v0, :cond_1

    invoke-static {v1}, LX/3qf;->a(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    :cond_1
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 641585
    invoke-static {p0}, LX/3pR;->b(Landroid/app/Activity;)Landroid/content/Intent;

    move-result-object v0

    .line 641586
    if-nez v0, :cond_0

    .line 641587
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Activity "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " does not have a parent activity name specified. (Did you forget to add the android.support.PARENT_ACTIVITY <meta-data> "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " element in your manifest?)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 641588
    :cond_0
    invoke-static {p0, v0}, LX/3pR;->b(Landroid/app/Activity;Landroid/content/Intent;)V

    .line 641589
    return-void
.end method

.method public static a(Landroid/app/Activity;Landroid/content/Intent;)Z
    .locals 1

    .prologue
    .line 641603
    sget-object v0, LX/3pR;->a:LX/3pO;

    invoke-interface {v0, p0, p1}, LX/3pO;->a(Landroid/app/Activity;Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method

.method public static b(Landroid/app/Activity;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 641579
    sget-object v0, LX/3pR;->a:LX/3pO;

    invoke-interface {v0, p0}, LX/3pO;->a(Landroid/app/Activity;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static b(Landroid/content/Context;Landroid/content/ComponentName;)Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 641575
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 641576
    const/16 v1, 0x80

    invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    move-result-object v0

    .line 641577
    sget-object v1, LX/3pR;->a:LX/3pO;

    invoke-interface {v1, p0, v0}, LX/3pO;->a(Landroid/content/Context;Landroid/content/pm/ActivityInfo;)Ljava/lang/String;

    move-result-object v0

    .line 641578
    return-object v0
.end method

.method public static b(Landroid/app/Activity;Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 641580
    sget-object v0, LX/3pR;->a:LX/3pO;

    invoke-interface {v0, p0, p1}, LX/3pO;->b(Landroid/app/Activity;Landroid/content/Intent;)V

    .line 641581
    return-void
.end method

.method public static c(Landroid/app/Activity;)Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 641582
    :try_start_0
    invoke-virtual {p0}, Landroid/app/Activity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v0

    invoke-static {p0, v0}, LX/3pR;->b(Landroid/content/Context;Landroid/content/ComponentName;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 641583
    :catch_0
    move-exception v0

    .line 641584
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
