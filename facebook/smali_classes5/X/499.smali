.class public final enum LX/499;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/499;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/499;

.field public static final enum NUX_DIALOG_NOT_NOW_CLICKED:LX/499;

.field public static final enum NUX_DIALOG_PRE_SHOW:LX/499;

.field public static final enum NUX_DIALOG_SHOWN:LX/499;

.field public static final enum NUX_DIALOG_TURN_ON_CLICKED:LX/499;

.field public static final enum SETTINGS_DSM_AUTO_DISABLED:LX/499;

.field public static final enum SETTINGS_DSM_AUTO_ENABLED:LX/499;

.field public static final enum SETTINGS_DSM_DISABLED:LX/499;

.field public static final enum SETTINGS_DSM_ENABLED:LX/499;

.field public static final enum SETTINGS_DSM_OFF:LX/499;

.field public static final enum SETTINGS_DSM_ON:LX/499;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 674166
    new-instance v0, LX/499;

    const-string v1, "SETTINGS_DSM_ENABLED"

    invoke-direct {v0, v1, v3}, LX/499;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/499;->SETTINGS_DSM_ENABLED:LX/499;

    .line 674167
    new-instance v0, LX/499;

    const-string v1, "SETTINGS_DSM_DISABLED"

    invoke-direct {v0, v1, v4}, LX/499;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/499;->SETTINGS_DSM_DISABLED:LX/499;

    .line 674168
    new-instance v0, LX/499;

    const-string v1, "SETTINGS_DSM_AUTO_ENABLED"

    invoke-direct {v0, v1, v5}, LX/499;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/499;->SETTINGS_DSM_AUTO_ENABLED:LX/499;

    .line 674169
    new-instance v0, LX/499;

    const-string v1, "SETTINGS_DSM_AUTO_DISABLED"

    invoke-direct {v0, v1, v6}, LX/499;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/499;->SETTINGS_DSM_AUTO_DISABLED:LX/499;

    .line 674170
    new-instance v0, LX/499;

    const-string v1, "NUX_DIALOG_SHOWN"

    invoke-direct {v0, v1, v7}, LX/499;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/499;->NUX_DIALOG_SHOWN:LX/499;

    .line 674171
    new-instance v0, LX/499;

    const-string v1, "NUX_DIALOG_PRE_SHOW"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/499;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/499;->NUX_DIALOG_PRE_SHOW:LX/499;

    .line 674172
    new-instance v0, LX/499;

    const-string v1, "NUX_DIALOG_NOT_NOW_CLICKED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/499;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/499;->NUX_DIALOG_NOT_NOW_CLICKED:LX/499;

    .line 674173
    new-instance v0, LX/499;

    const-string v1, "NUX_DIALOG_TURN_ON_CLICKED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/499;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/499;->NUX_DIALOG_TURN_ON_CLICKED:LX/499;

    .line 674174
    new-instance v0, LX/499;

    const-string v1, "SETTINGS_DSM_ON"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/499;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/499;->SETTINGS_DSM_ON:LX/499;

    .line 674175
    new-instance v0, LX/499;

    const-string v1, "SETTINGS_DSM_OFF"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/499;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/499;->SETTINGS_DSM_OFF:LX/499;

    .line 674176
    const/16 v0, 0xa

    new-array v0, v0, [LX/499;

    sget-object v1, LX/499;->SETTINGS_DSM_ENABLED:LX/499;

    aput-object v1, v0, v3

    sget-object v1, LX/499;->SETTINGS_DSM_DISABLED:LX/499;

    aput-object v1, v0, v4

    sget-object v1, LX/499;->SETTINGS_DSM_AUTO_ENABLED:LX/499;

    aput-object v1, v0, v5

    sget-object v1, LX/499;->SETTINGS_DSM_AUTO_DISABLED:LX/499;

    aput-object v1, v0, v6

    sget-object v1, LX/499;->NUX_DIALOG_SHOWN:LX/499;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/499;->NUX_DIALOG_PRE_SHOW:LX/499;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/499;->NUX_DIALOG_NOT_NOW_CLICKED:LX/499;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/499;->NUX_DIALOG_TURN_ON_CLICKED:LX/499;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/499;->SETTINGS_DSM_ON:LX/499;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/499;->SETTINGS_DSM_OFF:LX/499;

    aput-object v2, v0, v1

    sput-object v0, LX/499;->$VALUES:[LX/499;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 674177
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/499;
    .locals 1

    .prologue
    .line 674178
    const-class v0, LX/499;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/499;

    return-object v0
.end method

.method public static values()[LX/499;
    .locals 1

    .prologue
    .line 674179
    sget-object v0, LX/499;->$VALUES:[LX/499;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/499;

    return-object v0
.end method
