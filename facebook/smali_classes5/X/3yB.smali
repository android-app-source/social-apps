.class public LX/3yB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0cb;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 660802
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 660803
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 660797
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0cb;",
            ">;"
        }
    .end annotation

    .prologue
    .line 660804
    invoke-static {p0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0Yu;)V
    .locals 0

    .prologue
    .line 660800
    return-void
.end method

.method public final b(Ljava/lang/String;)Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;
    .locals 1

    .prologue
    .line 660801
    new-instance v0, LX/2WQ;

    invoke-direct {v0}, LX/2WQ;-><init>()V

    invoke-virtual {v0}, LX/2WQ;->a()Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryInterfaces$Configuration;",
            ">;"
        }
    .end annotation

    .prologue
    .line 660799
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    return-object v0
.end method

.method public final c()LX/0d6;
    .locals 1

    .prologue
    .line 660798
    new-instance v0, LX/3yA;

    invoke-direct {v0, p0}, LX/3yA;-><init>(LX/3yB;)V

    return-object v0
.end method

.method public final c(Ljava/lang/String;)Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;
    .locals 1

    .prologue
    .line 660796
    new-instance v0, LX/2WQ;

    invoke-direct {v0}, LX/2WQ;-><init>()V

    invoke-virtual {v0}, LX/2WQ;->a()Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;

    move-result-object v0

    return-object v0
.end method
