.class public final LX/3uK;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field public g:Landroid/view/ViewGroup;

.field public h:Landroid/view/View;

.field public i:Landroid/view/View;

.field public j:LX/3v0;

.field public k:LX/3uz;

.field public l:Landroid/content/Context;

.field public m:Z

.field public n:Z

.field public o:Z

.field public p:Z

.field public q:Z

.field public r:Z

.field public s:Landroid/os/Bundle;


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 648273
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 648274
    iput p1, p0, LX/3uK;->a:I

    .line 648275
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/3uK;->q:Z

    .line 648276
    return-void
.end method


# virtual methods
.method public final a(LX/3uE;)LX/3ux;
    .locals 3

    .prologue
    .line 648257
    iget-object v0, p0, LX/3uK;->j:LX/3v0;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 648258
    :goto_0
    return-object v0

    .line 648259
    :cond_0
    iget-object v0, p0, LX/3uK;->k:LX/3uz;

    if-nez v0, :cond_1

    .line 648260
    new-instance v0, LX/3uz;

    iget-object v1, p0, LX/3uK;->l:Landroid/content/Context;

    const v2, 0x7f03000d

    invoke-direct {v0, v1, v2}, LX/3uz;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, LX/3uK;->k:LX/3uz;

    .line 648261
    iget-object v0, p0, LX/3uK;->k:LX/3uz;

    .line 648262
    iput-object p1, v0, LX/3uz;->i:LX/3uE;

    .line 648263
    iget-object v0, p0, LX/3uK;->j:LX/3v0;

    iget-object v1, p0, LX/3uK;->k:LX/3uz;

    invoke-virtual {v0, v1}, LX/3v0;->a(LX/3us;)V

    .line 648264
    :cond_1
    iget-object v0, p0, LX/3uK;->k:LX/3uz;

    iget-object v1, p0, LX/3uK;->g:Landroid/view/ViewGroup;

    .line 648265
    iget-object v2, v0, LX/3uz;->d:Landroid/support/v7/internal/view/menu/ExpandedMenuView;

    if-nez v2, :cond_3

    .line 648266
    iget-object v2, v0, LX/3uz;->b:Landroid/view/LayoutInflater;

    const p0, 0x7f03000a

    const/4 p1, 0x0

    invoke-virtual {v2, p0, v1, p1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/support/v7/internal/view/menu/ExpandedMenuView;

    iput-object v2, v0, LX/3uz;->d:Landroid/support/v7/internal/view/menu/ExpandedMenuView;

    .line 648267
    iget-object v2, v0, LX/3uz;->g:LX/3uy;

    if-nez v2, :cond_2

    .line 648268
    new-instance v2, LX/3uy;

    invoke-direct {v2, v0}, LX/3uy;-><init>(LX/3uz;)V

    iput-object v2, v0, LX/3uz;->g:LX/3uy;

    .line 648269
    :cond_2
    iget-object v2, v0, LX/3uz;->d:Landroid/support/v7/internal/view/menu/ExpandedMenuView;

    iget-object p0, v0, LX/3uz;->g:LX/3uy;

    invoke-virtual {v2, p0}, Landroid/support/v7/internal/view/menu/ExpandedMenuView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 648270
    iget-object v2, v0, LX/3uz;->d:Landroid/support/v7/internal/view/menu/ExpandedMenuView;

    invoke-virtual {v2, v0}, Landroid/support/v7/internal/view/menu/ExpandedMenuView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 648271
    :cond_3
    iget-object v2, v0, LX/3uz;->d:Landroid/support/v7/internal/view/menu/ExpandedMenuView;

    move-object v0, v2

    .line 648272
    goto :goto_0
.end method

.method public final a(LX/3v0;)V
    .locals 2

    .prologue
    .line 648250
    iget-object v0, p0, LX/3uK;->j:LX/3v0;

    if-ne p1, v0, :cond_1

    .line 648251
    :cond_0
    :goto_0
    return-void

    .line 648252
    :cond_1
    iget-object v0, p0, LX/3uK;->j:LX/3v0;

    if-eqz v0, :cond_2

    .line 648253
    iget-object v0, p0, LX/3uK;->j:LX/3v0;

    iget-object v1, p0, LX/3uK;->k:LX/3uz;

    invoke-virtual {v0, v1}, LX/3v0;->b(LX/3us;)V

    .line 648254
    :cond_2
    iput-object p1, p0, LX/3uK;->j:LX/3v0;

    .line 648255
    if-eqz p1, :cond_0

    .line 648256
    iget-object v0, p0, LX/3uK;->k:LX/3uz;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3uK;->k:LX/3uz;

    invoke-virtual {p1, v0}, LX/3v0;->a(LX/3us;)V

    goto :goto_0
.end method
