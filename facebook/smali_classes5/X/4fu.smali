.class public LX/4fu;
.super LX/0R5;
.source ""

# interfaces
.implements LX/0QD;


# instance fields
.field private final a:LX/0QA;

.field private final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "LX/0Q4;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0R2;

.field private d:LX/0QA;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/0RI",
            "<*>;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private f:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/0RI",
            "<*>;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private g:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/0RI",
            "<*>;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private h:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0QA;LX/0R2;Ljava/lang/Class;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QA;",
            "LX/0R2;",
            "Ljava/lang/Class",
            "<+",
            "LX/0Q4;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 798852
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    invoke-direct/range {v0 .. v7}, LX/4fu;-><init>(LX/0QA;LX/0R2;Ljava/lang/Class;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;)V

    .line 798853
    return-void
.end method

.method private constructor <init>(LX/0QA;LX/0R2;Ljava/lang/Class;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;)V
    .locals 0
    .param p4    # Ljava/util/Set;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/util/Set;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/util/Set;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/util/Set;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QA;",
            "LX/0R2;",
            "Ljava/lang/Class",
            "<+",
            "LX/0Q4;",
            ">;",
            "Ljava/util/Set",
            "<",
            "LX/0RI",
            "<*>;>;",
            "Ljava/util/Set",
            "<",
            "LX/0RI",
            "<*>;>;",
            "Ljava/util/Set",
            "<",
            "LX/0RI",
            "<*>;>;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 798854
    invoke-direct {p0, p1}, LX/0R5;-><init>(LX/0QA;)V

    .line 798855
    iput-object p1, p0, LX/4fu;->a:LX/0QA;

    .line 798856
    iput-object p2, p0, LX/4fu;->c:LX/0R2;

    .line 798857
    iput-object p3, p0, LX/4fu;->b:Ljava/lang/Class;

    .line 798858
    iput-object p4, p0, LX/4fu;->e:Ljava/util/Set;

    .line 798859
    iput-object p5, p0, LX/4fu;->f:Ljava/util/Set;

    .line 798860
    iput-object p6, p0, LX/4fu;->g:Ljava/util/Set;

    .line 798861
    iput-object p7, p0, LX/4fu;->h:Ljava/util/Set;

    .line 798862
    return-void
.end method

.method public synthetic constructor <init>(LX/0QA;LX/0R2;Ljava/lang/Class;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;B)V
    .locals 0

    .prologue
    .line 798895
    invoke-direct/range {p0 .. p7}, LX/4fu;-><init>(LX/0QA;LX/0R2;Ljava/lang/Class;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;)V

    return-void
.end method

.method public static declared-synchronized a(LX/4fu;)Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "LX/0RI",
            "<*>;>;"
        }
    .end annotation

    .prologue
    .line 798836
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/4fu;->e:Ljava/util/Set;

    if-eqz v0, :cond_0

    .line 798837
    iget-object v0, p0, LX/4fu;->e:Ljava/util/Set;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 798838
    :goto_0
    monitor-exit p0

    return-object v0

    .line 798839
    :cond_0
    :try_start_1
    invoke-static {p0}, LX/4fu;->b(LX/4fu;)V

    .line 798840
    iget-object v0, p0, LX/4fu;->e:Ljava/util/Set;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 798841
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized b(LX/4fu;)V
    .locals 10

    .prologue
    .line 798863
    monitor-enter p0

    :try_start_0
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v2

    .line 798864
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v3

    .line 798865
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v4

    .line 798866
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v5

    .line 798867
    iget-object v0, p0, LX/4fu;->b:Ljava/lang/Class;

    const-class v1, LX/4fw;

    if-ne v0, v1, :cond_0

    .line 798868
    invoke-virtual {v2}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    iput-object v0, p0, LX/4fu;->e:Ljava/util/Set;

    .line 798869
    invoke-virtual {v3}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    iput-object v0, p0, LX/4fu;->f:Ljava/util/Set;

    .line 798870
    invoke-virtual {v4}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    iput-object v0, p0, LX/4fu;->g:Ljava/util/Set;

    .line 798871
    invoke-virtual {v5}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    iput-object v0, p0, LX/4fu;->h:Ljava/util/Set;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 798872
    :goto_0
    monitor-exit p0

    return-void

    .line 798873
    :cond_0
    :try_start_1
    invoke-virtual {p0}, LX/0R5;->getBinders()Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, LX/4fu;->b:Ljava/lang/Class;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0RH;

    .line 798874
    invoke-virtual {v0}, LX/0RH;->e()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0RI;

    .line 798875
    invoke-virtual {v2, v0}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 798876
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 798877
    :cond_1
    :try_start_2
    invoke-direct {p0}, LX/4fu;->c()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0RH;

    .line 798878
    invoke-virtual {v0}, LX/0RH;->a()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0RN;

    .line 798879
    iget-object v8, v1, LX/0RN;->b:LX/0RI;

    move-object v1, v8

    .line 798880
    invoke-virtual {v2, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    goto :goto_3

    .line 798881
    :cond_2
    invoke-virtual {v0}, LX/0RH;->b()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_4
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/4fR;

    .line 798882
    iget-object v8, v1, LX/4fR;->b:LX/0RI;

    move-object v1, v8

    .line 798883
    invoke-virtual {v3, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    goto :goto_4

    .line 798884
    :cond_3
    invoke-virtual {v0}, LX/0RH;->c()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_5
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0RI;

    .line 798885
    invoke-virtual {v4, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 798886
    iget-object v8, v1, LX/0RI;->b:LX/0RL;

    move-object v8, v8

    .line 798887
    iget-object v9, v8, LX/0RL;->a:Ljava/lang/Class;

    move-object v8, v9

    .line 798888
    invoke-virtual {v1}, LX/0RI;->b()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v8, v1}, LX/0QA;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/0RI;

    move-result-object v1

    .line 798889
    invoke-virtual {v2, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    goto :goto_5

    .line 798890
    :cond_4
    invoke-virtual {v0}, LX/0RH;->h()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-virtual {v5, v0}, LX/0cA;->b(Ljava/lang/Iterable;)LX/0cA;

    goto :goto_2

    .line 798891
    :cond_5
    invoke-virtual {v2}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    iput-object v0, p0, LX/4fu;->e:Ljava/util/Set;

    .line 798892
    invoke-virtual {v3}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    iput-object v0, p0, LX/4fu;->f:Ljava/util/Set;

    .line 798893
    invoke-virtual {v4}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    iput-object v0, p0, LX/4fu;->g:Ljava/util/Set;

    .line 798894
    invoke-virtual {v5}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    iput-object v0, p0, LX/4fu;->h:Ljava/util/Set;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method

.method private c()Ljava/util/Set;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/inject/Binder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 798896
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v1

    .line 798897
    invoke-static {}, LX/0R9;->b()Ljava/util/LinkedList;

    move-result-object v2

    .line 798898
    invoke-virtual {p0}, LX/0R5;->getBinders()Ljava/util/Map;

    move-result-object v0

    iget-object v3, p0, LX/4fu;->b:Ljava/lang/Class;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0RH;

    .line 798899
    if-nez v0, :cond_0

    move-object v0, v1

    .line 798900
    :goto_0
    return-object v0

    .line 798901
    :cond_0
    iget-object v3, p0, LX/4fu;->b:Ljava/lang/Class;

    invoke-interface {v2, v3}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 798902
    iget-object v3, v0, LX/0RH;->h:Ljava/util/Set;

    if-nez v3, :cond_4

    .line 798903
    sget-object v3, LX/0Re;->a:LX/0Re;

    move-object v3, v3

    .line 798904
    :goto_1
    move-object v0, v3

    .line 798905
    invoke-interface {v2, v0}, Ljava/util/Queue;->addAll(Ljava/util/Collection;)Z

    .line 798906
    :cond_1
    :goto_2
    invoke-interface {v2}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    if-eqz v0, :cond_3

    .line 798907
    invoke-virtual {p0}, LX/0R5;->getBinders()Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0RH;

    .line 798908
    if-nez v0, :cond_2

    .line 798909
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Module was not installed: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/4fu;->b:Ljava/lang/Class;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 798910
    :cond_2
    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 798911
    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 798912
    :cond_3
    invoke-virtual {p0}, LX/0R5;->getBinders()Ljava/util/Map;

    move-result-object v0

    const-class v2, LX/0RG;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-object v0, v1

    .line 798913
    goto :goto_0

    :cond_4
    iget-object v3, v0, LX/0RH;->h:Ljava/util/Set;

    goto :goto_1
.end method

.method private d(LX/0RI;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0RI",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 798842
    iget-object v0, p0, LX/4fu;->c:LX/0R2;

    .line 798843
    iget-boolean v1, v0, LX/0R2;->a:Z

    move v0, v1

    .line 798844
    if-eqz v0, :cond_0

    .line 798845
    invoke-static {p0}, LX/4fu;->a(LX/4fu;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    move v0, v0

    .line 798846
    if-nez v0, :cond_0

    .line 798847
    iget-object v0, p0, LX/4fu;->c:LX/0R2;

    invoke-virtual {v0}, LX/0R2;->d()LX/4ff;

    .line 798848
    new-instance v0, LX/4fr;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Module "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/4fu;->b:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " used undeclared binding "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    .line 798849
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Class;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    .prologue
    .line 798850
    iget-object v0, p0, LX/4fu;->a:LX/0QA;

    invoke-virtual {v0, p1, p2}, LX/0QA;->a(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 798851
    return-void
.end method

.method public final declared-synchronized getApplicationInjector()LX/0QA;
    .locals 8

    .prologue
    .line 798819
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/4fu;->d:LX/0QA;

    if-eqz v0, :cond_0

    .line 798820
    iget-object v0, p0, LX/4fu;->d:LX/0QA;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 798821
    :goto_0
    monitor-exit p0

    return-object v0

    .line 798822
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/4fu;->a:LX/0QA;

    invoke-virtual {v0}, LX/0QA;->getApplicationInjector()LX/0QA;

    move-result-object v1

    .line 798823
    iget-object v0, p0, LX/4fu;->a:LX/0QA;

    if-ne v1, v0, :cond_1

    .line 798824
    iput-object p0, p0, LX/4fu;->d:LX/0QA;

    .line 798825
    :goto_1
    iget-object v0, p0, LX/4fu;->d:LX/0QA;

    goto :goto_0

    .line 798826
    :cond_1
    new-instance v0, LX/4fu;

    iget-object v2, p0, LX/4fu;->c:LX/0R2;

    iget-object v3, p0, LX/4fu;->b:Ljava/lang/Class;

    iget-object v4, p0, LX/4fu;->e:Ljava/util/Set;

    iget-object v5, p0, LX/4fu;->f:Ljava/util/Set;

    iget-object v6, p0, LX/4fu;->g:Ljava/util/Set;

    iget-object v7, p0, LX/4fu;->h:Ljava/util/Set;

    invoke-direct/range {v0 .. v7}, LX/4fu;-><init>(LX/0QA;LX/0R2;Ljava/lang/Class;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;)V

    iput-object v0, p0, LX/4fu;->d:LX/0QA;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 798827
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final getInstance(LX/0RI;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0RI",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 798814
    invoke-direct {p0, p1}, LX/4fu;->d(LX/0RI;)V

    .line 798815
    iget-object v0, p0, LX/4fu;->a:LX/0QA;

    invoke-virtual {v0, p1}, LX/0QA;->getInstance(LX/0RI;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getLazy(LX/0RI;)LX/0Ot;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0RI",
            "<TT;>;)",
            "LX/0Ot",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 798816
    invoke-direct {p0, p1}, LX/4fu;->d(LX/0RI;)V

    .line 798817
    iget-object v0, p0, LX/4fu;->a:LX/0QA;

    invoke-virtual {v0, p1}, LX/0QA;->getLazy(LX/0RI;)LX/0Ot;

    move-result-object v0

    return-object v0
.end method

.method public final getModuleInjector(Ljava/lang/Class;)LX/0QA;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "LX/0Q4;",
            ">;)",
            "LX/0QA;"
        }
    .end annotation

    .prologue
    .line 798818
    iget-object v0, p0, LX/4fu;->a:LX/0QA;

    invoke-virtual {v0, p1}, LX/0QA;->getModuleInjector(Ljava/lang/Class;)LX/0QA;

    move-result-object v0

    return-object v0
.end method

.method public final getProvider(LX/0RI;)LX/0Or;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0RI",
            "<TT;>;)",
            "LX/0Or",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 798828
    invoke-direct {p0, p1}, LX/4fu;->d(LX/0RI;)V

    .line 798829
    iget-object v0, p0, LX/4fu;->a:LX/0QA;

    invoke-virtual {v0, p1}, LX/0QA;->getProvider(LX/0RI;)LX/0Or;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized getScopeAwareInjector()LX/0R6;
    .locals 9

    .prologue
    .line 798830
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/4fu;->a:LX/0QA;

    invoke-virtual {v0}, LX/0QA;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    .line 798831
    new-instance v0, LX/4fv;

    iget-object v2, p0, LX/4fu;->c:LX/0R2;

    iget-object v3, p0, LX/4fu;->b:Ljava/lang/Class;

    iget-object v4, p0, LX/4fu;->e:Ljava/util/Set;

    iget-object v5, p0, LX/4fu;->f:Ljava/util/Set;

    iget-object v6, p0, LX/4fu;->g:Ljava/util/Set;

    iget-object v7, p0, LX/4fu;->h:Ljava/util/Set;

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, LX/4fv;-><init>(LX/0R6;LX/0R2;Ljava/lang/Class;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 798832
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized getScopeUnawareInjector()LX/0QD;
    .locals 8

    .prologue
    .line 798833
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/4fu;->a:LX/0QA;

    invoke-virtual {v0}, LX/0QA;->getScopeUnawareInjector()LX/0QD;

    move-result-object v1

    .line 798834
    new-instance v0, LX/4fu;

    check-cast v1, LX/0QA;

    iget-object v2, p0, LX/4fu;->c:LX/0R2;

    iget-object v3, p0, LX/4fu;->b:Ljava/lang/Class;

    iget-object v4, p0, LX/4fu;->e:Ljava/util/Set;

    iget-object v5, p0, LX/4fu;->f:Ljava/util/Set;

    iget-object v6, p0, LX/4fu;->g:Ljava/util/Set;

    iget-object v7, p0, LX/4fu;->h:Ljava/util/Set;

    invoke-direct/range {v0 .. v7}, LX/4fu;-><init>(LX/0QA;LX/0R2;Ljava/lang/Class;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 798835
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
