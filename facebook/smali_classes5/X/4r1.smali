.class public LX/4r1;
.super LX/4r0;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x4a2f47f9ad71b962L


# direct methods
.method public constructor <init>(LX/0lJ;LX/4qx;Ljava/lang/String;ZLjava/lang/Class;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lJ;",
            "LX/4qx;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 814951
    invoke-direct/range {p0 .. p5}, LX/4r0;-><init>(LX/0lJ;LX/4qx;Ljava/lang/String;ZLjava/lang/Class;)V

    .line 814952
    return-void
.end method

.method public constructor <init>(LX/4r1;LX/2Ay;)V
    .locals 0

    .prologue
    .line 814986
    invoke-direct {p0, p1, p2}, LX/4r0;-><init>(LX/4r0;LX/2Ay;)V

    .line 814987
    return-void
.end method

.method private final e(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 814972
    invoke-virtual {p1}, LX/15w;->m()Z

    move-result v0

    .line 814973
    invoke-direct {p0, p1, p2}, LX/4r1;->f(LX/15w;LX/0n3;)Ljava/lang/String;

    move-result-object v1

    .line 814974
    invoke-virtual {p0, p2, v1}, LX/4r0;->a(LX/0n3;Ljava/lang/String;)Lcom/fasterxml/jackson/databind/JsonDeserializer;

    move-result-object v2

    .line 814975
    iget-boolean v3, p0, LX/4r0;->_typeIdVisible:Z

    if-eqz v3, :cond_0

    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-ne v3, v4, :cond_0

    .line 814976
    new-instance v3, LX/0nW;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, LX/0nW;-><init>(LX/0lD;)V

    .line 814977
    invoke-virtual {v3}, LX/0nX;->f()V

    .line 814978
    iget-object v4, p0, LX/4r0;->_typePropertyName:Ljava/lang/String;

    invoke-virtual {v3, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 814979
    invoke-virtual {v3, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 814980
    invoke-virtual {v3, p1}, LX/0nW;->a(LX/15w;)LX/15w;

    move-result-object v1

    invoke-static {v1, p1}, LX/4pj;->a(LX/15w;LX/15w;)LX/4pj;

    move-result-object p1

    .line 814981
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 814982
    :cond_0
    invoke-virtual {v2, p1, p2}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v1

    .line 814983
    if-eqz v0, :cond_1

    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v0, v2, :cond_1

    .line 814984
    sget-object v0, LX/15z;->END_ARRAY:LX/15z;

    const-string v1, "expected closing END_ARRAY after type information and deserialized value"

    invoke-static {p1, v0, v1}, LX/0n3;->a(LX/15w;LX/15z;Ljava/lang/String;)LX/28E;

    move-result-object v0

    throw v0

    .line 814985
    :cond_1
    return-object v1
.end method

.method private f(LX/15w;LX/0n3;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 814960
    invoke-virtual {p1}, LX/15w;->m()Z

    move-result v0

    if-nez v0, :cond_1

    .line 814961
    iget-object v0, p0, LX/4r0;->_defaultImpl:LX/0lJ;

    if-eqz v0, :cond_0

    .line 814962
    iget-object v0, p0, LX/4r0;->_idResolver:LX/4qx;

    invoke-interface {v0}, LX/4qx;->a()Ljava/lang/String;

    move-result-object v0

    .line 814963
    :goto_0
    return-object v0

    .line 814964
    :cond_0
    sget-object v0, LX/15z;->START_ARRAY:LX/15z;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "need JSON Array to contain As.WRAPPER_ARRAY type information for class "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/4r0;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/0n3;->a(LX/15w;LX/15z;Ljava/lang/String;)LX/28E;

    move-result-object v0

    throw v0

    .line 814965
    :cond_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v0

    .line 814966
    sget-object v1, LX/15z;->VALUE_STRING:LX/15z;

    if-ne v0, v1, :cond_2

    .line 814967
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    .line 814968
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    goto :goto_0

    .line 814969
    :cond_2
    iget-object v0, p0, LX/4r0;->_defaultImpl:LX/0lJ;

    if-eqz v0, :cond_3

    .line 814970
    iget-object v0, p0, LX/4r0;->_idResolver:LX/4qx;

    invoke-interface {v0}, LX/4qx;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 814971
    :cond_3
    sget-object v0, LX/15z;->VALUE_STRING:LX/15z;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "need JSON String that contains type id (for subtype of "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/4r0;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/0n3;->a(LX/15w;LX/15z;Ljava/lang/String;)LX/28E;

    move-result-object v0

    throw v0
.end method


# virtual methods
.method public a()LX/4pO;
    .locals 1

    .prologue
    .line 814959
    sget-object v0, LX/4pO;->WRAPPER_ARRAY:LX/4pO;

    return-object v0
.end method

.method public a(LX/2Ay;)LX/4qw;
    .locals 1

    .prologue
    .line 814957
    iget-object v0, p0, LX/4r0;->_property:LX/2Ay;

    if-ne p1, v0, :cond_0

    .line 814958
    :goto_0
    return-object p0

    :cond_0
    new-instance v0, LX/4r1;

    invoke-direct {v0, p0, p1}, LX/4r1;-><init>(LX/4r1;LX/2Ay;)V

    move-object p0, v0

    goto :goto_0
.end method

.method public a(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 814956
    invoke-direct {p0, p1, p2}, LX/4r1;->e(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 814955
    invoke-direct {p0, p1, p2}, LX/4r1;->e(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final c(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 814954
    invoke-direct {p0, p1, p2}, LX/4r1;->e(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public d(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 814953
    invoke-direct {p0, p1, p2}, LX/4r1;->e(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
