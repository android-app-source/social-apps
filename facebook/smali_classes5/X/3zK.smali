.class public final enum LX/3zK;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/3zK;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/3zK;

.field public static final enum BOOKMARKS:LX/3zK;

.field public static final enum CHAT_HEAD:LX/3zK;

.field public static final enum DIVEBAR:LX/3zK;

.field public static final enum MESSAGING_TAB:LX/3zK;

.field public static final enum PROFILE:LX/3zK;

.field public static final enum SEND_AS_MESSAGE:LX/3zK;


# instance fields
.field public final name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 662366
    new-instance v0, LX/3zK;

    const-string v1, "BOOKMARKS"

    const-string v2, "messenger_entry_bookmarks"

    invoke-direct {v0, v1, v4, v2}, LX/3zK;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3zK;->BOOKMARKS:LX/3zK;

    .line 662367
    new-instance v0, LX/3zK;

    const-string v1, "CHAT_HEAD"

    const-string v2, "messenger_entry_chat_head"

    invoke-direct {v0, v1, v5, v2}, LX/3zK;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3zK;->CHAT_HEAD:LX/3zK;

    .line 662368
    new-instance v0, LX/3zK;

    const-string v1, "DIVEBAR"

    const-string v2, "messenger_entry_divebar"

    invoke-direct {v0, v1, v6, v2}, LX/3zK;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3zK;->DIVEBAR:LX/3zK;

    .line 662369
    new-instance v0, LX/3zK;

    const-string v1, "MESSAGING_TAB"

    const-string v2, "messenger_entry_messaging_tab"

    invoke-direct {v0, v1, v7, v2}, LX/3zK;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3zK;->MESSAGING_TAB:LX/3zK;

    .line 662370
    new-instance v0, LX/3zK;

    const-string v1, "PROFILE"

    const-string v2, "messenger_entry_profile"

    invoke-direct {v0, v1, v8, v2}, LX/3zK;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3zK;->PROFILE:LX/3zK;

    .line 662371
    new-instance v0, LX/3zK;

    const-string v1, "SEND_AS_MESSAGE"

    const/4 v2, 0x5

    const-string v3, "messenger_entry_send_as_message"

    invoke-direct {v0, v1, v2, v3}, LX/3zK;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/3zK;->SEND_AS_MESSAGE:LX/3zK;

    .line 662372
    const/4 v0, 0x6

    new-array v0, v0, [LX/3zK;

    sget-object v1, LX/3zK;->BOOKMARKS:LX/3zK;

    aput-object v1, v0, v4

    sget-object v1, LX/3zK;->CHAT_HEAD:LX/3zK;

    aput-object v1, v0, v5

    sget-object v1, LX/3zK;->DIVEBAR:LX/3zK;

    aput-object v1, v0, v6

    sget-object v1, LX/3zK;->MESSAGING_TAB:LX/3zK;

    aput-object v1, v0, v7

    sget-object v1, LX/3zK;->PROFILE:LX/3zK;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/3zK;->SEND_AS_MESSAGE:LX/3zK;

    aput-object v2, v0, v1

    sput-object v0, LX/3zK;->$VALUES:[LX/3zK;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 662373
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 662374
    iput-object p3, p0, LX/3zK;->name:Ljava/lang/String;

    .line 662375
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/3zK;
    .locals 1

    .prologue
    .line 662365
    const-class v0, LX/3zK;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/3zK;

    return-object v0
.end method

.method public static values()[LX/3zK;
    .locals 1

    .prologue
    .line 662364
    sget-object v0, LX/3zK;->$VALUES:[LX/3zK;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/3zK;

    return-object v0
.end method
