.class public final LX/3dL;
.super LX/0ur;
.source ""


# instance fields
.field public A:Lcom/facebook/graphql/model/GraphQLFriendsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public B:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

.field public C:Lcom/facebook/graphql/enums/GraphQLGender;

.field public D:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public E:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public F:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public G:Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

.field public H:Z

.field public I:Z

.field public J:Z

.field public K:Z

.field public L:Z

.field public M:Z

.field public N:Z

.field public O:Z

.field public P:Z

.field public Q:Z

.field public R:Z

.field public S:Z

.field public T:Z

.field public U:Z

.field public V:Z

.field public W:Lcom/facebook/graphql/model/GraphQLLikedProfilesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public X:Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

.field public Y:Lcom/facebook/graphql/model/GraphQLLocation;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Z:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aA:Lcom/facebook/graphql/model/GraphQLSinglePublisherVideoChannelsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aB:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aC:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aD:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aE:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aF:Lcom/facebook/graphql/model/GraphQLName;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aG:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

.field public aH:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aI:I

.field public aJ:Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aK:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aL:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aM:Z

.field public aN:Z

.field public aO:Lcom/facebook/graphql/model/GraphQLProfile;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aP:Z

.field public aQ:Z

.field public aR:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aS:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aT:Z

.field public aU:Z

.field public aV:D

.field public aW:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aa:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ab:J

.field public ac:D

.field public ad:Lcom/facebook/graphql/model/GraphQLUser;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ae:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public af:Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ag:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ah:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public ai:Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aj:Z

.field public ak:Lcom/facebook/graphql/model/GraphQLPageLikersConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public al:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public am:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public an:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

.field public ao:Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ap:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aq:Lcom/facebook/graphql/model/GraphQLProfileBadge;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ar:Lcom/facebook/graphql/model/GraphQLPhoto;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public as:Lcom/facebook/graphql/model/GraphQLImage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public at:J

.field public au:Z

.field public av:Lcom/facebook/graphql/model/GraphQLProfileVideo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aw:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ax:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ay:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

.field public az:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Lcom/facebook/graphql/model/GraphQLStreetAddress;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Lcom/facebook/graphql/model/GraphQLPageAdminInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Lcom/facebook/graphql/model/GraphQLAppStoreApplication;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:I

.field public h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/model/GraphQLDate;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/model/GraphQLFriendsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Z

.field public l:Z

.field public m:Z

.field public n:Z

.field public o:Z

.field public p:Z

.field public q:Z

.field public r:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public s:D

.field public t:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Z

.field public w:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public x:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:Lcom/facebook/graphql/model/GraphQLEventsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 616876
    invoke-direct {p0}, LX/0ur;-><init>()V

    .line 616877
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iput-object v0, p0, LX/3dL;->B:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 616878
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGender;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGender;

    iput-object v0, p0, LX/3dL;->C:Lcom/facebook/graphql/enums/GraphQLGender;

    .line 616879
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

    iput-object v0, p0, LX/3dL;->G:Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

    .line 616880
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    iput-object v0, p0, LX/3dL;->X:Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    .line 616881
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    iput-object v0, p0, LX/3dL;->an:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    .line 616882
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    iput-object v0, p0, LX/3dL;->ay:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    .line 616883
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    iput-object v0, p0, LX/3dL;->aG:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 616884
    const/4 v0, 0x0

    iput-object v0, p0, LX/3dL;->aW:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 616885
    instance-of v0, p0, LX/3dL;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 616886
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLActor;)LX/3dL;
    .locals 4

    .prologue
    .line 616887
    new-instance v0, LX/3dL;

    invoke-direct {v0}, LX/3dL;-><init>()V

    .line 616888
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 616889
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aR()Lcom/facebook/graphql/model/GraphQLStreetAddress;

    move-result-object v1

    iput-object v1, v0, LX/3dL;->b:Lcom/facebook/graphql/model/GraphQLStreetAddress;

    .line 616890
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->k()Lcom/facebook/graphql/model/GraphQLPageAdminInfo;

    move-result-object v1

    iput-object v1, v0, LX/3dL;->c:Lcom/facebook/graphql/model/GraphQLPageAdminInfo;

    .line 616891
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->l()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/3dL;->d:Ljava/lang/String;

    .line 616892
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->m()Lcom/facebook/graphql/model/GraphQLAppStoreApplication;

    move-result-object v1

    iput-object v1, v0, LX/3dL;->e:Lcom/facebook/graphql/model/GraphQLAppStoreApplication;

    .line 616893
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aL()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/3dL;->f:Ljava/lang/String;

    .line 616894
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aT()I

    move-result v1

    iput v1, v0, LX/3dL;->g:I

    .line 616895
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/3dL;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 616896
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aN()Lcom/facebook/graphql/model/GraphQLDate;

    move-result-object v1

    iput-object v1, v0, LX/3dL;->i:Lcom/facebook/graphql/model/GraphQLDate;

    .line 616897
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->o()Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    move-result-object v1

    iput-object v1, v0, LX/3dL;->j:Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    .line 616898
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->p()Z

    move-result v1

    iput-boolean v1, v0, LX/3dL;->k:Z

    .line 616899
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->q()Z

    move-result v1

    iput-boolean v1, v0, LX/3dL;->l:Z

    .line 616900
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->r()Z

    move-result v1

    iput-boolean v1, v0, LX/3dL;->m:Z

    .line 616901
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->s()Z

    move-result v1

    iput-boolean v1, v0, LX/3dL;->n:Z

    .line 616902
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->t()Z

    move-result v1

    iput-boolean v1, v0, LX/3dL;->o:Z

    .line 616903
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->u()Z

    move-result v1

    iput-boolean v1, v0, LX/3dL;->p:Z

    .line 616904
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->v()Z

    move-result v1

    iput-boolean v1, v0, LX/3dL;->q:Z

    .line 616905
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->w()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/3dL;->r:LX/0Px;

    .line 616906
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->x()D

    move-result-wide v2

    iput-wide v2, v0, LX/3dL;->s:D

    .line 616907
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->y()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v1

    iput-object v1, v0, LX/3dL;->t:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 616908
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->z()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v1

    iput-object v1, v0, LX/3dL;->u:Lcom/facebook/graphql/model/GraphQLPage;

    .line 616909
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->bb()Z

    move-result v1

    iput-boolean v1, v0, LX/3dL;->v:Z

    .line 616910
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->A()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/3dL;->w:LX/0Px;

    .line 616911
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->B()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/3dL;->x:Ljava/lang/String;

    .line 616912
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aP()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/3dL;->y:Ljava/lang/String;

    .line 616913
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->C()Lcom/facebook/graphql/model/GraphQLEventsConnection;

    move-result-object v1

    iput-object v1, v0, LX/3dL;->z:Lcom/facebook/graphql/model/GraphQLEventsConnection;

    .line 616914
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->D()Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    move-result-object v1

    iput-object v1, v0, LX/3dL;->A:Lcom/facebook/graphql/model/GraphQLFriendsConnection;

    .line 616915
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->E()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v1

    iput-object v1, v0, LX/3dL;->B:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 616916
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->F()Lcom/facebook/graphql/enums/GraphQLGender;

    move-result-object v1

    iput-object v1, v0, LX/3dL;->C:Lcom/facebook/graphql/enums/GraphQLGender;

    .line 616917
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->G()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v1

    iput-object v1, v0, LX/3dL;->D:Lcom/facebook/graphql/model/GraphQLPage;

    .line 616918
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/3dL;->E:Ljava/lang/String;

    .line 616919
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aY()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/3dL;->F:Ljava/lang/String;

    .line 616920
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aK()Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

    move-result-object v1

    iput-object v1, v0, LX/3dL;->G:Lcom/facebook/graphql/enums/GraphQLPageInviteeStatus;

    .line 616921
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->I()Z

    move-result v1

    iput-boolean v1, v0, LX/3dL;->H:Z

    .line 616922
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->J()Z

    move-result v1

    iput-boolean v1, v0, LX/3dL;->I:Z

    .line 616923
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->K()Z

    move-result v1

    iput-boolean v1, v0, LX/3dL;->J:Z

    .line 616924
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->L()Z

    move-result v1

    iput-boolean v1, v0, LX/3dL;->K:Z

    .line 616925
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->M()Z

    move-result v1

    iput-boolean v1, v0, LX/3dL;->L:Z

    .line 616926
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aO()Z

    move-result v1

    iput-boolean v1, v0, LX/3dL;->M:Z

    .line 616927
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->N()Z

    move-result v1

    iput-boolean v1, v0, LX/3dL;->N:Z

    .line 616928
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->O()Z

    move-result v1

    iput-boolean v1, v0, LX/3dL;->O:Z

    .line 616929
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->P()Z

    move-result v1

    iput-boolean v1, v0, LX/3dL;->P:Z

    .line 616930
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->Q()Z

    move-result v1

    iput-boolean v1, v0, LX/3dL;->Q:Z

    .line 616931
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->R()Z

    move-result v1

    iput-boolean v1, v0, LX/3dL;->R:Z

    .line 616932
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aW()Z

    move-result v1

    iput-boolean v1, v0, LX/3dL;->S:Z

    .line 616933
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->S()Z

    move-result v1

    iput-boolean v1, v0, LX/3dL;->T:Z

    .line 616934
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->T()Z

    move-result v1

    iput-boolean v1, v0, LX/3dL;->U:Z

    .line 616935
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->U()Z

    move-result v1

    iput-boolean v1, v0, LX/3dL;->V:Z

    .line 616936
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->V()Lcom/facebook/graphql/model/GraphQLLikedProfilesConnection;

    move-result-object v1

    iput-object v1, v0, LX/3dL;->W:Lcom/facebook/graphql/model/GraphQLLikedProfilesConnection;

    .line 616937
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->W()Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    move-result-object v1

    iput-object v1, v0, LX/3dL;->X:Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    .line 616938
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aS()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v1

    iput-object v1, v0, LX/3dL;->Y:Lcom/facebook/graphql/model/GraphQLLocation;

    .line 616939
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aM()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/3dL;->Z:Lcom/facebook/graphql/model/GraphQLImage;

    .line 616940
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aZ()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/3dL;->aa:Lcom/facebook/graphql/model/GraphQLImage;

    .line 616941
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->X()J

    move-result-wide v2

    iput-wide v2, v0, LX/3dL;->ab:J

    .line 616942
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->Y()D

    move-result-wide v2

    iput-wide v2, v0, LX/3dL;->ac:D

    .line 616943
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aQ()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v1

    iput-object v1, v0, LX/3dL;->ad:Lcom/facebook/graphql/model/GraphQLUser;

    .line 616944
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->Z()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/3dL;->ae:Ljava/lang/String;

    .line 616945
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aa()Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;

    move-result-object v1

    iput-object v1, v0, LX/3dL;->af:Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;

    .line 616946
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/3dL;->ag:Ljava/lang/String;

    .line 616947
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ac()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/3dL;->ah:LX/0Px;

    .line 616948
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ad()Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;

    move-result-object v1

    iput-object v1, v0, LX/3dL;->ai:Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;

    .line 616949
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->be()Z

    move-result v1

    iput-boolean v1, v0, LX/3dL;->aj:Z

    .line 616950
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ae()Lcom/facebook/graphql/model/GraphQLPageLikersConnection;

    move-result-object v1

    iput-object v1, v0, LX/3dL;->ak:Lcom/facebook/graphql/model/GraphQLPageLikersConnection;

    .line 616951
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aU()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/3dL;->al:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 616952
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aX()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/3dL;->am:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 616953
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aV()Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    move-result-object v1

    iput-object v1, v0, LX/3dL;->an:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    .line 616954
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->af()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v1

    iput-object v1, v0, LX/3dL;->ao:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 616955
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ag()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/3dL;->ap:Lcom/facebook/graphql/model/GraphQLImage;

    .line 616956
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ah()Lcom/facebook/graphql/model/GraphQLProfileBadge;

    move-result-object v1

    iput-object v1, v0, LX/3dL;->aq:Lcom/facebook/graphql/model/GraphQLProfileBadge;

    .line 616957
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ai()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v1

    iput-object v1, v0, LX/3dL;->ar:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 616958
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/3dL;->as:Lcom/facebook/graphql/model/GraphQLImage;

    .line 616959
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ak()J

    move-result-wide v2

    iput-wide v2, v0, LX/3dL;->at:J

    .line 616960
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->al()Z

    move-result v1

    iput-boolean v1, v0, LX/3dL;->au:Z

    .line 616961
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->am()Lcom/facebook/graphql/model/GraphQLProfileVideo;

    move-result-object v1

    iput-object v1, v0, LX/3dL;->av:Lcom/facebook/graphql/model/GraphQLProfileVideo;

    .line 616962
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->an()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/3dL;->aw:Ljava/lang/String;

    .line 616963
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ao()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/3dL;->ax:Ljava/lang/String;

    .line 616964
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ap()Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    move-result-object v1

    iput-object v1, v0, LX/3dL;->ay:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    .line 616965
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aq()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/3dL;->az:Ljava/lang/String;

    .line 616966
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ar()Lcom/facebook/graphql/model/GraphQLSinglePublisherVideoChannelsConnection;

    move-result-object v1

    iput-object v1, v0, LX/3dL;->aA:Lcom/facebook/graphql/model/GraphQLSinglePublisherVideoChannelsConnection;

    .line 616967
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->bc()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/3dL;->aB:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 616968
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->as()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/3dL;->aC:Lcom/facebook/graphql/model/GraphQLImage;

    .line 616969
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->at()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/3dL;->aD:Lcom/facebook/graphql/model/GraphQLImage;

    .line 616970
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->au()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    iput-object v1, v0, LX/3dL;->aE:Lcom/facebook/graphql/model/GraphQLImage;

    .line 616971
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->av()Lcom/facebook/graphql/model/GraphQLName;

    move-result-object v1

    iput-object v1, v0, LX/3dL;->aF:Lcom/facebook/graphql/model/GraphQLName;

    .line 616972
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aw()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v1

    iput-object v1, v0, LX/3dL;->aG:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 616973
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ax()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/3dL;->aH:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 616974
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ay()I

    move-result v1

    iput v1, v0, LX/3dL;->aI:I

    .line 616975
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ba()Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;

    move-result-object v1

    iput-object v1, v0, LX/3dL;->aJ:Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;

    .line 616976
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->az()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/3dL;->aK:Ljava/lang/String;

    .line 616977
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aA()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/3dL;->aL:Ljava/lang/String;

    .line 616978
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aB()Z

    move-result v1

    iput-boolean v1, v0, LX/3dL;->aM:Z

    .line 616979
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aC()Z

    move-result v1

    iput-boolean v1, v0, LX/3dL;->aN:Z

    .line 616980
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aD()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    iput-object v1, v0, LX/3dL;->aO:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 616981
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aE()Z

    move-result v1

    iput-boolean v1, v0, LX/3dL;->aP:Z

    .line 616982
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aF()Z

    move-result v1

    iput-boolean v1, v0, LX/3dL;->aQ:Z

    .line 616983
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aG()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/3dL;->aR:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 616984
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aH()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/3dL;->aS:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 616985
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aI()Z

    move-result v1

    iput-boolean v1, v0, LX/3dL;->aT:Z

    .line 616986
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->bd()Z

    move-result v1

    iput-boolean v1, v0, LX/3dL;->aU:Z

    .line 616987
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aJ()D

    move-result-wide v2

    iput-wide v2, v0, LX/3dL;->aV:D

    .line 616988
    invoke-static {v0, p0}, LX/0ur;->a(LX/0ur;Lcom/facebook/graphql/modelutil/BaseModel;)V

    .line 616989
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    iput-object v1, v0, LX/3dL;->aW:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 616990
    return-object v0
.end method


# virtual methods
.method public final a()Lcom/facebook/graphql/model/GraphQLActor;
    .locals 2

    .prologue
    .line 616991
    new-instance v0, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-direct {v0, p0}, Lcom/facebook/graphql/model/GraphQLActor;-><init>(LX/3dL;)V

    .line 616992
    return-object v0
.end method
