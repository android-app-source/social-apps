.class public LX/3vn;
.super Landroid/widget/ListView;
.source ""


# static fields
.field private static final f:[I


# instance fields
.field public final a:Landroid/graphics/Rect;

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public g:Ljava/lang/reflect/Field;

.field private h:LX/3vm;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 653555
    const/4 v0, 0x1

    new-array v0, v0, [I

    aput v1, v0, v1

    sput-object v0, LX/3vn;->f:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 653543
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 653544
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/3vn;->a:Landroid/graphics/Rect;

    .line 653545
    iput v1, p0, LX/3vn;->b:I

    .line 653546
    iput v1, p0, LX/3vn;->c:I

    .line 653547
    iput v1, p0, LX/3vn;->d:I

    .line 653548
    iput v1, p0, LX/3vn;->e:I

    .line 653549
    :try_start_0
    const-class v0, Landroid/widget/AbsListView;

    const-string v1, "mIsChildViewEnabled"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    iput-object v0, p0, LX/3vn;->g:Ljava/lang/reflect/Field;

    .line 653550
    iget-object v0, p0, LX/3vn;->g:Ljava/lang/reflect/Field;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0

    .line 653551
    :goto_0
    return-void

    .line 653552
    :catch_0
    move-exception v0

    .line 653553
    invoke-virtual {v0}, Ljava/lang/NoSuchFieldException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public final a(III)I
    .locals 12

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x0

    .line 653512
    invoke-virtual {p0}, LX/3vn;->getListPaddingTop()I

    move-result v2

    .line 653513
    invoke-virtual {p0}, LX/3vn;->getListPaddingBottom()I

    move-result v3

    .line 653514
    invoke-virtual {p0}, LX/3vn;->getListPaddingLeft()I

    .line 653515
    invoke-virtual {p0}, LX/3vn;->getListPaddingRight()I

    .line 653516
    invoke-virtual {p0}, LX/3vn;->getDividerHeight()I

    move-result v0

    .line 653517
    invoke-virtual {p0}, LX/3vn;->getDivider()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 653518
    invoke-virtual {p0}, LX/3vn;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v9

    .line 653519
    if-nez v9, :cond_1

    .line 653520
    add-int v4, v2, v3

    .line 653521
    :cond_0
    :goto_0
    return v4

    .line 653522
    :cond_1
    add-int/2addr v3, v2

    .line 653523
    if-lez v0, :cond_3

    if-eqz v4, :cond_3

    .line 653524
    :goto_1
    invoke-interface {v9}, Landroid/widget/ListAdapter;->getCount()I

    move-result v10

    move v8, v1

    move v5, v1

    move-object v7, v6

    move v4, v1

    .line 653525
    :goto_2
    if-ge v8, v10, :cond_6

    .line 653526
    invoke-interface {v9, v8}, Landroid/widget/ListAdapter;->getItemViewType(I)I

    move-result v2

    .line 653527
    if-eq v2, v5, :cond_7

    move v5, v2

    move-object v2, v6

    .line 653528
    :goto_3
    invoke-interface {v9, v8, v2, p0}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    .line 653529
    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 653530
    if-eqz v2, :cond_4

    iget v11, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-lez v11, :cond_4

    .line 653531
    iget v2, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    const/high16 v11, 0x40000000    # 2.0f

    invoke-static {v2, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 653532
    :goto_4
    invoke-virtual {v7, p1, v2}, Landroid/view/View;->measure(II)V

    .line 653533
    if-lez v8, :cond_9

    .line 653534
    add-int v2, v3, v0

    .line 653535
    :goto_5
    invoke-virtual {v7}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v3, v2

    .line 653536
    if-lt v3, p2, :cond_5

    .line 653537
    if-ltz p3, :cond_2

    if-le v8, p3, :cond_2

    if-lez v4, :cond_2

    if-ne v3, p2, :cond_0

    :cond_2
    move v4, p2

    goto :goto_0

    :cond_3
    move v0, v1

    .line 653538
    goto :goto_1

    .line 653539
    :cond_4
    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    goto :goto_4

    .line 653540
    :cond_5
    if-ltz p3, :cond_8

    if-lt v8, p3, :cond_8

    move v2, v3

    .line 653541
    :goto_6
    add-int/lit8 v4, v8, 0x1

    move v8, v4

    move v4, v2

    goto :goto_2

    :cond_6
    move v4, v3

    .line 653542
    goto :goto_0

    :cond_7
    move-object v2, v7

    goto :goto_3

    :cond_8
    move v2, v4

    goto :goto_6

    :cond_9
    move v2, v3

    goto :goto_5
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 653554
    const/4 v0, 0x0

    return v0
.end method

.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 653505
    iget-object v0, p0, LX/3vn;->a:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 653506
    invoke-virtual {p0}, LX/3vn;->getSelector()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 653507
    if-eqz v0, :cond_0

    .line 653508
    iget-object v1, p0, LX/3vn;->a:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 653509
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 653510
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/ListView;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 653511
    return-void
.end method

.method public final drawableStateChanged()V
    .locals 2

    .prologue
    .line 653482
    invoke-super {p0}, Landroid/widget/ListView;->drawableStateChanged()V

    .line 653483
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/3vn;->setSelectorEnabled(Z)V

    .line 653484
    invoke-virtual {p0}, LX/3vn;->getSelector()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 653485
    if-eqz v0, :cond_0

    .line 653486
    invoke-virtual {p0}, LX/3vn;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, LX/3vn;->isPressed()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 653487
    if-eqz v1, :cond_0

    .line 653488
    invoke-virtual {p0}, LX/3vn;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 653489
    :cond_0
    return-void

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setSelector(Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .prologue
    .line 653494
    if-eqz p1, :cond_1

    new-instance v0, LX/3vm;

    invoke-direct {v0, p1}, LX/3vm;-><init>(Landroid/graphics/drawable/Drawable;)V

    :goto_0
    iput-object v0, p0, LX/3vn;->h:LX/3vm;

    .line 653495
    iget-object v0, p0, LX/3vn;->h:LX/3vm;

    invoke-super {p0, v0}, Landroid/widget/ListView;->setSelector(Landroid/graphics/drawable/Drawable;)V

    .line 653496
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 653497
    if-eqz p1, :cond_0

    .line 653498
    invoke-virtual {p1, v0}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 653499
    :cond_0
    iget v1, v0, Landroid/graphics/Rect;->left:I

    iput v1, p0, LX/3vn;->b:I

    .line 653500
    iget v1, v0, Landroid/graphics/Rect;->top:I

    iput v1, p0, LX/3vn;->c:I

    .line 653501
    iget v1, v0, Landroid/graphics/Rect;->right:I

    iput v1, p0, LX/3vn;->d:I

    .line 653502
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    iput v0, p0, LX/3vn;->e:I

    .line 653503
    return-void

    .line 653504
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setSelectorEnabled(Z)V
    .locals 1

    .prologue
    .line 653490
    iget-object v0, p0, LX/3vn;->h:LX/3vm;

    if-eqz v0, :cond_0

    .line 653491
    iget-object v0, p0, LX/3vn;->h:LX/3vm;

    .line 653492
    iput-boolean p1, v0, LX/3vm;->a:Z

    .line 653493
    :cond_0
    return-void
.end method
