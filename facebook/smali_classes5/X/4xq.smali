.class public abstract LX/4xq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<TT;>;"
    }
.end annotation


# instance fields
.field public b:LX/1Ek;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ek",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public c:LX/1Ek;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ek",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public d:I

.field public final synthetic e:LX/1Ei;


# direct methods
.method public constructor <init>(LX/1Ei;)V
    .locals 1

    .prologue
    .line 821462
    iput-object p1, p0, LX/4xq;->e:LX/1Ei;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 821463
    iget-object v0, p0, LX/4xq;->e:LX/1Ei;

    iget-object v0, v0, LX/1Ei;->c:LX/1Ek;

    iput-object v0, p0, LX/4xq;->b:LX/1Ek;

    .line 821464
    const/4 v0, 0x0

    iput-object v0, p0, LX/4xq;->c:LX/1Ek;

    .line 821465
    iget-object v0, p0, LX/4xq;->e:LX/1Ei;

    iget v0, v0, LX/1Ei;->g:I

    iput v0, p0, LX/4xq;->d:I

    return-void
.end method


# virtual methods
.method public abstract a(LX/1Ek;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Ek",
            "<TK;TV;>;)TT;"
        }
    .end annotation
.end method

.method public final hasNext()Z
    .locals 2

    .prologue
    .line 821459
    iget-object v0, p0, LX/4xq;->e:LX/1Ei;

    iget v0, v0, LX/1Ei;->g:I

    iget v1, p0, LX/4xq;->d:I

    if-eq v0, v1, :cond_0

    .line 821460
    new-instance v0, Ljava/util/ConcurrentModificationException;

    invoke-direct {v0}, Ljava/util/ConcurrentModificationException;-><init>()V

    throw v0

    .line 821461
    :cond_0
    iget-object v0, p0, LX/4xq;->b:LX/1Ek;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final next()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 821445
    invoke-virtual {p0}, LX/4xq;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 821446
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 821447
    :cond_0
    iget-object v0, p0, LX/4xq;->b:LX/1Ek;

    .line 821448
    iget-object v1, v0, LX/1Ek;->nextInKeyInsertionOrder:LX/1Ek;

    iput-object v1, p0, LX/4xq;->b:LX/1Ek;

    .line 821449
    iput-object v0, p0, LX/4xq;->c:LX/1Ek;

    .line 821450
    invoke-virtual {p0, v0}, LX/4xq;->a(LX/1Ek;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final remove()V
    .locals 2

    .prologue
    .line 821451
    iget-object v0, p0, LX/4xq;->e:LX/1Ei;

    iget v0, v0, LX/1Ei;->g:I

    iget v1, p0, LX/4xq;->d:I

    if-eq v0, v1, :cond_0

    .line 821452
    new-instance v0, Ljava/util/ConcurrentModificationException;

    invoke-direct {v0}, Ljava/util/ConcurrentModificationException;-><init>()V

    throw v0

    .line 821453
    :cond_0
    iget-object v0, p0, LX/4xq;->c:LX/1Ek;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0P6;->a(Z)V

    .line 821454
    iget-object v0, p0, LX/4xq;->e:LX/1Ei;

    iget-object v1, p0, LX/4xq;->c:LX/1Ek;

    invoke-static {v0, v1}, LX/1Ei;->a$redex0(LX/1Ei;LX/1Ek;)V

    .line 821455
    iget-object v0, p0, LX/4xq;->e:LX/1Ei;

    iget v0, v0, LX/1Ei;->g:I

    iput v0, p0, LX/4xq;->d:I

    .line 821456
    const/4 v0, 0x0

    iput-object v0, p0, LX/4xq;->c:LX/1Ek;

    .line 821457
    return-void

    .line 821458
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
