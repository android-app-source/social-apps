.class public LX/3sO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3sN;


# instance fields
.field public a:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Landroid/view/View;",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 643853
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 643854
    const/4 v0, 0x0

    iput-object v0, p0, LX/3sO;->a:Ljava/util/WeakHashMap;

    .line 643855
    return-void
.end method

.method public static c(LX/3sO;LX/3sU;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 643869
    const/high16 v0, 0x7e000000

    invoke-virtual {p2, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    .line 643870
    const/4 v1, 0x0

    .line 643871
    instance-of v2, v0, LX/3oQ;

    if-eqz v2, :cond_4

    .line 643872
    check-cast v0, LX/3oQ;

    .line 643873
    :goto_0
    iget-object v1, p1, LX/3sU;->c:Ljava/lang/Runnable;

    .line 643874
    iget-object v2, p1, LX/3sU;->d:Ljava/lang/Runnable;

    .line 643875
    if-eqz v1, :cond_0

    .line 643876
    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    .line 643877
    :cond_0
    if-eqz v0, :cond_1

    .line 643878
    invoke-interface {v0, p2}, LX/3oQ;->a(Landroid/view/View;)V

    .line 643879
    invoke-interface {v0, p2}, LX/3oQ;->b(Landroid/view/View;)V

    .line 643880
    :cond_1
    if-eqz v2, :cond_2

    .line 643881
    invoke-interface {v2}, Ljava/lang/Runnable;->run()V

    .line 643882
    :cond_2
    iget-object v0, p0, LX/3sO;->a:Ljava/util/WeakHashMap;

    if-eqz v0, :cond_3

    .line 643883
    iget-object v0, p0, LX/3sO;->a:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p2}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 643884
    :cond_3
    return-void

    :cond_4
    move-object v0, v1

    goto :goto_0
.end method

.method private d(LX/3sU;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 643858
    const/4 v0, 0x0

    .line 643859
    iget-object v1, p0, LX/3sO;->a:Ljava/util/WeakHashMap;

    if-eqz v1, :cond_0

    .line 643860
    iget-object v0, p0, LX/3sO;->a:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p2}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 643861
    :cond_0
    if-nez v0, :cond_2

    .line 643862
    new-instance v0, Landroid/support/v4/view/ViewPropertyAnimatorCompat$BaseViewPropertyAnimatorCompatImpl$Starter;

    invoke-direct {v0, p0, p1, p2}, Landroid/support/v4/view/ViewPropertyAnimatorCompat$BaseViewPropertyAnimatorCompatImpl$Starter;-><init>(LX/3sO;LX/3sU;Landroid/view/View;)V

    .line 643863
    iget-object v1, p0, LX/3sO;->a:Ljava/util/WeakHashMap;

    if-nez v1, :cond_1

    .line 643864
    new-instance v1, Ljava/util/WeakHashMap;

    invoke-direct {v1}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v1, p0, LX/3sO;->a:Ljava/util/WeakHashMap;

    .line 643865
    :cond_1
    iget-object v1, p0, LX/3sO;->a:Ljava/util/WeakHashMap;

    invoke-virtual {v1, p2, v0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 643866
    :cond_2
    invoke-virtual {p2, v0}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 643867
    invoke-virtual {p2, v0}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 643868
    return-void
.end method


# virtual methods
.method public a(LX/3sU;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 643856
    invoke-direct {p0, p1, p2}, LX/3sO;->d(LX/3sU;Landroid/view/View;)V

    .line 643857
    return-void
.end method

.method public a(LX/3sU;Landroid/view/View;F)V
    .locals 0

    .prologue
    .line 643851
    invoke-direct {p0, p1, p2}, LX/3sO;->d(LX/3sU;Landroid/view/View;)V

    .line 643852
    return-void
.end method

.method public a(LX/3sU;Landroid/view/View;LX/3oQ;)V
    .locals 1

    .prologue
    .line 643849
    const/high16 v0, 0x7e000000

    invoke-virtual {p2, v0, p3}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 643850
    return-void
.end method

.method public a(Landroid/view/View;J)V
    .locals 0

    .prologue
    .line 643848
    return-void
.end method

.method public a(Landroid/view/View;LX/3sb;)V
    .locals 0

    .prologue
    .line 643885
    return-void
.end method

.method public a(Landroid/view/View;Landroid/view/animation/Interpolator;)V
    .locals 0

    .prologue
    .line 643847
    return-void
.end method

.method public b(LX/3sU;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 643841
    iget-object v0, p0, LX/3sO;->a:Ljava/util/WeakHashMap;

    if-eqz v0, :cond_0

    .line 643842
    iget-object v0, p0, LX/3sO;->a:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p2}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 643843
    if-eqz v0, :cond_0

    .line 643844
    invoke-virtual {p2, v0}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 643845
    :cond_0
    invoke-static {p0, p1, p2}, LX/3sO;->c(LX/3sO;LX/3sU;Landroid/view/View;)V

    .line 643846
    return-void
.end method

.method public b(LX/3sU;Landroid/view/View;F)V
    .locals 0

    .prologue
    .line 643839
    invoke-direct {p0, p1, p2}, LX/3sO;->d(LX/3sU;Landroid/view/View;)V

    .line 643840
    return-void
.end method

.method public b(Landroid/view/View;J)V
    .locals 0

    .prologue
    .line 643832
    return-void
.end method

.method public c(LX/3sU;Landroid/view/View;F)V
    .locals 0

    .prologue
    .line 643837
    invoke-direct {p0, p1, p2}, LX/3sO;->d(LX/3sU;Landroid/view/View;)V

    .line 643838
    return-void
.end method

.method public d(LX/3sU;Landroid/view/View;F)V
    .locals 0

    .prologue
    .line 643835
    invoke-direct {p0, p1, p2}, LX/3sO;->d(LX/3sU;Landroid/view/View;)V

    .line 643836
    return-void
.end method

.method public e(LX/3sU;Landroid/view/View;F)V
    .locals 0

    .prologue
    .line 643833
    invoke-direct {p0, p1, p2}, LX/3sO;->d(LX/3sU;Landroid/view/View;)V

    .line 643834
    return-void
.end method
