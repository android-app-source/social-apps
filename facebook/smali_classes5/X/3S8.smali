.class public LX/3S8;
.super LX/3R9;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/3S8;


# instance fields
.field private final a:LX/2Pw;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2Og;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/2Pw;LX/0Or;LX/0Or;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/annotations/IsMessengerAppIconBadgingEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2Pw;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "LX/2Og;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 581927
    invoke-direct {p0}, LX/3R9;-><init>()V

    .line 581928
    iput-object p1, p0, LX/3S8;->a:LX/2Pw;

    .line 581929
    iput-object p2, p0, LX/3S8;->b:LX/0Or;

    .line 581930
    iput-object p3, p0, LX/3S8;->c:LX/0Or;

    .line 581931
    return-void
.end method

.method public static a(LX/0QB;)LX/3S8;
    .locals 6

    .prologue
    .line 581932
    sget-object v0, LX/3S8;->d:LX/3S8;

    if-nez v0, :cond_1

    .line 581933
    const-class v1, LX/3S8;

    monitor-enter v1

    .line 581934
    :try_start_0
    sget-object v0, LX/3S8;->d:LX/3S8;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 581935
    if-eqz v2, :cond_0

    .line 581936
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 581937
    new-instance v4, LX/3S8;

    invoke-static {v0}, LX/2Pw;->a(LX/0QB;)LX/2Pw;

    move-result-object v3

    check-cast v3, LX/2Pw;

    const/16 v5, 0x14e3

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 p0, 0xce8

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v4, v3, v5, p0}, LX/3S8;-><init>(LX/2Pw;LX/0Or;LX/0Or;)V

    .line 581938
    move-object v0, v4

    .line 581939
    sput-object v0, LX/3S8;->d:LX/3S8;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 581940
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 581941
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 581942
    :cond_1
    sget-object v0, LX/3S8;->d:LX/3S8;

    return-object v0

    .line 581943
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 581944
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final b(Lcom/facebook/messaging/notify/NewMessageNotification;)V
    .locals 2

    .prologue
    .line 581945
    iget-object v0, p0, LX/3S8;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/facebook/messaging/notify/NewMessageNotification;->f:Lcom/facebook/push/PushProperty;

    iget-object v0, v0, Lcom/facebook/push/PushProperty;->a:LX/3B4;

    sget-object v1, LX/3B4;->C2DM:LX/3B4;

    if-ne v0, v1, :cond_0

    .line 581946
    iget-object v0, p0, LX/3S8;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Og;

    .line 581947
    sget-object v1, LX/6ek;->INBOX:LX/6ek;

    .line 581948
    iget-object p1, v0, LX/2Og;->c:LX/2OQ;

    invoke-virtual {p1, v1}, LX/2OQ;->e(LX/6ek;)Lcom/facebook/messaging/model/folders/FolderCounts;

    move-result-object p1

    move-object v0, p1

    .line 581949
    if-eqz v0, :cond_0

    iget v1, v0, Lcom/facebook/messaging/model/folders/FolderCounts;->c:I

    if-lez v1, :cond_0

    .line 581950
    iget-object v1, p0, LX/3S8;->a:LX/2Pw;

    iget v0, v0, Lcom/facebook/messaging/model/folders/FolderCounts;->c:I

    invoke-virtual {v1, v0}, LX/2Pw;->a(I)V

    .line 581951
    :cond_0
    return-void
.end method
