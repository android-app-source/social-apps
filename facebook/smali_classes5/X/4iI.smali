.class public LX/4iI;
.super LX/3nE;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3nE",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "LX/03R;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "LX/4iI;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Lorg/apache/http/HttpHost;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 802944
    const-class v0, LX/4iI;

    sput-object v0, LX/4iI;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Lorg/apache/http/HttpHost;)V
    .locals 0

    .prologue
    .line 802945
    invoke-direct {p0}, LX/3nE;-><init>()V

    .line 802946
    iput-object p1, p0, LX/4iI;->b:Lorg/apache/http/HttpHost;

    .line 802947
    return-void
.end method


# virtual methods
.method public final a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 802948
    iget-object v0, p0, LX/4iI;->b:Lorg/apache/http/HttpHost;

    if-nez v0, :cond_0

    .line 802949
    sget-object v0, LX/03R;->UNSET:LX/03R;

    .line 802950
    :goto_0
    return-object v0

    .line 802951
    :cond_0
    new-instance v1, Ljava/net/Socket;

    invoke-direct {v1}, Ljava/net/Socket;-><init>()V

    .line 802952
    const/16 v0, 0x1f4

    :try_start_0
    invoke-virtual {v1, v0}, Ljava/net/Socket;->setSoTimeout(I)V

    .line 802953
    new-instance v0, Ljava/net/InetSocketAddress;

    iget-object v2, p0, LX/4iI;->b:Lorg/apache/http/HttpHost;

    invoke-virtual {v2}, Lorg/apache/http/HttpHost;->getHostName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/4iI;->b:Lorg/apache/http/HttpHost;

    invoke-virtual {v3}, Lorg/apache/http/HttpHost;->getPort()I

    move-result v3

    invoke-direct {v0, v2, v3}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v1, v0}, Ljava/net/Socket;->connect(Ljava/net/SocketAddress;)V

    .line 802954
    sget-object v0, LX/03R;->YES:LX/03R;
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 802955
    :try_start_1
    invoke-virtual {v1}, Ljava/net/Socket;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 802956
    :catch_0
    move-exception v1

    .line 802957
    sget-object v2, LX/4iI;->a:Ljava/lang/Class;

    const-string v3, "Failed to close socket"

    invoke-static {v2, v3, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 802958
    :catch_1
    move-exception v0

    .line 802959
    :try_start_2
    sget-object v2, LX/4iI;->a:Ljava/lang/Class;

    const-string v3, "Failed to find host"

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 802960
    sget-object v0, LX/03R;->UNSET:LX/03R;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 802961
    :try_start_3
    invoke-virtual {v1}, Ljava/net/Socket;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 802962
    :catch_2
    move-exception v1

    .line 802963
    sget-object v2, LX/4iI;->a:Ljava/lang/Class;

    const-string v3, "Failed to close socket"

    invoke-static {v2, v3, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 802964
    :catch_3
    :try_start_4
    invoke-virtual {v1}, Ljava/net/Socket;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4

    .line 802965
    :goto_1
    sget-object v0, LX/03R;->NO:LX/03R;

    goto :goto_0

    .line 802966
    :catch_4
    move-exception v0

    .line 802967
    sget-object v1, LX/4iI;->a:Ljava/lang/Class;

    const-string v2, "Failed to close socket"

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 802968
    :catchall_0
    move-exception v0

    .line 802969
    :try_start_5
    invoke-virtual {v1}, Ljava/net/Socket;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_5

    .line 802970
    :goto_2
    throw v0

    .line 802971
    :catch_5
    move-exception v1

    .line 802972
    sget-object v2, LX/4iI;->a:Ljava/lang/Class;

    const-string v3, "Failed to close socket"

    invoke-static {v2, v3, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2
.end method
