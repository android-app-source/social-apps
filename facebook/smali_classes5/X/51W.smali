.class public final LX/51W;
.super LX/0Rr;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Rr",
        "<",
        "Ljava/util/Map$Entry",
        "<",
        "LX/4xM",
        "<TC;>;",
        "LX/50M",
        "<TC;>;>;>;"
    }
.end annotation


# instance fields
.field public a:LX/4xM;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4xM",
            "<TC;>;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/4xM;

.field public final synthetic c:LX/4yu;

.field public final synthetic d:LX/51Y;


# direct methods
.method public constructor <init>(LX/51Y;LX/4xM;LX/4yu;)V
    .locals 1

    .prologue
    .line 825182
    iput-object p1, p0, LX/51W;->d:LX/51Y;

    iput-object p2, p0, LX/51W;->b:LX/4xM;

    iput-object p3, p0, LX/51W;->c:LX/4yu;

    invoke-direct {p0}, LX/0Rr;-><init>()V

    .line 825183
    iget-object v0, p0, LX/51W;->b:LX/4xM;

    iput-object v0, p0, LX/51W;->a:LX/4xM;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 825172
    iget-object v0, p0, LX/51W;->d:LX/51Y;

    iget-object v0, v0, LX/51Y;->c:LX/50M;

    iget-object v0, v0, LX/50M;->upperBound:LX/4xM;

    iget-object v1, p0, LX/51W;->a:LX/4xM;

    invoke-virtual {v0, v1}, LX/4xM;->a(Ljava/lang/Comparable;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/51W;->a:LX/4xM;

    sget-object v1, LX/4xN;->a:LX/4xN;

    if-ne v0, v1, :cond_1

    .line 825173
    :cond_0
    invoke-virtual {p0}, LX/0Rr;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 825174
    :goto_0
    return-object v0

    .line 825175
    :cond_1
    iget-object v0, p0, LX/51W;->c:LX/4yu;

    invoke-interface {v0}, LX/4yu;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 825176
    iget-object v0, p0, LX/51W;->c:LX/4yu;

    invoke-interface {v0}, LX/4yu;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/50M;

    .line 825177
    iget-object v1, p0, LX/51W;->a:LX/4xM;

    iget-object v2, v0, LX/50M;->lowerBound:LX/4xM;

    invoke-static {v1, v2}, LX/50M;->a(LX/4xM;LX/4xM;)LX/50M;

    move-result-object v1

    .line 825178
    iget-object v0, v0, LX/50M;->upperBound:LX/4xM;

    iput-object v0, p0, LX/51W;->a:LX/4xM;

    move-object v0, v1

    .line 825179
    :goto_1
    iget-object v1, v0, LX/50M;->lowerBound:LX/4xM;

    invoke-static {v1, v0}, LX/0PM;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    goto :goto_0

    .line 825180
    :cond_2
    iget-object v0, p0, LX/51W;->a:LX/4xM;

    sget-object v1, LX/4xN;->a:LX/4xN;

    invoke-static {v0, v1}, LX/50M;->a(LX/4xM;LX/4xM;)LX/50M;

    move-result-object v0

    .line 825181
    sget-object v1, LX/4xN;->a:LX/4xN;

    iput-object v1, p0, LX/51W;->a:LX/4xM;

    goto :goto_1
.end method
