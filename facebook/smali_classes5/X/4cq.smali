.class public LX/4cq;
.super LX/4cp;
.source ""


# instance fields
.field private b:[B


# direct methods
.method public constructor <init>([BLjava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 795344
    array-length v0, p1

    int-to-long v0, v0

    invoke-direct {p0, p2, v0, v1, p3}, LX/4cp;-><init>(Ljava/lang/String;JLjava/lang/String;)V

    .line 795345
    iput-object p1, p0, LX/4cq;->b:[B

    .line 795346
    return-void
.end method


# virtual methods
.method public final b(Ljava/io/OutputStream;)V
    .locals 2

    .prologue
    .line 795347
    if-nez p1, :cond_0

    .line 795348
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Output stream may not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 795349
    :cond_0
    iget-object v0, p0, LX/4cq;->b:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 795350
    return-void
.end method
