.class public LX/3YR;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 595990
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 595991
    return-void
.end method

.method public static a(LX/0QB;)LX/3YR;
    .locals 1

    .prologue
    .line 595992
    new-instance v0, LX/3YR;

    invoke-direct {v0}, LX/3YR;-><init>()V

    .line 595993
    move-object v0, v0

    .line 595994
    return-object v0
.end method

.method public static c(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 595995
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 595996
    :cond_0
    :goto_0
    return v0

    .line 595997
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    .line 595998
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    const v2, -0x48d0bc60

    if-eq v1, v2, :cond_0

    .line 595999
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v4

    .line 596000
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLNode;->fE()Lcom/facebook/graphql/model/GraphQLOpenGraphObject;

    move-result-object v5

    if-eqz v5, :cond_3

    .line 596001
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLNode;->fE()Lcom/facebook/graphql/model/GraphQLOpenGraphObject;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->o()Lcom/facebook/graphql/enums/GraphQLMusicType;

    move-result-object v5

    .line 596002
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLNode;->fE()Lcom/facebook/graphql/model/GraphQLOpenGraphObject;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->k()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v6

    .line 596003
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLNode;->fE()Lcom/facebook/graphql/model/GraphQLOpenGraphObject;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->j()LX/0Px;

    move-result-object v7

    .line 596004
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLNode;->fE()Lcom/facebook/graphql/model/GraphQLOpenGraphObject;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->p()LX/0Px;

    move-result-object v8

    .line 596005
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLNode;->fE()Lcom/facebook/graphql/model/GraphQLOpenGraphObject;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->r()LX/0Px;

    move-result-object v9

    .line 596006
    :goto_1
    new-instance v4, LX/JS6;

    invoke-direct/range {v4 .. v9}, LX/JS6;-><init>(Lcom/facebook/graphql/enums/GraphQLMusicType;Lcom/facebook/graphql/model/GraphQLApplication;Ljava/util/List;LX/0Px;LX/0Px;)V

    move-object v1, v4

    .line 596007
    iget-object v2, v1, LX/JS6;->a:Lcom/facebook/graphql/enums/GraphQLMusicType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLMusicType;->SONG:Lcom/facebook/graphql/enums/GraphQLMusicType;

    if-ne v2, v3, :cond_0

    .line 596008
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    const/4 v2, 0x1

    :goto_2
    move v2, v2

    .line 596009
    if-eqz v2, :cond_0

    .line 596010
    iget-object v2, v1, LX/JS6;->b:Lcom/facebook/graphql/model/GraphQLApplication;

    if-eqz v2, :cond_6

    iget-object v2, v1, LX/JS6;->b:Lcom/facebook/graphql/model/GraphQLApplication;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLApplication;->j()Lcom/facebook/graphql/model/GraphQLAndroidAppConfig;

    move-result-object v2

    if-eqz v2, :cond_6

    iget-object v2, v1, LX/JS6;->b:Lcom/facebook/graphql/model/GraphQLApplication;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLApplication;->j()Lcom/facebook/graphql/model/GraphQLAndroidAppConfig;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLAndroidAppConfig;->a()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_6

    iget-object v2, v1, LX/JS6;->b:Lcom/facebook/graphql/model/GraphQLApplication;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLApplication;->l()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    iget-object v2, v1, LX/JS6;->b:Lcom/facebook/graphql/model/GraphQLApplication;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLApplication;->o()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eqz v2, :cond_6

    iget-object v2, v1, LX/JS6;->b:Lcom/facebook/graphql/model/GraphQLApplication;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLApplication;->o()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_6

    const/4 v2, 0x1

    :goto_3
    move v2, v2

    .line 596011
    if-eqz v2, :cond_0

    const/4 v3, 0x0

    .line 596012
    iget-object v2, v1, LX/JS6;->c:Ljava/util/List;

    if-eqz v2, :cond_7

    iget-object v2, v1, LX/JS6;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_7

    iget-object v2, v1, LX/JS6;->c:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_7

    const/4 v2, 0x1

    :goto_4
    move v2, v2

    .line 596013
    if-eqz v2, :cond_0

    const/4 v3, 0x0

    .line 596014
    iget-object v2, v1, LX/JS6;->d:LX/0Px;

    if-eqz v2, :cond_8

    iget-object v2, v1, LX/JS6;->d:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_8

    iget-object v2, v1, LX/JS6;->d:LX/0Px;

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_8

    iget-object v2, v1, LX/JS6;->d:LX/0Px;

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->q()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_8

    const/4 v2, 0x1

    :goto_5
    move v2, v2

    .line 596015
    if-eqz v2, :cond_0

    const/4 v3, 0x0

    .line 596016
    iget-object v2, v1, LX/JS6;->e:LX/0Px;

    if-eqz v2, :cond_9

    iget-object v2, v1, LX/JS6;->e:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_9

    iget-object v2, v1, LX/JS6;->e:LX/0Px;

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_9

    iget-object v2, v1, LX/JS6;->e:LX/0Px;

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLAudio;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLAudio;->a()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_9

    const/4 v2, 0x1

    :goto_6
    move v1, v2

    .line 596017
    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto/16 :goto_0

    .line 596018
    :cond_3
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLNode;->hb()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v5

    if-eqz v5, :cond_4

    .line 596019
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLNode;->hb()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLNode;->fG()Lcom/facebook/graphql/enums/GraphQLMusicType;

    move-result-object v5

    .line 596020
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLNode;->V()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v6

    .line 596021
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLNode;->hb()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLNode;->P()LX/0Px;

    move-result-object v7

    .line 596022
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLNode;->hb()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLNode;->fH()LX/0Px;

    move-result-object v8

    .line 596023
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLNode;->hb()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLNode;->gU()LX/0Px;

    move-result-object v9

    goto/16 :goto_1

    .line 596024
    :cond_4
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLNode;->fG()Lcom/facebook/graphql/enums/GraphQLMusicType;

    move-result-object v5

    .line 596025
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLNode;->V()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v6

    .line 596026
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLNode;->P()LX/0Px;

    move-result-object v7

    .line 596027
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLNode;->fH()LX/0Px;

    move-result-object v8

    .line 596028
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLNode;->gU()LX/0Px;

    move-result-object v9

    goto/16 :goto_1

    :cond_5
    const/4 v2, 0x0

    goto/16 :goto_2

    :cond_6
    const/4 v2, 0x0

    goto/16 :goto_3

    :cond_7
    move v2, v3

    goto/16 :goto_4

    :cond_8
    move v2, v3

    goto :goto_5

    :cond_9
    move v2, v3

    goto :goto_6
.end method
