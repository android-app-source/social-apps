.class public LX/3qq;
.super Landroid/graphics/drawable/Drawable;
.source ""

# interfaces
.implements Landroid/graphics/drawable/Drawable$Callback;
.implements LX/3qp;


# static fields
.field public static final a:Landroid/graphics/PorterDuff$Mode;


# instance fields
.field public b:Landroid/graphics/drawable/Drawable;

.field private c:Landroid/content/res/ColorStateList;

.field private d:Landroid/graphics/PorterDuff$Mode;

.field private e:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 642520
    sget-object v0, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    sput-object v0, LX/3qq;->a:Landroid/graphics/PorterDuff$Mode;

    return-void
.end method

.method public constructor <init>(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 642521
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 642522
    sget-object v0, LX/3qq;->a:Landroid/graphics/PorterDuff$Mode;

    iput-object v0, p0, LX/3qq;->d:Landroid/graphics/PorterDuff$Mode;

    .line 642523
    const/high16 v0, -0x80000000

    iput v0, p0, LX/3qq;->e:I

    .line 642524
    invoke-direct {p0, p1}, LX/3qq;->a(Landroid/graphics/drawable/Drawable;)V

    .line 642525
    return-void
.end method

.method private a(Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .prologue
    .line 642526
    iget-object v0, p0, LX/3qq;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 642527
    iget-object v0, p0, LX/3qq;->b:Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 642528
    :cond_0
    iput-object p1, p0, LX/3qq;->b:Landroid/graphics/drawable/Drawable;

    .line 642529
    if-eqz p1, :cond_1

    .line 642530
    invoke-virtual {p1, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 642531
    :cond_1
    invoke-virtual {p0}, LX/3qq;->invalidateSelf()V

    .line 642532
    return-void
.end method

.method private a([I)Z
    .locals 2

    .prologue
    .line 642533
    iget-object v0, p0, LX/3qq;->c:Landroid/content/res/ColorStateList;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3qq;->d:Landroid/graphics/PorterDuff$Mode;

    if-eqz v0, :cond_0

    .line 642534
    iget-object v0, p0, LX/3qq;->c:Landroid/content/res/ColorStateList;

    iget-object v1, p0, LX/3qq;->c:Landroid/content/res/ColorStateList;

    invoke-virtual {v1}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v1

    invoke-virtual {v0, p1, v1}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v0

    .line 642535
    iget v1, p0, LX/3qq;->e:I

    if-eq v0, v1, :cond_0

    .line 642536
    iget-object v1, p0, LX/3qq;->d:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {p0, v0, v1}, LX/3qq;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 642537
    iput v0, p0, LX/3qq;->e:I

    .line 642538
    const/4 v0, 0x1

    .line 642539
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final draw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 642540
    iget-object v0, p0, LX/3qq;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 642541
    return-void
.end method

.method public final getChangingConfigurations()I
    .locals 1

    .prologue
    .line 642542
    iget-object v0, p0, LX/3qq;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getChangingConfigurations()I

    move-result v0

    return v0
.end method

.method public final getCurrent()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 642543
    iget-object v0, p0, LX/3qq;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getCurrent()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public final getIntrinsicHeight()I
    .locals 1

    .prologue
    .line 642550
    iget-object v0, p0, LX/3qq;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    return v0
.end method

.method public final getIntrinsicWidth()I
    .locals 1

    .prologue
    .line 642544
    iget-object v0, p0, LX/3qq;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    return v0
.end method

.method public final getMinimumHeight()I
    .locals 1

    .prologue
    .line 642545
    iget-object v0, p0, LX/3qq;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getMinimumHeight()I

    move-result v0

    return v0
.end method

.method public final getMinimumWidth()I
    .locals 1

    .prologue
    .line 642546
    iget-object v0, p0, LX/3qq;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getMinimumWidth()I

    move-result v0

    return v0
.end method

.method public final getOpacity()I
    .locals 1

    .prologue
    .line 642547
    iget-object v0, p0, LX/3qq;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getOpacity()I

    move-result v0

    return v0
.end method

.method public final getPadding(Landroid/graphics/Rect;)Z
    .locals 1

    .prologue
    .line 642548
    iget-object v0, p0, LX/3qq;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    move-result v0

    return v0
.end method

.method public final getState()[I
    .locals 1

    .prologue
    .line 642549
    iget-object v0, p0, LX/3qq;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getState()[I

    move-result-object v0

    return-object v0
.end method

.method public final getTransparentRegion()Landroid/graphics/Region;
    .locals 1

    .prologue
    .line 642517
    iget-object v0, p0, LX/3qq;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getTransparentRegion()Landroid/graphics/Region;

    move-result-object v0

    return-object v0
.end method

.method public final invalidateDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 642518
    invoke-virtual {p0}, LX/3qq;->invalidateSelf()V

    .line 642519
    return-void
.end method

.method public final isStateful()Z
    .locals 1

    .prologue
    .line 642481
    iget-object v0, p0, LX/3qq;->c:Landroid/content/res/ColorStateList;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3qq;->c:Landroid/content/res/ColorStateList;

    invoke-virtual {v0}, Landroid/content/res/ColorStateList;->isStateful()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, LX/3qq;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final mutate()Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 642482
    iget-object v0, p0, LX/3qq;->b:Landroid/graphics/drawable/Drawable;

    .line 642483
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 642484
    if-eq v1, v0, :cond_0

    .line 642485
    invoke-direct {p0, v1}, LX/3qq;->a(Landroid/graphics/drawable/Drawable;)V

    .line 642486
    :cond_0
    return-object p0
.end method

.method public final onBoundsChange(Landroid/graphics/Rect;)V
    .locals 1

    .prologue
    .line 642487
    iget-object v0, p0, LX/3qq;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 642488
    return-void
.end method

.method public final onLevelChange(I)Z
    .locals 1

    .prologue
    .line 642489
    iget-object v0, p0, LX/3qq;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setLevel(I)Z

    move-result v0

    return v0
.end method

.method public final scheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;J)V
    .locals 1

    .prologue
    .line 642490
    invoke-virtual {p0, p2, p3, p4}, LX/3qq;->scheduleSelf(Ljava/lang/Runnable;J)V

    .line 642491
    return-void
.end method

.method public final setAlpha(I)V
    .locals 1

    .prologue
    .line 642492
    iget-object v0, p0, LX/3qq;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 642493
    return-void
.end method

.method public final setChangingConfigurations(I)V
    .locals 1

    .prologue
    .line 642494
    iget-object v0, p0, LX/3qq;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setChangingConfigurations(I)V

    .line 642495
    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1

    .prologue
    .line 642496
    iget-object v0, p0, LX/3qq;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 642497
    return-void
.end method

.method public final setDither(Z)V
    .locals 1

    .prologue
    .line 642498
    iget-object v0, p0, LX/3qq;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setDither(Z)V

    .line 642499
    return-void
.end method

.method public final setFilterBitmap(Z)V
    .locals 1

    .prologue
    .line 642500
    iget-object v0, p0, LX/3qq;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setFilterBitmap(Z)V

    .line 642501
    return-void
.end method

.method public final setState([I)Z
    .locals 2

    .prologue
    .line 642502
    iget-object v0, p0, LX/3qq;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    move-result v0

    .line 642503
    invoke-direct {p0, p1}, LX/3qq;->a([I)Z

    move-result v1

    if-nez v1, :cond_0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 642504
    :goto_0
    return v0

    .line 642505
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setTint(I)V
    .locals 1

    .prologue
    .line 642506
    invoke-static {p1}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/3qq;->setTintList(Landroid/content/res/ColorStateList;)V

    .line 642507
    return-void
.end method

.method public setTintList(Landroid/content/res/ColorStateList;)V
    .locals 1

    .prologue
    .line 642508
    iput-object p1, p0, LX/3qq;->c:Landroid/content/res/ColorStateList;

    .line 642509
    invoke-virtual {p0}, LX/3qq;->getState()[I

    move-result-object v0

    invoke-direct {p0, v0}, LX/3qq;->a([I)Z

    .line 642510
    return-void
.end method

.method public setTintMode(Landroid/graphics/PorterDuff$Mode;)V
    .locals 1

    .prologue
    .line 642511
    iput-object p1, p0, LX/3qq;->d:Landroid/graphics/PorterDuff$Mode;

    .line 642512
    invoke-virtual {p0}, LX/3qq;->getState()[I

    move-result-object v0

    invoke-direct {p0, v0}, LX/3qq;->a([I)Z

    .line 642513
    return-void
.end method

.method public final setVisible(ZZ)Z
    .locals 1

    .prologue
    .line 642514
    invoke-super {p0, p1, p2}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/3qq;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final unscheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 642515
    invoke-virtual {p0, p2}, LX/3qq;->unscheduleSelf(Ljava/lang/Runnable;)V

    .line 642516
    return-void
.end method
