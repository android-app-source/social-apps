.class public LX/3hV;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JRQ;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JRU;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 626870
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/3hV;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/JRU;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 626871
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 626872
    iput-object p1, p0, LX/3hV;->b:LX/0Ot;

    .line 626873
    return-void
.end method

.method public static a(LX/0QB;)LX/3hV;
    .locals 4

    .prologue
    .line 626859
    const-class v1, LX/3hV;

    monitor-enter v1

    .line 626860
    :try_start_0
    sget-object v0, LX/3hV;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 626861
    sput-object v2, LX/3hV;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 626862
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 626863
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 626864
    new-instance v3, LX/3hV;

    const/16 p0, 0x202b

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/3hV;-><init>(LX/0Ot;)V

    .line 626865
    move-object v0, v3

    .line 626866
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 626867
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/3hV;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 626868
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 626869
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 4

    .prologue
    .line 626856
    check-cast p2, LX/JRR;

    .line 626857
    iget-object v0, p0, LX/3hV;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JRU;

    iget-object v1, p2, LX/JRR;->a:LX/1Pm;

    iget-object v2, p2, LX/JRR;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v0, p1, v1, v2}, LX/JRU;->a(LX/1De;LX/1Pm;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1Dg;

    move-result-object v0

    .line 626858
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 626854
    invoke-static {}, LX/1dS;->b()V

    .line 626855
    const/4 v0, 0x0

    return-object v0
.end method
