.class public LX/4fy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0fJ;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 798927
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Landroid/content/ComponentName;
    .locals 2

    .prologue
    .line 798930
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "This class doesn\'t perform any security checks. Please create an InternalIntentSigner for your app if you want to sign intents."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 798929
    return-void
.end method

.method public final b(Landroid/content/Intent;)Z
    .locals 2

    .prologue
    .line 798928
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "This class doesn\'t perform any security checks. Please create an InternalIntentSigner for your app if you want to sign intents."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
