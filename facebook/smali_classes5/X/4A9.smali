.class public LX/4A9;
.super LX/4A8;
.source ""

# interfaces
.implements LX/3Sb;


# static fields
.field public static final b:LX/4A9;


# instance fields
.field private final c:LX/15i;

.field private final d:I

.field private final e:Z

.field private final f:I

.field private final g:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 675398
    new-instance v0, LX/4A9;

    invoke-direct {v0}, LX/4A9;-><init>()V

    sput-object v0, LX/4A9;->b:LX/4A9;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 675369
    invoke-direct {p0}, LX/4A8;-><init>()V

    .line 675370
    const/4 v0, 0x0

    iput-object v0, p0, LX/4A9;->c:LX/15i;

    .line 675371
    iput v1, p0, LX/4A9;->d:I

    .line 675372
    iput-boolean v1, p0, LX/4A9;->e:Z

    .line 675373
    iput v1, p0, LX/4A9;->f:I

    .line 675374
    iput v1, p0, LX/4A9;->g:I

    .line 675375
    return-void
.end method

.method public constructor <init>(LX/15i;IZI)V
    .locals 1

    .prologue
    .line 675391
    invoke-direct {p0}, LX/4A8;-><init>()V

    .line 675392
    iput-object p1, p0, LX/4A9;->c:LX/15i;

    .line 675393
    iput p2, p0, LX/4A9;->d:I

    .line 675394
    iput-boolean p3, p0, LX/4A9;->e:Z

    .line 675395
    iput p4, p0, LX/4A9;->f:I

    .line 675396
    invoke-virtual {p1, p2}, LX/15i;->d(I)I

    move-result v0

    iput v0, p0, LX/4A9;->g:I

    .line 675397
    return-void
.end method

.method public static a(LX/15i;III)LX/4A9;
    .locals 3

    .prologue
    .line 675385
    const/4 v0, 0x0

    .line 675386
    invoke-virtual {p0, p1, p2}, LX/15i;->p(II)I

    move-result v2

    .line 675387
    if-nez v2, :cond_0

    .line 675388
    sget-object v1, LX/4A9;->b:LX/4A9;

    .line 675389
    :goto_0
    move-object v0, v1

    .line 675390
    return-object v0

    :cond_0
    new-instance v1, LX/4A9;

    invoke-direct {v1, p0, v2, v0, p3}, LX/4A9;-><init>(LX/15i;IZI)V

    goto :goto_0
.end method


# virtual methods
.method public final a(I)LX/1vs;
    .locals 4

    .prologue
    .line 675377
    if-ltz p1, :cond_0

    iget v0, p0, LX/4A9;->g:I

    if-lt p1, v0, :cond_1

    .line 675378
    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v0

    .line 675379
    :cond_1
    iget-object v0, p0, LX/4A9;->c:LX/15i;

    iget v1, p0, LX/4A9;->d:I

    invoke-virtual {v0, v1, p1}, LX/15i;->q(II)I

    move-result v1

    .line 675380
    iget v0, p0, LX/4A9;->f:I

    .line 675381
    iget-boolean v2, p0, LX/4A9;->e:Z

    if-eqz v2, :cond_2

    .line 675382
    iget-object v0, p0, LX/4A9;->c:LX/15i;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/15i;->i(II)S

    move-result v0

    .line 675383
    iget-object v2, p0, LX/4A9;->c:LX/15i;

    const/4 v3, 0x1

    invoke-virtual {v2, v1, v3}, LX/15i;->g(II)I

    move-result v1

    .line 675384
    :cond_2
    iget-object v2, p0, LX/4A9;->c:LX/15i;

    invoke-static {v2, v1, v0}, LX/1vs;->a(LX/15i;II)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 675376
    iget v0, p0, LX/4A9;->g:I

    return v0
.end method
