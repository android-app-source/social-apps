.class public final LX/4yS;
.super LX/0P7;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "LX/0P7",
        "<TE;>;"
    }
.end annotation


# instance fields
.field public final a:LX/1M1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1M1",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 821801
    invoke-static {}, LX/4z2;->g()LX/4z2;

    move-result-object v0

    invoke-direct {p0, v0}, LX/4yS;-><init>(LX/1M1;)V

    .line 821802
    return-void
.end method

.method private constructor <init>(LX/1M1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1M1",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 821798
    invoke-direct {p0}, LX/0P7;-><init>()V

    .line 821799
    iput-object p1, p0, LX/4yS;->a:LX/1M1;

    .line 821800
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Iterable;)LX/0P7;
    .locals 4

    .prologue
    .line 821783
    instance-of v0, p1, LX/1M1;

    if-eqz v0, :cond_0

    .line 821784
    check-cast p1, LX/1M1;

    move-object v0, p1

    .line 821785
    invoke-interface {v0}, LX/1M1;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4wx;

    .line 821786
    invoke-virtual {v0}, LX/4wx;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0}, LX/4wx;->b()I

    move-result v0

    .line 821787
    iget-object v3, p0, LX/4yS;->a:LX/1M1;

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    invoke-interface {v3, p1, v0}, LX/1M1;->a(Ljava/lang/Object;I)I

    .line 821788
    goto :goto_0

    .line 821789
    :cond_0
    invoke-super {p0, p1}, LX/0P7;->a(Ljava/lang/Iterable;)LX/0P7;

    .line 821790
    :cond_1
    return-object p0
.end method

.method public final a(Ljava/util/Iterator;)LX/0P7;
    .locals 1

    .prologue
    .line 821796
    invoke-super {p0, p1}, LX/0P7;->a(Ljava/util/Iterator;)LX/0P7;

    .line 821797
    return-object p0
.end method

.method public final a([Ljava/lang/Object;)LX/0P7;
    .locals 1

    .prologue
    .line 821794
    invoke-super {p0, p1}, LX/0P7;->a([Ljava/lang/Object;)LX/0P7;

    .line 821795
    return-object p0
.end method

.method public final a()LX/0Py;
    .locals 1

    .prologue
    .line 821793
    iget-object v0, p0, LX/4yS;->a:LX/1M1;

    invoke-static {v0}, LX/4yO;->a(Ljava/lang/Iterable;)LX/4yO;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/Object;)LX/0P7;
    .locals 2

    .prologue
    .line 821791
    iget-object v0, p0, LX/4yS;->a:LX/1M1;

    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1M1;->add(Ljava/lang/Object;)Z

    .line 821792
    return-object p0
.end method
