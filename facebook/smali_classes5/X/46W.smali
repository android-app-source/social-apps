.class public LX/46W;
.super Ljava/security/SecureRandom;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/46W;


# instance fields
.field private final mDelegate:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/security/SecureRandom;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .param p1    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/random/FixedSecureRandom;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Ljava/security/SecureRandom;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 671255
    invoke-direct {p0}, Ljava/security/SecureRandom;-><init>()V

    .line 671256
    iput-object p1, p0, LX/46W;->mDelegate:LX/0Ot;

    .line 671257
    return-void
.end method

.method public static a(LX/0QB;)LX/46W;
    .locals 4

    .prologue
    .line 671262
    sget-object v0, LX/46W;->a:LX/46W;

    if-nez v0, :cond_1

    .line 671263
    const-class v1, LX/46W;

    monitor-enter v1

    .line 671264
    :try_start_0
    sget-object v0, LX/46W;->a:LX/46W;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 671265
    if-eqz v2, :cond_0

    .line 671266
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 671267
    new-instance v3, LX/46W;

    const/16 p0, 0x1611

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/46W;-><init>(LX/0Ot;)V

    .line 671268
    move-object v0, v3

    .line 671269
    sput-object v0, LX/46W;->a:LX/46W;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 671270
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 671271
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 671272
    :cond_1
    sget-object v0, LX/46W;->a:LX/46W;

    return-object v0

    .line 671273
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 671274
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final generateSeed(I)[B
    .locals 1

    .prologue
    .line 671261
    iget-object v0, p0, LX/46W;->mDelegate:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/security/SecureRandom;

    invoke-virtual {v0, p1}, Ljava/security/SecureRandom;->generateSeed(I)[B

    move-result-object v0

    return-object v0
.end method

.method public final getAlgorithm()Ljava/lang/String;
    .locals 1

    .prologue
    .line 671260
    iget-object v0, p0, LX/46W;->mDelegate:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/security/SecureRandom;

    invoke-virtual {v0}, Ljava/security/SecureRandom;->getAlgorithm()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 671259
    iget-object v0, p0, LX/46W;->mDelegate:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/security/SecureRandom;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public final nextBoolean()Z
    .locals 1

    .prologue
    .line 671258
    iget-object v0, p0, LX/46W;->mDelegate:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/security/SecureRandom;

    invoke-virtual {v0}, Ljava/security/SecureRandom;->nextBoolean()Z

    move-result v0

    return v0
.end method

.method public final declared-synchronized nextBytes([B)V
    .locals 1

    .prologue
    .line 671251
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/46W;->mDelegate:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/security/SecureRandom;

    invoke-virtual {v0, p1}, Ljava/security/SecureRandom;->nextBytes([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 671252
    monitor-exit p0

    return-void

    .line 671253
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final nextDouble()D
    .locals 2

    .prologue
    .line 671254
    iget-object v0, p0, LX/46W;->mDelegate:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/security/SecureRandom;

    invoke-virtual {v0}, Ljava/security/SecureRandom;->nextDouble()D

    move-result-wide v0

    return-wide v0
.end method

.method public final nextFloat()F
    .locals 1

    .prologue
    .line 671275
    iget-object v0, p0, LX/46W;->mDelegate:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/security/SecureRandom;

    invoke-virtual {v0}, Ljava/security/SecureRandom;->nextFloat()F

    move-result v0

    return v0
.end method

.method public final declared-synchronized nextGaussian()D
    .locals 2

    .prologue
    .line 671240
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/46W;->mDelegate:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/security/SecureRandom;

    invoke-virtual {v0}, Ljava/security/SecureRandom;->nextGaussian()D
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final nextInt()I
    .locals 1

    .prologue
    .line 671241
    iget-object v0, p0, LX/46W;->mDelegate:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/security/SecureRandom;

    invoke-virtual {v0}, Ljava/security/SecureRandom;->nextInt()I

    move-result v0

    return v0
.end method

.method public final nextInt(I)I
    .locals 1

    .prologue
    .line 671242
    iget-object v0, p0, LX/46W;->mDelegate:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/security/SecureRandom;

    invoke-virtual {v0, p1}, Ljava/security/SecureRandom;->nextInt(I)I

    move-result v0

    return v0
.end method

.method public final nextLong()J
    .locals 2

    .prologue
    .line 671243
    iget-object v0, p0, LX/46W;->mDelegate:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/security/SecureRandom;

    invoke-virtual {v0}, Ljava/security/SecureRandom;->nextLong()J

    move-result-wide v0

    return-wide v0
.end method

.method public final setSeed(J)V
    .locals 3

    .prologue
    .line 671244
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    .line 671245
    :goto_0
    return-void

    .line 671246
    :cond_0
    iget-object v0, p0, LX/46W;->mDelegate:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/security/SecureRandom;

    invoke-virtual {v0, p1, p2}, Ljava/security/SecureRandom;->setSeed(J)V

    goto :goto_0
.end method

.method public final declared-synchronized setSeed([B)V
    .locals 1

    .prologue
    .line 671247
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/46W;->mDelegate:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/security/SecureRandom;

    invoke-virtual {v0, p1}, Ljava/security/SecureRandom;->setSeed([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 671248
    monitor-exit p0

    return-void

    .line 671249
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 671250
    iget-object v0, p0, LX/46W;->mDelegate:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/security/SecureRandom;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
