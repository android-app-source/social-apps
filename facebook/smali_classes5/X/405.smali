.class public final LX/405;
.super Landroid/os/Handler;
.source ""


# instance fields
.field public final synthetic a:LX/406;


# direct methods
.method public constructor <init>(LX/406;Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 662869
    iput-object p1, p0, LX/405;->a:LX/406;

    .line 662870
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 662871
    return-void
.end method


# virtual methods
.method public final a(Ljava/io/ByteArrayOutputStream;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 662872
    invoke-virtual {p0, v0, p1}, LX/405;->removeMessages(ILjava/lang/Object;)V

    .line 662873
    invoke-virtual {p0, v0, p1}, LX/405;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/405;->sendMessage(Landroid/os/Message;)Z

    .line 662874
    return-void
.end method

.method public final handleMessage(Landroid/os/Message;)V
    .locals 9

    .prologue
    .line 662875
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 662876
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown what="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 662877
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/io/ByteArrayOutputStream;

    .line 662878
    new-instance v6, LX/40A;

    iget-object v3, p0, LX/405;->a:LX/406;

    iget-object v3, v3, LX/406;->f:LX/2Y2;

    invoke-direct {v6, v0, v3}, LX/40A;-><init>(Ljava/io/ByteArrayOutputStream;LX/2Y2;)V

    .line 662879
    new-instance v3, LX/2YF;

    iget-object v4, p0, LX/405;->a:LX/406;

    iget-object v4, v4, LX/406;->i:Lcom/facebook/analytics2/logger/Uploader;

    iget-object v5, p0, LX/405;->a:LX/406;

    iget-object v5, v5, LX/406;->c:LX/0pC;

    iget-object v5, v5, LX/0pC;->d:LX/0pD;

    invoke-static {v6}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    new-instance v7, LX/404;

    invoke-direct {v7}, LX/404;-><init>()V

    iget-object v8, p0, LX/405;->a:LX/406;

    iget-object v8, v8, LX/406;->d:Lcom/facebook/flexiblesampling/SamplingPolicyConfig;

    invoke-direct/range {v3 .. v8}, LX/2YF;-><init>(Lcom/facebook/analytics2/logger/Uploader;LX/0pD;Ljava/util/Iterator;LX/2WY;Lcom/facebook/flexiblesampling/SamplingPolicyConfig;)V

    .line 662880
    :goto_0
    invoke-virtual {v3}, LX/2YF;->a()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 662881
    invoke-virtual {v3}, LX/2YF;->b()V

    goto :goto_0

    .line 662882
    :cond_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
