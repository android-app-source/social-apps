.class public final LX/4yP;
.super LX/4yO;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/4yO",
        "<TK;>;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:LX/18f;


# direct methods
.method public constructor <init>(LX/18f;)V
    .locals 0

    .prologue
    .line 821755
    iput-object p1, p0, LX/4yP;->this$0:LX/18f;

    invoke-direct {p0}, LX/4yO;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 821756
    iget-object v0, p0, LX/4yP;->this$0:LX/18f;

    iget-object v0, v0, LX/18f;->b:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 821757
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    goto :goto_0
.end method

.method public final a(I)LX/4wx;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/google/common/collect/Multiset$Entry",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 821758
    iget-object v0, p0, LX/4yP;->this$0:LX/18f;

    iget-object v0, v0, LX/18f;->b:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Py;->asList()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 821759
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-static {v1, v0}, LX/2Tc;->a(Ljava/lang/Object;I)LX/4wx;

    move-result-object v0

    return-object v0
.end method

.method public final contains(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 821760
    iget-object v0, p0, LX/4yP;->this$0:LX/18f;

    invoke-virtual {v0, p1}, LX/18f;->f(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final d()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 821761
    iget-object v0, p0, LX/4yP;->this$0:LX/18f;

    invoke-virtual {v0}, LX/18f;->e()LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final isPartialView()Z
    .locals 1

    .prologue
    .line 821762
    const/4 v0, 0x1

    return v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 821763
    iget-object v0, p0, LX/4yP;->this$0:LX/18f;

    invoke-virtual {v0}, LX/18f;->f()I

    move-result v0

    return v0
.end method
