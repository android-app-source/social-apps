.class public LX/3p2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3ou;


# instance fields
.field private a:Landroid/animation/TimeInterpolator;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 641094
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 641095
    return-void
.end method


# virtual methods
.method public final a()LX/3ow;
    .locals 2

    .prologue
    .line 641100
    new-instance v0, LX/3p1;

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v1

    invoke-direct {v0, v1}, LX/3p1;-><init>(Landroid/animation/Animator;)V

    return-object v0

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public final a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 641096
    iget-object v0, p0, LX/3p2;->a:Landroid/animation/TimeInterpolator;

    if-nez v0, :cond_0

    .line 641097
    new-instance v0, Landroid/animation/ValueAnimator;

    invoke-direct {v0}, Landroid/animation/ValueAnimator;-><init>()V

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->getInterpolator()Landroid/animation/TimeInterpolator;

    move-result-object v0

    iput-object v0, p0, LX/3p2;->a:Landroid/animation/TimeInterpolator;

    .line 641098
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, LX/3p2;->a:Landroid/animation/TimeInterpolator;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    .line 641099
    return-void
.end method
