.class public LX/4Ms;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 689487
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 21

    .prologue
    .line 689488
    const/4 v15, 0x0

    .line 689489
    const/4 v14, 0x0

    .line 689490
    const/4 v13, 0x0

    .line 689491
    const/4 v12, 0x0

    .line 689492
    const/4 v11, 0x0

    .line 689493
    const/4 v10, 0x0

    .line 689494
    const-wide/16 v8, 0x0

    .line 689495
    const/4 v7, 0x0

    .line 689496
    const/4 v6, 0x0

    .line 689497
    const/4 v5, 0x0

    .line 689498
    const/4 v4, 0x0

    .line 689499
    const/4 v3, 0x0

    .line 689500
    const/4 v2, 0x0

    .line 689501
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v16

    sget-object v17, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    if-eq v0, v1, :cond_f

    .line 689502
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 689503
    const/4 v2, 0x0

    .line 689504
    :goto_0
    return v2

    .line 689505
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v17, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v17

    if-eq v3, v0, :cond_d

    .line 689506
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 689507
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 689508
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v17

    sget-object v18, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    if-eq v0, v1, :cond_0

    if-eqz v3, :cond_0

    .line 689509
    const-string v17, "client_generated_text_info"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_1

    .line 689510
    invoke-static/range {p0 .. p1}, LX/4Mn;->a(LX/15w;LX/186;)I

    move-result v3

    move/from16 v16, v3

    goto :goto_1

    .line 689511
    :cond_1
    const-string v17, "custom_font"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_2

    .line 689512
    invoke-static/range {p0 .. p1}, LX/4PF;->a(LX/15w;LX/186;)I

    move-result v3

    move v15, v3

    goto :goto_1

    .line 689513
    :cond_2
    const-string v17, "font_name"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_3

    .line 689514
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v14, v3

    goto :goto_1

    .line 689515
    :cond_3
    const-string v17, "id"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_4

    .line 689516
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v13, v3

    goto :goto_1

    .line 689517
    :cond_4
    const-string v17, "landscape_anchoring"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_5

    .line 689518
    invoke-static/range {p0 .. p1}, LX/4Mm;->a(LX/15w;LX/186;)I

    move-result v3

    move v7, v3

    goto :goto_1

    .line 689519
    :cond_5
    const-string v17, "portrait_anchoring"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_6

    .line 689520
    invoke-static/range {p0 .. p1}, LX/4Mm;->a(LX/15w;LX/186;)I

    move-result v3

    move v6, v3

    goto/16 :goto_1

    .line 689521
    :cond_6
    const-string v17, "rotation_degree"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_7

    .line 689522
    const/4 v2, 0x1

    .line 689523
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v4

    goto/16 :goto_1

    .line 689524
    :cond_7
    const-string v17, "text_color"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_8

    .line 689525
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v12, v3

    goto/16 :goto_1

    .line 689526
    :cond_8
    const-string v17, "text_content"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_9

    .line 689527
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v11, v3

    goto/16 :goto_1

    .line 689528
    :cond_9
    const-string v17, "text_landscape_size"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_a

    .line 689529
    invoke-static/range {p0 .. p1}, LX/4Mt;->a(LX/15w;LX/186;)I

    move-result v3

    move v10, v3

    goto/16 :goto_1

    .line 689530
    :cond_a
    const-string v17, "text_portrait_size"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_b

    .line 689531
    invoke-static/range {p0 .. p1}, LX/4Mt;->a(LX/15w;LX/186;)I

    move-result v3

    move v9, v3

    goto/16 :goto_1

    .line 689532
    :cond_b
    const-string v17, "url"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 689533
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v8, v3

    goto/16 :goto_1

    .line 689534
    :cond_c
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 689535
    :cond_d
    const/16 v3, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 689536
    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 689537
    const/4 v3, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v15}, LX/186;->b(II)V

    .line 689538
    const/4 v3, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v14}, LX/186;->b(II)V

    .line 689539
    const/4 v3, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v13}, LX/186;->b(II)V

    .line 689540
    const/4 v3, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 689541
    const/4 v3, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 689542
    if-eqz v2, :cond_e

    .line 689543
    const/4 v3, 0x6

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 689544
    :cond_e
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 689545
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 689546
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 689547
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 689548
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 689549
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_f
    move/from16 v16, v15

    move v15, v14

    move v14, v13

    move v13, v12

    move v12, v7

    move v7, v11

    move v11, v6

    move v6, v10

    move v10, v5

    move-wide/from16 v19, v8

    move v9, v4

    move v8, v3

    move-wide/from16 v4, v19

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    const-wide/16 v2, 0x0

    .line 689550
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 689551
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 689552
    if-eqz v0, :cond_1

    .line 689553
    const-string v1, "client_generated_text_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 689554
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 689555
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v4

    if-ge v1, v4, :cond_0

    .line 689556
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v4

    invoke-static {p0, v4, p2}, LX/4Mn;->a(LX/15i;ILX/0nX;)V

    .line 689557
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 689558
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 689559
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 689560
    if-eqz v0, :cond_2

    .line 689561
    const-string v1, "custom_font"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 689562
    invoke-static {p0, v0, p2, p3}, LX/4PF;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 689563
    :cond_2
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 689564
    if-eqz v0, :cond_3

    .line 689565
    const-string v1, "font_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 689566
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 689567
    :cond_3
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 689568
    if-eqz v0, :cond_4

    .line 689569
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 689570
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 689571
    :cond_4
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 689572
    if-eqz v0, :cond_5

    .line 689573
    const-string v1, "landscape_anchoring"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 689574
    invoke-static {p0, v0, p2}, LX/4Mm;->a(LX/15i;ILX/0nX;)V

    .line 689575
    :cond_5
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 689576
    if-eqz v0, :cond_6

    .line 689577
    const-string v1, "portrait_anchoring"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 689578
    invoke-static {p0, v0, p2}, LX/4Mm;->a(LX/15i;ILX/0nX;)V

    .line 689579
    :cond_6
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 689580
    cmpl-double v2, v0, v2

    if-eqz v2, :cond_7

    .line 689581
    const-string v2, "rotation_degree"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 689582
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 689583
    :cond_7
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 689584
    if-eqz v0, :cond_8

    .line 689585
    const-string v1, "text_color"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 689586
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 689587
    :cond_8
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 689588
    if-eqz v0, :cond_9

    .line 689589
    const-string v1, "text_content"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 689590
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 689591
    :cond_9
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 689592
    if-eqz v0, :cond_a

    .line 689593
    const-string v1, "text_landscape_size"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 689594
    invoke-static {p0, v0, p2}, LX/4Mt;->a(LX/15i;ILX/0nX;)V

    .line 689595
    :cond_a
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 689596
    if-eqz v0, :cond_b

    .line 689597
    const-string v1, "text_portrait_size"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 689598
    invoke-static {p0, v0, p2}, LX/4Mt;->a(LX/15i;ILX/0nX;)V

    .line 689599
    :cond_b
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 689600
    if-eqz v0, :cond_c

    .line 689601
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 689602
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 689603
    :cond_c
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 689604
    return-void
.end method
