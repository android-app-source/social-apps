.class public LX/41w;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/41u;",
        "LX/41v;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 666602
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 666603
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 666604
    check-cast p1, LX/41u;

    .line 666605
    const/4 v0, 0x2

    invoke-static {v0}, LX/0R9;->a(I)Ljava/util/ArrayList;

    move-result-object v0

    .line 666606
    iget-object v1, p1, LX/41u;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 666607
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "target_user_id"

    iget-object v3, p1, LX/41u;->a:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 666608
    :cond_0
    iget-object v1, p1, LX/41u;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 666609
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "target_session_token"

    iget-object v3, p1, LX/41u;->b:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 666610
    :cond_1
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    sget-object v2, LX/11I;->MESSENGER_ONLY_MIGRATE_ACCOUNT:LX/11I;

    iget-object v2, v2, LX/11I;->requestNameString:Ljava/lang/String;

    .line 666611
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 666612
    move-object v1, v1

    .line 666613
    const-string v2, "POST"

    .line 666614
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 666615
    move-object v1, v1

    .line 666616
    const-string v2, "/me/messenger_only_account_migrations"

    .line 666617
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 666618
    move-object v1, v1

    .line 666619
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 666620
    move-object v0, v1

    .line 666621
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 666622
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 666623
    move-object v0, v0

    .line 666624
    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v1}, LX/14O;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/14O;

    move-result-object v0

    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 666625
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 666626
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 666627
    const-string v1, "logout_success"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->g(LX/0lF;)Z

    move-result v1

    .line 666628
    const-string v2, "migration_pending"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->g(LX/0lF;)Z

    move-result v0

    .line 666629
    new-instance v2, LX/41v;

    invoke-direct {v2, v1, v0}, LX/41v;-><init>(ZZ)V

    return-object v2
.end method
