.class public LX/4LZ;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 683893
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 38

    .prologue
    .line 683977
    const/16 v32, 0x0

    .line 683978
    const/16 v31, 0x0

    .line 683979
    const/16 v30, 0x0

    .line 683980
    const-wide/16 v28, 0x0

    .line 683981
    const/16 v27, 0x0

    .line 683982
    const/16 v26, 0x0

    .line 683983
    const/16 v25, 0x0

    .line 683984
    const/16 v24, 0x0

    .line 683985
    const/16 v23, 0x0

    .line 683986
    const/16 v22, 0x0

    .line 683987
    const/16 v21, 0x0

    .line 683988
    const/16 v20, 0x0

    .line 683989
    const/16 v19, 0x0

    .line 683990
    const/16 v18, 0x0

    .line 683991
    const/16 v17, 0x0

    .line 683992
    const/16 v16, 0x0

    .line 683993
    const/4 v13, 0x0

    .line 683994
    const-wide/16 v14, 0x0

    .line 683995
    const/4 v12, 0x0

    .line 683996
    const/4 v11, 0x0

    .line 683997
    const/4 v10, 0x0

    .line 683998
    const/4 v9, 0x0

    .line 683999
    const/4 v8, 0x0

    .line 684000
    const/4 v7, 0x0

    .line 684001
    const/4 v6, 0x0

    .line 684002
    const/4 v5, 0x0

    .line 684003
    const/4 v4, 0x0

    .line 684004
    const/4 v3, 0x0

    .line 684005
    const/4 v2, 0x0

    .line 684006
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v33

    sget-object v34, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v33

    move-object/from16 v1, v34

    if-eq v0, v1, :cond_1f

    .line 684007
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 684008
    const/4 v2, 0x0

    .line 684009
    :goto_0
    return v2

    .line 684010
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v34, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v34

    if-eq v2, v0, :cond_15

    .line 684011
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 684012
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 684013
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v34

    sget-object v35, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v34

    move-object/from16 v1, v35

    if-eq v0, v1, :cond_0

    if-eqz v2, :cond_0

    .line 684014
    const-string v34, "claim_count"

    move-object/from16 v0, v34

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_1

    .line 684015
    const/4 v2, 0x1

    .line 684016
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v7

    move/from16 v33, v7

    move v7, v2

    goto :goto_1

    .line 684017
    :cond_1
    const-string v34, "coupon_claim_location"

    move-object/from16 v0, v34

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_2

    .line 684018
    const/4 v2, 0x1

    .line 684019
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/graphql/enums/GraphQLCouponClaimLocation;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLCouponClaimLocation;

    move-result-object v6

    move-object/from16 v32, v6

    move v6, v2

    goto :goto_1

    .line 684020
    :cond_2
    const-string v34, "creation_story"

    move-object/from16 v0, v34

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_3

    .line 684021
    invoke-static/range {p0 .. p1}, LX/2aD;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v31, v2

    goto :goto_1

    .line 684022
    :cond_3
    const-string v34, "expiration_date"

    move-object/from16 v0, v34

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_4

    .line 684023
    const/4 v2, 0x1

    .line 684024
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 684025
    :cond_4
    const-string v34, "filtered_claim_count"

    move-object/from16 v0, v34

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_5

    .line 684026
    const/4 v2, 0x1

    .line 684027
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v13

    move/from16 v30, v13

    move v13, v2

    goto :goto_1

    .line 684028
    :cond_5
    const-string v34, "has_viewer_claimed"

    move-object/from16 v0, v34

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_6

    .line 684029
    const/4 v2, 0x1

    .line 684030
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v12

    move/from16 v29, v12

    move v12, v2

    goto/16 :goto_1

    .line 684031
    :cond_6
    const-string v34, "id"

    move-object/from16 v0, v34

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_7

    .line 684032
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v28, v2

    goto/16 :goto_1

    .line 684033
    :cond_7
    const-string v34, "is_active"

    move-object/from16 v0, v34

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_8

    .line 684034
    const/4 v2, 0x1

    .line 684035
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v11

    move/from16 v27, v11

    move v11, v2

    goto/16 :goto_1

    .line 684036
    :cond_8
    const-string v34, "is_expired"

    move-object/from16 v0, v34

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_9

    .line 684037
    const/4 v2, 0x1

    .line 684038
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v10

    move/from16 v26, v10

    move v10, v2

    goto/16 :goto_1

    .line 684039
    :cond_9
    const-string v34, "is_stopped"

    move-object/from16 v0, v34

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_a

    .line 684040
    const/4 v2, 0x1

    .line 684041
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v9

    move/from16 v25, v9

    move v9, v2

    goto/16 :goto_1

    .line 684042
    :cond_a
    const-string v34, "message"

    move-object/from16 v0, v34

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_b

    .line 684043
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v24, v2

    goto/16 :goto_1

    .line 684044
    :cond_b
    const-string v34, "mobile_post_claim_message"

    move-object/from16 v0, v34

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_c

    .line 684045
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v23, v2

    goto/16 :goto_1

    .line 684046
    :cond_c
    const-string v34, "name"

    move-object/from16 v0, v34

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_d

    .line 684047
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v22, v2

    goto/16 :goto_1

    .line 684048
    :cond_d
    const-string v34, "owning_page"

    move-object/from16 v0, v34

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_e

    .line 684049
    invoke-static/range {p0 .. p1}, LX/2bc;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v21, v2

    goto/16 :goto_1

    .line 684050
    :cond_e
    const-string v34, "photo"

    move-object/from16 v0, v34

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_f

    .line 684051
    invoke-static/range {p0 .. p1}, LX/2sY;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v20, v2

    goto/16 :goto_1

    .line 684052
    :cond_f
    const-string v34, "redemption_code"

    move-object/from16 v0, v34

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_10

    .line 684053
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v19, v2

    goto/16 :goto_1

    .line 684054
    :cond_10
    const-string v34, "redemption_url"

    move-object/from16 v0, v34

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_11

    .line 684055
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v18, v2

    goto/16 :goto_1

    .line 684056
    :cond_11
    const-string v34, "reminder_time"

    move-object/from16 v0, v34

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_12

    .line 684057
    const/4 v2, 0x1

    .line 684058
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v16

    move v8, v2

    goto/16 :goto_1

    .line 684059
    :cond_12
    const-string v34, "terms"

    move-object/from16 v0, v34

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_13

    .line 684060
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v15, v2

    goto/16 :goto_1

    .line 684061
    :cond_13
    const-string v34, "url"

    move-object/from16 v0, v34

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_14

    .line 684062
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v14, v2

    goto/16 :goto_1

    .line 684063
    :cond_14
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 684064
    :cond_15
    const/16 v2, 0x15

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 684065
    if-eqz v7, :cond_16

    .line 684066
    const/4 v2, 0x1

    const/4 v7, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v2, v1, v7}, LX/186;->a(III)V

    .line 684067
    :cond_16
    if-eqz v6, :cond_17

    .line 684068
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move-object/from16 v1, v32

    invoke-virtual {v0, v2, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 684069
    :cond_17
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 684070
    if-eqz v3, :cond_18

    .line 684071
    const/4 v3, 0x4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 684072
    :cond_18
    if-eqz v13, :cond_19

    .line 684073
    const/4 v2, 0x5

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 684074
    :cond_19
    if-eqz v12, :cond_1a

    .line 684075
    const/4 v2, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 684076
    :cond_1a
    const/4 v2, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 684077
    if-eqz v11, :cond_1b

    .line 684078
    const/16 v2, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 684079
    :cond_1b
    if-eqz v10, :cond_1c

    .line 684080
    const/16 v2, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 684081
    :cond_1c
    if-eqz v9, :cond_1d

    .line 684082
    const/16 v2, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 684083
    :cond_1d
    const/16 v2, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 684084
    const/16 v2, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 684085
    const/16 v2, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 684086
    const/16 v2, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 684087
    const/16 v2, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 684088
    const/16 v2, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 684089
    const/16 v2, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 684090
    if-eqz v8, :cond_1e

    .line 684091
    const/16 v3, 0x12

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v16

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 684092
    :cond_1e
    const/16 v2, 0x13

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 684093
    const/16 v2, 0x14

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 684094
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_1f
    move/from16 v33, v32

    move-object/from16 v32, v31

    move/from16 v31, v30

    move/from16 v30, v27

    move/from16 v27, v24

    move/from16 v24, v21

    move/from16 v21, v18

    move/from16 v18, v13

    move v13, v7

    move v7, v10

    move v10, v4

    move-wide/from16 v36, v14

    move v14, v11

    move v15, v12

    move v12, v6

    move v11, v5

    move-wide/from16 v4, v28

    move v6, v9

    move/from16 v28, v25

    move/from16 v29, v26

    move v9, v3

    move/from16 v26, v23

    move v3, v8

    move/from16 v25, v22

    move/from16 v22, v19

    move/from16 v23, v20

    move v8, v2

    move/from16 v20, v17

    move/from16 v19, v16

    move-wide/from16 v16, v36

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const/4 v2, 0x2

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    .line 683894
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 683895
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 683896
    if-eqz v0, :cond_0

    .line 683897
    const-string v1, "claim_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 683898
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 683899
    :cond_0
    invoke-virtual {p0, p1, v2, v3}, LX/15i;->a(IIS)S

    move-result v0

    .line 683900
    if-eqz v0, :cond_1

    .line 683901
    const-string v0, "coupon_claim_location"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 683902
    const-class v0, Lcom/facebook/graphql/enums/GraphQLCouponClaimLocation;

    invoke-virtual {p0, p1, v2, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLCouponClaimLocation;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLCouponClaimLocation;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 683903
    :cond_1
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 683904
    if-eqz v0, :cond_2

    .line 683905
    const-string v1, "creation_story"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 683906
    invoke-static {p0, v0, p2, p3}, LX/2aD;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 683907
    :cond_2
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 683908
    cmp-long v2, v0, v4

    if-eqz v2, :cond_3

    .line 683909
    const-string v2, "expiration_date"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 683910
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 683911
    :cond_3
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 683912
    if-eqz v0, :cond_4

    .line 683913
    const-string v1, "filtered_claim_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 683914
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 683915
    :cond_4
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 683916
    if-eqz v0, :cond_5

    .line 683917
    const-string v1, "has_viewer_claimed"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 683918
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 683919
    :cond_5
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 683920
    if-eqz v0, :cond_6

    .line 683921
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 683922
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 683923
    :cond_6
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 683924
    if-eqz v0, :cond_7

    .line 683925
    const-string v1, "is_active"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 683926
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 683927
    :cond_7
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 683928
    if-eqz v0, :cond_8

    .line 683929
    const-string v1, "is_expired"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 683930
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 683931
    :cond_8
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 683932
    if-eqz v0, :cond_9

    .line 683933
    const-string v1, "is_stopped"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 683934
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 683935
    :cond_9
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 683936
    if-eqz v0, :cond_a

    .line 683937
    const-string v1, "message"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 683938
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 683939
    :cond_a
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 683940
    if-eqz v0, :cond_b

    .line 683941
    const-string v1, "mobile_post_claim_message"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 683942
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 683943
    :cond_b
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 683944
    if-eqz v0, :cond_c

    .line 683945
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 683946
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 683947
    :cond_c
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 683948
    if-eqz v0, :cond_d

    .line 683949
    const-string v1, "owning_page"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 683950
    invoke-static {p0, v0, p2, p3}, LX/2bc;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 683951
    :cond_d
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 683952
    if-eqz v0, :cond_e

    .line 683953
    const-string v1, "photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 683954
    invoke-static {p0, v0, p2, p3}, LX/2sY;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 683955
    :cond_e
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 683956
    if-eqz v0, :cond_f

    .line 683957
    const-string v1, "redemption_code"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 683958
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 683959
    :cond_f
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 683960
    if-eqz v0, :cond_10

    .line 683961
    const-string v1, "redemption_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 683962
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 683963
    :cond_10
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 683964
    cmp-long v2, v0, v4

    if-eqz v2, :cond_11

    .line 683965
    const-string v2, "reminder_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 683966
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 683967
    :cond_11
    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 683968
    if-eqz v0, :cond_12

    .line 683969
    const-string v1, "terms"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 683970
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 683971
    :cond_12
    const/16 v0, 0x14

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 683972
    if-eqz v0, :cond_13

    .line 683973
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 683974
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 683975
    :cond_13
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 683976
    return-void
.end method
