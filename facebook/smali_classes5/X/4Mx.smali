.class public LX/4Mx;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 689837
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 689838
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_7

    .line 689839
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 689840
    :goto_0
    return v1

    .line 689841
    :cond_0
    const-string v8, "total_possibility_count"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 689842
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v3

    move v5, v3

    move v3, v2

    .line 689843
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_4

    .line 689844
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 689845
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 689846
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_1

    if-eqz v7, :cond_1

    .line 689847
    const-string v8, "page_info"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 689848
    invoke-static {p0, p1}, LX/264;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 689849
    :cond_2
    const-string v8, "unseen_count"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 689850
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v4, v0

    move v0, v2

    goto :goto_1

    .line 689851
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 689852
    :cond_4
    const/4 v7, 0x3

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 689853
    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 689854
    if-eqz v3, :cond_5

    .line 689855
    invoke-virtual {p1, v2, v5, v1}, LX/186;->a(III)V

    .line 689856
    :cond_5
    if-eqz v0, :cond_6

    .line 689857
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v4, v1}, LX/186;->a(III)V

    .line 689858
    :cond_6
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_7
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 689859
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 689860
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 689861
    if-eqz v0, :cond_0

    .line 689862
    const-string v1, "page_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 689863
    invoke-static {p0, v0, p2}, LX/264;->a(LX/15i;ILX/0nX;)V

    .line 689864
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 689865
    if-eqz v0, :cond_1

    .line 689866
    const-string v1, "total_possibility_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 689867
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 689868
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 689869
    if-eqz v0, :cond_2

    .line 689870
    const-string v1, "unseen_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 689871
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 689872
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 689873
    return-void
.end method
