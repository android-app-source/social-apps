.class public LX/4f5;
.super LX/4ep;
.source ""


# instance fields
.field private final a:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;LX/1Fj;Landroid/content/res/Resources;Z)V
    .locals 0

    .prologue
    .line 797907
    invoke-direct {p0, p1, p2, p4}, LX/4ep;-><init>(Ljava/util/concurrent/Executor;LX/1Fj;Z)V

    .line 797908
    iput-object p3, p0, LX/4f5;->a:Landroid/content/res/Resources;

    .line 797909
    return-void
.end method

.method private b(LX/1bf;)I
    .locals 5

    .prologue
    .line 797914
    const/4 v0, 0x0

    .line 797915
    :try_start_0
    iget-object v1, p0, LX/4f5;->a:Landroid/content/res/Resources;

    invoke-static {p1}, LX/4f5;->c(LX/1bf;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->openRawResourceFd(I)Landroid/content/res/AssetFileDescriptor;
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 797916
    :try_start_1
    invoke-virtual {v1}, Landroid/content/res/AssetFileDescriptor;->getLength()J
    :try_end_1
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-wide v2

    long-to-int v0, v2

    .line 797917
    if-eqz v1, :cond_0

    .line 797918
    :try_start_2
    invoke-virtual {v1}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 797919
    :cond_0
    :goto_0
    return v0

    .line 797920
    :catch_0
    :goto_1
    if-eqz v0, :cond_1

    .line 797921
    :try_start_3
    invoke-virtual {v0}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 797922
    :cond_1
    :goto_2
    const/4 v0, -0x1

    goto :goto_0

    .line 797923
    :catchall_0
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    .line 797924
    :goto_3
    if-eqz v1, :cond_2

    .line 797925
    :try_start_4
    invoke-virtual {v1}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 797926
    :cond_2
    :goto_4
    throw v0

    :catch_1
    goto :goto_0

    :catch_2
    goto :goto_2

    :catch_3
    goto :goto_4

    .line 797927
    :catchall_1
    move-exception v0

    goto :goto_3

    :catch_4
    move-object v0, v1

    goto :goto_1
.end method

.method private static c(LX/1bf;)I
    .locals 2

    .prologue
    .line 797912
    iget-object v0, p0, LX/1bf;->b:Landroid/net/Uri;

    move-object v0, v0

    .line 797913
    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(LX/1bf;)LX/1FL;
    .locals 2

    .prologue
    .line 797911
    iget-object v0, p0, LX/4f5;->a:Landroid/content/res/Resources;

    invoke-static {p1}, LX/4f5;->c(LX/1bf;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v0

    invoke-direct {p0, p1}, LX/4f5;->b(LX/1bf;)I

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/4ep;->b(Ljava/io/InputStream;I)LX/1FL;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 797910
    const-string v0, "LocalResourceFetchProducer"

    return-object v0
.end method
