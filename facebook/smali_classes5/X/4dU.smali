.class public LX/4dU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4dG;


# instance fields
.field private final a:LX/1bw;

.field private final b:LX/4dO;

.field private final c:LX/1GB;

.field private final d:Landroid/graphics/Rect;

.field private final e:[I

.field private final f:[I

.field private final g:I

.field private final h:[LX/4dL;

.field private i:Landroid/graphics/Bitmap;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1bw;LX/4dO;Landroid/graphics/Rect;)V
    .locals 3

    .prologue
    .line 795873
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 795874
    iput-object p1, p0, LX/4dU;->a:LX/1bw;

    .line 795875
    iput-object p2, p0, LX/4dU;->b:LX/4dO;

    .line 795876
    iget-object v0, p2, LX/4dO;->a:LX/1GB;

    move-object v0, v0

    .line 795877
    iput-object v0, p0, LX/4dU;->c:LX/1GB;

    .line 795878
    iget-object v0, p0, LX/4dU;->c:LX/1GB;

    invoke-interface {v0}, LX/1GB;->d()[I

    move-result-object v0

    iput-object v0, p0, LX/4dU;->e:[I

    .line 795879
    iget-object v0, p0, LX/4dU;->e:[I

    .line 795880
    const/4 v1, 0x0

    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_1

    .line 795881
    aget v2, v0, v1

    const/16 p1, 0xb

    if-ge v2, p1, :cond_0

    .line 795882
    const/16 v2, 0x64

    aput v2, v0, v1

    .line 795883
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 795884
    :cond_1
    iget-object v0, p0, LX/4dU;->e:[I

    const/4 v1, 0x0

    .line 795885
    move v2, v1

    .line 795886
    :goto_1
    array-length p1, v0

    if-ge v1, p1, :cond_2

    .line 795887
    aget p1, v0, v1

    add-int/2addr v2, p1

    .line 795888
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 795889
    :cond_2
    move v0, v2

    .line 795890
    iput v0, p0, LX/4dU;->g:I

    .line 795891
    iget-object v0, p0, LX/4dU;->e:[I

    const/4 v1, 0x0

    .line 795892
    array-length v2, v0

    new-array p1, v2, [I

    move v2, v1

    .line 795893
    :goto_2
    array-length p2, v0

    if-ge v1, p2, :cond_3

    .line 795894
    aput v2, p1, v1

    .line 795895
    aget p2, v0, v1

    add-int/2addr v2, p2

    .line 795896
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 795897
    :cond_3
    move-object v0, p1

    .line 795898
    iput-object v0, p0, LX/4dU;->f:[I

    .line 795899
    iget-object v0, p0, LX/4dU;->c:LX/1GB;

    invoke-static {v0, p3}, LX/4dU;->a(LX/1GB;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v0

    iput-object v0, p0, LX/4dU;->d:Landroid/graphics/Rect;

    .line 795900
    iget-object v0, p0, LX/4dU;->c:LX/1GB;

    invoke-interface {v0}, LX/1GB;->c()I

    move-result v0

    new-array v0, v0, [LX/4dL;

    iput-object v0, p0, LX/4dU;->h:[LX/4dL;

    .line 795901
    const/4 v0, 0x0

    :goto_3
    iget-object v1, p0, LX/4dU;->c:LX/1GB;

    invoke-interface {v1}, LX/1GB;->c()I

    move-result v1

    if-ge v0, v1, :cond_4

    .line 795902
    iget-object v1, p0, LX/4dU;->h:[LX/4dL;

    iget-object v2, p0, LX/4dU;->c:LX/1GB;

    invoke-interface {v2, v0}, LX/1GB;->b(I)LX/4dL;

    move-result-object v2

    aput-object v2, v1, v0

    .line 795903
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 795904
    :cond_4
    return-void
.end method

.method private static a(LX/1GB;Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 795905
    if-nez p1, :cond_0

    .line 795906
    new-instance v0, Landroid/graphics/Rect;

    invoke-interface {p0}, LX/1GB;->a()I

    move-result v1

    invoke-interface {p0}, LX/1GB;->b()I

    move-result v2

    invoke-direct {v0, v4, v4, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 795907
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v1

    invoke-interface {p0}, LX/1GB;->a()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v2

    invoke-interface {p0}, LX/1GB;->b()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-direct {v0, v4, v4, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    goto :goto_0
.end method

.method private a(Landroid/graphics/Canvas;LX/40I;)V
    .locals 8

    .prologue
    .line 795858
    iget-object v0, p0, LX/4dU;->d:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-double v0, v0

    iget-object v2, p0, LX/4dU;->c:LX/1GB;

    invoke-interface {v2}, LX/1GB;->a()I

    move-result v2

    int-to-double v2, v2

    div-double/2addr v0, v2

    .line 795859
    iget-object v2, p0, LX/4dU;->d:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-double v2, v2

    iget-object v4, p0, LX/4dU;->c:LX/1GB;

    invoke-interface {v4}, LX/1GB;->b()I

    move-result v4

    int-to-double v4, v4

    div-double/2addr v2, v4

    .line 795860
    invoke-interface {p2}, LX/40I;->b()I

    move-result v4

    int-to-double v4, v4

    mul-double/2addr v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->round(D)J

    move-result-wide v4

    long-to-int v4, v4

    .line 795861
    invoke-interface {p2}, LX/40I;->c()I

    move-result v5

    int-to-double v6, v5

    mul-double/2addr v6, v2

    invoke-static {v6, v7}, Ljava/lang/Math;->round(D)J

    move-result-wide v6

    long-to-int v5, v6

    .line 795862
    invoke-interface {p2}, LX/40I;->d()I

    move-result v6

    int-to-double v6, v6

    mul-double/2addr v0, v6

    double-to-int v0, v0

    .line 795863
    invoke-interface {p2}, LX/40I;->e()I

    move-result v1

    int-to-double v6, v1

    mul-double/2addr v2, v6

    double-to-int v1, v2

    .line 795864
    monitor-enter p0

    .line 795865
    :try_start_0
    iget-object v2, p0, LX/4dU;->i:Landroid/graphics/Bitmap;

    if-nez v2, :cond_0

    .line 795866
    iget-object v2, p0, LX/4dU;->d:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    iget-object v3, p0, LX/4dU;->d:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, LX/4dU;->i:Landroid/graphics/Bitmap;

    .line 795867
    :cond_0
    iget-object v2, p0, LX/4dU;->i:Landroid/graphics/Bitmap;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 795868
    iget-object v2, p0, LX/4dU;->i:Landroid/graphics/Bitmap;

    invoke-interface {p2, v4, v5, v2}, LX/40I;->a(IILandroid/graphics/Bitmap;)V

    .line 795869
    iget-object v2, p0, LX/4dU;->i:Landroid/graphics/Bitmap;

    int-to-float v0, v0

    int-to-float v1, v1

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v0, v1, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 795870
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private b(Landroid/graphics/Canvas;LX/40I;)V
    .locals 7

    .prologue
    .line 795908
    invoke-interface {p2}, LX/40I;->b()I

    move-result v0

    .line 795909
    invoke-interface {p2}, LX/40I;->c()I

    move-result v1

    .line 795910
    invoke-interface {p2}, LX/40I;->d()I

    move-result v2

    .line 795911
    invoke-interface {p2}, LX/40I;->e()I

    move-result v3

    .line 795912
    monitor-enter p0

    .line 795913
    :try_start_0
    iget-object v4, p0, LX/4dU;->i:Landroid/graphics/Bitmap;

    if-nez v4, :cond_0

    .line 795914
    iget-object v4, p0, LX/4dU;->c:LX/1GB;

    invoke-interface {v4}, LX/1GB;->a()I

    move-result v4

    iget-object v5, p0, LX/4dU;->c:LX/1GB;

    invoke-interface {v5}, LX/1GB;->b()I

    move-result v5

    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v4, v5, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    iput-object v4, p0, LX/4dU;->i:Landroid/graphics/Bitmap;

    .line 795915
    :cond_0
    iget-object v4, p0, LX/4dU;->i:Landroid/graphics/Bitmap;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 795916
    iget-object v4, p0, LX/4dU;->i:Landroid/graphics/Bitmap;

    invoke-interface {p2, v0, v1, v4}, LX/40I;->a(IILandroid/graphics/Bitmap;)V

    .line 795917
    iget-object v0, p0, LX/4dU;->d:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, LX/4dU;->c:LX/1GB;

    invoke-interface {v1}, LX/1GB;->a()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 795918
    iget-object v1, p0, LX/4dU;->d:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    int-to-float v1, v1

    iget-object v4, p0, LX/4dU;->c:LX/1GB;

    invoke-interface {v4}, LX/1GB;->b()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v1, v4

    .line 795919
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 795920
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->scale(FF)V

    .line 795921
    int-to-float v0, v2

    int-to-float v1, v3

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 795922
    iget-object v0, p0, LX/4dU;->i:Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 795923
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 795924
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a(Landroid/graphics/Rect;)LX/4dG;
    .locals 3

    .prologue
    .line 795925
    iget-object v0, p0, LX/4dU;->c:LX/1GB;

    invoke-static {v0, p1}, LX/4dU;->a(LX/1GB;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v0

    .line 795926
    iget-object v1, p0, LX/4dU;->d:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 795927
    :goto_0
    return-object p0

    :cond_0
    new-instance v0, LX/4dU;

    iget-object v1, p0, LX/4dU;->a:LX/1bw;

    iget-object v2, p0, LX/4dU;->b:LX/4dO;

    invoke-direct {v0, v1, v2, p1}, LX/4dU;-><init>(LX/1bw;LX/4dO;Landroid/graphics/Rect;)V

    move-object p0, v0

    goto :goto_0
.end method

.method public final a(I)LX/4dL;
    .locals 1

    .prologue
    .line 795928
    iget-object v0, p0, LX/4dU;->h:[LX/4dL;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public final a()LX/4dO;
    .locals 1

    .prologue
    .line 795929
    iget-object v0, p0, LX/4dU;->b:LX/4dO;

    return-object v0
.end method

.method public final a(ILandroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 795930
    iget-object v0, p0, LX/4dU;->c:LX/1GB;

    invoke-interface {v0, p1}, LX/1GB;->a(I)LX/40I;

    move-result-object v1

    .line 795931
    :try_start_0
    iget-object v0, p0, LX/4dU;->c:LX/1GB;

    invoke-interface {v0}, LX/1GB;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 795932
    invoke-direct {p0, p2, v1}, LX/4dU;->a(Landroid/graphics/Canvas;LX/40I;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 795933
    :goto_0
    invoke-interface {v1}, LX/40I;->a()V

    .line 795934
    return-void

    .line 795935
    :cond_0
    :try_start_1
    invoke-direct {p0, p2, v1}, LX/4dU;->b(Landroid/graphics/Canvas;LX/40I;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 795936
    :catchall_0
    move-exception v0

    invoke-interface {v1}, LX/40I;->a()V

    throw v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 795937
    iget v0, p0, LX/4dU;->g:I

    return v0
.end method

.method public final b(I)I
    .locals 1

    .prologue
    .line 795938
    iget-object v0, p0, LX/4dU;->f:[I

    .line 795939
    invoke-static {v0, p1}, Ljava/util/Arrays;->binarySearch([II)I

    move-result p0

    .line 795940
    if-gez p0, :cond_0

    .line 795941
    neg-int p0, p0

    add-int/lit8 p0, p0, -0x1

    add-int/lit8 p0, p0, -0x1

    .line 795942
    :cond_0
    move v0, p0

    .line 795943
    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 795944
    iget-object v0, p0, LX/4dU;->c:LX/1GB;

    invoke-interface {v0}, LX/1GB;->c()I

    move-result v0

    return v0
.end method

.method public final c(I)I
    .locals 1

    .prologue
    .line 795871
    iget-object v0, p0, LX/4dU;->f:[I

    array-length v0, v0

    invoke-static {p1, v0}, LX/03g;->a(II)I

    .line 795872
    iget-object v0, p0, LX/4dU;->f:[I

    aget v0, v0, p1

    return v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 795857
    iget-object v0, p0, LX/4dU;->c:LX/1GB;

    invoke-interface {v0}, LX/1GB;->e()I

    move-result v0

    return v0
.end method

.method public final d(I)I
    .locals 1

    .prologue
    .line 795856
    iget-object v0, p0, LX/4dU;->e:[I

    aget v0, v0, p1

    return v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 795855
    iget-object v0, p0, LX/4dU;->c:LX/1GB;

    invoke-interface {v0}, LX/1GB;->a()I

    move-result v0

    return v0
.end method

.method public final e(I)LX/1FJ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 795854
    iget-object v0, p0, LX/4dU;->b:LX/4dO;

    invoke-virtual {v0, p1}, LX/4dO;->a(I)LX/1FJ;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 795853
    iget-object v0, p0, LX/4dU;->c:LX/1GB;

    invoke-interface {v0}, LX/1GB;->b()I

    move-result v0

    return v0
.end method

.method public final f(I)Z
    .locals 1

    .prologue
    .line 795852
    iget-object v0, p0, LX/4dU;->b:LX/4dO;

    invoke-virtual {v0, p1}, LX/4dO;->b(I)Z

    move-result v0

    return v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 795851
    iget-object v0, p0, LX/4dU;->d:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    return v0
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 795850
    iget-object v0, p0, LX/4dU;->d:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    return v0
.end method

.method public final i()I
    .locals 1

    .prologue
    .line 795847
    iget-object v0, p0, LX/4dU;->b:LX/4dO;

    .line 795848
    iget p0, v0, LX/4dO;->b:I

    move v0, p0

    .line 795849
    return v0
.end method

.method public final declared-synchronized j()I
    .locals 2

    .prologue
    .line 795836
    monitor-enter p0

    const/4 v0, 0x0

    .line 795837
    :try_start_0
    iget-object v1, p0, LX/4dU;->i:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    .line 795838
    iget-object v0, p0, LX/4dU;->i:Landroid/graphics/Bitmap;

    invoke-static {v0}, LX/1bw;->a(Landroid/graphics/Bitmap;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 795839
    :cond_0
    iget-object v1, p0, LX/4dU;->c:LX/1GB;

    invoke-interface {v1}, LX/1GB;->g()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    add-int/2addr v0, v1

    .line 795840
    monitor-exit p0

    return v0

    .line 795841
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized k()V
    .locals 1

    .prologue
    .line 795842
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/4dU;->i:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 795843
    iget-object v0, p0, LX/4dU;->i:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 795844
    const/4 v0, 0x0

    iput-object v0, p0, LX/4dU;->i:Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 795845
    :cond_0
    monitor-exit p0

    return-void

    .line 795846
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
