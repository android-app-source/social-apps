.class public LX/4eM;
.super LX/1FR;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1FR",
        "<",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# direct methods
.method public constructor <init>(LX/0rb;LX/1F7;LX/1F0;)V
    .locals 0

    .prologue
    .line 797217
    invoke-direct {p0, p1, p2, p3}, LX/1FR;-><init>(LX/0rb;LX/1F7;LX/1F0;)V

    .line 797218
    invoke-virtual {p0}, LX/1FR;->a()V

    .line 797219
    return-void
.end method


# virtual methods
.method public final b(I)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 797220
    const/4 v0, 0x1

    int-to-double v2, p1

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    div-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v1, v2

    sget-object v2, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 797212
    check-cast p1, Landroid/graphics/Bitmap;

    .line 797213
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 797214
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    .line 797215
    return-void
.end method

.method public final c(I)I
    .locals 0

    .prologue
    .line 797216
    return p1
.end method

.method public final c(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 797209
    check-cast p1, Landroid/graphics/Bitmap;

    .line 797210
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 797211
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getAllocationByteCount()I

    move-result v0

    return v0
.end method

.method public final d(I)I
    .locals 0

    .prologue
    .line 797208
    return p1
.end method

.method public final d(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 797205
    check-cast p1, Landroid/graphics/Bitmap;

    .line 797206
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 797207
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isMutable()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
