.class public final LX/4by;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4bv;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/4by;


# instance fields
.field private final a:Lorg/apache/http/client/HttpClient;

.field public final b:LX/4bu;


# direct methods
.method public constructor <init>(Lorg/apache/http/conn/ClientConnectionManager;Lorg/apache/http/params/HttpParams;LX/4bu;LX/4c0;LX/1Gl;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 794357
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 794358
    iput-object p3, p0, LX/4by;->b:LX/4bu;

    .line 794359
    new-instance v0, LX/4bx;

    invoke-direct {v0, p0, p1, p2, p5}, LX/4bx;-><init>(LX/4by;Lorg/apache/http/conn/ClientConnectionManager;Lorg/apache/http/params/HttpParams;LX/1Gl;)V

    .line 794360
    invoke-virtual {v0, p4}, Lorg/apache/http/impl/client/DefaultHttpClient;->setHttpRequestRetryHandler(Lorg/apache/http/client/HttpRequestRetryHandler;)V

    .line 794361
    new-instance v1, LX/4bq;

    invoke-direct {v1}, LX/4bq;-><init>()V

    invoke-virtual {v0, v1}, Lorg/apache/http/impl/client/DefaultHttpClient;->setRedirectHandler(Lorg/apache/http/client/RedirectHandler;)V

    .line 794362
    iput-object v0, p0, LX/4by;->a:Lorg/apache/http/client/HttpClient;

    .line 794363
    return-void
.end method

.method public static a(LX/0QB;)LX/4by;
    .locals 9

    .prologue
    .line 794369
    sget-object v0, LX/4by;->c:LX/4by;

    if-nez v0, :cond_1

    .line 794370
    const-class v1, LX/4by;

    monitor-enter v1

    .line 794371
    :try_start_0
    sget-object v0, LX/4by;->c:LX/4by;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 794372
    if-eqz v2, :cond_0

    .line 794373
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 794374
    new-instance v3, LX/4by;

    invoke-static {v0}, LX/4bs;->a(LX/0QB;)LX/4bs;

    move-result-object v4

    check-cast v4, Lorg/apache/http/conn/ClientConnectionManager;

    invoke-static {v0}, LX/1i0;->b(LX/0QB;)Lorg/apache/http/params/HttpParams;

    move-result-object v5

    check-cast v5, Lorg/apache/http/params/HttpParams;

    invoke-static {v0}, LX/4bu;->a(LX/0QB;)LX/4bu;

    move-result-object v6

    check-cast v6, LX/4bu;

    invoke-static {v0}, LX/4c0;->a(LX/0QB;)LX/4c0;

    move-result-object v7

    check-cast v7, LX/4c0;

    invoke-static {v0}, LX/1Gl;->a(LX/0QB;)LX/1Gl;

    move-result-object v8

    check-cast v8, LX/1Gl;

    invoke-direct/range {v3 .. v8}, LX/4by;-><init>(Lorg/apache/http/conn/ClientConnectionManager;Lorg/apache/http/params/HttpParams;LX/4bu;LX/4c0;LX/1Gl;)V

    .line 794375
    move-object v0, v3

    .line 794376
    sput-object v0, LX/4by;->c:LX/4by;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 794377
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 794378
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 794379
    :cond_1
    sget-object v0, LX/4by;->c:LX/4by;

    return-object v0

    .line 794380
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 794381
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpHost;
    .locals 4

    .prologue
    .line 794364
    const/4 v0, 0x0

    .line 794365
    invoke-interface {p0}, Lorg/apache/http/client/methods/HttpUriRequest;->getURI()Ljava/net/URI;

    move-result-object v1

    .line 794366
    invoke-virtual {v1}, Ljava/net/URI;->isAbsolute()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 794367
    new-instance v0, Lorg/apache/http/HttpHost;

    invoke-virtual {v1}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Ljava/net/URI;->getPort()I

    move-result v3

    invoke-virtual {v1}, Ljava/net/URI;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v2, v3, v1}, Lorg/apache/http/HttpHost;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 794368
    :cond_0
    return-object v0
.end method

.method private static b()Ljava/lang/RuntimeException;
    .locals 2

    .prologue
    .line 794356
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Use FbHttpRequest and FbHttpRequestProcessor instead"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final a()Lorg/apache/http/client/CookieStore;
    .locals 1

    .prologue
    .line 794355
    iget-object v0, p0, LX/4by;->b:LX/4bu;

    return-object v0
.end method

.method public final execute(Lorg/apache/http/HttpHost;Lorg/apache/http/HttpRequest;Lorg/apache/http/client/ResponseHandler;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/apache/http/HttpHost;",
            "Lorg/apache/http/HttpRequest;",
            "Lorg/apache/http/client/ResponseHandler",
            "<+TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 794354
    invoke-static {}, LX/4by;->b()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public final execute(Lorg/apache/http/HttpHost;Lorg/apache/http/HttpRequest;Lorg/apache/http/client/ResponseHandler;Lorg/apache/http/protocol/HttpContext;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/apache/http/HttpHost;",
            "Lorg/apache/http/HttpRequest;",
            "Lorg/apache/http/client/ResponseHandler",
            "<+TT;>;",
            "Lorg/apache/http/protocol/HttpContext;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 794353
    invoke-static {}, LX/4by;->b()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public final execute(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/client/ResponseHandler;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            "Lorg/apache/http/client/ResponseHandler",
            "<+TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 794382
    invoke-static {}, LX/4by;->b()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public final execute(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/client/ResponseHandler;Lorg/apache/http/protocol/HttpContext;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            "Lorg/apache/http/client/ResponseHandler",
            "<+TT;>;",
            "Lorg/apache/http/protocol/HttpContext;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 794352
    invoke-static {}, LX/4by;->b()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public final execute(Lorg/apache/http/HttpHost;Lorg/apache/http/HttpRequest;)Lorg/apache/http/HttpResponse;
    .locals 1

    .prologue
    .line 794351
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, LX/4by;->execute(Lorg/apache/http/HttpHost;Lorg/apache/http/HttpRequest;Lorg/apache/http/protocol/HttpContext;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    return-object v0
.end method

.method public final execute(Lorg/apache/http/HttpHost;Lorg/apache/http/HttpRequest;Lorg/apache/http/protocol/HttpContext;)Lorg/apache/http/HttpResponse;
    .locals 1

    .prologue
    .line 794350
    iget-object v0, p0, LX/4by;->a:Lorg/apache/http/client/HttpClient;

    invoke-interface {v0, p1, p2, p3}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/HttpHost;Lorg/apache/http/HttpRequest;Lorg/apache/http/protocol/HttpContext;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    return-object v0
.end method

.method public final execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
    .locals 2

    .prologue
    .line 794349
    invoke-static {p1}, LX/4by;->a(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpHost;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, LX/4by;->execute(Lorg/apache/http/HttpHost;Lorg/apache/http/HttpRequest;Lorg/apache/http/protocol/HttpContext;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    return-object v0
.end method

.method public final execute(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/protocol/HttpContext;)Lorg/apache/http/HttpResponse;
    .locals 1

    .prologue
    .line 794346
    invoke-static {p1}, LX/4by;->a(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpHost;

    move-result-object v0

    invoke-virtual {p0, v0, p1, p2}, LX/4by;->execute(Lorg/apache/http/HttpHost;Lorg/apache/http/HttpRequest;Lorg/apache/http/protocol/HttpContext;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    return-object v0
.end method

.method public final getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;
    .locals 1

    .prologue
    .line 794348
    iget-object v0, p0, LX/4by;->a:Lorg/apache/http/client/HttpClient;

    invoke-interface {v0}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v0

    return-object v0
.end method

.method public final getParams()Lorg/apache/http/params/HttpParams;
    .locals 1

    .prologue
    .line 794347
    iget-object v0, p0, LX/4by;->a:Lorg/apache/http/client/HttpClient;

    invoke-interface {v0}, Lorg/apache/http/client/HttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v0

    return-object v0
.end method
