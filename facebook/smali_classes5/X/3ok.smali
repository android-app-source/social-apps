.class public LX/3ok;
.super LX/3oi;
.source ""


# static fields
.field public static final a:Landroid/os/Handler;


# instance fields
.field public b:J

.field public c:Z

.field private final d:[I

.field private final e:[F

.field public f:I

.field public g:Landroid/view/animation/Interpolator;

.field public h:LX/3og;

.field public i:F

.field public final j:Ljava/lang/Runnable;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 640960
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, LX/3ok;->a:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 640955
    invoke-direct {p0}, LX/3oi;-><init>()V

    .line 640956
    new-array v0, v1, [I

    iput-object v0, p0, LX/3ok;->d:[I

    .line 640957
    new-array v0, v1, [F

    iput-object v0, p0, LX/3ok;->e:[F

    .line 640958
    const/16 v0, 0xc8

    iput v0, p0, LX/3ok;->f:I

    .line 640959
    new-instance v0, Landroid/support/design/widget/ValueAnimatorCompatImplEclairMr1$1;

    invoke-direct {v0, p0}, Landroid/support/design/widget/ValueAnimatorCompatImplEclairMr1$1;-><init>(LX/3ok;)V

    iput-object v0, p0, LX/3ok;->j:Ljava/lang/Runnable;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    .line 640948
    iget-boolean v0, p0, LX/3ok;->c:Z

    if-eqz v0, :cond_0

    .line 640949
    :goto_0
    return-void

    .line 640950
    :cond_0
    iget-object v0, p0, LX/3ok;->g:Landroid/view/animation/Interpolator;

    if-nez v0, :cond_1

    .line 640951
    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    iput-object v0, p0, LX/3ok;->g:Landroid/view/animation/Interpolator;

    .line 640952
    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, LX/3ok;->b:J

    .line 640953
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/3ok;->c:Z

    .line 640954
    sget-object v0, LX/3ok;->a:Landroid/os/Handler;

    iget-object v1, p0, LX/3ok;->j:Ljava/lang/Runnable;

    const-wide/16 v2, 0xa

    const v4, -0x584713f5

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    goto :goto_0
.end method

.method public final a(FF)V
    .locals 2

    .prologue
    .line 640937
    iget-object v0, p0, LX/3ok;->e:[F

    const/4 v1, 0x0

    aput p1, v0, v1

    .line 640938
    iget-object v0, p0, LX/3ok;->e:[F

    const/4 v1, 0x1

    aput p2, v0, v1

    .line 640939
    return-void
.end method

.method public final a(I)V
    .locals 0

    .prologue
    .line 640946
    iput p1, p0, LX/3ok;->f:I

    .line 640947
    return-void
.end method

.method public final a(II)V
    .locals 2

    .prologue
    .line 640943
    iget-object v0, p0, LX/3ok;->d:[I

    const/4 v1, 0x0

    aput p1, v0, v1

    .line 640944
    iget-object v0, p0, LX/3ok;->d:[I

    const/4 v1, 0x1

    aput p2, v0, v1

    .line 640945
    return-void
.end method

.method public final a(LX/3og;)V
    .locals 0

    .prologue
    .line 640961
    iput-object p1, p0, LX/3ok;->h:LX/3og;

    .line 640962
    return-void
.end method

.method public final a(Landroid/view/animation/Interpolator;)V
    .locals 0

    .prologue
    .line 640941
    iput-object p1, p0, LX/3ok;->g:Landroid/view/animation/Interpolator;

    .line 640942
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 640940
    iget-boolean v0, p0, LX/3ok;->c:Z

    return v0
.end method

.method public final c()I
    .locals 3

    .prologue
    .line 640933
    iget-object v0, p0, LX/3ok;->d:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    iget-object v1, p0, LX/3ok;->d:[I

    const/4 v2, 0x1

    aget v1, v1, v2

    .line 640934
    iget v2, p0, LX/3ok;->i:F

    move v2, v2

    .line 640935
    sub-int p0, v1, v0

    int-to-float p0, p0

    mul-float/2addr p0, v2

    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    move-result p0

    add-int/2addr p0, v0

    move v0, p0

    .line 640936
    return v0
.end method

.method public final d()F
    .locals 3

    .prologue
    .line 640930
    iget-object v0, p0, LX/3ok;->e:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    iget-object v1, p0, LX/3ok;->e:[F

    const/4 v2, 0x1

    aget v1, v1, v2

    .line 640931
    iget v2, p0, LX/3ok;->i:F

    move v2, v2

    .line 640932
    invoke-static {v0, v1, v2}, LX/3ns;->a(FFF)F

    move-result v0

    return v0
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 640927
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/3ok;->c:Z

    .line 640928
    sget-object v0, LX/3ok;->a:Landroid/os/Handler;

    iget-object v1, p0, LX/3ok;->j:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 640929
    return-void
.end method
