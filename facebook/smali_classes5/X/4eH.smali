.class public LX/4eH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1GU;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1GD;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Z

.field public final d:I

.field public final e:J


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0ad;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/1GD;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;",
            ">;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 797130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 797131
    sget v0, LX/1FD;->q:I

    invoke-interface {p3, v0, v4}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/4eH;->d:I

    .line 797132
    const-wide/32 v0, 0x36ee80

    sget v2, LX/1FD;->o:I

    const/4 v3, 0x1

    invoke-interface {p3, v2, v3}, LX/0ad;->a(II)I

    move-result v2

    int-to-long v2, v2

    mul-long/2addr v0, v2

    iput-wide v0, p0, LX/4eH;->e:J

    .line 797133
    sget-short v0, LX/1FD;->b:S

    invoke-interface {p3, v0, v4}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/4eH;->c:Z

    .line 797134
    iget-boolean v0, p0, LX/4eH;->c:Z

    if-eqz v0, :cond_0

    .line 797135
    iput-object p2, p0, LX/4eH;->b:LX/0Ot;

    .line 797136
    iput-object v5, p0, LX/4eH;->a:LX/0Ot;

    .line 797137
    :goto_0
    return-void

    .line 797138
    :cond_0
    iput-object p1, p0, LX/4eH;->a:LX/0Ot;

    .line 797139
    iput-object v5, p0, LX/4eH;->b:LX/0Ot;

    goto :goto_0
.end method

.method public static synthetic a(LX/4eH;IIJJ)I
    .locals 8

    .prologue
    .line 797140
    invoke-static {}, Landroid/os/SystemClock;->currentThreadTimeMillis()J

    move-result-wide v1

    .line 797141
    sub-long v3, v1, p3

    iget-wide v5, p0, LX/4eH;->e:J

    cmp-long v3, v3, v5

    if-gtz v3, :cond_0

    sub-long v3, v1, p5

    iget-wide v5, p0, LX/4eH;->e:J

    cmp-long v3, v3, v5

    if-lez v3, :cond_0

    .line 797142
    const/4 v1, 0x1

    .line 797143
    :goto_0
    move v0, v1

    .line 797144
    return v0

    .line 797145
    :cond_0
    sub-long v3, v1, p5

    iget-wide v5, p0, LX/4eH;->e:J

    cmp-long v3, v3, v5

    if-gtz v3, :cond_1

    sub-long/2addr v1, p3

    iget-wide v3, p0, LX/4eH;->e:J

    cmp-long v1, v1, v3

    if-lez v1, :cond_1

    .line 797146
    const/4 v1, -0x1

    goto :goto_0

    .line 797147
    :cond_1
    sub-int v1, p1, p2

    invoke-static {v1}, Ljava/lang/Integer;->signum(I)I

    move-result v1

    move v1, v1

    .line 797148
    goto :goto_0
.end method

.method public static a$redex0(LX/4eH;LX/35g;)I
    .locals 2

    .prologue
    .line 797149
    iget-object v0, p1, LX/35g;->a:Ljava/lang/String;

    move-object v1, v0

    .line 797150
    iget-boolean v0, p0, LX/4eH;->c:Z

    if-eqz v0, :cond_0

    .line 797151
    iget-object v0, p0, LX/4eH;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;

    .line 797152
    invoke-static {v0}, Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;->c(Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;)V

    .line 797153
    iget-object p0, v0, Lcom/facebook/imagepipeline/instrumentation/DiskCacheImageHistoriesDbMarshaller;->f:Ljava/util/Map;

    invoke-static {p0}, LX/2oC;->a(Ljava/util/Map;)LX/2oC;

    move-result-object p0

    move-object v0, p0

    .line 797154
    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 797155
    :goto_0
    return v0

    .line 797156
    :cond_0
    iget-object v0, p0, LX/4eH;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1GD;

    invoke-virtual {v0}, LX/1GD;->j()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3rL;

    .line 797157
    if-nez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, v0, LX/3rL;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/43d;
    .locals 1

    .prologue
    .line 797158
    new-instance v0, LX/4eG;

    invoke-direct {v0, p0}, LX/4eG;-><init>(LX/4eH;)V

    return-object v0
.end method
