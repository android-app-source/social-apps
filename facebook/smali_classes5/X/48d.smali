.class public final LX/48d;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 673083
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;
    .locals 6
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # LX/17c;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 673069
    packed-switch p1, :pswitch_data_0

    .line 673070
    const/4 v1, 0x0

    :goto_0
    move-object v1, v1

    .line 673071
    if-nez v1, :cond_0

    .line 673072
    const-string v1, "UriMapHelper"

    const-string v2, "ComponentMap.getNameForType(%d) returned null"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 673073
    :goto_1
    return-object v0

    .line 673074
    :cond_0
    if-eqz p6, :cond_1

    invoke-interface {p6, p2}, LX/17c;->a(I)LX/48b;

    move-result-object v5

    .line 673075
    :goto_2
    if-nez p4, :cond_2

    .line 673076
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 673077
    :goto_3
    const-string v0, "target_fragment"

    invoke-virtual {v3, v0, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    move-object v0, p0

    move-object v2, p3

    move-object v4, p5

    .line 673078
    invoke-static/range {v0 .. v5}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/48b;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_1

    :cond_1
    move-object v5, v0

    .line 673079
    goto :goto_2

    :cond_2
    move-object v3, p4

    goto :goto_3

    .line 673080
    :pswitch_0
    const-string v1, "com.facebook.crudolib.urimap.runtime.DummyComponentMapActivity"

    goto :goto_0

    .line 673081
    :pswitch_1
    const-string v1, "com.facebook.katana.activity.ImmersiveActivity"

    goto :goto_0

    .line 673082
    :pswitch_2
    const-string v1, "com.facebook.katana.activity.react.ImmersiveReactActivity"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;
    .locals 6
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # LX/17c;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 673066
    if-eqz p5, :cond_0

    invoke-interface {p5, p1}, LX/17c;->a(Ljava/lang/String;)LX/48b;

    move-result-object v5

    :goto_0
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    .line 673067
    invoke-static/range {v0 .. v5}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/48b;)Landroid/content/Intent;

    move-result-object v0

    return-object v0

    .line 673068
    :cond_0
    const/4 v5, 0x0

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/48b;)Landroid/content/Intent;
    .locals 8
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # LX/48b;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 673041
    if-eqz p5, :cond_0

    invoke-virtual {p5}, LX/48b;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 673042
    const/4 v0, 0x0

    .line 673043
    :goto_0
    return-object v0

    .line 673044
    :cond_0
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 673045
    invoke-virtual {v3, p0, p1}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    .line 673046
    if-eqz p3, :cond_1

    .line 673047
    invoke-virtual {v3, p3}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 673048
    :cond_1
    if-eqz p2, :cond_6

    .line 673049
    if-eqz p3, :cond_2

    .line 673050
    invoke-virtual {p3}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 673051
    invoke-virtual {p3, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    .line 673052
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "<"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ">"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v0, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p2

    goto :goto_1

    :cond_2
    move-object v2, p2

    .line 673053
    if-eqz p4, :cond_8

    .line 673054
    const-string v0, "?"

    invoke-virtual {v2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    .line 673055
    :goto_2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 673056
    invoke-virtual {p4}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v2, v0

    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 673057
    invoke-virtual {p4, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    .line 673058
    if-eqz v2, :cond_4

    .line 673059
    const/16 v2, 0x3f

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move v2, v1

    .line 673060
    :goto_4
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v7, 0x3d

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_3

    :cond_3
    move v0, v1

    .line 673061
    goto :goto_2

    .line 673062
    :cond_4
    const/16 v7, 0x26

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 673063
    :cond_5
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 673064
    :goto_5
    const-string v1, "key_templated_str"

    invoke-virtual {v3, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 673065
    :cond_6
    if-eqz p5, :cond_7

    invoke-virtual {p5, v3}, LX/48b;->a(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_0

    :cond_7
    move-object v0, v3

    goto/16 :goto_0

    :cond_8
    move-object v0, v2

    goto :goto_5
.end method

.method public static a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 3
    .param p0    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x2

    .line 673035
    const/4 v0, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 673036
    const/4 p0, 0x0

    :goto_1
    return-object p0

    .line 673037
    :sswitch_0
    const-string v2, "fref"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :sswitch_1
    const-string v2, "pn_ref"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :sswitch_2
    const-string v2, "__tn__"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :sswitch_3
    const-string v2, "_ft_"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x3

    goto :goto_0

    .line 673038
    :pswitch_0
    if-nez p0, :cond_1

    .line 673039
    new-instance p0, Landroid/os/Bundle;

    invoke-direct {p0, v1}, Landroid/os/Bundle;-><init>(I)V

    .line 673040
    :cond_1
    invoke-virtual {p0, p1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x58728046 -> :sswitch_2
        -0x3aa6392e -> :sswitch_1
        0x2cbc92 -> :sswitch_3
        0x30166d -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 673029
    if-nez p0, :cond_1

    .line 673030
    :cond_0
    :goto_0
    return-object v0

    .line 673031
    :cond_1
    const-string v1, "true"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "1"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 673032
    :cond_2
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 673033
    :cond_3
    const-string v1, "false"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "0"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 673034
    :cond_4
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method public static a([CIILjava/lang/String;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 673026
    new-instance v0, Ljava/lang/String;

    sub-int v1, p2, p1

    invoke-direct {v0, p0, p1, v1}, Ljava/lang/String;-><init>([CII)V

    .line 673027
    invoke-virtual {p4, p3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 673028
    return-void
.end method

.method public static a([CILjava/lang/String;)Z
    .locals 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "BadMethodUse"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 673017
    array-length v2, p0

    .line 673018
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v3

    .line 673019
    if-gt v2, p1, :cond_1

    .line 673020
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v1, v0

    .line 673021
    :goto_1
    if-ge p1, v2, :cond_2

    if-ge v1, v3, :cond_2

    .line 673022
    aget-char v4, p0, p1

    invoke-virtual {p2, v1}, Ljava/lang/String;->charAt(I)C

    move-result v5

    if-ne v4, v5, :cond_0

    .line 673023
    add-int/lit8 p1, p1, 0x1

    .line 673024
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 673025
    :cond_2
    if-gt v3, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static a([CILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)[I
    .locals 8

    .prologue
    .line 672979
    array-length v4, p0

    .line 672980
    const-wide/16 v2, 0x0

    .line 672981
    if-eqz p3, :cond_0

    const/4 v0, 0x1

    :goto_0
    move v1, p1

    .line 672982
    :goto_1
    if-gt v4, v1, :cond_1

    move v4, v1

    .line 672983
    :goto_2
    const/4 v1, -0x1

    if-ne v4, v1, :cond_6

    .line 672984
    const/4 v0, 0x0

    .line 672985
    :goto_3
    return-object v0

    .line 672986
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 672987
    :cond_1
    aget-char v5, p0, v1

    .line 672988
    const/16 v6, 0x2f

    if-eq v5, v6, :cond_2

    const/16 v6, 0x3f

    if-ne v5, v6, :cond_3

    :cond_2
    move v4, v1

    .line 672989
    goto :goto_2

    .line 672990
    :cond_3
    if-eqz v0, :cond_4

    .line 672991
    const/16 v6, 0xa

    invoke-static {v5, v6}, Ljava/lang/Character;->digit(CI)I

    move-result v5

    .line 672992
    const/4 v6, -0x1

    if-eq v5, v6, :cond_5

    .line 672993
    const-wide/16 v6, 0xa

    mul-long/2addr v2, v6

    .line 672994
    int-to-long v6, v5

    add-long/2addr v2, v6

    .line 672995
    :cond_4
    :goto_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 672996
    :cond_5
    const/4 v0, 0x0

    goto :goto_4

    .line 672997
    :cond_6
    new-instance v5, Ljava/lang/String;

    sub-int v1, v4, p1

    invoke-direct {v5, p0, p1, v1}, Ljava/lang/String;-><init>([CII)V

    .line 672998
    const/4 v1, -0x1

    .line 672999
    if-eqz p2, :cond_7

    .line 673000
    invoke-static {v5}, LX/48d;->a(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v6

    .line 673001
    if-eqz v6, :cond_7

    .line 673002
    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p5, p2, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 673003
    const/4 v1, 0x1

    .line 673004
    :cond_7
    const/4 v6, -0x1

    if-ne v1, v6, :cond_a

    if-eqz v0, :cond_a

    .line 673005
    invoke-virtual {p5, p3, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 673006
    const/4 v0, 0x2

    .line 673007
    :goto_5
    const/4 v1, -0x1

    if-ne v0, v1, :cond_9

    if-eqz p4, :cond_9

    .line 673008
    invoke-virtual {p5, p4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 673009
    const/4 v0, 0x3

    move v1, v0

    .line 673010
    :goto_6
    const/4 v0, -0x1

    if-ne v1, v0, :cond_8

    .line 673011
    const/4 v0, 0x0

    goto :goto_3

    .line 673012
    :cond_8
    const/4 v0, 0x2

    new-array v0, v0, [I

    const/4 v2, 0x0

    aput v4, v0, v2

    const/4 v2, 0x1

    aput v1, v0, v2

    goto :goto_3

    :cond_9
    move v1, v0

    goto :goto_6

    :cond_a
    move v0, v1

    goto :goto_5
.end method

.method public static b(Ljava/lang/String;)Ljava/lang/Long;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 673013
    if-nez p0, :cond_0

    .line 673014
    :goto_0
    return-object v0

    .line 673015
    :cond_0
    :try_start_0
    invoke-static {p0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 673016
    :catch_0
    goto :goto_0
.end method
