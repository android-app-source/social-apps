.class public LX/3yR;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/3yR;


# instance fields
.field public final a:LX/0cb;

.field private final b:LX/3yJ;

.field public final c:LX/0ae;


# direct methods
.method public constructor <init>(LX/0cb;LX/3yJ;LX/0ae;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 661011
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 661012
    iput-object p1, p0, LX/3yR;->a:LX/0cb;

    .line 661013
    iput-object p2, p0, LX/3yR;->b:LX/3yJ;

    .line 661014
    iput-object p3, p0, LX/3yR;->c:LX/0ae;

    .line 661015
    return-void
.end method

.method public static a(LX/0QB;)LX/3yR;
    .locals 6

    .prologue
    .line 661016
    sget-object v0, LX/3yR;->d:LX/3yR;

    if-nez v0, :cond_1

    .line 661017
    const-class v1, LX/3yR;

    monitor-enter v1

    .line 661018
    :try_start_0
    sget-object v0, LX/3yR;->d:LX/3yR;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 661019
    if-eqz v2, :cond_0

    .line 661020
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 661021
    new-instance p0, LX/3yR;

    invoke-static {v0}, LX/0ca;->a(LX/0QB;)LX/0ca;

    move-result-object v3

    check-cast v3, LX/0cb;

    invoke-static {v0}, LX/3yJ;->a(LX/0QB;)LX/3yJ;

    move-result-object v4

    check-cast v4, LX/3yJ;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ae;

    invoke-direct {p0, v3, v4, v5}, LX/3yR;-><init>(LX/0cb;LX/3yJ;LX/0ae;)V

    .line 661022
    move-object v0, p0

    .line 661023
    sput-object v0, LX/3yR;->d:LX/3yR;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 661024
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 661025
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 661026
    :cond_1
    sget-object v0, LX/3yR;->d:LX/3yR;

    return-object v0

    .line 661027
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 661028
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/3yQ;",
            ">;"
        }
    .end annotation

    .prologue
    .line 661029
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 661030
    iget-object v0, p0, LX/3yR;->a:LX/0cb;

    invoke-interface {v0}, LX/0cb;->b()Ljava/util/Map;

    move-result-object v3

    .line 661031
    iget-object v0, p0, LX/3yR;->b:LX/3yJ;

    invoke-virtual {v0}, LX/3yJ;->a()LX/0Rf;

    move-result-object v0

    invoke-static {v0}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v1

    .line 661032
    iget-object v0, p0, LX/3yR;->b:LX/3yJ;

    .line 661033
    invoke-static {v0}, LX/3yJ;->d(LX/3yJ;)V

    .line 661034
    iget-object v4, v0, LX/3yJ;->f:LX/0Rf;

    move-object v0, v4

    .line 661035
    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 661036
    iget-object v0, p0, LX/3yR;->c:LX/0ae;

    invoke-interface {v0}, LX/0ae;->c()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 661037
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 661038
    :cond_0
    invoke-static {v1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 661039
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 661040
    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationModel;

    .line 661041
    if-eqz v1, :cond_1

    .line 661042
    const/4 v9, 0x0

    const/4 v6, -0x1

    .line 661043
    iget-object v5, p0, LX/3yR;->c:LX/0ae;

    invoke-interface {v5, v0}, LX/0ae;->b(Ljava/lang/String;)Z

    move-result v11

    .line 661044
    if-eqz v11, :cond_2

    .line 661045
    iget-object v5, p0, LX/3yR;->c:LX/0ae;

    sget-object v7, LX/0oc;->EFFECTIVE:LX/0oc;

    invoke-interface {v5, v7, v0}, LX/0ae;->a(LX/0oc;Ljava/lang/String;)Z

    move-result v7

    .line 661046
    :goto_2
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v10

    .line 661047
    invoke-virtual {v1}, Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationModel;->e_()Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationParameterSetsConnectionModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationParameterSetsConnectionModel;->a()LX/0Px;

    move-result-object v12

    invoke-virtual {v12}, LX/0Px;->size()I

    move-result v13

    move v8, v9

    .line 661048
    :goto_3
    if-ge v8, v13, :cond_3

    invoke-virtual {v12, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationParameterSetsConnectionModel$EdgesModel;

    .line 661049
    invoke-virtual {v5}, Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationParameterSetsConnectionModel$EdgesModel;->a()Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationParameterSetModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationParameterSetModel;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v10, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 661050
    add-int/lit8 v5, v8, 0x1

    move v8, v5

    goto :goto_3

    .line 661051
    :cond_2
    iget-object v5, p0, LX/3yR;->a:LX/0cb;

    invoke-interface {v5, v0}, LX/0cb;->b(Ljava/lang/String;)Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;

    move-result-object v5

    .line 661052
    iget-boolean v7, v5, LX/2Wx;->c:Z

    move v7, v7

    .line 661053
    goto :goto_2

    .line 661054
    :cond_3
    invoke-virtual {v10}, LX/0Pz;->b()LX/0Px;

    move-result-object v8

    .line 661055
    const/4 v5, 0x0

    .line 661056
    invoke-virtual {v1}, Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationModel;->a()Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationParameterSetsConnectionModel;

    move-result-object v10

    if-eqz v10, :cond_4

    invoke-virtual {v1}, Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationModel;->a()Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationParameterSetsConnectionModel;

    move-result-object v10

    invoke-virtual {v10}, Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationParameterSetsConnectionModel;->a()LX/0Px;

    move-result-object v10

    invoke-virtual {v10}, LX/0Px;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_4

    .line 661057
    invoke-virtual {v1}, Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationModel;->a()Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationParameterSetsConnectionModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationParameterSetsConnectionModel;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationParameterSetsConnectionModel$EdgesModel;

    invoke-virtual {v5}, Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationParameterSetsConnectionModel$EdgesModel;->a()Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationParameterSetModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/abtest/qe/protocol/sync/full/ViewerConfigurationQueryModels$ConfigurationParameterSetModel;->a()Ljava/lang/String;

    move-result-object v5

    .line 661058
    :cond_4
    if-eqz v7, :cond_7

    if-eqz v5, :cond_7

    const-string v9, "local_default_group"

    invoke-virtual {v5, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_7

    .line 661059
    invoke-virtual {v8, v5}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v9

    .line 661060
    :goto_4
    iget-object v5, p0, LX/3yR;->c:LX/0ae;

    invoke-interface {v5, v0}, LX/0ae;->b(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 661061
    iget-object v5, p0, LX/3yR;->c:LX/0ae;

    sget-object v10, LX/0oc;->OVERRIDE:LX/0oc;

    invoke-interface {v5, v10, v0}, LX/0ae;->b(LX/0oc;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 661062
    :goto_5
    move-object v5, v5

    .line 661063
    if-eqz v5, :cond_6

    .line 661064
    invoke-virtual {v8, v5}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v10

    .line 661065
    :goto_6
    new-instance v5, LX/3yQ;

    move-object v6, v0

    invoke-direct/range {v5 .. v11}, LX/3yQ;-><init>(Ljava/lang/String;ZLX/0Px;IIZ)V

    move-object v0, v5

    .line 661066
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_1

    .line 661067
    :cond_5
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    :cond_6
    move v10, v6

    goto :goto_6

    :cond_7
    move v9, v6

    goto :goto_4

    .line 661068
    :cond_8
    iget-object v5, p0, LX/3yR;->a:LX/0cb;

    invoke-interface {v5, v0}, LX/0cb;->c(Ljava/lang/String;)Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;

    move-result-object v5

    .line 661069
    if-nez v5, :cond_9

    const/4 v5, 0x0

    goto :goto_5

    .line 661070
    :cond_9
    iget-object v10, v5, LX/2Wx;->e:Ljava/lang/String;

    move-object v5, v10

    .line 661071
    goto :goto_5
.end method
