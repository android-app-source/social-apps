.class public final LX/3tU;
.super LX/3nx;
.source ""


# instance fields
.field public final synthetic a:LX/3tW;

.field public final b:I

.field public c:LX/3ty;

.field private final d:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(LX/3tW;I)V
    .locals 1

    .prologue
    .line 644945
    iput-object p1, p0, LX/3tU;->a:LX/3tW;

    invoke-direct {p0}, LX/3nx;-><init>()V

    .line 644946
    new-instance v0, Landroid/support/v4/widget/DrawerLayout$ViewDragCallback$1;

    invoke-direct {v0, p0}, Landroid/support/v4/widget/DrawerLayout$ViewDragCallback$1;-><init>(LX/3tU;)V

    iput-object v0, p0, LX/3tU;->d:Ljava/lang/Runnable;

    .line 644947
    iput p2, p0, LX/3tU;->b:I

    .line 644948
    return-void
.end method

.method public static e(LX/3tU;)V
    .locals 2

    .prologue
    const/4 v0, 0x3

    .line 645033
    iget v1, p0, LX/3tU;->b:I

    if-ne v1, v0, :cond_0

    const/4 v0, 0x5

    .line 645034
    :cond_0
    iget-object v1, p0, LX/3tU;->a:LX/3tW;

    invoke-virtual {v1, v0}, LX/3tW;->b(I)Landroid/view/View;

    move-result-object v0

    .line 645035
    if-eqz v0, :cond_1

    .line 645036
    iget-object v1, p0, LX/3tU;->a:LX/3tW;

    invoke-virtual {v1, v0}, LX/3tW;->e(Landroid/view/View;)V

    .line 645037
    :cond_1
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 645031
    iget-object v0, p0, LX/3tU;->a:LX/3tW;

    iget-object v1, p0, LX/3tU;->d:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, LX/3tW;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 645032
    return-void
.end method

.method public final a(I)V
    .locals 6

    .prologue
    .line 644989
    iget-object v0, p0, LX/3tU;->a:LX/3tW;

    iget-object v1, p0, LX/3tU;->c:LX/3ty;

    .line 644990
    iget-object p0, v1, LX/3ty;->s:Landroid/view/View;

    move-object v1, p0

    .line 644991
    const/4 v2, 0x2

    const/4 v3, 0x1

    .line 644992
    iget-object v4, v0, LX/3tW;->i:LX/3ty;

    .line 644993
    iget v5, v4, LX/3ty;->a:I

    move v4, v5

    .line 644994
    iget-object v5, v0, LX/3tW;->j:LX/3ty;

    .line 644995
    iget p0, v5, LX/3ty;->a:I

    move v5, p0

    .line 644996
    if-eq v4, v3, :cond_0

    if-ne v5, v3, :cond_4

    .line 644997
    :cond_0
    :goto_0
    if-eqz v1, :cond_2

    if-nez p1, :cond_2

    .line 644998
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, LX/3tS;

    .line 644999
    iget v4, v2, LX/3tS;->b:F

    const/4 v5, 0x0

    cmpl-float v4, v4, v5

    if-nez v4, :cond_7

    .line 645000
    const/4 v5, 0x0

    .line 645001
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, LX/3tS;

    .line 645002
    iget-boolean v4, v2, LX/3tS;->d:Z

    if-eqz v4, :cond_2

    .line 645003
    iput-boolean v5, v2, LX/3tS;->d:Z

    .line 645004
    iget-object v2, v0, LX/3tW;->t:LX/3tR;

    if-eqz v2, :cond_1

    .line 645005
    iget-object v2, v0, LX/3tW;->t:LX/3tR;

    invoke-interface {v2}, LX/3tR;->b()V

    .line 645006
    :cond_1
    invoke-static {v0, v1, v5}, LX/3tW;->a(LX/3tW;Landroid/view/View;Z)V

    .line 645007
    invoke-virtual {v0}, LX/3tW;->hasWindowFocus()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 645008
    invoke-virtual {v0}, LX/3tW;->getRootView()Landroid/view/View;

    move-result-object v2

    .line 645009
    if-eqz v2, :cond_2

    .line 645010
    const/16 v4, 0x20

    invoke-virtual {v2, v4}, Landroid/view/View;->sendAccessibilityEvent(I)V

    .line 645011
    :cond_2
    :goto_1
    iget v2, v0, LX/3tW;->m:I

    if-eq v3, v2, :cond_3

    .line 645012
    iput v3, v0, LX/3tW;->m:I

    .line 645013
    iget-object v2, v0, LX/3tW;->t:LX/3tR;

    if-eqz v2, :cond_3

    .line 645014
    iget-object v2, v0, LX/3tW;->t:LX/3tR;

    invoke-interface {v2, v3}, LX/3tR;->a(I)V

    .line 645015
    :cond_3
    return-void

    .line 645016
    :cond_4
    if-eq v4, v2, :cond_5

    if-ne v5, v2, :cond_6

    :cond_5
    move v3, v2

    .line 645017
    goto :goto_0

    .line 645018
    :cond_6
    const/4 v2, 0x0

    move v3, v2

    goto :goto_0

    .line 645019
    :cond_7
    iget v2, v2, LX/3tS;->b:F

    const/high16 v4, 0x3f800000    # 1.0f

    cmpl-float v2, v2, v4

    if-nez v2, :cond_2

    .line 645020
    const/4 v5, 0x1

    .line 645021
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, LX/3tS;

    .line 645022
    iget-boolean v4, v2, LX/3tS;->d:Z

    if-nez v4, :cond_a

    .line 645023
    iput-boolean v5, v2, LX/3tS;->d:Z

    .line 645024
    iget-object v2, v0, LX/3tW;->t:LX/3tR;

    if-eqz v2, :cond_8

    .line 645025
    iget-object v2, v0, LX/3tW;->t:LX/3tR;

    invoke-interface {v2}, LX/3tR;->a()V

    .line 645026
    :cond_8
    invoke-static {v0, v1, v5}, LX/3tW;->a(LX/3tW;Landroid/view/View;Z)V

    .line 645027
    invoke-virtual {v0}, LX/3tW;->hasWindowFocus()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 645028
    const/16 v2, 0x20

    invoke-virtual {v0, v2}, LX/3tW;->sendAccessibilityEvent(I)V

    .line 645029
    :cond_9
    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    .line 645030
    :cond_a
    goto :goto_1
.end method

.method public final a(II)V
    .locals 2

    .prologue
    .line 644983
    and-int/lit8 v0, p1, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 644984
    iget-object v0, p0, LX/3tU;->a:LX/3tW;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, LX/3tW;->b(I)Landroid/view/View;

    move-result-object v0

    .line 644985
    :goto_0
    if-eqz v0, :cond_0

    iget-object v1, p0, LX/3tU;->a:LX/3tW;

    invoke-virtual {v1, v0}, LX/3tW;->a(Landroid/view/View;)I

    move-result v1

    if-nez v1, :cond_0

    .line 644986
    iget-object v1, p0, LX/3tU;->c:LX/3ty;

    invoke-virtual {v1, v0, p2}, LX/3ty;->a(Landroid/view/View;I)V

    .line 644987
    :cond_0
    return-void

    .line 644988
    :cond_1
    iget-object v0, p0, LX/3tU;->a:LX/3tW;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, LX/3tW;->b(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Landroid/view/View;FF)V
    .locals 6

    .prologue
    const/high16 v5, 0x3f000000    # 0.5f

    const/4 v4, 0x0

    .line 644973
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/3tS;

    iget v0, v0, LX/3tS;->b:F

    move v1, v0

    .line 644974
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v2

    .line 644975
    iget-object v0, p0, LX/3tU;->a:LX/3tW;

    const/4 v3, 0x3

    invoke-virtual {v0, p1, v3}, LX/3tW;->a(Landroid/view/View;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 644976
    cmpl-float v0, p2, v4

    if-gtz v0, :cond_0

    cmpl-float v0, p2, v4

    if-nez v0, :cond_2

    cmpl-float v0, v1, v5

    if-lez v0, :cond_2

    :cond_0
    const/4 v0, 0x0

    .line 644977
    :cond_1
    :goto_0
    iget-object v1, p0, LX/3tU;->c:LX/3ty;

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v2

    invoke-virtual {v1, v0, v2}, LX/3ty;->a(II)Z

    .line 644978
    iget-object v0, p0, LX/3tU;->a:LX/3tW;

    invoke-virtual {v0}, LX/3tW;->invalidate()V

    .line 644979
    return-void

    .line 644980
    :cond_2
    neg-int v0, v2

    goto :goto_0

    .line 644981
    :cond_3
    iget-object v0, p0, LX/3tU;->a:LX/3tW;

    invoke-virtual {v0}, LX/3tW;->getWidth()I

    move-result v0

    .line 644982
    cmpg-float v3, p2, v4

    if-ltz v3, :cond_4

    cmpl-float v3, p2, v4

    if-nez v3, :cond_1

    cmpl-float v1, v1, v5

    if-lez v1, :cond_1

    :cond_4
    sub-int/2addr v0, v2

    goto :goto_0
.end method

.method public final a(Landroid/view/View;I)V
    .locals 3

    .prologue
    .line 644963
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    .line 644964
    iget-object v1, p0, LX/3tU;->a:LX/3tW;

    const/4 v2, 0x3

    invoke-virtual {v1, p1, v2}, LX/3tW;->a(Landroid/view/View;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 644965
    add-int v1, v0, p2

    int-to-float v1, v1

    int-to-float v0, v0

    div-float v0, v1, v0

    .line 644966
    :goto_0
    iget-object v1, p0, LX/3tU;->a:LX/3tW;

    invoke-virtual {v1, p1, v0}, LX/3tW;->a(Landroid/view/View;F)V

    .line 644967
    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    const/4 v0, 0x4

    :goto_1
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 644968
    iget-object v0, p0, LX/3tU;->a:LX/3tW;

    invoke-virtual {v0}, LX/3tW;->invalidate()V

    .line 644969
    return-void

    .line 644970
    :cond_0
    iget-object v1, p0, LX/3tU;->a:LX/3tW;

    invoke-virtual {v1}, LX/3tW;->getWidth()I

    move-result v1

    .line 644971
    sub-int/2addr v1, p2

    int-to-float v1, v1

    int-to-float v0, v0

    div-float v0, v1, v0

    goto :goto_0

    .line 644972
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(Landroid/view/View;)Z
    .locals 2

    .prologue
    .line 645038
    invoke-static {p1}, LX/3tW;->d(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3tU;->a:LX/3tW;

    iget v1, p0, LX/3tU;->b:I

    invoke-virtual {v0, p1, v1}, LX/3tW;->a(Landroid/view/View;I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3tU;->a:LX/3tW;

    invoke-virtual {v0, p1}, LX/3tW;->a(Landroid/view/View;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Landroid/view/View;I)I
    .locals 2

    .prologue
    .line 644958
    iget-object v0, p0, LX/3tU;->a:LX/3tW;

    const/4 v1, 0x3

    invoke-virtual {v0, p1, v1}, LX/3tW;->a(Landroid/view/View;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 644959
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    neg-int v0, v0

    const/4 v1, 0x0

    invoke-static {p2, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 644960
    :goto_0
    return v0

    .line 644961
    :cond_0
    iget-object v0, p0, LX/3tU;->a:LX/3tW;

    invoke-virtual {v0}, LX/3tW;->getWidth()I

    move-result v0

    .line 644962
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v1

    sub-int v1, v0, v1

    invoke-static {p2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_0
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 644956
    iget-object v0, p0, LX/3tU;->a:LX/3tW;

    iget-object v1, p0, LX/3tU;->d:Ljava/lang/Runnable;

    const-wide/16 v2, 0xa0

    invoke-virtual {v0, v1, v2, v3}, LX/3tW;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 644957
    return-void
.end method

.method public final b(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 644952
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/3tS;

    .line 644953
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/3tS;->c:Z

    .line 644954
    invoke-static {p0}, LX/3tU;->e(LX/3tU;)V

    .line 644955
    return-void
.end method

.method public final c(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 644951
    invoke-static {p1}, LX/3tW;->d(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Landroid/view/View;I)I
    .locals 1

    .prologue
    .line 644950
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v0

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 644949
    const/4 v0, 0x0

    return v0
.end method
