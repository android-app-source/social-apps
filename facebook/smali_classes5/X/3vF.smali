.class public LX/3vF;
.super LX/3uv;
.source ""

# interfaces
.implements Landroid/view/Menu;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3uv",
        "<",
        "LX/3qu;",
        ">;",
        "Landroid/view/Menu;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/3qu;)V
    .locals 0

    .prologue
    .line 650992
    invoke-direct {p0, p1, p2}, LX/3uv;-><init>(Landroid/content/Context;Ljava/lang/Object;)V

    .line 650993
    return-void
.end method


# virtual methods
.method public final add(I)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 650994
    iget-object v0, p0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v0, LX/3qu;

    invoke-interface {v0, p1}, LX/3qu;->add(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/3uv;->a(Landroid/view/MenuItem;)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public final add(IIII)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 650995
    iget-object v0, p0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v0, LX/3qu;

    invoke-interface {v0, p1, p2, p3, p4}, LX/3qu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/3uv;->a(Landroid/view/MenuItem;)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public final add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 650996
    iget-object v0, p0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v0, LX/3qu;

    invoke-interface {v0, p1, p2, p3, p4}, LX/3qu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/3uv;->a(Landroid/view/MenuItem;)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public final add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 650997
    iget-object v0, p0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v0, LX/3qu;

    invoke-interface {v0, p1}, LX/3qu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/3uv;->a(Landroid/view/MenuItem;)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public final addIntentOptions(IIILandroid/content/ComponentName;[Landroid/content/Intent;Landroid/content/Intent;I[Landroid/view/MenuItem;)I
    .locals 10

    .prologue
    .line 650998
    const/4 v9, 0x0

    .line 650999
    if-eqz p8, :cond_0

    .line 651000
    move-object/from16 v0, p8

    array-length v1, v0

    new-array v9, v1, [Landroid/view/MenuItem;

    .line 651001
    :cond_0
    iget-object v1, p0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v1, LX/3qu;

    move v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move/from16 v8, p7

    invoke-interface/range {v1 .. v9}, LX/3qu;->addIntentOptions(IIILandroid/content/ComponentName;[Landroid/content/Intent;Landroid/content/Intent;I[Landroid/view/MenuItem;)I

    move-result v2

    .line 651002
    if-eqz v9, :cond_1

    .line 651003
    const/4 v1, 0x0

    array-length v3, v9

    :goto_0
    if-ge v1, v3, :cond_1

    .line 651004
    aget-object v4, v9, v1

    invoke-virtual {p0, v4}, LX/3uv;->a(Landroid/view/MenuItem;)Landroid/view/MenuItem;

    move-result-object v4

    aput-object v4, p8, v1

    .line 651005
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 651006
    :cond_1
    return v2
.end method

.method public final addSubMenu(I)Landroid/view/SubMenu;
    .locals 1

    .prologue
    .line 651007
    iget-object v0, p0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v0, LX/3qu;

    invoke-interface {v0, p1}, LX/3qu;->addSubMenu(I)Landroid/view/SubMenu;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/3uv;->a(Landroid/view/SubMenu;)Landroid/view/SubMenu;

    move-result-object v0

    return-object v0
.end method

.method public final addSubMenu(IIII)Landroid/view/SubMenu;
    .locals 1

    .prologue
    .line 651008
    iget-object v0, p0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v0, LX/3qu;

    invoke-interface {v0, p1, p2, p3, p4}, LX/3qu;->addSubMenu(IIII)Landroid/view/SubMenu;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/3uv;->a(Landroid/view/SubMenu;)Landroid/view/SubMenu;

    move-result-object v0

    return-object v0
.end method

.method public final addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;
    .locals 1

    .prologue
    .line 651009
    iget-object v0, p0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v0, LX/3qu;

    invoke-interface {v0, p1, p2, p3, p4}, LX/3qu;->addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/3uv;->a(Landroid/view/SubMenu;)Landroid/view/SubMenu;

    move-result-object v0

    return-object v0
.end method

.method public final addSubMenu(Ljava/lang/CharSequence;)Landroid/view/SubMenu;
    .locals 1

    .prologue
    .line 651010
    iget-object v0, p0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v0, LX/3qu;

    invoke-interface {v0, p1}, LX/3qu;->addSubMenu(Ljava/lang/CharSequence;)Landroid/view/SubMenu;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/3uv;->a(Landroid/view/SubMenu;)Landroid/view/SubMenu;

    move-result-object v0

    return-object v0
.end method

.method public final clear()V
    .locals 1

    .prologue
    .line 651011
    iget-object v0, p0, LX/3uv;->c:Ljava/util/Map;

    if-eqz v0, :cond_0

    .line 651012
    iget-object v0, p0, LX/3uv;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 651013
    :cond_0
    iget-object v0, p0, LX/3uv;->d:Ljava/util/Map;

    if-eqz v0, :cond_1

    .line 651014
    iget-object v0, p0, LX/3uv;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 651015
    :cond_1
    iget-object v0, p0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v0, LX/3qu;

    invoke-interface {v0}, LX/3qu;->clear()V

    .line 651016
    return-void
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 650989
    iget-object v0, p0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v0, LX/3qu;

    invoke-interface {v0}, LX/3qu;->close()V

    .line 650990
    return-void
.end method

.method public final findItem(I)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 650991
    iget-object v0, p0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v0, LX/3qu;

    invoke-interface {v0, p1}, LX/3qu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/3uv;->a(Landroid/view/MenuItem;)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public final getItem(I)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 650988
    iget-object v0, p0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v0, LX/3qu;

    invoke-interface {v0, p1}, LX/3qu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/3uv;->a(Landroid/view/MenuItem;)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public final hasVisibleItems()Z
    .locals 1

    .prologue
    .line 650987
    iget-object v0, p0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v0, LX/3qu;

    invoke-interface {v0}, LX/3qu;->hasVisibleItems()Z

    move-result v0

    return v0
.end method

.method public final isShortcutKey(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 650986
    iget-object v0, p0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v0, LX/3qu;

    invoke-interface {v0, p1, p2}, LX/3qu;->isShortcutKey(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public final performIdentifierAction(II)Z
    .locals 1

    .prologue
    .line 650985
    iget-object v0, p0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v0, LX/3qu;

    invoke-interface {v0, p1, p2}, LX/3qu;->performIdentifierAction(II)Z

    move-result v0

    return v0
.end method

.method public final performShortcut(ILandroid/view/KeyEvent;I)Z
    .locals 1

    .prologue
    .line 650959
    iget-object v0, p0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v0, LX/3qu;

    invoke-interface {v0, p1, p2, p3}, LX/3qu;->performShortcut(ILandroid/view/KeyEvent;I)Z

    move-result v0

    return v0
.end method

.method public final removeGroup(I)V
    .locals 2

    .prologue
    .line 650977
    iget-object v0, p0, LX/3uv;->c:Ljava/util/Map;

    if-nez v0, :cond_1

    .line 650978
    :cond_0
    iget-object v0, p0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v0, LX/3qu;

    invoke-interface {v0, p1}, LX/3qu;->removeGroup(I)V

    .line 650979
    return-void

    .line 650980
    :cond_1
    iget-object v0, p0, LX/3uv;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 650981
    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 650982
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/MenuItem;

    .line 650983
    invoke-interface {v0}, Landroid/view/MenuItem;->getGroupId()I

    move-result v0

    if-ne p1, v0, :cond_2

    .line 650984
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0
.end method

.method public final removeItem(I)V
    .locals 2

    .prologue
    .line 650969
    iget-object v0, p0, LX/3uv;->c:Ljava/util/Map;

    if-nez v0, :cond_1

    .line 650970
    :cond_0
    :goto_0
    iget-object v0, p0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v0, LX/3qu;

    invoke-interface {v0, p1}, LX/3qu;->removeItem(I)V

    .line 650971
    return-void

    .line 650972
    :cond_1
    iget-object v0, p0, LX/3uv;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 650973
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 650974
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/MenuItem;

    .line 650975
    invoke-interface {v0}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    if-ne p1, v0, :cond_2

    .line 650976
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0
.end method

.method public final setGroupCheckable(IZZ)V
    .locals 1

    .prologue
    .line 650967
    iget-object v0, p0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v0, LX/3qu;

    invoke-interface {v0, p1, p2, p3}, LX/3qu;->setGroupCheckable(IZZ)V

    .line 650968
    return-void
.end method

.method public final setGroupEnabled(IZ)V
    .locals 1

    .prologue
    .line 650965
    iget-object v0, p0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v0, LX/3qu;

    invoke-interface {v0, p1, p2}, LX/3qu;->setGroupEnabled(IZ)V

    .line 650966
    return-void
.end method

.method public final setGroupVisible(IZ)V
    .locals 1

    .prologue
    .line 650963
    iget-object v0, p0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v0, LX/3qu;

    invoke-interface {v0, p1, p2}, LX/3qu;->setGroupVisible(IZ)V

    .line 650964
    return-void
.end method

.method public final setQwertyMode(Z)V
    .locals 1

    .prologue
    .line 650961
    iget-object v0, p0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v0, LX/3qu;

    invoke-interface {v0, p1}, LX/3qu;->setQwertyMode(Z)V

    .line 650962
    return-void
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 650960
    iget-object v0, p0, LX/3uu;->b:Ljava/lang/Object;

    check-cast v0, LX/3qu;

    invoke-interface {v0}, LX/3qu;->size()I

    move-result v0

    return v0
.end method
