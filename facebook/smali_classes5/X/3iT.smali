.class public LX/3iT;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/util/regex/Pattern;


# instance fields
.field private final b:Ljava/util/Locale;

.field private final c:LX/2Rd;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 629578
    const-string v0, " +"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/3iT;->a:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Ljava/util/Locale;LX/2Rd;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 629573
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 629574
    iput-object p1, p0, LX/3iT;->b:Ljava/util/Locale;

    .line 629575
    iput-object p2, p0, LX/3iT;->c:LX/2Rd;

    .line 629576
    return-void
.end method

.method public static a(LX/0QB;)LX/3iT;
    .locals 1

    .prologue
    .line 629577
    invoke-static {p0}, LX/3iT;->b(LX/0QB;)LX/3iT;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/8cK;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/tagging/model/TaggingProfile;
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 629523
    iget-object v1, p0, LX/8cI;->a:Ljava/lang/String;

    move-object v1, v1

    .line 629524
    :try_start_0
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 629525
    :goto_0
    move-object v1, v5

    .line 629526
    if-nez v1, :cond_0

    .line 629527
    :goto_1
    return-object v0

    :cond_0
    new-instance v2, LX/7Gq;

    invoke-direct {v2}, LX/7Gq;-><init>()V

    new-instance v3, Lcom/facebook/user/model/Name;

    .line 629528
    iget-object v4, p0, LX/8cI;->b:Ljava/lang/String;

    move-object v4, v4

    .line 629529
    invoke-direct {v3, v4, v0, v0}, Lcom/facebook/user/model/Name;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 629530
    iput-object v3, v2, LX/7Gq;->a:Lcom/facebook/user/model/Name;

    .line 629531
    move-object v0, v2

    .line 629532
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 629533
    iput-wide v2, v0, LX/7Gq;->b:J

    .line 629534
    move-object v0, v0

    .line 629535
    iget-object v1, p0, LX/8cK;->c:Ljava/lang/String;

    move-object v1, v1

    .line 629536
    iput-object v1, v0, LX/7Gq;->c:Ljava/lang/String;

    .line 629537
    move-object v0, v0

    .line 629538
    iget-object v1, p0, LX/8cI;->d:Ljava/lang/String;

    move-object v1, v1

    .line 629539
    new-instance v2, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-direct {v2, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Lcom/facebook/tagging/model/TaggingProfile;->a(Lcom/facebook/graphql/enums/GraphQLObjectType;)LX/7Gr;

    move-result-object v2

    move-object v1, v2

    .line 629540
    iput-object v1, v0, LX/7Gq;->e:LX/7Gr;

    .line 629541
    move-object v0, v0

    .line 629542
    iput-object p1, v0, LX/7Gq;->h:Ljava/lang/String;

    .line 629543
    move-object v0, v0

    .line 629544
    iput-object p2, v0, LX/7Gq;->i:Ljava/lang/String;

    .line 629545
    move-object v0, v0

    .line 629546
    invoke-virtual {v0}, LX/7Gq;->l()Lcom/facebook/tagging/model/TaggingProfile;

    move-result-object v0

    goto :goto_1

    .line 629547
    :catch_0
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public static final b(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Z)LX/0Px;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/api/SearchTypeaheadResult;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z)",
            "LX/0Px",
            "<",
            "Lcom/facebook/tagging/model/TaggingProfile;",
            ">;"
        }
    .end annotation

    .prologue
    .line 629548
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 629549
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/api/SearchTypeaheadResult;

    .line 629550
    const/4 v8, 0x0

    .line 629551
    new-instance v5, LX/7Gq;

    invoke-direct {v5}, LX/7Gq;-><init>()V

    new-instance v6, Lcom/facebook/user/model/Name;

    iget-object v7, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->l:Ljava/lang/String;

    invoke-direct {v6, v7, v8, v8}, Lcom/facebook/user/model/Name;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 629552
    iput-object v6, v5, LX/7Gq;->a:Lcom/facebook/user/model/Name;

    .line 629553
    move-object v5, v5

    .line 629554
    iget-wide v7, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->n:J

    .line 629555
    iput-wide v7, v5, LX/7Gq;->b:J

    .line 629556
    move-object v5, v5

    .line 629557
    iget-object v6, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->f:Landroid/net/Uri;

    invoke-virtual {v6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    .line 629558
    iput-object v6, v5, LX/7Gq;->c:Ljava/lang/String;

    .line 629559
    move-object v5, v5

    .line 629560
    invoke-virtual {v0}, Lcom/facebook/search/api/SearchTypeaheadResult;->a()I

    move-result v6

    invoke-static {v6}, Lcom/facebook/tagging/model/TaggingProfile;->a(I)LX/7Gr;

    move-result-object v6

    .line 629561
    iput-object v6, v5, LX/7Gq;->e:LX/7Gr;

    .line 629562
    move-object v5, v5

    .line 629563
    iput-object p1, v5, LX/7Gq;->h:Ljava/lang/String;

    .line 629564
    move-object v5, v5

    .line 629565
    iput-object p2, v5, LX/7Gq;->i:Ljava/lang/String;

    .line 629566
    move-object v5, v5

    .line 629567
    invoke-virtual {v5}, LX/7Gq;->l()Lcom/facebook/tagging/model/TaggingProfile;

    move-result-object v5

    move-object v0, v5

    .line 629568
    if-nez p3, :cond_1

    .line 629569
    iget-object v3, v0, Lcom/facebook/tagging/model/TaggingProfile;->e:LX/7Gr;

    move-object v3, v3

    .line 629570
    sget-object v4, LX/7Gr;->UNKNOWN:LX/7Gr;

    if-eq v3, v4, :cond_0

    .line 629571
    :cond_1
    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 629572
    :cond_2
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/3iT;
    .locals 3

    .prologue
    .line 629521
    new-instance v2, LX/3iT;

    invoke-static {p0}, LX/0eD;->b(LX/0QB;)Ljava/util/Locale;

    move-result-object v0

    check-cast v0, Ljava/util/Locale;

    invoke-static {p0}, LX/2Rd;->b(LX/0QB;)LX/2Rd;

    move-result-object v1

    check-cast v1, LX/2Rd;

    invoke-direct {v2, v0, v1}, LX/3iT;-><init>(Ljava/util/Locale;LX/2Rd;)V

    .line 629522
    return-object v2
.end method


# virtual methods
.method public final a(Lcom/facebook/user/model/Name;JLjava/lang/String;LX/7Gr;)Lcom/facebook/tagging/model/TaggingProfile;
    .locals 8

    .prologue
    .line 629520
    const/4 v6, 0x0

    const-string v7, ""

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v7}, LX/3iT;->a(Lcom/facebook/user/model/Name;JLjava/lang/String;LX/7Gr;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/tagging/model/TaggingProfile;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/user/model/Name;JLjava/lang/String;LX/7Gr;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/tagging/model/TaggingProfile;
    .locals 10

    .prologue
    .line 629519
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-virtual/range {v0 .. v8}, LX/3iT;->a(Lcom/facebook/user/model/Name;JLjava/lang/String;LX/7Gr;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/tagging/model/TaggingProfile;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/user/model/Name;JLjava/lang/String;LX/7Gr;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/tagging/model/TaggingProfile;
    .locals 2
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 629490
    invoke-virtual/range {p0 .. p8}, LX/3iT;->b(Lcom/facebook/user/model/Name;JLjava/lang/String;LX/7Gr;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/7Gq;

    move-result-object v0

    invoke-virtual {v0}, LX/7Gq;->l()Lcom/facebook/tagging/model/TaggingProfile;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/facebook/user/model/Name;JLjava/lang/String;LX/7Gr;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/7Gq;
    .locals 5
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 629491
    invoke-virtual {p1}, Lcom/facebook/user/model/Name;->i()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/3iT;->b:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 629492
    iget-object v1, p0, LX/3iT;->c:LX/2Rd;

    invoke-virtual {v1, v0}, LX/2Rd;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 629493
    new-instance v2, LX/7Gq;

    invoke-direct {v2}, LX/7Gq;-><init>()V

    .line 629494
    iput-object p1, v2, LX/7Gq;->a:Lcom/facebook/user/model/Name;

    .line 629495
    move-object v2, v2

    .line 629496
    iput-wide p2, v2, LX/7Gq;->b:J

    .line 629497
    move-object v2, v2

    .line 629498
    const/4 v3, 0x0

    .line 629499
    if-nez p4, :cond_1

    move-object p4, v3

    .line 629500
    :cond_0
    :goto_0
    move-object v3, p4

    .line 629501
    iput-object v3, v2, LX/7Gq;->c:Ljava/lang/String;

    .line 629502
    move-object v2, v2

    .line 629503
    iput-object p5, v2, LX/7Gq;->e:LX/7Gr;

    .line 629504
    move-object v2, v2

    .line 629505
    iput-object p6, v2, LX/7Gq;->d:Ljava/lang/String;

    .line 629506
    move-object v2, v2

    .line 629507
    iput-object v0, v2, LX/7Gq;->f:Ljava/lang/String;

    .line 629508
    move-object v0, v2

    .line 629509
    iput-object v1, v0, LX/7Gq;->g:Ljava/lang/String;

    .line 629510
    move-object v0, v0

    .line 629511
    iput-object p7, v0, LX/7Gq;->h:Ljava/lang/String;

    .line 629512
    move-object v0, v0

    .line 629513
    iput-object p8, v0, LX/7Gq;->i:Ljava/lang/String;

    .line 629514
    move-object v0, v0

    .line 629515
    return-object v0

    .line 629516
    :cond_1
    invoke-static {p4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 629517
    invoke-virtual {v4}, Landroid/net/Uri;->isAbsolute()Z

    move-result v4

    if-nez v4, :cond_0

    move-object p4, v3

    .line 629518
    goto :goto_0
.end method
