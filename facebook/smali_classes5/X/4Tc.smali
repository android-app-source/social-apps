.class public LX/4Tc;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 719078
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    .line 719079
    const-wide/16 v8, 0x0

    .line 719080
    const/4 v7, 0x0

    .line 719081
    const/4 v6, 0x0

    .line 719082
    const/4 v3, 0x0

    .line 719083
    const-wide/16 v4, 0x0

    .line 719084
    const/4 v2, 0x0

    .line 719085
    const/4 v1, 0x0

    .line 719086
    const/4 v0, 0x0

    .line 719087
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->START_OBJECT:LX/15z;

    if-eq v10, v11, :cond_b

    .line 719088
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 719089
    const/4 v0, 0x0

    .line 719090
    :goto_0
    return v0

    .line 719091
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v0

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v0, v4, :cond_8

    .line 719092
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v0

    .line 719093
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 719094
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_0

    if-eqz v0, :cond_0

    .line 719095
    const-string v4, "end_time"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 719096
    const/4 v0, 0x1

    .line 719097
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    move v1, v0

    goto :goto_1

    .line 719098
    :cond_1
    const-string v4, "frames"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 719099
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 719100
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->START_ARRAY:LX/15z;

    if-ne v4, v5, :cond_2

    .line 719101
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_ARRAY:LX/15z;

    if-eq v4, v5, :cond_2

    .line 719102
    invoke-static {p0, p1}, LX/4Tb;->a(LX/15w;LX/186;)I

    move-result v4

    .line 719103
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 719104
    :cond_2
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    move v0, v0

    .line 719105
    move v12, v0

    goto :goto_1

    .line 719106
    :cond_3
    const-string v4, "id"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 719107
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    move v11, v0

    goto :goto_1

    .line 719108
    :cond_4
    const-string v4, "name"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 719109
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    move v10, v0

    goto :goto_1

    .line 719110
    :cond_5
    const-string v4, "start_time"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 719111
    const/4 v0, 0x1

    .line 719112
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v4

    move v6, v0

    move-wide v8, v4

    goto/16 :goto_1

    .line 719113
    :cond_6
    const-string v4, "url"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 719114
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    move v7, v0

    goto/16 :goto_1

    .line 719115
    :cond_7
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 719116
    :cond_8
    const/4 v0, 0x6

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 719117
    if-eqz v1, :cond_9

    .line 719118
    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 719119
    :cond_9
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v12}, LX/186;->b(II)V

    .line 719120
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v11}, LX/186;->b(II)V

    .line 719121
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 719122
    if-eqz v6, :cond_a

    .line 719123
    const/4 v1, 0x4

    const-wide/16 v4, 0x0

    move-object v0, p1

    move-wide v2, v8

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 719124
    :cond_a
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 719125
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :cond_b
    move v10, v3

    move v11, v6

    move v12, v7

    move v6, v0

    move v7, v2

    move-wide v2, v8

    move-wide v8, v4

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 719126
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 719127
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 719128
    cmp-long v2, v0, v4

    if-eqz v2, :cond_0

    .line 719129
    const-string v2, "end_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 719130
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 719131
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 719132
    if-eqz v0, :cond_2

    .line 719133
    const-string v1, "frames"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 719134
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 719135
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 719136
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/4Tb;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 719137
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 719138
    :cond_1
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 719139
    :cond_2
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 719140
    if-eqz v0, :cond_3

    .line 719141
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 719142
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 719143
    :cond_3
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 719144
    if-eqz v0, :cond_4

    .line 719145
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 719146
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 719147
    :cond_4
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 719148
    cmp-long v2, v0, v4

    if-eqz v2, :cond_5

    .line 719149
    const-string v2, "start_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 719150
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 719151
    :cond_5
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 719152
    if-eqz v0, :cond_6

    .line 719153
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 719154
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 719155
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 719156
    return-void
.end method
