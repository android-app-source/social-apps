.class public LX/435;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile b:LX/435;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 668369
    const-class v0, LX/435;

    sput-object v0, LX/435;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 668382
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/435;
    .locals 3

    .prologue
    .line 668370
    sget-object v0, LX/435;->b:LX/435;

    if-nez v0, :cond_1

    .line 668371
    const-class v1, LX/435;

    monitor-enter v1

    .line 668372
    :try_start_0
    sget-object v0, LX/435;->b:LX/435;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 668373
    if-eqz v2, :cond_0

    .line 668374
    :try_start_1
    new-instance v0, LX/435;

    invoke-direct {v0}, LX/435;-><init>()V

    .line 668375
    move-object v0, v0

    .line 668376
    sput-object v0, LX/435;->b:LX/435;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 668377
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 668378
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 668379
    :cond_1
    sget-object v0, LX/435;->b:LX/435;

    return-object v0

    .line 668380
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 668381
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
