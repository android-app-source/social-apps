.class public final LX/3ob;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Landroid/support/design/widget/TextInputLayout;


# direct methods
.method public constructor <init>(Landroid/support/design/widget/TextInputLayout;)V
    .locals 0

    .prologue
    .line 640470
    iput-object p1, p0, LX/3ob;->a:Landroid/support/design/widget/TextInputLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .prologue
    .line 640471
    iget-object v0, p0, LX/3ob;->a:Landroid/support/design/widget/TextInputLayout;

    const/4 v1, 0x1

    .line 640472
    invoke-static {v0, v1}, Landroid/support/design/widget/TextInputLayout;->a$redex0(Landroid/support/design/widget/TextInputLayout;Z)V

    .line 640473
    iget-object v0, p0, LX/3ob;->a:Landroid/support/design/widget/TextInputLayout;

    iget-boolean v0, v0, Landroid/support/design/widget/TextInputLayout;->i:Z

    if-eqz v0, :cond_0

    .line 640474
    iget-object v0, p0, LX/3ob;->a:Landroid/support/design/widget/TextInputLayout;

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v1

    .line 640475
    invoke-static {v0, v1}, Landroid/support/design/widget/TextInputLayout;->a$redex0(Landroid/support/design/widget/TextInputLayout;I)V

    .line 640476
    :cond_0
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 640477
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 640478
    return-void
.end method
