.class public final LX/3gh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2ZE;


# instance fields
.field public final synthetic a:LX/3gf;


# direct methods
.method public constructor <init>(LX/3gf;)V
    .locals 0

    .prologue
    .line 624696
    iput-object p1, p0, LX/3gh;->a:LX/3gf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Iterable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "LX/2Vj;",
            ">;"
        }
    .end annotation

    .prologue
    .line 624708
    iget-object v0, p0, LX/3gh;->a:LX/3gf;

    iget-object v0, v0, LX/3gf;->b:LX/3gg;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v0

    const-string v1, "getLoggedInUserProfilePicture"

    .line 624709
    iput-object v1, v0, LX/2Vk;->c:Ljava/lang/String;

    .line 624710
    move-object v0, v0

    .line 624711
    invoke-virtual {v0}, LX/2Vk;->a()LX/2Vj;

    move-result-object v0

    .line 624712
    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 624697
    const-string v0, "getLoggedInUserProfilePicture"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/PicSquare;

    .line 624698
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/user/model/PicSquare;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/user/model/PicSquare;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/user/model/PicSquareUrlWithSize;

    iget-object v1, v1, Lcom/facebook/user/model/PicSquareUrlWithSize;->url:Ljava/lang/String;

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 624699
    iget-object v1, p0, LX/3gh;->a:LX/3gf;

    iget-object v1, v1, LX/3gf;->a:LX/0WJ;

    invoke-virtual {v1}, LX/0WJ;->c()Lcom/facebook/user/model/User;

    move-result-object v1

    .line 624700
    if-eqz v1, :cond_0

    .line 624701
    new-instance v2, LX/0XI;

    invoke-direct {v2}, LX/0XI;-><init>()V

    .line 624702
    invoke-virtual {v2, v1}, LX/0XI;->a(Lcom/facebook/user/model/User;)LX/0XI;

    .line 624703
    invoke-virtual {v0}, Lcom/facebook/user/model/PicSquare;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/user/model/PicSquareUrlWithSize;

    iget-object v1, v1, Lcom/facebook/user/model/PicSquareUrlWithSize;->url:Ljava/lang/String;

    .line 624704
    iput-object v1, v2, LX/0XI;->n:Ljava/lang/String;

    .line 624705
    iput-object v0, v2, LX/0XI;->p:Lcom/facebook/user/model/PicSquare;

    .line 624706
    iget-object v0, p0, LX/3gh;->a:LX/3gf;

    iget-object v0, v0, LX/3gf;->a:LX/0WJ;

    invoke-virtual {v2}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0WJ;->c(Lcom/facebook/user/model/User;)V

    .line 624707
    :cond_0
    return-void
.end method
