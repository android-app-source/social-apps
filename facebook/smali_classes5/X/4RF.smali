.class public LX/4RF;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 708267
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 708261
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 708262
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 708263
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 708264
    invoke-static {p0, p1}, LX/4RF;->b(LX/15w;LX/186;)I

    move-result v1

    .line 708265
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 708266
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 708238
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 708239
    invoke-virtual {p0, p1, v2, v2}, LX/15i;->a(III)I

    move-result v0

    .line 708240
    if-eqz v0, :cond_0

    .line 708241
    const-string v1, "col"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 708242
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 708243
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 708244
    if-eqz v0, :cond_1

    .line 708245
    const-string v1, "face"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 708246
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 708247
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 708248
    if-eqz v0, :cond_2

    .line 708249
    const-string v1, "level"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 708250
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 708251
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 708252
    if-eqz v0, :cond_3

    .line 708253
    const-string v1, "row"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 708254
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 708255
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 708256
    if-eqz v0, :cond_4

    .line 708257
    const-string v1, "uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 708258
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 708259
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 708260
    return-void
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 708203
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 708204
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 708205
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2}, LX/4RF;->a(LX/15i;ILX/0nX;)V

    .line 708206
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 708207
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 708208
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 708209
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_b

    .line 708210
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 708211
    :goto_0
    return v1

    .line 708212
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_6

    .line 708213
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 708214
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 708215
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_0

    if-eqz v11, :cond_0

    .line 708216
    const-string v12, "col"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 708217
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v5

    move v10, v5

    move v5, v2

    goto :goto_1

    .line 708218
    :cond_1
    const-string v12, "face"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 708219
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v4

    move v9, v4

    move v4, v2

    goto :goto_1

    .line 708220
    :cond_2
    const-string v12, "level"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 708221
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v3

    move v8, v3

    move v3, v2

    goto :goto_1

    .line 708222
    :cond_3
    const-string v12, "row"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 708223
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v7, v0

    move v0, v2

    goto :goto_1

    .line 708224
    :cond_4
    const-string v12, "uri"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 708225
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 708226
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 708227
    :cond_6
    const/4 v11, 0x5

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 708228
    if-eqz v5, :cond_7

    .line 708229
    invoke-virtual {p1, v1, v10, v1}, LX/186;->a(III)V

    .line 708230
    :cond_7
    if-eqz v4, :cond_8

    .line 708231
    invoke-virtual {p1, v2, v9, v1}, LX/186;->a(III)V

    .line 708232
    :cond_8
    if-eqz v3, :cond_9

    .line 708233
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v8, v1}, LX/186;->a(III)V

    .line 708234
    :cond_9
    if-eqz v0, :cond_a

    .line 708235
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v7, v1}, LX/186;->a(III)V

    .line 708236
    :cond_a
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 708237
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_b
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    move v9, v1

    move v10, v1

    goto/16 :goto_1
.end method
