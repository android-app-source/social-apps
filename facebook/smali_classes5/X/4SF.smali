.class public LX/4SF;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 712054
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 11

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 712011
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_8

    .line 712012
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 712013
    :goto_0
    return v1

    .line 712014
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_5

    .line 712015
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 712016
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 712017
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_0

    if-eqz v8, :cond_0

    .line 712018
    const-string v9, "is_request"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 712019
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v3

    move v7, v3

    move v3, v2

    goto :goto_1

    .line 712020
    :cond_1
    const-string v9, "is_seen"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 712021
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v6, v0

    move v0, v2

    goto :goto_1

    .line 712022
    :cond_2
    const-string v9, "requester"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 712023
    invoke-static {p0, p1}, LX/2bO;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 712024
    :cond_3
    const-string v9, "suggesters"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 712025
    invoke-static {p0, p1}, LX/2bO;->b(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 712026
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 712027
    :cond_5
    const/4 v8, 0x4

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 712028
    if-eqz v3, :cond_6

    .line 712029
    invoke-virtual {p1, v1, v7}, LX/186;->a(IZ)V

    .line 712030
    :cond_6
    if-eqz v0, :cond_7

    .line 712031
    invoke-virtual {p1, v2, v6}, LX/186;->a(IZ)V

    .line 712032
    :cond_7
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 712033
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 712034
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_8
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    goto :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 712035
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 712036
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 712037
    if-eqz v0, :cond_0

    .line 712038
    const-string v1, "is_request"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 712039
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 712040
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 712041
    if-eqz v0, :cond_1

    .line 712042
    const-string v1, "is_seen"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 712043
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 712044
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 712045
    if-eqz v0, :cond_2

    .line 712046
    const-string v1, "requester"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 712047
    invoke-static {p0, v0, p2, p3}, LX/2bO;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 712048
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 712049
    if-eqz v0, :cond_3

    .line 712050
    const-string v1, "suggesters"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 712051
    invoke-static {p0, v0, p2, p3}, LX/2bO;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 712052
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 712053
    return-void
.end method
