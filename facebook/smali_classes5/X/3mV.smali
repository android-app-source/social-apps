.class public LX/3mV;
.super LX/3mW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/3mW",
        "<",
        "LX/3mR;",
        "TE;>;"
    }
.end annotation


# instance fields
.field private final c:LX/3mZ;

.field private final d:LX/3ma;

.field private final e:LX/3mb;

.field private final f:LX/3mc;

.field private final g:LX/1Po;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Px;LX/3mU;LX/1Po;Lcom/facebook/feed/rows/core/props/FeedProps;LX/25M;LX/3mZ;LX/3ma;LX/3mb;LX/3mc;)V
    .locals 8
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/3mU;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/1Po;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/25M;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Px",
            "<",
            "LX/3mR;",
            ">;",
            "LX/3mU;",
            "TE;",
            "Lcom/facebook/feed/rows/core/props/FeedProps;",
            "LX/25M;",
            "LX/3mZ;",
            "LX/3ma;",
            "LX/3mb;",
            "LX/3mc;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 636437
    move-object v5, p4

    check-cast v5, LX/1Pq;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v1 .. v7}, LX/3mW;-><init>(Landroid/content/Context;LX/0Px;LX/3mU;LX/1Pq;Lcom/facebook/feed/rows/core/props/FeedProps;LX/25M;)V

    .line 636438
    iput-object p7, p0, LX/3mV;->c:LX/3mZ;

    .line 636439
    move-object/from16 v0, p8

    iput-object v0, p0, LX/3mV;->d:LX/3ma;

    .line 636440
    move-object/from16 v0, p9

    iput-object v0, p0, LX/3mV;->e:LX/3mb;

    .line 636441
    move-object/from16 v0, p10

    iput-object v0, p0, LX/3mV;->f:LX/3mc;

    .line 636442
    iput-object p4, p0, LX/3mV;->g:LX/1Po;

    .line 636443
    return-void
.end method


# virtual methods
.method public final a(LX/1De;)LX/1X1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 636382
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;Ljava/lang/Object;)LX/1X1;
    .locals 6

    .prologue
    .line 636384
    check-cast p2, LX/3mR;

    const/4 v3, 0x0

    const/4 v0, 0x0

    .line 636385
    iget v1, p2, LX/3mR;->d:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 636386
    iget-object v0, p0, LX/3mV;->d:LX/3ma;

    invoke-virtual {v0, p1}, LX/3ma;->c(LX/1De;)LX/DCu;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/DCu;->h(I)LX/DCu;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 636387
    :cond_0
    :goto_0
    return-object v0

    .line 636388
    :cond_1
    iget v1, p2, LX/3mR;->d:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_3

    .line 636389
    iget-object v1, p2, LX/3mR;->a:Lcom/facebook/graphql/model/GroupsYouShouldJoinFeedUnit;

    invoke-static {v1}, LX/3mQ;->a(Lcom/facebook/graphql/model/GroupsYouShouldJoinFeedUnit;)Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v1

    .line 636390
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 636391
    iget-object v2, p0, LX/3mV;->e:LX/3mb;

    invoke-virtual {v2, p1}, LX/3mb;->c(LX/1De;)LX/DCx;

    move-result-object v2

    invoke-virtual {v2, v3}, LX/DCx;->h(I)LX/DCx;

    move-result-object v2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGroup;->t()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/DCx;->b(Ljava/lang/String;)LX/DCx;

    move-result-object v2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGroup;->v()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/DCx;->c(Ljava/lang/String;)LX/DCx;

    move-result-object v2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGroup;->m()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGroup;->m()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGroup;->m()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGroup;->m()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    :cond_2
    invoke-virtual {v2, v0}, LX/DCx;->d(Ljava/lang/String;)LX/DCx;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    goto :goto_0

    .line 636392
    :cond_3
    iget v1, p2, LX/3mR;->d:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_6

    .line 636393
    iget-object v1, p2, LX/3mR;->a:Lcom/facebook/graphql/model/GroupsYouShouldJoinFeedUnit;

    invoke-static {v1}, LX/3mQ;->b(Lcom/facebook/graphql/model/GroupsYouShouldJoinFeedUnit;)Lcom/facebook/graphql/model/GraphQLGroupsFeedUnitCoverItem;

    move-result-object v1

    .line 636394
    iget-object v2, p2, LX/3mR;->a:Lcom/facebook/graphql/model/GroupsYouShouldJoinFeedUnit;

    invoke-static {v2}, LX/3mQ;->a(Lcom/facebook/graphql/model/GroupsYouShouldJoinFeedUnit;)Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v2

    .line 636395
    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGroupsFeedUnitCoverItem;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 636396
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGroupsFeedUnitCoverItem;->j()Ljava/lang/String;

    move-result-object v0

    .line 636397
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGroupsFeedUnitCoverItem;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v2

    .line 636398
    iget-object v3, p0, LX/3mV;->f:LX/3mc;

    const/4 v4, 0x0

    .line 636399
    new-instance v5, LX/DCt;

    invoke-direct {v5, v3}, LX/DCt;-><init>(LX/3mc;)V

    .line 636400
    sget-object p0, LX/3mc;->a:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/DCs;

    .line 636401
    if-nez p0, :cond_4

    .line 636402
    new-instance p0, LX/DCs;

    invoke-direct {p0}, LX/DCs;-><init>()V

    .line 636403
    :cond_4
    invoke-static {p0, p1, v4, v4, v5}, LX/DCs;->a$redex0(LX/DCs;LX/1De;IILX/DCt;)V

    .line 636404
    move-object v5, p0

    .line 636405
    move-object v4, v5

    .line 636406
    move-object v3, v4

    .line 636407
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    const/4 v0, -0x1

    .line 636408
    :goto_1
    iget-object v4, v3, LX/DCs;->a:LX/DCt;

    iput v0, v4, LX/DCt;->a:I

    .line 636409
    iget-object v4, v3, LX/DCs;->d:Ljava/util/BitSet;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/util/BitSet;->set(I)V

    .line 636410
    move-object v0, v3

    .line 636411
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGroupsFeedUnitCoverItem;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    .line 636412
    iget-object v3, v0, LX/DCs;->a:LX/DCt;

    iput-object v1, v3, LX/DCt;->b:Ljava/lang/String;

    .line 636413
    iget-object v3, v0, LX/DCs;->d:Ljava/util/BitSet;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Ljava/util/BitSet;->set(I)V

    .line 636414
    move-object v0, v0

    .line 636415
    iget-object v1, v0, LX/DCs;->a:LX/DCt;

    iput-object v2, v1, LX/DCt;->c:Ljava/lang/String;

    .line 636416
    iget-object v1, v0, LX/DCs;->d:Ljava/util/BitSet;

    const/4 v3, 0x2

    invoke-virtual {v1, v3}, Ljava/util/BitSet;->set(I)V

    .line 636417
    move-object v0, v0

    .line 636418
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    goto/16 :goto_0

    :cond_5
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "#"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 636419
    :cond_6
    iget v1, p2, LX/3mR;->d:I

    if-nez v1, :cond_0

    .line 636420
    iget-object v0, p0, LX/3mV;->c:LX/3mZ;

    const/4 v1, 0x0

    .line 636421
    new-instance v2, LX/3mr;

    invoke-direct {v2, v0}, LX/3mr;-><init>(LX/3mZ;)V

    .line 636422
    iget-object v3, v0, LX/3mZ;->b:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/3ms;

    .line 636423
    if-nez v3, :cond_7

    .line 636424
    new-instance v3, LX/3ms;

    invoke-direct {v3, v0}, LX/3ms;-><init>(LX/3mZ;)V

    .line 636425
    :cond_7
    invoke-static {v3, p1, v1, v1, v2}, LX/3ms;->a$redex0(LX/3ms;LX/1De;IILX/3mr;)V

    .line 636426
    move-object v2, v3

    .line 636427
    move-object v1, v2

    .line 636428
    move-object v0, v1

    .line 636429
    iget-object v1, v0, LX/3ms;->a:LX/3mr;

    iput-object p2, v1, LX/3mr;->a:LX/3mR;

    .line 636430
    iget-object v1, v0, LX/3ms;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 636431
    move-object v0, v0

    .line 636432
    iget-object v1, p0, LX/3mV;->g:LX/1Po;

    .line 636433
    iget-object v2, v0, LX/3ms;->a:LX/3mr;

    iput-object v1, v2, LX/3mr;->b:LX/1Po;

    .line 636434
    iget-object v2, v0, LX/3ms;->e:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 636435
    move-object v0, v0

    .line 636436
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 636383
    const/4 v0, 0x0

    return v0
.end method
