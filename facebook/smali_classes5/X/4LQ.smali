.class public LX/4LQ;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 683052
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 683053
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 683054
    invoke-virtual {p0, p1, v2}, LX/15i;->b(II)Z

    move-result v0

    .line 683055
    if-eqz v0, :cond_0

    .line 683056
    const-string v1, "can_override"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 683057
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 683058
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 683059
    if-eqz v0, :cond_1

    .line 683060
    const-string v1, "description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 683061
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 683062
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 683063
    if-eqz v0, :cond_2

    .line 683064
    const-string v1, "enabled"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 683065
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 683066
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 683067
    if-eqz v0, :cond_3

    .line 683068
    const-string v1, "hash_string"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 683069
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 683070
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 683071
    if-eqz v0, :cond_4

    .line 683072
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 683073
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 683074
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 683075
    if-eqz v0, :cond_5

    .line 683076
    const-string v1, "override_details"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 683077
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 683078
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 683079
    if-eqz v0, :cond_6

    .line 683080
    const-string v1, "time_loaded"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 683081
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 683082
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 683083
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 13

    .prologue
    .line 683084
    const/4 v9, 0x0

    .line 683085
    const/4 v8, 0x0

    .line 683086
    const/4 v7, 0x0

    .line 683087
    const/4 v6, 0x0

    .line 683088
    const/4 v5, 0x0

    .line 683089
    const/4 v4, 0x0

    .line 683090
    const/4 v3, 0x0

    .line 683091
    const/4 v2, 0x0

    .line 683092
    const/4 v1, 0x0

    .line 683093
    const/4 v0, 0x0

    .line 683094
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->START_OBJECT:LX/15z;

    if-eq v10, v11, :cond_1

    .line 683095
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 683096
    const/4 v0, 0x0

    .line 683097
    :goto_0
    return v0

    .line 683098
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 683099
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_8

    .line 683100
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 683101
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 683102
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_1

    if-eqz v10, :cond_1

    .line 683103
    const-string v11, "can_override"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 683104
    const/4 v2, 0x1

    .line 683105
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v9

    goto :goto_1

    .line 683106
    :cond_2
    const-string v11, "description"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 683107
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 683108
    :cond_3
    const-string v11, "enabled"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 683109
    const/4 v1, 0x1

    .line 683110
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v7

    goto :goto_1

    .line 683111
    :cond_4
    const-string v11, "hash_string"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 683112
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 683113
    :cond_5
    const-string v11, "name"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 683114
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 683115
    :cond_6
    const-string v11, "override_details"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 683116
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 683117
    :cond_7
    const-string v11, "time_loaded"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 683118
    const/4 v0, 0x1

    .line 683119
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v3

    goto/16 :goto_1

    .line 683120
    :cond_8
    const/4 v10, 0x7

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 683121
    if-eqz v2, :cond_9

    .line 683122
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v9}, LX/186;->a(IZ)V

    .line 683123
    :cond_9
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v8}, LX/186;->b(II)V

    .line 683124
    if-eqz v1, :cond_a

    .line 683125
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v7}, LX/186;->a(IZ)V

    .line 683126
    :cond_a
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 683127
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 683128
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 683129
    if-eqz v0, :cond_b

    .line 683130
    const/4 v0, 0x6

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v3, v1}, LX/186;->a(III)V

    .line 683131
    :cond_b
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0
.end method
