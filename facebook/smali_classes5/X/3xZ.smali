.class public final LX/3xZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3us;


# instance fields
.field public a:LX/3v0;

.field public b:LX/3v3;

.field public final synthetic c:Landroid/support/v7/widget/Toolbar;


# direct methods
.method public constructor <init>(Landroid/support/v7/widget/Toolbar;)V
    .locals 0

    .prologue
    .line 658845
    iput-object p1, p0, LX/3xZ;->c:Landroid/support/v7/widget/Toolbar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/support/v7/widget/Toolbar;B)V
    .locals 0

    .prologue
    .line 658846
    invoke-direct {p0, p1}, LX/3xZ;-><init>(Landroid/support/v7/widget/Toolbar;)V

    return-void
.end method


# virtual methods
.method public final a(LX/3v0;Z)V
    .locals 0

    .prologue
    .line 658847
    return-void
.end method

.method public final a(Landroid/content/Context;LX/3v0;)V
    .locals 2

    .prologue
    .line 658848
    iget-object v0, p0, LX/3xZ;->a:LX/3v0;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3xZ;->b:LX/3v3;

    if-eqz v0, :cond_0

    .line 658849
    iget-object v0, p0, LX/3xZ;->a:LX/3v0;

    iget-object v1, p0, LX/3xZ;->b:LX/3v3;

    invoke-virtual {v0, v1}, LX/3v0;->b(LX/3v3;)Z

    .line 658850
    :cond_0
    iput-object p2, p0, LX/3xZ;->a:LX/3v0;

    .line 658851
    return-void
.end method

.method public final a(LX/3vG;)Z
    .locals 1

    .prologue
    .line 658852
    const/4 v0, 0x0

    return v0
.end method

.method public final b(Z)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 658853
    iget-object v1, p0, LX/3xZ;->b:LX/3v3;

    if-eqz v1, :cond_1

    .line 658854
    iget-object v1, p0, LX/3xZ;->a:LX/3v0;

    if-eqz v1, :cond_0

    .line 658855
    iget-object v1, p0, LX/3xZ;->a:LX/3v0;

    invoke-virtual {v1}, LX/3v0;->size()I

    move-result v2

    move v1, v0

    .line 658856
    :goto_0
    if-ge v1, v2, :cond_0

    .line 658857
    iget-object v3, p0, LX/3xZ;->a:LX/3v0;

    invoke-virtual {v3, v1}, LX/3v0;->getItem(I)Landroid/view/MenuItem;

    move-result-object v3

    .line 658858
    iget-object v4, p0, LX/3xZ;->b:LX/3v3;

    if-ne v3, v4, :cond_2

    .line 658859
    const/4 v0, 0x1

    .line 658860
    :cond_0
    if-nez v0, :cond_1

    .line 658861
    iget-object v0, p0, LX/3xZ;->b:LX/3v3;

    invoke-virtual {p0, v0}, LX/3xZ;->c(LX/3v3;)Z

    .line 658862
    :cond_1
    return-void

    .line 658863
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 658864
    const/4 v0, 0x0

    return v0
.end method

.method public final b(LX/3v3;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 658865
    iget-object v0, p0, LX/3xZ;->c:Landroid/support/v7/widget/Toolbar;

    invoke-static {v0}, Landroid/support/v7/widget/Toolbar;->n(Landroid/support/v7/widget/Toolbar;)V

    .line 658866
    iget-object v0, p0, LX/3xZ;->c:Landroid/support/v7/widget/Toolbar;

    iget-object v0, v0, Landroid/support/v7/widget/Toolbar;->i:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    iget-object v1, p0, LX/3xZ;->c:Landroid/support/v7/widget/Toolbar;

    if-eq v0, v1, :cond_0

    .line 658867
    iget-object v0, p0, LX/3xZ;->c:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, LX/3xZ;->c:Landroid/support/v7/widget/Toolbar;

    iget-object v1, v1, Landroid/support/v7/widget/Toolbar;->i:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->addView(Landroid/view/View;)V

    .line 658868
    :cond_0
    iget-object v0, p0, LX/3xZ;->c:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {p1}, LX/3v3;->getActionView()Landroid/view/View;

    move-result-object v1

    iput-object v1, v0, Landroid/support/v7/widget/Toolbar;->a:Landroid/view/View;

    .line 658869
    iput-object p1, p0, LX/3xZ;->b:LX/3v3;

    .line 658870
    iget-object v0, p0, LX/3xZ;->c:Landroid/support/v7/widget/Toolbar;

    iget-object v0, v0, Landroid/support/v7/widget/Toolbar;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    iget-object v1, p0, LX/3xZ;->c:Landroid/support/v7/widget/Toolbar;

    if-eq v0, v1, :cond_1

    .line 658871
    invoke-static {}, Landroid/support/v7/widget/Toolbar;->i()LX/3xa;

    move-result-object v0

    .line 658872
    const v1, 0x800003

    iget-object v2, p0, LX/3xZ;->c:Landroid/support/v7/widget/Toolbar;

    iget v2, v2, Landroid/support/v7/widget/Toolbar;->n:I

    and-int/lit8 v2, v2, 0x70

    or-int/2addr v1, v2

    iput v1, v0, LX/3xa;->a:I

    .line 658873
    const/4 v1, 0x2

    iput v1, v0, LX/3xa;->b:I

    .line 658874
    iget-object v1, p0, LX/3xZ;->c:Landroid/support/v7/widget/Toolbar;

    iget-object v1, v1, Landroid/support/v7/widget/Toolbar;->a:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 658875
    iget-object v0, p0, LX/3xZ;->c:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, LX/3xZ;->c:Landroid/support/v7/widget/Toolbar;

    iget-object v1, v1, Landroid/support/v7/widget/Toolbar;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->addView(Landroid/view/View;)V

    .line 658876
    :cond_1
    iget-object v0, p0, LX/3xZ;->c:Landroid/support/v7/widget/Toolbar;

    invoke-static {v0, v3}, Landroid/support/v7/widget/Toolbar;->setChildVisibilityForExpandedActionView(Landroid/support/v7/widget/Toolbar;Z)V

    .line 658877
    iget-object v0, p0, LX/3xZ;->c:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->requestLayout()V

    .line 658878
    invoke-virtual {p1, v3}, LX/3v3;->e(Z)V

    .line 658879
    iget-object v0, p0, LX/3xZ;->c:Landroid/support/v7/widget/Toolbar;

    iget-object v0, v0, Landroid/support/v7/widget/Toolbar;->a:Landroid/view/View;

    instance-of v0, v0, LX/3v5;

    if-eqz v0, :cond_2

    .line 658880
    iget-object v0, p0, LX/3xZ;->c:Landroid/support/v7/widget/Toolbar;

    iget-object v0, v0, Landroid/support/v7/widget/Toolbar;->a:Landroid/view/View;

    check-cast v0, LX/3v5;

    invoke-interface {v0}, LX/3v5;->onActionViewExpanded()V

    .line 658881
    :cond_2
    return v3
.end method

.method public final c(LX/3v3;)Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 658882
    iget-object v0, p0, LX/3xZ;->c:Landroid/support/v7/widget/Toolbar;

    iget-object v0, v0, Landroid/support/v7/widget/Toolbar;->a:Landroid/view/View;

    instance-of v0, v0, LX/3v5;

    if-eqz v0, :cond_0

    .line 658883
    iget-object v0, p0, LX/3xZ;->c:Landroid/support/v7/widget/Toolbar;

    iget-object v0, v0, Landroid/support/v7/widget/Toolbar;->a:Landroid/view/View;

    check-cast v0, LX/3v5;

    invoke-interface {v0}, LX/3v5;->onActionViewCollapsed()V

    .line 658884
    :cond_0
    iget-object v0, p0, LX/3xZ;->c:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, LX/3xZ;->c:Landroid/support/v7/widget/Toolbar;

    iget-object v1, v1, Landroid/support/v7/widget/Toolbar;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->removeView(Landroid/view/View;)V

    .line 658885
    iget-object v0, p0, LX/3xZ;->c:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, LX/3xZ;->c:Landroid/support/v7/widget/Toolbar;

    iget-object v1, v1, Landroid/support/v7/widget/Toolbar;->i:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->removeView(Landroid/view/View;)V

    .line 658886
    iget-object v0, p0, LX/3xZ;->c:Landroid/support/v7/widget/Toolbar;

    iput-object v3, v0, Landroid/support/v7/widget/Toolbar;->a:Landroid/view/View;

    .line 658887
    iget-object v0, p0, LX/3xZ;->c:Landroid/support/v7/widget/Toolbar;

    invoke-static {v0, v2}, Landroid/support/v7/widget/Toolbar;->setChildVisibilityForExpandedActionView(Landroid/support/v7/widget/Toolbar;Z)V

    .line 658888
    iput-object v3, p0, LX/3xZ;->b:LX/3v3;

    .line 658889
    iget-object v0, p0, LX/3xZ;->c:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->requestLayout()V

    .line 658890
    invoke-virtual {p1, v2}, LX/3v3;->e(Z)V

    .line 658891
    const/4 v0, 0x1

    return v0
.end method
