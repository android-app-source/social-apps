.class public LX/4Ph;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 701563
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 12

    .prologue
    const/4 v1, 0x0

    .line 701564
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_b

    .line 701565
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 701566
    :goto_0
    return v1

    .line 701567
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 701568
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_a

    .line 701569
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 701570
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 701571
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_1

    if-eqz v9, :cond_1

    .line 701572
    const-string v10, "__type__"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_2

    const-string v10, "__typename"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 701573
    :cond_2
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->a(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 701574
    :cond_3
    const-string v10, "image_value"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 701575
    invoke-static {p0, p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 701576
    :cond_4
    const-string v10, "name"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 701577
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 701578
    :cond_5
    const-string v10, "video_value"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 701579
    invoke-static {p0, p1}, LX/4UG;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 701580
    :cond_6
    const-string v10, "twe_value"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 701581
    invoke-static {p0, p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 701582
    :cond_7
    const-string v10, "filter_type_sets_value"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 701583
    invoke-static {p0, p1}, LX/4Nt;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 701584
    :cond_8
    const-string v10, "feedback_value"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 701585
    invoke-static {p0, p1}, LX/2bG;->a(LX/15w;LX/186;)I

    move-result v2

    goto/16 :goto_1

    .line 701586
    :cond_9
    const-string v10, "mutation_action_node_value"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 701587
    invoke-static {p0, p1}, LX/2bR;->a(LX/15w;LX/186;)I

    move-result v0

    goto/16 :goto_1

    .line 701588
    :cond_a
    const/16 v9, 0x8

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 701589
    invoke-virtual {p1, v1, v8}, LX/186;->b(II)V

    .line 701590
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 701591
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 701592
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 701593
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 701594
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 701595
    const/4 v1, 0x6

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 701596
    const/4 v1, 0x7

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 701597
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_b
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 701598
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 701599
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 701600
    if-eqz v0, :cond_0

    .line 701601
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 701602
    invoke-static {p0, p1, v1, p2}, LX/2bt;->a(LX/15i;IILX/0nX;)V

    .line 701603
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 701604
    if-eqz v0, :cond_1

    .line 701605
    const-string v1, "image_value"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 701606
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 701607
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 701608
    if-eqz v0, :cond_2

    .line 701609
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 701610
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 701611
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 701612
    if-eqz v0, :cond_3

    .line 701613
    const-string v1, "video_value"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 701614
    invoke-static {p0, v0, p2, p3}, LX/4UG;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 701615
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 701616
    if-eqz v0, :cond_4

    .line 701617
    const-string v1, "twe_value"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 701618
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 701619
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 701620
    if-eqz v0, :cond_5

    .line 701621
    const-string v1, "filter_type_sets_value"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 701622
    invoke-static {p0, v0, p2, p3}, LX/4Nt;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 701623
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 701624
    if-eqz v0, :cond_6

    .line 701625
    const-string v1, "feedback_value"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 701626
    invoke-static {p0, v0, p2, p3}, LX/2bG;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 701627
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 701628
    if-eqz v0, :cond_7

    .line 701629
    const-string v1, "mutation_action_node_value"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 701630
    invoke-static {p0, v0, p2, p3}, LX/2bR;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 701631
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 701632
    return-void
.end method
