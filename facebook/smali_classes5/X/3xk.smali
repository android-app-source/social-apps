.class public final LX/3xk;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source ""


# instance fields
.field public final synthetic a:LX/3xm;


# direct methods
.method public constructor <init>(LX/3xm;)V
    .locals 0

    .prologue
    .line 659950
    iput-object p1, p0, LX/3xk;->a:LX/3xm;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public final onDown(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 659951
    const/4 v0, 0x1

    return v0
.end method

.method public final onLongPress(Landroid/view/MotionEvent;)V
    .locals 4

    .prologue
    .line 659952
    iget-object v0, p0, LX/3xk;->a:LX/3xm;

    invoke-static {v0, p1}, LX/3xm;->b$redex0(LX/3xm;Landroid/view/MotionEvent;)Landroid/view/View;

    move-result-object v0

    .line 659953
    if-eqz v0, :cond_0

    .line 659954
    iget-object v1, p0, LX/3xk;->a:LX/3xm;

    iget-object v1, v1, LX/3xm;->p:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/view/View;)LX/1a1;

    move-result-object v0

    .line 659955
    if-eqz v0, :cond_0

    .line 659956
    iget-object v1, p0, LX/3xk;->a:LX/3xm;

    iget-object v1, v1, LX/3xm;->j:LX/3xj;

    iget-object v2, p0, LX/3xk;->a:LX/3xm;

    iget-object v2, v2, LX/3xm;->p:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v1, v2, v0}, LX/3xj;->c(LX/3xj;Landroid/support/v7/widget/RecyclerView;LX/1a1;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 659957
    :cond_0
    :goto_0
    return-void

    .line 659958
    :cond_1
    const/4 v1, 0x0

    invoke-static {p1, v1}, LX/2xd;->b(Landroid/view/MotionEvent;I)I

    move-result v1

    .line 659959
    iget-object v2, p0, LX/3xk;->a:LX/3xm;

    iget v2, v2, LX/3xm;->i:I

    if-ne v1, v2, :cond_0

    .line 659960
    iget-object v1, p0, LX/3xk;->a:LX/3xm;

    iget v1, v1, LX/3xm;->i:I

    invoke-static {p1, v1}, LX/2xd;->a(Landroid/view/MotionEvent;I)I

    move-result v1

    .line 659961
    invoke-static {p1, v1}, LX/2xd;->c(Landroid/view/MotionEvent;I)F

    move-result v2

    .line 659962
    invoke-static {p1, v1}, LX/2xd;->d(Landroid/view/MotionEvent;I)F

    move-result v1

    .line 659963
    iget-object v3, p0, LX/3xk;->a:LX/3xm;

    iput v2, v3, LX/3xm;->c:F

    .line 659964
    iget-object v2, p0, LX/3xk;->a:LX/3xm;

    iput v1, v2, LX/3xm;->d:F

    .line 659965
    iget-object v1, p0, LX/3xk;->a:LX/3xm;

    iget-object v2, p0, LX/3xk;->a:LX/3xm;

    const/4 v3, 0x0

    iput v3, v2, LX/3xm;->f:F

    iput v3, v1, LX/3xm;->e:F

    .line 659966
    iget-object v1, p0, LX/3xk;->a:LX/3xm;

    iget-object v1, v1, LX/3xm;->j:LX/3xj;

    invoke-virtual {v1}, LX/3xj;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 659967
    iget-object v1, p0, LX/3xk;->a:LX/3xm;

    const/4 v2, 0x2

    invoke-static {v1, v0, v2}, LX/3xm;->a$redex0(LX/3xm;LX/1a1;I)V

    goto :goto_0
.end method
