.class public final enum LX/4Vg;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/4Vg;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/4Vg;

.field public static final enum FAKE_SUCCESS:LX/4Vg;

.field public static final enum NEVER_FINISH:LX/4Vg;

.field public static final enum THROW_CUSTOM_EXCEPTION:LX/4Vg;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 742812
    new-instance v0, LX/4Vg;

    const-string v1, "THROW_CUSTOM_EXCEPTION"

    invoke-direct {v0, v1, v2}, LX/4Vg;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4Vg;->THROW_CUSTOM_EXCEPTION:LX/4Vg;

    .line 742813
    new-instance v0, LX/4Vg;

    const-string v1, "NEVER_FINISH"

    invoke-direct {v0, v1, v3}, LX/4Vg;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4Vg;->NEVER_FINISH:LX/4Vg;

    .line 742814
    new-instance v0, LX/4Vg;

    const-string v1, "FAKE_SUCCESS"

    invoke-direct {v0, v1, v4}, LX/4Vg;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4Vg;->FAKE_SUCCESS:LX/4Vg;

    .line 742815
    const/4 v0, 0x3

    new-array v0, v0, [LX/4Vg;

    sget-object v1, LX/4Vg;->THROW_CUSTOM_EXCEPTION:LX/4Vg;

    aput-object v1, v0, v2

    sget-object v1, LX/4Vg;->NEVER_FINISH:LX/4Vg;

    aput-object v1, v0, v3

    sget-object v1, LX/4Vg;->FAKE_SUCCESS:LX/4Vg;

    aput-object v1, v0, v4

    sput-object v0, LX/4Vg;->$VALUES:[LX/4Vg;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 742811
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/4Vg;
    .locals 1

    .prologue
    .line 742810
    const-class v0, LX/4Vg;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/4Vg;

    return-object v0
.end method

.method public static values()[LX/4Vg;
    .locals 1

    .prologue
    .line 742809
    sget-object v0, LX/4Vg;->$VALUES:[LX/4Vg;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/4Vg;

    return-object v0
.end method
