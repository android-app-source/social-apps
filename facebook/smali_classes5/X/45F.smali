.class public final LX/45F;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/CharSequence;


# instance fields
.field private final a:C


# direct methods
.method public constructor <init>(C)V
    .locals 0

    .prologue
    .line 670236
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 670237
    iput-char p1, p0, LX/45F;->a:C

    .line 670238
    return-void
.end method


# virtual methods
.method public final charAt(I)C
    .locals 1

    .prologue
    .line 670239
    if-eqz p1, :cond_0

    .line 670240
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v0

    .line 670241
    :cond_0
    iget-char v0, p0, LX/45F;->a:C

    return v0
.end method

.method public final length()I
    .locals 1

    .prologue
    .line 670242
    const/4 v0, 0x1

    return v0
.end method

.method public final subSequence(II)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 670243
    if-nez p1, :cond_2

    .line 670244
    if-nez p2, :cond_1

    .line 670245
    const-string p0, ""

    .line 670246
    :cond_0
    return-object p0

    .line 670247
    :cond_1
    const/4 v0, 0x1

    if-eq p2, v0, :cond_0

    .line 670248
    :cond_2
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v0
.end method
