.class public abstract LX/4d9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4cn;


# instance fields
.field public a:I

.field public b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 795446
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 795447
    iput v0, p0, LX/4d9;->a:I

    .line 795448
    iput v0, p0, LX/4d9;->b:I

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 795449
    iget v0, p0, LX/4d9;->b:I

    iget v1, p0, LX/4d9;->a:I

    if-lt v0, v1, :cond_0

    .line 795450
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Already received number of expected batches. Batches expected : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, LX/4d9;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Batches received : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, LX/4d9;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 795451
    :cond_0
    iget v0, p0, LX/4d9;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/4d9;->b:I

    .line 795452
    return-void
.end method
