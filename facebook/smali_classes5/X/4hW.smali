.class public LX/4hW;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 801497
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 801498
    return-void
.end method

.method public static a(Lcom/facebook/platform/common/action/PlatformAppCall;LX/1nY;Ljava/lang/Throwable;ZLjava/lang/String;)Landroid/os/Bundle;
    .locals 6

    .prologue
    .line 801431
    sget-object v0, LX/4hV;->a:[I

    invoke-virtual {p1}, LX/1nY;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 801432
    const-string v0, "ApplicationError"

    .line 801433
    :goto_0
    const/4 v3, 0x0

    .line 801434
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 801435
    invoke-virtual {p2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    .line 801436
    instance-of v4, p2, Lcom/facebook/fbservice/service/ServiceException;

    if-eqz v4, :cond_1

    .line 801437
    check-cast p2, Lcom/facebook/fbservice/service/ServiceException;

    .line 801438
    iget-object v4, p2, Lcom/facebook/fbservice/service/ServiceException;->result:Lcom/facebook/fbservice/service/OperationResult;

    move-object v4, v4

    .line 801439
    if-eqz v4, :cond_1

    .line 801440
    invoke-virtual {v4}, Lcom/facebook/fbservice/service/OperationResult;->getResultData()Landroid/os/Bundle;

    move-result-object v4

    .line 801441
    if-eqz v4, :cond_1

    .line 801442
    const-string v1, "originalExceptionMessage"

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 801443
    if-eqz v1, :cond_5

    .line 801444
    :goto_1
    const-string v3, ""

    .line 801445
    const-string v2, "result"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 801446
    const-string v2, "result"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    .line 801447
    iget-object v5, p2, Lcom/facebook/fbservice/service/ServiceException;->errorCode:LX/1nY;

    move-object v5, v5

    .line 801448
    sget-object p1, LX/1nY;->API_ERROR:LX/1nY;

    invoke-virtual {v5, p1}, LX/1nY;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    instance-of v5, v2, Lcom/facebook/http/protocol/ApiErrorResult;

    if-eqz v5, :cond_6

    .line 801449
    check-cast v2, Lcom/facebook/http/protocol/ApiErrorResult;

    .line 801450
    iget-object v3, v2, Lcom/facebook/http/protocol/ApiErrorResult;->mJsonResponse:Ljava/lang/String;

    move-object v2, v3

    .line 801451
    :goto_2
    move-object v3, v2

    .line 801452
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 801453
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_7

    .line 801454
    :cond_0
    :goto_3
    move-object v2, v2

    .line 801455
    move-object v5, v1

    move-object v1, v2

    move-object v2, v5

    .line 801456
    :cond_1
    if-eqz p3, :cond_4

    .line 801457
    const/4 v1, 0x0

    .line 801458
    if-eqz v3, :cond_2

    .line 801459
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, v3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 801460
    :cond_2
    if-eqz v1, :cond_b

    .line 801461
    const-string v2, "error"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    move-object v2, v1

    .line 801462
    :goto_4
    if-eqz v2, :cond_a

    .line 801463
    const-string v1, "code"

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 801464
    const-string v1, "fbplatse:"

    .line 801465
    if-eqz v4, :cond_3

    .line 801466
    new-instance v5, Ljava/lang/StringBuilder;

    const-string p1, "(#"

    invoke-direct {v5, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 801467
    :cond_3
    const-string v4, "message"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 801468
    if-eqz v2, :cond_a

    invoke-virtual {v2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 801469
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v2, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 801470
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 801471
    const-string v1, "message_android"

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 801472
    const-string v1, "message_android"

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1}, LX/4hW;->a(Lcom/facebook/platform/common/action/PlatformAppCall;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    .line 801473
    :goto_5
    move-object v0, v1

    .line 801474
    :goto_6
    return-object v0

    .line 801475
    :pswitch_0
    const-string v0, "NetworkError"

    goto/16 :goto_0

    .line 801476
    :pswitch_1
    const-string v0, "UnknownError"

    goto/16 :goto_0

    .line 801477
    :cond_4
    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    .line 801478
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 801479
    invoke-static {p0}, LX/4hW;->a(Lcom/facebook/platform/common/action/PlatformAppCall;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 801480
    invoke-static {p0}, LX/4hW;->b(Lcom/facebook/platform/common/action/PlatformAppCall;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 801481
    if-eqz p0, :cond_c

    invoke-virtual {p0}, Lcom/facebook/platform/common/action/PlatformAppCall;->d()Z

    move-result v4

    if-eqz v4, :cond_c

    const/4 v4, 0x1

    .line 801482
    :goto_7
    if-eqz v4, :cond_d

    const-string v4, "error_json"

    :goto_8
    move-object v4, v4

    .line 801483
    invoke-virtual {v3, v4, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 801484
    move-object v0, v3

    .line 801485
    goto :goto_6

    :cond_5
    move-object v1, v2

    goto/16 :goto_1

    :cond_6
    move-object v2, v3

    goto/16 :goto_2

    .line 801486
    :cond_7
    :try_start_1
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4, v3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 801487
    const-string v5, "error_code"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 801488
    const-string v5, "error_code"

    const-string p1, "error_code"

    invoke-virtual {v4, p1}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {v2, v5, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 801489
    :cond_8
    const-string v5, "error_subcode"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 801490
    const-string v5, "error_subcode"

    const-string p1, "error_subcode"

    invoke-virtual {v4, p1}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_3

    :catch_0
    goto/16 :goto_3

    .line 801491
    :cond_9
    :try_start_2
    const-string v1, "message"

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 801492
    const-string v1, "message"

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1}, LX/4hW;->a(Lcom/facebook/platform/common/action/PlatformAppCall;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    goto :goto_5

    .line 801493
    :cond_a
    invoke-static {p0, v0, p4}, LX/4hW;->a(Lcom/facebook/platform/common/action/PlatformAppCall;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v1

    goto :goto_5

    .line 801494
    :catch_1
    invoke-static {p0, v0, p4}, LX/4hW;->a(Lcom/facebook/platform/common/action/PlatformAppCall;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    goto/16 :goto_5

    :cond_b
    move-object v2, v1

    goto/16 :goto_4

    .line 801495
    :cond_c
    const/4 v4, 0x0

    goto :goto_7

    .line 801496
    :cond_d
    const-string v4, "com.facebook.platform.status.ERROR_JSON"

    goto :goto_8

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Lcom/facebook/platform/common/action/PlatformAppCall;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 801427
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 801428
    invoke-static {p0}, LX/4hW;->a(Lcom/facebook/platform/common/action/PlatformAppCall;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "UserCanceled"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 801429
    invoke-static {p0}, LX/4hW;->b(Lcom/facebook/platform/common/action/PlatformAppCall;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 801430
    return-object v0
.end method

.method public static a(Lcom/facebook/platform/common/action/PlatformAppCall;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 2

    .prologue
    .line 801411
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 801412
    invoke-static {p0}, LX/4hW;->a(Lcom/facebook/platform/common/action/PlatformAppCall;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 801413
    invoke-static {p0}, LX/4hW;->b(Lcom/facebook/platform/common/action/PlatformAppCall;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 801414
    return-object v0
.end method

.method public static a(Lcom/facebook/platform/common/action/PlatformAppCall;Ljava/lang/Throwable;)Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 801424
    invoke-static {p1}, LX/3d6;->forException(Ljava/lang/Throwable;)LX/1nY;

    move-result-object v0

    .line 801425
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {p0, v0, p1, v1, v2}, LX/4hW;->a(Lcom/facebook/platform/common/action/PlatformAppCall;LX/1nY;Ljava/lang/Throwable;ZLjava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    move-object v0, v1

    .line 801426
    return-object v0
.end method

.method public static a(Lcom/facebook/platform/common/action/PlatformAppCall;Ljava/lang/Throwable;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 2

    .prologue
    .line 801423
    invoke-static {p1}, LX/3d6;->forException(Ljava/lang/Throwable;)LX/1nY;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {p0, v0, p1, v1, p2}, LX/4hW;->a(Lcom/facebook/platform/common/action/PlatformAppCall;LX/1nY;Ljava/lang/Throwable;ZLjava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/platform/common/action/PlatformAppCall;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 801419
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/platform/common/action/PlatformAppCall;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 801420
    :goto_0
    if-eqz v0, :cond_1

    const-string v0, "error_type"

    :goto_1
    return-object v0

    .line 801421
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 801422
    :cond_1
    const-string v0, "com.facebook.platform.status.ERROR_TYPE"

    goto :goto_1
.end method

.method public static b(Lcom/facebook/platform/common/action/PlatformAppCall;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 801415
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/platform/common/action/PlatformAppCall;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 801416
    :goto_0
    if-eqz v0, :cond_1

    const-string v0, "error_description"

    :goto_1
    return-object v0

    .line 801417
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 801418
    :cond_1
    const-string v0, "com.facebook.platform.status.ERROR_DESCRIPTION"

    goto :goto_1
.end method
