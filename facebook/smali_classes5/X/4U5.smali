.class public LX/4U5;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 720630
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 11

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 720631
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v3, :cond_9

    .line 720632
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 720633
    :goto_0
    return v0

    .line 720634
    :cond_0
    const-string v9, "is_checked"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 720635
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v5, v0

    move v0, v1

    .line 720636
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_7

    .line 720637
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 720638
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 720639
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_1

    if-eqz v8, :cond_1

    .line 720640
    const-string v9, "icon_dominant_color"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 720641
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 720642
    :cond_2
    const-string v9, "id"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 720643
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 720644
    :cond_3
    const-string v9, "option_icon"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 720645
    invoke-static {p0, p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 720646
    :cond_4
    const-string v9, "topic_feed_option"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 720647
    invoke-static {p0, p1}, LX/4U1;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 720648
    :cond_5
    const-string v9, "url"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 720649
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 720650
    :cond_6
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 720651
    :cond_7
    const/4 v8, 0x7

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 720652
    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 720653
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 720654
    if-eqz v0, :cond_8

    .line 720655
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v5}, LX/186;->a(IZ)V

    .line 720656
    :cond_8
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 720657
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 720658
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 720659
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :cond_9
    move v2, v0

    move v3, v0

    move v4, v0

    move v5, v0

    move v6, v0

    move v7, v0

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 720660
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 720661
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 720662
    if-eqz v0, :cond_0

    .line 720663
    const-string v1, "icon_dominant_color"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 720664
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 720665
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 720666
    if-eqz v0, :cond_1

    .line 720667
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 720668
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 720669
    :cond_1
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 720670
    if-eqz v0, :cond_2

    .line 720671
    const-string v1, "is_checked"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 720672
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 720673
    :cond_2
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 720674
    if-eqz v0, :cond_3

    .line 720675
    const-string v1, "option_icon"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 720676
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 720677
    :cond_3
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 720678
    if-eqz v0, :cond_4

    .line 720679
    const-string v1, "topic_feed_option"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 720680
    invoke-static {p0, v0, p2}, LX/4U1;->a(LX/15i;ILX/0nX;)V

    .line 720681
    :cond_4
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 720682
    if-eqz v0, :cond_5

    .line 720683
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 720684
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 720685
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 720686
    return-void
.end method
