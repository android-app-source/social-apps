.class public LX/4dO;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/1GB;

.field public final b:I

.field private c:LX/1FJ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method private constructor <init>(LX/1GB;)V
    .locals 1

    .prologue
    .line 795789
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 795790
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1GB;

    iput-object v0, p0, LX/4dO;->a:LX/1GB;

    .line 795791
    const/4 v0, 0x0

    iput v0, p0, LX/4dO;->b:I

    .line 795792
    return-void
.end method

.method public constructor <init>(LX/4dP;)V
    .locals 1

    .prologue
    .line 795779
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 795780
    iget-object v0, p1, LX/4dP;->a:LX/1GB;

    move-object v0, v0

    .line 795781
    invoke-static {v0}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1GB;

    iput-object v0, p0, LX/4dO;->a:LX/1GB;

    .line 795782
    iget v0, p1, LX/4dP;->d:I

    move v0, v0

    .line 795783
    iput v0, p0, LX/4dO;->b:I

    .line 795784
    iget-object v0, p1, LX/4dP;->b:LX/1FJ;

    invoke-static {v0}, LX/1FJ;->b(LX/1FJ;)LX/1FJ;

    move-result-object v0

    move-object v0, v0

    .line 795785
    iput-object v0, p0, LX/4dO;->c:LX/1FJ;

    .line 795786
    iget-object v0, p1, LX/4dP;->c:Ljava/util/List;

    invoke-static {v0}, LX/1FJ;->a(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    move-object v0, v0

    .line 795787
    iput-object v0, p0, LX/4dO;->d:Ljava/util/List;

    .line 795788
    return-void
.end method

.method public static a(LX/1GB;)LX/4dO;
    .locals 1

    .prologue
    .line 795778
    new-instance v0, LX/4dO;

    invoke-direct {v0, p0}, LX/4dO;-><init>(LX/1GB;)V

    return-object v0
.end method


# virtual methods
.method public final declared-synchronized a(I)LX/1FJ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 795774
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/4dO;->d:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 795775
    iget-object v0, p0, LX/4dO;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FJ;

    invoke-static {v0}, LX/1FJ;->b(LX/1FJ;)LX/1FJ;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 795776
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 795777
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(I)Z
    .locals 1

    .prologue
    .line 795773
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/4dO;->d:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/4dO;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()LX/1FJ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 795766
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/4dO;->c:LX/1FJ;

    invoke-static {v0}, LX/1FJ;->b(LX/1FJ;)LX/1FJ;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()V
    .locals 1

    .prologue
    .line 795767
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/4dO;->c:LX/1FJ;

    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    .line 795768
    const/4 v0, 0x0

    iput-object v0, p0, LX/4dO;->c:LX/1FJ;

    .line 795769
    iget-object v0, p0, LX/4dO;->d:Ljava/util/List;

    invoke-static {v0}, LX/1FJ;->a(Ljava/lang/Iterable;)V

    .line 795770
    const/4 v0, 0x0

    iput-object v0, p0, LX/4dO;->d:Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 795771
    monitor-exit p0

    return-void

    .line 795772
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
