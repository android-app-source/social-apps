.class public LX/4o6;
.super Landroid/view/ViewGroup;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private b:LX/4o4;

.field private c:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 808659
    const-class v0, LX/4o6;

    sput-object v0, LX/4o6;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 808654
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 808655
    sget-object v0, LX/4o4;->VERTICAL:LX/4o4;

    iput-object v0, p0, LX/4o6;->b:LX/4o4;

    .line 808656
    const/4 v0, 0x0

    iput v0, p0, LX/4o6;->c:I

    .line 808657
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/4o6;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 808658
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 808649
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 808650
    sget-object v0, LX/4o4;->VERTICAL:LX/4o4;

    iput-object v0, p0, LX/4o6;->b:LX/4o4;

    .line 808651
    const/4 v0, 0x0

    iput v0, p0, LX/4o6;->c:I

    .line 808652
    invoke-direct {p0, p1, p2}, LX/4o6;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 808653
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 808644
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 808645
    sget-object v0, LX/4o4;->VERTICAL:LX/4o4;

    iput-object v0, p0, LX/4o6;->b:LX/4o4;

    .line 808646
    const/4 v0, 0x0

    iput v0, p0, LX/4o6;->c:I

    .line 808647
    invoke-direct {p0, p1, p2}, LX/4o6;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 808648
    return-void
.end method

.method private a(IILX/4o4;)LX/4o5;
    .locals 11

    .prologue
    .line 808623
    invoke-direct {p0}, LX/4o6;->getVisibleChildren()LX/0Px;

    move-result-object v7

    .line 808624
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v6

    .line 808625
    iget v0, p0, LX/4o6;->c:I

    add-int/lit8 v1, v6, -0x1

    mul-int v5, v0, v1

    .line 808626
    const/high16 v3, -0x80000000

    .line 808627
    const/high16 v4, -0x80000000

    .line 808628
    sget-object v0, LX/4o4;->HORIZONTAL:LX/4o4;

    if-ne p3, v0, :cond_0

    .line 808629
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v1, -0x2

    const/4 v2, -0x1

    invoke-direct {v0, v1, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    move-object v1, v0

    .line 808630
    :goto_0
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 808631
    invoke-interface {v7, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 808632
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 808633
    invoke-virtual {p0, v0, p1, p2}, LX/4o6;->measureChild(Landroid/view/View;II)V

    .line 808634
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v8

    .line 808635
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    .line 808636
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Visible child "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " initial measurement: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "x"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 808637
    invoke-static {v3, v8}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 808638
    invoke-static {v4, v0}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 808639
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 808640
    :cond_0
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    move-object v1, v0

    goto :goto_0

    .line 808641
    :cond_1
    const/high16 v0, -0x80000000

    if-ne v3, v0, :cond_2

    const/high16 v0, -0x80000000

    if-ne v4, v0, :cond_2

    .line 808642
    new-instance v0, LX/4o5;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, LX/4o5;-><init>(II)V

    .line 808643
    :goto_2
    return-object v0

    :cond_2
    move v0, p1

    move v1, p2

    move-object v2, p3

    invoke-static/range {v0 .. v6}, LX/4o6;->a(IILX/4o4;IIII)LX/4o5;

    move-result-object v0

    goto :goto_2
.end method

.method private static a(IILX/4o4;IIII)LX/4o5;
    .locals 1

    .prologue
    .line 808615
    sget-object v0, LX/4o4;->HORIZONTAL:LX/4o4;

    if-ne p2, v0, :cond_0

    .line 808616
    invoke-static {p0}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 808617
    :goto_0
    new-instance v0, LX/4o5;

    invoke-direct {v0, p3, p4}, LX/4o5;-><init>(II)V

    return-object v0

    .line 808618
    :sswitch_0
    invoke-static {p0}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    sub-int/2addr v0, p5

    div-int/2addr v0, p6

    invoke-static {p3, v0}, Ljava/lang/Math;->max(II)I

    move-result p3

    goto :goto_0

    .line 808619
    :sswitch_1
    invoke-static {p0}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    sub-int/2addr v0, p5

    div-int/2addr v0, p6

    invoke-static {p3, v0}, Ljava/lang/Math;->min(II)I

    move-result p3

    goto :goto_0

    .line 808620
    :cond_0
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    sparse-switch v0, :sswitch_data_1

    goto :goto_0

    .line 808621
    :sswitch_2
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    sub-int/2addr v0, p5

    div-int/2addr v0, p6

    invoke-static {p4, v0}, Ljava/lang/Math;->min(II)I

    move-result p4

    goto :goto_0

    .line 808622
    :sswitch_3
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    sub-int/2addr v0, p5

    div-int p4, v0, p6

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_1
        0x40000000 -> :sswitch_0
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        -0x80000000 -> :sswitch_2
        0x40000000 -> :sswitch_3
    .end sparse-switch
.end method

.method private a(LX/4o5;IILX/4o4;)LX/4o5;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 808594
    invoke-direct {p0}, LX/4o6;->getVisibleChildren()LX/0Px;

    move-result-object v0

    .line 808595
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 808596
    iget v1, p0, LX/4o6;->c:I

    add-int/lit8 v3, v0, -0x1

    mul-int/2addr v3, v1

    .line 808597
    sget-object v1, LX/4o4;->HORIZONTAL:LX/4o4;

    if-ne p4, v1, :cond_0

    .line 808598
    iget v1, p1, LX/4o5;->a:I

    mul-int/2addr v0, v1

    .line 808599
    invoke-virtual {p0}, LX/4o6;->getPaddingLeft()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0}, LX/4o6;->getPaddingRight()I

    move-result v1

    add-int/2addr v0, v1

    add-int v1, v0, v3

    .line 808600
    iget v0, p1, LX/4o5;->b:I

    invoke-virtual {p0}, LX/4o6;->getPaddingTop()I

    move-result v3

    add-int/2addr v0, v3

    invoke-virtual {p0}, LX/4o6;->getPaddingBottom()I

    move-result v3

    add-int/2addr v0, v3

    .line 808601
    :goto_0
    invoke-virtual {p0}, LX/4o6;->getSuggestedMinimumWidth()I

    move-result v3

    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 808602
    invoke-static {v3, p2}, LX/4o6;->resolveSize(II)I

    move-result v3

    .line 808603
    if-le v1, v3, :cond_1

    .line 808604
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "Desired width "

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " > spec width "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", switching modes..."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object v0, v2

    .line 808605
    :goto_1
    return-object v0

    .line 808606
    :cond_0
    iget v1, p1, LX/4o5;->b:I

    mul-int/2addr v0, v1

    .line 808607
    iget v1, p1, LX/4o5;->a:I

    invoke-virtual {p0}, LX/4o6;->getPaddingLeft()I

    move-result v4

    add-int/2addr v1, v4

    invoke-virtual {p0}, LX/4o6;->getPaddingRight()I

    move-result v4

    add-int/2addr v1, v4

    .line 808608
    invoke-virtual {p0}, LX/4o6;->getPaddingTop()I

    move-result v4

    add-int/2addr v0, v4

    invoke-virtual {p0}, LX/4o6;->getPaddingBottom()I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    goto :goto_0

    .line 808609
    :cond_1
    invoke-virtual {p0}, LX/4o6;->getSuggestedMinimumHeight()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 808610
    invoke-static {v1, p3}, LX/4o6;->resolveSize(II)I

    move-result v1

    .line 808611
    if-le v0, v1, :cond_2

    .line 808612
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Desired height "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " > spec height "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-object v0, v2

    .line 808613
    goto :goto_1

    .line 808614
    :cond_2
    new-instance v0, LX/4o5;

    invoke-direct {v0, v3, v1}, LX/4o5;-><init>(II)V

    goto :goto_1
.end method

.method private final a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    .line 808589
    if-eqz p2, :cond_0

    .line 808590
    sget-object v0, LX/03r;->HorizontalOrVerticalViewGroup:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 808591
    const/16 v1, 0x0

    iget v2, p0, LX/4o6;->c:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, LX/4o6;->c:I

    .line 808592
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 808593
    :cond_0
    return-void
.end method

.method private getVisibleChildren()LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 808541
    invoke-virtual {p0}, LX/4o6;->getChildCount()I

    move-result v1

    .line 808542
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 808543
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 808544
    invoke-virtual {p0, v0}, LX/4o6;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 808545
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v4

    const/16 v5, 0x8

    if-eq v4, v5, :cond_0

    .line 808546
    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 808547
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 808548
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getMode()LX/4o4;
    .locals 1

    .prologue
    .line 808588
    iget-object v0, p0, LX/4o6;->b:LX/4o4;

    return-object v0
.end method

.method public final onLayout(ZIIII)V
    .locals 13

    .prologue
    .line 808575
    invoke-direct {p0}, LX/4o6;->getVisibleChildren()LX/0Px;

    move-result-object v2

    .line 808576
    invoke-virtual {p0}, LX/4o6;->getPaddingLeft()I

    move-result v1

    .line 808577
    invoke-virtual {p0}, LX/4o6;->getPaddingTop()I

    move-result v0

    .line 808578
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v1

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 808579
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v4, v2

    .line 808580
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v5, v1

    .line 808581
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Laying out child "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " @ "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ","

    invoke-static {v7}, LX/0PO;->on(Ljava/lang/String;)LX/0PO;

    move-result-object v7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v7, v8, v9, v10}, LX/0PO;->join(Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 808582
    invoke-virtual {v0, v2, v1, v4, v5}, Landroid/view/View;->layout(IIII)V

    .line 808583
    iget-object v0, p0, LX/4o6;->b:LX/4o4;

    sget-object v6, LX/4o4;->HORIZONTAL:LX/4o4;

    if-ne v0, v6, :cond_0

    .line 808584
    iget v0, p0, LX/4o6;->c:I

    add-int/2addr v0, v4

    move v2, v0

    goto :goto_0

    .line 808585
    :cond_0
    iget v0, p0, LX/4o6;->c:I

    add-int/2addr v0, v5

    move v1, v0

    .line 808586
    goto :goto_0

    .line 808587
    :cond_1
    return-void
.end method

.method public final onMeasure(II)V
    .locals 9

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    const/4 v1, 0x0

    .line 808549
    invoke-virtual {p0}, LX/4o6;->getChildCount()I

    move-result v3

    .line 808550
    if-lez v3, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v2, "Must have at least one child"

    invoke-static {v0, v2}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 808551
    sget-object v0, LX/4o4;->HORIZONTAL:LX/4o4;

    invoke-direct {p0, p1, p2, v0}, LX/4o6;->a(IILX/4o4;)LX/4o5;

    move-result-object v2

    .line 808552
    sget-object v0, LX/4o4;->HORIZONTAL:LX/4o4;

    invoke-direct {p0, v2, p1, p2, v0}, LX/4o6;->a(LX/4o5;IILX/4o4;)LX/4o5;

    move-result-object v0

    .line 808553
    if-eqz v0, :cond_2

    .line 808554
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Laying out view group horizontally, size "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 808555
    sget-object v4, LX/4o4;->HORIZONTAL:LX/4o4;

    iput-object v4, p0, LX/4o6;->b:LX/4o4;

    .line 808556
    :goto_1
    iget v4, v2, LX/4o5;->a:I

    invoke-static {v4, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 808557
    iget v5, v2, LX/4o5;->b:I

    invoke-static {v5, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    .line 808558
    :goto_2
    if-ge v1, v3, :cond_4

    .line 808559
    invoke-virtual {p0, v1}, LX/4o6;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 808560
    invoke-virtual {v6}, Landroid/view/View;->getVisibility()I

    move-result v7

    const/16 v8, 0x8

    if-eq v7, v8, :cond_0

    .line 808561
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Setting measured size of child "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " to "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 808562
    invoke-virtual {v6, v4, v5}, Landroid/view/View;->measure(II)V

    .line 808563
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_1
    move v0, v1

    .line 808564
    goto :goto_0

    .line 808565
    :cond_2
    sget-object v0, LX/4o4;->VERTICAL:LX/4o4;

    invoke-direct {p0, p1, p2, v0}, LX/4o6;->a(IILX/4o4;)LX/4o5;

    move-result-object v2

    .line 808566
    sget-object v0, LX/4o4;->VERTICAL:LX/4o4;

    invoke-direct {p0, v2, p1, p2, v0}, LX/4o6;->a(LX/4o5;IILX/4o4;)LX/4o5;

    move-result-object v0

    .line 808567
    if-eqz v0, :cond_3

    .line 808568
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Laying out view group vertically, size "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 808569
    sget-object v4, LX/4o4;->VERTICAL:LX/4o4;

    iput-object v4, p0, LX/4o6;->b:LX/4o4;

    goto :goto_1

    .line 808570
    :cond_3
    sget-object v0, LX/4o6;->a:Ljava/lang/Class;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Children of view group "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " do not fit either horizontally or vertically."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 808571
    sget-object v0, LX/4o4;->VERTICAL:LX/4o4;

    iput-object v0, p0, LX/4o6;->b:LX/4o4;

    .line 808572
    new-instance v0, LX/4o5;

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v5

    invoke-direct {v0, v4, v5}, LX/4o5;-><init>(II)V

    goto :goto_1

    .line 808573
    :cond_4
    iget v1, v0, LX/4o5;->a:I

    iget v0, v0, LX/4o5;->b:I

    invoke-virtual {p0, v1, v0}, LX/4o6;->setMeasuredDimension(II)V

    .line 808574
    return-void
.end method
