.class public final LX/4fN;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 798368
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(I)I
    .locals 2

    .prologue
    .line 798369
    int-to-float v0, p0

    const v1, 0x3faaaaab

    mul-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public static a(IILX/1o9;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 798370
    if-nez p2, :cond_2

    .line 798371
    invoke-static {p0}, LX/4fN;->a(I)I

    move-result v2

    int-to-float v2, v2

    const/high16 v3, 0x45000000    # 2048.0f

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_1

    invoke-static {p1}, LX/4fN;->a(I)I

    move-result v2

    const/16 v3, 0x800

    if-lt v2, v3, :cond_1

    .line 798372
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 798373
    goto :goto_0

    .line 798374
    :cond_2
    invoke-static {p0}, LX/4fN;->a(I)I

    move-result v2

    iget v3, p2, LX/1o9;->a:I

    if-lt v2, v3, :cond_3

    invoke-static {p1}, LX/4fN;->a(I)I

    move-result v2

    iget v3, p2, LX/1o9;->b:I

    if-ge v2, v3, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public static a(LX/1FL;LX/1o9;)Z
    .locals 2

    .prologue
    .line 798375
    if-nez p0, :cond_0

    .line 798376
    const/4 v0, 0x0

    .line 798377
    :goto_0
    return v0

    .line 798378
    :cond_0
    iget v0, p0, LX/1FL;->d:I

    move v0, v0

    .line 798379
    sparse-switch v0, :sswitch_data_0

    .line 798380
    iget v0, p0, LX/1FL;->e:I

    move v0, v0

    .line 798381
    iget v1, p0, LX/1FL;->f:I

    move v1, v1

    .line 798382
    invoke-static {v0, v1, p1}, LX/4fN;->a(IILX/1o9;)Z

    move-result v0

    goto :goto_0

    .line 798383
    :sswitch_0
    iget v0, p0, LX/1FL;->f:I

    move v0, v0

    .line 798384
    iget v1, p0, LX/1FL;->e:I

    move v1, v1

    .line 798385
    invoke-static {v0, v1, p1}, LX/4fN;->a(IILX/1o9;)Z

    move-result v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x5a -> :sswitch_0
        0x10e -> :sswitch_0
    .end sparse-switch
.end method
