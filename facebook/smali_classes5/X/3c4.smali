.class public LX/3c4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1TE;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 612976
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 612977
    return-void
.end method

.method public static a(LX/0QB;)LX/3c4;
    .locals 3

    .prologue
    .line 612978
    const-class v1, LX/3c4;

    monitor-enter v1

    .line 612979
    :try_start_0
    sget-object v0, LX/3c4;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 612980
    sput-object v2, LX/3c4;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 612981
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 612982
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 612983
    new-instance v0, LX/3c4;

    invoke-direct {v0}, LX/3c4;-><init>()V

    .line 612984
    move-object v0, v0

    .line 612985
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 612986
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/3c4;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 612987
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 612988
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1KB;)V
    .locals 1

    .prologue
    .line 612989
    sget-object v0, Lcom/facebook/timeline/publisher/rows/OpenPublisherBarWithStatusButtonsPartDefinition;->a:LX/1Cz;

    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 612990
    return-void
.end method
