.class public final LX/4nM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0rf;


# annotations
.annotation build Lcom/google/common/annotations/VisibleForTesting;
.end annotation


# instance fields
.field public final synthetic a:LX/2Vx;


# direct methods
.method public constructor <init>(LX/2Vx;)V
    .locals 0

    .prologue
    .line 806760
    iput-object p1, p0, LX/4nM;->a:LX/2Vx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/32G;)V
    .locals 6

    .prologue
    .line 806755
    invoke-virtual {p1}, LX/32G;->getSuggestedTrimRatio()D

    move-result-wide v0

    .line 806756
    iget-object v2, p0, LX/4nM;->a:LX/2Vx;

    iget-object v2, v2, LX/2Vx;->h:Ljava/lang/Object;

    monitor-enter v2

    .line 806757
    :try_start_0
    iget-object v3, p0, LX/4nM;->a:LX/2Vx;

    iget v3, v3, LX/2Vx;->o:I

    iget-object v4, p0, LX/4nM;->a:LX/2Vx;

    iget v4, v4, LX/2Vx;->o:I

    int-to-double v4, v4

    mul-double/2addr v0, v4

    double-to-int v0, v0

    sub-int v0, v3, v0

    .line 806758
    iget-object v1, p0, LX/4nM;->a:LX/2Vx;

    const v3, 0x7fffffff

    invoke-virtual {v1, v0, v3}, LX/2Vx;->a(II)V

    .line 806759
    monitor-exit v2

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
