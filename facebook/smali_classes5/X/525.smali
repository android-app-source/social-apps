.class public final LX/525;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1vL;


# annotations
.annotation build Lcom/google/common/annotations/VisibleForTesting;
.end annotation


# static fields
.field public static final a:LX/525;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 825754
    new-instance v0, LX/525;

    invoke-direct {v0}, LX/525;-><init>()V

    sput-object v0, LX/525;->a:LX/525;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 825755
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/io/Closeable;Ljava/lang/Throwable;Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 825756
    sget-object v0, LX/1md;->a:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Suppressing exception thrown when closing "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 825757
    return-void
.end method
