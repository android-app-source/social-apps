.class public final LX/4Yc;
.super LX/0ur;
.source ""


# instance fields
.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsEdge;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lcom/facebook/graphql/model/GraphQLPageInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 781725
    invoke-direct {p0}, LX/0ur;-><init>()V

    .line 781726
    instance-of v0, p0, LX/4Yc;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 781727
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsConnection;)LX/4Yc;
    .locals 2

    .prologue
    .line 781728
    new-instance v0, LX/4Yc;

    invoke-direct {v0}, LX/4Yc;-><init>()V

    .line 781729
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 781730
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsConnection;->a()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/4Yc;->b:LX/0Px;

    .line 781731
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsConnection;->j()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v1

    iput-object v1, v0, LX/4Yc;->c:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 781732
    invoke-static {v0, p0}, LX/0ur;->a(LX/0ur;Lcom/facebook/graphql/modelutil/BaseModel;)V

    .line 781733
    return-object v0
.end method


# virtual methods
.method public final a()Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsConnection;
    .locals 2

    .prologue
    .line 781734
    new-instance v0, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsConnection;

    invoke-direct {v0, p0}, Lcom/facebook/graphql/model/GraphQLSaleGroupsNearYouFeedUnitGroupsConnection;-><init>(LX/4Yc;)V

    .line 781735
    return-object v0
.end method
