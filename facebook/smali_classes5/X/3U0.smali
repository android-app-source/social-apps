.class public final LX/3U0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1Ep;

.field public final synthetic b:Ljava/util/Collection;

.field public final synthetic c:LX/0uk;

.field public final synthetic d:Lcom/facebook/api/prefetch/GraphQLPrefetchController;


# direct methods
.method public constructor <init>(Lcom/facebook/api/prefetch/GraphQLPrefetchController;LX/1Ep;Ljava/util/Collection;LX/0uk;)V
    .locals 0

    .prologue
    .line 585329
    iput-object p1, p0, LX/3U0;->d:Lcom/facebook/api/prefetch/GraphQLPrefetchController;

    iput-object p2, p0, LX/3U0;->a:LX/1Ep;

    iput-object p3, p0, LX/3U0;->b:Ljava/util/Collection;

    iput-object p4, p0, LX/3U0;->c:LX/0uk;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 585323
    iget-object v0, p0, LX/3U0;->a:LX/1Ep;

    iget-object v1, p0, LX/3U0;->b:Ljava/util/Collection;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    .line 585324
    if-nez v1, :cond_1

    .line 585325
    :cond_0
    sget-object v0, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->a:Ljava/lang/String;

    const-string v1, "Prefetch failed in prefetcher %s: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LX/3U0;->c:LX/0uk;

    invoke-virtual {v4}, LX/0uk;->d()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 585326
    return-void

    .line 585327
    :cond_1
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 585328
    invoke-virtual {v0, v3, v2}, LX/1Ep;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 14

    .prologue
    .line 585315
    iget-object v0, p0, LX/3U0;->a:LX/1Ep;

    iget-object v1, p0, LX/3U0;->b:Ljava/util/Collection;

    const/4 v6, 0x0

    .line 585316
    if-nez v1, :cond_1

    .line 585317
    :cond_0
    iget-object v0, p0, LX/3U0;->b:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    .line 585318
    return-void

    .line 585319
    :cond_1
    iget-object v2, v0, LX/1Ep;->g:Ljava/util/Random;

    invoke-virtual {v2}, Ljava/util/Random;->nextInt()I

    move-result v9

    .line 585320
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 585321
    invoke-static {v0, v4}, LX/1Ep;->g(LX/1Ep;Ljava/lang/String;)J

    move-result-wide v12

    .line 585322
    sget-object v3, LX/1Eq;->PREFETCH_SUCCEEDED:LX/1Eq;

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object v2, v0

    move-object v8, v6

    invoke-static/range {v2 .. v8}, LX/1Ep;->a(LX/1Ep;LX/1Eq;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/String;)V

    goto :goto_0
.end method
