.class public final LX/3mz;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 637560
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLGroup;)Lcom/facebook/graphql/model/GraphQLProfile;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 637561
    new-instance v0, LX/25F;

    invoke-direct {v0}, LX/25F;-><init>()V

    .line 637562
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->j()Z

    move-result v1

    .line 637563
    iput-boolean v1, v0, LX/25F;->h:Z

    .line 637564
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->k()Z

    move-result v1

    .line 637565
    iput-boolean v1, v0, LX/25F;->i:Z

    .line 637566
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->S()Z

    move-result v1

    .line 637567
    iput-boolean v1, v0, LX/25F;->j:Z

    .line 637568
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->l()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v1

    .line 637569
    iput-object v1, v0, LX/25F;->m:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    .line 637570
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->m()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v1

    .line 637571
    iput-object v1, v0, LX/25F;->o:Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    .line 637572
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->o()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 637573
    iput-object v1, v0, LX/25F;->u:Lcom/facebook/graphql/model/GraphQLImage;

    .line 637574
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->p()Lcom/facebook/graphql/model/GraphQLGroupConfigurationsConnection;

    move-result-object v1

    .line 637575
    iput-object v1, v0, LX/25F;->z:Lcom/facebook/graphql/model/GraphQLGroupConfigurationsConnection;

    .line 637576
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->r()Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;

    move-result-object v1

    .line 637577
    iput-object v1, v0, LX/25F;->A:Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;

    .line 637578
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->s()I

    move-result v1

    .line 637579
    iput v1, v0, LX/25F;->B:I

    .line 637580
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->t()Ljava/lang/String;

    move-result-object v1

    .line 637581
    iput-object v1, v0, LX/25F;->C:Ljava/lang/String;

    .line 637582
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->P()Z

    move-result v1

    .line 637583
    iput-boolean v1, v0, LX/25F;->G:Z

    .line 637584
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->O()Z

    move-result v1

    .line 637585
    iput-boolean v1, v0, LX/25F;->H:Z

    .line 637586
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->u()Z

    move-result v1

    .line 637587
    iput-boolean v1, v0, LX/25F;->J:Z

    .line 637588
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->v()Ljava/lang/String;

    move-result-object v1

    .line 637589
    iput-object v1, v0, LX/25F;->T:Ljava/lang/String;

    .line 637590
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->w()LX/0Px;

    move-result-object v1

    .line 637591
    iput-object v1, v0, LX/25F;->U:LX/0Px;

    .line 637592
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->x()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v1

    .line 637593
    iput-object v1, v0, LX/25F;->Y:Lcom/facebook/graphql/model/GraphQLGroup;

    .line 637594
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->y()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v1

    .line 637595
    iput-object v1, v0, LX/25F;->aa:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 637596
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 637597
    iput-object v1, v0, LX/25F;->ab:Lcom/facebook/graphql/model/GraphQLImage;

    .line 637598
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->A()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 637599
    iput-object v1, v0, LX/25F;->ac:Lcom/facebook/graphql/model/GraphQLImage;

    .line 637600
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->Q()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 637601
    iput-object v1, v0, LX/25F;->ad:Lcom/facebook/graphql/model/GraphQLImage;

    .line 637602
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->B()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 637603
    iput-object v1, v0, LX/25F;->ae:Lcom/facebook/graphql/model/GraphQLImage;

    .line 637604
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->C()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 637605
    iput-object v1, v0, LX/25F;->af:Lcom/facebook/graphql/model/GraphQLImage;

    .line 637606
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->D()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 637607
    iput-object v1, v0, LX/25F;->ag:Lcom/facebook/graphql/model/GraphQLImage;

    .line 637608
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->E()Z

    move-result v1

    .line 637609
    iput-boolean v1, v0, LX/25F;->ah:Z

    .line 637610
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->R()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 637611
    iput-object v1, v0, LX/25F;->ai:Lcom/facebook/graphql/model/GraphQLImage;

    .line 637612
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->F()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 637613
    iput-object v1, v0, LX/25F;->am:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 637614
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->G()Lcom/facebook/graphql/model/GraphQLStreamingImage;

    move-result-object v1

    .line 637615
    iput-object v1, v0, LX/25F;->an:Lcom/facebook/graphql/model/GraphQLStreamingImage;

    .line 637616
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->H()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v1

    .line 637617
    iput-object v1, v0, LX/25F;->ap:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 637618
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->I()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 637619
    iput-object v1, v0, LX/25F;->aq:Lcom/facebook/graphql/model/GraphQLImage;

    .line 637620
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->J()I

    move-result v1

    .line 637621
    iput v1, v0, LX/25F;->av:I

    .line 637622
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->K()Ljava/lang/String;

    move-result-object v1

    .line 637623
    iput-object v1, v0, LX/25F;->ax:Ljava/lang/String;

    .line 637624
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->L()Ljava/lang/String;

    move-result-object v1

    .line 637625
    iput-object v1, v0, LX/25F;->ay:Ljava/lang/String;

    .line 637626
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->M()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v1

    .line 637627
    iput-object v1, v0, LX/25F;->aC:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 637628
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->N()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 637629
    iput-object v1, v0, LX/25F;->aF:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 637630
    new-instance v1, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v2, 0x41e065f

    invoke-direct {v1, v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    .line 637631
    iput-object v1, v0, LX/25F;->aH:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 637632
    invoke-virtual {v0}, LX/25F;->a()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    return-object v0
.end method
