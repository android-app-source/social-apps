.class public abstract LX/3cc;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Params:",
        "Ljava/lang/Object;",
        "Progress:",
        "Ljava/lang/Object;",
        "Result:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final a:Ljava/util/concurrent/ThreadFactory;

.field private static final b:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:LX/3ce;

.field public static final d:Ljava/util/concurrent/Executor;

.field private static volatile e:Ljava/util/concurrent/Executor;


# instance fields
.field private final f:LX/3ch;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/3ch",
            "<TParams;TResult;>;"
        }
    .end annotation
.end field

.field public final g:Ljava/util/concurrent/FutureTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/FutureTask",
            "<TResult;>;"
        }
    .end annotation
.end field

.field public volatile h:LX/3cf;

.field public final i:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    .line 614769
    new-instance v0, LX/3cd;

    invoke-direct {v0}, LX/3cd;-><init>()V

    sput-object v0, LX/3cc;->a:Ljava/util/concurrent/ThreadFactory;

    .line 614770
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>(I)V

    sput-object v0, LX/3cc;->b:Ljava/util/concurrent/BlockingQueue;

    .line 614771
    new-instance v1, Ljava/util/concurrent/ThreadPoolExecutor;

    const/4 v2, 0x5

    const/16 v3, 0x80

    const-wide/16 v4, 0x1

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    sget-object v7, LX/3cc;->b:Ljava/util/concurrent/BlockingQueue;

    sget-object v8, LX/3cc;->a:Ljava/util/concurrent/ThreadFactory;

    invoke-direct/range {v1 .. v8}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    sput-object v1, LX/3cc;->d:Ljava/util/concurrent/Executor;

    .line 614772
    new-instance v0, LX/3ce;

    invoke-direct {v0}, LX/3ce;-><init>()V

    sput-object v0, LX/3cc;->c:LX/3ce;

    .line 614773
    sget-object v0, LX/3cc;->d:Ljava/util/concurrent/Executor;

    sput-object v0, LX/3cc;->e:Ljava/util/concurrent/Executor;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 614763
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 614764
    sget-object v0, LX/3cf;->PENDING:LX/3cf;

    iput-object v0, p0, LX/3cc;->h:LX/3cf;

    .line 614765
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, LX/3cc;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 614766
    new-instance v0, LX/3cg;

    invoke-direct {v0, p0}, LX/3cg;-><init>(LX/3cc;)V

    iput-object v0, p0, LX/3cc;->f:LX/3ch;

    .line 614767
    new-instance v0, LX/3ci;

    iget-object v1, p0, LX/3cc;->f:LX/3ch;

    invoke-direct {v0, p0, v1}, LX/3ci;-><init>(LX/3cc;Ljava/util/concurrent/Callable;)V

    iput-object v0, p0, LX/3cc;->g:Ljava/util/concurrent/FutureTask;

    .line 614768
    return-void
.end method

.method public static b$redex0(LX/3cc;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TResult;)V"
        }
    .end annotation

    .prologue
    .line 614759
    iget-object v0, p0, LX/3cc;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    .line 614760
    if-nez v0, :cond_0

    .line 614761
    invoke-static {p0, p1}, LX/3cc;->c(LX/3cc;Ljava/lang/Object;)Ljava/lang/Object;

    .line 614762
    :cond_0
    return-void
.end method

.method public static c(LX/3cc;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TResult;)TResult;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 614744
    sget-object v0, LX/3cc;->c:LX/3ce;

    new-instance v1, LX/3ck;

    new-array v2, v4, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-direct {v1, p0, v2}, LX/3ck;-><init>(LX/3cc;[Ljava/lang/Object;)V

    invoke-virtual {v0, v4, v1}, LX/3ce;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 614745
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 614746
    return-object p1
.end method


# virtual methods
.method public final varargs a(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)LX/3cc;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Executor;",
            "[TParams;)",
            "LX/3cc",
            "<TParams;TProgress;TResult;>;"
        }
    .end annotation

    .prologue
    .line 614750
    iget-object v0, p0, LX/3cc;->h:LX/3cf;

    sget-object v1, LX/3cf;->PENDING:LX/3cf;

    if-eq v0, v1, :cond_0

    .line 614751
    sget-object v0, LX/3qh;->a:[I

    iget-object v1, p0, LX/3cc;->h:LX/3cf;

    invoke-virtual {v1}, LX/3cf;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 614752
    :cond_0
    sget-object v0, LX/3cf;->RUNNING:LX/3cf;

    iput-object v0, p0, LX/3cc;->h:LX/3cf;

    .line 614753
    invoke-virtual {p0}, LX/3cc;->b()V

    .line 614754
    iget-object v0, p0, LX/3cc;->f:LX/3ch;

    iput-object p2, v0, LX/3ch;->b:[Ljava/lang/Object;

    .line 614755
    iget-object v0, p0, LX/3cc;->g:Ljava/util/concurrent/FutureTask;

    const v1, 0x68f12ab7

    invoke-static {p1, v0, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 614756
    return-object p0

    .line 614757
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot execute task: the task is already running."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 614758
    :pswitch_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot execute task: the task has already been executed (a task can be executed only once)"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public varargs abstract a([Ljava/lang/Object;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TParams;)TResult;"
        }
    .end annotation
.end method

.method public a()V
    .locals 0

    .prologue
    .line 614749
    return-void
.end method

.method public a(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TResult;)V"
        }
    .end annotation

    .prologue
    .line 614748
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 614747
    return-void
.end method
