.class public final LX/3TB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/facebook/megaphone/api/FetchMegaphoneResult;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 583971
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 583973
    new-instance v0, Lcom/facebook/megaphone/api/FetchMegaphoneResult;

    invoke-direct {v0, p1}, Lcom/facebook/megaphone/api/FetchMegaphoneResult;-><init>(Landroid/os/Parcel;)V

    return-object v0
.end method

.method public final newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 583972
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/facebook/megaphone/api/FetchMegaphoneResult;

    return-object v0
.end method
