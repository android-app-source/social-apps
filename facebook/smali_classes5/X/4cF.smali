.class public LX/4cF;
.super Ljava/io/FilterInputStream;
.source ""


# instance fields
.field private final a:LX/1iX;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;LX/1iX;)V
    .locals 1

    .prologue
    .line 794776
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/InputStream;

    invoke-direct {p0, v0}, Ljava/io/FilterInputStream;-><init>(Ljava/io/InputStream;)V

    .line 794777
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1iX;

    iput-object v0, p0, LX/4cF;->a:LX/1iX;

    .line 794778
    return-void
.end method


# virtual methods
.method public final close()V
    .locals 1

    .prologue
    .line 794779
    invoke-super {p0}, Ljava/io/FilterInputStream;->close()V

    .line 794780
    iget-object v0, p0, LX/4cF;->a:LX/1iX;

    .line 794781
    iget-object p0, v0, LX/1iX;->b:LX/1hg;

    if-eqz p0, :cond_0

    .line 794782
    iget-object p0, v0, LX/1iX;->b:LX/1hg;

    invoke-virtual {p0}, LX/1hg;->a()V

    .line 794783
    :cond_0
    return-void
.end method

.method public final mark(I)V
    .locals 1

    .prologue
    .line 794784
    iget-object v0, p0, Ljava/io/FilterInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v0, p1}, Ljava/io/InputStream;->mark(I)V

    .line 794785
    return-void
.end method

.method public final markSupported()Z
    .locals 1

    .prologue
    .line 794786
    const/4 v0, 0x0

    return v0
.end method

.method public final read()I
    .locals 4

    .prologue
    .line 794787
    iget-object v0, p0, Ljava/io/FilterInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->read()I

    move-result v0

    .line 794788
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 794789
    iget-object v1, p0, LX/4cF;->a:LX/1iX;

    const-wide/16 v2, 0x1

    invoke-virtual {v1, v2, v3}, LX/1iX;->b(J)V

    .line 794790
    :cond_0
    return v0
.end method

.method public final read([BII)I
    .locals 4

    .prologue
    .line 794791
    iget-object v0, p0, Ljava/io/FilterInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/InputStream;->read([BII)I

    move-result v0

    .line 794792
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 794793
    iget-object v1, p0, LX/4cF;->a:LX/1iX;

    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, LX/1iX;->b(J)V

    .line 794794
    :cond_0
    return v0
.end method

.method public final reset()V
    .locals 2

    .prologue
    .line 794795
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Mark not supported"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final skip(J)J
    .locals 3

    .prologue
    .line 794796
    iget-object v0, p0, Ljava/io/FilterInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v0, p1, p2}, Ljava/io/InputStream;->skip(J)J

    move-result-wide v0

    .line 794797
    iget-object v2, p0, LX/4cF;->a:LX/1iX;

    invoke-virtual {v2, v0, v1}, LX/1iX;->b(J)V

    .line 794798
    return-wide v0
.end method
