.class public LX/3my;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Uh;


# direct methods
.method public constructor <init>(LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 637551
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 637552
    iput-object p1, p0, LX/3my;->a:LX/0Uh;

    .line 637553
    return-void
.end method

.method public static a(LX/0QB;)LX/3my;
    .locals 1

    .prologue
    .line 637554
    invoke-static {p0}, LX/3my;->b(LX/0QB;)LX/3my;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/3my;
    .locals 2

    .prologue
    .line 637555
    new-instance v1, LX/3my;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v0

    check-cast v0, LX/0Uh;

    invoke-direct {v1, v0}, LX/3my;-><init>(LX/0Uh;)V

    .line 637556
    return-object v1
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/enums/GraphQLGroupCategory;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 637557
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->CITY:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    if-ne p1, v1, :cond_0

    iget-object v1, p0, LX/3my;->a:LX/0Uh;

    const/16 v2, 0x33

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final b(Lcom/facebook/graphql/enums/GraphQLGroupCategory;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 637558
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->INTEREST:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    if-ne p1, v1, :cond_0

    iget-object v1, p0, LX/3my;->a:LX/0Uh;

    const/16 v2, 0x99

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final d(Lcom/facebook/graphql/enums/GraphQLGroupCategory;)Z
    .locals 1

    .prologue
    .line 637559
    invoke-virtual {p0, p1}, LX/3my;->a(Lcom/facebook/graphql/enums/GraphQLGroupCategory;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->COLLEGE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    invoke-virtual {v0, p1}, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
