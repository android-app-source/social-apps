.class public LX/4ln;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4lm;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/4ln;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 804785
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 804786
    return-void
.end method

.method public static a(LX/0QB;)LX/4ln;
    .locals 3

    .prologue
    .line 804771
    sget-object v0, LX/4ln;->a:LX/4ln;

    if-nez v0, :cond_1

    .line 804772
    const-class v1, LX/4ln;

    monitor-enter v1

    .line 804773
    :try_start_0
    sget-object v0, LX/4ln;->a:LX/4ln;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 804774
    if-eqz v2, :cond_0

    .line 804775
    :try_start_1
    new-instance v0, LX/4ln;

    invoke-direct {v0}, LX/4ln;-><init>()V

    .line 804776
    move-object v0, v0

    .line 804777
    sput-object v0, LX/4ln;->a:LX/4ln;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 804778
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 804779
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 804780
    :cond_1
    sget-object v0, LX/4ln;->a:LX/4ln;

    return-object v0

    .line 804781
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 804782
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 804783
    sget-boolean v0, LX/4lt;->d:Z

    move v0, v0

    .line 804784
    return v0
.end method
