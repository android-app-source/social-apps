.class public LX/4oI;
.super Landroid/graphics/drawable/Drawable;
.source ""

# interfaces
.implements Landroid/graphics/drawable/Drawable$Callback;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field public final a:[F

.field private final b:Landroid/graphics/Paint;

.field public final c:Landroid/graphics/Paint;

.field public final d:Landroid/graphics/Paint;

.field public final e:Landroid/graphics/RectF;

.field public final f:Landroid/graphics/RectF;

.field public final g:[Landroid/graphics/Path;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 809227
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 809228
    const/16 v1, 0x8

    new-array v1, v1, [F

    iput-object v1, p0, LX/4oI;->a:[F

    .line 809229
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, LX/4oI;->b:Landroid/graphics/Paint;

    .line 809230
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, LX/4oI;->c:Landroid/graphics/Paint;

    .line 809231
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, LX/4oI;->d:Landroid/graphics/Paint;

    .line 809232
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    iput-object v1, p0, LX/4oI;->e:Landroid/graphics/RectF;

    .line 809233
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    iput-object v1, p0, LX/4oI;->f:Landroid/graphics/RectF;

    .line 809234
    new-array v1, v3, [Landroid/graphics/Path;

    iput-object v1, p0, LX/4oI;->g:[Landroid/graphics/Path;

    .line 809235
    iget-object v1, p0, LX/4oI;->b:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 809236
    iget-object v1, p0, LX/4oI;->c:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 809237
    iget-object v1, p0, LX/4oI;->b:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 809238
    iget-object v1, p0, LX/4oI;->c:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 809239
    iget-object v1, p0, LX/4oI;->d:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 809240
    :goto_0
    if-ge v0, v3, :cond_0

    .line 809241
    iget-object v1, p0, LX/4oI;->g:[Landroid/graphics/Path;

    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    aput-object v2, v1, v0

    .line 809242
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 809243
    :cond_0
    return-void
.end method

.method private a()V
    .locals 12

    .prologue
    const/high16 v11, -0x3d4c0000    # -90.0f

    const/4 v10, 0x3

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/high16 v7, 0x40000000    # 2.0f

    .line 809309
    invoke-virtual {p0}, LX/4oI;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 809310
    iget-object v1, p0, LX/4oI;->e:Landroid/graphics/RectF;

    iget v2, v0, Landroid/graphics/Rect;->left:I

    int-to-float v2, v2

    iget v3, v0, Landroid/graphics/Rect;->top:I

    int-to-float v3, v3

    iget v4, v0, Landroid/graphics/Rect;->left:I

    int-to-float v4, v4

    iget-object v5, p0, LX/4oI;->a:[F

    const/4 v6, 0x0

    aget v5, v5, v6

    mul-float/2addr v5, v7

    add-float/2addr v4, v5

    iget v5, v0, Landroid/graphics/Rect;->top:I

    int-to-float v5, v5

    iget-object v6, p0, LX/4oI;->a:[F

    aget v6, v6, v9

    mul-float/2addr v6, v7

    add-float/2addr v5, v6

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/RectF;->set(FFFF)V

    .line 809311
    iget-object v1, p0, LX/4oI;->g:[Landroid/graphics/Path;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    .line 809312
    invoke-virtual {v1}, Landroid/graphics/Path;->reset()V

    .line 809313
    iget v2, v0, Landroid/graphics/Rect;->left:I

    int-to-float v2, v2

    iget v3, v0, Landroid/graphics/Rect;->top:I

    int-to-float v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 809314
    iget-object v2, p0, LX/4oI;->a:[F

    aget v2, v2, v9

    invoke-virtual {v1, v8, v2}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 809315
    iget-object v2, p0, LX/4oI;->e:Landroid/graphics/RectF;

    const/high16 v3, 0x43340000    # 180.0f

    const/high16 v4, 0x42b40000    # 90.0f

    invoke-virtual {v1, v2, v3, v4}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 809316
    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 809317
    iget-object v1, p0, LX/4oI;->e:Landroid/graphics/RectF;

    iget v2, v0, Landroid/graphics/Rect;->right:I

    int-to-float v2, v2

    iget-object v3, p0, LX/4oI;->a:[F

    const/4 v4, 0x2

    aget v3, v3, v4

    mul-float/2addr v3, v7

    sub-float/2addr v2, v3

    iget v3, v0, Landroid/graphics/Rect;->top:I

    int-to-float v3, v3

    iget v4, v0, Landroid/graphics/Rect;->right:I

    int-to-float v4, v4

    iget v5, v0, Landroid/graphics/Rect;->top:I

    int-to-float v5, v5

    iget-object v6, p0, LX/4oI;->a:[F

    aget v6, v6, v10

    mul-float/2addr v6, v7

    add-float/2addr v5, v6

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/RectF;->set(FFFF)V

    .line 809318
    iget-object v1, p0, LX/4oI;->g:[Landroid/graphics/Path;

    aget-object v1, v1, v9

    .line 809319
    invoke-virtual {v1}, Landroid/graphics/Path;->reset()V

    .line 809320
    iget v2, v0, Landroid/graphics/Rect;->right:I

    int-to-float v2, v2

    iget v3, v0, Landroid/graphics/Rect;->top:I

    int-to-float v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 809321
    iget-object v2, p0, LX/4oI;->a:[F

    aget v2, v2, v10

    invoke-virtual {v1, v8, v2}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 809322
    iget-object v2, p0, LX/4oI;->e:Landroid/graphics/RectF;

    invoke-virtual {v1, v2, v8, v11}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 809323
    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 809324
    iget-object v1, p0, LX/4oI;->e:Landroid/graphics/RectF;

    iget v2, v0, Landroid/graphics/Rect;->right:I

    int-to-float v2, v2

    iget-object v3, p0, LX/4oI;->a:[F

    const/4 v4, 0x4

    aget v3, v3, v4

    mul-float/2addr v3, v7

    sub-float/2addr v2, v3

    iget v3, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v3, v3

    iget-object v4, p0, LX/4oI;->a:[F

    const/4 v5, 0x5

    aget v4, v4, v5

    mul-float/2addr v4, v7

    sub-float/2addr v3, v4

    iget v4, v0, Landroid/graphics/Rect;->right:I

    int-to-float v4, v4

    iget v5, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v5, v5

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/RectF;->set(FFFF)V

    .line 809325
    iget-object v1, p0, LX/4oI;->g:[Landroid/graphics/Path;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    .line 809326
    invoke-virtual {v1}, Landroid/graphics/Path;->reset()V

    .line 809327
    iget v2, v0, Landroid/graphics/Rect;->right:I

    int-to-float v2, v2

    iget v3, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 809328
    iget-object v2, p0, LX/4oI;->a:[F

    const/4 v3, 0x5

    aget v2, v2, v3

    neg-float v2, v2

    invoke-virtual {v1, v8, v2}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 809329
    iget-object v2, p0, LX/4oI;->e:Landroid/graphics/RectF;

    const/high16 v3, 0x42b40000    # 90.0f

    invoke-virtual {v1, v2, v8, v3}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 809330
    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 809331
    iget-object v1, p0, LX/4oI;->e:Landroid/graphics/RectF;

    iget v2, v0, Landroid/graphics/Rect;->left:I

    int-to-float v2, v2

    iget v3, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v3, v3

    iget-object v4, p0, LX/4oI;->a:[F

    const/4 v5, 0x7

    aget v4, v4, v5

    mul-float/2addr v4, v7

    sub-float/2addr v3, v4

    iget v4, v0, Landroid/graphics/Rect;->left:I

    int-to-float v4, v4

    iget-object v5, p0, LX/4oI;->a:[F

    const/4 v6, 0x6

    aget v5, v5, v6

    mul-float/2addr v5, v7

    add-float/2addr v4, v5

    iget v5, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v5, v5

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/RectF;->set(FFFF)V

    .line 809332
    iget-object v1, p0, LX/4oI;->g:[Landroid/graphics/Path;

    aget-object v1, v1, v10

    .line 809333
    invoke-virtual {v1}, Landroid/graphics/Path;->reset()V

    .line 809334
    iget v2, v0, Landroid/graphics/Rect;->left:I

    int-to-float v2, v2

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v0, v0

    invoke-virtual {v1, v2, v0}, Landroid/graphics/Path;->moveTo(FF)V

    .line 809335
    iget-object v0, p0, LX/4oI;->a:[F

    const/4 v2, 0x7

    aget v0, v0, v2

    neg-float v0, v0

    invoke-virtual {v1, v8, v0}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 809336
    iget-object v0, p0, LX/4oI;->e:Landroid/graphics/RectF;

    const/high16 v2, 0x43340000    # 180.0f

    invoke-virtual {v1, v0, v2, v11}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 809337
    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 809338
    return-void
.end method

.method private static a(ILandroid/graphics/Paint;)V
    .locals 2

    .prologue
    .line 809306
    invoke-virtual {p1}, Landroid/graphics/Paint;->getAlpha()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x437f0000    # 255.0f

    div-float/2addr v0, v1

    int-to-float v1, p0

    mul-float/2addr v0, v1

    float-to-int v0, v0

    .line 809307
    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 809308
    return-void
.end method

.method private static d(I)I
    .locals 1

    .prologue
    .line 809302
    sparse-switch p0, :sswitch_data_0

    .line 809303
    const/4 v0, -0x3

    :goto_0
    return v0

    .line 809304
    :sswitch_0
    const/4 v0, -0x2

    goto :goto_0

    .line 809305
    :sswitch_1
    const/4 v0, -0x1

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xff -> :sswitch_1
    .end sparse-switch
.end method


# virtual methods
.method public final a(FFFF)V
    .locals 8

    .prologue
    const/4 v7, 0x6

    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 809292
    iget-object v0, p0, LX/4oI;->a:[F

    aget v0, v0, v2

    iget-object v3, p0, LX/4oI;->a:[F

    aget v3, v3, v1

    cmpl-float v0, v0, v3

    if-nez v0, :cond_0

    iget-object v0, p0, LX/4oI;->a:[F

    aget v0, v0, v2

    cmpl-float v0, v0, p1

    if-nez v0, :cond_0

    iget-object v0, p0, LX/4oI;->a:[F

    aget v0, v0, v5

    iget-object v3, p0, LX/4oI;->a:[F

    const/4 v4, 0x3

    aget v3, v3, v4

    cmpl-float v0, v0, v3

    if-nez v0, :cond_0

    iget-object v0, p0, LX/4oI;->a:[F

    aget v0, v0, v5

    cmpl-float v0, v0, p2

    if-nez v0, :cond_0

    iget-object v0, p0, LX/4oI;->a:[F

    aget v0, v0, v6

    iget-object v3, p0, LX/4oI;->a:[F

    const/4 v4, 0x5

    aget v3, v3, v4

    cmpl-float v0, v0, v3

    if-nez v0, :cond_0

    iget-object v0, p0, LX/4oI;->a:[F

    aget v0, v0, v6

    cmpl-float v0, v0, p3

    if-nez v0, :cond_0

    iget-object v0, p0, LX/4oI;->a:[F

    aget v0, v0, v7

    iget-object v3, p0, LX/4oI;->a:[F

    const/4 v4, 0x7

    aget v3, v3, v4

    cmpl-float v0, v0, v3

    if-nez v0, :cond_0

    iget-object v0, p0, LX/4oI;->a:[F

    aget v0, v0, v7

    cmpl-float v0, v0, p4

    if-nez v0, :cond_0

    move v0, v1

    .line 809293
    :goto_0
    if-eqz v0, :cond_1

    .line 809294
    :goto_1
    return-void

    :cond_0
    move v0, v2

    .line 809295
    goto :goto_0

    .line 809296
    :cond_1
    iget-object v0, p0, LX/4oI;->a:[F

    iget-object v3, p0, LX/4oI;->a:[F

    aput p1, v3, v1

    aput p1, v0, v2

    .line 809297
    iget-object v0, p0, LX/4oI;->a:[F

    iget-object v1, p0, LX/4oI;->a:[F

    const/4 v2, 0x3

    aput p2, v1, v2

    aput p2, v0, v5

    .line 809298
    iget-object v0, p0, LX/4oI;->a:[F

    iget-object v1, p0, LX/4oI;->a:[F

    const/4 v2, 0x5

    aput p3, v1, v2

    aput p3, v0, v6

    .line 809299
    iget-object v0, p0, LX/4oI;->a:[F

    iget-object v1, p0, LX/4oI;->a:[F

    const/4 v2, 0x7

    aput p4, v1, v2

    aput p4, v0, v7

    .line 809300
    invoke-direct {p0}, LX/4oI;->a()V

    .line 809301
    invoke-virtual {p0}, LX/4oI;->invalidateSelf()V

    goto :goto_1
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 809288
    iget-object v0, p0, LX/4oI;->b:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    if-ne p1, v0, :cond_0

    .line 809289
    :goto_0
    return-void

    .line 809290
    :cond_0
    iget-object v0, p0, LX/4oI;->b:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 809291
    invoke-virtual {p0}, LX/4oI;->invalidateSelf()V

    goto :goto_0
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 13

    .prologue
    .line 809263
    iget-object v0, p0, LX/4oI;->b:Landroid/graphics/Paint;

    const/4 v12, 0x1

    const/4 v11, 0x4

    const/high16 v10, 0x42b40000    # 90.0f

    const/4 v7, 0x0

    const/high16 v9, 0x40000000    # 2.0f

    .line 809264
    iget-object v1, p0, LX/4oI;->d:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->getColor()I

    move-result v1

    if-eqz v1, :cond_0

    .line 809265
    invoke-virtual {p0}, LX/4oI;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    iget-object v2, p0, LX/4oI;->d:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 809266
    :cond_0
    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    move-result v1

    if-eqz v1, :cond_1

    move v1, v7

    .line 809267
    :goto_0
    if-ge v1, v11, :cond_1

    .line 809268
    iget-object v2, p0, LX/4oI;->g:[Landroid/graphics/Path;

    aget-object v2, v2, v1

    invoke-virtual {p1, v2, v0}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 809269
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 809270
    :cond_1
    iget-object v1, p0, LX/4oI;->c:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->getColor()I

    move-result v1

    if-eqz v1, :cond_2

    .line 809271
    iget-object v1, p0, LX/4oI;->c:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v1

    div-float/2addr v1, v9

    .line 809272
    invoke-virtual {p0}, LX/4oI;->getBounds()Landroid/graphics/Rect;

    move-result-object v2

    .line 809273
    iget-object v3, p0, LX/4oI;->f:Landroid/graphics/RectF;

    iget v4, v2, Landroid/graphics/Rect;->left:I

    int-to-float v4, v4

    add-float/2addr v4, v1

    iget v5, v2, Landroid/graphics/Rect;->top:I

    int-to-float v5, v5

    add-float/2addr v5, v1

    iget v6, v2, Landroid/graphics/Rect;->right:I

    int-to-float v6, v6

    sub-float/2addr v6, v1

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    int-to-float v2, v2

    sub-float v1, v2, v1

    invoke-virtual {v3, v4, v5, v6, v1}, Landroid/graphics/RectF;->set(FFFF)V

    .line 809274
    iget-object v8, p0, LX/4oI;->f:Landroid/graphics/RectF;

    .line 809275
    iget v2, v8, Landroid/graphics/RectF;->left:F

    iget v1, v8, Landroid/graphics/RectF;->top:F

    iget-object v3, p0, LX/4oI;->a:[F

    aget v3, v3, v12

    add-float/2addr v3, v1

    iget v4, v8, Landroid/graphics/RectF;->left:F

    iget v1, v8, Landroid/graphics/RectF;->bottom:F

    iget-object v5, p0, LX/4oI;->a:[F

    const/4 v6, 0x7

    aget v5, v5, v6

    sub-float v5, v1, v5

    iget-object v6, p0, LX/4oI;->c:Landroid/graphics/Paint;

    move-object v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 809276
    iget v1, v8, Landroid/graphics/RectF;->left:F

    iget-object v2, p0, LX/4oI;->a:[F

    aget v2, v2, v7

    add-float/2addr v2, v1

    iget v3, v8, Landroid/graphics/RectF;->top:F

    iget v1, v8, Landroid/graphics/RectF;->right:F

    iget-object v4, p0, LX/4oI;->a:[F

    const/4 v5, 0x2

    aget v4, v4, v5

    sub-float v4, v1, v4

    iget v5, v8, Landroid/graphics/RectF;->top:F

    iget-object v6, p0, LX/4oI;->c:Landroid/graphics/Paint;

    move-object v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 809277
    iget v2, v8, Landroid/graphics/RectF;->right:F

    iget v1, v8, Landroid/graphics/RectF;->top:F

    iget-object v3, p0, LX/4oI;->a:[F

    const/4 v4, 0x3

    aget v3, v3, v4

    add-float/2addr v3, v1

    iget v4, v8, Landroid/graphics/RectF;->right:F

    iget v1, v8, Landroid/graphics/RectF;->bottom:F

    iget-object v5, p0, LX/4oI;->a:[F

    const/4 v6, 0x5

    aget v5, v5, v6

    sub-float v5, v1, v5

    iget-object v6, p0, LX/4oI;->c:Landroid/graphics/Paint;

    move-object v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 809278
    iget v1, v8, Landroid/graphics/RectF;->left:F

    iget-object v2, p0, LX/4oI;->a:[F

    const/4 v3, 0x6

    aget v2, v2, v3

    add-float/2addr v2, v1

    iget v3, v8, Landroid/graphics/RectF;->bottom:F

    iget v1, v8, Landroid/graphics/RectF;->right:F

    iget-object v4, p0, LX/4oI;->a:[F

    aget v4, v4, v11

    sub-float v4, v1, v4

    iget v5, v8, Landroid/graphics/RectF;->bottom:F

    iget-object v6, p0, LX/4oI;->c:Landroid/graphics/Paint;

    move-object v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 809279
    iget-object v1, p0, LX/4oI;->e:Landroid/graphics/RectF;

    iget v2, v8, Landroid/graphics/RectF;->left:F

    iget v3, v8, Landroid/graphics/RectF;->top:F

    iget-object v4, p0, LX/4oI;->a:[F

    aget v4, v4, v7

    mul-float/2addr v4, v9

    iget-object v5, p0, LX/4oI;->a:[F

    aget v5, v5, v12

    mul-float/2addr v5, v9

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/RectF;->set(FFFF)V

    .line 809280
    iget-object v2, p0, LX/4oI;->e:Landroid/graphics/RectF;

    const/high16 v3, 0x43340000    # 180.0f

    iget-object v6, p0, LX/4oI;->c:Landroid/graphics/Paint;

    move-object v1, p1

    move v4, v10

    move v5, v7

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 809281
    iget-object v1, p0, LX/4oI;->e:Landroid/graphics/RectF;

    iget v2, v8, Landroid/graphics/RectF;->right:F

    iget-object v3, p0, LX/4oI;->a:[F

    const/4 v4, 0x2

    aget v3, v3, v4

    mul-float/2addr v3, v9

    sub-float/2addr v2, v3

    iget v3, v8, Landroid/graphics/RectF;->top:F

    iget v4, v8, Landroid/graphics/RectF;->right:F

    iget-object v5, p0, LX/4oI;->a:[F

    const/4 v6, 0x3

    aget v5, v5, v6

    mul-float/2addr v5, v9

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/RectF;->set(FFFF)V

    .line 809282
    iget-object v2, p0, LX/4oI;->e:Landroid/graphics/RectF;

    const/high16 v3, 0x43870000    # 270.0f

    iget-object v6, p0, LX/4oI;->c:Landroid/graphics/Paint;

    move-object v1, p1

    move v4, v10

    move v5, v7

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 809283
    iget-object v1, p0, LX/4oI;->e:Landroid/graphics/RectF;

    iget v2, v8, Landroid/graphics/RectF;->right:F

    iget-object v3, p0, LX/4oI;->a:[F

    aget v3, v3, v11

    mul-float/2addr v3, v9

    sub-float/2addr v2, v3

    iget v3, v8, Landroid/graphics/RectF;->bottom:F

    iget-object v4, p0, LX/4oI;->a:[F

    const/4 v5, 0x5

    aget v4, v4, v5

    mul-float/2addr v4, v9

    sub-float/2addr v3, v4

    iget v4, v8, Landroid/graphics/RectF;->right:F

    iget v5, v8, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/RectF;->set(FFFF)V

    .line 809284
    iget-object v2, p0, LX/4oI;->e:Landroid/graphics/RectF;

    const/4 v3, 0x0

    iget-object v6, p0, LX/4oI;->c:Landroid/graphics/Paint;

    move-object v1, p1

    move v4, v10

    move v5, v7

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 809285
    iget-object v1, p0, LX/4oI;->e:Landroid/graphics/RectF;

    iget v2, v8, Landroid/graphics/RectF;->left:F

    iget v3, v8, Landroid/graphics/RectF;->bottom:F

    iget-object v4, p0, LX/4oI;->a:[F

    const/4 v5, 0x7

    aget v4, v4, v5

    mul-float/2addr v4, v9

    sub-float/2addr v3, v4

    iget v4, v8, Landroid/graphics/RectF;->left:F

    iget-object v5, p0, LX/4oI;->a:[F

    const/4 v6, 0x6

    aget v5, v5, v6

    mul-float/2addr v5, v9

    add-float/2addr v4, v5

    iget v5, v8, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/RectF;->set(FFFF)V

    .line 809286
    iget-object v2, p0, LX/4oI;->e:Landroid/graphics/RectF;

    iget-object v6, p0, LX/4oI;->c:Landroid/graphics/Paint;

    move-object v1, p1

    move v3, v10

    move v4, v10

    move v5, v7

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 809287
    :cond_2
    return-void
.end method

.method public final getOpacity()I
    .locals 2

    .prologue
    .line 809339
    iget-object v0, p0, LX/4oI;->b:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getAlpha()I

    move-result v0

    invoke-static {v0}, LX/4oI;->d(I)I

    move-result v0

    iget-object v1, p0, LX/4oI;->c:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->getAlpha()I

    move-result v1

    invoke-static {v1}, LX/4oI;->d(I)I

    move-result v1

    invoke-static {v0, v1}, LX/4oI;->resolveOpacity(II)I

    move-result v0

    .line 809340
    iget-object v1, p0, LX/4oI;->d:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->getAlpha()I

    move-result v1

    invoke-static {v1}, LX/4oI;->d(I)I

    move-result v1

    invoke-static {v0, v1}, LX/4oI;->resolveOpacity(II)I

    move-result v0

    return v0
.end method

.method public final invalidateDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 809261
    invoke-virtual {p0}, LX/4oI;->invalidateSelf()V

    .line 809262
    return-void
.end method

.method public final onBoundsChange(Landroid/graphics/Rect;)V
    .locals 0

    .prologue
    .line 809258
    invoke-super {p0, p1}, Landroid/graphics/drawable/Drawable;->onBoundsChange(Landroid/graphics/Rect;)V

    .line 809259
    invoke-direct {p0}, LX/4oI;->a()V

    .line 809260
    return-void
.end method

.method public final scheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;J)V
    .locals 1

    .prologue
    .line 809256
    invoke-virtual {p0, p2, p3, p4}, LX/4oI;->scheduleSelf(Ljava/lang/Runnable;J)V

    .line 809257
    return-void
.end method

.method public final setAlpha(I)V
    .locals 1

    .prologue
    .line 809251
    iget-object v0, p0, LX/4oI;->c:Landroid/graphics/Paint;

    invoke-static {p1, v0}, LX/4oI;->a(ILandroid/graphics/Paint;)V

    .line 809252
    iget-object v0, p0, LX/4oI;->b:Landroid/graphics/Paint;

    invoke-static {p1, v0}, LX/4oI;->a(ILandroid/graphics/Paint;)V

    .line 809253
    iget-object v0, p0, LX/4oI;->d:Landroid/graphics/Paint;

    invoke-static {p1, v0}, LX/4oI;->a(ILandroid/graphics/Paint;)V

    .line 809254
    invoke-virtual {p0}, LX/4oI;->invalidateSelf()V

    .line 809255
    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1

    .prologue
    .line 809246
    iget-object v0, p0, LX/4oI;->c:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 809247
    iget-object v0, p0, LX/4oI;->b:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 809248
    iget-object v0, p0, LX/4oI;->d:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 809249
    invoke-virtual {p0}, LX/4oI;->invalidateSelf()V

    .line 809250
    return-void
.end method

.method public final unscheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 809244
    invoke-virtual {p0, p2}, LX/4oI;->unscheduleSelf(Ljava/lang/Runnable;)V

    .line 809245
    return-void
.end method
