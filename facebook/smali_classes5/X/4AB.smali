.class public abstract LX/4AB;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/dracula/runtime/jdk/DraculaMap$1$Dracula",
        "<TK;>;"
    }
.end annotation


# instance fields
.field public a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<TK;>;"
        }
    .end annotation
.end field

.field public b:LX/39P;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 675519
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 675520
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;LX/15i;II)LX/1vs;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "LX/15i;",
            "II)",
            "LX/1vs;"
        }
    .end annotation

    .prologue
    .line 675417
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 675517
    invoke-virtual {p0}, LX/4AB;->b()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 675518
    return-void
.end method

.method public a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 675504
    invoke-virtual {p0}, LX/4AB;->b()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 675505
    if-eqz p1, :cond_1

    .line 675506
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 675507
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4AI;

    .line 675508
    iget-object p0, v0, LX/4AI;->a:Ljava/lang/Object;

    move-object v0, p0

    .line 675509
    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 675510
    :goto_0
    return v0

    .line 675511
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 675512
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4AI;

    .line 675513
    iget-object p0, v0, LX/4AI;->a:Ljava/lang/Object;

    move-object v0, p0

    .line 675514
    if-nez v0, :cond_1

    move v0, v1

    .line 675515
    goto :goto_0

    .line 675516
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Ljava/lang/Object;)LX/1vs;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 675482
    invoke-virtual {p0}, LX/4AB;->b()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 675483
    if-eqz p1, :cond_1

    .line 675484
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 675485
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4AI;

    .line 675486
    iget-object v2, v0, LX/4AI;->a:Ljava/lang/Object;

    move-object v2, v2

    .line 675487
    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 675488
    invoke-virtual {v0}, LX/4AI;->b()LX/1vs;

    move-result-object v0

    .line 675489
    iget-object v1, v0, LX/1vs;->a:LX/15i;

    .line 675490
    iget v2, v0, LX/1vs;->b:I

    .line 675491
    iget v0, v0, LX/1vs;->c:I

    .line 675492
    invoke-static {v1, v2, v0}, LX/1vs;->a(LX/15i;II)LX/1vs;

    move-result-object v0

    .line 675493
    :goto_0
    return-object v0

    .line 675494
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 675495
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4AI;

    .line 675496
    iget-object v2, v0, LX/4AI;->a:Ljava/lang/Object;

    move-object v2, v2

    .line 675497
    if-nez v2, :cond_1

    .line 675498
    invoke-virtual {v0}, LX/4AI;->b()LX/1vs;

    move-result-object v0

    .line 675499
    iget-object v1, v0, LX/1vs;->a:LX/15i;

    .line 675500
    iget v2, v0, LX/1vs;->b:I

    .line 675501
    iget v0, v0, LX/1vs;->c:I

    .line 675502
    invoke-static {v1, v2, v0}, LX/1vs;->a(LX/15i;II)LX/1vs;

    move-result-object v0

    goto :goto_0

    .line 675503
    :cond_2
    const/4 v0, 0x0

    invoke-static {v0, v3, v3}, LX/1vs;->a(LX/15i;II)LX/1vs;

    move-result-object v0

    goto :goto_0
.end method

.method public abstract b()Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/dracula/runtime/jdk/DraculaMap$1$Dracula$Entry$1$Dracula",
            "<TK;>;>;"
        }
    .end annotation
.end method

.method public c(Ljava/lang/Object;)LX/1vs;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 675521
    invoke-virtual {p0}, LX/4AB;->b()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 675522
    if-eqz p1, :cond_1

    .line 675523
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 675524
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4AI;

    .line 675525
    iget-object v2, v0, LX/4AI;->a:Ljava/lang/Object;

    move-object v2, v2

    .line 675526
    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 675527
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 675528
    invoke-virtual {v0}, LX/4AI;->b()LX/1vs;

    move-result-object v0

    .line 675529
    iget-object v1, v0, LX/1vs;->a:LX/15i;

    .line 675530
    iget v2, v0, LX/1vs;->b:I

    .line 675531
    iget v0, v0, LX/1vs;->c:I

    .line 675532
    invoke-static {v1, v2, v0}, LX/1vs;->a(LX/15i;II)LX/1vs;

    move-result-object v0

    .line 675533
    :goto_0
    return-object v0

    .line 675534
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 675535
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4AI;

    .line 675536
    iget-object v2, v0, LX/4AI;->a:Ljava/lang/Object;

    move-object v2, v2

    .line 675537
    if-nez v2, :cond_1

    .line 675538
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 675539
    invoke-virtual {v0}, LX/4AI;->b()LX/1vs;

    move-result-object v0

    .line 675540
    iget-object v1, v0, LX/1vs;->a:LX/15i;

    .line 675541
    iget v2, v0, LX/1vs;->b:I

    .line 675542
    iget v0, v0, LX/1vs;->c:I

    .line 675543
    invoke-static {v1, v2, v0}, LX/1vs;->a(LX/15i;II)LX/1vs;

    move-result-object v0

    goto :goto_0

    .line 675544
    :cond_2
    const/4 v0, 0x0

    invoke-static {v0, v3, v3}, LX/1vs;->a(LX/15i;II)LX/1vs;

    move-result-object v0

    goto :goto_0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 675481
    invoke-virtual {p0}, LX/4AB;->d()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public clone()Ljava/lang/Object;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 675477
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4AB;

    .line 675478
    iput-object v1, v0, LX/4AB;->a:Ljava/util/Set;

    .line 675479
    iput-object v1, v0, LX/4AB;->b:LX/39P;

    .line 675480
    return-object v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 675476
    invoke-virtual {p0}, LX/4AB;->b()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 675448
    if-ne p0, p1, :cond_0

    move v0, v1

    .line 675449
    :goto_0
    return v0

    .line 675450
    :cond_0
    :try_start_0
    check-cast p1, LX/4AB;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 675451
    invoke-virtual {p0}, LX/4AB;->d()I

    move-result v0

    invoke-virtual {p1}, LX/4AB;->d()I

    move-result v3

    if-eq v0, v3, :cond_1

    move v0, v2

    .line 675452
    goto :goto_0

    .line 675453
    :catch_0
    move v0, v2

    goto :goto_0

    .line 675454
    :cond_1
    :try_start_1
    invoke-virtual {p0}, LX/4AB;->b()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4AI;

    .line 675455
    iget-object v4, v0, LX/4AI;->a:Ljava/lang/Object;

    move-object v4, v4

    .line 675456
    invoke-virtual {v0}, LX/4AI;->b()LX/1vs;

    move-result-object v0

    .line 675457
    iget-object v5, v0, LX/1vs;->a:LX/15i;

    .line 675458
    iget v6, v0, LX/1vs;->b:I

    .line 675459
    sget-object v7, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v7
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/ClassCastException; {:try_start_1 .. :try_end_1} :catch_2

    .line 675460
    :try_start_2
    monitor-exit v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 675461
    :try_start_3
    invoke-virtual {p1, v4}, LX/4AB;->b(Ljava/lang/Object;)LX/1vs;

    move-result-object v0

    .line 675462
    iget-object v7, v0, LX/1vs;->a:LX/15i;

    .line 675463
    iget v8, v0, LX/1vs;->b:I

    .line 675464
    sget-object v9, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v9
    :try_end_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/ClassCastException; {:try_start_3 .. :try_end_3} :catch_2

    .line 675465
    :try_start_4
    monitor-exit v9
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 675466
    if-nez v6, :cond_4

    .line 675467
    if-nez v8, :cond_3

    :try_start_5
    invoke-virtual {p1, v4}, LX/4AB;->a(Ljava/lang/Object;)Z
    :try_end_5
    .catch Ljava/lang/NullPointerException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/ClassCastException; {:try_start_5 .. :try_end_5} :catch_2

    move-result v0

    if-nez v0, :cond_2

    :cond_3
    move v0, v2

    .line 675468
    goto :goto_0

    .line 675469
    :catchall_0
    move-exception v0

    :try_start_6
    monitor-exit v7
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :try_start_7
    throw v0
    :try_end_7
    .catch Ljava/lang/NullPointerException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/lang/ClassCastException; {:try_start_7 .. :try_end_7} :catch_2

    .line 675470
    :catch_1
    move v0, v2

    goto :goto_0

    .line 675471
    :catchall_1
    move-exception v0

    :try_start_8
    monitor-exit v9
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    :try_start_9
    throw v0

    .line 675472
    :catch_2
    move v0, v2

    goto :goto_0

    .line 675473
    :cond_4
    invoke-static {v5, v6, v7, v8}, LX/1vu;->a(LX/15i;ILX/15i;I)Z
    :try_end_9
    .catch Ljava/lang/NullPointerException; {:try_start_9 .. :try_end_9} :catch_1
    .catch Ljava/lang/ClassCastException; {:try_start_9 .. :try_end_9} :catch_2

    move-result v0

    if-nez v0, :cond_2

    move v0, v2

    .line 675474
    goto :goto_0

    :cond_5
    move v0, v1

    .line 675475
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 675443
    const/4 v0, 0x0

    .line 675444
    invoke-virtual {p0}, LX/4AB;->b()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    .line 675445
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 675446
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4AI;

    invoke-virtual {v0}, LX/4AI;->hashCode()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_0

    .line 675447
    :cond_0
    return v1
.end method

.method public final toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 675418
    invoke-virtual {p0}, LX/4AB;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 675419
    const-string v0, "{}"

    .line 675420
    :goto_0
    return-object v0

    .line 675421
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, LX/4AB;->d()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1c

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 675422
    const/16 v0, 0x7b

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 675423
    invoke-virtual {p0}, LX/4AB;->b()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 675424
    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 675425
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4AI;

    .line 675426
    iget-object v3, v0, LX/4AI;->a:Ljava/lang/Object;

    move-object v3, v3

    .line 675427
    if-eq v3, p0, :cond_2

    .line 675428
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 675429
    :goto_2
    const/16 v3, 0x3d

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 675430
    invoke-virtual {v0}, LX/4AI;->b()LX/1vs;

    move-result-object v0

    .line 675431
    iget-object v3, v0, LX/1vs;->a:LX/15i;

    .line 675432
    iget v4, v0, LX/1vs;->b:I

    .line 675433
    iget v0, v0, LX/1vs;->c:I

    .line 675434
    sget-object v5, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v5

    .line 675435
    :try_start_0
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 675436
    invoke-static {v3, v4, v0}, LX/1vu;->a(LX/15i;II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 675437
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 675438
    const-string v0, ", "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 675439
    :cond_2
    const-string v3, "(this DraculaMap)"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 675440
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 675441
    :cond_3
    const/16 v0, 0x7d

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 675442
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
