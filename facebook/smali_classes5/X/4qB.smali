.class public final LX/4qB;
.super LX/32s;
.source ""


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final _annotated:LX/2At;

.field public final transient c:Ljava/lang/reflect/Method;


# direct methods
.method public constructor <init>(LX/2Aq;LX/0lJ;LX/4qw;LX/0lQ;LX/2At;)V
    .locals 1

    .prologue
    .line 813170
    invoke-direct {p0, p1, p2, p3, p4}, LX/32s;-><init>(LX/2Aq;LX/0lJ;LX/4qw;LX/0lQ;)V

    .line 813171
    iput-object p5, p0, LX/4qB;->_annotated:LX/2At;

    .line 813172
    iget-object v0, p5, LX/2At;->a:Ljava/lang/reflect/Method;

    move-object v0, v0

    .line 813173
    iput-object v0, p0, LX/4qB;->c:Ljava/lang/reflect/Method;

    .line 813174
    return-void
.end method

.method private constructor <init>(LX/4qB;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4qB;",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 813166
    invoke-direct {p0, p1, p2}, LX/32s;-><init>(LX/32s;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 813167
    iget-object v0, p1, LX/4qB;->_annotated:LX/2At;

    iput-object v0, p0, LX/4qB;->_annotated:LX/2At;

    .line 813168
    iget-object v0, p1, LX/4qB;->c:Ljava/lang/reflect/Method;

    iput-object v0, p0, LX/4qB;->c:Ljava/lang/reflect/Method;

    .line 813169
    return-void
.end method

.method private constructor <init>(LX/4qB;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 813162
    invoke-direct {p0, p1, p2}, LX/32s;-><init>(LX/32s;Ljava/lang/String;)V

    .line 813163
    iget-object v0, p1, LX/4qB;->_annotated:LX/2At;

    iput-object v0, p0, LX/4qB;->_annotated:LX/2At;

    .line 813164
    iget-object v0, p1, LX/4qB;->c:Ljava/lang/reflect/Method;

    iput-object v0, p0, LX/4qB;->c:Ljava/lang/reflect/Method;

    .line 813165
    return-void
.end method

.method private constructor <init>(LX/4qB;Ljava/lang/reflect/Method;)V
    .locals 1

    .prologue
    .line 813158
    invoke-direct {p0, p1}, LX/32s;-><init>(LX/32s;)V

    .line 813159
    iget-object v0, p1, LX/4qB;->_annotated:LX/2At;

    iput-object v0, p0, LX/4qB;->_annotated:LX/2At;

    .line 813160
    iput-object p2, p0, LX/4qB;->c:Ljava/lang/reflect/Method;

    .line 813161
    return-void
.end method

.method private a(Lcom/fasterxml/jackson/databind/JsonDeserializer;)LX/4qB;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fasterxml/jackson/databind/JsonDeserializer",
            "<*>;)",
            "LX/4qB;"
        }
    .end annotation

    .prologue
    .line 813157
    new-instance v0, LX/4qB;

    invoke-direct {v0, p0, p1}, LX/4qB;-><init>(LX/4qB;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    return-object v0
.end method

.method private c(Ljava/lang/String;)LX/4qB;
    .locals 1

    .prologue
    .line 813156
    new-instance v0, LX/4qB;

    invoke-direct {v0, p0, p1}, LX/4qB;-><init>(LX/4qB;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/String;)LX/32s;
    .locals 1

    .prologue
    .line 813155
    invoke-direct {p0, p1}, LX/4qB;->c(Ljava/lang/String;)LX/4qB;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15w;LX/0n3;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 813136
    invoke-virtual {p0, p1, p2}, LX/32s;->a(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, p3, v0}, LX/32s;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 813137
    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 813151
    :try_start_0
    iget-object v0, p0, LX/4qB;->c:Ljava/lang/reflect/Method;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    invoke-virtual {v0, p1, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 813152
    :goto_0
    return-void

    .line 813153
    :catch_0
    move-exception v0

    .line 813154
    invoke-virtual {p0, v0, p2}, LX/32s;->a(Ljava/lang/Exception;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final b()LX/2An;
    .locals 1

    .prologue
    .line 813150
    iget-object v0, p0, LX/4qB;->_annotated:LX/2At;

    return-object v0
.end method

.method public final synthetic b(Lcom/fasterxml/jackson/databind/JsonDeserializer;)LX/32s;
    .locals 1

    .prologue
    .line 813149
    invoke-direct {p0, p1}, LX/4qB;->a(Lcom/fasterxml/jackson/databind/JsonDeserializer;)LX/4qB;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15w;LX/0n3;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 813148
    invoke-virtual {p0, p1, p2}, LX/32s;->a(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, p3, v0}, LX/32s;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 813141
    :try_start_0
    iget-object v0, p0, LX/4qB;->c:Ljava/lang/reflect/Method;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    invoke-virtual {v0, p1, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 813142
    if-nez v0, :cond_0

    .line 813143
    :goto_0
    return-object p1

    :cond_0
    move-object p1, v0

    .line 813144
    goto :goto_0

    .line 813145
    :catch_0
    move-exception v0

    .line 813146
    invoke-virtual {p0, v0, p2}, LX/32s;->a(Ljava/lang/Exception;Ljava/lang/Object;)V

    .line 813147
    const/4 p1, 0x0

    goto :goto_0
.end method

.method public final readResolve()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 813138
    new-instance v0, LX/4qB;

    iget-object v1, p0, LX/4qB;->_annotated:LX/2At;

    .line 813139
    iget-object v2, v1, LX/2At;->a:Ljava/lang/reflect/Method;

    move-object v1, v2

    .line 813140
    invoke-direct {v0, p0, v1}, LX/4qB;-><init>(LX/4qB;Ljava/lang/reflect/Method;)V

    return-object v0
.end method
