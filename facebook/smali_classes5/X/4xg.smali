.class public final LX/4xg;
.super LX/2Tb;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2Tb",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/4xj;


# direct methods
.method public constructor <init>(LX/4xj;)V
    .locals 0

    .prologue
    .line 821219
    iput-object p1, p0, LX/4xg;->a:LX/4xj;

    .line 821220
    invoke-direct {p0, p1}, LX/2Tb;-><init>(LX/0Xu;)V

    .line 821221
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/google/common/collect/Multiset$Entry",
            "<TK;>;>;"
        }
    .end annotation

    .prologue
    .line 821222
    new-instance v0, LX/4xf;

    invoke-direct {v0, p0}, LX/4xf;-><init>(LX/4xg;)V

    return-object v0
.end method

.method public final b(Ljava/lang/Object;I)I
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 821223
    const-string v0, "occurrences"

    invoke-static {p2, v0}, LX/0P6;->a(ILjava/lang/String;)I

    .line 821224
    if-nez p2, :cond_1

    .line 821225
    invoke-virtual {p0, p1}, LX/1M0;->a(Ljava/lang/Object;)I

    move-result v1

    .line 821226
    :cond_0
    :goto_0
    return v1

    .line 821227
    :cond_1
    iget-object v0, p0, LX/4xg;->a:LX/4xj;

    iget-object v0, v0, LX/4xj;->a:LX/0Xu;

    invoke-interface {v0}, LX/0Xu;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 821228
    if-eqz v0, :cond_0

    .line 821229
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v0, v1

    .line 821230
    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 821231
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 821232
    iget-object v3, p0, LX/4xg;->a:LX/4xj;

    invoke-static {v3, p1, v1}, LX/4xj;->d(LX/4xj;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 821233
    add-int/lit8 v0, v0, 0x1

    .line 821234
    if-gt v0, p2, :cond_2

    .line 821235
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    :cond_3
    move v1, v0

    .line 821236
    goto :goto_0
.end method
