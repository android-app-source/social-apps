.class public LX/4hs;
.super LX/4hr;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/4hr",
        "<",
        "LX/4hu;",
        ">;"
    }
.end annotation


# static fields
.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 802127
    const-class v0, LX/4hs;

    sput-object v0, LX/4hs;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Or;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/4hu;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 802128
    const v0, 0x10006

    invoke-direct {p0, p1, v0}, LX/4hr;-><init>(LX/0Or;I)V

    .line 802129
    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Message;LX/4ht;)V
    .locals 4

    .prologue
    .line 802130
    invoke-static {p1}, Landroid/os/Message;->obtain(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v0

    .line 802131
    iget v1, p1, Landroid/os/Message;->arg1:I

    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 802132
    iget v1, p1, Landroid/os/Message;->arg2:I

    iput v1, v0, Landroid/os/Message;->arg2:I

    .line 802133
    const v1, 0x10007

    iput v1, v0, Landroid/os/Message;->what:I

    .line 802134
    move-object v0, v0

    .line 802135
    const/4 v1, 0x0

    const-string v2, "PermissionDenied"

    const-string v3, "Cannot retrieve like status for the provided object id"

    invoke-static {v1, v2, v3}, LX/4hW;->a(Lcom/facebook/platform/common/action/PlatformAppCall;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 802136
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 802137
    :try_start_0
    iget-object v1, p1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    invoke-virtual {v1, v0}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 802138
    :goto_0
    return-void

    .line 802139
    :catch_0
    move-exception v0

    .line 802140
    sget-object v1, LX/4hs;->b:Ljava/lang/Class;

    const-string v2, "Unable to respond to like state request"

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
