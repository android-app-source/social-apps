.class public LX/4q8;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:[LX/4q7;

.field private final b:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final c:[Ljava/lang/String;

.field private final d:[LX/0nW;


# direct methods
.method private constructor <init>(LX/4q8;)V
    .locals 2

    .prologue
    .line 813044
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 813045
    iget-object v0, p1, LX/4q8;->a:[LX/4q7;

    iput-object v0, p0, LX/4q8;->a:[LX/4q7;

    .line 813046
    iget-object v0, p1, LX/4q8;->b:Ljava/util/HashMap;

    iput-object v0, p0, LX/4q8;->b:Ljava/util/HashMap;

    .line 813047
    iget-object v0, p0, LX/4q8;->a:[LX/4q7;

    array-length v0, v0

    .line 813048
    new-array v1, v0, [Ljava/lang/String;

    iput-object v1, p0, LX/4q8;->c:[Ljava/lang/String;

    .line 813049
    new-array v0, v0, [LX/0nW;

    iput-object v0, p0, LX/4q8;->d:[LX/0nW;

    .line 813050
    return-void
.end method

.method public constructor <init>([LX/4q7;Ljava/util/HashMap;[Ljava/lang/String;[LX/0nW;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "LX/4q7;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;[",
            "Ljava/lang/String;",
            "[",
            "LX/0nW;",
            ")V"
        }
    .end annotation

    .prologue
    .line 813038
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 813039
    iput-object p1, p0, LX/4q8;->a:[LX/4q7;

    .line 813040
    iput-object p2, p0, LX/4q8;->b:Ljava/util/HashMap;

    .line 813041
    iput-object p3, p0, LX/4q8;->c:[Ljava/lang/String;

    .line 813042
    iput-object p4, p0, LX/4q8;->d:[LX/0nW;

    .line 813043
    return-void
.end method

.method private a(LX/15w;LX/0n3;ILjava/lang/String;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 813026
    new-instance v0, LX/0nW;

    invoke-virtual {p1}, LX/15w;->a()LX/0lD;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0nW;-><init>(LX/0lD;)V

    .line 813027
    invoke-virtual {v0}, LX/0nX;->d()V

    .line 813028
    invoke-virtual {v0, p4}, LX/0nX;->b(Ljava/lang/String;)V

    .line 813029
    iget-object v1, p0, LX/4q8;->d:[LX/0nW;

    aget-object v1, v1, p3

    invoke-virtual {v1, p1}, LX/0nW;->a(LX/15w;)LX/15w;

    move-result-object v1

    .line 813030
    invoke-virtual {v1}, LX/15w;->c()LX/15z;

    .line 813031
    invoke-virtual {v0, v1}, LX/0nW;->b(LX/15w;)V

    .line 813032
    invoke-virtual {v0}, LX/0nX;->e()V

    .line 813033
    invoke-virtual {v0, p1}, LX/0nW;->a(LX/15w;)LX/15w;

    move-result-object v0

    .line 813034
    invoke-virtual {v0}, LX/15w;->c()LX/15z;

    .line 813035
    iget-object v1, p0, LX/4q8;->a:[LX/4q7;

    aget-object v1, v1, p3

    .line 813036
    iget-object p0, v1, LX/4q7;->a:LX/32s;

    move-object v1, p0

    .line 813037
    invoke-virtual {v1, v0, p2}, LX/32s;->a(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private a(LX/15w;LX/0n3;Ljava/lang/Object;ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 813013
    new-instance v0, LX/0nW;

    invoke-virtual {p1}, LX/15w;->a()LX/0lD;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0nW;-><init>(LX/0lD;)V

    .line 813014
    invoke-virtual {v0}, LX/0nX;->d()V

    .line 813015
    invoke-virtual {v0, p5}, LX/0nX;->b(Ljava/lang/String;)V

    .line 813016
    iget-object v1, p0, LX/4q8;->d:[LX/0nW;

    aget-object v1, v1, p4

    invoke-virtual {v1, p1}, LX/0nW;->a(LX/15w;)LX/15w;

    move-result-object v1

    .line 813017
    invoke-virtual {v1}, LX/15w;->c()LX/15z;

    .line 813018
    invoke-virtual {v0, v1}, LX/0nW;->b(LX/15w;)V

    .line 813019
    invoke-virtual {v0}, LX/0nX;->e()V

    .line 813020
    invoke-virtual {v0, p1}, LX/0nW;->a(LX/15w;)LX/15w;

    move-result-object v0

    .line 813021
    invoke-virtual {v0}, LX/15w;->c()LX/15z;

    .line 813022
    iget-object v1, p0, LX/4q8;->a:[LX/4q7;

    aget-object v1, v1, p4

    .line 813023
    iget-object p0, v1, LX/4q7;->a:LX/32s;

    move-object v1, p0

    .line 813024
    invoke-virtual {v1, v0, p2, p3}, LX/32s;->a(LX/15w;LX/0n3;Ljava/lang/Object;)V

    .line 813025
    return-void
.end method


# virtual methods
.method public final a()LX/4q8;
    .locals 1

    .prologue
    .line 813012
    new-instance v0, LX/4q8;

    invoke-direct {v0, p0}, LX/4q8;-><init>(LX/4q8;)V

    return-object v0
.end method

.method public final a(LX/15w;LX/0n3;LX/4qL;LX/4qF;)Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 812911
    iget-object v0, p0, LX/4q8;->a:[LX/4q7;

    array-length v3, v0

    .line 812912
    new-array v4, v3, [Ljava/lang/Object;

    move v2, v1

    .line 812913
    :goto_0
    if-ge v2, v3, :cond_4

    .line 812914
    iget-object v0, p0, LX/4q8;->c:[Ljava/lang/String;

    aget-object v0, v0, v2

    .line 812915
    if-nez v0, :cond_3

    .line 812916
    iget-object v0, p0, LX/4q8;->d:[LX/0nW;

    aget-object v0, v0, v2

    if-eqz v0, :cond_2

    .line 812917
    iget-object v0, p0, LX/4q8;->a:[LX/4q7;

    aget-object v0, v0, v2

    invoke-virtual {v0}, LX/4q7;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 812918
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Missing external type id property \'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/4q8;->a:[LX/4q7;

    aget-object v1, v1, v2

    .line 812919
    iget-object v2, v1, LX/4q7;->c:Ljava/lang/String;

    move-object v1, v2

    .line 812920
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0n3;->c(Ljava/lang/String;)LX/28E;

    move-result-object v0

    throw v0

    .line 812921
    :cond_0
    iget-object v0, p0, LX/4q8;->a:[LX/4q7;

    aget-object v0, v0, v2

    invoke-virtual {v0}, LX/4q7;->b()Ljava/lang/String;

    move-result-object v0

    .line 812922
    :cond_1
    invoke-direct {p0, p1, p2, v2, v0}, LX/4q8;->a(LX/15w;LX/0n3;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    aput-object v0, v4, v2

    .line 812923
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 812924
    :cond_3
    iget-object v5, p0, LX/4q8;->d:[LX/0nW;

    aget-object v5, v5, v2

    if-nez v5, :cond_1

    .line 812925
    iget-object v0, p0, LX/4q8;->a:[LX/4q7;

    aget-object v0, v0, v2

    .line 812926
    iget-object v1, v0, LX/4q7;->a:LX/32s;

    move-object v0, v1

    .line 812927
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Missing property \'"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 812928
    iget-object v3, v0, LX/32s;->_propName:Ljava/lang/String;

    move-object v0, v3

    .line 812929
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\' for external type id \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/4q8;->a:[LX/4q7;

    aget-object v1, v1, v2

    .line 812930
    iget-object v2, v1, LX/4q7;->c:Ljava/lang/String;

    move-object v1, v2

    .line 812931
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0n3;->c(Ljava/lang/String;)LX/28E;

    move-result-object v0

    throw v0

    :cond_4
    move v0, v1

    .line 812932
    :goto_1
    if-ge v0, v3, :cond_6

    .line 812933
    iget-object v2, p0, LX/4q8;->a:[LX/4q7;

    aget-object v2, v2, v0

    .line 812934
    iget-object v5, v2, LX/4q7;->a:LX/32s;

    move-object v2, v5

    .line 812935
    iget-object v5, v2, LX/32s;->_propName:Ljava/lang/String;

    move-object v5, v5

    .line 812936
    invoke-virtual {p4, v5}, LX/4qF;->a(Ljava/lang/String;)LX/32s;

    move-result-object v5

    if-eqz v5, :cond_5

    .line 812937
    invoke-virtual {v2}, LX/32s;->c()I

    move-result v2

    aget-object v5, v4, v0

    invoke-virtual {p3, v2, v5}, LX/4qL;->a(ILjava/lang/Object;)Z

    .line 812938
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 812939
    :cond_6
    invoke-virtual {p4, p2, p3}, LX/4qF;->a(LX/0n3;LX/4qL;)Ljava/lang/Object;

    move-result-object v2

    move v0, v1

    .line 812940
    :goto_2
    if-ge v0, v3, :cond_8

    .line 812941
    iget-object v1, p0, LX/4q8;->a:[LX/4q7;

    aget-object v1, v1, v0

    .line 812942
    iget-object v5, v1, LX/4q7;->a:LX/32s;

    move-object v1, v5

    .line 812943
    iget-object v5, v1, LX/32s;->_propName:Ljava/lang/String;

    move-object v5, v5

    .line 812944
    invoke-virtual {p4, v5}, LX/4qF;->a(Ljava/lang/String;)LX/32s;

    move-result-object v5

    if-nez v5, :cond_7

    .line 812945
    aget-object v5, v4, v0

    invoke-virtual {v1, v2, v5}, LX/32s;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 812946
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 812947
    :cond_8
    return-object v2
.end method

.method public final a(LX/15w;LX/0n3;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 812982
    const/4 v4, 0x0

    iget-object v0, p0, LX/4q8;->a:[LX/4q7;

    array-length v6, v0

    :goto_0
    if-ge v4, v6, :cond_5

    .line 812983
    iget-object v0, p0, LX/4q8;->c:[Ljava/lang/String;

    aget-object v0, v0, v4

    .line 812984
    if-nez v0, :cond_4

    .line 812985
    iget-object v1, p0, LX/4q8;->d:[LX/0nW;

    aget-object v1, v1, v4

    .line 812986
    if-eqz v1, :cond_0

    .line 812987
    invoke-virtual {v1}, LX/0nW;->j()LX/15z;

    move-result-object v2

    .line 812988
    if-eqz v2, :cond_3

    invoke-virtual {v2}, LX/15z;->isScalarValue()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 812989
    invoke-virtual {v1, p1}, LX/0nW;->a(LX/15w;)LX/15w;

    move-result-object v0

    .line 812990
    invoke-virtual {v0}, LX/15w;->c()LX/15z;

    .line 812991
    iget-object v1, p0, LX/4q8;->a:[LX/4q7;

    aget-object v1, v1, v4

    .line 812992
    iget-object v2, v1, LX/4q7;->a:LX/32s;

    move-object v1, v2

    .line 812993
    invoke-virtual {v1}, LX/32s;->a()LX/0lJ;

    move-result-object v2

    invoke-static {v0, p2, v2}, LX/4qw;->a(LX/15w;LX/0n3;LX/0lJ;)Ljava/lang/Object;

    move-result-object v0

    .line 812994
    if-eqz v0, :cond_1

    .line 812995
    invoke-virtual {v1, p3, v0}, LX/32s;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 812996
    :cond_0
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 812997
    :cond_1
    iget-object v0, p0, LX/4q8;->a:[LX/4q7;

    aget-object v0, v0, v4

    invoke-virtual {v0}, LX/4q7;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 812998
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Missing external type id property \'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/4q8;->a:[LX/4q7;

    aget-object v1, v1, v4

    .line 812999
    iget-object v2, v1, LX/4q7;->c:Ljava/lang/String;

    move-object v1, v2

    .line 813000
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0n3;->c(Ljava/lang/String;)LX/28E;

    move-result-object v0

    throw v0

    .line 813001
    :cond_2
    iget-object v0, p0, LX/4q8;->a:[LX/4q7;

    aget-object v0, v0, v4

    invoke-virtual {v0}, LX/4q7;->b()Ljava/lang/String;

    move-result-object v0

    :cond_3
    move-object v5, v0

    :goto_2
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    .line 813002
    invoke-direct/range {v0 .. v5}, LX/4q8;->a(LX/15w;LX/0n3;Ljava/lang/Object;ILjava/lang/String;)V

    goto :goto_1

    .line 813003
    :cond_4
    iget-object v1, p0, LX/4q8;->d:[LX/0nW;

    aget-object v1, v1, v4

    if-nez v1, :cond_6

    .line 813004
    iget-object v0, p0, LX/4q8;->a:[LX/4q7;

    aget-object v0, v0, v4

    .line 813005
    iget-object v1, v0, LX/4q7;->a:LX/32s;

    move-object v0, v1

    .line 813006
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Missing property \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 813007
    iget-object v2, v0, LX/32s;->_propName:Ljava/lang/String;

    move-object v0, v2

    .line 813008
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\' for external type id \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/4q8;->a:[LX/4q7;

    aget-object v1, v1, v4

    .line 813009
    iget-object v2, v1, LX/4q7;->c:Ljava/lang/String;

    move-object v1, v2

    .line 813010
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0n3;->c(Ljava/lang/String;)LX/28E;

    move-result-object v0

    throw v0

    .line 813011
    :cond_5
    return-object p3

    :cond_6
    move-object v5, v0

    goto :goto_2
.end method

.method public final a(LX/15w;LX/0n3;Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 812968
    iget-object v0, p0, LX/4q8;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 812969
    if-nez v0, :cond_0

    move v0, v1

    .line 812970
    :goto_0
    return v0

    .line 812971
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 812972
    iget-object v0, p0, LX/4q8;->a:[LX/4q7;

    aget-object v0, v0, v4

    .line 812973
    invoke-virtual {v0, p3}, LX/4q7;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 812974
    goto :goto_0

    .line 812975
    :cond_1
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    .line 812976
    if-eqz p4, :cond_2

    iget-object v0, p0, LX/4q8;->d:[LX/0nW;

    aget-object v0, v0, v4

    if-eqz v0, :cond_2

    move v1, v6

    .line 812977
    :cond_2
    if-eqz v1, :cond_3

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    .line 812978
    invoke-direct/range {v0 .. v5}, LX/4q8;->a(LX/15w;LX/0n3;Ljava/lang/Object;ILjava/lang/String;)V

    .line 812979
    iget-object v0, p0, LX/4q8;->d:[LX/0nW;

    const/4 v1, 0x0

    aput-object v1, v0, v4

    :goto_1
    move v0, v6

    .line 812980
    goto :goto_0

    .line 812981
    :cond_3
    iget-object v0, p0, LX/4q8;->c:[Ljava/lang/String;

    aput-object v5, v0, v4

    goto :goto_1
.end method

.method public final b(LX/15w;LX/0n3;Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 812948
    iget-object v0, p0, LX/4q8;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 812949
    if-nez v0, :cond_0

    .line 812950
    :goto_0
    return v1

    .line 812951
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 812952
    iget-object v0, p0, LX/4q8;->a:[LX/4q7;

    aget-object v0, v0, v4

    .line 812953
    invoke-virtual {v0, p3}, LX/4q7;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 812954
    iget-object v0, p0, LX/4q8;->c:[Ljava/lang/String;

    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v4

    .line 812955
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 812956
    if-eqz p4, :cond_2

    iget-object v0, p0, LX/4q8;->d:[LX/0nW;

    aget-object v0, v0, v4

    if-eqz v0, :cond_2

    move v0, v6

    .line 812957
    :goto_1
    if-eqz v0, :cond_1

    .line 812958
    iget-object v0, p0, LX/4q8;->c:[Ljava/lang/String;

    aget-object v5, v0, v4

    .line 812959
    iget-object v0, p0, LX/4q8;->c:[Ljava/lang/String;

    aput-object v7, v0, v4

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    .line 812960
    invoke-direct/range {v0 .. v5}, LX/4q8;->a(LX/15w;LX/0n3;Ljava/lang/Object;ILjava/lang/String;)V

    .line 812961
    iget-object v0, p0, LX/4q8;->d:[LX/0nW;

    aput-object v7, v0, v4

    :cond_1
    move v1, v6

    .line 812962
    goto :goto_0

    :cond_2
    move v0, v1

    .line 812963
    goto :goto_1

    .line 812964
    :cond_3
    new-instance v0, LX/0nW;

    invoke-virtual {p1}, LX/15w;->a()LX/0lD;

    move-result-object v2

    invoke-direct {v0, v2}, LX/0nW;-><init>(LX/0lD;)V

    .line 812965
    invoke-virtual {v0, p1}, LX/0nW;->b(LX/15w;)V

    .line 812966
    iget-object v2, p0, LX/4q8;->d:[LX/0nW;

    aput-object v0, v2, v4

    .line 812967
    if-eqz p4, :cond_4

    iget-object v0, p0, LX/4q8;->c:[Ljava/lang/String;

    aget-object v0, v0, v4

    if-eqz v0, :cond_4

    move v1, v6

    :cond_4
    move v0, v1

    goto :goto_1
.end method
