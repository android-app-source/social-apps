.class public LX/4ML;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 687496
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 40

    .prologue
    .line 687568
    const/16 v36, 0x0

    .line 687569
    const/16 v35, 0x0

    .line 687570
    const/16 v34, 0x0

    .line 687571
    const/16 v33, 0x0

    .line 687572
    const/16 v32, 0x0

    .line 687573
    const/16 v31, 0x0

    .line 687574
    const/16 v30, 0x0

    .line 687575
    const/16 v29, 0x0

    .line 687576
    const/16 v28, 0x0

    .line 687577
    const/16 v27, 0x0

    .line 687578
    const/16 v26, 0x0

    .line 687579
    const/16 v25, 0x0

    .line 687580
    const/16 v24, 0x0

    .line 687581
    const/16 v23, 0x0

    .line 687582
    const/16 v22, 0x0

    .line 687583
    const/16 v21, 0x0

    .line 687584
    const/16 v20, 0x0

    .line 687585
    const/16 v19, 0x0

    .line 687586
    const/16 v18, 0x0

    .line 687587
    const/16 v17, 0x0

    .line 687588
    const/16 v16, 0x0

    .line 687589
    const/4 v15, 0x0

    .line 687590
    const/4 v14, 0x0

    .line 687591
    const/4 v13, 0x0

    .line 687592
    const/4 v12, 0x0

    .line 687593
    const/4 v11, 0x0

    .line 687594
    const/4 v10, 0x0

    .line 687595
    const/4 v9, 0x0

    .line 687596
    const/4 v8, 0x0

    .line 687597
    const/4 v7, 0x0

    .line 687598
    const/4 v6, 0x0

    .line 687599
    const/4 v5, 0x0

    .line 687600
    const/4 v4, 0x0

    .line 687601
    const/4 v3, 0x0

    .line 687602
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v37

    sget-object v38, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v37

    move-object/from16 v1, v38

    if-eq v0, v1, :cond_1

    .line 687603
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 687604
    const/4 v3, 0x0

    .line 687605
    :goto_0
    return v3

    .line 687606
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 687607
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v37

    sget-object v38, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v37

    move-object/from16 v1, v38

    if-eq v0, v1, :cond_12

    .line 687608
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v37

    .line 687609
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 687610
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v38

    sget-object v39, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v38

    move-object/from16 v1, v39

    if-eq v0, v1, :cond_1

    if-eqz v37, :cond_1

    .line 687611
    const-string v38, "can_viewer_create_repeat_event"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_2

    .line 687612
    const/16 v19, 0x1

    .line 687613
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v36

    goto :goto_1

    .line 687614
    :cond_2
    const-string v38, "can_viewer_decline"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_3

    .line 687615
    const/16 v18, 0x1

    .line 687616
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v35

    goto :goto_1

    .line 687617
    :cond_3
    const-string v38, "can_viewer_delete"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_4

    .line 687618
    const/16 v17, 0x1

    .line 687619
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v34

    goto :goto_1

    .line 687620
    :cond_4
    const-string v38, "can_viewer_edit"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_5

    .line 687621
    const/16 v16, 0x1

    .line 687622
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v33

    goto :goto_1

    .line 687623
    :cond_5
    const-string v38, "can_viewer_edit_host"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_6

    .line 687624
    const/4 v15, 0x1

    .line 687625
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v32

    goto :goto_1

    .line 687626
    :cond_6
    const-string v38, "can_viewer_invite"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_7

    .line 687627
    const/4 v14, 0x1

    .line 687628
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v31

    goto :goto_1

    .line 687629
    :cond_7
    const-string v38, "can_viewer_join"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_8

    .line 687630
    const/4 v13, 0x1

    .line 687631
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v30

    goto/16 :goto_1

    .line 687632
    :cond_8
    const-string v38, "can_viewer_maybe"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_9

    .line 687633
    const/4 v12, 0x1

    .line 687634
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v29

    goto/16 :goto_1

    .line 687635
    :cond_9
    const-string v38, "can_viewer_promote_as_parent"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_a

    .line 687636
    const/4 v11, 0x1

    .line 687637
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v28

    goto/16 :goto_1

    .line 687638
    :cond_a
    const-string v38, "can_viewer_remove_self"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_b

    .line 687639
    const/4 v10, 0x1

    .line 687640
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v27

    goto/16 :goto_1

    .line 687641
    :cond_b
    const-string v38, "can_viewer_report"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_c

    .line 687642
    const/4 v9, 0x1

    .line 687643
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v26

    goto/16 :goto_1

    .line 687644
    :cond_c
    const-string v38, "can_viewer_save"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_d

    .line 687645
    const/4 v8, 0x1

    .line 687646
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v25

    goto/16 :goto_1

    .line 687647
    :cond_d
    const-string v38, "can_viewer_send_message_to_guests"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_e

    .line 687648
    const/4 v7, 0x1

    .line 687649
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v24

    goto/16 :goto_1

    .line 687650
    :cond_e
    const-string v38, "can_viewer_share"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_f

    .line 687651
    const/4 v6, 0x1

    .line 687652
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v23

    goto/16 :goto_1

    .line 687653
    :cond_f
    const-string v38, "is_viewer_admin"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_10

    .line 687654
    const/4 v5, 0x1

    .line 687655
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v22

    goto/16 :goto_1

    .line 687656
    :cond_10
    const-string v38, "remaining_invites"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v38

    if-eqz v38, :cond_11

    .line 687657
    const/4 v4, 0x1

    .line 687658
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v21

    goto/16 :goto_1

    .line 687659
    :cond_11
    const-string v38, "seen_event"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v37

    if-eqz v37, :cond_0

    .line 687660
    const/4 v3, 0x1

    .line 687661
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Lcom/facebook/graphql/enums/GraphQLEventSeenState;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    move-result-object v20

    goto/16 :goto_1

    .line 687662
    :cond_12
    const/16 v37, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 687663
    if-eqz v19, :cond_13

    .line 687664
    const/16 v19, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v36

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 687665
    :cond_13
    if-eqz v18, :cond_14

    .line 687666
    const/16 v18, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v35

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 687667
    :cond_14
    if-eqz v17, :cond_15

    .line 687668
    const/16 v17, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v34

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 687669
    :cond_15
    if-eqz v16, :cond_16

    .line 687670
    const/16 v16, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v16

    move/from16 v2, v33

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 687671
    :cond_16
    if-eqz v15, :cond_17

    .line 687672
    const/4 v15, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v15, v1}, LX/186;->a(IZ)V

    .line 687673
    :cond_17
    if-eqz v14, :cond_18

    .line 687674
    const/4 v14, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v14, v1}, LX/186;->a(IZ)V

    .line 687675
    :cond_18
    if-eqz v13, :cond_19

    .line 687676
    const/4 v13, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v13, v1}, LX/186;->a(IZ)V

    .line 687677
    :cond_19
    if-eqz v12, :cond_1a

    .line 687678
    const/4 v12, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v12, v1}, LX/186;->a(IZ)V

    .line 687679
    :cond_1a
    if-eqz v11, :cond_1b

    .line 687680
    const/16 v11, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v11, v1}, LX/186;->a(IZ)V

    .line 687681
    :cond_1b
    if-eqz v10, :cond_1c

    .line 687682
    const/16 v10, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v10, v1}, LX/186;->a(IZ)V

    .line 687683
    :cond_1c
    if-eqz v9, :cond_1d

    .line 687684
    const/16 v9, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v9, v1}, LX/186;->a(IZ)V

    .line 687685
    :cond_1d
    if-eqz v8, :cond_1e

    .line 687686
    const/16 v8, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v8, v1}, LX/186;->a(IZ)V

    .line 687687
    :cond_1e
    if-eqz v7, :cond_1f

    .line 687688
    const/16 v7, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v7, v1}, LX/186;->a(IZ)V

    .line 687689
    :cond_1f
    if-eqz v6, :cond_20

    .line 687690
    const/16 v6, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v6, v1}, LX/186;->a(IZ)V

    .line 687691
    :cond_20
    if-eqz v5, :cond_21

    .line 687692
    const/16 v5, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v5, v1}, LX/186;->a(IZ)V

    .line 687693
    :cond_21
    if-eqz v4, :cond_22

    .line 687694
    const/16 v4, 0xf

    const/4 v5, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v4, v1, v5}, LX/186;->a(III)V

    .line 687695
    :cond_22
    if-eqz v3, :cond_23

    .line 687696
    const/16 v3, 0x10

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v3, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 687697
    :cond_23
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 4

    .prologue
    const/16 v3, 0x10

    const/4 v2, 0x0

    .line 687497
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 687498
    invoke-virtual {p0, p1, v2}, LX/15i;->b(II)Z

    move-result v0

    .line 687499
    if-eqz v0, :cond_0

    .line 687500
    const-string v1, "can_viewer_create_repeat_event"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 687501
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 687502
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 687503
    if-eqz v0, :cond_1

    .line 687504
    const-string v1, "can_viewer_decline"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 687505
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 687506
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 687507
    if-eqz v0, :cond_2

    .line 687508
    const-string v1, "can_viewer_delete"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 687509
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 687510
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 687511
    if-eqz v0, :cond_3

    .line 687512
    const-string v1, "can_viewer_edit"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 687513
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 687514
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 687515
    if-eqz v0, :cond_4

    .line 687516
    const-string v1, "can_viewer_edit_host"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 687517
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 687518
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 687519
    if-eqz v0, :cond_5

    .line 687520
    const-string v1, "can_viewer_invite"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 687521
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 687522
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 687523
    if-eqz v0, :cond_6

    .line 687524
    const-string v1, "can_viewer_join"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 687525
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 687526
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 687527
    if-eqz v0, :cond_7

    .line 687528
    const-string v1, "can_viewer_maybe"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 687529
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 687530
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 687531
    if-eqz v0, :cond_8

    .line 687532
    const-string v1, "can_viewer_promote_as_parent"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 687533
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 687534
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 687535
    if-eqz v0, :cond_9

    .line 687536
    const-string v1, "can_viewer_remove_self"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 687537
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 687538
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 687539
    if-eqz v0, :cond_a

    .line 687540
    const-string v1, "can_viewer_report"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 687541
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 687542
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 687543
    if-eqz v0, :cond_b

    .line 687544
    const-string v1, "can_viewer_save"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 687545
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 687546
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 687547
    if-eqz v0, :cond_c

    .line 687548
    const-string v1, "can_viewer_send_message_to_guests"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 687549
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 687550
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 687551
    if-eqz v0, :cond_d

    .line 687552
    const-string v1, "can_viewer_share"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 687553
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 687554
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 687555
    if-eqz v0, :cond_e

    .line 687556
    const-string v1, "is_viewer_admin"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 687557
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 687558
    :cond_e
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 687559
    if-eqz v0, :cond_f

    .line 687560
    const-string v1, "remaining_invites"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 687561
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 687562
    :cond_f
    invoke-virtual {p0, p1, v3, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 687563
    if-eqz v0, :cond_10

    .line 687564
    const-string v0, "seen_event"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 687565
    const-class v0, Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    invoke-virtual {p0, p1, v3, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLEventSeenState;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 687566
    :cond_10
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 687567
    return-void
.end method
