.class public final LX/4xc;
.super LX/4xb;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/4xb",
        "<TK;",
        "Ljava/util/Collection",
        "<TV;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/4xd;


# direct methods
.method public constructor <init>(LX/4xd;)V
    .locals 0

    .prologue
    .line 821161
    iput-object p1, p0, LX/4xc;->a:LX/4xd;

    .line 821162
    invoke-direct {p0, p1}, LX/4xb;-><init>(Ljava/util/Map;)V

    .line 821163
    return-void
.end method


# virtual methods
.method public final remove(Ljava/lang/Object;)Z
    .locals 6
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 821164
    instance-of v0, p1, Ljava/util/Collection;

    if-eqz v0, :cond_2

    .line 821165
    check-cast p1, Ljava/util/Collection;

    .line 821166
    iget-object v0, p0, LX/4xc;->a:LX/4xd;

    iget-object v0, v0, LX/4xd;->a:LX/4xj;

    iget-object v0, v0, LX/4xj;->a:LX/0Xu;

    invoke-interface {v0}, LX/0Xu;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 821167
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 821168
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 821169
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    .line 821170
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    new-instance v4, LX/4xh;

    iget-object v5, p0, LX/4xc;->a:LX/4xd;

    iget-object v5, v5, LX/4xd;->a:LX/4xj;

    invoke-direct {v4, v5, v3}, LX/4xh;-><init>(LX/4xj;Ljava/lang/Object;)V

    invoke-static {v1, v4}, LX/4xj;->a(Ljava/util/Collection;LX/0Rl;)Ljava/util/Collection;

    move-result-object v1

    .line 821171
    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-interface {p1, v1}, Ljava/util/Collection;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 821172
    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    if-ne v3, v0, :cond_1

    .line 821173
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    .line 821174
    :goto_0
    const/4 v0, 0x1

    .line 821175
    :goto_1
    return v0

    .line 821176
    :cond_1
    invoke-interface {v1}, Ljava/util/Collection;->clear()V

    goto :goto_0

    .line 821177
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final removeAll(Ljava/util/Collection;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 821178
    iget-object v0, p0, LX/4xc;->a:LX/4xd;

    iget-object v0, v0, LX/4xd;->a:LX/4xj;

    invoke-static {p1}, LX/0Rj;->in(Ljava/util/Collection;)LX/0Rl;

    move-result-object v1

    invoke-static {v1}, LX/0PM;->b(LX/0Rl;)LX/0Rl;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4xj;->a(LX/0Rl;)Z

    move-result v0

    return v0
.end method

.method public final retainAll(Ljava/util/Collection;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 821179
    iget-object v0, p0, LX/4xc;->a:LX/4xd;

    iget-object v0, v0, LX/4xd;->a:LX/4xj;

    invoke-static {p1}, LX/0Rj;->in(Ljava/util/Collection;)LX/0Rl;

    move-result-object v1

    invoke-static {v1}, LX/0Rj;->not(LX/0Rl;)LX/0Rl;

    move-result-object v1

    invoke-static {v1}, LX/0PM;->b(LX/0Rl;)LX/0Rl;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4xj;->a(LX/0Rl;)Z

    move-result v0

    return v0
.end method
