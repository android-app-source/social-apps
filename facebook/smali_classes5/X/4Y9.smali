.class public final LX/4Y9;
.super LX/0ur;
.source ""


# instance fields
.field public b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Lcom/facebook/graphql/model/GraphQLPlaceListItemToRecommendingCommentsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 774989
    invoke-direct {p0}, LX/0ur;-><init>()V

    .line 774990
    instance-of v0, p0, LX/4Y9;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 774991
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPlaceListItem;)LX/4Y9;
    .locals 2

    .prologue
    .line 774980
    new-instance v0, LX/4Y9;

    invoke-direct {v0}, LX/4Y9;-><init>()V

    .line 774981
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 774982
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->j()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Y9;->b:Ljava/lang/String;

    .line 774983
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v1

    iput-object v1, v0, LX/4Y9;->c:Lcom/facebook/graphql/model/GraphQLPage;

    .line 774984
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, v0, LX/4Y9;->d:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 774985
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->l()Lcom/facebook/graphql/model/GraphQLPlaceListItemToRecommendingCommentsConnection;

    move-result-object v1

    iput-object v1, v0, LX/4Y9;->e:Lcom/facebook/graphql/model/GraphQLPlaceListItemToRecommendingCommentsConnection;

    .line 774986
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->m()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4Y9;->f:Ljava/lang/String;

    .line 774987
    invoke-static {v0, p0}, LX/0ur;->a(LX/0ur;Lcom/facebook/graphql/modelutil/BaseModel;)V

    .line 774988
    return-object v0
.end method


# virtual methods
.method public final a()Lcom/facebook/graphql/model/GraphQLPlaceListItem;
    .locals 2

    .prologue
    .line 774978
    new-instance v0, Lcom/facebook/graphql/model/GraphQLPlaceListItem;

    invoke-direct {v0, p0}, Lcom/facebook/graphql/model/GraphQLPlaceListItem;-><init>(LX/4Y9;)V

    .line 774979
    return-object v0
.end method
