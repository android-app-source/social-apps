.class public LX/4lo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4lm;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static a:Z

.field private static volatile b:LX/4lo;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 804787
    sput-boolean v0, LX/4lo;->a:Z

    .line 804788
    :try_start_0
    const-string v0, "org.apache.harmony.xnet.provider.jsse.OpenSSLSocketImpl"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    .line 804789
    const-string v0, "org.apache.harmony.xnet.provider.jsse.OpenSSLSocketImplWrapper"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    .line 804790
    const-class v0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSocketImpl;

    const-string v1, "setHostname"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-class v4, Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    .line 804791
    const-class v0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSocketImpl;

    const-string v1, "setUseSessionTickets"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    sget-object v4, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    .line 804792
    const-class v0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSocketImpl;

    const-string v1, "setHandshakeTimeout"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    .line 804793
    const/4 v0, 0x1

    sput-boolean v0, LX/4lo;->a:Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 804794
    :goto_0
    return-void

    :catch_0
    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 804795
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 804796
    return-void
.end method

.method public static a(LX/0QB;)LX/4lo;
    .locals 3

    .prologue
    .line 804797
    sget-object v0, LX/4lo;->b:LX/4lo;

    if-nez v0, :cond_1

    .line 804798
    const-class v1, LX/4lo;

    monitor-enter v1

    .line 804799
    :try_start_0
    sget-object v0, LX/4lo;->b:LX/4lo;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 804800
    if-eqz v2, :cond_0

    .line 804801
    :try_start_1
    new-instance v0, LX/4lo;

    invoke-direct {v0}, LX/4lo;-><init>()V

    .line 804802
    move-object v0, v0

    .line 804803
    sput-object v0, LX/4lo;->b:LX/4lo;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 804804
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 804805
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 804806
    :cond_1
    sget-object v0, LX/4lo;->b:LX/4lo;

    return-object v0

    .line 804807
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 804808
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 804809
    sget-boolean v0, LX/4lo;->a:Z

    return v0
.end method
