.class public LX/3fb;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile c:LX/3fb;


# instance fields
.field private final b:LX/0SG;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 622645
    const-class v0, LX/3fb;

    sput-object v0, LX/3fb;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0SG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 622646
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 622647
    iput-object p1, p0, LX/3fb;->b:LX/0SG;

    .line 622648
    return-void
.end method

.method private static a(J)J
    .locals 2

    .prologue
    .line 622649
    const-wide/16 v0, 0x3e8

    mul-long/2addr v0, p0

    return-wide v0
.end method

.method private static a(Lcom/facebook/graphql/enums/GraphQLObjectType;)LX/2RU;
    .locals 4

    .prologue
    .line 622650
    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v0

    .line 622651
    const-string v1, "User"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 622652
    sget-object v0, LX/2RU;->USER:LX/2RU;

    .line 622653
    :goto_0
    return-object v0

    .line 622654
    :cond_0
    const-string v1, "Page"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 622655
    sget-object v0, LX/2RU;->PAGE:LX/2RU;

    goto :goto_0

    .line 622656
    :cond_1
    sget-object v1, LX/3fb;->a:Ljava/lang/Class;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Malformed contact type name: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 622657
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Malformed contact type name: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static a(LX/0QB;)LX/3fb;
    .locals 4

    .prologue
    .line 622658
    sget-object v0, LX/3fb;->c:LX/3fb;

    if-nez v0, :cond_1

    .line 622659
    const-class v1, LX/3fb;

    monitor-enter v1

    .line 622660
    :try_start_0
    sget-object v0, LX/3fb;->c:LX/3fb;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 622661
    if-eqz v2, :cond_0

    .line 622662
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 622663
    new-instance p0, LX/3fb;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    invoke-direct {p0, v3}, LX/3fb;-><init>(LX/0SG;)V

    .line 622664
    move-object v0, p0

    .line 622665
    sput-object v0, LX/3fb;->c:LX/3fb;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 622666
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 622667
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 622668
    :cond_1
    sget-object v0, LX/3fb;->c:LX/3fb;

    return-object v0

    .line 622669
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 622670
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactNameModel;)Lcom/facebook/user/model/Name;
    .locals 12
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "deserializeStructuredName"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    const/4 v11, 0x0

    const/4 v3, 0x0

    .line 622671
    if-nez p0, :cond_0

    .line 622672
    new-instance v0, Lcom/facebook/user/model/Name;

    invoke-direct {v0, v3, v3}, Lcom/facebook/user/model/Name;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 622673
    :goto_0
    return-object v0

    .line 622674
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactNameModel;->a()Ljava/lang/String;

    move-result-object v5

    .line 622675
    invoke-virtual {p0}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactNameModel;->b()LX/2uF;

    move-result-object v0

    .line 622676
    if-eqz v5, :cond_1

    if-nez v0, :cond_2

    .line 622677
    :cond_1
    new-instance v0, Lcom/facebook/user/model/Name;

    invoke-direct {v0, v3, v3, v5}, Lcom/facebook/user/model/Name;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 622678
    :cond_2
    invoke-virtual {v0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v6

    move-object v2, v3

    move-object v4, v3

    :goto_1
    invoke-interface {v6}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v6}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 622679
    const/4 v7, 0x1

    invoke-virtual {v1, v0, v7}, LX/15i;->j(II)I

    move-result v7

    .line 622680
    invoke-virtual {v1, v0, v11}, LX/15i;->j(II)I

    move-result v8

    .line 622681
    const/4 v9, 0x2

    const-class v10, Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;

    invoke-virtual {v1, v0, v9, v10, v3}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;

    .line 622682
    invoke-virtual {v5, v11, v7}, Ljava/lang/String;->offsetByCodePoints(II)I

    move-result v1

    .line 622683
    invoke-virtual {v5, v1, v8}, Ljava/lang/String;->offsetByCodePoints(II)I

    move-result v7

    .line 622684
    invoke-virtual {v5, v1, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 622685
    sget-object v7, Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;->FIRST:Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;

    invoke-static {v0, v7}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    move-object v4, v1

    .line 622686
    goto :goto_1

    .line 622687
    :cond_3
    sget-object v7, Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;->LAST:Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;

    invoke-static {v0, v7}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    move-object v0, v1

    :goto_2
    move-object v2, v0

    .line 622688
    goto :goto_1

    .line 622689
    :cond_4
    new-instance v0, Lcom/facebook/user/model/Name;

    invoke-direct {v0, v4, v2, v5}, Lcom/facebook/user/model/Name;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    move-object v0, v2

    goto :goto_2
.end method


# virtual methods
.method public final a(Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;)LX/3hB;
    .locals 18
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "deserializeFromContactModel"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 622690
    if-nez p1, :cond_0

    .line 622691
    new-instance v2, Ljava/io/IOException;

    const-string v3, "Server sent a null response for a contact"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 622692
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->p()Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactNameModel;

    move-result-object v2

    invoke-static {v2}, LX/3fb;->a(Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactNameModel;)Lcom/facebook/user/model/Name;

    move-result-object v2

    .line 622693
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->m()Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactNameModel;

    move-result-object v3

    invoke-static {v3}, LX/3fb;->a(Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactNameModel;)Lcom/facebook/user/model/Name;

    move-result-object v3

    .line 622694
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->o()LX/1vs;

    move-result-object v4

    iget-object v5, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    .line 622695
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->dz_()LX/1vs;

    move-result-object v6

    iget-object v7, v6, LX/1vs;->a:LX/15i;

    iget v6, v6, LX/1vs;->b:I

    .line 622696
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->j()LX/1vs;

    move-result-object v8

    iget-object v9, v8, LX/1vs;->a:LX/15i;

    iget v8, v8, LX/1vs;->b:I

    .line 622697
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->o()LX/1vs;

    move-result-object v10

    iget-object v11, v10, LX/1vs;->a:LX/15i;

    iget v10, v10, LX/1vs;->b:I

    .line 622698
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->dz_()LX/1vs;

    move-result-object v12

    iget-object v13, v12, LX/1vs;->a:LX/15i;

    iget v12, v12, LX/1vs;->b:I

    .line 622699
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->j()LX/1vs;

    move-result-object v14

    iget-object v15, v14, LX/1vs;->a:LX/15i;

    iget v14, v14, LX/1vs;->b:I

    .line 622700
    invoke-static {}, Lcom/facebook/contacts/graphql/Contact;->newBuilder()LX/3hB;

    move-result-object v16

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->d()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, LX/3hB;->a(Ljava/lang/String;)LX/3hB;

    move-result-object v16

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->c()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, LX/3hB;->c(Ljava/lang/String;)LX/3hB;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, LX/3hB;->a(Lcom/facebook/user/model/Name;)LX/3hB;

    move-result-object v2

    invoke-virtual {v2, v3}, LX/3hB;->b(Lcom/facebook/user/model/Name;)LX/3hB;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->e()Z

    move-result v3

    invoke-virtual {v2, v3}, LX/3hB;->e(Z)LX/3hB;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->dA_()Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/3hB;->a(Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;)LX/3hB;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->b()J

    move-result-wide v16

    invoke-static/range {v16 .. v17}, LX/3fb;->a(J)J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-virtual {v2, v0, v1}, LX/3hB;->b(J)LX/3hB;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v5, v4, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/3hB;->d(Ljava/lang/String;)LX/3hB;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v7, v6, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/3hB;->e(Ljava/lang/String;)LX/3hB;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v9, v8, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/3hB;->f(Ljava/lang/String;)LX/3hB;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v11, v10, v3}, LX/15i;->j(II)I

    move-result v3

    invoke-virtual {v2, v3}, LX/3hB;->a(I)LX/3hB;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v13, v12, v3}, LX/15i;->j(II)I

    move-result v3

    invoke-virtual {v2, v3}, LX/3hB;->b(I)LX/3hB;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v15, v14, v3}, LX/15i;->j(II)I

    move-result v3

    invoke-virtual {v2, v3}, LX/3hB;->c(I)LX/3hB;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->l()LX/0Px;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/3hB;->b(LX/0Px;)LX/3hB;

    move-result-object v8

    .line 622701
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->n()Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$RepresentedProfileModel;

    move-result-object v4

    .line 622702
    if-eqz v4, :cond_6

    .line 622703
    invoke-virtual {v4}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$RepresentedProfileModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v2}, LX/3hB;->b(Ljava/lang/String;)LX/3hB;

    move-result-object v2

    invoke-virtual {v4}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$RepresentedProfileModel;->dB_()D

    move-result-wide v6

    double-to-float v3, v6

    invoke-virtual {v2, v3}, LX/3hB;->a(F)LX/3hB;

    move-result-object v2

    invoke-virtual {v4}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$RepresentedProfileModel;->v()D

    move-result-wide v6

    double-to-float v3, v6

    invoke-virtual {v2, v3}, LX/3hB;->b(F)LX/3hB;

    move-result-object v2

    invoke-virtual {v4}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$RepresentedProfileModel;->m()Z

    move-result v3

    invoke-virtual {v2, v3}, LX/3hB;->a(Z)LX/3hB;

    move-result-object v2

    invoke-virtual {v4}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$RepresentedProfileModel;->e()Z

    move-result v3

    invoke-virtual {v2, v3}, LX/3hB;->b(Z)LX/3hB;

    move-result-object v2

    invoke-virtual {v4}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$RepresentedProfileModel;->u()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/3hB;->a(Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)LX/3hB;

    move-result-object v2

    invoke-virtual {v4}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$RepresentedProfileModel;->j()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/3hB;->a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)LX/3hB;

    move-result-object v5

    invoke-virtual {v4}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$RepresentedProfileModel;->s()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-virtual {v4}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$RepresentedProfileModel;->s()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    :goto_0
    invoke-virtual {v5, v2, v3}, LX/3hB;->d(J)LX/3hB;

    move-result-object v2

    invoke-virtual {v4}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$RepresentedProfileModel;->d()Z

    move-result v3

    invoke-virtual {v2, v3}, LX/3hB;->g(Z)LX/3hB;

    .line 622704
    invoke-virtual {v4}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$RepresentedProfileModel;->o()Z

    move-result v2

    .line 622705
    if-eqz v2, :cond_5

    sget-object v2, LX/03R;->YES:LX/03R;

    :goto_1
    invoke-virtual {v8, v2}, LX/3hB;->a(LX/03R;)LX/3hB;

    .line 622706
    invoke-virtual {v4}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$RepresentedProfileModel;->n()Z

    move-result v2

    .line 622707
    invoke-virtual {v8, v2}, LX/3hB;->c(Z)LX/3hB;

    .line 622708
    invoke-virtual {v4}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$RepresentedProfileModel;->q()J

    move-result-wide v2

    invoke-static {v2, v3}, LX/3fb;->a(J)J

    move-result-wide v2

    .line 622709
    invoke-virtual {v8, v2, v3}, LX/3hB;->a(J)LX/3hB;

    .line 622710
    move-object/from16 v0, p0

    iget-object v2, v0, LX/3fb;->b:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v8, v2, v3}, LX/3hB;->c(J)LX/3hB;

    .line 622711
    invoke-virtual {v4}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$RepresentedProfileModel;->l()Z

    move-result v2

    invoke-virtual {v8, v2}, LX/3hB;->d(Z)LX/3hB;

    .line 622712
    invoke-virtual {v4}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$RepresentedProfileModel;->c()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-static {v2}, LX/3fb;->a(Lcom/facebook/graphql/enums/GraphQLObjectType;)LX/2RU;

    move-result-object v2

    .line 622713
    invoke-virtual {v8, v2}, LX/3hB;->a(LX/2RU;)LX/3hB;

    .line 622714
    invoke-virtual {v4}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$RepresentedProfileModel;->t()LX/0Px;

    move-result-object v2

    .line 622715
    if-eqz v2, :cond_1

    .line 622716
    invoke-virtual {v8, v2}, LX/3hB;->c(LX/0Px;)LX/3hB;

    .line 622717
    :cond_1
    invoke-virtual {v4}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$RepresentedProfileModel;->b()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    if-eqz v2, :cond_2

    .line 622718
    invoke-virtual {v4}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$RepresentedProfileModel;->b()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 622719
    invoke-virtual {v4}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$RepresentedProfileModel;->b()LX/1vs;

    move-result-object v5

    iget-object v6, v5, LX/1vs;->a:LX/15i;

    iget v5, v5, LX/1vs;->b:I

    .line 622720
    const/4 v7, 0x1

    invoke-virtual {v3, v2, v7}, LX/15i;->j(II)I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v6, v5, v3}, LX/15i;->j(II)I

    move-result v3

    invoke-virtual {v8, v2, v3}, LX/3hB;->a(II)LX/3hB;

    .line 622721
    :cond_2
    invoke-virtual {v4}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$RepresentedProfileModel;->dC_()Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$RepresentedProfileModel$CurrentCityModel;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v4}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$RepresentedProfileModel;->dC_()Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$RepresentedProfileModel$CurrentCityModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$RepresentedProfileModel$CurrentCityModel;->a()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 622722
    invoke-virtual {v4}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$RepresentedProfileModel;->dC_()Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$RepresentedProfileModel$CurrentCityModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$RepresentedProfileModel$CurrentCityModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v2}, LX/3hB;->g(Ljava/lang/String;)LX/3hB;

    .line 622723
    :cond_3
    invoke-virtual {v4}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$RepresentedProfileModel;->p()Z

    move-result v2

    .line 622724
    invoke-virtual {v8, v2}, LX/3hB;->f(Z)LX/3hB;

    .line 622725
    invoke-virtual {v4}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$RepresentedProfileModel;->r()D

    move-result-wide v2

    double-to-float v2, v2

    invoke-virtual {v8, v2}, LX/3hB;->d(F)LX/3hB;

    .line 622726
    :goto_2
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v9

    .line 622727
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel;->k()LX/2uF;

    move-result-object v2

    .line 622728
    if-eqz v2, :cond_8

    invoke-virtual {v2}, LX/39O;->a()Z

    move-result v3

    if-nez v3, :cond_8

    .line 622729
    invoke-virtual {v2}, LX/3Sa;->e()LX/3Sh;

    move-result-object v10

    :goto_3
    invoke-interface {v10}, LX/2sN;->a()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v10}, LX/2sN;->b()LX/1vs;

    move-result-object v2

    iget-object v7, v2, LX/1vs;->a:LX/15i;

    iget v11, v2, LX/1vs;->b:I

    .line 622730
    const/4 v2, 0x1

    const-class v3, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$ImportedPhoneEntriesModel$PrimaryFieldModel;

    invoke-virtual {v7, v11, v2, v3}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v2

    move-object v4, v2

    check-cast v4, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$ImportedPhoneEntriesModel$PrimaryFieldModel;

    .line 622731
    invoke-virtual {v4}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$ImportedPhoneEntriesModel$PrimaryFieldModel;->d()LX/1vs;

    move-result-object v2

    iget-object v6, v2, LX/1vs;->a:LX/15i;

    iget v12, v2, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 622732
    new-instance v2, Lcom/facebook/contacts/graphql/ContactPhone;

    invoke-virtual {v4}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$ImportedPhoneEntriesModel$PrimaryFieldModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Lcom/facebook/contacts/graphql/ContactGraphQLModels$ContactModel$ImportedPhoneEntriesModel$PrimaryFieldModel;->c()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v6, v12, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    const/4 v13, 0x1

    invoke-virtual {v6, v12, v13}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v6

    const/4 v12, 0x0

    invoke-virtual {v7, v11, v12}, LX/15i;->h(II)Z

    move-result v7

    invoke-direct/range {v2 .. v7}, Lcom/facebook/contacts/graphql/ContactPhone;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 622733
    invoke-virtual {v9, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_3

    .line 622734
    :cond_4
    const-wide/16 v2, 0x0

    goto/16 :goto_0

    .line 622735
    :cond_5
    sget-object v2, LX/03R;->NO:LX/03R;

    goto/16 :goto_1

    .line 622736
    :cond_6
    sget-object v2, LX/2RU;->UNMATCHED:LX/2RU;

    invoke-virtual {v8, v2}, LX/3hB;->a(LX/2RU;)LX/3hB;

    goto :goto_2

    .line 622737
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 622738
    :cond_7
    invoke-virtual {v9}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-virtual {v8, v2}, LX/3hB;->a(LX/0Px;)LX/3hB;

    .line 622739
    :cond_8
    return-object v8
.end method
