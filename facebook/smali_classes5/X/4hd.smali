.class public abstract LX/4hd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4hc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<EXECUTOR::",
        "Lcom/facebook/platform/common/action/PlatformActionExecutor;",
        "REQUEST:",
        "LX/4hh;",
        ">",
        "Ljava/lang/Object;",
        "LX/4hc;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TREQUEST;>;"
        }
    .end annotation
.end field

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/Class;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<TREQUEST;>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 801623
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 801624
    iput-object p1, p0, LX/4hd;->a:Ljava/lang/Class;

    .line 801625
    iput-object p2, p0, LX/4hd;->b:Ljava/lang/String;

    .line 801626
    return-void
.end method


# virtual methods
.method public final a(Landroid/app/Activity;LX/4hh;)LX/4hY;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "LX/4hh;",
            ")TEXECUTOR;"
        }
    .end annotation

    .prologue
    .line 801627
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 801628
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 801629
    iget-object v0, p0, LX/4hd;->a:Ljava/lang/Class;

    invoke-virtual {v0, p2}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 801630
    invoke-virtual {p0, p1, p2}, LX/4hd;->b(Landroid/app/Activity;LX/4hh;)LX/4hY;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 801631
    iget-object v0, p0, LX/4hd;->b:Ljava/lang/String;

    return-object v0
.end method

.method public abstract b(Landroid/app/Activity;LX/4hh;)LX/4hY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "TREQUEST;)TEXECUTOR;"
        }
    .end annotation
.end method

.method public b()LX/4hh;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TREQUEST;"
        }
    .end annotation

    .prologue
    .line 801622
    iget-object v0, p0, LX/4hd;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4hh;

    return-object v0
.end method
