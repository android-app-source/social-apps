.class public final LX/3p1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3ow;


# instance fields
.field public final a:Landroid/animation/Animator;


# direct methods
.method public constructor <init>(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 641077
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 641078
    iput-object p1, p0, LX/3p1;->a:Landroid/animation/Animator;

    .line 641079
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 641080
    iget-object v0, p0, LX/3p1;->a:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    .line 641081
    return-void
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 641082
    iget-object v0, p0, LX/3p1;->a:Landroid/animation/Animator;

    invoke-virtual {v0, p1, p2}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 641083
    return-void
.end method

.method public final a(LX/3ot;)V
    .locals 2

    .prologue
    .line 641084
    iget-object v0, p0, LX/3p1;->a:Landroid/animation/Animator;

    new-instance v1, LX/3oz;

    invoke-direct {v1, p1, p0}, LX/3oz;-><init>(LX/3ot;LX/3ow;)V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 641085
    return-void
.end method

.method public final a(LX/3ov;)V
    .locals 2

    .prologue
    .line 641086
    iget-object v0, p0, LX/3p1;->a:Landroid/animation/Animator;

    instance-of v0, v0, Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    .line 641087
    iget-object v0, p0, LX/3p1;->a:Landroid/animation/Animator;

    check-cast v0, Landroid/animation/ValueAnimator;

    new-instance v1, LX/3p0;

    invoke-direct {v1, p0, p1}, LX/3p0;-><init>(LX/3p1;LX/3ov;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 641088
    :cond_0
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 641089
    iget-object v0, p0, LX/3p1;->a:Landroid/animation/Animator;

    invoke-virtual {v0, p1}, Landroid/animation/Animator;->setTarget(Ljava/lang/Object;)V

    .line 641090
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 641091
    iget-object v0, p0, LX/3p1;->a:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    .line 641092
    return-void
.end method

.method public final c()F
    .locals 1

    .prologue
    .line 641093
    iget-object v0, p0, LX/3p1;->a:Landroid/animation/Animator;

    check-cast v0, Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->getAnimatedFraction()F

    move-result v0

    return v0
.end method
