.class public LX/3zc;
.super LX/3zb;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/3zc;


# instance fields
.field private final a:LX/0jk;

.field private final b:LX/3zd;


# direct methods
.method public constructor <init>(LX/0jk;LX/3zd;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 662543
    invoke-direct {p0}, LX/3zb;-><init>()V

    .line 662544
    iput-object p2, p0, LX/3zc;->b:LX/3zd;

    .line 662545
    iput-object p1, p0, LX/3zc;->a:LX/0jk;

    .line 662546
    return-void
.end method

.method public static a(LX/0QB;)LX/3zc;
    .locals 5

    .prologue
    .line 662530
    sget-object v0, LX/3zc;->c:LX/3zc;

    if-nez v0, :cond_1

    .line 662531
    const-class v1, LX/3zc;

    monitor-enter v1

    .line 662532
    :try_start_0
    sget-object v0, LX/3zc;->c:LX/3zc;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 662533
    if-eqz v2, :cond_0

    .line 662534
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 662535
    new-instance p0, LX/3zc;

    invoke-static {v0}, LX/0jk;->a(LX/0QB;)LX/0jk;

    move-result-object v3

    check-cast v3, LX/0jk;

    invoke-static {v0}, LX/3zd;->a(LX/0QB;)LX/3zd;

    move-result-object v4

    check-cast v4, LX/3zd;

    invoke-direct {p0, v3, v4}, LX/3zc;-><init>(LX/0jk;LX/3zd;)V

    .line 662536
    move-object v0, p0

    .line 662537
    sput-object v0, LX/3zc;->c:LX/3zc;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 662538
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 662539
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 662540
    :cond_1
    sget-object v0, LX/3zc;->c:LX/3zc;

    return-object v0

    .line 662541
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 662542
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/view/MotionEvent;)V
    .locals 5

    .prologue
    .line 662503
    iget-object v0, p0, LX/3zc;->a:LX/0jk;

    .line 662504
    iget-object v1, v0, LX/0jk;->b:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v3

    .line 662505
    iget-object v1, v0, LX/0jk;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jl;

    .line 662506
    invoke-interface {v1, v3, v4}, LX/0jl;->a(J)V

    goto :goto_0

    .line 662507
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/ui/dialogs/FbDialogFragment;)V
    .locals 4

    .prologue
    .line 662519
    const-string v0, "dialog"

    .line 662520
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v1

    .line 662521
    const-string v2, "dest_module_class"

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 662522
    instance-of v2, p1, LX/0fh;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 662523
    check-cast v0, LX/0fh;

    invoke-interface {v0}, LX/0f2;->a()Ljava/lang/String;

    move-result-object v0

    .line 662524
    instance-of v2, p1, LX/0hF;

    if-eqz v2, :cond_0

    .line 662525
    check-cast p1, LX/0hF;

    invoke-interface {p1}, LX/0f1;->b()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 662526
    :cond_0
    iget-object v2, p0, LX/3zc;->b:LX/3zd;

    const/4 v3, 0x1

    .line 662527
    iget-object p0, v2, LX/3zd;->a:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/util/Set;

    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p0

    if-eqz p0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/0gh;

    .line 662528
    invoke-virtual {p0, v0, v3, v1}, LX/0gh;->a(Ljava/lang/String;ZLjava/util/Map;)V

    goto :goto_0

    .line 662529
    :cond_1
    return-void
.end method

.method public final b(Lcom/facebook/ui/dialogs/FbDialogFragment;)V
    .locals 4

    .prologue
    .line 662508
    const-string v0, "dialog"

    .line 662509
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v1

    .line 662510
    const-string v2, "source_module_class"

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 662511
    instance-of v2, p1, LX/0fh;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 662512
    check-cast v0, LX/0fh;

    invoke-interface {v0}, LX/0f2;->a()Ljava/lang/String;

    move-result-object v0

    .line 662513
    instance-of v2, p1, LX/0hF;

    if-eqz v2, :cond_0

    .line 662514
    check-cast p1, LX/0hF;

    invoke-interface {p1}, LX/0f1;->b()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 662515
    :cond_0
    iget-object v2, p0, LX/3zc;->b:LX/3zd;

    .line 662516
    iget-object v3, v2, LX/3zd;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0gh;

    .line 662517
    invoke-virtual {v3, v0, v1}, LX/0gh;->a(Ljava/lang/String;Ljava/util/Map;)V

    goto :goto_0

    .line 662518
    :cond_1
    return-void
.end method
