.class public LX/4LK;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 682759
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 12

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 682725
    const/4 v0, 0x0

    .line 682726
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v4, :cond_a

    .line 682727
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 682728
    :goto_0
    return v1

    .line 682729
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_7

    .line 682730
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 682731
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 682732
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_0

    if-eqz v9, :cond_0

    .line 682733
    const-string v10, "block_type"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 682734
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    move-result-object v3

    move-object v8, v3

    move v3, v2

    goto :goto_1

    .line 682735
    :cond_1
    const-string v10, "depth"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 682736
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v7, v0

    move v0, v2

    goto :goto_1

    .line 682737
    :cond_2
    const-string v10, "entity_ranges"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 682738
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 682739
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->START_ARRAY:LX/15z;

    if-ne v9, v10, :cond_3

    .line 682740
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_ARRAY:LX/15z;

    if-eq v9, v10, :cond_3

    .line 682741
    invoke-static {p0, p1}, LX/4LM;->b(LX/15w;LX/186;)I

    move-result v9

    .line 682742
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 682743
    :cond_3
    invoke-static {v6, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v6

    move v6, v6

    .line 682744
    goto :goto_1

    .line 682745
    :cond_4
    const-string v10, "inline_style_ranges"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 682746
    invoke-static {p0, p1}, LX/4Od;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 682747
    :cond_5
    const-string v10, "text"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 682748
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto/16 :goto_1

    .line 682749
    :cond_6
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 682750
    :cond_7
    const/4 v9, 0x5

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 682751
    if-eqz v3, :cond_8

    .line 682752
    invoke-virtual {p1, v1, v8}, LX/186;->a(ILjava/lang/Enum;)V

    .line 682753
    :cond_8
    if-eqz v0, :cond_9

    .line 682754
    invoke-virtual {p1, v2, v7, v1}, LX/186;->a(III)V

    .line 682755
    :cond_9
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 682756
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 682757
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 682758
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_a
    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move-object v8, v0

    move v0, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 682686
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 682687
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 682688
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2, p3}, LX/4LK;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 682689
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 682690
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 682691
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 682719
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 682720
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 682721
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 682722
    invoke-static {p0, p1}, LX/4LK;->a(LX/15w;LX/186;)I

    move-result v1

    .line 682723
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 682724
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 682692
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 682693
    invoke-virtual {p0, p1, v1, v1}, LX/15i;->a(IIS)S

    move-result v0

    .line 682694
    if-eqz v0, :cond_0

    .line 682695
    const-string v0, "block_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 682696
    const-class v0, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 682697
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->a(III)I

    move-result v0

    .line 682698
    if-eqz v0, :cond_1

    .line 682699
    const-string v1, "depth"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 682700
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 682701
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 682702
    if-eqz v0, :cond_3

    .line 682703
    const-string v1, "entity_ranges"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 682704
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 682705
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 682706
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/4LM;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 682707
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 682708
    :cond_2
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 682709
    :cond_3
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 682710
    if-eqz v0, :cond_4

    .line 682711
    const-string v1, "inline_style_ranges"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 682712
    invoke-static {p0, v0, p2, p3}, LX/4Od;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 682713
    :cond_4
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 682714
    if-eqz v0, :cond_5

    .line 682715
    const-string v1, "text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 682716
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 682717
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 682718
    return-void
.end method
