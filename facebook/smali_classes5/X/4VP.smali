.class public LX/4VP;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/4VP;


# instance fields
.field private final a:LX/0V8;

.field private b:LX/03R;


# direct methods
.method public constructor <init>(LX/0V8;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 742410
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 742411
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/4VP;->b:LX/03R;

    .line 742412
    iput-object p1, p0, LX/4VP;->a:LX/0V8;

    .line 742413
    return-void
.end method

.method public static a(LX/0QB;)LX/4VP;
    .locals 4

    .prologue
    .line 742420
    sget-object v0, LX/4VP;->c:LX/4VP;

    if-nez v0, :cond_1

    .line 742421
    const-class v1, LX/4VP;

    monitor-enter v1

    .line 742422
    :try_start_0
    sget-object v0, LX/4VP;->c:LX/4VP;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 742423
    if-eqz v2, :cond_0

    .line 742424
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 742425
    new-instance p0, LX/4VP;

    invoke-static {v0}, LX/0V7;->a(LX/0QB;)LX/0V8;

    move-result-object v3

    check-cast v3, LX/0V8;

    invoke-direct {p0, v3}, LX/4VP;-><init>(LX/0V8;)V

    .line 742426
    move-object v0, p0

    .line 742427
    sput-object v0, LX/4VP;->c:LX/4VP;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 742428
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 742429
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 742430
    :cond_1
    sget-object v0, LX/4VP;->c:LX/4VP;

    return-object v0

    .line 742431
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 742432
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(JJ)J
    .locals 5

    .prologue
    .line 742414
    iget-object v0, p0, LX/4VP;->b:LX/03R;

    sget-object v1, LX/03R;->UNSET:LX/03R;

    if-ne v0, v1, :cond_0

    .line 742415
    iget-object v0, p0, LX/4VP;->a:LX/0V8;

    sget-object v1, LX/0VA;->INTERNAL:LX/0VA;

    invoke-virtual {v0, v1}, LX/0V8;->c(LX/0VA;)J

    move-result-wide v0

    .line 742416
    const-wide/32 v2, 0xc800000

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v0

    iput-object v0, p0, LX/4VP;->b:LX/03R;

    .line 742417
    :cond_0
    iget-object v0, p0, LX/4VP;->b:LX/03R;

    invoke-virtual {v0}, LX/03R;->asBoolean()Z

    move-result v0

    if-eqz v0, :cond_2

    :goto_1
    return-wide p1

    .line 742418
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    move-wide p1, p3

    .line 742419
    goto :goto_1
.end method
