.class public LX/4B2;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public mDataSource:LX/4B1;

.field public mFellbackToCachedDataAfterFailedToHitServer:LX/03R;

.field public mFromAuthoritativeData:LX/03R;

.field public mIsIncompleteData:LX/03R;

.field public mIsStaleData:LX/03R;

.field public mNeedsInitialFetch:LX/03R;

.field public mWasFetchSynchronous:LX/03R;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 677394
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 677395
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/4B2;->mFromAuthoritativeData:LX/03R;

    .line 677396
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/4B2;->mIsStaleData:LX/03R;

    .line 677397
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/4B2;->mIsIncompleteData:LX/03R;

    .line 677398
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/4B2;->mFellbackToCachedDataAfterFailedToHitServer:LX/03R;

    .line 677399
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/4B2;->mNeedsInitialFetch:LX/03R;

    .line 677400
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/4B2;->mWasFetchSynchronous:LX/03R;

    .line 677401
    return-void
.end method


# virtual methods
.method public build()Lcom/facebook/fbservice/results/DataFetchDisposition;
    .locals 1

    .prologue
    .line 677402
    new-instance v0, Lcom/facebook/fbservice/results/DataFetchDisposition;

    invoke-direct {v0, p0}, Lcom/facebook/fbservice/results/DataFetchDisposition;-><init>(LX/4B2;)V

    return-object v0
.end method

.method public setFrom(Lcom/facebook/fbservice/results/DataFetchDisposition;)LX/4B2;
    .locals 1

    .prologue
    .line 677403
    iget-object v0, p1, Lcom/facebook/fbservice/results/DataFetchDisposition;->dataSource:LX/4B1;

    iput-object v0, p0, LX/4B2;->mDataSource:LX/4B1;

    .line 677404
    iget-object v0, p1, Lcom/facebook/fbservice/results/DataFetchDisposition;->fromAuthoritativeData:LX/03R;

    iput-object v0, p0, LX/4B2;->mFromAuthoritativeData:LX/03R;

    .line 677405
    iget-object v0, p1, Lcom/facebook/fbservice/results/DataFetchDisposition;->isStaleData:LX/03R;

    iput-object v0, p0, LX/4B2;->mIsStaleData:LX/03R;

    .line 677406
    iget-object v0, p1, Lcom/facebook/fbservice/results/DataFetchDisposition;->isIncompleteData:LX/03R;

    iput-object v0, p0, LX/4B2;->mIsIncompleteData:LX/03R;

    .line 677407
    iget-object v0, p1, Lcom/facebook/fbservice/results/DataFetchDisposition;->fellbackToCachedDataAfterFailedToHitServer:LX/03R;

    iput-object v0, p0, LX/4B2;->mFellbackToCachedDataAfterFailedToHitServer:LX/03R;

    .line 677408
    iget-object v0, p1, Lcom/facebook/fbservice/results/DataFetchDisposition;->needsInitialFetch:LX/03R;

    iput-object v0, p0, LX/4B2;->mNeedsInitialFetch:LX/03R;

    .line 677409
    iget-object v0, p1, Lcom/facebook/fbservice/results/DataFetchDisposition;->wasFetchSynchronous:LX/03R;

    iput-object v0, p0, LX/4B2;->mWasFetchSynchronous:LX/03R;

    .line 677410
    return-object p0
.end method
