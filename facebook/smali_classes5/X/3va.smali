.class public LX/3va;
.super Landroid/database/DataSetObservable;
.source ""


# static fields
.field public static final a:Ljava/lang/String;

.field private static final b:Ljava/lang/Object;

.field private static final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/3va;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final d:Ljava/lang/Object;

.field public final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/3vV;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/3vX;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Landroid/content/Context;

.field public final h:Ljava/lang/String;

.field public i:Landroid/content/Intent;

.field private j:LX/3vW;

.field private k:I

.field public l:Z

.field public m:Z

.field public n:Z

.field public o:Z

.field private p:LX/3vY;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 652535
    const-class v0, LX/3va;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/3va;->a:Ljava/lang/String;

    .line 652536
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/3va;->b:Ljava/lang/Object;

    .line 652537
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, LX/3va;->c:Ljava/util/Map;

    return-void
.end method

.method private static a(LX/3va;LX/3vX;)Z
    .locals 2

    .prologue
    .line 652527
    iget-object v0, p0, LX/3va;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-result v0

    .line 652528
    if-eqz v0, :cond_0

    .line 652529
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/3va;->n:Z

    .line 652530
    invoke-direct {p0}, LX/3va;->i()V

    .line 652531
    invoke-direct {p0}, LX/3va;->d()V

    .line 652532
    invoke-direct {p0}, LX/3va;->f()Z

    .line 652533
    invoke-virtual {p0}, LX/3va;->notifyChanged()V

    .line 652534
    :cond_0
    return v0
.end method

.method private d()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 652520
    iget-boolean v0, p0, LX/3va;->m:Z

    if-nez v0, :cond_0

    .line 652521
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No preceding call to #readHistoricalData"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 652522
    :cond_0
    iget-boolean v0, p0, LX/3va;->n:Z

    if-nez v0, :cond_2

    .line 652523
    :cond_1
    :goto_0
    return-void

    .line 652524
    :cond_2
    iput-boolean v3, p0, LX/3va;->n:Z

    .line 652525
    iget-object v0, p0, LX/3va;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 652526
    new-instance v0, LX/3vZ;

    invoke-direct {v0, p0}, LX/3vZ;-><init>(LX/3va;)V

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, LX/3va;->f:Ljava/util/List;

    aput-object v2, v1, v3

    const/4 v2, 0x1

    iget-object v3, p0, LX/3va;->h:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, LX/3r9;->a(Landroid/os/AsyncTask;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method private static e(LX/3va;)V
    .locals 6

    .prologue
    .line 652496
    const/4 v0, 0x0

    .line 652497
    iget-boolean v1, p0, LX/3va;->o:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/3va;->i:Landroid/content/Intent;

    if-eqz v1, :cond_1

    .line 652498
    iput-boolean v0, p0, LX/3va;->o:Z

    .line 652499
    iget-object v1, p0, LX/3va;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 652500
    iget-object v1, p0, LX/3va;->g:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    iget-object v2, p0, LX/3va;->i:Landroid/content/Intent;

    invoke-virtual {v1, v2, v0}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v2

    .line 652501
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    move v1, v0

    .line 652502
    :goto_0
    if-ge v1, v3, :cond_0

    .line 652503
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 652504
    iget-object v4, p0, LX/3va;->e:Ljava/util/List;

    new-instance v5, LX/3vV;

    invoke-direct {v5, p0, v0}, LX/3vV;-><init>(LX/3va;Landroid/content/pm/ResolveInfo;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 652505
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 652506
    :cond_0
    const/4 v0, 0x1

    .line 652507
    :cond_1
    move v0, v0

    .line 652508
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 652509
    iget-boolean v3, p0, LX/3va;->l:Z

    if-eqz v3, :cond_3

    iget-boolean v3, p0, LX/3va;->n:Z

    if-eqz v3, :cond_3

    iget-object v3, p0, LX/3va;->h:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 652510
    iput-boolean v2, p0, LX/3va;->l:Z

    .line 652511
    iput-boolean v1, p0, LX/3va;->m:Z

    .line 652512
    invoke-static {p0}, LX/3va;->j(LX/3va;)V

    .line 652513
    :goto_1
    move v1, v1

    .line 652514
    or-int/2addr v0, v1

    .line 652515
    invoke-direct {p0}, LX/3va;->i()V

    .line 652516
    if-eqz v0, :cond_2

    .line 652517
    invoke-direct {p0}, LX/3va;->f()Z

    .line 652518
    invoke-virtual {p0}, LX/3va;->notifyChanged()V

    .line 652519
    :cond_2
    return-void

    :cond_3
    move v1, v2

    goto :goto_1
.end method

.method private f()Z
    .locals 1

    .prologue
    .line 652389
    iget-object v0, p0, LX/3va;->j:LX/3vW;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3va;->i:Landroid/content/Intent;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3va;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/3va;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 652390
    iget-object v0, p0, LX/3va;->f:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    .line 652391
    const/4 v0, 0x1

    .line 652392
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private i()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 652451
    iget-object v0, p0, LX/3va;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget v2, p0, LX/3va;->k:I

    sub-int v2, v0, v2

    .line 652452
    if-gtz v2, :cond_1

    .line 652453
    :cond_0
    return-void

    .line 652454
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/3va;->n:Z

    move v0, v1

    .line 652455
    :goto_0
    if-ge v0, v2, :cond_0

    .line 652456
    iget-object v3, p0, LX/3va;->f:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 652457
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static j(LX/3va;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 652458
    :try_start_0
    iget-object v0, p0, LX/3va;->g:Landroid/content/Context;

    iget-object v1, p0, LX/3va;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_6

    move-result-object v1

    .line 652459
    :try_start_1
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v2

    .line 652460
    const/4 v0, 0x0

    invoke-interface {v2, v1, v0}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 652461
    const/4 v0, 0x0

    .line 652462
    :goto_0
    if-eq v0, v8, :cond_0

    const/4 v3, 0x2

    if-eq v0, v3, :cond_0

    .line 652463
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    goto :goto_0

    .line 652464
    :cond_0
    const-string v0, "historical-records"

    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 652465
    new-instance v0, Lorg/xmlpull/v1/XmlPullParserException;

    const-string v2, "Share records file does not start with historical-records tag."

    invoke-direct {v0, v2}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 652466
    :catch_0
    move-exception v0

    .line 652467
    :try_start_2
    sget-object v2, LX/3va;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Error reading historical recrod file: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, LX/3va;->h:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 652468
    if-eqz v1, :cond_1

    .line 652469
    :try_start_3
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4

    .line 652470
    :cond_1
    :goto_1
    return-void

    .line 652471
    :cond_2
    :try_start_4
    iget-object v0, p0, LX/3va;->f:Ljava/util/List;

    .line 652472
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 652473
    :cond_3
    :goto_2
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v3

    .line 652474
    if-eq v3, v8, :cond_6

    .line 652475
    const/4 v4, 0x3

    if-eq v3, v4, :cond_3

    const/4 v4, 0x4

    if-eq v3, v4, :cond_3

    .line 652476
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v3

    .line 652477
    const-string v4, "historical-record"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 652478
    new-instance v0, Lorg/xmlpull/v1/XmlPullParserException;

    const-string v2, "Share records file not well-formed."

    invoke-direct {v0, v2}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_4
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 652479
    :catch_1
    move-exception v0

    .line 652480
    :try_start_5
    sget-object v2, LX/3va;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Error reading historical recrod file: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, LX/3va;->h:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 652481
    if-eqz v1, :cond_1

    .line 652482
    :try_start_6
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    goto :goto_1

    .line 652483
    :catch_2
    goto :goto_1

    .line 652484
    :cond_4
    const/4 v3, 0x0

    :try_start_7
    const-string v4, "activity"

    invoke-interface {v2, v3, v4}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 652485
    const/4 v4, 0x0

    const-string v5, "time"

    invoke-interface {v2, v4, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 652486
    const/4 v6, 0x0

    const-string v7, "weight"

    invoke-interface {v2, v6, v7}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v6

    .line 652487
    new-instance v7, LX/3vX;

    invoke-direct {v7, v3, v4, v5, v6}, LX/3vX;-><init>(Ljava/lang/String;JF)V

    .line 652488
    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_7
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_2

    .line 652489
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_5

    .line 652490
    :try_start_8
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    .line 652491
    :cond_5
    :goto_3
    throw v0

    .line 652492
    :cond_6
    if-eqz v1, :cond_1

    .line 652493
    :try_start_9
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_3

    goto :goto_1

    .line 652494
    :catch_3
    goto :goto_1

    :catch_4
    goto :goto_1

    :catch_5
    goto :goto_3

    .line 652495
    :catch_6
    goto :goto_1
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 652447
    iget-object v1, p0, LX/3va;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 652448
    :try_start_0
    invoke-static {p0}, LX/3va;->e(LX/3va;)V

    .line 652449
    iget-object v0, p0, LX/3va;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    monitor-exit v1

    return v0

    .line 652450
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Landroid/content/pm/ResolveInfo;)I
    .locals 5

    .prologue
    .line 652435
    iget-object v2, p0, LX/3va;->d:Ljava/lang/Object;

    monitor-enter v2

    .line 652436
    :try_start_0
    invoke-static {p0}, LX/3va;->e(LX/3va;)V

    .line 652437
    iget-object v3, p0, LX/3va;->e:Ljava/util/List;

    .line 652438
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    .line 652439
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v4, :cond_1

    .line 652440
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3vV;

    .line 652441
    iget-object v0, v0, LX/3vV;->a:Landroid/content/pm/ResolveInfo;

    if-ne v0, p1, :cond_0

    .line 652442
    monitor-exit v2

    move v0, v1

    .line 652443
    :goto_1
    return v0

    .line 652444
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 652445
    :cond_1
    const/4 v0, -0x1

    monitor-exit v2

    goto :goto_1

    .line 652446
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(I)Landroid/content/pm/ResolveInfo;
    .locals 2

    .prologue
    .line 652431
    iget-object v1, p0, LX/3va;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 652432
    :try_start_0
    invoke-static {p0}, LX/3va;->e(LX/3va;)V

    .line 652433
    iget-object v0, p0, LX/3va;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3vV;

    iget-object v0, v0, LX/3vV;->a:Landroid/content/pm/ResolveInfo;

    monitor-exit v1

    return-object v0

    .line 652434
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final b(I)Landroid/content/Intent;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 652413
    iget-object v2, p0, LX/3va;->d:Ljava/lang/Object;

    monitor-enter v2

    .line 652414
    :try_start_0
    iget-object v0, p0, LX/3va;->i:Landroid/content/Intent;

    if-nez v0, :cond_0

    .line 652415
    monitor-exit v2

    move-object v0, v1

    .line 652416
    :goto_0
    return-object v0

    .line 652417
    :cond_0
    invoke-static {p0}, LX/3va;->e(LX/3va;)V

    .line 652418
    iget-object v0, p0, LX/3va;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3vV;

    .line 652419
    new-instance v3, Landroid/content/ComponentName;

    iget-object v4, v0, LX/3vV;->a:Landroid/content/pm/ResolveInfo;

    iget-object v4, v4, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v4, v4, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    iget-object v0, v0, LX/3vV;->a:Landroid/content/pm/ResolveInfo;

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    invoke-direct {v3, v4, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 652420
    new-instance v0, Landroid/content/Intent;

    iget-object v4, p0, LX/3va;->i:Landroid/content/Intent;

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 652421
    invoke-virtual {v0, v3}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 652422
    iget-object v4, p0, LX/3va;->p:LX/3vY;

    if-eqz v4, :cond_1

    .line 652423
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4, v0}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 652424
    iget-object v4, p0, LX/3va;->p:LX/3vY;

    invoke-interface {v4}, LX/3vY;->a()Z

    move-result v4

    .line 652425
    if-eqz v4, :cond_1

    .line 652426
    monitor-exit v2

    move-object v0, v1

    goto :goto_0

    .line 652427
    :cond_1
    new-instance v1, LX/3vX;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-direct {v1, v3, v4, v5, v6}, LX/3vX;-><init>(Landroid/content/ComponentName;JF)V

    .line 652428
    invoke-static {p0, v1}, LX/3va;->a(LX/3va;LX/3vX;)Z

    .line 652429
    monitor-exit v2

    goto :goto_0

    .line 652430
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final b()Landroid/content/pm/ResolveInfo;
    .locals 3

    .prologue
    .line 652405
    iget-object v1, p0, LX/3va;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 652406
    :try_start_0
    invoke-static {p0}, LX/3va;->e(LX/3va;)V

    .line 652407
    iget-object v0, p0, LX/3va;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 652408
    iget-object v0, p0, LX/3va;->e:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3vV;

    iget-object v0, v0, LX/3vV;->a:Landroid/content/pm/ResolveInfo;

    monitor-exit v1

    .line 652409
    :goto_0
    return-object v0

    .line 652410
    :cond_0
    monitor-exit v1

    .line 652411
    const/4 v0, 0x0

    goto :goto_0

    .line 652412
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final c(I)V
    .locals 6

    .prologue
    .line 652393
    iget-object v2, p0, LX/3va;->d:Ljava/lang/Object;

    monitor-enter v2

    .line 652394
    :try_start_0
    invoke-static {p0}, LX/3va;->e(LX/3va;)V

    .line 652395
    iget-object v0, p0, LX/3va;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3vV;

    .line 652396
    iget-object v1, p0, LX/3va;->e:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3vV;

    .line 652397
    if-eqz v1, :cond_0

    .line 652398
    iget v1, v1, LX/3vV;->b:F

    iget v3, v0, LX/3vV;->b:F

    sub-float/2addr v1, v3

    const/high16 v3, 0x40a00000    # 5.0f

    add-float/2addr v1, v3

    .line 652399
    :goto_0
    new-instance v3, Landroid/content/ComponentName;

    iget-object v4, v0, LX/3vV;->a:Landroid/content/pm/ResolveInfo;

    iget-object v4, v4, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v4, v4, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    iget-object v0, v0, LX/3vV;->a:Landroid/content/pm/ResolveInfo;

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    invoke-direct {v3, v4, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 652400
    new-instance v0, LX/3vX;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct {v0, v3, v4, v5, v1}, LX/3vX;-><init>(Landroid/content/ComponentName;JF)V

    .line 652401
    invoke-static {p0, v0}, LX/3va;->a(LX/3va;LX/3vX;)Z

    .line 652402
    monitor-exit v2

    return-void

    .line 652403
    :cond_0
    const/high16 v1, 0x3f800000    # 1.0f

    goto :goto_0

    .line 652404
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
