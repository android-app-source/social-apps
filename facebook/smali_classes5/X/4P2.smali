.class public LX/4P2;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 699426
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 699427
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v3, :cond_8

    .line 699428
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 699429
    :goto_0
    return v0

    .line 699430
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_6

    .line 699431
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 699432
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 699433
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_0

    if-eqz v7, :cond_0

    .line 699434
    const-string v8, "has_shared_info"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 699435
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v6, v0

    move v0, v1

    goto :goto_1

    .line 699436
    :cond_1
    const-string v8, "id"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 699437
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 699438
    :cond_2
    const-string v8, "signed_request"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 699439
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 699440
    :cond_3
    const-string v8, "split_flow_url"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 699441
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 699442
    :cond_4
    const-string v8, "url"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 699443
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 699444
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 699445
    :cond_6
    const/4 v7, 0x6

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 699446
    if-eqz v0, :cond_7

    .line 699447
    invoke-virtual {p1, v1, v6}, LX/186;->a(IZ)V

    .line 699448
    :cond_7
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 699449
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 699450
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 699451
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 699452
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :cond_8
    move v2, v0

    move v3, v0

    move v4, v0

    move v5, v0

    move v6, v0

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 2

    .prologue
    .line 699453
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 699454
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 699455
    if-eqz v0, :cond_0

    .line 699456
    const-string v1, "has_shared_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 699457
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 699458
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 699459
    if-eqz v0, :cond_1

    .line 699460
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 699461
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 699462
    :cond_1
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 699463
    if-eqz v0, :cond_2

    .line 699464
    const-string v1, "signed_request"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 699465
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 699466
    :cond_2
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 699467
    if-eqz v0, :cond_3

    .line 699468
    const-string v1, "split_flow_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 699469
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 699470
    :cond_3
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 699471
    if-eqz v0, :cond_4

    .line 699472
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 699473
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 699474
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 699475
    return-void
.end method
