.class public final LX/43T;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/0Zd;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/0Zd;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method private constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 669076
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 669077
    iput-object p1, p0, LX/43T;->a:LX/0QB;

    .line 669078
    return-void
.end method

.method public static a(LX/0QB;)Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QB;",
            ")",
            "Ljava/util/Set",
            "<",
            "LX/0Zd;",
            ">;"
        }
    .end annotation

    .prologue
    .line 669056
    new-instance v0, LX/0U8;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    new-instance v2, LX/43T;

    invoke-direct {v2, p0}, LX/43T;-><init>(LX/0QB;)V

    invoke-direct {v0, v1, v2}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 669075
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/43T;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 669058
    packed-switch p2, :pswitch_data_0

    .line 669059
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 669060
    :pswitch_0
    invoke-static {p1}, LX/AkQ;->a(LX/0QB;)LX/AkQ;

    move-result-object v0

    .line 669061
    :goto_0
    return-object v0

    .line 669062
    :pswitch_1
    invoke-static {p1}, LX/3mH;->a(LX/0QB;)LX/3mH;

    move-result-object v0

    goto :goto_0

    .line 669063
    :pswitch_2
    invoke-static {p1}, LX/0pV;->a(LX/0QB;)LX/0pV;

    move-result-object v0

    goto :goto_0

    .line 669064
    :pswitch_3
    invoke-static {p1}, LX/1BW;->a(LX/0QB;)LX/1BW;

    move-result-object v0

    goto :goto_0

    .line 669065
    :pswitch_4
    invoke-static {p1}, LX/Jd9;->b(LX/0QB;)LX/Jd9;

    move-result-object v0

    goto :goto_0

    .line 669066
    :pswitch_5
    new-instance v2, LX/FMF;

    const-class v3, Landroid/content/Context;

    invoke-interface {p1, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {p1}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v5, 0xd9e

    invoke-static {p1, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0xd9f

    invoke-static {p1, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x2979

    invoke-static {p1, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-direct/range {v2 .. v7}, LX/FMF;-><init>(Landroid/content/Context;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 669067
    move-object v0, v2

    .line 669068
    goto :goto_0

    .line 669069
    :pswitch_6
    invoke-static {p1}, LX/DqE;->b(LX/0QB;)LX/DqE;

    move-result-object v0

    goto :goto_0

    .line 669070
    :pswitch_7
    invoke-static {p1}, LX/74M;->b(LX/0QB;)LX/74M;

    move-result-object v0

    goto :goto_0

    .line 669071
    :pswitch_8
    new-instance v1, LX/Ckx;

    invoke-static {p1}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {v1, v0}, LX/Ckx;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 669072
    move-object v0, v1

    .line 669073
    goto :goto_0

    .line 669074
    :pswitch_9
    invoke-static {p1}, LX/0Zc;->a(LX/0QB;)LX/0Zc;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 669057
    const/16 v0, 0xa

    return v0
.end method
