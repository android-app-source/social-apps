.class public LX/3mN;
.super LX/25J;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/25J",
        "<",
        "Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile k:LX/3mN;


# instance fields
.field public final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0tX;

.field public final c:Ljava/util/concurrent/ExecutorService;

.field public final d:LX/03V;

.field public final e:LX/189;

.field public final f:Lcom/facebook/performancelogger/PerformanceLogger;

.field private final g:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "LX/DCq;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3iV;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/0sa;

.field public final j:LX/0bH;


# direct methods
.method public constructor <init>(LX/03V;LX/0tX;Ljava/util/concurrent/ExecutorService;LX/189;LX/1Ck;Lcom/facebook/performancelogger/PerformanceLogger;LX/0Ot;LX/0sa;LX/0bH;)V
    .locals 1
    .param p3    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0tX;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/189;",
            "LX/1Ck;",
            "Lcom/facebook/performancelogger/PerformanceLogger;",
            "LX/0Ot",
            "<",
            "LX/3iV;",
            ">;",
            "LX/0sa;",
            "LX/0bH;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 636327
    invoke-direct {p0}, LX/25J;-><init>()V

    .line 636328
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/3mN;->a:Ljava/util/Set;

    .line 636329
    iput-object p1, p0, LX/3mN;->d:LX/03V;

    .line 636330
    iput-object p2, p0, LX/3mN;->b:LX/0tX;

    .line 636331
    iput-object p3, p0, LX/3mN;->c:Ljava/util/concurrent/ExecutorService;

    .line 636332
    iput-object p4, p0, LX/3mN;->e:LX/189;

    .line 636333
    iput-object p5, p0, LX/3mN;->g:LX/1Ck;

    .line 636334
    iput-object p6, p0, LX/3mN;->f:Lcom/facebook/performancelogger/PerformanceLogger;

    .line 636335
    iput-object p7, p0, LX/3mN;->h:LX/0Ot;

    .line 636336
    iput-object p8, p0, LX/3mN;->i:LX/0sa;

    .line 636337
    iput-object p9, p0, LX/3mN;->j:LX/0bH;

    .line 636338
    return-void
.end method

.method public static a(LX/0QB;)LX/3mN;
    .locals 13

    .prologue
    .line 636314
    sget-object v0, LX/3mN;->k:LX/3mN;

    if-nez v0, :cond_1

    .line 636315
    const-class v1, LX/3mN;

    monitor-enter v1

    .line 636316
    :try_start_0
    sget-object v0, LX/3mN;->k:LX/3mN;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 636317
    if-eqz v2, :cond_0

    .line 636318
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 636319
    new-instance v3, LX/3mN;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v5

    check-cast v5, LX/0tX;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/189;->b(LX/0QB;)LX/189;

    move-result-object v7

    check-cast v7, LX/189;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v8

    check-cast v8, LX/1Ck;

    invoke-static {v0}, LX/0XW;->a(LX/0QB;)LX/0XW;

    move-result-object v9

    check-cast v9, Lcom/facebook/performancelogger/PerformanceLogger;

    const/16 v10, 0x474

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-static {v0}, LX/0sa;->a(LX/0QB;)LX/0sa;

    move-result-object v11

    check-cast v11, LX/0sa;

    invoke-static {v0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v12

    check-cast v12, LX/0bH;

    invoke-direct/range {v3 .. v12}, LX/3mN;-><init>(LX/03V;LX/0tX;Ljava/util/concurrent/ExecutorService;LX/189;LX/1Ck;Lcom/facebook/performancelogger/PerformanceLogger;LX/0Ot;LX/0sa;LX/0bH;)V

    .line 636320
    move-object v0, v3

    .line 636321
    sput-object v0, LX/3mN;->k:LX/3mN;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 636322
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 636323
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 636324
    :cond_1
    sget-object v0, LX/3mN;->k:LX/3mN;

    return-object v0

    .line 636325
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 636326
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;)Z
    .locals 2

    .prologue
    .line 636296
    invoke-static {p0}, LX/3mN;->c(Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;)Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    .line 636297
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPageInfo;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPageInfo;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c(Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;)Lcom/facebook/graphql/model/GraphQLPageInfo;
    .locals 1

    .prologue
    .line 636313
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsConnection;->j()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;I)Z
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 636306
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;->k()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    .line 636307
    if-ne p2, v2, :cond_0

    .line 636308
    iget-object v3, p0, LX/3mN;->f:Lcom/facebook/performancelogger/PerformanceLogger;

    new-instance v4, LX/0Yj;

    const v5, 0x640001

    const-string v6, "PaginatedGysjFeedUnitTTI"

    invoke-direct {v4, v5, v6}, LX/0Yj;-><init>(ILjava/lang/String;)V

    new-array v5, v0, [Ljava/lang/String;

    const-string v6, "native_newsfeed"

    aput-object v6, v5, v1

    invoke-virtual {v4, v5}, LX/0Yj;->a([Ljava/lang/String;)LX/0Yj;

    move-result-object v4

    invoke-interface {v3, v4, v0}, Lcom/facebook/performancelogger/PerformanceLogger;->a(LX/0Yj;Z)V

    .line 636309
    :cond_0
    const/4 v4, 0x1

    .line 636310
    const/4 v3, 0x3

    if-gt v2, v3, :cond_1

    .line 636311
    :cond_1
    move v3, v4

    .line 636312
    iget-object v4, p0, LX/3mN;->a:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;->g()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    sub-int/2addr v2, v3

    if-lt p2, v2, :cond_2

    :goto_0
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)Z
    .locals 1

    .prologue
    .line 636305
    check-cast p1, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;

    invoke-static {p1}, LX/3mN;->a(Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;I)Z
    .locals 1

    .prologue
    .line 636304
    check-cast p1, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;

    invoke-virtual {p0, p1, p2}, LX/3mN;->a(Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;I)Z

    move-result v0

    return v0
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;)V
    .locals 5

    .prologue
    .line 636299
    invoke-static {p1}, LX/3mN;->c(Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;)Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    .line 636300
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPageInfo;->a()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    .line 636301
    :cond_0
    :goto_0
    return-void

    .line 636302
    :cond_1
    iget-object v1, p0, LX/3mN;->a:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;->g()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 636303
    iget-object v1, p0, LX/3mN;->g:LX/1Ck;

    sget-object v2, LX/DCq;->GYSJ_FETCH_MORE:LX/DCq;

    new-instance v3, LX/DCn;

    invoke-direct {v3, p0, p1, v0}, LX/DCn;-><init>(LX/3mN;Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;Lcom/facebook/graphql/model/GraphQLPageInfo;)V

    new-instance v4, LX/DCo;

    invoke-direct {v4, p0, p1, v0}, LX/DCo;-><init>(LX/3mN;Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;Lcom/facebook/graphql/model/GraphQLPageInfo;)V

    invoke-virtual {v1, v2, v3, v4}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    goto :goto_0
.end method

.method public final bridge synthetic b(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)V
    .locals 0

    .prologue
    .line 636298
    check-cast p1, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;

    invoke-virtual {p0, p1}, LX/3mN;->b(Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;)V

    return-void
.end method
