.class public final LX/4qc;
.super LX/3CB;
.source ""


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JacksonStdImpl;
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final _factory:LX/2At;

.field public final _resolver:LX/4rm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4rm",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/4rm;LX/2At;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4rm",
            "<*>;",
            "LX/2At;",
            ")V"
        }
    .end annotation

    .prologue
    .line 814537
    iget-object v0, p1, LX/4rm;->_enumClass:Ljava/lang/Class;

    move-object v0, v0

    .line 814538
    invoke-direct {p0, v0}, LX/3CB;-><init>(Ljava/lang/Class;)V

    .line 814539
    iput-object p1, p0, LX/4qc;->_resolver:LX/4rm;

    .line 814540
    iput-object p2, p0, LX/4qc;->_factory:LX/2At;

    .line 814541
    return-void
.end method


# virtual methods
.method public final b(Ljava/lang/String;LX/0n3;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 814542
    iget-object v0, p0, LX/4qc;->_factory:LX/2At;

    if-eqz v0, :cond_1

    .line 814543
    :try_start_0
    iget-object v0, p0, LX/4qc;->_factory:LX/2At;

    invoke-virtual {v0, p1}, LX/2Au;->a(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 814544
    :cond_0
    return-object v0

    .line 814545
    :catch_0
    move-exception v0

    .line 814546
    invoke-static {v0}, LX/1Xw;->b(Ljava/lang/Throwable;)V

    .line 814547
    :cond_1
    iget-object v0, p0, LX/4qc;->_resolver:LX/4rm;

    invoke-virtual {v0, p1}, LX/4rm;->a(Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    .line 814548
    if-nez v0, :cond_0

    .line 814549
    iget-object v1, p2, LX/0n3;->_config:LX/0mu;

    move-object v1, v1

    .line 814550
    sget-object v2, LX/0mv;->READ_UNKNOWN_ENUM_VALUES_AS_NULL:LX/0mv;

    invoke-virtual {v1, v2}, LX/0mu;->c(LX/0mv;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 814551
    iget-object v0, p0, LX/3CB;->_keyClass:Ljava/lang/Class;

    const-string v1, "not one of values for Enum class"

    invoke-virtual {p2, v0, p1, v1}, LX/0n3;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)LX/28E;

    move-result-object v0

    throw v0
.end method
