.class public final LX/3uS;
.super LX/3oR;
.source ""


# instance fields
.field public final synthetic a:LX/3uZ;


# direct methods
.method public constructor <init>(LX/3uZ;)V
    .locals 0

    .prologue
    .line 648473
    iput-object p1, p0, LX/3uS;->a:LX/3uZ;

    invoke-direct {p0}, LX/3oR;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(Landroid/view/View;)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 648474
    iget-object v0, p0, LX/3uS;->a:LX/3uZ;

    iget-boolean v0, v0, LX/3uZ;->D:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3uS;->a:LX/3uZ;

    iget-object v0, v0, LX/3uZ;->s:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 648475
    iget-object v0, p0, LX/3uS;->a:LX/3uZ;

    iget-object v0, v0, LX/3uZ;->s:Landroid/view/View;

    invoke-static {v0, v1}, LX/0vv;->b(Landroid/view/View;F)V

    .line 648476
    iget-object v0, p0, LX/3uS;->a:LX/3uZ;

    iget-object v0, v0, LX/3uZ;->o:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v0, v1}, LX/0vv;->b(Landroid/view/View;F)V

    .line 648477
    :cond_0
    iget-object v0, p0, LX/3uS;->a:LX/3uZ;

    iget-object v0, v0, LX/3uZ;->r:Landroid/support/v7/internal/widget/ActionBarContainer;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/3uS;->a:LX/3uZ;

    iget v0, v0, LX/3uZ;->A:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 648478
    iget-object v0, p0, LX/3uS;->a:LX/3uZ;

    iget-object v0, v0, LX/3uZ;->r:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0, v2}, Landroid/support/v7/internal/widget/ActionBarContainer;->setVisibility(I)V

    .line 648479
    :cond_1
    iget-object v0, p0, LX/3uS;->a:LX/3uZ;

    iget-object v0, v0, LX/3uZ;->o:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0, v2}, Landroid/support/v7/internal/widget/ActionBarContainer;->setVisibility(I)V

    .line 648480
    iget-object v0, p0, LX/3uS;->a:LX/3uZ;

    iget-object v0, v0, LX/3uZ;->o:Landroid/support/v7/internal/widget/ActionBarContainer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/ActionBarContainer;->setTransitioning(Z)V

    .line 648481
    iget-object v0, p0, LX/3uS;->a:LX/3uZ;

    const/4 v1, 0x0

    .line 648482
    iput-object v1, v0, LX/3uZ;->I:LX/3uk;

    .line 648483
    iget-object v0, p0, LX/3uS;->a:LX/3uZ;

    const/4 p1, 0x0

    .line 648484
    iget-object v1, v0, LX/3uZ;->c:LX/3uG;

    if-eqz v1, :cond_2

    .line 648485
    iget-object v1, v0, LX/3uZ;->c:LX/3uG;

    iget-object v2, v0, LX/3uZ;->b:LX/3uV;

    invoke-interface {v1, v2}, LX/3uG;->a(LX/3uV;)V

    .line 648486
    iput-object p1, v0, LX/3uZ;->b:LX/3uV;

    .line 648487
    iput-object p1, v0, LX/3uZ;->c:LX/3uG;

    .line 648488
    :cond_2
    iget-object v0, p0, LX/3uS;->a:LX/3uZ;

    iget-object v0, v0, LX/3uZ;->n:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    if-eqz v0, :cond_3

    .line 648489
    iget-object v0, p0, LX/3uS;->a:LX/3uZ;

    iget-object v0, v0, LX/3uZ;->n:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    invoke-static {v0}, LX/0vv;->z(Landroid/view/View;)V

    .line 648490
    :cond_3
    return-void
.end method
