.class public final LX/3SX;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 11

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 582634
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_8

    .line 582635
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 582636
    :goto_0
    return v1

    .line 582637
    :cond_0
    const-string v9, "priority"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 582638
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v5, v0

    move v0, v2

    .line 582639
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_6

    .line 582640
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 582641
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 582642
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_1

    if-eqz v8, :cond_1

    .line 582643
    const-string v9, "id"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 582644
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 582645
    :cond_2
    const-string v9, "name"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 582646
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 582647
    :cond_3
    const-string v9, "priority_subunits"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 582648
    invoke-static {p0, p1}, LX/3SY;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 582649
    :cond_4
    const-string v9, "required_subunits"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 582650
    invoke-static {p0, p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 582651
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 582652
    :cond_6
    const/4 v8, 0x5

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 582653
    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 582654
    invoke-virtual {p1, v2, v6}, LX/186;->b(II)V

    .line 582655
    if-eqz v0, :cond_7

    .line 582656
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v5, v1}, LX/186;->a(III)V

    .line 582657
    :cond_7
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 582658
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 582659
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_8
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x0

    .line 582660
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 582661
    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 582662
    if-eqz v0, :cond_0

    .line 582663
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 582664
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 582665
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 582666
    if-eqz v0, :cond_1

    .line 582667
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 582668
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 582669
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 582670
    if-eqz v0, :cond_2

    .line 582671
    const-string v1, "priority"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 582672
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 582673
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 582674
    if-eqz v0, :cond_6

    .line 582675
    const-string v1, "priority_subunits"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 582676
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 582677
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_5

    .line 582678
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    const/4 v4, 0x0

    .line 582679
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 582680
    invoke-virtual {p0, v2, v4, v4}, LX/15i;->a(III)I

    move-result v4

    .line 582681
    if-eqz v4, :cond_3

    .line 582682
    const-string p3, "priority"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 582683
    invoke-virtual {p2, v4}, LX/0nX;->b(I)V

    .line 582684
    :cond_3
    const/4 v4, 0x1

    invoke-virtual {p0, v2, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 582685
    if-eqz v4, :cond_4

    .line 582686
    const-string p3, "subunit_id"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 582687
    invoke-virtual {p2, v4}, LX/0nX;->b(Ljava/lang/String;)V

    .line 582688
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 582689
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 582690
    :cond_5
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 582691
    :cond_6
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 582692
    if-eqz v0, :cond_7

    .line 582693
    const-string v0, "required_subunits"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 582694
    invoke-virtual {p0, p1, v3}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 582695
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 582696
    return-void
.end method
