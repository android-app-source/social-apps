.class public final LX/3TU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3TV;


# instance fields
.field public final synthetic a:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;)V
    .locals 0

    .prologue
    .line 584295
    iput-object p1, p0, LX/3TU;->a:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;I)Z
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 584296
    iget-object v0, p0, LX/3TU;->a:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    invoke-virtual {v0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->k()LX/0g8;

    move-result-object v0

    if-nez v0, :cond_0

    move v0, v6

    .line 584297
    :goto_0
    return v0

    .line 584298
    :cond_0
    iget-object v0, p0, LX/3TU;->a:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    invoke-virtual {v0}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->k()LX/0g8;

    move-result-object v0

    invoke-interface {v0, p2}, LX/0g8;->f(I)Ljava/lang/Object;

    move-result-object v1

    .line 584299
    instance-of v0, v1, LX/2lq;

    if-eqz v0, :cond_1

    .line 584300
    const/4 v0, 0x0

    goto :goto_0

    .line 584301
    :cond_1
    instance-of v0, p1, Lcom/facebook/widget/text/BetterTextView;

    if-nez v0, :cond_4

    .line 584302
    check-cast v1, LX/2nq;

    .line 584303
    if-eqz v1, :cond_2

    invoke-interface {v1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-nez v0, :cond_3

    .line 584304
    :cond_2
    iget-object v0, p0, LX/3TU;->a:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    iget-object v0, v0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->o:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->Z:Ljava/lang/Class;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_long_click"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Null story edge in long click"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v6

    .line 584305
    goto :goto_0

    .line 584306
    :cond_3
    iget-object v0, p0, LX/3TU;->a:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    iget-object v0, v0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->K:LX/3D1;

    iget-object v2, p0, LX/3TU;->a:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, LX/3TU;->a:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    iget-object v3, v3, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->L:LX/2jO;

    invoke-interface {v1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/2jO;->a(Ljava/lang/String;Ljava/lang/String;)LX/3D3;

    move-result-object v4

    move-object v3, p1

    move v5, p2

    invoke-virtual/range {v0 .. v5}, LX/3D1;->a(LX/2nq;Landroid/content/Context;Landroid/view/View;LX/3D3;I)V

    :cond_4
    move v0, v6

    .line 584307
    goto :goto_0
.end method
