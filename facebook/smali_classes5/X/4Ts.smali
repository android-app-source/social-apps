.class public LX/4Ts;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 719968
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 11

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 719919
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_8

    .line 719920
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 719921
    :goto_0
    return v1

    .line 719922
    :cond_0
    const-string v9, "year"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 719923
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v3, v0

    move v0, v2

    .line 719924
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_6

    .line 719925
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 719926
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 719927
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_1

    if-eqz v8, :cond_1

    .line 719928
    const-string v9, "id"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 719929
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 719930
    :cond_2
    const-string v9, "label"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 719931
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 719932
    :cond_3
    const-string v9, "timeline_units"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 719933
    invoke-static {p0, p1}, LX/4Tt;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 719934
    :cond_4
    const-string v9, "url"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 719935
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 719936
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 719937
    :cond_6
    const/4 v8, 0x6

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 719938
    invoke-virtual {p1, v2, v7}, LX/186;->b(II)V

    .line 719939
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v6}, LX/186;->b(II)V

    .line 719940
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v5}, LX/186;->b(II)V

    .line 719941
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v4}, LX/186;->b(II)V

    .line 719942
    if-eqz v0, :cond_7

    .line 719943
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v3, v1}, LX/186;->a(III)V

    .line 719944
    :cond_7
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_8
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 719945
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 719946
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 719947
    if-eqz v0, :cond_0

    .line 719948
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 719949
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 719950
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 719951
    if-eqz v0, :cond_1

    .line 719952
    const-string v1, "label"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 719953
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 719954
    :cond_1
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 719955
    if-eqz v0, :cond_2

    .line 719956
    const-string v1, "timeline_units"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 719957
    invoke-static {p0, v0, p2, p3}, LX/4Tt;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 719958
    :cond_2
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 719959
    if-eqz v0, :cond_3

    .line 719960
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 719961
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 719962
    :cond_3
    const/4 v0, 0x5

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->a(III)I

    move-result v0

    .line 719963
    if-eqz v0, :cond_4

    .line 719964
    const-string v1, "year"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 719965
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 719966
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 719967
    return-void
.end method
