.class public final enum LX/3cf;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/3cf;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/3cf;

.field public static final enum FINISHED:LX/3cf;

.field public static final enum PENDING:LX/3cf;

.field public static final enum RUNNING:LX/3cf;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 614792
    new-instance v0, LX/3cf;

    const-string v1, "PENDING"

    invoke-direct {v0, v1, v2}, LX/3cf;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3cf;->PENDING:LX/3cf;

    .line 614793
    new-instance v0, LX/3cf;

    const-string v1, "RUNNING"

    invoke-direct {v0, v1, v3}, LX/3cf;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3cf;->RUNNING:LX/3cf;

    .line 614794
    new-instance v0, LX/3cf;

    const-string v1, "FINISHED"

    invoke-direct {v0, v1, v4}, LX/3cf;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/3cf;->FINISHED:LX/3cf;

    .line 614795
    const/4 v0, 0x3

    new-array v0, v0, [LX/3cf;

    sget-object v1, LX/3cf;->PENDING:LX/3cf;

    aput-object v1, v0, v2

    sget-object v1, LX/3cf;->RUNNING:LX/3cf;

    aput-object v1, v0, v3

    sget-object v1, LX/3cf;->FINISHED:LX/3cf;

    aput-object v1, v0, v4

    sput-object v0, LX/3cf;->$VALUES:[LX/3cf;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 614791
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/3cf;
    .locals 1

    .prologue
    .line 614790
    const-class v0, LX/3cf;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/3cf;

    return-object v0
.end method

.method public static values()[LX/3cf;
    .locals 1

    .prologue
    .line 614789
    sget-object v0, LX/3cf;->$VALUES:[LX/3cf;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/3cf;

    return-object v0
.end method
