.class public LX/4e2;
.super LX/1cZ;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "LX/1cZ",
        "<",
        "Ljava/util/List",
        "<",
        "LX/1FJ",
        "<TT;>;>;>;"
    }
.end annotation


# instance fields
.field public final a:[LX/1ca;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<TT;>;>;"
        }
    .end annotation
.end field

.field private b:I
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method private constructor <init>([LX/1ca;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<TT;>;>;)V"
        }
    .end annotation

    .prologue
    .line 796578
    invoke-direct {p0}, LX/1cZ;-><init>()V

    .line 796579
    iput-object p1, p0, LX/4e2;->a:[LX/1ca;

    .line 796580
    const/4 v0, 0x0

    iput v0, p0, LX/4e2;->b:I

    .line 796581
    return-void
.end method

.method public static varargs a([LX/1ca;)LX/4e2;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<TT;>;>;)",
            "LX/4e2",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 796582
    invoke-static {p0}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 796583
    array-length v0, p0

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/03g;->b(Z)V

    .line 796584
    new-instance v2, LX/4e2;

    invoke-direct {v2, p0}, LX/4e2;-><init>([LX/1ca;)V

    .line 796585
    array-length v3, p0

    move v0, v1

    :goto_1
    if-ge v0, v3, :cond_2

    aget-object v4, p0, v0

    .line 796586
    if-eqz v4, :cond_0

    .line 796587
    new-instance v5, LX/4e1;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {v5, v2}, LX/4e1;-><init>(LX/4e2;)V

    .line 796588
    sget-object v1, LX/1fo;->a:LX/1fo;

    move-object v6, v1

    .line 796589
    invoke-interface {v4, v5, v6}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V

    .line 796590
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    move v0, v1

    .line 796591
    goto :goto_0

    .line 796592
    :cond_2
    return-object v2
.end method

.method public static declared-synchronized j(LX/4e2;)Z
    .locals 2

    .prologue
    .line 796593
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/4e2;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/4e2;->b:I

    iget-object v1, p0, LX/4e2;->a:[LX/1ca;

    array-length v1, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized c()Z
    .locals 2

    .prologue
    .line 796594
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LX/1cZ;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, LX/4e2;->b:I

    iget-object v1, p0, LX/4e2;->a:[LX/1ca;

    array-length v1, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final d()Ljava/lang/Object;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 796595
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LX/1cZ;->c()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 796596
    const/4 v0, 0x0

    .line 796597
    :cond_0
    monitor-exit p0

    return-object v0

    .line 796598
    :cond_1
    :try_start_1
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, LX/4e2;->a:[LX/1ca;

    array-length v1, v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 796599
    iget-object v2, p0, LX/4e2;->a:[LX/1ca;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 796600
    invoke-interface {v4}, LX/1ca;->d()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 796601
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 796602
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final g()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 796603
    invoke-super {p0}, LX/1cZ;->g()Z

    move-result v1

    if-nez v1, :cond_0

    .line 796604
    :goto_0
    return v0

    .line 796605
    :cond_0
    iget-object v1, p0, LX/4e2;->a:[LX/1ca;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 796606
    invoke-interface {v3}, LX/1ca;->g()Z

    .line 796607
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 796608
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
