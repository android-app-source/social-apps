.class public LX/3mF;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0tX;

.field public final b:LX/1Sl;


# direct methods
.method public constructor <init>(LX/0tX;LX/1Sl;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 635738
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 635739
    iput-object p1, p0, LX/3mF;->a:LX/0tX;

    .line 635740
    iput-object p2, p0, LX/3mF;->b:LX/1Sl;

    .line 635741
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGroupJoinState;Ljava/lang/String;)LX/399;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/GroupMemberActionSourceValue;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/enums/GraphQLGroupJoinState;",
            "Ljava/lang/String;",
            ")",
            "LX/399",
            "<",
            "Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupRequestToJoinCoreMutationModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 635725
    new-instance v0, LX/4G4;

    invoke-direct {v0}, LX/4G4;-><init>()V

    invoke-virtual {v0, p0}, LX/4G4;->a(Ljava/lang/String;)LX/4G4;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/4G4;->b(Ljava/lang/String;)LX/4G4;

    move-result-object v0

    .line 635726
    if-eqz p3, :cond_0

    .line 635727
    const-string v1, "story_id"

    invoke-virtual {v0, v1, p3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 635728
    :cond_0
    invoke-static {}, LX/B2A;->b()LX/B29;

    move-result-object v1

    .line 635729
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 635730
    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 635731
    new-instance v1, LX/B2M;

    invoke-direct {v1}, LX/B2M;-><init>()V

    .line 635732
    iput-object p0, v1, LX/B2M;->a:Ljava/lang/String;

    .line 635733
    move-object v1, v1

    .line 635734
    iput-object p2, v1, LX/B2M;->b:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 635735
    move-object v1, v1

    .line 635736
    invoke-virtual {v1}, LX/B2M;->a()Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupRequestToJoinCoreMutationModel$GroupModel;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/399;->a(LX/0jT;)LX/399;

    .line 635737
    return-object v0
.end method

.method public static a(LX/0QB;)LX/3mF;
    .locals 1

    .prologue
    .line 635724
    invoke-static {p0}, LX/3mF;->b(LX/0QB;)LX/3mF;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/3mF;
    .locals 3

    .prologue
    .line 635722
    new-instance v2, LX/3mF;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-static {p0}, LX/1Sl;->b(LX/0QB;)LX/1Sl;

    move-result-object v1

    check-cast v1, LX/1Sl;

    invoke-direct {v2, v0, v1}, LX/3mF;-><init>(LX/0tX;LX/1Sl;)V

    .line 635723
    return-object v2
.end method

.method public static d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/399;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/GroupMemberActionSourceValue;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/GroupReaddPolicy;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "LX/399",
            "<",
            "Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupLeaveCoreMutationModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 635710
    new-instance v0, LX/4Fq;

    invoke-direct {v0}, LX/4Fq;-><init>()V

    invoke-virtual {v0, p0}, LX/4Fq;->a(Ljava/lang/String;)LX/4Fq;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/4Fq;->c(Ljava/lang/String;)LX/4Fq;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/4Fq;->b(Ljava/lang/String;)LX/4Fq;

    move-result-object v0

    .line 635711
    invoke-static {}, LX/B2A;->a()LX/B27;

    move-result-object v1

    .line 635712
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 635713
    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 635714
    new-instance v1, LX/B2J;

    invoke-direct {v1}, LX/B2J;-><init>()V

    .line 635715
    iput-object p0, v1, LX/B2J;->a:Ljava/lang/String;

    .line 635716
    move-object v1, v1

    .line 635717
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 635718
    iput-object v2, v1, LX/B2J;->b:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 635719
    move-object v1, v1

    .line 635720
    invoke-virtual {v1}, LX/B2J;->a()Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupLeaveCoreMutationModel$GroupModel;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/399;->a(LX/0jT;)LX/399;

    .line 635721
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/groups/mutations/protocol/GroupArchiveStatusMutationModels$GroupArchiveMutationFieldsModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 635742
    new-instance v0, LX/4Ff;

    invoke-direct {v0}, LX/4Ff;-><init>()V

    .line 635743
    const-string v1, "group_id"

    invoke-virtual {v0, v1, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 635744
    move-object v0, v0

    .line 635745
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    .line 635746
    const-string v2, "client_mutation_id"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 635747
    move-object v0, v0

    .line 635748
    new-instance v1, LX/B1q;

    invoke-direct {v1}, LX/B1q;-><init>()V

    move-object v1, v1

    .line 635749
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 635750
    new-instance v0, LX/B1u;

    invoke-direct {v0}, LX/B1u;-><init>()V

    .line 635751
    iput-object p1, v0, LX/B1u;->b:Ljava/lang/String;

    .line 635752
    move-object v0, v0

    .line 635753
    sget-object v2, LX/0SF;->a:LX/0SF;

    move-object v2, v2

    .line 635754
    invoke-virtual {v2}, LX/0SF;->a()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    .line 635755
    iput-wide v2, v0, LX/B1u;->a:J

    .line 635756
    move-object v0, v0

    .line 635757
    invoke-virtual {v0}, LX/B1u;->a()Lcom/facebook/groups/mutations/protocol/GroupArchiveStatusMutationModels$GroupArchiveMutationFieldsModel$GroupModel;

    move-result-object v0

    .line 635758
    new-instance v2, LX/B1t;

    invoke-direct {v2}, LX/B1t;-><init>()V

    .line 635759
    iput-object v0, v2, LX/B1t;->a:Lcom/facebook/groups/mutations/protocol/GroupArchiveStatusMutationModels$GroupArchiveMutationFieldsModel$GroupModel;

    .line 635760
    move-object v0, v2

    .line 635761
    const/4 v10, 0x1

    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 635762
    new-instance v6, LX/186;

    const/16 v7, 0x80

    invoke-direct {v6, v7}, LX/186;-><init>(I)V

    .line 635763
    iget-object v7, v0, LX/B1t;->a:Lcom/facebook/groups/mutations/protocol/GroupArchiveStatusMutationModels$GroupArchiveMutationFieldsModel$GroupModel;

    invoke-static {v6, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 635764
    invoke-virtual {v6, v10}, LX/186;->c(I)V

    .line 635765
    invoke-virtual {v6, v9, v7}, LX/186;->b(II)V

    .line 635766
    invoke-virtual {v6}, LX/186;->d()I

    move-result v7

    .line 635767
    invoke-virtual {v6, v7}, LX/186;->d(I)V

    .line 635768
    invoke-virtual {v6}, LX/186;->e()[B

    move-result-object v6

    invoke-static {v6}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v7

    .line 635769
    invoke-virtual {v7, v9}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 635770
    new-instance v6, LX/15i;

    move-object v9, v8

    move-object v11, v8

    invoke-direct/range {v6 .. v11}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 635771
    new-instance v7, Lcom/facebook/groups/mutations/protocol/GroupArchiveStatusMutationModels$GroupArchiveMutationFieldsModel;

    invoke-direct {v7, v6}, Lcom/facebook/groups/mutations/protocol/GroupArchiveStatusMutationModels$GroupArchiveMutationFieldsModel;-><init>(LX/15i;)V

    .line 635772
    move-object v0, v7

    .line 635773
    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    .line 635774
    invoke-virtual {v1, v0}, LX/399;->a(LX/0jT;)LX/399;

    .line 635775
    iget-object v0, p0, LX/3mF;->a:LX/0tX;

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/FollowLocations;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 635699
    new-instance v0, LX/4G5;

    invoke-direct {v0}, LX/4G5;-><init>()V

    .line 635700
    const-string v1, "group_id"

    invoke-virtual {v0, v1, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 635701
    move-object v0, v0

    .line 635702
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    .line 635703
    const-string v2, "client_mutation_id"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 635704
    move-object v0, v0

    .line 635705
    const-string v1, "subscribe_location"

    invoke-virtual {v0, v1, p2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 635706
    move-object v0, v0

    .line 635707
    new-instance v1, LX/B14;

    invoke-direct {v1}, LX/B14;-><init>()V

    move-object v1, v1

    .line 635708
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/B14;

    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 635709
    iget-object v1, p0, LX/3mF;->a:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1}, LX/2Ra;->constant(Ljava/lang/Object;)LX/0QK;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGroupJoinState;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/GroupMemberActionSourceValue;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/enums/GraphQLGroupJoinState;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 635697
    invoke-static {p1, p2, p3, v2}, LX/3mF;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGroupJoinState;Ljava/lang/String;)LX/399;

    move-result-object v0

    .line 635698
    iget-object v1, p0, LX/3mF;->a:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    invoke-static {v2}, LX/2Ra;->constant(Ljava/lang/Object;)LX/0QK;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/groups/mutations/protocol/GroupArchiveStatusMutationModels$GroupUnarchiveMutationFieldsModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 635652
    new-instance v0, LX/4G7;

    invoke-direct {v0}, LX/4G7;-><init>()V

    .line 635653
    const-string v1, "group_id"

    invoke-virtual {v0, v1, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 635654
    move-object v0, v0

    .line 635655
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    .line 635656
    const-string v2, "client_mutation_id"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 635657
    move-object v0, v0

    .line 635658
    new-instance v1, LX/B1r;

    invoke-direct {v1}, LX/B1r;-><init>()V

    move-object v1, v1

    .line 635659
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 635660
    new-instance v0, LX/B1x;

    invoke-direct {v0}, LX/B1x;-><init>()V

    .line 635661
    iput-object p1, v0, LX/B1x;->b:Ljava/lang/String;

    .line 635662
    move-object v0, v0

    .line 635663
    const-wide/16 v2, 0x0

    .line 635664
    iput-wide v2, v0, LX/B1x;->a:J

    .line 635665
    move-object v0, v0

    .line 635666
    const/4 v12, 0x1

    const/4 v5, 0x0

    const/4 v11, 0x0

    .line 635667
    new-instance v4, LX/186;

    const/16 v6, 0x80

    invoke-direct {v4, v6}, LX/186;-><init>(I)V

    .line 635668
    iget-object v6, v0, LX/B1x;->b:Ljava/lang/String;

    invoke-virtual {v4, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 635669
    const/4 v6, 0x2

    invoke-virtual {v4, v6}, LX/186;->c(I)V

    .line 635670
    iget-wide v6, v0, LX/B1x;->a:J

    const-wide/16 v8, 0x0

    invoke-virtual/range {v4 .. v9}, LX/186;->a(IJJ)V

    .line 635671
    invoke-virtual {v4, v12, v10}, LX/186;->b(II)V

    .line 635672
    invoke-virtual {v4}, LX/186;->d()I

    move-result v6

    .line 635673
    invoke-virtual {v4, v6}, LX/186;->d(I)V

    .line 635674
    invoke-virtual {v4}, LX/186;->e()[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v6

    .line 635675
    invoke-virtual {v6, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 635676
    new-instance v4, LX/15i;

    move-object v5, v6

    move-object v6, v11

    move-object v7, v11

    move v8, v12

    move-object v9, v11

    invoke-direct/range {v4 .. v9}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 635677
    new-instance v5, Lcom/facebook/groups/mutations/protocol/GroupArchiveStatusMutationModels$GroupUnarchiveMutationFieldsModel$GroupModel;

    invoke-direct {v5, v4}, Lcom/facebook/groups/mutations/protocol/GroupArchiveStatusMutationModels$GroupUnarchiveMutationFieldsModel$GroupModel;-><init>(LX/15i;)V

    .line 635678
    move-object v0, v5

    .line 635679
    new-instance v2, LX/B1w;

    invoke-direct {v2}, LX/B1w;-><init>()V

    .line 635680
    iput-object v0, v2, LX/B1w;->a:Lcom/facebook/groups/mutations/protocol/GroupArchiveStatusMutationModels$GroupUnarchiveMutationFieldsModel$GroupModel;

    .line 635681
    move-object v0, v2

    .line 635682
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 635683
    new-instance v4, LX/186;

    const/16 v5, 0x80

    invoke-direct {v4, v5}, LX/186;-><init>(I)V

    .line 635684
    iget-object v5, v0, LX/B1w;->a:Lcom/facebook/groups/mutations/protocol/GroupArchiveStatusMutationModels$GroupUnarchiveMutationFieldsModel$GroupModel;

    invoke-static {v4, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 635685
    invoke-virtual {v4, v8}, LX/186;->c(I)V

    .line 635686
    invoke-virtual {v4, v7, v5}, LX/186;->b(II)V

    .line 635687
    invoke-virtual {v4}, LX/186;->d()I

    move-result v5

    .line 635688
    invoke-virtual {v4, v5}, LX/186;->d(I)V

    .line 635689
    invoke-virtual {v4}, LX/186;->e()[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 635690
    invoke-virtual {v5, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 635691
    new-instance v4, LX/15i;

    move-object v7, v6

    move-object v9, v6

    invoke-direct/range {v4 .. v9}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 635692
    new-instance v5, Lcom/facebook/groups/mutations/protocol/GroupArchiveStatusMutationModels$GroupUnarchiveMutationFieldsModel;

    invoke-direct {v5, v4}, Lcom/facebook/groups/mutations/protocol/GroupArchiveStatusMutationModels$GroupUnarchiveMutationFieldsModel;-><init>(LX/15i;)V

    .line 635693
    move-object v0, v5

    .line 635694
    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    .line 635695
    invoke-virtual {v1, v0}, LX/399;->a(LX/0jT;)LX/399;

    .line 635696
    iget-object v0, p0, LX/3mF;->a:LX/0tX;

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/FollowLocations;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 635641
    new-instance v0, LX/4GD;

    invoke-direct {v0}, LX/4GD;-><init>()V

    .line 635642
    const-string v1, "group_id"

    invoke-virtual {v0, v1, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 635643
    move-object v0, v0

    .line 635644
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    .line 635645
    const-string v2, "client_mutation_id"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 635646
    move-object v0, v0

    .line 635647
    const-string v1, "subscribe_location"

    invoke-virtual {v0, v1, p2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 635648
    move-object v0, v0

    .line 635649
    new-instance v1, LX/B15;

    invoke-direct {v1}, LX/B15;-><init>()V

    move-object v1, v1

    .line 635650
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/B15;

    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 635651
    iget-object v1, p0, LX/3mF;->a:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1}, LX/2Ra;->constant(Ljava/lang/Object;)LX/0QK;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/GroupMemberActionSourceValue;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/GroupReaddPolicy;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 635638
    invoke-static {p1, p2, p3}, LX/3mF;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/399;

    move-result-object v0

    .line 635639
    iget-object v1, p0, LX/3mF;->a:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1}, LX/2Ra;->constant(Ljava/lang/Object;)LX/0QK;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/GroupMemberActionSourceValue;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 635640
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->REQUESTED:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-virtual {p0, p1, p2, v0}, LX/3mF;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGroupJoinState;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
