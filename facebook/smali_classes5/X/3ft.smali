.class public LX/3ft;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final h:Ljava/lang/Object;


# instance fields
.field public final a:LX/3fX;

.field public final b:LX/0Xl;

.field public final c:LX/11H;

.field public final d:LX/3fu;

.field public final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2Jp;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3fg;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6Nd;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 623717
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/3ft;->h:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/3fX;LX/0Xl;LX/11H;LX/3fu;LX/0Or;LX/0Ot;LX/0Ot;)V
    .locals 0
    .param p2    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3fX;",
            "LX/0Xl;",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            "LX/3fu;",
            "LX/0Or",
            "<",
            "LX/2Jp;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/3fg;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/6Nd;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 623718
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 623719
    iput-object p1, p0, LX/3ft;->a:LX/3fX;

    .line 623720
    iput-object p2, p0, LX/3ft;->b:LX/0Xl;

    .line 623721
    iput-object p3, p0, LX/3ft;->c:LX/11H;

    .line 623722
    iput-object p4, p0, LX/3ft;->d:LX/3fu;

    .line 623723
    iput-object p5, p0, LX/3ft;->e:LX/0Or;

    .line 623724
    iput-object p6, p0, LX/3ft;->f:LX/0Ot;

    .line 623725
    iput-object p7, p0, LX/3ft;->g:LX/0Ot;

    .line 623726
    return-void
.end method

.method public static a(LX/0QB;)LX/3ft;
    .locals 15

    .prologue
    .line 623727
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 623728
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 623729
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 623730
    if-nez v1, :cond_0

    .line 623731
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 623732
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 623733
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 623734
    sget-object v1, LX/3ft;->h:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 623735
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 623736
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 623737
    :cond_1
    if-nez v1, :cond_4

    .line 623738
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 623739
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 623740
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 623741
    new-instance v7, LX/3ft;

    invoke-static {v0}, LX/3fX;->a(LX/0QB;)LX/3fX;

    move-result-object v8

    check-cast v8, LX/3fX;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v9

    check-cast v9, LX/0Xl;

    invoke-static {v0}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/0QB;)Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    move-result-object v10

    check-cast v10, LX/11H;

    .line 623742
    new-instance v11, LX/3fu;

    invoke-direct {v11}, LX/3fu;-><init>()V

    .line 623743
    move-object v11, v11

    .line 623744
    move-object v11, v11

    .line 623745
    check-cast v11, LX/3fu;

    const/16 v12, 0x438

    invoke-static {v0, v12}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v12

    const/16 v13, 0x416

    invoke-static {v0, v13}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v13

    const/16 v14, 0x1a18

    invoke-static {v0, v14}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v14

    invoke-direct/range {v7 .. v14}, LX/3ft;-><init>(LX/3fX;LX/0Xl;LX/11H;LX/3fu;LX/0Or;LX/0Ot;LX/0Ot;)V

    .line 623746
    move-object v1, v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 623747
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 623748
    if-nez v1, :cond_2

    .line 623749
    sget-object v0, LX/3ft;->h:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3ft;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 623750
    :goto_1
    if-eqz v0, :cond_3

    .line 623751
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 623752
    :goto_3
    check-cast v0, LX/3ft;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 623753
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 623754
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 623755
    :catchall_1
    move-exception v0

    .line 623756
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 623757
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 623758
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 623759
    :cond_2
    :try_start_8
    sget-object v0, LX/3ft;->h:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3ft;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method
