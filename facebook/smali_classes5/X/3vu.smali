.class public LX/3vu;
.super Landroid/widget/HorizontalScrollView;
.source ""

# interfaces
.implements LX/3vf;


# static fields
.field private static final l:Landroid/view/animation/Interpolator;


# instance fields
.field public a:Ljava/lang/Runnable;

.field public b:I

.field public c:I

.field public d:LX/3sU;

.field public final e:LX/3vt;

.field public f:LX/3vr;

.field public g:Landroid/support/v7/widget/LinearLayoutCompat;

.field public h:LX/3w4;

.field public i:Z

.field public j:I

.field public k:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 653863
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    sput-object v0, LX/3vu;->l:Landroid/view/animation/Interpolator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 653864
    invoke-direct {p0, p1}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;)V

    .line 653865
    new-instance v0, LX/3vt;

    invoke-direct {v0, p0}, LX/3vt;-><init>(LX/3vu;)V

    iput-object v0, p0, LX/3vu;->e:LX/3vt;

    .line 653866
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/3vu;->setHorizontalScrollBarEnabled(Z)V

    .line 653867
    invoke-static {p1}, LX/3ub;->a(Landroid/content/Context;)LX/3ub;

    move-result-object v0

    .line 653868
    invoke-virtual {v0}, LX/3ub;->e()I

    move-result v1

    invoke-virtual {p0, v1}, LX/3vu;->setContentHeight(I)V

    .line 653869
    invoke-virtual {v0}, LX/3ub;->g()I

    move-result v0

    iput v0, p0, LX/3vu;->c:I

    .line 653870
    new-instance v0, Landroid/support/v7/widget/LinearLayoutCompat;

    invoke-virtual {p0}, LX/3vu;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    const v3, 0x7f01000c

    invoke-direct {v0, v1, v2, v3}, Landroid/support/v7/widget/LinearLayoutCompat;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 653871
    const/4 v1, 0x1

    .line 653872
    iput-boolean v1, v0, Landroid/support/v7/widget/LinearLayoutCompat;->mUseLargestChild:Z

    .line 653873
    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/LinearLayoutCompat;->setGravity(I)V

    .line 653874
    new-instance v1, LX/3wd;

    const/4 v2, -0x2

    const/4 v3, -0x1

    invoke-direct {v1, v2, v3}, LX/3wd;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/LinearLayoutCompat;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 653875
    move-object v0, v0

    .line 653876
    iput-object v0, p0, LX/3vu;->g:Landroid/support/v7/widget/LinearLayoutCompat;

    .line 653877
    iget-object v0, p0, LX/3vu;->g:Landroid/support/v7/widget/LinearLayoutCompat;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    const/4 v2, -0x2

    const/4 v3, -0x1

    invoke-direct {v1, v2, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v1}, LX/3vu;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 653878
    return-void
.end method

.method public static a(LX/3vu;)Z
    .locals 1

    .prologue
    .line 653777
    iget-object v0, p0, LX/3vu;->h:LX/3w4;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3vu;->h:LX/3w4;

    invoke-virtual {v0}, LX/3w4;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-ne v0, p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c()Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 653856
    invoke-static {p0}, LX/3vu;->a(LX/3vu;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 653857
    :goto_0
    return v4

    .line 653858
    :cond_0
    iget-object v0, p0, LX/3vu;->h:LX/3w4;

    invoke-virtual {p0, v0}, LX/3vu;->removeView(Landroid/view/View;)V

    .line 653859
    iget-object v0, p0, LX/3vu;->g:Landroid/support/v7/widget/LinearLayoutCompat;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    const/4 v2, -0x2

    const/4 v3, -0x1

    invoke-direct {v1, v2, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v1}, LX/3vu;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 653860
    iget-object v0, p0, LX/3vu;->h:LX/3w4;

    .line 653861
    iget v1, v0, LX/3vM;->v:I

    move v0, v1

    .line 653862
    invoke-virtual {p0, v0}, LX/3vu;->setTabSelected(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    .prologue
    .line 653850
    iget-object v0, p0, LX/3vu;->g:Landroid/support/v7/widget/LinearLayoutCompat;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/LinearLayoutCompat;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 653851
    iget-object v1, p0, LX/3vu;->a:Ljava/lang/Runnable;

    if-eqz v1, :cond_0

    .line 653852
    iget-object v1, p0, LX/3vu;->a:Ljava/lang/Runnable;

    invoke-virtual {p0, v1}, LX/3vu;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 653853
    :cond_0
    new-instance v1, Landroid/support/v7/internal/widget/ScrollingTabContainerView$1;

    invoke-direct {v1, p0, v0}, Landroid/support/v7/internal/widget/ScrollingTabContainerView$1;-><init>(LX/3vu;Landroid/view/View;)V

    iput-object v1, p0, LX/3vu;->a:Ljava/lang/Runnable;

    .line 653854
    iget-object v0, p0, LX/3vu;->a:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, LX/3vu;->post(Ljava/lang/Runnable;)Z

    .line 653855
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .prologue
    .line 653846
    check-cast p1, LX/3vs;

    .line 653847
    iget-object v0, p1, LX/3vs;->c:LX/3u0;

    move-object v0, v0

    .line 653848
    invoke-virtual {v0}, LX/3u0;->e()V

    .line 653849
    return-void
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x44f82f7e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 653879
    invoke-super {p0}, Landroid/widget/HorizontalScrollView;->onAttachedToWindow()V

    .line 653880
    iget-object v1, p0, LX/3vu;->a:Ljava/lang/Runnable;

    if-eqz v1, :cond_0

    .line 653881
    iget-object v1, p0, LX/3vu;->a:Ljava/lang/Runnable;

    invoke-virtual {p0, v1}, LX/3vu;->post(Ljava/lang/Runnable;)Z

    .line 653882
    :cond_0
    const/16 v1, 0x2d

    const v2, -0x5671c6d6

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 653840
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x8

    if-lt v0, v1, :cond_0

    .line 653841
    invoke-super {p0, p1}, Landroid/widget/HorizontalScrollView;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 653842
    :cond_0
    invoke-virtual {p0}, LX/3vu;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/3ub;->a(Landroid/content/Context;)LX/3ub;

    move-result-object v0

    .line 653843
    invoke-virtual {v0}, LX/3ub;->e()I

    move-result v1

    invoke-virtual {p0, v1}, LX/3vu;->setContentHeight(I)V

    .line 653844
    invoke-virtual {v0}, LX/3ub;->g()I

    move-result v0

    iput v0, p0, LX/3vu;->c:I

    .line 653845
    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x28922e0b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 653836
    invoke-super {p0}, Landroid/widget/HorizontalScrollView;->onDetachedFromWindow()V

    .line 653837
    iget-object v1, p0, LX/3vu;->a:Ljava/lang/Runnable;

    if-eqz v1, :cond_0

    .line 653838
    iget-object v1, p0, LX/3vu;->a:Ljava/lang/Runnable;

    invoke-virtual {p0, v1}, LX/3vu;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 653839
    :cond_0
    const/16 v1, 0x2d

    const v2, -0x7fa6b77a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onMeasure(II)V
    .locals 7

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 653794
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    .line 653795
    if-ne v3, v6, :cond_2

    move v0, v1

    .line 653796
    :goto_0
    invoke-virtual {p0, v0}, LX/3vu;->setFillViewport(Z)V

    .line 653797
    iget-object v4, p0, LX/3vu;->g:Landroid/support/v7/widget/LinearLayoutCompat;

    invoke-virtual {v4}, Landroid/support/v7/widget/LinearLayoutCompat;->getChildCount()I

    move-result v4

    .line 653798
    if-le v4, v1, :cond_4

    if-eq v3, v6, :cond_0

    const/high16 v5, -0x80000000

    if-ne v3, v5, :cond_4

    .line 653799
    :cond_0
    const/4 v3, 0x2

    if-le v4, v3, :cond_3

    .line 653800
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    int-to-float v3, v3

    const v4, 0x3ecccccd    # 0.4f

    mul-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, p0, LX/3vu;->b:I

    .line 653801
    :goto_1
    iget v3, p0, LX/3vu;->b:I

    iget v4, p0, LX/3vu;->c:I

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    iput v3, p0, LX/3vu;->b:I

    .line 653802
    :goto_2
    iget v3, p0, LX/3vu;->j:I

    invoke-static {v3, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 653803
    if-nez v0, :cond_5

    iget-boolean v4, p0, LX/3vu;->i:Z

    if-eqz v4, :cond_5

    .line 653804
    :goto_3
    if-eqz v1, :cond_7

    .line 653805
    iget-object v1, p0, LX/3vu;->g:Landroid/support/v7/widget/LinearLayoutCompat;

    invoke-virtual {v1, v2, v3}, Landroid/support/v7/widget/LinearLayoutCompat;->measure(II)V

    .line 653806
    iget-object v1, p0, LX/3vu;->g:Landroid/support/v7/widget/LinearLayoutCompat;

    invoke-virtual {v1}, Landroid/support/v7/widget/LinearLayoutCompat;->getMeasuredWidth()I

    move-result v1

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    if-le v1, v2, :cond_6

    .line 653807
    invoke-static {p0}, LX/3vu;->a(LX/3vu;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 653808
    :goto_4
    invoke-virtual {p0}, LX/3vu;->getMeasuredWidth()I

    move-result v1

    .line 653809
    invoke-super {p0, p1, v3}, Landroid/widget/HorizontalScrollView;->onMeasure(II)V

    .line 653810
    invoke-virtual {p0}, LX/3vu;->getMeasuredWidth()I

    move-result v2

    .line 653811
    if-eqz v0, :cond_1

    if-eq v1, v2, :cond_1

    .line 653812
    iget v0, p0, LX/3vu;->k:I

    invoke-virtual {p0, v0}, LX/3vu;->setTabSelected(I)V

    .line 653813
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 653814
    goto :goto_0

    .line 653815
    :cond_3
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    iput v3, p0, LX/3vu;->b:I

    goto :goto_1

    .line 653816
    :cond_4
    const/4 v3, -0x1

    iput v3, p0, LX/3vu;->b:I

    goto :goto_2

    :cond_5
    move v1, v2

    .line 653817
    goto :goto_3

    .line 653818
    :cond_6
    invoke-direct {p0}, LX/3vu;->c()Z

    goto :goto_4

    .line 653819
    :cond_7
    invoke-direct {p0}, LX/3vu;->c()Z

    goto :goto_4

    .line 653820
    :cond_8
    iget-object v1, p0, LX/3vu;->h:LX/3w4;

    if-nez v1, :cond_9

    .line 653821
    new-instance v1, LX/3w4;

    invoke-virtual {p0}, LX/3vu;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v4, 0x0

    const v5, 0x7f01002b

    invoke-direct {v1, v2, v4, v5}, LX/3w4;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 653822
    new-instance v2, LX/3wd;

    const/4 v4, -0x2

    const/4 v5, -0x1

    invoke-direct {v2, v4, v5}, LX/3wd;-><init>(II)V

    invoke-virtual {v1, v2}, LX/3w4;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 653823
    invoke-virtual {v1, p0}, LX/3w4;->a(LX/3vf;)V

    .line 653824
    move-object v1, v1

    .line 653825
    iput-object v1, p0, LX/3vu;->h:LX/3w4;

    .line 653826
    :cond_9
    iget-object v1, p0, LX/3vu;->g:Landroid/support/v7/widget/LinearLayoutCompat;

    invoke-virtual {p0, v1}, LX/3vu;->removeView(Landroid/view/View;)V

    .line 653827
    iget-object v1, p0, LX/3vu;->h:LX/3w4;

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    const/4 v4, -0x2

    const/4 v5, -0x1

    invoke-direct {v2, v4, v5}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v1, v2}, LX/3vu;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 653828
    iget-object v1, p0, LX/3vu;->h:LX/3w4;

    .line 653829
    iget-object v2, v1, LX/3vN;->a:Landroid/widget/SpinnerAdapter;

    move-object v1, v2

    .line 653830
    if-nez v1, :cond_a

    .line 653831
    iget-object v1, p0, LX/3vu;->h:LX/3w4;

    new-instance v2, LX/3vq;

    invoke-direct {v2, p0}, LX/3vq;-><init>(LX/3vu;)V

    invoke-virtual {v1, v2}, LX/3w4;->a(Landroid/widget/SpinnerAdapter;)V

    .line 653832
    :cond_a
    iget-object v1, p0, LX/3vu;->a:Ljava/lang/Runnable;

    if-eqz v1, :cond_b

    .line 653833
    iget-object v1, p0, LX/3vu;->a:Ljava/lang/Runnable;

    invoke-virtual {p0, v1}, LX/3vu;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 653834
    const/4 v1, 0x0

    iput-object v1, p0, LX/3vu;->a:Ljava/lang/Runnable;

    .line 653835
    :cond_b
    iget-object v1, p0, LX/3vu;->h:LX/3w4;

    iget v2, p0, LX/3vu;->k:I

    invoke-virtual {v1, v2}, LX/3vM;->setSelection(I)V

    goto/16 :goto_4
.end method

.method public setContentHeight(I)V
    .locals 0

    .prologue
    .line 653791
    iput p1, p0, LX/3vu;->j:I

    .line 653792
    invoke-virtual {p0}, LX/3vu;->requestLayout()V

    .line 653793
    return-void
.end method

.method public setTabSelected(I)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 653778
    iput p1, p0, LX/3vu;->k:I

    .line 653779
    iget-object v0, p0, LX/3vu;->g:Landroid/support/v7/widget/LinearLayoutCompat;

    invoke-virtual {v0}, Landroid/support/v7/widget/LinearLayoutCompat;->getChildCount()I

    move-result v3

    move v2, v1

    .line 653780
    :goto_0
    if-ge v2, v3, :cond_2

    .line 653781
    iget-object v0, p0, LX/3vu;->g:Landroid/support/v7/widget/LinearLayoutCompat;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/LinearLayoutCompat;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 653782
    if-ne v2, p1, :cond_1

    const/4 v0, 0x1

    .line 653783
    :goto_1
    invoke-virtual {v4, v0}, Landroid/view/View;->setSelected(Z)V

    .line 653784
    if-eqz v0, :cond_0

    .line 653785
    invoke-virtual {p0, p1}, LX/3vu;->a(I)V

    .line 653786
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 653787
    goto :goto_1

    .line 653788
    :cond_2
    iget-object v0, p0, LX/3vu;->h:LX/3w4;

    if-eqz v0, :cond_3

    if-ltz p1, :cond_3

    .line 653789
    iget-object v0, p0, LX/3vu;->h:LX/3w4;

    invoke-virtual {v0, p1}, LX/3vM;->setSelection(I)V

    .line 653790
    :cond_3
    return-void
.end method
