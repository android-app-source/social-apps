.class public LX/3iR;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/3iR;


# instance fields
.field private final a:LX/0Zb;

.field private final b:LX/1BA;

.field private final c:LX/17V;

.field private final d:LX/0SG;


# direct methods
.method public constructor <init>(LX/0Zb;LX/1BA;LX/17V;LX/0SG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 629474
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 629475
    iput-object p1, p0, LX/3iR;->a:LX/0Zb;

    .line 629476
    iput-object p2, p0, LX/3iR;->b:LX/1BA;

    .line 629477
    iput-object p3, p0, LX/3iR;->c:LX/17V;

    .line 629478
    iput-object p4, p0, LX/3iR;->d:LX/0SG;

    .line 629479
    return-void
.end method

.method public static a(LX/0QB;)LX/3iR;
    .locals 7

    .prologue
    .line 629461
    sget-object v0, LX/3iR;->e:LX/3iR;

    if-nez v0, :cond_1

    .line 629462
    const-class v1, LX/3iR;

    monitor-enter v1

    .line 629463
    :try_start_0
    sget-object v0, LX/3iR;->e:LX/3iR;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 629464
    if-eqz v2, :cond_0

    .line 629465
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 629466
    new-instance p0, LX/3iR;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/1B6;->a(LX/0QB;)LX/1BA;

    move-result-object v4

    check-cast v4, LX/1BA;

    invoke-static {v0}, LX/17V;->b(LX/0QB;)LX/17V;

    move-result-object v5

    check-cast v5, LX/17V;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v6

    check-cast v6, LX/0SG;

    invoke-direct {p0, v3, v4, v5, v6}, LX/3iR;-><init>(LX/0Zb;LX/1BA;LX/17V;LX/0SG;)V

    .line 629467
    move-object v0, p0

    .line 629468
    sput-object v0, LX/3iR;->e:LX/3iR;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 629469
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 629470
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 629471
    :cond_1
    sget-object v0, LX/3iR;->e:LX/3iR;

    return-object v0

    .line 629472
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 629473
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/3iR;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)V
    .locals 4

    .prologue
    .line 629449
    iget-object v0, p0, LX/3iR;->a:LX/0Zb;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLFeedback;->s_()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    .line 629450
    if-eqz p3, :cond_0

    .line 629451
    iget-object v3, p3, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->c:Ljava/lang/String;

    move-object v3, v3

    .line 629452
    if-nez v3, :cond_1

    :cond_0
    const-string v3, "unknown"

    :goto_0
    move-object v3, v3

    .line 629453
    invoke-static {p1, v1, v2, v3}, LX/17V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "tracking"

    .line 629454
    if-nez p3, :cond_2

    const/4 v3, 0x0

    :goto_1
    move-object v3, v3

    .line 629455
    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 629456
    return-void

    .line 629457
    :cond_1
    iget-object v3, p3, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->c:Ljava/lang/String;

    move-object v3, v3

    .line 629458
    goto :goto_0

    .line 629459
    :cond_2
    iget-object v3, p3, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->a:LX/162;

    move-object v3, v3

    .line 629460
    goto :goto_1
.end method

.method public static b(LX/3iR;Ljava/lang/String;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)LX/0oG;
    .locals 3

    .prologue
    .line 629426
    iget-object v0, p0, LX/3iR;->a:LX/0Zb;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 629427
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    if-eqz p2, :cond_2

    .line 629428
    const-string v1, "display_type"

    .line 629429
    iget-object v2, p2, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->e:LX/21B;

    move-object v2, v2

    .line 629430
    invoke-virtual {v2}, LX/21B;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 629431
    const-string v1, "tracking"

    .line 629432
    iget-object v2, p2, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->a:LX/162;

    move-object v2, v2

    .line 629433
    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;LX/0lF;)LX/0oG;

    .line 629434
    iget-object v1, p2, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->c:Ljava/lang/String;

    move-object v1, v1

    .line 629435
    if-eqz v1, :cond_0

    .line 629436
    iget-object v1, p2, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->c:Ljava/lang/String;

    move-object v1, v1

    .line 629437
    invoke-virtual {v0, v1}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 629438
    :cond_0
    iget-object v1, p2, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->f:LX/21C;

    move-object v1, v1

    .line 629439
    if-eqz v1, :cond_1

    .line 629440
    const-string v1, "notification_source"

    .line 629441
    iget-object v2, p2, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->f:LX/21C;

    move-object v2, v2

    .line 629442
    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;

    .line 629443
    :cond_1
    iget-object v1, p2, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->d:Ljava/lang/String;

    move-object v1, v1

    .line 629444
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 629445
    const-string v1, "feedback_referrer"

    .line 629446
    iget-object v2, p2, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->d:Ljava/lang/String;

    move-object v2, v2

    .line 629447
    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 629448
    :cond_2
    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 629422
    iget-object v0, p0, LX/3iR;->a:LX/0Zb;

    const-string v1, "live_comment_received"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 629423
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 629424
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 629425
    :cond_0
    return-void
.end method

.method public final a(LX/6W6;Ljava/lang/String;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)V
    .locals 2

    .prologue
    .line 629414
    sget-object v0, LX/6W6;->REQUEST:LX/6W6;

    if-ne v0, p1, :cond_0

    .line 629415
    iget-object v0, p0, LX/3iR;->b:LX/1BA;

    const v1, 0x710002

    invoke-virtual {v0, v1}, LX/1BA;->a(I)V

    .line 629416
    :cond_0
    const-string v0, "comment_post_request"

    invoke-static {p0, v0, p3}, LX/3iR;->b(LX/3iR;Ljava/lang/String;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)LX/0oG;

    move-result-object v0

    .line 629417
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 629418
    const-string v1, "comment_text"

    invoke-virtual {v0, v1, p2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 629419
    const-string v1, "comment_post_type"

    invoke-virtual {v0, v1, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;

    .line 629420
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 629421
    :cond_1
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;ZZ)V
    .locals 6

    .prologue
    .line 629407
    const-string v0, "cached_feedback_count_stale"

    invoke-static {p0, v0, p2}, LX/3iR;->b(LX/3iR;Ljava/lang/String;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)LX/0oG;

    move-result-object v0

    .line 629408
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 629409
    const-string v1, "comment_counts_stale"

    invoke-virtual {v0, v1, p3}, LX/0oG;->a(Ljava/lang/String;Z)LX/0oG;

    .line 629410
    const-string v1, "like_counts_stale"

    invoke-virtual {v0, v1, p4}, LX/0oG;->a(Ljava/lang/String;Z)LX/0oG;

    .line 629411
    const-string v1, "cached_feedback_age"

    iget-object v2, p0, LX/3iR;->d:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->x()J

    move-result-wide v4

    sub-long/2addr v2, v4

    invoke-virtual {v0, v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 629412
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 629413
    :cond_0
    return-void
.end method

.method public final b(Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)V
    .locals 3

    .prologue
    .line 629400
    const-string v0, "comment_ufi_opened"

    invoke-static {p0, v0, p1}, LX/3iR;->b(LX/3iR;Ljava/lang/String;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)LX/0oG;

    move-result-object v0

    .line 629401
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    .line 629402
    const-string v1, "nectar_module"

    .line 629403
    iget-object v2, p1, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->b:Ljava/lang/String;

    move-object v2, v2

    .line 629404
    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 629405
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 629406
    :cond_0
    return-void
.end method
