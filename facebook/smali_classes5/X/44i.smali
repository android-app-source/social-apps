.class public LX/44i;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "LX/0TD;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/0TD;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 669882
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/0TD;
    .locals 6

    .prologue
    .line 669883
    sget-object v0, LX/44i;->a:LX/0TD;

    if-nez v0, :cond_1

    .line 669884
    const-class v1, LX/44i;

    monitor-enter v1

    .line 669885
    :try_start_0
    sget-object v0, LX/44i;->a:LX/0TD;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 669886
    if-eqz v2, :cond_0

    .line 669887
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 669888
    invoke-static {v0}, LX/0TJ;->a(LX/0QB;)LX/0TJ;

    move-result-object v3

    check-cast v3, LX/0TJ;

    invoke-static {v0}, LX/0Si;->a(LX/0QB;)LX/0Si;

    move-result-object v4

    check-cast v4, LX/0Sj;

    const/16 v5, 0x1a1

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/0TR;->a(LX/0QB;)LX/0TR;

    move-result-object v5

    check-cast v5, LX/0TR;

    invoke-static {v3, v4, p0, v5}, LX/0Su;->p(LX/0TJ;LX/0Sj;LX/0Or;LX/0TR;)LX/0TD;

    move-result-object v3

    move-object v0, v3

    .line 669889
    sput-object v0, LX/44i;->a:LX/0TD;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 669890
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 669891
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 669892
    :cond_1
    sget-object v0, LX/44i;->a:LX/0TD;

    return-object v0

    .line 669893
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 669894
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 669895
    invoke-static {p0}, LX/0TJ;->a(LX/0QB;)LX/0TJ;

    move-result-object v0

    check-cast v0, LX/0TJ;

    invoke-static {p0}, LX/0Si;->a(LX/0QB;)LX/0Si;

    move-result-object v1

    check-cast v1, LX/0Sj;

    const/16 v2, 0x1a1

    invoke-static {p0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static {p0}, LX/0TR;->a(LX/0QB;)LX/0TR;

    move-result-object v2

    check-cast v2, LX/0TR;

    invoke-static {v0, v1, v3, v2}, LX/0Su;->p(LX/0TJ;LX/0Sj;LX/0Or;LX/0TR;)LX/0TD;

    move-result-object v0

    return-object v0
.end method
