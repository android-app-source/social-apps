.class public LX/4bI;
.super Ljava/io/InputStream;
.source ""


# instance fields
.field private final a:LX/0So;

.field private final b:Ljava/io/InputStream;

.field public c:J
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private d:J
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0So;Ljava/io/InputStream;)V
    .locals 0
    .param p2    # Ljava/io/InputStream;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 793613
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 793614
    iput-object p1, p0, LX/4bI;->a:LX/0So;

    .line 793615
    iput-object p2, p0, LX/4bI;->b:Ljava/io/InputStream;

    .line 793616
    return-void
.end method

.method private declared-synchronized b()V
    .locals 4

    .prologue
    .line 793609
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, LX/4bI;->d:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 793610
    iget-object v0, p0, LX/4bI;->a:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/4bI;->d:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 793611
    monitor-exit p0

    return-void

    .line 793612
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized c()V
    .locals 6

    .prologue
    const-wide/16 v2, 0x0

    .line 793604
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, LX/4bI;->d:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 793605
    iget-wide v0, p0, LX/4bI;->c:J

    iget-object v2, p0, LX/4bI;->a:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    iget-wide v4, p0, LX/4bI;->d:J

    sub-long/2addr v2, v4

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/4bI;->c:J

    .line 793606
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/4bI;->d:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 793607
    monitor-exit p0

    return-void

    .line 793608
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized available()I
    .locals 1

    .prologue
    .line 793600
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LX/4bI;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 793601
    :try_start_1
    iget-object v0, p0, LX/4bI;->b:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->available()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    .line 793602
    :try_start_2
    invoke-direct {p0}, LX/4bI;->c()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    :try_start_3
    invoke-direct {p0}, LX/4bI;->c()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 793603
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 793598
    iget-object v0, p0, LX/4bI;->b:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 793599
    return-void
.end method

.method public final declared-synchronized mark(I)V
    .locals 1

    .prologue
    .line 793617
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LX/4bI;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 793618
    :try_start_1
    iget-object v0, p0, LX/4bI;->b:Ljava/io/InputStream;

    invoke-virtual {v0, p1}, Ljava/io/InputStream;->mark(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 793619
    :try_start_2
    invoke-direct {p0}, LX/4bI;->c()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 793620
    monitor-exit p0

    return-void

    .line 793621
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-direct {p0}, LX/4bI;->c()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 793622
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized markSupported()Z
    .locals 1

    .prologue
    .line 793594
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LX/4bI;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 793595
    :try_start_1
    iget-object v0, p0, LX/4bI;->b:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->markSupported()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    .line 793596
    :try_start_2
    invoke-direct {p0}, LX/4bI;->c()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    :try_start_3
    invoke-direct {p0}, LX/4bI;->c()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 793597
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized read()I
    .locals 1

    .prologue
    .line 793590
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LX/4bI;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 793591
    :try_start_1
    iget-object v0, p0, LX/4bI;->b:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->read()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    .line 793592
    :try_start_2
    invoke-direct {p0}, LX/4bI;->c()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    :try_start_3
    invoke-direct {p0}, LX/4bI;->c()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 793593
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized read([B)I
    .locals 1

    .prologue
    .line 793586
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LX/4bI;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 793587
    :try_start_1
    iget-object v0, p0, LX/4bI;->b:Ljava/io/InputStream;

    invoke-virtual {v0, p1}, Ljava/io/InputStream;->read([B)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    .line 793588
    :try_start_2
    invoke-direct {p0}, LX/4bI;->c()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    :try_start_3
    invoke-direct {p0}, LX/4bI;->c()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 793589
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized read([BII)I
    .locals 1

    .prologue
    .line 793572
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LX/4bI;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 793573
    :try_start_1
    iget-object v0, p0, LX/4bI;->b:Ljava/io/InputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/InputStream;->read([BII)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    .line 793574
    :try_start_2
    invoke-direct {p0}, LX/4bI;->c()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    :try_start_3
    invoke-direct {p0}, LX/4bI;->c()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 793575
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized reset()V
    .locals 1

    .prologue
    .line 793580
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LX/4bI;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 793581
    :try_start_1
    iget-object v0, p0, LX/4bI;->b:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->reset()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 793582
    :try_start_2
    invoke-direct {p0}, LX/4bI;->c()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 793583
    monitor-exit p0

    return-void

    .line 793584
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-direct {p0}, LX/4bI;->c()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 793585
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized skip(J)J
    .locals 3

    .prologue
    .line 793576
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LX/4bI;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 793577
    :try_start_1
    iget-object v0, p0, LX/4bI;->b:Ljava/io/InputStream;

    invoke-virtual {v0, p1, p2}, Ljava/io/InputStream;->skip(J)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v0

    .line 793578
    :try_start_2
    invoke-direct {p0}, LX/4bI;->c()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    :try_start_3
    invoke-direct {p0}, LX/4bI;->c()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 793579
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method
