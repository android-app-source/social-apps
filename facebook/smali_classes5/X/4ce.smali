.class public final enum LX/4ce;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/4ce;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/4ce;

.field public static final enum DISABLED:LX/4ce;

.field public static final enum DISABLED_ERROR:LX/4ce;

.field public static final enum ENABLED:LX/4ce;

.field public static final enum REQUESTED:LX/4ce;

.field public static final enum STARTING:LX/4ce;


# instance fields
.field public resId:I


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 795266
    new-instance v0, LX/4ce;

    const-string v1, "DISABLED"

    const v2, 0x7f08007b

    invoke-direct {v0, v1, v3, v2}, LX/4ce;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/4ce;->DISABLED:LX/4ce;

    .line 795267
    new-instance v0, LX/4ce;

    const-string v1, "REQUESTED"

    const v2, 0x7f08007e

    invoke-direct {v0, v1, v4, v2}, LX/4ce;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/4ce;->REQUESTED:LX/4ce;

    .line 795268
    new-instance v0, LX/4ce;

    const-string v1, "STARTING"

    const v2, 0x7f08007c

    invoke-direct {v0, v1, v5, v2}, LX/4ce;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/4ce;->STARTING:LX/4ce;

    .line 795269
    new-instance v0, LX/4ce;

    const-string v1, "ENABLED"

    const v2, 0x7f08007d

    invoke-direct {v0, v1, v6, v2}, LX/4ce;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/4ce;->ENABLED:LX/4ce;

    .line 795270
    new-instance v0, LX/4ce;

    const-string v1, "DISABLED_ERROR"

    const v2, 0x7f08007b

    invoke-direct {v0, v1, v7, v2}, LX/4ce;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/4ce;->DISABLED_ERROR:LX/4ce;

    .line 795271
    const/4 v0, 0x5

    new-array v0, v0, [LX/4ce;

    sget-object v1, LX/4ce;->DISABLED:LX/4ce;

    aput-object v1, v0, v3

    sget-object v1, LX/4ce;->REQUESTED:LX/4ce;

    aput-object v1, v0, v4

    sget-object v1, LX/4ce;->STARTING:LX/4ce;

    aput-object v1, v0, v5

    sget-object v1, LX/4ce;->ENABLED:LX/4ce;

    aput-object v1, v0, v6

    sget-object v1, LX/4ce;->DISABLED_ERROR:LX/4ce;

    aput-object v1, v0, v7

    sput-object v0, LX/4ce;->$VALUES:[LX/4ce;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 795272
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 795273
    iput p3, p0, LX/4ce;->resId:I

    .line 795274
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/4ce;
    .locals 1

    .prologue
    .line 795275
    const-class v0, LX/4ce;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/4ce;

    return-object v0
.end method

.method public static values()[LX/4ce;
    .locals 1

    .prologue
    .line 795276
    sget-object v0, LX/4ce;->$VALUES:[LX/4ce;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/4ce;

    return-object v0
.end method


# virtual methods
.method public final isComplete()Z
    .locals 1

    .prologue
    .line 795277
    sget-object v0, LX/4ce;->DISABLED:LX/4ce;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/4ce;->DISABLED_ERROR:LX/4ce;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/4ce;->ENABLED:LX/4ce;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isEnabled()Z
    .locals 1

    .prologue
    .line 795278
    sget-object v0, LX/4ce;->ENABLED:LX/4ce;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isStarting()Z
    .locals 1

    .prologue
    .line 795279
    sget-object v0, LX/4ce;->REQUESTED:LX/4ce;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/4ce;->STARTING:LX/4ce;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
