.class public final LX/4ao;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 792911
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 792912
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 792913
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 792914
    invoke-static {p0, p1}, LX/4ao;->b(LX/15w;LX/186;)I

    move-result v1

    .line 792915
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 792916
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 792917
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 792918
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 792919
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2, p3}, LX/4ao;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 792920
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 792921
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 792922
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 792923
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_7

    .line 792924
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 792925
    :goto_0
    return v1

    .line 792926
    :cond_0
    const-string v8, "length"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 792927
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v3

    move v5, v3

    move v3, v2

    .line 792928
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_4

    .line 792929
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 792930
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 792931
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_1

    if-eqz v7, :cond_1

    .line 792932
    const-string v8, "entity"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 792933
    invoke-static {p0, p1}, LX/4am;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 792934
    :cond_2
    const-string v8, "offset"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 792935
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v4, v0

    move v0, v2

    goto :goto_1

    .line 792936
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 792937
    :cond_4
    const/4 v7, 0x3

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 792938
    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 792939
    if-eqz v3, :cond_5

    .line 792940
    invoke-virtual {p1, v2, v5, v1}, LX/186;->a(III)V

    .line 792941
    :cond_5
    if-eqz v0, :cond_6

    .line 792942
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v4, v1}, LX/186;->a(III)V

    .line 792943
    :cond_6
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_7
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    goto :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 792944
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 792945
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 792946
    if-eqz v0, :cond_0

    .line 792947
    const-string v1, "entity"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 792948
    invoke-static {p0, v0, p2}, LX/4am;->a(LX/15i;ILX/0nX;)V

    .line 792949
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 792950
    if-eqz v0, :cond_1

    .line 792951
    const-string v1, "length"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 792952
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 792953
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 792954
    if-eqz v0, :cond_2

    .line 792955
    const-string v1, "offset"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 792956
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 792957
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 792958
    return-void
.end method
