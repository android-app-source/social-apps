.class public final LX/4iR;
.super Ljava/io/FilterOutputStream;
.source ""


# direct methods
.method public constructor <init>(Ljava/io/OutputStream;)V
    .locals 0

    .prologue
    .line 803130
    invoke-direct {p0, p1}, Ljava/io/FilterOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 803131
    return-void
.end method


# virtual methods
.method public close()V
    .locals 0

    .prologue
    .line 803140
    return-void
.end method

.method public reallyClose()V
    .locals 1

    .prologue
    .line 803136
    iget-object v0, p0, Ljava/io/FilterOutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    .line 803137
    return-void
.end method

.method public write(I)V
    .locals 1

    .prologue
    .line 803138
    iget-object v0, p0, Ljava/io/FilterOutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write(I)V

    .line 803139
    return-void
.end method

.method public write([B)V
    .locals 1

    .prologue
    .line 803134
    iget-object v0, p0, Ljava/io/FilterOutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write([B)V

    .line 803135
    return-void
.end method

.method public write([BII)V
    .locals 1

    .prologue
    .line 803132
    iget-object v0, p0, Ljava/io/FilterOutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V

    .line 803133
    return-void
.end method
