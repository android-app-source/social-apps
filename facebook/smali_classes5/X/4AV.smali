.class public LX/4AV;
.super Landroid/graphics/drawable/BitmapDrawable;
.source ""

# interfaces
.implements LX/1oM;
.implements LX/1aj;


# instance fields
.field public final a:[F
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field

.field public final b:Landroid/graphics/RectF;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field

.field public final c:Landroid/graphics/RectF;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field

.field public final d:Landroid/graphics/RectF;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field

.field public final e:Landroid/graphics/RectF;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field

.field public final f:Landroid/graphics/Matrix;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field

.field public final g:Landroid/graphics/Matrix;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field

.field public final h:Landroid/graphics/Matrix;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field

.field public final i:Landroid/graphics/Matrix;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field

.field public final j:Landroid/graphics/Matrix;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field

.field public final k:Landroid/graphics/Matrix;
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation
.end field

.field public l:Z

.field public m:Z

.field public final n:[F

.field public o:F

.field private p:I

.field public q:F

.field public final r:Landroid/graphics/Path;

.field public final s:Landroid/graphics/Path;

.field public t:Z

.field public final u:Landroid/graphics/Paint;

.field private final v:Landroid/graphics/Paint;

.field public w:Z

.field public x:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field public y:LX/1ak;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;Landroid/graphics/Paint;)V
    .locals 5
    .param p3    # Landroid/graphics/Paint;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 676437
    invoke-direct {p0, p1, p2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 676438
    iput-boolean v2, p0, LX/4AV;->l:Z

    .line 676439
    iput-boolean v2, p0, LX/4AV;->m:Z

    .line 676440
    new-array v0, v4, [F

    iput-object v0, p0, LX/4AV;->n:[F

    .line 676441
    new-array v0, v4, [F

    iput-object v0, p0, LX/4AV;->a:[F

    .line 676442
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, LX/4AV;->b:Landroid/graphics/RectF;

    .line 676443
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, LX/4AV;->c:Landroid/graphics/RectF;

    .line 676444
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, LX/4AV;->d:Landroid/graphics/RectF;

    .line 676445
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, LX/4AV;->e:Landroid/graphics/RectF;

    .line 676446
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, LX/4AV;->f:Landroid/graphics/Matrix;

    .line 676447
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, LX/4AV;->g:Landroid/graphics/Matrix;

    .line 676448
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, LX/4AV;->h:Landroid/graphics/Matrix;

    .line 676449
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, LX/4AV;->i:Landroid/graphics/Matrix;

    .line 676450
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, LX/4AV;->j:Landroid/graphics/Matrix;

    .line 676451
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, LX/4AV;->k:Landroid/graphics/Matrix;

    .line 676452
    iput v3, p0, LX/4AV;->o:F

    .line 676453
    iput v2, p0, LX/4AV;->p:I

    .line 676454
    iput v3, p0, LX/4AV;->q:F

    .line 676455
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, LX/4AV;->r:Landroid/graphics/Path;

    .line 676456
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, LX/4AV;->s:Landroid/graphics/Path;

    .line 676457
    iput-boolean v1, p0, LX/4AV;->t:Z

    .line 676458
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, LX/4AV;->u:Landroid/graphics/Paint;

    .line 676459
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, LX/4AV;->v:Landroid/graphics/Paint;

    .line 676460
    iput-boolean v1, p0, LX/4AV;->w:Z

    .line 676461
    if-eqz p3, :cond_0

    .line 676462
    iget-object v0, p0, LX/4AV;->u:Landroid/graphics/Paint;

    invoke-virtual {v0, p3}, Landroid/graphics/Paint;->set(Landroid/graphics/Paint;)V

    .line 676463
    :cond_0
    iget-object v0, p0, LX/4AV;->u:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setFlags(I)V

    .line 676464
    iget-object v0, p0, LX/4AV;->v:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 676465
    return-void
.end method


# virtual methods
.method public final a(F)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v1, 0x1

    .line 676430
    cmpl-float v0, p1, v3

    if-ltz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/03g;->b(Z)V

    .line 676431
    iget-object v0, p0, LX/4AV;->n:[F

    invoke-static {v0, p1}, Ljava/util/Arrays;->fill([FF)V

    .line 676432
    cmpl-float v0, p1, v3

    if-eqz v0, :cond_0

    move v2, v1

    :cond_0
    iput-boolean v2, p0, LX/4AV;->m:Z

    .line 676433
    iput-boolean v1, p0, LX/4AV;->t:Z

    .line 676434
    invoke-virtual {p0}, LX/4AV;->invalidateSelf()V

    .line 676435
    return-void

    :cond_1
    move v0, v2

    .line 676436
    goto :goto_0
.end method

.method public final a(IF)V
    .locals 1

    .prologue
    .line 676424
    iget v0, p0, LX/4AV;->p:I

    if-ne v0, p1, :cond_0

    iget v0, p0, LX/4AV;->o:F

    cmpl-float v0, v0, p2

    if-eqz v0, :cond_1

    .line 676425
    :cond_0
    iput p1, p0, LX/4AV;->p:I

    .line 676426
    iput p2, p0, LX/4AV;->o:F

    .line 676427
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4AV;->t:Z

    .line 676428
    invoke-virtual {p0}, LX/4AV;->invalidateSelf()V

    .line 676429
    :cond_1
    return-void
.end method

.method public final a(LX/1ak;)V
    .locals 0
    .param p1    # LX/1ak;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 676422
    iput-object p1, p0, LX/4AV;->y:LX/1ak;

    .line 676423
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 676418
    iput-boolean p1, p0, LX/4AV;->l:Z

    .line 676419
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4AV;->t:Z

    .line 676420
    invoke-virtual {p0}, LX/4AV;->invalidateSelf()V

    .line 676421
    return-void
.end method

.method public final a([F)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/16 v5, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 676404
    if-nez p1, :cond_1

    .line 676405
    iget-object v0, p0, LX/4AV;->n:[F

    invoke-static {v0, v6}, Ljava/util/Arrays;->fill([FF)V

    .line 676406
    iput-boolean v2, p0, LX/4AV;->m:Z

    .line 676407
    :cond_0
    iput-boolean v1, p0, LX/4AV;->t:Z

    .line 676408
    invoke-virtual {p0}, LX/4AV;->invalidateSelf()V

    .line 676409
    return-void

    .line 676410
    :cond_1
    array-length v0, p1

    if-ne v0, v5, :cond_2

    move v0, v1

    :goto_0
    const-string v3, "radii should have exactly 8 values"

    invoke-static {v0, v3}, LX/03g;->a(ZLjava/lang/Object;)V

    .line 676411
    iget-object v0, p0, LX/4AV;->n:[F

    invoke-static {p1, v2, v0, v2, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 676412
    iput-boolean v2, p0, LX/4AV;->m:Z

    move v3, v2

    .line 676413
    :goto_1
    if-ge v3, v5, :cond_0

    .line 676414
    iget-boolean v4, p0, LX/4AV;->m:Z

    aget v0, p1, v3

    cmpl-float v0, v0, v6

    if-lez v0, :cond_3

    move v0, v1

    :goto_2
    or-int/2addr v0, v4

    iput-boolean v0, p0, LX/4AV;->m:Z

    .line 676415
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_2
    move v0, v2

    .line 676416
    goto :goto_0

    :cond_3
    move v0, v2

    .line 676417
    goto :goto_2
.end method

.method public final b(F)V
    .locals 1

    .prologue
    .line 676399
    iget v0, p0, LX/4AV;->q:F

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_0

    .line 676400
    iput p1, p0, LX/4AV;->q:F

    .line 676401
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4AV;->t:Z

    .line 676402
    invoke-virtual {p0}, LX/4AV;->invalidateSelf()V

    .line 676403
    :cond_0
    return-void
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    .line 676332
    iget-boolean v0, p0, LX/4AV;->l:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/4AV;->m:Z

    if-nez v0, :cond_0

    iget v0, p0, LX/4AV;->o:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_a

    :cond_0
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 676333
    if-nez v0, :cond_1

    .line 676334
    invoke-super {p0, p1}, Landroid/graphics/drawable/BitmapDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 676335
    :goto_1
    return-void

    .line 676336
    :cond_1
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 676337
    iget-object v0, p0, LX/4AV;->y:LX/1ak;

    if-eqz v0, :cond_b

    .line 676338
    iget-object v0, p0, LX/4AV;->y:LX/1ak;

    iget-object v1, p0, LX/4AV;->h:Landroid/graphics/Matrix;

    invoke-interface {v0, v1}, LX/1ak;->a(Landroid/graphics/Matrix;)V

    .line 676339
    iget-object v0, p0, LX/4AV;->y:LX/1ak;

    iget-object v1, p0, LX/4AV;->b:Landroid/graphics/RectF;

    invoke-interface {v0, v1}, LX/1ak;->a(Landroid/graphics/RectF;)V

    .line 676340
    :goto_2
    iget-object v0, p0, LX/4AV;->d:Landroid/graphics/RectF;

    invoke-virtual {p0}, LX/4AV;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0}, LX/4AV;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/RectF;->set(FFFF)V

    .line 676341
    iget-object v0, p0, LX/4AV;->e:Landroid/graphics/RectF;

    invoke-virtual {p0}, LX/4AV;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 676342
    iget-object v0, p0, LX/4AV;->f:Landroid/graphics/Matrix;

    iget-object v1, p0, LX/4AV;->d:Landroid/graphics/RectF;

    iget-object v2, p0, LX/4AV;->e:Landroid/graphics/RectF;

    sget-object v3, Landroid/graphics/Matrix$ScaleToFit;->FILL:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    .line 676343
    iget-object v0, p0, LX/4AV;->h:Landroid/graphics/Matrix;

    iget-object v1, p0, LX/4AV;->i:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/4AV;->f:Landroid/graphics/Matrix;

    iget-object v1, p0, LX/4AV;->g:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 676344
    :cond_2
    iput-boolean v4, p0, LX/4AV;->w:Z

    .line 676345
    iget-object v0, p0, LX/4AV;->h:Landroid/graphics/Matrix;

    iget-object v1, p0, LX/4AV;->j:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 676346
    iget-object v0, p0, LX/4AV;->k:Landroid/graphics/Matrix;

    iget-object v1, p0, LX/4AV;->h:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 676347
    iget-object v0, p0, LX/4AV;->k:Landroid/graphics/Matrix;

    iget-object v1, p0, LX/4AV;->f:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->preConcat(Landroid/graphics/Matrix;)Z

    .line 676348
    iget-object v0, p0, LX/4AV;->i:Landroid/graphics/Matrix;

    iget-object v1, p0, LX/4AV;->h:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 676349
    iget-object v0, p0, LX/4AV;->g:Landroid/graphics/Matrix;

    iget-object v1, p0, LX/4AV;->f:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 676350
    :cond_3
    iget-object v0, p0, LX/4AV;->b:Landroid/graphics/RectF;

    iget-object v1, p0, LX/4AV;->c:Landroid/graphics/RectF;

    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 676351
    iput-boolean v4, p0, LX/4AV;->t:Z

    .line 676352
    iget-object v0, p0, LX/4AV;->c:Landroid/graphics/RectF;

    iget-object v1, p0, LX/4AV;->b:Landroid/graphics/RectF;

    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 676353
    :cond_4
    const/4 v1, 0x0

    const/high16 v6, 0x40000000    # 2.0f

    .line 676354
    iget-boolean v0, p0, LX/4AV;->t:Z

    if-eqz v0, :cond_5

    .line 676355
    iget-object v0, p0, LX/4AV;->s:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 676356
    iget-object v0, p0, LX/4AV;->b:Landroid/graphics/RectF;

    iget v2, p0, LX/4AV;->o:F

    div-float/2addr v2, v6

    iget v3, p0, LX/4AV;->o:F

    div-float/2addr v3, v6

    invoke-virtual {v0, v2, v3}, Landroid/graphics/RectF;->inset(FF)V

    .line 676357
    iget-boolean v0, p0, LX/4AV;->l:Z

    if-eqz v0, :cond_c

    .line 676358
    iget-object v0, p0, LX/4AV;->b:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    iget-object v2, p0, LX/4AV;->b:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->min(FF)F

    move-result v0

    div-float/2addr v0, v6

    .line 676359
    iget-object v2, p0, LX/4AV;->s:Landroid/graphics/Path;

    iget-object v3, p0, LX/4AV;->b:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->centerX()F

    move-result v3

    iget-object v4, p0, LX/4AV;->b:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->centerY()F

    move-result v4

    sget-object v5, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v2, v3, v4, v0, v5}, Landroid/graphics/Path;->addCircle(FFFLandroid/graphics/Path$Direction;)V

    .line 676360
    :goto_3
    iget-object v0, p0, LX/4AV;->b:Landroid/graphics/RectF;

    iget v2, p0, LX/4AV;->o:F

    neg-float v2, v2

    div-float/2addr v2, v6

    iget v3, p0, LX/4AV;->o:F

    neg-float v3, v3

    div-float/2addr v3, v6

    invoke-virtual {v0, v2, v3}, Landroid/graphics/RectF;->inset(FF)V

    .line 676361
    iget-object v0, p0, LX/4AV;->r:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 676362
    iget-object v0, p0, LX/4AV;->b:Landroid/graphics/RectF;

    iget v2, p0, LX/4AV;->q:F

    iget v3, p0, LX/4AV;->q:F

    invoke-virtual {v0, v2, v3}, Landroid/graphics/RectF;->inset(FF)V

    .line 676363
    iget-boolean v0, p0, LX/4AV;->l:Z

    if-eqz v0, :cond_e

    .line 676364
    iget-object v0, p0, LX/4AV;->r:Landroid/graphics/Path;

    iget-object v2, p0, LX/4AV;->b:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->centerX()F

    move-result v2

    iget-object v3, p0, LX/4AV;->b:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->centerY()F

    move-result v3

    iget-object v4, p0, LX/4AV;->b:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v4

    iget-object v5, p0, LX/4AV;->b:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->min(FF)F

    move-result v4

    div-float/2addr v4, v6

    sget-object v5, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Path;->addCircle(FFFLandroid/graphics/Path$Direction;)V

    .line 676365
    :goto_4
    iget-object v0, p0, LX/4AV;->b:Landroid/graphics/RectF;

    iget v2, p0, LX/4AV;->q:F

    neg-float v2, v2

    iget v3, p0, LX/4AV;->q:F

    neg-float v3, v3

    invoke-virtual {v0, v2, v3}, Landroid/graphics/RectF;->inset(FF)V

    .line 676366
    iget-object v0, p0, LX/4AV;->r:Landroid/graphics/Path;

    sget-object v2, Landroid/graphics/Path$FillType;->WINDING:Landroid/graphics/Path$FillType;

    invoke-virtual {v0, v2}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    .line 676367
    iput-boolean v1, p0, LX/4AV;->t:Z

    .line 676368
    :cond_5
    invoke-virtual {p0}, LX/4AV;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 676369
    iget-object v1, p0, LX/4AV;->x:Ljava/lang/ref/WeakReference;

    if-eqz v1, :cond_6

    iget-object v1, p0, LX/4AV;->x:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    if-eq v1, v0, :cond_7

    .line 676370
    :cond_6
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, p0, LX/4AV;->x:Ljava/lang/ref/WeakReference;

    .line 676371
    iget-object v1, p0, LX/4AV;->u:Landroid/graphics/Paint;

    new-instance v2, Landroid/graphics/BitmapShader;

    sget-object v3, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    sget-object v4, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    invoke-direct {v2, v0, v3, v4}, Landroid/graphics/BitmapShader;-><init>(Landroid/graphics/Bitmap;Landroid/graphics/Shader$TileMode;Landroid/graphics/Shader$TileMode;)V

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 676372
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4AV;->w:Z

    .line 676373
    :cond_7
    iget-boolean v0, p0, LX/4AV;->w:Z

    if-eqz v0, :cond_8

    .line 676374
    iget-object v0, p0, LX/4AV;->u:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getShader()Landroid/graphics/Shader;

    move-result-object v0

    iget-object v1, p0, LX/4AV;->k:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Shader;->setLocalMatrix(Landroid/graphics/Matrix;)V

    .line 676375
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/4AV;->w:Z

    .line 676376
    :cond_8
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v0

    .line 676377
    iget-object v1, p0, LX/4AV;->j:Landroid/graphics/Matrix;

    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    .line 676378
    iget-object v1, p0, LX/4AV;->r:Landroid/graphics/Path;

    iget-object v2, p0, LX/4AV;->u:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 676379
    iget v1, p0, LX/4AV;->o:F

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-lez v1, :cond_9

    .line 676380
    iget-object v1, p0, LX/4AV;->v:Landroid/graphics/Paint;

    iget v2, p0, LX/4AV;->o:F

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 676381
    iget-object v1, p0, LX/4AV;->v:Landroid/graphics/Paint;

    iget v2, p0, LX/4AV;->p:I

    iget-object v3, p0, LX/4AV;->u:Landroid/graphics/Paint;

    invoke-virtual {v3}, Landroid/graphics/Paint;->getAlpha()I

    move-result v3

    invoke-static {v2, v3}, LX/1am;->a(II)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 676382
    iget-object v1, p0, LX/4AV;->s:Landroid/graphics/Path;

    iget-object v2, p0, LX/4AV;->v:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 676383
    :cond_9
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    goto/16 :goto_1

    :cond_a
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 676384
    :cond_b
    iget-object v0, p0, LX/4AV;->h:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    .line 676385
    iget-object v0, p0, LX/4AV;->b:Landroid/graphics/RectF;

    invoke-virtual {p0}, LX/4AV;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    goto/16 :goto_2

    :cond_c
    move v0, v1

    .line 676386
    :goto_5
    iget-object v2, p0, LX/4AV;->a:[F

    array-length v2, v2

    if-ge v0, v2, :cond_d

    .line 676387
    iget-object v2, p0, LX/4AV;->a:[F

    iget-object v3, p0, LX/4AV;->n:[F

    aget v3, v3, v0

    iget v4, p0, LX/4AV;->q:F

    add-float/2addr v3, v4

    iget v4, p0, LX/4AV;->o:F

    div-float/2addr v4, v6

    sub-float/2addr v3, v4

    aput v3, v2, v0

    .line 676388
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 676389
    :cond_d
    iget-object v0, p0, LX/4AV;->s:Landroid/graphics/Path;

    iget-object v2, p0, LX/4AV;->b:Landroid/graphics/RectF;

    iget-object v3, p0, LX/4AV;->a:[F

    sget-object v4, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v0, v2, v3, v4}, Landroid/graphics/Path;->addRoundRect(Landroid/graphics/RectF;[FLandroid/graphics/Path$Direction;)V

    goto/16 :goto_3

    .line 676390
    :cond_e
    iget-object v0, p0, LX/4AV;->r:Landroid/graphics/Path;

    iget-object v2, p0, LX/4AV;->b:Landroid/graphics/RectF;

    iget-object v3, p0, LX/4AV;->n:[F

    sget-object v4, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v0, v2, v3, v4}, Landroid/graphics/Path;->addRoundRect(Landroid/graphics/RectF;[FLandroid/graphics/Path$Direction;)V

    goto/16 :goto_4
.end method

.method public final setAlpha(I)V
    .locals 1

    .prologue
    .line 676394
    iget-object v0, p0, LX/4AV;->u:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getAlpha()I

    move-result v0

    if-eq p1, v0, :cond_0

    .line 676395
    iget-object v0, p0, LX/4AV;->u:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 676396
    invoke-super {p0, p1}, Landroid/graphics/drawable/BitmapDrawable;->setAlpha(I)V

    .line 676397
    invoke-virtual {p0}, LX/4AV;->invalidateSelf()V

    .line 676398
    :cond_0
    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1

    .prologue
    .line 676391
    iget-object v0, p0, LX/4AV;->u:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 676392
    invoke-super {p0, p1}, Landroid/graphics/drawable/BitmapDrawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 676393
    return-void
.end method
