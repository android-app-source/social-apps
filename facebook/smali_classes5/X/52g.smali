.class public final enum LX/52g;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/52g;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/52g;

.field public static final enum FULL:LX/52g;

.field public static final enum NATIVE_ONLY:LX/52g;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 826219
    new-instance v0, LX/52g;

    const-string v1, "NATIVE_ONLY"

    invoke-direct {v0, v1, v2}, LX/52g;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/52g;->NATIVE_ONLY:LX/52g;

    .line 826220
    new-instance v0, LX/52g;

    const-string v1, "FULL"

    invoke-direct {v0, v1, v3}, LX/52g;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/52g;->FULL:LX/52g;

    .line 826221
    const/4 v0, 0x2

    new-array v0, v0, [LX/52g;

    sget-object v1, LX/52g;->NATIVE_ONLY:LX/52g;

    aput-object v1, v0, v2

    sget-object v1, LX/52g;->FULL:LX/52g;

    aput-object v1, v0, v3

    sput-object v0, LX/52g;->$VALUES:[LX/52g;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 826218
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/52g;
    .locals 1

    .prologue
    .line 826222
    const-class v0, LX/52g;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/52g;

    return-object v0
.end method

.method public static values()[LX/52g;
    .locals 1

    .prologue
    .line 826217
    sget-object v0, LX/52g;->$VALUES:[LX/52g;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/52g;

    return-object v0
.end method
