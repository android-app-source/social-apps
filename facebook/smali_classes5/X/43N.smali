.class public LX/43N;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/43N;


# instance fields
.field public final a:Lcom/facebook/bitmaps/NativeImageLibraries;

.field public final b:Z


# direct methods
.method public constructor <init>(Lcom/facebook/bitmaps/NativeImageLibraries;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 668926
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 668927
    iput-object p1, p0, LX/43N;->a:Lcom/facebook/bitmaps/NativeImageLibraries;

    .line 668928
    iget-object v0, p0, LX/43N;->a:Lcom/facebook/bitmaps/NativeImageLibraries;

    invoke-virtual {v0}, LX/03m;->T_()Z

    move-result v0

    iput-boolean v0, p0, LX/43N;->b:Z

    .line 668929
    return-void
.end method

.method public static a(LX/0QB;)LX/43N;
    .locals 4

    .prologue
    .line 668930
    sget-object v0, LX/43N;->c:LX/43N;

    if-nez v0, :cond_1

    .line 668931
    const-class v1, LX/43N;

    monitor-enter v1

    .line 668932
    :try_start_0
    sget-object v0, LX/43N;->c:LX/43N;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 668933
    if-eqz v2, :cond_0

    .line 668934
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 668935
    new-instance p0, LX/43N;

    invoke-static {v0}, Lcom/facebook/bitmaps/NativeImageLibraries;->a(LX/0QB;)Lcom/facebook/bitmaps/NativeImageLibraries;

    move-result-object v3

    check-cast v3, Lcom/facebook/bitmaps/NativeImageLibraries;

    invoke-direct {p0, v3}, LX/43N;-><init>(Lcom/facebook/bitmaps/NativeImageLibraries;)V

    .line 668936
    move-object v0, p0

    .line 668937
    sput-object v0, LX/43N;->c:LX/43N;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 668938
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 668939
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 668940
    :cond_1
    sget-object v0, LX/43N;->c:LX/43N;

    return-object v0

    .line 668941
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 668942
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/io/InputStream;Ljava/io/OutputStream;I)V
    .locals 2

    .prologue
    .line 668943
    invoke-static {p1}, LX/1la;->a(Ljava/io/InputStream;)LX/1lW;

    move-result-object v0

    .line 668944
    sget-object v1, LX/1ld;->f:LX/1lW;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, LX/1ld;->h:LX/1lW;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 668945
    :cond_0
    iget-boolean v0, p0, LX/43N;->b:Z

    const-string v1, "Transcode to Png invoked when isAvailable() returns false"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 668946
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 668947
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 668948
    iget-object v0, p0, LX/43N;->a:Lcom/facebook/bitmaps/NativeImageLibraries;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/bitmaps/NativeImageLibraries;->transcode2Png(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    .line 668949
    :goto_0
    return-void

    .line 668950
    :cond_1
    iget-boolean v0, p0, LX/43N;->b:Z

    const-string v1, "Transcode to Jpeg invoked when isAvailable() returns false"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 668951
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 668952
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 668953
    iget-object v0, p0, LX/43N;->a:Lcom/facebook/bitmaps/NativeImageLibraries;

    invoke-virtual {v0, p1, p2, p3}, Lcom/facebook/bitmaps/NativeImageLibraries;->transcode2Jpeg(Ljava/io/InputStream;Ljava/io/OutputStream;I)V

    .line 668954
    goto :goto_0
.end method
