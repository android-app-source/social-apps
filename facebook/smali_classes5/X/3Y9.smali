.class public LX/3Y9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1TE;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 595501
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 595502
    return-void
.end method


# virtual methods
.method public final a(LX/1KB;)V
    .locals 4

    .prologue
    .line 595503
    sget-object v0, Lcom/facebook/feedplugins/hotconversations/BaseHotConversationPartDefinition;->a:LX/1Cz;

    sget-object v1, Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithCollagePartDefinition;->a:LX/1Cz;

    sget-object v2, Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithThumbnailPartDefinition;->a:LX/1Cz;

    sget-object v3, Lcom/facebook/feedplugins/hotconversations/HotConversationLinkSharePartDefinition;->a:LX/1Cz;

    invoke-static {v0, v1, v2, v3}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 595504
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Cz;

    .line 595505
    invoke-virtual {p1, v0}, LX/1KB;->a(LX/1Cz;)V

    .line 595506
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 595507
    :cond_0
    return-void
.end method
