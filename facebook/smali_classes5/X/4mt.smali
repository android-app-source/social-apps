.class public LX/4mt;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;",
            "LX/1Mv",
            "<*>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 806354
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 806355
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/4mt;->a:Ljava/util/Map;

    .line 806356
    return-void
.end method

.method public static a(LX/0QB;)LX/4mt;
    .locals 1

    .prologue
    .line 806357
    new-instance v0, LX/4mt;

    invoke-direct {v0}, LX/4mt;-><init>()V

    .line 806358
    move-object v0, v0

    .line 806359
    return-object v0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 3

    .prologue
    .line 806360
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/4mt;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 806361
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 806362
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 806363
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Mv;

    .line 806364
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, LX/1Mv;->a(Z)V

    .line 806365
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 806366
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 806367
    :cond_0
    monitor-exit p0

    return-void
.end method
