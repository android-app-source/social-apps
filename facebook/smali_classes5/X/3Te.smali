.class public LX/3Te;
.super LX/3Tf;
.source ""

# interfaces
.implements LX/2iM;
.implements LX/3Tg;


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# instance fields
.field public final c:LX/23P;

.field private final d:LX/0xW;

.field public final e:LX/3Tk;

.field public final f:LX/3UE;

.field public final g:LX/3UK;

.field public final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/3Tl;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/support/v4/app/Fragment;LX/23P;LX/0xW;LX/3Th;LX/3Ti;LX/3Tj;)V
    .locals 7
    .param p1    # Landroid/support/v4/app/Fragment;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 584540
    invoke-direct {p0}, LX/3Tf;-><init>()V

    .line 584541
    iput-object p2, p0, LX/3Te;->c:LX/23P;

    .line 584542
    iput-object p3, p0, LX/3Te;->d:LX/0xW;

    .line 584543
    invoke-virtual {p4, p0, p1}, LX/3Th;->a(LX/3Tg;Landroid/support/v4/app/Fragment;)LX/3Tk;

    move-result-object v0

    iput-object v0, p0, LX/3Te;->e:LX/3Tk;

    .line 584544
    invoke-virtual {p5, p0}, LX/3Ti;->a(LX/3Tg;)LX/3UE;

    move-result-object v0

    iput-object v0, p0, LX/3Te;->f:LX/3UE;

    .line 584545
    new-instance v1, LX/3UK;

    invoke-static {p6}, LX/0xW;->a(LX/0QB;)LX/0xW;

    move-result-object v2

    check-cast v2, LX/0xW;

    invoke-static {p6}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v3

    check-cast v3, LX/17W;

    const-class v4, LX/2iO;

    invoke-interface {p6, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/2iO;

    invoke-static {p6}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v5

    check-cast v5, Landroid/content/res/Resources;

    move-object v6, p0

    invoke-direct/range {v1 .. v6}, LX/3UK;-><init>(LX/0xW;LX/17W;LX/2iO;Landroid/content/res/Resources;LX/3Tg;)V

    .line 584546
    move-object v0, v1

    .line 584547
    iput-object v0, p0, LX/3Te;->g:LX/3UK;

    .line 584548
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/3Te;->h:Ljava/util/List;

    .line 584549
    return-void
.end method

.method private static a(LX/3Tl;I)Z
    .locals 1

    .prologue
    .line 584474
    invoke-interface {p0}, LX/3Tl;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, LX/3Tl;->g()I

    move-result v0

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private y()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 584475
    iget-object v0, p0, LX/3Te;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 584476
    iget-object v0, p0, LX/3Te;->e:LX/3Tk;

    invoke-virtual {v0}, LX/3Tk;->p()Z

    move-result v0

    if-nez v0, :cond_1

    .line 584477
    :cond_0
    :goto_0
    return-void

    .line 584478
    :cond_1
    iget-object v0, p0, LX/3Te;->h:Ljava/util/List;

    iget-object v3, p0, LX/3Te;->e:LX/3Tk;

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 584479
    iget-object v0, p0, LX/3Te;->f:LX/3UE;

    invoke-virtual {v0}, LX/3UE;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 584480
    iget-object v0, p0, LX/3Te;->d:LX/0xW;

    invoke-virtual {v0}, LX/0xW;->r()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, LX/3Te;->d:LX/0xW;

    invoke-virtual {v0}, LX/0xW;->q()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 584481
    :cond_2
    invoke-virtual {p0}, LX/3Te;->i()I

    move-result v0

    if-gtz v0, :cond_3

    iget-object v0, p0, LX/3Te;->f:LX/3UE;

    invoke-virtual {v0}, LX/3UE;->o()Z

    move-result v0

    if-nez v0, :cond_7

    :cond_3
    move v0, v2

    .line 584482
    :goto_1
    if-nez v0, :cond_4

    iget-object v3, p0, LX/3Te;->g:LX/3UK;

    invoke-virtual {v3}, LX/3UK;->m()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 584483
    :cond_4
    if-nez v0, :cond_5

    invoke-virtual {p0}, LX/3Te;->j()I

    move-result v0

    if-nez v0, :cond_8

    :cond_5
    move v0, v2

    .line 584484
    :goto_2
    iget-object v3, p0, LX/3Te;->g:LX/3UK;

    .line 584485
    iput-boolean v0, v3, LX/3UK;->i:Z

    .line 584486
    if-eqz v0, :cond_6

    .line 584487
    iget-object v0, p0, LX/3Te;->h:Ljava/util/List;

    iget-object v3, p0, LX/3Te;->f:LX/3UE;

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 584488
    :cond_6
    iget-object v0, p0, LX/3Te;->g:LX/3UK;

    invoke-virtual {v0}, LX/3UK;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 584489
    invoke-virtual {p0}, LX/3Te;->j()I

    move-result v0

    if-lez v0, :cond_9

    .line 584490
    :goto_3
    iget-object v0, p0, LX/3Te;->f:LX/3UE;

    .line 584491
    iput-boolean v2, v0, LX/3UE;->m:Z

    .line 584492
    if-eqz v2, :cond_0

    .line 584493
    iget-object v0, p0, LX/3Te;->h:Ljava/util/List;

    iget-object v1, p0, LX/3Te;->g:LX/3UK;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_7
    move v0, v1

    .line 584494
    goto :goto_1

    :cond_8
    move v0, v1

    .line 584495
    goto :goto_2

    :cond_9
    move v2, v1

    .line 584496
    goto :goto_3

    :cond_a
    move v0, v2

    goto :goto_2
.end method


# virtual methods
.method public final a(I)I
    .locals 1

    .prologue
    .line 584497
    sget-object v0, LX/3UN;->HEADER:LX/3UN;

    invoke-virtual {v0}, LX/3UN;->ordinal()I

    move-result v0

    return v0
.end method

.method public final a(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 584498
    iget-object v0, p0, LX/3Te;->h:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Tl;

    .line 584499
    invoke-static {v0, p2}, LX/3Te;->a(LX/3Tl;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 584500
    sget-object v1, LX/3UN;->SEE_ALL_FOOTER:LX/3UN;

    invoke-interface {v0}, LX/3Tl;->k()LX/3UN;

    move-result-object p1

    invoke-virtual {v1, p1}, LX/3UN;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 584501
    if-nez p4, :cond_2

    .line 584502
    invoke-virtual {p5}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const p1, 0x7f030c31

    const/4 p2, 0x0

    invoke-virtual {v1, p1, p5, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 584503
    iget-object p1, p0, LX/3Te;->c:LX/23P;

    invoke-virtual {v1}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const p4, 0x7f083932

    invoke-virtual {p2, p4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2, v1}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 584504
    move-object p4, v1

    .line 584505
    :goto_0
    move-object v1, p4

    .line 584506
    :goto_1
    invoke-interface {v0}, LX/3Tl;->l()Landroid/view/View$OnClickListener;

    move-result-object p1

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 584507
    move-object v0, v1

    .line 584508
    :goto_2
    return-object v0

    :cond_0
    invoke-interface {v0, p2, p4, p5}, LX/3Tl;->a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_2

    .line 584509
    :cond_1
    if-nez p4, :cond_3

    .line 584510
    invoke-virtual {p5}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const p1, 0x7f030c32

    const/4 p2, 0x0

    invoke-virtual {v1, p1, p5, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 584511
    iget-object p1, p0, LX/3Te;->c:LX/23P;

    invoke-virtual {v1}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const p4, 0x7f083933

    invoke-virtual {p2, p4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2, v1}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 584512
    move-object p4, v1

    .line 584513
    :goto_3
    move-object v1, p4

    .line 584514
    goto :goto_1

    :cond_2
    check-cast p4, Landroid/widget/TextView;

    goto :goto_0

    :cond_3
    check-cast p4, Landroid/widget/TextView;

    goto :goto_3
.end method

.method public final a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 584515
    if-nez p2, :cond_0

    .line 584516
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030c30

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 584517
    const v1, 0x7f0d0222

    const v2, 0x7f0d13c6

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 584518
    const v1, 0x7f0d0223

    const v2, 0x7f0d1de5

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 584519
    move-object p2, v0

    .line 584520
    :cond_0
    const v0, 0x7f0d0222

    invoke-virtual {p2, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 584521
    const v1, 0x7f0d0223

    invoke-virtual {p2, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 584522
    iget-object v2, p0, LX/3Te;->h:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/3Tl;

    .line 584523
    invoke-interface {v2}, LX/3Tl;->d()Z

    move-result v3

    if-nez v3, :cond_1

    .line 584524
    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 584525
    :goto_0
    invoke-interface {v2}, LX/3Tl;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 584526
    return-object p2

    .line 584527
    :cond_1
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 584528
    invoke-interface {v2}, LX/3Tl;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 584529
    invoke-interface {v2}, LX/3Tl;->f()Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public final a(II)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 584555
    iget-object v0, p0, LX/3Te;->h:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Tl;

    .line 584556
    invoke-static {v0, p2}, LX/3Te;->a(LX/3Tl;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 584557
    invoke-interface {v0}, LX/3Tl;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 584558
    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v0, p2}, LX/3Tl;->b(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 584530
    iget-object v0, p0, LX/3Te;->f:LX/3UE;

    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LX/3UE;->a(J)V

    .line 584531
    return-void
.end method

.method public final a(Ljava/lang/String;LX/2lu;Z)V
    .locals 4

    .prologue
    .line 584532
    iget-object v0, p0, LX/3Te;->f:LX/3UE;

    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 584533
    iget-object v1, v0, LX/3UE;->k:Ljava/util/Set;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p0

    invoke-interface {v1, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 584534
    :cond_0
    :goto_0
    return-void

    .line 584535
    :cond_1
    invoke-static {v0, v2, v3}, LX/3UE;->d(LX/3UE;J)I

    move-result v1

    .line 584536
    const/4 p0, -0x1

    if-eq v1, p0, :cond_0

    .line 584537
    iget-object p0, v0, LX/3UE;->j:Ljava/util/List;

    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/friends/model/FriendRequest;

    .line 584538
    iput-object p2, v1, Lcom/facebook/friends/model/FriendRequest;->j:LX/2lu;

    .line 584539
    iget-object v1, v0, LX/3UE;->i:LX/3Tg;

    invoke-interface {v1}, LX/3Tg;->e()V

    goto :goto_0
.end method

.method public final a(Ljava/util/List;Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/2nq;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 584460
    iget-object v0, p0, LX/3Te;->e:LX/3Tk;

    .line 584461
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/3Tk;->m:Z

    .line 584462
    iget-object v1, v0, LX/3Tk;->p:LX/3Ts;

    .line 584463
    iget-object v2, v1, LX/3Ts;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 584464
    iget-object v1, v0, LX/3Tk;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 584465
    iget-object v1, v0, LX/3Tk;->n:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 584466
    invoke-virtual {v0, p1, p2}, LX/3Tk;->b(Ljava/util/List;Z)V

    .line 584467
    const v0, -0x3b83e85f

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 584468
    return-void
.end method

.method public final b(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 584550
    iget-object v0, p0, LX/3Te;->h:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Tl;

    return-object v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 584551
    iget-object v0, p0, LX/3Te;->g:LX/3UK;

    .line 584552
    iget-object p0, v0, LX/3UK;->c:LX/2iS;

    .line 584553
    iput-object p1, p0, LX/2iS;->o:Ljava/lang/String;

    .line 584554
    return-void
.end method

.method public final b(Ljava/util/List;Z)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/2nq;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 584469
    iget-object v0, p0, LX/3Te;->e:LX/3Tk;

    invoke-virtual {v0, p1, p2}, LX/3Tk;->b(Ljava/util/List;Z)V

    .line 584470
    invoke-direct {p0}, LX/3Te;->y()V

    .line 584471
    const v0, 0x742dea4a

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 584472
    return-void
.end method

.method public final b(II)Z
    .locals 1

    .prologue
    .line 584473
    iget-object v0, p0, LX/3Te;->h:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Tl;

    invoke-interface {v0}, LX/3Tl;->h()Z

    move-result v0

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 584427
    iget-object v0, p0, LX/3Te;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final c(I)I
    .locals 2

    .prologue
    .line 584428
    iget-object v0, p0, LX/3Te;->h:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Tl;

    .line 584429
    invoke-interface {v0}, LX/3Tl;->j()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/3Tl;->g()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-interface {v0}, LX/3Tl;->g()I

    move-result v0

    goto :goto_0
.end method

.method public final c(II)I
    .locals 4

    .prologue
    .line 584430
    iget-object v0, p0, LX/3Te;->h:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Tl;

    .line 584431
    invoke-static {v0, p2}, LX/3Te;->a(LX/3Tl;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 584432
    invoke-interface {v0}, LX/3Tl;->k()LX/3UN;

    move-result-object v0

    invoke-virtual {v0}, LX/3UN;->ordinal()I

    move-result v0

    .line 584433
    :goto_0
    return v0

    .line 584434
    :cond_0
    invoke-static {}, LX/3UN;->values()[LX/3UN;

    move-result-object v1

    array-length v2, v1

    .line 584435
    const/4 v1, 0x0

    move v3, v2

    move v2, v1

    :goto_1
    if-ge v2, p1, :cond_1

    .line 584436
    iget-object v1, p0, LX/3Te;->h:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3Tl;

    invoke-interface {v1}, LX/3Tl;->i()I

    move-result v1

    add-int/2addr v3, v1

    .line 584437
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 584438
    :cond_1
    invoke-interface {v0, p2}, LX/3Tl;->a(I)I

    move-result v0

    add-int/2addr v0, v3

    goto :goto_0
.end method

.method public final c(J)V
    .locals 1

    .prologue
    .line 584439
    iget-object v0, p0, LX/3Te;->g:LX/3UK;

    invoke-virtual {v0, p1, p2}, LX/3UK;->a(J)V

    .line 584440
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 584441
    invoke-direct {p0}, LX/3Te;->y()V

    .line 584442
    const v0, 0x49e11354    # 1843818.5f

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 584443
    return-void
.end method

.method public final getViewTypeCount()I
    .locals 2

    .prologue
    .line 584444
    invoke-static {}, LX/3UN;->values()[LX/3UN;

    move-result-object v0

    array-length v0, v0

    iget-object v1, p0, LX/3Te;->e:LX/3Tk;

    invoke-virtual {v1}, LX/3Tk;->i()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, LX/3Te;->f:LX/3UE;

    invoke-virtual {v1}, LX/3UE;->i()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, LX/3Te;->g:LX/3UK;

    invoke-virtual {v1}, LX/3UK;->i()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final h()I
    .locals 2

    .prologue
    .line 584445
    invoke-virtual {p0}, LX/3Te;->n()Z

    move-result v0

    const-string v1, "The Friend Request list is empty"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 584446
    iget-object v0, p0, LX/3Te;->h:Ljava/util/List;

    iget-object v1, p0, LX/3Te;->f:LX/3UE;

    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    invoke-virtual {p0, v0}, LX/3Tf;->p_(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public final i()I
    .locals 1

    .prologue
    .line 584447
    iget-object v0, p0, LX/3Te;->f:LX/3UE;

    .line 584448
    iget-object p0, v0, LX/3UE;->j:Ljava/util/List;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result p0

    move v0, p0

    .line 584449
    return v0
.end method

.method public final j()I
    .locals 1

    .prologue
    .line 584450
    iget-object v0, p0, LX/3Te;->g:LX/3UK;

    .line 584451
    iget-object p0, v0, LX/3UK;->f:Ljava/util/List;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result p0

    move v0, p0

    .line 584452
    return v0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 584453
    iget-object v0, p0, LX/3Te;->e:LX/3Tk;

    invoke-virtual {v0}, LX/3Tk;->p()Z

    move-result v0

    return v0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 584454
    iget-object v0, p0, LX/3Te;->f:LX/3UE;

    invoke-virtual {v0}, LX/3UE;->b()Z

    move-result v0

    return v0
.end method

.method public final n()Z
    .locals 2

    .prologue
    .line 584455
    invoke-virtual {p0}, LX/3Te;->i()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, LX/3Te;->h:Ljava/util/List;

    iget-object v1, p0, LX/3Te;->f:LX/3UE;

    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final notifyDataSetChanged()V
    .locals 2

    .prologue
    .line 584456
    iget-object v0, p0, LX/3Te;->e:LX/3Tk;

    .line 584457
    iget-object v1, v0, LX/3Tk;->l:LX/1Qq;

    invoke-interface {v1}, LX/1Cw;->notifyDataSetChanged()V

    .line 584458
    invoke-super {p0}, LX/3Tf;->notifyDataSetChanged()V

    .line 584459
    return-void
.end method
