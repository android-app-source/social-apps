.class public LX/4U4;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 720629
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    const/4 v0, 0x0

    .line 720546
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v2, :cond_c

    .line 720547
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 720548
    :goto_0
    return v0

    .line 720549
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 720550
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_b

    .line 720551
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 720552
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 720553
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_1

    if-eqz v10, :cond_1

    .line 720554
    const-string v11, "attribution_uri"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 720555
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 720556
    :cond_2
    const-string v11, "context_description"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 720557
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 720558
    :cond_3
    const-string v11, "context_photo"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 720559
    invoke-static {p0, p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 720560
    :cond_4
    const-string v11, "context_photo_fullscreen_landscape"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 720561
    invoke-static {p0, p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 720562
    :cond_5
    const-string v11, "context_photo_fullscreen_portrait"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 720563
    invoke-static {p0, p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 720564
    :cond_6
    const-string v11, "headline_source"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 720565
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 720566
    :cond_7
    const-string v11, "id"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 720567
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto/16 :goto_1

    .line 720568
    :cond_8
    const-string v11, "topic_description"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_9

    .line 720569
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto/16 :goto_1

    .line 720570
    :cond_9
    const-string v11, "unique_keyword"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_a

    .line 720571
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    goto/16 :goto_1

    .line 720572
    :cond_a
    const-string v11, "url"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 720573
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_1

    .line 720574
    :cond_b
    const/16 v10, 0xb

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 720575
    const/4 v10, 0x1

    invoke-virtual {p1, v10, v9}, LX/186;->b(II)V

    .line 720576
    const/4 v9, 0x2

    invoke-virtual {p1, v9, v8}, LX/186;->b(II)V

    .line 720577
    const/4 v8, 0x3

    invoke-virtual {p1, v8, v7}, LX/186;->b(II)V

    .line 720578
    const/4 v7, 0x4

    invoke-virtual {p1, v7, v6}, LX/186;->b(II)V

    .line 720579
    const/4 v6, 0x5

    invoke-virtual {p1, v6, v5}, LX/186;->b(II)V

    .line 720580
    const/4 v5, 0x6

    invoke-virtual {p1, v5, v4}, LX/186;->b(II)V

    .line 720581
    const/4 v4, 0x7

    invoke-virtual {p1, v4, v3}, LX/186;->b(II)V

    .line 720582
    const/16 v3, 0x8

    invoke-virtual {p1, v3, v2}, LX/186;->b(II)V

    .line 720583
    const/16 v2, 0x9

    invoke-virtual {p1, v2, v1}, LX/186;->b(II)V

    .line 720584
    const/16 v1, 0xa

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 720585
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :cond_c
    move v1, v0

    move v2, v0

    move v3, v0

    move v4, v0

    move v5, v0

    move v6, v0

    move v7, v0

    move v8, v0

    move v9, v0

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 720586
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 720587
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 720588
    if-eqz v0, :cond_0

    .line 720589
    const-string v1, "attribution_uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 720590
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 720591
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 720592
    if-eqz v0, :cond_1

    .line 720593
    const-string v1, "context_description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 720594
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 720595
    :cond_1
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 720596
    if-eqz v0, :cond_2

    .line 720597
    const-string v1, "context_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 720598
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 720599
    :cond_2
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 720600
    if-eqz v0, :cond_3

    .line 720601
    const-string v1, "context_photo_fullscreen_landscape"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 720602
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 720603
    :cond_3
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 720604
    if-eqz v0, :cond_4

    .line 720605
    const-string v1, "context_photo_fullscreen_portrait"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 720606
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 720607
    :cond_4
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 720608
    if-eqz v0, :cond_5

    .line 720609
    const-string v1, "headline_source"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 720610
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 720611
    :cond_5
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 720612
    if-eqz v0, :cond_6

    .line 720613
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 720614
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 720615
    :cond_6
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 720616
    if-eqz v0, :cond_7

    .line 720617
    const-string v1, "topic_description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 720618
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 720619
    :cond_7
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 720620
    if-eqz v0, :cond_8

    .line 720621
    const-string v1, "unique_keyword"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 720622
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 720623
    :cond_8
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 720624
    if-eqz v0, :cond_9

    .line 720625
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 720626
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 720627
    :cond_9
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 720628
    return-void
.end method
