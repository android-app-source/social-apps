.class public LX/4y2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J


# instance fields
.field private final keys:[Ljava/lang/Object;

.field private final values:[Ljava/lang/Object;


# direct methods
.method public constructor <init>(LX/0P1;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0P1",
            "<**>;)V"
        }
    .end annotation

    .prologue
    .line 821552
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 821553
    invoke-virtual {p1}, LX/0P1;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/Object;

    iput-object v0, p0, LX/4y2;->keys:[Ljava/lang/Object;

    .line 821554
    invoke-virtual {p1}, LX/0P1;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/Object;

    iput-object v0, p0, LX/4y2;->values:[Ljava/lang/Object;

    .line 821555
    const/4 v0, 0x0

    .line 821556
    invoke-virtual {p1}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v1

    invoke-virtual {v1}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 821557
    iget-object v3, p0, LX/4y2;->keys:[Ljava/lang/Object;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v3, v1

    .line 821558
    iget-object v3, p0, LX/4y2;->values:[Ljava/lang/Object;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    aput-object v0, v3, v1

    .line 821559
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 821560
    goto :goto_0

    .line 821561
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/0P2;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0P2",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .prologue
    .line 821562
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, LX/4y2;->keys:[Ljava/lang/Object;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 821563
    iget-object v1, p0, LX/4y2;->keys:[Ljava/lang/Object;

    aget-object v1, v1, v0

    iget-object v2, p0, LX/4y2;->values:[Ljava/lang/Object;

    aget-object v2, v2, v0

    invoke-virtual {p1, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 821564
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 821565
    :cond_0
    invoke-virtual {p1}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    return-object v0
.end method

.method public readResolve()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 821566
    new-instance v0, LX/0P2;

    iget-object v1, p0, LX/4y2;->keys:[Ljava/lang/Object;

    array-length v1, v1

    invoke-direct {v0, v1}, LX/0P2;-><init>(I)V

    .line 821567
    invoke-virtual {p0, v0}, LX/4y2;->a(LX/0P2;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
