.class public LX/3jj;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "LX/3jN;",
            ">;"
        }
    .end annotation
.end field

.field public static b:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "LX/3jN;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 632495
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    const-string v1, "NT:ACTION:APPEND"

    new-instance v2, LX/3jk;

    invoke-direct {v2}, LX/3jk;-><init>()V

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "NT:ACTION:DATE"

    new-instance v2, LX/3jl;

    invoke-direct {v2}, LX/3jl;-><init>()V

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "NT:ACTION:DELAY"

    new-instance v2, LX/3jm;

    invoke-direct {v2}, LX/3jm;-><init>()V

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "NT:ACTION:GROUP"

    new-instance v2, LX/3jn;

    invoke-direct {v2}, LX/3jn;-><init>()V

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "NT:ACTION:LIMIT"

    new-instance v2, LX/3jo;

    invoke-direct {v2}, LX/3jo;-><init>()V

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "NT:ACTION:INSERT_AFTER"

    new-instance v2, LX/3jk;

    invoke-direct {v2}, LX/3jk;-><init>()V

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "NT:ACTION:INSERT_BEFORE"

    new-instance v2, LX/3jk;

    invoke-direct {v2}, LX/3jk;-><init>()V

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "NT:ACTION:NIL_ATTRIBUTES"

    new-instance v2, LX/3jp;

    invoke-direct {v2}, LX/3jp;-><init>()V

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "NT:ACTION:PREPEND"

    new-instance v2, LX/3jk;

    invoke-direct {v2}, LX/3jk;-><init>()V

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "NT:ACTION:SET_ATTRIBUTES"

    new-instance v2, LX/3jq;

    invoke-direct {v2}, LX/3jq;-><init>()V

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "NT:ACTION:REMOVE"

    new-instance v2, LX/3jk;

    invoke-direct {v2}, LX/3jk;-><init>()V

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "NT:ACTION:REPLACE"

    new-instance v2, LX/3jk;

    invoke-direct {v2}, LX/3jk;-><init>()V

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    .line 632496
    sput-object v0, LX/3jj;->a:LX/0P1;

    sput-object v0, LX/3jj;->b:LX/0P1;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 632497
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;)LX/3jN;
    .locals 1

    .prologue
    .line 632498
    sget-object v0, LX/3jj;->b:LX/0P1;

    invoke-virtual {v0, p0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3jN;

    return-object v0
.end method
