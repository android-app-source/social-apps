.class public abstract LX/4dC;
.super Landroid/graphics/drawable/Drawable;
.source ""

# interfaces
.implements Landroid/graphics/drawable/Animatable;
.implements LX/2dJ;


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private A:Z

.field public B:Z

.field public C:J

.field private D:Z

.field private final E:Ljava/lang/Runnable;

.field private final F:Ljava/lang/Runnable;

.field private final G:Ljava/lang/Runnable;

.field public final H:Ljava/lang/Runnable;

.field public final b:Ljava/util/concurrent/ScheduledExecutorService;

.field private final c:LX/4dI;

.field public final d:LX/0So;

.field public final e:I

.field private final f:I

.field private final g:I

.field private final h:Landroid/graphics/Paint;

.field private final i:Landroid/graphics/Rect;

.field private final j:Landroid/graphics/Paint;

.field public volatile k:Ljava/lang/String;

.field public l:LX/4dH;

.field private m:J

.field public n:I

.field public o:I

.field public p:I

.field public q:I

.field private r:I

.field private s:I

.field private t:LX/1FJ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field public u:Z

.field public v:J

.field public w:Z

.field public x:Z

.field private y:F

.field private z:F


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 795702
    const-class v0, LX/4dF;

    sput-object v0, LX/4dC;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/ScheduledExecutorService;LX/4dH;LX/4dI;LX/0So;)V
    .locals 8

    .prologue
    const-wide/16 v6, -0x1

    const/4 v4, 0x0

    const/4 v3, -0x1

    const/high16 v2, 0x3f800000    # 1.0f

    .line 795600
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 795601
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x6

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, LX/4dC;->h:Landroid/graphics/Paint;

    .line 795602
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/4dC;->i:Landroid/graphics/Rect;

    .line 795603
    iput v3, p0, LX/4dC;->r:I

    .line 795604
    iput v3, p0, LX/4dC;->s:I

    .line 795605
    iput-wide v6, p0, LX/4dC;->v:J

    .line 795606
    iput v2, p0, LX/4dC;->y:F

    .line 795607
    iput v2, p0, LX/4dC;->z:F

    .line 795608
    iput-wide v6, p0, LX/4dC;->C:J

    .line 795609
    iput-boolean v4, p0, LX/4dC;->D:Z

    .line 795610
    new-instance v0, Lcom/facebook/imagepipeline/animated/base/AbstractAnimatedDrawable$1;

    invoke-direct {v0, p0}, Lcom/facebook/imagepipeline/animated/base/AbstractAnimatedDrawable$1;-><init>(LX/4dC;)V

    iput-object v0, p0, LX/4dC;->E:Ljava/lang/Runnable;

    .line 795611
    new-instance v0, Lcom/facebook/imagepipeline/animated/base/AbstractAnimatedDrawable$2;

    invoke-direct {v0, p0}, Lcom/facebook/imagepipeline/animated/base/AbstractAnimatedDrawable$2;-><init>(LX/4dC;)V

    iput-object v0, p0, LX/4dC;->F:Ljava/lang/Runnable;

    .line 795612
    new-instance v0, Lcom/facebook/imagepipeline/animated/base/AbstractAnimatedDrawable$3;

    invoke-direct {v0, p0}, Lcom/facebook/imagepipeline/animated/base/AbstractAnimatedDrawable$3;-><init>(LX/4dC;)V

    iput-object v0, p0, LX/4dC;->G:Ljava/lang/Runnable;

    .line 795613
    new-instance v0, Lcom/facebook/imagepipeline/animated/base/AbstractAnimatedDrawable$4;

    invoke-direct {v0, p0}, Lcom/facebook/imagepipeline/animated/base/AbstractAnimatedDrawable$4;-><init>(LX/4dC;)V

    iput-object v0, p0, LX/4dC;->H:Ljava/lang/Runnable;

    .line 795614
    iput-object p1, p0, LX/4dC;->b:Ljava/util/concurrent/ScheduledExecutorService;

    .line 795615
    iput-object p2, p0, LX/4dC;->l:LX/4dH;

    .line 795616
    iput-object p3, p0, LX/4dC;->c:LX/4dI;

    .line 795617
    iput-object p4, p0, LX/4dC;->d:LX/0So;

    .line 795618
    iget-object v0, p0, LX/4dC;->l:LX/4dH;

    invoke-interface {v0}, LX/4dG;->b()I

    move-result v0

    iput v0, p0, LX/4dC;->e:I

    .line 795619
    iget-object v0, p0, LX/4dC;->l:LX/4dH;

    invoke-interface {v0}, LX/4dG;->c()I

    move-result v0

    iput v0, p0, LX/4dC;->f:I

    .line 795620
    iget-object v0, p0, LX/4dC;->c:LX/4dI;

    iget-object v1, p0, LX/4dC;->l:LX/4dH;

    invoke-interface {v0, v1}, LX/4dI;->a(LX/4dH;)V

    .line 795621
    iget-object v0, p0, LX/4dC;->l:LX/4dH;

    invoke-interface {v0}, LX/4dG;->d()I

    move-result v0

    iput v0, p0, LX/4dC;->g:I

    .line 795622
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, LX/4dC;->j:Landroid/graphics/Paint;

    .line 795623
    iget-object v0, p0, LX/4dC;->j:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 795624
    iget-object v0, p0, LX/4dC;->j:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 795625
    const/4 v1, -0x1

    .line 795626
    iget-object v0, p0, LX/4dC;->l:LX/4dH;

    invoke-interface {v0}, LX/4dG;->i()I

    move-result v0

    iput v0, p0, LX/4dC;->n:I

    .line 795627
    iget v0, p0, LX/4dC;->n:I

    iput v0, p0, LX/4dC;->o:I

    .line 795628
    iput v1, p0, LX/4dC;->p:I

    .line 795629
    iput v1, p0, LX/4dC;->q:I

    .line 795630
    return-void
.end method

.method private static a(LX/4dC;Z)V
    .locals 8

    .prologue
    .line 795631
    iget v0, p0, LX/4dC;->e:I

    if-nez v0, :cond_1

    .line 795632
    :cond_0
    :goto_0
    return-void

    .line 795633
    :cond_1
    iget-object v0, p0, LX/4dC;->d:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v2

    .line 795634
    iget-wide v0, p0, LX/4dC;->m:J

    sub-long v0, v2, v0

    iget v4, p0, LX/4dC;->e:I

    int-to-long v4, v4

    div-long/2addr v0, v4

    long-to-int v1, v0

    .line 795635
    iget v0, p0, LX/4dC;->g:I

    if-eqz v0, :cond_2

    iget v0, p0, LX/4dC;->g:I

    if-ge v1, v0, :cond_0

    .line 795636
    :cond_2
    iget-wide v4, p0, LX/4dC;->m:J

    sub-long v4, v2, v4

    iget v0, p0, LX/4dC;->e:I

    int-to-long v6, v0

    rem-long/2addr v4, v6

    long-to-int v4, v4

    .line 795637
    iget-object v0, p0, LX/4dC;->l:LX/4dH;

    invoke-interface {v0, v4}, LX/4dG;->b(I)I

    move-result v5

    .line 795638
    iget v0, p0, LX/4dC;->n:I

    if-eq v0, v5, :cond_3

    const/4 v0, 0x1

    .line 795639
    :goto_1
    iput v5, p0, LX/4dC;->n:I

    .line 795640
    iget v6, p0, LX/4dC;->f:I

    mul-int/2addr v1, v6

    add-int/2addr v1, v5

    iput v1, p0, LX/4dC;->o:I

    .line 795641
    if-eqz p1, :cond_0

    .line 795642
    if-eqz v0, :cond_4

    .line 795643
    invoke-static {p0}, LX/4dC;->i(LX/4dC;)V

    goto :goto_0

    .line 795644
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 795645
    :cond_4
    iget-object v0, p0, LX/4dC;->l:LX/4dH;

    iget v1, p0, LX/4dC;->n:I

    invoke-interface {v0, v1}, LX/4dG;->c(I)I

    move-result v0

    iget-object v1, p0, LX/4dC;->l:LX/4dH;

    iget v5, p0, LX/4dC;->n:I

    invoke-interface {v1, v5}, LX/4dG;->d(I)I

    move-result v1

    add-int/2addr v0, v1

    sub-int/2addr v0, v4

    .line 795646
    int-to-long v0, v0

    add-long/2addr v0, v2

    .line 795647
    iget-wide v2, p0, LX/4dC;->C:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_5

    iget-wide v2, p0, LX/4dC;->C:J

    cmp-long v2, v2, v0

    if-lez v2, :cond_0

    .line 795648
    :cond_5
    iget-object v2, p0, LX/4dC;->F:Ljava/lang/Runnable;

    invoke-virtual {p0, v2}, LX/4dC;->unscheduleSelf(Ljava/lang/Runnable;)V

    .line 795649
    iget-object v2, p0, LX/4dC;->F:Ljava/lang/Runnable;

    invoke-virtual {p0, v2, v0, v1}, LX/4dC;->scheduleSelf(Ljava/lang/Runnable;J)V

    .line 795650
    iput-wide v0, p0, LX/4dC;->C:J

    goto :goto_0
.end method

.method private a(Landroid/graphics/Canvas;II)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 795651
    iget-object v0, p0, LX/4dC;->l:LX/4dH;

    invoke-interface {v0, p2}, LX/4dH;->g(I)LX/1FJ;

    move-result-object v2

    .line 795652
    if-eqz v2, :cond_2

    .line 795653
    invoke-virtual {v2}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    iget-object v3, p0, LX/4dC;->h:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v4, v4, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 795654
    iget-object v0, p0, LX/4dC;->t:LX/1FJ;

    if-eqz v0, :cond_0

    .line 795655
    iget-object v0, p0, LX/4dC;->t:LX/1FJ;

    invoke-virtual {v0}, LX/1FJ;->close()V

    .line 795656
    :cond_0
    iget-boolean v0, p0, LX/4dC;->w:Z

    if-eqz v0, :cond_1

    iget v0, p0, LX/4dC;->s:I

    if-le p3, v0, :cond_1

    .line 795657
    iget v0, p0, LX/4dC;->s:I

    sub-int v0, p3, v0

    add-int/lit8 v0, v0, -0x1

    .line 795658
    iget-object v3, p0, LX/4dC;->c:LX/4dI;

    invoke-interface {v3, v1}, LX/4dI;->b(I)V

    .line 795659
    iget-object v3, p0, LX/4dC;->c:LX/4dI;

    invoke-interface {v3, v0}, LX/4dI;->a(I)V

    .line 795660
    if-lez v0, :cond_1

    .line 795661
    :cond_1
    iput-object v2, p0, LX/4dC;->t:LX/1FJ;

    .line 795662
    iput p2, p0, LX/4dC;->r:I

    .line 795663
    iput p3, p0, LX/4dC;->s:I

    .line 795664
    move v0, v1

    .line 795665
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e$redex0(LX/4dC;)V
    .locals 4

    .prologue
    .line 795666
    iget-boolean v0, p0, LX/4dC;->w:Z

    if-nez v0, :cond_0

    .line 795667
    :goto_0
    return-void

    .line 795668
    :cond_0
    iget-object v0, p0, LX/4dC;->c:LX/4dI;

    invoke-interface {v0}, LX/4dI;->a()V

    .line 795669
    :try_start_0
    iget-object v0, p0, LX/4dC;->d:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/4dC;->m:J

    .line 795670
    iget-boolean v0, p0, LX/4dC;->D:Z

    if-eqz v0, :cond_1

    .line 795671
    iget-wide v0, p0, LX/4dC;->m:J

    iget-object v2, p0, LX/4dC;->l:LX/4dH;

    iget v3, p0, LX/4dC;->n:I

    invoke-interface {v2, v3}, LX/4dG;->c(I)I

    move-result v2

    int-to-long v2, v2

    sub-long/2addr v0, v2

    iput-wide v0, p0, LX/4dC;->m:J

    .line 795672
    :goto_1
    iget-wide v0, p0, LX/4dC;->m:J

    iget-object v2, p0, LX/4dC;->l:LX/4dH;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, LX/4dG;->d(I)I

    move-result v2

    int-to-long v2, v2

    add-long/2addr v0, v2

    .line 795673
    iget-object v2, p0, LX/4dC;->F:Ljava/lang/Runnable;

    invoke-virtual {p0, v2, v0, v1}, LX/4dC;->scheduleSelf(Ljava/lang/Runnable;J)V

    .line 795674
    iput-wide v0, p0, LX/4dC;->C:J

    .line 795675
    invoke-static {p0}, LX/4dC;->i(LX/4dC;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 795676
    iget-object v0, p0, LX/4dC;->c:LX/4dI;

    invoke-interface {v0}, LX/4dI;->b()V

    goto :goto_0

    .line 795677
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    iput v0, p0, LX/4dC;->n:I

    .line 795678
    const/4 v0, 0x0

    iput v0, p0, LX/4dC;->o:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 795679
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/4dC;->c:LX/4dI;

    invoke-interface {v1}, LX/4dI;->b()V

    throw v0
.end method

.method public static f(LX/4dC;)V
    .locals 2

    .prologue
    .line 795680
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/4dC;->C:J

    .line 795681
    iget-boolean v0, p0, LX/4dC;->w:Z

    if-nez v0, :cond_1

    .line 795682
    :cond_0
    :goto_0
    return-void

    .line 795683
    :cond_1
    iget v0, p0, LX/4dC;->e:I

    if-eqz v0, :cond_0

    .line 795684
    iget-object v0, p0, LX/4dC;->c:LX/4dI;

    invoke-interface {v0}, LX/4dI;->c()V

    .line 795685
    const/4 v0, 0x1

    :try_start_0
    invoke-static {p0, v0}, LX/4dC;->a(LX/4dC;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 795686
    iget-object v0, p0, LX/4dC;->c:LX/4dI;

    invoke-interface {v0}, LX/4dI;->d()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/4dC;->c:LX/4dI;

    invoke-interface {v1}, LX/4dI;->d()V

    throw v0
.end method

.method private g()V
    .locals 4

    .prologue
    .line 795687
    iget-boolean v0, p0, LX/4dC;->B:Z

    if-eqz v0, :cond_0

    .line 795688
    :goto_0
    return-void

    .line 795689
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4dC;->B:Z

    .line 795690
    iget-object v0, p0, LX/4dC;->G:Ljava/lang/Runnable;

    const-wide/16 v2, 0x5

    invoke-virtual {p0, v0, v2, v3}, LX/4dC;->scheduleSelf(Ljava/lang/Runnable;J)V

    goto :goto_0
.end method

.method public static i(LX/4dC;)V
    .locals 2

    .prologue
    .line 795691
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4dC;->u:Z

    .line 795692
    iget-object v0, p0, LX/4dC;->d:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/4dC;->v:J

    .line 795693
    invoke-virtual {p0}, LX/4dC;->invalidateSelf()V

    .line 795694
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 795695
    iget-object v0, p0, LX/4dC;->t:LX/1FJ;

    if-eqz v0, :cond_0

    .line 795696
    iget-object v0, p0, LX/4dC;->t:LX/1FJ;

    invoke-virtual {v0}, LX/1FJ;->close()V

    .line 795697
    const/4 v0, 0x0

    iput-object v0, p0, LX/4dC;->t:LX/1FJ;

    .line 795698
    iput v1, p0, LX/4dC;->r:I

    .line 795699
    iput v1, p0, LX/4dC;->s:I

    .line 795700
    :cond_0
    iget-object v0, p0, LX/4dC;->l:LX/4dH;

    invoke-interface {v0}, LX/4dG;->k()V

    .line 795701
    return-void
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    const/4 v7, -0x1

    .line 795540
    iget-object v2, p0, LX/4dC;->c:LX/4dI;

    invoke-interface {v2}, LX/4dI;->e()V

    .line 795541
    const/4 v2, 0x0

    :try_start_0
    iput-boolean v2, p0, LX/4dC;->u:Z

    .line 795542
    iget-boolean v2, p0, LX/4dC;->w:Z

    if-eqz v2, :cond_0

    iget-boolean v2, p0, LX/4dC;->x:Z

    if-nez v2, :cond_0

    .line 795543
    iget-object v2, p0, LX/4dC;->b:Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v3, p0, LX/4dC;->H:Ljava/lang/Runnable;

    const-wide/16 v4, 0x7d0

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v2, v3, v4, v5, v6}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 795544
    const/4 v2, 0x1

    iput-boolean v2, p0, LX/4dC;->x:Z

    .line 795545
    :cond_0
    iget-boolean v2, p0, LX/4dC;->A:Z

    if-eqz v2, :cond_2

    .line 795546
    iget-object v2, p0, LX/4dC;->i:Landroid/graphics/Rect;

    invoke-virtual {p0}, LX/4dC;->getBounds()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 795547
    iget-object v2, p0, LX/4dC;->i:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 795548
    iget-object v2, p0, LX/4dC;->l:LX/4dH;

    iget-object v3, p0, LX/4dC;->i:Landroid/graphics/Rect;

    invoke-interface {v2, v3}, LX/4dH;->b(Landroid/graphics/Rect;)LX/4dH;

    move-result-object v2

    .line 795549
    iget-object v3, p0, LX/4dC;->l:LX/4dH;

    if-eq v2, v3, :cond_1

    .line 795550
    iget-object v3, p0, LX/4dC;->l:LX/4dH;

    invoke-interface {v3}, LX/4dG;->k()V

    .line 795551
    iput-object v2, p0, LX/4dC;->l:LX/4dH;

    .line 795552
    iget-object v3, p0, LX/4dC;->c:LX/4dI;

    invoke-interface {v3, v2}, LX/4dI;->a(LX/4dH;)V

    .line 795553
    :cond_1
    iget-object v2, p0, LX/4dC;->i:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, LX/4dC;->l:LX/4dH;

    invoke-interface {v3}, LX/4dG;->g()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    iput v2, p0, LX/4dC;->y:F

    .line 795554
    iget-object v2, p0, LX/4dC;->i:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, LX/4dC;->l:LX/4dH;

    invoke-interface {v3}, LX/4dG;->h()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    iput v2, p0, LX/4dC;->z:F

    .line 795555
    const/4 v2, 0x0

    iput-boolean v2, p0, LX/4dC;->A:Z

    .line 795556
    :cond_2
    iget-object v2, p0, LX/4dC;->i:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_3

    .line 795557
    iget-object v0, p0, LX/4dC;->c:LX/4dI;

    invoke-interface {v0}, LX/4dI;->f()V

    .line 795558
    :goto_0
    return-void

    .line 795559
    :cond_3
    :try_start_1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 795560
    iget v2, p0, LX/4dC;->y:F

    iget v3, p0, LX/4dC;->z:F

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->scale(FF)V

    .line 795561
    iget v2, p0, LX/4dC;->p:I

    if-eq v2, v7, :cond_4

    .line 795562
    iget v0, p0, LX/4dC;->p:I

    iget v2, p0, LX/4dC;->q:I

    invoke-direct {p0, p1, v0, v2}, LX/4dC;->a(Landroid/graphics/Canvas;II)Z

    move-result v2

    .line 795563
    or-int/lit8 v0, v2, 0x0

    .line 795564
    if-eqz v2, :cond_a

    .line 795565
    const/4 v2, -0x1

    iput v2, p0, LX/4dC;->p:I

    .line 795566
    const/4 v2, -0x1

    iput v2, p0, LX/4dC;->q:I

    .line 795567
    :cond_4
    :goto_1
    iget v2, p0, LX/4dC;->p:I

    if-ne v2, v7, :cond_6

    .line 795568
    iget-boolean v2, p0, LX/4dC;->w:Z

    if-eqz v2, :cond_5

    .line 795569
    const/4 v2, 0x0

    invoke-static {p0, v2}, LX/4dC;->a(LX/4dC;Z)V

    .line 795570
    :cond_5
    iget v2, p0, LX/4dC;->n:I

    iget v3, p0, LX/4dC;->o:I

    invoke-direct {p0, p1, v2, v3}, LX/4dC;->a(Landroid/graphics/Canvas;II)Z

    move-result v2

    .line 795571
    or-int/2addr v0, v2

    .line 795572
    if-eqz v2, :cond_b

    .line 795573
    iget-boolean v2, p0, LX/4dC;->w:Z

    if-eqz v2, :cond_6

    .line 795574
    const/4 v2, 0x1

    invoke-static {p0, v2}, LX/4dC;->a(LX/4dC;Z)V

    .line 795575
    :cond_6
    :goto_2
    if-nez v0, :cond_7

    .line 795576
    iget-object v2, p0, LX/4dC;->t:LX/1FJ;

    if-eqz v2, :cond_7

    .line 795577
    iget-object v0, p0, LX/4dC;->t:LX/1FJ;

    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    const/4 v2, 0x0

    const/4 v3, 0x0

    iget-object v4, p0, LX/4dC;->h:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 795578
    move v0, v1

    .line 795579
    :cond_7
    if-nez v0, :cond_8

    .line 795580
    iget-object v2, p0, LX/4dC;->l:LX/4dH;

    invoke-interface {v2}, LX/4dH;->l()LX/1FJ;

    move-result-object v2

    .line 795581
    if-eqz v2, :cond_8

    .line 795582
    invoke-virtual {v2}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    const/4 v3, 0x0

    const/4 v4, 0x0

    iget-object v5, p0, LX/4dC;->h:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 795583
    invoke-virtual {v2}, LX/1FJ;->close()V

    .line 795584
    move v0, v1

    .line 795585
    :cond_8
    if-nez v0, :cond_9

    .line 795586
    const/4 v1, 0x0

    const/4 v2, 0x0

    iget-object v0, p0, LX/4dC;->i:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v3, v0

    iget-object v0, p0, LX/4dC;->i:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    int-to-float v4, v0

    iget-object v5, p0, LX/4dC;->j:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 795587
    :cond_9
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 795588
    iget-object v0, p0, LX/4dC;->c:LX/4dI;

    iget-object v1, p0, LX/4dC;->i:Landroid/graphics/Rect;

    invoke-interface {v0, p1, v1}, LX/4dI;->a(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 795589
    iget-object v0, p0, LX/4dC;->c:LX/4dI;

    invoke-interface {v0}, LX/4dI;->f()V

    goto/16 :goto_0

    .line 795590
    :cond_a
    :try_start_2
    invoke-direct {p0}, LX/4dC;->g()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 795591
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/4dC;->c:LX/4dI;

    invoke-interface {v1}, LX/4dI;->f()V

    throw v0

    .line 795592
    :cond_b
    :try_start_3
    iget v2, p0, LX/4dC;->n:I

    iput v2, p0, LX/4dC;->p:I

    .line 795593
    iget v2, p0, LX/4dC;->o:I

    iput v2, p0, LX/4dC;->q:I

    .line 795594
    invoke-direct {p0}, LX/4dC;->g()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2
.end method

.method public final finalize()V
    .locals 1

    .prologue
    .line 795595
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 795596
    iget-object v0, p0, LX/4dC;->t:LX/1FJ;

    if-eqz v0, :cond_0

    .line 795597
    iget-object v0, p0, LX/4dC;->t:LX/1FJ;

    invoke-virtual {v0}, LX/1FJ;->close()V

    .line 795598
    const/4 v0, 0x0

    iput-object v0, p0, LX/4dC;->t:LX/1FJ;

    .line 795599
    :cond_0
    return-void
.end method

.method public final getIntrinsicHeight()I
    .locals 1

    .prologue
    .line 795505
    iget-object v0, p0, LX/4dC;->l:LX/4dH;

    invoke-interface {v0}, LX/4dG;->f()I

    move-result v0

    return v0
.end method

.method public final getIntrinsicWidth()I
    .locals 1

    .prologue
    .line 795506
    iget-object v0, p0, LX/4dC;->l:LX/4dH;

    invoke-interface {v0}, LX/4dG;->e()I

    move-result v0

    return v0
.end method

.method public final getOpacity()I
    .locals 1

    .prologue
    .line 795507
    const/4 v0, -0x3

    return v0
.end method

.method public final isRunning()Z
    .locals 1

    .prologue
    .line 795508
    iget-boolean v0, p0, LX/4dC;->w:Z

    return v0
.end method

.method public final onBoundsChange(Landroid/graphics/Rect;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 795509
    invoke-super {p0, p1}, Landroid/graphics/drawable/Drawable;->onBoundsChange(Landroid/graphics/Rect;)V

    .line 795510
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4dC;->A:Z

    .line 795511
    iget-object v0, p0, LX/4dC;->t:LX/1FJ;

    if-eqz v0, :cond_0

    .line 795512
    iget-object v0, p0, LX/4dC;->t:LX/1FJ;

    invoke-virtual {v0}, LX/1FJ;->close()V

    .line 795513
    const/4 v0, 0x0

    iput-object v0, p0, LX/4dC;->t:LX/1FJ;

    .line 795514
    :cond_0
    iput v1, p0, LX/4dC;->r:I

    .line 795515
    iput v1, p0, LX/4dC;->s:I

    .line 795516
    iget-object v0, p0, LX/4dC;->l:LX/4dH;

    invoke-interface {v0}, LX/4dG;->k()V

    .line 795517
    return-void
.end method

.method public final onLevelChange(I)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 795531
    iget-boolean v1, p0, LX/4dC;->w:Z

    if-eqz v1, :cond_1

    .line 795532
    :cond_0
    :goto_0
    return v0

    .line 795533
    :cond_1
    iget-object v1, p0, LX/4dC;->l:LX/4dH;

    invoke-interface {v1, p1}, LX/4dG;->b(I)I

    move-result v1

    .line 795534
    iget v2, p0, LX/4dC;->n:I

    if-eq v1, v2, :cond_0

    .line 795535
    :try_start_0
    iput v1, p0, LX/4dC;->n:I

    .line 795536
    iput v1, p0, LX/4dC;->o:I

    .line 795537
    invoke-static {p0}, LX/4dC;->i(LX/4dC;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 795538
    const/4 v0, 0x1

    goto :goto_0

    .line 795539
    :catch_0
    goto :goto_0
.end method

.method public final setAlpha(I)V
    .locals 1

    .prologue
    .line 795518
    iget-object v0, p0, LX/4dC;->h:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 795519
    invoke-static {p0}, LX/4dC;->i(LX/4dC;)V

    .line 795520
    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1

    .prologue
    .line 795521
    iget-object v0, p0, LX/4dC;->h:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 795522
    invoke-static {p0}, LX/4dC;->i(LX/4dC;)V

    .line 795523
    return-void
.end method

.method public final start()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 795524
    iget v0, p0, LX/4dC;->e:I

    if-eqz v0, :cond_0

    iget v0, p0, LX/4dC;->f:I

    if-gt v0, v1, :cond_1

    .line 795525
    :cond_0
    :goto_0
    return-void

    .line 795526
    :cond_1
    iput-boolean v1, p0, LX/4dC;->w:Z

    .line 795527
    iget-object v0, p0, LX/4dC;->E:Ljava/lang/Runnable;

    iget-object v1, p0, LX/4dC;->d:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    invoke-virtual {p0, v0, v2, v3}, LX/4dC;->scheduleSelf(Ljava/lang/Runnable;J)V

    goto :goto_0
.end method

.method public final stop()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 795528
    iput-boolean v0, p0, LX/4dC;->D:Z

    .line 795529
    iput-boolean v0, p0, LX/4dC;->w:Z

    .line 795530
    return-void
.end method
