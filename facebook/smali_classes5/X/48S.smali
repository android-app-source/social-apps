.class public LX/48S;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/48R;


# instance fields
.field private final a:LX/48T;


# direct methods
.method public constructor <init>(LX/48T;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 672845
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 672846
    iput-object p1, p0, LX/48S;->a:LX/48T;

    .line 672847
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;ILandroid/app/Activity;)Z
    .locals 1

    .prologue
    .line 672848
    iget-object v0, p0, LX/48S;->a:LX/48T;

    invoke-virtual {v0, p1}, LX/48T;->a(Landroid/content/Intent;)V

    .line 672849
    invoke-virtual {p3, p1, p2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 672850
    const/4 v0, 0x1

    return v0
.end method

.method public final a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)Z
    .locals 1

    .prologue
    .line 672851
    iget-object v0, p0, LX/48S;->a:LX/48T;

    invoke-virtual {v0, p1}, LX/48T;->a(Landroid/content/Intent;)V

    .line 672852
    invoke-virtual {p3, p1, p2}, Landroid/support/v4/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 672853
    const/4 v0, 0x1

    return v0
.end method

.method public final a(Landroid/content/Intent;Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 672854
    iget-object v0, p0, LX/48S;->a:LX/48T;

    invoke-virtual {v0, p1}, LX/48T;->a(Landroid/content/Intent;)V

    .line 672855
    invoke-virtual {p2, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 672856
    const/4 v0, 0x1

    return v0
.end method
