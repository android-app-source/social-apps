.class public LX/3nb;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 639020
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 639021
    const/4 v0, 0x0

    .line 639022
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v4, :cond_7

    .line 639023
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 639024
    :goto_0
    return v1

    .line 639025
    :cond_0
    const-string v8, "cover_type"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 639026
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;

    move-result-object v0

    move-object v4, v0

    move v0, v2

    .line 639027
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_5

    .line 639028
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 639029
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 639030
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_1

    if-eqz v7, :cond_1

    .line 639031
    const-string v8, "cover_overlay_text"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 639032
    invoke-static {p0, p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 639033
    :cond_2
    const-string v8, "cover_theme_color"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 639034
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 639035
    :cond_3
    const-string v8, "cover_overlay_image"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 639036
    invoke-static {p0, p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 639037
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 639038
    :cond_5
    const/4 v7, 0x4

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 639039
    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 639040
    invoke-virtual {p1, v2, v5}, LX/186;->b(II)V

    .line 639041
    if-eqz v0, :cond_6

    .line 639042
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v4}, LX/186;->a(ILjava/lang/Enum;)V

    .line 639043
    :cond_6
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 639044
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_7
    move v3, v1

    move-object v4, v0

    move v5, v1

    move v6, v1

    move v0, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 639001
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 639002
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 639003
    if-eqz v0, :cond_0

    .line 639004
    const-string v1, "cover_overlay_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 639005
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 639006
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 639007
    if-eqz v0, :cond_1

    .line 639008
    const-string v1, "cover_theme_color"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 639009
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 639010
    :cond_1
    invoke-virtual {p0, p1, v3, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 639011
    if-eqz v0, :cond_2

    .line 639012
    const-string v0, "cover_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 639013
    const-class v0, Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;

    invoke-virtual {p0, p1, v3, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLGroupsFeedUnitCoverType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 639014
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 639015
    if-eqz v0, :cond_3

    .line 639016
    const-string v1, "cover_overlay_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 639017
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 639018
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 639019
    return-void
.end method
