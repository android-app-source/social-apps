.class public LX/4h7;
.super LX/31y;
.source ""


# instance fields
.field public b:LX/282;

.field public final c:LX/282;

.field public d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/282;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 800466
    invoke-direct {p0, p1}, LX/31y;-><init>(Ljava/lang/String;)V

    .line 800467
    iput-object p2, p0, LX/4h7;->c:LX/282;

    .line 800468
    iput-object p3, p0, LX/4h7;->d:Ljava/lang/String;

    .line 800469
    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 800470
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "{src_pkg="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 800471
    iget-object v1, p0, LX/31y;->b:Ljava/lang/String;

    move-object v1, v1

    .line 800472
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", phone_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 800473
    iget-object v0, p0, LX/4h7;->b:LX/282;

    move-object v0, v0

    .line 800474
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, LX/31y;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", duration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, LX/31y;->g()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", prev_phone_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 800475
    iget-object v1, p0, LX/4h7;->c:LX/282;

    move-object v1, v1

    .line 800476
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", sync_medium="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 800477
    iget-object v1, p0, LX/4h7;->d:Ljava/lang/String;

    move-object v1, v1

    .line 800478
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 800479
    :cond_0
    iget-object v0, p0, LX/4h7;->b:LX/282;

    move-object v0, v0

    .line 800480
    invoke-virtual {v0}, LX/282;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
