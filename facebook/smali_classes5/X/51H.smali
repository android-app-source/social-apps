.class public final LX/51H;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Lcom/google/common/collect/Multiset$Entry",
        "<TE;>;>;"
    }
.end annotation


# instance fields
.field public a:LX/51M;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/51M",
            "<TE;>;"
        }
    .end annotation
.end field

.field public b:LX/4wx;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/Multiset$Entry",
            "<TE;>;"
        }
    .end annotation
.end field

.field public final synthetic c:LX/51O;


# direct methods
.method public constructor <init>(LX/51O;)V
    .locals 5

    .prologue
    .line 824553
    iput-object p1, p0, LX/51H;->c:LX/51O;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 824554
    iget-object v0, p0, LX/51H;->c:LX/51O;

    const/4 v2, 0x0

    .line 824555
    iget-object v1, v0, LX/51O;->a:LX/51N;

    .line 824556
    iget-object v3, v1, LX/51N;->a:Ljava/lang/Object;

    move-object v1, v3

    .line 824557
    check-cast v1, LX/51M;

    .line 824558
    if-nez v1, :cond_1

    move-object v1, v2

    .line 824559
    :cond_0
    :goto_0
    move-object v0, v1

    .line 824560
    iput-object v0, p0, LX/51H;->a:LX/51M;

    .line 824561
    const/4 v0, 0x0

    iput-object v0, p0, LX/51H;->b:LX/4wx;

    return-void

    .line 824562
    :cond_1
    iget-object v1, v0, LX/51O;->b:LX/4xo;

    .line 824563
    iget-boolean v3, v1, LX/4xo;->hasUpperBound:Z

    move v1, v3

    .line 824564
    if-eqz v1, :cond_5

    .line 824565
    iget-object v1, v0, LX/51O;->b:LX/4xo;

    .line 824566
    iget-object v3, v1, LX/4xo;->upperEndpoint:Ljava/lang/Object;

    move-object v3, v3

    .line 824567
    iget-object v1, v0, LX/51O;->a:LX/51N;

    .line 824568
    iget-object v4, v1, LX/51N;->a:Ljava/lang/Object;

    move-object v1, v4

    .line 824569
    check-cast v1, LX/51M;

    invoke-virtual {v0}, LX/4xC;->comparator()Ljava/util/Comparator;

    move-result-object v4

    invoke-static {v1, v4, v3}, LX/51M;->c(LX/51M;Ljava/util/Comparator;Ljava/lang/Object;)LX/51M;

    move-result-object v1

    .line 824570
    if-nez v1, :cond_2

    move-object v1, v2

    .line 824571
    goto :goto_0

    .line 824572
    :cond_2
    iget-object v4, v0, LX/51O;->b:LX/4xo;

    .line 824573
    iget-object p1, v4, LX/4xo;->upperBoundType:LX/4xG;

    move-object v4, p1

    .line 824574
    sget-object p1, LX/4xG;->OPEN:LX/4xG;

    if-ne v4, p1, :cond_3

    invoke-virtual {v0}, LX/4xC;->comparator()Ljava/util/Comparator;

    move-result-object v4

    invoke-virtual {v1}, LX/51M;->a()Ljava/lang/Object;

    move-result-object p1

    invoke-interface {v4, v3, p1}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v3

    if-nez v3, :cond_3

    .line 824575
    iget-object v1, v1, LX/51M;->h:LX/51M;

    .line 824576
    :cond_3
    :goto_1
    iget-object v3, v0, LX/51O;->c:LX/51M;

    if-eq v1, v3, :cond_4

    iget-object v3, v0, LX/51O;->b:LX/4xo;

    invoke-virtual {v1}, LX/51M;->a()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/4xo;->c(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_4
    move-object v1, v2

    goto :goto_0

    .line 824577
    :cond_5
    iget-object v1, v0, LX/51O;->c:LX/51M;

    iget-object v1, v1, LX/51M;->h:LX/51M;

    goto :goto_1
.end method


# virtual methods
.method public final hasNext()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 824578
    iget-object v1, p0, LX/51H;->a:LX/51M;

    if-nez v1, :cond_0

    .line 824579
    :goto_0
    return v0

    .line 824580
    :cond_0
    iget-object v1, p0, LX/51H;->c:LX/51O;

    iget-object v1, v1, LX/51O;->b:LX/4xo;

    iget-object v2, p0, LX/51H;->a:LX/51M;

    invoke-virtual {v2}, LX/51M;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/4xo;->a(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 824581
    const/4 v1, 0x0

    iput-object v1, p0, LX/51H;->a:LX/51M;

    goto :goto_0

    .line 824582
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final next()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 824583
    invoke-virtual {p0}, LX/51H;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 824584
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 824585
    :cond_0
    iget-object v0, p0, LX/51H;->c:LX/51O;

    iget-object v1, p0, LX/51H;->a:LX/51M;

    invoke-static {v0, v1}, LX/51O;->b(LX/51O;LX/51M;)LX/4wx;

    move-result-object v0

    .line 824586
    iput-object v0, p0, LX/51H;->b:LX/4wx;

    .line 824587
    iget-object v1, p0, LX/51H;->a:LX/51M;

    iget-object v1, v1, LX/51M;->h:LX/51M;

    iget-object v2, p0, LX/51H;->c:LX/51O;

    iget-object v2, v2, LX/51O;->c:LX/51M;

    if-ne v1, v2, :cond_1

    .line 824588
    const/4 v1, 0x0

    iput-object v1, p0, LX/51H;->a:LX/51M;

    .line 824589
    :goto_0
    return-object v0

    .line 824590
    :cond_1
    iget-object v1, p0, LX/51H;->a:LX/51M;

    iget-object v1, v1, LX/51M;->h:LX/51M;

    iput-object v1, p0, LX/51H;->a:LX/51M;

    goto :goto_0
.end method

.method public final remove()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 824591
    iget-object v0, p0, LX/51H;->b:LX/4wx;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0P6;->a(Z)V

    .line 824592
    iget-object v0, p0, LX/51H;->c:LX/51O;

    iget-object v2, p0, LX/51H;->b:LX/4wx;

    invoke-virtual {v2}, LX/4wx;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, LX/1M0;->c(Ljava/lang/Object;I)I

    .line 824593
    const/4 v0, 0x0

    iput-object v0, p0, LX/51H;->b:LX/4wx;

    .line 824594
    return-void

    :cond_0
    move v0, v1

    .line 824595
    goto :goto_0
.end method
