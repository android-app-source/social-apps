.class public LX/3l0;
.super LX/3ko;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field public final b:LX/3kp;

.field public final c:LX/3l1;

.field private final d:LX/0ad;

.field public e:LX/0hs;

.field public f:Z

.field public g:I

.field public h:LX/BHh;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/3kp;LX/3l1;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 633437
    invoke-direct {p0}, LX/3ko;-><init>()V

    .line 633438
    iput-object p1, p0, LX/3l0;->a:Landroid/content/Context;

    .line 633439
    iput-object p2, p0, LX/3l0;->b:LX/3kp;

    .line 633440
    iput-object p3, p0, LX/3l0;->c:LX/3l1;

    .line 633441
    iput-object p4, p0, LX/3l0;->d:LX/0ad;

    .line 633442
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 633443
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/10S;
    .locals 4

    .prologue
    .line 633444
    iget-object v0, p0, LX/3l0;->d:LX/0ad;

    sget-object v1, LX/0c0;->Live:LX/0c0;

    sget-short v2, LX/1EB;->P:S

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 633445
    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;

    .line 633446
    :goto_0
    return-object v0

    :cond_0
    iget-boolean v0, p0, LX/3l0;->f:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/3l0;->b:LX/3kp;

    invoke-virtual {v0}, LX/3kp;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/3l0;->h:LX/BHh;

    if-eqz v0, :cond_1

    sget-object v0, LX/10S;->ELIGIBLE:LX/10S;

    goto :goto_0

    :cond_1
    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 633447
    const-string v0, "4194"

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 633448
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PHOTO_PICKER:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 633449
    const/4 v0, 0x0

    iput-object v0, p0, LX/3l0;->h:LX/BHh;

    .line 633450
    return-void
.end method

.method public final e()V
    .locals 6

    .prologue
    .line 633451
    iget-object v0, p0, LX/3l0;->h:LX/BHh;

    if-nez v0, :cond_0

    .line 633452
    :goto_0
    return-void

    .line 633453
    :cond_0
    new-instance v0, LX/0hs;

    iget-object v1, p0, LX/3l0;->a:Landroid/content/Context;

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, LX/0hs;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, LX/3l0;->e:LX/0hs;

    .line 633454
    iget-object v0, p0, LX/3l0;->e:LX/0hs;

    const/4 v1, -0x1

    .line 633455
    iput v1, v0, LX/0hs;->t:I

    .line 633456
    iget-object v0, p0, LX/3l0;->e:LX/0hs;

    new-instance v1, LX/BI8;

    invoke-direct {v1, p0}, LX/BI8;-><init>(LX/3l0;)V

    invoke-virtual {v0, v1}, LX/0hs;->a(LX/5Od;)V

    .line 633457
    iget-object v0, p0, LX/3l0;->e:LX/0hs;

    new-instance v1, LX/BI9;

    invoke-direct {v1, p0}, LX/BI9;-><init>(LX/3l0;)V

    .line 633458
    iput-object v1, v0, LX/0ht;->H:LX/2dD;

    .line 633459
    iget-object v0, p0, LX/3l0;->e:LX/0hs;

    sget-object v1, LX/3AV;->BELOW:LX/3AV;

    invoke-virtual {v0, v1}, LX/0ht;->a(LX/3AV;)V

    .line 633460
    iget-object v0, p0, LX/3l0;->e:LX/0hs;

    iget-object v1, p0, LX/3l0;->a:Landroid/content/Context;

    const v2, 0x7f08137c

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0hs;->a(Ljava/lang/CharSequence;)V

    .line 633461
    iget-object v0, p0, LX/3l0;->e:LX/0hs;

    iget-object v1, p0, LX/3l0;->a:Landroid/content/Context;

    const v2, 0x7f08137d

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget v5, p0, LX/3l0;->g:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0hs;->b(Ljava/lang/CharSequence;)V

    .line 633462
    iget-object v0, p0, LX/3l0;->c:LX/3l1;

    .line 633463
    sget-object v1, LX/BOR;->PICKER_NUX_SEEN:LX/BOR;

    invoke-static {v1}, LX/3l1;->a(LX/BOR;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-static {v0, v1}, LX/3l1;->a(LX/3l1;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 633464
    iget-object v0, p0, LX/3l0;->h:LX/BHh;

    .line 633465
    iget-object v1, v0, LX/BHh;->f:Landroid/animation/AnimatorSet;

    if-eqz v1, :cond_1

    iget-object v1, v0, LX/BHh;->f:Landroid/animation/AnimatorSet;

    invoke-virtual {v1}, Landroid/animation/AnimatorSet;->isRunning()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 633466
    iget-object v1, v0, LX/BHh;->f:Landroid/animation/AnimatorSet;

    invoke-virtual {v1}, Landroid/animation/AnimatorSet;->end()V

    .line 633467
    :cond_1
    iget-object v1, v0, LX/BHh;->c:Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;

    if-nez v1, :cond_4

    .line 633468
    invoke-static {v0}, LX/BHh;->d(LX/BHh;)V

    .line 633469
    :cond_2
    iget-object v1, v0, LX/BHh;->g:Landroid/animation/AnimatorSet;

    if-nez v1, :cond_3

    .line 633470
    invoke-static {v0}, LX/BHh;->f(LX/BHh;)Landroid/animation/AnimatorSet;

    move-result-object v1

    iput-object v1, v0, LX/BHh;->g:Landroid/animation/AnimatorSet;

    .line 633471
    :cond_3
    iget-object v1, v0, LX/BHh;->g:Landroid/animation/AnimatorSet;

    iput-object v1, v0, LX/BHh;->f:Landroid/animation/AnimatorSet;

    .line 633472
    iget-object v1, v0, LX/BHh;->f:Landroid/animation/AnimatorSet;

    invoke-virtual {v1}, Landroid/animation/AnimatorSet;->start()V

    .line 633473
    :goto_1
    iget-object v0, p0, LX/3l0;->e:LX/0hs;

    iget-object v1, p0, LX/3l0;->h:LX/BHh;

    .line 633474
    iget-object v2, v1, LX/BHh;->c:Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;

    const v3, 0x7f0d2cab

    invoke-virtual {v2, v3}, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object v1, v2

    .line 633475
    invoke-virtual {v0, v1}, LX/0ht;->f(Landroid/view/View;)V

    goto/16 :goto_0

    .line 633476
    :cond_4
    iget-object v1, v0, LX/BHh;->c:Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;

    invoke-virtual {v1}, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_2

    goto :goto_1
.end method
