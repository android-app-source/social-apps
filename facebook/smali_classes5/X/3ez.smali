.class public final LX/3ez;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/pages/adminedpages/backgroundtasks/AdminedPagesPrefetchBackgroundTask;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/adminedpages/backgroundtasks/AdminedPagesPrefetchBackgroundTask;)V
    .locals 0

    .prologue
    .line 621181
    iput-object p1, p0, LX/3ez;->a:Lcom/facebook/pages/adminedpages/backgroundtasks/AdminedPagesPrefetchBackgroundTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 621182
    iget-object v0, p0, LX/3ez;->a:Lcom/facebook/pages/adminedpages/backgroundtasks/AdminedPagesPrefetchBackgroundTask;

    iget-object v0, v0, Lcom/facebook/pages/adminedpages/backgroundtasks/AdminedPagesPrefetchBackgroundTask;->g:LX/2U1;

    invoke-virtual {v0}, LX/2U2;->b()I

    move-result v0

    .line 621183
    iget-object v1, p0, LX/3ez;->a:Lcom/facebook/pages/adminedpages/backgroundtasks/AdminedPagesPrefetchBackgroundTask;

    iget-object v1, v1, Lcom/facebook/pages/adminedpages/backgroundtasks/AdminedPagesPrefetchBackgroundTask;->g:LX/2U1;

    invoke-virtual {v1}, LX/2U1;->clearUserData()V

    .line 621184
    iget-object v1, p0, LX/3ez;->a:Lcom/facebook/pages/adminedpages/backgroundtasks/AdminedPagesPrefetchBackgroundTask;

    iget-object v1, v1, Lcom/facebook/pages/adminedpages/backgroundtasks/AdminedPagesPrefetchBackgroundTask;->e:LX/03V;

    const-string v2, "page_data_fetch"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Prefetch admined Pages fail, number of entries cleared: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 621185
    return-void
.end method

.method public final bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 621186
    return-void
.end method
