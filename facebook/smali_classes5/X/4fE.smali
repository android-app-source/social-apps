.class public final LX/4fE;
.super LX/1eP;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1eP",
        "<",
        "LX/1FJ",
        "<",
        "LX/1ln;",
        ">;",
        "LX/1FJ",
        "<",
        "LX/1ln;",
        ">;>;",
        "Lcom/facebook/imagepipeline/request/RepeatedPostprocessorRunner;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/4fG;

.field private b:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "RepeatedPostprocessorConsumer.this"
    .end annotation
.end field

.field private c:LX/1FJ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "RepeatedPostprocessorConsumer.this"
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/4fG;LX/4fC;LX/4fO;LX/1cW;)V
    .locals 1

    .prologue
    .line 798212
    iput-object p1, p0, LX/4fE;->a:LX/4fG;

    .line 798213
    invoke-direct {p0, p2}, LX/1eP;-><init>(LX/1cd;)V

    .line 798214
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/4fE;->b:Z

    .line 798215
    const/4 v0, 0x0

    iput-object v0, p0, LX/4fE;->c:LX/1FJ;

    .line 798216
    invoke-virtual {p3, p0}, LX/4fO;->a(LX/4fE;)V

    .line 798217
    new-instance v0, LX/4fD;

    invoke-direct {v0, p0, p1}, LX/4fD;-><init>(LX/4fE;LX/4fG;)V

    invoke-virtual {p4, v0}, LX/1cW;->a(LX/1cg;)V

    .line 798218
    return-void
.end method

.method private a(LX/1FJ;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 798203
    monitor-enter p0

    .line 798204
    :try_start_0
    iget-boolean v0, p0, LX/4fE;->b:Z

    if-eqz v0, :cond_0

    .line 798205
    monitor-exit p0

    .line 798206
    :goto_0
    return-void

    .line 798207
    :cond_0
    iget-object v0, p0, LX/4fE;->c:LX/1FJ;

    .line 798208
    invoke-static {p1}, LX/1FJ;->b(LX/1FJ;)LX/1FJ;

    move-result-object v1

    iput-object v1, p0, LX/4fE;->c:LX/1FJ;

    .line 798209
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 798210
    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    goto :goto_0

    .line 798211
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private static e(LX/4fE;)V
    .locals 3

    .prologue
    .line 798192
    monitor-enter p0

    .line 798193
    :try_start_0
    iget-boolean v0, p0, LX/4fE;->b:Z

    if-eqz v0, :cond_0

    .line 798194
    monitor-exit p0

    .line 798195
    :goto_0
    return-void

    .line 798196
    :cond_0
    iget-object v0, p0, LX/4fE;->c:LX/1FJ;

    invoke-static {v0}, LX/1FJ;->b(LX/1FJ;)LX/1FJ;

    move-result-object v1

    .line 798197
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 798198
    :try_start_1
    iget-object v0, p0, LX/1eP;->a:LX/1cd;

    move-object v0, v0

    .line 798199
    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/1cd;->b(Ljava/lang/Object;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 798200
    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    goto :goto_0

    .line 798201
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 798202
    :catchall_1
    move-exception v0

    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    throw v0
.end method

.method public static f(LX/4fE;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 798182
    monitor-enter p0

    .line 798183
    :try_start_0
    iget-boolean v1, p0, LX/4fE;->b:Z

    if-eqz v1, :cond_0

    .line 798184
    const/4 v0, 0x0

    monitor-exit p0

    .line 798185
    :goto_0
    return v0

    .line 798186
    :cond_0
    iget-object v1, p0, LX/4fE;->c:LX/1FJ;

    .line 798187
    const/4 v2, 0x0

    iput-object v2, p0, LX/4fE;->c:LX/1FJ;

    .line 798188
    const/4 v2, 0x1

    iput-boolean v2, p0, LX/4fE;->b:Z

    .line 798189
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 798190
    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    goto :goto_0

    .line 798191
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 798178
    invoke-static {p0}, LX/4fE;->f(LX/4fE;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 798179
    iget-object v0, p0, LX/1eP;->a:LX/1cd;

    move-object v0, v0

    .line 798180
    invoke-virtual {v0}, LX/1cd;->b()V

    .line 798181
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 798166
    check-cast p1, LX/1FJ;

    .line 798167
    if-nez p2, :cond_0

    .line 798168
    :goto_0
    return-void

    .line 798169
    :cond_0
    invoke-direct {p0, p1}, LX/4fE;->a(LX/1FJ;)V

    .line 798170
    invoke-static {p0}, LX/4fE;->e(LX/4fE;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 798174
    invoke-static {p0}, LX/4fE;->f(LX/4fE;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 798175
    iget-object v0, p0, LX/1eP;->a:LX/1cd;

    move-object v0, v0

    .line 798176
    invoke-virtual {v0, p1}, LX/1cd;->b(Ljava/lang/Throwable;)V

    .line 798177
    :cond_0
    return-void
.end method

.method public final declared-synchronized c()V
    .locals 1

    .prologue
    .line 798171
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/4fE;->e(LX/4fE;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 798172
    monitor-exit p0

    return-void

    .line 798173
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
