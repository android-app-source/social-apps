.class public LX/4Kx;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 681386
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 681387
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_b

    .line 681388
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 681389
    :goto_0
    return v1

    .line 681390
    :cond_0
    const-string v12, "eligible_for_audience_alignment_education"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 681391
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v5

    move v9, v5

    move v5, v2

    .line 681392
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_6

    .line 681393
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 681394
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 681395
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_1

    if-eqz v11, :cond_1

    .line 681396
    const-string v12, "composer_privacy_guardrail_info"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 681397
    invoke-static {p0, p1}, LX/4LP;->a(LX/15w;LX/186;)I

    move-result v10

    goto :goto_1

    .line 681398
    :cond_2
    const-string v12, "eligible_for_audience_alignment_only_me_education"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 681399
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v4

    move v8, v4

    move v4, v2

    goto :goto_1

    .line 681400
    :cond_3
    const-string v12, "eligible_for_newcomer_audience_selector"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 681401
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v3

    move v7, v3

    move v3, v2

    goto :goto_1

    .line 681402
    :cond_4
    const-string v12, "has_default_privacy"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 681403
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v6, v0

    move v0, v2

    goto :goto_1

    .line 681404
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 681405
    :cond_6
    const/4 v11, 0x5

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 681406
    invoke-virtual {p1, v1, v10}, LX/186;->b(II)V

    .line 681407
    if-eqz v5, :cond_7

    .line 681408
    invoke-virtual {p1, v2, v9}, LX/186;->a(IZ)V

    .line 681409
    :cond_7
    if-eqz v4, :cond_8

    .line 681410
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v8}, LX/186;->a(IZ)V

    .line 681411
    :cond_8
    if-eqz v3, :cond_9

    .line 681412
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v7}, LX/186;->a(IZ)V

    .line 681413
    :cond_9
    if-eqz v0, :cond_a

    .line 681414
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v6}, LX/186;->a(IZ)V

    .line 681415
    :cond_a
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_b
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    move v9, v1

    move v10, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 681416
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 681417
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 681418
    if-eqz v0, :cond_0

    .line 681419
    const-string v1, "composer_privacy_guardrail_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 681420
    invoke-static {p0, v0, p2, p3}, LX/4LP;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 681421
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 681422
    if-eqz v0, :cond_1

    .line 681423
    const-string v1, "eligible_for_audience_alignment_education"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 681424
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 681425
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 681426
    if-eqz v0, :cond_2

    .line 681427
    const-string v1, "eligible_for_audience_alignment_only_me_education"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 681428
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 681429
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 681430
    if-eqz v0, :cond_3

    .line 681431
    const-string v1, "eligible_for_newcomer_audience_selector"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 681432
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 681433
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 681434
    if-eqz v0, :cond_4

    .line 681435
    const-string v1, "has_default_privacy"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 681436
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 681437
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 681438
    return-void
.end method
