.class public LX/4cL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lorg/apache/http/HttpEntity;


# static fields
.field public static final a:[C


# instance fields
.field public final b:LX/4cU;

.field private final c:Lorg/apache/http/Header;

.field private d:J

.field public volatile e:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 794903
    const-string v0, "-_1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, LX/4cL;->a:[C

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 794901
    sget-object v0, LX/4cV;->STRICT:LX/4cV;

    invoke-direct {p0, v0, v1, v1}, LX/4cL;-><init>(LX/4cV;Ljava/lang/String;Ljava/nio/charset/Charset;)V

    .line 794902
    return-void
.end method

.method private constructor <init>(LX/4cV;Ljava/lang/String;Ljava/nio/charset/Charset;)V
    .locals 5
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/nio/charset/Charset;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 794878
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 794879
    if-nez p2, :cond_1

    .line 794880
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 794881
    new-instance v2, Ljava/util/Random;

    invoke-direct {v2}, Ljava/util/Random;-><init>()V

    .line 794882
    const/16 v0, 0xb

    invoke-virtual {v2, v0}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    add-int/lit8 v3, v0, 0x1e

    .line 794883
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    .line 794884
    sget-object v4, LX/4cL;->a:[C

    sget-object p2, LX/4cL;->a:[C

    array-length p2, p2

    invoke-virtual {v2, p2}, Ljava/util/Random;->nextInt(I)I

    move-result p2

    aget-char v4, v4, p2

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 794885
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 794886
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object p2, v0

    .line 794887
    :cond_1
    if-nez p1, :cond_2

    .line 794888
    sget-object p1, LX/4cV;->STRICT:LX/4cV;

    .line 794889
    :cond_2
    new-instance v0, LX/4cU;

    const-string v1, "form-data"

    invoke-direct {v0, v1, p3, p2, p1}, LX/4cU;-><init>(Ljava/lang/String;Ljava/nio/charset/Charset;Ljava/lang/String;LX/4cV;)V

    iput-object v0, p0, LX/4cL;->b:LX/4cU;

    .line 794890
    new-instance v0, Lorg/apache/http/message/BasicHeader;

    const-string v1, "Content-Type"

    .line 794891
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 794892
    const-string v3, "multipart/form-data; boundary="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 794893
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 794894
    if-eqz p3, :cond_3

    .line 794895
    const-string v3, "; charset="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 794896
    invoke-virtual {p3}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 794897
    :cond_3
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v2, v2

    .line 794898
    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, LX/4cL;->c:Lorg/apache/http/Header;

    .line 794899
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4cL;->e:Z

    .line 794900
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/4cO;)V
    .locals 1

    .prologue
    .line 794872
    new-instance v0, LX/4cQ;

    invoke-direct {v0, p1, p2}, LX/4cQ;-><init>(Ljava/lang/String;LX/4cO;)V

    .line 794873
    iget-object p1, p0, LX/4cL;->b:LX/4cU;

    .line 794874
    if-nez v0, :cond_0

    .line 794875
    :goto_0
    const/4 p1, 0x1

    iput-boolean p1, p0, LX/4cL;->e:Z

    .line 794876
    return-void

    .line 794877
    :cond_0
    iget-object p2, p1, LX/4cU;->g:Ljava/util/List;

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final consumeContent()V
    .locals 2

    .prologue
    .line 794869
    invoke-virtual {p0}, LX/4cL;->isStreaming()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 794870
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Streaming entity does not implement #consumeContent()"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 794871
    :cond_0
    return-void
.end method

.method public final getContent()Ljava/io/InputStream;
    .locals 2

    .prologue
    .line 794839
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Multipart form entity does not implement #getContent()"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final getContentEncoding()Lorg/apache/http/Header;
    .locals 1

    .prologue
    .line 794868
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getContentLength()J
    .locals 13

    .prologue
    .line 794853
    iget-boolean v0, p0, LX/4cL;->e:Z

    if-eqz v0, :cond_1

    .line 794854
    iget-object v0, p0, LX/4cL;->b:LX/4cU;

    const-wide/16 v6, 0x0

    const-wide/16 v8, -0x1

    .line 794855
    iget-object v2, v0, LX/4cU;->g:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    move-wide v4, v6

    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/4cQ;

    .line 794856
    iget-object v3, v2, LX/4cQ;->c:LX/4cO;

    move-object v2, v3

    .line 794857
    invoke-virtual {v2}, LX/4cO;->d()J

    move-result-wide v2

    .line 794858
    cmp-long v11, v2, v6

    if-ltz v11, :cond_0

    .line 794859
    add-long/2addr v2, v4

    move-wide v4, v2

    goto :goto_0

    :cond_0
    move-wide v2, v8

    .line 794860
    :goto_1
    move-wide v0, v2

    .line 794861
    iput-wide v0, p0, LX/4cL;->d:J

    .line 794862
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/4cL;->e:Z

    .line 794863
    :cond_1
    iget-wide v0, p0, LX/4cL;->d:J

    return-wide v0

    .line 794864
    :cond_2
    new-instance v2, LX/4cT;

    invoke-direct {v2}, LX/4cT;-><init>()V

    .line 794865
    :try_start_0
    iget-object v3, v0, LX/4cU;->h:LX/4cV;

    const/4 v6, 0x0

    invoke-static {v0, v3, v2, v6}, LX/4cU;->a(LX/4cU;LX/4cV;Ljava/io/OutputStream;Z)V

    .line 794866
    iget v2, v2, LX/4cT;->a:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    int-to-long v2, v2

    add-long/2addr v2, v4

    goto :goto_1

    .line 794867
    :catch_0
    move-wide v2, v8

    goto :goto_1
.end method

.method public final getContentType()Lorg/apache/http/Header;
    .locals 1

    .prologue
    .line 794852
    iget-object v0, p0, LX/4cL;->c:Lorg/apache/http/Header;

    return-object v0
.end method

.method public final isChunked()Z
    .locals 1

    .prologue
    .line 794851
    invoke-virtual {p0}, LX/4cL;->isRepeatable()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isRepeatable()Z
    .locals 6

    .prologue
    .line 794844
    iget-object v0, p0, LX/4cL;->b:LX/4cU;

    .line 794845
    iget-object v1, v0, LX/4cU;->g:Ljava/util/List;

    move-object v0, v1

    .line 794846
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4cQ;

    .line 794847
    iget-object v2, v0, LX/4cQ;->c:LX/4cO;

    move-object v0, v2

    .line 794848
    invoke-virtual {v0}, LX/4cO;->d()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-gez v0, :cond_0

    .line 794849
    const/4 v0, 0x0

    .line 794850
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final isStreaming()Z
    .locals 1

    .prologue
    .line 794843
    invoke-virtual {p0}, LX/4cL;->isRepeatable()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public writeTo(Ljava/io/OutputStream;)V
    .locals 2

    .prologue
    .line 794840
    iget-object v0, p0, LX/4cL;->b:LX/4cU;

    .line 794841
    iget-object v1, v0, LX/4cU;->h:LX/4cV;

    const/4 p0, 0x1

    invoke-static {v0, v1, p1, p0}, LX/4cU;->a(LX/4cU;LX/4cV;Ljava/io/OutputStream;Z)V

    .line 794842
    return-void
.end method
