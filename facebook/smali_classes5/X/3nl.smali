.class public LX/3nl;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile g:LX/3nl;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/376;

.field public final c:LX/378;

.field public final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/7Rz;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:LX/7Ru;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/376;LX/378;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 639161
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 639162
    iput-object p1, p0, LX/3nl;->a:Landroid/content/Context;

    .line 639163
    iput-object p2, p0, LX/3nl;->b:LX/376;

    .line 639164
    iput-object p3, p0, LX/3nl;->c:LX/378;

    .line 639165
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/3nl;->d:Ljava/util/List;

    .line 639166
    return-void
.end method

.method public static a(LX/0QB;)LX/3nl;
    .locals 7

    .prologue
    .line 639145
    sget-object v0, LX/3nl;->g:LX/3nl;

    if-nez v0, :cond_1

    .line 639146
    const-class v1, LX/3nl;

    monitor-enter v1

    .line 639147
    :try_start_0
    sget-object v0, LX/3nl;->g:LX/3nl;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 639148
    if-eqz v2, :cond_0

    .line 639149
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 639150
    new-instance v6, LX/3nl;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    .line 639151
    new-instance p0, LX/376;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v5

    check-cast v5, LX/0So;

    invoke-direct {p0, v4, v5}, LX/376;-><init>(LX/03V;LX/0So;)V

    .line 639152
    move-object v4, p0

    .line 639153
    check-cast v4, LX/376;

    invoke-static {v0}, LX/378;->a(LX/0QB;)LX/378;

    move-result-object v5

    check-cast v5, LX/378;

    invoke-direct {v6, v3, v4, v5}, LX/3nl;-><init>(Landroid/content/Context;LX/376;LX/378;)V

    .line 639154
    move-object v0, v6

    .line 639155
    sput-object v0, LX/3nl;->g:LX/3nl;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 639156
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 639157
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 639158
    :cond_1
    sget-object v0, LX/3nl;->g:LX/3nl;

    return-object v0

    .line 639159
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 639160
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static i(LX/3nl;)V
    .locals 2

    .prologue
    .line 639136
    invoke-static {p0}, LX/3nl;->j(LX/3nl;)LX/7S1;

    .line 639137
    iget-object v0, p0, LX/3nl;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_0

    .line 639138
    :cond_0
    return-void
.end method

.method public static j(LX/3nl;)LX/7S1;
    .locals 2

    .prologue
    .line 639144
    new-instance v0, LX/7S1;

    iget-object v1, p0, LX/3nl;->b:LX/376;

    invoke-direct {v0, v1}, LX/7S1;-><init>(LX/376;)V

    return-object v0
.end method

.method public static k(LX/3nl;)V
    .locals 2

    .prologue
    .line 639139
    iget-object v0, p0, LX/3nl;->e:LX/7Rz;

    if-nez v0, :cond_0

    .line 639140
    new-instance v0, LX/7Rz;

    invoke-direct {v0, p0}, LX/7Rz;-><init>(LX/3nl;)V

    iput-object v0, p0, LX/3nl;->e:LX/7Rz;

    .line 639141
    :cond_0
    iget-object v0, p0, LX/3nl;->e:LX/7Rz;

    iget-object v1, p0, LX/3nl;->a:Landroid/content/Context;

    .line 639142
    iget-object p0, v0, LX/7Rz;->b:Landroid/content/IntentFilter;

    invoke-virtual {v1, v0, p0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 639143
    return-void
.end method
