.class public LX/3oj;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/3oi;


# direct methods
.method public constructor <init>(LX/3oi;)V
    .locals 0

    .prologue
    .line 640911
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 640912
    iput-object p1, p0, LX/3oj;->a:LX/3oi;

    .line 640913
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 640909
    iget-object v0, p0, LX/3oj;->a:LX/3oi;

    invoke-virtual {v0}, LX/3oi;->a()V

    .line 640910
    return-void
.end method

.method public final a(FF)V
    .locals 1

    .prologue
    .line 640907
    iget-object v0, p0, LX/3oj;->a:LX/3oi;

    invoke-virtual {v0, p1, p2}, LX/3oi;->a(FF)V

    .line 640908
    return-void
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 640905
    iget-object v0, p0, LX/3oj;->a:LX/3oi;

    invoke-virtual {v0, p1}, LX/3oi;->a(I)V

    .line 640906
    return-void
.end method

.method public final a(II)V
    .locals 1

    .prologue
    .line 640903
    iget-object v0, p0, LX/3oj;->a:LX/3oi;

    invoke-virtual {v0, p1, p2}, LX/3oi;->a(II)V

    .line 640904
    return-void
.end method

.method public final a(LX/3nt;)V
    .locals 2

    .prologue
    .line 640899
    if-eqz p1, :cond_0

    .line 640900
    iget-object v0, p0, LX/3oj;->a:LX/3oi;

    new-instance v1, LX/3oh;

    invoke-direct {v1, p0, p1}, LX/3oh;-><init>(LX/3oj;LX/3nt;)V

    invoke-virtual {v0, v1}, LX/3oi;->a(LX/3og;)V

    .line 640901
    :goto_0
    return-void

    .line 640902
    :cond_0
    iget-object v0, p0, LX/3oj;->a:LX/3oi;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/3oi;->a(LX/3og;)V

    goto :goto_0
.end method

.method public final a(Landroid/view/animation/Interpolator;)V
    .locals 1

    .prologue
    .line 640892
    iget-object v0, p0, LX/3oj;->a:LX/3oi;

    invoke-virtual {v0, p1}, LX/3oi;->a(Landroid/view/animation/Interpolator;)V

    .line 640893
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 640898
    iget-object v0, p0, LX/3oj;->a:LX/3oi;

    invoke-virtual {v0}, LX/3oi;->b()Z

    move-result v0

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 640897
    iget-object v0, p0, LX/3oj;->a:LX/3oi;

    invoke-virtual {v0}, LX/3oi;->c()I

    move-result v0

    return v0
.end method

.method public final d()F
    .locals 1

    .prologue
    .line 640896
    iget-object v0, p0, LX/3oj;->a:LX/3oi;

    invoke-virtual {v0}, LX/3oi;->d()F

    move-result v0

    return v0
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 640894
    iget-object v0, p0, LX/3oj;->a:LX/3oi;

    invoke-virtual {v0}, LX/3oi;->e()V

    .line 640895
    return-void
.end method
