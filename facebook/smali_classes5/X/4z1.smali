.class public final LX/4z1;
.super LX/0vW;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LX/0vW",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field public transient a:I
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public transient b:LX/4yy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4yy",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(II)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 822237
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0, p1}, Ljava/util/LinkedHashMap;-><init>(I)V

    invoke-direct {p0, v0}, LX/0vW;-><init>(Ljava/util/Map;)V

    .line 822238
    const/4 v0, 0x2

    iput v0, p0, LX/4z1;->a:I

    .line 822239
    const-string v0, "expectedValuesPerKey"

    invoke-static {p2, v0}, LX/0P6;->a(ILjava/lang/String;)I

    .line 822240
    iput p2, p0, LX/4z1;->a:I

    .line 822241
    new-instance v0, LX/4yy;

    const/4 v1, 0x0

    invoke-direct {v0, v2, v2, v1, v2}, LX/4yy;-><init>(Ljava/lang/Object;Ljava/lang/Object;ILX/4yy;)V

    iput-object v0, p0, LX/4z1;->b:LX/4yy;

    .line 822242
    iget-object v0, p0, LX/4z1;->b:LX/4yy;

    iget-object v1, p0, LX/4z1;->b:LX/4yy;

    invoke-static {v0, v1}, LX/4z1;->b(LX/4yy;LX/4yy;)V

    .line 822243
    return-void
.end method

.method public static b(LX/4yx;LX/4yx;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "LX/4yx",
            "<TK;TV;>;",
            "LX/4yx",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 822244
    invoke-interface {p0, p1}, LX/4yx;->b(LX/4yx;)V

    .line 822245
    invoke-interface {p1, p0}, LX/4yx;->a(LX/4yx;)V

    .line 822246
    return-void
.end method

.method public static b(LX/4yy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "LX/4yy",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 822247
    iget-object v0, p0, LX/4yy;->predecessorInMultimap:LX/4yy;

    move-object v0, v0

    .line 822248
    iget-object v1, p0, LX/4yy;->successorInMultimap:LX/4yy;

    move-object v1, v1

    .line 822249
    invoke-static {v0, v1}, LX/4z1;->b(LX/4yy;LX/4yy;)V

    .line 822250
    return-void
.end method

.method public static b(LX/4yy;LX/4yy;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "LX/4yy",
            "<TK;TV;>;",
            "LX/4yy",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 822251
    iput-object p1, p0, LX/4yy;->successorInMultimap:LX/4yy;

    .line 822252
    iput-object p0, p1, LX/4yy;->predecessorInMultimap:LX/4yy;

    .line 822253
    return-void
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 6
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "java.io.ObjectInputStream"
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 822254
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 822255
    new-instance v1, LX/4yy;

    invoke-direct {v1, v2, v2, v0, v2}, LX/4yy;-><init>(Ljava/lang/Object;Ljava/lang/Object;ILX/4yy;)V

    iput-object v1, p0, LX/4z1;->b:LX/4yy;

    .line 822256
    iget-object v1, p0, LX/4z1;->b:LX/4yy;

    iget-object v2, p0, LX/4z1;->b:LX/4yy;

    invoke-static {v1, v2}, LX/4z1;->b(LX/4yy;LX/4yy;)V

    .line 822257
    const/4 v1, 0x2

    iput v1, p0, LX/4z1;->a:I

    .line 822258
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v2

    .line 822259
    new-instance v3, Ljava/util/LinkedHashMap;

    invoke-direct {v3}, Ljava/util/LinkedHashMap;-><init>()V

    move v1, v0

    .line 822260
    :goto_0
    if-ge v1, v2, :cond_0

    .line 822261
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v4

    .line 822262
    invoke-virtual {p0, v4}, LX/4z1;->e(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 822263
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 822264
    :cond_0
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v2

    move v1, v0

    .line 822265
    :goto_1
    if-ge v1, v2, :cond_1

    .line 822266
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    .line 822267
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v4

    .line 822268
    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 822269
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 822270
    :cond_1
    invoke-virtual {p0, v3}, LX/0Xs;->a(Ljava/util/Map;)V

    .line 822271
    return-void
.end method

.method public static u()LX/4z1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">()",
            "LX/4z1",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 822272
    new-instance v0, LX/4z1;

    const/16 v1, 0x10

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, LX/4z1;-><init>(II)V

    return-object v0
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 3
    .annotation build Lcom/google/common/annotations/GwtIncompatible;
        value = "java.io.ObjectOutputStream"
    .end annotation

    .prologue
    .line 822273
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V

    .line 822274
    invoke-virtual {p0}, LX/0Xt;->p()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    .line 822275
    invoke-virtual {p0}, LX/0Xt;->p()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 822276
    invoke-virtual {p1, v1}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    goto :goto_0

    .line 822277
    :cond_0
    invoke-virtual {p0}, LX/0Xs;->f()I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    .line 822278
    invoke-virtual {p0}, LX/0vW;->t()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 822279
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 822280
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    goto :goto_1

    .line 822281
    :cond_1
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 822282
    new-instance v0, Ljava/util/LinkedHashSet;

    iget v1, p0, LX/4z1;->a:I

    invoke-direct {v0, v1}, Ljava/util/LinkedHashSet;-><init>(I)V

    return-object v0
.end method

.method public final bridge synthetic a(LX/0Xu;)Z
    .locals 1

    .prologue
    .line 822283
    invoke-super {p0, p1}, LX/0vW;->a(LX/0Xu;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Iterable;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 822284
    invoke-super {p0, p1, p2}, LX/0vW;->a(Ljava/lang/Object;Ljava/lang/Iterable;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic b(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 822285
    invoke-super {p0, p1, p2}, LX/0vW;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final synthetic c()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 822286
    invoke-virtual {p0}, LX/4z1;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic c(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 822236
    invoke-super {p0, p1, p2}, LX/0vW;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final e(Ljava/lang/Object;)Ljava/util/Collection;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)",
            "Ljava/util/Collection",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 822287
    new-instance v0, LX/4z0;

    iget v1, p0, LX/4z1;->a:I

    invoke-direct {v0, p0, p1, v1}, LX/4z0;-><init>(LX/4z1;Ljava/lang/Object;I)V

    return-object v0
.end method

.method public final bridge synthetic f()I
    .locals 1

    .prologue
    .line 822221
    invoke-super {p0}, LX/0vW;->f()I

    move-result v0

    return v0
.end method

.method public final bridge synthetic f(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 822222
    invoke-super {p0, p1}, LX/0vW;->f(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final g()V
    .locals 2

    .prologue
    .line 822223
    invoke-super {p0}, LX/0vW;->g()V

    .line 822224
    iget-object v0, p0, LX/4z1;->b:LX/4yy;

    iget-object v1, p0, LX/4z1;->b:LX/4yy;

    invoke-static {v0, v1}, LX/4z1;->b(LX/4yy;LX/4yy;)V

    .line 822225
    return-void
.end method

.method public final bridge synthetic g(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 822226
    invoke-super {p0, p1}, LX/0vW;->g(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic hashCode()I
    .locals 1

    .prologue
    .line 822227
    invoke-super {p0}, LX/0vW;->hashCode()I

    move-result v0

    return v0
.end method

.method public final i()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 822228
    invoke-super {p0}, LX/0vW;->i()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public final j()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 822229
    invoke-virtual {p0}, LX/4z1;->l()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/0PM;->b(Ljava/util/Iterator;)Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic k()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 822230
    invoke-virtual {p0}, LX/0vW;->t()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final l()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 822231
    new-instance v0, LX/4yw;

    invoke-direct {v0, p0}, LX/4yw;-><init>(LX/4z1;)V

    return-object v0
.end method

.method public final bridge synthetic n()Z
    .locals 1

    .prologue
    .line 822232
    invoke-super {p0}, LX/0vW;->n()Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic p()Ljava/util/Set;
    .locals 1

    .prologue
    .line 822233
    invoke-super {p0}, LX/0vW;->p()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic q()LX/1M1;
    .locals 1

    .prologue
    .line 822234
    invoke-super {p0}, LX/0vW;->q()LX/1M1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 822235
    invoke-super {p0}, LX/0vW;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
