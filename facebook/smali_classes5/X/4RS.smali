.class public LX/4RS;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 708936
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 17

    .prologue
    .line 708890
    const/4 v11, 0x0

    .line 708891
    const/4 v10, 0x0

    .line 708892
    const-wide/16 v8, 0x0

    .line 708893
    const/4 v7, 0x0

    .line 708894
    const/4 v6, 0x0

    .line 708895
    const/4 v5, 0x0

    .line 708896
    const/4 v4, 0x0

    .line 708897
    const/4 v3, 0x0

    .line 708898
    const/4 v2, 0x0

    .line 708899
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->START_OBJECT:LX/15z;

    if-eq v12, v13, :cond_b

    .line 708900
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 708901
    const/4 v2, 0x0

    .line 708902
    :goto_0
    return v2

    .line 708903
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v13, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v13, :cond_9

    .line 708904
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 708905
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 708906
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v13, v14, :cond_0

    if-eqz v3, :cond_0

    .line 708907
    const-string v13, "cache_id"

    invoke-virtual {v3, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_1

    .line 708908
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v7, v3

    goto :goto_1

    .line 708909
    :cond_1
    const-string v13, "debug_info"

    invoke-virtual {v3, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    .line 708910
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v6, v3

    goto :goto_1

    .line 708911
    :cond_2
    const-string v13, "fetchTimeMs"

    invoke-virtual {v3, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_3

    .line 708912
    const/4 v2, 0x1

    .line 708913
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    goto :goto_1

    .line 708914
    :cond_3
    const-string v13, "items"

    invoke-virtual {v3, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 708915
    invoke-static/range {p0 .. p1}, LX/4RT;->a(LX/15w;LX/186;)I

    move-result v3

    move v12, v3

    goto :goto_1

    .line 708916
    :cond_4
    const-string v13, "short_term_cache_key"

    invoke-virtual {v3, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_5

    .line 708917
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v11, v3

    goto :goto_1

    .line 708918
    :cond_5
    const-string v13, "title"

    invoke-virtual {v3, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_6

    .line 708919
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v3

    move v10, v3

    goto :goto_1

    .line 708920
    :cond_6
    const-string v13, "tracking"

    invoke-virtual {v3, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_7

    .line 708921
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v9, v3

    goto/16 :goto_1

    .line 708922
    :cond_7
    const-string v13, "local_valid_items"

    invoke-virtual {v3, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 708923
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v3

    move v8, v3

    goto/16 :goto_1

    .line 708924
    :cond_8
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 708925
    :cond_9
    const/16 v3, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 708926
    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 708927
    const/4 v3, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 708928
    if-eqz v2, :cond_a

    .line 708929
    const/4 v3, 0x2

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 708930
    :cond_a
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 708931
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 708932
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 708933
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 708934
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 708935
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_b
    move v12, v7

    move v7, v11

    move v11, v6

    move v6, v10

    move v10, v5

    move-wide v15, v8

    move v8, v3

    move v9, v4

    move-wide v4, v15

    goto/16 :goto_1
.end method

.method public static a(LX/15w;S)LX/15i;
    .locals 5

    .prologue
    .line 708879
    const/4 v0, 0x1

    const/4 v4, 0x0

    .line 708880
    new-instance v2, LX/186;

    const/16 v1, 0x80

    invoke-direct {v2, v1}, LX/186;-><init>(I)V

    .line 708881
    invoke-static {p0, v2}, LX/4RS;->a(LX/15w;LX/186;)I

    move-result v1

    .line 708882
    if-eqz v0, :cond_0

    .line 708883
    const/4 v3, 0x2

    invoke-virtual {v2, v3}, LX/186;->c(I)V

    .line 708884
    invoke-virtual {v2, v4, p1, v4}, LX/186;->a(ISI)V

    .line 708885
    const/4 v3, 0x1

    invoke-virtual {v2, v3, v1}, LX/186;->b(II)V

    .line 708886
    invoke-virtual {v2}, LX/186;->d()I

    move-result v1

    .line 708887
    :cond_0
    invoke-virtual {v2, v1}, LX/186;->d(I)V

    .line 708888
    invoke-static {v2}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v1

    move-object v0, v1

    .line 708889
    return-object v0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x7

    .line 708836
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 708837
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 708838
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 708839
    const-string v0, "name"

    const-string v1, "PlaceReviewFeedUnit"

    invoke-virtual {p2, v0, v1}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 708840
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 708841
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 708842
    if-eqz v0, :cond_0

    .line 708843
    const-string v1, "cache_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 708844
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 708845
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 708846
    if-eqz v0, :cond_1

    .line 708847
    const-string v1, "debug_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 708848
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 708849
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 708850
    cmp-long v2, v0, v4

    if-eqz v2, :cond_2

    .line 708851
    const-string v2, "fetchTimeMs"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 708852
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 708853
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 708854
    if-eqz v0, :cond_4

    .line 708855
    const-string v1, "items"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 708856
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 708857
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_3

    .line 708858
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/4RT;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 708859
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 708860
    :cond_3
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 708861
    :cond_4
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 708862
    if-eqz v0, :cond_5

    .line 708863
    const-string v1, "short_term_cache_key"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 708864
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 708865
    :cond_5
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 708866
    if-eqz v0, :cond_6

    .line 708867
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 708868
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 708869
    :cond_6
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 708870
    if-eqz v0, :cond_7

    .line 708871
    const-string v1, "tracking"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 708872
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 708873
    :cond_7
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 708874
    if-eqz v0, :cond_8

    .line 708875
    const-string v0, "local_valid_items"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 708876
    invoke-virtual {p0, p1, v3}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 708877
    :cond_8
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 708878
    return-void
.end method
