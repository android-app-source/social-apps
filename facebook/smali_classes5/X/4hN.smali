.class public LX/4hN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Externalizable;


# instance fields
.field public metadata_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/4hL;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 801262
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 801263
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/4hN;->metadata_:Ljava/util/List;

    .line 801264
    return-void
.end method

.method public static newBuilder()LX/4hO;
    .locals 1

    .prologue
    .line 801261
    new-instance v0, LX/4hO;

    invoke-direct {v0}, LX/4hO;-><init>()V

    return-object v0
.end method


# virtual methods
.method public readExternal(Ljava/io/ObjectInput;)V
    .locals 4

    .prologue
    .line 801254
    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v1

    .line 801255
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 801256
    new-instance v2, LX/4hL;

    invoke-direct {v2}, LX/4hL;-><init>()V

    .line 801257
    invoke-virtual {v2, p1}, LX/4hL;->readExternal(Ljava/io/ObjectInput;)V

    .line 801258
    iget-object v3, p0, LX/4hN;->metadata_:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 801259
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 801260
    :cond_0
    return-void
.end method

.method public writeExternal(Ljava/io/ObjectOutput;)V
    .locals 3

    .prologue
    .line 801248
    iget-object v0, p0, LX/4hN;->metadata_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    move v2, v0

    .line 801249
    invoke-interface {p1, v2}, Ljava/io/ObjectOutput;->writeInt(I)V

    .line 801250
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 801251
    iget-object v0, p0, LX/4hN;->metadata_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4hL;

    invoke-virtual {v0, p1}, LX/4hL;->writeExternal(Ljava/io/ObjectOutput;)V

    .line 801252
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 801253
    :cond_0
    return-void
.end method
