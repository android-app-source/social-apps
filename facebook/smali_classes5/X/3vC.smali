.class public final LX/3vC;
.super Landroid/widget/BaseAdapter;
.source ""


# instance fields
.field public final synthetic a:LX/3vD;

.field public b:LX/3v0;

.field private c:I


# direct methods
.method public constructor <init>(LX/3vD;LX/3v0;)V
    .locals 1

    .prologue
    .line 650821
    iput-object p1, p0, LX/3vC;->a:LX/3vD;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 650822
    const/4 v0, -0x1

    iput v0, p0, LX/3vC;->c:I

    .line 650823
    iput-object p2, p0, LX/3vC;->b:LX/3v0;

    .line 650824
    invoke-direct {p0}, LX/3vC;->a()V

    .line 650825
    return-void
.end method

.method private a()V
    .locals 5

    .prologue
    .line 650809
    iget-object v0, p0, LX/3vC;->a:LX/3vD;

    iget-object v0, v0, LX/3vD;->e:LX/3v0;

    .line 650810
    iget-object v1, v0, LX/3v0;->x:LX/3v3;

    move-object v2, v1

    .line 650811
    if-eqz v2, :cond_1

    .line 650812
    iget-object v0, p0, LX/3vC;->a:LX/3vD;

    iget-object v0, v0, LX/3vD;->e:LX/3v0;

    invoke-virtual {v0}, LX/3v0;->m()Ljava/util/ArrayList;

    move-result-object v3

    .line 650813
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 650814
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    .line 650815
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3v3;

    .line 650816
    if-ne v0, v2, :cond_0

    .line 650817
    iput v1, p0, LX/3vC;->c:I

    .line 650818
    :goto_1
    return-void

    .line 650819
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 650820
    :cond_1
    const/4 v0, -0x1

    iput v0, p0, LX/3vC;->c:I

    goto :goto_1
.end method


# virtual methods
.method public final a(I)LX/3v3;
    .locals 2

    .prologue
    .line 650804
    iget-object v0, p0, LX/3vC;->a:LX/3vD;

    iget-boolean v0, v0, LX/3vD;->g:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/3vC;->b:LX/3v0;

    invoke-virtual {v0}, LX/3v0;->m()Ljava/util/ArrayList;

    move-result-object v0

    .line 650805
    :goto_0
    iget v1, p0, LX/3vC;->c:I

    if-ltz v1, :cond_0

    iget v1, p0, LX/3vC;->c:I

    if-lt p1, v1, :cond_0

    .line 650806
    add-int/lit8 p1, p1, 0x1

    .line 650807
    :cond_0
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3v3;

    return-object v0

    .line 650808
    :cond_1
    iget-object v0, p0, LX/3vC;->b:LX/3v0;

    invoke-virtual {v0}, LX/3v0;->j()Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0
.end method

.method public final getCount()I
    .locals 2

    .prologue
    .line 650798
    iget-object v0, p0, LX/3vC;->a:LX/3vD;

    iget-boolean v0, v0, LX/3vD;->g:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3vC;->b:LX/3v0;

    invoke-virtual {v0}, LX/3v0;->m()Ljava/util/ArrayList;

    move-result-object v0

    .line 650799
    :goto_0
    iget v1, p0, LX/3vC;->c:I

    if-gez v1, :cond_1

    .line 650800
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 650801
    :goto_1
    return v0

    .line 650802
    :cond_0
    iget-object v0, p0, LX/3vC;->b:LX/3v0;

    invoke-virtual {v0}, LX/3v0;->j()Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0

    .line 650803
    :cond_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    goto :goto_1
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 650797
    invoke-virtual {p0, p1}, LX/3vC;->a(I)LX/3v3;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 650796
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 650786
    if-nez p2, :cond_1

    .line 650787
    iget-object v0, p0, LX/3vC;->a:LX/3vD;

    iget-object v0, v0, LX/3vD;->d:Landroid/view/LayoutInflater;

    sget v1, LX/3vD;->a:I

    invoke-virtual {v0, v1, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    :goto_0
    move-object v0, v1

    .line 650788
    check-cast v0, LX/3uq;

    .line 650789
    iget-object v2, p0, LX/3vC;->a:LX/3vD;

    iget-boolean v2, v2, LX/3vD;->b:Z

    if-eqz v2, :cond_0

    move-object v2, v1

    .line 650790
    check-cast v2, Landroid/support/v7/internal/view/menu/ListMenuItemView;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/support/v7/internal/view/menu/ListMenuItemView;->setForceShowIcon(Z)V

    .line 650791
    :cond_0
    invoke-virtual {p0, p1}, LX/3vC;->a(I)LX/3v3;

    move-result-object v2

    invoke-interface {v0, v2, v4}, LX/3uq;->a(LX/3v3;I)V

    .line 650792
    return-object v1

    :cond_1
    move-object v1, p2

    goto :goto_0
.end method

.method public final notifyDataSetChanged()V
    .locals 0

    .prologue
    .line 650793
    invoke-direct {p0}, LX/3vC;->a()V

    .line 650794
    invoke-super {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 650795
    return-void
.end method
