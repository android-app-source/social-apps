.class public final LX/4Un;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/executor/GraphQLResult;

.field public final synthetic b:LX/0TF;

.field public final synthetic c:LX/1My;

.field private volatile d:I


# direct methods
.method public constructor <init>(LX/1My;Lcom/facebook/graphql/executor/GraphQLResult;LX/0TF;)V
    .locals 1

    .prologue
    .line 741716
    iput-object p1, p0, LX/4Un;->c:LX/1My;

    iput-object p2, p0, LX/4Un;->a:Lcom/facebook/graphql/executor/GraphQLResult;

    iput-object p3, p0, LX/4Un;->b:LX/0TF;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 741717
    const/4 v0, -0x1

    iput v0, p0, LX/4Un;->d:I

    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/facebook/flatbuffers/MutableFlattenable;I)V
    .locals 3

    .prologue
    .line 741718
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/4Un;->c:LX/1My;

    iget-boolean v0, v0, LX/1My;->m:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    .line 741719
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 741720
    :cond_1
    :try_start_1
    iget v0, p0, LX/4Un;->d:I

    if-le p2, v0, :cond_0

    .line 741721
    iput p2, p0, LX/4Un;->d:I

    .line 741722
    iget-object v0, p0, LX/4Un;->a:Lcom/facebook/graphql/executor/GraphQLResult;

    invoke-static {v0}, LX/1lO;->a(Lcom/facebook/graphql/executor/GraphQLResult;)LX/1lO;

    move-result-object v0

    .line 741723
    iput-object p1, v0, LX/1lO;->k:Ljava/lang/Object;

    .line 741724
    move-object v0, v0

    .line 741725
    invoke-virtual {v0}, LX/1lO;->a()Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v0

    .line 741726
    iget-object v1, p0, LX/4Un;->c:LX/1My;

    iget-object v1, v1, LX/1My;->e:Ljava/util/concurrent/Executor;

    new-instance v2, Lcom/facebook/graphql/executor/GraphQLObserverHolder$1$1;

    invoke-direct {v2, p0, v0}, Lcom/facebook/graphql/executor/GraphQLObserverHolder$1$1;-><init>(LX/4Un;Lcom/facebook/graphql/executor/GraphQLResult;)V

    const v0, -0x585efa1e

    invoke-static {v1, v2, v0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 741727
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
