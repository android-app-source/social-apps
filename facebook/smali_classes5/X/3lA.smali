.class public LX/3lA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/16E;
.implements LX/0i1;


# instance fields
.field private a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private b:LX/0xX;

.field public c:Landroid/view/View;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0xX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 633591
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 633592
    iput-object p1, p0, LX/3lA;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 633593
    iput-object p2, p0, LX/3lA;->b:LX/0xX;

    .line 633594
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 633590
    const-wide/32 v0, 0x5265c00

    return-wide v0
.end method

.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/10S;
    .locals 3

    .prologue
    .line 633585
    iget-object v0, p0, LX/3lA;->b:LX/0xX;

    sget-object v1, LX/1vy;->VH_LIVE_NOTIFICATIONS:LX/1vy;

    invoke-virtual {v0, v1}, LX/0xX;->a(LX/1vy;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 633586
    iget-object v0, p0, LX/3lA;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2ns;->d:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;

    .line 633587
    :goto_0
    return-object v0

    .line 633588
    :cond_0
    sget-object v0, LX/10S;->ELIGIBLE:LX/10S;

    goto :goto_0

    .line 633589
    :cond_1
    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;

    goto :goto_0
.end method

.method public final a(J)V
    .locals 0

    .prologue
    .line 633584
    return-void
.end method

.method public final a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 633570
    iget-object v0, p0, LX/3lA;->c:Landroid/view/View;

    if-nez v0, :cond_0

    .line 633571
    :goto_0
    return-void

    .line 633572
    :cond_0
    new-instance v0, LX/0hs;

    const/4 v1, 0x2

    invoke-direct {v0, p1, v1}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 633573
    const v1, 0x7f08225a

    invoke-virtual {v0, v1}, LX/0hs;->a(I)V

    .line 633574
    const v1, 0x7f08225b

    invoke-virtual {v0, v1}, LX/0hs;->b(I)V

    .line 633575
    sget-object v1, LX/3AV;->BELOW:LX/3AV;

    invoke-virtual {v0, v1}, LX/0ht;->a(LX/3AV;)V

    .line 633576
    const/4 v1, -0x1

    .line 633577
    iput v1, v0, LX/0hs;->t:I

    .line 633578
    iget-object v1, p0, LX/3lA;->c:Landroid/view/View;

    invoke-virtual {v0, v1}, LX/0ht;->c(Landroid/view/View;)V

    .line 633579
    const/16 v1, 0x190

    invoke-virtual {v0, v2, v2, v1, v2}, LX/0ht;->a(IIII)V

    .line 633580
    invoke-virtual {v0}, LX/0ht;->d()V

    .line 633581
    iget-object v0, p0, LX/3lA;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 633582
    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/2ns;->d:LX/0Tn;

    const/4 p0, 0x1

    invoke-interface {v1, v2, p0}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 633583
    goto :goto_0
.end method

.method public final a(Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 633567
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 633569
    const-string v0, "4296"

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 633568
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->VIDEO_HOME_NOTIF_TOOLTIP:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
