.class public final LX/3vA;
.super LX/3v4;
.source ""

# interfaces
.implements Landroid/view/ActionProvider$VisibilityListener;


# instance fields
.field public c:LX/3rQ;

.field public final synthetic d:LX/3vB;


# direct methods
.method public constructor <init>(LX/3vB;Landroid/content/Context;Landroid/view/ActionProvider;)V
    .locals 0

    .prologue
    .line 650779
    iput-object p1, p0, LX/3vA;->d:LX/3vB;

    .line 650780
    invoke-direct {p0, p1, p2, p3}, LX/3v4;-><init>(LX/3v9;Landroid/content/Context;Landroid/view/ActionProvider;)V

    .line 650781
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/MenuItem;)Landroid/view/View;
    .locals 1

    .prologue
    .line 650770
    iget-object v0, p0, LX/3v4;->a:Landroid/view/ActionProvider;

    invoke-virtual {v0, p1}, Landroid/view/ActionProvider;->onCreateActionView(Landroid/view/MenuItem;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/3rQ;)V
    .locals 1

    .prologue
    .line 650775
    iput-object p1, p0, LX/3vA;->c:LX/3rQ;

    .line 650776
    iget-object v0, p0, LX/3v4;->a:Landroid/view/ActionProvider;

    if-eqz p1, :cond_0

    :goto_0
    invoke-virtual {v0, p0}, Landroid/view/ActionProvider;->setVisibilityListener(Landroid/view/ActionProvider$VisibilityListener;)V

    .line 650777
    return-void

    .line 650778
    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 650782
    iget-object v0, p0, LX/3v4;->a:Landroid/view/ActionProvider;

    invoke-virtual {v0}, Landroid/view/ActionProvider;->overridesItemVisibility()Z

    move-result v0

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 650774
    iget-object v0, p0, LX/3v4;->a:Landroid/view/ActionProvider;

    invoke-virtual {v0}, Landroid/view/ActionProvider;->isVisible()Z

    move-result v0

    return v0
.end method

.method public final onActionProviderVisibilityChanged(Z)V
    .locals 1

    .prologue
    .line 650771
    iget-object v0, p0, LX/3vA;->c:LX/3rQ;

    if-eqz v0, :cond_0

    .line 650772
    iget-object v0, p0, LX/3vA;->c:LX/3rQ;

    invoke-interface {v0}, LX/3rQ;->a()V

    .line 650773
    :cond_0
    return-void
.end method
