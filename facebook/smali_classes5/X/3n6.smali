.class public LX/3n6;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/3mE;


# direct methods
.method public constructor <init>(LX/3mE;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 637712
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 637713
    iput-object p1, p0, LX/3n6;->a:LX/3mE;

    .line 637714
    return-void
.end method

.method public static a(LX/0QB;)LX/3n6;
    .locals 4

    .prologue
    .line 637715
    const-class v1, LX/3n6;

    monitor-enter v1

    .line 637716
    :try_start_0
    sget-object v0, LX/3n6;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 637717
    sput-object v2, LX/3n6;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 637718
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 637719
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 637720
    new-instance p0, LX/3n6;

    invoke-static {v0}, LX/3mE;->a(LX/0QB;)LX/3mE;

    move-result-object v3

    check-cast v3, LX/3mE;

    invoke-direct {p0, v3}, LX/3n6;-><init>(LX/3mE;)V

    .line 637721
    move-object v0, p0

    .line 637722
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 637723
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/3n6;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 637724
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 637725
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
