.class public LX/4pj;
.super LX/2WJ;
.source ""


# instance fields
.field public final c:[LX/15w;

.field public d:I


# direct methods
.method private constructor <init>([LX/15w;)V
    .locals 1

    .prologue
    .line 811800
    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-direct {p0, v0}, LX/2WJ;-><init>(LX/15w;)V

    .line 811801
    iput-object p1, p0, LX/4pj;->c:[LX/15w;

    .line 811802
    const/4 v0, 0x1

    iput v0, p0, LX/4pj;->d:I

    .line 811803
    return-void
.end method

.method private K()Z
    .locals 3

    .prologue
    .line 811774
    iget v0, p0, LX/4pj;->d:I

    iget-object v1, p0, LX/4pj;->c:[LX/15w;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 811775
    const/4 v0, 0x0

    .line 811776
    :goto_0
    return v0

    .line 811777
    :cond_0
    iget-object v0, p0, LX/4pj;->c:[LX/15w;

    iget v1, p0, LX/4pj;->d:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/4pj;->d:I

    aget-object v0, v0, v1

    iput-object v0, p0, LX/4pj;->b:LX/15w;

    .line 811778
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static a(LX/15w;LX/15w;)LX/4pj;
    .locals 3

    .prologue
    .line 811789
    instance-of v0, p0, LX/4pj;

    if-nez v0, :cond_0

    instance-of v0, p1, LX/4pj;

    if-nez v0, :cond_0

    .line 811790
    new-instance v0, LX/4pj;

    const/4 v1, 0x2

    new-array v1, v1, [LX/15w;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    invoke-direct {v0, v1}, LX/4pj;-><init>([LX/15w;)V

    .line 811791
    :goto_0
    return-object v0

    .line 811792
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 811793
    instance-of v1, p0, LX/4pj;

    if-eqz v1, :cond_1

    .line 811794
    check-cast p0, LX/4pj;

    invoke-direct {p0, v0}, LX/4pj;->a(Ljava/util/List;)V

    .line 811795
    :goto_1
    instance-of v1, p1, LX/4pj;

    if-eqz v1, :cond_2

    .line 811796
    check-cast p1, LX/4pj;

    invoke-direct {p1, v0}, LX/4pj;->a(Ljava/util/List;)V

    .line 811797
    :goto_2
    new-instance v1, LX/4pj;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [LX/15w;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/15w;

    invoke-direct {v1, v0}, LX/4pj;-><init>([LX/15w;)V

    move-object v0, v1

    goto :goto_0

    .line 811798
    :cond_1
    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 811799
    :cond_2
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2
.end method

.method private a(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/15w;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 811804
    iget v0, p0, LX/4pj;->d:I

    add-int/lit8 v0, v0, -0x1

    iget-object v1, p0, LX/4pj;->c:[LX/15w;

    array-length v2, v1

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 811805
    iget-object v0, p0, LX/4pj;->c:[LX/15w;

    aget-object v0, v0, v1

    .line 811806
    instance-of v3, v0, LX/4pj;

    if-eqz v3, :cond_0

    .line 811807
    check-cast v0, LX/4pj;

    invoke-direct {v0, p1}, LX/4pj;->a(Ljava/util/List;)V

    .line 811808
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 811809
    :cond_0
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 811810
    :cond_1
    return-void
.end method


# virtual methods
.method public final c()LX/15z;
    .locals 1

    .prologue
    .line 811782
    iget-object v0, p0, LX/2WJ;->b:LX/15w;

    invoke-virtual {v0}, LX/15w;->c()LX/15z;

    move-result-object v0

    .line 811783
    if-eqz v0, :cond_0

    .line 811784
    :goto_0
    return-object v0

    .line 811785
    :cond_0
    invoke-direct {p0}, LX/4pj;->K()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 811786
    iget-object v0, p0, LX/2WJ;->b:LX/15w;

    invoke-virtual {v0}, LX/15w;->c()LX/15z;

    move-result-object v0

    .line 811787
    if-eqz v0, :cond_0

    goto :goto_0

    .line 811788
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 811779
    :cond_0
    iget-object v0, p0, LX/2WJ;->b:LX/15w;

    invoke-virtual {v0}, LX/15w;->close()V

    .line 811780
    invoke-direct {p0}, LX/4pj;->K()Z

    move-result v0

    if-nez v0, :cond_0

    .line 811781
    return-void
.end method
