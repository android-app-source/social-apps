.class public LX/4Mm;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 689258
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 17

    .prologue
    .line 689200
    const/4 v9, 0x0

    .line 689201
    const-wide/16 v10, 0x0

    .line 689202
    const/4 v8, 0x0

    .line 689203
    const-wide/16 v6, 0x0

    .line 689204
    const/4 v5, 0x0

    .line 689205
    const/4 v4, 0x0

    .line 689206
    const/4 v3, 0x0

    .line 689207
    const/4 v2, 0x0

    .line 689208
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->START_OBJECT:LX/15z;

    if-eq v12, v13, :cond_a

    .line 689209
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 689210
    const/4 v2, 0x0

    .line 689211
    :goto_0
    return v2

    .line 689212
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v7, :cond_5

    .line 689213
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 689214
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 689215
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v14, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v14, :cond_0

    if-eqz v2, :cond_0

    .line 689216
    const-string v7, "horizontal_alignment"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 689217
    const/4 v2, 0x1

    .line 689218
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    move-result-object v6

    move-object v13, v6

    move v6, v2

    goto :goto_1

    .line 689219
    :cond_1
    const-string v7, "horizontal_margin"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 689220
    const/4 v2, 0x1

    .line 689221
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 689222
    :cond_2
    const-string v7, "vertical_alignment"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 689223
    const/4 v2, 0x1

    .line 689224
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

    move-result-object v7

    move v9, v2

    move-object v12, v7

    goto :goto_1

    .line 689225
    :cond_3
    const-string v7, "vertical_margin"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 689226
    const/4 v2, 0x1

    .line 689227
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v10

    move v8, v2

    goto :goto_1

    .line 689228
    :cond_4
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 689229
    :cond_5
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 689230
    if-eqz v6, :cond_6

    .line 689231
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->a(ILjava/lang/Enum;)V

    .line 689232
    :cond_6
    if-eqz v3, :cond_7

    .line 689233
    const/4 v3, 0x1

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 689234
    :cond_7
    if-eqz v9, :cond_8

    .line 689235
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->a(ILjava/lang/Enum;)V

    .line 689236
    :cond_8
    if-eqz v8, :cond_9

    .line 689237
    const/4 v3, 0x3

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide v4, v10

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 689238
    :cond_9
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_a
    move-object v12, v8

    move-object v13, v9

    move v8, v2

    move v9, v3

    move v3, v4

    move-wide v15, v6

    move v6, v5

    move-wide v4, v10

    move-wide v10, v15

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    .line 689239
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 689240
    invoke-virtual {p0, p1, v3, v3}, LX/15i;->a(IIS)S

    move-result v0

    .line 689241
    if-eqz v0, :cond_0

    .line 689242
    const-string v0, "horizontal_alignment"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 689243
    const-class v0, Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    invoke-virtual {p0, p1, v3, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 689244
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 689245
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_1

    .line 689246
    const-string v2, "horizontal_margin"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 689247
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 689248
    :cond_1
    invoke-virtual {p0, p1, v6, v3}, LX/15i;->a(IIS)S

    move-result v0

    .line 689249
    if-eqz v0, :cond_2

    .line 689250
    const-string v0, "vertical_alignment"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 689251
    const-class v0, Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

    invoke-virtual {p0, p1, v6, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 689252
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 689253
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_3

    .line 689254
    const-string v2, "vertical_margin"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 689255
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 689256
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 689257
    return-void
.end method
