.class public LX/4Nr;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 694831
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 12

    .prologue
    const/4 v0, 0x0

    .line 694832
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v2, :cond_b

    .line 694833
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 694834
    :goto_0
    return v0

    .line 694835
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 694836
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_a

    .line 694837
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 694838
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 694839
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_1

    if-eqz v9, :cond_1

    .line 694840
    const-string v10, "current_value"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 694841
    invoke-static {p0, p1}, LX/4Nu;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 694842
    :cond_2
    const-string v10, "custom_value"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 694843
    invoke-static {p0, p1}, LX/4Nq;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 694844
    :cond_3
    const-string v10, "default_option_text"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 694845
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 694846
    :cond_4
    const-string v10, "filter_values"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 694847
    invoke-static {p0, p1}, LX/4Nv;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 694848
    :cond_5
    const-string v10, "handle"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 694849
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 694850
    :cond_6
    const-string v10, "id"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 694851
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 694852
    :cond_7
    const-string v10, "name"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 694853
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto/16 :goto_1

    .line 694854
    :cond_8
    const-string v10, "search_place_holder"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 694855
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    goto/16 :goto_1

    .line 694856
    :cond_9
    const-string v10, "text"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 694857
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_1

    .line 694858
    :cond_a
    const/16 v9, 0xa

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 694859
    const/4 v9, 0x1

    invoke-virtual {p1, v9, v8}, LX/186;->b(II)V

    .line 694860
    const/4 v8, 0x2

    invoke-virtual {p1, v8, v7}, LX/186;->b(II)V

    .line 694861
    const/4 v7, 0x3

    invoke-virtual {p1, v7, v6}, LX/186;->b(II)V

    .line 694862
    const/4 v6, 0x4

    invoke-virtual {p1, v6, v5}, LX/186;->b(II)V

    .line 694863
    const/4 v5, 0x5

    invoke-virtual {p1, v5, v4}, LX/186;->b(II)V

    .line 694864
    const/4 v4, 0x6

    invoke-virtual {p1, v4, v3}, LX/186;->b(II)V

    .line 694865
    const/4 v3, 0x7

    invoke-virtual {p1, v3, v2}, LX/186;->b(II)V

    .line 694866
    const/16 v2, 0x8

    invoke-virtual {p1, v2, v1}, LX/186;->b(II)V

    .line 694867
    const/16 v1, 0x9

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 694868
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :cond_b
    move v1, v0

    move v2, v0

    move v3, v0

    move v4, v0

    move v5, v0

    move v6, v0

    move v7, v0

    move v8, v0

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 694869
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 694870
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 694871
    if-eqz v0, :cond_0

    .line 694872
    const-string v1, "current_value"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 694873
    invoke-static {p0, v0, p2, p3}, LX/4Nu;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 694874
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 694875
    if-eqz v0, :cond_1

    .line 694876
    const-string v1, "custom_value"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 694877
    invoke-static {p0, v0, p2}, LX/4Nq;->a(LX/15i;ILX/0nX;)V

    .line 694878
    :cond_1
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 694879
    if-eqz v0, :cond_2

    .line 694880
    const-string v1, "default_option_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 694881
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 694882
    :cond_2
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 694883
    if-eqz v0, :cond_3

    .line 694884
    const-string v1, "filter_values"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 694885
    invoke-static {p0, v0, p2, p3}, LX/4Nv;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 694886
    :cond_3
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 694887
    if-eqz v0, :cond_4

    .line 694888
    const-string v1, "handle"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 694889
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 694890
    :cond_4
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 694891
    if-eqz v0, :cond_5

    .line 694892
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 694893
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 694894
    :cond_5
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 694895
    if-eqz v0, :cond_6

    .line 694896
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 694897
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 694898
    :cond_6
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 694899
    if-eqz v0, :cond_7

    .line 694900
    const-string v1, "search_place_holder"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 694901
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 694902
    :cond_7
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 694903
    if-eqz v0, :cond_8

    .line 694904
    const-string v1, "text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 694905
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 694906
    :cond_8
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 694907
    return-void
.end method
