.class public LX/3kA;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/3JO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/3JO",
            "<",
            "LX/3kK;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 632765
    new-instance v0, LX/3kB;

    invoke-direct {v0}, LX/3kB;-><init>()V

    sput-object v0, LX/3kA;->a:LX/3JO;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 632752
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/util/JsonReader;)LX/3kK;
    .locals 4

    .prologue
    .line 632753
    invoke-virtual {p0}, Landroid/util/JsonReader;->beginObject()V

    .line 632754
    new-instance v1, LX/3kC;

    invoke-direct {v1}, LX/3kC;-><init>()V

    .line 632755
    :goto_0
    invoke-virtual {p0}, Landroid/util/JsonReader;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 632756
    invoke-virtual {p0}, Landroid/util/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v2

    .line 632757
    const/4 v0, -0x1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :cond_0
    :goto_1
    packed-switch v0, :pswitch_data_0

    .line 632758
    invoke-virtual {p0}, Landroid/util/JsonReader;->skipValue()V

    goto :goto_0

    .line 632759
    :sswitch_0
    const-string v3, "color_start"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x0

    goto :goto_1

    :sswitch_1
    const-string v3, "color_end"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    goto :goto_1

    .line 632760
    :pswitch_0
    invoke-static {p0}, LX/3kD;->a(Landroid/util/JsonReader;)LX/3kJ;

    move-result-object v0

    iput-object v0, v1, LX/3kC;->a:LX/3kJ;

    goto :goto_0

    .line 632761
    :pswitch_1
    invoke-static {p0}, LX/3kD;->a(Landroid/util/JsonReader;)LX/3kJ;

    move-result-object v0

    iput-object v0, v1, LX/3kC;->b:LX/3kJ;

    goto :goto_0

    .line 632762
    :cond_1
    invoke-virtual {p0}, Landroid/util/JsonReader;->endObject()V

    .line 632763
    new-instance v0, LX/3kK;

    iget-object v2, v1, LX/3kC;->a:LX/3kJ;

    iget-object v3, v1, LX/3kC;->b:LX/3kJ;

    invoke-direct {v0, v2, v3}, LX/3kK;-><init>(LX/3kJ;LX/3kJ;)V

    move-object v0, v0

    .line 632764
    return-object v0

    nop

    :sswitch_data_0
    .sparse-switch
        0x4ef80086 -> :sswitch_0
        0x76177dff -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
