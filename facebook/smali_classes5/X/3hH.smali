.class public LX/3hH;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/3hF;

.field private final b:I

.field private final c:I


# direct methods
.method public constructor <init>(Ljava/util/Locale;)V
    .locals 3

    .prologue
    .line 626462
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 626463
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 626464
    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 626465
    sget-object v1, Ljava/util/Locale;->JAPANESE:Ljava/util/Locale;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 626466
    sget-object v1, Ljava/util/Locale;->KOREAN:Ljava/util/Locale;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 626467
    sget-object v1, LX/3Lu;->e:Ljava/util/Locale;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 626468
    sget-object v1, LX/3Lu;->a:Ljava/util/Locale;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 626469
    sget-object v1, LX/3Lu;->c:Ljava/util/Locale;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 626470
    sget-object v1, LX/3Lu;->b:Ljava/util/Locale;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 626471
    sget-object v1, LX/3Lu;->d:Ljava/util/Locale;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 626472
    new-instance v1, LX/3hF;

    const/16 v2, 0x12c

    invoke-direct {v1, p1, v0, v2}, LX/3hF;-><init>(Ljava/util/Locale;Ljava/util/List;I)V

    iput-object v1, p0, LX/3hH;->a:LX/3hF;

    .line 626473
    iget-object v0, p0, LX/3hH;->a:LX/3hF;

    .line 626474
    iget-object v1, v0, LX/3hF;->b:Ljava/lang/reflect/Method;

    iget-object v2, v0, LX/3hF;->a:Ljava/lang/Object;

    const/4 p1, 0x0

    new-array p1, p1, [Ljava/lang/Object;

    invoke-virtual {v1, v2, p1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    move v0, v1

    .line 626475
    iput v0, p0, LX/3hH;->b:I

    .line 626476
    iget v0, p0, LX/3hH;->b:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/3hH;->c:I

    .line 626477
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 626478
    iget v0, p0, LX/3hH;->b:I

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public a(Ljava/lang/String;)I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 626479
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    move v1, v0

    .line 626480
    :goto_0
    if-ge v1, v2, :cond_0

    .line 626481
    invoke-static {p1, v1}, Ljava/lang/Character;->codePointAt(Ljava/lang/CharSequence;I)I

    move-result v3

    .line 626482
    invoke-static {v3}, Ljava/lang/Character;->isDigit(I)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 626483
    const/4 v0, 0x1

    .line 626484
    :cond_0
    if-eqz v0, :cond_4

    .line 626485
    iget v0, p0, LX/3hH;->c:I

    .line 626486
    :cond_1
    :goto_1
    return v0

    .line 626487
    :cond_2
    invoke-static {v3}, Ljava/lang/Character;->isSpaceChar(I)Z

    move-result v4

    if-nez v4, :cond_3

    const/16 v4, 0x2b

    if-eq v3, v4, :cond_3

    const/16 v4, 0x28

    if-eq v3, v4, :cond_3

    const/16 v4, 0x29

    if-eq v3, v4, :cond_3

    const/16 v4, 0x2e

    if-eq v3, v4, :cond_3

    const/16 v4, 0x2d

    if-eq v3, v4, :cond_3

    const/16 v4, 0x23

    if-ne v3, v4, :cond_0

    .line 626488
    :cond_3
    invoke-static {v3}, Ljava/lang/Character;->charCount(I)I

    move-result v3

    add-int/2addr v1, v3

    .line 626489
    goto :goto_0

    .line 626490
    :cond_4
    iget-object v0, p0, LX/3hH;->a:LX/3hF;

    .line 626491
    iget-object v1, v0, LX/3hF;->c:Ljava/lang/reflect/Method;

    iget-object v2, v0, LX/3hF;->a:Ljava/lang/Object;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    move v0, v1

    .line 626492
    if-gez v0, :cond_5

    .line 626493
    const/4 v0, -0x1

    goto :goto_1

    .line 626494
    :cond_5
    iget v1, p0, LX/3hH;->c:I

    if-lt v0, v1, :cond_1

    .line 626495
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public a(I)Ljava/lang/String;
    .locals 5

    .prologue
    .line 626496
    if-ltz p1, :cond_0

    invoke-virtual {p0}, LX/3hH;->a()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 626497
    :cond_0
    const-string v0, ""

    .line 626498
    :goto_0
    return-object v0

    .line 626499
    :cond_1
    iget v0, p0, LX/3hH;->c:I

    if-ne p1, v0, :cond_2

    .line 626500
    const-string v0, "#"

    goto :goto_0

    .line 626501
    :cond_2
    iget v0, p0, LX/3hH;->c:I

    if-le p1, v0, :cond_3

    .line 626502
    add-int/lit8 p1, p1, -0x1

    .line 626503
    :cond_3
    iget-object v0, p0, LX/3hH;->a:LX/3hF;

    .line 626504
    iget-object v1, v0, LX/3hF;->d:Ljava/lang/reflect/Method;

    iget-object v2, v0, LX/3hF;->a:Ljava/lang/Object;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    aput-object p0, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object v0, v1

    .line 626505
    goto :goto_0
.end method
