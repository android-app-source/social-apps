.class public final LX/50H;
.super LX/1sm;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "LX/1sm",
        "<TT;>;",
        "Ljava/io/Serializable;"
    }
.end annotation


# instance fields
.field public final ordering:LX/1sm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1sm",
            "<-TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1sm;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1sm",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 823597
    invoke-direct {p0}, LX/1sm;-><init>()V

    .line 823598
    iput-object p1, p0, LX/50H;->ordering:LX/1sm;

    .line 823599
    return-void
.end method


# virtual methods
.method public final a()LX/1sm;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:TT;>()",
            "LX/1sm",
            "<TS;>;"
        }
    .end annotation

    .prologue
    .line 823596
    iget-object v0, p0, LX/50H;->ordering:LX/1sm;

    invoke-virtual {v0}, LX/1sm;->a()LX/1sm;

    move-result-object v0

    invoke-virtual {v0}, LX/1sm;->b()LX/1sm;

    move-result-object v0

    return-object v0
.end method

.method public final b()LX/1sm;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:TT;>()",
            "LX/1sm",
            "<TS;>;"
        }
    .end annotation

    .prologue
    .line 823595
    iget-object v0, p0, LX/50H;->ordering:LX/1sm;

    invoke-virtual {v0}, LX/1sm;->b()LX/1sm;

    move-result-object v0

    return-object v0
.end method

.method public final c()LX/1sm;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:TT;>()",
            "LX/1sm",
            "<TS;>;"
        }
    .end annotation

    .prologue
    .line 823600
    return-object p0
.end method

.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;TT;)I"
        }
    .end annotation

    .prologue
    .line 823587
    if-ne p1, p2, :cond_0

    .line 823588
    const/4 v0, 0x0

    .line 823589
    :goto_0
    return v0

    .line 823590
    :cond_0
    if-nez p1, :cond_1

    .line 823591
    const/4 v0, 0x1

    goto :goto_0

    .line 823592
    :cond_1
    if-nez p2, :cond_2

    .line 823593
    const/4 v0, -0x1

    goto :goto_0

    .line 823594
    :cond_2
    iget-object v0, p0, LX/50H;->ordering:LX/1sm;

    invoke-virtual {v0, p1, p2}, LX/1sm;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 823580
    if-ne p1, p0, :cond_0

    .line 823581
    const/4 v0, 0x1

    .line 823582
    :goto_0
    return v0

    .line 823583
    :cond_0
    instance-of v0, p1, LX/50H;

    if-eqz v0, :cond_1

    .line 823584
    check-cast p1, LX/50H;

    .line 823585
    iget-object v0, p0, LX/50H;->ordering:LX/1sm;

    iget-object v1, p1, LX/50H;->ordering:LX/1sm;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 823586
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 823579
    iget-object v0, p0, LX/50H;->ordering:LX/1sm;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    const v1, -0x36e88db8    # -620324.5f

    xor-int/2addr v0, v1

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 823578
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, LX/50H;->ordering:LX/1sm;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".nullsLast()"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
