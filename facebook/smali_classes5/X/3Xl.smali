.class public LX/3Xl;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements LX/24a;


# static fields
.field public static final a:LX/1Cz;


# instance fields
.field private final b:I

.field private final c:I

.field private final d:I

.field private final e:Landroid/widget/TextView;

.field private final f:Landroid/widget/TextView;

.field private final g:Landroid/widget/TextView;

.field private final h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public final i:Landroid/widget/ImageView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 593899
    new-instance v0, LX/3Xm;

    invoke-direct {v0}, LX/3Xm;-><init>()V

    sput-object v0, LX/3Xl;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 593866
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 593867
    const v0, 0x7f0314a8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 593868
    const v0, 0x7f0d14bf

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/3Xl;->e:Landroid/widget/TextView;

    .line 593869
    const v0, 0x7f0d2eea

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/3Xl;->f:Landroid/widget/TextView;

    .line 593870
    const v0, 0x7f0d2eeb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/3Xl;->g:Landroid/widget/TextView;

    .line 593871
    const v0, 0x7f0d0bde

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/3Xl;->i:Landroid/widget/ImageView;

    .line 593872
    const v0, 0x7f0d0bbe

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/3Xl;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 593873
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 593874
    const v1, 0x7f0b0052

    invoke-static {v0, v1}, LX/0tP;->c(Landroid/content/res/Resources;I)I

    move-result v1

    iput v1, p0, LX/3Xl;->b:I

    .line 593875
    const v1, 0x7f0b0050

    invoke-static {v0, v1}, LX/0tP;->c(Landroid/content/res/Resources;I)I

    move-result v1

    iput v1, p0, LX/3Xl;->c:I

    .line 593876
    const v1, 0x7f0b004e

    invoke-static {v0, v1}, LX/0tP;->c(Landroid/content/res/Resources;I)I

    move-result v0

    iput v0, p0, LX/3Xl;->d:I

    .line 593877
    return-void
.end method


# virtual methods
.method public final a(LX/0hs;)V
    .locals 1

    .prologue
    .line 593897
    iget-object v0, p0, LX/3Xl;->i:Landroid/widget/ImageView;

    invoke-virtual {p1, v0}, LX/0ht;->f(Landroid/view/View;)V

    .line 593898
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 593895
    iget-object v0, p0, LX/3Xl;->i:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 593896
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setCallToAction(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 593892
    iget-object v0, p0, LX/3Xl;->g:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 593893
    iget-object v0, p0, LX/3Xl;->g:Landroid/widget/TextView;

    iget v1, p0, LX/3Xl;->d:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 593894
    return-void
.end method

.method public setHeader(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 593884
    iget-object v0, p0, LX/3Xl;->e:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 593885
    if-nez p1, :cond_0

    .line 593886
    iget-object v0, p0, LX/3Xl;->e:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 593887
    iget-object v0, p0, LX/3Xl;->f:Landroid/widget/TextView;

    invoke-virtual {p0}, LX/3Xl;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a010c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 593888
    :goto_0
    iget-object v0, p0, LX/3Xl;->e:Landroid/widget/TextView;

    iget v1, p0, LX/3Xl;->b:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 593889
    return-void

    .line 593890
    :cond_0
    iget-object v0, p0, LX/3Xl;->e:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 593891
    iget-object v0, p0, LX/3Xl;->f:Landroid/widget/TextView;

    invoke-virtual {p0}, LX/3Xl;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a010d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method

.method public setMenuButtonActive(Z)V
    .locals 2

    .prologue
    .line 593881
    iget-object v1, p0, LX/3Xl;->i:Landroid/widget/ImageView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 593882
    return-void

    .line 593883
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 593878
    iget-object v0, p0, LX/3Xl;->f:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 593879
    iget-object v0, p0, LX/3Xl;->f:Landroid/widget/TextView;

    iget v1, p0, LX/3Xl;->c:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 593880
    return-void
.end method
