.class public abstract LX/4uO;
.super LX/4uN;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;


# instance fields
.field public b:Z

.field public c:Z

.field public final d:LX/1vX;

.field public e:Lcom/google/android/gms/common/ConnectionResult;

.field public f:I

.field private final g:Landroid/os/Handler;


# direct methods
.method public constructor <init>(LX/4ur;)V
    .locals 1

    sget-object v0, LX/1vX;->c:LX/1vX;

    move-object v0, v0

    invoke-direct {p0, p1, v0}, LX/4uO;-><init>(LX/4ur;LX/1vX;)V

    return-void
.end method

.method private constructor <init>(LX/4ur;LX/1vX;)V
    .locals 2

    invoke-direct {p0, p1}, LX/4uN;-><init>(LX/4ur;)V

    const/4 v0, -0x1

    iput v0, p0, LX/4uO;->f:I

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, LX/4uO;->g:Landroid/os/Handler;

    iput-object p2, p0, LX/4uO;->d:LX/1vX;

    return-void
.end method


# virtual methods
.method public final a(IILandroid/content/Intent;)V
    .locals 5

    const/16 v4, 0x12

    const/16 v2, 0xd

    const/4 v0, 0x1

    const/4 v1, 0x0

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    move v0, v1

    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {p0}, LX/4uO;->e()V

    :goto_1
    return-void

    :pswitch_0
    iget-object v2, p0, LX/4uO;->d:LX/1vX;

    invoke-virtual {p0}, LX/4uN;->a()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/1od;->a(Landroid/content/Context;)I

    move-result v2

    if-nez v2, :cond_4

    :goto_2
    iget-object v1, p0, LX/4uO;->e:Lcom/google/android/gms/common/ConnectionResult;

    iget v3, v1, Lcom/google/android/gms/common/ConnectionResult;->c:I

    move v1, v3

    if-ne v1, v4, :cond_1

    if-ne v2, v4, :cond_1

    goto :goto_1

    :pswitch_1
    const/4 v3, -0x1

    if-eq p2, v3, :cond_1

    if-nez p2, :cond_0

    if-eqz p3, :cond_3

    const-string v0, "<<ResolutionFailureErrorDetail>>"

    invoke-virtual {p3, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    :goto_3
    new-instance v2, Lcom/google/android/gms/common/ConnectionResult;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v3}, Lcom/google/android/gms/common/ConnectionResult;-><init>(ILandroid/app/PendingIntent;)V

    iput-object v2, p0, LX/4uO;->e:Lcom/google/android/gms/common/ConnectionResult;

    goto :goto_0

    :cond_2
    iget-object v0, p0, LX/4uO;->e:Lcom/google/android/gms/common/ConnectionResult;

    iget v1, p0, LX/4uO;->f:I

    invoke-virtual {p0, v0, v1}, LX/4uO;->a(Lcom/google/android/gms/common/ConnectionResult;I)V

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_3

    :cond_4
    move v0, v1

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, LX/4uN;->a(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    const-string v0, "resolving_error"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, LX/4uO;->c:Z

    iget-boolean v0, p0, LX/4uO;->c:Z

    if-eqz v0, :cond_0

    const-string v0, "failed_client_id"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, LX/4uO;->f:I

    new-instance v1, Lcom/google/android/gms/common/ConnectionResult;

    const-string v0, "failed_status"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    const-string v0, "failed_resolution"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/common/ConnectionResult;-><init>(ILandroid/app/PendingIntent;)V

    iput-object v1, p0, LX/4uO;->e:Lcom/google/android/gms/common/ConnectionResult;

    :cond_0
    return-void
.end method

.method public abstract a(Lcom/google/android/gms/common/ConnectionResult;I)V
.end method

.method public b()V
    .locals 1

    invoke-super {p0}, LX/4uN;->b()V

    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4uO;->b:Z

    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, LX/4uN;->b(Landroid/os/Bundle;)V

    const-string v0, "resolving_error"

    iget-boolean v1, p0, LX/4uO;->c:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-boolean v0, p0, LX/4uO;->c:Z

    if-eqz v0, :cond_0

    const-string v0, "failed_client_id"

    iget v1, p0, LX/4uO;->f:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "failed_status"

    iget-object v1, p0, LX/4uO;->e:Lcom/google/android/gms/common/ConnectionResult;

    iget v2, v1, Lcom/google/android/gms/common/ConnectionResult;->c:I

    move v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "failed_resolution"

    iget-object v1, p0, LX/4uO;->e:Lcom/google/android/gms/common/ConnectionResult;

    iget-object v2, v1, Lcom/google/android/gms/common/ConnectionResult;->d:Landroid/app/PendingIntent;

    move-object v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_0
    return-void
.end method

.method public final b(Lcom/google/android/gms/common/ConnectionResult;I)V
    .locals 3

    iget-boolean v0, p0, LX/4uO;->c:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, LX/4uO;->c:Z

    iput p2, p0, LX/4uO;->f:I

    iput-object p1, p0, LX/4uO;->e:Lcom/google/android/gms/common/ConnectionResult;

    iget-object v0, p0, LX/4uO;->g:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/internal/zzpn$zza;

    invoke-direct {v1, p0}, Lcom/google/android/gms/internal/zzpn$zza;-><init>(LX/4uO;)V

    const v2, -0x4b2b3559

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    :cond_0
    return-void
.end method

.method public c()V
    .locals 1

    invoke-super {p0}, LX/4uN;->c()V

    const/4 v0, 0x0

    iput-boolean v0, p0, LX/4uO;->b:Z

    return-void
.end method

.method public abstract d()V
.end method

.method public final e()V
    .locals 1

    const/4 v0, -0x1

    iput v0, p0, LX/4uO;->f:I

    const/4 v0, 0x0

    iput-boolean v0, p0, LX/4uO;->c:Z

    const/4 v0, 0x0

    iput-object v0, p0, LX/4uO;->e:Lcom/google/android/gms/common/ConnectionResult;

    invoke-virtual {p0}, LX/4uO;->d()V

    return-void
.end method

.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 3

    new-instance v0, Lcom/google/android/gms/common/ConnectionResult;

    const/16 v1, 0xd

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/common/ConnectionResult;-><init>(ILandroid/app/PendingIntent;)V

    iget v1, p0, LX/4uO;->f:I

    invoke-virtual {p0, v0, v1}, LX/4uO;->a(Lcom/google/android/gms/common/ConnectionResult;I)V

    invoke-virtual {p0}, LX/4uO;->e()V

    return-void
.end method
