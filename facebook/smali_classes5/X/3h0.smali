.class public final LX/3h0;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 41

    .prologue
    .line 625083
    const/16 v35, 0x0

    .line 625084
    const/16 v34, 0x0

    .line 625085
    const/16 v33, 0x0

    .line 625086
    const/16 v32, 0x0

    .line 625087
    const-wide/16 v30, 0x0

    .line 625088
    const/16 v29, 0x0

    .line 625089
    const/16 v28, 0x0

    .line 625090
    const/16 v27, 0x0

    .line 625091
    const/16 v26, 0x0

    .line 625092
    const/16 v25, 0x0

    .line 625093
    const/16 v24, 0x0

    .line 625094
    const/16 v23, 0x0

    .line 625095
    const/16 v22, 0x0

    .line 625096
    const-wide/16 v20, 0x0

    .line 625097
    const-wide/16 v18, 0x0

    .line 625098
    const/16 v17, 0x0

    .line 625099
    const/16 v16, 0x0

    .line 625100
    const/4 v13, 0x0

    .line 625101
    const-wide/16 v14, 0x0

    .line 625102
    const/4 v12, 0x0

    .line 625103
    const/4 v11, 0x0

    .line 625104
    const/4 v10, 0x0

    .line 625105
    const/4 v9, 0x0

    .line 625106
    const/4 v8, 0x0

    .line 625107
    const/4 v7, 0x0

    .line 625108
    const/4 v6, 0x0

    .line 625109
    const/4 v5, 0x0

    .line 625110
    const/4 v4, 0x0

    .line 625111
    const/4 v3, 0x0

    .line 625112
    const/4 v2, 0x0

    .line 625113
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v36

    sget-object v37, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v36

    move-object/from16 v1, v37

    if-eq v0, v1, :cond_21

    .line 625114
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 625115
    const/4 v2, 0x0

    .line 625116
    :goto_0
    return v2

    .line 625117
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v37, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v37

    if-eq v2, v0, :cond_15

    .line 625118
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 625119
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 625120
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v37

    sget-object v38, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v37

    move-object/from16 v1, v38

    if-eq v0, v1, :cond_0

    if-eqz v2, :cond_0

    .line 625121
    const-string v37, "__type__"

    move-object/from16 v0, v37

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v37

    if-nez v37, :cond_1

    const-string v37, "__typename"

    move-object/from16 v0, v37

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v37

    if-eqz v37, :cond_2

    .line 625122
    :cond_1
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v2

    move/from16 v36, v2

    goto :goto_1

    .line 625123
    :cond_2
    const-string v37, "birthdate"

    move-object/from16 v0, v37

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v37

    if-eqz v37, :cond_3

    .line 625124
    invoke-static/range {p0 .. p1}, LX/3h1;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v35, v2

    goto :goto_1

    .line 625125
    :cond_3
    const-string v37, "can_see_viewer_montage_thread"

    move-object/from16 v0, v37

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v37

    if-eqz v37, :cond_4

    .line 625126
    const/4 v2, 0x1

    .line 625127
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v7

    move/from16 v34, v7

    move v7, v2

    goto :goto_1

    .line 625128
    :cond_4
    const-string v37, "can_viewer_message"

    move-object/from16 v0, v37

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v37

    if-eqz v37, :cond_5

    .line 625129
    const/4 v2, 0x1

    .line 625130
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move/from16 v33, v6

    move v6, v2

    goto :goto_1

    .line 625131
    :cond_5
    const-string v37, "communicationRank"

    move-object/from16 v0, v37

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v37

    if-eqz v37, :cond_6

    .line 625132
    const/4 v2, 0x1

    .line 625133
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v4

    move v3, v2

    goto/16 :goto_1

    .line 625134
    :cond_6
    const-string v37, "current_city"

    move-object/from16 v0, v37

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v37

    if-eqz v37, :cond_7

    .line 625135
    invoke-static/range {p0 .. p1}, LX/6Mb;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v32, v2

    goto/16 :goto_1

    .line 625136
    :cond_7
    const-string v37, "friendship_status"

    move-object/from16 v0, v37

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v37

    if-eqz v37, :cond_8

    .line 625137
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move/from16 v31, v2

    goto/16 :goto_1

    .line 625138
    :cond_8
    const-string v37, "id"

    move-object/from16 v0, v37

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v37

    if-eqz v37, :cond_9

    .line 625139
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v30, v2

    goto/16 :goto_1

    .line 625140
    :cond_9
    const-string v37, "is_memorialized"

    move-object/from16 v0, v37

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v37

    if-eqz v37, :cond_a

    .line 625141
    const/4 v2, 0x1

    .line 625142
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v15

    move/from16 v29, v15

    move v15, v2

    goto/16 :goto_1

    .line 625143
    :cond_a
    const-string v37, "is_message_blocked_by_viewer"

    move-object/from16 v0, v37

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v37

    if-eqz v37, :cond_b

    .line 625144
    const/4 v2, 0x1

    .line 625145
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v14

    move/from16 v28, v14

    move v14, v2

    goto/16 :goto_1

    .line 625146
    :cond_b
    const-string v37, "is_messenger_user"

    move-object/from16 v0, v37

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v37

    if-eqz v37, :cond_c

    .line 625147
    const/4 v2, 0x1

    .line 625148
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v13

    move/from16 v27, v13

    move v13, v2

    goto/16 :goto_1

    .line 625149
    :cond_c
    const-string v37, "is_mobile_pushable"

    move-object/from16 v0, v37

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v37

    if-eqz v37, :cond_d

    .line 625150
    const/4 v2, 0x1

    .line 625151
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v12

    move/from16 v26, v12

    move v12, v2

    goto/16 :goto_1

    .line 625152
    :cond_d
    const-string v37, "is_partial"

    move-object/from16 v0, v37

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v37

    if-eqz v37, :cond_e

    .line 625153
    const/4 v2, 0x1

    .line 625154
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v11

    move/from16 v21, v11

    move v11, v2

    goto/16 :goto_1

    .line 625155
    :cond_e
    const-string v37, "messenger_install_time"

    move-object/from16 v0, v37

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v37

    if-eqz v37, :cond_f

    .line 625156
    const/4 v2, 0x1

    .line 625157
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v24

    move v10, v2

    goto/16 :goto_1

    .line 625158
    :cond_f
    const-string v37, "messenger_invite_priority"

    move-object/from16 v0, v37

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v37

    if-eqz v37, :cond_10

    .line 625159
    const/4 v2, 0x1

    .line 625160
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v22

    move v9, v2

    goto/16 :goto_1

    .line 625161
    :cond_10
    const-string v37, "montage_thread_fbid"

    move-object/from16 v0, v37

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v37

    if-eqz v37, :cond_11

    .line 625162
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v20, v2

    goto/16 :goto_1

    .line 625163
    :cond_11
    const-string v37, "name_search_tokens"

    move-object/from16 v0, v37

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v37

    if-eqz v37, :cond_12

    .line 625164
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v19, v2

    goto/16 :goto_1

    .line 625165
    :cond_12
    const-string v37, "subscribe_status"

    move-object/from16 v0, v37

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v37

    if-eqz v37, :cond_13

    .line 625166
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move/from16 v18, v2

    goto/16 :goto_1

    .line 625167
    :cond_13
    const-string v37, "withTaggingRank"

    move-object/from16 v0, v37

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_14

    .line 625168
    const/4 v2, 0x1

    .line 625169
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v16

    move v8, v2

    goto/16 :goto_1

    .line 625170
    :cond_14
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 625171
    :cond_15
    const/16 v2, 0x13

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 625172
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 625173
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 625174
    if-eqz v7, :cond_16

    .line 625175
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 625176
    :cond_16
    if-eqz v6, :cond_17

    .line 625177
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 625178
    :cond_17
    if-eqz v3, :cond_18

    .line 625179
    const/4 v3, 0x4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 625180
    :cond_18
    const/4 v2, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 625181
    const/4 v2, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 625182
    const/4 v2, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 625183
    if-eqz v15, :cond_19

    .line 625184
    const/16 v2, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 625185
    :cond_19
    if-eqz v14, :cond_1a

    .line 625186
    const/16 v2, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 625187
    :cond_1a
    if-eqz v13, :cond_1b

    .line 625188
    const/16 v2, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 625189
    :cond_1b
    if-eqz v12, :cond_1c

    .line 625190
    const/16 v2, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 625191
    :cond_1c
    if-eqz v11, :cond_1d

    .line 625192
    const/16 v2, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 625193
    :cond_1d
    if-eqz v10, :cond_1e

    .line 625194
    const/16 v3, 0xd

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v24

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 625195
    :cond_1e
    if-eqz v9, :cond_1f

    .line 625196
    const/16 v3, 0xe

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v22

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 625197
    :cond_1f
    const/16 v2, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 625198
    const/16 v2, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 625199
    const/16 v2, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 625200
    if-eqz v8, :cond_20

    .line 625201
    const/16 v3, 0x12

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v16

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 625202
    :cond_20
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_21
    move/from16 v36, v35

    move/from16 v35, v34

    move/from16 v34, v33

    move/from16 v33, v32

    move/from16 v32, v29

    move/from16 v29, v26

    move/from16 v26, v23

    move/from16 v39, v9

    move v9, v3

    move v3, v10

    move v10, v4

    move/from16 v40, v12

    move v12, v6

    move v6, v11

    move v11, v5

    move-wide/from16 v4, v30

    move/from16 v30, v27

    move/from16 v31, v28

    move/from16 v27, v24

    move/from16 v28, v25

    move-wide/from16 v24, v20

    move/from16 v21, v22

    move/from16 v20, v17

    move-wide/from16 v22, v18

    move/from16 v19, v16

    move/from16 v18, v13

    move v13, v7

    move-wide/from16 v16, v14

    move/from16 v7, v40

    move v14, v8

    move/from16 v15, v39

    move v8, v2

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 8

    .prologue
    const/16 v7, 0x11

    const/16 v6, 0x10

    const/4 v3, 0x6

    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    .line 625203
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 625204
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 625205
    if-eqz v0, :cond_0

    .line 625206
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 625207
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 625208
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 625209
    if-eqz v0, :cond_3

    .line 625210
    const-string v1, "birthdate"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 625211
    const/4 p3, 0x0

    .line 625212
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 625213
    invoke-virtual {p0, v0, p3, p3}, LX/15i;->a(III)I

    move-result v1

    .line 625214
    if-eqz v1, :cond_1

    .line 625215
    const-string v2, "day"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 625216
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 625217
    :cond_1
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1, p3}, LX/15i;->a(III)I

    move-result v1

    .line 625218
    if-eqz v1, :cond_2

    .line 625219
    const-string v2, "month"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 625220
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 625221
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 625222
    :cond_3
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 625223
    if-eqz v0, :cond_4

    .line 625224
    const-string v1, "can_see_viewer_montage_thread"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 625225
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 625226
    :cond_4
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 625227
    if-eqz v0, :cond_5

    .line 625228
    const-string v1, "can_viewer_message"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 625229
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 625230
    :cond_5
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 625231
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_6

    .line 625232
    const-string v2, "communicationRank"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 625233
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 625234
    :cond_6
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 625235
    if-eqz v0, :cond_7

    .line 625236
    const-string v1, "current_city"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 625237
    invoke-static {p0, v0, p2}, LX/6Mb;->a(LX/15i;ILX/0nX;)V

    .line 625238
    :cond_7
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 625239
    if-eqz v0, :cond_8

    .line 625240
    const-string v0, "friendship_status"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 625241
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 625242
    :cond_8
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 625243
    if-eqz v0, :cond_9

    .line 625244
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 625245
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 625246
    :cond_9
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 625247
    if-eqz v0, :cond_a

    .line 625248
    const-string v1, "is_memorialized"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 625249
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 625250
    :cond_a
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 625251
    if-eqz v0, :cond_b

    .line 625252
    const-string v1, "is_message_blocked_by_viewer"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 625253
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 625254
    :cond_b
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 625255
    if-eqz v0, :cond_c

    .line 625256
    const-string v1, "is_messenger_user"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 625257
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 625258
    :cond_c
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 625259
    if-eqz v0, :cond_d

    .line 625260
    const-string v1, "is_mobile_pushable"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 625261
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 625262
    :cond_d
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 625263
    if-eqz v0, :cond_e

    .line 625264
    const-string v1, "is_partial"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 625265
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 625266
    :cond_e
    const/16 v0, 0xd

    const-wide/16 v2, 0x0

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 625267
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-eqz v2, :cond_f

    .line 625268
    const-string v2, "messenger_install_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 625269
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 625270
    :cond_f
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 625271
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_10

    .line 625272
    const-string v2, "messenger_invite_priority"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 625273
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 625274
    :cond_10
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 625275
    if-eqz v0, :cond_11

    .line 625276
    const-string v1, "montage_thread_fbid"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 625277
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 625278
    :cond_11
    invoke-virtual {p0, p1, v6}, LX/15i;->g(II)I

    move-result v0

    .line 625279
    if-eqz v0, :cond_12

    .line 625280
    const-string v0, "name_search_tokens"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 625281
    invoke-virtual {p0, p1, v6}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 625282
    :cond_12
    invoke-virtual {p0, p1, v7}, LX/15i;->g(II)I

    move-result v0

    .line 625283
    if-eqz v0, :cond_13

    .line 625284
    const-string v0, "subscribe_status"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 625285
    invoke-virtual {p0, p1, v7}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 625286
    :cond_13
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 625287
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_14

    .line 625288
    const-string v2, "withTaggingRank"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 625289
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 625290
    :cond_14
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 625291
    return-void
.end method
