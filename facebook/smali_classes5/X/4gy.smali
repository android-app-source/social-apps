.class public final enum LX/4gy;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/4gy;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/4gy;

.field public static final enum GOOGLE:LX/4gy;


# instance fields
.field public final clientId:Ljava/lang/String;

.field public final name:Ljava/lang/String;

.field public final type:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 800313
    new-instance v0, LX/4gy;

    const-string v1, "GOOGLE"

    const-string v3, "google"

    const-string v4, "com.google"

    const-string v5, "15057814354-80cg059cn49j6kmhhkjam4b00on1gb2n.apps.googleusercontent.com"

    invoke-direct/range {v0 .. v5}, LX/4gy;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/4gy;->GOOGLE:LX/4gy;

    .line 800314
    const/4 v0, 0x1

    new-array v0, v0, [LX/4gy;

    sget-object v1, LX/4gy;->GOOGLE:LX/4gy;

    aput-object v1, v0, v2

    sput-object v0, LX/4gy;->$VALUES:[LX/4gy;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 800315
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 800316
    iput-object p3, p0, LX/4gy;->name:Ljava/lang/String;

    .line 800317
    iput-object p4, p0, LX/4gy;->type:Ljava/lang/String;

    .line 800318
    iput-object p5, p0, LX/4gy;->clientId:Ljava/lang/String;

    .line 800319
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/4gy;
    .locals 1

    .prologue
    .line 800320
    const-class v0, LX/4gy;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/4gy;

    return-object v0
.end method

.method public static values()[LX/4gy;
    .locals 1

    .prologue
    .line 800321
    sget-object v0, LX/4gy;->$VALUES:[LX/4gy;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/4gy;

    return-object v0
.end method
