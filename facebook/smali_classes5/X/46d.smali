.class public LX/46d;
.super Ljava/io/FilterInputStream;
.source ""


# instance fields
.field private final a:[B

.field private b:I

.field private c:I


# direct methods
.method public constructor <init>(Ljava/io/InputStream;[B)V
    .locals 1

    .prologue
    .line 671474
    invoke-direct {p0, p1}, Ljava/io/FilterInputStream;-><init>(Ljava/io/InputStream;)V

    .line 671475
    if-nez p1, :cond_0

    .line 671476
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 671477
    :cond_0
    if-nez p2, :cond_1

    .line 671478
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 671479
    :cond_1
    iput-object p2, p0, LX/46d;->a:[B

    .line 671480
    return-void
.end method

.method private a()I
    .locals 3

    .prologue
    .line 671471
    iget v0, p0, LX/46d;->b:I

    iget-object v1, p0, LX/46d;->a:[B

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 671472
    const/4 v0, -0x1

    .line 671473
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/46d;->a:[B

    iget v1, p0, LX/46d;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/46d;->b:I

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    goto :goto_0
.end method


# virtual methods
.method public final mark(I)V
    .locals 1

    .prologue
    .line 671464
    iget-object v0, p0, Ljava/io/FilterInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->markSupported()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 671465
    invoke-super {p0, p1}, Ljava/io/FilterInputStream;->mark(I)V

    .line 671466
    iget v0, p0, LX/46d;->b:I

    iput v0, p0, LX/46d;->c:I

    .line 671467
    :cond_0
    return-void
.end method

.method public final read()I
    .locals 2

    .prologue
    .line 671468
    iget-object v0, p0, Ljava/io/FilterInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->read()I

    move-result v0

    .line 671469
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 671470
    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, LX/46d;->a()I

    move-result v0

    goto :goto_0
.end method

.method public final read([B)I
    .locals 2

    .prologue
    .line 671463
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, LX/46d;->read([BII)I

    move-result v0

    return v0
.end method

.method public final read([BII)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v2, -0x1

    .line 671447
    iget-object v0, p0, Ljava/io/FilterInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/InputStream;->read([BII)I

    move-result v0

    .line 671448
    if-eq v0, v2, :cond_1

    .line 671449
    :cond_0
    :goto_0
    return v0

    .line 671450
    :cond_1
    if-nez p3, :cond_2

    move v0, v1

    .line 671451
    goto :goto_0

    :cond_2
    move v0, v1

    .line 671452
    :goto_1
    if-ge v0, p3, :cond_3

    .line 671453
    invoke-direct {p0}, LX/46d;->a()I

    move-result v1

    .line 671454
    if-eq v1, v2, :cond_3

    .line 671455
    add-int v3, p2, v0

    int-to-byte v1, v1

    aput-byte v1, p1, v3

    .line 671456
    add-int/lit8 v0, v0, 0x1

    .line 671457
    goto :goto_1

    .line 671458
    :cond_3
    if-gtz v0, :cond_0

    move v0, v2

    goto :goto_0
.end method

.method public final reset()V
    .locals 2

    .prologue
    .line 671459
    iget-object v0, p0, Ljava/io/FilterInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->markSupported()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 671460
    iget-object v0, p0, Ljava/io/FilterInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->reset()V

    .line 671461
    iget v0, p0, LX/46d;->c:I

    iput v0, p0, LX/46d;->b:I

    return-void

    .line 671462
    :cond_0
    new-instance v0, Ljava/io/IOException;

    const-string v1, "mark is not supported"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
