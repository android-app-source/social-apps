.class public LX/47G;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field public final g:Ljava/lang/String;

.field public final h:Ljava/lang/String;

.field public final i:Ljava/lang/String;

.field public final j:Ljava/lang/String;

.field public final k:I

.field public final l:Ljava/lang/String;

.field public final m:Ljava/lang/String;

.field public final n:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

.field public final o:Ljava/lang/String;

.field public final p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Landroid/os/Bundle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final t:Lcom/facebook/graphql/enums/GraphQLAppStoreDownloadConnectivityPolicy;

.field public u:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/47F;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/47F;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Ljava/lang/Object;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Ljava/lang/Object;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0Px;LX/0Px;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLAppStoreDownloadConnectivityPolicy;)V
    .locals 2
    .param p15    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p16    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/enums/GraphQLAppStoreDownloadConnectivityPolicy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 672020
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 672021
    const/4 v1, 0x0

    iput-object v1, p0, LX/47G;->u:LX/0Px;

    .line 672022
    const/4 v1, 0x0

    iput-object v1, p0, LX/47G;->v:LX/0Px;

    .line 672023
    const/4 v1, 0x0

    iput-object v1, p0, LX/47G;->w:Ljava/lang/Object;

    .line 672024
    const/4 v1, 0x0

    iput-object v1, p0, LX/47G;->x:Ljava/lang/Object;

    .line 672025
    iput-object p1, p0, LX/47G;->a:Ljava/lang/String;

    .line 672026
    iput-object p2, p0, LX/47G;->b:Ljava/lang/String;

    .line 672027
    iput-object p3, p0, LX/47G;->g:Ljava/lang/String;

    .line 672028
    iput-object p4, p0, LX/47G;->c:LX/0Px;

    .line 672029
    iput-object p5, p0, LX/47G;->d:LX/0Px;

    .line 672030
    iput-object p6, p0, LX/47G;->e:Ljava/lang/String;

    .line 672031
    iput-object p7, p0, LX/47G;->f:Ljava/lang/String;

    .line 672032
    iput-object p8, p0, LX/47G;->h:Ljava/lang/String;

    .line 672033
    iput-object p9, p0, LX/47G;->i:Ljava/lang/String;

    .line 672034
    iput-object p10, p0, LX/47G;->j:Ljava/lang/String;

    .line 672035
    iput p11, p0, LX/47G;->k:I

    .line 672036
    iput-object p12, p0, LX/47G;->l:Ljava/lang/String;

    .line 672037
    iput-object p13, p0, LX/47G;->m:Ljava/lang/String;

    .line 672038
    move-object/from16 v0, p14

    iput-object v0, p0, LX/47G;->n:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    .line 672039
    move-object/from16 v0, p15

    iput-object v0, p0, LX/47G;->p:Ljava/lang/String;

    .line 672040
    move-object/from16 v0, p16

    iput-object v0, p0, LX/47G;->q:Ljava/lang/String;

    .line 672041
    move-object/from16 v0, p17

    iput-object v0, p0, LX/47G;->o:Ljava/lang/String;

    .line 672042
    const/4 v1, 0x0

    iput-object v1, p0, LX/47G;->r:Landroid/os/Bundle;

    .line 672043
    const/4 v1, 0x0

    iput-object v1, p0, LX/47G;->s:Ljava/util/Map;

    .line 672044
    move-object/from16 v0, p18

    iput-object v0, p0, LX/47G;->t:Lcom/facebook/graphql/enums/GraphQLAppStoreDownloadConnectivityPolicy;

    .line 672045
    return-void
.end method

.method public static a(LX/47G;Ljava/lang/String;)LX/47G;
    .locals 20

    .prologue
    .line 672046
    new-instance v1, LX/47G;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/47G;->a:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/47G;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/47G;->c:LX/0Px;

    move-object/from16 v0, p0

    iget-object v6, v0, LX/47G;->d:LX/0Px;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/47G;->e:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, LX/47G;->f:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, LX/47G;->h:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v10, v0, LX/47G;->i:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v11, v0, LX/47G;->j:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v12, v0, LX/47G;->k:I

    move-object/from16 v0, p0

    iget-object v13, v0, LX/47G;->l:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v14, v0, LX/47G;->m:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v15, v0, LX/47G;->n:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    move-object/from16 v0, p0

    iget-object v0, v0, LX/47G;->p:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/47G;->q:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/47G;->o:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/47G;->t:Lcom/facebook/graphql/enums/GraphQLAppStoreDownloadConnectivityPolicy;

    move-object/from16 v19, v0

    move-object/from16 v4, p1

    invoke-direct/range {v1 .. v19}, LX/47G;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0Px;LX/0Px;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLAppStoreDownloadConnectivityPolicy;)V

    return-object v1
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 672047
    iget-object v0, p0, LX/47G;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget v0, p0, LX/47G;->k:I

    if-lez v0, :cond_0

    iget-object v0, p0, LX/47G;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/47G;->c:LX/0Px;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/47G;->d:LX/0Px;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
