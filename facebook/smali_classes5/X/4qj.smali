.class public final LX/4qj;
.super LX/3CB;
.source ""


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JacksonStdImpl;
.end annotation


# static fields
.field private static final a:LX/4qj;

.field private static final b:LX/4qj;

.field private static final serialVersionUID:J = 0x1L


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 814576
    new-instance v0, LX/4qj;

    const-class v1, Ljava/lang/String;

    invoke-direct {v0, v1}, LX/4qj;-><init>(Ljava/lang/Class;)V

    sput-object v0, LX/4qj;->a:LX/4qj;

    .line 814577
    new-instance v0, LX/4qj;

    const-class v1, Ljava/lang/Object;

    invoke-direct {v0, v1}, LX/4qj;-><init>(Ljava/lang/Class;)V

    sput-object v0, LX/4qj;->b:LX/4qj;

    return-void
.end method

.method private constructor <init>(Ljava/lang/Class;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 814578
    invoke-direct {p0, p1}, LX/3CB;-><init>(Ljava/lang/Class;)V

    return-void
.end method

.method public static a(Ljava/lang/Class;)LX/4qj;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "LX/4qj;"
        }
    .end annotation

    .prologue
    .line 814579
    const-class v0, Ljava/lang/String;

    if-ne p0, v0, :cond_0

    .line 814580
    sget-object v0, LX/4qj;->a:LX/4qj;

    .line 814581
    :goto_0
    return-object v0

    .line 814582
    :cond_0
    const-class v0, Ljava/lang/Object;

    if-ne p0, v0, :cond_1

    .line 814583
    sget-object v0, LX/4qj;->b:LX/4qj;

    goto :goto_0

    .line 814584
    :cond_1
    new-instance v0, LX/4qj;

    invoke-direct {v0, p0}, LX/4qj;-><init>(Ljava/lang/Class;)V

    goto :goto_0
.end method


# virtual methods
.method public final synthetic b(Ljava/lang/String;LX/0n3;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 814585
    move-object v0, p1

    .line 814586
    return-object v0
.end method
