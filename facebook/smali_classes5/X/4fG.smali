.class public LX/4fG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1cF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1cF",
        "<",
        "LX/1FJ",
        "<",
        "LX/1ln;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/1cF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1cF",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;"
        }
    .end annotation
.end field

.field public final b:LX/1FZ;

.field public final c:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(LX/1cF;LX/1FZ;Ljava/util/concurrent/Executor;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1cF",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;",
            "LX/1FZ;",
            "Ljava/util/concurrent/Executor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 798227
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 798228
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1cF;

    iput-object v0, p0, LX/4fG;->a:LX/1cF;

    .line 798229
    iput-object p2, p0, LX/4fG;->b:LX/1FZ;

    .line 798230
    invoke-static {p3}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, LX/4fG;->c:Ljava/util/concurrent/Executor;

    .line 798231
    return-void
.end method


# virtual methods
.method public final a(LX/1cd;LX/1cW;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/imagepipeline/producers/Consumer",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;",
            "Lcom/facebook/imagepipeline/producers/ProducerContext;",
            ")V"
        }
    .end annotation

    .prologue
    .line 798232
    iget-object v0, p2, LX/1cW;->c:LX/1BV;

    move-object v3, v0

    .line 798233
    iget-object v0, p2, LX/1cW;->a:LX/1bf;

    move-object v0, v0

    .line 798234
    iget-object v1, v0, LX/1bf;->m:LX/33B;

    move-object v5, v1

    .line 798235
    new-instance v0, LX/4fC;

    .line 798236
    iget-object v1, p2, LX/1cW;->b:Ljava/lang/String;

    move-object v4, v1

    .line 798237
    move-object v1, p0

    move-object v2, p1

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, LX/4fC;-><init>(LX/4fG;LX/1cd;LX/1BV;Ljava/lang/String;LX/33B;LX/1cW;)V

    .line 798238
    instance-of v1, v5, LX/4fO;

    if-eqz v1, :cond_0

    .line 798239
    new-instance v1, LX/4fE;

    move-object v4, v5

    check-cast v4, LX/4fO;

    move-object v2, p0

    move-object v3, v0

    move-object v5, p2

    invoke-direct/range {v1 .. v5}, LX/4fE;-><init>(LX/4fG;LX/4fC;LX/4fO;LX/1cW;)V

    .line 798240
    :goto_0
    iget-object v0, p0, LX/4fG;->a:LX/1cF;

    invoke-interface {v0, v1, p2}, LX/1cF;->a(LX/1cd;LX/1cW;)V

    .line 798241
    return-void

    .line 798242
    :cond_0
    new-instance v1, LX/4fF;

    invoke-direct {v1, p0, v0}, LX/4fF;-><init>(LX/4fG;LX/4fC;)V

    goto :goto_0
.end method
