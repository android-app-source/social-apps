.class public LX/4Mw;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 689830
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 689831
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 689832
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 689833
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 689834
    invoke-static {p0, p1}, LX/4Mw;->b(LX/15w;LX/186;)I

    move-result v1

    .line 689835
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 689836
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 20

    .prologue
    .line 689773
    const/4 v14, 0x0

    .line 689774
    const/4 v13, 0x0

    .line 689775
    const/4 v12, 0x0

    .line 689776
    const/4 v11, 0x0

    .line 689777
    const/4 v10, 0x0

    .line 689778
    const/4 v7, 0x0

    .line 689779
    const-wide/16 v8, 0x0

    .line 689780
    const/4 v6, 0x0

    .line 689781
    const/4 v5, 0x0

    .line 689782
    const/4 v4, 0x0

    .line 689783
    const/4 v3, 0x0

    .line 689784
    const/4 v2, 0x0

    .line 689785
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v15

    sget-object v16, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v16

    if-eq v15, v0, :cond_e

    .line 689786
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 689787
    const/4 v2, 0x0

    .line 689788
    :goto_0
    return v2

    .line 689789
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v16, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v16

    if-eq v6, v0, :cond_b

    .line 689790
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 689791
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 689792
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v16

    sget-object v17, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    if-eq v0, v1, :cond_0

    if-eqz v6, :cond_0

    .line 689793
    const-string v16, "approximate_location"

    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_1

    .line 689794
    invoke-static/range {p0 .. p1}, LX/2sz;->a(LX/15w;LX/186;)I

    move-result v6

    move v15, v6

    goto :goto_1

    .line 689795
    :cond_1
    const-string v16, "friends_clusters"

    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_2

    .line 689796
    invoke-static/range {p0 .. p1}, LX/4N0;->a(LX/15w;LX/186;)I

    move-result v6

    move v14, v6

    goto :goto_1

    .line 689797
    :cond_2
    const-string v16, "hideable_token"

    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_3

    .line 689798
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    move v13, v6

    goto :goto_1

    .line 689799
    :cond_3
    const-string v16, "location_category"

    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_4

    .line 689800
    const/4 v3, 0x1

    .line 689801
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/graphql/enums/GraphQLFriendLocationCategory;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFriendLocationCategory;

    move-result-object v6

    move-object v12, v6

    goto :goto_1

    .line 689802
    :cond_4
    const-string v16, "location_context"

    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_5

    .line 689803
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v6

    move v11, v6

    goto :goto_1

    .line 689804
    :cond_5
    const-string v16, "profile"

    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_6

    .line 689805
    invoke-static/range {p0 .. p1}, LX/2bO;->a(LX/15w;LX/186;)I

    move-result v6

    move v7, v6

    goto/16 :goto_1

    .line 689806
    :cond_6
    const-string v16, "radius"

    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_7

    .line 689807
    const/4 v2, 0x1

    .line 689808
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v4

    goto/16 :goto_1

    .line 689809
    :cond_7
    const-string v16, "story_identifier"

    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_8

    .line 689810
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    move v10, v6

    goto/16 :goto_1

    .line 689811
    :cond_8
    const-string v16, "title"

    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_9

    .line 689812
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v6

    move v9, v6

    goto/16 :goto_1

    .line 689813
    :cond_9
    const-string v16, "tracking"

    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 689814
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    move v8, v6

    goto/16 :goto_1

    .line 689815
    :cond_a
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 689816
    :cond_b
    const/16 v6, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->c(I)V

    .line 689817
    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v15}, LX/186;->b(II)V

    .line 689818
    const/4 v6, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v14}, LX/186;->b(II)V

    .line 689819
    const/4 v6, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v13}, LX/186;->b(II)V

    .line 689820
    if-eqz v3, :cond_c

    .line 689821
    const/4 v3, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v12}, LX/186;->a(ILjava/lang/Enum;)V

    .line 689822
    :cond_c
    const/4 v3, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, LX/186;->b(II)V

    .line 689823
    const/4 v3, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 689824
    if-eqz v2, :cond_d

    .line 689825
    const/4 v3, 0x6

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 689826
    :cond_d
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 689827
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 689828
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 689829
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_e
    move v15, v14

    move v14, v13

    move v13, v12

    move-object v12, v11

    move v11, v10

    move v10, v6

    move-wide/from16 v18, v8

    move v9, v5

    move v8, v4

    move-wide/from16 v4, v18

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 7

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    const-wide/16 v2, 0x0

    .line 689726
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 689727
    invoke-virtual {p0, p1, v4}, LX/15i;->g(II)I

    move-result v0

    .line 689728
    if-eqz v0, :cond_0

    .line 689729
    const-string v1, "approximate_location"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 689730
    invoke-static {p0, v0, p2}, LX/2sz;->a(LX/15i;ILX/0nX;)V

    .line 689731
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 689732
    if-eqz v0, :cond_2

    .line 689733
    const-string v1, "friends_clusters"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 689734
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 689735
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v6

    if-ge v1, v6, :cond_1

    .line 689736
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v6

    invoke-static {p0, v6, p2, p3}, LX/4N0;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 689737
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 689738
    :cond_1
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 689739
    :cond_2
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 689740
    if-eqz v0, :cond_3

    .line 689741
    const-string v1, "hideable_token"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 689742
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 689743
    :cond_3
    invoke-virtual {p0, p1, v5, v4}, LX/15i;->a(IIS)S

    move-result v0

    .line 689744
    if-eqz v0, :cond_4

    .line 689745
    const-string v0, "location_category"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 689746
    const-class v0, Lcom/facebook/graphql/enums/GraphQLFriendLocationCategory;

    invoke-virtual {p0, p1, v5, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFriendLocationCategory;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLFriendLocationCategory;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 689747
    :cond_4
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 689748
    if-eqz v0, :cond_5

    .line 689749
    const-string v1, "location_context"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 689750
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 689751
    :cond_5
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 689752
    if-eqz v0, :cond_6

    .line 689753
    const-string v1, "profile"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 689754
    invoke-static {p0, v0, p2, p3}, LX/2bO;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 689755
    :cond_6
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 689756
    cmpl-double v2, v0, v2

    if-eqz v2, :cond_7

    .line 689757
    const-string v2, "radius"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 689758
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 689759
    :cond_7
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 689760
    if-eqz v0, :cond_8

    .line 689761
    const-string v1, "story_identifier"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 689762
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 689763
    :cond_8
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 689764
    if-eqz v0, :cond_9

    .line 689765
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 689766
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 689767
    :cond_9
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 689768
    if-eqz v0, :cond_a

    .line 689769
    const-string v1, "tracking"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 689770
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 689771
    :cond_a
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 689772
    return-void
.end method
