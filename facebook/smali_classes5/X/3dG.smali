.class public abstract LX/3dG;
.super LX/3dH;
.source ""

# interfaces
.implements LX/3Bq;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final b:LX/3d4;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/3d4",
            "<",
            "LX/0jT;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/2lk;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public varargs constructor <init>(LX/2lk;[Ljava/lang/String;)V
    .locals 1
    .param p1    # LX/2lk;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 616497
    invoke-direct {p0}, LX/3dH;-><init>()V

    .line 616498
    new-instance v0, LX/3dJ;

    invoke-direct {v0, p0}, LX/3dJ;-><init>(LX/3dG;)V

    iput-object v0, p0, LX/3dG;->b:LX/3d4;

    .line 616499
    invoke-static {p2}, LX/0Rf;->copyOf([Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, LX/3dG;->c:Ljava/util/Set;

    .line 616500
    iput-object p1, p0, LX/3dG;->d:LX/2lk;

    .line 616501
    return-void
.end method

.method public varargs constructor <init>([Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 616502
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, LX/3dG;-><init>(LX/2lk;[Ljava/lang/String;)V

    .line 616503
    return-void
.end method


# virtual methods
.method public abstract a(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLFeedback;
.end method

.method public final a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)TT;"
        }
    .end annotation

    .prologue
    .line 616504
    new-instance v0, LX/3d2;

    const-class v1, LX/0jT;

    iget-object v2, p0, LX/3dG;->b:LX/3d4;

    invoke-direct {v0, v1, v2}, LX/3d2;-><init>(Ljava/lang/Class;LX/3d4;)V

    .line 616505
    invoke-virtual {v0, p1}, LX/3d2;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 616506
    iget-object v0, p0, LX/3dG;->c:Ljava/util/Set;

    return-object v0
.end method
