.class public LX/4h8;
.super Landroid/content/BroadcastReceiver;
.source ""


# instance fields
.field private final a:LX/281;

.field private final b:LX/27y;

.field private final c:LX/4h7;


# direct methods
.method public constructor <init>(LX/281;LX/27y;LX/4h7;)V
    .locals 0

    .prologue
    .line 800481
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 800482
    iput-object p1, p0, LX/4h8;->a:LX/281;

    .line 800483
    iput-object p2, p0, LX/4h8;->b:LX/27y;

    .line 800484
    iput-object p3, p0, LX/4h8;->c:LX/4h7;

    .line 800485
    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x26

    const v2, -0x3ff344ac

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 800486
    iget-object v1, p0, LX/4h8;->c:LX/4h7;

    invoke-virtual {v1}, LX/31y;->d()V

    .line 800487
    invoke-virtual {p0}, LX/4h8;->getResultCode()I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    .line 800488
    invoke-virtual {p0}, LX/4h8;->getResultData()Ljava/lang/String;

    move-result-object v1

    .line 800489
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, LX/4h8;->getResultExtras(Z)Landroid/os/Bundle;

    move-result-object v2

    .line 800490
    const-string v3, "timestamp"

    const-wide v4, 0x7fffffffffffffffL

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 800491
    iget-object v4, p0, LX/4h8;->c:LX/4h7;

    new-instance v5, LX/282;

    invoke-direct {v5, v1, v2, v3}, LX/282;-><init>(Ljava/lang/String;J)V

    .line 800492
    iput-object v5, v4, LX/4h7;->b:LX/282;

    .line 800493
    iget-object v1, p0, LX/4h8;->a:LX/281;

    iget-object v2, p0, LX/4h8;->c:LX/4h7;

    invoke-virtual {v1, v2}, LX/281;->a(LX/4h7;)V

    .line 800494
    :goto_0
    iget-object v1, p0, LX/4h8;->b:LX/27y;

    if-eqz v1, :cond_0

    .line 800495
    iget-object v1, p0, LX/4h8;->b:LX/27y;

    iget-object v2, p0, LX/4h8;->c:LX/4h7;

    invoke-virtual {v1, v2}, LX/27y;->a(LX/31y;)V

    .line 800496
    :cond_0
    const v1, 0x27df3f39

    invoke-static {p2, v1, v0}, LX/02F;->a(Landroid/content/Intent;II)V

    return-void

    .line 800497
    :cond_1
    iget-object v1, p0, LX/4h8;->c:LX/4h7;

    sget-object v2, LX/4h9;->FAILED:LX/4h9;

    .line 800498
    iput-object v2, v1, LX/31y;->a:LX/4h9;

    .line 800499
    goto :goto_0
.end method
