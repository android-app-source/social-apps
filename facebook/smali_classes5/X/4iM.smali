.class public LX/4iM;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "LX/4iM;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile f:LX/4iM;


# instance fields
.field private final b:Ljava/util/concurrent/ExecutorService;

.field public final c:LX/0dy;

.field public final d:LX/4nf;

.field public final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/webkit/WebView;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 803016
    const-class v0, LX/4iM;

    sput-object v0, LX/4iM;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/4nf;Ljava/util/concurrent/ExecutorService;LX/0dy;)V
    .locals 1
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 803017
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 803018
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/4iM;->e:Ljava/util/List;

    .line 803019
    iput-object p1, p0, LX/4iM;->d:LX/4nf;

    .line 803020
    iput-object p2, p0, LX/4iM;->b:Ljava/util/concurrent/ExecutorService;

    .line 803021
    iput-object p3, p0, LX/4iM;->c:LX/0dy;

    .line 803022
    return-void
.end method

.method public static a(LX/0QB;)LX/4iM;
    .locals 6

    .prologue
    .line 803023
    sget-object v0, LX/4iM;->f:LX/4iM;

    if-nez v0, :cond_1

    .line 803024
    const-class v1, LX/4iM;

    monitor-enter v1

    .line 803025
    :try_start_0
    sget-object v0, LX/4iM;->f:LX/4iM;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 803026
    if-eqz v2, :cond_0

    .line 803027
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 803028
    new-instance p0, LX/4iM;

    .line 803029
    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v3}, LX/FA0;->a(Landroid/content/Context;)LX/4nf;

    move-result-object v3

    move-object v3, v3

    .line 803030
    check-cast v3, LX/4nf;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ExecutorService;

    .line 803031
    const/16 v5, 0x301c

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {v5}, LX/4iH;->a(LX/0Ot;)LX/0dy;

    move-result-object v5

    move-object v5, v5

    .line 803032
    check-cast v5, LX/0dy;

    invoke-direct {p0, v3, v4, v5}, LX/4iM;-><init>(LX/4nf;Ljava/util/concurrent/ExecutorService;LX/0dy;)V

    .line 803033
    move-object v0, p0

    .line 803034
    sput-object v0, LX/4iM;->f:LX/4iM;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 803035
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 803036
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 803037
    :cond_1
    sget-object v0, LX/4iM;->f:LX/4iM;

    return-object v0

    .line 803038
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 803039
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/4iM;Lorg/apache/http/HttpHost;Ljava/util/List;Z)Ljava/util/concurrent/Future;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/http/HttpHost;",
            "Ljava/util/List",
            "<",
            "Landroid/webkit/WebView;",
            ">;Z)",
            "Ljava/util/concurrent/Future",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 803040
    iget-object v0, p0, LX/4iM;->b:Ljava/util/concurrent/ExecutorService;

    new-instance v1, LX/4iL;

    invoke-direct {v1, p0, p2, p1, p3}, LX/4iL;-><init>(LX/4iM;Ljava/util/List;Lorg/apache/http/HttpHost;Z)V

    const v2, 0x6757223e

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/Callable;I)Ljava/util/concurrent/Future;

    move-result-object v0

    return-object v0
.end method
