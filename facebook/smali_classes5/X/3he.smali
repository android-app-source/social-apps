.class public LX/3he;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2eJ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pn;",
        ">",
        "Ljava/lang/Object;",
        "LX/2eJ",
        "<",
        "Ljava/lang/Object;",
        "TE;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/3hc;

.field private final c:F

.field private final d:F

.field private final e:F

.field public final f:LX/3hr;

.field public final g:Lcom/facebook/feedplugins/multishare/MultiShareBusinessLocationItemPartDefinition;

.field private final h:LX/3hW;

.field public final i:Lcom/facebook/feedplugins/multishare/MultiShareProductItemPartDefinition;

.field private final j:LX/0Zb;

.field public final k:Lcom/facebook/feedplugins/multishare/MultiShareEndItemPartDefinition;

.field private final l:LX/17Q;

.field private final m:LX/3ho;

.field public final n:LX/0So;

.field private final o:LX/0ti;

.field private final p:Z

.field private final q:LX/3hq;

.field public final r:Lcom/facebook/feedplugins/multishare/MultiShareSpinnerPartDefinition;

.field private final s:LX/3hd;

.field private final t:LX/2di;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/core/props/FeedProps;FFFLX/3hc;LX/3hd;Lcom/facebook/feedplugins/multishare/MultiShareBusinessLocationItemPartDefinition;LX/3hW;Lcom/facebook/feedplugins/multishare/MultiShareProductItemPartDefinition;LX/0Zb;Lcom/facebook/feedplugins/multishare/MultiShareEndItemPartDefinition;LX/17Q;LX/3ho;LX/0So;Lcom/facebook/feedplugins/multishare/MultiShareSpinnerPartDefinition;LX/0ti;LX/2di;)V
    .locals 4
    .param p1    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # F
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # F
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # F
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/3hc;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/3hd;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;FFF",
            "LX/3hc;",
            "LX/3hd;",
            "Lcom/facebook/feedplugins/multishare/MultiShareBusinessLocationItemPartDefinition;",
            "LX/3hW;",
            "Lcom/facebook/feedplugins/multishare/MultiShareProductItemPartDefinition;",
            "LX/0Zb;",
            "Lcom/facebook/feedplugins/multishare/MultiShareEndItemPartDefinition;",
            "LX/17Q;",
            "LX/3ho;",
            "LX/0So;",
            "Lcom/facebook/feedplugins/multishare/MultiShareSpinnerPartDefinition;",
            "LX/0ti;",
            "LX/2di;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 627273
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 627274
    iput-object p1, p0, LX/3he;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 627275
    iput p2, p0, LX/3he;->c:F

    .line 627276
    iput p3, p0, LX/3he;->d:F

    .line 627277
    iput p4, p0, LX/3he;->e:F

    .line 627278
    iput-object p5, p0, LX/3he;->b:LX/3hc;

    .line 627279
    iput-object p7, p0, LX/3he;->g:Lcom/facebook/feedplugins/multishare/MultiShareBusinessLocationItemPartDefinition;

    .line 627280
    iput-object p8, p0, LX/3he;->h:LX/3hW;

    .line 627281
    iput-object p9, p0, LX/3he;->i:Lcom/facebook/feedplugins/multishare/MultiShareProductItemPartDefinition;

    .line 627282
    iput-object p10, p0, LX/3he;->j:LX/0Zb;

    .line 627283
    iput-object p11, p0, LX/3he;->k:Lcom/facebook/feedplugins/multishare/MultiShareEndItemPartDefinition;

    .line 627284
    move-object/from16 v0, p12

    iput-object v0, p0, LX/3he;->l:LX/17Q;

    .line 627285
    move-object/from16 v0, p13

    iput-object v0, p0, LX/3he;->m:LX/3ho;

    .line 627286
    move-object/from16 v0, p14

    iput-object v0, p0, LX/3he;->n:LX/0So;

    .line 627287
    move-object/from16 v0, p15

    iput-object v0, p0, LX/3he;->r:Lcom/facebook/feedplugins/multishare/MultiShareSpinnerPartDefinition;

    .line 627288
    move-object/from16 v0, p16

    iput-object v0, p0, LX/3he;->o:LX/0ti;

    .line 627289
    invoke-direct {p0}, LX/3he;->m()LX/3hq;

    move-result-object v1

    iput-object v1, p0, LX/3he;->q:LX/3hq;

    .line 627290
    iput-object p6, p0, LX/3he;->s:LX/3hd;

    .line 627291
    invoke-virtual {p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v1}, LX/3ha;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v1

    iput-boolean v1, p0, LX/3he;->p:Z

    .line 627292
    move-object/from16 v0, p17

    iput-object v0, p0, LX/3he;->t:LX/2di;

    .line 627293
    new-instance v1, LX/3hr;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v2}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, LX/3hr;-><init>(Ljava/util/List;LX/3hq;)V

    iput-object v1, p0, LX/3he;->f:LX/3hr;

    .line 627294
    invoke-direct {p0}, LX/3he;->g()V

    .line 627295
    return-void
.end method

.method private static a(ZI)I
    .locals 1

    .prologue
    .line 627233
    if-eqz p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    add-int/lit8 v0, p1, 0x1

    goto :goto_0
.end method

.method private static a(Lcom/facebook/feed/rows/core/props/FeedProps;FFF)LX/0Px;
    .locals 17
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;FFF)",
            "LX/0Px",
            "<",
            "LX/3hq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 627241
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 627242
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v14

    .line 627243
    const/4 v3, 0x0

    .line 627244
    invoke-static {v12}, LX/3he;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v6, 0x1

    .line 627245
    :goto_0
    invoke-static {v12}, LX/3ha;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v12}, LX/3ha;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    const/4 v7, 0x1

    .line 627246
    :goto_1
    invoke-virtual {v12}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v15

    invoke-virtual {v15}, LX/0Px;->size()I

    move-result v16

    const/4 v1, 0x0

    move v13, v1

    :goto_2
    move/from16 v0, v16

    if-ge v13, v0, :cond_3

    invoke-virtual {v15, v13}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 627247
    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    .line 627248
    new-instance v1, LX/3hq;

    const/4 v4, 0x0

    invoke-static {v4, v3}, LX/3he;->a(ZI)I

    move-result v4

    const/4 v5, 0x0

    const/4 v8, 0x0

    move/from16 v9, p1

    move/from16 v10, p2

    move/from16 v11, p3

    invoke-direct/range {v1 .. v11}, LX/3hq;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;IIZZZZFFF)V

    invoke-virtual {v14, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 627249
    add-int/lit8 v3, v3, 0x1

    .line 627250
    add-int/lit8 v1, v13, 0x1

    move v13, v1

    goto :goto_2

    .line 627251
    :cond_1
    const/4 v6, 0x0

    goto :goto_0

    .line 627252
    :cond_2
    const/4 v7, 0x0

    goto :goto_1

    .line 627253
    :cond_3
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MULTI_SHARE_NO_END_CARD:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-static {v12, v1}, LX/1VO;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;)Z

    move-result v1

    .line 627254
    if-nez v1, :cond_4

    .line 627255
    new-instance v1, LX/3hq;

    const/4 v2, 0x1

    invoke-static {v2, v3}, LX/3he;->a(ZI)I

    move-result v4

    const/4 v5, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v2, p0

    move/from16 v9, p1

    move/from16 v10, p2

    move/from16 v11, p3

    invoke-direct/range {v1 .. v11}, LX/3hq;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;IIZZZZFFF)V

    invoke-virtual {v14, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 627256
    :cond_4
    invoke-virtual {v14}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    return-object v1
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 627257
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 627258
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object p0

    if-eqz p0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_2

    const/4 p0, 0x1

    :goto_1
    move v0, p0

    .line 627259
    if-eqz v0, :cond_0

    .line 627260
    const/4 v0, 0x1

    .line 627261
    :goto_2
    return v0

    .line 627262
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 627263
    goto :goto_2

    :cond_2
    const/4 p0, 0x0

    goto :goto_1
.end method

.method public static b(LX/3he;LX/3hq;)V
    .locals 1

    .prologue
    .line 627264
    if-nez p1, :cond_0

    .line 627265
    :goto_0
    return-void

    .line 627266
    :cond_0
    iget-object v0, p0, LX/3he;->f:LX/3hr;

    iget-object v0, v0, LX/3hr;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static c(LX/3he;Z)V
    .locals 1

    .prologue
    .line 627267
    iget-object v0, p0, LX/3he;->b:LX/3hc;

    .line 627268
    iput-boolean p1, v0, LX/3hc;->j:Z

    .line 627269
    return-void
.end method

.method public static d(LX/3he;I)Z
    .locals 2

    .prologue
    .line 627270
    iget-object v0, p0, LX/3he;->b:LX/3hc;

    .line 627271
    iget v1, v0, LX/3hc;->k:I

    move v0, v1

    .line 627272
    if-ne p1, v0, :cond_0

    invoke-static {p0}, LX/3he;->i(LX/3he;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, LX/3he;->n(LX/3he;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, LX/3he;->o(LX/3he;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f(I)Z
    .locals 1

    .prologue
    .line 627139
    invoke-static {p0}, LX/3he;->o(LX/3he;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, LX/3he;->p(LX/3he;)LX/3hq;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3he;->f:LX/3hr;

    iget-object v0, v0, LX/3hr;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3hq;

    .line 627140
    iget-boolean p0, v0, LX/3hq;->e:Z

    move v0, p0

    .line 627141
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g()V
    .locals 5

    .prologue
    .line 627296
    iget-object v0, p0, LX/3he;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget v1, p0, LX/3he;->c:F

    iget v2, p0, LX/3he;->d:F

    iget v3, p0, LX/3he;->e:F

    invoke-static {v0, v1, v2, v3}, LX/3he;->a(Lcom/facebook/feed/rows/core/props/FeedProps;FFF)LX/0Px;

    move-result-object v2

    .line 627297
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_3

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3hq;

    .line 627298
    iget-boolean v4, v0, LX/3hq;->e:Z

    move v4, v4

    .line 627299
    if-nez v4, :cond_1

    .line 627300
    invoke-static {p0, v0}, LX/3he;->b(LX/3he;LX/3hq;)V

    .line 627301
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 627302
    :cond_1
    iget-object v4, p0, LX/3he;->f:LX/3hr;

    .line 627303
    iput-object v0, v4, LX/3hr;->b:LX/3hq;

    .line 627304
    invoke-static {p0}, LX/3he;->h(LX/3he;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p0}, LX/3he;->o(LX/3he;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, LX/3he;->p(LX/3he;)LX/3hq;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 627305
    :cond_2
    invoke-static {p0}, LX/3he;->p(LX/3he;)LX/3hq;

    move-result-object v0

    invoke-static {p0, v0}, LX/3he;->b(LX/3he;LX/3hq;)V

    goto :goto_1

    .line 627306
    :cond_3
    invoke-static {p0}, LX/3he;->h(LX/3he;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-static {p0}, LX/3he;->i(LX/3he;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 627307
    invoke-static {p0}, LX/3he;->o(LX/3he;)Z

    move-result v0

    if-nez v0, :cond_4

    invoke-static {p0}, LX/3he;->n(LX/3he;)Z

    move-result v0

    if-nez v0, :cond_4

    invoke-static {p0}, LX/3he;->i(LX/3he;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 627308
    :cond_4
    :goto_2
    iget-object v0, p0, LX/3he;->b:LX/3hc;

    invoke-virtual {p0}, LX/3he;->a()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    .line 627309
    iput v1, v0, LX/3hc;->k:I

    .line 627310
    :cond_5
    iget-object v0, p0, LX/3he;->b:LX/3hc;

    iget-object v1, p0, LX/3he;->f:LX/3hr;

    .line 627311
    iput-object v1, v0, LX/3hc;->b:LX/3hr;

    .line 627312
    return-void

    .line 627313
    :cond_6
    const/4 v0, 0x1

    invoke-static {p0, v0}, LX/3he;->c(LX/3he;Z)V

    goto :goto_2
.end method

.method public static h(LX/3he;)Z
    .locals 3

    .prologue
    .line 627314
    iget-boolean v0, p0, LX/3he;->p:Z

    if-nez v0, :cond_1

    iget-object v0, p0, LX/3he;->h:LX/3hW;

    .line 627315
    iget-object v1, v0, LX/3hW;->g:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 627316
    iget-object v1, v0, LX/3hW;->a:LX/0ad;

    sget-short v2, LX/3hX;->b:S

    const/4 p0, 0x0

    invoke-interface {v1, v2, p0}, LX/0ad;->a(SZ)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, LX/3hW;->g:Ljava/lang/Boolean;

    .line 627317
    :cond_0
    iget-object v1, v0, LX/3hW;->g:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    move v0, v1

    .line 627318
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static i(LX/3he;)Z
    .locals 3

    .prologue
    .line 627319
    iget-boolean v0, p0, LX/3he;->p:Z

    if-nez v0, :cond_1

    iget-object v0, p0, LX/3he;->h:LX/3hW;

    .line 627320
    iget-object v1, v0, LX/3hW;->h:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 627321
    iget-object v1, v0, LX/3hW;->a:LX/0ad;

    sget-short v2, LX/3hX;->e:S

    const/4 p0, 0x0

    invoke-interface {v1, v2, p0}, LX/0ad;->a(SZ)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, LX/3hW;->h:Ljava/lang/Boolean;

    .line 627322
    :cond_0
    iget-object v1, v0, LX/3hW;->h:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    move v0, v1

    .line 627323
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private m()LX/3hq;
    .locals 11

    .prologue
    const/4 v2, -0x1

    const/4 v4, 0x0

    .line 627234
    iget-object v0, p0, LX/3he;->q:LX/3hq;

    if-nez v0, :cond_0

    .line 627235
    iget-object v1, p0, LX/3he;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {p0}, LX/3he;->b()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-virtual {v1, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 627236
    iget-object v0, p0, LX/3he;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 627237
    iget-object v3, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v3

    .line 627238
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v0}, LX/3he;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    .line 627239
    new-instance v0, LX/3hq;

    const/4 v7, 0x1

    iget v8, p0, LX/3he;->c:F

    iget v9, p0, LX/3he;->d:F

    iget v10, p0, LX/3he;->e:F

    move v3, v2

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v10}, LX/3hq;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;IIZZZZFFF)V

    .line 627240
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/3he;->q:LX/3hq;

    goto :goto_0
.end method

.method public static n(LX/3he;)Z
    .locals 1

    .prologue
    .line 627230
    iget-object v0, p0, LX/3he;->b:LX/3hc;

    .line 627231
    iget-boolean p0, v0, LX/3hc;->j:Z

    move v0, p0

    .line 627232
    return v0
.end method

.method public static o(LX/3he;)Z
    .locals 1

    .prologue
    .line 627227
    iget-object v0, p0, LX/3he;->b:LX/3hc;

    .line 627228
    iget-boolean p0, v0, LX/3hc;->e:Z

    move v0, p0

    .line 627229
    return v0
.end method

.method public static p(LX/3he;)LX/3hq;
    .locals 1

    .prologue
    .line 627226
    iget-object v0, p0, LX/3he;->f:LX/3hr;

    iget-object v0, v0, LX/3hr;->b:LX/3hq;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 627225
    iget-object v0, p0, LX/3he;->f:LX/3hr;

    iget-object v0, v0, LX/3hr;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final a(I)LX/1RC;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/1RC",
            "<",
            "Ljava/lang/Object;",
            "*-TE;*>;"
        }
    .end annotation

    .prologue
    .line 627210
    invoke-virtual {p0, p1}, LX/3he;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3hq;

    .line 627211
    iget-object v1, v0, LX/3hq;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v1, v1

    .line 627212
    iget-object p1, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, p1

    .line 627213
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v1}, LX/1VO;->i(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v1

    .line 627214
    iget-boolean p1, v0, LX/3hq;->e:Z

    move p1, p1

    .line 627215
    if-eqz p1, :cond_0

    .line 627216
    iget-object v1, p0, LX/3he;->k:Lcom/facebook/feedplugins/multishare/MultiShareEndItemPartDefinition;

    .line 627217
    :goto_0
    move-object v0, v1

    .line 627218
    return-object v0

    .line 627219
    :cond_0
    iget-boolean p1, v0, LX/3hq;->h:Z

    move p1, p1

    .line 627220
    if-eqz p1, :cond_1

    .line 627221
    iget-object v1, p0, LX/3he;->r:Lcom/facebook/feedplugins/multishare/MultiShareSpinnerPartDefinition;

    goto :goto_0

    .line 627222
    :cond_1
    if-eqz v1, :cond_2

    .line 627223
    iget-object v1, p0, LX/3he;->g:Lcom/facebook/feedplugins/multishare/MultiShareBusinessLocationItemPartDefinition;

    goto :goto_0

    .line 627224
    :cond_2
    iget-object v1, p0, LX/3he;->i:Lcom/facebook/feedplugins/multishare/MultiShareProductItemPartDefinition;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0Px;Ljava/lang/String;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 627198
    iget-object v1, p0, LX/3he;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 627199
    new-instance v2, LX/4XB;

    invoke-direct {v2}, LX/4XB;-><init>()V

    invoke-static {v1}, LX/2dc;->a(Lcom/facebook/graphql/model/GraphQLImage;)LX/2dc;

    move-result-object v1

    invoke-virtual {v1, p3}, LX/2dc;->b(Ljava/lang/String;)LX/2dc;

    move-result-object v1

    invoke-virtual {v1}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/4XB;->b(Lcom/facebook/graphql/model/GraphQLImage;)LX/4XB;

    move-result-object v1

    invoke-virtual {v1}, LX/4XB;->a()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    .line 627200
    iget-object v1, p0, LX/3he;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v1

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v1}, LX/39x;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/39x;

    move-result-object v1

    invoke-virtual {v1, p1}, LX/39x;->e(Ljava/lang/String;)LX/39x;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/39x;->f(Ljava/lang/String;)LX/39x;

    move-result-object v1

    invoke-virtual {v1, v2}, LX/39x;->a(Lcom/facebook/graphql/model/GraphQLMedia;)LX/39x;

    move-result-object v1

    move-object/from16 v0, p4

    invoke-virtual {v1, v0}, LX/39x;->a(LX/0Px;)LX/39x;

    move-result-object v1

    new-instance v2, LX/173;

    invoke-direct {v2}, LX/173;-><init>()V

    move-object/from16 v0, p5

    invoke-virtual {v2, v0}, LX/173;->a(Ljava/lang/String;)LX/173;

    move-result-object v2

    invoke-virtual {v2}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/39x;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)LX/39x;

    move-result-object v1

    invoke-virtual {v1}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    .line 627201
    invoke-virtual {p0}, LX/3he;->a()I

    move-result v3

    .line 627202
    iget-object v2, p0, LX/3he;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    .line 627203
    iget-object v1, p0, LX/3he;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 627204
    invoke-static {v1}, LX/3he;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v4

    if-nez v4, :cond_1

    const/4 v6, 0x1

    .line 627205
    :goto_0
    invoke-static {v1}, LX/3ha;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {v1}, LX/3ha;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    const/4 v7, 0x1

    .line 627206
    :goto_1
    new-instance v1, LX/3hq;

    const/4 v4, 0x0

    invoke-static {v4, v3}, LX/3he;->a(ZI)I

    move-result v4

    const/4 v5, 0x0

    const/4 v8, 0x0

    iget v9, p0, LX/3he;->c:F

    iget v10, p0, LX/3he;->d:F

    iget v11, p0, LX/3he;->e:F

    invoke-direct/range {v1 .. v11}, LX/3hq;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;IIZZZZFFF)V

    invoke-static {p0, v1}, LX/3he;->b(LX/3he;LX/3hq;)V

    .line 627207
    return-void

    .line 627208
    :cond_1
    const/4 v6, 0x0

    goto :goto_0

    .line 627209
    :cond_2
    const/4 v7, 0x0

    goto :goto_1
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 627195
    iget-object v0, p0, LX/3he;->b:LX/3hc;

    .line 627196
    iput-boolean p1, v0, LX/3hc;->c:Z

    .line 627197
    return-void
.end method

.method public final b()Lcom/facebook/graphql/model/GraphQLStoryAttachment;
    .locals 1

    .prologue
    .line 627192
    iget-object v0, p0, LX/3he;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 627193
    iget-object p0, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, p0

    .line 627194
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    return-object v0
.end method

.method public final b(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 627189
    invoke-static {p0, p1}, LX/3he;->d(LX/3he;I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, LX/3he;->m()LX/3hq;

    move-result-object v0

    :goto_0
    return-object v0

    .line 627190
    :cond_0
    iget-object v0, p0, LX/3he;->f:LX/3hr;

    iget-object v0, v0, LX/3hr;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3hq;

    move-object v0, v0

    .line 627191
    goto :goto_0
.end method

.method public final c()V
    .locals 4

    .prologue
    .line 627174
    iget-object v0, p0, LX/3he;->s:LX/3hd;

    .line 627175
    iget-object v1, v0, LX/3hd;->a:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    if-nez v1, :cond_2

    const/4 v1, 0x0

    :goto_0
    move v0, v1

    .line 627176
    if-nez v0, :cond_1

    iget-object v0, p0, LX/3he;->s:LX/3hd;

    .line 627177
    iget-object v1, v0, LX/3hd;->a:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    if-nez v1, :cond_3

    const/4 v1, 0x0

    :goto_1
    move v0, v1

    .line 627178
    if-nez v0, :cond_1

    .line 627179
    iget-object v0, p0, LX/3he;->b:LX/3hc;

    iget-object v1, p0, LX/3he;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p0, LX/3he;->t:LX/2di;

    invoke-static {v0, v1, v2}, LX/3hv;->a(LX/3hc;Lcom/facebook/feed/rows/core/props/FeedProps;LX/2di;)V

    .line 627180
    iget-object v0, p0, LX/3he;->s:LX/3hd;

    iget-object v1, p0, LX/3he;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 627181
    iget-object v2, v0, LX/3hd;->b:LX/1Pq;

    if-eqz v2, :cond_0

    .line 627182
    iget-object v2, v0, LX/3hd;->b:LX/1Pq;

    const/4 v3, 0x1

    new-array v3, v3, [Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 p0, 0x0

    aput-object v1, v3, p0

    invoke-interface {v2, v3}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 627183
    :cond_0
    :goto_2
    return-void

    .line 627184
    :cond_1
    iget-object v0, p0, LX/3he;->b:LX/3hc;

    const/4 v1, 0x1

    .line 627185
    iput-boolean v1, v0, LX/3hc;->d:Z

    .line 627186
    goto :goto_2

    :cond_2
    iget-object v1, v0, LX/3hd;->a:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    .line 627187
    iget v0, v1, Landroid/support/v7/widget/RecyclerView;->M:I

    move v1, v0

    .line 627188
    goto :goto_0

    :cond_3
    iget-object v1, v0, LX/3hd;->a:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->e()Z

    move-result v1

    goto :goto_1
.end method

.method public final c(I)V
    .locals 8

    .prologue
    .line 627142
    if-lez p1, :cond_0

    iget-object v0, p0, LX/3he;->b:LX/3hc;

    .line 627143
    iget-boolean v1, v0, LX/3hc;->i:Z

    move v0, v1

    .line 627144
    if-nez v0, :cond_0

    .line 627145
    iget-object v0, p0, LX/3he;->b:LX/3hc;

    const/4 v1, 0x1

    .line 627146
    iput-boolean v1, v0, LX/3hc;->i:Z

    .line 627147
    :cond_0
    iget-object v3, p0, LX/3he;->b:LX/3hc;

    iget-object v4, p0, LX/3he;->n:LX/0So;

    invoke-interface {v4}, LX/0So;->now()J

    move-result-wide v5

    .line 627148
    iput-wide v5, v3, LX/3hc;->g:J

    .line 627149
    iget-object v0, p0, LX/3he;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 627150
    invoke-static {v0}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 627151
    iget-object v1, p0, LX/3he;->b:LX/3hc;

    .line 627152
    iput p1, v1, LX/3hc;->a:I

    .line 627153
    invoke-static {v0}, LX/182;->s(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    invoke-direct {p0, p1}, LX/3he;->f(I)Z

    move-result v2

    invoke-static {v2, p1}, LX/3he;->a(ZI)I

    move-result v2

    invoke-static {v0}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v0

    invoke-static {v1, v2, v0}, LX/17Q;->a(ZILX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 627154
    const/4 v1, 0x0

    .line 627155
    if-ltz p1, :cond_1

    invoke-virtual {p0}, LX/3he;->a()I

    move-result v2

    if-lt p1, v2, :cond_4

    .line 627156
    :cond_1
    :goto_0
    move v1, v1

    .line 627157
    if-nez v1, :cond_2

    .line 627158
    iget-object v1, p0, LX/3he;->j:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 627159
    :cond_2
    invoke-static {p0}, LX/3he;->h(LX/3he;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 627160
    iget-object v1, p0, LX/3he;->m:LX/3ho;

    invoke-virtual {v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->r()LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, LX/3he;->a()I

    move-result v2

    .line 627161
    if-eqz v0, :cond_3

    invoke-static {v1, v2, p1, p0}, LX/3ho;->a(LX/3ho;IILX/3he;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 627162
    :cond_3
    :goto_1
    return-void

    :cond_4
    invoke-static {p0}, LX/3he;->h(LX/3he;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {p0}, LX/3he;->n(LX/3he;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {p0, p1}, LX/3he;->d(LX/3he;I)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    .line 627163
    :cond_5
    invoke-static {p0}, LX/3ho;->e(LX/3he;)Z

    move-result v3

    if-eqz v3, :cond_7

    iget v3, v1, LX/3ho;->f:I

    if-ge v2, v3, :cond_7

    const/4 v3, 0x1

    :goto_2
    move v3, v3

    .line 627164
    if-nez v3, :cond_6

    .line 627165
    invoke-static {p0}, LX/3ho;->d(LX/3he;)V

    goto :goto_1

    .line 627166
    :cond_6
    iget v3, v1, LX/3ho;->d:I

    .line 627167
    new-instance v4, LX/JRl;

    invoke-direct {v4}, LX/JRl;-><init>()V

    move-object v4, v4

    .line 627168
    const-string v5, "client_id"

    invoke-virtual {v4, v5, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v5

    const-string v6, "after"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v5

    const-string v6, "last"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 627169
    iget-object v5, v1, LX/3ho;->a:LX/0tX;

    invoke-static {v4}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v4

    sget-object v6, LX/0zS;->a:LX/0zS;

    invoke-virtual {v4, v6}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v4

    invoke-virtual {v5, v4}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v4

    move-object v4, v4

    .line 627170
    new-instance v5, LX/JRk;

    invoke-direct {v5, v1, p0}, LX/JRk;-><init>(LX/3ho;LX/3he;)V

    move-object v5, v5

    .line 627171
    const/4 v6, 0x1

    invoke-virtual {p0, v6}, LX/3he;->a(Z)V

    .line 627172
    iget-object v6, v1, LX/3ho;->b:LX/1Ck;

    const-string v7, "FETCH_MULTISHARE_KEY"

    new-instance p1, LX/JRj;

    invoke-direct {p1, v1, v4}, LX/JRj;-><init>(LX/3ho;Lcom/google/common/util/concurrent/ListenableFuture;)V

    invoke-virtual {v6, v7, p1, v5}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 627173
    goto :goto_1

    :cond_7
    const/4 v3, 0x0

    goto :goto_2
.end method
