.class public final LX/4DC;
.super LX/2vO;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 678976
    invoke-direct {p0}, LX/2vO;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/4Cf;)LX/4DC;
    .locals 1

    .prologue
    .line 678974
    const-string v0, "creative"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 678975
    return-object p0
.end method

.method public final a(LX/4DB;)LX/4DC;
    .locals 1

    .prologue
    .line 678972
    const-string v0, "audience"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 678973
    return-object p0
.end method

.method public final a(Ljava/lang/Integer;)LX/4DC;
    .locals 1

    .prologue
    .line 678970
    const-string v0, "stop_time"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 678971
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/4DC;
    .locals 1

    .prologue
    .line 678968
    const-string v0, "page_id"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 678969
    return-object p0
.end method

.method public final b(Ljava/lang/Integer;)LX/4DC;
    .locals 1

    .prologue
    .line 678966
    const-string v0, "budget"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 678967
    return-object p0
.end method

.method public final b(Ljava/lang/String;)LX/4DC;
    .locals 1

    .prologue
    .line 678960
    const-string v0, "object_id"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 678961
    return-object p0
.end method

.method public final c(Ljava/lang/String;)LX/4DC;
    .locals 1

    .prologue
    .line 678964
    const-string v0, "ad_account_id"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 678965
    return-object p0
.end method

.method public final d(Ljava/lang/String;)LX/4DC;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/BoostedComponentMobileAppID;
        .end annotation
    .end param

    .prologue
    .line 678962
    const-string v0, "boosted_component_app"

    invoke-virtual {p0, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 678963
    return-object p0
.end method
