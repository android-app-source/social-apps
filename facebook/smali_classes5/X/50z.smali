.class public final LX/50z;
.super LX/50y;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LX/50y",
        "<TK;",
        "Ljava/util/Collection",
        "<TV;>;>;"
    }
.end annotation


# instance fields
.field public transient a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;",
            "Ljava/util/Collection",
            "<TV;>;>;>;"
        }
    .end annotation
.end field

.field public transient b:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Ljava/util/Collection",
            "<TV;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Map;Ljava/lang/Object;)V
    .locals 0
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<TK;",
            "Ljava/util/Collection",
            "<TV;>;>;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 824257
    invoke-direct {p0, p1, p2}, LX/50y;-><init>(Ljava/util/Map;Ljava/lang/Object;)V

    .line 824258
    return-void
.end method


# virtual methods
.method public final containsValue(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 824259
    invoke-virtual {p0}, LX/50z;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final entrySet()Ljava/util/Set;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;",
            "Ljava/util/Collection",
            "<TV;>;>;>;"
        }
    .end annotation

    .prologue
    .line 824260
    iget-object v1, p0, LX/18e;->mutex:Ljava/lang/Object;

    monitor-enter v1

    .line 824261
    :try_start_0
    iget-object v0, p0, LX/50z;->a:Ljava/util/Set;

    if-nez v0, :cond_0

    .line 824262
    new-instance v0, LX/514;

    invoke-virtual {p0}, LX/50y;->a()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    iget-object v3, p0, LX/18e;->mutex:Ljava/lang/Object;

    invoke-direct {v0, v2, v3}, LX/514;-><init>(Ljava/util/Set;Ljava/lang/Object;)V

    iput-object v0, p0, LX/50z;->a:Ljava/util/Set;

    .line 824263
    :cond_0
    iget-object v0, p0, LX/50z;->a:Ljava/util/Set;

    monitor-exit v1

    return-object v0

    .line 824264
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 824265
    iget-object v1, p0, LX/18e;->mutex:Ljava/lang/Object;

    monitor-enter v1

    .line 824266
    :try_start_0
    invoke-super {p0, p1}, LX/50y;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 824267
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    monitor-exit v1

    return-object v0

    :cond_0
    iget-object v2, p0, LX/18e;->mutex:Ljava/lang/Object;

    invoke-static {v0, v2}, LX/2sR;->d(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    goto :goto_0

    .line 824268
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final values()Ljava/util/Collection;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/util/Collection",
            "<TV;>;>;"
        }
    .end annotation

    .prologue
    .line 824269
    iget-object v1, p0, LX/18e;->mutex:Ljava/lang/Object;

    monitor-enter v1

    .line 824270
    :try_start_0
    iget-object v0, p0, LX/50z;->b:Ljava/util/Collection;

    if-nez v0, :cond_0

    .line 824271
    new-instance v0, LX/516;

    invoke-virtual {p0}, LX/50y;->a()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    iget-object v3, p0, LX/18e;->mutex:Ljava/lang/Object;

    invoke-direct {v0, v2, v3}, LX/516;-><init>(Ljava/util/Collection;Ljava/lang/Object;)V

    iput-object v0, p0, LX/50z;->b:Ljava/util/Collection;

    .line 824272
    :cond_0
    iget-object v0, p0, LX/50z;->b:Ljava/util/Collection;

    monitor-exit v1

    return-object v0

    .line 824273
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
