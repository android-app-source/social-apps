.class public final LX/4nI;
.super Ljava/io/InputStream;
.source ""


# instance fields
.field public final synthetic a:LX/3DZ;

.field private b:J

.field private c:Ljava/io/InputStream;


# direct methods
.method public constructor <init>(LX/3DZ;J)V
    .locals 0

    .prologue
    .line 806695
    iput-object p1, p0, LX/4nI;->a:LX/3DZ;

    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 806696
    iput-wide p2, p0, LX/4nI;->b:J

    .line 806697
    invoke-direct {p0}, LX/4nI;->a()Z

    .line 806698
    return-void
.end method

.method private a([BII)I
    .locals 1

    .prologue
    .line 806699
    iget-object v0, p0, LX/4nI;->c:Ljava/io/InputStream;

    if-nez v0, :cond_0

    .line 806700
    const/4 v0, -0x1

    .line 806701
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/4nI;->c:Ljava/io/InputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/InputStream;->read([BII)I

    move-result v0

    goto :goto_0
.end method

.method private a()Z
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 806702
    iget-object v0, p0, LX/4nI;->a:LX/3DZ;

    invoke-static {v0}, LX/3DZ;->o(LX/3DZ;)Ljava/util/Map;

    move-result-object v0

    .line 806703
    const/4 v2, 0x0

    .line 806704
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 806705
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2WF;

    iget-wide v6, p0, LX/4nI;->b:J

    invoke-virtual {v1, v6, v7}, LX/2WF;->a(J)Z

    move-result v1

    if-eqz v1, :cond_3

    :goto_1
    move-object v2, v0

    .line 806706
    goto :goto_0

    .line 806707
    :cond_0
    if-eqz v2, :cond_2

    .line 806708
    iget-object v0, p0, LX/4nI;->c:Ljava/io/InputStream;

    if-eqz v0, :cond_1

    .line 806709
    iget-object v0, p0, LX/4nI;->c:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 806710
    :cond_1
    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    iput-object v1, p0, LX/4nI;->c:Ljava/io/InputStream;

    .line 806711
    iget-object v1, p0, LX/4nI;->c:Ljava/io/InputStream;

    iget-wide v4, p0, LX/4nI;->b:J

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2WF;

    iget-wide v6, v0, LX/2WF;->a:J

    sub-long/2addr v4, v6

    invoke-virtual {v1, v4, v5}, Ljava/io/InputStream;->skip(J)J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 806712
    const/4 v0, 0x1

    .line 806713
    :goto_2
    return v0

    .line 806714
    :catch_0
    move v0, v3

    goto :goto_2

    :cond_2
    move v0, v3

    .line 806715
    goto :goto_2

    :cond_3
    move-object v0, v2

    goto :goto_1
.end method


# virtual methods
.method public final close()V
    .locals 1

    .prologue
    .line 806716
    iget-object v0, p0, LX/4nI;->c:Ljava/io/InputStream;

    if-eqz v0, :cond_0

    .line 806717
    iget-object v0, p0, LX/4nI;->c:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 806718
    const/4 v0, 0x0

    iput-object v0, p0, LX/4nI;->c:Ljava/io/InputStream;

    .line 806719
    :cond_0
    return-void
.end method

.method public final read()I
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 806720
    new-array v0, v2, [B

    .line 806721
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1, v2}, LX/4nI;->read([BII)I

    move-result v0

    return v0
.end method

.method public final read([BII)I
    .locals 6

    .prologue
    .line 806722
    invoke-direct {p0, p1, p2, p3}, LX/4nI;->a([BII)I

    move-result v0

    .line 806723
    if-gez v0, :cond_0

    .line 806724
    invoke-direct {p0}, LX/4nI;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 806725
    invoke-direct {p0, p1, p2, p3}, LX/4nI;->a([BII)I

    move-result v0

    .line 806726
    :cond_0
    if-lez v0, :cond_1

    .line 806727
    iget-wide v2, p0, LX/4nI;->b:J

    int-to-long v4, v0

    add-long/2addr v2, v4

    iput-wide v2, p0, LX/4nI;->b:J

    .line 806728
    :cond_1
    return v0
.end method
