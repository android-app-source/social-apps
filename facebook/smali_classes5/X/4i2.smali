.class public LX/4i2;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 802410
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(JLX/0So;LX/0Uo;Z)LX/0P1;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "LX/0So;",
            "LX/0Uo;",
            "Z)",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 802411
    invoke-virtual {p3}, LX/0Uo;->k()LX/03R;

    move-result-object v0

    sget-object v1, LX/03R;->YES:LX/03R;

    if-eq v0, v1, :cond_2

    const-wide/16 v8, 0x0

    .line 802412
    iget-wide v6, p3, LX/0Uo;->M:J

    cmp-long v6, v6, v8

    if-gtz v6, :cond_0

    iget-wide v6, p3, LX/0Uo;->P:J

    cmp-long v6, v6, v8

    if-lez v6, :cond_3

    :cond_0
    const/4 v6, 0x1

    :goto_0
    move v0, v6

    .line 802413
    if-nez v0, :cond_2

    const/4 v0, 0x1

    .line 802414
    :goto_1
    invoke-virtual {p3}, LX/0Uo;->c()J

    move-result-wide v2

    .line 802415
    const-wide/16 v4, 0x0

    cmp-long v1, p0, v4

    if-lez v1, :cond_1

    .line 802416
    invoke-interface {p2}, LX/0So;->now()J

    move-result-wide v4

    sub-long/2addr v4, p0

    .line 802417
    sub-long/2addr v2, v4

    .line 802418
    :cond_1
    new-instance v1, LX/0P2;

    invoke-direct {v1}, LX/0P2;-><init>()V

    const-string v4, "time_since_launch_ms"

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v4, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    const-string v2, "process_uptime"

    .line 802419
    invoke-static {}, Landroid/os/Process;->getElapsedCpuTime()J

    move-result-wide v6

    move-wide v4, v6

    .line 802420
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    const-string v2, "maybe_cold_start"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "scenario_first_run"

    invoke-static {p4}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    .line 802421
    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    return-object v0

    .line 802422
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    const/4 v6, 0x0

    goto :goto_0
.end method
