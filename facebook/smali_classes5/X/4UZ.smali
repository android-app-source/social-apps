.class public final enum LX/4UZ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/4UZ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/4UZ;

.field public static final enum CRITICAL:LX/4UZ;

.field public static final enum ERROR:LX/4UZ;

.field public static final enum WARNING:LX/4UZ;


# instance fields
.field private final name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 741448
    new-instance v0, LX/4UZ;

    const-string v1, "CRITICAL"

    const-string v2, "CRITICAL"

    invoke-direct {v0, v1, v3, v2}, LX/4UZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/4UZ;->CRITICAL:LX/4UZ;

    .line 741449
    new-instance v0, LX/4UZ;

    const-string v1, "ERROR"

    const-string v2, "ERROR"

    invoke-direct {v0, v1, v4, v2}, LX/4UZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/4UZ;->ERROR:LX/4UZ;

    .line 741450
    new-instance v0, LX/4UZ;

    const-string v1, "WARNING"

    const-string v2, "WARNING"

    invoke-direct {v0, v1, v5, v2}, LX/4UZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/4UZ;->WARNING:LX/4UZ;

    .line 741451
    const/4 v0, 0x3

    new-array v0, v0, [LX/4UZ;

    sget-object v1, LX/4UZ;->CRITICAL:LX/4UZ;

    aput-object v1, v0, v3

    sget-object v1, LX/4UZ;->ERROR:LX/4UZ;

    aput-object v1, v0, v4

    sget-object v1, LX/4UZ;->WARNING:LX/4UZ;

    aput-object v1, v0, v5

    sput-object v0, LX/4UZ;->$VALUES:[LX/4UZ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 741452
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 741453
    iput-object p3, p0, LX/4UZ;->name:Ljava/lang/String;

    .line 741454
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/4UZ;
    .locals 1

    .prologue
    .line 741455
    const-class v0, LX/4UZ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/4UZ;

    return-object v0
.end method

.method public static values()[LX/4UZ;
    .locals 1

    .prologue
    .line 741456
    sget-object v0, LX/4UZ;->$VALUES:[LX/4UZ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/4UZ;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 741457
    iget-object v0, p0, LX/4UZ;->name:Ljava/lang/String;

    return-object v0
.end method
