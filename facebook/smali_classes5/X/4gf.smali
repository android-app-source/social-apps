.class public LX/4gf;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Landroid/net/Uri;

.field public d:Landroid/net/Uri;

.field public e:LX/4ge;

.field public f:Ljava/lang/String;

.field public g:Z

.field public h:Z

.field public i:Lcom/facebook/messaging/business/common/calltoaction/model/CTAUserConfirmation;

.field public j:D

.field public k:Z

.field public l:Lcom/facebook/messaging/business/common/calltoaction/model/CTAPaymentInfo;

.field public m:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 799959
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 799960
    return-void
.end method

.method public constructor <init>(Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;)V
    .locals 2

    .prologue
    .line 799944
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 799945
    iget-object v0, p1, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->a:Ljava/lang/String;

    iput-object v0, p0, LX/4gf;->a:Ljava/lang/String;

    .line 799946
    iget-object v0, p1, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->b:Ljava/lang/String;

    iput-object v0, p0, LX/4gf;->b:Ljava/lang/String;

    .line 799947
    iget-object v0, p1, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->c:Landroid/net/Uri;

    iput-object v0, p0, LX/4gf;->c:Landroid/net/Uri;

    .line 799948
    iget-object v0, p1, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->d:Landroid/net/Uri;

    iput-object v0, p0, LX/4gf;->d:Landroid/net/Uri;

    .line 799949
    iget-object v0, p1, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->e:LX/4ge;

    iput-object v0, p0, LX/4gf;->e:LX/4ge;

    .line 799950
    iget-object v0, p1, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->f:Ljava/lang/String;

    iput-object v0, p0, LX/4gf;->f:Ljava/lang/String;

    .line 799951
    iget-boolean v0, p1, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->g:Z

    iput-boolean v0, p0, LX/4gf;->g:Z

    .line 799952
    iget-boolean v0, p1, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->h:Z

    iput-boolean v0, p0, LX/4gf;->h:Z

    .line 799953
    iget-object v0, p1, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->k:Lcom/facebook/messaging/business/common/calltoaction/model/CTAUserConfirmation;

    iput-object v0, p0, LX/4gf;->i:Lcom/facebook/messaging/business/common/calltoaction/model/CTAUserConfirmation;

    .line 799954
    iget-wide v0, p1, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->i:D

    iput-wide v0, p0, LX/4gf;->j:D

    .line 799955
    iget-boolean v0, p1, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->j:Z

    iput-boolean v0, p0, LX/4gf;->k:Z

    .line 799956
    iget-object v0, p1, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->l:Lcom/facebook/messaging/business/common/calltoaction/model/CTAPaymentInfo;

    iput-object v0, p0, LX/4gf;->l:Lcom/facebook/messaging/business/common/calltoaction/model/CTAPaymentInfo;

    .line 799957
    iget-object v0, p1, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->m:Ljava/lang/String;

    iput-object v0, p0, LX/4gf;->m:Ljava/lang/String;

    .line 799958
    return-void
.end method


# virtual methods
.method public final a(D)LX/4gf;
    .locals 1

    .prologue
    .line 799961
    iput-wide p1, p0, LX/4gf;->j:D

    .line 799962
    return-object p0
.end method

.method public final a(LX/4ge;)LX/4gf;
    .locals 0

    .prologue
    .line 799967
    iput-object p1, p0, LX/4gf;->e:LX/4ge;

    .line 799968
    return-object p0
.end method

.method public final a(Lcom/facebook/messaging/business/common/calltoaction/model/CTAPaymentInfo;)LX/4gf;
    .locals 0

    .prologue
    .line 799963
    iput-object p1, p0, LX/4gf;->l:Lcom/facebook/messaging/business/common/calltoaction/model/CTAPaymentInfo;

    .line 799964
    return-object p0
.end method

.method public final a(Lcom/facebook/messaging/business/common/calltoaction/model/CTAUserConfirmation;)LX/4gf;
    .locals 0

    .prologue
    .line 799965
    iput-object p1, p0, LX/4gf;->i:Lcom/facebook/messaging/business/common/calltoaction/model/CTAUserConfirmation;

    .line 799966
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/4gf;
    .locals 0

    .prologue
    .line 799940
    iput-object p1, p0, LX/4gf;->a:Ljava/lang/String;

    .line 799941
    return-object p0
.end method

.method public final a(Z)LX/4gf;
    .locals 0

    .prologue
    .line 799942
    iput-boolean p1, p0, LX/4gf;->g:Z

    .line 799943
    return-object p0
.end method

.method public final b(Ljava/lang/String;)LX/4gf;
    .locals 0

    .prologue
    .line 799938
    iput-object p1, p0, LX/4gf;->b:Ljava/lang/String;

    .line 799939
    return-object p0
.end method

.method public final b(Z)LX/4gf;
    .locals 0

    .prologue
    .line 799936
    iput-boolean p1, p0, LX/4gf;->h:Z

    .line 799937
    return-object p0
.end method

.method public final c(Ljava/lang/String;)LX/4gf;
    .locals 1

    .prologue
    .line 799933
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    :goto_0
    iput-object v0, p0, LX/4gf;->c:Landroid/net/Uri;

    .line 799934
    return-object p0

    .line 799935
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Z)LX/4gf;
    .locals 0

    .prologue
    .line 799931
    iput-boolean p1, p0, LX/4gf;->k:Z

    .line 799932
    return-object p0
.end method

.method public final d(Ljava/lang/String;)LX/4gf;
    .locals 1

    .prologue
    .line 799928
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    :goto_0
    iput-object v0, p0, LX/4gf;->d:Landroid/net/Uri;

    .line 799929
    return-object p0

    .line 799930
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e(Ljava/lang/String;)LX/4gf;
    .locals 0

    .prologue
    .line 799926
    iput-object p1, p0, LX/4gf;->f:Ljava/lang/String;

    .line 799927
    return-object p0
.end method

.method public final f(Ljava/lang/String;)LX/4gf;
    .locals 0

    .prologue
    .line 799923
    iput-object p1, p0, LX/4gf;->m:Ljava/lang/String;

    .line 799924
    return-object p0
.end method

.method public final n()Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;
    .locals 1

    .prologue
    .line 799925
    new-instance v0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    invoke-direct {v0, p0}, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;-><init>(LX/4gf;)V

    return-object v0
.end method
