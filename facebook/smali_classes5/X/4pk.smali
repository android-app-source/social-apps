.class public LX/4pk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0lZ;
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = -0x7cf57e08715650aL


# instance fields
.field public _rootValueSeparator:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 811834
    const-string v0, " "

    invoke-direct {p0, v0}, LX/4pk;-><init>(Ljava/lang/String;)V

    .line 811835
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 811830
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 811831
    const-string v0, " "

    iput-object v0, p0, LX/4pk;->_rootValueSeparator:Ljava/lang/String;

    .line 811832
    iput-object p1, p0, LX/4pk;->_rootValueSeparator:Ljava/lang/String;

    .line 811833
    return-void
.end method


# virtual methods
.method public final a(LX/0nX;)V
    .locals 1

    .prologue
    .line 811827
    iget-object v0, p0, LX/4pk;->_rootValueSeparator:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 811828
    iget-object v0, p0, LX/4pk;->_rootValueSeparator:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/0nX;->c(Ljava/lang/String;)V

    .line 811829
    :cond_0
    return-void
.end method

.method public final a(LX/0nX;I)V
    .locals 1

    .prologue
    .line 811825
    const/16 v0, 0x7d

    invoke-virtual {p1, v0}, LX/0nX;->a(C)V

    .line 811826
    return-void
.end method

.method public final b(LX/0nX;)V
    .locals 1

    .prologue
    .line 811823
    const/16 v0, 0x7b

    invoke-virtual {p1, v0}, LX/0nX;->a(C)V

    .line 811824
    return-void
.end method

.method public final b(LX/0nX;I)V
    .locals 1

    .prologue
    .line 811821
    const/16 v0, 0x5d

    invoke-virtual {p1, v0}, LX/0nX;->a(C)V

    .line 811822
    return-void
.end method

.method public final c(LX/0nX;)V
    .locals 1

    .prologue
    .line 811811
    const/16 v0, 0x2c

    invoke-virtual {p1, v0}, LX/0nX;->a(C)V

    .line 811812
    return-void
.end method

.method public final d(LX/0nX;)V
    .locals 1

    .prologue
    .line 811819
    const/16 v0, 0x3a

    invoke-virtual {p1, v0}, LX/0nX;->a(C)V

    .line 811820
    return-void
.end method

.method public final e(LX/0nX;)V
    .locals 1

    .prologue
    .line 811817
    const/16 v0, 0x5b

    invoke-virtual {p1, v0}, LX/0nX;->a(C)V

    .line 811818
    return-void
.end method

.method public final f(LX/0nX;)V
    .locals 1

    .prologue
    .line 811815
    const/16 v0, 0x2c

    invoke-virtual {p1, v0}, LX/0nX;->a(C)V

    .line 811816
    return-void
.end method

.method public final g(LX/0nX;)V
    .locals 0

    .prologue
    .line 811814
    return-void
.end method

.method public final h(LX/0nX;)V
    .locals 0

    .prologue
    .line 811813
    return-void
.end method
