.class public LX/3f7;
.super LX/0Tv;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/3f7;


# direct methods
.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 621408
    const-string v0, "admined_pages_part"

    const/4 v1, 0x1

    new-instance v2, LX/3f8;

    invoke-direct {v2}, LX/3f8;-><init>()V

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, LX/0Tv;-><init>(Ljava/lang/String;ILX/0Px;)V

    .line 621409
    return-void
.end method

.method public static a(LX/0QB;)LX/3f7;
    .locals 3

    .prologue
    .line 621410
    sget-object v0, LX/3f7;->a:LX/3f7;

    if-nez v0, :cond_1

    .line 621411
    const-class v1, LX/3f7;

    monitor-enter v1

    .line 621412
    :try_start_0
    sget-object v0, LX/3f7;->a:LX/3f7;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 621413
    if-eqz v2, :cond_0

    .line 621414
    :try_start_1
    new-instance v0, LX/3f7;

    invoke-direct {v0}, LX/3f7;-><init>()V

    .line 621415
    move-object v0, v0

    .line 621416
    sput-object v0, LX/3f7;->a:LX/3f7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 621417
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 621418
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 621419
    :cond_1
    sget-object v0, LX/3f7;->a:LX/3f7;

    return-object v0

    .line 621420
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 621421
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
