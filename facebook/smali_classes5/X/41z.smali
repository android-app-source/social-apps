.class public final LX/41z;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/facebook/auth/protocol/ConfirmedMessengerCredentials;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 666647
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 666646
    new-instance v0, Lcom/facebook/auth/protocol/ConfirmedMessengerCredentials;

    invoke-direct {v0, p1}, Lcom/facebook/auth/protocol/ConfirmedMessengerCredentials;-><init>(Landroid/os/Parcel;)V

    return-object v0
.end method

.method public final newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 666645
    new-array v0, p1, [Lcom/facebook/auth/protocol/ConfirmedMessengerCredentials;

    return-object v0
.end method
