.class public abstract LX/4dQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4dG;


# instance fields
.field private final a:LX/4dG;


# direct methods
.method public constructor <init>(LX/4dG;)V
    .locals 0

    .prologue
    .line 795825
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 795826
    iput-object p1, p0, LX/4dQ;->a:LX/4dG;

    .line 795827
    return-void
.end method


# virtual methods
.method public final a(I)LX/4dL;
    .locals 1

    .prologue
    .line 795824
    iget-object v0, p0, LX/4dQ;->a:LX/4dG;

    invoke-interface {v0, p1}, LX/4dG;->a(I)LX/4dL;

    move-result-object v0

    return-object v0
.end method

.method public final a()LX/4dO;
    .locals 1

    .prologue
    .line 795823
    iget-object v0, p0, LX/4dQ;->a:LX/4dG;

    invoke-interface {v0}, LX/4dG;->a()LX/4dO;

    move-result-object v0

    return-object v0
.end method

.method public a(ILandroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 795821
    iget-object v0, p0, LX/4dQ;->a:LX/4dG;

    invoke-interface {v0, p1, p2}, LX/4dG;->a(ILandroid/graphics/Canvas;)V

    .line 795822
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 795805
    iget-object v0, p0, LX/4dQ;->a:LX/4dG;

    invoke-interface {v0}, LX/4dG;->b()I

    move-result v0

    return v0
.end method

.method public final b(I)I
    .locals 1

    .prologue
    .line 795820
    iget-object v0, p0, LX/4dQ;->a:LX/4dG;

    invoke-interface {v0, p1}, LX/4dG;->b(I)I

    move-result v0

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 795819
    iget-object v0, p0, LX/4dQ;->a:LX/4dG;

    invoke-interface {v0}, LX/4dG;->c()I

    move-result v0

    return v0
.end method

.method public final c(I)I
    .locals 1

    .prologue
    .line 795818
    iget-object v0, p0, LX/4dQ;->a:LX/4dG;

    invoke-interface {v0, p1}, LX/4dG;->c(I)I

    move-result v0

    return v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 795817
    iget-object v0, p0, LX/4dQ;->a:LX/4dG;

    invoke-interface {v0}, LX/4dG;->d()I

    move-result v0

    return v0
.end method

.method public final d(I)I
    .locals 1

    .prologue
    .line 795816
    iget-object v0, p0, LX/4dQ;->a:LX/4dG;

    invoke-interface {v0, p1}, LX/4dG;->d(I)I

    move-result v0

    return v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 795815
    iget-object v0, p0, LX/4dQ;->a:LX/4dG;

    invoke-interface {v0}, LX/4dG;->e()I

    move-result v0

    return v0
.end method

.method public final e(I)LX/1FJ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 795814
    iget-object v0, p0, LX/4dQ;->a:LX/4dG;

    invoke-interface {v0, p1}, LX/4dG;->e(I)LX/1FJ;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 795813
    iget-object v0, p0, LX/4dQ;->a:LX/4dG;

    invoke-interface {v0}, LX/4dG;->f()I

    move-result v0

    return v0
.end method

.method public final f(I)Z
    .locals 1

    .prologue
    .line 795812
    iget-object v0, p0, LX/4dQ;->a:LX/4dG;

    invoke-interface {v0, p1}, LX/4dG;->f(I)Z

    move-result v0

    return v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 795811
    iget-object v0, p0, LX/4dQ;->a:LX/4dG;

    invoke-interface {v0}, LX/4dG;->g()I

    move-result v0

    return v0
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 795810
    iget-object v0, p0, LX/4dQ;->a:LX/4dG;

    invoke-interface {v0}, LX/4dG;->h()I

    move-result v0

    return v0
.end method

.method public final i()I
    .locals 1

    .prologue
    .line 795809
    iget-object v0, p0, LX/4dQ;->a:LX/4dG;

    invoke-interface {v0}, LX/4dG;->i()I

    move-result v0

    return v0
.end method

.method public j()I
    .locals 1

    .prologue
    .line 795808
    iget-object v0, p0, LX/4dQ;->a:LX/4dG;

    invoke-interface {v0}, LX/4dG;->j()I

    move-result v0

    return v0
.end method

.method public k()V
    .locals 1

    .prologue
    .line 795806
    iget-object v0, p0, LX/4dQ;->a:LX/4dG;

    invoke-interface {v0}, LX/4dG;->k()V

    .line 795807
    return-void
.end method
