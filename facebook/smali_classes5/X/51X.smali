.class public final LX/51X;
.super LX/0Rr;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Rr",
        "<",
        "Ljava/util/Map$Entry",
        "<",
        "LX/4xM",
        "<TC;>;",
        "LX/50M",
        "<TC;>;>;>;"
    }
.end annotation


# instance fields
.field public a:LX/4xM;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4xM",
            "<TC;>;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/4xM;

.field public final synthetic c:LX/4yu;

.field public final synthetic d:LX/51Y;


# direct methods
.method public constructor <init>(LX/51Y;LX/4xM;LX/4yu;)V
    .locals 1

    .prologue
    .line 825184
    iput-object p1, p0, LX/51X;->d:LX/51Y;

    iput-object p2, p0, LX/51X;->b:LX/4xM;

    iput-object p3, p0, LX/51X;->c:LX/4yu;

    invoke-direct {p0}, LX/0Rr;-><init>()V

    .line 825185
    iget-object v0, p0, LX/51X;->b:LX/4xM;

    iput-object v0, p0, LX/51X;->a:LX/4xM;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 825186
    iget-object v0, p0, LX/51X;->a:LX/4xM;

    sget-object v1, LX/4xP;->a:LX/4xP;

    if-ne v0, v1, :cond_0

    .line 825187
    invoke-virtual {p0}, LX/0Rr;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 825188
    :goto_0
    return-object v0

    .line 825189
    :cond_0
    iget-object v0, p0, LX/51X;->c:LX/4yu;

    invoke-interface {v0}, LX/4yu;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 825190
    iget-object v0, p0, LX/51X;->c:LX/4yu;

    invoke-interface {v0}, LX/4yu;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/50M;

    .line 825191
    iget-object v1, v0, LX/50M;->upperBound:LX/4xM;

    iget-object v2, p0, LX/51X;->a:LX/4xM;

    invoke-static {v1, v2}, LX/50M;->a(LX/4xM;LX/4xM;)LX/50M;

    move-result-object v1

    .line 825192
    iget-object v0, v0, LX/50M;->lowerBound:LX/4xM;

    iput-object v0, p0, LX/51X;->a:LX/4xM;

    .line 825193
    iget-object v0, p0, LX/51X;->d:LX/51Y;

    iget-object v0, v0, LX/51Y;->c:LX/50M;

    iget-object v0, v0, LX/50M;->lowerBound:LX/4xM;

    iget-object v2, v1, LX/50M;->lowerBound:LX/4xM;

    invoke-virtual {v0, v2}, LX/4xM;->a(Ljava/lang/Comparable;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 825194
    iget-object v0, v1, LX/50M;->lowerBound:LX/4xM;

    invoke-static {v0, v1}, LX/0PM;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    goto :goto_0

    .line 825195
    :cond_1
    iget-object v0, p0, LX/51X;->d:LX/51Y;

    iget-object v0, v0, LX/51Y;->c:LX/50M;

    iget-object v0, v0, LX/50M;->lowerBound:LX/4xM;

    sget-object v1, LX/4xP;->a:LX/4xP;

    invoke-virtual {v0, v1}, LX/4xM;->a(Ljava/lang/Comparable;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 825196
    sget-object v0, LX/4xP;->a:LX/4xP;

    iget-object v1, p0, LX/51X;->a:LX/4xM;

    invoke-static {v0, v1}, LX/50M;->a(LX/4xM;LX/4xM;)LX/50M;

    move-result-object v0

    .line 825197
    sget-object v1, LX/4xP;->a:LX/4xP;

    iput-object v1, p0, LX/51X;->a:LX/4xM;

    .line 825198
    sget-object v1, LX/4xP;->a:LX/4xP;

    invoke-static {v1, v0}, LX/0PM;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    goto :goto_0

    .line 825199
    :cond_2
    invoke-virtual {p0}, LX/0Rr;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    goto :goto_0
.end method
