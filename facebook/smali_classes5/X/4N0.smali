.class public LX/4N0;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 690041
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 689992
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 689993
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 689994
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 689995
    invoke-static {p0, p1}, LX/4N0;->b(LX/15w;LX/186;)I

    move-result v1

    .line 689996
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 689997
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const-wide/16 v4, 0x0

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 690017
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_8

    .line 690018
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 690019
    :goto_0
    return v1

    .line 690020
    :cond_0
    const-string v12, "friends_count"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 690021
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v7

    move v8, v7

    move v7, v6

    .line 690022
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_5

    .line 690023
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 690024
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 690025
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_1

    if-eqz v11, :cond_1

    .line 690026
    const-string v12, "approximate_location"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 690027
    invoke-static {p0, p1}, LX/2sz;->a(LX/15w;LX/186;)I

    move-result v10

    goto :goto_1

    .line 690028
    :cond_2
    const-string v12, "display_users"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 690029
    invoke-static {p0, p1}, LX/2bO;->b(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 690030
    :cond_3
    const-string v12, "radius_km"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 690031
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v2

    move v0, v6

    goto :goto_1

    .line 690032
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 690033
    :cond_5
    const/4 v11, 0x4

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 690034
    invoke-virtual {p1, v1, v10}, LX/186;->b(II)V

    .line 690035
    invoke-virtual {p1, v6, v9}, LX/186;->b(II)V

    .line 690036
    if-eqz v7, :cond_6

    .line 690037
    const/4 v6, 0x2

    invoke-virtual {p1, v6, v8, v1}, LX/186;->a(III)V

    .line 690038
    :cond_6
    if-eqz v0, :cond_7

    .line 690039
    const/4 v1, 0x3

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 690040
    :cond_7
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_8
    move v0, v1

    move v7, v1

    move-wide v2, v4

    move v8, v1

    move v9, v1

    move v10, v1

    goto :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const-wide/16 v2, 0x0

    .line 689998
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 689999
    invoke-virtual {p0, p1, v4}, LX/15i;->g(II)I

    move-result v0

    .line 690000
    if-eqz v0, :cond_0

    .line 690001
    const-string v1, "approximate_location"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 690002
    invoke-static {p0, v0, p2}, LX/2sz;->a(LX/15i;ILX/0nX;)V

    .line 690003
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 690004
    if-eqz v0, :cond_1

    .line 690005
    const-string v1, "display_users"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 690006
    invoke-static {p0, v0, p2, p3}, LX/2bO;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 690007
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v4}, LX/15i;->a(III)I

    move-result v0

    .line 690008
    if-eqz v0, :cond_2

    .line 690009
    const-string v1, "friends_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 690010
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 690011
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 690012
    cmpl-double v2, v0, v2

    if-eqz v2, :cond_3

    .line 690013
    const-string v2, "radius_km"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 690014
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 690015
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 690016
    return-void
.end method
