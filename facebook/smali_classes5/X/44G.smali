.class public LX/44G;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0Tn;


# instance fields
.field public final b:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 669558
    sget-object v0, LX/0Tm;->c:LX/0Tn;

    const-string v1, "fb_log_net_access"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/44G;->a:LX/0Tn;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 669568
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 669569
    iput-object p1, p0, LX/44G;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 669570
    return-void
.end method

.method public static a(LX/0QB;)LX/44G;
    .locals 1

    .prologue
    .line 669571
    invoke-static {p0}, LX/44G;->b(LX/0QB;)LX/44G;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/44G;
    .locals 2

    .prologue
    .line 669566
    new-instance v1, LX/44G;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {v1, v0}, LX/44G;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 669567
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 669559
    const/4 v0, 0x0

    .line 669560
    iget-object v1, p0, LX/44G;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 669561
    iget-object v1, p0, LX/44G;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/44G;->a:LX/0Tn;

    invoke-interface {v1, v2, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    .line 669562
    :cond_0
    move v0, v0

    .line 669563
    if-eqz v0, :cond_1

    .line 669564
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    .line 669565
    :cond_1
    return-void
.end method
