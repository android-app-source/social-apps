.class public final LX/4yi;
.super LX/0cA;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "LX/0cA",
        "<TE;>;"
    }
.end annotation


# instance fields
.field private final c:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<-TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Comparator;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Comparator",
            "<-TE;>;)V"
        }
    .end annotation

    .prologue
    .line 821997
    invoke-direct {p0}, LX/0cA;-><init>()V

    .line 821998
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Comparator;

    iput-object v0, p0, LX/4yi;->c:Ljava/util/Comparator;

    .line 821999
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Iterable;)LX/0P7;
    .locals 1

    .prologue
    .line 821994
    invoke-super {p0, p1}, LX/0cA;->b(Ljava/lang/Iterable;)LX/0cA;

    .line 821995
    move-object v0, p0

    .line 821996
    return-object v0
.end method

.method public final synthetic a(Ljava/util/Iterator;)LX/0P7;
    .locals 1

    .prologue
    .line 821991
    invoke-super {p0, p1}, LX/0cA;->b(Ljava/util/Iterator;)LX/0cA;

    .line 821992
    move-object v0, p0

    .line 821993
    return-object v0
.end method

.method public final synthetic a([Ljava/lang/Object;)LX/0P7;
    .locals 1

    .prologue
    .line 821988
    invoke-super {p0, p1}, LX/0cA;->b([Ljava/lang/Object;)LX/0cA;

    .line 821989
    move-object v0, p0

    .line 821990
    return-object v0
.end method

.method public final synthetic a()LX/0Py;
    .locals 1

    .prologue
    .line 821987
    invoke-virtual {p0}, LX/4yi;->c()LX/0dW;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Ljava/lang/Object;)LX/0Q0;
    .locals 1

    .prologue
    .line 821984
    invoke-super {p0, p1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 821985
    move-object v0, p0

    .line 821986
    return-object v0
.end method

.method public final synthetic b(Ljava/lang/Object;)LX/0P7;
    .locals 1

    .prologue
    .line 822000
    invoke-super {p0, p1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 822001
    move-object v0, p0

    .line 822002
    return-object v0
.end method

.method public final synthetic b()LX/0Rf;
    .locals 1

    .prologue
    .line 821983
    invoke-virtual {p0}, LX/4yi;->c()LX/0dW;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Ljava/lang/Iterable;)LX/0cA;
    .locals 1

    .prologue
    .line 821980
    invoke-super {p0, p1}, LX/0cA;->b(Ljava/lang/Iterable;)LX/0cA;

    .line 821981
    move-object v0, p0

    .line 821982
    return-object v0
.end method

.method public final synthetic b(Ljava/util/Iterator;)LX/0cA;
    .locals 1

    .prologue
    .line 821977
    invoke-super {p0, p1}, LX/0cA;->b(Ljava/util/Iterator;)LX/0cA;

    .line 821978
    move-object v0, p0

    .line 821979
    return-object v0
.end method

.method public final synthetic b([Ljava/lang/Object;)LX/0cA;
    .locals 1

    .prologue
    .line 821974
    invoke-super {p0, p1}, LX/0cA;->b([Ljava/lang/Object;)LX/0cA;

    .line 821975
    move-object v0, p0

    .line 821976
    return-object v0
.end method

.method public final synthetic c(Ljava/lang/Object;)LX/0cA;
    .locals 1

    .prologue
    .line 821971
    invoke-super {p0, p1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 821972
    move-object v0, p0

    .line 821973
    return-object v0
.end method

.method public final c()LX/0dW;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0dW",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 821967
    iget-object v0, p0, LX/0Q0;->a:[Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    .line 821968
    iget-object v1, p0, LX/4yi;->c:Ljava/util/Comparator;

    iget v2, p0, LX/0Q0;->b:I

    invoke-static {v1, v2, v0}, LX/0dW;->a(Ljava/util/Comparator;I[Ljava/lang/Object;)LX/0dW;

    move-result-object v0

    .line 821969
    invoke-virtual {v0}, LX/0dW;->size()I

    move-result v1

    iput v1, p0, LX/4yi;->b:I

    .line 821970
    return-object v0
.end method

.method public final varargs c([Ljava/lang/Object;)LX/4yi;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TE;)",
            "LX/4yi",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 821965
    invoke-super {p0, p1}, LX/0cA;->b([Ljava/lang/Object;)LX/0cA;

    .line 821966
    return-object p0
.end method

.method public final d(Ljava/lang/Object;)LX/4yi;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)",
            "LX/4yi",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 821963
    invoke-super {p0, p1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 821964
    return-object p0
.end method
