.class public LX/3dx;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final b:[Ljava/lang/String;

.field private static final c:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "LX/3do;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final d:Ljava/lang/String;

.field private static final e:Ljava/lang/String;

.field private static final f:Ljava/lang/String;

.field private static volatile i:LX/3dx;


# instance fields
.field private final g:LX/3e2;

.field private final h:LX/3eI;


# direct methods
.method public static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v7, 0x3

    const/4 v5, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 618605
    const-class v0, LX/3dx;

    sput-object v0, LX/3dx;->a:Ljava/lang/Class;

    .line 618606
    const/16 v0, 0x17

    new-array v0, v0, [Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "sticker_packs."

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, LX/3dy;->a:LX/0U1;

    .line 618607
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 618608
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v8

    sget-object v1, LX/3dy;->b:LX/0U1;

    .line 618609
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 618610
    aput-object v1, v0, v9

    sget-object v1, LX/3dy;->c:LX/0U1;

    .line 618611
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 618612
    aput-object v1, v0, v5

    sget-object v1, LX/3dy;->d:LX/0U1;

    .line 618613
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 618614
    aput-object v1, v0, v7

    const/4 v1, 0x4

    sget-object v2, LX/3dy;->e:LX/0U1;

    .line 618615
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 618616
    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, LX/3dy;->f:LX/0U1;

    .line 618617
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 618618
    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/3dy;->g:LX/0U1;

    .line 618619
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 618620
    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/3dy;->h:LX/0U1;

    .line 618621
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 618622
    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/3dy;->i:LX/0U1;

    .line 618623
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 618624
    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/3dy;->j:LX/0U1;

    .line 618625
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 618626
    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/3dy;->k:LX/0U1;

    .line 618627
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 618628
    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/3dy;->l:LX/0U1;

    .line 618629
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 618630
    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/3dy;->m:LX/0U1;

    .line 618631
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 618632
    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/3dy;->n:LX/0U1;

    .line 618633
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 618634
    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/3dy;->o:LX/0U1;

    .line 618635
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 618636
    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/3dy;->p:LX/0U1;

    .line 618637
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 618638
    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/3dy;->q:LX/0U1;

    .line 618639
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 618640
    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/3dy;->r:LX/0U1;

    .line 618641
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 618642
    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/3dy;->s:LX/0U1;

    .line 618643
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 618644
    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/3dy;->t:LX/0U1;

    .line 618645
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 618646
    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/3dy;->u:LX/0U1;

    .line 618647
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 618648
    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/3dy;->v:LX/0U1;

    .line 618649
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 618650
    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/3dy;->w:LX/0U1;

    .line 618651
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 618652
    aput-object v2, v0, v1

    sput-object v0, LX/3dx;->b:[Ljava/lang/String;

    .line 618653
    sget-object v0, LX/3do;->OWNED_PACKS:LX/3do;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, LX/3do;->STORE_PACKS:LX/3do;

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    sget-object v4, LX/3do;->DOWNLOADED_PACKS:LX/3do;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    sget-object v6, LX/3do;->AUTODOWNLOADED_PACKS:LX/3do;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-static/range {v0 .. v7}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    sput-object v0, LX/3dx;->c:LX/0P1;

    .line 618654
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SELECT "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, LX/3dz;->b:LX/0U1;

    .line 618655
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 618656
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " FROM stickers"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " WHERE "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/3dz;->a:LX/0U1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = ?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/3dx;->d:Ljava/lang/String;

    .line 618657
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "INSERT OR REPLACE INTO sticker_asserts ("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, LX/3e0;->a:LX/0U1;

    .line 618658
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 618659
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/3e0;->b:LX/0U1;

    .line 618660
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 618661
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/3e0;->c:LX/0U1;

    .line 618662
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 618663
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/3e0;->g:LX/0U1;

    .line 618664
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 618665
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/3e0;->h:LX/0U1;

    .line 618666
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 618667
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") VALUES (?, ?, (%s), ?, ?)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v9, [Ljava/lang/Object;

    sget-object v2, LX/3dx;->d:Ljava/lang/String;

    aput-object v2, v1, v8

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/3dx;->e:Ljava/lang/String;

    .line 618668
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SELECT s."

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, LX/3dz;->a:LX/0U1;

    .line 618669
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 618670
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " as sticker_id, s."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/3dz;->b:LX/0U1;

    .line 618671
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 618672
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " as sticker_pack_id, s."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/3dz;->c:LX/0U1;

    .line 618673
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 618674
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " as static_uri, s."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/3dz;->d:LX/0U1;

    .line 618675
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 618676
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " as animated_uri, s."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/3dz;->e:LX/0U1;

    .line 618677
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 618678
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " as preview_uri, s."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/3dz;->f:LX/0U1;

    .line 618679
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 618680
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " as is_comments_capable, s."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/3dz;->g:LX/0U1;

    .line 618681
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 618682
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " as is_composer_capable, s."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/3dz;->h:LX/0U1;

    .line 618683
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 618684
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " as is_messenger_capable, s."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/3dz;->i:LX/0U1;

    .line 618685
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 618686
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " as is_sms_capable, s."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/3dz;->j:LX/0U1;

    .line 618687
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 618688
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " as is_posts_capable, s."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/3dz;->k:LX/0U1;

    .line 618689
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 618690
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " as is_montage_capable, static_assets."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/3e0;->g:LX/0U1;

    .line 618691
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 618692
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " as static_asset, animated_assets."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/3e0;->g:LX/0U1;

    .line 618693
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 618694
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " as animated_asset, preview_assets."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/3e0;->g:LX/0U1;

    .line 618695
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 618696
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " as preview_asset FROM "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "stickers AS s "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "LEFT OUTER JOIN sticker_asserts"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " as static_assets ON ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "static_assets."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/3e0;->b:LX/0U1;

    .line 618697
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 618698
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/3e1;->STATIC:LX/3e1;

    invoke-virtual {v1}, LX/3e1;->getDbName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\' AND static_assets."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/3e0;->a:LX/0U1;

    .line 618699
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 618700
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = s."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/3dz;->a:LX/0U1;

    .line 618701
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 618702
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") LEFT OUTER JOIN "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "sticker_asserts as animated_assets "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "ON (animated_assets."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/3e0;->b:LX/0U1;

    .line 618703
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 618704
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/3e1;->ANIMATED:LX/3e1;

    invoke-virtual {v1}, LX/3e1;->getDbName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\' AND animated_assets."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/3e0;->a:LX/0U1;

    .line 618705
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 618706
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = s."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/3dz;->a:LX/0U1;

    .line 618707
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 618708
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") LEFT OUTER JOIN "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "sticker_asserts as preview_assets "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "ON (preview_assets."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/3e0;->b:LX/0U1;

    .line 618709
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 618710
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/3e1;->PREVIEW:LX/3e1;

    invoke-virtual {v1}, LX/3e1;->getDbName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\' AND preview_assets."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/3e0;->a:LX/0U1;

    .line 618711
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 618712
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = s."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/3dz;->a:LX/0U1;

    .line 618713
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 618714
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/3dx;->f:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/3e2;LX/3eI;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 618715
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 618716
    iput-object p1, p0, LX/3dx;->g:LX/3e2;

    .line 618717
    iput-object p2, p0, LX/3dx;->h:LX/3eI;

    .line 618718
    return-void
.end method

.method public static a(LX/0QB;)LX/3dx;
    .locals 5

    .prologue
    .line 618719
    sget-object v0, LX/3dx;->i:LX/3dx;

    if-nez v0, :cond_1

    .line 618720
    const-class v1, LX/3dx;

    monitor-enter v1

    .line 618721
    :try_start_0
    sget-object v0, LX/3dx;->i:LX/3dx;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 618722
    if-eqz v2, :cond_0

    .line 618723
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 618724
    new-instance p0, LX/3dx;

    invoke-static {v0}, LX/3e2;->a(LX/0QB;)LX/3e2;

    move-result-object v3

    check-cast v3, LX/3e2;

    invoke-static {v0}, LX/3eI;->a(LX/0QB;)LX/3eI;

    move-result-object v4

    check-cast v4, LX/3eI;

    invoke-direct {p0, v3, v4}, LX/3dx;-><init>(LX/3e2;LX/3eI;)V

    .line 618725
    move-object v0, p0

    .line 618726
    sput-object v0, LX/3dx;->i:LX/3dx;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 618727
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 618728
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 618729
    :cond_1
    sget-object v0, LX/3dx;->i:LX/3dx;

    return-object v0

    .line 618730
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 618731
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Landroid/database/Cursor;)Lcom/facebook/stickers/model/StickerPack;
    .locals 26

    .prologue
    .line 618451
    sget-object v2, LX/3dy;->a:LX/0U1;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v2

    .line 618452
    sget-object v3, LX/3dy;->b:LX/0U1;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v3

    .line 618453
    sget-object v4, LX/3dy;->c:LX/0U1;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v4

    .line 618454
    sget-object v5, LX/3dy;->d:LX/0U1;

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v5

    .line 618455
    sget-object v6, LX/3dy;->e:LX/0U1;

    move-object/from16 v0, p1

    invoke-virtual {v6, v0}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v6

    .line 618456
    sget-object v7, LX/3dy;->f:LX/0U1;

    move-object/from16 v0, p1

    invoke-virtual {v7, v0}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v7

    .line 618457
    sget-object v8, LX/3dy;->g:LX/0U1;

    move-object/from16 v0, p1

    invoke-virtual {v8, v0}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v8

    .line 618458
    sget-object v9, LX/3dy;->h:LX/0U1;

    move-object/from16 v0, p1

    invoke-virtual {v9, v0}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v9

    .line 618459
    sget-object v10, LX/3dy;->i:LX/0U1;

    move-object/from16 v0, p1

    invoke-virtual {v10, v0}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v10

    .line 618460
    sget-object v11, LX/3dy;->j:LX/0U1;

    move-object/from16 v0, p1

    invoke-virtual {v11, v0}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v11

    .line 618461
    sget-object v12, LX/3dy;->k:LX/0U1;

    move-object/from16 v0, p1

    invoke-virtual {v12, v0}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v12

    .line 618462
    sget-object v13, LX/3dy;->l:LX/0U1;

    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v13

    .line 618463
    sget-object v14, LX/3dy;->p:LX/0U1;

    move-object/from16 v0, p1

    invoke-virtual {v14, v0}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v14

    .line 618464
    sget-object v15, LX/3dy;->q:LX/0U1;

    move-object/from16 v0, p1

    invoke-virtual {v15, v0}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v15

    .line 618465
    sget-object v16, LX/3dy;->r:LX/0U1;

    move-object/from16 v0, v16

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v16

    .line 618466
    sget-object v17, LX/3dy;->s:LX/0U1;

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v17

    .line 618467
    sget-object v18, LX/3dy;->t:LX/0U1;

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v18

    .line 618468
    sget-object v19, LX/3dy;->m:LX/0U1;

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v19

    .line 618469
    sget-object v20, LX/3dy;->n:LX/0U1;

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v20

    .line 618470
    sget-object v21, LX/3dy;->o:LX/0U1;

    move-object/from16 v0, v21

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v21

    .line 618471
    sget-object v22, LX/3dy;->u:LX/0U1;

    move-object/from16 v0, v22

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v22

    .line 618472
    sget-object v23, LX/3dy;->v:LX/0U1;

    move-object/from16 v0, v23

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v23

    .line 618473
    sget-object v24, LX/3dy;->w:LX/0U1;

    move-object/from16 v0, v24

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v24

    .line 618474
    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 618475
    move-object/from16 v0, p0

    iget-object v0, v0, LX/3dx;->h:LX/3eI;

    move-object/from16 v25, v0

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v25

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, LX/3eI;->a(Ljava/lang/String;)LX/0Px;

    move-result-object v17

    .line 618476
    move-object/from16 v0, p0

    iget-object v0, v0, LX/3dx;->h:LX/3eI;

    move-object/from16 v25, v0

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v25

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, LX/3eI;->c(Ljava/lang/String;)LX/0Px;

    move-result-object v18

    .line 618477
    move-object/from16 v0, p1

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 618478
    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v19

    invoke-static/range {v19 .. v19}, LX/03R;->fromDbValue(I)LX/03R;

    move-result-object v19

    .line 618479
    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v20

    invoke-static/range {v20 .. v20}, LX/03R;->fromDbValue(I)LX/03R;

    move-result-object v20

    .line 618480
    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v21

    invoke-static/range {v21 .. v21}, LX/03R;->fromDbValue(I)LX/03R;

    move-result-object v21

    .line 618481
    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v22

    invoke-static/range {v22 .. v22}, LX/03R;->fromDbValue(I)LX/03R;

    move-result-object v22

    .line 618482
    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v23

    invoke-static/range {v23 .. v23}, LX/03R;->fromDbValue(I)LX/03R;

    move-result-object v23

    .line 618483
    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v24

    invoke-static/range {v24 .. v24}, LX/03R;->fromDbValue(I)LX/03R;

    move-result-object v24

    .line 618484
    invoke-static {}, Lcom/facebook/stickers/model/StickerCapabilities;->newBuilder()LX/4m3;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, LX/4m3;->a(LX/03R;)LX/4m3;

    move-result-object v19

    invoke-virtual/range {v19 .. v20}, LX/4m3;->b(LX/03R;)LX/4m3;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, LX/4m3;->c(LX/03R;)LX/4m3;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, LX/4m3;->d(LX/03R;)LX/4m3;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, LX/4m3;->e(LX/03R;)LX/4m3;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, LX/4m3;->f(LX/03R;)LX/4m3;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, LX/4m3;->a()Lcom/facebook/stickers/model/StickerCapabilities;

    move-result-object v19

    .line 618485
    new-instance v20, LX/4m6;

    invoke-direct/range {v20 .. v20}, LX/4m6;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, LX/4m6;->a(Ljava/lang/String;)LX/4m6;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/4m6;->b(Ljava/lang/String;)LX/4m6;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/4m6;->c(Ljava/lang/String;)LX/4m6;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/4m6;->d(Ljava/lang/String;)LX/4m6;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/4m6;->a(Landroid/net/Uri;)LX/4m6;

    move-result-object v3

    if-nez v7, :cond_0

    const/4 v2, 0x0

    :goto_0
    invoke-virtual {v3, v2}, LX/4m6;->b(Landroid/net/Uri;)LX/4m6;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/4m6;->c(Landroid/net/Uri;)LX/4m6;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/4m6;->d(Landroid/net/Uri;)LX/4m6;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v2, v3}, LX/4m6;->a(I)LX/4m6;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    int-to-long v4, v3

    invoke-virtual {v2, v4, v5}, LX/4m6;->a(J)LX/4m6;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    const/4 v4, 0x1

    if-ne v2, v4, :cond_1

    const/4 v2, 0x1

    :goto_1
    invoke-virtual {v3, v2}, LX/4m6;->a(Z)LX/4m6;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    const/4 v4, 0x1

    if-ne v2, v4, :cond_2

    const/4 v2, 0x1

    :goto_2
    invoke-virtual {v3, v2}, LX/4m6;->b(Z)LX/4m6;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    const/4 v4, 0x1

    if-ne v2, v4, :cond_3

    const/4 v2, 0x1

    :goto_3
    invoke-virtual {v3, v2}, LX/4m6;->c(Z)LX/4m6;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    const/4 v4, 0x1

    if-ne v2, v4, :cond_4

    const/4 v2, 0x1

    :goto_4
    invoke-virtual {v3, v2}, LX/4m6;->d(Z)LX/4m6;

    move-result-object v3

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    const/4 v4, 0x1

    if-ne v2, v4, :cond_5

    const/4 v2, 0x1

    :goto_5
    invoke-virtual {v3, v2}, LX/4m6;->e(Z)LX/4m6;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, LX/4m6;->a(Ljava/util/List;)LX/4m6;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, LX/4m6;->b(Ljava/util/List;)LX/4m6;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, LX/4m6;->a(Lcom/facebook/stickers/model/StickerCapabilities;)LX/4m6;

    move-result-object v2

    invoke-virtual {v2}, LX/4m6;->s()Lcom/facebook/stickers/model/StickerPack;

    move-result-object v2

    return-object v2

    :cond_0
    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    goto/16 :goto_0

    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    goto :goto_3

    :cond_4
    const/4 v2, 0x0

    goto :goto_4

    :cond_5
    const/4 v2, 0x0

    goto :goto_5
.end method

.method private static a(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 618732
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(LX/3do;Lcom/facebook/stickers/model/StickerPack;J)V
    .locals 5

    .prologue
    .line 618733
    invoke-static {p1}, LX/3dx;->e(LX/3do;)I

    move-result v0

    .line 618734
    iget-object v1, p0, LX/3dx;->g:LX/3e2;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 618735
    const v2, -0x24ed7350

    invoke-static {v1, v2}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 618736
    :try_start_0
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 618737
    sget-object v3, LX/3e8;->a:LX/0U1;

    .line 618738
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 618739
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 618740
    sget-object v0, LX/3e8;->b:LX/0U1;

    .line 618741
    iget-object v3, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v3

    .line 618742
    iget-object v3, p2, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v3, v3

    .line 618743
    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 618744
    sget-object v0, LX/3e8;->c:LX/0U1;

    .line 618745
    iget-object v3, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v3

    .line 618746
    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 618747
    const-string v0, "pack_types"

    const/4 v3, 0x0

    const v4, 0x1a183eab

    invoke-static {v4}, LX/03h;->a(I)V

    invoke-virtual {v1, v0, v3, v2}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v0, -0x3a56d135

    invoke-static {v0}, LX/03h;->a(I)V

    .line 618748
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 618749
    const v0, -0x1dfbc431

    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 618750
    return-void

    .line 618751
    :catchall_0
    move-exception v0

    const v2, 0x33d23780

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method

.method private a(Lcom/facebook/stickers/model/Sticker;)V
    .locals 5

    .prologue
    .line 618752
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 618753
    sget-object v1, LX/3dz;->a:LX/0U1;

    .line 618754
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 618755
    iget-object v2, p1, Lcom/facebook/stickers/model/Sticker;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 618756
    sget-object v1, LX/3dz;->b:LX/0U1;

    .line 618757
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 618758
    iget-object v2, p1, Lcom/facebook/stickers/model/Sticker;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 618759
    sget-object v1, LX/3dz;->c:LX/0U1;

    .line 618760
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 618761
    iget-object v2, p1, Lcom/facebook/stickers/model/Sticker;->c:Landroid/net/Uri;

    invoke-static {v2}, LX/3dx;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 618762
    sget-object v1, LX/3dz;->d:LX/0U1;

    .line 618763
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 618764
    iget-object v2, p1, Lcom/facebook/stickers/model/Sticker;->e:Landroid/net/Uri;

    invoke-static {v2}, LX/3dx;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 618765
    sget-object v1, LX/3dz;->e:LX/0U1;

    .line 618766
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 618767
    iget-object v2, p1, Lcom/facebook/stickers/model/Sticker;->g:Landroid/net/Uri;

    invoke-static {v2}, LX/3dx;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 618768
    sget-object v1, LX/3dz;->f:LX/0U1;

    .line 618769
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 618770
    iget-object v2, p1, Lcom/facebook/stickers/model/Sticker;->i:Lcom/facebook/stickers/model/StickerCapabilities;

    iget-object v2, v2, Lcom/facebook/stickers/model/StickerCapabilities;->a:LX/03R;

    invoke-virtual {v2}, LX/03R;->getDbValue()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 618771
    sget-object v1, LX/3dz;->g:LX/0U1;

    .line 618772
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 618773
    iget-object v2, p1, Lcom/facebook/stickers/model/Sticker;->i:Lcom/facebook/stickers/model/StickerCapabilities;

    iget-object v2, v2, Lcom/facebook/stickers/model/StickerCapabilities;->b:LX/03R;

    invoke-virtual {v2}, LX/03R;->getDbValue()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 618774
    sget-object v1, LX/3dz;->h:LX/0U1;

    .line 618775
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 618776
    iget-object v2, p1, Lcom/facebook/stickers/model/Sticker;->i:Lcom/facebook/stickers/model/StickerCapabilities;

    iget-object v2, v2, Lcom/facebook/stickers/model/StickerCapabilities;->c:LX/03R;

    invoke-virtual {v2}, LX/03R;->getDbValue()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 618777
    sget-object v1, LX/3dz;->i:LX/0U1;

    .line 618778
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 618779
    iget-object v2, p1, Lcom/facebook/stickers/model/Sticker;->i:Lcom/facebook/stickers/model/StickerCapabilities;

    iget-object v2, v2, Lcom/facebook/stickers/model/StickerCapabilities;->d:LX/03R;

    invoke-virtual {v2}, LX/03R;->getDbValue()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 618780
    sget-object v1, LX/3dz;->j:LX/0U1;

    .line 618781
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 618782
    iget-object v2, p1, Lcom/facebook/stickers/model/Sticker;->i:Lcom/facebook/stickers/model/StickerCapabilities;

    iget-object v2, v2, Lcom/facebook/stickers/model/StickerCapabilities;->e:LX/03R;

    invoke-virtual {v2}, LX/03R;->getDbValue()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 618783
    sget-object v1, LX/3dz;->k:LX/0U1;

    .line 618784
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 618785
    iget-object v2, p1, Lcom/facebook/stickers/model/Sticker;->i:Lcom/facebook/stickers/model/StickerCapabilities;

    iget-object v2, v2, Lcom/facebook/stickers/model/StickerCapabilities;->f:LX/03R;

    invoke-virtual {v2}, LX/03R;->getDbValue()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 618786
    iget-object v1, p0, LX/3dx;->g:LX/3e2;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 618787
    const v2, -0x50870f92

    invoke-static {v1, v2}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 618788
    :try_start_0
    const-string v2, "stickers"

    const/4 v3, 0x0

    const v4, 0x56ee0383

    invoke-static {v4}, LX/03h;->a(I)V

    invoke-virtual {v1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->replaceOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v0, -0x48032f25

    invoke-static {v0}, LX/03h;->a(I)V

    .line 618789
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 618790
    const v0, 0x73a7933d

    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 618791
    return-void

    .line 618792
    :catchall_0
    move-exception v0

    const v2, -0x6f562f8d

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method

.method public static a(LX/3dx;Ljava/lang/String;LX/3do;)Z
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 618793
    invoke-static {}, LX/0uu;->a()LX/0uw;

    move-result-object v4

    .line 618794
    sget-object v0, LX/3e8;->a:LX/0U1;

    .line 618795
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 618796
    invoke-static {p2}, LX/3dx;->e(LX/3do;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 618797
    sget-object v0, LX/3e8;->b:LX/0U1;

    .line 618798
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 618799
    invoke-static {v0, p1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 618800
    iget-object v0, p0, LX/3dx;->g:LX/3e2;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 618801
    const v1, 0xd48d2aa

    invoke-static {v0, v1}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 618802
    const-string v1, "pack_types"

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 618803
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v1, 0x1

    .line 618804
    :goto_0
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 618805
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 618806
    const v2, 0x5fd9f00

    invoke-static {v0, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    return v1

    .line 618807
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 618808
    :catchall_0
    move-exception v1

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 618809
    const v2, -0x1f6a8e1

    invoke-static {v0, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v1
.end method

.method private b(Ljava/lang/String;LX/3e1;)Ljava/io/File;
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 618810
    invoke-static {}, LX/0uu;->a()LX/0uw;

    move-result-object v4

    .line 618811
    sget-object v0, LX/3e0;->a:LX/0U1;

    .line 618812
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 618813
    invoke-static {v0, p1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 618814
    sget-object v0, LX/3e0;->b:LX/0U1;

    .line 618815
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 618816
    invoke-virtual {p2}, LX/3e1;->getDbName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 618817
    iget-object v0, p0, LX/3dx;->g:LX/3e2;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 618818
    const v1, -0x7f2fd382

    invoke-static {v0, v1}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 618819
    const-string v1, "sticker_asserts"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    sget-object v3, LX/3e0;->g:LX/0U1;

    .line 618820
    iget-object v7, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v7

    .line 618821
    aput-object v3, v2, v6

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 618822
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_1

    .line 618823
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    .line 618824
    const/4 v1, 0x0

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 618825
    :goto_0
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 618826
    if-nez v1, :cond_0

    .line 618827
    :goto_1
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 618828
    const v1, -0x34f83f6

    invoke-static {v0, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    return-object v5

    .line 618829
    :cond_0
    :try_start_1
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 618830
    :catch_0
    move-exception v1

    .line 618831
    :try_start_2
    sget-object v3, LX/3dx;->a:Ljava/lang/Class;

    const-string v4, "error deleting old asset file."

    invoke-static {v3, v4, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 618832
    invoke-static {v1}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v1

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 618833
    :catchall_0
    move-exception v1

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 618834
    const v2, -0x39ae33e9

    invoke-static {v0, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v1

    :cond_1
    move-object v1, v5

    goto :goto_0
.end method

.method private static c(LX/3dx;Lcom/facebook/stickers/model/StickerPack;)I
    .locals 5

    .prologue
    .line 618934
    sget-object v0, LX/3dy;->a:LX/0U1;

    .line 618935
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 618936
    iget-object v1, p1, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v1, v1

    .line 618937
    invoke-static {v0, v1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v0

    .line 618938
    iget-object v1, p0, LX/3dx;->g:LX/3e2;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 618939
    const v2, 0x557d8314

    invoke-static {v1, v2}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 618940
    :try_start_0
    const-string v2, "sticker_packs"

    invoke-static {p0, p1}, LX/3dx;->e(LX/3dx;Lcom/facebook/stickers/model/StickerPack;)Landroid/content/ContentValues;

    move-result-object v3

    invoke-virtual {v0}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v3, v4, v0}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 618941
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 618942
    const v2, -0x6e6b7331

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 618943
    return v0

    .line 618944
    :catchall_0
    move-exception v0

    const v2, -0x476ebe3d

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method

.method private c(LX/3do;)V
    .locals 4

    .prologue
    .line 618835
    invoke-static {p0, p1}, LX/3dx;->d(LX/3dx;LX/3do;)Ljava/util/List;

    move-result-object v0

    .line 618836
    invoke-static {p0, v0}, LX/3dx;->c(LX/3dx;Ljava/util/List;)V

    .line 618837
    invoke-static {p1}, LX/3dx;->e(LX/3do;)I

    move-result v0

    .line 618838
    sget-object v1, LX/3e8;->a:LX/0U1;

    .line 618839
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 618840
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v0

    .line 618841
    iget-object v1, p0, LX/3dx;->g:LX/3e2;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 618842
    const v2, -0x6767ce55

    invoke-static {v1, v2}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 618843
    :try_start_0
    const-string v2, "pack_types"

    invoke-virtual {v0}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 618844
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 618845
    const v0, -0x7aa885d

    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 618846
    return-void

    .line 618847
    :catchall_0
    move-exception v0

    const v2, 0x3e23354d

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method

.method private c(LX/3do;Ljava/util/List;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3do;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/stickers/model/StickerPack;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-wide/16 v10, 0x1

    .line 618908
    iget-object v0, p0, LX/3dx;->g:LX/3e2;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 618909
    invoke-static {p1}, LX/3dx;->e(LX/3do;)I

    move-result v1

    .line 618910
    sget-object v2, LX/3e8;->a:LX/0U1;

    .line 618911
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 618912
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v4

    .line 618913
    const v1, -0x4d2ba866

    invoke-static {v0, v1}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 618914
    :try_start_0
    const-string v1, "pack_types"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    sget-object v5, LX/3e8;->c:LX/0U1;

    .line 618915
    iget-object v6, v5, LX/0U1;->d:Ljava/lang/String;

    move-object v5, v6

    .line 618916
    aput-object v5, v2, v3

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, LX/3e8;->c:LX/0U1;

    .line 618917
    iget-object v9, v8, LX/0U1;->d:Ljava/lang/String;

    move-object v8, v9

    .line 618918
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " DESC"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 618919
    const-wide/16 v2, 0x0

    .line 618920
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 618921
    sget-object v2, LX/3e8;->c:LX/0U1;

    invoke-virtual {v2, v1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    add-long/2addr v2, v10

    .line 618922
    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 618923
    invoke-static {p2}, LX/0R9;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 618924
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/stickers/model/StickerPack;

    .line 618925
    iget-object v5, v1, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v5, v5

    .line 618926
    invoke-static {p0, v5, p1}, LX/3dx;->a(LX/3dx;Ljava/lang/String;LX/3do;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 618927
    invoke-direct {p0, p1, v1, v2, v3}, LX/3dx;->a(LX/3do;Lcom/facebook/stickers/model/StickerPack;J)V

    .line 618928
    add-long/2addr v2, v10

    .line 618929
    :cond_1
    invoke-direct {p0, v1}, LX/3dx;->d(Lcom/facebook/stickers/model/StickerPack;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 618930
    :catchall_0
    move-exception v1

    const v2, -0x2b4f483c

    invoke-static {v0, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v1

    .line 618931
    :cond_2
    :try_start_1
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 618932
    const v1, 0x78889ce1

    invoke-static {v0, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 618933
    return-void
.end method

.method private static c(LX/3dx;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 618898
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 618899
    :goto_0
    return-void

    .line 618900
    :cond_0
    sget-object v0, LX/3dy;->a:LX/0U1;

    .line 618901
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 618902
    invoke-static {v0, p1}, LX/0uu;->a(Ljava/lang/String;Ljava/util/Collection;)LX/0ux;

    move-result-object v0

    .line 618903
    iget-object v1, p0, LX/3dx;->g:LX/3e2;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 618904
    const v2, 0x1da8483c

    invoke-static {v1, v2}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 618905
    :try_start_0
    const-string v2, "sticker_packs"

    invoke-virtual {v0}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 618906
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 618907
    const v0, 0x25b76598

    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    goto :goto_0

    :catchall_0
    move-exception v0

    const v2, -0x28764d9a

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method

.method private static d(Ljava/lang/String;)Landroid/net/Uri;
    .locals 1
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 618896
    :try_start_0
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 618897
    :goto_0
    return-object v0

    :catch_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static d(LX/3dx;LX/3do;)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3do;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 618872
    sget-object v0, LX/3dx;->c:LX/0P1;

    invoke-static {v0}, LX/0PM;->a(Ljava/util/Map;)Ljava/util/HashMap;

    move-result-object v0

    .line 618873
    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 618874
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 618875
    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 618876
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 618877
    :cond_0
    invoke-static {p1}, LX/3dx;->e(LX/3do;)I

    move-result v0

    .line 618878
    const/4 v3, 0x2

    new-array v3, v3, [LX/0ux;

    const/4 v4, 0x0

    sget-object v5, LX/3e8;->a:LX/0U1;

    .line 618879
    iget-object v6, v5, LX/0U1;->d:Ljava/lang/String;

    move-object v5, v6

    .line 618880
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    sget-object v4, LX/3e8;->a:LX/0U1;

    .line 618881
    iget-object v5, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v5

    .line 618882
    invoke-static {v4, v1}, LX/0uu;->a(Ljava/lang/String;Ljava/util/Collection;)LX/0ux;

    move-result-object v1

    invoke-static {v1}, LX/0uu;->a(LX/0ux;)LX/0ux;

    move-result-object v1

    aput-object v1, v3, v0

    invoke-static {v3}, LX/0uu;->a([LX/0ux;)LX/0uw;

    move-result-object v4

    .line 618883
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v8

    .line 618884
    iget-object v0, p0, LX/3dx;->g:LX/3e2;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 618885
    const v1, 0x573ea4b2

    invoke-static {v0, v1}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 618886
    const-string v1, "pack_types"

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 618887
    :try_start_0
    sget-object v1, LX/3dy;->a:LX/0U1;

    invoke-virtual {v1, v2}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v1

    .line 618888
    :goto_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 618889
    invoke-interface {v2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v8, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 618890
    :catchall_0
    move-exception v1

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 618891
    const v2, 0x29d77580

    invoke-static {v0, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v1

    .line 618892
    :cond_1
    :try_start_1
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 618893
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 618894
    const v1, 0x69bf5fc7

    invoke-static {v0, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 618895
    return-object v8
.end method

.method public static d(LX/3dx;Ljava/util/Collection;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 618859
    sget-object v0, LX/3e0;->a:LX/0U1;

    .line 618860
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 618861
    invoke-static {v0, p1}, LX/0uu;->a(Ljava/lang/String;Ljava/util/Collection;)LX/0ux;

    move-result-object v0

    .line 618862
    iget-object v1, p0, LX/3dx;->g:LX/3e2;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 618863
    const v2, 0x2531db7c

    invoke-static {v1, v2}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 618864
    :try_start_0
    const-string v2, "sticker_asserts"

    invoke-virtual {v0}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 618865
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 618866
    const v0, 0x54d9bfb9

    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 618867
    return-void

    .line 618868
    :catch_0
    move-exception v0

    .line 618869
    :try_start_1
    sget-object v2, LX/3dx;->a:Ljava/lang/Class;

    const-string v3, "error deleting old assets files."

    invoke-static {v2, v3, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 618870
    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 618871
    :catchall_0
    move-exception v0

    const v2, -0x6b1ddf3a

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method

.method private d(Lcom/facebook/stickers/model/StickerPack;)V
    .locals 5

    .prologue
    .line 618852
    iget-object v0, p0, LX/3dx;->g:LX/3e2;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 618853
    const v0, -0x235f0832

    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 618854
    :try_start_0
    const-string v0, "sticker_packs"

    const/4 v2, 0x0

    invoke-static {p0, p1}, LX/3dx;->e(LX/3dx;Lcom/facebook/stickers/model/StickerPack;)Landroid/content/ContentValues;

    move-result-object v3

    const v4, -0x29d2b630

    invoke-static {v4}, LX/03h;->a(I)V

    invoke-virtual {v1, v0, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->replaceOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v0, 0x46a5548a

    invoke-static {v0}, LX/03h;->a(I)V

    .line 618855
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 618856
    const v0, -0x57e54e7d

    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 618857
    return-void

    .line 618858
    :catchall_0
    move-exception v0

    const v2, -0x5f3b6008

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method

.method private static e(LX/3do;)I
    .locals 3

    .prologue
    .line 618848
    sget-object v0, LX/3dx;->c:LX/0P1;

    invoke-virtual {v0, p0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 618849
    if-nez v0, :cond_0

    .line 618850
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown sticker pack type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 618851
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method private static e(LX/3dx;Lcom/facebook/stickers/model/StickerPack;)Landroid/content/ContentValues;
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 618486
    iget-object v0, p1, Lcom/facebook/stickers/model/StickerPack;->q:LX/0Px;

    move-object v3, v0

    .line 618487
    if-nez v3, :cond_4

    .line 618488
    const/4 v0, 0x0

    .line 618489
    :goto_0
    move-object v3, v0

    .line 618490
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 618491
    sget-object v0, LX/3dy;->a:LX/0U1;

    .line 618492
    iget-object v5, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v5

    .line 618493
    iget-object v5, p1, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v5, v5

    .line 618494
    invoke-virtual {v4, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 618495
    sget-object v0, LX/3dy;->b:LX/0U1;

    .line 618496
    iget-object v5, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v5

    .line 618497
    iget-object v5, p1, Lcom/facebook/stickers/model/StickerPack;->b:Ljava/lang/String;

    move-object v5, v5

    .line 618498
    invoke-virtual {v4, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 618499
    sget-object v0, LX/3dy;->c:LX/0U1;

    .line 618500
    iget-object v5, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v5

    .line 618501
    iget-object v5, p1, Lcom/facebook/stickers/model/StickerPack;->c:Ljava/lang/String;

    move-object v5, v5

    .line 618502
    invoke-virtual {v4, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 618503
    sget-object v0, LX/3dy;->d:LX/0U1;

    .line 618504
    iget-object v5, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v5

    .line 618505
    iget-object v5, p1, Lcom/facebook/stickers/model/StickerPack;->d:Ljava/lang/String;

    move-object v5, v5

    .line 618506
    invoke-virtual {v4, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 618507
    sget-object v0, LX/3dy;->e:LX/0U1;

    .line 618508
    iget-object v5, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v5

    .line 618509
    iget-object v5, p1, Lcom/facebook/stickers/model/StickerPack;->e:Landroid/net/Uri;

    move-object v5, v5

    .line 618510
    invoke-static {v5}, LX/3dx;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 618511
    sget-object v0, LX/3dy;->f:LX/0U1;

    .line 618512
    iget-object v5, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v5

    .line 618513
    iget-object v5, p1, Lcom/facebook/stickers/model/StickerPack;->f:Landroid/net/Uri;

    move-object v5, v5

    .line 618514
    invoke-static {v5}, LX/3dx;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 618515
    sget-object v0, LX/3dy;->g:LX/0U1;

    .line 618516
    iget-object v5, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v5

    .line 618517
    iget-object v5, p1, Lcom/facebook/stickers/model/StickerPack;->g:Landroid/net/Uri;

    move-object v5, v5

    .line 618518
    invoke-static {v5}, LX/3dx;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 618519
    sget-object v0, LX/3dy;->h:LX/0U1;

    .line 618520
    iget-object v5, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v5

    .line 618521
    iget-object v5, p1, Lcom/facebook/stickers/model/StickerPack;->h:Landroid/net/Uri;

    move-object v5, v5

    .line 618522
    invoke-static {v5}, LX/3dx;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 618523
    sget-object v0, LX/3dy;->d:LX/0U1;

    .line 618524
    iget-object v5, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v5

    .line 618525
    iget-object v5, p1, Lcom/facebook/stickers/model/StickerPack;->d:Ljava/lang/String;

    move-object v5, v5

    .line 618526
    invoke-virtual {v4, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 618527
    sget-object v0, LX/3dy;->i:LX/0U1;

    .line 618528
    iget-object v5, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v5

    .line 618529
    iget v5, p1, Lcom/facebook/stickers/model/StickerPack;->i:I

    move v5, v5

    .line 618530
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 618531
    sget-object v0, LX/3dy;->j:LX/0U1;

    .line 618532
    iget-object v5, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v5

    .line 618533
    invoke-virtual {p1}, Lcom/facebook/stickers/model/StickerPack;->j()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 618534
    sget-object v0, LX/3dy;->k:LX/0U1;

    .line 618535
    iget-object v5, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v5

    .line 618536
    iget-boolean v5, p1, Lcom/facebook/stickers/model/StickerPack;->k:Z

    move v5, v5

    .line 618537
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 618538
    sget-object v0, LX/3dy;->l:LX/0U1;

    .line 618539
    iget-object v5, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v5, v5

    .line 618540
    iget-boolean v0, p1, Lcom/facebook/stickers/model/StickerPack;->l:Z

    move v0, v0

    .line 618541
    if-eqz v0, :cond_0

    move v0, v1

    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 618542
    sget-object v0, LX/3dy;->p:LX/0U1;

    .line 618543
    iget-object v5, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v5, v5

    .line 618544
    iget-boolean v0, p1, Lcom/facebook/stickers/model/StickerPack;->m:Z

    move v0, v0

    .line 618545
    if-eqz v0, :cond_1

    move v0, v1

    :goto_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 618546
    sget-object v0, LX/3dy;->q:LX/0U1;

    .line 618547
    iget-object v5, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v5, v5

    .line 618548
    iget-boolean v0, p1, Lcom/facebook/stickers/model/StickerPack;->n:Z

    move v0, v0

    .line 618549
    if-eqz v0, :cond_2

    move v0, v1

    :goto_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 618550
    sget-object v0, LX/3dy;->r:LX/0U1;

    .line 618551
    iget-object v5, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v5

    .line 618552
    iget-boolean v5, p1, Lcom/facebook/stickers/model/StickerPack;->o:Z

    move v5, v5

    .line 618553
    if-eqz v5, :cond_3

    :goto_4
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 618554
    sget-object v0, LX/3dy;->s:LX/0U1;

    .line 618555
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 618556
    iget-object v1, p1, Lcom/facebook/stickers/model/StickerPack;->p:LX/0Px;

    move-object v2, v1

    .line 618557
    if-nez v2, :cond_6

    .line 618558
    const/4 v1, 0x0

    .line 618559
    :goto_5
    move-object v1, v1

    .line 618560
    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 618561
    sget-object v0, LX/3dy;->t:LX/0U1;

    .line 618562
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 618563
    invoke-virtual {v4, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 618564
    sget-object v0, LX/3dy;->m:LX/0U1;

    .line 618565
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 618566
    iget-object v1, p1, Lcom/facebook/stickers/model/StickerPack;->r:Lcom/facebook/stickers/model/StickerCapabilities;

    move-object v1, v1

    .line 618567
    iget-object v1, v1, Lcom/facebook/stickers/model/StickerCapabilities;->a:LX/03R;

    invoke-virtual {v1}, LX/03R;->getDbValue()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 618568
    sget-object v0, LX/3dy;->n:LX/0U1;

    .line 618569
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 618570
    iget-object v1, p1, Lcom/facebook/stickers/model/StickerPack;->r:Lcom/facebook/stickers/model/StickerCapabilities;

    move-object v1, v1

    .line 618571
    iget-object v1, v1, Lcom/facebook/stickers/model/StickerCapabilities;->b:LX/03R;

    invoke-virtual {v1}, LX/03R;->getDbValue()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 618572
    sget-object v0, LX/3dy;->o:LX/0U1;

    .line 618573
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 618574
    iget-object v1, p1, Lcom/facebook/stickers/model/StickerPack;->r:Lcom/facebook/stickers/model/StickerCapabilities;

    move-object v1, v1

    .line 618575
    iget-object v1, v1, Lcom/facebook/stickers/model/StickerCapabilities;->c:LX/03R;

    invoke-virtual {v1}, LX/03R;->getDbValue()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 618576
    sget-object v0, LX/3dy;->u:LX/0U1;

    .line 618577
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 618578
    iget-object v1, p1, Lcom/facebook/stickers/model/StickerPack;->r:Lcom/facebook/stickers/model/StickerCapabilities;

    move-object v1, v1

    .line 618579
    iget-object v1, v1, Lcom/facebook/stickers/model/StickerCapabilities;->d:LX/03R;

    invoke-virtual {v1}, LX/03R;->getDbValue()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 618580
    sget-object v0, LX/3dy;->v:LX/0U1;

    .line 618581
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 618582
    iget-object v1, p1, Lcom/facebook/stickers/model/StickerPack;->r:Lcom/facebook/stickers/model/StickerCapabilities;

    move-object v1, v1

    .line 618583
    iget-object v1, v1, Lcom/facebook/stickers/model/StickerCapabilities;->e:LX/03R;

    invoke-virtual {v1}, LX/03R;->getDbValue()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 618584
    sget-object v0, LX/3dy;->w:LX/0U1;

    .line 618585
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 618586
    iget-object v1, p1, Lcom/facebook/stickers/model/StickerPack;->r:Lcom/facebook/stickers/model/StickerCapabilities;

    move-object v1, v1

    .line 618587
    iget-object v1, v1, Lcom/facebook/stickers/model/StickerCapabilities;->f:LX/03R;

    invoke-virtual {v1}, LX/03R;->getDbValue()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 618588
    return-object v4

    :cond_0
    move v0, v2

    .line 618589
    goto/16 :goto_1

    :cond_1
    move v0, v2

    .line 618590
    goto/16 :goto_2

    :cond_2
    move v0, v2

    .line 618591
    goto/16 :goto_3

    :cond_3
    move v1, v2

    .line 618592
    goto/16 :goto_4

    .line 618593
    :cond_4
    new-instance v4, LX/162;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v4, v0}, LX/162;-><init>(LX/0mC;)V

    .line 618594
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_6
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 618595
    new-instance v6, LX/0m9;

    sget-object v7, LX/0mC;->a:LX/0mC;

    invoke-direct {v6, v7}, LX/0m9;-><init>(LX/0mC;)V

    .line 618596
    const-string v7, "id"

    invoke-virtual {v6, v7, v0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 618597
    invoke-virtual {v4, v6}, LX/162;->a(LX/0lF;)LX/162;

    goto :goto_6

    .line 618598
    :cond_5
    move-object v0, v4

    .line 618599
    invoke-virtual {v0}, LX/162;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 618600
    :cond_6
    new-instance v5, LX/162;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v5, v1}, LX/162;-><init>(LX/0mC;)V

    .line 618601
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_7
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 618602
    invoke-virtual {v5, v1}, LX/162;->g(Ljava/lang/String;)LX/162;

    goto :goto_7

    .line 618603
    :cond_7
    move-object v1, v5

    .line 618604
    invoke-virtual {v1}, LX/162;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_5
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/stickers/model/Sticker;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 618167
    iget-object v0, p0, LX/3dx;->g:LX/3e2;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 618168
    const v1, -0x1e14e23b

    invoke-static {v0, v1}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 618169
    const-string v1, "recent_stickers"

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 618170
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 618171
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 618172
    sget-object v1, LX/3eB;->a:LX/0U1;

    invoke-virtual {v1, v2}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v1

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 618173
    iget-object v3, p0, LX/3dx;->h:LX/3eI;

    .line 618174
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 618175
    const/4 v4, 0x0

    .line 618176
    :goto_0
    move-object v1, v4

    .line 618177
    :cond_0
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 618178
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 618179
    const v2, -0x66102552

    invoke-static {v0, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 618180
    return-object v1

    .line 618181
    :catchall_0
    move-exception v1

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 618182
    const v2, -0x448a96bb

    invoke-static {v0, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v1

    .line 618183
    :cond_1
    iget-object v4, v3, LX/3eI;->a:LX/0lC;

    invoke-virtual {v4, v1}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    .line 618184
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v6

    .line 618185
    invoke-static {}, LX/4m0;->newBuilder()LX/4m0;

    move-result-object v7

    .line 618186
    const/4 v5, 0x0

    :goto_1
    invoke-virtual {v4}, LX/0lF;->e()I

    move-result v8

    if-ge v5, v8, :cond_2

    .line 618187
    invoke-virtual {v4, v5}, LX/0lF;->a(I)LX/0lF;

    move-result-object v8

    .line 618188
    const-string v9, "id"

    invoke-virtual {v8, v9}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v9

    invoke-static {v9}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v9

    .line 618189
    const-string v10, "pack_id"

    invoke-virtual {v8, v10}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v10

    invoke-static {v10}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v10

    .line 618190
    const-string v11, "uri"

    invoke-virtual {v8, v11}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v11

    invoke-static {v11}, LX/3eI;->d(LX/0lF;)Landroid/net/Uri;

    move-result-object v11

    .line 618191
    const-string v12, "disk_uri"

    invoke-virtual {v8, v12}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v12

    invoke-static {v12}, LX/3eI;->d(LX/0lF;)Landroid/net/Uri;

    move-result-object v12

    .line 618192
    const-string v13, "animated_uri"

    invoke-virtual {v8, v13}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v13

    invoke-static {v13}, LX/3eI;->d(LX/0lF;)Landroid/net/Uri;

    move-result-object v13

    .line 618193
    const-string p0, "animated_disk_uri"

    invoke-virtual {v8, p0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object p0

    invoke-static {p0}, LX/3eI;->d(LX/0lF;)Landroid/net/Uri;

    move-result-object p0

    .line 618194
    const-string v3, "preview_uri"

    invoke-virtual {v8, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    invoke-static {v3}, LX/3eI;->d(LX/0lF;)Landroid/net/Uri;

    move-result-object v3

    .line 618195
    const-string v1, "preview_disk_uri"

    invoke-virtual {v8, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/3eI;->d(LX/0lF;)Landroid/net/Uri;

    move-result-object v1

    .line 618196
    invoke-static {v8}, LX/3eI;->a(LX/0lF;)Lcom/facebook/stickers/model/StickerCapabilities;

    move-result-object v8

    .line 618197
    iput-object v9, v7, LX/4m0;->a:Ljava/lang/String;

    .line 618198
    move-object v9, v7

    .line 618199
    iput-object v10, v9, LX/4m0;->b:Ljava/lang/String;

    .line 618200
    move-object v9, v9

    .line 618201
    iput-object v11, v9, LX/4m0;->c:Landroid/net/Uri;

    .line 618202
    move-object v9, v9

    .line 618203
    iput-object v12, v9, LX/4m0;->d:Landroid/net/Uri;

    .line 618204
    move-object v9, v9

    .line 618205
    iput-object v13, v9, LX/4m0;->e:Landroid/net/Uri;

    .line 618206
    move-object v9, v9

    .line 618207
    iput-object p0, v9, LX/4m0;->f:Landroid/net/Uri;

    .line 618208
    move-object v9, v9

    .line 618209
    iput-object v3, v9, LX/4m0;->g:Landroid/net/Uri;

    .line 618210
    move-object v9, v9

    .line 618211
    iput-object v1, v9, LX/4m0;->h:Landroid/net/Uri;

    .line 618212
    move-object v9, v9

    .line 618213
    iput-object v8, v9, LX/4m0;->i:Lcom/facebook/stickers/model/StickerCapabilities;

    .line 618214
    move-object v8, v9

    .line 618215
    invoke-virtual {v8}, LX/4m0;->a()Lcom/facebook/stickers/model/Sticker;

    move-result-object v8

    .line 618216
    invoke-virtual {v6, v8}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 618217
    invoke-virtual {v7}, LX/4m0;->b()V

    .line 618218
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_1

    .line 618219
    :cond_2
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    move-object v4, v5

    .line 618220
    goto/16 :goto_0
.end method

.method public final a(LX/3do;)LX/0Px;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3do;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/stickers/model/StickerPack;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 618049
    invoke-static {p1}, LX/3dx;->e(LX/3do;)I

    move-result v1

    .line 618050
    new-instance v8, LX/0Pz;

    invoke-direct {v8}, LX/0Pz;-><init>()V

    .line 618051
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 618052
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "pack_types INNER JOIN sticker_packs ON pack_types."

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v3, LX/3e8;->b:LX/0U1;

    .line 618053
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 618054
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "=sticker_packs"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, LX/3dy;->a:LX/0U1;

    .line 618055
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 618056
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 618057
    sget-object v2, LX/3e8;->a:LX/0U1;

    .line 618058
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 618059
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v4

    .line 618060
    iget-object v1, p0, LX/3dx;->g:LX/3e2;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v9

    .line 618061
    const v1, -0x2bc98fec

    invoke-static {v9, v1}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 618062
    iget-object v1, p0, LX/3dx;->g:LX/3e2;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    sget-object v2, LX/3dx;->b:[Ljava/lang/String;

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, LX/3e8;->c:LX/0U1;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " DESC"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    move-object v6, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 618063
    :goto_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 618064
    invoke-direct {p0, v1}, LX/3dx;->a(Landroid/database/Cursor;)Lcom/facebook/stickers/model/StickerPack;

    move-result-object v0

    invoke-virtual {v8, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 618065
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 618066
    const v1, 0x33fa7904

    invoke-static {v9, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0

    .line 618067
    :cond_0
    :try_start_1
    invoke-virtual {v9}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 618068
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 618069
    const v0, 0x27d31d2c

    invoke-static {v9, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 618070
    invoke-virtual {v8}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/util/Collection;)LX/0Px;
    .locals 32
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/stickers/model/Sticker;",
            ">;"
        }
    .end annotation

    .prologue
    .line 618071
    sget-object v2, LX/3dz;->a:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v2, v0}, LX/0uu;->a(Ljava/lang/String;Ljava/util/Collection;)LX/0ux;

    move-result-object v2

    .line 618072
    move-object/from16 v0, p0

    iget-object v3, v0, LX/3dx;->g:LX/3e2;

    invoke-virtual {v3}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 618073
    const v4, 0x59b8e7c8

    invoke-static {v3, v4}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 618074
    move-object/from16 v0, p0

    iget-object v4, v0, LX/3dx;->g:LX/3e2;

    invoke-virtual {v4}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, LX/3dx;->f:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "WHERE s."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v5, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    .line 618075
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 618076
    :try_start_0
    const-string v5, "sticker_id"

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    .line 618077
    const-string v6, "sticker_pack_id"

    invoke-interface {v4, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    .line 618078
    const-string v7, "static_uri"

    invoke-interface {v4, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    .line 618079
    const-string v8, "animated_uri"

    invoke-interface {v4, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    .line 618080
    const-string v9, "static_asset"

    invoke-interface {v4, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    .line 618081
    const-string v10, "animated_asset"

    invoke-interface {v4, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    .line 618082
    const-string v11, "preview_uri"

    invoke-interface {v4, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    .line 618083
    const-string v12, "preview_asset"

    invoke-interface {v4, v12}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v12

    .line 618084
    const-string v13, "is_comments_capable"

    invoke-interface {v4, v13}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    .line 618085
    const-string v14, "is_composer_capable"

    invoke-interface {v4, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    .line 618086
    const-string v15, "is_messenger_capable"

    invoke-interface {v4, v15}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v15

    .line 618087
    const-string v16, "is_sms_capable"

    move-object/from16 v0, v16

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v16

    .line 618088
    const-string v17, "is_posts_capable"

    move-object/from16 v0, v17

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v17

    .line 618089
    const-string v18, "is_montage_capable"

    move-object/from16 v0, v18

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v18

    .line 618090
    invoke-static {}, LX/4m0;->newBuilder()LX/4m0;

    move-result-object v19

    .line 618091
    :goto_0
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    move-result v20

    if-eqz v20, :cond_3

    .line 618092
    invoke-interface {v4, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    .line 618093
    invoke-interface {v4, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v21

    .line 618094
    invoke-interface {v4, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v22 .. v22}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v22

    .line 618095
    invoke-interface {v4, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v23 .. v23}, LX/3dx;->d(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v23

    .line 618096
    invoke-interface {v4, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v24 .. v24}, LX/3dx;->d(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v24

    .line 618097
    invoke-interface {v4, v13}, Landroid/database/Cursor;->getInt(I)I

    move-result v25

    invoke-static/range {v25 .. v25}, LX/03R;->fromDbValue(I)LX/03R;

    move-result-object v25

    .line 618098
    invoke-interface {v4, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v26

    invoke-static/range {v26 .. v26}, LX/03R;->fromDbValue(I)LX/03R;

    move-result-object v26

    .line 618099
    invoke-interface {v4, v15}, Landroid/database/Cursor;->getInt(I)I

    move-result v27

    invoke-static/range {v27 .. v27}, LX/03R;->fromDbValue(I)LX/03R;

    move-result-object v27

    .line 618100
    move/from16 v0, v16

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v28

    invoke-static/range {v28 .. v28}, LX/03R;->fromDbValue(I)LX/03R;

    move-result-object v28

    .line 618101
    move/from16 v0, v17

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v29

    invoke-static/range {v29 .. v29}, LX/03R;->fromDbValue(I)LX/03R;

    move-result-object v29

    .line 618102
    move/from16 v0, v18

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v30

    invoke-static/range {v30 .. v30}, LX/03R;->fromDbValue(I)LX/03R;

    move-result-object v30

    .line 618103
    invoke-static {}, Lcom/facebook/stickers/model/StickerCapabilities;->newBuilder()LX/4m3;

    move-result-object v31

    move-object/from16 v0, v31

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, LX/4m3;->a(LX/03R;)LX/4m3;

    move-result-object v25

    invoke-virtual/range {v25 .. v26}, LX/4m3;->b(LX/03R;)LX/4m3;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, LX/4m3;->c(LX/03R;)LX/4m3;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, LX/4m3;->d(LX/03R;)LX/4m3;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, LX/4m3;->e(LX/03R;)LX/4m3;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, LX/4m3;->f(LX/03R;)LX/4m3;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, LX/4m3;->a()Lcom/facebook/stickers/model/StickerCapabilities;

    move-result-object v25

    .line 618104
    invoke-virtual/range {v19 .. v20}, LX/4m0;->a(Ljava/lang/String;)LX/4m0;

    move-result-object v20

    invoke-virtual/range {v20 .. v21}, LX/4m0;->b(Ljava/lang/String;)LX/4m0;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, LX/4m0;->a(Landroid/net/Uri;)LX/4m0;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, LX/4m0;->c(Landroid/net/Uri;)LX/4m0;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, LX/4m0;->e(Landroid/net/Uri;)LX/4m0;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, LX/4m0;->a(Lcom/facebook/stickers/model/StickerCapabilities;)LX/4m0;

    .line 618105
    invoke-interface {v4, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    .line 618106
    invoke-interface {v4, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v21

    .line 618107
    invoke-interface {v4, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v22

    .line 618108
    if-eqz v20, :cond_0

    .line 618109
    new-instance v23, Ljava/io/File;

    move-object/from16 v0, v23

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static/range {v23 .. v23}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, LX/4m0;->b(Landroid/net/Uri;)LX/4m0;

    .line 618110
    :cond_0
    if-eqz v21, :cond_1

    .line 618111
    new-instance v20, Ljava/io/File;

    invoke-direct/range {v20 .. v21}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static/range {v20 .. v20}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, LX/4m0;->d(Landroid/net/Uri;)LX/4m0;

    .line 618112
    :cond_1
    if-eqz v22, :cond_2

    .line 618113
    new-instance v20, Ljava/io/File;

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static/range {v20 .. v20}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, LX/4m0;->f(Landroid/net/Uri;)LX/4m0;

    .line 618114
    :cond_2
    invoke-virtual/range {v19 .. v19}, LX/4m0;->a()Lcom/facebook/stickers/model/Sticker;

    move-result-object v20

    .line 618115
    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 618116
    invoke-virtual/range {v19 .. v19}, LX/4m0;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    .line 618117
    :catchall_0
    move-exception v2

    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 618118
    const v4, -0x5180aacb

    invoke-static {v3, v4}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v2

    .line 618119
    :cond_3
    :try_start_1
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 618120
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 618121
    const v4, -0x5f72f184

    invoke-static {v3, v4}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 618122
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    return-object v2
.end method

.method public final a(LX/0Px;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/stickers/model/StickerTag;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 618123
    iget-object v0, p0, LX/3dx;->g:LX/3e2;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 618124
    const v0, 0x332e106e

    invoke-static {v4, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 618125
    :try_start_0
    const-string v0, "sticker_tags"

    const/4 v1, 0x0

    const/4 v3, 0x0

    invoke-virtual {v4, v0, v1, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 618126
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v5

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_1

    invoke-virtual {p1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/model/StickerTag;

    .line 618127
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 618128
    sget-object v1, LX/3eE;->a:LX/0U1;

    .line 618129
    iget-object v7, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v7

    .line 618130
    iget-object v7, v0, Lcom/facebook/stickers/model/StickerTag;->a:Ljava/lang/String;

    move-object v7, v7

    .line 618131
    invoke-virtual {v6, v1, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 618132
    sget-object v1, LX/3eE;->b:LX/0U1;

    .line 618133
    iget-object v7, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v7

    .line 618134
    iget-object v7, v0, Lcom/facebook/stickers/model/StickerTag;->b:Ljava/lang/String;

    move-object v7, v7

    .line 618135
    invoke-virtual {v6, v1, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 618136
    sget-object v1, LX/3eE;->c:LX/0U1;

    .line 618137
    iget-object v7, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v7

    .line 618138
    iget-object v7, v0, Lcom/facebook/stickers/model/StickerTag;->c:Ljava/lang/String;

    move-object v7, v7

    .line 618139
    invoke-virtual {v6, v1, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 618140
    sget-object v1, LX/3eE;->d:LX/0U1;

    .line 618141
    iget-object v7, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v7, v7

    .line 618142
    iget-boolean v1, v0, Lcom/facebook/stickers/model/StickerTag;->d:Z

    move v1, v1

    .line 618143
    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_1
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v6, v7, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 618144
    sget-object v1, LX/3eE;->e:LX/0U1;

    .line 618145
    iget-object v7, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v7

    .line 618146
    iget v7, v0, Lcom/facebook/stickers/model/StickerTag;->e:I

    move v7, v7

    .line 618147
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v1, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 618148
    sget-object v1, LX/3eE;->f:LX/0U1;

    .line 618149
    iget-object v7, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v7

    .line 618150
    iget-object v7, v0, Lcom/facebook/stickers/model/StickerTag;->f:Ljava/lang/String;

    move-object v0, v7

    .line 618151
    invoke-virtual {v6, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 618152
    const-string v0, "sticker_tags"

    const/4 v1, 0x0

    const v7, 0x4445d662

    invoke-static {v7}, LX/03h;->a(I)V

    invoke-virtual {v4, v0, v1, v6}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v0, -0x60c1455e

    invoke-static {v0}, LX/03h;->a(I)V

    .line 618153
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_0
    move v1, v2

    .line 618154
    goto :goto_1

    .line 618155
    :cond_1
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 618156
    const v0, 0x622f414

    invoke-static {v4, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 618157
    return-void

    .line 618158
    :catchall_0
    move-exception v0

    const v1, 0x6ed63c67

    invoke-static {v4, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method

.method public final a(LX/3do;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3do;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/stickers/model/StickerPack;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 618159
    iget-object v0, p0, LX/3dx;->g:LX/3e2;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 618160
    const v0, -0x75e447d2

    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 618161
    :try_start_0
    invoke-direct {p0, p1}, LX/3dx;->c(LX/3do;)V

    .line 618162
    invoke-direct {p0, p1, p2}, LX/3dx;->c(LX/3do;Ljava/util/List;)V

    .line 618163
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 618164
    const v0, -0x75605848

    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 618165
    return-void

    .line 618166
    :catchall_0
    move-exception v0

    const v2, 0x7b25b252

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method

.method public final a(Lcom/facebook/stickers/model/StickerPack;)V
    .locals 2

    .prologue
    .line 618221
    invoke-static {p0, p1}, LX/3dx;->c(LX/3dx;Lcom/facebook/stickers/model/StickerPack;)I

    move-result v0

    .line 618222
    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 618223
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot update a sticker pack not originally in the table."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 618224
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;LX/3e1;)V
    .locals 4

    .prologue
    .line 618225
    invoke-static {}, LX/0uu;->a()LX/0uw;

    move-result-object v0

    .line 618226
    sget-object v1, LX/3e0;->a:LX/0U1;

    .line 618227
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 618228
    invoke-static {v1, p1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 618229
    sget-object v1, LX/3e0;->b:LX/0U1;

    .line 618230
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 618231
    invoke-virtual {p2}, LX/3e1;->getDbName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 618232
    iget-object v1, p0, LX/3dx;->g:LX/3e2;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 618233
    const v2, 0x31b23637

    invoke-static {v1, v2}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 618234
    :try_start_0
    const-string v2, "sticker_asserts"

    invoke-virtual {v0}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 618235
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 618236
    const v0, -0x5836986

    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 618237
    return-void

    .line 618238
    :catch_0
    move-exception v0

    .line 618239
    :try_start_1
    sget-object v2, LX/3dx;->a:Ljava/lang/Class;

    const-string v3, "error deleting one asset file."

    invoke-static {v2, v3, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 618240
    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 618241
    :catchall_0
    move-exception v0

    const v2, 0x64b9666d

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method

.method public final a(Ljava/lang/String;LX/3e1;Ljava/io/File;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 618242
    iget-object v0, p0, LX/3dx;->g:LX/3e2;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 618243
    const v0, -0x416df4d6

    invoke-static {v2, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 618244
    const/4 v1, 0x0

    .line 618245
    sget-object v0, LX/3dx;->e:Ljava/lang/String;

    invoke-virtual {v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v3

    .line 618246
    :try_start_0
    invoke-direct {p0, p1, p2}, LX/3dx;->b(Ljava/lang/String;LX/3e1;)Ljava/io/File;

    move-result-object v1

    .line 618247
    if-eqz v1, :cond_1

    invoke-static {v1, p3}, LX/1t3;->b(Ljava/io/File;Ljava/io/File;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 618248
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 618249
    const v0, 0x58e0131b

    invoke-static {v2, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 618250
    :cond_0
    :goto_0
    return-void

    .line 618251
    :cond_1
    if-eqz v1, :cond_2

    :try_start_1
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-eqz v0, :cond_2

    .line 618252
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 618253
    const v0, -0x972c56d

    invoke-static {v2, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    goto :goto_0

    .line 618254
    :cond_2
    const/4 v0, 0x1

    :try_start_2
    invoke-virtual {v3, v0, p1}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 618255
    const/4 v0, 0x2

    invoke-virtual {p2}, LX/3e1;->getDbName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 618256
    const/4 v0, 0x3

    invoke-virtual {v3, v0, p1}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 618257
    const/4 v0, 0x4

    invoke-virtual {p3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 618258
    const/4 v4, 0x0

    .line 618259
    invoke-virtual {p3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    .line 618260
    if-eqz v0, :cond_5

    .line 618261
    const-string v5, "."

    invoke-virtual {v0, v5}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v5

    .line 618262
    const/4 p0, -0x1

    if-eq v5, p0, :cond_5

    .line 618263
    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v0, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 618264
    :goto_1
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/CharSequence;

    const/4 p0, 0x0

    aput-object v0, v5, p0

    invoke-static {v5}, LX/0YN;->a([Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 618265
    :goto_2
    move-object v0, v4

    .line 618266
    if-eqz v0, :cond_3

    .line 618267
    const/4 v4, 0x5

    invoke-virtual {v3, v4, v0}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 618268
    :goto_3
    const v0, -0x4dbf27cf

    invoke-static {v0}, LX/03h;->a(I)V

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteStatement;->executeInsert()J

    const v0, -0x6d3aee94

    invoke-static {v0}, LX/03h;->a(I)V

    .line 618269
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 618270
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 618271
    const v0, 0x33c80672

    invoke-static {v2, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 618272
    :goto_4
    if-eqz v1, :cond_0

    .line 618273
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    move-result v0

    if-nez v0, :cond_0

    .line 618274
    sget-object v0, LX/3dx;->a:Ljava/lang/Class;

    const-string v2, "cannot delete old asset file: %s"

    new-array v3, v6, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    invoke-static {v0, v2, v3}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 618275
    :cond_3
    const/4 v0, 0x5

    :try_start_3
    invoke-virtual {v3, v0}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_3

    .line 618276
    :catch_0
    move-exception v0

    .line 618277
    :try_start_4
    sget-object v4, LX/3dx;->a:Ljava/lang/Class;

    const-string v5, "failed saving file"

    invoke-static {v4, v5, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 618278
    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 618279
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 618280
    const v0, -0x4cc44244

    invoke-static {v2, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    goto :goto_4

    .line 618281
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 618282
    const v1, 0x25807100

    invoke-static {v2, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0

    :cond_4
    move-object v4, v0

    goto :goto_2

    :cond_5
    move-object v0, v4

    goto :goto_1
.end method

.method public final a(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/stickers/model/Sticker;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 618283
    if-nez p1, :cond_1

    .line 618284
    const/4 v1, 0x0

    .line 618285
    :goto_0
    move-object v0, v1

    .line 618286
    iget-object v1, p0, LX/3dx;->g:LX/3e2;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 618287
    const v2, 0x23e5fbab

    invoke-static {v1, v2}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 618288
    :try_start_0
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 618289
    sget-object v3, LX/3eB;->a:LX/0U1;

    .line 618290
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 618291
    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 618292
    const-string v0, "recent_stickers"

    const/4 v3, 0x0

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {v1, v0, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 618293
    if-nez v0, :cond_0

    .line 618294
    const-string v0, "recent_stickers"

    const/4 v3, 0x0

    const v4, -0x5d380391

    invoke-static {v4}, LX/03h;->a(I)V

    invoke-virtual {v1, v0, v3, v2}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v0, 0x5b7b8684

    invoke-static {v0}, LX/03h;->a(I)V

    .line 618295
    :cond_0
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 618296
    const v0, -0x31bddc0b

    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 618297
    return-void

    .line 618298
    :catchall_0
    move-exception v0

    const v2, -0x28977d99

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0

    .line 618299
    :cond_1
    new-instance v2, LX/162;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v2, v1}, LX/162;-><init>(LX/0mC;)V

    .line 618300
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/stickers/model/Sticker;

    .line 618301
    new-instance v4, LX/0m9;

    sget-object v5, LX/0mC;->a:LX/0mC;

    invoke-direct {v4, v5}, LX/0m9;-><init>(LX/0mC;)V

    .line 618302
    const-string v5, "id"

    iget-object v0, v1, Lcom/facebook/stickers/model/Sticker;->a:Ljava/lang/String;

    invoke-virtual {v4, v5, v0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 618303
    const-string v5, "pack_id"

    iget-object v0, v1, Lcom/facebook/stickers/model/Sticker;->b:Ljava/lang/String;

    invoke-virtual {v4, v5, v0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 618304
    const-string v5, "uri"

    iget-object v0, v1, Lcom/facebook/stickers/model/Sticker;->c:Landroid/net/Uri;

    invoke-static {v0}, LX/3eI;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v5, v0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 618305
    const-string v5, "disk_uri"

    iget-object v0, v1, Lcom/facebook/stickers/model/Sticker;->d:Landroid/net/Uri;

    invoke-static {v0}, LX/3eI;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v5, v0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 618306
    const-string v5, "animated_uri"

    iget-object v0, v1, Lcom/facebook/stickers/model/Sticker;->e:Landroid/net/Uri;

    invoke-static {v0}, LX/3eI;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v5, v0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 618307
    const-string v5, "animated_disk_uri"

    iget-object v0, v1, Lcom/facebook/stickers/model/Sticker;->f:Landroid/net/Uri;

    invoke-static {v0}, LX/3eI;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v5, v0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 618308
    const-string v5, "preview_uri"

    iget-object v0, v1, Lcom/facebook/stickers/model/Sticker;->g:Landroid/net/Uri;

    invoke-static {v0}, LX/3eI;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v5, v0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 618309
    const-string v5, "preview_disk_uri"

    iget-object v0, v1, Lcom/facebook/stickers/model/Sticker;->h:Landroid/net/Uri;

    invoke-static {v0}, LX/3eI;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v5, v0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 618310
    iget-object v1, v1, Lcom/facebook/stickers/model/Sticker;->i:Lcom/facebook/stickers/model/StickerCapabilities;

    .line 618311
    const-string v5, "is_comments_capable"

    iget-object v0, v1, Lcom/facebook/stickers/model/StickerCapabilities;->a:LX/03R;

    invoke-virtual {v0}, LX/03R;->getDbValue()I

    move-result v0

    invoke-virtual {v4, v5, v0}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 618312
    const-string v5, "is_composer_capable"

    iget-object v0, v1, Lcom/facebook/stickers/model/StickerCapabilities;->b:LX/03R;

    invoke-virtual {v0}, LX/03R;->getDbValue()I

    move-result v0

    invoke-virtual {v4, v5, v0}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 618313
    const-string v5, "is_messenger_capable"

    iget-object v0, v1, Lcom/facebook/stickers/model/StickerCapabilities;->c:LX/03R;

    invoke-virtual {v0}, LX/03R;->getDbValue()I

    move-result v0

    invoke-virtual {v4, v5, v0}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 618314
    const-string v5, "is_sms_capable"

    iget-object v0, v1, Lcom/facebook/stickers/model/StickerCapabilities;->d:LX/03R;

    invoke-virtual {v0}, LX/03R;->getDbValue()I

    move-result v0

    invoke-virtual {v4, v5, v0}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 618315
    const-string v5, "is_posts_capable"

    iget-object v0, v1, Lcom/facebook/stickers/model/StickerCapabilities;->e:LX/03R;

    invoke-virtual {v0}, LX/03R;->getDbValue()I

    move-result v0

    invoke-virtual {v4, v5, v0}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 618316
    const-string v5, "is_montage_capable"

    iget-object v0, v1, Lcom/facebook/stickers/model/StickerCapabilities;->f:LX/03R;

    invoke-virtual {v0}, LX/03R;->getDbValue()I

    move-result v0

    invoke-virtual {v4, v5, v0}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 618317
    invoke-virtual {v2, v4}, LX/162;->a(LX/0lF;)LX/162;

    goto/16 :goto_1

    .line 618318
    :cond_2
    move-object v1, v2

    .line 618319
    invoke-virtual {v1}, LX/162;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 618320
    sget-object v0, LX/3dy;->a:LX/0U1;

    .line 618321
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 618322
    invoke-static {v0, p1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v4

    .line 618323
    iget-object v0, p0, LX/3dx;->g:LX/3e2;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 618324
    const v1, -0x5f6d9b7d

    invoke-static {v0, v1}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 618325
    const-string v1, "sticker_packs"

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 618326
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v1, 0x1

    .line 618327
    :goto_0
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 618328
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 618329
    const v2, 0x1b75a504

    invoke-static {v0, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    return v1

    .line 618330
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 618331
    :catchall_0
    move-exception v1

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 618332
    const v2, -0x50b3be33

    invoke-static {v0, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v1
.end method

.method public final b()LX/0Px;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 618333
    new-instance v8, LX/0Pz;

    invoke-direct {v8}, LX/0Pz;-><init>()V

    .line 618334
    iget-object v0, p0, LX/3dx;->g:LX/3e2;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 618335
    const v1, -0x14959b19

    invoke-static {v0, v1}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 618336
    const-string v1, "closed_download_preview_sticker_packs"

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 618337
    :try_start_0
    sget-object v1, LX/3eG;->a:LX/0U1;

    .line 618338
    iget-object v3, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v3

    .line 618339
    invoke-interface {v2, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 618340
    :goto_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 618341
    invoke-interface {v2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v8, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 618342
    :catchall_0
    move-exception v1

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 618343
    const v2, 0x30496910

    invoke-static {v0, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v1

    .line 618344
    :cond_0
    :try_start_1
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 618345
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 618346
    const v1, 0x75d10854

    invoke-static {v0, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 618347
    invoke-virtual {v8}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/String;)Lcom/facebook/stickers/model/StickerPack;
    .locals 8
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 618348
    sget-object v0, LX/3dy;->a:LX/0U1;

    .line 618349
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 618350
    invoke-static {v0, p1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v4

    .line 618351
    iget-object v0, p0, LX/3dx;->g:LX/3e2;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 618352
    const v1, -0x16f3fd5

    invoke-static {v0, v1}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 618353
    const-string v1, "sticker_packs"

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 618354
    :try_start_0
    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_0

    .line 618355
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    .line 618356
    invoke-direct {p0, v3}, LX/3dx;->a(Landroid/database/Cursor;)Lcom/facebook/stickers/model/StickerPack;

    move-result-object v2

    .line 618357
    :cond_0
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 618358
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 618359
    const v1, -0x77f9b0eb

    invoke-static {v0, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    return-object v2

    .line 618360
    :catchall_0
    move-exception v1

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 618361
    const v2, 0x1668ae39

    invoke-static {v0, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v1
.end method

.method public final b(LX/3do;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3do;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/stickers/model/StickerPack;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 618042
    iget-object v0, p0, LX/3dx;->g:LX/3e2;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 618043
    const v0, 0x72fad5b8

    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 618044
    :try_start_0
    invoke-direct {p0, p1, p2}, LX/3dx;->c(LX/3do;Ljava/util/List;)V

    .line 618045
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 618046
    const v0, 0x7cb40b16

    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 618047
    return-void

    .line 618048
    :catchall_0
    move-exception v0

    const v2, -0x12565363

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method

.method public final b(Lcom/facebook/stickers/model/StickerPack;)V
    .locals 6

    .prologue
    .line 618362
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 618363
    sget-object v1, LX/3eG;->a:LX/0U1;

    .line 618364
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 618365
    iget-object v2, p1, Lcom/facebook/stickers/model/StickerPack;->a:Ljava/lang/String;

    move-object v2, v2

    .line 618366
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 618367
    iget-object v1, p0, LX/3dx;->g:LX/3e2;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 618368
    const v2, 0x7235ed63

    invoke-static {v1, v2}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 618369
    :try_start_0
    const-string v2, "closed_download_preview_sticker_packs"

    const/4 v3, 0x0

    const/4 v4, 0x4

    const v5, -0x2b11f2e9

    invoke-static {v5}, LX/03h;->a(I)V

    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    const v0, 0x241cfcef

    invoke-static {v0}, LX/03h;->a(I)V

    .line 618370
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 618371
    const v0, -0x30f6e01d

    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 618372
    return-void

    .line 618373
    :catchall_0
    move-exception v0

    const v2, 0x3fed8feb

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method

.method public final b(Ljava/util/Collection;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/facebook/stickers/model/Sticker;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 618374
    iget-object v0, p0, LX/3dx;->g:LX/3e2;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 618375
    const v0, -0x40c0f959

    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 618376
    :try_start_0
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/model/Sticker;

    .line 618377
    invoke-direct {p0, v0}, LX/3dx;->a(Lcom/facebook/stickers/model/Sticker;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 618378
    :catchall_0
    move-exception v0

    const v2, -0x11108030

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0

    .line 618379
    :cond_0
    :try_start_1
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 618380
    const v0, 0x4fe770cd

    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 618381
    return-void
.end method

.method public final b(LX/3do;)Z
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 618382
    invoke-static {p1}, LX/3dx;->e(LX/3do;)I

    move-result v0

    .line 618383
    sget-object v1, LX/3e8;->a:LX/0U1;

    .line 618384
    iget-object v3, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v3

    .line 618385
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v4

    .line 618386
    iget-object v0, p0, LX/3dx;->g:LX/3e2;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 618387
    const v1, -0x21584d1c

    invoke-static {v0, v1}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 618388
    const-string v1, "pack_types"

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 618389
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v1, 0x1

    .line 618390
    :goto_0
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 618391
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 618392
    const v2, 0x8c3221

    invoke-static {v0, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    return v1

    .line 618393
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 618394
    :catchall_0
    move-exception v1

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 618395
    const v2, 0x50d873b5

    invoke-static {v0, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v1
.end method

.method public final c()LX/0Px;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/stickers/model/StickerTag;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 618396
    new-instance v8, LX/0Pz;

    invoke-direct {v8}, LX/0Pz;-><init>()V

    .line 618397
    iget-object v0, p0, LX/3dx;->g:LX/3e2;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 618398
    const v1, 0x2a047112

    invoke-static {v0, v1}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 618399
    const-string v1, "sticker_tags"

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 618400
    :try_start_0
    sget-object v1, LX/3eE;->a:LX/0U1;

    .line 618401
    iget-object v3, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v3

    .line 618402
    invoke-interface {v2, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 618403
    sget-object v1, LX/3eE;->b:LX/0U1;

    .line 618404
    iget-object v4, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v4

    .line 618405
    invoke-interface {v2, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    .line 618406
    sget-object v1, LX/3eE;->c:LX/0U1;

    .line 618407
    iget-object v5, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v5

    .line 618408
    invoke-interface {v2, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    .line 618409
    sget-object v1, LX/3eE;->d:LX/0U1;

    .line 618410
    iget-object v6, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v6

    .line 618411
    invoke-interface {v2, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    .line 618412
    sget-object v1, LX/3eE;->e:LX/0U1;

    .line 618413
    iget-object v7, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v7

    .line 618414
    invoke-interface {v2, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    .line 618415
    sget-object v1, LX/3eE;->f:LX/0U1;

    .line 618416
    iget-object v9, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v9

    .line 618417
    invoke-interface {v2, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    .line 618418
    :goto_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 618419
    invoke-static {}, Lcom/facebook/stickers/model/StickerTag;->newBuilder()LX/4m8;

    move-result-object v10

    .line 618420
    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 618421
    iput-object v1, v10, LX/4m8;->a:Ljava/lang/String;

    .line 618422
    invoke-interface {v2, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 618423
    iput-object v1, v10, LX/4m8;->b:Ljava/lang/String;

    .line 618424
    invoke-interface {v2, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 618425
    iput-object v1, v10, LX/4m8;->c:Ljava/lang/String;

    .line 618426
    invoke-interface {v2, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-lez v1, :cond_0

    const/4 v1, 0x1

    .line 618427
    :goto_1
    iput-boolean v1, v10, LX/4m8;->d:Z

    .line 618428
    invoke-interface {v2, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 618429
    iput v1, v10, LX/4m8;->e:I

    .line 618430
    invoke-interface {v2, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 618431
    iput-object v1, v10, LX/4m8;->f:Ljava/lang/String;

    .line 618432
    invoke-virtual {v10}, LX/4m8;->a()Lcom/facebook/stickers/model/StickerTag;

    move-result-object v1

    invoke-virtual {v8, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 618433
    :catchall_0
    move-exception v1

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 618434
    const v2, 0x80b4a64

    invoke-static {v0, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v1

    .line 618435
    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    .line 618436
    :cond_1
    :try_start_1
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 618437
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 618438
    const v1, 0x280279cf

    invoke-static {v0, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 618439
    invoke-virtual {v8}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/util/Collection;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 618440
    iget-object v0, p0, LX/3dx;->g:LX/3e2;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 618441
    const v0, -0x5e20b2a2

    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 618442
    :try_start_0
    sget-object v0, LX/3dz;->a:LX/0U1;

    .line 618443
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 618444
    invoke-static {v0, p1}, LX/0uu;->a(Ljava/lang/String;Ljava/util/Collection;)LX/0ux;

    move-result-object v0

    .line 618445
    const-string v2, "stickers"

    invoke-virtual {v0}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 618446
    invoke-static {p0, p1}, LX/3dx;->d(LX/3dx;Ljava/util/Collection;)V

    .line 618447
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 618448
    const v0, 0xdb14430

    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 618449
    return-void

    .line 618450
    :catchall_0
    move-exception v0

    const v2, 0x1a0e3ef2

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method
