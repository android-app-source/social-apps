.class public final LX/3mp;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/3mm;",
        ">;"
    }
.end annotation


# static fields
.field private static b:[Ljava/lang/String;

.field private static c:I


# instance fields
.field public a:LX/3mn;

.field private d:Ljava/util/BitSet;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 637314
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "footerText"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "buttonClickHandler"

    aput-object v2, v0, v1

    sput-object v0, LX/3mp;->b:[Ljava/lang/String;

    .line 637315
    sput v3, LX/3mp;->c:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 637316
    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 637317
    new-instance v0, Ljava/util/BitSet;

    sget v1, LX/3mp;->c:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/3mp;->d:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/3mp;LX/1De;IILX/3mn;)V
    .locals 1

    .prologue
    .line 637310
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 637311
    iput-object p4, p0, LX/3mp;->a:LX/3mn;

    .line 637312
    iget-object v0, p0, LX/3mp;->d:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 637313
    return-void
.end method


# virtual methods
.method public final a(LX/1dQ;)LX/3mp;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;)",
            "LX/3mp;"
        }
    .end annotation

    .prologue
    .line 637318
    iget-object v0, p0, LX/3mp;->a:LX/3mn;

    iput-object p1, v0, LX/3mn;->b:LX/1dQ;

    .line 637319
    iget-object v0, p0, LX/3mp;->d:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 637320
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 637306
    invoke-super {p0}, LX/1X5;->a()V

    .line 637307
    const/4 v0, 0x0

    iput-object v0, p0, LX/3mp;->a:LX/3mn;

    .line 637308
    sget-object v0, LX/3mm;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 637309
    return-void
.end method

.method public final b(Ljava/lang/String;)LX/3mp;
    .locals 2

    .prologue
    .line 637303
    iget-object v0, p0, LX/3mp;->a:LX/3mn;

    iput-object p1, v0, LX/3mn;->a:Ljava/lang/String;

    .line 637304
    iget-object v0, p0, LX/3mp;->d:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 637305
    return-object p0
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/3mm;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 637290
    iget-object v1, p0, LX/3mp;->d:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/3mp;->d:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    sget v2, LX/3mp;->c:I

    if-ge v1, v2, :cond_2

    .line 637291
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 637292
    :goto_0
    sget v2, LX/3mp;->c:I

    if-ge v0, v2, :cond_1

    .line 637293
    iget-object v2, p0, LX/3mp;->d:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 637294
    sget-object v2, LX/3mp;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 637295
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 637296
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 637297
    :cond_2
    iget-object v0, p0, LX/3mp;->a:LX/3mn;

    .line 637298
    invoke-virtual {p0}, LX/3mp;->a()V

    .line 637299
    return-object v0
.end method

.method public final h(I)LX/3mp;
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 637300
    iget-object v0, p0, LX/3mp;->a:LX/3mn;

    invoke-virtual {p0, p1}, LX/1Dp;->b(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/3mn;->a:Ljava/lang/String;

    .line 637301
    iget-object v0, p0, LX/3mp;->d:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 637302
    return-object p0
.end method
