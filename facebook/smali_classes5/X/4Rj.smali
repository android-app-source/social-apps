.class public LX/4Rj;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 710255
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 29

    .prologue
    .line 710256
    const/16 v23, 0x0

    .line 710257
    const/16 v22, 0x0

    .line 710258
    const/16 v21, 0x0

    .line 710259
    const/16 v20, 0x0

    .line 710260
    const/16 v19, 0x0

    .line 710261
    const/16 v18, 0x0

    .line 710262
    const/16 v17, 0x0

    .line 710263
    const/16 v16, 0x0

    .line 710264
    const-wide/16 v14, 0x0

    .line 710265
    const/4 v13, 0x0

    .line 710266
    const/4 v12, 0x0

    .line 710267
    const/4 v11, 0x0

    .line 710268
    const/4 v10, 0x0

    .line 710269
    const/4 v9, 0x0

    .line 710270
    const/4 v8, 0x0

    .line 710271
    const/4 v7, 0x0

    .line 710272
    const/4 v6, 0x0

    .line 710273
    const/4 v5, 0x0

    .line 710274
    const/4 v4, 0x0

    .line 710275
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v24

    sget-object v25, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    if-eq v0, v1, :cond_15

    .line 710276
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 710277
    const/4 v4, 0x0

    .line 710278
    :goto_0
    return v4

    .line 710279
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 710280
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v24

    sget-object v25, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    if-eq v0, v1, :cond_f

    .line 710281
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v24

    .line 710282
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 710283
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v25

    sget-object v26, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    if-eq v0, v1, :cond_1

    if-eqz v24, :cond_1

    .line 710284
    const-string v25, "id"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_2

    .line 710285
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v23

    goto :goto_1

    .line 710286
    :cond_2
    const-string v25, "inspiration_landing_position"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_3

    .line 710287
    const/4 v10, 0x1

    .line 710288
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v22

    goto :goto_1

    .line 710289
    :cond_3
    const-string v25, "prompt_confidence"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_4

    .line 710290
    const/4 v9, 0x1

    .line 710291
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Lcom/facebook/graphql/enums/GraphQLPromptConfidence;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPromptConfidence;

    move-result-object v21

    goto :goto_1

    .line 710292
    :cond_4
    const-string v25, "prompt_display_reason"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_5

    .line 710293
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v20

    goto :goto_1

    .line 710294
    :cond_5
    const-string v25, "prompt_feed_type"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_6

    .line 710295
    const/4 v8, 0x1

    .line 710296
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Lcom/facebook/graphql/enums/GraphQLPromptFeedType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPromptFeedType;

    move-result-object v19

    goto :goto_1

    .line 710297
    :cond_6
    const-string v25, "prompt_image"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_7

    .line 710298
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v18

    goto/16 :goto_1

    .line 710299
    :cond_7
    const-string v25, "prompt_title"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_8

    .line 710300
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v17

    goto/16 :goto_1

    .line 710301
    :cond_8
    const-string v25, "prompt_type"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_9

    .line 710302
    const/4 v5, 0x1

    .line 710303
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Lcom/facebook/graphql/enums/GraphQLPromptType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPromptType;

    move-result-object v16

    goto/16 :goto_1

    .line 710304
    :cond_9
    const-string v25, "ranking_score"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_a

    .line 710305
    const/4 v4, 0x1

    .line 710306
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    goto/16 :goto_1

    .line 710307
    :cond_a
    const-string v25, "time_range"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_b

    .line 710308
    invoke-static/range {p0 .. p1}, LX/4MJ;->a(LX/15w;LX/186;)I

    move-result v15

    goto/16 :goto_1

    .line 710309
    :cond_b
    const-string v25, "tracking_string"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_c

    .line 710310
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    goto/16 :goto_1

    .line 710311
    :cond_c
    const-string v25, "url"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_d

    .line 710312
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    goto/16 :goto_1

    .line 710313
    :cond_d
    const-string v25, "prompt_survey"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_e

    .line 710314
    invoke-static/range {p0 .. p1}, LX/4Rk;->a(LX/15w;LX/186;)I

    move-result v12

    goto/16 :goto_1

    .line 710315
    :cond_e
    const-string v25, "suggested_composition"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_0

    .line 710316
    invoke-static/range {p0 .. p1}, LX/4TV;->a(LX/15w;LX/186;)I

    move-result v11

    goto/16 :goto_1

    .line 710317
    :cond_f
    const/16 v24, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 710318
    const/16 v24, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v24

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 710319
    if-eqz v10, :cond_10

    .line 710320
    const/4 v10, 0x1

    const/16 v23, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v10, v1, v2}, LX/186;->a(III)V

    .line 710321
    :cond_10
    if-eqz v9, :cond_11

    .line 710322
    const/4 v9, 0x2

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v9, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 710323
    :cond_11
    const/4 v9, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v9, v1}, LX/186;->b(II)V

    .line 710324
    if-eqz v8, :cond_12

    .line 710325
    const/4 v8, 0x4

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v8, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 710326
    :cond_12
    const/4 v8, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 710327
    const/4 v8, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 710328
    if-eqz v5, :cond_13

    .line 710329
    const/4 v5, 0x7

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v5, v1}, LX/186;->a(ILjava/lang/Enum;)V

    .line 710330
    :cond_13
    if-eqz v4, :cond_14

    .line 710331
    const/16 v5, 0x8

    const-wide/16 v8, 0x0

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, LX/186;->a(IDD)V

    .line 710332
    :cond_14
    const/16 v4, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v15}, LX/186;->b(II)V

    .line 710333
    const/16 v4, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v14}, LX/186;->b(II)V

    .line 710334
    const/16 v4, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v13}, LX/186;->b(II)V

    .line 710335
    const/16 v4, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v12}, LX/186;->b(II)V

    .line 710336
    const/16 v4, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v11}, LX/186;->b(II)V

    .line 710337
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v4

    goto/16 :goto_0

    :cond_15
    move/from16 v27, v6

    move/from16 v28, v7

    move-wide v6, v14

    move v14, v12

    move v15, v13

    move v12, v10

    move v13, v11

    move v11, v9

    move v10, v8

    move/from16 v8, v27

    move/from16 v9, v28

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 8

    .prologue
    const/4 v7, 0x7

    const/4 v6, 0x4

    const/4 v3, 0x2

    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    .line 710338
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 710339
    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 710340
    if-eqz v0, :cond_0

    .line 710341
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 710342
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 710343
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 710344
    if-eqz v0, :cond_1

    .line 710345
    const-string v1, "inspiration_landing_position"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 710346
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 710347
    :cond_1
    invoke-virtual {p0, p1, v3, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 710348
    if-eqz v0, :cond_2

    .line 710349
    const-string v0, "prompt_confidence"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 710350
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPromptConfidence;

    invoke-virtual {p0, p1, v3, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPromptConfidence;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPromptConfidence;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 710351
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 710352
    if-eqz v0, :cond_3

    .line 710353
    const-string v1, "prompt_display_reason"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 710354
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 710355
    :cond_3
    invoke-virtual {p0, p1, v6, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 710356
    if-eqz v0, :cond_4

    .line 710357
    const-string v0, "prompt_feed_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 710358
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPromptFeedType;

    invoke-virtual {p0, p1, v6, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPromptFeedType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPromptFeedType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 710359
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 710360
    if-eqz v0, :cond_5

    .line 710361
    const-string v1, "prompt_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 710362
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 710363
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 710364
    if-eqz v0, :cond_6

    .line 710365
    const-string v1, "prompt_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 710366
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 710367
    :cond_6
    invoke-virtual {p0, p1, v7, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 710368
    if-eqz v0, :cond_7

    .line 710369
    const-string v0, "prompt_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 710370
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPromptType;

    invoke-virtual {p0, p1, v7, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPromptType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPromptType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 710371
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 710372
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_8

    .line 710373
    const-string v2, "ranking_score"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 710374
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 710375
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 710376
    if-eqz v0, :cond_9

    .line 710377
    const-string v1, "time_range"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 710378
    invoke-static {p0, v0, p2}, LX/4MJ;->a(LX/15i;ILX/0nX;)V

    .line 710379
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 710380
    if-eqz v0, :cond_a

    .line 710381
    const-string v1, "tracking_string"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 710382
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 710383
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 710384
    if-eqz v0, :cond_b

    .line 710385
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 710386
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 710387
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 710388
    if-eqz v0, :cond_c

    .line 710389
    const-string v1, "prompt_survey"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 710390
    invoke-static {p0, v0, p2}, LX/4Rk;->a(LX/15i;ILX/0nX;)V

    .line 710391
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 710392
    if-eqz v0, :cond_d

    .line 710393
    const-string v1, "suggested_composition"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 710394
    invoke-static {p0, v0, p2, p3}, LX/4TV;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 710395
    :cond_d
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 710396
    return-void
.end method
