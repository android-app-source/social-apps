.class public LX/4PQ;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 700349
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 21

    .prologue
    .line 700350
    const/16 v17, 0x0

    .line 700351
    const/16 v16, 0x0

    .line 700352
    const/4 v15, 0x0

    .line 700353
    const/4 v14, 0x0

    .line 700354
    const/4 v13, 0x0

    .line 700355
    const/4 v12, 0x0

    .line 700356
    const/4 v11, 0x0

    .line 700357
    const/4 v10, 0x0

    .line 700358
    const/4 v9, 0x0

    .line 700359
    const/4 v8, 0x0

    .line 700360
    const/4 v7, 0x0

    .line 700361
    const/4 v6, 0x0

    .line 700362
    const/4 v5, 0x0

    .line 700363
    const/4 v4, 0x0

    .line 700364
    const/4 v3, 0x0

    .line 700365
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v18

    sget-object v19, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-eq v0, v1, :cond_1

    .line 700366
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 700367
    const/4 v3, 0x0

    .line 700368
    :goto_0
    return v3

    .line 700369
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 700370
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v18

    sget-object v19, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-eq v0, v1, :cond_e

    .line 700371
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v18

    .line 700372
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 700373
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v19

    sget-object v20, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    if-eq v0, v1, :cond_1

    if-eqz v18, :cond_1

    .line 700374
    const-string v19, "action"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_2

    .line 700375
    invoke-static/range {p0 .. p1}, LX/4PP;->a(LX/15w;LX/186;)I

    move-result v17

    goto :goto_1

    .line 700376
    :cond_2
    const-string v19, "cache_id"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_3

    .line 700377
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    goto :goto_1

    .line 700378
    :cond_3
    const-string v19, "close_label"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_4

    .line 700379
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    goto :goto_1

    .line 700380
    :cond_4
    const-string v19, "content"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_5

    .line 700381
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v14

    goto :goto_1

    .line 700382
    :cond_5
    const-string v19, "id"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_6

    .line 700383
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    goto :goto_1

    .line 700384
    :cond_6
    const-string v19, "image"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_7

    .line 700385
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v12

    goto/16 :goto_1

    .line 700386
    :cond_7
    const-string v19, "image_action"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_8

    .line 700387
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    goto/16 :goto_1

    .line 700388
    :cond_8
    const-string v19, "is_persistent"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_9

    .line 700389
    const/4 v4, 0x1

    .line 700390
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v10

    goto/16 :goto_1

    .line 700391
    :cond_9
    const-string v19, "location"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_a

    .line 700392
    const/4 v3, 0x1

    .line 700393
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;

    move-result-object v9

    goto/16 :goto_1

    .line 700394
    :cond_a
    const-string v19, "social_context"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_b

    .line 700395
    invoke-static/range {p0 .. p1}, LX/2aE;->a(LX/15w;LX/186;)I

    move-result v8

    goto/16 :goto_1

    .line 700396
    :cond_b
    const-string v19, "title"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_c

    .line 700397
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto/16 :goto_1

    .line 700398
    :cond_c
    const-string v19, "tracking"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_d

    .line 700399
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_1

    .line 700400
    :cond_d
    const-string v19, "ui_version"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_0

    .line 700401
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto/16 :goto_1

    .line 700402
    :cond_e
    const/16 v18, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 700403
    const/16 v18, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 700404
    const/16 v17, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v16

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 700405
    const/16 v16, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1, v15}, LX/186;->b(II)V

    .line 700406
    const/4 v15, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v14}, LX/186;->b(II)V

    .line 700407
    const/4 v14, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v13}, LX/186;->b(II)V

    .line 700408
    const/4 v13, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v12}, LX/186;->b(II)V

    .line 700409
    const/4 v12, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v11}, LX/186;->b(II)V

    .line 700410
    if-eqz v4, :cond_f

    .line 700411
    const/4 v4, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v10}, LX/186;->a(IZ)V

    .line 700412
    :cond_f
    if-eqz v3, :cond_10

    .line 700413
    const/16 v3, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->a(ILjava/lang/Enum;)V

    .line 700414
    :cond_10
    const/16 v3, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 700415
    const/16 v3, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 700416
    const/16 v3, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 700417
    const/16 v3, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v5}, LX/186;->b(II)V

    .line 700418
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 700419
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 700420
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 700421
    if-eqz v0, :cond_0

    .line 700422
    const-string v1, "action"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 700423
    invoke-static {p0, v0, p2}, LX/4PP;->a(LX/15i;ILX/0nX;)V

    .line 700424
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 700425
    if-eqz v0, :cond_1

    .line 700426
    const-string v1, "cache_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 700427
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 700428
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 700429
    if-eqz v0, :cond_2

    .line 700430
    const-string v1, "close_label"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 700431
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 700432
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 700433
    if-eqz v0, :cond_3

    .line 700434
    const-string v1, "content"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 700435
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 700436
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 700437
    if-eqz v0, :cond_4

    .line 700438
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 700439
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 700440
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 700441
    if-eqz v0, :cond_5

    .line 700442
    const-string v1, "image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 700443
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 700444
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 700445
    if-eqz v0, :cond_6

    .line 700446
    const-string v1, "image_action"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 700447
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 700448
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 700449
    if-eqz v0, :cond_7

    .line 700450
    const-string v1, "is_persistent"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 700451
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 700452
    :cond_7
    invoke-virtual {p0, p1, v3, v2}, LX/15i;->a(IIS)S

    move-result v0

    .line 700453
    if-eqz v0, :cond_8

    .line 700454
    const-string v0, "location"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 700455
    const-class v0, Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;

    invoke-virtual {p0, p1, v3, v0}, LX/15i;->a(IILjava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLMegaphoneLocation;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 700456
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 700457
    if-eqz v0, :cond_9

    .line 700458
    const-string v1, "social_context"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 700459
    invoke-static {p0, v0, p2, p3}, LX/2aE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 700460
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 700461
    if-eqz v0, :cond_a

    .line 700462
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 700463
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 700464
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 700465
    if-eqz v0, :cond_b

    .line 700466
    const-string v1, "tracking"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 700467
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 700468
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 700469
    if-eqz v0, :cond_c

    .line 700470
    const-string v1, "ui_version"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 700471
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 700472
    :cond_c
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 700473
    return-void
.end method
