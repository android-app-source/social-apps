.class public LX/3gO;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static final a:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static e:LX/0Xm;


# instance fields
.field public final c:LX/3gP;

.field public final d:LX/1xu;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 624361
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0, v3}, Landroid/util/SparseArray;-><init>(I)V

    .line 624362
    sput-object v0, LX/3gO;->a:Landroid/util/SparseArray;

    const v1, 0x7f0d0081

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 624363
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0, v3}, Landroid/util/SparseArray;-><init>(I)V

    .line 624364
    sput-object v0, LX/3gO;->b:Landroid/util/SparseArray;

    const v1, 0x7f0d0081

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 624365
    return-void
.end method

.method public constructor <init>(LX/3gP;LX/1xu;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 624366
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 624367
    iput-object p1, p0, LX/3gO;->c:LX/3gP;

    .line 624368
    iput-object p2, p0, LX/3gO;->d:LX/1xu;

    .line 624369
    return-void
.end method

.method public static a(LX/0QB;)LX/3gO;
    .locals 5

    .prologue
    .line 624370
    const-class v1, LX/3gO;

    monitor-enter v1

    .line 624371
    :try_start_0
    sget-object v0, LX/3gO;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 624372
    sput-object v2, LX/3gO;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 624373
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 624374
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 624375
    new-instance p0, LX/3gO;

    invoke-static {v0}, LX/3gP;->a(LX/0QB;)LX/3gP;

    move-result-object v3

    check-cast v3, LX/3gP;

    invoke-static {v0}, LX/1xu;->a(LX/0QB;)LX/1xu;

    move-result-object v4

    check-cast v4, LX/1xu;

    invoke-direct {p0, v3, v4}, LX/3gO;-><init>(LX/3gP;LX/1xu;)V

    .line 624376
    move-object v0, p0

    .line 624377
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 624378
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/3gO;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 624379
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 624380
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
