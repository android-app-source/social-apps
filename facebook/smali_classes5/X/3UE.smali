.class public LX/3UE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Tl;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/3Tl",
        "<",
        "Lcom/facebook/friends/model/FriendRequest;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/3TK;

.field public final b:LX/0xW;

.field public final c:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final d:LX/17W;

.field private final e:LX/3UI;

.field public final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Landroid/content/res/Resources;

.field public final h:LX/0hL;

.field public final i:LX/3Tg;

.field public final j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/friends/model/FriendRequest;",
            ">;"
        }
    .end annotation
.end field

.field public final k:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public l:LX/3UG;

.field public m:Z


# direct methods
.method public constructor <init>(LX/3TK;LX/0xW;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/17W;LX/3UF;LX/0Or;Landroid/content/res/Resources;LX/0hL;LX/3Tg;)V
    .locals 2
    .param p6    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .param p9    # LX/3Tg;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3TK;",
            "LX/0xW;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/17W;",
            "LX/3UF;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Landroid/content/res/Resources;",
            "LX/0hL;",
            "LX/3Tg;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 585586
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 585587
    sget-object v0, LX/3UG;->LOADING:LX/3UG;

    iput-object v0, p0, LX/3UE;->l:LX/3UG;

    .line 585588
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/3UE;->m:Z

    .line 585589
    iput-object p9, p0, LX/3UE;->i:LX/3Tg;

    .line 585590
    iput-object p3, p0, LX/3UE;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 585591
    iput-object p4, p0, LX/3UE;->d:LX/17W;

    .line 585592
    sget-object v0, LX/2h7;->JEWEL:LX/2h7;

    new-instance v1, LX/3UH;

    invoke-direct {v1, p0}, LX/3UH;-><init>(LX/3UE;)V

    invoke-virtual {p5, v0, v1}, LX/3UF;->a(LX/2h7;LX/3UH;)LX/3UI;

    move-result-object v0

    iput-object v0, p0, LX/3UE;->e:LX/3UI;

    .line 585593
    iput-object p1, p0, LX/3UE;->a:LX/3TK;

    .line 585594
    iput-object p2, p0, LX/3UE;->b:LX/0xW;

    .line 585595
    iput-object p7, p0, LX/3UE;->g:Landroid/content/res/Resources;

    .line 585596
    iput-object p8, p0, LX/3UE;->h:LX/0hL;

    .line 585597
    iput-object p6, p0, LX/3UE;->f:LX/0Or;

    .line 585598
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/3UE;->j:Ljava/util/List;

    .line 585599
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/3UE;->k:Ljava/util/Set;

    .line 585600
    return-void
.end method

.method private a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 585601
    sget-object v0, LX/3dY;->a:[I

    invoke-static {p0}, LX/3UE;->p(LX/3UE;)LX/3cV;

    move-result-object v1

    invoke-virtual {v1}, LX/3cV;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 585602
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unexpected view type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 585603
    :pswitch_0
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03066a

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 585604
    iget-object v1, p0, LX/3UE;->b:LX/0xW;

    invoke-virtual {v1}, LX/0xW;->r()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 585605
    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 585606
    :cond_0
    iget-object v1, v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    move-object v1, v1

    .line 585607
    check-cast v1, Landroid/widget/ImageView;

    .line 585608
    iget-object v2, p0, LX/3UE;->h:LX/0hL;

    const p1, 0x7f0207eb

    invoke-virtual {v2, p1}, LX/0hL;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 585609
    move-object v0, v0

    .line 585610
    :goto_0
    return-object v0

    .line 585611
    :pswitch_1
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0306ec

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 585612
    const v1, 0x7f0d1292

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, LX/3dZ;

    invoke-direct {v2, p0}, LX/3dZ;-><init>(LX/3UE;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 585613
    move-object v0, v0

    .line 585614
    goto :goto_0

    .line 585615
    :pswitch_2
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0306ed

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    move-object v0, v0

    .line 585616
    goto :goto_0

    .line 585617
    :pswitch_3
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030c2f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    move-object v0, v0

    .line 585618
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private a(ILandroid/view/View;)V
    .locals 6

    .prologue
    .line 585619
    sget-object v0, LX/3dY;->a:[I

    invoke-static {p0}, LX/3UE;->p(LX/3UE;)LX/3cV;

    move-result-object v1

    invoke-virtual {v1}, LX/3cV;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 585620
    :goto_0
    :pswitch_0
    return-void

    .line 585621
    :pswitch_1
    check-cast p2, Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 585622
    const v1, 0x7f083937

    .line 585623
    sget-object v0, LX/5P0;->SUGGESTIONS:LX/5P0;

    .line 585624
    iget-boolean v2, p0, LX/3UE;->m:Z

    if-nez v2, :cond_1

    .line 585625
    iget-object v0, p0, LX/3UE;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v1, 0x0

    .line 585626
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 585627
    :cond_0
    :goto_1
    move v0, v1

    .line 585628
    if-eqz v0, :cond_4

    .line 585629
    const v1, 0x7f083939

    .line 585630
    sget-object v0, LX/5P0;->CONTACTS:LX/5P0;

    .line 585631
    :cond_1
    :goto_2
    invoke-virtual {p2, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(I)V

    .line 585632
    new-instance v1, LX/Iw1;

    invoke-direct {v1, p0, v0}, LX/Iw1;-><init>(LX/3UE;LX/5P0;)V

    move-object v0, v1

    .line 585633
    invoke-virtual {p2, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 585634
    goto :goto_0

    .line 585635
    :pswitch_2
    invoke-static {p0, p1}, LX/3UE;->c(LX/3UE;I)Lcom/facebook/friends/model/FriendRequest;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friends/model/FriendRequest;

    .line 585636
    iget-object v1, p0, LX/3UE;->e:LX/3UI;

    check-cast p2, Lcom/facebook/friending/common/list/FriendRequestItemView;

    .line 585637
    invoke-virtual {v0}, Lcom/facebook/friends/model/FriendRequest;->b()Ljava/lang/String;

    move-result-object v2

    .line 585638
    const/4 p1, 0x0

    .line 585639
    invoke-virtual {v0}, Lcom/facebook/friends/model/FriendRequest;->k()Z

    move-result v4

    .line 585640
    iget-object v3, v1, LX/3UI;->d:LX/2nY;

    .line 585641
    iget-object v5, v0, Lcom/facebook/friends/model/FriendRequest;->j:LX/2lu;

    move-object v5, v5

    .line 585642
    invoke-virtual {v3, v5, v4}, LX/2nY;->a(LX/2lu;Z)Ljava/lang/String;

    move-result-object v3

    .line 585643
    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 585644
    if-eqz v4, :cond_6

    .line 585645
    iget-object v3, v0, Lcom/facebook/friends/model/FriendRequest;->g:LX/0Px;

    move-object v3, v3

    .line 585646
    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_6

    .line 585647
    iget-object v3, v1, LX/3UI;->e:Landroid/content/res/Resources;

    const v4, 0x7f081fe6

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    .line 585648
    iget-object p0, v0, Lcom/facebook/friends/model/FriendRequest;->g:LX/0Px;

    move-object p0, p0

    .line 585649
    invoke-virtual {p0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object p0

    aput-object p0, v5, p1

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 585650
    :cond_2
    :goto_3
    if-nez v3, :cond_3

    const-string v3, ""

    :cond_3
    move-object v3, v3

    .line 585651
    invoke-virtual {v0}, Lcom/facebook/friends/model/FriendRequest;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v4}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Ljava/lang/String;)V

    .line 585652
    invoke-virtual {p2, v2}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 585653
    invoke-virtual {p2, v3}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 585654
    const-string v4, "%s %s"

    invoke-static {v4, v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Lcom/facebook/friending/common/list/FriendRequestItemView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 585655
    sget-object v2, LX/EzL;->a:[I

    .line 585656
    iget-object v3, v0, Lcom/facebook/friends/model/FriendRequest;->j:LX/2lu;

    move-object v3, v3

    .line 585657
    invoke-virtual {v3}, LX/2lu;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_1

    .line 585658
    iget-boolean v2, v0, Lcom/facebook/friends/model/FriendRequest;->h:Z

    move v2, v2

    .line 585659
    if-eqz v2, :cond_7

    const v2, 0x7f0a004f

    .line 585660
    :goto_4
    invoke-virtual {p2, v2}, Lcom/facebook/friending/common/list/FriendRequestItemView;->setBackgroundResource(I)V

    .line 585661
    sget-object v2, LX/2lu;->NEEDS_RESPONSE:LX/2lu;

    .line 585662
    iget-object v3, v0, Lcom/facebook/friends/model/FriendRequest;->j:LX/2lu;

    move-object v3, v3

    .line 585663
    invoke-virtual {v2, v3}, LX/2lu;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    .line 585664
    const/4 v2, 0x0

    invoke-virtual {p2, v2}, Lcom/facebook/friending/common/list/FriendRequestItemView;->setFriendRequestButtonsVisible(Z)V

    .line 585665
    :goto_5
    goto/16 :goto_0

    .line 585666
    :cond_4
    const v1, 0x7f083938

    .line 585667
    sget-object v0, LX/5P0;->SEARCH:LX/5P0;

    goto/16 :goto_2

    :cond_5
    iget-object v2, p0, LX/3UE;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/1nR;->a(Ljava/lang/String;)LX/0Tn;

    move-result-object p1

    invoke-interface {v2, p1, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    goto/16 :goto_1

    .line 585668
    :cond_6
    invoke-virtual {v0}, Lcom/facebook/friends/model/FriendRequest;->h()Ljava/lang/String;

    move-result-object v3

    goto :goto_3

    .line 585669
    :pswitch_3
    const v2, 0x7f0a08a2

    goto :goto_4

    .line 585670
    :pswitch_4
    const v2, 0x7f0a08a3

    goto :goto_4

    .line 585671
    :cond_7
    const v2, 0x7f020b67

    goto :goto_4

    .line 585672
    :cond_8
    const/4 v2, 0x1

    invoke-virtual {p2, v2}, Lcom/facebook/friending/common/list/FriendRequestItemView;->setFriendRequestButtonsVisible(Z)V

    .line 585673
    invoke-virtual {v0}, Lcom/facebook/friends/model/FriendRequest;->k()Z

    move-result v2

    .line 585674
    iget-object v3, v1, LX/3UI;->d:LX/2nY;

    invoke-virtual {v3, v2}, LX/2nY;->a(Z)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, LX/3UI;->a(LX/3UI;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v3

    iget-object v4, v1, LX/3UI;->d:LX/2nY;

    invoke-virtual {v4, v2}, LX/2nY;->d(Z)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, LX/3UI;->a(LX/3UI;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {p2, v3, v4}, Lcom/facebook/friending/common/list/FriendRequestItemView;->b(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 585675
    iget-object v3, v1, LX/3UI;->d:LX/2nY;

    invoke-virtual {v3, v2}, LX/2nY;->e(Z)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/3UI;->a(LX/3UI;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {p2, v2}, Lcom/facebook/friending/common/list/FriendRequestItemView;->setNegativeButtonText(Ljava/lang/CharSequence;)V

    .line 585676
    new-instance v2, LX/EzH;

    invoke-direct {v2, v1, v0}, LX/EzH;-><init>(LX/3UI;Lcom/facebook/friends/model/FriendRequest;)V

    invoke-virtual {p2, v2}, Lcom/facebook/friending/common/list/FriendRequestItemView;->setPositiveButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 585677
    new-instance v2, LX/EzI;

    invoke-direct {v2, v1, v0}, LX/EzI;-><init>(LX/3UI;Lcom/facebook/friends/model/FriendRequest;)V

    invoke-virtual {p2, v2}, Lcom/facebook/friending/common/list/FriendRequestItemView;->setNegativeButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_5

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private static c(LX/3UE;I)Lcom/facebook/friends/model/FriendRequest;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 585678
    iget-object v0, p0, LX/3UE;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/3UE;->j:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friends/model/FriendRequest;

    goto :goto_0
.end method

.method public static d(LX/3UE;J)I
    .locals 5

    .prologue
    .line 585679
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/3UE;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 585680
    iget-object v0, p0, LX/3UE;->j:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friends/model/FriendRequest;

    invoke-virtual {v0}, Lcom/facebook/friends/model/FriendRequest;->a()J

    move-result-wide v2

    cmp-long v0, v2, p1

    if-nez v0, :cond_0

    .line 585681
    :goto_1
    return v1

    .line 585682
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 585683
    :cond_1
    const/4 v1, -0x1

    goto :goto_1
.end method

.method private static p(LX/3UE;)LX/3cV;
    .locals 2

    .prologue
    .line 585697
    iget-object v0, p0, LX/3UE;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 585698
    iget-object v0, p0, LX/3UE;->l:LX/3UG;

    sget-object v1, LX/3UG;->SUCCESS:LX/3UG;

    if-eq v0, v1, :cond_0

    .line 585699
    sget-object v0, LX/3cV;->FRIEND_REQUEST_ERROR_VIEW:LX/3cV;

    .line 585700
    :goto_0
    return-object v0

    .line 585701
    :cond_0
    invoke-direct {p0}, LX/3UE;->q()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 585702
    sget-object v0, LX/3cV;->FIND_FRIENDS:LX/3cV;

    goto :goto_0

    .line 585703
    :cond_1
    sget-object v0, LX/3cV;->FRIEND_REQUEST_EMPTY_VIEW:LX/3cV;

    goto :goto_0

    .line 585704
    :cond_2
    sget-object v0, LX/3cV;->FRIEND_REQUEST:LX/3cV;

    goto :goto_0
.end method

.method private q()Z
    .locals 1

    .prologue
    .line 585684
    iget-object v0, p0, LX/3UE;->b:LX/0xW;

    invoke-virtual {v0}, LX/0xW;->s()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(I)I
    .locals 1

    .prologue
    .line 585685
    invoke-static {p0}, LX/3UE;->p(LX/3UE;)LX/3cV;

    move-result-object v0

    invoke-virtual {v0}, LX/3cV;->ordinal()I

    move-result v0

    return v0
.end method

.method public final a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 0

    .prologue
    .line 585686
    if-nez p2, :cond_0

    .line 585687
    invoke-direct {p0, p1, p3}, LX/3UE;->a(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 585688
    :cond_0
    invoke-direct {p0, p1, p2}, LX/3UE;->a(ILandroid/view/View;)V

    .line 585689
    return-object p2
.end method

.method public final a(J)V
    .locals 3

    .prologue
    .line 585690
    iget-object v0, p0, LX/3UE;->k:Ljava/util/Set;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 585691
    :cond_0
    :goto_0
    return-void

    .line 585692
    :cond_1
    invoke-static {p0, p1, p2}, LX/3UE;->d(LX/3UE;J)I

    move-result v0

    .line 585693
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 585694
    iget-object v1, p0, LX/3UE;->j:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 585695
    iget-object v0, p0, LX/3UE;->k:Ljava/util/Set;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 585696
    iget-object v0, p0, LX/3UE;->i:LX/3Tg;

    invoke-interface {v0}, LX/3Tg;->e()V

    goto :goto_0
.end method

.method public final synthetic b(I)Ljava/lang/Object;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 585584
    invoke-static {p0, p1}, LX/3UE;->c(LX/3UE;I)Lcom/facebook/friends/model/FriendRequest;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 585585
    sget-object v0, LX/3UG;->LOADING:LX/3UG;

    iget-object v1, p0, LX/3UE;->l:LX/3UG;

    invoke-virtual {v0, v1}, LX/3UG;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(J)Z
    .locals 3

    .prologue
    .line 585566
    iget-object v0, p0, LX/3UE;->k:Ljava/util/Set;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 585567
    iget-object v0, p0, LX/3UE;->b:LX/0xW;

    invoke-virtual {v0}, LX/0xW;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 585568
    iget-object v0, p0, LX/3UE;->g:Landroid/content/res/Resources;

    const v1, 0x7f080faa

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 585569
    :goto_0
    return-object v0

    .line 585570
    :cond_0
    iget-object v0, p0, LX/3UE;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/3UE;->l:LX/3UG;

    sget-object v1, LX/3UG;->SUCCESS:LX/3UG;

    if-ne v0, v1, :cond_1

    invoke-direct {p0}, LX/3UE;->q()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 585571
    :goto_1
    iget-object v1, p0, LX/3UE;->g:Landroid/content/res/Resources;

    if-eqz v0, :cond_2

    const v0, 0x7f080f71

    :goto_2
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 585572
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 585573
    :cond_2
    const v0, 0x7f080f70

    goto :goto_2
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 585574
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 585575
    const/4 v0, 0x0

    return-object v0
.end method

.method public final f()Landroid/view/View$OnClickListener;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 585576
    const/4 v0, 0x0

    return-object v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 585577
    iget-object v0, p0, LX/3UE;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/3UE;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 585578
    iget-object v0, p0, LX/3UE;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()I
    .locals 1

    .prologue
    .line 585579
    invoke-static {}, LX/3cV;->values()[LX/3cV;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 585580
    iget-object v0, p0, LX/3UE;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()LX/3UN;
    .locals 1

    .prologue
    .line 585581
    sget-object v0, LX/3UN;->SEE_ALL_FOOTER:LX/3UN;

    return-object v0
.end method

.method public final l()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 585582
    new-instance v0, LX/Iw0;

    invoke-direct {v0, p0}, LX/Iw0;-><init>(LX/3UE;)V

    return-object v0
.end method

.method public final o()Z
    .locals 2

    .prologue
    .line 585583
    iget-object v0, p0, LX/3UE;->l:LX/3UG;

    sget-object v1, LX/3UG;->SUCCESS:LX/3UG;

    invoke-virtual {v0, v1}, LX/3UG;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
