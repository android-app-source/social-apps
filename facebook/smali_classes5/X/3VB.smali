.class public LX/3VB;
.super LX/2ee;
.source ""

# interfaces
.implements LX/3V9;


# static fields
.field public static final a:LX/1Cz;


# instance fields
.field private final c:Landroid/widget/ImageView;

.field private final d:Landroid/widget/ImageView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 587570
    new-instance v0, LX/3VC;

    invoke-direct {v0}, LX/3VC;-><init>()V

    sput-object v0, LX/3VB;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 587571
    const v0, 0x7f031496

    invoke-direct {p0, p1, v0}, LX/2ee;-><init>(Landroid/content/Context;I)V

    .line 587572
    const v0, 0x7f0d0bde

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/3VB;->c:Landroid/widget/ImageView;

    .line 587573
    const v0, 0x7f0d1614

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/3VB;->d:Landroid/widget/ImageView;

    .line 587574
    return-void
.end method


# virtual methods
.method public final a_(LX/0hs;)V
    .locals 1

    .prologue
    .line 587575
    iget-object v0, p0, LX/3VB;->d:Landroid/widget/ImageView;

    invoke-virtual {p1, v0}, LX/0ht;->f(Landroid/view/View;)V

    .line 587576
    return-void
.end method

.method public setMenuButtonActive(Z)V
    .locals 2

    .prologue
    .line 587577
    iget-object v1, p0, LX/3VB;->c:Landroid/widget/ImageView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 587578
    return-void

    .line 587579
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method
