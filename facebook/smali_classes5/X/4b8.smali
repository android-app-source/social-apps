.class public LX/4b8;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/4b8;


# instance fields
.field private final a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/http/common/FbHttpRequestProcessor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Or",
            "<",
            "Lcom/facebook/http/common/FbHttpRequestProcessor;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 793446
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 793447
    iput-object p1, p0, LX/4b8;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 793448
    iput-object p2, p0, LX/4b8;->b:LX/0Or;

    .line 793449
    return-void
.end method

.method public static a(LX/0QB;)LX/4b8;
    .locals 5

    .prologue
    .line 793433
    sget-object v0, LX/4b8;->c:LX/4b8;

    if-nez v0, :cond_1

    .line 793434
    const-class v1, LX/4b8;

    monitor-enter v1

    .line 793435
    :try_start_0
    sget-object v0, LX/4b8;->c:LX/4b8;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 793436
    if-eqz v2, :cond_0

    .line 793437
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 793438
    new-instance v4, LX/4b8;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 p0, 0xb45

    invoke-static {v0, p0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v4, v3, p0}, LX/4b8;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;)V

    .line 793439
    move-object v0, v4

    .line 793440
    sput-object v0, LX/4b8;->c:LX/4b8;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 793441
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 793442
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 793443
    :cond_1
    sget-object v0, LX/4b8;->c:LX/4b8;

    return-object v0

    .line 793444
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 793445
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(LX/4b7;LX/15D;Lcom/facebook/http/interfaces/RequestPriority;Ljava/lang/Throwable;)V
    .locals 9
    .param p3    # Lcom/facebook/http/interfaces/RequestPriority;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/Throwable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4b7;",
            "LX/15D",
            "<*>;",
            "Lcom/facebook/http/interfaces/RequestPriority;",
            "Ljava/lang/Throwable;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/high16 v4, 0x447a0000    # 1000.0f

    .line 793376
    invoke-static {p0}, LX/4b8;->b(LX/4b8;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 793377
    :goto_0
    return-void

    .line 793378
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 793379
    invoke-virtual {p2}, LX/15D;->h()Lcom/facebook/http/interfaces/RequestPriority;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/http/common/FbHttpUtils;->a(Lcom/facebook/http/interfaces/RequestPriority;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 793380
    sget-object v1, LX/4b7;->PRIORITY:LX/4b7;

    if-ne p1, v1, :cond_3

    .line 793381
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 793382
    const-string v1, "->"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 793383
    invoke-static {p3}, Lcom/facebook/http/common/FbHttpUtils;->a(Lcom/facebook/http/interfaces/RequestPriority;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 793384
    :goto_1
    const/4 v1, 0x5

    invoke-static {v0, v1}, LX/4b8;->a(Ljava/lang/StringBuilder;I)V

    .line 793385
    iget v1, p2, LX/15D;->m:I

    move v1, v1

    .line 793386
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 793387
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 793388
    const/16 v1, 0xc

    invoke-static {v0, v1}, LX/4b8;->a(Ljava/lang/StringBuilder;I)V

    .line 793389
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 793390
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 793391
    const/16 v1, 0x16

    invoke-static {v0, v1}, LX/4b8;->a(Ljava/lang/StringBuilder;I)V

    .line 793392
    iget-object v1, p2, LX/15D;->c:Ljava/lang/String;

    move-object v1, v1

    .line 793393
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 793394
    const-string v1, "  ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 793395
    invoke-static {p2}, Lcom/facebook/http/common/FbHttpUtils;->b(LX/15D;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 793396
    const-string v1, ") "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 793397
    const/16 v1, 0x5a

    invoke-static {v0, v1}, LX/4b8;->a(Ljava/lang/StringBuilder;I)V

    .line 793398
    sget-object v1, LX/4b6;->a:[I

    invoke-virtual {p1}, LX/4b7;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 793399
    iget-object v1, p2, LX/15D;->b:Lorg/apache/http/client/methods/HttpUriRequest;

    move-object v1, v1

    .line 793400
    if-eqz v1, :cond_2

    invoke-interface {v1}, Lorg/apache/http/client/methods/HttpUriRequest;->getURI()Ljava/net/URI;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 793401
    invoke-interface {v1}, Lorg/apache/http/client/methods/HttpUriRequest;->getURI()Ljava/net/URI;

    move-result-object v1

    .line 793402
    invoke-virtual {v1}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 793403
    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 793404
    invoke-virtual {v1}, Ljava/net/URI;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 793405
    const-string v2, ".jpg"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, ".png"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, ".gif"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, ".mp4"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 793406
    :cond_1
    const/16 v2, 0x2f

    invoke-virtual {v1, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 793407
    :cond_2
    :goto_2
    const/16 v3, 0xb

    .line 793408
    iget-object v0, p0, LX/4b8;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-virtual {v0}, Lcom/facebook/http/common/FbHttpRequestProcessor;->a()LX/2Ee;

    move-result-object v0

    .line 793409
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 793410
    invoke-static {v1, v3}, LX/4b8;->a(Ljava/lang/StringBuilder;I)V

    .line 793411
    const-string v2, "RUNNING: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 793412
    iget-object v2, v0, LX/2Ee;->a:Ljava/util/ArrayList;

    move-object v2, v2

    .line 793413
    invoke-static {v2, v1}, LX/4b8;->a(Ljava/util/ArrayList;Ljava/lang/StringBuilder;)V

    .line 793414
    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 793415
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 793416
    invoke-static {v1, v3}, LX/4b8;->a(Ljava/lang/StringBuilder;I)V

    .line 793417
    const-string v2, "WAITING: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 793418
    iget-object v2, v0, LX/2Ee;->b:Ljava/util/ArrayList;

    move-object v0, v2

    .line 793419
    invoke-static {v0, v1}, LX/4b8;->a(Ljava/util/ArrayList;Ljava/lang/StringBuilder;)V

    .line 793420
    const-string v0, "\n"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 793421
    goto/16 :goto_0

    .line 793422
    :cond_3
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    .line 793423
    :pswitch_0
    invoke-static {}, LX/4b8;->c()J

    move-result-wide v2

    long-to-float v1, v2

    .line 793424
    iget-wide v7, p2, LX/15D;->n:J

    move-wide v2, v7

    .line 793425
    long-to-float v2, v2

    sub-float/2addr v1, v2

    .line 793426
    const-string v2, "%.3fs (Queue)"

    new-array v3, v6, [Ljava/lang/Object;

    div-float/2addr v1, v4

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    aput-object v1, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 793427
    :pswitch_1
    invoke-static {}, LX/4b8;->c()J

    move-result-wide v2

    long-to-float v1, v2

    .line 793428
    iget-wide v7, p2, LX/15D;->n:J

    move-wide v2, v7

    .line 793429
    long-to-float v2, v2

    sub-float/2addr v1, v2

    .line 793430
    const-string v2, "%.3fs (Total)"

    new-array v3, v6, [Ljava/lang/Object;

    div-float/2addr v1, v4

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    aput-object v1, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 793431
    :pswitch_2
    invoke-static {p4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 793432
    invoke-virtual {p4}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(LX/4b8;LX/4b7;LX/15D;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4b7;",
            "LX/15D",
            "<*>;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 793374
    invoke-direct {p0, p1, p2, v0, v0}, LX/4b8;->a(LX/4b7;LX/15D;Lcom/facebook/http/interfaces/RequestPriority;Ljava/lang/Throwable;)V

    .line 793375
    return-void
.end method

.method public static a(Ljava/lang/StringBuilder;I)V
    .locals 3

    .prologue
    .line 793450
    invoke-virtual {p0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    .line 793451
    const/4 v0, 0x0

    :goto_0
    sub-int v2, p1, v1

    if-ge v0, v2, :cond_0

    .line 793452
    const-string v2, " "

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 793453
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 793454
    :cond_0
    return-void
.end method

.method public static a(Ljava/util/ArrayList;Ljava/lang/StringBuilder;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "LX/15D",
            "<*>;>;",
            "Ljava/lang/StringBuilder;",
            ")V"
        }
    .end annotation

    .prologue
    .line 793362
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 793363
    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/15D;

    .line 793364
    iget v2, v0, LX/15D;->m:I

    move v2, v2

    .line 793365
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 793366
    const/16 v2, 0x28

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 793367
    invoke-virtual {v0}, LX/15D;->h()Lcom/facebook/http/interfaces/RequestPriority;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/http/common/FbHttpUtils;->a(Lcom/facebook/http/interfaces/RequestPriority;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 793368
    iget-boolean v2, v0, LX/15D;->o:Z

    move v0, v2

    .line 793369
    if-eqz v0, :cond_0

    .line 793370
    const-string v0, "*"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 793371
    :cond_0
    const-string v0, ") "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 793372
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 793373
    :cond_1
    return-void
.end method

.method public static a$redex0(LX/4b8;LX/15D;Ljava/lang/Throwable;)V
    .locals 2
    .param p1    # LX/15D;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 793356
    if-nez p2, :cond_0

    .line 793357
    sget-object v0, LX/4b7;->FINISHED:LX/4b7;

    invoke-static {p0, v0, p1}, LX/4b8;->a(LX/4b8;LX/4b7;LX/15D;)V

    .line 793358
    :goto_0
    return-void

    .line 793359
    :cond_0
    instance-of v0, p2, Ljava/util/concurrent/CancellationException;

    if-eqz v0, :cond_1

    .line 793360
    sget-object v0, LX/4b7;->CANCELLED:LX/4b7;

    invoke-static {p0, v0, p1}, LX/4b8;->a(LX/4b8;LX/4b7;LX/15D;)V

    goto :goto_0

    .line 793361
    :cond_1
    sget-object v0, LX/4b7;->FAILED:LX/4b7;

    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, v1, p2}, LX/4b8;->a(LX/4b7;LX/15D;Lcom/facebook/http/interfaces/RequestPriority;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static b(LX/4b8;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 793355
    const/4 v1, 0x3

    invoke-static {v1}, LX/01m;->b(I)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/4b8;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/0dU;->h:LX/0Tn;

    invoke-interface {v1, v2, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private static c()J
    .locals 2

    .prologue
    .line 793354
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method public final a(LX/15D;Lcom/facebook/http/interfaces/RequestPriority;)V
    .locals 2

    .prologue
    .line 793352
    sget-object v0, LX/4b7;->PRIORITY:LX/4b7;

    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, p2, v1}, LX/4b8;->a(LX/4b7;LX/15D;Lcom/facebook/http/interfaces/RequestPriority;Ljava/lang/Throwable;)V

    .line 793353
    return-void
.end method
