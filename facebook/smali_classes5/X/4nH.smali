.class public final LX/4nH;
.super LX/45a;
.source ""


# instance fields
.field public final synthetic a:Ljava/io/File;

.field public final synthetic b:LX/3DZ;


# direct methods
.method public constructor <init>(LX/3DZ;Ljava/io/OutputStream;Ljava/io/File;)V
    .locals 0

    .prologue
    .line 806694
    iput-object p1, p0, LX/4nH;->b:LX/3DZ;

    iput-object p3, p0, LX/4nH;->a:Ljava/io/File;

    invoke-direct {p0, p2}, LX/45a;-><init>(Ljava/io/OutputStream;)V

    return-void
.end method


# virtual methods
.method public final close()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 806688
    :try_start_0
    invoke-super {p0}, LX/45a;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 806689
    iget-object v0, p0, LX/4nH;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 806690
    iget-object v0, p0, LX/4nH;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 806691
    :cond_0
    return-void

    .line 806692
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/4nH;->a:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v2

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 806693
    iget-object v1, p0, LX/4nH;->a:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    :cond_1
    throw v0
.end method
