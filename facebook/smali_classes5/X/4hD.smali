.class public final enum LX/4hD;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/4hD;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/4hD;

.field public static final enum INVALID_COUNTRY_CODE:LX/4hD;

.field public static final enum NOT_A_NUMBER:LX/4hD;

.field public static final enum TOO_LONG:LX/4hD;

.field public static final enum TOO_SHORT_AFTER_IDD:LX/4hD;

.field public static final enum TOO_SHORT_NSN:LX/4hD;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 800807
    new-instance v0, LX/4hD;

    const-string v1, "INVALID_COUNTRY_CODE"

    invoke-direct {v0, v1, v2}, LX/4hD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4hD;->INVALID_COUNTRY_CODE:LX/4hD;

    .line 800808
    new-instance v0, LX/4hD;

    const-string v1, "NOT_A_NUMBER"

    invoke-direct {v0, v1, v3}, LX/4hD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4hD;->NOT_A_NUMBER:LX/4hD;

    .line 800809
    new-instance v0, LX/4hD;

    const-string v1, "TOO_SHORT_AFTER_IDD"

    invoke-direct {v0, v1, v4}, LX/4hD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4hD;->TOO_SHORT_AFTER_IDD:LX/4hD;

    .line 800810
    new-instance v0, LX/4hD;

    const-string v1, "TOO_SHORT_NSN"

    invoke-direct {v0, v1, v5}, LX/4hD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4hD;->TOO_SHORT_NSN:LX/4hD;

    .line 800811
    new-instance v0, LX/4hD;

    const-string v1, "TOO_LONG"

    invoke-direct {v0, v1, v6}, LX/4hD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/4hD;->TOO_LONG:LX/4hD;

    .line 800812
    const/4 v0, 0x5

    new-array v0, v0, [LX/4hD;

    sget-object v1, LX/4hD;->INVALID_COUNTRY_CODE:LX/4hD;

    aput-object v1, v0, v2

    sget-object v1, LX/4hD;->NOT_A_NUMBER:LX/4hD;

    aput-object v1, v0, v3

    sget-object v1, LX/4hD;->TOO_SHORT_AFTER_IDD:LX/4hD;

    aput-object v1, v0, v4

    sget-object v1, LX/4hD;->TOO_SHORT_NSN:LX/4hD;

    aput-object v1, v0, v5

    sget-object v1, LX/4hD;->TOO_LONG:LX/4hD;

    aput-object v1, v0, v6

    sput-object v0, LX/4hD;->$VALUES:[LX/4hD;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 800806
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/4hD;
    .locals 1

    .prologue
    .line 800813
    const-class v0, LX/4hD;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/4hD;

    return-object v0
.end method

.method public static values()[LX/4hD;
    .locals 1

    .prologue
    .line 800805
    sget-object v0, LX/4hD;->$VALUES:[LX/4hD;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/4hD;

    return-object v0
.end method
