.class public LX/3ky;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/16E;
.implements LX/0i1;


# instance fields
.field private a:Landroid/content/Context;

.field private b:LX/0ad;

.field private c:LX/1Bf;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0ad;LX/1Bf;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 633417
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 633418
    iput-object p1, p0, LX/3ky;->a:Landroid/content/Context;

    .line 633419
    iput-object p2, p0, LX/3ky;->b:LX/0ad;

    .line 633420
    iput-object p3, p0, LX/3ky;->c:LX/1Bf;

    .line 633421
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 633416
    const-wide/32 v0, 0x5265c00

    return-wide v0
.end method

.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/10S;
    .locals 3

    .prologue
    .line 633406
    iget-object v0, p0, LX/3ky;->b:LX/0ad;

    sget-short v1, LX/1Bm;->b:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 633407
    sget-object v0, LX/10S;->ELIGIBLE:LX/10S;

    .line 633408
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;

    goto :goto_0
.end method

.method public final a(J)V
    .locals 0

    .prologue
    .line 633415
    return-void
.end method

.method public final a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 633412
    iget-object v0, p0, LX/3ky;->c:LX/1Bf;

    iget-object v1, p0, LX/3ky;->a:Landroid/content/Context;

    .line 633413
    invoke-static {v0}, LX/1Bf;->c(LX/1Bf;)Z

    move-result p0

    invoke-static {v0}, LX/1Bf;->d(LX/1Bf;)Landroid/os/Bundle;

    move-result-object p1

    invoke-static {v1, p0, p1}, LX/049;->g(Landroid/content/Context;ZLandroid/os/Bundle;)V

    .line 633414
    return-void
.end method

.method public final a(Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 633411
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 633410
    const-string v0, "4174"

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 633409
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->BUILT_IN_BROWSER:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
