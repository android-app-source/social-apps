.class public LX/3uz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3us;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field public a:Landroid/content/Context;

.field public b:Landroid/view/LayoutInflater;

.field public c:LX/3v0;

.field public d:Landroid/support/v7/internal/view/menu/ExpandedMenuView;

.field public e:I

.field public f:I

.field public g:LX/3uy;

.field public h:I

.field public i:LX/3uE;


# direct methods
.method public constructor <init>(II)V
    .locals 0

    .prologue
    .line 649967
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 649968
    iput p1, p0, LX/3uz;->f:I

    .line 649969
    iput p2, p0, LX/3uz;->e:I

    .line 649970
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 649963
    const/4 v0, 0x0

    invoke-direct {p0, p2, v0}, LX/3uz;-><init>(II)V

    .line 649964
    iput-object p1, p0, LX/3uz;->a:Landroid/content/Context;

    .line 649965
    iget-object v0, p0, LX/3uz;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, LX/3uz;->b:Landroid/view/LayoutInflater;

    .line 649966
    return-void
.end method


# virtual methods
.method public final a()Landroid/widget/ListAdapter;
    .locals 1

    .prologue
    .line 649960
    iget-object v0, p0, LX/3uz;->g:LX/3uy;

    if-nez v0, :cond_0

    .line 649961
    new-instance v0, LX/3uy;

    invoke-direct {v0, p0}, LX/3uy;-><init>(LX/3uz;)V

    iput-object v0, p0, LX/3uz;->g:LX/3uy;

    .line 649962
    :cond_0
    iget-object v0, p0, LX/3uz;->g:LX/3uy;

    return-object v0
.end method

.method public final a(LX/3v0;Z)V
    .locals 1

    .prologue
    .line 649957
    iget-object v0, p0, LX/3uz;->i:LX/3uE;

    if-eqz v0, :cond_0

    .line 649958
    iget-object v0, p0, LX/3uz;->i:LX/3uE;

    invoke-interface {v0, p1, p2}, LX/3uE;->a(LX/3v0;Z)V

    .line 649959
    :cond_0
    return-void
.end method

.method public final a(Landroid/content/Context;LX/3v0;)V
    .locals 2

    .prologue
    .line 649939
    iget v0, p0, LX/3uz;->e:I

    if-eqz v0, :cond_2

    .line 649940
    new-instance v0, Landroid/view/ContextThemeWrapper;

    iget v1, p0, LX/3uz;->e:I

    invoke-direct {v0, p1, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, LX/3uz;->a:Landroid/content/Context;

    .line 649941
    iget-object v0, p0, LX/3uz;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, LX/3uz;->b:Landroid/view/LayoutInflater;

    .line 649942
    :cond_0
    :goto_0
    iput-object p2, p0, LX/3uz;->c:LX/3v0;

    .line 649943
    iget-object v0, p0, LX/3uz;->g:LX/3uy;

    if-eqz v0, :cond_1

    .line 649944
    iget-object v0, p0, LX/3uz;->g:LX/3uy;

    const v1, -0x4e9c9533

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 649945
    :cond_1
    return-void

    .line 649946
    :cond_2
    iget-object v0, p0, LX/3uz;->a:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 649947
    iput-object p1, p0, LX/3uz;->a:Landroid/content/Context;

    .line 649948
    iget-object v0, p0, LX/3uz;->b:Landroid/view/LayoutInflater;

    if-nez v0, :cond_0

    .line 649949
    iget-object v0, p0, LX/3uz;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, LX/3uz;->b:Landroid/view/LayoutInflater;

    goto :goto_0
.end method

.method public final a(LX/3vG;)Z
    .locals 7

    .prologue
    .line 649971
    invoke-virtual {p1}, LX/3v0;->hasVisibleItems()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 649972
    :goto_0
    return v0

    .line 649973
    :cond_0
    new-instance v0, LX/3v1;

    invoke-direct {v0, p1}, LX/3v1;-><init>(LX/3v0;)V

    const/4 v1, 0x0

    .line 649974
    iget-object v2, v0, LX/3v1;->b:LX/3v0;

    .line 649975
    new-instance v3, Landroid/app/AlertDialog$Builder;

    .line 649976
    iget-object v4, v2, LX/3v0;->e:Landroid/content/Context;

    move-object v4, v4

    .line 649977
    invoke-direct {v3, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 649978
    new-instance v4, LX/3uz;

    const v5, 0x7f03000d

    const v6, 0x7f0e00d2

    invoke-direct {v4, v5, v6}, LX/3uz;-><init>(II)V

    iput-object v4, v0, LX/3v1;->a:LX/3uz;

    .line 649979
    iget-object v4, v0, LX/3v1;->a:LX/3uz;

    .line 649980
    iput-object v0, v4, LX/3uz;->i:LX/3uE;

    .line 649981
    iget-object v4, v0, LX/3v1;->b:LX/3v0;

    iget-object v5, v0, LX/3v1;->a:LX/3uz;

    invoke-virtual {v4, v5}, LX/3v0;->a(LX/3us;)V

    .line 649982
    iget-object v4, v0, LX/3v1;->a:LX/3uz;

    invoke-virtual {v4}, LX/3uz;->a()Landroid/widget/ListAdapter;

    move-result-object v4

    invoke-virtual {v3, v4, v0}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 649983
    iget-object v4, v2, LX/3v0;->c:Landroid/view/View;

    move-object v4, v4

    .line 649984
    if-eqz v4, :cond_3

    .line 649985
    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setCustomTitle(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 649986
    :goto_1
    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 649987
    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    iput-object v2, v0, LX/3v1;->c:Landroid/app/AlertDialog;

    .line 649988
    iget-object v2, v0, LX/3v1;->c:Landroid/app/AlertDialog;

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 649989
    iget-object v2, v0, LX/3v1;->c:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    .line 649990
    const/16 v3, 0x3eb

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 649991
    if-eqz v1, :cond_1

    .line 649992
    iput-object v1, v2, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    .line 649993
    :cond_1
    iget v3, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    const/high16 v4, 0x20000

    or-int/2addr v3, v4

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 649994
    iget-object v2, v0, LX/3v1;->c:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    .line 649995
    iget-object v0, p0, LX/3uz;->i:LX/3uE;

    if-eqz v0, :cond_2

    .line 649996
    iget-object v0, p0, LX/3uz;->i:LX/3uE;

    invoke-interface {v0, p1}, LX/3uE;->a_(LX/3v0;)Z

    .line 649997
    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    .line 649998
    :cond_3
    iget-object v4, v2, LX/3v0;->b:Landroid/graphics/drawable/Drawable;

    move-object v4, v4

    .line 649999
    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    .line 650000
    iget-object v5, v2, LX/3v0;->a:Ljava/lang/CharSequence;

    move-object v2, v5

    .line 650001
    invoke-virtual {v4, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    goto :goto_1
.end method

.method public final b(Z)V
    .locals 2

    .prologue
    .line 649955
    iget-object v0, p0, LX/3uz;->g:LX/3uy;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3uz;->g:LX/3uy;

    const v1, 0x6a210b7b

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 649956
    :cond_0
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 649954
    const/4 v0, 0x0

    return v0
.end method

.method public final b(LX/3v3;)Z
    .locals 1

    .prologue
    .line 649953
    const/4 v0, 0x0

    return v0
.end method

.method public final c(LX/3v3;)Z
    .locals 1

    .prologue
    .line 649952
    const/4 v0, 0x0

    return v0
.end method

.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 649950
    iget-object v0, p0, LX/3uz;->c:LX/3v0;

    iget-object v1, p0, LX/3uz;->g:LX/3uy;

    invoke-virtual {v1, p3}, LX/3uy;->a(I)LX/3v3;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, LX/3v0;->a(Landroid/view/MenuItem;LX/3us;I)Z

    .line 649951
    return-void
.end method
