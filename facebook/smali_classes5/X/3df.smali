.class public LX/3df;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0pQ;


# static fields
.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;

.field public static final c:LX/0Tn;

.field public static final d:LX/0Tn;

.field public static final e:LX/0Tn;

.field public static final f:LX/0Tn;

.field public static final g:LX/0Tn;

.field public static final h:LX/0Tn;

.field public static final i:LX/0Tn;

.field public static final j:LX/0Tn;

.field private static final k:LX/0Tn;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 617292
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "account_confirmation/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 617293
    sput-object v0, LX/3df;->k:LX/0Tn;

    const-string v1, "read_sms_last_scan_time"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3df;->a:LX/0Tn;

    .line 617294
    sget-object v0, LX/3df;->k:LX/0Tn;

    const-string v1, "codes_attempted"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3df;->b:LX/0Tn;

    .line 617295
    sget-object v0, LX/3df;->k:LX/0Tn;

    const-string v1, "codes_to_retry"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3df;->c:LX/0Tn;

    .line 617296
    sget-object v0, LX/3df;->k:LX/0Tn;

    const-string v1, "num_reads"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3df;->d:LX/0Tn;

    .line 617297
    sget-object v0, LX/3df;->k:LX/0Tn;

    const-string v1, "sms_confirmation_reader_pointer"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3df;->e:LX/0Tn;

    .line 617298
    sget-object v0, LX/3df;->k:LX/0Tn;

    const-string v1, "pending_contactpoints/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3df;->f:LX/0Tn;

    .line 617299
    sget-object v0, LX/3df;->k:LX/0Tn;

    const-string v1, "email_oauth_last_attempt_time"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3df;->g:LX/0Tn;

    .line 617300
    sget-object v0, LX/3df;->k:LX/0Tn;

    const-string v1, "email_oauth_attempt_count/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3df;->h:LX/0Tn;

    .line 617301
    sget-object v0, LX/3df;->k:LX/0Tn;

    const-string v1, "cliff_skipped"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3df;->i:LX/0Tn;

    .line 617302
    sget-object v0, LX/3df;->k:LX/0Tn;

    const-string v1, "background_task_first_run"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/3df;->j:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 617289
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;)LX/0Tn;
    .locals 1

    .prologue
    .line 617291
    sget-object v0, LX/3df;->j:LX/0Tn;

    invoke-virtual {v0, p0}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method


# virtual methods
.method public final a()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "LX/0Tn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 617290
    sget-object v0, LX/3df;->j:LX/0Tn;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method
