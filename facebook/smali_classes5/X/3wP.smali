.class public final LX/3wP;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/3wS;",
            ">;"
        }
    .end annotation
.end field

.field public final b:[I

.field public final c:[I

.field public final d:LX/3wN;

.field public final e:I

.field public final f:I

.field public final g:Z


# direct methods
.method public constructor <init>(LX/3wN;Ljava/util/List;[I[IZ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3wN;",
            "Ljava/util/List",
            "<",
            "LX/3wS;",
            ">;[I[IZ)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 655140
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 655141
    iput-object p2, p0, LX/3wP;->a:Ljava/util/List;

    .line 655142
    iput-object p3, p0, LX/3wP;->b:[I

    .line 655143
    iput-object p4, p0, LX/3wP;->c:[I

    .line 655144
    iget-object v0, p0, LX/3wP;->b:[I

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    .line 655145
    iget-object v0, p0, LX/3wP;->c:[I

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    .line 655146
    iput-object p1, p0, LX/3wP;->d:LX/3wN;

    .line 655147
    invoke-virtual {p1}, LX/3wN;->a()I

    move-result v0

    iput v0, p0, LX/3wP;->e:I

    .line 655148
    invoke-virtual {p1}, LX/3wN;->b()I

    move-result v0

    iput v0, p0, LX/3wP;->f:I

    .line 655149
    iput-boolean p5, p0, LX/3wP;->g:Z

    .line 655150
    const/4 p1, 0x0

    .line 655151
    iget-object v0, p0, LX/3wP;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    const/4 v0, 0x0

    .line 655152
    :goto_0
    if-eqz v0, :cond_0

    iget v1, v0, LX/3wS;->a:I

    if-nez v1, :cond_0

    iget v0, v0, LX/3wS;->b:I

    if-eqz v0, :cond_1

    .line 655153
    :cond_0
    new-instance v0, LX/3wS;

    invoke-direct {v0}, LX/3wS;-><init>()V

    .line 655154
    iput p1, v0, LX/3wS;->a:I

    .line 655155
    iput p1, v0, LX/3wS;->b:I

    .line 655156
    iput-boolean p1, v0, LX/3wS;->d:Z

    .line 655157
    iput p1, v0, LX/3wS;->c:I

    .line 655158
    iput-boolean p1, v0, LX/3wS;->e:Z

    .line 655159
    iget-object v1, p0, LX/3wP;->a:Ljava/util/List;

    invoke-interface {v1, p1, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 655160
    :cond_1
    iget v2, p0, LX/3wP;->e:I

    .line 655161
    iget v1, p0, LX/3wP;->f:I

    .line 655162
    iget-object v0, p0, LX/3wP;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move p1, v0

    :goto_1
    if-ltz p1, :cond_6

    .line 655163
    iget-object v0, p0, LX/3wP;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3wS;

    .line 655164
    iget p2, v0, LX/3wS;->a:I

    iget p3, v0, LX/3wS;->c:I

    add-int/2addr p2, p3

    .line 655165
    iget p3, v0, LX/3wS;->b:I

    iget p4, v0, LX/3wS;->c:I

    add-int/2addr p3, p4

    .line 655166
    iget-boolean p4, p0, LX/3wP;->g:Z

    if-eqz p4, :cond_3

    .line 655167
    :goto_2
    if-le v2, p2, :cond_2

    .line 655168
    iget-object p4, p0, LX/3wP;->b:[I

    add-int/lit8 p5, v2, -0x1

    aget p4, p4, p5

    if-eqz p4, :cond_8

    .line 655169
    :goto_3
    add-int/lit8 v2, v2, -0x1

    goto :goto_2

    .line 655170
    :cond_2
    :goto_4
    if-le v1, p3, :cond_3

    .line 655171
    iget-object p2, p0, LX/3wP;->c:[I

    add-int/lit8 p4, v1, -0x1

    aget p2, p2, p4

    if-eqz p2, :cond_9

    .line 655172
    :goto_5
    add-int/lit8 v1, v1, -0x1

    goto :goto_4

    .line 655173
    :cond_3
    const/4 v1, 0x0

    :goto_6
    iget v2, v0, LX/3wS;->c:I

    if-ge v1, v2, :cond_5

    .line 655174
    iget v2, v0, LX/3wS;->a:I

    add-int p2, v2, v1

    .line 655175
    iget v2, v0, LX/3wS;->b:I

    add-int p3, v2, v1

    .line 655176
    iget-object v2, p0, LX/3wP;->d:LX/3wN;

    invoke-virtual {v2, p2, p3}, LX/3wN;->b(II)Z

    move-result v2

    .line 655177
    if-eqz v2, :cond_4

    const/4 v2, 0x1

    .line 655178
    :goto_7
    iget-object p4, p0, LX/3wP;->b:[I

    shl-int/lit8 p5, p3, 0x5

    or-int/2addr p5, v2

    aput p5, p4, p2

    .line 655179
    iget-object p4, p0, LX/3wP;->c:[I

    shl-int/lit8 p2, p2, 0x5

    or-int/2addr v2, p2

    aput v2, p4, p3

    .line 655180
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 655181
    :cond_4
    const/4 v2, 0x2

    goto :goto_7

    .line 655182
    :cond_5
    iget v2, v0, LX/3wS;->a:I

    .line 655183
    iget v1, v0, LX/3wS;->b:I

    .line 655184
    add-int/lit8 v0, p1, -0x1

    move p1, v0

    goto :goto_1

    .line 655185
    :cond_6
    return-void

    .line 655186
    :cond_7
    iget-object v0, p0, LX/3wP;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3wS;

    goto/16 :goto_0

    .line 655187
    :cond_8
    const/4 p4, 0x0

    invoke-static {p0, v2, v1, p1, p4}, LX/3wP;->a(LX/3wP;IIIZ)Z

    goto :goto_3

    .line 655188
    :cond_9
    const/4 p2, 0x1

    invoke-static {p0, v2, v1, p1, p2}, LX/3wP;->a(LX/3wP;IIIZ)Z

    goto :goto_5
.end method

.method private static a(Ljava/util/List;IZ)LX/3wQ;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/3wQ;",
            ">;IZ)",
            "LX/3wQ;"
        }
    .end annotation

    .prologue
    .line 655130
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v1, v0, -0x1

    :goto_0
    if-ltz v1, :cond_2

    .line 655131
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3wQ;

    .line 655132
    iget v2, v0, LX/3wQ;->a:I

    if-ne v2, p1, :cond_1

    iget-boolean v2, v0, LX/3wQ;->c:Z

    if-ne v2, p2, :cond_1

    .line 655133
    invoke-interface {p0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move v2, v1

    .line 655134
    :goto_1
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    if-ge v2, v1, :cond_3

    .line 655135
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3wQ;

    iget v4, v1, LX/3wQ;->b:I

    if-eqz p2, :cond_0

    const/4 v3, 0x1

    :goto_2
    add-int/2addr v3, v4

    iput v3, v1, LX/3wQ;->b:I

    .line 655136
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 655137
    :cond_0
    const/4 v3, -0x1

    goto :goto_2

    .line 655138
    :cond_1
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 655139
    :cond_2
    const/4 v0, 0x0

    :cond_3
    return-object v0
.end method

.method public static a(LX/3wP;LX/3wK;)V
    .locals 11

    .prologue
    .line 655104
    instance-of v0, p1, LX/3wL;

    if-eqz v0, :cond_3

    .line 655105
    check-cast p1, LX/3wL;

    move-object v2, p1

    .line 655106
    :goto_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 655107
    iget v4, p0, LX/3wP;->e:I

    .line 655108
    iget v3, p0, LX/3wP;->f:I

    .line 655109
    iget-object v0, p0, LX/3wP;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v7, v0

    move v8, v3

    :goto_1
    if-ltz v7, :cond_5

    .line 655110
    iget-object v0, p0, LX/3wP;->a:Ljava/util/List;

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, LX/3wS;

    .line 655111
    iget v9, v6, LX/3wS;->c:I

    .line 655112
    iget v0, v6, LX/3wS;->a:I

    add-int v3, v0, v9

    .line 655113
    iget v0, v6, LX/3wS;->b:I

    add-int v10, v0, v9

    .line 655114
    if-ge v3, v4, :cond_0

    .line 655115
    sub-int/2addr v4, v3

    move-object v0, p0

    move v5, v3

    invoke-static/range {v0 .. v5}, LX/3wP;->b(LX/3wP;Ljava/util/List;LX/3wK;III)V

    .line 655116
    :cond_0
    if-ge v10, v8, :cond_1

    .line 655117
    sub-int v4, v8, v10

    move-object v0, p0

    move v5, v10

    invoke-static/range {v0 .. v5}, LX/3wP;->a(LX/3wP;Ljava/util/List;LX/3wK;III)V

    .line 655118
    :cond_1
    add-int/lit8 v0, v9, -0x1

    :goto_2
    if-ltz v0, :cond_4

    .line 655119
    iget-object v3, p0, LX/3wP;->b:[I

    iget v4, v6, LX/3wS;->a:I

    add-int/2addr v4, v0

    aget v3, v3, v4

    and-int/lit8 v3, v3, 0x1f

    const/4 v4, 0x2

    if-ne v3, v4, :cond_2

    .line 655120
    iget v3, v6, LX/3wS;->a:I

    add-int/2addr v3, v0

    const/4 v4, 0x1

    .line 655121
    const/4 v5, 0x0

    move-object v5, v5

    .line 655122
    invoke-virtual {v2, v3, v4, v5}, LX/3wL;->a(IILjava/lang/Object;)V

    .line 655123
    :cond_2
    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    .line 655124
    :cond_3
    new-instance v2, LX/3wL;

    invoke-direct {v2, p1}, LX/3wL;-><init>(LX/3wK;)V

    goto :goto_0

    .line 655125
    :cond_4
    iget v4, v6, LX/3wS;->a:I

    .line 655126
    iget v3, v6, LX/3wS;->b:I

    .line 655127
    add-int/lit8 v0, v7, -0x1

    move v7, v0

    move v8, v3

    goto :goto_1

    .line 655128
    :cond_5
    invoke-virtual {v2}, LX/3wL;->a()V

    .line 655129
    return-void
.end method

.method private static a(LX/3wP;Ljava/util/List;LX/3wK;III)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/3wQ;",
            ">;",
            "LX/3wK;",
            "III)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 655086
    iget-boolean v0, p0, LX/3wP;->g:Z

    if-nez v0, :cond_1

    .line 655087
    invoke-interface {p2, p3, p4}, LX/3wK;->a(II)V

    .line 655088
    :cond_0
    return-void

    .line 655089
    :cond_1
    add-int/lit8 v0, p4, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 655090
    iget-object v0, p0, LX/3wP;->c:[I

    add-int v2, p5, v1

    aget v0, v0, v2

    and-int/lit8 v0, v0, 0x1f

    .line 655091
    sparse-switch v0, :sswitch_data_0

    .line 655092
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "unknown flag for pos "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/2addr v1, p5

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    int-to-long v4, v0

    invoke-static {v4, v5}, Ljava/lang/Long;->toBinaryString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 655093
    :sswitch_0
    invoke-interface {p2, p3, v4}, LX/3wK;->a(II)V

    .line 655094
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3wQ;

    .line 655095
    iget v3, v0, LX/3wQ;->b:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v0, LX/3wQ;->b:I

    goto :goto_1

    .line 655096
    :sswitch_1
    iget-object v2, p0, LX/3wP;->c:[I

    add-int v3, p5, v1

    aget v2, v2, v3

    shr-int/lit8 v2, v2, 0x5

    .line 655097
    invoke-static {p1, v2, v4}, LX/3wP;->a(Ljava/util/List;IZ)LX/3wQ;

    move-result-object v2

    .line 655098
    iget v2, v2, LX/3wQ;->b:I

    invoke-interface {p2, v2, p3}, LX/3wK;->c(II)V

    .line 655099
    const/4 v2, 0x4

    if-ne v0, v2, :cond_2

    .line 655100
    const/4 v0, 0x0

    move-object v0, v0

    .line 655101
    invoke-interface {p2, p3, v4, v0}, LX/3wK;->a(IILjava/lang/Object;)V

    .line 655102
    :cond_2
    :goto_2
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 655103
    :sswitch_2
    new-instance v0, LX/3wQ;

    add-int v2, p5, v1

    const/4 v3, 0x0

    invoke-direct {v0, v2, p3, v3}, LX/3wQ;-><init>(IIZ)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x4 -> :sswitch_1
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public static a(LX/3wP;IIIZ)Z
    .locals 9

    .prologue
    const/16 v2, 0x8

    const/4 v3, 0x4

    const/4 v4, 0x1

    .line 655036
    if-eqz p4, :cond_0

    .line 655037
    add-int/lit8 p2, p2, -0x1

    move v0, p1

    move v1, p2

    :goto_0
    move v5, v0

    .line 655038
    :goto_1
    if-ltz p3, :cond_7

    .line 655039
    iget-object v0, p0, LX/3wP;->a:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3wS;

    .line 655040
    iget v6, v0, LX/3wS;->a:I

    iget v7, v0, LX/3wS;->c:I

    add-int/2addr v6, v7

    .line 655041
    iget v7, v0, LX/3wS;->b:I

    iget v8, v0, LX/3wS;->c:I

    add-int/2addr v7, v8

    .line 655042
    if-eqz p4, :cond_3

    .line 655043
    add-int/lit8 v5, v5, -0x1

    :goto_2
    if-lt v5, v6, :cond_6

    .line 655044
    iget-object v7, p0, LX/3wP;->d:LX/3wN;

    invoke-virtual {v7, v5, v1}, LX/3wN;->a(II)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 655045
    iget-object v0, p0, LX/3wP;->d:LX/3wN;

    invoke-virtual {v0, v5, v1}, LX/3wN;->b(II)Z

    move-result v0

    .line 655046
    if-eqz v0, :cond_1

    move v0, v2

    .line 655047
    :goto_3
    iget-object v2, p0, LX/3wP;->c:[I

    shl-int/lit8 v3, v5, 0x5

    or-int/lit8 v3, v3, 0x10

    aput v3, v2, v1

    .line 655048
    iget-object v2, p0, LX/3wP;->b:[I

    shl-int/lit8 v1, v1, 0x5

    or-int/2addr v0, v1

    aput v0, v2, v5

    move v0, v4

    .line 655049
    :goto_4
    return v0

    .line 655050
    :cond_0
    add-int/lit8 v0, p1, -0x1

    move v1, v0

    .line 655051
    goto :goto_0

    :cond_1
    move v0, v3

    .line 655052
    goto :goto_3

    .line 655053
    :cond_2
    add-int/lit8 v5, v5, -0x1

    goto :goto_2

    .line 655054
    :cond_3
    add-int/lit8 v5, p2, -0x1

    :goto_5
    if-lt v5, v7, :cond_6

    .line 655055
    iget-object v6, p0, LX/3wP;->d:LX/3wN;

    invoke-virtual {v6, v1, v5}, LX/3wN;->a(II)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 655056
    iget-object v0, p0, LX/3wP;->d:LX/3wN;

    invoke-virtual {v0, v1, v5}, LX/3wN;->b(II)Z

    move-result v0

    .line 655057
    if-eqz v0, :cond_4

    .line 655058
    :goto_6
    iget-object v0, p0, LX/3wP;->b:[I

    add-int/lit8 v1, p1, -0x1

    shl-int/lit8 v3, v5, 0x5

    or-int/lit8 v3, v3, 0x10

    aput v3, v0, v1

    .line 655059
    iget-object v0, p0, LX/3wP;->c:[I

    add-int/lit8 v1, p1, -0x1

    shl-int/lit8 v1, v1, 0x5

    or-int/2addr v1, v2

    aput v1, v0, v5

    move v0, v4

    .line 655060
    goto :goto_4

    :cond_4
    move v2, v3

    .line 655061
    goto :goto_6

    .line 655062
    :cond_5
    add-int/lit8 v5, v5, -0x1

    goto :goto_5

    .line 655063
    :cond_6
    iget v5, v0, LX/3wS;->a:I

    .line 655064
    iget p2, v0, LX/3wS;->b:I

    .line 655065
    add-int/lit8 p3, p3, -0x1

    goto :goto_1

    .line 655066
    :cond_7
    const/4 v0, 0x0

    goto :goto_4
.end method

.method private static b(LX/3wP;Ljava/util/List;LX/3wK;III)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/3wQ;",
            ">;",
            "LX/3wK;",
            "III)V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 655067
    iget-boolean v0, p0, LX/3wP;->g:Z

    if-nez v0, :cond_1

    .line 655068
    invoke-interface {p2, p3, p4}, LX/3wK;->b(II)V

    .line 655069
    :cond_0
    return-void

    .line 655070
    :cond_1
    add-int/lit8 v0, p4, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 655071
    iget-object v0, p0, LX/3wP;->b:[I

    add-int v2, p5, v1

    aget v0, v0, v2

    and-int/lit8 v0, v0, 0x1f

    .line 655072
    sparse-switch v0, :sswitch_data_0

    .line 655073
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "unknown flag for pos "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/2addr v1, p5

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    int-to-long v4, v0

    invoke-static {v4, v5}, Ljava/lang/Long;->toBinaryString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 655074
    :sswitch_0
    add-int v0, p3, v1

    invoke-interface {p2, v0, v5}, LX/3wK;->b(II)V

    .line 655075
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3wQ;

    .line 655076
    iget v3, v0, LX/3wQ;->b:I

    add-int/lit8 v3, v3, -0x1

    iput v3, v0, LX/3wQ;->b:I

    goto :goto_1

    .line 655077
    :sswitch_1
    iget-object v2, p0, LX/3wP;->b:[I

    add-int v3, p5, v1

    aget v2, v2, v3

    shr-int/lit8 v2, v2, 0x5

    .line 655078
    const/4 v3, 0x0

    invoke-static {p1, v2, v3}, LX/3wP;->a(Ljava/util/List;IZ)LX/3wQ;

    move-result-object v2

    .line 655079
    add-int v3, p3, v1

    iget v4, v2, LX/3wQ;->b:I

    add-int/lit8 v4, v4, -0x1

    invoke-interface {p2, v3, v4}, LX/3wK;->c(II)V

    .line 655080
    const/4 v3, 0x4

    if-ne v0, v3, :cond_2

    .line 655081
    iget v0, v2, LX/3wQ;->b:I

    add-int/lit8 v0, v0, -0x1

    .line 655082
    const/4 v2, 0x0

    move-object v2, v2

    .line 655083
    invoke-interface {p2, v0, v5, v2}, LX/3wK;->a(IILjava/lang/Object;)V

    .line 655084
    :cond_2
    :goto_2
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 655085
    :sswitch_2
    new-instance v0, LX/3wQ;

    add-int v2, p5, v1

    add-int v3, p3, v1

    invoke-direct {v0, v2, v3, v5}, LX/3wQ;-><init>(IIZ)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x4 -> :sswitch_1
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method
