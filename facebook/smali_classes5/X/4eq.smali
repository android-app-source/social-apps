.class public LX/4eq;
.super LX/4ep;
.source ""


# direct methods
.method public constructor <init>(LX/1Fj;Z)V
    .locals 1

    .prologue
    .line 797559
    sget-object v0, LX/1fo;->a:LX/1fo;

    move-object v0, v0

    .line 797560
    invoke-direct {p0, v0, p1, p2}, LX/4ep;-><init>(Ljava/util/concurrent/Executor;LX/1Fj;Z)V

    .line 797561
    return-void
.end method


# virtual methods
.method public final a(LX/1bf;)LX/1FL;
    .locals 4

    .prologue
    .line 797563
    iget-object v0, p1, LX/1bf;->b:Landroid/net/Uri;

    move-object v0, v0

    .line 797564
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 p1, 0x0

    .line 797565
    const/4 v1, 0x5

    invoke-virtual {v0, p1, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    const-string v2, "data:"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    invoke-static {v1}, LX/03g;->a(Z)V

    .line 797566
    const/16 v1, 0x2c

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 797567
    add-int/lit8 v2, v1, 0x1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 797568
    invoke-virtual {v0, p1, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 797569
    const-string v3, ";"

    invoke-virtual {v1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 797570
    const/4 v3, 0x0

    .line 797571
    :goto_0
    move v1, v3

    .line 797572
    if-eqz v1, :cond_0

    .line 797573
    invoke-static {v2, p1}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v1

    .line 797574
    :goto_1
    move-object v0, v1

    .line 797575
    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    array-length v0, v0

    invoke-virtual {p0, v1, v0}, LX/4ep;->a(Ljava/io/InputStream;I)LX/1FL;

    move-result-object v0

    return-object v0

    .line 797576
    :cond_0
    invoke-static {v2}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 797577
    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    goto :goto_1

    .line 797578
    :cond_1
    const-string v3, ";"

    invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 797579
    array-length v0, v3

    add-int/lit8 v0, v0, -0x1

    aget-object v3, v3, v0

    const-string v0, "base64"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 797562
    const-string v0, "DataFetchProducer"

    return-object v0
.end method
