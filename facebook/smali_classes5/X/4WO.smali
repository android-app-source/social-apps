.class public final LX/4WO;
.super LX/0ur;
.source ""


# instance fields
.field public b:Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityActorsConnection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 751375
    invoke-direct {p0}, LX/0ur;-><init>()V

    .line 751376
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;

    iput-object v0, p0, LX/4WO;->d:Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;

    .line 751377
    instance-of v0, p0, LX/4WO;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 751378
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;
    .locals 2

    .prologue
    .line 751373
    new-instance v0, Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;

    invoke-direct {v0, p0}, Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;-><init>(LX/4WO;)V

    .line 751374
    return-object v0
.end method
