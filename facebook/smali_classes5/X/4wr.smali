.class public final LX/4wr;
.super LX/305;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/305",
        "<TK;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/4wp;


# direct methods
.method public constructor <init>(LX/4wp;)V
    .locals 0

    .prologue
    .line 820550
    iput-object p1, p0, LX/4wr;->a:LX/4wp;

    invoke-direct {p0}, LX/305;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 820551
    iget-object v0, p0, LX/4wr;->a:LX/4wp;

    iget-object v0, v0, LX/4wp;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 820547
    invoke-virtual {p0}, LX/4wr;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final clear()V
    .locals 1

    .prologue
    .line 820548
    iget-object v0, p0, LX/4wr;->a:LX/4wp;

    invoke-virtual {v0}, LX/4wo;->clear()V

    .line 820549
    return-void
.end method

.method public final synthetic e()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 820546
    invoke-virtual {p0}, LX/4wr;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 820545
    iget-object v0, p0, LX/4wr;->a:LX/4wp;

    invoke-virtual {v0}, LX/4wo;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/0PM;->a(Ljava/util/Iterator;)Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 820540
    invoke-virtual {p0, p1}, LX/306;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 820541
    const/4 v0, 0x0

    .line 820542
    :goto_0
    return v0

    .line 820543
    :cond_0
    iget-object v0, p0, LX/4wr;->a:LX/4wp;

    invoke-static {v0, p1}, LX/4wp;->c(LX/4wp;Ljava/lang/Object;)Ljava/lang/Object;

    .line 820544
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final removeAll(Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 820539
    invoke-virtual {p0, p1}, LX/305;->c(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public final retainAll(Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 820538
    invoke-virtual {p0, p1}, LX/306;->d(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method
