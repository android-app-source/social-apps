.class public final LX/3ik;
.super LX/3il;
.source ""


# instance fields
.field public final synthetic a:LX/3if;


# direct methods
.method public constructor <init>(LX/3if;)V
    .locals 0

    .prologue
    .line 630529
    iput-object p1, p0, LX/3ik;->a:LX/3if;

    invoke-direct {p0}, LX/3il;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;F)V
    .locals 13

    .prologue
    const/4 v3, 0x0

    .line 630530
    iget-object v0, p0, LX/3ik;->a:LX/3if;

    iget-object v0, v0, LX/3if;->j:LX/3ij;

    float-to-int v1, p2

    const v10, 0x7fffffff

    const/high16 v9, -0x80000000

    const/4 v5, 0x0

    .line 630531
    iget-object v4, v0, LX/3ij;->q:Landroid/widget/Scroller;

    invoke-virtual {v4}, Landroid/widget/Scroller;->abortAnimation()V

    .line 630532
    iget-object v4, v0, LX/3ij;->q:Landroid/widget/Scroller;

    move v6, v5

    move v7, v5

    move v8, v1

    move v11, v9

    move v12, v10

    invoke-virtual/range {v4 .. v12}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    .line 630533
    iget-object v4, v0, LX/3ij;->q:Landroid/widget/Scroller;

    invoke-virtual {v4}, Landroid/widget/Scroller;->getFinalY()I

    move-result v4

    .line 630534
    iget-object v5, v0, LX/3ij;->q:Landroid/widget/Scroller;

    invoke-virtual {v5}, Landroid/widget/Scroller;->abortAnimation()V

    .line 630535
    move v0, v4

    .line 630536
    iget-object v1, p0, LX/3ik;->a:LX/3if;

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v2

    add-int/2addr v0, v2

    .line 630537
    iput v0, v1, LX/3if;->l:I

    .line 630538
    iget-object v0, p0, LX/3ik;->a:LX/3if;

    invoke-static {v0}, LX/3if;->e$redex0(LX/3if;)V

    .line 630539
    iget-object v0, p0, LX/3ik;->a:LX/3if;

    iget-boolean v0, v0, LX/3if;->o:Z

    if-eqz v0, :cond_0

    .line 630540
    iget-object v0, p0, LX/3ik;->a:LX/3if;

    iget-object v0, v0, LX/3if;->k:LX/0wd;

    neg-float v1, p2

    iget-object v2, p0, LX/3ik;->a:LX/3if;

    invoke-virtual {v2}, LX/3if;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    float-to-double v2, v1

    invoke-virtual {v0, v2, v3}, LX/0wd;->c(D)LX/0wd;

    .line 630541
    :goto_0
    return-void

    .line 630542
    :cond_0
    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget-object v1, p0, LX/3ik;->a:LX/3if;

    iget v1, v1, LX/3if;->m:F

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_1

    .line 630543
    iget-object v0, p0, LX/3ik;->a:LX/3if;

    iget-object v0, v0, LX/3if;->j:LX/3ij;

    const/high16 v1, -0x80000000

    iget-object v2, p0, LX/3ik;->a:LX/3if;

    invoke-virtual {v2}, LX/3if;->getHeight()I

    move-result v2

    invoke-virtual {v0, v3, v1, v3, v2}, LX/3ij;->a(IIII)V

    .line 630544
    :goto_1
    iget-object v0, p0, LX/3ik;->a:LX/3if;

    invoke-static {v0}, LX/0vv;->d(Landroid/view/View;)V

    goto :goto_0

    .line 630545
    :cond_1
    iget-object v0, p0, LX/3ik;->a:LX/3if;

    iget-object v0, v0, LX/3if;->j:LX/3ij;

    iget-object v1, p0, LX/3ik;->a:LX/3if;

    iget v1, v1, LX/3if;->l:I

    invoke-virtual {v0, v3, v1}, LX/3ij;->a(II)Z

    goto :goto_1
.end method

.method public final b(I)I
    .locals 0

    .prologue
    .line 630546
    return p1
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 630547
    iget-object v0, p0, LX/3ik;->a:LX/3if;

    invoke-virtual {v0}, LX/3if;->getHeight()I

    move-result v0

    return v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 630548
    const/4 v0, 0x1

    return v0
.end method
