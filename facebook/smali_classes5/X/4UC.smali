.class public LX/4UC;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 721239
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 45

    .prologue
    .line 721009
    const-wide/16 v38, 0x0

    .line 721010
    const/16 v36, 0x0

    .line 721011
    const/16 v31, 0x0

    .line 721012
    const-wide/16 v34, 0x0

    .line 721013
    const-wide/16 v32, 0x0

    .line 721014
    const/16 v30, 0x0

    .line 721015
    const/16 v29, 0x0

    .line 721016
    const/16 v28, 0x0

    .line 721017
    const/16 v27, 0x0

    .line 721018
    const/16 v26, 0x0

    .line 721019
    const/16 v25, 0x0

    .line 721020
    const/16 v24, 0x0

    .line 721021
    const/16 v21, 0x0

    .line 721022
    const-wide/16 v22, 0x0

    .line 721023
    const/16 v20, 0x0

    .line 721024
    const/16 v19, 0x0

    .line 721025
    const/16 v18, 0x0

    .line 721026
    const/16 v17, 0x0

    .line 721027
    const/16 v16, 0x0

    .line 721028
    const/4 v13, 0x0

    .line 721029
    const-wide/16 v14, 0x0

    .line 721030
    const/4 v12, 0x0

    .line 721031
    const/4 v11, 0x0

    .line 721032
    const/4 v10, 0x0

    .line 721033
    const/4 v9, 0x0

    .line 721034
    const/4 v8, 0x0

    .line 721035
    const/4 v7, 0x0

    .line 721036
    const/4 v6, 0x0

    .line 721037
    const/4 v5, 0x0

    .line 721038
    const/4 v4, 0x0

    .line 721039
    const/4 v3, 0x0

    .line 721040
    const/4 v2, 0x0

    .line 721041
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v37

    sget-object v40, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v37

    move-object/from16 v1, v40

    if-eq v0, v1, :cond_22

    .line 721042
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 721043
    const/4 v2, 0x0

    .line 721044
    :goto_0
    return v2

    .line 721045
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_19

    .line 721046
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 721047
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 721048
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_0

    if-eqz v2, :cond_0

    .line 721049
    const-string v6, "expiration_time"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 721050
    const/4 v2, 0x1

    .line 721051
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 721052
    :cond_1
    const-string v6, "id"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 721053
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v41, v2

    goto :goto_1

    .line 721054
    :cond_2
    const-string v6, "is_viewer_subscribed"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 721055
    const/4 v2, 0x1

    .line 721056
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move v14, v2

    move/from16 v40, v6

    goto :goto_1

    .line 721057
    :cond_3
    const-string v6, "lobby_open_time"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 721058
    const/4 v2, 0x1

    .line 721059
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v6

    move v13, v2

    move-wide/from16 v38, v6

    goto :goto_1

    .line 721060
    :cond_4
    const-string v6, "start_time"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 721061
    const/4 v2, 0x1

    .line 721062
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v6

    move v12, v2

    move-wide/from16 v36, v6

    goto :goto_1

    .line 721063
    :cond_5
    const-string v6, "canceled_endscreen_body"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 721064
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v35, v2

    goto/16 :goto_1

    .line 721065
    :cond_6
    const-string v6, "canceled_title"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 721066
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v34, v2

    goto/16 :goto_1

    .line 721067
    :cond_7
    const-string v6, "expiration_endscreen_body"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 721068
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v33, v2

    goto/16 :goto_1

    .line 721069
    :cond_8
    const-string v6, "expiration_title"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 721070
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v32, v2

    goto/16 :goto_1

    .line 721071
    :cond_9
    const-string v6, "is_scheduled"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 721072
    const/4 v2, 0x1

    .line 721073
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move v11, v2

    move/from16 v31, v6

    goto/16 :goto_1

    .line 721074
    :cond_a
    const-string v6, "rescheduled_endscreen_body"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 721075
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v30, v2

    goto/16 :goto_1

    .line 721076
    :cond_b
    const-string v6, "rescheduled_endscreen_title"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 721077
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v29, v2

    goto/16 :goto_1

    .line 721078
    :cond_c
    const-string v6, "rescheduled_heading"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_d

    .line 721079
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v28, v2

    goto/16 :goto_1

    .line 721080
    :cond_d
    const-string v6, "running_late_start_time"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_e

    .line 721081
    const/4 v2, 0x1

    .line 721082
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v6

    move v10, v2

    move-wide/from16 v26, v6

    goto/16 :goto_1

    .line 721083
    :cond_e
    const-string v6, "running_late_title"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_f

    .line 721084
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v25, v2

    goto/16 :goto_1

    .line 721085
    :cond_f
    const-string v6, "schedule_background_image"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_10

    .line 721086
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v24, v2

    goto/16 :goto_1

    .line 721087
    :cond_10
    const-string v6, "schedule_custom_image"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_11

    .line 721088
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v23, v2

    goto/16 :goto_1

    .line 721089
    :cond_11
    const-string v6, "schedule_profile_image"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_12

    .line 721090
    invoke-static/range {p0 .. p1}, LX/2ax;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v22, v2

    goto/16 :goto_1

    .line 721091
    :cond_12
    const-string v6, "is_rescheduled"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_13

    .line 721092
    const/4 v2, 0x1

    .line 721093
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move v9, v2

    move/from16 v21, v6

    goto/16 :goto_1

    .line 721094
    :cond_13
    const-string v6, "formatted_start_time"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_14

    .line 721095
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v20, v2

    goto/16 :goto_1

    .line 721096
    :cond_14
    const-string v6, "client_latency_buffer"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_15

    .line 721097
    const/4 v2, 0x1

    .line 721098
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v6

    move v8, v2

    move-wide/from16 v18, v6

    goto/16 :goto_1

    .line 721099
    :cond_15
    const-string v6, "canceled_endscreen_title"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_16

    .line 721100
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v17, v2

    goto/16 :goto_1

    .line 721101
    :cond_16
    const-string v6, "expiration_endscreen_title"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_17

    .line 721102
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v16, v2

    goto/16 :goto_1

    .line 721103
    :cond_17
    const-string v6, "running_late_lobby_title"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_18

    .line 721104
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v15, v2

    goto/16 :goto_1

    .line 721105
    :cond_18
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 721106
    :cond_19
    const/16 v2, 0x19

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 721107
    if-eqz v3, :cond_1a

    .line 721108
    const/4 v3, 0x1

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 721109
    :cond_1a
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 721110
    if-eqz v14, :cond_1b

    .line 721111
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v40

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 721112
    :cond_1b
    if-eqz v13, :cond_1c

    .line 721113
    const/4 v3, 0x4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v38

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 721114
    :cond_1c
    if-eqz v12, :cond_1d

    .line 721115
    const/4 v3, 0x5

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v36

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 721116
    :cond_1d
    const/4 v2, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 721117
    const/4 v2, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 721118
    const/16 v2, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 721119
    const/16 v2, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 721120
    if-eqz v11, :cond_1e

    .line 721121
    const/16 v2, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 721122
    :cond_1e
    const/16 v2, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 721123
    const/16 v2, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 721124
    const/16 v2, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 721125
    if-eqz v10, :cond_1f

    .line 721126
    const/16 v3, 0xe

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v26

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 721127
    :cond_1f
    const/16 v2, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 721128
    const/16 v2, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 721129
    const/16 v2, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 721130
    const/16 v2, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 721131
    if-eqz v9, :cond_20

    .line 721132
    const/16 v2, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 721133
    :cond_20
    const/16 v2, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 721134
    if-eqz v8, :cond_21

    .line 721135
    const/16 v3, 0x15

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v18

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 721136
    :cond_21
    const/16 v2, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 721137
    const/16 v2, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 721138
    const/16 v2, 0x18

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 721139
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_22
    move/from16 v40, v31

    move/from16 v41, v36

    move/from16 v31, v26

    move-wide/from16 v36, v32

    move/from16 v32, v27

    move/from16 v33, v28

    move-wide/from16 v26, v22

    move/from16 v28, v21

    move/from16 v21, v16

    move/from16 v22, v17

    move/from16 v23, v18

    move/from16 v16, v11

    move/from16 v17, v12

    move v12, v6

    move v11, v5

    move/from16 v42, v13

    move v13, v7

    move/from16 v43, v19

    move-wide/from16 v18, v14

    move v14, v8

    move v15, v10

    move v10, v4

    move v8, v2

    move-wide/from16 v4, v38

    move-wide/from16 v38, v34

    move/from16 v34, v29

    move/from16 v35, v30

    move/from16 v29, v24

    move/from16 v30, v25

    move/from16 v24, v43

    move/from16 v25, v20

    move/from16 v20, v42

    move/from16 v44, v3

    move v3, v9

    move/from16 v9, v44

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 721140
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 721141
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 721142
    cmp-long v2, v0, v4

    if-eqz v2, :cond_0

    .line 721143
    const-string v2, "expiration_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 721144
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 721145
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 721146
    if-eqz v0, :cond_1

    .line 721147
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 721148
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 721149
    :cond_1
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 721150
    if-eqz v0, :cond_2

    .line 721151
    const-string v1, "is_viewer_subscribed"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 721152
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 721153
    :cond_2
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 721154
    cmp-long v2, v0, v4

    if-eqz v2, :cond_3

    .line 721155
    const-string v2, "lobby_open_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 721156
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 721157
    :cond_3
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 721158
    cmp-long v2, v0, v4

    if-eqz v2, :cond_4

    .line 721159
    const-string v2, "start_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 721160
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 721161
    :cond_4
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 721162
    if-eqz v0, :cond_5

    .line 721163
    const-string v1, "canceled_endscreen_body"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 721164
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 721165
    :cond_5
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 721166
    if-eqz v0, :cond_6

    .line 721167
    const-string v1, "canceled_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 721168
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 721169
    :cond_6
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 721170
    if-eqz v0, :cond_7

    .line 721171
    const-string v1, "expiration_endscreen_body"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 721172
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 721173
    :cond_7
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 721174
    if-eqz v0, :cond_8

    .line 721175
    const-string v1, "expiration_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 721176
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 721177
    :cond_8
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 721178
    if-eqz v0, :cond_9

    .line 721179
    const-string v1, "is_scheduled"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 721180
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 721181
    :cond_9
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 721182
    if-eqz v0, :cond_a

    .line 721183
    const-string v1, "rescheduled_endscreen_body"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 721184
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 721185
    :cond_a
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 721186
    if-eqz v0, :cond_b

    .line 721187
    const-string v1, "rescheduled_endscreen_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 721188
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 721189
    :cond_b
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 721190
    if-eqz v0, :cond_c

    .line 721191
    const-string v1, "rescheduled_heading"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 721192
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 721193
    :cond_c
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 721194
    cmp-long v2, v0, v4

    if-eqz v2, :cond_d

    .line 721195
    const-string v2, "running_late_start_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 721196
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 721197
    :cond_d
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 721198
    if-eqz v0, :cond_e

    .line 721199
    const-string v1, "running_late_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 721200
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 721201
    :cond_e
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 721202
    if-eqz v0, :cond_f

    .line 721203
    const-string v1, "schedule_background_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 721204
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 721205
    :cond_f
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 721206
    if-eqz v0, :cond_10

    .line 721207
    const-string v1, "schedule_custom_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 721208
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 721209
    :cond_10
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 721210
    if-eqz v0, :cond_11

    .line 721211
    const-string v1, "schedule_profile_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 721212
    invoke-static {p0, v0, p2}, LX/2ax;->a(LX/15i;ILX/0nX;)V

    .line 721213
    :cond_11
    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 721214
    if-eqz v0, :cond_12

    .line 721215
    const-string v1, "is_rescheduled"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 721216
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 721217
    :cond_12
    const/16 v0, 0x14

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 721218
    if-eqz v0, :cond_13

    .line 721219
    const-string v1, "formatted_start_time"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 721220
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 721221
    :cond_13
    const/16 v0, 0x15

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 721222
    cmp-long v2, v0, v4

    if-eqz v2, :cond_14

    .line 721223
    const-string v2, "client_latency_buffer"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 721224
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 721225
    :cond_14
    const/16 v0, 0x16

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 721226
    if-eqz v0, :cond_15

    .line 721227
    const-string v1, "canceled_endscreen_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 721228
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 721229
    :cond_15
    const/16 v0, 0x17

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 721230
    if-eqz v0, :cond_16

    .line 721231
    const-string v1, "expiration_endscreen_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 721232
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 721233
    :cond_16
    const/16 v0, 0x18

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 721234
    if-eqz v0, :cond_17

    .line 721235
    const-string v1, "running_late_lobby_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 721236
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 721237
    :cond_17
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 721238
    return-void
.end method
