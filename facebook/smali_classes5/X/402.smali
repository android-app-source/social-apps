.class public final LX/402;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:I

.field public final b:Lcom/google/android/gms/gcm/OneoffTask;


# direct methods
.method public constructor <init>(ILcom/google/android/gms/gcm/OneoffTask;)V
    .locals 0

    .prologue
    .line 662845
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 662846
    iput p1, p0, LX/402;->a:I

    .line 662847
    iput-object p2, p0, LX/402;->b:Lcom/google/android/gms/gcm/OneoffTask;

    .line 662848
    return-void
.end method

.method public constructor <init>(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v1, -0x1

    .line 662849
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 662850
    const-string v0, "job_id"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 662851
    if-ne v0, v1, :cond_0

    .line 662852
    new-instance v0, LX/403;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid job_id: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "job_id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/403;-><init>(Ljava/lang/String;)V

    throw v0

    .line 662853
    :cond_0
    iput v0, p0, LX/402;->a:I

    .line 662854
    const-string v0, "task"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/gcm/OneoffTask;

    .line 662855
    if-nez v0, :cond_1

    .line 662856
    new-instance v0, LX/403;

    const-string v1, "Missing task"

    invoke-direct {v0, v1}, LX/403;-><init>(Ljava/lang/String;)V

    throw v0

    .line 662857
    :cond_1
    iput-object v0, p0, LX/402;->b:Lcom/google/android/gms/gcm/OneoffTask;

    .line 662858
    return-void
.end method


# virtual methods
.method public final a()Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 662859
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 662860
    const-string v1, "job_id"

    iget v2, p0, LX/402;->a:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 662861
    const-string v1, "task"

    iget-object v2, p0, LX/402;->b:Lcom/google/android/gms/gcm/OneoffTask;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 662862
    return-object v0
.end method
