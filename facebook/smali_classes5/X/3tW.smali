.class public LX/3tW;
.super Landroid/view/ViewGroup;
.source ""

# interfaces
.implements LX/3tV;


# static fields
.field public static final a:LX/3tO;

.field public static final b:[I

.field public static final c:Z


# instance fields
.field public A:Ljava/lang/CharSequence;

.field private B:Ljava/lang/Object;

.field private C:Z

.field private final d:LX/3tN;

.field private e:I

.field private f:I

.field private g:F

.field private h:Landroid/graphics/Paint;

.field public final i:LX/3ty;

.field public final j:LX/3ty;

.field private final k:LX/3tU;

.field private final l:LX/3tU;

.field public m:I

.field private n:Z

.field private o:Z

.field private p:I

.field private q:I

.field private r:Z

.field public s:Z

.field public t:LX/3tR;

.field private u:F

.field private v:F

.field private w:Landroid/graphics/drawable/Drawable;

.field private x:Landroid/graphics/drawable/Drawable;

.field private y:Landroid/graphics/drawable/Drawable;

.field public z:Ljava/lang/CharSequence;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 645342
    new-array v2, v0, [I

    const v3, 0x10100b3

    aput v3, v2, v1

    sput-object v2, LX/3tW;->b:[I

    .line 645343
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x13

    if-lt v2, v3, :cond_0

    :goto_0
    sput-boolean v0, LX/3tW;->c:Z

    .line 645344
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 645345
    const/16 v1, 0x15

    if-lt v0, v1, :cond_1

    .line 645346
    new-instance v0, LX/3tP;

    invoke-direct {v0}, LX/3tP;-><init>()V

    sput-object v0, LX/3tW;->a:LX/3tO;

    .line 645347
    :goto_1
    return-void

    :cond_0
    move v0, v1

    .line 645348
    goto :goto_0

    .line 645349
    :cond_1
    new-instance v0, LX/3tQ;

    invoke-direct {v0}, LX/3tQ;-><init>()V

    sput-object v0, LX/3tW;->a:LX/3tO;

    goto :goto_1
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 645350
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/3tW;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 645351
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 645352
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/3tW;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 645353
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v3, 0x1

    .line 645354
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 645355
    new-instance v0, LX/3tN;

    invoke-direct {v0, p0}, LX/3tN;-><init>(LX/3tW;)V

    iput-object v0, p0, LX/3tW;->d:LX/3tN;

    .line 645356
    const/high16 v0, -0x67000000

    iput v0, p0, LX/3tW;->f:I

    .line 645357
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, LX/3tW;->h:Landroid/graphics/Paint;

    .line 645358
    iput-boolean v3, p0, LX/3tW;->o:Z

    .line 645359
    const/high16 v0, 0x40000

    invoke-virtual {p0, v0}, LX/3tW;->setDescendantFocusability(I)V

    .line 645360
    invoke-virtual {p0}, LX/3tW;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 645361
    const/high16 v1, 0x42800000    # 64.0f

    mul-float/2addr v1, v0

    const/high16 v2, 0x3f000000    # 0.5f

    add-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, LX/3tW;->e:I

    .line 645362
    const/high16 v1, 0x43c80000    # 400.0f

    mul-float/2addr v0, v1

    .line 645363
    new-instance v1, LX/3tU;

    const/4 v2, 0x3

    invoke-direct {v1, p0, v2}, LX/3tU;-><init>(LX/3tW;I)V

    iput-object v1, p0, LX/3tW;->k:LX/3tU;

    .line 645364
    new-instance v1, LX/3tU;

    const/4 v2, 0x5

    invoke-direct {v1, p0, v2}, LX/3tU;-><init>(LX/3tW;I)V

    iput-object v1, p0, LX/3tW;->l:LX/3tU;

    .line 645365
    iget-object v1, p0, LX/3tW;->k:LX/3tU;

    invoke-static {p0, v4, v1}, LX/3ty;->a(Landroid/view/ViewGroup;FLX/3nx;)LX/3ty;

    move-result-object v1

    iput-object v1, p0, LX/3tW;->i:LX/3ty;

    .line 645366
    iget-object v1, p0, LX/3tW;->i:LX/3ty;

    .line 645367
    iput v3, v1, LX/3ty;->p:I

    .line 645368
    iget-object v1, p0, LX/3tW;->i:LX/3ty;

    .line 645369
    iput v0, v1, LX/3ty;->n:F

    .line 645370
    iget-object v1, p0, LX/3tW;->k:LX/3tU;

    iget-object v2, p0, LX/3tW;->i:LX/3ty;

    .line 645371
    iput-object v2, v1, LX/3tU;->c:LX/3ty;

    .line 645372
    iget-object v1, p0, LX/3tW;->l:LX/3tU;

    invoke-static {p0, v4, v1}, LX/3ty;->a(Landroid/view/ViewGroup;FLX/3nx;)LX/3ty;

    move-result-object v1

    iput-object v1, p0, LX/3tW;->j:LX/3ty;

    .line 645373
    iget-object v1, p0, LX/3tW;->j:LX/3ty;

    const/4 v2, 0x2

    .line 645374
    iput v2, v1, LX/3ty;->p:I

    .line 645375
    iget-object v1, p0, LX/3tW;->j:LX/3ty;

    .line 645376
    iput v0, v1, LX/3ty;->n:F

    .line 645377
    iget-object v0, p0, LX/3tW;->l:LX/3tU;

    iget-object v1, p0, LX/3tW;->j:LX/3ty;

    .line 645378
    iput-object v1, v0, LX/3tU;->c:LX/3ty;

    .line 645379
    invoke-virtual {p0, v3}, LX/3tW;->setFocusableInTouchMode(Z)V

    .line 645380
    invoke-static {p0, v3}, LX/0vv;->d(Landroid/view/View;I)V

    .line 645381
    new-instance v0, LX/3tM;

    invoke-direct {v0, p0}, LX/3tM;-><init>(LX/3tW;)V

    invoke-static {p0, v0}, LX/0vv;->a(Landroid/view/View;LX/0vn;)V

    .line 645382
    const/4 v0, 0x0

    .line 645383
    sget-object v1, LX/3sF;->a:LX/3s9;

    invoke-interface {v1, p0, v0}, LX/3s9;->a(Landroid/view/ViewGroup;Z)V

    .line 645384
    invoke-static {p0}, LX/0vv;->A(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 645385
    sget-object v0, LX/3tW;->a:LX/3tO;

    invoke-interface {v0, p0}, LX/3tO;->a(Landroid/view/View;)V

    .line 645386
    sget-object v0, LX/3tW;->a:LX/3tO;

    invoke-interface {v0, p1}, LX/3tO;->a(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, LX/3tW;->y:Landroid/graphics/drawable/Drawable;

    .line 645387
    :cond_0
    return-void
.end method

.method private a(II)V
    .locals 3

    .prologue
    const/4 v2, 0x3

    .line 645388
    invoke-static {p0}, LX/0vv;->h(Landroid/view/View;)I

    move-result v0

    invoke-static {p2, v0}, LX/1uf;->a(II)I

    move-result v1

    .line 645389
    if-ne v1, v2, :cond_3

    .line 645390
    iput p1, p0, LX/3tW;->p:I

    .line 645391
    :cond_0
    :goto_0
    if-eqz p1, :cond_1

    .line 645392
    if-ne v1, v2, :cond_4

    iget-object v0, p0, LX/3tW;->i:LX/3ty;

    .line 645393
    :goto_1
    invoke-virtual {v0}, LX/3ty;->e()V

    .line 645394
    :cond_1
    packed-switch p1, :pswitch_data_0

    .line 645395
    :cond_2
    :goto_2
    return-void

    .line 645396
    :cond_3
    const/4 v0, 0x5

    if-ne v1, v0, :cond_0

    .line 645397
    iput p1, p0, LX/3tW;->q:I

    goto :goto_0

    .line 645398
    :cond_4
    iget-object v0, p0, LX/3tW;->j:LX/3ty;

    goto :goto_1

    .line 645399
    :pswitch_0
    invoke-virtual {p0, v1}, LX/3tW;->b(I)Landroid/view/View;

    move-result-object v0

    .line 645400
    if-eqz v0, :cond_2

    .line 645401
    invoke-direct {p0, v0}, LX/3tW;->k(Landroid/view/View;)V

    goto :goto_2

    .line 645402
    :pswitch_1
    invoke-virtual {p0, v1}, LX/3tW;->b(I)Landroid/view/View;

    move-result-object v0

    .line 645403
    if-eqz v0, :cond_2

    .line 645404
    invoke-virtual {p0, v0}, LX/3tW;->e(Landroid/view/View;)V

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static a(LX/3tW;Landroid/view/View;Z)V
    .locals 4

    .prologue
    .line 645405
    invoke-virtual {p0}, LX/3tW;->getChildCount()I

    move-result v1

    .line 645406
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_3

    .line 645407
    invoke-virtual {p0, v0}, LX/3tW;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 645408
    if-nez p2, :cond_0

    invoke-static {v2}, LX/3tW;->d(Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    if-eqz p2, :cond_2

    if-ne v2, p1, :cond_2

    .line 645409
    :cond_1
    const/4 v3, 0x1

    invoke-static {v2, v3}, LX/0vv;->d(Landroid/view/View;I)V

    .line 645410
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 645411
    :cond_2
    const/4 v3, 0x4

    invoke-static {v2, v3}, LX/0vv;->d(Landroid/view/View;I)V

    goto :goto_1

    .line 645412
    :cond_3
    return-void
.end method

.method public static a(LX/3tW;Z)V
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 645413
    invoke-virtual {p0}, LX/3tW;->getChildCount()I

    move-result v4

    move v2, v3

    move v1, v3

    .line 645414
    :goto_0
    if-ge v2, v4, :cond_3

    .line 645415
    invoke-virtual {p0, v2}, LX/3tW;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 645416
    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/3tS;

    .line 645417
    invoke-static {v5}, LX/3tW;->d(Landroid/view/View;)Z

    move-result v6

    if-eqz v6, :cond_1

    if-eqz p1, :cond_0

    iget-boolean v6, v0, LX/3tS;->c:Z

    if-eqz v6, :cond_1

    .line 645418
    :cond_0
    invoke-virtual {v5}, Landroid/view/View;->getWidth()I

    move-result v6

    .line 645419
    const/4 v7, 0x3

    invoke-virtual {p0, v5, v7}, LX/3tW;->a(Landroid/view/View;I)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 645420
    iget-object v7, p0, LX/3tW;->i:LX/3ty;

    neg-int v6, v6

    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v8

    invoke-virtual {v7, v5, v6, v8}, LX/3ty;->a(Landroid/view/View;II)Z

    move-result v5

    or-int/2addr v1, v5

    .line 645421
    :goto_1
    iput-boolean v3, v0, LX/3tS;->c:Z

    .line 645422
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 645423
    :cond_2
    iget-object v6, p0, LX/3tW;->j:LX/3ty;

    invoke-virtual {p0}, LX/3tW;->getWidth()I

    move-result v7

    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v8

    invoke-virtual {v6, v5, v7, v8}, LX/3ty;->a(Landroid/view/View;II)Z

    move-result v5

    or-int/2addr v1, v5

    goto :goto_1

    .line 645424
    :cond_3
    iget-object v0, p0, LX/3tW;->k:LX/3tU;

    invoke-virtual {v0}, LX/3tU;->a()V

    .line 645425
    iget-object v0, p0, LX/3tW;->l:LX/3tU;

    invoke-virtual {v0}, LX/3tU;->a()V

    .line 645426
    if-eqz v1, :cond_4

    .line 645427
    invoke-virtual {p0}, LX/3tW;->invalidate()V

    .line 645428
    :cond_4
    return-void
.end method

.method private d()Landroid/view/View;
    .locals 4

    .prologue
    .line 645429
    invoke-virtual {p0}, LX/3tW;->getChildCount()I

    move-result v3

    .line 645430
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_1

    .line 645431
    invoke-virtual {p0, v2}, LX/3tW;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 645432
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/3tS;

    iget-boolean v0, v0, LX/3tS;->d:Z

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 645433
    :goto_1
    return-object v0

    .line 645434
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 645435
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static d(Landroid/view/View;)Z
    .locals 2

    .prologue
    .line 645436
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/3tS;

    iget v0, v0, LX/3tS;->a:I

    .line 645437
    invoke-static {p0}, LX/0vv;->h(Landroid/view/View;)I

    move-result v1

    invoke-static {v0, v1}, LX/1uf;->a(II)I

    move-result v0

    .line 645438
    and-int/lit8 v0, v0, 0x7

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static e(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 645439
    and-int/lit8 v0, p0, 0x3

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 645440
    const-string v0, "LEFT"

    .line 645441
    :goto_0
    return-object v0

    .line 645442
    :cond_0
    and-int/lit8 v0, p0, 0x5

    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    .line 645443
    const-string v0, "RIGHT"

    goto :goto_0

    .line 645444
    :cond_1
    invoke-static {p0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static h(LX/3tW;)Landroid/view/View;
    .locals 4

    .prologue
    .line 645445
    invoke-virtual {p0}, LX/3tW;->getChildCount()I

    move-result v2

    .line 645446
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 645447
    invoke-virtual {p0, v1}, LX/3tW;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 645448
    invoke-static {v0}, LX/3tW;->d(Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {v0}, LX/3tW;->l(Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 645449
    :goto_1
    return-object v0

    .line 645450
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 645451
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static j(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 645331
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/3tS;

    iget v0, v0, LX/3tS;->a:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private k(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 645452
    invoke-static {p1}, LX/3tW;->d(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 645453
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "View "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a sliding drawer"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 645454
    :cond_0
    iget-boolean v0, p0, LX/3tW;->o:Z

    if-eqz v0, :cond_1

    .line 645455
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/3tS;

    .line 645456
    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, v0, LX/3tS;->b:F

    .line 645457
    iput-boolean v2, v0, LX/3tS;->d:Z

    .line 645458
    invoke-static {p0, p1, v2}, LX/3tW;->a(LX/3tW;Landroid/view/View;Z)V

    .line 645459
    :goto_0
    invoke-virtual {p0}, LX/3tW;->invalidate()V

    .line 645460
    return-void

    .line 645461
    :cond_1
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/3tW;->a(Landroid/view/View;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 645462
    iget-object v0, p0, LX/3tW;->i:LX/3ty;

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v2

    invoke-virtual {v0, p1, v1, v2}, LX/3ty;->a(Landroid/view/View;II)Z

    goto :goto_0

    .line 645463
    :cond_2
    iget-object v0, p0, LX/3tW;->j:LX/3ty;

    invoke-virtual {p0}, LX/3tW;->getWidth()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v2

    invoke-virtual {v0, p1, v1, v2}, LX/3ty;->a(Landroid/view/View;II)Z

    goto :goto_0
.end method

.method private static l(Landroid/view/View;)Z
    .locals 3

    .prologue
    .line 645464
    invoke-static {p0}, LX/3tW;->d(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 645465
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "View "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a drawer"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 645466
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/3tS;

    iget v0, v0, LX/3tS;->b:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static m(Landroid/view/View;)Z
    .locals 2

    .prologue
    .line 645467
    invoke-static {p0}, LX/0vv;->e(Landroid/view/View;)I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    invoke-static {p0}, LX/0vv;->e(Landroid/view/View;)I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/View;)I
    .locals 2

    .prologue
    .line 645468
    invoke-virtual {p0, p1}, LX/3tW;->c(Landroid/view/View;)I

    move-result v0

    .line 645469
    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 645470
    iget v0, p0, LX/3tW;->p:I

    .line 645471
    :goto_0
    return v0

    .line 645472
    :cond_0
    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    .line 645473
    iget v0, p0, LX/3tW;->q:I

    goto :goto_0

    .line 645474
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/view/View;F)V
    .locals 2

    .prologue
    .line 645475
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/3tS;

    .line 645476
    iget v1, v0, LX/3tS;->b:F

    cmpl-float v1, p2, v1

    if-nez v1, :cond_0

    .line 645477
    :goto_0
    return-void

    .line 645478
    :cond_0
    iput p2, v0, LX/3tS;->b:F

    .line 645479
    iget-object v0, p0, LX/3tW;->t:LX/3tR;

    if-eqz v0, :cond_1

    .line 645480
    iget-object v0, p0, LX/3tW;->t:LX/3tR;

    invoke-interface {v0, p2}, LX/3tR;->a(F)V

    .line 645481
    :cond_1
    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 645482
    iput-object p1, p0, LX/3tW;->B:Ljava/lang/Object;

    .line 645483
    iput-boolean p2, p0, LX/3tW;->C:Z

    .line 645484
    if-nez p2, :cond_0

    invoke-virtual {p0}, LX/3tW;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, LX/3tW;->setWillNotDraw(Z)V

    .line 645485
    invoke-virtual {p0}, LX/3tW;->requestLayout()V

    .line 645486
    return-void

    .line 645487
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/view/View;I)Z
    .locals 1

    .prologue
    .line 645488
    invoke-virtual {p0, p1}, LX/3tW;->c(Landroid/view/View;)I

    move-result v0

    .line 645489
    and-int/2addr v0, p2

    if-ne v0, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    .prologue
    .line 645490
    invoke-super {p0, p1, p2, p3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 645491
    invoke-direct {p0}, LX/3tW;->d()Landroid/view/View;

    move-result-object v0

    .line 645492
    if-nez v0, :cond_0

    invoke-static {p1}, LX/3tW;->d(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 645493
    :cond_0
    const/4 v0, 0x4

    invoke-static {p1, v0}, LX/0vv;->d(Landroid/view/View;I)V

    .line 645494
    :goto_0
    sget-boolean v0, LX/3tW;->c:Z

    if-nez v0, :cond_1

    .line 645495
    iget-object v0, p0, LX/3tW;->d:LX/3tN;

    invoke-static {p1, v0}, LX/0vv;->a(Landroid/view/View;LX/0vn;)V

    .line 645496
    :cond_1
    return-void

    .line 645497
    :cond_2
    const/4 v0, 0x1

    invoke-static {p1, v0}, LX/0vv;->d(Landroid/view/View;I)V

    goto :goto_0
.end method

.method public final b(I)Landroid/view/View;
    .locals 5

    .prologue
    .line 645498
    invoke-static {p0}, LX/0vv;->h(Landroid/view/View;)I

    move-result v0

    invoke-static {p1, v0}, LX/1uf;->a(II)I

    move-result v0

    and-int/lit8 v2, v0, 0x7

    .line 645499
    invoke-virtual {p0}, LX/3tW;->getChildCount()I

    move-result v3

    .line 645500
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    .line 645501
    invoke-virtual {p0, v1}, LX/3tW;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 645502
    invoke-virtual {p0, v0}, LX/3tW;->c(Landroid/view/View;)I

    move-result v4

    .line 645503
    and-int/lit8 v4, v4, 0x7

    if-ne v4, v2, :cond_0

    .line 645504
    :goto_1
    return-object v0

    .line 645505
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 645506
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final c(Landroid/view/View;)I
    .locals 2

    .prologue
    .line 645507
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/3tS;

    iget v0, v0, LX/3tS;->a:I

    .line 645508
    invoke-static {p0}, LX/0vv;->h(Landroid/view/View;)I

    move-result v1

    invoke-static {v0, v1}, LX/1uf;->a(II)I

    move-result v0

    return v0
.end method

.method public final c(I)V
    .locals 3

    .prologue
    .line 645509
    invoke-virtual {p0, p1}, LX/3tW;->b(I)Landroid/view/View;

    move-result-object v0

    .line 645510
    if-nez v0, :cond_0

    .line 645511
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No drawer view found with gravity "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, LX/3tW;->e(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 645512
    :cond_0
    invoke-direct {p0, v0}, LX/3tW;->k(Landroid/view/View;)V

    .line 645513
    return-void
.end method

.method public final checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1

    .prologue
    .line 645514
    instance-of v0, p1, LX/3tS;

    if-eqz v0, :cond_0

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final computeScroll()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 645332
    invoke-virtual {p0}, LX/3tW;->getChildCount()I

    move-result v3

    .line 645333
    const/4 v1, 0x0

    .line 645334
    const/4 v0, 0x0

    move v2, v1

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 645335
    invoke-virtual {p0, v1}, LX/3tW;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/3tS;

    iget v0, v0, LX/3tS;->b:F

    .line 645336
    invoke-static {v2, v0}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 645337
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 645338
    :cond_0
    iput v2, p0, LX/3tW;->g:F

    .line 645339
    iget-object v0, p0, LX/3tW;->i:LX/3ty;

    invoke-virtual {v0, v4}, LX/3ty;->a(Z)Z

    move-result v0

    iget-object v1, p0, LX/3tW;->j:LX/3ty;

    invoke-virtual {v1, v4}, LX/3ty;->a(Z)Z

    move-result v1

    or-int/2addr v0, v1

    if-eqz v0, :cond_1

    .line 645340
    invoke-static {p0}, LX/0vv;->d(Landroid/view/View;)V

    .line 645341
    :cond_1
    return-void
.end method

.method public final d(I)V
    .locals 3

    .prologue
    .line 645109
    invoke-virtual {p0, p1}, LX/3tW;->b(I)Landroid/view/View;

    move-result-object v0

    .line 645110
    if-nez v0, :cond_0

    .line 645111
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No drawer view found with gravity "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, LX/3tW;->e(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 645112
    :cond_0
    invoke-virtual {p0, v0}, LX/3tW;->e(Landroid/view/View;)V

    .line 645113
    return-void
.end method

.method public final drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
    .locals 11

    .prologue
    .line 645127
    invoke-virtual {p0}, LX/3tW;->getHeight()I

    move-result v4

    .line 645128
    invoke-static {p2}, LX/3tW;->j(Landroid/view/View;)Z

    move-result v5

    .line 645129
    const/4 v1, 0x0

    invoke-virtual {p0}, LX/3tW;->getWidth()I

    move-result v2

    .line 645130
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v6

    .line 645131
    if-eqz v5, :cond_5

    .line 645132
    invoke-virtual {p0}, LX/3tW;->getChildCount()I

    move-result v7

    .line 645133
    const/4 v0, 0x0

    move v3, v0

    :goto_0
    if-ge v3, v7, :cond_4

    .line 645134
    invoke-virtual {p0, v3}, LX/3tW;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 645135
    if-eq v0, p2, :cond_3

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v8

    if-nez v8, :cond_3

    const/4 v8, 0x0

    .line 645136
    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v9

    .line 645137
    if-eqz v9, :cond_0

    .line 645138
    invoke-virtual {v9}, Landroid/graphics/drawable/Drawable;->getOpacity()I

    move-result v9

    const/4 v10, -0x1

    if-ne v9, v10, :cond_0

    const/4 v8, 0x1

    .line 645139
    :cond_0
    move v8, v8

    .line 645140
    if-eqz v8, :cond_3

    invoke-static {v0}, LX/3tW;->d(Landroid/view/View;)Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v8

    if-lt v8, v4, :cond_3

    .line 645141
    const/4 v8, 0x3

    invoke-virtual {p0, v0, v8}, LX/3tW;->a(Landroid/view/View;I)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 645142
    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v0

    .line 645143
    if-le v0, v1, :cond_9

    :goto_1
    move v1, v0

    move v0, v2

    .line 645144
    :cond_1
    :goto_2
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v0

    goto :goto_0

    .line 645145
    :cond_2
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    .line 645146
    if-lt v0, v2, :cond_1

    :cond_3
    move v0, v2

    goto :goto_2

    .line 645147
    :cond_4
    const/4 v0, 0x0

    invoke-virtual {p0}, LX/3tW;->getHeight()I

    move-result v3

    invoke-virtual {p1, v1, v0, v2, v3}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    :cond_5
    move v0, v2

    .line 645148
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v7

    .line 645149
    invoke-virtual {p1, v6}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 645150
    iget v2, p0, LX/3tW;->g:F

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-lez v2, :cond_7

    if-eqz v5, :cond_7

    .line 645151
    iget v2, p0, LX/3tW;->f:I

    const/high16 v3, -0x1000000

    and-int/2addr v2, v3

    ushr-int/lit8 v2, v2, 0x18

    .line 645152
    int-to-float v2, v2

    iget v3, p0, LX/3tW;->g:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    .line 645153
    shl-int/lit8 v2, v2, 0x18

    iget v3, p0, LX/3tW;->f:I

    const v4, 0xffffff

    and-int/2addr v3, v4

    or-int/2addr v2, v3

    .line 645154
    iget-object v3, p0, LX/3tW;->h:Landroid/graphics/Paint;

    invoke-virtual {v3, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 645155
    int-to-float v1, v1

    const/4 v2, 0x0

    int-to-float v3, v0

    invoke-virtual {p0}, LX/3tW;->getHeight()I

    move-result v0

    int-to-float v4, v0

    iget-object v5, p0, LX/3tW;->h:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 645156
    :cond_6
    :goto_3
    return v7

    .line 645157
    :cond_7
    iget-object v0, p0, LX/3tW;->w:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_8

    const/4 v0, 0x3

    invoke-virtual {p0, p2, v0}, LX/3tW;->a(Landroid/view/View;I)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 645158
    iget-object v0, p0, LX/3tW;->w:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    .line 645159
    invoke-virtual {p2}, Landroid/view/View;->getRight()I

    move-result v1

    .line 645160
    iget-object v2, p0, LX/3tW;->i:LX/3ty;

    .line 645161
    iget v3, v2, LX/3ty;->o:I

    move v2, v3

    .line 645162
    const/4 v3, 0x0

    int-to-float v4, v1

    int-to-float v2, v2

    div-float v2, v4, v2

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-static {v2, v4}, Ljava/lang/Math;->min(FF)F

    move-result v2

    invoke-static {v3, v2}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 645163
    iget-object v3, p0, LX/3tW;->w:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p2}, Landroid/view/View;->getTop()I

    move-result v4

    add-int/2addr v0, v1

    invoke-virtual {p2}, Landroid/view/View;->getBottom()I

    move-result v5

    invoke-virtual {v3, v1, v4, v0, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 645164
    iget-object v0, p0, LX/3tW;->w:Landroid/graphics/drawable/Drawable;

    const/high16 v1, 0x437f0000    # 255.0f

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 645165
    iget-object v0, p0, LX/3tW;->w:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_3

    .line 645166
    :cond_8
    iget-object v0, p0, LX/3tW;->x:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_6

    const/4 v0, 0x5

    invoke-virtual {p0, p2, v0}, LX/3tW;->a(Landroid/view/View;I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 645167
    iget-object v0, p0, LX/3tW;->x:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    .line 645168
    invoke-virtual {p2}, Landroid/view/View;->getLeft()I

    move-result v1

    .line 645169
    invoke-virtual {p0}, LX/3tW;->getWidth()I

    move-result v2

    sub-int/2addr v2, v1

    .line 645170
    iget-object v3, p0, LX/3tW;->j:LX/3ty;

    .line 645171
    iget v4, v3, LX/3ty;->o:I

    move v3, v4

    .line 645172
    const/4 v4, 0x0

    int-to-float v2, v2

    int-to-float v3, v3

    div-float/2addr v2, v3

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v2

    invoke-static {v4, v2}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 645173
    iget-object v3, p0, LX/3tW;->x:Landroid/graphics/drawable/Drawable;

    sub-int v0, v1, v0

    invoke-virtual {p2}, Landroid/view/View;->getTop()I

    move-result v4

    invoke-virtual {p2}, Landroid/view/View;->getBottom()I

    move-result v5

    invoke-virtual {v3, v0, v4, v1, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 645174
    iget-object v0, p0, LX/3tW;->x:Landroid/graphics/drawable/Drawable;

    const/high16 v1, 0x437f0000    # 255.0f

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 645175
    iget-object v0, p0, LX/3tW;->x:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_3

    :cond_9
    move v0, v1

    goto/16 :goto_1
.end method

.method public final e(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 645116
    invoke-static {p1}, LX/3tW;->d(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 645117
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "View "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a sliding drawer"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 645118
    :cond_0
    iget-boolean v0, p0, LX/3tW;->o:Z

    if-eqz v0, :cond_1

    .line 645119
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/3tS;

    .line 645120
    const/4 v1, 0x0

    iput v1, v0, LX/3tS;->b:F

    .line 645121
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/3tS;->d:Z

    .line 645122
    :goto_0
    invoke-virtual {p0}, LX/3tW;->invalidate()V

    .line 645123
    return-void

    .line 645124
    :cond_1
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/3tW;->a(Landroid/view/View;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 645125
    iget-object v0, p0, LX/3tW;->i:LX/3ty;

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v1

    neg-int v1, v1

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v2

    invoke-virtual {v0, p1, v1, v2}, LX/3ty;->a(Landroid/view/View;II)Z

    goto :goto_0

    .line 645126
    :cond_2
    iget-object v0, p0, LX/3tW;->j:LX/3ty;

    invoke-virtual {p0}, LX/3tW;->getWidth()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v2

    invoke-virtual {v0, p1, v1, v2}, LX/3ty;->a(Landroid/view/View;II)Z

    goto :goto_0
.end method

.method public final generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 645115
    new-instance v0, LX/3tS;

    invoke-direct {v0, v1, v1}, LX/3tS;-><init>(II)V

    return-object v0
.end method

.method public final generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    .prologue
    .line 645114
    new-instance v0, LX/3tS;

    invoke-virtual {p0}, LX/3tW;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, LX/3tS;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method public final generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 645176
    instance-of v0, p1, LX/3tS;

    if-eqz v0, :cond_0

    new-instance v0, LX/3tS;

    check-cast p1, LX/3tS;

    invoke-direct {v0, p1}, LX/3tS;-><init>(LX/3tS;)V

    :goto_0
    return-object v0

    :cond_0
    instance-of v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v0, :cond_1

    new-instance v0, LX/3tS;

    check-cast p1, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-direct {v0, p1}, LX/3tS;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    goto :goto_0

    :cond_1
    new-instance v0, LX/3tS;

    invoke-direct {v0, p1}, LX/3tS;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public getStatusBarBackgroundDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 645108
    iget-object v0, p0, LX/3tW;->y:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x2621e148

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 645105
    invoke-super {p0}, Landroid/view/ViewGroup;->onAttachedToWindow()V

    .line 645106
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/3tW;->o:Z

    .line 645107
    const/16 v1, 0x2d

    const v2, 0xfc1aa3d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x6938b719

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 645102
    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    .line 645103
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/3tW;->o:Z

    .line 645104
    const/16 v1, 0x2d

    const v2, 0x1d5f879f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 645095
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onDraw(Landroid/graphics/Canvas;)V

    .line 645096
    iget-boolean v0, p0, LX/3tW;->C:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3tW;->y:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 645097
    sget-object v0, LX/3tW;->a:LX/3tO;

    iget-object v1, p0, LX/3tW;->B:Ljava/lang/Object;

    invoke-interface {v0, v1}, LX/3tO;->a(Ljava/lang/Object;)I

    move-result v0

    .line 645098
    if-lez v0, :cond_0

    .line 645099
    iget-object v1, p0, LX/3tW;->y:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, LX/3tW;->getWidth()I

    move-result v2

    invoke-virtual {v1, v3, v3, v2, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 645100
    iget-object v0, p0, LX/3tW;->y:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 645101
    :cond_0
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 14

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 645045
    invoke-static {p1}, LX/2xd;->a(Landroid/view/MotionEvent;)I

    move-result v0

    .line 645046
    iget-object v3, p0, LX/3tW;->i:LX/3ty;

    invoke-virtual {v3, p1}, LX/3ty;->a(Landroid/view/MotionEvent;)Z

    move-result v3

    iget-object v4, p0, LX/3tW;->j:LX/3ty;

    invoke-virtual {v4, p1}, LX/3ty;->a(Landroid/view/MotionEvent;)Z

    move-result v4

    or-int/2addr v3, v4

    .line 645047
    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    move v0, v2

    .line 645048
    :goto_1
    if-nez v3, :cond_1

    if-nez v0, :cond_1

    const/4 v3, 0x0

    .line 645049
    invoke-virtual {p0}, LX/3tW;->getChildCount()I

    move-result v5

    move v4, v3

    .line 645050
    :goto_2
    if-ge v4, v5, :cond_7

    .line 645051
    invoke-virtual {p0, v4}, LX/3tW;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/3tS;

    .line 645052
    iget-boolean v0, v0, LX/3tS;->c:Z

    if-eqz v0, :cond_6

    .line 645053
    const/4 v0, 0x1

    .line 645054
    :goto_3
    move v0, v0

    .line 645055
    if-nez v0, :cond_1

    iget-boolean v0, p0, LX/3tW;->s:Z

    if-eqz v0, :cond_2

    :cond_1
    move v2, v1

    :cond_2
    return v2

    .line 645056
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 645057
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    .line 645058
    iput v0, p0, LX/3tW;->u:F

    .line 645059
    iput v4, p0, LX/3tW;->v:F

    .line 645060
    iget v5, p0, LX/3tW;->g:F

    const/4 v6, 0x0

    cmpl-float v5, v5, v6

    if-lez v5, :cond_5

    .line 645061
    iget-object v5, p0, LX/3tW;->i:LX/3ty;

    float-to-int v0, v0

    float-to-int v4, v4

    invoke-virtual {v5, v0, v4}, LX/3ty;->b(II)Landroid/view/View;

    move-result-object v0

    .line 645062
    if-eqz v0, :cond_5

    invoke-static {v0}, LX/3tW;->j(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v1

    .line 645063
    :goto_4
    iput-boolean v2, p0, LX/3tW;->r:Z

    .line 645064
    iput-boolean v2, p0, LX/3tW;->s:Z

    goto :goto_1

    .line 645065
    :pswitch_1
    iget-object v0, p0, LX/3tW;->i:LX/3ty;

    const/4 v4, 0x3

    const/4 v5, 0x0

    .line 645066
    iget-object v6, v0, LX/3ty;->d:[F

    array-length v7, v6

    move v6, v5

    .line 645067
    :goto_5
    if-ge v6, v7, :cond_4

    .line 645068
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 645069
    invoke-static {v0, v6}, LX/3ty;->f(LX/3ty;I)Z

    move-result v8

    if-nez v8, :cond_9

    .line 645070
    :cond_3
    :goto_6
    move v8, v10

    .line 645071
    if-eqz v8, :cond_8

    .line 645072
    const/4 v5, 0x1

    .line 645073
    :cond_4
    move v0, v5

    .line 645074
    if-eqz v0, :cond_0

    .line 645075
    iget-object v0, p0, LX/3tW;->k:LX/3tU;

    invoke-virtual {v0}, LX/3tU;->a()V

    .line 645076
    iget-object v0, p0, LX/3tW;->l:LX/3tU;

    invoke-virtual {v0}, LX/3tU;->a()V

    move v0, v2

    goto :goto_1

    .line 645077
    :pswitch_2
    invoke-static {p0, v1}, LX/3tW;->a(LX/3tW;Z)V

    .line 645078
    iput-boolean v2, p0, LX/3tW;->r:Z

    .line 645079
    iput-boolean v2, p0, LX/3tW;->s:Z

    goto :goto_0

    :cond_5
    move v0, v2

    goto :goto_4

    .line 645080
    :cond_6
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_2

    :cond_7
    move v0, v3

    .line 645081
    goto :goto_3

    .line 645082
    :cond_8
    add-int/lit8 v6, v6, 0x1

    goto :goto_5

    .line 645083
    :cond_9
    and-int/lit8 v8, v4, 0x1

    if-ne v8, v9, :cond_a

    move v11, v9

    .line 645084
    :goto_7
    and-int/lit8 v8, v4, 0x2

    const/4 v12, 0x2

    if-ne v8, v12, :cond_b

    move v8, v9

    .line 645085
    :goto_8
    iget-object v12, v0, LX/3ty;->f:[F

    aget v12, v12, v6

    iget-object v13, v0, LX/3ty;->d:[F

    aget v13, v13, v6

    sub-float/2addr v12, v13

    .line 645086
    iget-object v13, v0, LX/3ty;->g:[F

    aget v13, v13, v6

    iget-object p1, v0, LX/3ty;->e:[F

    aget p1, p1, v6

    sub-float/2addr v13, p1

    .line 645087
    if-eqz v11, :cond_c

    if-eqz v8, :cond_c

    .line 645088
    mul-float v8, v12, v12

    mul-float v11, v13, v13

    add-float/2addr v8, v11

    iget v11, v0, LX/3ty;->b:I

    iget v12, v0, LX/3ty;->b:I

    mul-int/2addr v11, v12

    int-to-float v11, v11

    cmpl-float v8, v8, v11

    if-lez v8, :cond_3

    move v10, v9

    goto :goto_6

    :cond_a
    move v11, v10

    .line 645089
    goto :goto_7

    :cond_b
    move v8, v10

    .line 645090
    goto :goto_8

    .line 645091
    :cond_c
    if-eqz v11, :cond_d

    .line 645092
    invoke-static {v12}, Ljava/lang/Math;->abs(F)F

    move-result v8

    iget v11, v0, LX/3ty;->b:I

    int-to-float v11, v11

    cmpl-float v8, v8, v11

    if-lez v8, :cond_3

    move v10, v9

    goto :goto_6

    .line 645093
    :cond_d
    if-eqz v8, :cond_3

    .line 645094
    invoke-static {v13}, Ljava/lang/Math;->abs(F)F

    move-result v8

    iget v11, v0, LX/3ty;->b:I

    int-to-float v11, v11

    cmpl-float v8, v8, v11

    if-lez v8, :cond_3

    move v10, v9

    goto :goto_6

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 645039
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 645040
    invoke-static {p0}, LX/3tW;->h(LX/3tW;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 645041
    if-eqz v0, :cond_0

    .line 645042
    sget-object v0, LX/3rb;->a:LX/3rX;

    invoke-interface {v0, p2}, LX/3rX;->a(Landroid/view/KeyEvent;)V

    .line 645043
    const/4 v0, 0x1

    .line 645044
    :goto_1
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    .line 645177
    const/4 v0, 0x4

    if-ne p1, v0, :cond_2

    .line 645178
    invoke-static {p0}, LX/3tW;->h(LX/3tW;)Landroid/view/View;

    move-result-object v0

    .line 645179
    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, LX/3tW;->a(Landroid/view/View;)I

    move-result v1

    if-nez v1, :cond_0

    .line 645180
    const/4 v1, 0x0

    invoke-static {p0, v1}, LX/3tW;->a(LX/3tW;Z)V

    .line 645181
    :cond_0
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 645182
    :goto_0
    return v0

    .line 645183
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 645184
    :cond_2
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final onLayout(ZIIII)V
    .locals 14

    .prologue
    .line 645185
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/3tW;->n:Z

    .line 645186
    sub-int v6, p4, p2

    .line 645187
    invoke-virtual {p0}, LX/3tW;->getChildCount()I

    move-result v7

    .line 645188
    const/4 v0, 0x0

    move v5, v0

    :goto_0
    if-ge v5, v7, :cond_8

    .line 645189
    invoke-virtual {p0, v5}, LX/3tW;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    .line 645190
    invoke-virtual {v8}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    .line 645191
    invoke-virtual {v8}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/3tS;

    .line 645192
    invoke-static {v8}, LX/3tW;->j(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 645193
    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    invoke-virtual {v8}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v3, v4

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    invoke-virtual {v8}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v0, v4

    invoke-virtual {v8, v1, v2, v3, v0}, Landroid/view/View;->layout(IIII)V

    .line 645194
    :cond_0
    :goto_1
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_0

    .line 645195
    :cond_1
    invoke-virtual {v8}, Landroid/view/View;->getMeasuredWidth()I

    move-result v9

    .line 645196
    invoke-virtual {v8}, Landroid/view/View;->getMeasuredHeight()I

    move-result v10

    .line 645197
    const/4 v1, 0x3

    invoke-virtual {p0, v8, v1}, LX/3tW;->a(Landroid/view/View;I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 645198
    neg-int v1, v9

    int-to-float v2, v9

    iget v3, v0, LX/3tS;->b:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    add-int/2addr v2, v1

    .line 645199
    add-int v1, v9, v2

    int-to-float v1, v1

    int-to-float v3, v9

    div-float/2addr v1, v3

    .line 645200
    :goto_2
    iget v3, v0, LX/3tS;->b:F

    cmpl-float v3, v1, v3

    if-eqz v3, :cond_4

    const/4 v3, 0x1

    .line 645201
    :goto_3
    iget v4, v0, LX/3tS;->a:I

    and-int/lit8 v4, v4, 0x70

    .line 645202
    sparse-switch v4, :sswitch_data_0

    .line 645203
    iget v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v9, v2

    iget v11, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v10, v11

    invoke-virtual {v8, v2, v4, v9, v10}, Landroid/view/View;->layout(IIII)V

    .line 645204
    :goto_4
    if-eqz v3, :cond_2

    .line 645205
    invoke-virtual {p0, v8, v1}, LX/3tW;->a(Landroid/view/View;F)V

    .line 645206
    :cond_2
    iget v0, v0, LX/3tS;->b:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_7

    const/4 v0, 0x0

    .line 645207
    :goto_5
    invoke-virtual {v8}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eq v1, v0, :cond_0

    .line 645208
    invoke-virtual {v8, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 645209
    :cond_3
    int-to-float v1, v9

    iget v2, v0, LX/3tS;->b:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    sub-int v2, v6, v1

    .line 645210
    sub-int v1, v6, v2

    int-to-float v1, v1

    int-to-float v3, v9

    div-float/2addr v1, v3

    goto :goto_2

    .line 645211
    :cond_4
    const/4 v3, 0x0

    goto :goto_3

    .line 645212
    :sswitch_0
    sub-int v4, p5, p3

    .line 645213
    iget v10, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    sub-int v10, v4, v10

    invoke-virtual {v8}, Landroid/view/View;->getMeasuredHeight()I

    move-result v11

    sub-int/2addr v10, v11

    add-int/2addr v9, v2

    iget v11, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    sub-int/2addr v4, v11

    invoke-virtual {v8, v2, v10, v9, v4}, Landroid/view/View;->layout(IIII)V

    goto :goto_4

    .line 645214
    :sswitch_1
    sub-int v11, p5, p3

    .line 645215
    sub-int v4, v11, v10

    div-int/lit8 v4, v4, 0x2

    .line 645216
    iget v12, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    if-ge v4, v12, :cond_6

    .line 645217
    iget v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 645218
    :cond_5
    :goto_6
    add-int/2addr v9, v2

    add-int/2addr v10, v4

    invoke-virtual {v8, v2, v4, v9, v10}, Landroid/view/View;->layout(IIII)V

    goto :goto_4

    .line 645219
    :cond_6
    add-int v12, v4, v10

    iget v13, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    sub-int v13, v11, v13

    if-le v12, v13, :cond_5

    .line 645220
    iget v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    sub-int v4, v11, v4

    sub-int/2addr v4, v10

    goto :goto_6

    .line 645221
    :cond_7
    const/4 v0, 0x4

    goto :goto_5

    .line 645222
    :cond_8
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/3tW;->n:Z

    .line 645223
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/3tW;->o:Z

    .line 645224
    return-void

    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_1
        0x50 -> :sswitch_0
    .end sparse-switch
.end method

.method public final onMeasure(II)V
    .locals 12

    .prologue
    const/16 v1, 0x12c

    const/4 v4, 0x0

    const/high16 v7, -0x80000000

    const/high16 v11, 0x40000000    # 2.0f

    .line 645225
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    .line 645226
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v5

    .line 645227
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 645228
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 645229
    if-ne v3, v11, :cond_0

    if-eq v5, v11, :cond_b

    .line 645230
    :cond_0
    invoke-virtual {p0}, LX/3tW;->isInEditMode()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 645231
    if-eq v3, v7, :cond_1

    .line 645232
    if-nez v3, :cond_1

    move v2, v1

    .line 645233
    :cond_1
    if-eq v5, v7, :cond_b

    .line 645234
    if-nez v5, :cond_b

    .line 645235
    :goto_0
    invoke-virtual {p0, v2, v1}, LX/3tW;->setMeasuredDimension(II)V

    .line 645236
    iget-object v0, p0, LX/3tW;->B:Ljava/lang/Object;

    if-eqz v0, :cond_5

    invoke-static {p0}, LX/0vv;->A(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    move v3, v0

    .line 645237
    :goto_1
    invoke-static {p0}, LX/0vv;->h(Landroid/view/View;)I

    move-result v5

    .line 645238
    invoke-virtual {p0}, LX/3tW;->getChildCount()I

    move-result v6

    .line 645239
    :goto_2
    if-ge v4, v6, :cond_a

    .line 645240
    invoke-virtual {p0, v4}, LX/3tW;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    .line 645241
    invoke-virtual {v7}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v8, 0x8

    if-eq v0, v8, :cond_3

    .line 645242
    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/3tS;

    .line 645243
    if-eqz v3, :cond_2

    .line 645244
    iget v8, v0, LX/3tS;->a:I

    invoke-static {v8, v5}, LX/1uf;->a(II)I

    move-result v8

    .line 645245
    invoke-static {v7}, LX/0vv;->A(Landroid/view/View;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 645246
    sget-object v9, LX/3tW;->a:LX/3tO;

    iget-object v10, p0, LX/3tW;->B:Ljava/lang/Object;

    invoke-interface {v9, v7, v10, v8}, LX/3tO;->a(Landroid/view/View;Ljava/lang/Object;I)V

    .line 645247
    :cond_2
    :goto_3
    invoke-static {v7}, LX/3tW;->j(Landroid/view/View;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 645248
    iget v8, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    sub-int v8, v2, v8

    iget v9, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    sub-int/2addr v8, v9

    invoke-static {v8, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    .line 645249
    iget v9, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    sub-int v9, v1, v9

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    sub-int v0, v9, v0

    invoke-static {v0, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 645250
    invoke-virtual {v7, v8, v0}, Landroid/view/View;->measure(II)V

    .line 645251
    :cond_3
    :goto_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 645252
    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "DrawerLayout must be measured with MeasureSpec.EXACTLY."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    move v3, v4

    .line 645253
    goto :goto_1

    .line 645254
    :cond_6
    sget-object v9, LX/3tW;->a:LX/3tO;

    iget-object v10, p0, LX/3tW;->B:Ljava/lang/Object;

    invoke-interface {v9, v0, v10, v8}, LX/3tO;->a(Landroid/view/ViewGroup$MarginLayoutParams;Ljava/lang/Object;I)V

    goto :goto_3

    .line 645255
    :cond_7
    invoke-static {v7}, LX/3tW;->d(Landroid/view/View;)Z

    move-result v8

    if-eqz v8, :cond_9

    .line 645256
    invoke-virtual {p0, v7}, LX/3tW;->c(Landroid/view/View;)I

    move-result v8

    and-int/lit8 v8, v8, 0x7

    .line 645257
    and-int/lit8 v9, v8, 0x0

    if-eqz v9, :cond_8

    .line 645258
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Child drawer has absolute gravity "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v8}, LX/3tW;->e(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " but this DrawerLayout"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " already has a drawer view along that edge"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 645259
    :cond_8
    iget v8, p0, LX/3tW;->e:I

    iget v9, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v8, v9

    iget v9, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v8, v9

    iget v9, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-static {p1, v8, v9}, LX/3tW;->getChildMeasureSpec(III)I

    move-result v8

    .line 645260
    iget v9, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v10, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v9, v10

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {p2, v9, v0}, LX/3tW;->getChildMeasureSpec(III)I

    move-result v0

    .line 645261
    invoke-virtual {v7, v8, v0}, Landroid/view/View;->measure(II)V

    goto :goto_4

    .line 645262
    :cond_9
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Child "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " at index "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " does not have a valid layout_gravity - must be Gravity.LEFT, Gravity.RIGHT or Gravity.NO_GRAVITY"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 645263
    :cond_a
    return-void

    :cond_b
    move v1, v0

    goto/16 :goto_0
.end method

.method public final onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    .prologue
    .line 645264
    check-cast p1, Landroid/support/v4/widget/DrawerLayout$SavedState;

    .line 645265
    invoke-virtual {p1}, Landroid/support/v4/widget/DrawerLayout$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/view/ViewGroup;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 645266
    iget v0, p1, Landroid/support/v4/widget/DrawerLayout$SavedState;->a:I

    if-eqz v0, :cond_0

    .line 645267
    iget v0, p1, Landroid/support/v4/widget/DrawerLayout$SavedState;->a:I

    invoke-virtual {p0, v0}, LX/3tW;->b(I)Landroid/view/View;

    move-result-object v0

    .line 645268
    if-eqz v0, :cond_0

    .line 645269
    invoke-direct {p0, v0}, LX/3tW;->k(Landroid/view/View;)V

    .line 645270
    :cond_0
    iget v0, p1, Landroid/support/v4/widget/DrawerLayout$SavedState;->b:I

    const/4 v1, 0x3

    invoke-direct {p0, v0, v1}, LX/3tW;->a(II)V

    .line 645271
    iget v0, p1, Landroid/support/v4/widget/DrawerLayout$SavedState;->c:I

    const/4 v1, 0x5

    invoke-direct {p0, v0, v1}, LX/3tW;->a(II)V

    .line 645272
    return-void
.end method

.method public final onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .prologue
    .line 645273
    invoke-super {p0}, Landroid/view/ViewGroup;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 645274
    new-instance v1, Landroid/support/v4/widget/DrawerLayout$SavedState;

    invoke-direct {v1, v0}, Landroid/support/v4/widget/DrawerLayout$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 645275
    invoke-direct {p0}, LX/3tW;->d()Landroid/view/View;

    move-result-object v0

    .line 645276
    if-eqz v0, :cond_0

    .line 645277
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/3tS;

    iget v0, v0, LX/3tS;->a:I

    iput v0, v1, Landroid/support/v4/widget/DrawerLayout$SavedState;->a:I

    .line 645278
    :cond_0
    iget v0, p0, LX/3tW;->p:I

    iput v0, v1, Landroid/support/v4/widget/DrawerLayout$SavedState;->b:I

    .line 645279
    iget v0, p0, LX/3tW;->q:I

    iput v0, v1, Landroid/support/v4/widget/DrawerLayout$SavedState;->c:I

    .line 645280
    return-object v1
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    const v0, 0x19667ab8

    invoke-static {v8, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v3

    .line 645281
    iget-object v0, p0, LX/3tW;->i:LX/3ty;

    invoke-virtual {v0, p1}, LX/3ty;->b(Landroid/view/MotionEvent;)V

    .line 645282
    iget-object v0, p0, LX/3tW;->j:LX/3ty;

    invoke-virtual {v0, p1}, LX/3ty;->b(Landroid/view/MotionEvent;)V

    .line 645283
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 645284
    and-int/lit16 v0, v0, 0xff

    packed-switch v0, :pswitch_data_0

    .line 645285
    :goto_0
    :pswitch_0
    const v0, 0x487b04d7

    invoke-static {v0, v3}, LX/02F;->a(II)V

    return v1

    .line 645286
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 645287
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    .line 645288
    iput v0, p0, LX/3tW;->u:F

    .line 645289
    iput v4, p0, LX/3tW;->v:F

    .line 645290
    iput-boolean v2, p0, LX/3tW;->r:Z

    .line 645291
    iput-boolean v2, p0, LX/3tW;->s:Z

    goto :goto_0

    .line 645292
    :pswitch_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 645293
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    .line 645294
    iget-object v5, p0, LX/3tW;->i:LX/3ty;

    float-to-int v6, v0

    float-to-int v7, v4

    invoke-virtual {v5, v6, v7}, LX/3ty;->b(II)Landroid/view/View;

    move-result-object v5

    .line 645295
    if-eqz v5, :cond_1

    invoke-static {v5}, LX/3tW;->j(Landroid/view/View;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 645296
    iget v5, p0, LX/3tW;->u:F

    sub-float/2addr v0, v5

    .line 645297
    iget v5, p0, LX/3tW;->v:F

    sub-float/2addr v4, v5

    .line 645298
    iget-object v5, p0, LX/3tW;->i:LX/3ty;

    .line 645299
    iget v6, v5, LX/3ty;->b:I

    move v5, v6

    .line 645300
    mul-float/2addr v0, v0

    mul-float/2addr v4, v4

    add-float/2addr v0, v4

    mul-int v4, v5, v5

    int-to-float v4, v4

    cmpg-float v0, v0, v4

    if-gez v0, :cond_1

    .line 645301
    invoke-direct {p0}, LX/3tW;->d()Landroid/view/View;

    move-result-object v0

    .line 645302
    if-eqz v0, :cond_1

    .line 645303
    invoke-virtual {p0, v0}, LX/3tW;->a(Landroid/view/View;)I

    move-result v0

    if-ne v0, v8, :cond_0

    move v0, v1

    .line 645304
    :goto_1
    invoke-static {p0, v0}, LX/3tW;->a(LX/3tW;Z)V

    .line 645305
    iput-boolean v2, p0, LX/3tW;->r:Z

    goto :goto_0

    :cond_0
    move v0, v2

    .line 645306
    goto :goto_1

    .line 645307
    :pswitch_3
    invoke-static {p0, v1}, LX/3tW;->a(LX/3tW;Z)V

    .line 645308
    iput-boolean v2, p0, LX/3tW;->r:Z

    .line 645309
    iput-boolean v2, p0, LX/3tW;->s:Z

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public final requestDisallowInterceptTouchEvent(Z)V
    .locals 1

    .prologue
    .line 645310
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->requestDisallowInterceptTouchEvent(Z)V

    .line 645311
    iput-boolean p1, p0, LX/3tW;->r:Z

    .line 645312
    if-eqz p1, :cond_0

    .line 645313
    const/4 v0, 0x1

    invoke-static {p0, v0}, LX/3tW;->a(LX/3tW;Z)V

    .line 645314
    :cond_0
    return-void
.end method

.method public final requestLayout()V
    .locals 1

    .prologue
    .line 645315
    iget-boolean v0, p0, LX/3tW;->n:Z

    if-nez v0, :cond_0

    .line 645316
    invoke-super {p0}, Landroid/view/ViewGroup;->requestLayout()V

    .line 645317
    :cond_0
    return-void
.end method

.method public setDrawerLockMode(I)V
    .locals 1

    .prologue
    .line 645318
    const/4 v0, 0x3

    invoke-direct {p0, p1, v0}, LX/3tW;->a(II)V

    .line 645319
    const/4 v0, 0x5

    invoke-direct {p0, p1, v0}, LX/3tW;->a(II)V

    .line 645320
    return-void
.end method

.method public setScrimColor(I)V
    .locals 0

    .prologue
    .line 645321
    iput p1, p0, LX/3tW;->f:I

    .line 645322
    invoke-virtual {p0}, LX/3tW;->invalidate()V

    .line 645323
    return-void
.end method

.method public setStatusBarBackground(I)V
    .locals 1

    .prologue
    .line 645324
    if-eqz p1, :cond_0

    invoke-virtual {p0}, LX/3tW;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, LX/1ED;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_0
    iput-object v0, p0, LX/3tW;->y:Landroid/graphics/drawable/Drawable;

    .line 645325
    return-void

    .line 645326
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setStatusBarBackground(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 645327
    iput-object p1, p0, LX/3tW;->y:Landroid/graphics/drawable/Drawable;

    .line 645328
    return-void
.end method

.method public setStatusBarBackgroundColor(I)V
    .locals 1

    .prologue
    .line 645329
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v0, p1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    iput-object v0, p0, LX/3tW;->y:Landroid/graphics/drawable/Drawable;

    .line 645330
    return-void
.end method
