.class public LX/4m3;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/03R;

.field public b:LX/03R;

.field public c:LX/03R;

.field public d:LX/03R;

.field public e:LX/03R;

.field public f:LX/03R;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 805246
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 805247
    return-void
.end method

.method private static g(LX/03R;)LX/03R;
    .locals 0
    .param p0    # LX/03R;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 805245
    if-nez p0, :cond_0

    sget-object p0, LX/03R;->UNSET:LX/03R;

    :cond_0
    return-object p0
.end method


# virtual methods
.method public final a(LX/03R;)LX/4m3;
    .locals 0

    .prologue
    .line 805243
    iput-object p1, p0, LX/4m3;->a:LX/03R;

    .line 805244
    return-object p0
.end method

.method public final a()Lcom/facebook/stickers/model/StickerCapabilities;
    .locals 7

    .prologue
    .line 805242
    new-instance v0, Lcom/facebook/stickers/model/StickerCapabilities;

    iget-object v1, p0, LX/4m3;->a:LX/03R;

    invoke-static {v1}, LX/4m3;->g(LX/03R;)LX/03R;

    move-result-object v1

    iget-object v2, p0, LX/4m3;->b:LX/03R;

    invoke-static {v2}, LX/4m3;->g(LX/03R;)LX/03R;

    move-result-object v2

    iget-object v3, p0, LX/4m3;->c:LX/03R;

    invoke-static {v3}, LX/4m3;->g(LX/03R;)LX/03R;

    move-result-object v3

    iget-object v4, p0, LX/4m3;->d:LX/03R;

    invoke-static {v4}, LX/4m3;->g(LX/03R;)LX/03R;

    move-result-object v4

    iget-object v5, p0, LX/4m3;->e:LX/03R;

    invoke-static {v5}, LX/4m3;->g(LX/03R;)LX/03R;

    move-result-object v5

    iget-object v6, p0, LX/4m3;->f:LX/03R;

    invoke-static {v6}, LX/4m3;->g(LX/03R;)LX/03R;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/facebook/stickers/model/StickerCapabilities;-><init>(LX/03R;LX/03R;LX/03R;LX/03R;LX/03R;LX/03R;)V

    return-object v0
.end method

.method public final b(LX/03R;)LX/4m3;
    .locals 0

    .prologue
    .line 805240
    iput-object p1, p0, LX/4m3;->b:LX/03R;

    .line 805241
    return-object p0
.end method

.method public final c(LX/03R;)LX/4m3;
    .locals 0

    .prologue
    .line 805232
    iput-object p1, p0, LX/4m3;->c:LX/03R;

    .line 805233
    return-object p0
.end method

.method public final d(LX/03R;)LX/4m3;
    .locals 0

    .prologue
    .line 805238
    iput-object p1, p0, LX/4m3;->d:LX/03R;

    .line 805239
    return-object p0
.end method

.method public final e(LX/03R;)LX/4m3;
    .locals 0

    .prologue
    .line 805236
    iput-object p1, p0, LX/4m3;->e:LX/03R;

    .line 805237
    return-object p0
.end method

.method public final f(LX/03R;)LX/4m3;
    .locals 0

    .prologue
    .line 805234
    iput-object p1, p0, LX/4m3;->f:LX/03R;

    .line 805235
    return-object p0
.end method
