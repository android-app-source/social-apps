.class public LX/3fx;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/3fx;


# instance fields
.field public final a:LX/3Lz;

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/3Lz;LX/0Or;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/common/hardware/PhoneIsoCountryCode;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3Lz;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 623868
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 623869
    iput-object p1, p0, LX/3fx;->a:LX/3Lz;

    .line 623870
    iput-object p2, p0, LX/3fx;->b:LX/0Or;

    .line 623871
    return-void
.end method

.method public static a(LX/0QB;)LX/3fx;
    .locals 5

    .prologue
    .line 623872
    sget-object v0, LX/3fx;->c:LX/3fx;

    if-nez v0, :cond_1

    .line 623873
    const-class v1, LX/3fx;

    monitor-enter v1

    .line 623874
    :try_start_0
    sget-object v0, LX/3fx;->c:LX/3fx;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 623875
    if-eqz v2, :cond_0

    .line 623876
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 623877
    new-instance v4, LX/3fx;

    invoke-static {v0}, LX/3Ly;->a(LX/0QB;)LX/3Lz;

    move-result-object v3

    check-cast v3, LX/3Lz;

    const/16 p0, 0x15ec

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v4, v3, p0}, LX/3fx;-><init>(LX/3Lz;LX/0Or;)V

    .line 623878
    move-object v0, v4

    .line 623879
    sput-object v0, LX/3fx;->c:LX/3fx;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 623880
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 623881
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 623882
    :cond_1
    sget-object v0, LX/3fx;->c:LX/3fx;

    return-object v0

    .line 623883
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 623884
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;I)Lcom/facebook/user/model/UserPhoneNumber;
    .locals 3

    .prologue
    .line 623885
    new-instance v0, LX/7Hx;

    invoke-direct {v0, p0, p1}, LX/7Hx;-><init>(LX/3fx;Ljava/lang/String;)V

    .line 623886
    new-instance v1, Lcom/facebook/user/model/UserPhoneNumber;

    invoke-virtual {v0}, LX/7Hx;->c()Ljava/lang/String;

    move-result-object v2

    iget-object p0, v0, LX/7Hx;->b:Ljava/lang/String;

    invoke-virtual {v0}, LX/7Hx;->b()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, v2, p0, p1, p2}, Lcom/facebook/user/model/UserPhoneNumber;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    move-object v0, v1

    .line 623887
    return-object v0
.end method

.method public final c(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 623888
    new-instance v0, LX/7Hx;

    invoke-direct {v0, p0, p1}, LX/7Hx;-><init>(LX/3fx;Ljava/lang/String;)V

    invoke-virtual {v0}, LX/7Hx;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 623889
    new-instance v0, LX/7Hx;

    invoke-direct {v0, p0, p1}, LX/7Hx;-><init>(LX/3fx;Ljava/lang/String;)V

    invoke-virtual {v0}, LX/7Hx;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
