.class public LX/3Ro;
.super LX/0Tv;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/3Ro;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Dor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Dor;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 581593
    const-string v10, "tincan"

    const/16 v11, 0x20

    new-instance v0, LX/3Rp;

    invoke-direct {v0}, LX/3Rp;-><init>()V

    new-instance v1, LX/3Rr;

    invoke-direct {v1}, LX/3Rr;-><init>()V

    new-instance v2, LX/3Rs;

    invoke-direct {v2}, LX/3Rs;-><init>()V

    new-instance v3, LX/3Rt;

    invoke-direct {v3}, LX/3Rt;-><init>()V

    new-instance v4, LX/3Ru;

    invoke-direct {v4}, LX/3Ru;-><init>()V

    new-instance v5, LX/3Rw;

    invoke-direct {v5}, LX/3Rw;-><init>()V

    new-instance v6, LX/3Rx;

    invoke-direct {v6}, LX/3Rx;-><init>()V

    new-instance v7, LX/3Rz;

    invoke-direct {v7}, LX/3Rz;-><init>()V

    new-instance v8, LX/3S1;

    invoke-direct {v8}, LX/3S1;-><init>()V

    invoke-static {}, LX/2PF;->b()LX/2PF;

    move-result-object v9

    invoke-static/range {v0 .. v9}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-direct {p0, v10, v11, v0}, LX/0Tv;-><init>(Ljava/lang/String;ILX/0Px;)V

    .line 581594
    iput-object p1, p0, LX/3Ro;->a:LX/0Ot;

    .line 581595
    iput-object p2, p0, LX/3Ro;->b:LX/0Ot;

    .line 581596
    return-void
.end method

.method public static a(LX/0QB;)LX/3Ro;
    .locals 5

    .prologue
    .line 581597
    sget-object v0, LX/3Ro;->c:LX/3Ro;

    if-nez v0, :cond_1

    .line 581598
    const-class v1, LX/3Ro;

    monitor-enter v1

    .line 581599
    :try_start_0
    sget-object v0, LX/3Ro;->c:LX/3Ro;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 581600
    if-eqz v2, :cond_0

    .line 581601
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 581602
    new-instance v3, LX/3Ro;

    const/16 v4, 0x259

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 p0, 0x2a18

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, p0}, LX/3Ro;-><init>(LX/0Ot;LX/0Ot;)V

    .line 581603
    move-object v0, v3

    .line 581604
    sput-object v0, LX/3Ro;->c:LX/3Ro;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 581605
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 581606
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 581607
    :cond_1
    sget-object v0, LX/3Ro;->c:LX/3Ro;

    return-object v0

    .line 581608
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 581609
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private d(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 5

    .prologue
    .line 581610
    iget-object v0, p0, LX/0Tv;->a:LX/0Px;

    move-object v2, v0

    .line 581611
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Tz;

    .line 581612
    iget-object v4, v0, LX/0Tz;->a:Ljava/lang/String;

    move-object v0, v4

    .line 581613
    invoke-static {v0}, LX/0Tz;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const v4, -0x7f6ce96a

    invoke-static {v4}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x3c4ac36a

    invoke-static {v0}, LX/03h;->a(I)V

    .line 581614
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 581615
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 8

    .prologue
    .line 581616
    const/16 v0, 0x17

    move v0, v0

    .line 581617
    if-ge p2, v0, :cond_1

    .line 581618
    invoke-direct {p0, p1}, LX/3Ro;->d(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 581619
    invoke-virtual {p0, p1}, LX/0Tv;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 581620
    :cond_0
    :goto_0
    return-void

    .line 581621
    :cond_1
    :goto_1
    if-ge p2, p3, :cond_0

    .line 581622
    iget-object v0, p0, LX/3Ro;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dor;

    const/16 v7, 0x17

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 581623
    if-lt p2, v7, :cond_3

    move v1, v2

    :goto_2
    const-string v4, "currentVersion (%s) must be greater than %s for incremental upgrade: "

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v3

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-static {v1, v4, v5}, LX/0PB;->checkArgument(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 581624
    packed-switch p2, :pswitch_data_0

    .line 581625
    :goto_3
    move v0, v3

    .line 581626
    if-eqz v0, :cond_2

    .line 581627
    add-int/lit8 p2, p2, 0x1

    goto :goto_1

    .line 581628
    :cond_2
    invoke-direct {p0, p1}, LX/3Ro;->d(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 581629
    invoke-virtual {p0, p1}, LX/0Tv;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 581630
    iget-object v0, p0, LX/3Ro;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "tincan_upgrade"

    const-string v2, "Database not upgraded incrementally from %s to %s"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    move v1, v3

    .line 581631
    goto :goto_2

    .line 581632
    :pswitch_0
    const-string v1, "ALTER TABLE messages ADD COLUMN facebook_hmac BLOB"

    const v2, -0x28feea79

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, -0x2fd0f586

    invoke-static {v1}, LX/03h;->a(I)V

    .line 581633
    const/4 v1, 0x1

    move v3, v1

    .line 581634
    goto :goto_3

    .line 581635
    :pswitch_1
    const-string v1, "ALTER TABLE messages ADD COLUMN expired INTEGER"

    const v2, -0x4c3971f7

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, 0x4242893b

    invoke-static {v1}, LX/03h;->a(I)V

    .line 581636
    const/4 v1, 0x1

    move v3, v1

    .line 581637
    goto :goto_3

    .line 581638
    :pswitch_2
    iget-object v1, v0, LX/Dor;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Dos;

    invoke-virtual {v1, p1}, LX/Dos;->a(Landroid/database/sqlite/SQLiteDatabase;)Z

    move-result v3

    goto :goto_3

    .line 581639
    :pswitch_3
    iget-object v1, v0, LX/Dor;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Dot;

    invoke-virtual {v1, p1}, LX/Dot;->a(Landroid/database/sqlite/SQLiteDatabase;)Z

    move-result v3

    goto :goto_3

    .line 581640
    :pswitch_4
    iget-object v1, v0, LX/Dor;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Dou;

    invoke-virtual {v1, p1}, LX/Dou;->a(Landroid/database/sqlite/SQLiteDatabase;)Z

    move-result v3

    goto :goto_3

    .line 581641
    :pswitch_5
    const-string v1, "pending_sessions"

    invoke-static {v1}, LX/0Tz;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const v2, 0x605ef00a

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, -0x23d11a7c

    invoke-static {v1}, LX/03h;->a(I)V

    .line 581642
    const/4 v1, 0x1

    move v3, v1

    .line 581643
    goto/16 :goto_3

    .line 581644
    :pswitch_6
    new-instance v1, LX/3S1;

    invoke-direct {v1}, LX/3S1;-><init>()V

    move-object v1, v1

    .line 581645
    invoke-virtual {v1, p1}, LX/0Tz;->b(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 581646
    const/4 v1, 0x1

    move v3, v1

    .line 581647
    goto/16 :goto_3

    .line 581648
    :pswitch_7
    invoke-static {}, LX/2PF;->b()LX/2PF;

    move-result-object v1

    invoke-virtual {v1, p1}, LX/0Tz;->b(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 581649
    const/4 v1, 0x1

    move v3, v1

    .line 581650
    goto/16 :goto_3

    .line 581651
    :pswitch_8
    iget-object v1, v0, LX/Dor;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Dov;

    .line 581652
    iget-object v2, v1, LX/Dov;->a:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Dok;

    invoke-static {p1, v2}, LX/Dow;->a(Landroid/database/sqlite/SQLiteDatabase;LX/Dok;)[B

    move-result-object v2

    .line 581653
    if-nez v2, :cond_4

    invoke-static {p1}, LX/Dov;->b(Landroid/database/sqlite/SQLiteDatabase;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 581654
    const/4 v2, 0x0

    .line 581655
    :goto_4
    move v3, v2

    .line 581656
    goto/16 :goto_3

    :cond_4
    const/4 v2, 0x1

    goto :goto_4

    :pswitch_data_0
    .packed-switch 0x17
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public final b(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0

    .prologue
    .line 581657
    return-void
.end method
